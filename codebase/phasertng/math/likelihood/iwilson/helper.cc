//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/iwilson/helper.h>
#include <scitbx/array_family/misc_functions.h>
#include <scitbx/constants.h>
#include <complex>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
}

typedef std::complex<double> cmplex;
using namespace scitbx;

namespace phasertng { namespace likelihood { namespace iwilson {

  double
  DobsSigaSqr(
      double SSQR,double SigaSqr,double FS,double DOBS,double DRMS,double BFAC,double G_DRMS)
  {
    double thisDobsSigaSqr(0);
    double minusSsqrOn2 = -SSQR/2.0;
    double minusSsqrTwoPiSqrOn3 = -(constants::two_pi_sq/3.)*SSQR;
    thisDobsSigaSqr = SigaSqr;
    thisDobsSigaSqr *= FS; //SCATTERING[modlid]/(TS);
    thisDobsSigaSqr *= fn::pow2(DOBS);
    thisDobsSigaSqr *= std::exp(2*minusSsqrTwoPiSqrOn3*DRMS); //Vrms-shift term not squared
    thisDobsSigaSqr *= std::exp(minusSsqrOn2*BFAC); //this is I/F
    thisDobsSigaSqr *= G_DRMS;
    return thisDobsSigaSqr;
  }

  cmplex
  EcalcTerm(
      double DobsSigaSqr,double EPS,double NSYMP,millnx SymHKL,dvect3 TRA,double symTra)
  {
    cmplex scalefac = DobsSigaSqr;
           scalefac *= EPS;
           scalefac /= NSYMP;
    double dphi_arg = SymHKL*TRA;
           dphi_arg += symTra;
    cmplex dphi = tbl_cos_sin.get(dphi_arg);
           scalefac = std::sqrt(scalefac);
           scalefac *= dphi; //dphi after square root!!
    return scalefac;
  }

}}} //phasertng
