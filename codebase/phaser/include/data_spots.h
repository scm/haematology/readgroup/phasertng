//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_SPOTS_CLASS__
#define __PHASER_DATA_SPOTS_CLASS__
#include <phaser/main/Phaser.h>

namespace phaser {

class data_spots
{
  public:
    data_spots(af_float F_=af_float(),
               af_float SIGF_=af_float(),
               af_bool PRESENT_=af_bool())
    {
      clear();
      F = F_;
      SIGF = SIGF_;
      PRESENT = PRESENT_;
      INPUT_INTENSITIES=false;
    }
    data_spots(const data_spots & init)
    { set_data_spots(init); }
    const data_spots& operator=(const data_spots& right)
    { if (&right != this) set_data_spots(right); return *this; }

    void set_data_spots(const data_spots& init)
    {
      // deep copy of af::shared for threads
      F_ID = init.F_ID;
      SIGF_ID = init.SIGF_ID;
      INPUT_INTENSITIES = init.INPUT_INTENSITIES;
      F = init.F.deep_copy();
      SIGF = init.SIGF.deep_copy();
      PRESENT = init.PRESENT.deep_copy();
    }

  af_float  F,SIGF;
  bool INPUT_INTENSITIES;
  std::string F_ID,SIGF_ID;
  af_bool   PRESENT;

  bool set_default_id(std::string tag)
  {
    if (F.size() && F_ID == "") F_ID = "F"+tag;
    if (SIGF.size() && SIGF_ID == "") SIGF_ID = "SIGF"+tag;
    std::set<std::string> labin;
    if (F.size()) labin.insert(F_ID);
    if (SIGF.size()) labin.insert(SIGF_ID);
    int count_labin(0);
    if (F.size()) count_labin++;
    if (SIGF.size()) count_labin++;
    //not Feff, PRESENT
    //detect duplicates
    return (count_labin != labin.size());
  }

  bool no_data()
  {
    for (unsigned r = 0; r < F.size(); r++) if (F[r] != 0) return true;
    return true;
  }


  void erase(int r)
  {
    if (r<F.size())      F.erase(F.begin()+r);
    if (r<SIGF.size())   SIGF.erase(SIGF.begin()+r);
    if (r<PRESENT.size()) PRESENT.erase(PRESENT.begin()+r);
  }

  void clear()
  {
    F_ID = SIGF_ID = "";
    F.clear();
    SIGF.clear();
    PRESENT.clear();
  }

};

} //phaser

#endif
