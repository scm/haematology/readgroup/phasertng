# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
from __future__ import absolute_import, division, print_function

import sys, zmq, subprocess, time, traceback, zlib, io, os, shutil, math, os.path
import xml.etree.ElementTree as et

if sys.version_info[0] < 3:
  print("VoyagerViewer GUI must be run from Python 3")
  sys.exit(-42)

from crys3d.hklviewer.qt import Qt, QtCore, QCoreApplication, QEvent, QItemSelectionModel, QSize, QSettings, QTimer, QUrl
from crys3d.hklviewer.qt import (  QAction, QCheckBox, QComboBox, QDialog,
    QFileDialog, QFrame, QGridLayout, QGroupBox, QHeaderView, QHBoxLayout, QLabel, QLineEdit,
    QMainWindow, QMenu, QMenuBar, QProgressBar, QPushButton, QRadioButton, QRect,
    QScrollBar, QSizePolicy, QSlider, QSpinBox, QStyleFactory, QStatusBar, QTableView, QTableWidget,
    QTableWidgetItem, QTabWidget, QTextEdit, QTextBrowser, QWidget )

from crys3d.hklviewer.qt import QColor, QFont, QCursor, QDesktopServices, QtWidgets
from crys3d.hklviewer.qt import ( QWebEngineView, QWebEngineProfile, QWebEnginePage )
import VoyagerGui
from phasertng import callbacks,Phasertng,Identifier,enum_entry_data,enum_other_data
from phasertng.voyager.picard import inner_picard
from phasertng.voyager.scotty import inner_scotty
from phasertng.voyager.nomad import inner_nomad
from phasertng.voyager.xtricorder import inner_xtricorder
from phasertng.voyager.exocomp import inner_exocomp
from phasertng.voyager.logfile import inner_voyager_logfile
import logging

from PySide2.QtCore import ( QObject )   # special import

def stof(s):
   if s is not None:
     s = s.strip()
     if len(s) > 1 and s[0] == '"' and s[-1] == '"':
       s = s[1:-1]
   return s

class WorkerSignals(QtCore.QObject):
    '''
    Defines the signals available from a running worker thread.
    Supported signals are:
    - finished: No data
    - error:`tuple` (exctype, value, traceback.format_exc() )
    - result: `object` data returned from processing, anything
    - progress: `tuple` indicating progress metadata
    '''
    finished = QtCore.Signal()
    error = QtCore.Signal(tuple)
    result = QtCore.Signal(object)
   #progress = QtCore.Signal(tuple)
    progress = QtCore.Signal(int)

class WorkerXtricorder(QtCore.QRunnable):
    def __init__(self, basetng, user_phil_str, *args, **kwargs):
      QtCore.QRunnable.__init__(self)
      self.basetng = basetng
      self.user_phil_str = user_phil_str
      self.signals = WorkerSignals()
      self.args = args
      self.kwargs = kwargs
      self.kwargs['progress_callback'] = self.signals.progress
    def run(self):
      inner_xtricorder(
            basetng=self.basetng,
            user_phil_str=self.user_phil_str,
            *self.args, **self.kwargs,
           )
      self.signals.finished.emit()  # Done

class WorkerScotty(QtCore.QRunnable):
    def __init__(self, basetng, user_phil_str):
      QtCore.QRunnable.__init__(self)
      self.basetng = basetng
      self.user_phil_str = user_phil_str
      self.signals = WorkerSignals()
    def run(self):
      inner_scotty(
            basetng=self.basetng,
            user_phil_str=self.user_phil_str)
      self.signals.finished.emit()  # Done

class WorkerNomad(QtCore.QRunnable):
    def __init__(self, basetng, user_phil_str):
      QtCore.QRunnable.__init__(self)
      self.basetng = basetng
      self.user_phil_str = user_phil_str
      self.signals = WorkerSignals()
    def run(self):
      inner_nomad(
            basetng=self.basetng,
            user_phil_str=self.user_phil_str)
      self.signals.finished.emit()  # Done

class WorkerPicard(QtCore.QRunnable):
    def __init__(self, basetng, user_phil_str, *args, **kwargs):
      QtCore.QRunnable.__init__(self)
      self.basetng = basetng
      self.user_phil_str = user_phil_str
      self.signals = WorkerSignals()
      self.args = args
      self.kwargs = kwargs
      self.kwargs['progress_callback'] = self.signals.progress
    def run(self):
      inner_picard(
            basetng=self.basetng,
            user_phil_str=self.user_phil_str,
            *self.args, **self.kwargs)
      self.signals.finished.emit()  # Done

class WorkerPyvis(QtCore.QRunnable):
    def __init__(self, database_directory):
      QtCore.QRunnable.__init__(self)
      self.database_directory = database_directory
      self.signals = WorkerSignals()
    def run(self):
      inner_exocomp(
            walk_dir=self.database_directory,
            io_database=self.database_directory,
            verbose=False,
            io_output=self.database_directory)
      self.signals.finished.emit()  # Done

class WorkerLogfile(QtCore.QRunnable):
    def __init__(self, database_directory, node_number):
      QtCore.QRunnable.__init__(self)
      self.database_directory = database_directory
      self.node_number = node_number
      self.signals = WorkerSignals()
    def run(self):
      inner_voyager_logfile(
              walk_dir=self.database_directory,
              io_output=self.database_directory,
              verbose=False,
              node_number=int(self.node_number))
      self.signals.finished.emit()  # Done

class MyQMainWindow(QMainWindow):
  def __init__(self, parent):
    super(MyQMainWindow, self).__init__()
    self.parent = parent

  def closeEvent(self, event):
    self.parent.closeEvent(event)
    event.accept()

class MyQMainDialog(QDialog):
  def __init__(self, parent):
    super(MyQMainDialog, self).__init__()
    self.parent = parent

  def closeEvent(self, event):
    self.parent.closeEvent(event)
    event.accept()

class VoyagerViewer(VoyagerGui.Ui_MainWindow):
  def __init__(self, thisapp, isembedded=False): #, cctbxpython=None):
    self.settings = QSettings("Voyager")
    # qversion() comes out like '5.12.5'. We just want '5.12'
    self.Qtversion = "Qt" + ".".join( QtCore.qVersion().split(".")[0:2])
    self.datatypedict = { }
    self.browserfontsize = None
    self.mousespeedscale = 2000
    self.isembedded = isembedded
    print("version " + self.Qtversion)
    self.colnames_select_dict = {}
    self.lasttime = time.monotonic()
    self.build = os.environ.get("LIBTBX_BUILD")
    self.python = os.environ.get("LIBTBX_PYEXE")
    self.scripts = "../modules/phasertng/phasertng/scripts"
    self.tutorial = "../modules/phasertng/tutorial"
    self.pool = QtCore.QThreadPool().globalInstance()

    if isembedded:
      self.window = MyQMainDialog(self)
      self.window.hide()
    else:
      self.window = MyQMainWindow(self)
    self.setupUi(self.window)

    #Tab1
    self.documentation.clicked.connect(self.onDocumentation)
    self.combo_tute.activated[str].connect(self.onLoadTute)
    self.edit_tmp.setText("/tmp/Voyager/tutorial")
    self.dot_tmp.clicked.connect(self.onDotTmpFileButtonClicked)
    self.reset_tutorial.clicked.connect(self.onClearTute)
    #Tab2
    self.dot_data.clicked.connect(self.onDataFileButtonClicked)
    self.edit_database.textChanged.connect(self.onEditDatabase)
    self.dot_database.clicked.connect(self.onDatabaseDirButtonClicked)
    self.edit_phil.textChanged.connect(self.onEditPhil)
    self.dot_phil.clicked.connect(self.onPhilFileButtonClicked)
    self.edit_protocol.activated[str].connect(self.onComboProtocol)
    self.combo_protocol = "scotty"
    self.run_protocol.clicked.connect(self.onRunButtonClicked)
    #Tab3
    self.srfpdir = None
    self.target2 = None
    self.edit_tree.textChanged.connect(self.onEditTree)
    self.dot_tree.clicked.connect(self.onTreeDirButtonClicked)
    self.show_1.clicked.connect(self.onShow1)
    self.update_1.clicked.connect(self.onUpdate1)
    #Tab4
    self.nodetext_1.textChanged.connect(self.ValidateNode1)
    self.nodetext_2.textChanged.connect(self.ValidateNode2)
    self.radioButton_1.toggled.connect(self.onRadioButton1)
    self.radioButton_2.toggled.connect(self.onRadioButton2)
    self.radioButton_3.toggled.connect(self.onRadioButton3)
    self.radioButton_4.toggled.connect(self.onRadioButton4)
    self.radioButton_5.toggled.connect(self.onRadioButton5)
   #below fires on single key press not return
    self.spinBox_2.valueChanged.connect(self.ValidateSpinBox2)
    self.counddown = QTimer()
    self.counddown.timeout.connect(self.updateCountdown)

    self.onDocumentation()
    self.window.show()
    self.nodetext_2.setText("187347113")
    self.onRadioButton345("summary")

  #Tab1
  def onDocumentation(self):
    try:
    # self.BrowserBox.load(QUrl("http://www.phaser.cimr.cam.ac.uk/"))
      build = os.environ.get("LIBTBX_BUILD")
      html = os.path.abspath(os.path.join(build,"../modules/phasertng/doc/splash.html"))
      local_url = QUrl.fromLocalFile(html)
      self.BrowserBox.load(QUrl(local_url))
    except Exception:
      self.BrowserBox.setHtml("<html>http://www.phaser.cimr.cam.ac.uk/ is not available</html>")
  def onLoadTute(self,text):
    self.reset_tutorial.setEnabled(True) #safety only allow clear once per session
    if text == "---":
      text = ""
    else:
      text = text.replace("/","_")
      text = text.replace(" ","_")
    self.edit_data.setText(os.path.abspath(os.path.join(self.build,self.tutorial,"gui",text)))
    self.edit_database.setText(os.path.abspath(os.path.join(self.edit_tmp.text(),text)))
    self.edit_phil.setText(os.path.abspath(os.path.join(self.build,self.tutorial,"gui",text,"tutorial_extras.phil")))
    self.out_phil.setText(os.path.abspath(os.path.join(self.edit_database.text(),text,"voyager.phil")))
    self.edit_tree.setText(self.edit_database.text())
    if (self.combo_tute.currentIndex() == 0):
      self.label_3.setText("")
    else:
      self.label_3.setText("Tutorial data " + self.combo_tute.itemText(self.combo_tute.currentIndex()) + " loaded")
  def onClearTute(self):
    tutedir = stof(self.edit_database.text())
    if "Tutorial" in tutedir and "Voyager" in tutedir: #paranoia
      dtext = os.path.abspath(os.path.join(tutedir))
      if os.path.exists(dtext):
        shutil.rmtree(dtext)
    self.reset_tutorial.setEnabled(False) #safety only allow clear once per session
  def onDotTmpFileButtonClicked(self):
    tmpdir = QFileDialog.getExistingDirectory()
    if tmpdir:
      self.edit_tmp.setText(tmpdir)

  #Tab2
  def onDataFileButtonClicked(self):
    data_file = QFileDialog.getOpenFileName(filter='*.mtz')[0]
    if data_file:
      self.edit_data.setText(data_file)
  def onDataFileButtonClicked(self):
    data_file = QFileDialog.getOpenFileName(filter='*.mtz')[0]
    if data_file:
      self.edit_data.setText(data_file)
  def onDatabaseDirButtonClicked(self):
    database_directory = QFileDialog.getExistingDirectory()
    if database_directory:
      self.edit_database.setText(database_directory)
  def onEditPhil(self):
      try:
        phil = open(self.edit_phil.text(),"r").read()
        self.plainTextEdit.setPlainText(phil)
      except Exception:
        self.textInfo.appendPlainText("Error: Phil file not read")
  def onPhilFileButtonClicked(self):
    phil_file = QFileDialog.getOpenFileName(filter="(*.phil *.eff)")[0]
    if phil_file:
      self.edit_phil.setText(phil_file)
  def onEditDatabase(self):
    phil = os.path.join(self.edit_database.text(),"voyager.phil")
    self.out_phil.setText(phil)
  def onComboProtocol(self,text):
    self.combo_protocol = text
    self.edit_protocol.currentIndex()
    self.status_protocol.setText("waiting")
    #same index in the two menus, so no need for text lookup
    self.view_protocol.setCurrentIndex(self.edit_protocol.currentIndex())
  def onRunButtonClicked(self):
    if stof(self.out_phil.text()) == "":
      return
    user_phil_str = """
phasertng.suite.database = "database_directory"
phasertng.biological_unit_builder.directory = "data_file"
phasertng.xmlout = "xmlout_file"
"""
    data = stof(self.edit_data.text())
    database = stof(self.edit_database.text())
    if data != "" and database != "":
      user_phil_str = user_phil_str.replace("data_file",str(data))
      user_phil_str = user_phil_str.replace("database_directory",str(database))
      xmlout = self.combo_protocol.replace(" ","") + ".xml"
      user_phil_str = user_phil_str.replace("xmlout_file",str(xmlout))
      dbdir = stof(self.edit_database.text())
      try:
        phil = open(self.edit_phil.text(),"r").read()
        user_phil_str = user_phil_str + "\n" + phil
      except Exception:
        self.textInfo.appendPlainText("Error: Phil file not read")
      os.makedirs(os.path.dirname(self.out_phil.text()), exist_ok=True)
      try:
        with open(self.out_phil.text(), "w") as f:
          f.write(user_phil_str)
          f.close()
      except Exception:
        self.textInfo.appendPlainText("Error: Phil file not written")
      #must write the file before setting the edit_phil, because this triggers read
      self.edit_tree.setText(dbdir)
    print("Starting voyager: " + self.combo_protocol + "\n",user_phil_str)
    self.status_protocol.setText("running")
    basetng = Phasertng("VIEWER")
    if (self.combo_protocol == "scotty"):
      worker = WorkerScotty(basetng=basetng,user_phil_str=user_phil_str)
    elif (self.combo_protocol == "nomad"):
      worker = WorkerNomad(basetng=basetng,user_phil_str=user_phil_str)
    elif (self.combo_protocol == "xtricorder"):
      worker = WorkerXtricorder(basetng=basetng,user_phil_str=user_phil_str)
    elif (self.combo_protocol == "bones"):
      dryline = "picard.dryrun = True"
      user_phil_str = user_phil_str + "\n" + dryline
      worker = WorkerPicard(basetng=basetng,user_phil_str=user_phil_str)
    elif (self.combo_protocol == "picard"):
      worker = WorkerPicard(basetng=basetng,user_phil_str=user_phil_str)
    worker.signals.finished.connect(self.StatusProtocolFinished)
    self.pool.start(worker)

  #Tab3
  def onEditTree(self):
   #self.nodetext_1.setText("")
   #self.nodetext_2.setText("")
    #clear the nodes because they are no longer valid
    return
  def onTreeDirButtonClicked(self):
    tree_directory = QFileDialog.getExistingDirectory()
    if tree_directory:
      self.edit_tree.setText(tree_directory)
  def printTree(self,f):
    tree = et.fromstring(f) #pointer to top element of tree
    self.treeWidget.clear()
    self.treeWidget.setColumnCount(1)
    a = QtWidgets.QTreeWidgetItem([tree.tag])
    self.treeWidget.addTopLevelItem(a)
    def displaytree(a,s):
      for child in s:
        if child.get('info') is None:
          branch = QtWidgets.QTreeWidgetItem([child.tag.replace("_"," ")])
        else:
          branch = QtWidgets.QTreeWidgetItem([child.tag.replace("_"," ") + " " + child.get('info')])
        a.addChild(branch)
        a.setExpanded(True)
        displaytree(branch,child)
      if s.text is not None and len(s.text.strip()) > 0:
        content = s.text
        a.addChild(QtWidgets.QTreeWidgetItem([content]))
    displaytree(a,tree)
  def onTreeItemClicked(self):
    item = self.treeWidget.currentItem()
  # print the paths to the nodes, if you want them
  # print(item.text(0))
  # print(self.getParentPath(item))
  def getParentPath(self,item):
    def getParent(item,outstring):
      if item is not None:
        if item.parent() is None:
          return outstring
        outstring = item.text(0) + "/" + outstring
        return getParent(item.parent(),outstring)
    output = getParent(item.parent(),item.text(0))
    return output
  def updateTree(self, f):
    self.printTree(f)
   #self.stepLabel.setText(f"Long-Running Step: {n}")
  def StatusProtocolFinished(self):
    self.status_protocol.setText("finished")
   #self.onUpdate1() #finish with dag
   #self.onShow1() #finish with tree
  def LoadPyvis(self):
    html = None
    try:
      dbdir = stof(self.edit_tree.text())
      html = os.path.abspath(os.path.join(dbdir, "exocomp.html"))
      local_url = QUrl.fromLocalFile(html)
      self.BrowserBox.load(QUrl(local_url))
    # self.edit_html.setText(html)
    except Exception:
      self.BrowserBox.setHtml("<html>File read failure</html>")
      if html is not None:
        self.textInfo.appendPlainText("Error: File read failure for " + html)
  def onShow1(self):
    dbdir = stof(self.edit_tree.text())
    xmlout = None
    try:
      #file already written by basetng
      xmlout = str(self.view_protocol.currentText());
      xmlout = xmlout + ".xml"
      xmlout = os.path.join(dbdir,xmlout)
      xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
      f = open(xmlout,"r").read()
      self.printTree(f)
      self.treeWidget.itemClicked.connect(self.onTreeItemClicked)
    except Exception:
      self.printTree("<Tree>File read failure</Tree>")
      if xmlout is not None:
        self.textInfo.appendPlainText("Error: File read failure for " + xmlout)
  def onUpdate1(self):
    dbdir = stof(self.edit_tree.text())
    worker = WorkerPyvis(database_directory=dbdir)
    worker.signals.finished.connect(self.StatusProtocolFinished)
    worker.signals.finished.connect(self.LoadPyvis)
    self.pool.start(worker)


  #Tab4
  def ValidateSpinBox2(self,val): #srf angles
    #valueChanged means val is an int (?)
    new_val = 5*round(float(val)/float(5)) # one way to fix
    if new_val < 5: new_val = 5
    if new_val > 180: new_val = 180
    self.spinBox_2.setValue(int(new_val))
    self.radioButton_2.setChecked(True)
    self.onRadioButton2()
  def ValidateNode1(self):
    val = self.nodetext_1.text()
    try:
      if val.isnumeric():
        if int(val) > 0:
          target = Identifier()
          target.initialize_from_ullint(int(val))
          target.initialize_tag("srfp")
          dbdir = stof(self.edit_database.text())
          self.srfpdir = os.path.abspath(os.path.join(dbdir, target.database_subdir()))
          if os.path.exists(self.srfpdir):
            return
    except Exception:
      self.textInfo.appendPlainText("Error: Node not valid "+ val)
    self.srfpdir = None
  def onRadioButton1(self):
      self.ValidateNode1()
      if self.srfpdir is None:
        self.BrowserBox.load("")
        return
      index = "00" + str(self.spinBox_2.value())
      stem_ext = 'chi-' + index[-3:] + ".png"
      html = None
      for path in os.listdir(self.srfpdir):
        if path.endswith(stem_ext):
          html = os.path.join(self.srfpdir,path)
          local_url = QUrl.fromLocalFile(html)
         #self.BrowserBox.page().setBackgroundColor(Qt.white)
          self.BrowserBox.load(QUrl(local_url))
          break
      if html is None:
        self.BrowserBox.load("")
        self.textInfo.appendPlainText("Error: Node not self rotation function " + self.nodetext_1.text())
  def onRadioButton2(self):
      self.ValidateNode1()
      if self.srfpdir is None:
        self.BrowserBox.load("")
        return
      try:
        html = os.path.join(self.srfpdir,"self_rotation_function_peaks.png")
        local_url = QUrl.fromLocalFile(html)
       #self.BrowserBox.page().setBackgroundColor(Qt.white)
        self.BrowserBox.load(QUrl(local_url))
      except Exception:
        self.textInfo.appendPlainText("Error: Node not self rotation function " + self.nodetext_1.text())
  def ValidateNode2(self):
    val = self.nodetext_2.text()
    try:
      if val.isnumeric():
        if int(val) > 0:
          target = Identifier()
          target.initialize_from_ullint(int(val))
          target.initialize_tag("pathway")
          dbdir = stof(self.edit_database.text())
          if os.path.exists(dbdir):
            worker = WorkerLogfile(database_directory=dbdir,node_number=val)
            self.pool.start(worker)
            self.target2 = target
            return
    except Exception:
      self.textInfo.appendPlainText("Error: Node not valid "+ val)
    self.target2 = None #flag for passing validation
  def onRadioButton345(self,combo_3):
      self.ValidateNode2()
      if self.target2 is None:
        self.BrowserBox.load("")
        return
      v = None
      if combo_3 == "summary":
        v = enum_other_data.summary_log
      elif combo_3 == "standard":
        v = enum_other_data.logfile_log
      elif combo_3 == "verbose":
        v = enum_other_data.verbose_log
      logfile = None
      try:
        logfile = os.path.abspath(os.path.join(self.edit_database.text(), self.target2.other_data_stem_ext(v)))
        self.textInfo.appendPlainText("File: " + logfile)
        local_url = QUrl.fromLocalFile(logfile)
        self.BrowserBox.load(QUrl(local_url))
      except Exception:
        self.BrowserBox.load("")
        if logfile is not None:
          self.textInfo.appendPlainText("Error: File read failure " + logfile)
        else:
          self.textInfo.appendPlainText("Error: File read failure")
  def onRadioButton3(self):
    self.onRadioButton345(combo_3="summary")
  def onRadioButton4(self):
    self.onRadioButton345(combo_3="standard")
  def onRadioButton5(self):
    self.onRadioButton345(combo_3="verbose")

  def closeEvent(self, event):
    self.closing = True
    print("Voyager closing down")
    if not self.isembedded:
      event.accept()

  def onBackButtonClicked(self):
    self.BrowserBox.page().triggerAction(QWebEnginePage.Back)

  def onFailed(err):
    raise err

  def updateCountdown(self):
    if self.start_time == 0:
        self.view_progress.setText("now")
    elif self.start_time == 1:
        self.view_progress.setText("in 1 second")
    elif self.start_time == 2:
        self.view_progress.setText("in 2 seconds")
    elif self.start_time == 3:
        self.view_progress.setText("in 3 seconds")
    else:
        self.tabWidget.setCurrentIndex(1) #move to view tab
        self.onUpdate1() #start with dag
        self.onShow1() #start with tree
        self.view_progress.setText("waiting")
        self.counddown.stop()
    self.start_time = self.start_time - 1

  def restartTimer(self):
    self.counddown.stop()
    self.start_time = 3
    self.counddown.start(1000) #milliseconds

def run(isembedded=False, chimeraxsession=None):
  try:
    from crys3d.hklviewer.qt import QApplication
    app = QApplication(sys.argv)

    VoyagerGuiObj = VoyagerViewer(app, isembedded)

    ret = app.exec_()

  except Exception as e:
    print( str(e)  +  traceback.format_exc(limit=10) )

if __name__ == "__main__":
  run()
