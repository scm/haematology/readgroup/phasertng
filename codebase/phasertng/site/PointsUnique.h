//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PointsUnique_class__
#define __phasertng_PointsUnique_class__
#include <phasertng/site/SiteList.h>
#include <cctbx/crystal/close_packing.h>
#include <cctbx/crystal/direct_space_asu.h>

namespace phasertng {

class PointsUnique : public SiteList
{
  private:
   cctbx::crystal::close_packing::hexagonal_sampling_generator< double > generator;

  public:
    PointsUnique(
      cctbx::sgtbx::change_of_basis_op const &,
      cctbx::crystal::direct_space_asu::float_asu<double> const&,
      af::tiny< bool, 3 > const &,
      double const &);

  public:
    bool         at_end();
    void         restart(unsigned=0);
    dvect3       next_site();
    std::size_t  count_sites();
    af_dvect3    all_sites();
    double       point_distance();
};

}
#endif
