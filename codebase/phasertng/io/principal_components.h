//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_principal_components__class__
#define __phasertng_principal_components__class__
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/sym_mat3.h>
#include <scitbx/array_family/shared.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::sym_mat3<double> dmat6;
typedef scitbx::af::shared<dvect3> af_dvect3;

namespace phasertng {

class principal_components
{
  public:
    dvect3 PT = {0,0,0};
    dmat33 PR = {1,0,0, 0,1,0, 0,0,1};
    //this will be the centre if the MT is not wrt another structure e.g. gyred

    void set(af_dvect3);
};

} //phasertng
#endif
