#ifndef __phasertng_inputbase_class__
#define __phasertng_inputbase_class__
#include <autogen/include/Scope.h>
#include <phasertng/main/CCP4base.h>
#include <phasertng/type/atom_selection.h>
#include <phasertng/type/boolean.h>
#include <phasertng/type/choice.h>
#include <phasertng/type/percent.h>
#include <phasertng/type/percent_paired.h>
#include <phasertng/type/int_number.h>
#include <phasertng/type/int_paired.h>
#include <phasertng/type/int_vector.h>
#include <phasertng/type/int_numbers.h>
#include <phasertng/type/uuid_number.h>
#include <phasertng/type/uuid_numbers.h>
#include <phasertng/type/flt_number.h>
#include <phasertng/type/flt_paired.h>
#include <phasertng/type/flt_vector.h>
#include <phasertng/type/flt_cell.h>
#include <phasertng/type/flt_matrix.h>
#include <phasertng/type/flt_numbers.h>
#include <phasertng/type/string.h>
#include <phasertng/type/strings.h>
#include <phasertng/type/path.h>
#include <phasertng/type/spacegroup.h>
#include <phasertng/type/mtzcol.h>
#include <type_traits>
#include <numeric>
#include <algorithm>
#include <set>
#include <map>

namespace phasertng {

class InputBase: public CCP4base
{
  public:

  InputBase() {}
  virtual ~InputBase() {}

  //User defined and analysis of keywords for modes
  //  key is mode, value is all keywords or master keywords
  //phil_parameters are the user-defined keywords for each mode
  //not used internally, but allows Phasertng to be queried for input parameters for mode
  std::map<std::string,std::set<const char*>>     phil_input_parameters; //from master phil
  std::map<std::string,std::set<const char*>>     phil_result_parameters; //from master phil
  //input_parameters are all the keywords during course of Phasertng instantiation
  mutable  std::multimap<std::string,std::string>  input_parameters; //all parameters read as input
  //result_parameters are all the keywords that were changed in the run
  //these are only modified through calls to input object, therefore
  //it doesn't need to be made mutable - in fact, best if it isn't
           std::multimap<std::string,std::string>  result_parameters; //all parameters changed
  std::string running_mode = "chk";
  int         generic_int = 0; //flag for interpretation of generic_string eg pdb in memory
  std::string generic_string = ""; //must be flagged for interpretation eg pdb in memory

  //mtzcol is public because we want to loop over all of the entries
  std::map<std::string,type::mtzcol>         value__mtzcol = {};

  public:

  std::set<std::string> present_in_cards = {};

  std::map<std::string,type::boolean>        value__boolean = {};
  std::map<std::string,type::choice>         value__choice = {};
  std::map<std::string,type::percent>        value__percent = {};
  std::map<std::string,type::percent_paired> value__percent_paired = {};
  std::map<std::string,type::int_number>     value__int_number = {};
  std::map<std::string,type::int_paired>     value__int_paired = {};
  std::map<std::string,type::int_vector>     value__int_vector = {};
  std::map<std::string,type::int_numbers>    value__int_numbers = {};
  std::map<std::string,type::uuid_number>    value__uuid_number = {};
  std::map<std::string,type::uuid_numbers>   value__uuid_numberS = {};
  std::map<std::string,type::flt_number>     value__flt_number = {};
  std::map<std::string,type::flt_paired>     value__flt_paired = {};
  std::map<std::string,type::flt_vector>     value__flt_vector = {};
  std::map<std::string,type::flt_cell>       value__flt_cell = {};
  std::map<std::string,type::flt_matrix>     value__flt_matrix = {};
  std::map<std::string,type::flt_numbers>    value__flt_numbers = {};
  std::map<std::string,type::string>         value__string = {};
  std::map<std::string,type::strings>        value__strings = {};
  std::map<std::string,type::path>           value__path = {};
  std::map<std::string,type::atom_selection> value__atom_selection = {};
  std::map<std::string,type::spacegroup>     value__spacegroup = {};

  std::map<std::string,std::vector<type::boolean> >        array__boolean = {};
  std::map<std::string,std::vector<type::choice> >         array__choice = {};
  std::map<std::string,std::vector<type::percent> >        array__percent = {};
  std::map<std::string,std::vector<type::int_number> >     array__int_number = {};
  std::map<std::string,std::vector<type::int_paired> >     array__int_paired = {};
  std::map<std::string,std::vector<type::int_vector> >     array__int_vector = {};
  std::map<std::string,std::vector<type::int_numbers> >    array__int_numbers = {};
  std::map<std::string,std::vector<type::uuid_number> >    array__uuid_number = {};
  std::map<std::string,std::vector<type::uuid_numbers> >   array__uuid_numberS = {};
  std::map<std::string,std::vector<type::flt_number> >     array__flt_number = {};
  std::map<std::string,std::vector<type::flt_paired> >     array__flt_paired = {};
  std::map<std::string,std::vector<type::flt_vector> >     array__flt_vector = {};
  std::map<std::string,std::vector<type::flt_cell> >       array__flt_cell = {};
  std::map<std::string,std::vector<type::flt_matrix> >     array__flt_matrix = {};
  std::map<std::string,std::vector<type::flt_numbers> >    array__flt_numbers = {};
  std::map<std::string,std::vector<type::string> >         array__string = {};
  std::map<std::string,std::vector<type::strings> >        array__strings = {};
  std::map<std::string,std::vector<type::path> >           array__path = {};
  std::map<std::string,std::vector<type::atom_selection> > array__atom_selection = {};
  std::map<std::string,std::vector<type::spacegroup> >     array__spacegroup = {};

  public:

  //{ //push_back brackets
  void push_back(const std::string p,std::string item);
  void push_back(const std::string p,type::choice item);
  void push_back(const std::string p,bool item);
  void push_back(const std::string p,double item);
  void push_back(const std::string p,int item);
  void push_back(const std::string p,uuid_t item);
  void push_back(const std::string p,std::pair<double,double> item);
  void push_back(const std::string p,dvect3 item);
  void push_back(const std::string p,ivect3 item);
  void push_back(const std::string p,af::double6 item);
  void push_back(const std::string p,dmat33 item);
  void push_back(const std::string p,sv_double item);
  void push_back(const std::string p,sv_int item);
  void pop_back(const std::string p);
  void clear(const std::string p);

  std::string get_as_str(const std::string p);
  std::string get_as_str_i(const std::string p,const int index);
  void set_as_str(const std::string p,std::string str);
  void set_as_str_i(const std::string p,std::string str, const int index);
  void set_as_def(const std::string p);
  int size(const std::string p) const;
  void resize(const std::string p,int n);
  std::string unparse(show::type show_type,bool phil) const;
  sv_string scope_keywords(const std::string SCOPE) const;
  std::set<std::string> input_keywords_for_mode(sv_string query) const;
  std::set<std::string> result_keywords_for_mode(sv_string query) const;
  void clear_incompatible_linkage_arrays();
  void copy(const std::string p,int j,InputBase* o);
  std::vector<type::mtzcol> labin(bool is_default) const;
  type::mtzcol labin_mtzcol(labin::col col) const;
  void check_unique(std::string keyword);
  void make_unique(std::string keyword);
  std::vector<std::string> split_tok(std::string str,std::string delim) const;
  void process_warnings(std::string warnkey,af_string newwarn);
  std::string card_str(bool without_mode) const;

  bool operator==(const InputBase& other) const
  {
    if (value__mtzcol != other.value__mtzcol) return false;
    if (value__boolean != other.value__boolean) return false;
    if (value__choice  != other.value__choice) return false;
    if (value__percent  != other.value__percent) return false;
    if (value__percent_paired  != other.value__percent_paired) return false;
    if (value__int_number  != other.value__int_number) return false;
    if (value__int_paired  != other.value__int_paired) return false;
    if (value__int_vector  != other.value__int_vector) return false;
    if (value__int_numbers  != other.value__int_numbers) return false;
    if (value__uuid_number  != other.value__uuid_number) return false;
    if (value__uuid_numberS  != other.value__uuid_numberS) return false;
    if (value__flt_number  != other.value__flt_number) return false;
    if (value__flt_paired  != other.value__flt_paired) return false;
    if (value__flt_vector  != other.value__flt_vector) return false;
    if (value__flt_cell  != other.value__flt_cell) return false;
    if (value__flt_matrix  != other.value__flt_matrix) return false;
    if (value__flt_numbers  != other.value__flt_numbers) return false;
    if (value__string  != other.value__string) return false;
    if (value__strings  != other.value__strings) return false;
    if (value__path  != other.value__path) return false;
    if (value__atom_selection  != other.value__atom_selection) return false;
    if (value__spacegroup  != other.value__spacegroup) return false;
    if (array__boolean != other.array__boolean) return false;
    if (array__choice  != other.array__choice) return false;
    if (array__percent  != other.array__percent) return false;
    if (array__int_number  != other.array__int_number) return false;
    if (array__int_paired  != other.array__int_paired) return false;
    if (array__int_vector  != other.array__int_vector) return false;
    if (array__int_numbers  != other.array__int_numbers) return false;
    if (array__uuid_number  != other.array__uuid_number) return false;
    if (array__uuid_numberS  != other.array__uuid_numberS) return false;
    if (array__flt_number  != other.array__flt_number) return false;
    if (array__flt_paired  != other.array__flt_paired) return false;
    if (array__flt_vector  != other.array__flt_vector) return false;
    if (array__flt_cell  != other.array__flt_cell) return false;
    if (array__flt_matrix  != other.array__flt_matrix) return false;
    if (array__flt_numbers  != other.array__flt_numbers) return false;
    if (array__string  != other.array__string) return false;
    if (array__strings  != other.array__strings) return false;
    if (array__path  != other.array__path) return false;
    if (array__atom_selection  != other.array__atom_selection) return false;
    if (array__spacegroup  != other.array__spacegroup) return false;
    return true;
  }
};

}
#endif
