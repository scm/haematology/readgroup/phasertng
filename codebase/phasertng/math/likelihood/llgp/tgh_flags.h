//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_llgp_tgh_flags_class__
#define __phasertng_likelihood_llgp_tgh_flags_class__

namespace phasertng { namespace likelihood { namespace llgp {

class tgh_flags
{
  private: //members
    bool target_ = false;
    bool gradient_ = false;
    bool hessian_ = false;
    bool phsprob_ = false;
    bool variances_ = true;

  public: //constructors
    tgh_flags() {}

  public: //functions
    const bool& target() const
    { //return type is by reference so must return member variable
      if (target_) return target_;
      if ( phsprob_) return phsprob_;
      if ( gradient_) return gradient_;
      if ( hessian_) return hessian_;
      return target_;
    }
    const bool& gradient() const
    { //return type is by reference so must return member variable
      if ( gradient_) return gradient_;
      if ( hessian_) return hessian_;
      return gradient_ ;
    }
    const bool& hessian() const   { return hessian_; }
    const bool& phsprob() const   { return phsprob_; }
    const bool& variances() const { return variances_; }

  public: //functions
    tgh_flags& target(bool b)    { target_ = b; return *this; }
    tgh_flags& gradient(bool b)  { gradient_ = b; return *this; }
    tgh_flags& hessian(bool b)   { hessian_ = b; return *this; }
    tgh_flags& phsprob(bool b)   { phsprob_ = b; return *this; }
    tgh_flags& variances(bool b) { variances_ = b; return *this; }
};

}}} //phasertng
#endif
