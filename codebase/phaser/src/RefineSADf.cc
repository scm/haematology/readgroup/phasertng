//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/main/jiffy.h>
#include <phaser/src/RefineSAD.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/xray/fast_gradients.h>
#include <cctbx/adptbx.h>
#include <scitbx/array_family/simple_io.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/eltbx/wavelengths.h>
#include <cctbx/miller/asu.h>
#include <phaser/lib/math_DLuzzati.h>
#include <phaser/lib/solTerm.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
}

namespace phaser {

// Calculate structure factors for heavy atoms.
// Structure factors are calculated by FFT.

void RefineSAD::prepareAtomicFFT()
{
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  double d_min = cctbx::uctbx::d_star_sq_as_d(cctbxUC.max_d_star_sq(miller.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  double Shannon(3);
  double resolution_factor(1.0/Shannon);
  double quality_factor(100);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
    cctbxUC,d_min,resolution_factor,sym_flags,SgInfo,mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  cfft = scitbx::fftpack::complex_to_complex_3d<double>(gridding);
  miller::index<int> UNIT(1,1,1);
  double HIRES(1/(1/HiRes() + UnitCell::S(UNIT)));
  u_base = cctbx::xray::calc_u_base(HIRES,resolution_factor, quality_factor);
}

void RefineSAD::calcAtomicDataFFT()
{
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();

  // --- make a grid of the density from the scatterers
  for (int i = 0; i < atoms.size(); i++)
  {
    int t = atomtype2t[atoms[i].SCAT.scattering_type];
    atoms[i].SCAT.fp = AtomFp[t];
    atoms[i].SCAT.fdp = AtomFdp[t];
  }
  cctbx::xray::scattering_type_registry scattering_type_registry;
  af::shared<xray::scatterer<double> > scatterers = atoms.get_scatterers(); //rejected have occupancy zero
  scattering_type_registry.process(scatterers.const_ref());
  scattering_type_registry.assign_from_table("WK1995");

  cctbx::xray::sampled_model_density<double> sampled_density(
    cctbxUC,
    scatterers.const_ref(),
    scattering_type_registry,
    cfft.n(),
    cfft.n(),
    u_base,
    1.e-8, // wing_cutoff 1.e-8
    -100, // exp_table_one_over_step_size
    true); // force_complex, otherwise all fdp = 0 gives a real map

  // --- fiddle to get correct form to pass to the fft calculation
  cctbx::xray::sampled_model_density<double>::complex_map_type density_map = sampled_density.complex_map();
  assert(density_map.size());
  af::ref<cmplxType, af::c_grid<3> > density_map_ref( &*density_map.begin(), af::c_grid<3>(cfft.n()));

  // --- do the fft
  cfft.backward(density_map_ref);
  // --- extract the complex structure factors
  af::const_ref<cmplxType, af::c_grid_padded<3> > density_map_cref(
      reinterpret_cast<cmplxType*>(&*density_map.begin()), af::c_grid_padded<3>(cfft.n()));

  typedef cctbx::maptbx::structure_factors::from_map<double> cctbxFromMap;
  bool conjugate_flag(false); //extracts h,k,l
  cctbxFromMap from_map;
  from_map = cctbxFromMap(cctbxSG,sampled_density.anomalous_flag(),miller.const_ref(),density_map_cref,conjugate_flag);
  FApos = from_map.data();

  for (int r = 0; r < miller.size(); r++) miller[r] *= -1;
  from_map = cctbxFromMap(cctbxSG,sampled_density.anomalous_flag(),miller.const_ref(),density_map_cref,conjugate_flag);
  FAneg = from_map.data();
  for (int r = 0; r < miller.size(); r++) miller[r] *= -1;

  sampled_density.eliminate_u_extra_and_normalize(miller.const_ref(),FApos.ref());
  sampled_density.eliminate_u_extra_and_normalize(miller.const_ref(),FAneg.ref());

  //store complex conj of FAneg
  for (int r = 0; r < NREFL; r++) FAneg[r] = std::conj(FAneg[r]);
}

void RefineSAD::calcGradFFT(af::shared<miller::index<int> > expanded_miller, af_cmplx expanded_gradient)
{
  assert(!input_partial); // can not do fft grads with partial structure
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();

  PHASER_ASSERT(atoms.size());
  for (int a = 0; a < atoms.size(); a++)
  {
    int t = atomtype2t[atoms[a].SCAT.scattering_type];
    atoms[a].SCAT.fp = AtomFp[t];
    atoms[a].SCAT.fdp = AtomFdp[t];
  }
  cctbx::xray::scattering_type_registry scattering_type_registry;
  af::shared<xray::scatterer<double> > scatterers = atoms.get_scatterers(); //rejected have occupancy zero
  PHASER_ASSERT(scatterers.size());
  scattering_type_registry.process(scatterers.const_ref());
  scattering_type_registry.assign_from_table("WK1995");

  cctbx::xray::fast_gradients<double> fast_gradients(
      cctbxUC,
      scatterers.const_ref(),
      scattering_type_registry,
      u_base,
      1.e-8 // wing_cutoff 1.e-8
      );
  double  multiplier = cctbxUC.volume() / ivect3(cfft.n()).product() * cctbxSG.n_ltr();
  cctbx::xray::apply_u_extra(
      cctbxUC,
      fast_gradients.u_extra(),
      expanded_miller.const_ref(),
      expanded_gradient.ref(),
      multiplier);

  bool anomalous_flag(true);
  bool conjugate_flag(true);
  bool treat_restricted(true);
  af::c_grid_padded<3> map_grid(cfft.n());
  cctbx::maptbx::structure_factors::to_map<double> gradmap(
    cctbxSG,
    anomalous_flag,
    expanded_miller.const_ref(),
    expanded_gradient.const_ref(),
    cfft.n(),
    map_grid,
    conjugate_flag,
    treat_restricted);

  // --- do the fft
  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref( gradmap.complex_map().begin(), af::c_grid<3>(cfft.n()));
  cfft.backward(gradmap_fft_ref);
  for(std::size_t k=0;k<scatterers.size();k++) {
    cctbx::xray::scatterer<>& sc = scatterers.ref()[k];
    sc.flags.set_grad_site(true);
    sc.flags.set_grad_u_iso(true);
    sc.flags.set_grad_u_aniso(true);
    sc.flags.set_grad_occupancy(true);
    sc.flags.set_grad_fp(true);
    sc.flags.set_grad_fdp(true);
    sc.flags.set_tan_u_iso(false);
  }

  af::const_ref<cmplxType, scitbx::af::c_grid_padded_periodic<3> > gradmap_const_ref(
    gradmap_fft_ref.begin(),
    scitbx::af::c_grid_padded_periodic<3>(cfft.n(), cfft.n()));
 // bool sampled_density_must_be_positive(true); //AJM CHANGE FROM PHASER
  bool sampled_density_must_be_positive(false);
  fast_gradients.sampling(
      scatterers.const_ref(),
      af::const_ref<double>(0,0),//mean_displacements // if sqrt_u_iso false in grad_flags, this is ignored
      scattering_type_registry,
      site_sym_table,
      gradmap_const_ref,
      0,//n_parameters, unpacked if zero, packed otherwise
      sampled_density_must_be_positive);
  // n_parameters == 0: unpacked
  af_dvect3 dtds = fast_gradients.d_target_d_site_cart();
  af_float  dtdu = fast_gradients.d_target_d_u_iso();
  af_dmat6  dtducart = fast_gradients.d_target_d_u_cart();
  af_float  dtdo = fast_gradients.d_target_d_occupancy();
  af_float  dtdfdp = fast_gradients.d_target_d_fdp();

  //alternative: n_parameters > 0: packed (see cctbx/xray/packing_order.h, minimization.h)
  //af_float dtdp = fast_gradients.packed();

  //initialize Atomic part of dL_by_dPar array
  for (unsigned j = nscales_ref; j < dL_by_dPar.size(); j++) dL_by_dPar[j] = 0;

  //pack the gradients into the right order in gradient vector
  int m(nscales_all),i(nscales_ref);
  //Cannot refine K and Occ at the same time
  //Cannot refine B and atomB at the same time

  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    if (refinePar[m++]) {
      dtds[a] = dtds[a] * cctbxUC.orthogonalization_matrix();
      af::small<double,3> x_indep = site_sym_table.get(a).site_constraints().independent_params(dtds[a]);
      for (int x = 0; x < atoms[a].XTRA.n_xyz; x++) dL_by_dPar[i++] += x_indep[x];
    }
    if (refinePar[m++]) dL_by_dPar[i++] += dtdo[a]; //occupancy
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        dL_by_dPar[i++] += dtdu[a]; // U
      } else
      {
        dmat6 u_star_grad = cctbx::adptbx::grad_u_cart_as_u_star(cctbxUC,dtducart[a]);
        scitbx::af::small<double,6> u_indep = site_sym_table.get(a).adp_constraints().independent_gradients(u_star_grad);
        for (int n = 0; n < atoms[a].XTRA.n_adp; n++) dL_by_dPar[i++] += u_indep[n];
      }
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    if (refinePar[m++])
    {
      for (unsigned a = 0; a < atoms.size(); a++)
      if (!atoms[a].XTRA.rejected)
        dL_by_dPar[i] += dtdfdp[a];
      i++;
    }
  }
  assert(m == npars_all);
  assert(i == npars_ref);

#ifdef __FDGRADATOMIC__
  std::cout << "=== First Derivative Test (r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  if (grad_refl++ >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
#endif
}

std::pair<bool,af_string> RefineSAD::calcPartialStructure()
{
//if anomalous, calculated in calcAnomPartialStructure in DataSAD
//the substructure does not have to be excluded from the partial structure
  bool warning (false);
  af_string output;
  if (PARTIAL.ANOMALOUS) return {warning,output};  //use DataSAD calcAnomPartialStructure
  if (!COORDINATES->PDB.size()) output.push_back("No partial structure");
  if (!COORDINATES->PDB.size()) return {warning,output};
  output.push_back("");
  output.push_back("--Partial Structure--");
  FPpos.resize(NREFL,0);
  FPneg.resize(NREFL,0);
  input_partial = true;
  if (!PARTIAL.map_format)
  {
    auto& pdb = COORDINATES->PDB;
    cctbx::af::shared<cctbx::xray::scatterer<double> > partial_atoms;
    cctbx::sgtbx::site_symmetry_table partial_sym_table;
    //Delete atoms from partial model within HiRes/2 of anomalous site
    //but not if atom type is purely imaginary
    //since the partial structure is real only
    float1D close_dist(atoms.size());
    int1D close_pdb(atoms.size());
    double max_separation_dist = std::max(1.0,HiRes()/2);
    double max_dist_sqr = fn::pow2(max_separation_dist);
    for (unsigned a = 0; a < atoms.size(); a++)
    if (atoms[a].SCAT.scattering_type != "AX" && !atoms[a].XTRA.rejected)
    {
      double min_delta = std::numeric_limits<double>::max();
      for (int p = 0; p < pdb.size(); p++)
      {
        dvect3 orthXyz = pdb[p].X;
        //dvect3 fracXyz = UnitCell::doOrth2Frac(orthXyz); // site
        for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
        {
          dvect3 frac_i = SpaceGroup::doSymXYZ(isym,atoms[a].SCAT.site);
          dvect3 cellTrans;
          for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
            for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
              for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
              {
                dvect3 fracCell_i = frac_i + cellTrans;
                dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
                double delta = std::fabs((orth_i-orthXyz)*(orth_i-orthXyz));
                if (delta < min_delta)
                {
                  min_delta = delta;
                  close_dist[a] = std::sqrt(delta);
                  close_pdb[a] = p;
                }
              }
        }
      }
      if (min_delta < max_dist_sqr) //closest atom must be within limits
        pdb_deleted_set.insert(close_pdb[a]);
    }

    //now make the partial structure
    for (int p = 0; p < pdb.size(); p++)
    if (pdb_deleted_set.find(p) == pdb_deleted_set.end())
    {
      dvect3 orthXyz = pdb[p].X;
      dvect3 fracXyz = UnitCell::doOrth2Frac(orthXyz); // site
      cctbx::xray::scatterer<> new_atom(
          "partial structure", // label
          fracXyz, // site
          cctbx::adptbx::b_as_u(pdb[p].B), // u_iso
          pdb[p].O, // occupancy
          cctbx::eltbx::xray_scattering::wk1995(pdb[p].get_element()).label(), // scattering_type
            0, // fp
            0); // fdp
      partial_atoms.push_back(new_atom);
      partial_sym_table.process(new_atom.apply_symmetry(getCctbxUC(),getCctbxSG()));
    }

    if (!partial_atoms.size())
    throw phasertng::Error(phasertng::err::FATAL,"Partial structure file does not have any atoms");
    int ndeleted = pdb_deleted_set.size();
    int natoms = pdb.size();
    output.push_back(std::to_string(natoms) + " atom" + std::string(natoms==1?"":"s") + " in partial structure");
    output.push_back(std::to_string(ndeleted) + " atom" + std::string(ndeleted==1?"":"s") + " in partial structure deleted because close to phasing atoms");
    if (pdb_deleted_set.size())
    {
      for (unsigned a = 0; a < atoms.size(); a++)
      if (atoms[a].SCAT.scattering_type != "AX")
      {
        int p = close_pdb[a];
        if (pdb_deleted_set.find(p) != pdb_deleted_set.end() && close_dist[a] < max_separation_dist)
        {
          output.push_back("Atom #" + std::to_string(a+1) + " close to partial structure atom #" + std::to_string(p+1) + " with distance = " + phasertng::dtos(close_dist[a],5,2));
          COORDINATES->PDB[p].generic_int = a+1; //non-zero, default
        }
      }
      output.push_back("");
    }

    //initialize
    for (unsigned r = 0; r < NREFL; r++) FPpos[r] = FPneg[r] = 0;

    float1D linearTerm(partial_atoms.size()),debye_waller_u_iso(partial_atoms.size(),0.);
    for (unsigned a = 0; a < partial_atoms.size(); a++)
    {
      double symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
      int M = SpaceGroup::NSYMM/partial_sym_table.get(a).multiplicity();
      linearTerm[a] = symFacPCIF*partial_atoms[a].occupancy/M;
      if (!partial_atoms[a].flags.use_u_aniso_only())
        debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(partial_atoms[a].u_iso) * 0.25;
    }

    //sf calculation
    output.push_back("Start structure factor calculation for partial structure");
    double fp(0);
    cctbx::eltbx::xray_scattering::gaussian ff_C = cctbx::eltbx::xray_scattering::wk1995("C").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_N = cctbx::eltbx::xray_scattering::wk1995("N").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_O = cctbx::eltbx::xray_scattering::wk1995("O").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_S = cctbx::eltbx::xray_scattering::wk1995("S").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_H = cctbx::eltbx::xray_scattering::wk1995("H").fetch();
    for (unsigned r = 0; r < NREFL; r++)
    {
      double fo_C =  ff_C.at_d_star_sq(ssqr[r]);
      double fo_N =  ff_N.at_d_star_sq(ssqr[r]);
      double fo_O =  ff_O.at_d_star_sq(ssqr[r]);
      double fo_S =  ff_S.at_d_star_sq(ssqr[r]);
      double fo_H =  ff_H.at_d_star_sq(ssqr[r]);
      for (unsigned a = 0; a < partial_atoms.size(); a++)
      {
        double isoB = partial_atoms[a].flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
        double symmOccBfac(linearTerm[a]*isoB);
        double fo_plus_fp(0);
        if (partial_atoms[a].scattering_type == "C")
          fo_plus_fp = fo_C + fp;
        else if (partial_atoms[a].scattering_type == "N")
          fo_plus_fp = fo_N + fp;
        else if (partial_atoms[a].scattering_type == "O")
          fo_plus_fp = fo_O + fp;
        else if (partial_atoms[a].scattering_type == "S")
          fo_plus_fp = fo_S + fp;
        else if (partial_atoms[a].scattering_type == "H")
          fo_plus_fp = fo_H + fp;
        else
        {
          cctbx::eltbx::xray_scattering::wk1995 ff(partial_atoms[a].scattering_type);
          fo_plus_fp = ff.fetch().at_d_star_sq(ssqr[r]) + fp;
        }
        double scat(fo_plus_fp*symmOccBfac);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
        {
          double theta = rotMiller[r][isym][0]*partial_atoms[a].site[0] +
                            rotMiller[r][isym][1]*partial_atoms[a].site[1] +
                            rotMiller[r][isym][2]*partial_atoms[a].site[2] +
                            traMiller[r][isym];
          cmplxType sincostheta = phasertng::tbl_cos_sin.get(theta); //multiplies by 2*pi internally
          double costheta = std::real(sincostheta);
          double sintheta = std::imag(sincostheta);
          double scat_costheta(scat*costheta);
          double scat_sintheta(scat*sintheta);
          FPpos[r] += cmplxType(scat_costheta,scat_sintheta);
          FPneg[r] += cmplxType(scat_costheta,scat_sintheta);
        }//end sym
      } //end  loop over partial_atoms
    } //end loop r
    output.push_back("...Done");
    output.push_back("Scale factor = " + std::to_string(PartK) + "  B-factor = " + std::to_string(cctbx::adptbx::u_as_b(PartU)));
  }
  else
  {
    //read ModelFcalc data into local array and scale each time
    //same as in phaser
    cctbx::sgtbx::space_group const& cctbxSG(getCctbxSG());
    cctbx::sgtbx::space_group_type SgInfo(getCctbxSG());
    cctbx::sgtbx::reciprocal_space::asu asu(SgInfo);
    int count_match(0);
    for (int mtzr = 0; mtzr < FCALCS->MILLER.size() ; mtzr++)
    {
      //read a map into ModelFcalc rather than Map, since ModelFcalc is already in Entry
      miller::index<int> HKL = FCALCS->MILLER[mtzr];
      auto fcalc = FCALCS->FCALC(0,mtzr);
      cctbx::miller::asym_index ai(cctbxSG, asu, HKL);
      bool anomalous_flag(false),deg(false);
      cctbx::miller::index_table_layout_adaptor ila = ai.one_column(anomalous_flag);
      HKL = ila.h();
      auto F = std::abs(fcalc);
      auto P = std::arg(fcalc);
      cctbx::miller::map_to_asu_policy<double>::eq(ila, F, false);
      cctbx::miller::map_to_asu_policy<double>::eq(ila, P, deg);
      //the SAD data were placed in the map_to_asu in DataSAD constructor
      //find the matching indices
      for (int r = 0; r < NREFL; r++)
      if (HKL[0] == miller[r][0] && HKL[1] == miller[r][1] && HKL[2] == miller[r][2])
      {
        FPpos[r] = std::polar(F,P);
        FPneg[r] = FPpos[r];
        count_match++;
        break;
      }
    }
    double perc_blank(100*double(NREFL-count_match)/NREFL);
    if (perc_blank > 5) //one percent, unselected values
    {
      warning = true;
      output.push_back(std::to_string(perc_blank) +"% of the observed reflections have no matching partial data: check that the resolutions of map and data agree");
    }
    bool1D outlier(NREFL,false);
    for (int o = 0; o < OUTLIER.ANO.size(); o++)
      outlier[OUTLIER.ANO[o].refl] = true;
    for (int o = 0; o < OUTLIER.SAD.size(); o++)
      outlier[OUTLIER.SAD[o].refl] = true;
    for (int r = 0; r < NREFL; r++)
    {
      bool partial_data(FPpos[r] != cmplxType(0));
      selected[r] = !bad_data[r] && working[r] && !outlier[r] && partial_data;
    }

    double sum(0),sumx(0),sumx2(0),sumy(0),sumxy(0);
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      double W = 1;
      if (FPpos[r] != cmplxType(0) && POS.F[r] > 0)
      {
   //scale FP to F (scale applies to F2)
        double Y = std::log(POS.F[r]/std::abs(FPpos[r]));
        double s2 = ssqr[r];
        sum += W;
        sumx += W*s2;
        sumx2 += W*s2*s2;
        sumy += W*Y;
        sumxy += W*s2*Y;
      }
    }
    assert(sum*sumx2-fn::pow2(sumx));
    double slope = (sum*sumxy-(sumx*sumy))/(sum*sumx2-fn::pow2(sumx));
    double intercept = (sumy - slope*sumx)/sum;
    double scaleK = exp(intercept);
    double scaleB = -4*slope;
    output.push_back("Map scale factor = " + std::to_string(scaleK) + "  B-factor = " + std::to_string(scaleB));
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      double K = scaleK*std::exp(-scaleB*ssqr[r]/4);
      FPpos[r] *= K;
      FPneg[r] *= K;
    }
    {
    double sum(0),sumx(0),sumx2(0),sumy(0),sumxy(0);
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      double W = 1;
      if (FPpos[r] != cmplxType(0) && POS.F[r] > 0)
      {
        double Y = std::log(POS.F[r]/std::abs(FPpos[r]));
        double s2 = ssqr[r];
        sum += W;
        sumx += W*s2;
        sumx2 += W*s2*s2;
        sumy += W*Y;
        sumxy += W*s2*Y;
      }
    }
    assert(sum*sumx2-fn::pow2(sumx));
    double slope = (sum*sumxy-(sumx*sumy))/(sum*sumx2-fn::pow2(sumx));
    double intercept = (sumy - slope*sumx)/sum;
    double scaleK = exp(intercept);
    double scaleB = -4*slope;
    //below paranoia AJM TODO remove
    output.push_back("Map scale factor = " + std::to_string(scaleK) + "  B-factor = " + std::to_string(scaleB));
    }
  } //pdb or map

  data_solpar SOLPAR;
  SOLPAR.SIGA_FSOL = 0.725;
  SOLPAR.SIGA_BSOL = 204.4;
  assert(FPpos.size() == NREFL);
  assert(FPneg.size() == NREFL);
  for (unsigned r = 0; r < miller.size(); r++)
  {
    double ssqr = UnitCell::Ssqr(miller[r]);
    double DLuz = solTerm(ssqr,SOLPAR)*DLuzzati(ssqr,PARTIAL.RMSID);
    FPpos[r] *= DLuz;
    FPneg[r] *= DLuz;
  }
  return {warning,output};
} // end calcPartialStructure

}//phaser
