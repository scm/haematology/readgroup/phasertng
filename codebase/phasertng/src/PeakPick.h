//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PeakPick_class__
#define __phasertng_PeakPick_class__
#include <phasertng/enum/pick_Choice.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <array>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>

namespace phasertng {
typedef af::versa<double, af::c_grid<3> >   versa_grid_double;

class FileSystem;

  class PeakPick
  {
    private:
      double STORE_ZSCORE;
      double HIRES;
      af::int3 GRIDDING;
      versa_grid_double real_map_unpadded;
      UnitCell UC;
      SpaceGroup SG;
      // --- peak search
      //level = 1: compare to the 6 nearest neighbors
      //      = 2: also compare to the 12 second-nearest neighbors
      //      > 2: also compare to the 8 third-nearest neighbors</pre>
      int peak_search_level = 3;

    public:
      //use std::array to allow interation easily (rather than get<n> from a pair)
      //first is peaks, second is holes, so they can be iterated
      struct statsinfo {
        double max;
        double min;
        double mean;
        double sigma;
        double maxZM;
        af_int histogram;
        std::pair<double,double>  mean_sigma() const
        { return { mean,sigma }; }
      };
      std::array<statsinfo,2> STATS;

      //these are indexed on peak for interation
      //sort order is zscore/multiplicity NOT heights
      struct peakinfo {
        double heights;
        dvect3 sites;
        double zscore;
        int    multiplicity;
        bool   peak_over_deepest_hole;
        double zscore_on_multiplicity() const
        { return zscore/static_cast<double>(multiplicity); }
      };
      std::array<std::vector<peakinfo>,2> SITES;

    public:
      int     number_significant(double,double,int);
      int     number(pick::Choice);
      void    purge_by_number(int);

    public:
      PeakPick(
          SpaceGroup SG_,
          UnitCell UC_,
          double HIRES_,
          versa_grid_double real_map_unpadded_,
          af::int3 GRIDDING_
        );

    public:
      void calculate(double,bool=false,bool=false);
      af_string purge_close_contacts(af_dvect3,double);
      void WriteMap(FileSystem);
      void WritePdb(FileSystem);
      af_string logStatistics(std::string,int);
      af_string logHistogram(std::string);
      void sort_on_zscore_over_multiplicity(int);
      void reverse_sort_on_zscore_over_multiplicity(int);

      PeakPick&
      level(int i) { peak_search_level = i; return *this; }

  };

}
#endif
