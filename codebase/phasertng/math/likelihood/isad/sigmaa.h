//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_isad_sigmaa_class__
#define __phasertng_likelihood_isad_sigmaa_class__
#include <vector>
#include <complex>

typedef std::complex<double> cmplex;

namespace phasertng { namespace likelihood { namespace isad {

class sigmaa
{
  public: //members
    //bin-wise parameters
    std::vector<double>  MIDSSQR = std::vector<double>(0);
    std::vector<double>  SigmaN_bin = std::vector<double>(0);
    std::vector<double>  SigmaP_bin = std::vector<double>(0);
    std::vector<double>  SigmaQ_bin = std::vector<double>(0); // Substructure in crystal
    std::vector<double>  SigmaH_bin = std::vector<double>(0); // Substructure model
    std::vector<double>  absrhoFF_bin = std::vector<double>(0);
    std::vector<cmplex>  sigmaPP_bin = std::vector<cmplex>(0);
    std::vector<cmplex>  sigmaQQ_bin = std::vector<cmplex>(0); // Crystal
    std::vector<cmplex>  sigmaHH_bin = std::vector<cmplex>(0); // Model
    std::vector<double>  fDelta_bin = std::vector<double>(0);
    std::vector<double>  rhopm_bin = std::vector<double>(0);

  public: //constructors
    sigmaa() {}
    bool in_memory() const { return bool(MIDSSQR.size()>0); }
    int  size() const { return MIDSSQR.size(); }
    int  numbins() const { return MIDSSQR.size(); }

  public:
    void
    setup_midssqr(std::vector<double> midssqr)
    {
      int nbins = midssqr.size();
      MIDSSQR = midssqr;
      SigmaN_bin.resize(nbins,0);
      SigmaP_bin.resize(nbins,0);
      SigmaQ_bin.resize(nbins,0);
      SigmaH_bin.resize(nbins,0);
      absrhoFF_bin.resize(nbins,0);
      sigmaPP_bin.resize(nbins,0);
      sigmaQQ_bin.resize(nbins,0);
      sigmaHH_bin.resize(nbins,0);
      fDelta_bin.resize(nbins,0);
      rhopm_bin.resize(nbins,0);
    };

    double minimum_rhopm() { return  1.0e-06; }
};

}}}//phasertng
#endif
