//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineANISO_class__
#define __phasertng_RefineANISO_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/includes.h>
#include <phasertng/math/likelihood/pIntensity.h>
#include <utility>

namespace phasertng {

class ReflRowsAnom; //forward declaration

class RefineANISO : public dtmin::RefineBase
{
  math::pIntensity pintensity;

  private: //members
    dmat6     sigmaSphericityBeta = dmat6(0,0,0,0,0,0);
    bool      REFINE_SCALE = false;
    bool      REFINE_BINS = false;
    bool      REFINE_ANISO = false;
    int       NTHREADS = 1;
    bool      USE_STRICTLY_NTHREADS = false;
    double    LOG_WILSON_SIGMA_CONSTANT = 0.15;
    double    LOG_WILSON_SIGMA_FACTOR = 0.001;
    int       JOBS = 0;

    bool new_memory_from_python = false;
    int  scale;
    //int bin; //variable with reflection, threadsafe local
    sv_int ano;
    af_int numinbin; //for speed, store

  public: //members
    const_ReflectionsPtr REFLECTIONS; //shared_ptr: needs creation and ownership in python
    SelectedPtr SELECTED;
    SigmaNPtr   SIGMAN;
    bool BEST_CURVE_RESTRAINT = true;

  public: //constructors
    RefineANISO() : dtmin::RefineBase() { }
    RefineANISO(
        const_ReflectionsPtr,
        SelectedPtr,
        SigmaNPtr
        );
    RefineANISO( //python
        const ReflRowsAnom&,
        Selected&,
        SigmaN&
        );
    ~RefineANISO() throw() { }

  public:
    void init();

  private:
    TargetGradientHessianType
    parallel_combined_tgh(
        bool, //gradient
        bool, //hessian
        int, //beg
        int//end
    );

    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
    );
    void add_restraint(TargetGradientHessianType&,bool,bool);
    void add_constraint(TargetGradientHessianType&,bool,bool);

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string reject_outliers();
    af_string finalize_parameters();

  private:
    sv_bool REFINE_ANISO_MASK;
    dmat6   constrained;
    std::vector<sv_int> constrain_equal;
    sv_bool anisoMask_equal;

  public:
    double sharpening_bfactor() const;
    double wilson_k_f() const;
    double wilson_b_f() const;
    double delta_bfactor() const;
    dmat33 direction_cosines() const;
    dmat6  beta() const;
    double resn(int r) const;
    double aniso(int r,bool) const;

    TargetType
    likelihood();

    sv_double labin_wll;

  public:
    void set_threading(bool,int);
    void set_sphericity_restraint();
    void setup_wll();
};

} //phasertng
#endif
