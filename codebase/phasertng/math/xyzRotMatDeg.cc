#include <phasertng/math/xyzRotMatDeg.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/math/table/cos_sin.h>
#include <iso646.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
}

namespace phasertng {

void
xyzRotMatDeg::setup_theta(int dir,double theta_deg)
{
  if (dir == 0)      { ind0=0; ind1=1; ind2=2; }
  else if (dir == 1) { ind0=1; ind1=2; ind2=0; }
  else               { ind0=2; ind1=0; ind2=1; } //dir == 2
  cos_sin = tbl_cos_sin.get(theta_deg/360.); //multiplies by 2*pi internally
  cosTheta = std::real(cos_sin);
  sinTheta = std::imag(cos_sin);
}

void
xyzRotMatDeg::setup(int dir,cmplex cos_sin_)
{
  if (dir == 0)      { ind0=0; ind1=1; ind2=2; }
  else if (dir == 1) { ind0=1; ind1=2; ind2=0; }
  else               { ind0=2; ind1=0; ind2=1; } //dir == 2
  cos_sin = cos_sin_;
  cosTheta = std::real(cos_sin);
  sinTheta = std::imag(cos_sin);
}

const dmat33&
xyzRotMatDeg::tgh(int dir, double theta_deg, bool do_gradient, bool do_hessian)
{
  setup_theta(dir,theta_deg);
  if (!do_gradient and !do_hessian)
  {
    xyzmat(ind0,ind0) = 1.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = cosTheta;
    xyzmat(ind1,ind2) = -sinTheta;
    xyzmat(ind2,ind1) = sinTheta;
    xyzmat(ind2,ind2) = cosTheta;
  }
  else if (do_gradient)
  {
    cosTheta *= c;
    sinTheta *= c;
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = -sinTheta;
    xyzmat(ind1,ind2) = -cosTheta;
    xyzmat(ind2,ind1) = cosTheta;
    xyzmat(ind2,ind2) = -sinTheta;
  }
  else // Hessian
  {
    cosTheta *= csqr;
    sinTheta *= csqr;
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = -cosTheta;
    xyzmat(ind1,ind2) = sinTheta;
    xyzmat(ind2,ind1) = -sinTheta;
    xyzmat(ind2,ind2) = -cosTheta;
  }
  return xyzmat;
}

const dmat33&
xyzRotMatDeg::matrix(int dir, double theta_deg)
{
  // This is used for real-space rotation
  setup_theta(dir,theta_deg);
  xyzmat(ind0,ind0) = 1.0;
  xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
  xyzmat(ind1,ind1) = cosTheta;
  xyzmat(ind1,ind2) = -sinTheta;
  xyzmat(ind2,ind1) = sinTheta;
  xyzmat(ind2,ind2) = cosTheta;
  return xyzmat;
}

// Compared to tgh gradient and hessian, grad_tr and hess_tr are transposes, because
// inverse rotations are applied in reverse order to the molecular transform and other
// reciprocal space objects.
const dmat33&
xyzRotMatDeg::grad_tr(int dir, cmplex cos_sin, bool do_gradient)
{
  setup(dir,cos_sin);
  if (do_gradient)
  {
    cosTheta *= c;
    sinTheta *= c;
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = 0.0;
    xyzmat(ind1,ind1) = -sinTheta;
    xyzmat(ind2,ind1) = -cosTheta;
    xyzmat(ind1,ind2) = cosTheta;
    xyzmat(ind2,ind2) = -sinTheta;
  }
  else
  {
    xyzmat(ind0,ind0) = 1.0;
    xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = 0.0;
    xyzmat(ind1,ind1) = cosTheta;
    xyzmat(ind2,ind1) = -sinTheta;
    xyzmat(ind1,ind2) = sinTheta;
    xyzmat(ind2,ind2) = cosTheta;
  }
  return xyzmat;
}

const dmat33&
xyzRotMatDeg::hess_tr(int dir, cmplex cos_sin, bool do_hessian)
{
  setup(dir,cos_sin);
  if (do_hessian)
  {
    cosTheta *= csqr;
    sinTheta *= csqr;
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = 0.0;
    xyzmat(ind1,ind1) = -cosTheta;
    xyzmat(ind2,ind1) = sinTheta;
    xyzmat(ind1,ind2) = -sinTheta;
    xyzmat(ind2,ind2) = -cosTheta;
  }
  else
  {
    xyzmat(ind0,ind0) = 1.0;
    xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = 0.0;
    xyzmat(ind1,ind1) = cosTheta;
    xyzmat(ind2,ind1) = -sinTheta;
    xyzmat(ind1,ind2) = sinTheta;
    xyzmat(ind2,ind2) = cosTheta;
  }
  return xyzmat;
}

const dmat33& //not const reference return
xyzRotMatDeg::perturbed(const dvect3& perturbRot)
{
  rotmat(0,0) = rotmat(1,1) = rotmat(2,2) = 1.;
  rotmat(0,1) = rotmat(1,0) = rotmat(2,0) = rotmat(0,2) = rotmat(2,1) = rotmat(1,2) = 0.;
  for (int dir = 0; dir < 3; dir++)
    rotmat = matrix(dir,perturbRot[dir])*rotmat;
  return rotmat;
}

dvect3 xyzRotMatDeg::xyz_angles(const dmat33& rotmat)
{
  dvect3 angles;
  double eps = 1.0e-12;

  if (rotmat(2,0) > (1-eps) )
  {
    angles = dvect3(
                scitbx::rad_as_deg( std::atan2( -rotmat(0,1),rotmat(1,1) ) ),
                -90.,
                0. );
  }
  else if (rotmat(2,0) < -(1-eps) )
  {
    angles = dvect3(
                scitbx::rad_as_deg( std::atan2( rotmat(0,1),rotmat(1,1) ) ),
                90.,
                0. );
  }
  else
  {
    angles = dvect3(
                scitbx::rad_as_deg( std::atan2( rotmat(2,1),rotmat(2,2) ) ),
                scitbx::rad_as_deg( std::asin( -rotmat(2,0) ) ),
                scitbx::rad_as_deg( std::atan2( rotmat(1,0),rotmat(0,0) ) ) );
  }
  return angles;
}

double
xyzRotMatDeg::xyzAngle(const dvect3& perturbRot)
{
  rotmat(0,0) = rotmat(1,1) = rotmat(2,2) = 1.;
  rotmat(0,1) = rotmat(1,0) = rotmat(2,0) = rotmat(0,2) = rotmat(2,1) = rotmat(1,2) = 0.;
  for (int dir = 0; dir < 3; dir++)
    rotmat = matrix(dir,perturbRot[dir])*rotmat;
  double rad = scitbx::math::r3_rotation::axis_and_angle_from_matrix<double>(rotmat).angle_rad;
  //return std::abs(scitbx::rad_as_deg(rad));
  return scitbx::rad_as_deg(rad);
}

}
