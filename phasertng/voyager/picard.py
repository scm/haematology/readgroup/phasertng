import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError,SpaceGroup,FileSystem
from phasertng import enum_out_stream
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.scripts.picard_jiffies import *
from phasertng.run.ModeRunner import *
from cctbx import sgtbx
import argparse

def picard(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_picard)

def polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback=None,debug=False):
    def logrfactor(DAGDB,init_rfac,last_rfac,title,last):
      msg = ""
      for r,node in enumerate(DAGDB.NODES.LIST):
        if float(node.RFACTOR) < 100:
          strR = "%.2f" % round(node.RFACTOR,2)
          if (node.RFACTOR < 1) : return ""
          last_rfac[r] = node.RFACTOR
          if (float(node.RFACTOR) < float(init_rfac[r])):
            msg=msg+"#"+str(r+1)+": " + title + " improved " + last + strR + "%)\n"
          else:
            msg=msg+"#"+str(r+1)+":" + title + " did not improve " + last + strR + "%)\n"
      return msg
   #{{{ ending
    DAGDB = basetng.DAGDATABASE
    assert(DAGDB.size() > 0)
    work = DAGDB.NODES.LIST[0]
    #if tncs is commensurate and more than 2, which means that the orientations are not refined
    basetng.input.set_as_str(".phasertng.resolution.",str(0))
    basetng.input.set_as_str(".phasertng.macrbr.last_only.",str(False))
    basetng.input.set_as_def(".phasertng.macrbr.ncyc.")
    basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.","rotn tran bfac vrms")
    #we want to branch from the exhaustive refinement
    jumppoint = ModeRunner(basetng=basetng,mode_list=['rbr']).solution()
    if debug:
      basetng.logLoopInfo(enum_out_stream.process,str("added jumppoint")+"\n" + str(jumppoint.get_dag_cards_filename()))
    posedag = DAGDB.cards()
    if debug:
      basetng.logLoopInfo(enum_out_stream.process,str(results['refined_dag']))
    if len(results['refined_dag']) > 0 and DAGDB.size() > 0:
      firstnode = posedag.split(node_separator)[0] + node_separator
      generic_string = ""
      for item in results['refined_dag']: #full set of refined coordinates, only maximum_stored included
        generic_string =  generic_string + item
      allnodes = firstnode + generic_string
      DAGDB.clear_cards()
      DAGDB.parse_cards(allnodes) #the new node is at the top and the rest are the already refined ones
      DAGDB.calculate_duplicate_poses(3.0,False,True,1);
      rfactor,zscore,identifier = DAGDB.NODES.duplicate_equivalent(0)
      DAGDB.clear_cards()
      DAGDB.parse_cards(posedag) #back to the original full set, not just the first
      if float(rfactor) > 0 and float(rfactor) < 99:
        basetng.logLoopInfo(enum_out_stream.process,"Top solution has already been refined")
        basetng.input.generic_int = ord('A') #4
        basetng.input.generic_string = identifier.str() #this is the one picked out by the duplicate match
        #delete those already refined, not just the top, and report in table
        #uses refinement.maximum_stored
        ModeRunner(basetng=basetng,mode_list=['join'])
        basetng.input.generic_string = generic_string #this is the set of refined dag
        refpoint = ModeRunner(basetng=basetng,mode_list=['rfac']).solution()
        DAGDB.clear_cards()
        DAGDB.parse_cards(posedag)
        if debug:
          basetng.logLoopInfo(enum_out_stream.process,str("early return jumppoint"))
        return basetng,results,jumppoint
      else:
        basetng.logLoopInfo(enum_out_stream.process,"Top solution has not already been refined")
    #so store the first as the return
    #refpoint is the last point that models are in memory with R-factor calculated (refined or not)
    ModeRunner(basetng=basetng,mode_list=['move','rbm','tfz'])
    ModeRunner(basetng=basetng,mode_list=['rfac'])
    init_rfac = []
    all_random_rfac=True
    for r,node in enumerate(DAGDB.NODES.LIST):
      init_rfac.append(node.RFACTOR)
      io_random_rfactor = 50.
      if float(node.RFACTOR) < float(io_random_rfactor):
        all_random_rfac=False
    #since the model is not perfect we need to limit the resolution
    #only refine if the R-factor is high, if ok they don't bother because it is slow
    node = DAGDB.NODES.LIST[0]
    protocol = "Polishing Protocol:\n"
    protocol = protocol + "Modelled fraction scattering of current working total = " + str(round(node.FULL.FS*100)/100) + "\n"
    io_refine = bool(working_params.picard.control.use_xref)
    protocol = protocol + "Constituents complete\n" if DAGDB.constituents_complete() else "Constituents not complete\n"
    if bool(working_params.picard.control.use_xref_only_if_complete):
      if not DAGDB.constituents_complete():
        io_refine = False
    io_jogging = True #and overwrite the protocol
    io_pruning = True #and overwrite the protocol
    io_nonrand = bool(working_params.picard.control.use_xref_only_if_low_rfactor)
    if all_random_rfac and io_nonrand:
      protocol = "No polishing (all random R-factors)\n"
      io_jogging = False
      io_pruning = False
      io_refine = False
    else:
   #{{
      fsmin = 20.
      if not io_refine:
        pp = "No coordinate refinement at user request\n"
        protocol = protocol + pp
        io_jogging = False #and overwrite the protocol
        io_pruning = False #and overwrite the protocol
      else:
        if (float(node.FULL.FS)>0) and (float(node.FULL.FS)<fsmin/100.): #7O9P
          io_refine = False #and overwrite the protocol
          io_jogging = False #and overwrite the protocol
          io_pruning = False #and overwrite the protocol
          pp = "No coordinate refinement (fraction scattering less than " + str(fsmin) + "%)\n"
          protocol = protocol + pp
        if io_refine:
          protocol = protocol + "Spanning analysis\n"
          protocol = protocol + "Coordinate refinement\n"
          io_pruning = bool(node.PACKS == "N")
          if io_pruning:
            protocol = protocol + "Occupancy refinement (packing clash)\n"
          else:
            protocol = protocol + "No occupancy refinement (no packing clash)\n"
          nmol = int(1) if bool(work.TNCS_MODELLED) else int(work.TNCS_ORDER)
          io_jogging = working_params.picard.control.use_jog and (nmol > 2)
          if io_jogging:
            protocol = protocol + "Jogging refinement (commensurate high order tncs)\n"
          else:
            protocol = protocol + "No jogging refinement (no commensurate high order tncs)\n"
   #}}
    basetng.logLoopInfo(enum_out_stream.process,protocol)
    if (io_refine):
   #{{{
      basetng.input.generic_int = ord('B') #12
      basetng.input.generic_string = protocol
      ModeRunner(basetng=basetng,mode_list=['join']).solution() #just report the protocol and exit
      refpoint = ModeRunner(basetng=basetng,mode_list=['span']).solution() #even if there will be changes with pruning etc
      last_rfac = copy.deepcopy(init_rfac)
      last = "R-factor (R="
      if (io_jogging):
     #{{{
        ModeRunner(basetng=basetng,mode_list=['jog']) #mcc after rbm
        jogged = bool(basetng.input.get_as_str(".phasertng.jog_refinement.jogged.").strip() == "True")
        if not jogged: #don't show this in the path
          basetng.load_moderunner(refpoint,user_cards,dryrun_cards)
        else:
          ModeRunner(basetng=basetng,mode_list=['move','rbm','mcc']) #mcc after rbm
          refpoint = ModeRunner(basetng=basetng,mode_list=['rfac']).solution()
          msg = logrfactor(DAGDB,init_rfac,last_rfac,"Jogging",last)
          if len(msg) > 0:
            basetng.logLoopInfo(enum_out_stream.process,msg)
            last = "R-factor over Jogging (R="
     #}}} jogging
      init_rfac = copy.deepcopy(last_rfac)
      if (io_pruning):
     #{{{
        ModeRunner(basetng=basetng,mode_list=['spr']) #pose to full
        refpoint = ModeRunner(basetng=basetng,mode_list=['rfac']).solution() #pose to full
        msg = logrfactor(DAGDB,init_rfac,last_rfac,"Pruning",last)
        if len(msg) > 0:
          basetng.logLoopInfo(enum_out_stream.process,msg)
          last = "R-factor over Pruning (R="
     #}}} pruning
      init_rfac = copy.deepcopy(last_rfac)
      for selection in ['refmac','phenix']:
          if selection in working_params.picard.control.xref_software:
         #{{{
            #branches from last refpoint
            basetng.load_moderunner(refpoint,user_cards,dryrun_cards)
            basetng.input.set_as_str(".phasertng.refinement.method.",selection)
            ModeRunner(basetng=basetng,mode_list=['xref']) #full from last which could be jog, spr
            msg = logrfactor(DAGDB,init_rfac,last_rfac,selection.title() + " refine",last)
            if len(msg) > 0:
              basetng.logLoopInfo(enum_out_stream.process,msg)
            msg = ""
            for r,node in enumerate(DAGDB.NODES.LIST):
                assert(node.RFACTOR > 1)
                strR = "%.2f" % round(node.RFACTOR,2)
                io_random_rfactor = 50.
                if float(node.RFACTOR) < float(io_random_rfactor):
                  results = found_solution_load(results,DAGDB.PATHWAY,node,basetng)
                  msg = msg+"#"+str(r+1)+": Found new solution this loop (R=" + strR + "%)\n"
            basetng.logLoopInfo(enum_out_stream.process,msg)
            if not working_params.picard.dryrun:
              emit_callback(results,progress_callback)
         #}}} coordinate refine
    #}}} coordinate refine
    else:
   #{{{
      #last point is rfac calculation
      msg = ""
      for r,node in enumerate(DAGDB.NODES.LIST):
          if (node.RFACTOR > 1): #rfactor calculation may have failed
            strR = "%.2f" % round(node.RFACTOR,2)
            io_random_rfactor = 50.
            if float(node.RFACTOR) < float(io_random_rfactor):
              results = found_solution_load(results,DAGDB.PATHWAY,node,basetng)
              msg = msg+"#"+str(r+1)+": Found new solution this loop (R=" + strR + "%)\n"
      if len(msg) > 0:
        basetng.logLoopInfo(enum_out_stream.process,msg)
      if not working_params.picard.dryrun:
        emit_callback(results,progress_callback)
    #}}} coordinate refine
    #current return cards is from first exhaustive rbr/tfz refinement
    posedag = DAGDB.cards() # has the rfactor on the node
    #store posedag for checking if next refinement has been done here already
    results['refined_dag'].append(posedag)
    if debug:
      basetng.logLoopInfo(enum_out_stream.process,"return jumppoint")
    return basetng,results,jumppoint
   #}}} ending

def inner_picard(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  debug = False #prints out information about path through the algorithm
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="picard")
    initparams = initparams +  "\n" +  build_auto_str("picard")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    #master_phil.show() #libtbx.phil.scope object
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    if (dbdir is None):
      raise ValueError(enum_err_code.input,"Database not set")
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)

    #phil_to_cards selects out only the phasertng cards with the program name
    working_params.phasertng.reflections.read_anomalous = False
    working_params.phasertng.dag.read_file = True
    working_params.phasertng.dag.skip_if_complete = True #global for picard
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    #can ignore dryrun above as it doesn't make a difference
    io_dryrun = copy.deepcopy(working_params.picard.dryrun)
    use_shortcuts = bool(copy.deepcopy(bool(working_params.picard.control.use_shortcuts)))
  # io_helices = copy.deepcopy(working_params.picard.customize.helices)
    dryrun_cards = ""
    if (working_params.picard.dryrun):
       dryrun_cards = '''
phasertng suite write_files on
phasertng suite write_cards on
phasertng suite write_xml on
phasertng suite write_phil off
phasertng suite write_log on
phasertng suite write_data off
phasertng put_solution use_tncs True
phasertng molecular_transform preparation variance
'''
    io_infinite_loop = int(200)

    loop_auto = str("Bones") if io_dryrun else str("Picard")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)
    basetng.logLoopInfo(enum_out_stream.process,"Shortcuts applied: " + str(use_shortcuts) + "\nSolutions will borrowed from previous branches where appropriate")
    if debug:
      basetng.logTab(enum_out_stream.process,user_phil_str)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout): #if write_files is false, no database
      os.remove(xmlout)
    #chk does not contribute to the DAG
    ModeRunner(basetng=basetng,mode_list=['chk'])

    #first check that there are some files, or else we don't fail until auto_search called
    #nb auto search calls this function again internally
    basetng.input = InputCard()
    ignore_unknown_keywords = False
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords)
    walk = ModeRunner(basetng=basetng,mode_list=['walk']).solution()

    #first choice
    hklin = None
    if working_params.picard.hklin is not None:
      hklin = (working_params.picard.hklin)
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) == 1:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is None:
      raise ValueError(enum_err_code.input,"No reflection file")

    #first choice
    seqin = working_params.picard.seqin
    if seqin is None:
      #second choice is phasertng
      seqin = isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
    if seqin is None and int(basetng.input.size(".phasertng.file_discovery.validated.seq_files.")) > 0:
      #third choice is discovery
      seqin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.seq_files.",0)

    pdbin = []
    nbu = basetng.input.size(".phasertng.biological_unit_builder.model.")
    nfd = basetng.input.size(".phasertng.file_discovery.validated.pdb_files.")
    #first xyzins
    if len(working_params.picard.xyzin) > 0:
      n = len(working_params.picard.xyzin)
      basetng.input.resize(".phasertng.biological_unit_builder.model.",n)
      for i,ifile in enumerate(working_params.picard.xyzin):
        ifile = os.path.normpath(ifile)
        basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(ifile),int(i))
        pdbin.append(ifile)
    #second bu models
    elif int(nbu) > 0:
      for i in range(nbu):
        ifile = basetng.input.get_as_str_i(".phasertng.biological_unit_builder.model.",int(i))
        ifile = os.path.normpath(ifile)
        pdbin.append(ifile)
    #third file discovery
    elif int(nfd) > 0:
      basetng.input.resize(".phasertng.biological_unit_builder.model.",nfd)
      for i in range(nfd):
        ifile = isNone(basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",int(i)))
        ifile = os.path.normpath(ifile)
        basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(ifile),int(i))
        pdbin.append(ifile)
    if len(pdbin) == 0:
      raise ValueError(enum_err_code.input,"No model file(s)")

    if seqin is not None: #set to the bub requirement
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    ModeRunner(basetng=basetng,mode_list=['bub'])

    #the pdb files are extracted from the combinations
    #the tags can be extracted from the pdb files
    #the search combinations are the full dir/filename path so that esm can find them
    n = basetng.input.size(".phasertng.search_combination.")
    if (int(n) == 0):
      basetng.logLoopInfo(enum_out_stream.process,"No search combinations")
      basetng.logLoopExit(enum_out_stream.process,loop_auto)
      return
    set_of_esm_filenames = set()
    search_combo_tags = [0]*n #list of 0 to start with
    for i in range(n):
      import ast
      search_combo_tags[i] = [] #list of list
      combo = ast.literal_eval(isNone(basetng.input.get_as_str_i(".phasertng.search_combination.",i)))
      for ifile in combo:
        set_of_esm_filenames.add(ifile)
        tag = os.path.splitext(os.path.basename(ifile))[0] # as in esm
        search_combo_tags[i].append(tag)
    set_of_esm_filenames = sorted(list(set_of_esm_filenames)) #sorted so that generation tncs_order is consistent

    DAGDB = basetng.DAGDATABASE

    wf = basetng.WriteData()
    dryrun_cards_extra = dryrun_cards + '''
phasertng suite write_data off
'''
    basetng.load_moderunner(walk,user_cards,dryrun_cards)
    if seqin is not None: #set to the bub requirement
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))
    ModeRunner(basetng=basetng,mode_list=['imtz'])
    basetng.input.set_as_str(".phasertng.suite.write_data.",str(wf)) #for sgx read
    aniso1 = ModeRunner(basetng=basetng,mode_list=['data','aniso']).solution()

   #{{{
   #{{{
    #generate ensembles
    #ensembles has to be done after the aniso, for the scaling
    loop_ens = "Generate models"
    #we can just change the pathway tracker because pathlog carries the logfile info
    basetng.logLoopOpen(enum_out_stream.process,loop_ens,len(set_of_esm_filenames),
          '\n'.join(n for n in set_of_esm_filenames))
    entries_dagcards = []
    for ifile in set_of_esm_filenames:
      basetng.logLoopNext(enum_out_stream.process,loop_ens,ifile)
      pdbf = os.path.join(ifile)
      basetng.input.set_as_str(".phasertng.model.filename.",pdbf)
      basetng.input.set_as_str(".phasertng.suite.write_data.",str(wf))
     #Mode-Runner(basetng=basetng,mode_list=['emm'])
      ModeRunner(basetng=basetng,mode_list=['esm'])
      entries_dagcards.append(basetng.input.generic_string)
      basetng.load_moderunner(aniso1,user_cards,dryrun_cards)
    basetng.logLoopShut(enum_out_stream.process,loop_ens)
   #}}}

    basetng.input.set_as_str(".phasertng.suite.write_data.",str(False))
    sga1 = ModeRunner(basetng=basetng,mode_list=['sga']).solution()
    basetng.input.set_as_str(".phasertng.suite.write_data.",str(wf))

    #object storing input termination factor keywords
    tfzobj = tfzscore() #does not depend on the input, values from research
    io_terminate = termination(working_params.picard.control)
  # basetng.io_certain = tfzobj.io_certain # for passing deep in runJOIN

    #AJM no -  inside loop for sg and tncs, since must match these two
    #with python there is no such thing as scope! on the second loop the parameter is still defined and collecting!!
    if debug:
        basetng.logLoopInfo(enum_out_stream.process,str(working_params.picard.control.subgroup_list))
    basetng.use_pickle = False
    basetng.mrsolution = dict()
    results = { 'found_solution' : [], #R-factor
                'refined_dag' : [], #will be used for list editing poses across nodes
              }
    subgroups = [] #because I program in c++
    original = {
        'hl' : basetng.input.get_as_str(".phasertng.subgroup.original.hall."),
        'sg' : basetng.input.get_as_str(".phasertng.subgroup.original.spacegroup."),
        'hm' : basetng.input.get_as_str(".phasertng.subgroup.original.hermann_mauguin_symbol.").replace(" ","") }
    n = int(basetng.input.size(".phasertng.subgroup.expansion.spacegroup."))
    assert(n>0)
    i = n-1 #the last one
    p1 = {
        'hl' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hall.",i),
        'sg' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.spacegroup.",i),
        'hm' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hermann_mauguin_symbol.",i).replace(" ",""), }
    if working_params.picard.control.subgroup_alt == 'original_only':
      subgroups = [original]
    elif working_params.picard.control.subgroup_alt == 'subgroup_list':
      #may be overridden by user tho
      if working_params.picard.control.subgroup_list is None or len(working_params.picard.control.subgroup_list) == 0:
          raise ValueError(enum_err_code.input,"No alternative spacegroup selection list")
      def remove_translational_symmetry(space_group_symbol):
          space_group_info = sgtbx.space_group_info(space_group_symbol)
          space_group = space_group_info.group()
          new_space_group = sgtbx.space_group()
          for rt_mx in space_group.all_ops():
            t = sgtbx.tr_vec([int(0) for j in range(3)], 12)
            new_space_group.expand_smx(sgtbx.rt_mx(rt_mx.r(), t))
          return new_space_group
      if debug:
        for i in range(n):
          c1 = basetng.input.get_as_str_i(".phasertng.subgroup.expansion.spacegroup.",i)
          c2 = remove_translational_symmetry(c1)
          c3 = c2.match_tabulated_settings().universal_hermann_mauguin()
          basetng.logLoopInfo(enum_out_stream.process,str(c1)+ " remove trans=  " + str(c3))
      for j in range(len(working_params.picard.control.subgroup_list)):
        c2 = working_params.picard.control.subgroup_list[j].strip()
        centring2 = c2[0]
        c2 = remove_translational_symmetry(c2)
        c2 = c2.match_tabulated_settings().universal_hermann_mauguin()
        for i in range(n):
          c1 = basetng.input.get_as_str_i(".phasertng.subgroup.expansion.spacegroup.",i).strip()
          centring1 = c1[0]
          c1 = remove_translational_symmetry(c1)
          c1 = c1.match_tabulated_settings().universal_hermann_mauguin()
          if debug:
            basetng.logLoopInfo(enum_out_stream.process,str(c1)+ "?=" + str(c2) + "      " + str(centring1)+ "?=" + str(centring2))
          if c1 == c2 and centring1 == centring2:
            basetng.logLoopInfo(enum_out_stream.process,str(c1)+ "=" + str(c2))
            expansion = {
                'hl' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hall.",i),
                'sg' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.spacegroup.",i),
                'hm' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hermann_mauguin_symbol.",i).replace(" ",""), }
            subgroups.append(expansion) # there may be more than one setting
    elif working_params.picard.control.subgroup_alt == 'original_and_p1':
      subgroups = [original, p1]
    elif working_params.picard.control.subgroup_alt == 'p1_only':
      subgroups = [p1]
    elif working_params.picard.control.subgroup_alt == 'all_subgroups':
      for i in range(n):
        expansion = {
              'hl' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hall.",i),
              'sg' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.spacegroup.",i),
              'hm' : basetng.input.get_as_str_i(".phasertng.subgroup.expansion.hermann_mauguin_symbol.",i), }
        subgroups.append(expansion)

   #{{{
    loop_sg = "Space Groups"
    basetng.logLoopOpen(enum_out_stream.process,loop_sg,len(subgroups),
          '\n'.join(n for n in [d['hm'] for d in subgroups]))
    for x,expansion in enumerate(subgroups):
   #{{{
      #this gets passed to modedata
      basetng.logLoopNext(enum_out_stream.process,loop_sg,expansion['hm'])
      if io_terminate.stop_search(results):
        basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nBreak loop and Stop search")
        basetng.logLoopExit(enum_out_stream.process,loop_sg)
        break
      #branch from the sga step
      basetng.load_moderunner(sga1,user_cards,dryrun_cards)
      if (expansion == original):
        #branch from the first aniso
        order_z_expansion_ratio = str(1)
      else:
        basetng.input.set_as_str(".phasertng.space_group_expansion.hall.",str(expansion['hl']))
        #no hm because derived from hall anew
        basetng.input.set_as_str(".phasertng.suite.write_data.",str(wf)) #for sgx read
        ModeRunner(basetng=basetng,mode_list=['sgx'])
        #includes expanding for aniso and correcting for the tncs epsilon changes
        #need result from intermediate
        order_z_expansion_ratio = basetng.input.get_as_str(".phasertng.space_group_expansion.order_z_expansion_ratio.")
      tncso = ModeRunner(basetng=basetng,mode_list=['tncso']).solution()

      #{{{
      loop_tncs = "Tncs order"
      assert(basetng.input.get_as_str(".phasertng.tncs_analysis.number.") != "None")
      tncs_selections = []
      for i in range(int(basetng.input.get_as_str(".phasertng.tncs_analysis.number."))):
          order = basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.order.",i)
          vector = basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.vector.",i).strip()
          tncs_selection = {
            'order': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.order.",i),
            'vector': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.vector.",i),
            'parity': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.parity.",i),
            'unique': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.unique.",i),
            'info': str(order) + ":(" + str(vector).replace(' ',',') +")"}
          tncs_selections.append(tncs_selection)
      #if we don't break before opening the loop the xml file gets stuffed up
      if (len(tncs_selections) == 0):
        continue
      basetng.logLoopOpen(enum_out_stream.process,loop_tncs,len(tncs_selections),
          '\n'.join(n for n in [d['info'] for d in tncs_selections]))
      for tncs_selection in tncs_selections:
     #{{{
        if io_terminate.stop_search(results):
          basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nBreak loop and Stop search")
          basetng.logLoopExit(enum_out_stream.process,loop_tncs)
          break
      # 7F8E, gets good signal with tncs but is actually tncs=1
        basetng.logLoopNext(enum_out_stream.process,loop_tncs,tncs_selection['info'])
        if io_terminate.stop_tncs_loop(results):
          basetng.logLoopInfo(enum_out_stream.process,"Terminate loop on R-factor below " + io_terminate.loop_rfac() + "\nBreak tncs loop")
         #basetng.logLoopExit(enum_out_stream.process,loop_tncs)
          continue
        #branch from tncso
        basetng.load_moderunner(tncso,user_cards,dryrun_cards)
        tncs_order = int(tncs_selection['order']) #cast to int
        basetng.input.set_as_str(".phasertng.tncs.order.",tncs_selection['order'])
        basetng.input.set_as_str(".phasertng.tncs.vector.",tncs_selection['vector'])
        basetng.input.set_as_str(".phasertng.tncs.parity.",tncs_selection['parity'])
        basetng.input.set_as_str(".phasertng.tncs.unique.",tncs_selection['unique'])
        basetng.input.set_as_str(".phasertng.composition.order_z_expansion_ratio.",order_z_expansion_ratio)
        ModeRunner(basetng=basetng,mode_list=['tncs','feff','twin'])
        basetng.input.set_as_str(".phasertng.suite.write_data.",str(wf))
        cca = ModeRunner(basetng=basetng,mode_list=['cca']).solution()
       #will be zero if does not fit
        maximum_z = isNone(basetng.input.get_as_str(".phasertng.composition.maximum_z."))
  #     if (io_nasu is not None and maximum_z is not None and int(maximum_z) < io_nasu):
  #       basetng.logLoopInfo(enum_out_stream.process,
  #              "Input matthews parameters outside allowed range: continue")
  #       continue
        if int(basetng.input.size(".phasertng.matthews.z.")) != float(basetng.input.size(".phasertng.matthews.z.")):
          basetng.logLoopInfo(enum_out_stream.process,
                 "No search combinations: continue")
          continue
        #this is the point for entrance to the cca loop
        loop_cca = "Cell Content Analysis"
        list_matthews_z = []
        for i in range(basetng.input.size(".phasertng.matthews.z.")):
          matthews_z = basetng.input.get_as_str_i(".phasertng.matthews.z.",i)
          #if the user has requested a given asu content
          #but the space group has symm gives uc volume too low
          #e.g. hyp1 input 28 in p422 space group
      #   if (io_nasu is not None and int(matthews_z) != io_nasu):
      #     continue
          list_matthews_z.append({'z':matthews_z,'info':"Z=" + str(matthews_z)})
        #if we don't break before opening the loop the xml file gets stuffed up
        if (len(list_matthews_z) == 0):
          basetng.logLoopInfo(enum_out_stream.process,"No matthews parameters this tncs")
          continue
       #outside matthews_z because can be shared
       #don't enter the restart loop if there will be no nodes inside, fails loops.xml
        #boh incremental initiate
        io_restart = working_params.picard.control.restart_at_cell_content_scaling
        if io_restart is None:
          restart_list = [False,True]
        else:
          restart_list = [io_restart]
        loop_restart = "Incremental vs Determined Cell Content"
        basetng.logLoopOpen(enum_out_stream.process,loop_restart,len(restart_list),str(restart_list))
        for restart in restart_list:
       #{{{
          basetng.restart_at_cell_content_scaling = bool(restart)
          if io_terminate.stop_search(results):
            basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nBreak loop and Stop search")
            basetng.logLoopExit(enum_out_stream.process,loop_restart)
            break
          basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nContinue search")
        # rather than restarting we only want to keep the solutions that have reached the high tfz
          if len(basetng.mrsolution):
            newmrsolution = dict() #inside loop for sg and tncs and restart, since must match these two
            #if the tncs loop terminates with high tfz, then assume we are retaining solutions
            #otherwise we should clear the solutions
            if not io_terminate.io_terminate_tncs_loop_on_rfactor:
              msg3 = "Restart: MR solutions cleared for new tfz"
            else:
              msg3 = "Restart: MR solutions edited to retain only partial with certain tfz"
              for k,modedata in basetng.mrsolution.items():
                if tfzobj.reached_certain_tfz(modedata):
                  msg3 = msg3 + "\nPartial placement, high tfz (TFZ=" + tfzobj.tfzstr(modedata) +")"
                  newmrsolution[k] = modedata
              if len(newmrsolution) == 1:
                 msg3 = msg3 + "\nNo partial placements"
              basetng.mrsolution = newmrsolution
            basetng.logLoopInfo(enum_out_stream.process,msg3)
          #need to reset because basetng.restart_at_cell_content_scaling is not sufficient check for base check if in non-incremental
         #initial placements may be
          basetng.logLoopNext(enum_out_stream.process,loop_restart,str(not basetng.restart_at_cell_content_scaling)) #log on increment not restart
        #can't continue because tncs is not a continue
        # if io_terminate_tncs_loop_on_rfactor and terminate_loop(results,io_terminate_cell_content_scaling_loop_on_rfactor,io_loop_termination_rfactor):
          if io_terminate.stop_loop(results):
            basetng.logLoopInfo(enum_out_stream.process,"Terminate loop on R-factor below " + io_terminate.loop_rfac() + "\nBreak cell content loop")
            continue

          #we add the holds to the nodes here after the cca
          #put the ensembles in the database
          #on the restart loop there may be no z's because the first is already done so 1->0
          #don't add the join node if we are not going to use it
          if len(restart_list) == 2 and basetng.restart_at_cell_content_scaling and len(list_matthews_z) == 1: #restart is invalid if it is the first
            continue
          else:
            basetng.load_moderunner(cca,user_cards,dryrun_cards) # branch from cca
            basetng.input.generic_int = ord('C') #0
            basetng.input.generic_string = ""
            for f in entries_dagcards:
              basetng.input.generic_string = basetng.input.generic_string + f + node_separator
            cca2 = ModeRunner(basetng=basetng,mode_list=['join']).solution()

          basetng.logLoopOpen(enum_out_stream.process,loop_cca,len(list_matthews_z),
              '\n'.join(n for n in [d['info'] for d in list_matthews_z]))
          nfound = len(results['found_solution'])
          found_last_z = True
          for iz,matthews_z in enumerate(list_matthews_z):
         #{{{
            if io_terminate.stop_search(results):
              basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nBreak loop and Stop search")
              basetng.logLoopExit(enum_out_stream.process,loop_cca)
              break
            basetng.logLoopNext(enum_out_stream.process,loop_cca,matthews_z['info'])
            if len(restart_list) == 2 and basetng.restart_at_cell_content_scaling and iz == 0: #restart is invalid if it is the first
              basetng.logLoopInfo(enum_out_stream.process,"Restart cell content scaling not required for initial cell content")
              continue
            elif basetng.restart_at_cell_content_scaling: #restart without partial solutions stored, use this z for css from the start
              found_last_z = False
            elif not found_last_z:
              basetng.logLoopInfo(enum_out_stream.process,"Terminate because nothing found last Z")
           #  basetng.logLoopExit(enum_out_stream.process,loop_cca)
              continue
        # if the copies are explicit in the biological seq then z=1
         #  elif int(matthews_z['z'])%int(tncs_selection['order']) != 0:
         #    basetng.logLoopInfo(enum_out_stream.process,"Cell content not divisible by tncs order")
         #    continue
            basetng.load_moderunner(cca2,user_cards,dryrun_cards)
            basetng.input.set_as_str(".phasertng.cell_content_scaling.z.",str(matthews_z['z']))
            ModeRunner(basetng=basetng,mode_list=['ccs'])

            basetng.input.resize(".phasertng.permutations.tags.",len(search_combo_tags))
            for i,combo in enumerate(search_combo_tags):
              basetng.input.set_as_str_i(".phasertng.permutations.tags.",
                  ' '.join(str(n) for n in combo),i)
           #the target for the ellg is the square of the solved tfz with some tolerance
            basetng.input.set_as_str(".phasertng.permutations.matthews_z.",matthews_z['z'])
           #below truncates the number of searches to maximum_stored
            ellg = ModeRunner(basetng=basetng,mode_list=['perm']).solution()
            if (int(DAGDB.size()) == 0):
                basetng.logLoopInfo(enum_out_stream.process,"cell content analysis error: break permutation loop")
                basetng.logLoopExit(enum_out_stream.process,loop_cca)
                break
            #now we are going to sort the ellg results into order of current partial solutions
            dagperm1 = list_dag_cards_filenames(basetng)
            #if we don't break before opening the loop the xml file gets stuffed up
            if (len(dagperm1) == 0):
              basetng.logLoopInfo(enum_out_stream.process,"No permutations this Z")
              continue
            loop_perm1 = "Seek Permutation Base"
            basetng.logLoopOpen(enum_out_stream.process,loop_perm1,len(dagperm1),"Loop may exit early")
           #assert(len(dagperm1) > 0) # can be zero if tncs not in ccs
            for pcount,perm1 in enumerate(dagperm1):
           #{{{
             #if there are partial solutions but the solution is not completing to low rfactor
             #maybe we want to reduce the solved tfz?
              #jump from ellg, pathlog
              basetng.logLoopNext(enum_out_stream.process,loop_perm1, "P="+str(pcount+1))
              if io_terminate.stop_search(results):
                basetng.logLoopInfo(enum_out_stream.process,"Terminate on R-factor below " + io_terminate.search_rfac() + "\nBreak loop and Stop search")
                basetng.logLoopExit(enum_out_stream.process,loop_perm1)
                break
              basetng.load_moderunner(ellg,user_cards,dryrun_cards)
              basetng.input.set_as_str(".phasertng.dag.cards.subdir.",perm1[0])
              basetng.input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",perm1[1])
             #load the permutation into the dag, print the seeks
              basetng.input.generic_string = "base find"
              find1 = ModeRunner(basetng=basetng,mode_list=['find']).solution()
              first = find1.deepcopy()
              if debug:
                basetng.logLoopInfo(enum_out_stream.process,"first=find1")
              if io_dryrun:
             #{{{
                basetng.logLoopInfo(enum_out_stream.process,"Dry Run")
                n = DAGDB.number_of_seek_loops_final()
                assert(n > 0)
                basetng.input.set_as_str(".phasertng.search.id.",str(DAGDB.seek_next().identifier()))
                basetng.input.set_as_str(".phasertng.search.tag.",str(DAGDB.seek_next().tag()))
                basetng.input.set_as_str(".phasertng.put_solution.clear.",str(False))
                basetng.input.set_as_str(".phasertng.title.","Dry run")
                for nput in range(n):
                  if not DAGDB.constituents_complete(): #this prevents overrun for the tncs multiples
                     ModeRunner(basetng=basetng,mode_list=['put'])
                basetng.input.set_as_def(".phasertng.title.")
                continue
             #}}} dryrun
              #load a bunch of stuff
              #here, look through current seeks, and if there is a significant subset the patch this on
              subset = False
            # first = None
              if use_shortcuts:
                if tfzobj.has_certain_tfz_solution(basetng.mrsolution):
                  basetng.input.generic_int = ord('D') #5
                  #uses full_search not current seeks for tests
                  join0 = ModeRunner(basetng=basetng,mode_list=['join']).solution()
                  subset = (basetng.input.generic_string == "subset")
                  if not subset:
                    basetng.load_moderunner(find1,user_cards,dryrun_cards)
                  else:
                    first = join0.deepcopy()
                    if debug:
                      basetng.logLoopInfo(enum_out_stream.process,"first=join0")
              n_first = int(DAGDB.number_of_seek_loops_first())
              if n_first > 0 and not subset: #if subset then will have reached tfz
             #{{{
                first_pose_searched = True
                loop_poses1 = "Find Base Poses"
                basetng.logLoopOpen(enum_out_stream.process,loop_poses1,n_first,"Expected number of base poses="+str(n_first))
                io_ellg_target = basetng.input.get_as_str(".phasertng.expected.ellg.target.");
                nseek = 1
                assert(int(DAGDB.size()) > 0) #must have one loop for xml
                assert(not DAGDB.first_constituents_found()) #must have one loop for xml
                while ((int(DAGDB.size()) > 0) and (not DAGDB.first_constituents_found())):
               #find first with frf frfr gyre ftf ftfr pose pak rbr tfz
               #{{{
                  nseek = nseek + 1
                  if int(nseek) > io_infinite_loop:
                    raise Sorry("Infinite loop circuit breaker invoked")
                  basetng.logLoopNext(enum_out_stream.process,loop_poses1,"Pose #" + str(DAGDB.NODES.number_of_poses()+1))
                  seek_next = DAGDB.seek_next() # identifier
                  DAGDB.increment_current_seeks_with_identifier(seek_next)
                  current_seeks = DAGDB.list_of_current_seeks()
                  basetng.logLoopInfo(enum_out_stream.process,"Current seeks:\n---"+' '.join(str(x) for x in current_seeks))
                  #this may or may not result in a significant solution
                  #either way, the seek components can't be set correctly when there is a subset in place
                  restored1 = False
                  if use_shortcuts:
                    if debug:
                      basetng.logLoopInfo(enum_out_stream.process,"Use shortcuts")
                    if runJOIN().has_preliminary_match_solution(basetng,current_seeks):
                      basetng.input.generic_int = ord('E') #6
                      join1 = ModeRunner(basetng=basetng,mode_list=['join']).solution()
                      restored1 = (basetng.input.generic_string == "restored")
                      #second and subsequent but without signal, no restoration
                      if not restored1:
                        if debug:
                          basetng.logLoopInfo(enum_out_stream.process,"Load first 1")
                        basetng.load_moderunner(first,user_cards,dryrun_cards)
                      elif restored1 and int(DAGDB.size()) == 0:
                        if debug:
                          basetng.logLoopInfo(enum_out_stream.process,"keep first")
                      else:
                        assert(restored1)
                        assert(int(DAGDB.size()) > 0)
                        if debug:
                          basetng.logLoopInfo(enum_out_stream.process,"Copy join1")
                        first = join1.deepcopy() #if next iter join fails, this is the jumppoint
                        #seek already set internally
                    else:
                      #join=5 fail join=6 fail no prelim match
                      if debug:
                        basetng.logLoopInfo(enum_out_stream.process,"Load first 2")
                      basetng.load_moderunner(first,user_cards,dryrun_cards)
                  if not restored1:
                 #{{{
                    if debug:
                      basetng.logLoopInfo(enum_out_stream.process,"New first search")
              #     seek_next = DAGDB.seek_next() # identifier
              #     DAGDB.increment_current_seeks_with_identifier(seek_next)
              #     current_seeks = DAGDB.list_of_current_seeks()
                    if debug:
                      basetng.logLoopInfo(enum_out_stream.process,"Current seeks:\n---"+' '.join(str(x) for x in current_seeks))
                    if tfzobj.reached_tfz(first):
                      #reset the SEEK_COMPONENTS if we have bailed out early
                      #this condition was the aim if predicting perm for tfz
                  #   DAGDB.reset_seek_components_from_pose()
                      basetng.logLoopExit(enum_out_stream.process,"Base loop truncated with high TFZ")
                      break
                    basetng.input.set_as_str(".phasertng.search.id.",str(seek_next.identifier()))
                    basetng.input.set_as_str(".phasertng.search.tag.",str(seek_next.tag()))
                  # basetng.input.set_as_str(".phasertng.fast_rotation_function.maximum_stored.",str(0))
                 #  if not io_helices:
                    basetng.input.set_as_str(".phasertng.macgyre.ncyc.","5 0 0")
                 #  else:
                 #    basetng.input.set_as_str(".phasertng.macgyre.protocol.",'off')
                    #we want to set the initial limits for speed bailout high
                    vrmsest = (' '.join(str(v) for v in working_params.picard.control.vrms_estimates))
                    basetng.input.set_as_str(".phasertng.fast_rotation_function.vrms_estimates.",vrmsest)
                    basetng.input.set_as_str(".phasertng.fast_translation_function.stop.rf_zscore_under.",str(6))
                    basetng.input.set_as_str(".phasertng.fast_translation_function.stop.rf_percent_under.",str(75))
                    basetng.input.set_as_str(".phasertng.fast_translation_function.stop.best_packing_tfz_over.",str(11))
                    tfz = DAGDB.solved_tfz(tfzobj.tfz_nonpolar,tfzobj.tfz_polar,tfzobj.tfz_p1)
                    basetng.input.set_as_str(".phasertng.packing.high_tfz.",str(tfz))
                    range_tfz = "5 " + str(tfz) #if they are over they are fine, want the grey zone to push these over
                    basetng.input.set_as_str(".phasertng.zscore_equivalent.range_zscore.",str(range_tfz))
                    if nseek > 2: #this puts a yellow circle in the dag before the frf, nothing else
                      ModeRunner(basetng=basetng,mode_list=['find'])
                    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords) #OVERWRITE with input keywords because we have set local defaults
                    first = ModeRunner(basetng=basetng,mode_list=['frf','frfr','gyre','ftf','ftfr','pose','pak']).solution()
                    n_final = int(DAGDB.number_of_seek_loops_final())
                    basetng = store_mrsolution(first,current_seeks,basetng)
                    if (int(DAGDB.size()) > 0):
                   #{{{
                      first_reached_tfz = tfzobj.reached_tfz(first)
                      #if the packing is tfz is high without refinement and there aren't too many, just do full rbr and xref
                      if first_reached_tfz:
                     #{{{
                        basetng.logLoopInfo(enum_out_stream.process,"High TFZ and contrast in base; polish solution")
                        #replace solution
                        basetng,results,first = \
                          polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback,debug)
                     #}}}
                      else:
                     #{{{
                        if first_reached_tfz: #has signal, but lots, do few cycles with vrms
                          basetng.logLoopInfo(enum_out_stream.process,"High TFZ but no contrast in base; quick refinement including vrms")
                          basetng.input.set_as_str(".phasertng.macrbr.ncyc.","5 0 0")
                          basetng.input.set_as_def(".phasertng.macrbr.macrocycle1.")
                        else: #no signal, lots, don't refine vrms
                          basetng.logLoopInfo(enum_out_stream.process,"Low TFZ and no contrast in base; quick refinement excluding vrms")
                          basetng.input.set_as_str(".phasertng.macrbr.ncyc.","5 0 0")
                          basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.","rotn tran")
                          basetng.logLoopInfo(enum_out_stream.process,"Low TFZ and no contrast in base")
                        basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords) #OVERWRITE with input keywords because we have set local defaults
                        first = ModeRunner(basetng=basetng,mode_list=['rbr','tfz']).solution()
                        #now if it has reached good values
                        first_reached_tfz = tfzobj.reached_tfz(first)
                        if first_reached_tfz:
                       #{{{
                          basetng.logLoopInfo(enum_out_stream.process,"High TFZ and contrast in base after rbr; polish solution")
                          basetng,results,first = \
                            polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback,debug)
                        #}}}
                        elif first_reached_tfz:
                       #{{{
                          msg = "High TFZ but no contrast in base after rbr; "
                          basetng.logLoopInfo(enum_out_stream.process,msg + "polishing")
                          basetng,results,first = \
                              polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback,debug)
                        #}}}
                        else:
                       #{{{
                          basetng.logLoopInfo(enum_out_stream.process,"No TFZ signal in base after rbr")
                        #}}}
                     #}}}
                      basetng = store_mrsolution(first,current_seeks,basetng)
                     #branch from first, in case of a side trip
                      basetng.load_moderunner(first,user_cards,dryrun_cards)
                   #}}}
                 #}}}
               #}}}
                basetng.logLoopShut(enum_out_stream.process,loop_poses1)
             #}}}
            # if first is not None:
            #   basetng.load_moderunner(first,user_cards,dryrun_cards)
              first_reached_tfz = tfzobj.reached_tfz(first)
              if not first_reached_tfz:
                basetng.logLoopInfo(enum_out_stream.process,"Base components not found with high TFZ")
                continue
              else:
                basetng.logLoopInfo(enum_out_stream.process,"Base components found with high TFZ")
             #second = first.deepcopy()
              second = first
              n_final = 0 if DAGDB.size() == 0 else int(DAGDB.number_of_seek_loops_final())
              if n_final > 0:
             #{{{
                subset = None # so that the final polishing is done
                loop_final = "Find Final Poses"
                basetng.logLoopOpen(enum_out_stream.process,loop_final,n_final,"Expected number of loops="+str(n_final))
                #use a while loop because fuse can make the loop shorter
                nseek = 0
                last_nfinal = DAGDB.number_of_seek_loops_final()+1 #so that first doesn't fail
                while (int(DAGDB.size()) > 0) and not DAGDB.constituents_complete():
               #{{{
                  #this is another way of testing for the infinite loop, we want to make sure we are going up
                  nseek = nseek + 1
                  if debug:
                    basetng.logLoopInfo(enum_out_stream.process,"Infinite loop count=" + str(nseek))
                  if int(nseek) > io_infinite_loop:
                    raise Sorry("Infinite loop circuit breaker invoked")
                  basetng.load_moderunner(second,user_cards,dryrun_cards)
                  basetng.logLoopNext(enum_out_stream.process,loop_final,DAGDB.seek_info_current_number())
                  #permute the next component in best order
                  etfz = ModeRunner(basetng=basetng,mode_list=['etfz']).solution()
                  dagperm2 = list_dag_cards_filenames(basetng)
                  if debug:
                    basetng.logLoopInfo(enum_out_stream.process,"Number of final=" + str(DAGDB.number_of_seek_loops_final()) + " last=" + str(last_nfinal))
                  if (int(DAGDB.number_of_seek_loops_final()) >= int(last_nfinal)):
                    raise Sorry("Number of components not increased")
                  last_nfinal = DAGDB.number_of_seek_loops_final()
                  found_perm2 = False
                  loop_perm2 = "Seek Permutation Final"
                  basetng.logLoopOpen(enum_out_stream.process,loop_perm2,len(dagperm2),"Loop may exit early")
             #    basetng.load_moderunner(first,user_cards,dryrun_cards)
                 #although all seeks are present, this loop only concerns options for next seek
                  for i,perm2 in enumerate(dagperm2):
                 #{{{
                    if found_perm2:
                      if debug:
                        basetng.logLoopInfo(enum_out_stream.process,"Break perm2")
                      break
                    basetng.logLoopNext(enum_out_stream.process,loop_perm2,DAGDB.seek_info_current_name())
                    basetng.load_moderunner(etfz,user_cards,dryrun_cards)
                    basetng.input.set_as_str(".phasertng.dag.cards.subdir.",perm2[0])
                    basetng.input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",perm2[1])
                   #load the permutation into the dag, print the seeks, this find
                    basetng.input.generic_string = "final find"
                    find2 = ModeRunner(basetng=basetng,mode_list=['find']).solution()
                 # because this is after signal reset from pose should be fine, but irrelevant since we are past this
                 #  DAGDB.reset_seek_components_from_pose() #crashes because is_search is set
                    seek_next = DAGDB.seek_next()
                    DAGDB.increment_current_seeks_with_identifier(seek_next)
                    current_seeks = DAGDB.list_of_current_seeks()
                    basetng.logLoopInfo(enum_out_stream.process,"Current seeks:\n---"+' '.join(str(x) for x in current_seeks))
                    restored2 = False
                    if use_shortcuts:
                      if runJOIN().has_preliminary_match_solution(basetng,current_seeks):
                        basetng.input.generic_int = ord('F') #7
                        join2 = ModeRunner(basetng=basetng,mode_list=['join']).solution()
                        restored2 = (basetng.input.generic_string == "restored")
                        if debug:
                          basetng.logLoopInfo(enum_out_stream.process,"Restored generic str " + str(basetng.input.generic_string))
                        if restored2 and int(DAGDB.size()) > 0:
                          second_reached_tfz = tfzobj.reached_tfz(second)
                          if second_reached_tfz:
                            second = join2.deepcopy()
                            found_perm2 = True
                        else:
                          if debug:
                            basetng.logLoopInfo(enum_out_stream.process,"not restored")
                          basetng.load_moderunner(find2,user_cards,dryrun_cards)
                    if not restored2: # and not found_perm2: #have not found identical with high tfz
                   #{{{
                      basetng.input.set_as_def(".phasertng.fast_rotation_function.maximum_stored.")
                      basetng.input.set_as_str(".phasertng.search.id.",str(seek_next.identifier()))
                      basetng.input.set_as_str(".phasertng.search.tag.",str(seek_next.tag()))
                      basetng.input.set_as_str(".phasertng.fast_translation_function.stop.rf_zscore_under.",str(6))
                      basetng.input.set_as_str(".phasertng.fast_translation_function.stop.rf_percent_under.",str(75))
                      tfz = DAGDB.solved_tfz(tfzobj.tfz_nonpolar,tfzobj.tfz_polar,tfzobj.tfz_p1)
                      basetng.input.set_as_str(".phasertng.fast_translation_function.stop.best_packing_tfz_over.",str(tfz))
                      basetng.input.set_as_str(".phasertng.packing.high_tfz.",str(tfz))
                      basetng.input.set_as_str(".phasertng.macgyre.ncyc.","5 0 0")
                      basetng.input.set_as_def(".phasertng.fast_rotation_function.maximum_stored.")
                     #have to get to pak stage so that stored solution has poses
                      basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords) #OVERWRITE with input keywords because we have set local defaults
                      pak = ModeRunner(basetng=basetng,mode_list=['frf','frfr','gyre','ftf','ftfr','pose','pak']).solution()
                      if (int(DAGDB.size()) > 0):
                     #{{{
                        found_perm2 = True
                        basetng.input.set_as_str(".phasertng.macrbr.ncyc.","10 0 0") #need more than 5 for convergence for 2qb0
                        first_reached_tfz = tfzobj.reached_tfz(first)
                        if first_reached_tfz:
                          basetng.input.set_as_def(".phasertng.macrbr.macrocycle1.")
                        else:
                          basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.","rotn tran") #will polish
                        basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords) #OVERWRITE with input keywords because we have set local defaults
                        pose2 = ModeRunner(basetng=basetng,mode_list=['rbr','tfz']).solution()
                        second = pose2.deepcopy()
                        n_final = int(DAGDB.number_of_seek_loops_final())
                        basetng = store_mrsolution(second,current_seeks,basetng)
                        fused = False
                        #do the fuse, will drop out if there is nothing to fuse
                        #use second reached tfz because if it hasn't then will fail at next step anyway, fuse will retain the best
                        second_reached_tfz = tfzobj.reached_tfz(second)
                        io_fuse = bool(working_params.picard.control.use_fuse)
                        if io_fuse and second_reached_tfz and int(DAGDB.size()) > 1:
                       #{{{
                          second = ModeRunner(basetng=basetng,mode_list=['fuse']).solution()
                          fused = bool(basetng.input.get_as_str(".phasertng.fuse_translation_function.fused.").strip() == "True")
                          if (not fused):
                         #{{{
                            basetng.logLoopInfo(enum_out_stream.process,"Step back to before fuse attempt")
                            basetng.load_moderunner(pose2,user_cards,dryrun_cards)
                            second = pose2.deepcopy()
                         #}}}
                       #}}}
                        second_reached_tfz = tfzobj.reached_tfz(second)
                        if fused or second_reached_tfz:
                       #{{{
                          basetng.logLoopInfo(enum_out_stream.process,"High TFZ and contrast in final after rbr; polish solution")
                          basetng,results,second = \
                            polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback,debug)
                       #  n_final = int(DAGDB.number_of_seek_loops_final())
                       #  isfinal = bool(n_final == 0)
                       # don't increment current seeks, we are storing wrt the loop current seek
                       #  seek_next = DAGDB.seek_next()
                       #  DAGDB.increment_current_seeks_with_identifier(seek_next)
                       #  current_seeks = DAGDB.list_of_current_seeks()
                          basetng = store_mrsolution(second,current_seeks,basetng)
                          basetng.load_moderunner(second,user_cards,dryrun_cards)
                       #}}}
                        elif second_reached_tfz:
                       #{{{
                        #just for interest
                          msg = "High TFZ but no contrast in final after rbr; "
                          basetng.logLoopInfo(enum_out_stream.process,msg + "polishing")
                          basetng,results,second = \
                              polish(basetng,results,working_params,user_cards,dryrun_cards,progress_callback,debug)
                          basetng = store_mrsolution(second,current_seeks,basetng)
                          basetng.load_moderunner(second,user_cards,dryrun_cards)
                       #}}}
                        else:
                       #{{{
                          found_perm2 = False
                          basetng.logLoopInfo(enum_out_stream.process,"No TFZ signal in placements")
                       #}}}
                     #}}}
                      else:
                     #{{{
                        found_perm2 = False
                        basetng = store_mrsolution(pak,current_seeks,basetng)
                        if debug:
                          basetng.logLoopInfo(enum_out_stream.process,"No nodes, perm2 not found")
                     #}}}
                   #}}} not subset
                    else:
                      if debug:
                        basetng.logLoopInfo(enum_out_stream.process,"has been restored perm " + str(found_perm2))
                 #}}} search second permutation
                  basetng.logLoopShut(enum_out_stream.process, loop_perm2)
                  if second is not None:
                    basetng.load_moderunner(second,user_cards,dryrun_cards)
                  new_final = DAGDB.number_of_seek_loops_final()
                  basetng.logLoopInfo(enum_out_stream.process,"Number remaining to place = "+str(new_final))
                  if not found_perm2:
                    basetng.logLoopInfo(enum_out_stream.process,"No placement of constituent, break")
                 #  basetng.logLoopExit(enum_out_stream.process,loop_final)
                    break
               #}}} permutations over the next to place
                basetng.logLoopShut(enum_out_stream.process,loop_final)
             #}}}
              if DAGDB.constituents_complete():
             #{{{
                basetng.logLoopInfo(enum_out_stream.process,"Constituents complete")
                constituents_complete = True
             #}}}
              else:
             #{{{
                basetng.logLoopInfo(enum_out_stream.process,"Constituents not complete")
             #}}}
           #}}} search first components to ellg
            basetng.logLoopShut(enum_out_stream.process,loop_perm1)
            if nfound > 0 and len(results['found_solution']) == nfound:
              basetng.logLoopInfo(enum_out_stream.process,"No additional solutions with this Cell Content Z")
        #     basetng.logLoopExit(enum_out_stream.process,loop_cca)
              found_last_z = False
        #     break
            nfound = len(results['found_solution'])
         #}}} matthews_z
          basetng.logLoopShut(enum_out_stream.process,loop_cca)
       #}}} restart
        basetng.logLoopShut(enum_out_stream.process,loop_restart)
        basetng.reset_reflections() #because the tncs order is about to change
     #}}} tncs
      basetng.logLoopShut(enum_out_stream.process,loop_tncs)
     #}}} tncso
   #}}} sg for loop
    basetng.logLoopShut(enum_out_stream.process,loop_sg)
   #}}}
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
   #}}}
    ModeRunner(basetng=basetng,mode_list=['tree'])
    if not working_params.picard.dryrun:
      emit_callback(basetng.input.generic_string,progress_callback)
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="directory path walked for data files",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  parser.add_argument('-n',"--nproc",help="number of processors",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is None and args.output is None):
    print("At least one of either --phil or --output must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  verbose = False
  if (args.input is not None):
    if verbose: print("Input Directory: " + args.input,flush=True)
    if not (os.path.exists(args.input)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.file_discovery.directory=\"" + str(args.input)+"\"\n"
  if (args.output is not None):
    if verbose: print("Output Directory: " + args.output,flush=True)
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.suite.database=\"" + str(args.output)+"\"\n"
  if (args.nproc is not None):
    if verbose: print("Number of processors: " + args.nproc,flush=True)
    user_phil_str = user_phil_str + "\nphasertng.threads.number=\"" + str(args.nproc)+"\"\n"
  user_phil_str = user_phil_str.replace("\n\n","\n")
  if verbose: print("Phil File: \n" + user_phil_str,flush=True)
  if (args.phil is not None or (args.input is not None and args.output is not None)):
    picard( user_phil_str=user_phil_str)
  else:
    print("command line arguments insufficient",flush=True)
    sys.exit(0)
