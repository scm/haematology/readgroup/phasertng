//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    //for checking for extended molecule
    //not compulsory, used if present
    UnitCell UC;
    double config_hires;
    if (REFLECTIONS != nullptr)
    {
      UC = UnitCell(REFLECTIONS->UC); //reflections not compulsory, used if present
      config_hires = REFLECTIONS->data_hires();
    }
    else
    {
      if (input.value__flt_cell.at(".phasertng.data.unitcell.").is_default())
        throw Error(err::INPUT,"data unitcell not set");
      UC = UnitCell(input.value__flt_cell.at(".phasertng.data.unitcell.").value_or_default());
      if (input.value__flt_paired.at(".phasertng.data.resolution_available.").is_default())
        throw Error(err::INPUT,"data resolution_available not set");
      config_hires =
        input.value__flt_paired.at(".phasertng.data.resolution_available.").value_or_default_min();
    }

    bool use_tncs_correction(false);
    double tncsrmsd(0);
#if 0
    if (use_tncs_correction)
    {
      int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
      if (!nmol)
        throw Error(err::FATAL,"tncs_order not set");
      if (nmol > 1)
      {
        tncsrmsd = REFLECTIONS->TNCS_RMS; //value
      }
    }
#endif

    Models models;
    double average_rmsd;
    models.USE_MSE = input.value__boolean.at(".phasertng.ensemble.selenomethionine.").value_or_default();
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Ensemble");
    if (input.value__path.at(".phasertng.model.filename.").is_default() and
        input.value__path.at(".phasertng.ensemble.filename.").is_default())
      throw Error(err::FILESET,"ensemble filename");
    //take the ensemble name over the model name
    input.value__path.at(".phasertng.ensemble.filename.").is_default() ?
      models.SetFileSystem( input.value__path.at(".phasertng.model.filename.").value_or_default()):
      models.SetFileSystem( input.value__path.at(".phasertng.ensemble.filename.").value_or_default());
    //models.REFLID = REFLECTIONS->reflid();
    logTab(where,"Ensemble read from file: " + models.qfstrq());
    models.read_from_disk(); ///ReadPdb !!!
    if (models.has_read_errors())
      throw Error(err::INPUT,models.read_errors());
    phaser_assert(models.PDB.size());
    if (models.PDB.size() > 1)
    {
      logTab(where,"Model is an Ensemble");
      logTab(where,"Number of models in Ensemble: " + itos(models.PDB.size()));
    }
    if (input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default())
      logTab(out::LOGFILE,"Reorient to principal axes");
    sv_double rmsd(models.PDB[0].size());
    double factor = 8.*scitbx::constants::pi_sq/3.;
    for (int a = 0; a < models.PDB[0].size(); a++)
      rmsd[a] = std::sqrt(std::max(0.0,models.PDB[0][a].B/factor));
    average_rmsd = std::accumulate( rmsd.begin(), rmsd.end(), 0.0)/ rmsd.size();
    logTab(where,"Average rmsd from B-factors: " + dtos(average_rmsd,2));
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Ensemble Variance Estimation");
    logTab(where,"Initial vrms estimation will be corrected by data");
    //find the lowest rms model, to set MTRACE for all stats calculations
    if (models.SERIAL.size() == models.PDB.size())
    {
      logTab(where,"Ensemble rms serial defined in REMARK");
    }
    else
    {
      //try reading the rms serial input
      sv_double VRMS = input.value__flt_numbers.at(".phasertng.ensemble.vrms_estimate.").value_or_default();
      //or the single input vrms of the model
      double VRMS0 = input.value__flt_number.at(".phasertng.model.vrms_estimate.").value_or_default();
      if (VRMS.size() == 1 and models.PDB.size() > 1)
      {
        VRMS.resize(models.PDB.size(),VRMS[0]);
        logWarning("Ensemble vrms estimate set to the same value for all models");
      }
      else
      {
        VRMS.resize(models.PDB.size(),VRMS0);
        logWarning("Ensemble vrms estimate set to the same value for all models");
      }
      if (VRMS.size() != models.PDB.size())
        throw Error(err::INPUT,"Ensemble vrms estimate has incorrect number of values (" + itos(VRMS.size()) + ":" + itos(models.PDB.size()) + ")");
      logTab(where,"Ensemble vrms estimate(s) defined in input");
      models.SERIAL.resize(VRMS.size());
      for (int i = 0; i < VRMS.size(); i++)
      {
        if (!models.SERIAL.add_rms(i,VRMS[i]))
        throw Error(err::FATAL,"Ensemble vrms limits incompatible");
      }
    }
    phaser_assert(models.SERIAL.vrms_input.size() == models.PDB.size());
    logTab(where,"VRMS input value(s): " + dvtos(models.SERIAL.vrms_input,8,6));
    logBlank(where);
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
    //modlid is here in case we want to set the modlid from the tracker not the filename
    Identifier modlid;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Ensemble Identifier");
    {
      //modlid = DAGDB.NODES.front().TRACKER.ULTIMATE;
      //if tag is not input then tne default tag is the stem of the pdb file
      //(stem = the filename without the final extension)
      std::string tag = models.stem();
      if (!input.value__string.at(".phasertng.ensemble.tag.").is_default())
      {
        tag = input.value__string.at(".phasertng.ensemble.tag.").value_or_default();
        logTab(where,"Ensemble tag from input");
      }
      else if (!input.value__string.at(".phasertng.model.tag.").is_default())
      {
        tag = input.value__string.at(".phasertng.model.tag.").value_or_default();
        logTab(where,"Model tag from input");
      }
      else
      {
        logTab(where,"Ensemble tag from filename");
      }
      auto reorient = btos(input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default());
      auto boxscale = dtos(input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").value_or_default());
      modlid.initialize_from_text(models.fstr()+reorient+boxscale); //full path filename
      modlid.initialize_tag(tag); //overwrites tag only
    }
    logTab(where,"Ensemble identifier: " + modlid.str());
    phaser_assert(modlid.is_set_valid());
    }}

    auto search = DAGDB.add_entry(modlid);
    logUnderLine(out::SUMMARY,"Search");
    logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
    search->ATOM = models.is_atom();
    bool io_helix_test = input.value__boolean.at(".phasertng.molecular_transform.helix_test.").value_or_default();
    search->HELIX = io_helix_test ? models.is_helix() : false;
    search->MAP = false;

    if (input.value__boolean.at(".phasertng.molecular_transform.fix.").value_or_default())
    {
      search->PRINCIPAL_ORIENTATION = {1,0,0, 0,1,0, 0,0,1};
      search->PRINCIPAL_TRANSLATION = {0,0,0};
      search->CENTRE = {0,0,0};
      logTab(out::VERBOSE,"Reorient rotation: [1,0,0, 0,1,0, 0,0,1] (fixed)");
      logTab(out::VERBOSE,"Reorient translation: [0,0,0] (fixed)");
    }
    else if (input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default())
    {
      models.set_principal_components();
      models.reorient_on_centre(models.components.PR,models.components.PT,dvect3(0,0,0));
      search->PRINCIPAL_ORIENTATION = models.components.PR;
      search->PRINCIPAL_TRANSLATION = models.components.PT;
      search->CENTRE = -models.components.PT;
      logTab(out::VERBOSE,"Reorient rotation: " + dmtos(models.components.PR,3));
      logTab(out::VERBOSE,"Reorient translation: " + dvtos(models.components.PT,3));
    }
    else
    {
      models.set_principal_statistics();
      models.recentre(models.statistics.CENTRE);
      search->PRINCIPAL_ORIENTATION = {1,0,0, 0,1,0, 0,0,1};
      search->PRINCIPAL_TRANSLATION = -models.statistics.CENTRE;
      search->CENTRE = models.statistics.CENTRE;
      logTab(out::VERBOSE,"No reorientation");
      logTab(out::VERBOSE,"Centring translation: " + dvtos(models.statistics.CENTRE,3));
    }

    if (!input.value__flt_number.at(".phasertng.molecular_transform.bfactor_trim.").is_default())
    {
      double trim = input.value__flt_number.at(".phasertng.molecular_transform.bfactor_trim.").value_or_default();
      double frac = models.trim(trim);
      logTab(out::LOGFILE,"Trimming at level " + dtos(trim) + " (" + dtos(100*frac,1) + "%)");
    }

    if (input.value__boolean.at(".phasertng.ensemble.convert_rmsd_to_bfac.").value_or_default())
    {
      models.convert_rmsd_to_bfac();
      logTab(out::LOGFILE,"Rmsd converted to B-factors");
    }


    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Scattering Analysis");
    double av_scattering;
    double av_mw;
    if (models.PDB.size() > 1)
    {
      sv_int models_scattering = models.scattering();
      sv_double models_mw = models.molecular_weight();
      av_scattering = std::accumulate(models_scattering.begin(),models_scattering.end(),0.0);
      av_scattering /= models_scattering.size();
      av_mw = std::accumulate(models_mw.begin(),models_mw.end(),0.0); //double for double accumulation
      av_mw /= models_mw.size();
      double percent = input.value__percent.at(".phasertng.ensemble.percent_deviation.").value_or_default();
      double maxdeviation((percent/100.00)*av_scattering);
      logTab(where,"Disable check on deviation of scattering from mean: " + btos(input.value__boolean.at(".phasertng.ensemble.disable_percent_deviation_check.").value_or_default()));
      logTab(where,"Maximum deviation of scattering " + dtos(maxdeviation,2));
      for (int m = 0; m < models.PDB.size(); m++)
      {
        double  thisScat = models_scattering[m];
        double& thisMw = models_mw[m];
        double deviation(std::fabs(thisScat - av_scattering));
        bool scattering_deviates(deviation > maxdeviation);
        logTab(where,"Scattering this model " + dtos(thisScat,2));
        logTab(where,"Mw this model " + dtos(thisMw,2));
        logTab(where,"Scattering deviation " + dtos(thisScat-av_scattering,2));
        if (scattering_deviates)
        {
          logTab(where,"Model #" + itos(m+1) + " deviates more than " + dtos(percent,2) + "% in scattering from the reference");
          logTab(where,"Scattering of reference " + dtos(av_scattering,2));
          logTab(where,"Scattering this model " + dtos(thisScat,2));
          logTab(where,"Scattering deviation " + dtos(thisScat-av_scattering,2));
          logTab(where,"This model may contain domains or large loops not present in the other models");
          if (!input.value__boolean.at(".phasertng.ensemble.disable_percent_deviation_check.").value_or_default())
            throw Error(err::INPUT,"Scattering deviates more than " + dtos(percent,2) + "%");
        }
      }
    }
    else
    {
      av_scattering = models.scattering()[0];
      av_mw = models.molecular_weight()[0];
    }
    logTab(where,"Scattering: " + dtos(av_scattering,0));
    logTab(where,"Molecular Weight: " + dtos(av_mw));
    search->SCATTERING = std::ceil(av_scattering);
    search->MOLECULAR_WEIGHT = av_mw;
    }}

    //use the user defined modlid if it is given
    models.set_principal_statistics();
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Ensemble Properties");
    search->MEAN_RADIUS = models.statistics.MEAN_RADIUS;
    search->EXTENT = models.statistics.EXTENT;
    {
    //calculate the volume from the sequence, same as for the map
    Sequence seq(models.SELENOMETHIONINE);
    bool use_unknown(false);
    seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
    seq.initialize(true,true);
    search->VOLUME = seq.volume();
    }
    logTab(where,"Mean Radius: " + dtos(models.statistics.MEAN_RADIUS,2));
    logTab(where,"Extent: " + dvtos(models.statistics.EXTENT,2));
    logTab(where,"Volume: " + dtos(search->VOLUME,2));
    }}


    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Model Atom/Helix/Barbwire Check");
    io_helix_test ? //delay report
      logTab(where,"Helix test will be performed") :
      logTab(where,"Helix test will NOT be performed");
    logTab(where,"Atom:  " + btos(search->ATOM));
    logTab(where,"Helix: " + btos(search->HELIX));
    if (models.is_atom())
    throw Error(err::INPUT,"Cannot build ensembles from single atom");
    double coilx = 2;
    bool barbwire_test = input.value__boolean.at(".phasertng.molecular_transform.barbwire_test.").value_or_default();
    dvect3 extent = models.statistics.EXTENT;
    dvect3 cell = UC.GetBox();
    std::sort(extent.begin(),extent.end());
    std::sort(cell.begin(),cell.end());
    if (!UC.is_default() and ( //if the data.unitcell is not set and no reflections
        extent[0] > coilx*cell[0] or
        extent[1] > coilx*cell[1] or
        extent[2] > coilx*cell[2]))
    {
      logBlank(where);
      logTab(where,"Model Extent: " + dvtos(extent,1));
      logTab(where,"Cell Extent:  " + dtos(coilx,2) + "*(" + dvtos(cell,1) + ")=" + dvtos(coilx*cell,1));
      logWarning("Model is extremely extended");
      logTab(where,"To remove this warning change phasertng.molecular_transform.random_coil_unit_cell_factor");
      if (models.is_random_coil(0.90))
      {
        logWarning("Model appears to be random coil with high per-residue asa");
      }
      if (barbwire_test)
      {
        throw Error(err::INPUT,"Model is extremely extended, larger than target unit cell, and likely alphafold barbwire. To remove this error turn off barbwire_test.");
      }
    }
    logTab(where,"Barbwire:  " + btos(false));
    }}

    ModelsFcalc& fcalcs = search->IFCALCS;
    fcalcs.MODLID = search->identify();
    //fcalcs.REFLID = REFLECTIONS->reflid();
    fcalcs.TIMESTAMP = TimeStamp();
    fcalcs.NMODELS = models.PDB.size();

    sv_int wilson_bin; //the bins used for wilson scaling
    int    min_bins_for_wilson(6); //3 in the middle (linear) + 2 unreliable end points
    double lores_for_wilson = std::max(2*config_hires, 5.); // Avoid data below 5 A if possible
    {{
    dvect3 extent = models.statistics.EXTENT;
    do
    {
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Structure Factor Calculation");
      logTab(where,"Extent: " + dvtos(extent));
      logTab(where,"Use Bulk: " + btos(input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default()));
      logTab(where,"Fsol: " + dtos(input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default()));
      logTab(where,"Bsol: " + dtos(input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()));
      double vboxscale = 2;
      UnitCell box(extent);
      auto str = box.build_variance_unit_cell(vboxscale,config_hires);
      logTabArray(where,str);
       //add padding to resolution for cell scaling
      //The structure factors below are calculated with the B-factors of the model
      // without the Data WilsonB subtracted from them,
      //they will be normalized to Ecalc, but the density calculation is affected by model b's
      double shannon = 3;
      bool interpolate = true; ////because we will need the binning for interpolation
      UnitCell ensembleUC(search->EXTENT);
      //use the min to account for worst case extrapolation
      double boxscale = input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").minval();
      ensembleUC.build_interpolation_unit_cell(boxscale,config_hires);
      double savereso; cctbx::uctbx::unit_cell gridding_uc;
      std::tie(savereso,std::ignore) = ensembleUC.gridding(
          config_hires,
          input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default(),
          interpolate,
          1.);
      std::tie(std::ignore,gridding_uc) = box.gridding(
          config_hires,
          input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default(),
          interpolate,
          1.);
      fcalcs.p1_structure_factor_calculation(
          &models,
          box,
          savereso,
          gridding_uc,
          shannon,
          NTHREADS,
          input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()
          );
      phaser_assert(fcalcs.FCALC.num_rows() == models.PDB.size());
      double f = fcalcs.setup_bins_with_statistically_weighted_bin_width(
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
           ensembleUC.GetBox(),
#else
           extent,
#endif
           input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_min(),
           input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_max(),
           input.value__int_number.at(".phasertng.bins.molecular_transforms.width.").value_or_default()
         );
      logTab(where,"Cell " + dvtos(ensembleUC.GetBox()));
      logTab(where,"Number of reflections " + itos(fcalcs.MILLER.size()));
      logTab(where,"Number of bins " + itos(fcalcs.BIN.numbins()));
      logTab(where,"Configure resolution " + dtos(config_hires));
      logTab(where,"Save resolution " + dtos(savereso));
      logTab(where,"Stored high resolution (accounting for cell scale) " + dtos(fcalcs.FCALC_HIRES));
      logTab(where,"Ensemble Bin hires " + dtos(fcalcs.BIN.hires()));
      logTab(where,"Statistical weighting factor = " + dtos(f));
      logTabArray(where,fcalcs.BIN.logBin("Statistically weighted binning"));
      fcalcs.initialize_bin(fcalcs.BIN); //paranoia check
      wilson_bin = fcalcs.BIN.wilson_bins(lores_for_wilson,config_hires);
      //for lookup into the bin array, for plotting
      if (wilson_bin.size() < min_bins_for_wilson) //should be defined by bins range limits also
      {
        logTab(where,"Not enough data for Wilson scaling");
        extent *= 1.1; //inflate the cell by 10% each time, will be small to start with
      }
      else
      {
        logTab(where,"Enough data for Wilson scaling");
      }
    }
    while (wilson_bin.size() < min_bins_for_wilson);
    }}

    models.RELATIVE_WILSON_B.resize(models.PDB.size());
    std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);

    double SIGA_RMSD = 0;
    double fsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
    double bsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
    double minb = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();
    FourParameterSigmaA solTerm(SIGA_RMSD,fsol,bsol,minb);

    double best_vrms(std::numeric_limits<double>::max());
    for (auto rms : models.SERIAL.vrms_input)
      best_vrms = std::min(rms, best_vrms);
    double best_rms(0),worst_rms(0);
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Initial V-rms parameters");
    logTab(where,"Number of bins " + itos(fcalcs.BIN.numbins()));
    //limit by non-zero sigmaa
    {
      worst_rms = solTerm.worst_rms(config_hires);
      for (auto& rms : models.SERIAL.vrms_input)
      {
        rms = std::min(rms,worst_rms);
        best_vrms = std::min(rms, best_vrms);
      }
      logTab(where,"Worst rms for resolution " + dtos(worst_rms));
    }
    {{
      //the following sets a wide range that would be reasonable, before considering ensemble correlations
      //if your input value is outside this range, adjust the range not the input value
      //user can do something stupid but don't want to flag this
      double best_factor = 10.;
      double worst_factor = 6.;
      best_rms = (config_hires/ // Lowest rms that would make a difference given resolution
                 best_factor);
      double def_worst_rms = (models.statistics.MEAN_RADIUS/
                  worst_factor); // Highest rms that might be sensible for size of model
      logTab(where,"Default best rms " + dtos(best_rms));
      logTab(where,"Default worst rms " + dtos(def_worst_rms));
      worst_rms = std::min(worst_rms,def_worst_rms);
      //these values can be fudged because they are fudged anyway
      //the input value must be acceptable, the default ranges should not mess with them
      bool limit_by_range(true);
      if (limit_by_range)
      {
        for (auto& rms : models.SERIAL.vrms_input)
        {
          rms = std::min(rms,worst_rms);
          rms = std::max(rms,best_rms);
          best_vrms = std::min(rms, best_vrms);
        }
      }
      else
      {
        for (auto rms : models.SERIAL.vrms_input)
          worst_rms = std::max(rms,worst_rms);
        best_rms = std::min(best_rms,best_vrms);
      }
    }}

#if 0
    if (use_tncs_correction)
    {
      logChevron(where,"Model rmsd tNCS initial limits");
      bool use_tncs_refined_rmsd = true;
      (use_tncs_refined_rmsd) ?
        logTab(where,"Rmsd limited by refined tNCS rmsd"):
        logTab(where,"Rmsd limited by upper limit for refinement of tNCS rmsd");
      logTab(where,"Rmsd input limit = " + dtos(tncsrmsd));
      for (auto& vrms_input : models.SERIAL.vrms_input)
        vrms_input = std::max(vrms_input, tncsrmsd);
    }
#endif
    logTab(where,"Initial best rms " + dtos(best_rms));
    logTab(where,"Initial worst rms " + dtos(worst_rms));
    }}

    if (best_rms > worst_rms)
      throw Error(err::FATAL,"Ensemble rms limits incompatible: best (" + dtos(best_rms) + ") > (" + dtos(worst_rms) + ") worst");

    bool uncorrelated(false);
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Checking low resolution correlations");
    double correlation_resolution_limit = 10;
    double OffDiagCorMat_lower_limit(0.1);
    logTab(where,"Correlations to " +  dtos(correlation_resolution_limit) + " Angstroms");
    logTab(where,"Lower limit: " + dtos(OffDiagCorMat_lower_limit));
    {//memory
      double midSsqr; versa_flex_double CorMat;
      std::tie(midSsqr,CorMat) = fcalcs.calculate_correlation_matrix(20.0,6.0,correlation_resolution_limit);
      phaser_assert(CorMat.size() == models.PDB.size()*models.PDB.size());
      for (int i = 0; i < models.PDB.size(); i++)
      {
        for (int j = i+1; j < models.PDB.size(); j++) // Unique off-diagonal elements
        {
          out::stream where = out::VERBOSE;
          double OffDiagCorMat = CorMat(i,j);
          if (OffDiagCorMat < OffDiagCorMat_lower_limit)
          {
            logTab(where,"Structure Factors of Models " + itos(i+1,2,false,false) + " and " + itos(j+1,2,false,false) + ": uncorrelated " + dtos(OffDiagCorMat) + " <  " + dtos(OffDiagCorMat_lower_limit));
            uncorrelated = true; //want them to be correlated
          }
          else
          {
            logTab(where,"Structure Factors of Models " + itos(i+1,2,false,false) + " and " + itos(j+1,2,false,false) + ":   correlated " + dtos(OffDiagCorMat) + " >= " + dtos(OffDiagCorMat_lower_limit));
          }
        }
      }
    }//memory
    if (uncorrelated)
    {
      logTab(where,"Structure factors are uncorrelated");
      logTab(where,"Check that the models in this ensemble are *aligned* and are *homologous*");
      if (!input.value__boolean.at(".phasertng.ensemble.disable_structure_factors_correlated_check.").value_or_default())
        throw Error(err::INPUT,"Structure factors uncorrelated");
    }
    else
    {
      logTab(where,"Structure factors are correlated");
      logTab(where,"Models in this ensemble are probably *aligned* and *homologous*");
    }
    }}

    sv_double unparse_cormat;
    sv_double unparse_rhomat;
    sv_double unparse_corrected_rms;
    sv_double unparse_input_rms;
    bool inconsistent(false);
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Checking high resolution correlations");
    // Start correlation analysis at full resolution regardless of initial RMS estimates to avoid
    // dependence on those estimates
    double resolution_d_min = config_hires;
    logTab(where,"Correlations to " +  dtos(resolution_d_min) + " Angstroms");
    // Correlations at full resolution  may be essentially random, so back off if necessary.
    // Don't go to lower than 10A, where correlations should be positive in any sensible calculation!
   // try {
    double midSsqr; versa_flex_double CorMat;
    std::tie(midSsqr,CorMat) = fcalcs.calculate_correlation_matrix(10.0,6.0,resolution_d_min);
   // } catch (...) { throw Error(err::FATAL,"Correlation matrix calculation"); }
    {{
      af_string lines;
      lines.push_back("Correlation Matrix");
      for (int i = 0; i < CorMat.ref().accessor().all()[0]; i++)
      {
        std::string line;
        for (int j = 0; j < CorMat.ref().accessor().all()[1]; j++)
          line += dtos(CorMat(i,j),5,3) + " ";
        lines.push_back(line);
      }
      logTabArray(where,lines);
    }}

    // Initialise lower and upper bounds.  Lower bound computed to give same value for any input vrms
    // values that only differ by change in DRMS.
    {//memory
    logChevron(where,"Model rmsd initial limits");
    for (int m = 0; m < models.SERIAL.size(); m++)
    {
      double lower(fn::pow2(best_rms)+fn::pow2(models.SERIAL.vrms_input[m])-fn::pow2(best_vrms));
      PHASER_ASSERT(lower>=0);
      models.SERIAL.vrms_lower[m] = std::sqrt(lower);
      models.SERIAL.vrms_upper[m] = worst_rms;
      models.SERIAL.vrms_start = models.SERIAL.vrms_input;
    }
    models.SERIAL.setup_drms_first_and_limits();
    logTabArray(where,models.SERIAL.logfile());
    }
    logChevron(where,"Model lower rmsd consistent with correlations");
    {//memory
      // Reset lower limits for VRMS to smallest values consistent with correlations by starting from
      // lowest plausible vrms estimates
      ConsistentDLuz min_consistent_dluz;
      sv_double minrms_list = models.SERIAL.vrms_lower;
      int whilecount = min_consistent_dluz.setup(solTerm,minrms_list,CorMat,midSsqr);
      models.SERIAL.vrms_lower = min_consistent_dluz.Rms_out;
      logTab(where,"Number of cycles for consistency " + itos(whilecount));
      models.SERIAL.setup_drms_first_and_limits();
      logTabArray(where,models.SERIAL.logfile());
    }//memory
    ConsistentDLuz consistent_dluz;
    {
    logChevron(where,"Model rmsd start within range");
    // Now make starting values consistent with correlations and latest lower limits
    phaser_assert(models.SERIAL.vrms_input.size() == models.PDB.size());
    int whilecount = consistent_dluz.setup(solTerm,models.SERIAL.vrms_input,CorMat,midSsqr);
    models.SERIAL.vrms_start = consistent_dluz.Rms_out;
    logTab(where,"Number of cycles for consistency " + itos(whilecount));
    models.SERIAL.setup_drms_first_and_limits();
    logTabArray(where,models.SERIAL.logfile());
    }
    // Readjust vrms_start so that all bins will reach vrms_lower with the same value of DRMS
    // This is to ensure consistency for any input vrms values that only differ by change in DRMS
    // First make sure that vrms_start is at least as large as earlier vrms_lower result, which
    // will change a bit with different starting points below the minimum
    {
    logChevron(where,"Model start rmsd adjusted for range");
    for (int m = 0; m < models.SERIAL.size(); m++)
      models.SERIAL.vrms_start[m] = std::max(models.SERIAL.vrms_start[m],models.SERIAL.vrms_lower[m]);
    models.SERIAL.setup_drms_first_and_limits();
    logTabArray(where,models.SERIAL.logfile());
    }
    {
    double mean_diff_of_squares(0.);
    logChevron(where,"Model rmsd adjusted for drms shifts");
    models.SERIAL.vrms_start.resize(models.SERIAL.size(),0);
    for (int m = 0; m < models.SERIAL.size(); m++)
      mean_diff_of_squares += fn::pow2(models.SERIAL.vrms_start[m]) - fn::pow2(models.SERIAL.vrms_lower[m]);
    mean_diff_of_squares /= models.SERIAL.size();
    for (int m = 0; m < models.SERIAL.size(); m++)
      models.SERIAL.vrms_start[m] = std::sqrt(fn::pow2(models.SERIAL.vrms_lower[m]) + mean_diff_of_squares);
    models.SERIAL.setup_drms_first_and_limits();
    logTabArray(where,models.SERIAL.logfile());
    }

    for (int m = 0; m < models.PDB.size(); m++)
    {
      unparse_corrected_rms.push_back(consistent_dluz.Rms_out[m]);
      unparse_input_rms.push_back(consistent_dluz.Rms_in[m]);
      for (int n = 0; n < models.PDB.size(); n++)
      {
        unparse_cormat.push_back(CorMat(m,n));
        unparse_rhomat.push_back(consistent_dluz.rhoDelta12(m,n));
      }
    }
    inconsistent = consistent_dluz.inconsistent;
    }}

    for (auto rms : models.SERIAL.vrms_start)
    {
      if (rms < best_rms) // Probably paranoia
      logWarning("Model rmsd ("+dtos(rms)+") < best allowed ("+dtos(best_rms)+")");
      if (rms > worst_rms)
      logWarning("Model rmsd ("+dtos(rms)+") > worst allowed ("+dtos(worst_rms)+")");
    }

    //must deal with the adjustments that come from tNCS relationships
    if (use_tncs_correction)
    {
      out::stream where = out::VERBOSE;
      // To ensure that variances do not become negative on refinement of the
      // VRMS of the model, the VRMS cannot go below the VRMS for tNCS-related copies,
      // deduced from the observed data.
      // This is actually conservative, because the model is probably not complete (fs=1)
      // and there will be a contribution of measurement error to the variance.
      logChevron(where,"Model rmsd tNCS lower limits");
      bool use_tncs_refined_rmsd = true;
      (use_tncs_refined_rmsd) ?
        logTab(where,"Rmsd limited by refined tNCS rmsd"):
        logTab(where,"Rmsd limited by upper limit for refinement of tNCS rmsd");
      logTab(where,"Rmsd lower limit = " + dtos(tncsrmsd));
      if (best_rms < tncsrmsd and tncsrmsd <= worst_rms)
        best_rms = tncsrmsd;
      for (int m = 0; m < models.SERIAL.size(); m++)
      {
        models.SERIAL.vrms_lower[m] = std::max(best_rms,models.SERIAL.vrms_lower[m]);
      }
      models.SERIAL.setup_drms_first_and_limits();
      logTabArray(where,models.SERIAL.logfile());
    }

    for (int m = 0; m < models.SERIAL.size(); m++)
    {
      //there is no good way of correcting this, it should throw error
      //but for analysis, want to report the results regardless
      //errors will be thrown if you try to use these values in the ensembling
      const double& rms = models.SERIAL.vrms_start[m]; //rms in scope
      best_rms = models.SERIAL.vrms_lower[m];
      worst_rms = models.SERIAL.vrms_upper[m];
      if (best_rms > worst_rms)
      throw Error(err::INPUT,"Best rmsd ("+dtos(best_rms)+") > worst allowed ("+dtos(worst_rms)+"): Adjust estimate of rmsd input");
      if (rms < best_rms)
      logWarning("Model rmsd ("+dtos(rms)+") < best allowed ("+dtos(best_rms)+")");
      if (rms > worst_rms)
      logWarning("Model rmsd ("+dtos(rms)+") > worst allowed ("+dtos(worst_rms)+")");
    }

    //setup all the internal parameters
    models.SERIAL.setup_drms_first_and_limits();

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Variance-rms start and range");
    logTab(where,"Adjusted to agree with data");
    logTab(where,"VRMS input value(s): " + dvtos(models.SERIAL.vrms_input,5,2));
    logTab(where,"VRMS start value(s): " + dvtos(models.SERIAL.vrms_start,5,2));
    logTab(where,"VRMS upper limit(s): " + dvtos(models.SERIAL.vrms_upper,5,2));
    logTab(where,"VRMS lower limit(s): " + dvtos(models.SERIAL.vrms_lower,5,2));
    //print results
    logTab(where,"DRMS lower/upper   : " + dtos(models.SERIAL.drms_lower(),8,6) + " / " + dtos(models.SERIAL.drms_upper(),8,6));
    logBlank(where);
    logTab(where,"Unit Cell: " + dvtos(fcalcs.UC.cctbxUC.parameters()));
    logBlank(where);
    }}

    models.INPUT_CELL = UnitCell();
    models.MODLID = search->identify();
    //models.REFLID = REFLECTIONS->reflid();
    models.TIMESTAMP = TimeStamp();

   // PHASER_ASSERT(models.has_relative_wilson_bfactor());

    search->IMODELS = models;

    //switch the results in memory too
    search->MODELS = search->IMODELS;
    search->FCALCS = search->IFCALCS;


    std::string molecular_transform_coordinates("");
    if (input.value__boolean.at(".phasertng.molecular_transform.write_coordinates.").value_or_default())
    {
      Models PRPT = models; //deep copy so no inplace modification
//activate this part of the code to write out a model at origin via PR and PT
//so that it can be input into phaser for the results of phasertng and phaser to be compared
      PRPT.move_and_align_to_principal_axes(
          search->PRINCIPAL_ORIENTATION,
          search->PRINCIPAL_TRANSLATION);
      PRPT.INPUT_CELL = UnitCell();
      logUnderLine(out::LOGFILE,"Models for molecular transform");
      PRPT.SetFileSystem(DataBase(),search->DOGTAG.FileName(".mt.pdb"));
      logFileWritten(out::LOGFILE,WriteFiles(),"Molecular Transform Model",PRPT);
      if (WriteFiles()) PRPT.write_to_disk();
      input.value__path.at(".phasertng.pdbout.filename.").set_value(molecular_transform_coordinates);
    }

    search->MODELS = models; //required
    search->DRMS_MIN = models.SERIAL.drms_lower();
    search->DRMS_MAX = models.SERIAL.drms_upper();
    search->VRMS_START = models.SERIAL.vrms_start;
    {{
    logTab(out::LOGFILE,"Append Dag with ensemble");
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      //add the models info to each dag entry
      auto& node = DAGDB.NODES[i];
      dag::Hold hold;
      hold.ENTRY = search; //copy pointer
      hold.IDENTIFIER = search->identify();
      node.HOLD.push_back(hold);
      auto modlid = search->identify();
      node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0.); //create
      node.VRMS_VALUE[modlid] = dag::node_t(modlid,search->VRMS_START[0]); //create
      node.CELL_SCALE[modlid] = dag::node_t(modlid,1.); //create
    }
    DAGDB.reset_entry_pointers(DataBase());
    }}

    //set all the output parameters here
    //input.value__uuid_number.at(".phasertng.ensemble.id.").set_value(modlid.identifier());
    //input.value__string.at(".phasertng.ensemble.tag.").set_value(modlid.tag());
