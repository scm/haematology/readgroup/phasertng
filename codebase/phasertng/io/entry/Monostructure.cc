//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Monostructure.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/constants.h>

namespace phasertng {

  void
  Monostructure::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());

    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    infile.close();
    Monostructure::SetPdb(pdblines);
  }

  void
  Monostructure::WritePdb(SpaceGroup SG,UnitCell UC)
  {
    PdbHandler pdbhandler(SG,UC,1);
    GetPdbHandler(pdbhandler);
    pdbhandler.VanillaWritePdb(*this);
  }

  af_string
  Monostructure::get_pdb_cards()
  {
    PdbHandler pdbhandler(SG,UC,1);
    GetPdbHandler(pdbhandler);
    return pdbhandler.PdbString();
  }

  void
  Monostructure::GetPdbHandler(PdbHandler& pdbhandler)
  {
    phaser_assert(PDB.size());
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(REFLID.is_set_valid());
    PHASER_ASSERT(TIMESTAMP.size());
    for (auto item : REMARKS)
    {
      pdbhandler.addVanillaRemark(item);
    }
    pdbhandler.addRemarkPhaser("MODLID " + MODLID.string() + " " + MODLID.tag());
    pdbhandler.addRemarkPhaser("REFLID " + REFLID.string() + " " + REFLID.tag());
    pdbhandler.addRemarkPhaser("TIMESTAMP " + TIMESTAMP);
/*
    if (SERIAL.size())
    {
      pdbhandler.addRemarkPhaser("DRMS " +
        dtos(SERIAL.drms_range.first,8,6) + " " +
        dtos(SERIAL.drms_range.second,8,6) + " " +
        dtos(SERIAL.drms_first,8,6));
      pdbhandler.addRemarkPhaser("VRMS " +
        dtos(SERIAL.vrms_input[0],8,6) + " " +
        dtos(SERIAL.vrms_start[0],8,6) + " " +
        dtos(SERIAL.vrms_upper[0],8,6) + " " +
        dtos(SERIAL.vrms_lower[0],8,6));
    }
*/
    pdbhandler.addModel();
    for (int a = 0; a < PDB.size(); a++)
    {
      PdbRecord card = PDB[a];
      pdbhandler.addRecord(card);
    }
  }

  void
  Monostructure::SetPdb(af_string pdblines)
  {
    PDB.clear();
    try
    {
      for (auto buffer : pdblines)
      {
        PdbRecord next_atom;
        //only allow ATOM cards at start
        if (!buffer.find("REMARK")) //REMARK
        {
          if (!buffer.find("REMARK " + constants::pdb_remark_number))
          {
            try
            {
              std::stringstream modelstream(buffer);
              std::string remark,remarktag,keyword;
              modelstream >> remark;
              modelstream >> remarktag;
              modelstream >> keyword;
              //card identified with REMARK remarktag TRACE
              if (keyword == "MODLID")
              {
                std::string number,tag;
                modelstream >> number;
                modelstream >> tag;
                MODLID.parse_string(number,tag);
              }
              else if (keyword == "REFLID")
              {
                std::string number,tag;
                modelstream >> number;
                modelstream >> tag;
                REFLID.parse_string(number,tag);
              }
              else if (keyword == "TIMESTAMP")
              {
                modelstream >> TIMESTAMP;
              }
/*
              else if (keyword == "VRMS")
              {
                double value;
                modelstream >> value; //number not used, just pushback in order
                SERIAL.vrms_input.push_back(value);
                modelstream >> value;
                SERIAL.vrms_start.push_back(value);
                modelstream >> value;
                SERIAL.vrms_upper.push_back(value);
                modelstream >> value;
                SERIAL.vrms_lower.push_back(value);
              }
              else if (keyword == "DRMS")
              {
                modelstream >> SERIAL.drms_range.first >> SERIAL.drms_range.second >> SERIAL.drms_first;
              }
*/
            }
            catch (std::stringstream::failure& e) { } //do nothing if parsing fails
          }
          else
          {
            REMARKS.push_back(buffer);
          }
        }
        else if (!buffer.find("ATOM") or !buffer.find("HETATM")) //ATOM and optionally HETATOM at start
        {
          std::set<std::string> this_pdb_errors;
          next_atom.ReadAtomCard(buffer);
          if (next_atom.O < 0.0 && next_atom.AtomName != "UNK ")
            this_pdb_errors.insert("Atom has negative occupancy");
          if (next_atom.X == dvect3(9999,9999,9999) && next_atom.O != 0)
            this_pdb_errors.insert("Atom has coordinates at \"infinity\"");
          if (!next_atom.valid_element)
            this_pdb_errors.insert("Atom element not recognised");
          if (!this_pdb_errors.size() &&
              next_atom.valid_element &&
              next_atom.Element != "H" &&
              next_atom.O >= 0)
          {
            //atom only accepted if it has an element type
            PDB.push_back(next_atom); //if above throws error this record is ignored
          }
          pdb_errors.insert(this_pdb_errors.begin(),this_pdb_errors.end());
        }
       // else if (!buffer.find("ENDMDL")) //no action required
       // else if (!buffer.find("END")) break; //don't read any more
       // pymol outputs END then ENDMDL
      }
    }
    catch (std::exception const& err) {
    }
    if (PDB.size() == 0)
      throw Error(err::INPUT,"Pdb file empty");
  }

  af_dvect3
  Monostructure::get_atom_list() const
  {
    af_dvect3 atom_list;
    for (int a = 0; a < PDB.size(); a++)
      if (PDB[a].O > 0)
        atom_list.push_back(PDB[a].X);
    return atom_list;
  }

  std::string
  Monostructure::read_errors()
  {
    std::string result;
    for (auto item : pdb_errors)
      result += "\n" + item;
    return result;
  }

  void
  Monostructure::erase_invalid()
  {
    if (!PDB.size()) return;
    size_t last = 0;
    for (size_t i = 0; i < PDB.size(); i++)
    { if (PDB[i].valid_element) { PDB[last++] = PDB[i]; } } //fill start with interesting, in order
    PDB.erase(PDB.begin() + last, PDB.end()); //erase the end, which is not rubbish
  }

  std::pair<int,bool>
  Monostructure::calculate_segment_identifiers(const int nres,const int offset)
  {
    phaser_assert(PDB.size());
    //first divide into residues
    std::vector<std::vector<int>> residues; //atoms (number a) in each residue
    std::vector<std::pair<dvect3,std::string>> alphas; //X and chain per residue
    int  last_resnum = PDB.at(0).ResNum;
    residues.push_back(std::vector<int>(0));
    alphas.push_back({PDB.at(0).X,PDB.at(0).Chain}); //initialize with first atom in residue
    for (int a = 0; a < PDB.size(); a++)
    {
      int this_resnum = PDB.at(a).ResNum;
      if (this_resnum != last_resnum) //create new resudue
      {
        residues.push_back(std::vector<int>(0));
        alphas.push_back({PDB.at(a).X,PDB.at(a).Chain}); //initialize with first atom in residue
        last_resnum = this_resnum;
      }
      if (PDB.at(a).is_calpha() or PDB.at(a).is_palpha())
        alphas.back() = {PDB.at(a).X,PDB.at(a).Chain}; //overwrite with the real alpha atom position
      residues.back().push_back(a); //add this atom to the current residue
    }
    //first divide into connected sets of residues
    std::vector<std::vector<int>> connected; //residues in each connected segment
    connected.push_back(std::vector<int>()); //first connected chain
    connected.back().push_back(0); //first residue in first connected chain
    std::string last_Chain; dvect3 last_alphaX;
    std::tie(last_alphaX,last_Chain) = alphas.at(1);
    for (int n = 1; n < residues.size(); n++) //start with second residue
    {
      std::string this_Chain; dvect3 this_alphaX;
      std::tie(this_alphaX,this_Chain) = alphas.at(n);
      bool new_chain(this_Chain != last_Chain);
      last_Chain = this_Chain;
      double distance_between_alphas = (this_alphaX-last_alphaX)*(this_alphaX-last_alphaX);
      bool separated = distance_between_alphas > fn::pow2(5.0);
      last_alphaX = this_alphaX;
      if (separated or new_chain)
        connected.push_back(std::vector<int>(0));
      connected.back().push_back(n); //add this atom to the current residue
    }

    int segid(1); bool maxedout(false);//don't start output at zero
    for (int c = 0; c < connected.size(); c++) //start with second residue
    {
      int segment_nres = connected[c].size();
      if (segment_nres <= nres)
      { //do the best upi cam
        for (int n = 0; n < connected[c].size(); n++)
        {
          for (int b = 0; b < residues[connected[c][n]].size(); b++)
          {
            int a = residues[connected[c][n]][b];
            PDB.at(a).Segment = segid;
          }
        }
        maxedout = true;
        segid++;
      }
      else
      {
        int remainder = segment_nres % nres; //the number hanging over at the end
        int window_nres(0);
        auto list_of_residues = connected[c];
        int nsegments(0);
        for (int n = 0; n < list_of_residues.size(); n++)
        {
          int maximum_nres = nres; //externally calculated, then local modification
          if (nsegments < remainder) //can't be =, if 0 don't inflate
            maximum_nres++; //inflate so that there are fewer left over at the end
          auto list_of_atoms = residues[list_of_residues[n]];
          for (int b = 0; b < list_of_atoms.size(); b++)
          {
            int a = list_of_atoms[b];
            PDB.at(a).Segment = segid;
          }
          window_nres++;
          if (window_nres == maximum_nres)
          {
            window_nres = 0;
            nsegments++;
            segid++;
          }
        }
      }
    }
    return { segid-1, maxedout }; //total number of segments
  }

  std::vector<PdbRecord>
  Monostructure::select_segment(const int segid,int& last)
  {
    //last increments externally and means that the function starts where previous left off
    std::vector<PdbRecord> new_pdb;
    for (size_t a = last; a < PDB.size(); a++)
    {
      auto& card = PDB[a];
      if (card.TER or card.Segment == segid) //will be contiguous in i
      {
        //PDB[last++] = card;
        new_pdb.push_back(card);
      }
      else if (new_pdb.size() > 0)
      {
        break; //we don't have to worry about the rest
        // because we have found the segment and moved past it, rest will fail condition
      }
    }
    phaser_assert(new_pdb.size());
    return new_pdb;
  }

  int
  Monostructure::nresidues()
  {
    int count(0);
    for (int a = 0; a < PDB.size(); a++)
    {
      auto atom = PDB[a];
      if ((atom.O > 0.0) and
          (atom.AltLoc == " " or atom.AltLoc == "A") and //only count altloc once
           atom.AtomName != "UNK ") //already ignored
      {
        if (atom.is_calpha())
          count++;
      }
    }
    return count;
  }

  void
  Monostructure::set_principal_statistics()
  {
    af_dvect3 atom_list;
    for (int a = 0; a < PDB.size(); a++)
      if (PDB[a].O > 0)
        atom_list.push_back(PDB[a].X);
    statistics.set(atom_list);
  }

  void
  Monostructure::set_principal_components()
  {
    af_dvect3 atom_list;
    for (int a = 0; a < PDB.size(); a++)
      if (PDB[a].O > 0)
        atom_list.push_back(PDB[a].X);
    components.set(atom_list);
  }

  void
  Monostructure::shift_centre(dvect3 orth)
  {
    af_dvect3 atom_list;
    for (int a = 0; a < PDB.size(); a++)
      PDB[a].X += orth;
  }

  void
  Monostructure::apply_wilson_bfactor(double WILSON_B_F)
  {
    double WILSON_B_I = 2*WILSON_B_F;
    double bfactor(0);
    for (int a = 0; a < PDB.size(); a++)
      bfactor += PDB[a].B;
    bfactor /= PDB.size();
    double shift = WILSON_B_I - bfactor;
    for (int a = 0; a < PDB.size(); a++)
      PDB[a].B += shift;
  }

} //phasertng
