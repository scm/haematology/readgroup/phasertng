//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/UnitCell.h>
#include <limits>

namespace phaser {

//-------------
// CONSTRUCTORS
//-------------
UnitCell::UnitCell()
{
    cctbxUC = cctbx::uctbx::unit_cell(
        af::double6( 1.0, 1.0, 1.0, 90.0, 90.0, 90.0 )
        );
}
UnitCell::UnitCell(cctbx::uctbx::unit_cell u) { cctbxUC = u; }
UnitCell::UnitCell(af::double6 u) { cctbxUC = cctbx::uctbx::unit_cell(u); }
UnitCell::UnitCell(dvect3 cell)   { cctbxUC = cctbx::uctbx::unit_cell(af::double6(cell[0],cell[1],cell[2],90.0,90.0,90.0)); }

//-------------
// RESET
//-------------
void UnitCell::setUnitCell(dvect3 cell)   { cctbxUC = cctbx::uctbx::unit_cell(af::double6(cell[0],cell[1],cell[2],90,90,90)); }
void UnitCell::setUnitCell(af::double6 u) { cctbxUC = cctbx::uctbx::unit_cell(u); }

//-------------
// PARAMETERS
//-------------
floatType UnitCell::A() { return cctbxUC.parameters()[0];}
floatType UnitCell::B() { return cctbxUC.parameters()[1];}
floatType UnitCell::C() { return cctbxUC.parameters()[2];}
floatType UnitCell::Alpha() { return cctbxUC.parameters()[3];}
floatType UnitCell::Beta()  { return cctbxUC.parameters()[4];}
floatType UnitCell::Gamma() { return cctbxUC.parameters()[5];}
floatType UnitCell::Param(int i) { return cctbxUC.parameters()[i];}
floatType UnitCell::cosAlpha() { return (cctbxUC.parameters()[3] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.parameters()[3])); }
floatType UnitCell::cosBeta()  { return (cctbxUC.parameters()[4] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.parameters()[4])); }
floatType UnitCell::cosGamma() { return (cctbxUC.parameters()[5] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.parameters()[5])); }
floatType UnitCell::aStar() { return cctbxUC.reciprocal_parameters()[0]; }
floatType UnitCell::bStar() { return cctbxUC.reciprocal_parameters()[1]; }
floatType UnitCell::cStar() { return cctbxUC.reciprocal_parameters()[2]; }
floatType UnitCell::cosAlphaStar()
{ return (cctbxUC.reciprocal_parameters()[3] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[3])); }
floatType UnitCell::cosBetaStar()
{ return (cctbxUC.reciprocal_parameters()[4] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[4])); }
floatType UnitCell::cosGammaStar()
{ return (cctbxUC.reciprocal_parameters()[5] == 90.) ? 0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[5])); }
cctbx::uctbx::unit_cell UnitCell::getCctbxUC() { return cctbxUC; }
af::double6 UnitCell::getCell6() const { return cctbxUC.parameters(); }

//-------------
// ORTH/FRAC
//-------------
dmat33 UnitCell::Orth2Frac() { return cctbxUC.fractionalization_matrix(); }
dmat33 UnitCell::Frac2Orth() { return cctbxUC.orthogonalization_matrix(); }
dmat33 UnitCell::doOrth2Frac(dmat33& m) { return cctbxUC.fractionalization_matrix()*m; }
dmat33 UnitCell::doFrac2Orth(dmat33& m) { return cctbxUC.orthogonalization_matrix()*m; }
dvect3 UnitCell::doOrth2Frac(dvect3& v) { return cctbxUC.fractionalization_matrix()*v; }

dvect3 UnitCell::doFrac2Orth(dvect3& v)
{
  dvect3 vv(cctbxUC.orthogonalization_matrix()*v);
  //correct for numerical instability
  floatType limit(1.0e-14/1000.0);
  if (std::fabs(vv[0]) < limit) vv[0] = 0;
  if (std::fabs(vv[1]) < limit) vv[1] = 0;
  if (std::fabs(vv[2]) < limit) vv[2] = 0;
  return vv;
}

//-------------
// SSQR/RESO
//-------------
floatType UnitCell::Ssqr(dvect3& m) const { miller::index<double>* mi = (miller::index<double>*) &m; return cctbxUC.d_star_sq(*mi); }
floatType UnitCell::Ssqr(miller::index<int>& m) const { return cctbxUC.d_star_sq(m); }
floatType UnitCell::S(miller::index<int>& m) { return sqrt(Ssqr(m)); }
floatType UnitCell::reso(miller::index<int>& m) { if (S(m)) return 1/S(m); return std::numeric_limits<floatType>::max(); }

//-------------
// OTHER
//-------------
dvect3 UnitCell::HKLtoVecS(int& h,int& k,int& l)
{
  floatType V = cctbxUC.volume();

  dvect3 basisaStar;
  basisaStar[0] = (cctbxUC.orthogonalization_matrix()(1,1)*cctbxUC.orthogonalization_matrix()(2,2)-cctbxUC.orthogonalization_matrix()(1,2)*cctbxUC.orthogonalization_matrix()(2,1))/V;
  basisaStar[1] = (cctbxUC.orthogonalization_matrix()(2,1)*cctbxUC.orthogonalization_matrix()(0,2)-cctbxUC.orthogonalization_matrix()(2,2)*cctbxUC.orthogonalization_matrix()(0,1))/V;
  basisaStar[2] = (cctbxUC.orthogonalization_matrix()(0,1)*cctbxUC.orthogonalization_matrix()(1,2)-cctbxUC.orthogonalization_matrix()(0,2)*cctbxUC.orthogonalization_matrix()(1,1))/V;

  dvect3 basisbStar;
  basisbStar[0] = (cctbxUC.orthogonalization_matrix()(1,2)*cctbxUC.orthogonalization_matrix()(2,0)-cctbxUC.orthogonalization_matrix()(1,0)*cctbxUC.orthogonalization_matrix()(2,2))/V;
  basisbStar[1] = (cctbxUC.orthogonalization_matrix()(2,2)*cctbxUC.orthogonalization_matrix()(0,0)-cctbxUC.orthogonalization_matrix()(2,0)*cctbxUC.orthogonalization_matrix()(0,2))/V;
  basisbStar[2] = (cctbxUC.orthogonalization_matrix()(0,2)*cctbxUC.orthogonalization_matrix()(1,0)-cctbxUC.orthogonalization_matrix()(0,0)*cctbxUC.orthogonalization_matrix()(1,2))/V;

  dvect3 basiscStar;
  basiscStar[0] = (cctbxUC.orthogonalization_matrix()(1,0)*cctbxUC.orthogonalization_matrix()(2,1)-cctbxUC.orthogonalization_matrix()(1,1)*cctbxUC.orthogonalization_matrix()(2,0))/V;
  basiscStar[1] = (cctbxUC.orthogonalization_matrix()(2,0)*cctbxUC.orthogonalization_matrix()(0,1)-cctbxUC.orthogonalization_matrix()(2,1)*cctbxUC.orthogonalization_matrix()(0,0))/V;
  basiscStar[2] = (cctbxUC.orthogonalization_matrix()(0,0)*cctbxUC.orthogonalization_matrix()(1,1)-cctbxUC.orthogonalization_matrix()(0,1)*cctbxUC.orthogonalization_matrix()(1,0))/V;

  dvect3 vecS;
  vecS[0] = h*basisaStar[0]+k*basisbStar[0]+l*basiscStar[0];
  vecS[1] = h*basisaStar[1]+k*basisbStar[1]+l*basiscStar[1];
  vecS[2] = h*basisaStar[2]+k*basisbStar[2]+l*basiscStar[2];
  return vecS;
}



}//phaser
