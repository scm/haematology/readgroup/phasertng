//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/minimizer/RefineANISO.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <phasertng/data/ReflRowsAnom.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Selected.h>
#include <future>

namespace phasertng {

  RefineANISO::RefineANISO(
      const ReflRowsAnom& REFLECTIONS_,
      Selected& SELECTED_,
      SigmaN& SIGMAN_)
    : RefineBase(),
      REFLECTIONS(&REFLECTIONS_),
      SELECTED(&SELECTED_),
      SIGMAN(&SIGMAN_)
  {
    init();
  }

  RefineANISO::RefineANISO(
      const_ReflectionsPtr REFLECTIONS_,
      SelectedPtr SELECTED_,
      SigmaNPtr SIGMAN_
      )
    : RefineBase(),
      REFLECTIONS(REFLECTIONS_),
      SELECTED(SELECTED_),
      SIGMAN(SIGMAN_)
  {
    init();
  }

  void
  RefineANISO::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineANISO::init()
  {
    phaser_assert(REFLECTIONS->NREFL);
    phaser_assert(REFLECTIONS->has_col(labin::INAT));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    phaser_assert(REFLECTIONS->has_col(labin::BIN));
    phaser_assert(NTHREADS > 0);
    set_sphericity_restraint();
    //outliers and normalization parameters already present
    //now copy to new data structure
    //rejection uses new data structure
    reject_outliers();
    setup_wll();
    numinbin = REFLECTIONS->numinbin();
  }

  void
  RefineANISO::set_sphericity_restraint()
  { //SetupSphericity
    //RESN on file already from estimation
    double sigmaSphericity(10.*fn::pow2(REFLECTIONS->data_hires()/2.5)); //Restrict relative correction at resolution limit
    double QUARTER(1./4.);
    double aStar = REFLECTIONS->UC.cctbxUC.reciprocal_parameters()[0];
    double bStar = REFLECTIONS->UC.cctbxUC.reciprocal_parameters()[1];
    double cStar = REFLECTIONS->UC.cctbxUC.reciprocal_parameters()[2];
    //Convert isotropic equivalent as in largeShifts
    sigmaSphericityBeta[0] = QUARTER*sigmaSphericity*aStar*aStar;
    sigmaSphericityBeta[1] = QUARTER*sigmaSphericity*bStar*bStar;
    sigmaSphericityBeta[2] = QUARTER*sigmaSphericity*cStar*cStar;
    sigmaSphericityBeta[3] = QUARTER*sigmaSphericity*aStar*bStar;
    sigmaSphericityBeta[4] = QUARTER*sigmaSphericity*aStar*cStar;
    sigmaSphericityBeta[5] = QUARTER*sigmaSphericity*bStar*cStar;
  }

  void
  RefineANISO::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(item);
    for (auto item : protocol)
      if (item != "" and
          item != "off" and
          item != "scale" and
          item != "bins" and
          item != "aniso"
        )
        throw Error(err::FATAL,"Protocol flag not recognised: " + item);
    REFINE_SCALE =  (protocol.find("scale") != protocol.end()); //unrestrained
    REFINE_BINS =  (protocol.find("bins") != protocol.end()); //restrained to best curve
    REFINE_ANISO = (protocol.find("aniso") != protocol.end());
    if (protocol.find("off") != protocol.end()) //overwrite
      REFINE_SCALE = REFINE_BINS = REFINE_ANISO = false;
    if (protocol.find("") != protocol.end()) //overwrite
      REFINE_SCALE = REFINE_BINS = REFINE_ANISO = false;

    refine_parameter_.clear();
    refine_parameter_.push_back(REFINE_SCALE ? true : false); //SCALE goes with BEST curve
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++) //BINSCALE - BEST curve shape correction scales
    {
      refine_parameter_.push_back(REFINE_BINS ? true : false);
    }
    REFINE_ANISO_MASK = sv_bool(6,false);
    if (REFINE_ANISO)
    { //setup REFINE_ANISO_MASK once!!!
      dmat6 aniso_junk(3,5,7,11,13,17); //any old junk, but rsym.unique
      //cctbx::sgtbx::space_group DERIVED_PG = REFLECTIONS->SG.group().build_derived_point_group();
      cctbx::sgtbx::site_symmetry site_sym(
          REFLECTIONS->UC.cctbxUC,
          REFLECTIONS->SG.group().build_derived_point_group(),
          cctbx::fractional<>(0,0,0)
        ); //default
      constrained = site_sym.average_u_star(aniso_junk);
      for (int n = 0; n < 6; n++)
        if (constrained[n] < DEF_PPM and constrained[n] > -DEF_PPM)
          constrained[n] = 0.0;
     // before off-diagonal applied
      for (int n = 0; n < 6; n++)
        REFINE_ANISO_MASK[n] = constrained[n] ? true : false;
      for (int n = 3; n < 6; n++)
        constrained[n] *= 2; //Factor of 2 for off-diagonal elements, paranoia
      //work out constraints and partial derivative matrix of beta(i) vs beta(j)
      //for test for whether mixed off-diagonal and diagonal
      constrain_equal = std::vector<sv_int>(6);
      anisoMask_equal = REFINE_ANISO_MASK;
      for (int n1 = 0; n1 < 6; n1++)
      for (int n2 = n1; n2 < 6; n2++)
      {
        if (anisoMask_equal[n1] and
            anisoMask_equal[n2] and
            std::abs(constrained[n1]-constrained[n2]) < DEF_PPM)
        {
          constrain_equal[n1].push_back(n2);
          anisoMask_equal[n2] = (n1==n2);
          // Only set flag for first time constraint is encountered
        }
      }
    }

    for (int n = 0; n < 6; n++)
    {
      (REFINE_ANISO) ? refine_parameter_.push_back(REFINE_ANISO_MASK[n]) : refine_parameter_.push_back(false);
    }

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i])
         nmp++;
    total_number_of_parameters_ = refine_parameter_.size();

    //find the index values of the relevant parameters in the refine_parameter_s array
    //changes by reflection with bin s
    scale = (-999);
    //bin changes per reflection
    ano = { -999, -999, -999, -999, -999, -999 };
    int i(0),m(0);
    m++;
    if (REFINE_SCALE)
    {
      scale = i++; //0 if refined
    }
    m += SIGMAN->BINSCALE.size();
    if (REFINE_BINS)
    {
      i += SIGMAN->BINSCALE.size();
    }
    for (int n = 0; n < 6; n++)
    {
      if (refine_parameter_[m++]) //pick out the refined values
      {
        ano[n] = i++;
      }
    }

    //make sure parameters are within bounds
    auto x = get_macrocycle_parameters();
    auto bound = bounds();
    if (x.size() == bound.size())
    {
      for (int i = 0; i < x.size(); i++)
      {
        if (bound[i].upper_bounded() and (x[i] > bound[i].upper_limit() ))
        {
          x[i] = bound[i].upper_limit();
        }
        if (bound[i].lower_bounded() and (x[i] < bound[i].lower_limit() ))
        {
          x[i] = bound[i].lower_limit();
        }
      }
    }
    set_macrocycle_parameters(x);

  } //set_macrocycle_protocol

  sv_string
  RefineANISO::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    int i(0),m(0);
    if (refine_parameter_[m++]) names[i++] = "SigmaN Scale";
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++]) names[i++] = "SigmaN Bin #" + std::to_string(s+1);
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BHH";
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BKK";
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BLL";
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BHK ";
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BHL ";
    if (refine_parameter_[m++]) names[i++] = "SigmaN Aniso BKL ";
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineANISO::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    if (refine_parameter_[m++])
      pars[i++] = SIGMAN->SCALE_I; //SCALE
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++])
        pars[i++] = SIGMAN->BINSCALE[s]; //BINSCALE
    for (int n = 0; n < 6; n++)
      if (refine_parameter_[m++])
        pars[i++] = SIGMAN->ANISOBETA[n]; //ANISOBETA
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineANISO::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
  //-- sigmaa --
    if (refine_parameter_[m++])
      SIGMAN->SCALE_I = newx[i++]; //SCALE
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++])
        SIGMAN->BINSCALE[s] = newx[i++];  //BINSCALE
    for (int n = 0; n < 6; n++)
      if (refine_parameter_[m++])
        SIGMAN->ANISOBETA[n] = newx[i++]; //ANISOBETA
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineANISO::macrocycle_large_shifts()
  {
    //Get anisotropic shifts that will have similar effect to isotropic B
    double largeIsoBShift(4.);
    dmat6 anisoLargeShifts = largeIsoBShift*SIGMAN->LargeShiftCoeffs();
    int m(0),i(0);
    sv_double largeShifts(nmp);
    if (refine_parameter_[m++])
      largeShifts[i++] = 0.01*SIGMAN->SCALE_I;  //SCALE
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++])
        largeShifts[i++] = 0.05*SIGMAN->BINSCALE[s]; //BINSCALE
    for (int n = 0; n < 6; n++)
      if (refine_parameter_[m++])
        largeShifts[i++] = anisoLargeShifts[n]; //ANISOBETA
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineANISO::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int i(0),m(0);
    //-- sigmaa --
    if (refine_parameter_[m++])
      bounds[i++].off(); //SCALE
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++])
        bounds[i++].lower_on(DEF_PPT); //BINSCALE
    //anisotropy
    for (int n = 0; n < 6; n++)
      if (refine_parameter_[m++])
        bounds[i++].off(); //ANISOBETA
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineANISO::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    if (refine_parameter_[m++])
      repar[i++].off(); //SCALE
    for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      if (refine_parameter_[m++])
        repar[i++].on(0.1); //BINSCALE
    for (int n = 0; n < 6; n++)
      if (refine_parameter_[m++])
        repar[i++].off(); //ANISOBETA
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineANISO::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetType
  RefineANISO::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineANISO::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineANISO::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineANISO::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,false);
    const double tinyprob(std::numeric_limits<double>::min());

    for (int r = beg; r < end; r++)
    {
      const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
      const millnx miller = work_row->get_miller();
      const double iobs = work_row->get_inat();
      const double sigi = work_row->get_siginat();
      const double wll  = labin_wll[r];
      const int    s    = work_row->get_bin();
      const double ssqr  = REFLECTIONS->work_ssqr(miller);
      const bool   cent = REFLECTIONS->work_cent(miller);
      const double eps  = REFLECTIONS->work_eps(miller);
      if (SELECTED->get_selected(r))
      {
        double epsnSigmaN = eps*SIGMAN->term(miller,ssqr,s);
        bool do_grad(do_gradient),do_hess(do_hessian);
        double grad(0),hess(0);
        double prob = cent ?
            pintensity.pIobsCen_grad_hess_by_dSN(iobs,epsnSigmaN,sigi,do_grad,grad,do_hess,hess) :
            pintensity.pIobsAcen_grad_hess_by_dSN(iobs,epsnSigmaN,sigi,do_grad,grad,do_hess,hess);
        if (prob < tinyprob) // Cope with search reaching extremes
        {
          continue;
        }
        tgh.Target -= (std::log(prob) - wll);

        if (do_gradient)
        {
          double dLL_by_dEpsnSigmaN = grad/prob;
          double d2LL_by_dEpsnSigmaN2(0);
          if (do_hessian)
            d2LL_by_dEpsnSigmaN2 = hess/prob - fn::pow2(grad/prob);

          sv_double millerSqr(6);
          millerSqr[0] = -miller[0]*miller[0];
          millerSqr[1] = -miller[1]*miller[1];
          millerSqr[2] = -miller[2]*miller[2];
          millerSqr[3] = -miller[0]*miller[1];
          millerSqr[4] = -miller[0]*miller[2];
          millerSqr[5] = -miller[1]*miller[2];

          //find the index values of the relevant parameters in the refine_parameter_s array
          //changes by reflection with bin s
          // scale in set_macrocycle_protocol
          int bin = REFINE_SCALE ? s+1 : s; //pick out this bin value from the refined set
          // ano in set_macrocycle_protocol

          //Start with ANISO
          double dEpsnSigmaN_by_dScale = 0;
          double dEpsnSigmaN_by_dBin = 0;
          dmat6 dEpsnSigmaN_by_dAno(0,0,0,0,0,0);
          if (REFINE_SCALE)
          {
            dEpsnSigmaN_by_dScale = epsnSigmaN/SIGMAN->SCALE_I;
            tgh.Gradient[scale] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dScale;
            if (do_hessian)
              tgh.Hessian(scale,scale) -= d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dScale);
          }
          if (REFINE_BINS)
          {
            dEpsnSigmaN_by_dBin = epsnSigmaN/SIGMAN->BINSCALE[s];
            tgh.Gradient[bin] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dBin;
            if (do_hessian)
              tgh.Hessian(bin,bin) -=
                  d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dBin);
          }
          if (REFINE_ANISO)
          {
            for (int n = 0; n < 6; n++)
            {
              if (REFINE_ANISO_MASK[n])
              {
                int a = ano[n]; //index into tgh.Hessian array
                dEpsnSigmaN_by_dAno[n] = millerSqr[n]*epsnSigmaN;
                PHASER_ASSERT(a < tgh.Gradient.size());
                tgh.Gradient[a] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dAno[n];
                if (do_hessian)
                {
                  for (int n2 = n; n2 < 6; n2++)
                  {
                    if (REFINE_ANISO_MASK[n2])
                    {
                      int a2 = ano[n2];
                      tgh.Hessian(a,a2) -=
                          d2LL_by_dEpsnSigmaN2*millerSqr[n]*millerSqr[n2]*fn::pow2(epsnSigmaN) +
                          dLL_by_dEpsnSigmaN*millerSqr[n]*millerSqr[n2]*epsnSigmaN;
                      tgh.Hessian(a2,a) = tgh.Hessian(a,a2);
                    }
                  }
                }
              }
            }
          }

          if (do_hessian) //cross terms
          {
            if (REFINE_BINS and REFINE_SCALE)
            {
              tgh.Hessian(scale,bin) -=
                 d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dScale +
                 dLL_by_dEpsnSigmaN*epsnSigmaN/(SIGMAN->BINSCALE[s]*SIGMAN->SCALE_I);
              tgh.Hessian(bin,scale) = tgh.Hessian(scale,bin);
            }
            if (REFINE_ANISO and REFINE_SCALE)
            {
              for (int n = 0; n < 6; n++)
              {
                if (anisoMask_equal[n])
                {
                  int a = ano[n]; //index into tgh.Hessian array
                  tgh.Hessian(a,scale) -=
                      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dScale*dEpsnSigmaN_by_dAno[n] +
                      dLL_by_dEpsnSigmaN*millerSqr[n]*dEpsnSigmaN_by_dScale;
                  tgh.Hessian(scale,a) = tgh.Hessian(a,scale);
                }
              }
            }
            if (REFINE_ANISO and REFINE_BINS)
            {
              PHASER_ASSERT(bin >= 0);
              for (int n = 0; n < 6; n++)
              {
                if (REFINE_ANISO_MASK[n])
                {
                  int a = ano[n]; //index into tgh.Hessian array
                  tgh.Hessian(a,bin) -=
                      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dAno[n] +
                      dLL_by_dEpsnSigmaN*millerSqr[n]*dEpsnSigmaN_by_dBin;
                  tgh.Hessian(bin,a) = tgh.Hessian(a,bin);
                }
              }
            }
          }
        } //do gradient
      }
    }

    return tgh;
  }

  TargetGradientHessianType
  RefineANISO::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,false);

    //calculate target, gradients and hessians without restraints
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector<std::future<TargetGradientHessianType>> results;
      std::vector<std::packaged_task<TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineANISO::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end);
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineANISO::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
      }
    }

    //add restraint terms

    if (do_restraint)
    {
      // Add sphericity restraint terms
      add_restraint(tgh,do_gradient,do_hessian);
    }
    //Gradient and tgh.Hessian already defined for minimization (negative)
    //negate Target here for minimization

    if (REFINE_ANISO) //only case where constraints are relevant
    {
      add_constraint(tgh,do_gradient,do_hessian);
    }
    return tgh;
  }

  void
  RefineANISO::add_restraint(
      TargetGradientHessianType& tgh,
      bool do_gradient,
      bool do_hessian
    )
  {
    dmat6 anisoRemoveIso = SIGMAN->AnisoBetaRemoveIsoB();
    dmat6 dBetaIso_by_dBIso = SIGMAN->IsoB2AnisoBeta();
    dmat6 dBIso_by_dBetaAno = SIGMAN->AnisoBeta2IsoBCoeffs();
    for (int ni = 0; ni < 6; ni++)
    {
      int ai = ano[ni];
      if (REFINE_ANISO_MASK[ni] and sigmaSphericityBeta[ni])
      {
        tgh.Target += fn::pow2(anisoRemoveIso[ni]/sigmaSphericityBeta[ni])/2.;
        if (do_gradient)
        {
          for (int nj = 0; nj < 6; nj++)
          {
            int aj = ano[nj];
            if (REFINE_ANISO_MASK[nj] and sigmaSphericityBeta[nj])
            {
              double dBetaIso_by_dBetaAnoI = dBetaIso_by_dBIso[nj]*dBIso_by_dBetaAno[ni];
              double dBetaAno_by_dBetaAnoI = (ni == nj) ? 1. : 0.;
              tgh.Gradient[ai] +=
                 anisoRemoveIso[nj]*(dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
                           fn::pow2(sigmaSphericityBeta[nj]);
              if (do_hessian)
              {
                for (int nk = 0; nk < 6; nk++)
                {
                  if (REFINE_ANISO_MASK[nk] and sigmaSphericityBeta[nk])
                  {
                    double dBetaIso_by_dBetaAnoI = dBetaIso_by_dBIso[nk]*dBIso_by_dBetaAno[ni];
                    double dBetaAno_by_dBetaAnoI = (nk == ni) ? 1. : 0.;
                    double dBetaIso_by_dBetaAnoJ = dBetaIso_by_dBIso[nk]*dBIso_by_dBetaAno[nj];
                    double dBetaAno_by_dBetaAnoJ = (nk == nj) ? 1. : 0.;
                    tgh.Hessian(ai,aj) +=
                       (dBetaAno_by_dBetaAnoJ-dBetaIso_by_dBetaAnoJ) *
                       (dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
                       fn::pow2(sigmaSphericityBeta[nk]);
                  }
                }
              }
            }
          }
        }
      }
    }

    // Add best correction factor restraint term
    if (BEST_CURVE_RESTRAINT and REFINE_BINS)
    {
      for (int s = 0; s < SIGMAN->BINSCALE.size(); s++)
      {
        // Restrain log of SIGMAN->BINSCALE (shape correction for BEST curve) to zero
        // Sigma for restraint is inspired by relative Wilson plot statistics, but
        // only very weak restraint below 6A resolution to avoid restraint
        // dominating in sparsely populated shells where the BEST curve is imprecise.
        double sigmascale(LOG_WILSON_SIGMA_CONSTANT +
                          LOG_WILSON_SIGMA_FACTOR*fn::pow3(SIGMAN->MIDRES[s]));
        double binfac(SIGMAN->BINSCALE[s]);
        double logbin(std::log(binfac));
        tgh.Target += fn::pow2(logbin/sigmascale)/2.;
        if (do_gradient)
        {
          double igrad = logbin/(binfac*fn::pow2(sigmascale));
          int bin = REFINE_SCALE ? s+1 : s; //pick out this bin value from the refined set
          tgh.Gradient[bin] += igrad;
          if (do_hessian)
          {
            tgh.Hessian(bin,bin) += (1.-logbin)/fn::pow2(binfac*sigmascale);
          }
        }
      }
    }
  }

  void
  RefineANISO::add_constraint(
      TargetGradientHessianType& tgh,
      bool do_gradient,
      bool do_hessian
    )
  {
    if (do_gradient)
    {
      //Constraints
      //Sum partial derivatives to get total derivatives for constrained parameters
      //work out constraints and partial derivative matrix of beta(i) vs beta(j)
      //for test for whether mixed off-diagonal and diagonal
      //dmat6 constrained_grad_check(0,0,0,0,0,0);
      dmat6 orig_grad(0,0,0,0,0,0);
      for (int n1 = 0; n1 < 6; n1++)
      {
        if (constrain_equal[n1].size()-1) // only sum if there are constraints
        {
          double sum(0.);
          for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
          {
            int esn1 = constrain_equal[n1][sn1];
            sum += tgh.Gradient[ano[esn1]];
          }
          for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
          {
            int esn1 = constrain_equal[n1][sn1];
            orig_grad[esn1] = tgh.Gradient[ano[esn1]];
            tgh.Gradient[ano[esn1]] = sum;
          //  constrained_grad_check[esn1] = sum;
          }
        }
      }
    }
    if (do_hessian)
    {
      // first index is for the constraint number
      // second contains indices of entries that are to be made equal
      std::vector<std::vector<int>> constrain_hess;
      for (int ni = 0 ; ni < constrain_equal.size(); ni++)
      {
        if (constrain_equal[ni].size()-1)
        {
          sv_int constraints;
          for (int nj = 0; nj < constrain_equal[ni].size(); nj++)
            constraints.push_back(ano[constrain_equal[ni][nj]]);
          constrain_hess.push_back(constraints);
        }
      }
      for (int ni = 0; ni < constrain_hess.size(); ni++) //loop over each constraint
      {
        for (int nj = 0; nj < nmp; nj++)
        {
          double sum(0.);
          for (int nk = 0; nk < constrain_hess[ni].size(); nk++)
            sum += tgh.Hessian(nj,constrain_hess[ni][nk]);
          for (int nk = 0; nk < constrain_hess[ni].size(); nk++)
            tgh.Hessian(nj,constrain_hess[ni][nk]) = sum;
        }
        for (int nj = 0; nj < nmp; nj++)
        {
          double sum(0.);
          for (int nk = 0; nk < constrain_hess[ni].size(); nk++)
            sum += tgh.Hessian(constrain_hess[ni][nk],nj);
          for (int nk = 0; nk < constrain_hess[ni].size(); nk++)
            tgh.Hessian(constrain_hess[ni][nk],nj) = sum;
        }
      }
    }
  }

  af_string
  RefineANISO::initial_statistics()
  {
    return SIGMAN->logScaling("Initial");
  }

  af_string
  RefineANISO::current_statistics()
  {
    return SIGMAN->logScaling("Current");
  }

  af_string
  RefineANISO::final_statistics()
  {
    auto out1 = SIGMAN->logScaling("Final");
    auto out2 = SIGMAN->logBinning("Final",numinbin);
    for (auto item : out2)
      out1.push_back(item);
    return out1;
  }

  af_string
  RefineANISO::reject_outliers()
  {
    sv_double epsnSigmaN(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      epsnSigmaN[r] = fn::pow2(resn(r));
    }
    SELECTED->flag_outliers_and_select(REFLECTIONS,
        SELECTED->minimal_criteria(),
        epsnSigmaN
      );
    //no point in logging these, wilson not excluded
    //SELECTED->logOutliers(out::VERBOSE,"Anisotropy Refinement");
    //SELECTED->logSelected("Anisotropy Refinement");
    return af_string();
  }

  af_string
  RefineANISO::finalize_parameters()
  {
    sv_double epsnSigmaN(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      epsnSigmaN[r] = fn::pow2(resn(r));
    }
    SELECTED->flag_outliers_and_select(REFLECTIONS,
        { //rejected::RESO,
          rejected::MISSING,
          rejected::SYSABS,
          rejected::WILSONNAT, //exclude these!
          rejected::NOMEAN,
          // rejected::LOINFO,
        },
        epsnSigmaN
      );
    return af_string();
  }

  double
  RefineANISO::resn(int r) const
  {
    const millnx miller = REFLECTIONS->get_miller(r);
    const double ssqr =   REFLECTIONS->work_ssqr(miller);
    const double eps  =   REFLECTIONS->work_eps(miller);
    const int    s  =   REFLECTIONS->get_int(labin::BIN,r);
    double epsnSigmaN = eps*SIGMAN->term(miller,ssqr,s);
    PHASER_ASSERT(epsnSigmaN > 0);
    return std::sqrt(epsnSigmaN);
  }

  double
  RefineANISO::aniso(int r,bool RemoveIsoB) const
  {
    const millnx miller = REFLECTIONS->get_miller(r);
    return SIGMAN->AnisoTerm(miller,RemoveIsoB); // store the correction as I/ANISO with IsoB removed
  }

  void
  RefineANISO::setup_wll()
  {
    labin_wll.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      //below is necessary
      if (REFLECTIONS->get_present(friedel::NAT,r)) //not "selected", as not selected yet
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
        const bool   cent = REFLECTIONS->work_cent(miller);
        const double inat = REFLECTIONS->get_flt(labin::INAT,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        double pWilson = cent ?
          pintensity.pIobsCen(inat,fn::pow2(resn),sigi) :
          pintensity.pIobsAcen(inat,fn::pow2(resn),sigi);
        pWilson = std::max(std::numeric_limits<double>::min(),pWilson);
        pWilson = std::log(pWilson);
        labin_wll[r] = pWilson;
      }
    }
  }

  double RefineANISO::sharpening_bfactor() const
  { return SIGMAN->sharpening_bfactor(); }

  double RefineANISO::wilson_k_f() const
  { return SIGMAN->wilson_k_f(); }

  double RefineANISO::wilson_b_f() const
  { return SIGMAN->IsoB_F(); }

  double RefineANISO::delta_bfactor() const
  { return SIGMAN->AnisoDeltaB(); }

  dmat33 RefineANISO::direction_cosines() const
  { return SIGMAN->direction_cosines(); }

  dmat6  RefineANISO::beta() const
  { return SIGMAN->AnisoBetaRemoveIsoB(); }

} //phasertng
