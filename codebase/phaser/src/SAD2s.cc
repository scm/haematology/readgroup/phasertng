//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/main/Phaser.h>
#include <phaser/src/RefineSAD.h>
#include <phasertng/math/RiceWoolfson.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
}

namespace phaser {

// -- acentric singleton reflections

singleton
RefineSAD::singletonReflIntgSAD2(unsigned& r,bool positive)
{
  singleton rIntg;
  floatType FO = positive ? POS.F[r] : NEG.F[r];
  floatType FH = positive ? std::abs(FHpos[r]) : std::abs(FHneg[r]);
  floatType VarFO = fn::pow2(positive ? POS.SIGF[r] : NEG.SIGF[r]);
  floatType V(sigDsqr[r] + VarFO);
  floatType X(2*FO*FH/V);
  rIntg.setProb(X,positive ? std::arg(FHpos[r]) : std::arg(FHneg[r]));
  floatType Like = phasertng::pRiceF(FO,FH,V);
  assert(Like > 0.);
  rIntg.LogLike = -log(Like);
  return rIntg;
}

floatType
RefineSAD::singletonReflGradSAD2(unsigned& r,bool positive,bool atomic,bool sigmaa)
{
//#define __FD_GRAD_SING__
#ifdef __FD_GRAD_SING__
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL2") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL2\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
#if 0
  cmplxType epsilon(shift,0);
  cmplxType *test = &(FHpos[r]);
  floatType &dL_by_dtest = dL_by_dReFHpos;
#endif
  floatType fLogLike = singletonReflIntgSAD2(r,positive).LogLike;
  (*test) += epsilon;
  floatType DLogLikeForward = singletonReflIntgSAD2(r,positive).LogLike;
  floatType FD_test_forward = (DLogLikeForward-fLogLike)/shift;
  (*test) -= epsilon;
  (*test) -= epsilon;
  floatType DLogLikeBackward = singletonReflIntgSAD2(r,positive).LogLike;
  floatType FD_test_backward = (DLogLikeBackward-fLogLike)/shift;
  (*test) += epsilon;
#endif

  //initialization
  dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0;
  dL_by_dSDsqr = 0;
  dL_by_dSA = dL_by_dSB = dL_by_dSP = dL_by_dSDsqr = 0;

  floatType FO = positive ? POS.F[r] : NEG.F[r];
  floatType ReFH = positive ? std::real(FHpos[r]) : std::real(FHneg[r]);
  floatType ImFH = positive ? std::imag(FHpos[r]) : std::imag(FHneg[r]);
  floatType FH = positive ? std::abs(FHpos[r]) : std::abs(FHneg[r]);
  floatType VarFO = fn::pow2(positive ? POS.SIGF[r] : NEG.SIGF[r]);
  floatType V(sigDsqr[r] + VarFO);
  floatType X(2*FO*FH/V);
  floatType eBess0 = phasertng::tbl_ebesseli0.get(X);
  floatType eBess1 = phasertng::tbl_ebesseli1.get(X);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  static floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType expArg(-fn::pow2(FO-FH)/V);
  floatType expterm((expArg > minExpArgHalf) ? exp(expArg) : exp(minExpArgHalf));
  floatType Like = phasertng::pRiceF(FO,FH,V);
  assert(Like > 0.);

  if (atomic)
  {
    floatType eBess1term = FH ? eBess1/FH : 0.;
    floatType eBess0term = FO ? eBess0/FO : 0.;
    floatType dL_by_dFH = expterm*4*(eBess1term-eBess0term)*fn::pow2(FO/V);
    floatType dL_by_dReFH = -dL_by_dFH*ReFH;
    floatType dL_by_dImFH = -dL_by_dFH*ImFH;
    dL_by_dReFHpos = positive ? dL_by_dReFH : 0;
    dL_by_dImFHpos = positive ? dL_by_dImFH : 0;
    dL_by_dReFHneg = positive ? 0 : dL_by_dReFH;
    dL_by_dImFHneg = positive ? 0 : dL_by_dImFH;
  }

  if (sigmaa)
  {
    floatType dLbydSD = -(1/fn::pow3(V))*expterm*(2*FO*((FO*FO+FH*FH-V)*eBess0 -2*FO*FH*eBess1));
    dL_by_dSDsqr = dLbydSD;
  }

#ifdef __FD_GRAD_SING__
if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
   std::cout << "positive   " << positive << std::endl;
   std::cout << "FHpos[r]   " << FHpos[r] << std::endl;
   std::cout << "FO         " << FO << std::endl;
   std::cout << "sigDsqr[r] " << sigDsqr[r] << std::endl;
   std::cout << "VarFO     " << VarFO << std::endl;
   std::cout << "V         " << V << std::endl;
   std::cout << "Like      " << Like  << std::endl;
   std::cout << "dL_by_dReFHpos " << dL_by_dReFHpos << std::endl;
   std::cout << "dL_by_dImFHpos " << dL_by_dImFHpos << std::endl;
   std::cout << "dL_by_dSDsqr " << dL_by_dSDsqr << std::endl;
}
#endif

  floatType LogLike(-log(Like));
  dL_by_dReFHpos /= Like;
  dL_by_dImFHpos /= Like;
  dL_by_dReFHneg /= Like;
  dL_by_dImFHneg /= Like;
  dL_by_dSDsqr   *= epsn[r]/Like;

#ifdef __FD_GRAD_SING__
  std::cout << "=== First Derivative FD test (r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest << " Forward " << FD_test_forward << " Backward " << FD_test_backward << std::endl;
  std::cout << "Ratio forward " << dL_by_dtest/FD_test_forward <<
                   " backward " << dL_by_dtest/FD_test_backward << "\n";
 //if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
 if (r >= std::atof(getenv("PHASER_TEST_NREFL2"))) std::exit(1);
#endif
  return LogLike;
}

floatType
RefineSAD::singletonReflHessSAD2(unsigned& r,bool positive,bool atomic,bool sigmaa)
{
//#define __FD_HESS_SING__
#ifdef __FD_HESS_SING__
 if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_NREFL2") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL2\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_MATHEMATICA") == 0)
    { std::cout << "setenv PHASER_TEST_MATHEMATICA\n" ; std::exit(1); }
 floatType shift_a = std::atof(getenv("PHASER_TEST_SHIFT"));
 floatType shift_b = std::atof(getenv("PHASER_TEST_SHIFT"));
  //tests single first _a, _b and same or mixed second derivatives _a_b
  //epsilons and test type depend on types of derivatives
  //d2L_by_dReFHpos_dReFHneg etc
#if 0
  cmplxType epsilon_a(shift_a,0);
  cmplxType epsilon_b(shift_b,0);
  cmplxType *test_a = &(FHpos[r]);
  cmplxType *test_b = &(FHpos[r]);
  floatType &d2L_by_dtest2 = d2L_by_dReFHpos2;
#endif
#if 0
  cmplxType epsilon_a(0,shift_a);
  floatType epsilon_b(shift_b);
  cmplxType *test_a = &(FHneg[r]);
  floatType *test_b = &(sigDsqr[r]);
  floatType &d2L_by_dtest2 = d2L_by_dImFHneg_dSDsqr;
#endif

  floatType f_start = singletonReflIntgSAD2(r,positive).LogLike;
  (*test_a) += epsilon_a;
  floatType f_forward_a = singletonReflIntgSAD2(r,positive).LogLike;
  (*test_a) -= epsilon_a;
  (*test_b) += epsilon_b;
  floatType f_forward_b = singletonReflIntgSAD2(r,positive).LogLike;
  (*test_b) -= epsilon_b;
  (*test_a) += epsilon_a;
  (*test_b) += epsilon_b;
  floatType f_both_a_b = singletonReflIntgSAD2(r,positive).LogLike;
  (*test_a) -= epsilon_a;
  (*test_b) -= epsilon_b;
  floatType FD_test_on_f = (f_both_a_b - f_forward_a - f_forward_b + f_start)/shift_a/shift_b;
#endif

  //initialization
  dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0;
  d2L_by_dReFHpos2 = d2L_by_dReFHpos_dImFHpos = d2L_by_dReFHpos_dReFHneg = d2L_by_dReFHpos_dImFHneg = 0;
  d2L_by_dImFHpos2 = d2L_by_dImFHpos_dReFHneg = d2L_by_dImFHpos_dImFHneg = 0;
  d2L_by_dReFHneg2 = d2L_by_dReFHneg_dImFHneg = 0;
  d2L_by_dImFHneg2 = 0;
  dL_by_dSDsqr = dL_by_dSA = dL_by_dSB = dL_by_dSP = 0;
  d2L_by_dSDsqr2 = d2L_by_dSA2 = d2L_by_dSB2 = d2L_by_dSP2 = 0;

  floatType FO = positive ? POS.F[r] : NEG.F[r];
  floatType ReFH = positive ? std::real(FHpos[r]) : std::real(FHneg[r]);
  floatType ImFH = positive ? std::imag(FHpos[r]) : std::imag(FHneg[r]);
  floatType FH = positive ? std::abs(FHpos[r]) : std::abs(FHneg[r]);
  floatType VarFO = fn::pow2(positive ? POS.SIGF[r] : NEG.SIGF[r]);
  floatType V(sigDsqr[r] + VarFO);
  floatType X(2*FO*FH/V);
  floatType eBess0 = phasertng::tbl_ebesseli0.get(X);
  floatType eBess1 = phasertng::tbl_ebesseli1.get(X);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  static floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType expArg(-fn::pow2(FO-FH)/V);
  floatType expterm((expArg > minExpArgHalf) ? exp(expArg) : exp(minExpArgHalf));
  floatType Like = phasertng::pRiceF(FO,FH,V);
  assert(Like > 0.);

  if (atomic)
  {
    floatType eBess1term = FH ? eBess1/FH : 0.;
    floatType eBess0term = FO ? eBess0/FO : 0.;
    floatType dL_by_dFH = expterm*4*(eBess1term-eBess0term)*fn::pow2(FO/V);
    floatType dL_by_dReFH = -dL_by_dFH*ReFH;
    floatType dL_by_dImFH = -dL_by_dFH*ImFH;
    dL_by_dReFHpos = positive ? dL_by_dReFH : 0;
    dL_by_dImFHpos = positive ? dL_by_dImFH : 0;
    dL_by_dReFHneg = positive ? 0 : dL_by_dReFH;
    dL_by_dImFHneg = positive ? 0 : dL_by_dImFH;
    floatType d2L_by_dReFH2(0.),d2L_by_dImFH2(0.),d2L_by_dReFH_dImFH(0.);
    floatType FHVcbd = fn::pow3(FH*V);
    if (FHVcbd > 0)
    {
      d2L_by_dReFH2 = (4*expterm*FO*(FH*(2*fn::pow2(FO)*fn::pow2(ReFH) + fn::pow2(FH)*(2*fn::pow2(ReFH) - V))*eBess0 -
         FO*(fn::pow2(ImFH)*(4*fn::pow2(ReFH) - V) + fn::pow2(ReFH)*(4*fn::pow2(ReFH) + V))*eBess1)) / FHVcbd;
      d2L_by_dImFH2 = (4*expterm*FO*(FH*(2*fn::pow2(FO)*fn::pow2(ImFH) + fn::pow2(FH)*(2*fn::pow2(ImFH) - V))*eBess0 -
         FO*(4*fn::pow4(ImFH)-fn::pow2(ReFH)*V + fn::pow2(ImFH)*(4*fn::pow2(ReFH) + V))*eBess1)) / FHVcbd;
      d2L_by_dReFH_dImFH = (8*expterm*FO*ImFH*ReFH*(FH*(fn::pow2(FO) + fn::pow2(ImFH) + fn::pow2(ReFH))*
         eBess0 - FO*(2*fn::pow2(ImFH) + 2*fn::pow2(ReFH) + V)*eBess1)) / FHVcbd;
    }
    d2L_by_dReFHpos2 = positive ? d2L_by_dReFH2 : 0;
    d2L_by_dImFHpos2 =  positive ? d2L_by_dImFH2 : 0;
    d2L_by_dReFHpos_dImFHpos =  positive ? d2L_by_dReFH_dImFH : 0;
    d2L_by_dReFHneg2 = !positive ? d2L_by_dReFH2 : 0;
    d2L_by_dImFHneg2 =  !positive ? d2L_by_dImFH2 : 0;
    d2L_by_dReFHneg_dImFHneg =  !positive ? d2L_by_dReFH_dImFH : 0;
  }

  if (sigmaa)
  {
    dL_by_dSA = dL_by_dSB = dL_by_dSP = 0;
    floatType dLbydSDsqr = -(1/fn::pow3(V))*expterm*(2*FO*((FO*FO+FH*FH-V)*eBess0 -2*FO*FH*eBess1));
    floatType d2LbydSDsqr2 = (2*expterm*FO*(-2*eBess1*FH*FO*(2*fn::pow2(FO) + 2*fn::pow2(ImFH) +
          2*fn::pow2(ReFH) - 3*V) +
       eBess0*(fn::pow4(FO) + fn::pow4(ImFH) + fn::pow4(ReFH) +
          fn::pow2(FO)*(6*fn::pow2(ImFH) + 6*fn::pow2(ReFH) - 4*V) +
          2*fn::pow2(ImFH)*(fn::pow2(ReFH) - 2*V) - 4*fn::pow2(ReFH)*V + 2*fn::pow2(V))))/std::pow(V,5);
    dL_by_dSDsqr = dLbydSDsqr;
    d2L_by_dSDsqr2 = d2LbydSDsqr2;
  }

#ifdef __FD_HESS_SING__
if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
  std::cout << "=== Second Derivative FD test (r=" << r << ") ===\n";
   std::cout << "positive   " << positive << std::endl;
   std::cout << "FHpos[r]   " << FHpos[r] << std::endl;
   std::cout << "FO         " << FO << std::endl;
   std::cout << "sigDsqr[r] " << sigDsqr[r] << std::endl;
   std::cout << "VarFO      " << VarFO << std::endl;
   std::cout << "V          " << V << std::endl;
   std::cout << "Like       " << Like  << std::endl;
   std::cout << "dL_by_dReFHpos " << dL_by_dReFHpos << std::endl;
   std::cout << "dL_by_dImFHpos " << dL_by_dImFHpos << std::endl;
   std::cout << "dL_by_dSDsqr   " << dL_by_dSDsqr << std::endl;
   std::cout << "d2L_by_dReFHpos2 " << d2L_by_dReFHpos2 << std::endl;
   std::cout << "d2L_by_dImFHpos2 " << d2L_by_dImFHpos2 << std::endl;
   std::cout << "d2L_by_dReFHpos_dImFHpos " << d2L_by_dReFHpos_dImFHpos << std::endl;
   std::cout << "d2L_by_dSDsqr2 " << d2L_by_dSDsqr2 << std::endl;
}
#endif
  floatType LogLike(-log(Like));
  floatType LikeFactor(1/Like);
  if (positive)
  {
    d2L_by_dReFHpos2         = fn::pow2(dL_by_dReFHpos*LikeFactor) - d2L_by_dReFHpos2*LikeFactor;
    d2L_by_dReFHpos_dImFHpos = (dL_by_dReFHpos*LikeFactor)*(dL_by_dImFHpos*LikeFactor) - d2L_by_dReFHpos_dImFHpos*LikeFactor;
    d2L_by_dImFHpos2         = fn::pow2(dL_by_dImFHpos*LikeFactor) - d2L_by_dImFHpos2*LikeFactor;
    dL_by_dReFHpos *= LikeFactor;
    dL_by_dImFHpos *= LikeFactor;
  }
  else
  {
    d2L_by_dReFHneg2         = fn::pow2(dL_by_dReFHneg*LikeFactor) - d2L_by_dReFHneg2*LikeFactor;
    d2L_by_dReFHneg_dImFHneg = (dL_by_dReFHneg*LikeFactor)*(dL_by_dImFHneg*LikeFactor) - d2L_by_dReFHneg_dImFHneg*LikeFactor;
    d2L_by_dImFHneg2         = fn::pow2(dL_by_dImFHneg*LikeFactor) - d2L_by_dImFHneg2*LikeFactor;
    dL_by_dReFHneg *= LikeFactor;
    dL_by_dImFHneg *= LikeFactor;
  }
  dL_by_dSDsqr   *= epsn[r]*LikeFactor;
  d2L_by_dSDsqr2 *= fn::pow2(epsn[r])*LikeFactor;
  d2L_by_dSDsqr2 = fn::pow2(dL_by_dSDsqr) - d2L_by_dSDsqr2;

#ifdef __FD_HESS_SING__
  std::cout << "=== Second Derivative FD test (r=" << r << ") ===\n";
  std::cout << "Analytic " << d2L_by_dtest2 << " on Function " << FD_test_on_f << std::endl;
  std::cout << "Ratio " << d2L_by_dtest2/FD_test_on_f << "\n";
 //if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
 if (r >= std::atof(getenv("PHASER_TEST_NREFL2"))) std::exit(1);
#endif
  return LogLike;
}

}//phaser
