//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_scatterer_class__
#define __phasertng_pod_scatterer_class__
#include <scitbx/vec3.h>
#include <scitbx/sym_mat3.h>
#include <cctbx/xray/scatterer.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::sym_mat3<double> dmat6;

namespace phasertng {
namespace pod {

namespace xray {

//allow the cctbx::xray::scatterer<double> type to be called as pod::xray::scatterer
//for consistency with the other types defined here
typedef cctbx::xray::scatterer<double> scatterer;

}

namespace xtra { class scatterer
{
  public:
    //CHANGES HERE WILL NEED TO BE ADDED TO UNPARSE IN Substructure
    //we use int not bool so that scanf %d can be used to read/write
    int fix_site = 0;
    int fix_occ = 0;
    int fix_bfac = 0;
    int fix_type = 0;
    int rejected = 0;
    int restored = 0;
    int Ax = 0;
    int Tx = 0;
    int  n_xyz = 0;
    int  n_adp = 0;
    int  multiplicity = 1; //important default
    double zscore = 0;
    double occupancy = 0; //store occupancy from xray::scatterer when it's rejected and occupancy->0
    bool bswap;
    bool ustar;
};}

namespace gradient { namespace mean { class scatterer
{
  public:
    dvect3 dReF_by_dX, dImF_by_dX;
    double dReF_by_dO, dImF_by_dO;
    double dReF_by_dIB, dImF_by_dIB;
    dmat6  dReF_by_dAB,dImF_by_dAB;

  public:
    scatterer() { init(); }
    void init()
    {
      for (int x = 0; x < 3; x++)
      {
        dReF_by_dX[x] = dImF_by_dX[x] = 0;
      }
      dReF_by_dO = dImF_by_dO = 0;
      dReF_by_dIB = dImF_by_dIB = 0;
      for (int n = 0; n < 6; n++)
      {
        dReF_by_dAB[n] = dImF_by_dAB[n] = 0;
      }
    }
};}}

namespace gradient { namespace anom { class scatterer
{
  public:
    dvect3 dReFHpos_by_dX, dImFHpos_by_dX, dReFHneg_by_dX, dImFHneg_by_dX;
    double dReFHpos_by_dO, dImFHpos_by_dO, dReFHneg_by_dO, dImFHneg_by_dO;
    double dReFHpos_by_dIB, dImFHpos_by_dIB, dReFHneg_by_dIB, dImFHneg_by_dIB;
    dmat6  dReFHpos_by_dAB,dImFHpos_by_dAB,dReFHneg_by_dAB,dImFHneg_by_dAB;
   // double dReFHpos_by_dFdp,dImFHpos_by_dFdp,dReFHneg_by_dFdp,dImFHneg_by_dFdp;

  public:
    scatterer() { init(); }
    void init()
    {
      for (int x = 0; x < 3; x++)
      {
        dReFHpos_by_dX[x] = dImFHpos_by_dX[x] = 0;
        dReFHneg_by_dX[x] = dImFHneg_by_dX[x] = 0;
      }
      dReFHpos_by_dO = dImFHpos_by_dO = dReFHneg_by_dO = dImFHneg_by_dO = 0;
      dReFHpos_by_dIB = dImFHpos_by_dIB = dReFHneg_by_dIB = dImFHneg_by_dIB = 0;
     // dReFHpos_by_dFdp = dImFHpos_by_dFdp = dReFHneg_by_dFdp = dImFHneg_by_dFdp = 0;
      for (int n = 0; n < 6; n++)
      {
        dReFHpos_by_dAB[n] = dImFHpos_by_dAB[n] = 0;
        dReFHneg_by_dAB[n] = dImFHneg_by_dAB[n] = 0;
      }
    }
};}}

namespace hessian { namespace mean { class scatterer
{
  public:
    dvect3 d2ReF_by_dX2, d2ImF_by_dX2;
    double d2ReF_by_dO2, d2ImF_by_dO2;
    double d2ReF_by_dIB2, d2ImF_by_dIB2;
    dmat6  d2ReF_by_dAB2, d2ImF_by_dAB2;

  public:
    scatterer() { init(); }
    void init()
    {
      for (int x = 0; x < 3; x++)
      {
        d2ReF_by_dX2[x] = d2ImF_by_dX2[x] = 0;
      }
      d2ReF_by_dO2 = d2ImF_by_dO2 = 0;
      d2ReF_by_dIB2 = d2ImF_by_dIB2 = 0;
      for (int n = 0; n < 6; n++)
      {
        d2ReF_by_dAB2[n] = d2ImF_by_dAB2[n] = 0;
      }
    }
};}}

namespace hessian { namespace anom { class scatterer
{
  public:
    dvect3 d2ReFHpos_by_dX2, d2ImFHpos_by_dX2, d2ReFHneg_by_dX2, d2ImFHneg_by_dX2;
    double d2ReFHpos_by_dO2, d2ImFHpos_by_dO2, d2ReFHneg_by_dO2, d2ImFHneg_by_dO2;
    double d2ReFHpos_by_dIB2, d2ImFHpos_by_dIB2, d2ReFHneg_by_dIB2, d2ImFHneg_by_dIB2;
    dmat6  d2ReFHpos_by_dAB2, d2ImFHpos_by_dAB2, d2ReFHneg_by_dAB2, d2ImFHneg_by_dAB2;

  public:
    scatterer() { init(); }
    void init()
    {
      for (int x = 0; x < 3; x++)
      {
        d2ReFHpos_by_dX2[x] = d2ImFHpos_by_dX2[x] = 0;
        d2ReFHneg_by_dX2[x] = d2ImFHneg_by_dX2[x] = 0;
      }
      d2ReFHpos_by_dO2 = d2ImFHpos_by_dO2 = d2ReFHneg_by_dO2 = d2ImFHneg_by_dO2 = 0;
      d2ReFHpos_by_dIB2 = d2ImFHpos_by_dIB2 = d2ReFHneg_by_dIB2 = d2ImFHneg_by_dIB2 = 0;
      for (int n = 0; n < 6; n++)
      {
        d2ReFHpos_by_dAB2[n] = d2ImFHpos_by_dAB2[n] = 0;
        d2ReFHneg_by_dAB2[n] = d2ImFHneg_by_dAB2[n] = 0;
      }
    }
};}}

}} //phasertng
#endif
