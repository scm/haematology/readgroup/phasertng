//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/minimizer/RefineRB.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>
#include <algorithm>

namespace phasertng {

  void Phasertng::runFUSE()
  {
    //set the defaults
    input.value__boolean.at(".phasertng.fuse_translation_function.complete.").set_value(false);
    input.value__boolean.at(".phasertng.fuse_translation_function.fused.").set_value(false);
    double io_fuse_tfz = input.value__flt_number.at(".phasertng.fuse_translation_function.tfz.").value_or_default();
    int io_packing_percent = input.value__percent.at(".phasertng.fuse_translation_function.packing_percent.").value_or_default();
    int io_max_remain = input.value__int_number.at(".phasertng.fuse_translation_function.maximum_remaining.").value_or_default();

    if (DAGDB.size() == 0)
    {
      logTab(out::SUMMARY,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    //this avoids having to make up a curl just to do the simple pose case
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with correct mode (pose failed?)");
    if (DAGDB.NODES.number_of_poses() <= 1)
    {
      logTab(out::SUMMARY,"No amalgamation");
      logTab(out::SUMMARY,"First translation function solutions may not be on same origin");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (DAGDB.NODES.size() == 1)
    {
      logTab(out::SUMMARY,"No amalgamation");
      logTab(out::SUMMARY,"Single node");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    if (nmol > 1)
    {
      logTab(out::SUMMARY,"tNCS symmetry present and not in model");
      logTab(out::SUMMARY,"No amalgamation");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    auto sg = DAGDB.NODES[0].SG->CCP4;
    for (int i = 1; i < DAGDB.NODES.size(); i++)
    if (DAGDB.NODES[i].SG->CCP4 != sg)
    {
      logTab(out::SUMMARY,"Space group not unique in solutions");
      logTab(out::SUMMARY,"No amalgamation");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    int fusable(0);
    for (int i = 0; i < DAGDB.size(); i++)
    {
      if (DAGDB.NODES[i].ZSCORE > io_fuse_tfz)
      {
        logTab(out::VERBOSE,"Fusable TFZ node #" + itos(i+1) + " " + dtos(DAGDB.NODES[i].ZSCORE,0));
        fusable++;
      }
    }
    if (fusable == 0)
    {
      logTab(out::SUMMARY,"No amalgamation");
      logTab(out::SUMMARY,"No nodes with high TFZ");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (fusable == 1)
    {
      logTab(out::SUMMARY,"No amalgamation");
      logTab(out::SUMMARY,"Single node with high TFZ");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::TRACE_PDB,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    dag::NodeList stored_input_dag = DAGDB.NODES;



    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(where,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    RefineRB refine( //this is used only for the init_node function
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon);
    refine.BFAC_MIN = REFLECTIONS->bfactor_minimum();
    if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
      refine.BFAC_MIN = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    refine.BFAC_MAX = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();

    dag::Node& node0 = DAGDB.NODES[0];
    auto fuseid = node0.POSE.back().ENTRY->identify();

    //if this comes from an executable then the number to seek for can be set explicitly here
    //otherwise we can use the seek
    //SEEK is also set here if not set already
    sv_string constituents = input.value__strings.at(".phasertng.fuse_translation_function.models.tags.").value_or_default();
    if (constituents.size())
    {
      logTab(out::VERBOSE,"Seek models determined from input");
      //set the seek here for executables
      // if the fuse goes up to the max then future calls to FRF etc drop out
      node0.SEEK.clear();
      for (int i = 0; i < constituents.size(); i++)
      {
        dag::Seek seek;
        seek.IDENTIFIER.initialize_tag(constituents[i]);
        auto entry = DAGDB.lookup_entry_tag(seek.IDENTIFIER,DataBase());
        seek.IDENTIFIER = entry->identify(); //with the correct uuid not just tag
        node0.SEEK.push_back(seek);
      }
      //need to set the seek star flag properly with found/search/missing
      //uses he number of poses present
      node0.set_star_from_pose();
      for (int i = 0; i < DAGDB.NODES.node_list.size(); i++)
      {
        DAGDB.NODES.node_list[i].SEEK = node0.SEEK;
      }
    }
    else if (!node0.SEEK.size())
    {
      throw Error(err::INPUT,"Seek components not set");
    }
    else
    {
      logTab(out::VERBOSE,"Seek models determined from dag");
    }
    DAGDB.reset_entry_pointers(DataBase());

    int num_remaining(0);
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Seek Information");
    logTabArray(where,DAGDB.seek_info_array());
    int npresent(0);
    for (int p = 0; p < node0.POSE.size()-1; p++) //exclude the last one, since this is to be fused
    {
      auto tag = node0.POSE[p].ENTRY->identify().tag();
      if (tag == fuseid.tag())
        npresent++;
    }
    //can't just use tags number directly because there may be multiple entries for each tag
    //the user may not have grouped the constituents nicely to make each number complete count
    int nsearch(0);
    num_remaining++; //because last of the seek has is_found=True but will be counted as remaining
    for (int s = 0; s < node0.SEEK.size(); s++)
    {
      auto tag = node0.SEEK[s].IDENTIFIER.tag();
      if (tag == fuseid.tag())
      {
        nsearch++;
        if (node0.SEEK[s].is_missing())
          num_remaining++;
      }
    }
    logTab(where,"Number of components present in background: " + itos(npresent));
    logTab(where,"Number of components in search: " + itos(nsearch));
    logTab(where,"Number of components remaining (including current): " + itos(num_remaining));
    PHASER_ASSERT(num_remaining == (nsearch-npresent));
    if (num_remaining > io_max_remain)
    logTab(where,"Combinatorial limit, lower number remaining");
    num_remaining = std::min(num_remaining,io_max_remain);
    logTab(where,"Number fusable = " + itos(num_remaining));
    }}

    if (DAGDB.NODES.front().SEEK.size() and DAGDB.NODES.front().SEEK.back().is_found())
    {
      logTab(out::SUMMARY,"No amalgamation");
      logTab(out::SUMMARY,"Constituents complete");
      input.value__boolean.at(".phasertng.fuse_translation_function.complete.").set_value(true);
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    //parents is the set of parent backgrounds for amalgamation
    //these are derived from the penultimate PARENT identifier - shared previous poses
    //in the best case this will be only 1 background, and all the fused will be wrt this
    int nbackground(0); //for ignoring in clashing
    std::unordered_set<uuid_t> parents;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Amalgamation");
    logTab(where,"Last placed = " + fuseid.str());
    nbackground = DAGDB.NODES.number_of_poses()-1;
    logTab(where,"Background Poses: " + itos(nbackground));
    //this looks back to the parent identifier for the search before this tfz
    //solutions that share this parent have the same background and can be fused
    //could also check all poses explicitly but would need to allow for refinement differences
    std::unordered_map<uuid_t,int> parent_count;
    logTab(where,"TFZ fuse cutoff = " + dtos(io_fuse_tfz,2));
    for (auto& item : DAGDB.NODES)
    {
      auto penultimate = item.PARENTS.front(); //of 2
      if (item.ZSCORE > io_fuse_tfz)
      {
        if (parent_count.count(penultimate))
          parent_count[penultimate]++ ;
        else //can't use ternary operation, different types
          parent_count[penultimate] = 1;
      }
    }
    for (auto item : parent_count)
      logTab(where,"Parents " + std::to_string(item.first) + " #" + itos(item.second));
    //remove the ones that have only one entry, since can't amalgamate
    for (auto iter = parent_count.cbegin() ; iter != parent_count.cend() ; )
    {
      //iter is std::map<Identifier,int>::const_iterator
      iter = (iter->second == 1) ? parent_count.erase(iter) : std::next(iter);
    }
    if (!parent_count.size())
    {
      logBlank(where);
      logTab(where,"No nodes with TFZ over limit for amalgamation");
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        DAGDB.WORK->ANNOTATION += " FUSE(NONE)";
      }
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    for (auto item : parent_count)
      parents.insert(item.first);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Sort database on llg");
    DAGDB.NODES.apply_sort_llg();
    logEllipsisShut(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Merge shared parent nodes with high TFZ");
    dag::NodeList output_dag;
    output_dag.resize(parents.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      uuid_t penultimate = *++DAGDB.WORK->PARENTS.rbegin();
      if (DAGDB.WORK->ZSCORE > io_fuse_tfz)
      { //only merge the nodes with high tfz
        if (parents.count(penultimate)) //#=1 removed from parents set
        {
          int i = std::distance(parents.begin(),std::find(parents.begin(),parents.end(),penultimate));
          PHASER_ASSERT(i < output_dag.size());
          if (output_dag[i].POSE.size() == nbackground+num_remaining)
          {
            continue; //no more if we have reached the limit for checking
          }
          else if (output_dag[i].POSE.size() == 0) // initialize this node
          {
            output_dag[i] = *DAGDB.WORK; //copy everything in the node, eg spacegroup, as well as the pose
          //  output_dag[i]->POSE.clear(); //but clear the background poses
          }
          else //append
          {
            output_dag[i].POSE.push_back(DAGDB.WORK->POSE.back()); //last placed, no tNCS allowed
          }
          output_dag[i].set_star_from_pose();
          output_dag[i].POSE.back().generic_float = DAGDB.WORK->ZSCORE;
          output_dag[i].generic_str = Identifier(penultimate,"tag").string();
        }
      }
    }
    DAGDB.NODES = output_dag;
    DAGDB.reset_entry_pointers(DataBase());
    PHASER_ASSERT(DAGDB.size());
    logEllipsisShut(where);
    logTab(where,"Number of nodes = " + itos(DAGDB.size()));
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      PHASER_ASSERT(DAGDB.NODES[i].POSE.size() > 1);
      logTab(where,itos(i+1,2) + ": Number of poses = " + itos(DAGDB.NODES[i].POSE.size()));
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    DAGDB.restart();
    int n(1),N(DAGDB.NODES.size());
    logUnderLine(where,"Pairwise Packing");
    while (!DAGDB.at_end())
    {
      std::string nN = nNtos(n++,N);
      Packing packing(0.5);
      packing.init_node(DAGDB.WORK); //must be linked with entries
      packing.multiplicity_and_exact_site();
      bool last_only(false); //assume that this has already been through a packing test
      bool no_cell_translation(REFLECTIONS->MAP);
      packing.calculate_packing(last_only,no_cell_translation);
      DAGDB.WORK->clash_matrix = packing.PACKING_DATA.matrix_as_matrix();
      logChevron(where,"Packing" + nN);
      logTab(where,"Parents: " + DAGDB.WORK->generic_str);
      logTabArray(where,DAGDB.WORK->logPairwiseClash(nbackground));
      logBlank(where);
    }
    }}

    //Knuth's combinatorial counting algorithm, avoids overflow
    auto choose_k_from_n = [](unsigned n, unsigned k) -> unsigned //Cool!
    { if (k > n) { return 0; }
      unsigned r = 1;
      for (unsigned d = 1; d <= k; ++d) { r *= n--; r /= d; }
      return r;
    };

    dag::NodeList output_dag;
    bool one_fused(false);
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Merging");
    logTab(where,"Solutions will be combined to find the largest subset that pack");
    int n(1),N(DAGDB.NODES.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      int ntfz = node->POSE.size()-nbackground;
      logTab(where,"Number solutions with high TFZ = " + itos(ntfz));
      logTab(where,"Number ensembles remaining to be found = " + itos(num_remaining));
      out::stream where = out::LOGFILE;
      std::string nN = nNtos(n++,N);
      logChevron(where,"Merging" + nN);
      int max_merge = std::min(num_remaining,ntfz);
      logTab(where,"Maximum for merge = " + itos(max_merge));
      int total_combinations(0); //include sum up to r
      {{ //local n
      int n(max_merge);
      for (int k = 2; k <= n; k++)
        total_combinations += choose_k_from_n(n,k);
      logTab(where,"Maximum number of combinations = " + itos(total_combinations));
      }}
      std::vector<dag::Pose> full_pose = node->POSE;
      node->POSE.erase(node->POSE.begin()+nbackground,node->POSE.end());
      std::vector<dag::Pose> background_pose = node->POSE;
      DAGDB.reset_work_entry_pointers(DataBase());
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      logTab(where,"Space Group: " + node->SG->CCP4);
      logTab(where,"Packing Cutoff: " + dtos(io_packing_percent,2) + "%");
      double background_llg;
      pod::FastPoseType ecalcs_sigmaa_pose;
      refine.init_node(DAGDB.WORK); //modify in place work node
      std::tie(background_llg) = DAGDB.work_node_likelihood(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS,
          &ecalcs_sigmaa_pose);
      //select r from max down, finding first that packs, increases
      for (int select = max_merge; select > 1; select--)
      {
        logBlank(where);
        //ntfz as the maximum to do the combination with is technically correct
        // but leads to memory exhaustion with all high tfz
        //by restricting the combinations to the top only, we avoid cases where there are lots of high tfz due to eg tncs
        std::vector<sv_int> combinations_for_selection(1); //index on combination
        int n(max_merge), r(select);
        std::vector<bool> v(n);
        std::fill(v.begin(), v.begin() + r, true);
        do {
          for (int i = 0; i < n; ++i) if (v[i]) combinations_for_selection.back().push_back(i);
          combinations_for_selection.push_back(sv_int(0));
        } while (std::prev_permutation(v.begin(), v.end())); //yes, permutation->combination
        combinations_for_selection.pop_back(); //remove last empty array
        for (int c = 0; c < combinations_for_selection.size(); c++) //sv_int
          for (int i = 0; i < combinations_for_selection[c].size(); i++) //sv_int
            combinations_for_selection[c][i] += nbackground+1;
        //for (auto& item : combinations_for_selection)
        //  std::cout << ivtos(item) << std::endl; //so cool!
        //combinations_for_selection contains all the options for selecting r from n
        //generate the background pose
        //array for flagging combos that are ok, if all fail, try smaller selection
        bool one_success(false);
        out::stream progress = out::LOGFILE;
        logProgressBarStart(progress,"Combinations: Select " + itos(select) + " from " + itos(max_merge), combinations_for_selection.size());
        for (int c = 0; c < combinations_for_selection.size(); c++) //sv_int
        {
          std::string nN = nNtos(c+1,combinations_for_selection.size());
          bool combination_success(true);
          sv_double combination_llg;
          sv_double combination_tfz;;
          out::stream where = out::VERBOSE;
          auto& combination = combinations_for_selection[c];
          logTab(where,"Combination ["+ivtos(combination)+"] background="+itos(nbackground) + nN);
          //below set of three are an inelegant way of testing the quadrants 2 3 and 4 of the matrix  1 2 | 3 4
          for (int i = 0; i < nbackground and combination_success; i++)
          { //pairwise with background
            int ii = i;
            for (int j = 0; j < combination.size() and combination_success; j++)  //not symmetrical so start from 0
            {
              int jj = combination[j]-1;
              PHASER_ASSERT(ii < node->clash_matrix.num_rows());
              PHASER_ASSERT(jj < node->clash_matrix.num_cols());
              double nclash = node->clash_matrix(ii,jj); //index from 0
              double ntrace = node->POSE[jj].ENTRY->TRACE.size();
              double perc = 100.*nclash/ntrace;
              if (nclash)
              logTab(where,"Packing " + itos(ii+1) + ":" + itos(jj+1) + " " + dtos(nclash,0) + "/" + dtos(ntrace,0) + "=" + dtos(perc,2) + "%");
              if (perc > io_packing_percent)
                  //matrix is not symmetric, so do both pairs
              {
                //have to regenerate the clacking percent from the trace length
                logTab(where,"Failure in packing");
                combination_success = false;
              }
            }
          }
          for (int i = 0; i < combination.size() and combination_success; i++)
          { //pairwise with background because not symmetric
            int ii = combination[i]-1;
            for (int j = 0; j < nbackground and combination_success; j++)  //not symmetrical so start from 0
            {
              int jj = j;
              PHASER_ASSERT(ii < node->clash_matrix.num_rows());
              PHASER_ASSERT(jj < node->clash_matrix.num_cols());
              double nclash = node->clash_matrix(ii,jj); //index from 0
              double ntrace = node->POSE[jj].ENTRY->TRACE.size();
              double perc = 100.*nclash/ntrace;
              if (nclash)
              logTab(where,"Packing " + itos(ii+1) + ":" + itos(jj+1) + " " + dtos(nclash,0) + "/" + dtos(ntrace,0) + "=" + dtos(perc,2) +"%");
              if (perc > io_packing_percent)
                  //matrix is not symmetric, so do both pairs
              {
                //have to regenerate the clacking percent from the trace length
                logTab(where,"Failure in packing");
                combination_success = false;
              }
            }
          }
          for (int i = 0; i < combination.size() and combination_success; i++)
          { //pairwise with themselves
            int ii = combination[i]-1;
            for (int j = 0; j < combination.size() and combination_success; j++)  //not symmetrical so start from 0
            {
              int jj = combination[j]-1;
              PHASER_ASSERT(ii < node->clash_matrix.num_rows());
              PHASER_ASSERT(jj < node->clash_matrix.num_cols());
              double nclash = node->clash_matrix(ii,jj); //index from 0
              double ntrace = node->POSE[jj].ENTRY->TRACE.size();
              double perc = 100.*nclash/ntrace;
              if (nclash)
              logTab(where,"Packing " + itos(ii+1) + ":" + itos(jj+1) + " " + dtos(nclash,0) + "/" + dtos(ntrace,0) + "=" + dtos(perc,2) +"%");
              if (perc > io_packing_percent)
                  //matrix is not symmetric, so do both pairs
              {
                //have to regenerate the clacking percent from the trace length
                logTab(where,"Failure in packing");
                combination_success = false;
              }
            }
          }
          if (!combination_success)
          {
            logTab(where,"Combination ["+ivtos(combination)+"] failed pairwise packing"+nN);
            logBlank(where);
            if (!logProgressBarNext(progress)) break;
            continue;
          }

          node->POSE = background_pose;
          //double last_llg(0);
          //the starting llg is NOT the background llg, it is the addition of the first component, which may go down from background
          double last_llg = std::numeric_limits<double>::lowest(); //so that first is always accepted
          //combination_llg.push_back(background_llg); //confusing if this is added
          int counter(1);
          for (int combo : combination) //sv_int
          {
            std::string cC = nNtos(counter++,combination.size());
            combo -= 1; //index from 0
            node->POSE.push_back(full_pose[combo]);
            //increment tNCS group otherwise they look like tNCS related set
/*
            std::set<int> tncs_groups;
            for (auto& pose : node->POSE)
              tncs_groups.insert(pose.TNCS_GROUP);
            int next_tncs_group_number(1);
            while (tncs_groups.size() and tncs_groups.count(next_tncs_group_number))
              next_tncs_group_number++;
            node->POSE.back().TNCS_GROUP = next_tncs_group_number;
            //above leaves the TNCS_GROUP in a strange order, depending on combination
            //instead, renumber from the start to keep it clean, no tncs
*/
            for (int p = 0; p < node->POSE.size(); p++)
              node->POSE[p].TNCS_GROUP.first = p+1; //no tncs
            combination_tfz.push_back(full_pose[combo].generic_float);
            DAGDB.reset_work_entry_pointers(DataBase());
            double next_llg(0);
            pod::FastPoseType ecalcs_sigmaa_pose;
            refine.init_node(DAGDB.WORK); //modify in place work node
            std::tie(next_llg) = DAGDB.work_node_likelihood(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS,
                &ecalcs_sigmaa_pose);
            combination_llg.push_back(next_llg);
            if (next_llg < last_llg)
            {
              logTab(where,"Failure in increasing llg" + cC + " " + dvtos(combination_llg,2));
              logTab(where,"Combination ["+ivtos(combination)+"] failed llg increase");
              combination_success = false;
              break; //out of the inner loop
            }
            if (counter == 1)
              logTab(where,"Starting llg" +cC + " " + dvtos(combination_llg,2));
            else
              logTab(where,"Current increasing llg" +cC + " " + dvtos(combination_llg,2));
            last_llg = next_llg;
          }
          if (combination_success)
          {
            logTab(where,"Success in increasing llg "+dvtos(combination_llg,2));
            Packing packing(0.5);
            dag::Node next = *node; //because we are going to change the clash_matrix size
            packing.init_node(&next); //must be linked with entries
            packing.multiplicity_and_exact_site();
            //AJM TODO this can be sped up below by not packing the background
            //AJM TODO generalize last-only to nlast
            bool last_only(false); //assume that this has already been through a packing test
            bool no_cell_translation(REFLECTIONS->MAP);
            packing.calculate_packing(last_only,no_cell_translation);
            next.clash_matrix = packing.PACKING_DATA.matrix_as_matrix();
            next.CLASH_WORST = packing.PACKING_DATA.worst_percent();
            logTabArray(where,next.logClash());
            logTab(where,"Full packing matrix worst=" + dtos(next.CLASH_WORST,2) + "%");
            if (next.CLASH_WORST <= io_packing_percent)
            {
              logTab(where,"Successful packing");
              pod::pakflag pak_flag;
              next.PACKS = pak_flag.Y;
              auto annotation =  "(#"+ivtos(combination,",#")+")/" +
                                  "("+dvtos(combination_llg,0,",")+")/("+
                                      dvtos(combination_tfz,0,"z,")+"z)";
              next.ANNOTATION += " FUSE="+annotation;
              next.ANNOTATION += " PAK="+dtoss(next.CLASH_WORST,0)+"%/"+chtos(next.PACKS);
              next.set_star_from_pose();
              output_dag.push_back(next); //deep copy
              one_success = true;
          //    logProgressBarAgain(progress,"Combination ["+ivtos(combination)+"] " + annotation);
              logProgressBarAgain(progress,"Combination ["+ivtos(combination,",")+"] fused" + nN);
            }
            else
            {
              logTab(where,"Combination ["+ivtos(combination)+"] failed fuse");
              pod::pakflag pak_flag;
              next.PACKS = pak_flag.X;
            }
            logBlank(where);
          }
          if (!logProgressBarNext(progress)) break;
        }
        logProgressBarEnd(progress);
        if (one_success) one_fused = true;
        if (one_success) break;
      }
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Purge Submaximal Fusings");
    logTab(where,"Purge the nodes of parents that have fewer poses (if any)");
    int before = output_dag.size();
    std::map<int,int> counts;
    size_t max_maxcombos(0);
    for (int i = 0; i < output_dag.size(); i++)
    {
      dag::Node* node = &output_dag[i];
      max_maxcombos = std::max(max_maxcombos,node->POSE.size());
    }
    for (int i = 0; i < output_dag.size(); i++)
    {
      dag::Node* node = &output_dag[i];
      size_t n = node->POSE.size();
      node->generic_flag = (n == max_maxcombos);
      if (counts.count(n)) counts[n]++; else counts[n] = 1;
    }
    output_dag.erase_invalid();
    for (auto item : counts)
      logTab(out::LOGFILE,"Poses = " + ntos(item.first,max_maxcombos) + " (in " + itos(item.second) + " nodes)");
    logTab(where,"Number of nodes until = " + itos(before));
    logTab(where,"Number of nodes after = " + itos(output_dag.size()));
    }}

    {{
    input.value__boolean.at(".phasertng.fuse_translation_function.fused.").set_value(one_fused);
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Fuse Summary");
    if (!one_fused)
    {
      logTab(where,"No fused solutions");
      output_dag = stored_input_dag;
      for (int i = 0; i < output_dag.size(); i++)
      {
        output_dag[i].ANNOTATION += " FUSE(NONE)";
      }
      DAGDB.NODES = stored_input_dag;
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    else
    {
      size_t max_npose(0);
      for (int i = 0; i < output_dag.size(); i++)
      {
        max_npose = std::max(max_npose,output_dag[i].POSE.size());
      }
      logTab(where,"Number of poses (original+fuse) = " + itos(max_npose));
      logTab(where,"Number of nodes with fused number of poses = " + itos(output_dag.size()));
      DAGDB.NODES = output_dag;
      DAGDB.reset_entry_pointers(DataBase());
      if (DAGDB.NODES.front().SEEK.size() and DAGDB.NODES.front().SEEK.back().is_found())
      {
        logTab(where,"Constituents complete");
        input.value__boolean.at(".phasertng.fuse_translation_function.complete.").set_value(true);
      }
      logUnderLine(out::LOGFILE,"Seek Information");
      logTabArray(out::LOGFILE,DAGDB.seek_info_array());
    }
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
    DAGDB.constituents_complete(); //for assert
  }

} //phasertn=g
