#include <phasertng/math/subgroups.h>
#include <algorithm>
#include <cassert>
#include <phasertng/main/hoist.h>

namespace phasertng {

  std::string remove_hall_translations(std::string hall_symbol)
  {
    std::string hall;
    bool bracket(false);
    for (int i = 0; i < hall_symbol.size(); i++)
    {
      if (hall_symbol[i] == '(')
      {
        bracket = true;
        hall += hall_symbol[i];
      }
      else if (!bracket)
      {
        hall += hall_symbol[i];
      }
      else
      {
        std::string bracket_tokens;
        int ji = i;
        for (int j = ji; j < hall_symbol.size(); j++)
        {
          if (hall_symbol[i] != ')')
          {
            bracket_tokens += hall_symbol[i];
          }
          i++;
        }
        //bracked already added
        std::vector<std::string> subtokens;
        hoist::algorithm::split(subtokens,bracket_tokens,",");
        if (subtokens.size() == 1) continue;
        assert(subtokens.size()==3);
        for (int k = 0; k < 3; k++)
        {
          //check that it is not only a number
          if (subtokens[k].find('x') != std::string::npos ||
              subtokens[k].find('y') != std::string::npos ||
              subtokens[k].find('z') != std::string::npos)
          {
            std::string erased("1234567890+-/"); //e.g. "+1/4" at the end
            for (int kk = subtokens[k].size()-1; kk >=0 ; kk--)
            {
              if (erased.find(subtokens[k][kk]) != std::string::npos)
                subtokens[k].pop_back();
              else break;
            }
          }
          hall += subtokens[k];
          if (k == 0 || k == 1)
            hall += ",";
          else hall += ")";
        }
      }
    }
    if (hall.find('(') == std::string::npos)
      hall = hall + " (x,y,z)";
    return hall;
  }

  std::vector<cctbx::sgtbx::space_group_type>
  subgroups(cctbx::sgtbx::space_group_type reference_sg,bool reference,
        std::set<std::string>& unique) //to prevent duplicates across loops
  {
    std::vector<cctbx::sgtbx::space_group> subgroups;
    //in place change back to original basis
    cctbx::sgtbx::change_of_basis_op z2p_op = reference_sg.group().z2p_op();
    cctbx::sgtbx::change_of_basis_op p2z_op = z2p_op.inverse();
    //cctbx::sgtbx::change_of_basis_op cb_op_ref = reference_sg.cb_op();
    cctbx::sgtbx::space_group p_parent_group = reference_sg.group().change_basis(z2p_op);
    p_parent_group.make_tidy();
    for (int i_smx = 0; i_smx <  p_parent_group.order_p(); i_smx++)
    {
      cctbx::sgtbx::space_group P1;
      cctbx::sgtbx::space_group group_i = P1.expand_smx(p_parent_group(i_smx));
      for (int j_smx = i_smx; j_smx <  p_parent_group.order_p(); j_smx++)
      {
        cctbx::sgtbx::space_group subgroup_i = group_i;
        cctbx::sgtbx::space_group subgroup = subgroup_i.expand_smx(p_parent_group(j_smx));
        subgroup.make_tidy();
        //cctbx::sgtbx::change_of_basis_op cb_op_new = subgroup.type().cb_op();
        //cctbx::sgtbx::change_of_basis_op cb_op_ref2new = cb_op_ref.inverse()*cb_op_new;
        try {
       //  cb_op_ref2new.as_hkl(); //throws if not rotational change
     //   deleted for toxd, not reporting P21 as subgroup
          subgroup = subgroup.change_basis(p2z_op);
          std::string hall_symbol(subgroup.type().hall_symbol());
         // remove_hall_translations removes translation terms from hall extension
         // hall_symbol = remove_hall_translations(hall_symbol);
          subgroup = cctbx::sgtbx::space_group(hall_symbol);
          if (std::find(subgroups.begin(),subgroups.end(),subgroup) == subgroups.end())
          {
            subgroups.push_back(subgroup);
          }
        }
        catch (...) {}
      }
    }
    //because there is no comparator == for space_group_type, only space_group
    //find only works with space group
    if (reference)
    {
      std::vector<cctbx::sgtbx::space_group> tmp;
      for (auto sg : subgroups)
      {
        cctbx::sgtbx::space_group_type sgtype(sg);
        cctbx::sgtbx::space_group grp = sgtype.group();
        if (std::find(tmp.begin(),tmp.end(),grp) == tmp.end())
        {
          tmp.push_back(grp);
        }
      }
      subgroups = tmp;
    }
    std::vector<cctbx::sgtbx::space_group_type> sglist;
    for (auto item: subgroups)
      sglist.push_back(item.type());
    auto sortcmp = [](const cctbx::sgtbx::space_group_type a,
                  const cctbx::sgtbx::space_group_type b)
      { return a.group().order_z() == b.group().order_z() ?
           a.hall_symbol() < b.hall_symbol() :
           a.group().order_z() < b.group().order_z() ;
      };
    std::sort(sglist.begin(),sglist.end(),sortcmp);
    //different translations are probably the same
    //but translations are still important
    std::vector<cctbx::sgtbx::space_group_type> ssglist;
    for (auto item: sglist)
    {
      std::string hall_symbol(item.hall_symbol());
      // remove_hall_translations removes translation terms from hall extension
      std::string noT = remove_hall_translations(hall_symbol);
      if (unique.count(noT) == 0)
        ssglist.push_back(item);
      unique.insert(noT);
    }
    return ssglist;
  }

  std::vector<cctbx::sgtbx::space_group_type>
  groups_sysabs(cctbx::sgtbx::space_group_type reference_sg)
  {
    std::vector<cctbx::sgtbx::space_group> sysabs;
    cctbx::sgtbx::matrix_group::code reference_pg(reference_sg.group().point_group_type());
    cctbx::sgtbx::space_group        reference_patterson(reference_sg.group().build_derived_patterson_group());
    char                             reference_centring(reference_sg.group().conventional_centring_type_symbol());
    cctbx::sgtbx::space_group_symbol_iterator sgiter;
    cctbx::sgtbx::change_of_basis_op cb_op = reference_sg.cb_op().inverse();
  //std::string hm = reference_sg.type().universal_hermann_mauguin_symbol();
    for(;;)
    {
      cctbx::sgtbx::space_group_symbols symbols = sgiter.next();
      if (symbols.number() == 0) break;
      //symbols.hall() iterates over the list, including C 2 1 1 etc
      cctbx::sgtbx::space_group sg(symbols.hall());
      //must apply the change of basis operation in the reference space group to the iterated ones
      //which are only the tabulated settings in the international tables
      try {
            sg = sg.change_basis(cb_op);
        if (sg.conventional_centring_type_symbol() == reference_centring &&
            sg.point_group_type() == reference_pg &&
            sg.build_derived_patterson_group() == reference_patterson &&
            std::find(sysabs.begin(),sysabs.end(),sg) == sysabs.end())
        {
          try {//don't do type() for everything, just matches, too slow
            cctbx::sgtbx::space_group_type sgtype = sg.type();
            cctbx::sgtbx::change_of_basis_op cb_op = sgtype.cb_op();
            cb_op.as_hkl(); //throws error if not rotational change
            sysabs.push_back(sg);
          } catch (...) {}
        }
      } catch (...) {}
    }
    //special case I222/I212121
    if (reference_sg.hall_symbol() == "I 2b 2c") //I212121
    {
      cctbx::sgtbx::space_group sg("I 2 2 2");
      if (std::find(sysabs.begin(),sysabs.end(),sg) == sysabs.end())
        sysabs.push_back(sg);
    }
    if (reference_sg.hall_symbol() == "I 2 2") //I222
    {
      cctbx::sgtbx::space_group sg("I 21 21 21");
      if (std::find(sysabs.begin(),sysabs.end(),sg) == sysabs.end())
        sysabs.push_back(sg);
    }
    //special case I23/I213
    if (reference_sg.group() == cctbx::sgtbx::space_group("I 2 3")) //I23
    {
      cctbx::sgtbx::space_group sg("I 21 3");
      if (std::find(sysabs.begin(),sysabs.end(),sg) == sysabs.end())
        sysabs.push_back(sg);
    }
    if (reference_sg.group() == cctbx::sgtbx::space_group("I 21 3")) //I213
    {
      cctbx::sgtbx::space_group sg("I 2 3");
      if (std::find(sysabs.begin(),sysabs.end(),sg) == sysabs.end())
        sysabs.push_back(sg);
    }
    //because there is no comparator == for space_group_type, only space_group
    //find only works with space group
    std::vector<cctbx::sgtbx::space_group_type> sglist;
    for (auto item: sysabs)
      sglist.push_back(item.type());
    auto sortcmp = [](const cctbx::sgtbx::space_group_type a,
                  const cctbx::sgtbx::space_group_type b)
      { return a.group().order_z() == b.group().order_z() ?
           a.hall_symbol() < b.hall_symbol() :
           a.group().order_z() < b.group().order_z() ;
      };
    std::sort(sglist.begin(),sglist.end(),sortcmp);
    return sglist;
  }

  std::vector<cctbx::sgtbx::space_group_type>
  space_groups_in_same_point_group(
      cctbx::sgtbx::space_group_type original,
      std::string choice,
      std::set<std::string> sglist)
  {
    std::vector<cctbx::sgtbx::space_group_type>  sgalternative;
    {
      if (choice == "original")
      {
        sgalternative.push_back(original);
      }
      else if (choice == "hand")
      {
        sgalternative.push_back(original);
        if (original.is_enantiomorphic())
        {
          cctbx::sgtbx::change_of_basis_op cb_op = original.change_of_hand_op();
          cctbx::sgtbx::space_group enantiomorph = original.group().change_basis(cb_op);
          sgalternative.push_back(enantiomorph.type());
        }
      }
      else if (choice == "all")
      {
        std::vector<cctbx::sgtbx::space_group_type> gsysabs = groups_sysabs(original);
        for (int i = 0; i < gsysabs.size(); i++)
          sgalternative.push_back(gsysabs[i]);
      }
      else if (choice == "list")
      {
        for (auto sg : sglist)
        {
          cctbx::sgtbx::space_group_type sg_type;
          try { sg_type = cctbx::sgtbx::space_group_type(sg); }
          catch(...) {
            assert(false);
          }
          sgalternative.push_back(sg_type);
        }
      }
    }
    return sgalternative;
  }

  std::set<sorted_space_group_type>
  subgroups_sysabs(cctbx::sgtbx::space_group_type original,bool reference)
  {
    std::set<sorted_space_group_type>  ALLSG;
    std::vector<cctbx::sgtbx::space_group_type> sysabs_sg = groups_sysabs(original);
    for (int t = 0; t < sysabs_sg.size(); t++)
    {
      cctbx::sgtbx::space_group_type SG(sysabs_sg[t]);
      std::set<std::string> unique;
      std::vector<cctbx::sgtbx::space_group_type> sglist = subgroups(SG,false,unique);
      for (int s = 0; s < sglist.size(); s++)
        ALLSG.emplace(sglist[s]);
    }
    if (reference)
    {
      std::set<sorted_space_group_type> refallsg;
      for (auto sg : ALLSG)
      {
        cctbx::sgtbx::space_group_symbols symbol(sg.number());
        cctbx::sgtbx::space_group_type sgtype("Hall: " + symbol.hall());
        refallsg.insert(sorted_space_group_type(sgtype));
      }
      ALLSG = refallsg;
    }
    return ALLSG;
  }

  std::vector<cctbx::sgtbx::space_group_type>
  sad_space_groups(
      cctbx::sgtbx::space_group_type original,
      std::string choice,
      std::set<std::string> sglist)
  {
    auto allsgs = space_groups_in_same_point_group(original,choice,{});
    std::set<sorted_space_group_type> sadset(allsgs.begin(),allsgs.end()); //vector to set
    for (auto sg : allsgs)
    {
      auto handsg = space_groups_in_same_point_group(sg,"hand",{});
      int ifound(-999);
      for (int i = 0; i < handsg.size(); i++)
        if (sadset.count(handsg[i]))
          ifound = i;
      for (int i = 0; i < handsg.size(); i++)
        if (i != ifound || ifound < 0)
          sadset.erase(handsg[i]);
    }
    std::vector<cctbx::sgtbx::space_group_type> result;
    for (auto sg : sadset)
      result.push_back(sg); //vector to set
    return result;
  }

} //phaser
