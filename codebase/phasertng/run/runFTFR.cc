//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/PointsStored.h>
#include <phasertng/site/PointsRandom.h>
#include <phasertng/site/AnglesRandom.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/math/mean_sigma_top.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <phasertng/src/Packing.h>

namespace phasertng {

  //Translation_function_zscore_equivalent
  void Phasertng::runFTFR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_curl())
    throw Error(err::INPUT,"Dag not prepared with correct mode (ftf failed?)");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete, skipping");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("curl");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::TRACE_PDB,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    pod::pakflag pak_flag;
    double io_percent = input.value__percent.at(".phasertng.rescore_translation_function.percent.").value_or_default();
    int io_maxstored = input.value__int_number.at(".phasertng.rescore_translation_function.maximum_stored.").value_or_default();
    double io_nprint = input.value__int_number.at(".phasertng.rescore_translation_function.maximum_printed.").value_or_default();
    bool io_keep_hightfz = (input.value__boolean.at(".phasertng.packing.keep_high_tfz.").value_or_default());
    double io_overlap = (input.value__percent.at(".phasertng.packing.overlap.").value_or_default());
    double io_hightfz = input.value__flt_number.at(".phasertng.packing.high_tfz.").value_or_default();
    bool io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").is_default() ? false : input.value__boolean.at(".phasertng.coiled_coil.").value_or_default(); //default None
    unsigned random_seed = 0;

    //determine if this is the first tf or not
    auto tf2 = DAGDB.NODES.number_of_poses();
    auto tf1 = !tf2;
    int isize = DAGDB.size();

    //initialize tNCS parameters from reflections and calculate correction terms
    //FTF
    bool store_halfR(true);
    bool store_fullR(tf2);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    auto io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    double resolution = io_hires ? io_hires: 0;
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    //calculate the default sampling
    double cluster_distance(0);
    {{
    std::tie(cluster_distance,std::ignore) = EntryHelper::clustdist(
      selected.STICKY_HIRES,
      DAGDB.WORK->NEXT.ENTRY->HELIX,
      io_coiled_coil,
      input.value__flt_number.at(".phasertng.fast_translation_function.helix_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.fast_translation_function.sampling_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.fast_translation_function.sampling.").value_or_default());
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Clustering");
    double io_helix_factor = input.value__flt_number.at(".phasertng.fast_translation_function.helix_factor.").value_or_default();
    if (io_helix_factor != 1)
    {
      logTab(where,"All-helical (helix factor gridding): " + btos(DAGDB.WORK->NEXT.ENTRY->HELIX));
      logTab(where,"Coiled-coil (helix factor gridding): " + btos(io_coiled_coil));
      logTab(where,"Helix factor: " + dtos(io_helix_factor));
    }
    logTab(where,"Cluster Distance:  " + dtos(cluster_distance,5,2) + " Angstroms");
    logBlank(where);
    }}

    int nparents = DAGDB.NODES.number_of_parents();
    int nrand  = input.value__int_number.at(".phasertng.rescore_translation_function.nrand.").value_or_default();
    bool calculate_tfz = input.value__boolean.at(".phasertng.rescore_translation_function.calculate_zscore.").value_or_default();

    double best_llg = std::numeric_limits<double>::lowest();
    pod::parent_mean_sigma_top stats; //id/mean/sd/top
    if (tf1 and REFLECTIONS->SG.is_p1())
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"First Translation Function in P1");
      AnglesRandom rotList(nrand,random_seed++);
      dvect3 best_site;
      logTab(where,"Random angles (" + itos(nrand) + ")");
      if (!calculate_tfz)
        logTab(out::LOGFILE,"Statistics do not change ftf zscores");

      DAGDB.restart(); //first work node
      DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      sv_double v;
      auto original_turn = DAGDB.WORK->NEXT.EULER; //all zero, at the origin
      double original_llg(0);
      //all backgrounds the same (none)
      //take first node only
      pod::FastPoseType ecalcs_sigmaa_pose;
      std::tie(original_llg) = DAGDB.work_node_likelihood(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS,
          &ecalcs_sigmaa_pose);
      rotList.restart();
      logProgressBarStart(where,"Scoring random rotations at origin",rotList.count_sites(),0,false);
      while (!rotList.at_end())
      {
        DAGDB.WORK->NEXT.EULER = rotList.next_site();
        //no change to position
        DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        //all the same space group
        Site newsite(DAGDB.WORK->NEXT.EULER,DAGDB.WORK->LLG);
        //could store in the dag here, but we select first, then append at the end
        v.push_back(DAGDB.WORK->LLG);
        if (DAGDB.WORK->LLG > best_llg)
        {
          best_llg = DAGDB.WORK->LLG;
          best_site = DAGDB.WORK->NEXT.EULER;
        }
        logProgressBarNext(where,0);
      }
      logProgressBarEnd(where,0);
      dvect3 mst = math::mean_sigma_top(v);
      stats.mst[DAGDB.WORK->PARENTS.back()] = mst;
      DAGDB.WORK->NEXT.EULER = original_turn;
      DAGDB.WORK->LLG = original_llg;
      DAGDB.WORK->FSS = false;
      logTab(where,"Random Mean (Sigma):  " + dtos(mst[0],10,2) + "   (" + dtos(mst[1],5,2) + ")");
      logTab(where,"Random Highest (TFZ): " + dtos(mst[2],10,2) + "   (" + dtos((mst[2]-mst[0])/mst[1],5,2) + ")");
      logBlank(where);
      DAGDB.restart();
      while (!DAGDB.at_end())
      { //there is only one entry in the stats, as all parents use the same stats
        if (calculate_tfz and mst[1])
        {
          DAGDB.WORK->ZSCORE = (DAGDB.WORK->LLG - mst[0])/mst[1];
        }
      }
      //we could use llg = 0 in this case
      if (stats.max_top() > DAGDB.NODES.top_llg())
      {
        out::stream where = out::LOGFILE;
        logTab(where,"Maximum Background  LLG = " + dtos(stats.max_top()));
        logTab(where,"Maximum Rescore LLG = " + dtos(DAGDB.NODES.top_llg()));
        logTab(where,"Random Best Site = (" + dvtos(best_site,6,4) + ")");
        logWarning("Best llg from random orientations (P1) is higher than search llg");
      }
    }
    else
    {
      pod::FastPoseType ecalcs_sigmaa_pose;
      out::stream progress = out::LOGFILE;
//the mean for any translation function is tha background poses, all else should be random about this
      for (int i = 0; i < DAGDB.size(); i++)
        DAGDB.NODES[i].generic_flag = false; // in this senario none are precalculated
      auto set_of_parents = DAGDB.set_of_parents();
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        bool new_parent = !stats.mst.count(DAGDB.WORK->PARENTS.back());
        if (new_parent)
        {
          DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
          if (diffsg)
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          DAGDB.WORK->NEXT.PRESENT = false;
          double original_llg = DAGDB.WORK->LLG;
          //original_llg is the refined value of the curl from the gyre, not the background poses
          //curl background could also be used but this would not be per-parent but per-node
          //however this would match the use of random rotation above
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
          //stats.mst[DAGDB.WORK->PARENTS.back()] = dvect3(0,DAGDB.WORK->LLG,0);
          stats.mst[DAGDB.WORK->PARENTS.back()] = dvect3(DAGDB.WORK->LLG,0,DAGDB.WORK->LLG);
          DAGDB.WORK->NEXT.PRESENT = true;
          DAGDB.WORK->LLG = original_llg;
        }
      } //dagdb
      logTab(out::VERBOSE,"Mean is background LLG");
      logTab(out::VERBOSE,"Maximum Background LLG = " + dtos(stats.max_top()));

      {{
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Rescoring Positions");
      logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
      auto set_of_parents = DAGDB.set_of_parents();
      int n(1),N(DAGDB.size());
      bool breakit(false);
      double best_score = std::numeric_limits<double>::lowest();
      logProgressBarStart(where,"Rescoring positions",DAGDB.size());
      for (auto parent : set_of_parents)
      {
        //group the turns by background so that the ecalcs_sigmaa_pose is done first time only
        if (breakit) break;
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.restart(); //very important
        while (!DAGDB.at_end() and !breakit)
        {
          std::string nN = nNtos(n,N);
          if (DAGDB.WORK->PARENTS.back() != parent)
            continue;
          if (DAGDB.WORK->generic_flag)
          {
            if (calculate_tfz and stats.has_sigma(parent))
            {
              DAGDB.WORK->ZSCORE = stats.zscore(parent,DAGDB.WORK->LLG);
            }
            if (DAGDB.WORK->LLG > best_score)
            {
              best_score = DAGDB.WORK->LLG;
              logProgressBarAgain(where,"New Best LLG=" + dtos(best_score,2)+nN);
            }
            n++;
            if (!logProgressBarNext(where)) break;
            continue;
          }
          DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
          if (diffsg)
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          if (!ecalcs_sigmaa_pose.precalculated)
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS);
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose); //if not the first parent node, then this will be precalculated
          DAGDB.WORK->FSS = false;
          if (calculate_tfz and stats.has_sigma(DAGDB.WORK->PARENTS.back()))
          {
            DAGDB.WORK->ZSCORE = stats.zscore(DAGDB.WORK->PARENTS.back(),DAGDB.WORK->LLG);
          }
          if (DAGDB.WORK->LLG > best_score)
          {
            best_score = DAGDB.WORK->LLG;
            logProgressBarAgain(where,"New Best LLG=" + dtos(best_score,2)+nN);
          }
          n++;
          if (!logProgressBarNext(where)) breakit = true;
        }
      }
      logProgressBarEnd(where);
      }}

      logUnderLine(out::LOGFILE,"Merged Results");
      logTab(out::LOGFILE,"Number of backgrounds: " + itos(stats.size()));
      if (stats.max_top() > DAGDB.NODES.top_llg())
      {
        out::stream where = out::LOGFILE;
        logTab(where,"Maximum Background LLG = " + dtos(stats.max_top(),2));
        logTab(where,"Maximum Rescore LLG = " + dtos(DAGDB.NODES.top_llg(),2));
        logWarning("Best llg from background positions is higher than search llg");
      }

      if (stats.size() > 1)
      {
        out::stream where = out::VERBOSE;
        stats.calculate_histogram_mean();
        logHistogram(where,stats.histdat,10,false,"TF means",stats.histmin,stats.histmax);
        stats.calculate_histogram_sigma();
        logHistogram(where,stats.histdat,10,false,"TF sigmas",stats.histmin,stats.histmax);
      }

      //if there is clustering we have to treat each background separately
      //group on parent, cluster, purge cluster, and make a new NODES
      //if no clustering, then the purge percent can be applied independent of background
      if (input.value__boolean.at(".phasertng.rescore_translation_function.cluster.").value_or_default())
      {
        out::stream progress = out::LOGFILE;
        logUnderLine(progress,"Clustering");
        dag::NodeList output_dag;
        int n(1),N(stats.size());
        logProgressBarStart(progress,"Clustering for parents",stats.size());
        for (auto item : stats.mst)
        {
          out::stream where = out::VERBOSE;
          std::string nN = nNtos(n++,N);
          logUnderLine(where,"Clustering for Parent" + nN);
          FastTranslationFunction trafun(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon);
          for (int i = 0; i < DAGDB.NODES.size(); i++)
          {
            trafun.setup_spacegroup(&DAGDB.NODES[i]);
            auto& node = DAGDB.NODES[i]; //because not using the iterator
            if (item.first == node.PARENTS.back())
            {
              auto& curl = node.NEXT;
              const dmat33 fracmat = DAGDB.WORK->CELL.fractionalization_matrix();
              dvect3 shift_fract = fracmat*curl.SHIFT_ORTHT; //i is to store the index, the curl is not used
              Site newsite(shift_fract,node.LLG,i); //i is to store the index, the curl is not used
              trafun.translation_function_data.push_back(newsite); //temp for purge and stats
            }
          }

          {{
          out::stream where = out::VERBOSE;
          bool AllSites(true); //not just the clustered top sites
          logTableTop(where,"Fast Translation Function Rescored (FSS sort order before clustering)"+nN,false);
          logTabArray(where,trafun.logTable(io_nprint,AllSites));
          logTableEnd(where);

          for (int i = 0; i < DAGDB.NODES.size(); i++)
             DAGDB.NODES[i].generic_int = i+1;
          logEllipsisOpen(where,"Sorting on LLG"+nN);
          trafun.full_sort(); //full sort on new value, no erase
          logEllipsisShut(where);

          logEllipsisOpen(where,"Cluster"+nN);
          int cluster_back = input.value__int_number.at(".phasertng.rescore_translation_function.cluster_back.").value_or_default();
          trafun.cluster(cluster_distance,cluster_back);
          logEllipsisShut(where);
          }}

          {{
          logEllipsisOpen(where,"Purge by cluster"+nN);
          int before = trafun.count_sites();
          trafun.select_topsites_and_erase();
          logEllipsisShut(where);
          logTab(where,"Number before purge: " + itos(before));
          logTab(where,"Number after purge:  " + itos(trafun.count_sites()));
          }}

          {{
          bool AllSites(false);
          logTableTop(where,"Fast Translation Function Rescored (LLG sort order after clustering)"+nN,false);
          logTabArray(where,trafun.logTable(io_nprint,AllSites));
          logTableEnd(where);
          }}

          {{
          logEllipsisOpen(where,"Append to DAG"+nN);
          //add the growing list to the DAG, note that this will not be sorted overall
          for (auto& item : trafun.translation_function_data)
          {
            //no selection for top peaks, recore them all
            output_dag.push_back(DAGDB.NODES[item.index]); //pointer to last in list
          }
          logEllipsisShut(where);
          }}
          if (!logProgressBarNext(progress)) break; //but not setting timeout
        }
        logProgressBarEnd(progress);
        DAGDB.NODES = output_dag;
        DAGDB.reset_entry_pointers(DataBase());
      }
    } //tf2

    bool io_use_packing = input.value__boolean.at(".phasertng.fast_translation_function.packing.use.").value_or_default();
    double io_lowtfz = input.value__flt_number.at(".phasertng.fast_translation_function.packing.low_tfz.").value_or_default();
    double io_worst_clash = input.value__percent.at(".phasertng.packing.percent.").value_or_default();

    if (io_use_packing)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Packing");
      logProgressBarStart(where,"Packing positions",DAGDB.size(),1,false);
      bool found_top_packing(false);
      auto curllist = DAGDB.NODES; //for reversion, contains the nodes as pose/curl
      for (int i = 0; i < DAGDB.size(); i++)
        DAGDB.NODES[i].convert_curl_to_pose(REFLECTIONS->TNCS_VECTOR);
      for (int i = 0; i < DAGDB.size(); i++)
      { //must check at least the top for packing if it has not already been checked in ftf
        std::string nN = nNtos(i+1,DAGDB.size());
        if (!found_top_packing)
        {
          // Find the maximum element in the unsorted part
          int idx = i;
          double best_top = DAGDB.NODES[i].LLG;
          bool all_unsorted_below_lowtfz(true);
          for (int j = i+1; j < DAGDB.size(); j++)
          {
            auto& node = DAGDB.NODES[j];
            if (node.LLG >= best_top)
              { best_top = node.LLG; idx = j; }
            if (node.ZSCORE > io_lowtfz)
              all_unsorted_below_lowtfz = false;
          }
          // Swap the found minimum element with the first element of the unsorted part
          if (Suite::Extra())
          {
            logTab(out::TESTING,"Swap: "  + itos(i) + " " + itos(idx) + " LLG=" + dtos(DAGDB.NODES[idx].LLG) + nN);
          }
          std::swap(DAGDB.NODES[i], DAGDB.NODES[idx]);
          std::swap(curllist[i], curllist[idx]); //must swap here or else not in correct order for top_llg_packs
          DAGDB.WORK = &DAGDB.NODES[i];
          DAGDB.reset_work_entry_pointers(DataBase());
          dag::Node* work = DAGDB.WORK;
          dag::Node* nwork = &curllist[i];
          if (pak_flag.packs_Y(work->PACKS))
          { //already packed in ftf
            found_top_packing = true;
          }
          else if (pak_flag.packs_XZO(work->PACKS))
          { //already packed in ftf as 'X' 'Z' 'O'
            //do nothing
          }
          else if (!all_unsorted_below_lowtfz)
          { //keep going unless the there is nothing interesting in the remaining tfz
            double distance_factor(0.5); //lower than trace distance sampling
            Packing packing(distance_factor);
            //have to have work in DagDatabase so that pointers to entries and sgs can be found
            packing.init_node(work); //must be linked with entries
            packing.multiplicity_and_exact_site();
            bool last_only(false);
            bool no_cell_translation(REFLECTIONS->MAP);
            packing.calculate_packing(last_only,no_cell_translation);
            double worst = packing.PACKING_DATA.worst_percent();
            if (worst <= io_worst_clash) //= very important for zero
            {
              work->PACKS = nwork->PACKS = pak_flag.Y;
              found_top_packing = true;
              logProgressBarAgain(where,"Packs LLG=" + dtos(work->LLG,2)+nN);
            }
            else if (io_keep_hightfz and work->ZSCORE >= io_hightfz)
            {
              if (worst >= io_overlap)
              {
                work->PACKS = nwork->PACKS = pak_flag.O;
              }
              else
              {
                work->PACKS = nwork->PACKS = pak_flag.Z;
              }
            }
            else
            {
              work->PACKS = nwork->PACKS = 'X';
            }
          }
          else if (all_unsorted_below_lowtfz)
          {
            logProgressBarAgain(where,"Remaining all TFZ<" + dtos(io_lowtfz,2)+nN);
            found_top_packing = true; //so only printed once
          }
          if (Suite::Extra())
          {
            logTab(out::TESTING,"Packs: " + chtos(work->PACKS) + nN);
          }
        }
        logProgressBarNext(where,1);
      }
      logProgressBarEnd(where);
      DAGDB.NODES = curllist;
    }

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    double lowest_mean = stats.merged_lowest_mean();
    bool hasvalue; double top;
    std::tie(hasvalue,top) = DAGDB.NODES.top_llg_packs(); //use packs so that w/wo high_tfz get same bottom of list
    if (hasvalue and top > lowest_mean) //lowest cannot be below mean
    {
      dag::NodeList overall_dag = DAGDB.NODES;
      std::map<std::string,dag::NodeList> sgnodes; //not used directly, copied for init
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        sgnodes[DAGDB.NODES[i].SG->sstr()] = dag::NodeList();
      auto sgnodes_overall = sgnodes; //blank
      double overall_cut;
      //always do overall, returns overall_dag purged and lookup of same on spacegroup
      {
        logEllipsisOpen(where,"Purge by percent of LLG");
        overall_cut = overall_dag.apply_partial_sort_and_percent_llg(io_percent,top,lowest_mean);
        logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
        logTab(where,"Top/Mean/Cutoff LLG: " + dtos(top,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(overall_cut,2));
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(overall_dag.size()));
        for (int i = 0; i < overall_dag.size(); i++)
          sgnodes_overall[overall_dag[i].SG->sstr()].push_back(overall_dag[i]);
        logEllipsisShut(where);
      }
      auto sgnodes_subgroups = sgnodes; //blank map, just the spacegroups and type defined
      bool purge_by_percent_by_space_group = true; //AJM TODO below, selection
      //if this is really hard, then keep everything separate at all times
    //  bool purge_by_percent_by_space_group = (DAGDB.WORK->TNCS_INDICATED == 'Y' and  DAGDB.WORK->TWINNED == 'Y');
    //  logTab(where,"tNCS     " + btos(DAGDB.WORK->TNCS_INDICATED == 'Y')); //not test case
     // logTab(where,"Twinning " + btos(DAGDB.WORK->TWINNED == 'Y'));
      if (purge_by_percent_by_space_group)
      {
        //start again and build the list on spacegroups
        logEllipsisOpen(where,"Purge by percent by space group");
        logTab(where,"Percent: "  + dtos(io_percent,4,1) + "%");
        for (int i = 0; i < DAGDB.NODES.size(); i++)
          sgnodes_subgroups[DAGDB.NODES[i].SG->sstr()].push_back(DAGDB.NODES[i]);
        for (auto& sgsubgroup : sgnodes_subgroups)
        {
          //purge each separately and return in subgroup
          logChevron(where,"Space Group: "  + sgsubgroup.first);
          int beforesg = sgsubgroup.second.size();
          bool hasvalue; double sg_top_llg_packs;
          std::tie(hasvalue,sg_top_llg_packs) =  sgsubgroup.second.top_llg_packs();
          std::string toppackstr = hasvalue? dtos(sg_top_llg_packs,2) : "---";
          if (hasvalue and sg_top_llg_packs <= lowest_mean)
          { //lowest if !hasvalue
            logTab(where,"Mean above top packing in this space group, keep all");
            //because some space groups may be randomly lower
            //keep all because there may be others that pack by we aren't going to do all packing here
            //this is a bit weird because the ones that have a low top get to keep everything
            //but there are probably few because the top is already very low in the overall list
          }
          else if (!hasvalue) //there are no top packing llg
          {
            logTab(where,"No (top) packing in this space group, clear all");
            sgsubgroup.second.clear();
          }
          else
          {
            double cut = sgsubgroup.second.apply_partial_sort_and_percent_llg(io_percent,sg_top_llg_packs,lowest_mean);
            logTab(where,"Top-Packs/Mean/Cutoff LLG:      " + toppackstr + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
          }
          logTab(where,"Number before purge: " + itos(beforesg));
          logTab(where,"Number after purge:  " + itos(sgsubgroup.second.size()));
        }
        logEllipsisShut(where);
      }
      else
      {
        sgnodes_subgroups = sgnodes_overall;
      }
      dag::NodeList output_dag;
      if (io_keep_hightfz and purge_by_percent_by_space_group)
      {
        auto best_sg = sgnodes_subgroups.begin()->first;
        //sgnodes_subgroups have been cut relative to the sg top
        for (auto& sgnode : sgnodes_subgroups)
          if (sgnode.second.top_llg_packs() > sgnodes_subgroups[best_sg].top_llg_packs())
            best_sg = sgnode.first;
        int best_sg_numpeaks = sgnodes_subgroups[best_sg].size();
        for (auto& sgnode : sgnodes_subgroups)
        {
          //if the peak is in the best sg, all peaks are taken
          //or if peak in the subgroup has a high tfz then up to best_sg numbpeaks is taken
          //low tfz ones are lost
          for (int i = 0; i < sgnode.second.size(); i++)
          {
            if (sgnode.first == best_sg or  //all the ones in the best space group, equivalent to the overall cut
                sgnode.second[i].LLG >= overall_cut or  //all the ones that made the overall cut
               (i < best_sg_numpeaks and (sgnode.second[i].ZSCORE > io_hightfz))) //below cut up to best_sg_numpeaks from the runners up
            {
              output_dag.push_back(sgnode.second[i]);
              //with tncs can end up with lots of bad solutions with high tfz, keep these???
            }
          }
        }
      }
      else
      {
        output_dag = overall_dag;
      }
      DAGDB.NODES = output_dag;
    }
    else
    {
      logWarning("Best llg is lower than mean llg from random search");
      logTab(where,"Purge by percent not performed");
    }
    }}

    bool io_prune_tncs_duplicates = input.value__boolean.at(".phasertng.rescore_translation_function.purge_tncs_duplicates.").value_or_default();
    if (io_prune_tncs_duplicates)
    {
      if (DAGDB.WORK->POSE.size() == 0 and nmol > 2)//because nmol==2 has rotations
      {
        out::stream where = out::LOGFILE;
        logEllipsisOpen(where,"Purge by tncs duplicates");
        int before = DAGDB.NODES.size();
        DAGDB.NODES.flag_tncs_duplicates(REFLECTIONS->TNCS_VECTOR);
        logEllipsisShut(where);
        DAGDB.NODES.erase_invalid();
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
      }
    }

    {{
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge by maximum number");
      DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored:      " + itos(io_maxstored));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
    }
    else
    {
      DAGDB.NODES.apply_sort_llg();
    }
    }}

    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    int w = itow(io_nprint);
    int ww = itow(isize);
    out::stream where = out::LOGFILE;
    logTableTop(where,"Fast Translation Function Rescored Results");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using LLG here
    int q = DAGDB.NODES.llg_width(false); //even if it is a map, we are using LLG here
    int cellw = REFLECTIONS->UC.width();
    if (DAGDB.size())
    {
      logTab(where,"#o      Final rank of the peak");
      logTab(where,"#i      Input rank of the peak");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %-*s %s  %s %*s %7s %6s  %*s %*s",
          w,"#o",ww,"#i",
          dag::header_polar().c_str(),
          dag::header_ortht(cellw).c_str(),
          q,"LLG","TFZ","FFS%",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      auto& curl = node->NEXT;
      auto modlid = curl.IDENTIFIER;
      auto reflid = node->REFLID;
      logTab(where,snprintftos(
          "%-*i %-*i %s  %s %*.*f %7.2f %6.1f  %*s %*s",
          w,i,ww,node->generic_int,
          dag::print_polar(curl.SHIFT_MATRIX).c_str(),
          dag::print_ortht(curl.SHIFT_ORTHT,cellw).c_str(),
          q,p,node->LLG,
          node->ZSCORE,
          node->PERCENT,
          unique_search ? 0:uuid_w,unique_search ? "":modlid.string().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":reflid.string().c_str()));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
      logTab(where,snprintftos(
            "%*s %5s EULER=[% 6.1f % 6.1f % 6.1f] FRACT=[% 7.4f % 7.4f % 7.4f]",
            w,"", "",curl.EULER[0],curl.EULER[1],curl.EULER[2],
            curl.FRACT[0],curl.FRACT[1],curl.FRACT[2]));
      logTab(where,snprintftos(
            "%*s %5s ORTHR={%+6.3f %+6.3f %+6.3f}",
            w,"", "",curl.ORTHR[0],curl.ORTHR[1],curl.ORTHR[2]));
#endif
      logTab(where,"---");
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    {{
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      double lowest_mean = stats.merged_lowest_mean();
      double top = DAGDB.NODES.top_llg(); //can't use packs because nothing may pack
      double percent = 100.*(node->LLG-lowest_mean)/(top-lowest_mean);
      double unstable100=101;
      std::string percentstr = (percent < 0) ? "<0" : std::string((percent > unstable100) ? "random" : dtoi(percent));
      node->ANNOTATION += " TFR=" + dtoi(node->LLG)+"/"+percentstr+"%/"+dtos(std::floor(node->ZSCORE),0)+"z";
    }
    }}

    for (std::string selection : sv_string({pak_flag.packs_YU_str(),pak_flag.packs_XZO_str()}))
    {{ //packs, clashes, rescued
    int count_packs(0);
    for (auto& node : DAGDB.NODES)
    {
      bool condition(false); //but will pick out a value in the loop
      if (selection == pak_flag.packs_YU_str())
        condition = pak_flag.packs_YU(node.PACKS);
      else if (selection == pak_flag.packs_XZO_str())
        condition = pak_flag.packs_XZO(node.PACKS);
      if (condition) count_packs++;
    }
    int w = itow(io_nprint);
    double lowest_mean = stats.merged_lowest_mean();
    double top = DAGDB.NODES.top_llg();
    out::stream where = out::SUMMARY;
    logTableTop(where,"Fast Translation Function Rescored Summary: Packing " + selection);
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    DAGDB.restart(); //very important to call this to set WORK
    logTab(where,"Last placed: " + DAGDB.WORK->NEXT.IDENTIFIER.str());
    logTab(where,"Percent Cutoff: " + dtos(io_percent) + "%");
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int p = DAGDB.NODES.llg_precision(false);
    int q = DAGDB.NODES.llg_width(false);
    logTab(where,snprintftos(
        "%-*s %*s %6s %7s %6s %5s %*s %-13s  %*s %*s",
        w,"#",
        q,"LLG",
        "LLG%","TFZ","FSS%","Packs",uuid_w,"parent","Space Group",
        unique_search ? 0:uuid_w,unique_search ? "":"modlid",
        unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      bool condition(false); //will pick out a condition
      if (selection == pak_flag.packs_YU_str())
        condition = (pak_flag.packs_YU(node->PACKS));
      else if (selection == pak_flag.packs_XZO_str())
        condition = (pak_flag.packs_XZO(node->PACKS));
      if (!condition)
        continue;
      auto modlid = node->NEXT.IDENTIFIER;
      auto reflid = node->REFLID;
      double percent = 100.*(node->LLG-lowest_mean)/(top-lowest_mean);
      double unstable100=101;
      std::string percentstr = (percent < 0) ? "< 0.0" : std::string((percent > unstable100) ? "random" : dtos(percent,6,1));
      logTab(where,snprintftos(
          "%-*i %*.*f %6s %7.2f %6.1f   %c   %*s %-13s  %*s %*s",
          w,i,
          q,p,node->LLG,
          percentstr.c_str(),
          node->ZSCORE,
          node->PERCENT,
          node->PACKS,
          uuid_w,std::to_string(node->PARENTS.back()).c_str(),
          node->SG->sstr().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":modlid.string().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":reflid.string().c_str()));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    if (count_packs == 0) logTab(where,"No poses");
    logTableEnd(where);
    }}

    {{
    out::stream where = out::SUMMARY;
    logBlank(where);
    double topl = std::numeric_limits<double>::lowest();
    double topz = std::numeric_limits<double>::lowest();
    for (auto& item : DAGDB.NODES)
    {
      topl = std::max(topl,item.LLG);
      topz = std::max(topz,item.ZSCORE);
    }
    logTab(where,"Highest Overall LLG=" + dtos(topl,2));
    logTab(where,"Highest Overall TFZ=" + dtos(topz,2));
    }}
    DAGDB.restart();
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
