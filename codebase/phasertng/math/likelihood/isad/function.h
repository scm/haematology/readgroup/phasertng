//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_isad_function_class__
#define __phasertng_likelihood_isad_function_class__
#include <phasertng/math/likelihood/isad/tgh.h>
#include <phasertng/math/likelihood/isad/sigmaa.h>
#include <phasertng/math/likelihood/isad/tgh_flags.h>
#include <phasertng/math/likelihood/isad/EqualUnity.h>
#include <scitbx/array_family/misc_functions.h>
#include <vector>
#include <limits>

namespace phasertng { namespace likelihood { namespace isad {

class function
{
  private:
    int TWO = 2;
    //runtime constants
    double minFloat = 0;
    double logMaxFloat = 0;
    double maxArgAllow = 0;
    double minArgAllow = 0;
    double minExpArgHalf = 0;

    //the function takes a pointer to the SADSIGMAA values
    //which are refined (via the pointer)
    //stored to the node
    //and passed back to the function later on
    likelihood::isad::sigmaa* SADSIGMAA;

  private:
    std::vector<EqualUnity> acentPhsIntg;

  public:
    function(likelihood::isad::sigmaa* SADSIGMAA_) : SADSIGMAA(SADSIGMAA_)
    {
      minFloat = std::numeric_limits<double>::min();
      logMaxFloat = std::log(std::numeric_limits<double>::max());
      maxArgAllow =  logMaxFloat/TWO-std::log(360.);
      minArgAllow =  -maxArgAllow;
      minExpArgHalf = -logMaxFloat/TWO;
      //setup step sizes for integration
      acentPhsIntg.clear();
      for (int p = 3; p <= 120; p++) //min and max ranges for number of points
        if (360 % p == 0) //number of points fits into 360
          acentPhsIntg.push_back(EqualUnity(p));
    }

  public:
    int
    calculate_integration_points(
      const double& RESN,
      const double& TEPS,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const cmplex& FHPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const cmplex& FHNEG,
      const double& epsnSigmaPos,
      const double& epsnSigmaNeg
    );

    likelihood::isad::tgh
    tgh_acentric_canonical(
      const int& s,
      const double& RESN,
      const double& TEPS,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const cmplex& FHPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const cmplex& FHNEG,
      const likelihood::isad::tgh_flags&,
      const int&
    ) const;

    likelihood::isad::tgh
    tgh_centric_canonical(
      const int& s,
      const double& RESN,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const double& PHSR,
      const likelihood::isad::tgh_flags&
    ) const;

    likelihood::isad::tgh
    tgh_singleton_canonical(
      const bool& PLUS,
      const int& s,
      const double& RESN,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const likelihood::isad::tgh_flags&
    ) const;

    likelihood::isad::tgh
    tgh_acentric(
      const int& s,
      const int& EPS,
      const double& TEPS,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const cmplex& FHPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const cmplex& FHNEG,
      const likelihood::isad::tgh_flags&,
      const int&
    ) const;

    likelihood::isad::tgh
    tgh_centric(
      const int& s,
      const int& EPS,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const double& PHSR,
      const likelihood::isad::tgh_flags&
    ) const;

    likelihood::isad::tgh
    tgh_singleton(
      const bool& PLUS,
      const int& s,
      const int& EPS,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const likelihood::isad::tgh_flags&
    ) const;

    likelihood::isad::tgh
    tgh_null(
      const int& s,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const double& RESN,
      const double& TEPS,
      const likelihood::isad::tgh_flags& flags
    ) const;
};

}}} //phasertng
#endif
