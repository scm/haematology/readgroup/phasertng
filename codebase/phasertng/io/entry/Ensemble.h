//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Ensemble_class____
#define __phasertng_Ensemble_class____
#include <phasertng/main/includes.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/EntryBase.h>
#include <phasertng/src/Bin.h>
#include <phasertng/main/Identifier.h>

namespace phasertng {

  class Ensemble : public EntryBase
  {
    public: //members
      Identifier MODLID;
      Identifier REFLID; //not set
      std::string TIMESTAMP = "";
      double     SPHERE = 0;
      Bin        BIN = Bin();
      UnitCell   UC = UnitCell();
      af_millnx  MILLER = af_millnx();
      af_cmplex  ECALC = af_cmplex();
      af_double  SIGMAA = af_double();
      af_double  SSQR = af_double();

    public: //constructors
      Ensemble() : EntryBase() {}
      Ensemble(af_string,af::double6,af_millnx,af_cmplex,af_double);
      void initialize_bin(Bin);
      bool in_memory() { return MILLER.size(); }
      void read_from_disk() { VanillaReadMtz(); }
      void write_to_disk() { VanillaWriteMtz(); }

    public:
      void addData(int,cmplex,double,int);
      void normalize();
      void VanillaWriteMtz();
      void VanillaReadMtz();
      void SetHistory(af_string);
      af_string get_history();
      double sphereOuter() { PHASER_ASSERT(SPHERE); return SPHERE; } //only set in decomposition
      af::double6 get_cell() { return UC.get_cell(); }
      const int nrefl() const { return MILLER.size(); }
      const double hires() const;
  };

} //phasertng
#endif
