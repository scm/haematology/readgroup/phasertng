#ifndef __phasertng_Suite_class__
#define __phasertng_Suite_class__
#include <phasertng/enum/out_stream.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/hoist.h>
#include <sstream>
#include <unordered_map>
#include <string>

namespace phasertng {

class Suite //don't derive from InputBase, this must be independent of all throws etc
            //first thing the program runs, must be very clean
{
  public:
  std::string suite;
  bool suite_running_mode_chk = false; //master switch for file write turned off
  struct suitestruct{
    out::stream  level;
    out::stream  store;
    bool         write_phil;
    bool         write_cards;
    bool         write_files;
    bool         write_data;
    bool         write_density;
    bool         write_log;
    bool         write_xml;
    bool         loggraphs;
    std::string  kill_file;
    double       kill_time;
    std::string  database;
  };
  suitestruct SUITE;
  bool called_from_executable = false;

  bool         Write() const;
  bool         Extra() const { return SUITE.level >= out::TESTING; }
  out::stream  Level() const { return SUITE.level; }
  out::stream  Store() const { return SUITE.store; }
  bool         WritePhil() const { return SUITE.write_phil and !suite_running_mode_chk; }
  bool         WriteCards() const { return SUITE.write_cards and !suite_running_mode_chk; }
  bool         WriteFiles() const { return SUITE.write_files and !suite_running_mode_chk; }
  bool         WriteData() const { return SUITE.write_data and !suite_running_mode_chk; }
  bool         WriteDensity() const { return SUITE.write_density and !suite_running_mode_chk; }
  bool         WriteLog() const { return SUITE.write_log and !suite_running_mode_chk; }
  bool         WriteXml() const { return SUITE.write_xml and !suite_running_mode_chk; }
  bool         Loggraphs() const { return SUITE.loggraphs and !suite_running_mode_chk; }
  std::string  KillFile() const { return SUITE.kill_file; }
  double       KillTime() const { return SUITE.kill_time; }
  std::string  DataBase() const { return SUITE.database; }

  void SetSilent();
  void SetLevel(const std::string);
  void SetStore(const std::string);
  void SetWritePhil(const bool);
  void SetWriteCards(const bool);
  void SetWriteFiles(const bool);
  void SetWriteData(const bool);
  void SetWriteDensity(const bool);
  void SetWriteLog(const bool);
  void SetWriteXml(const bool);
  void SetLoggraphs(const bool);
  void SetKillFile(const std::string);
  void SetKillTime(const double);
  void SetDataBase(const std::string);

  bool isLevel(out::stream level) const { return SUITE.level >= level; }

  Suite() {}
  Suite(std::string suite_);
  //see also Phasertng::set_user_input_for_suite()

  void check_store_level();
  void set_defaults_for_suite();

  out::stream String2Enum(std::string);
  std::string Enum2String(out::stream);
};

}
#endif
