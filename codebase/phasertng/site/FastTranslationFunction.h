//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_FastTranslationFunction_class__
#define __phasertng_FastTranslationFunction_class__
#include <phasertng/site/PointsStored.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <cctbx/translation_search/symmetry_flags.h>
#include <scitbx/array_family/accessors/c_grid_padded_periodic.h>
#include <utility>
#include <tuple>
#include <set>

namespace phasertng {

  class FileSystem; //forward declaration

    struct bestdata
    {
      double newmax = false;
      double f = std::numeric_limits<double>::lowest();
      double z = std::numeric_limits<double>::lowest();
      int paks = int('?'); //always set, not valid
    };


  class FastTranslationFunction : public PointsStored
  {
    //for ftf stores points wrt mt
    //for btf stores points wrt in
    private:
      const cmplex cmplxONE = cmplex(1.,0.);
      const cmplex TWOPII = cmplex(0.,scitbx::constants::two_pi);
      double ZTOTALSCAT;
      std::set<Identifier> USED_MODLID;
      std::set<Identifier> USED_MODLID_GYRE;
      std::map<Identifier,dvect3>  interpolate_pt;

    public: //fast clustering, constants for parallel section
      int    NTHREADS = 1;
      bool   USE_STRICTLY_NTHREADS = false;
      bool   negvar = false;

    private: //fast clustering, constants for parallel section
      std::vector<dmat33> rotsym_tr;
      double DIST2;
      dvect3 cellBoundMin;
      dvect3 cellBoundMax;

    public:
      cctbx::maptbx::grid_tags<long> tags;
      bool haveFpart;
      af::int3 gridding;
      af::versa<double, af::c_grid<3> > tsmap;
      double d_min;
      SpaceGroup SG = SpaceGroup("P 1");
      cctbx::translation_search::symmetry_flags sym_flags;

    public:
      double f_min = 0;
      double z_min = 0;
      bestdata* tf_max();
      bestdata* tz_max();
      std::map<SpaceGroup,bestdata> sg_f_max;
      std::map<SpaceGroup,bestdata> sg_z_max;
      int    num_peaks = 0;

    public:
       UnitCell UC;
       sv_bool SELECTED;
       const_EpsilonPtr EPSILON;

    public:
      FastTranslationFunction(const_ReflectionsPtr,sv_bool,const_EpsilonPtr);
      void setup(const_ReflectionsPtr,sv_bool,const_EpsilonPtr);
      void allocate_memory(const_ReflectionsPtr,const double, const bool);
      void setup_spacegroup(dag::Node*);

    private:
      std::tuple<af_millnx,af_cmplex>
      calculate_letf1_p1_turn_terms(const_ReflectionsPtr,dag::Node*);

      std::tuple<af_millnx,af_cmplex>
      calculate_letf1_p1_gyre_terms(const_ReflectionsPtr,dag::Node*);

      std::tuple<af_millnx,double,af_cmplex>
      calculate_ptf_p1_terms(const_ReflectionsPtr,dag::Node*);

      std::tuple<af_millnx,double,af_double,af_cmplex>
      calculate_sg_gyre_terms(const_ReflectionsPtr,dag::Node*);

      std::tuple<af_millnx,double,af_double,af_cmplex>
      calculate_sg_turn_terms(const_ReflectionsPtr,dag::Node*);

    public:
      void calculate_letf1(const_ReflectionsPtr,dag::Node*);
      void calculate_ptf(const_ReflectionsPtr,dag::Node*);
      std::pair<bool,int>  top_packs(char);

      af_double peak_search(af_double&,af_dvect3&);
      void apply_mean_cutoff(af_double&,af_dvect3&);
      void convert_heights_to_zscores(af_double);
      af_string apply_percent_cutoff(af_double,af_dvect3,double);
      af_string apply_percent_cutoff_with_packing(af_double,af_dvect3,double,af_char);
      af_string  log_peaks(int,af_double,af_dvect3,af_char);
      std::pair<af::tiny<bool, 3>,bool> continuous_shifts();
  //    af_double peak_search_continuous_shifts_and_store(double,dvect3,dvect3);
      void continuous_shifts_and_store(af_double,af_dvect3,dvect3,dvect3,af_char=af_char());
      void apply_partial_sort_and_maximum_stored_llg(const int);
      double apply_partial_sort_and_percent_fss(const double);
      void cluster(const double,int);
      void clustering_off();
      void statistics();
      void WriteMap(FileSystem);
      std::pair<bool,dvect3> coordinates(dvect3,af_dvect3,dvect3,dvect3,int&);

      af_string logSetup();
      af_string logTableHeader(int);
      af_string logFastTable(const int,const bool);
      af_string logJogTable(const int,const bool,const dvect3,dvect3,dvect3);
      af_string logTable(const int,const bool);
      af_string logSlowTable(const int,const bool);
      int  precision();
      int  width();

      int cluster_parallel(dvect3,int,int);
      int cluster_combine(dvect3,int,int);

  };

}//phasertng
#endif
