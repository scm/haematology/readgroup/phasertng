//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/PointsStored.h>
#include <scitbx/constants.h>
#include <algorithm>
#include <numeric>

namespace phasertng {

  bool
  PointsStored::at_end()
  { inext++; return inext != translation_function_data.end(); }

  void
  PointsStored::restart(unsigned seed)
  { inext = translation_function_data.begin(); } //next is increment value

  dvect3
  PointsStored::next_site()
  { return inext->grid; }

  std::size_t
  PointsStored::count_sites()
  { return translation_function_data.size(); }

  af_dvect3
  PointsStored::all_sites()
  {
    af_dvect3 tmp(translation_function_data.size());
    int i(0);
    for (auto& item : translation_function_data)
      tmp[i++] = item.grid;
    return tmp;
  }

  void
  PointsStored::partial_sort_and_erase(const int& maxstored)
  {
    int num = translation_function_data.size();
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (num <= maxstored)
    {
      std::sort(
        translation_function_data.begin(),
        translation_function_data.end(),
        cmp);
    }
    else
    {
      std::partial_sort(
        translation_function_data.begin(),
        translation_function_data.begin()+num,
        translation_function_data.end(),
        cmp);
      translation_function_data.erase(
        translation_function_data.begin()+num,
        translation_function_data.end());
    }
  }

  void
  PointsStored::full_sort()
  {
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    std::sort(
        translation_function_data.begin(),
        translation_function_data.end(),
        cmp);
  }

  void
  PointsStored::reverse()
  {
    std::reverse(
        translation_function_data.begin(),
        translation_function_data.end());
  }

  void
  PointsStored::select_topsites_and_erase()
  {
    std::vector<Site>::iterator iter = std::remove_if(
              translation_function_data.begin(), translation_function_data.end(),
              [](Site i){ return !i.is_topsite(); } );
    translation_function_data.erase( iter, translation_function_data.end() );
    for (int i = 0; i < translation_function_data.size(); i++)
      translation_function_data[i].clusterNum = i; //reset because old numbers not longer relevant
  }

  void
  PointsStored::mean_sigma()
  {
    std::vector<Site>& v = translation_function_data; //reference
    Site sum = std::accumulate(v.begin(), v.end(), Site(0.0));
    double mean = sum.value / v.size();
    sv_double diff(v.size());
    for (int i = 0; i < v.size(); i++) diff[i] = v[i].value - mean;
    double sq_sum(0);
    for (int i = 0; i < diff.size(); i++) sq_sum += diff[i]*diff[i] ;
    double stdev = std::sqrt(sq_sum / v.size());
    f_mean = mean;
    f_sigma = stdev;
    f_top = std::numeric_limits<double>::lowest();
    for (int i = 0; i < v.size(); i++) f_top = std::max(f_top,v[i].value);
    z_top = (f_top-f_mean)/f_sigma;
  }

  Loggraph
  PointsStored::logGraph(std::string description)
  {
    Loggraph loggraph;
    loggraph.title = "Top peaks " + description;
    loggraph.scatter = false;
    loggraph.graph.resize(1);
    loggraph.data_labels = "Peak_number  Score  Number_in_Cluster";
    int i(1);
    for (auto& item : translation_function_data)
    {
      if (item.is_topsite())
        loggraph.data_text += std::to_string(i++) + " " +
                              std::to_string(item.value) + " " +
                              std::to_string(item.ifTopNumInGroup) + " " +
                              "\n";
    }
    loggraph.graph[0] = ":N:1,2,3";
    return loggraph;
  }

  void
  PointsStored::fill_memory_points_on_disc(
      int num_pts
    )
  { //sunflower
    double z(0);
    for (int i = 0; i < num_pts; i++)
    {
      double indices = i + 0.5;
      double r = std::sqrt(indices/num_pts);
      double theta = scitbx::constants::pi * (1 + std::sqrt(5)) * indices;
      double x = r*std::cos(theta);
      double y = r*std::sin(theta);
      translation_function_data.push_back(Site(x,y,z));
    }
    int n(360); //number of points around circle
    for (int i = 0; i < n; i++)
    {
      double indices = 2*scitbx::constants::pi/n;
      double r = 1;
      double theta = scitbx::constants::pi * (1 + std::sqrt(5)) * indices;
      double x = r*std::cos(theta);
      double y = r*std::sin(theta);
      translation_function_data.push_back(Site(x,y,z));
    }
    num_pts = translation_function_data.size();
    sv_double vec(num_pts);
    for (int i = 0; i < num_pts; i++)
    {
      double mindist = std::numeric_limits<double>::max();
      double xi = translation_function_data[i].grid[0];
      double yi = translation_function_data[i].grid[1];
      for (int j = 0; j < num_pts; j++)
      {
        double xj = translation_function_data[j].grid[0];
        double yj = translation_function_data[j].grid[1];
        if (i != j)
          mindist = std::min(mindist,fn::pow2(xi-xj)+fn::pow2(yi-yj));
      }
      vec[i] = std::sqrt(mindist);
    }
    SAMP = std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
  }

  void
  PointsStored::fill_memory_points_on_sphere(
      int num_pts
    )
  {
    for (int i = 0; i < num_pts; i++)
    {
      double indices = i + 0.5;
      double phi = std::acos(1 - 2*indices/num_pts);
      double theta = scitbx::constants::pi * (1 + std::sqrt(5)) * indices;
      double x = std::cos(theta)*std::sin(phi);
      double y = std::sin(theta)*std::sin(phi);
      double z = std::cos(phi);
      translation_function_data.push_back(Site(x,y,z));
    }
    int n(360); //number of points around circle
    //add points right on the boundary so that the plots go to the edge
    for (int i = 0; i < n; i++)
    {
      double theta = i*2*scitbx::constants::pi/n;
      double r = 1;
      double x = r*std::cos(theta);
      double y = r*std::sin(theta);
      double z = 0;
      translation_function_data.push_back(Site(x,y,z));
    }
    num_pts = translation_function_data.size();
    //now we find the sampling, average spacing of points
    sv_double vec(num_pts);
    for (int i = 0; i < num_pts; i++)
    {
      double mindist = std::numeric_limits<double>::max();
      double xi = translation_function_data[i].grid[0];
      double yi = translation_function_data[i].grid[1];
      for (int j = 0; j < num_pts; j++)
      {
        double xj = translation_function_data[j].grid[0];
        double yj = translation_function_data[j].grid[1];
        if (i != j)
          mindist = std::min(mindist,fn::pow2(xi-xj)+fn::pow2(yi-yj));
      }
      vec[i] = std::sqrt(mindist);
    }
    SAMP = std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
  }

}// phasertng
