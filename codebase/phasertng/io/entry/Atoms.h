//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Atoms_class__
#define __phasertng_Atoms_class__
#include <phasertng/main/pointers.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/io/EntryBase.h>
#include <cctbx/sgtbx/site_symmetry_table.h>
#include <phasertng/pod/scatterer.h>
#include <set>
#include <phasertng/io/PdbHandler.h>

namespace phasertng {

  class Atoms : public EntryBase
  {
    public:
      Identifier MODLID;
      Identifier REFLID;
      std::string TIMESTAMP;
      //xray and xtra must be kept same length
      std::vector<pod::xray::scatterer>     XRAY;
      //vector for default deep copy
      //use af::shared copy function below for passing to cctbx library functions
      std::vector<pod::xtra::scatterer>     XTRA;
      //gradient and hessian are only resized if needed
      SpaceGroup SG;
      UnitCell UC;
      std::set<std::string> pdb_errors;

    protected:
      cctbx::sgtbx::site_symmetry_table site_sym_table;
      std::set<std::string> read_pdb_errors;

    public: //constructors
      //The unitcell is a base class because we must know the unit cell
      //for frac->orth and for u_star -> u_cart conversions for io etc
      Atoms() : EntryBase() {}
      Atoms(af_string pdb) : EntryBase() { SetPdb(pdb); }
      bool in_memory() { return bool(XRAY.size()); }
      void read_from_disk() { ReadPdb(); }
      void write_to_disk() { WritePdb(); }

    public: //getters
      const int size() const;
      af::shared<pod::xray::scatterer> scatterers_ref() const;

    public:
      void                        set_site_independent_params(int& a,scitbx::af::small<double,3>);
      scitbx::af::small<double,3> site_independent_params(int& a,cctbx::fractional<double>);
      scitbx::af::small<double,3> site_independent_gradients(int& a,cctbx::fractional<double>);
      scitbx::af::small<double,6> site_independent_curvatures(int& a,af_double);
      void                        set_adp_independent_params(int& a,scitbx::af::small<double,6>);
      scitbx::af::small<double,6> adp_independent_params(int& a,dmat6);
      scitbx::af::small<double,6> adp_independent_gradient(int& a,dmat6);
      af_double                   adp_independent_curvatures(int& a,af_double);

    public:
     af_string logChangeHand(std::string);
     af_string logOccupancyHistogram(std::string);
     af_string logAtoms(std::string) const;
     af_string logFracs(std::string) const;

    public:
      void       ReadPdb();
      void       WritePdb();
      void       SetPdb(af_string);
      af_string  GetPdb();
      void       GetPdbHandler(PdbHandler&);

    public:
      void       process(const_ScatterersPtr);
      af::shared<PdbRecord> Cards() const;
      af_dvect3  get_sites_as_array();
      void AddAtom(PdbRecord, pod::xtra::scatterer);
      void AddAtom(pod::xray::scatterer, pod::xtra::scatterer);
      std::set<std::string> set_of_atomtypes() const;
      int number_of_atomtypes() const;
      //prepares data for PdbHandler
      std::pair<double,double> average_occupancy_bfactor();
      bool is_duplicate(Atoms&,double,bool);
      void change_to_enantiomorph();
      void change_scatterers(std::string);
      bool has_read_errors() const { return pdb_errors.size() > 0; }
      std::string read_errors() const;

    public:
    // Overloaded operators
    bool operator==(const Atoms& other) const
    {
      //this is a stupid and simple test for unchanged substructure
      //only looks to see if the same scatterers are in the same order
      if (this->size() != other.size()) return false;
      {
        for (int a = 0; a < this->size(); a++)
        {
          if (this->XRAY[a].site != other.XRAY[a].site) return false;
          if (this->XRAY[a].occupancy != other.XRAY[a].occupancy) return false;
          if (this->XRAY[a].u_iso != other.XRAY[a].u_iso) return false;
          if (this->XRAY[a].u_star != other.XRAY[a].u_star) return false;
        }
      }
      return true;
    }
  };

} //phasertng
#endif
