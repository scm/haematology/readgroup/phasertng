//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
//#include <scitbx/stl/set_wrapper.h>
//#include <scitbx/stl/map_wrapper.h>
#include <scitbx/array_family/boost_python/c_grid_flex_conversions.h>
//#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost_adaptbx/std_pair_conversion.h>

#include <phasertng/site/AnglesStored.h>
#include <phasertng/site/PointsStored.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_phasertng_Sites()
{
//  using namespace scitbx::stl::boost_python;
  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  class_<AnglesStored>("AnglesStored")
    .enable_pickling()
    .def( init<>() )
    .def("at_end",&AnglesStored::at_end)
    .def("restart",&AnglesStored::restart)
    .def("next_site",&AnglesStored::next_site)
    .def("count_sites",&AnglesStored::count_sites)
    .def("all_sites",&AnglesStored::all_sites)
    .def("point_distance",&AnglesStored::point_distance)
    .def("fill_memory",&AnglesStored::fill_memory)
  ;

  class_<PointsStored>("PointsStored")
    .enable_pickling()
    .def( init<>() )
    .def("fill_memory_points_on_disc",&PointsStored::fill_memory_points_on_disc)
    .def("fill_memory_points_on_sphere",&PointsStored::fill_memory_points_on_sphere)
  ;

  class_<AnglesUnique>("AnglesUnique")
    .enable_pickling()
    .def( init<double>() )
    .def("at_end",&AnglesUnique::at_end)
    .def("restart",&AnglesUnique::restart)
    .def("next_site",&AnglesUnique::next_site)
    .def("count_sites",&AnglesUnique::count_sites)
    .def("all_sites",&AnglesUnique::all_sites)
    .def("point_distance",&AnglesUnique::point_distance)
    .def("sampling_for_approx_number",&AnglesUnique::sampling_for_approx_number)
    .def("set_sampling",&AnglesUnique::set_sampling)
  ;
}

} // namespace phasertng::<anonymous>

void init_phasertng_Sites()
{
  wrap_phasertng_Sites();
}

} // namespace boost_python
} // namespace phasertng
