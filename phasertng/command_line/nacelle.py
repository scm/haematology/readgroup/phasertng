# LIBTBX_SET_DISPATCHER_NAME phasertng.nacelle

from iotbx.cli_parser import run_program
from phasertng.programs import nacelle

if (__name__ == '__main__'):
  try:
    run_program(nacelle.Program)
  except Exception as e:
    print(str(e))
