//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Bin.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/Assert.h>


namespace phasertng {

  Bin::Bin(int m,int n,int w,af_double s) { setup(m,n,w,s,false,DEF_LORES); }

  //initialization functions
  void
  Bin::setup(int M,int N,int W,af_double TWOSTOL,bool do_recount,double hires)
  {
    int MINBINS = M;
    int MAXBINS = N;
    int WIDTH = W;
    int NREFL = TWOSTOL.size();
    SMAX = std::numeric_limits<double>::lowest();
    SMIN = std::numeric_limits<double>::max();
    for (int r = 0; r < NREFL; r++)
    {
      PHASER_ASSERT(TWOSTOL[r] != 0); //unit cell probably not set
      SMAX = std::max(TWOSTOL[r],SMAX);
      SMIN = std::min(TWOSTOL[r],SMIN);
    }
    SMAX = std::max(1./hires,SMAX); //extra limit
    PHASER_ASSERT(SMAX > (SMIN+DEF_PPM)); //there is some range here
    SMAX += DEF_PPM;//tolerance
    PHASER_ASSERT(NREFL);
    NUMBINS = (MINBINS == MAXBINS) ? MINBINS : std::max(MINBINS,std::min((NREFL/WIDTH),MAXBINS));
    PHASER_ASSERT(NUMBINS > 0);
   // double SDELTA = (SMAX-SMIN)/NUMBINS;
    FNSSHELL = (FnS(SMAX) - FnS(SMIN))/NUMBINS;
    SMAX = std::ceil(SMAX/DEF_PPM)*DEF_PPM; //max is the hires limit, ceil makes lower resolution
    SMIN = std::floor(SMIN/DEF_PPM)*DEF_PPM; //min is the lores limit, floor makes higher resolution
    FnS_SMIN  = FnS(SMIN);
   // round_value = std::ceil(FNSSHELL/DEF_PPM);
   // FNSSHELL = round_value*DEF_PPM;

    // --- Binning ---
    //This checks to see if any of the bins have no reflections
    //and makes sure there is at least one in every bin
    //This will override the NUM Cards option
    NUMINBIN.resize(NUMBINS);
    std::fill(NUMINBIN.begin(),NUMINBIN.end(),0);
    RBIN.resize(NREFL);
    PHASER_ASSERT(NUMBINS >= MINBINS);
    recount:
    if (NUMBINS >= MINBINS) //if MINBINS then accept however many in the bins
    {
      //count numbers in each bin
      for (int ibin = 0; ibin < NUMBINS; ibin++)
      {
        NUMINBIN[ibin] =  0;
      }
      for (int r = 0; r < NREFL; r++)
     // don't select or full resolution fails with empty bins
      {
        int rbin = get_bin_s(TWOSTOL[r]);
        PHASER_ASSERT(rbin < NUMBINS);
        NUMINBIN[rbin]++;
        RBIN[r] = rbin;
      }
      if (do_recount)
      {
        for (int ibin=0; ibin < NUMBINS; ibin++)
        {
          int miniumum_number_in_bin(10); //some semblance of reliable stats
          if (NUMINBIN[ibin] < miniumum_number_in_bin)
          {
          //reduce the number of bins by 1 and see if this helps
            NUMBINS--;
            NUMINBIN.resize(NUMBINS);
            std::fill(NUMINBIN.begin(),NUMINBIN.end(),0);
            //SDELTA = (SMAX-SMIN)/NUMBINS;
            FNSSHELL = (FnS(SMAX) - FnS(SMIN))/NUMBINS;
            //int round_value = std::floor(FNSSHELL/DEF_PPM);
            //FNSSHELL = round_value*DEF_PPM;
            goto recount;
          }
        }
      }
    }
/*
    for (int r = 0; r < NREFL; r++)
    { //paranoia check
      int rbin; bool error;
      std::tie(rbin,error) = get_bin_s_and_valid(TWOSTOL[r]);
      PHASER_ASSERT(error == 0);
    }
*/
    FnS_SMIN  = FnS(SMIN);
  }

  void
  Bin::store_ssqr_range(af_double twostol,int numbins)
  {
    int NREFL = twostol.size();
    SMAX = std::numeric_limits<double>::lowest();
    SMIN = std::numeric_limits<double>::max();
    for (int r = 0; r < NREFL; r++)
    {
      phaser_assert(twostol[r] != 0); //unit cell probably not set
      SMAX = std::max(twostol[r],SMAX);
      SMIN = std::min(twostol[r],SMIN);
    }
    phaser_assert(SMAX > (SMIN+DEF_PPM)); //there is some range here
    SMAX += DEF_PPM;//tolerance
    phaser_assert(numbins);
    NUMBINS = numbins;
    FNSSHELL = (FnS(SMAX) - FnS(SMIN))/NUMBINS;
    SMAX = std::ceil(SMAX/DEF_PPM)*DEF_PPM; //max is the hires limit, ceil makes lower resolution
    SMIN = std::floor(SMIN/DEF_PPM)*DEF_PPM; //min is the lores limit, floor makes higher resolution
    FnS_SMIN  = FnS(SMIN);
  }

  double
  Bin::LoRes(const int ibin) const
  { return 1/InvFnS(ibin*FNSSHELL + FnS_SMIN); }

  double
  Bin::HiRes(const int ibin) const
  { return 1/InvFnS((ibin+1)*FNSSHELL + FnS_SMIN); }

  bool
  Bin::ssqr_in_range(const double ssqr) const
  { return ssqr < fn::pow2(1./HiRes(NUMBINS-1)); }

  double
  Bin::MidRes(const int ibin) const
  {
  // the middle of the resolution range is the weighted mean
    double loS = 1/LoRes(ibin);
    double hiS = 1/HiRes(ibin);
    double loSsqr = fn::pow2(loS);
    double hiSsqr = fn::pow2(hiS);
    return 1/(std::sqrt((loSsqr+hiSsqr)/2));
  }

  double
  Bin::MidSsqr(const int ibin) const
  {
    double loS = 1/LoRes(ibin);
    double hiS = 1/HiRes(ibin);
    double loSsqr = fn::pow2(loS);
    double hiSsqr = fn::pow2(hiS);
    return (loSsqr+hiSsqr)/2.;
  }

  int
  Bin::get_bin_ssqr(const double ssqr) const
  {
    double s = std::sqrt(ssqr);
    return get_bin_s(s);
  }

  int
  Bin::get_bin_s(const double s) const
  {
    //int rbin; bool error;
    //if (error) std::cout << "s=" << s << " rbin=" << rbin << " v=" << error << std::endl;
    // PHASER_ASSERT(error == 0);
    if (s < SMIN) return 0; //even if correct there can be numerical problems at limit
    double drbin((FnS(s) - FnS_SMIN)/FNSSHELL);
    int rbin = std::floor(drbin); //explicit floor, rather than static cast
    if (rbin == NUMBINS and s < SMAX) return NUMBINS-1; //rescue where the math fails
    phaser_assert(FNSSHELL);
    phaser_assert((FnS(s) - FnS_SMIN) > 0);
    phaser_assert(rbin < NUMBINS);
    return rbin;
  }

  double
  Bin::get_midSsqr_ssqr(const double ssqr) const
  {
    double s = std::sqrt(ssqr);
    for (int ibin = 0; ibin < NUMBINS; ibin++)
    {
      double loS = 1/LoRes(ibin);
      double hiS = 1/HiRes(ibin);
      if (s > loS and s <= hiS)
      { //the function used in the original variance calculation
        return 1/fn::pow2(MidRes(ibin));
      }
    }
    phaser_assert(false);
    return 0;
  }

  std::pair<int, int>
  Bin::get_bin_s_and_valid(const double s) const
  {
    if (s < SMIN) return { 0, 1 };
    if (s > SMAX) return { NUMBINS-1, 2 };
    PHASER_ASSERT(FNSSHELL);
    double drbin((FnS(s) - FnS_SMIN)/FNSSHELL);
    int rbin = std::floor(drbin); //explicit floor, rather than static cast
    if (rbin < 0) return { 0, 3 };
    //below is a fallback for high resolution limit numerical instability
    //drbin will be just over NUMBINS in case below e.g. 6.000001 when it should be 5.999999
    if (rbin == NUMBINS and s < SMAX) return { NUMBINS-1, 4 }; //rescue where the math fails
    if (rbin >= NUMBINS) return { NUMBINS-1, 5 };
    return { rbin, 0 };
  }

  af_string
  Bin::logBin(const std::string description) const
  {
    af_string output;
    output.push_back(">>Binning: " + description + "<<");
    output.push_back("Numbins: " + std::to_string(NUMBINS));
    output.push_back("smax/smin: " + std::to_string(SMAX) + "/" + std::to_string(SMIN));
    output.push_back("hires/lores: " + std::to_string(hires()) + "/" + std::to_string(lores()));
    if (NUMBINS > 6)
    {
      for (int s = 0; s < 3; s++)
      {
        char buf[80];
        snprintf(buf,80,"%3d: resolution(hi/mid/lo)=%9.4f/%8.4f/%8.4f #%d",
           s+1,LoRes(s),MidRes(s),HiRes(s),(s < NUMINBIN.size()) ? NUMINBIN[s] : 0);
        output.push_back(std::string(buf));
      }
      output.push_back("--- etc");
      for (int s = NUMBINS-3; s < NUMBINS; s++)
      {
        char buf[80];
        snprintf(buf,80,"%3d: resolution(hi/mid/lo)=%9.4f/%8.4f/%8.4f #%d",
           s+1,LoRes(s),MidRes(s),HiRes(s),(s < NUMINBIN.size()) ? NUMINBIN[s] : 0);
        output.push_back(std::string(buf));
      }
    }
    else
    {
      for (int s = 0; s < NUMBINS; s++)
      {
        char buf[80];
        snprintf(buf,80,"%3d: resolution(hi/mid/lo)=%9.4f/%8.4f/%8.4f #%d",
           s+1,LoRes(s),MidRes(s),HiRes(s),(s < NUMINBIN.size()) ? NUMINBIN[s] : 0);
        output.push_back(std::string(buf));
      }
    }
    return output;
  }

  void
  Bin::parse(double a,double b,double c,int d)
  { SMIN = 1/a; SMAX = 1/b; FNSSHELL = 1/c; NUMBINS = d; }
  //numerically more stable if printed as the inverse

  std::string
  Bin::unparse() const
  {
    char buf[80];
    PHASER_ASSERT(FNSSHELL > 0);
    snprintf(buf,80,"%15.12f %15.12f %15.12f %d",
       lores(),hires(),1/FNSSHELL,NUMBINS);
    return std::string(buf);
  }

  int
  Bin::numbins() const
  { return NUMBINS; }

  void
  Bin::set_numinbin()
  {
    FnS_SMIN = FnS(SMIN);//hack
    NUMINBIN.resize(NUMBINS);
    std::fill(NUMINBIN.begin(),NUMINBIN.end(),0);
    for (int r = 0; r < RBIN.size(); r++)
      NUMINBIN[RBIN[r]]++;
  }

  sv_int
  Bin::wilson_bins(const double lores_for_wilson,const double hires_for_wilson) const
  {
    sv_int wilson_bin; //for lookup into the bin array, for plotting
    for (int s = 0; s < numbins(); s++)
    {
      if (MidRes(s) > lores_for_wilson) continue; //Wilson plot is nonsense at lores
      if (HiRes(s) < hires_for_wilson)
        break; //because the resolution may have been extended at the hires limit
      wilson_bin.push_back(s);
    }
    return wilson_bin;
  }

  void
  Bin::setup_rbin_with_ssqr(af_double ssqr)
  {
    RBIN.resize(ssqr.size());
    for (int r = 0; r < ssqr.size(); r++)
    {
      RBIN[r] = get_bin_ssqr(ssqr[r]);
    }
  }

}//phasertng
