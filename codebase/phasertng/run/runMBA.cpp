//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");

    //increment the tracker here so that we can use it for the reflid
    DAGDB.shift_tracker(hashing(),input.running_mode);

    if (input.value__path.at(".phasertng.model_as_map.filename.").is_default())
      throw Error(err::FILESET,"map filename");
    Map readmap;
    readmap.SetFileSystem(input.value__path.at(".phasertng.model_as_map.filename.").value_or_default());
    if (!readmap.file_exists()) //throws deep within vanilla read core dump
      throw Error(err::FILEOPEN,readmap.fstr());
    if (readmap.is_a_directory()) //throws deep within vanilla read core dump
      throw Error(err::FILEOPEN,readmap.fstr());
    auto io_cell_scale = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default();

    Identifier modlid;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Map Identifier");
    if (readmap.MODLID.is_set_valid())
    {
      logTab(where,"Map tag from model in memory");
      modlid = readmap.MODLID;
    }
    else
    {
      //modlid = DAGDB.NODES.front().TRACKER.ULTIMATE;
      //if tag is not input then tne default tag is the stem of the pdb file
      //(stem = the filename without the final extension)
      std::string tag;
      if (!input.value__string.at(".phasertng.model_as_map.tag.").is_default())
      {
        tag = input.value__string.at(".phasertng.model_as_map.tag.").value_or_default();
        logTab(where,"Map tag from input");
      }
      else
      {
        tag = readmap.stem();
        logTab(where,"Map tag from filename");
      }
      auto boxscale = dtos(input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").value_or_default());
      modlid.initialize_from_text(readmap.fstr()+boxscale+dvtos(io_cell_scale)); //full path filename
      modlid.initialize_tag(tag); //overwrites tag only
    }
    logTab(where,"Map identifier: " + modlid.str());
    phaser_assert(modlid.is_set_valid());
    }}
    readmap.MODLID = modlid;

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Map");
    if (!readmap.is_mtz() and !readmap.is_map())
      throw Error(err::INPUT,"Map format not recognised");
    logTab(where,"Map read from file: " + readmap.qfstrq());
    if (readmap.is_mtz()) logTab(where,"Map format mtz");
    if (readmap.is_map()) logTab(where,"Map format mrc");
    }}

    //if the map extends past the data resolution limit, then load the data slightly
    //past that for interpolation
    //if map resolution is less than data resolution, then will have to limit resolution
    //of map to allow for interpolation
    double config_hires;
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Configuration");
    logTab(where,"Read the input map/mtz");
    if (readmap.is_mtz())
    {
      auto txt = readmap.ReadMtz(
          input.value__string.at(".phasertng.model_as_map.fmap.").value_or_default(),
          input.value__string.at(".phasertng.model_as_map.phimap.").value_or_default(),
          input.value__boolean.at(".phasertng.model_as_map.no_phases.").value_or_default(),
          input.value__boolean.at(".phasertng.model_as_map.require_box.").value_or_default()
          );
      logTabArray(out::LOGFILE,txt);
    }
    else if (readmap.is_map())
    {
      logTab(where,"Read the map, but if it is a vastly oversampled (em) map, truncate to a resolution that is more sensible");
      bool write_files(Suite::Extra() and WriteFiles());
      auto txt = readmap.ReadMap(
          input.value__flt_number.at(".phasertng.model_as_map.oversample_resolution_limit.").value_or_default(),
          write_files,
          DataBase(),
          DAGDB.PATHWAY.ULTIMATE);
      logTabArray(out::LOGFILE,txt);
    }
    else
    {
      throw Error(err::INPUT,"Map format error");
    }

    //config_hires -= DEF_PPH; //tolerance for resolution limits, required
    logTab(where,"Map cell = " + readmap.UC.str());
    logTab(where,"Data resolution  = " + dtos(REFLECTIONS->data_hires(),5,2));
    //the map may be very highly oversampled
    logTab(where,"Map resolution = " + dtos(readmap.hires(),5,2));
    if (!input.value__flt_number.at(".phasertng.model_as_map.resolution.").is_default())
    {
      logTab(where,"User request to truncate the resolution of the map = true");
      double io_map_resolution = input.value__flt_number.at(".phasertng.model_as_map.resolution.").value_or_default();
      bool truncate(io_map_resolution > readmap.hires());
      logTab(where,"Map truncation resolution = " + dtos(io_map_resolution,5,2));
      logTab(where,"User resolution > map resolution = " + btos(truncate));
      if (truncate)
      {
        logEllipsisOpen(where,"Truncate reflections");
        readmap.truncate_to_resolution(io_map_resolution);
        logEllipsisShut(where);
      }
    }
    logTab(where,"Compare the resolution of the input map with the data resolution");
    logTab(where,"If it is higher, truncate, but allow padding for interpolation at the hires limit");
    logTab(where,"The padding has to take account of change in the cell scale");
    double trunkres = readmap.UC.extend_resolution(REFLECTIONS->data_hires(),io_cell_scale);
    logTab(where,"Map truncation resolution = " + dtos(trunkres,5,2));
    bool truncate(trunkres > readmap.available_hires(io_cell_scale));
    if (truncate)
    {
      logEllipsisOpen(where,"Truncate reflections");
      readmap.truncate_to_resolution(trunkres);
      logEllipsisShut(where);
    }
    logTab(where,"Sort reflections on cctbx default for miller indices: k first (0 1 0)");
    {
      logEllipsisOpen(where,"Sorting");
      readmap.sort_reflections();
      logEllipsisShut(where);
    }
    //if the data are to available_hires, then can only configure lower
    logTab(where,"Calculate the resolution to which the map can be used");
    logTab(where,"This is the resolution minus a buffer for interpolation at the edge");
    config_hires = readmap.available_hires(io_cell_scale);
    logTab(where,"Map configured for use to resolution = " + dtos(config_hires));
    }}

    //use the user defined modlid if it is given
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Identifier");
    logTab(where,"Map modlid: " + modlid.string());
    logTab(where,"Map tag: " + modlid.tag());
    phaser_assert(modlid.is_set_valid());
    }}

    auto search = DAGDB.add_entry(modlid); //create a new database entry
    logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
    search->HIRES = config_hires;
    search->ATOM = false;
    search->HELIX = false;
    search->MAP = true;
    logTab(out::VERBOSE,"Search resolution = " + dtos(search->HIRES));

   //use the user defined modlid if it is given
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Properties");
    bool selenomethionine(false);
    //use default water_inflation as 1.0 because this is the model, not the biological unit
    Sequence sequence(selenomethionine);
    //all the options can be used to add sequence
    if (input.value__path.at(".phasertng.model_as_map.sequence.filename.").is_default())
      throw Error(err::FATAL,"No map sequence");
    if (!input.value__path.at(".phasertng.model_as_map.sequence.filename.").is_default())
    {
      sequence.SetFileSystem(input.value__path.at(".phasertng.model_as_map.sequence.filename.").value_or_default());
      sequence.ReadSeq();
    }
    sequence.initialize(true,true);
    logTabArray(where,sequence.logSequence("Map"));
    search->VOLUME = sequence.volume();
    search->MEAN_RADIUS = sequence.minimum_radius_from_molecular_weight();
    search->EXTENT = dvect3(readmap.UC.A(),readmap.UC.B(),readmap.UC.C()); //keep it big,not tight on density
    //search->EXTENT = readmap.UC.GetBox();
    search->SCATTERING = sequence.total_scattering();
    search->MOLECULAR_WEIGHT = sequence.total_molecular_weight();
    PHASER_ASSERT(search->MOLECULAR_WEIGHT);
    logTab(where,"Scattering = " + itos(sequence.total_scattering()));
    logTab(where,"Molecular Weight = " + dtos(sequence.total_molecular_weight()));
    logTab(where,"Mean radius: " + dtos(search->MEAN_RADIUS));
    logTab(where,"extent: " + dvtos(search->EXTENT));
    logTab(where,"Volume: " + dtos(search->VOLUME));
    //set Entry values
    }}

   // for (auto loop : {1,2}) //for checking scaling has "converged"
   // if the above is uncommented, the scale and b in round 2 should be 1 and 0
    double relative_scaling_b_f = 0;
    double relative_scaling_k_f = 1;
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Data to Map Scale Calculation");
    logTab(where,"Data to Map B-factor calculated by linear regression");
    double min_hires_for_scaling(4);  //data must extend to at least this resolution
    double lores_for_scaling(5); // //Scaling plot is nonsense at lores
    int    min_bins_for_scaling(5); //3 in the middle (linear) + 2 unreliable end points
    bool data_to_hires_for_scaling(REFLECTIONS->data_hires() < min_hires_for_scaling);
    if (!data_to_hires_for_scaling)
      logTab(where,"Data does not extend beyond " + dtos(min_hires_for_scaling,3) + " Angstroms");
    double match_resolution = std::max(readmap.hires(),REFLECTIONS->data_hires());
    logTab(where,"Data to Map Scaling Resolution " + dtos(match_resolution));
    logTab(where,"Map  High Resolution = " + dtos(readmap.hires()));
    logTab(where,"Data High Resolution = " + dtos(REFLECTIONS->data_hires()));
    if (lores_for_scaling < match_resolution)
    {
      logTab(where,"Remove lores for scaling since match resolution is lower than default");
      lores_for_scaling = DEF_LORES;
    }
    //continue even if data not present, since it is the best we can do
    if (match_resolution > min_hires_for_scaling)
      logWarning("Resolution for Data to Map Scaling lower than recommended (" + dtos(match_resolution) + ")");
    af_double twostol;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const double reso = REFLECTIONS->work_reso(r);
      const double ssqr = REFLECTIONS->work_ssqr(r);
      if (reso > match_resolution and reso < lores_for_scaling)
      {
        double rtwostol = cctbx::uctbx::d_star_sq_as_two_stol(ssqr);
        twostol.push_back(rtwostol);
      }
    }
    logTab(where,"Number of reflections for binning " + itos(twostol.size()));
    if (!twostol.size())
    {
      logTab(where,"Get desperate and use all reflections");
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        twostol.push_back(readmap.UC.cctbxUC.two_stol(readmap.MILLER[r]));
      }
      logTab(where,"Adjusted number of reflections for binning " + itos(twostol.size()));
    }
    Bin scaling_bin;
    bool do_recount(true); //AJM not done in phaser!!
    scaling_bin.setup(
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_min(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_max(),
        input.value__int_number.at(".phasertng.bins.maps.width.").value_or_default(),
        twostol,
        do_recount,
        DEF_LORES);
    if (Suite::Extra())
      logTabArray(out::TESTING,scaling_bin.logBin("scaling bin based on reflection data"));
    if (scaling_bin.numbins() >= min_bins_for_scaling)
    {
      af_double SigmaN; af_int    numinbin_data;
      std::tie(SigmaN,numinbin_data) = REFLECTIONS->sigman_with_binning(scaling_bin);
      logTab(where,"Calculate SigmaP");
      af_double SigmaP = readmap.sigmap_with_binning(scaling_bin);
      phaser_assert(SigmaP.size() == SigmaN.size());
      af_double ratio(scaling_bin.numbins());
      af_double ssqr_bin(scaling_bin.numbins());
      for (int s = 0; s < scaling_bin.numbins(); s++)
        ssqr_bin[s] = (1.0/fn::pow2(scaling_bin.MidRes(s)));
      for (int s = 0; s < scaling_bin.numbins(); s++)
        ratio[s] = SigmaN[s]/SigmaP[s];
      double slope,intercept;
      std::tie(slope,intercept) = wilson_slope_and_intercept(ratio,ssqr_bin);
      double ScalingB_intensity = -4*slope;
      relative_scaling_k_f = std::sqrt(std::exp(intercept));
      relative_scaling_b_f = ScalingB_intensity/2.0; //local copy for output
      logTab(where,"Slope,intercept = " + dtos(slope) + "," + dtos(intercept));
      logTab(where,"Relative Scaling B = " + dtos(relative_scaling_b_f));
      logTab(where,"Relative Scaling K = " + dtos(relative_scaling_k_f));
      {{
      Loggraph loggraph;
      loggraph.title = "Scaling Plot for Map to Data";
      loggraph.scatter = false;
      loggraph.graph.resize(3);
      loggraph.data_labels = "1/d^2 d Mean-Imap Mean-Iobs Ratio";
      for (int ibin = 0; ibin < scaling_bin.numbins(); ibin++)
      {
        if (scaling_bin.NUMINBIN[ibin])
        {
          double midres = scaling_bin.MidRes(ibin);
           loggraph.data_text += dtos(1.0/fn::pow2(midres),5,4) + " " +
                                  dtos(midres,5,4) + " " +
                                  dtos(SigmaP[ibin],15,8) + " " +
                                  dtos(SigmaN[ibin],15,8) + " " +
                                  dtos(ratio[ibin],15,8) + " " +
                                  "\n";
        }
      }
      loggraph.graph[0] = "Average Intensity of Map vs Resolution:AUTO:1,3";
      loggraph.graph[1] = "Average Intensity of Data vs Resolution:AUTO:1,4";
      loggraph.graph[2] = "Ratio of Map and Data Intensity vs Resolution:AUTO:1,5";
      logGraph(where,loggraph);
      }}
      //after this the fcalcs match the data and are not evalues
    }
    else
    {
      logWarning("Too few bins of data for Scaling (" + itos(scaling_bin.numbins()) + ")");
    }
    }}

    //scale the map before the trace just to make sure the numbers are sensible
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Data to Map Scaling");
    logTab(where,"Relative Scaling B = " + dtos(relative_scaling_b_f));
    logTab(where,"Relative Scaling K = " + dtos(relative_scaling_k_f));
    //so that the numerical limits are about the same as the input data
    bool bscale = true;
    if (bscale)
    {
      for (int r = 0; r < readmap.MILLER.size(); r++)
      {
        double ssqr = readmap.UC.cctbxUC.d_star_sq(readmap.MILLER[r]);
        readmap.FCALC[r] *= std::exp(-ssqr*relative_scaling_b_f/4.)*relative_scaling_k_f;
      }
      logTab(where,"Relative Wilson scaling applied");
    }
    else
    {
      logTab(where,"Relative Wilson scaling NOT applied");
    }
    }}

    logHistogram(out::LOGFILE,readmap.density({1,1},false),20,true,"Map Density");

    Trace trace;
    dvect3 centre(0,0,0); //this will be the PhaseShift for the molecular transform, user or trace value
    if (input.value__boolean.at(".phasertng.model_as_map.require_box.").value_or_default())
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Map Centre");
      logTab(where,"Find the centre of the map, either as user input of from doing a trace");
      //double wang_volume_factor = 1.1;
      dvect3 start_centre(0,0,0);
      if (!input.value__flt_vector.at(".phasertng.model_as_map.centre.").is_default())
        start_centre = input.value__flt_vector.at(".phasertng.model_as_map.centre.").value_or_default();
      std::tie(trace.SAMPLING_DISTANCE,trace.statistics) = readmap.wang_trace(1.1,search->VOLUME,start_centre,trace.PDB);
      //trace may be distributed around the unit cell, corners or centre
      //trace is on the square grid of the map, same sampling as the original map
      trace.set_principal_statistics();
      dvect3 origin(0,0,0);
      dvect3 halfhalfhalf(readmap.UC.A()/2,readmap.UC.B()/2,readmap.UC.C()/2);
      sv_dvect3 possible_centres = { origin, halfhalfhalf };
      //the possible centres are the seed points for the distribution of trace points
      //the map centres in the databases are only ever have the interesting density
      //in the middle of the map as provided or at the origin, which might be the corner
      auto loginfo = trace.find_and_move_centre(readmap.UC,possible_centres);
      logTabArray(where,loginfo);
      logTab(where,"Trace Centre: " + dvtos(trace.statistics.CENTRE));
      logTab(where,"Trace Minbox: " + dvtos(trace.statistics.MINBOX));
      logTab(where,"Trace Maxbox: " + dvtos(trace.statistics.MAXBOX));
      if (input.value__flt_vector.at(".phasertng.model_as_map.centre.").is_default())
      {
        centre = trace.statistics.CENTRE;
      }
      else
      {
        centre = input.value__flt_vector.at(".phasertng.model_as_map.centre.").value_or_default();
        logTab(where,"Input centre: " + dvtos(centre));
      }
      //this centre is the centre of the trace points, not one of the possible centres
      logTab(where,"Centre Used:  " + dvtos(centre));
      if (Suite::Extra())
      {
        trace.MAX_RADIUS = trace.statistics.MAX_RADIUS;
        trace.MODLID = search->identify();
        trace.REFLID = REFLECTIONS->reflid();
        trace.TIMESTAMP = TimeStamp();
        auto& PDB1 = trace.PDB;
        dvect3 minbox = trace.statistics.MINBOX; //add 10A frame
        dvect3 maxbox = trace.statistics.MAXBOX; //add 10A frame
        PdbRecord next;
        next.X = centre; //this is the input centre
        next.set_element("I"); //this is the input centre
        next.AtomName = " I  "; //this is the input centre
        next.Chain = "B";
        PDB1.push_back(next);
        next.X = trace.statistics.CENTRE; //this is the trace centre
        next.Chain = "U"; //this is the trace centre
        next.Chain = "B"; //this is the trace centre
        PDB1.push_back(next);
        next.X = { minbox[0],minbox[1],minbox[2] }; //shifted
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { minbox[0],minbox[1],maxbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { minbox[0],maxbox[1],minbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { maxbox[0],minbox[1],minbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { minbox[0],maxbox[1],maxbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { maxbox[0],maxbox[1],minbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { maxbox[0],minbox[1],maxbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        next.X = { maxbox[0],maxbox[1],maxbox[2] };
        next.Chain = "B"; //this is the original centre
        PDB1.push_back(next);
        trace.renumber();
        trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.trace_centre.pdb"));
        logFileWritten(out::TESTING,WriteFiles(),"Debug Trace",trace);
        if (WriteFiles()) trace.write_to_disk();
      }
      for (int i = 0 ; i < 3; i++)
        if (centre[i] <= trace.statistics.MINBOX[i] or centre[i] >= trace.statistics.MAXBOX[i])
          throw Error(err::INPUT,"Centre outside Trace box");
    }

    double solvent_constant(0);
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Solvent Constant");
    double buffer(10);
#if 0
    dvect3 solvent,protein; //percent.mean.stddev
    std::tie(solvent,protein) = readmap.solvent_constant(
        trace.get_atom_list(),trace.SAMPLING_DISTANCE+buffer);
    if (solvent[0] == 0)
      logWarning("Map does not have identified solvent region");
    if (solvent[0] > 0 and solvent[2] > DEF_PPH)
      logWarning("Map does not have flat solvent region");
    logTab(where,"Solvent region is the volume outside the trace");
    logTab(where,"Buffer Distance (A):   " + dtos(buffer));
    logTab(where,"Percent Solvent:       " + dtos(solvent[0]));
    logTab(where,"Average Solvent Value: " + dtos(solvent[1]));
    logTab(where,"Std-Dev Solvent Value: " + dtos(solvent[2]));
    logTab(where,"Percent Protein:       " + dtos(protein[0]));
    logTab(where,"Average Protein Value: " + dtos(protein[1]));
    logTab(where,"Std-Dev Protein Value: " + dtos(protein[2]));
    logBlank(where);
#endif
    auto iosc = input.value__flt_number.at(".phasertng.model_as_map.solvent_constant.").value_or_default();
    logTab(where,"Solvent Constant = " + dtos(iosc));
    }}

    //copy the data to the fcalcs, which will immediately be phase shifted for mt
    ModelsFcalc& fcalcs = search->FCALCS;
    {{
    //fcalcs.REFLID = REFLECTIONS->reflid();
    fcalcs.MODLID = search->identify();
    fcalcs.TIMESTAMP = TimeStamp();
    fcalcs.MILLER = readmap.MILLER;
    fcalcs.UC = readmap.UC;
    fcalcs.NMODELS = 1;
    fcalcs.FCALC_HIRES = readmap.hires();
    fcalcs.FCALC.resize(1,readmap.MILLER.size());
    for (int r = 0; r < readmap.MILLER.size(); r++)
    {
      fcalcs.FCALC(0,r) = readmap.FCALC[r];
    }
    fcalcs.setup_bins_with_statistically_weighted_bin_width(
        readmap.UC.GetBox(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_min(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_max(),
        input.value__int_number.at(".phasertng.bins.maps.width.").value_or_default()
        );
    logTabArray(out::VERBOSE,fcalcs.BIN.logBin("fcalc"));
    }}

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Phase Shift");
    dvect3 frac = -fcalcs.UC.fractionalization_matrix()*centre;
    logTab(where,"fractional shift: " + dvtos(frac));
    cmplex two_pii(cmplex(0,2.)*scitbx::constants::pi);
    for (int r = 0; r < fcalcs.MILLER.size(); r++)
    {
      //apply phase shift to centre density on origin for molecular transform interpolation
      const millnx miller = fcalcs.MILLER[r];
      cmplex PhaseShift = std::exp(two_pii*(frac*miller));
      fcalcs.FCALC(0,r) *= PhaseShift;
    }
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      fcalcs.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.map_shifted_to_origin.mtz"));
      logFileWritten(out::LOGFILE,WriteFiles(),"Debug Structure Factor",fcalcs);
      if (WriteFiles()) fcalcs.write_to_disk();
    }
    }}

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Anisotropy Refinement");
    bool refine_off =
        input.value__choice.at(".phasertng.macbox.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macbox.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macbox.macrocycle3.").value_or_default_equals("off");
    refine_off = refine_off or
        input.value__choice.at(".phasertng.macbox.protocol.").value_or_default_equals("off");
    ReflRowsAnom reflrowsanom;
    {{
    ReflColsMap tmprefl;
    tmprefl.setup_miller(fcalcs.FCALC.size());
    tmprefl.setup_mtzcol(type::mtzcol('F',"FMAP",labin::FNAT));
    tmprefl.setup_mtzcol(type::mtzcol('Q',"SIGFMAP",labin::SIGFNAT));
    tmprefl.UC = fcalcs.UC;
    tmprefl.SG = SpaceGroup("P 1"); //default spacegroup
    //all other header records default, including totalscat 0
    for (int r = 0; r < fcalcs.FCALC.size(); r++)
    {
      //labin::INAT can be zero and that is OK
      //delete this reflection from the dataset, permanently
      tmprefl.set_miller(fcalcs.MILLER[r],r);
      //convert from complex to real here
      const int nmodel(0);
      tmprefl.set_flt(labin::FNAT,std::abs(fcalcs.FCALC(nmodel,r)),r);
      tmprefl.set_flt(labin::SIGFNAT,0.,r);
      tmprefl.set_present(friedel::NAT,true,r);
    }
    tmprefl.setup_mtzcol(type::mtzcol('J',"IMAP",labin::INAT));
    tmprefl.setup_mtzcol(type::mtzcol('Q',"SIGIMAP",labin::SIGINAT));
    tmprefl.reverse_not_french_wilson_IfromF(friedel::NAT);
    tmprefl.INTENSITIES = false;
    logTab(where,"Add binning");
    Bin bin = tmprefl.setup_bin(type::mtzcol('I',"MAPBIN",labin::BIN),
      input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_min(),
      input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_max(),
      input.value__int_number.at(".phasertng.bins.maps.width.").value_or_default());
    logTabArray(where,bin.logBin("Map anisotropy"));
    tmprefl.SHARPENING = 0;
    tmprefl.OVERSAMPLING = 1;
    logTab(where,"Add cols for extra data");
    logTab(where,"Null hypothesis");
    tmprefl.setup_resn_anisobeta(
       type::mtzcol('R',"RESN",labin::RESN),
       type::mtzcol('R',"ANISOBETA",labin::ANISOBETA));
    logTab(where,"Add aniso col");
    tmprefl.MAP = true;
    reflrowsanom.copy_and_reformat_data(&tmprefl);
    }}
    {{
    out::stream where = out::VERBOSE;
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(where,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(where,seltxt);

    SigmaN sigman = reflrowsanom.CalculateSigmaN();
    RefineANISO refa(
        &reflrowsanom,
        &selected,
        &sigman);
    refa.BEST_CURVE_RESTRAINT = input.value__boolean.at(".phasertng.macbox.best_curve_restraint.").value_or_default();
    refa.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);

    std::vector<sv_string> macro = {
        input.value__choice.at(".phasertng.macbox.macrocycle1.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macbox.macrocycle2.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macbox.macrocycle3.").value_or_default_multi()
      };
    std::vector<int> ncyc = {
      input.value__int_vector.at(".phasertng.macbox.ncyc.").value_or_default(0),
      input.value__int_vector.at(".phasertng.macbox.ncyc.").value_or_default(1),
      input.value__int_vector.at(".phasertng.macbox.ncyc.").value_or_default(2)
    };
    auto method =  input.value__choice.at(".phasertng.macbox.minimizer.").value_or_default();
    logProtocol(where,macro,ncyc,method);

    dtmin::Minimizer minimizer(this);
    minimizer.run(
        refa,
        macro,
        ncyc,
        method,
        input.value__boolean.at(".phasertng.macbox.study_parameters.").value_or_default()
        );
    (refine_off) ?
      logTab(where,"Anisotropic normalization NOT applied to map structure factors"):
      logTab(where,"Apply anisotropic normalization to map structure factors");
    //apply WilsonB
    phaser_assert(fcalcs.FCALC.size() == fcalcs.MILLER.size());
    for (int r = 0; r < fcalcs.MILLER.size(); r++)
    {
      double root_epsnSigmaN = refa.resn(r);
      fcalcs.FCALC(0,r) /= root_epsnSigmaN;
      //here, FCALCS become normalized Ecalcs
      if (refine_off) //now take out the anisotropy component if not required
      {
        const millnx miller = fcalcs.MILLER[r];
        bool RemoveIsoB(false); //AJM as for previous code, ??
        fcalcs.FCALC(0,r) *= refa.aniso(r,RemoveIsoB); // store the correction as I/ANISO;
      }
    }
    //log the really interesting results to the logfile
    logUnderLine(out::LOGFILE,"Anisotropy Scaling");
    logTabArray(out::LOGFILE,refa.SIGMAN->logScaling(""));
    }}
    }}

    //before the shift
    search->PRINCIPAL_TRANSLATION = -centre;
    search->CENTRE = centre;
    logTab(out::VERBOSE,"principal translation: " + dvtos(search->PRINCIPAL_TRANSLATION));
    logTab(out::VERBOSE,"centre: " + dvtos(search->CENTRE));

    dvect3 minbox,maxbox;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Coordinates for Boxing");
    double buffer(10);
    dvect3 padding(buffer,buffer,buffer);
    minbox = trace.statistics.MINBOX-padding-centre; //add 10A frame
    maxbox = trace.statistics.MAXBOX+padding-centre; //add 10A frame
    logTab(where,"Non-solvent region determined by trace");
    logTab(where,"Buffer : " + dvtos(padding));
    logTab(where,"Minbox : " + dvtos(minbox));
    logTab(where,"Maxbox : " + dvtos(maxbox));
    logTab(where,"Range  : " + dvtos(maxbox-minbox));
    logBlank(where);
    }}

    Models& models = search->MODELS;
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Model");
    logTab(where,"Generate fake Model-type database entry containting some points in corners of non-solvent region which can be viewed with the map in coot");
    models.REFLID = REFLECTIONS->reflid();
    models.MODLID = search->identify();
    models.TIMESTAMP = TimeStamp();
    models.RELATIVE_WILSON_B.resize(1); //local copy for output
    models.RELATIVE_WILSON_B[0] = 0;//WilsonB_intensity/2.0; //local copy for output
    models.PDB.resize(1);
    auto& PDB1 = models.PDB[0];
    PDB1.resize(9);
    PDB1[0].X = centre; //this is the original centre
    PDB1[1].X = { minbox[0],minbox[1],minbox[2] }; //shifted
    PDB1[2].X = { minbox[0],minbox[1],maxbox[2] };
    PDB1[3].X = { minbox[0],maxbox[1],minbox[2] };
    PDB1[4].X = { maxbox[0],minbox[1],minbox[2] };
    PDB1[5].X = { minbox[0],maxbox[1],maxbox[2] };
    PDB1[6].X = { maxbox[0],maxbox[1],minbox[2] };
    PDB1[7].X = { maxbox[0],minbox[1],maxbox[2] };
    PDB1[8].X = { maxbox[0],maxbox[1],maxbox[2] };
    models.renumber();
    }}

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Vrms Estimation");
    //try reading the rms serial input
    models.SERIAL.resize(1);
    if (!models.SERIAL.add_rms( 0,
         input.value__flt_number.at(".phasertng.model_as_map.vrms_estimate.").value_or_default()))
    {
      throw Error(err::FATAL,"Map RMS limits incompatible");
    }
    models.SERIAL.vrms_start[0] = input.value__flt_number.at(".phasertng.model_as_map.vrms_estimate.").value_or_default(); //ECA mode in one line for one model
    double best_rms(0);
    double worst_rms(0);
    {{
      double best_factor = 10;
      best_rms = config_hires/best_factor; // Lowest rms that would make a difference given resolution
      //The mean radius here comes from the sequence molecular weight, not the trace or map
      PHASER_ASSERT(search->MEAN_RADIUS > 0);
      double worst_factor = 6;
      worst_rms = search->MEAN_RADIUS/worst_factor; // Highest rms that might be sensible for size of model
    }}
    if (best_rms > worst_rms)
    throw Error(err::FATAL,"Map RMS limits incompatible: best (" + dtos(best_rms) + ") > (" + dtos(worst_rms) + ") worst");
    //all stats calculated from best
    models.SERIAL.vrms_lower[0] = best_rms;
    models.SERIAL.vrms_upper[0] = worst_rms;
    models.SERIAL.setup_drms_first_and_limits();
    logTab(where,"DRMS lower/upper: " + dtos(models.SERIAL.drms_lower(),8,6) + " / " + dtos(models.SERIAL.drms_upper(),8,6));
    logTab(where,"VRMS input value: " + dvtos(models.SERIAL.vrms_input,5,3));
    logTab(where,"VRMS start value: " + dvtos(models.SERIAL.vrms_start,5,3));
    logTab(where,"VRMS upper limit: " + dvtos(models.SERIAL.vrms_upper,5,3));
    logTab(where,"VRMS lower limit: " + dvtos(models.SERIAL.vrms_lower,5,3));
    logBlank(where);
    if (!models.SERIAL.is_serial())
    throw Error(err::INPUT,"Map vrms estimations not set");
    //no further adjustments for map in the density calculation below
    search->DRMS_MIN = models.SERIAL.drms_lower();
    search->DRMS_MAX = models.SERIAL.drms_upper();
    search->VRMS_START = models.SERIAL.vrms_start;
    }}

    //add this new entry to the list of entries in the database by generating a "hold" style node
    {{
      //add the models info to each dag entry
      dag::Hold hold;
      hold.IDENTIFIER = search->identify();
      hold.ENTRY = search;
      dag::Node& node = DAGDB.NODES.front();
      node.HOLD.push_back(hold);
      Identifier modlid = search->identify();
      node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0); //create
      node.VRMS_VALUE[modlid] = dag::node_t(modlid,search->VRMS_START[0]); //create
      node.CELL_SCALE[modlid] = dag::node_t(modlid,1); //create
    }}

    //---
    input.value__uuid_number.at(".phasertng.search.id.").set_value(modlid.identifier());
    input.value__string.at(".phasertng.search.tag.").set_value(modlid.tag());
    input.value__flt_vector.at(".phasertng.model_as_map.extent_minimum.").set_value(minbox);
    input.value__flt_vector.at(".phasertng.model_as_map.extent_maximum.").set_value(maxbox);
