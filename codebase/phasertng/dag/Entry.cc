//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/Entry.h>
#include <scitbx/math/euler_angles.h>

namespace phasertng {
namespace dag {

  std::set<entry::data>
  Entry::get_data_choice_selection() const
  {
    std::set<entry::data> choices; //convert to enumeration
    for (auto s : DATA)
    {
      std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
      choices.insert(entry::data2Enum(s));
    }
    //these are the new ones that have been added
    if (MODELS.filesystem_is_set())
      choices.insert(entry::MODELS_PDB);
    if (FCALCS.filesystem_is_set())
      choices.insert(entry::FCALCS_MTZ);
    if (IMODELS.filesystem_is_set())
      choices.insert(entry::IMODELS_PDB);
    if (IFCALCS.filesystem_is_set())
      choices.insert(entry::IFCALCS_MTZ);
    if (INTERPOLATION.filesystem_is_set())
      choices.insert(entry::INTERPOLATION_MTZ);
    if (DECOMPOSITION.filesystem_is_set())
      choices.insert(entry::DECOMPOSITION_MTZ);
    if (VARIANCE.filesystem_is_set())
      choices.insert(entry::VARIANCE_MTZ);
    if (TRACE.filesystem_is_set())
      choices.insert(entry::TRACE_PDB);
    if (MONOSTRUCTURE.filesystem_is_set())
      choices.insert(entry::MONOSTRUCTURE_PDB);
    if (SUBSTRUCTURE.filesystem_is_set())
      choices.insert(entry::SUBSTRUCTURE_PDB);
    if (COORDINATES.filesystem_is_set())
      choices.insert(entry::COORDINATES_PDB);
    if (DENSITY.filesystem_is_set())
      choices.insert(entry::DENSITY_MTZ);
    if (GRADIENT.filesystem_is_set())
      choices.insert(entry::GRADIENT_MTZ);
    return choices;
  }

  std::string
  Entry::get_data_choice_selection_str() const
  {
    auto choices = get_data_choice_selection();
    std::string selection;
    for (auto item : choices)
    {
      std::string s = entry::data2String(item) + " ";
      std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return tolower(c); });
      selection += s;
    }
    if (selection.size() >= 1) selection.pop_back(); //final space
    if (!choices.size()) selection = "\"None\"";
    return selection;
  }

  std::string
  Entry::get_data_choice_selection_int() const
  {
    auto choices = get_data_choice_selection();
    std::string selection;
    for (auto item : choices)
    {
      selection += std::to_string(item) + " ";
    }
    if (selection.size() >= 1) selection.pop_back(); //final space
    if (!choices.size()) selection = "\"None\"";
    return selection;
  }

  void
  Entry::set_data_choice_selection(std::string s)
  {
    //if not recognised, eg None, then enum will be 0 == UNDEFINED
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
    entry::data tmp = entry::data2Enum(s);
    if (tmp != entry::UNDEFINED)
      DATA.insert(entry::data2String(tmp));
  }

  void
  Entry::set_data_choice_selection_int(std::vector<int> s)
  {
    for (auto i : s)
      DATA.insert(entry::data2String(static_cast<entry::data>(i)));
  }

  std::pair<double,double>
  Entry::clustang(
      double hires,
      double io_sampling,
      bool io_coiledcoil,
      bool dupl_only,
      double helixfactor,
      int highorderaxis)
  {
    double box_diagonal = std::sqrt(EXTENT*EXTENT);
    bool finesamp = (HELIX and io_coiledcoil);
    double radius = finesamp ? box_diagonal : MEAN_RADIUS;
    return EntryHelper::clustang(
      hires,
      io_sampling,
      helixfactor,
      highorderaxis,
      radius,
      finesamp,
      dupl_only);
  }

  double
  Entry::sphereOuter(double decomposition_radius_factor,double config_hires)
  {
    double max_vector = decomposition_radius_factor*MEAN_RADIUS; ////mean radius is 90% atoms
    return EntryHelper::sphereOuter(max_vector,config_hires);
  }

  int
  Entry::calculate_lmax(double radius,double hires)
  {
    return EntryHelper::calculate_lmax(radius,hires);
  }

  af_string
  Entry::logEntry() const
  {
    af_string output;
    double Da = MOLECULAR_WEIGHT;
    double kDa = Da/1000.;
    output.push_back(std::string("Entry: (" + DOGTAG.str() + ")"));
    output.push_back(snprintftos(
        "drms-range[%+7.3f:%+7.3f] vrms-start[%s]",
        DRMS_MIN,DRMS_MAX,
        dvtos(VRMS_START,6,3).c_str()
        ));
    output.push_back(snprintftos(
        "molecular-weight[%s%s] scattering[%se] mean-radius[%sA] point-group[%s]",
        (kDa>10) ? dtos(kDa,3).c_str() : dtos(Da,3).c_str(),
        (kDa>10) ? "kDa":"Da",
        itos(SCATTERING).c_str(),
        dtos(MEAN_RADIUS,1).c_str(),
        POINT_GROUP_SYMBOL.size() ? POINT_GROUP_SYMBOL.c_str() : "0"
        ));
    return output;
  }

  void
  Entry::copy_header(Entry& other)
  {
    ATOM = other.ATOM;
    HELIX = other.HELIX;
    MAP = other.MAP;
    SCATTERING = other.SCATTERING;
    MOLECULAR_WEIGHT = other.MOLECULAR_WEIGHT;
    PRINCIPAL_TRANSLATION = other.PRINCIPAL_TRANSLATION;
    PRINCIPAL_ORIENTATION = other.PRINCIPAL_ORIENTATION;
    MEAN_RADIUS = other.MEAN_RADIUS;
    VOLUME = other.VOLUME;
    CENTRE = other.CENTRE;
    EXTENT = other.EXTENT;
    POINT_GROUP_SYMBOL = other.POINT_GROUP_SYMBOL;
    POINT_GROUP_EULER = other.POINT_GROUP_EULER;
    VRMS_START = other.VRMS_START;
    DRMS_MIN = other.DRMS_MIN;
    DRMS_MAX = other.DRMS_MAX;
    HIRES = other.HIRES;
    LORES = other.LORES;
  }

}

namespace EntryHelper {
  //this is not a member of Entry but we put it here so that it is with clustang_extent_sampling
  std::pair<double,double>
  clustang(
      double hires,
      double io_sampling,
      double helixfactor,
      int highorderaxis,
      double radius,
      bool finesamp,
      bool dupl_only)
  {
    //unfortunately there is a lot that goes into this calculation
    //this is to avoid passing the parameters in the io
    PHASER_ASSERT(radius>0);
    PHASER_ASSERT(hires>0);
    // Rotation sampling chosen so that point on surface of sphere moves by something
    // less than dmin/2, so that its structure factor contribution is still correlated on average
    // Currently using dmin/3
    double default_sampling = 2.0*scitbx::rad_as_deg(std::atan(hires/(6.*radius)));
    double sampling = default_sampling;
    if (io_sampling > 0)
    {
      sampling = io_sampling;
    }
/*//not here, do this check even for input sampling angles
    else
    {
      //divides into 180 or whatever the max b is under symmetry
      double betamax = 180./highorderaxis;
      double newsamp(0);
      int bmax = std::ceil(betamax/sampling);
      newsamp = betamax/bmax;
      //PHASER_ASSERT(newsamp <= sampling);
      sampling = newsamp;
    }
*/
    PHASER_ASSERT(sampling>0);
    double clusfactor(1.2); //Half-way between 1st (1) and 2nd (sqrt(2)) nearest neighbour in perfect hcp
    double cluster_angle = (sampling*clusfactor);
    if (finesamp and io_sampling <= 0) //flag -999
    {
      PHASER_ASSERT(helixfactor>0);
      sampling /= helixfactor;
      cluster_angle /= helixfactor;
    }
    //don't let it go lower than the minimum clustering distance
    sampling = std::max(0.1,sampling);
    //always check that the sampling is divisible under symmetry or else there are problems
    if (true)
    {
      //divides into 180 or whatever the max b is under symmetry
      double betamax = 180./highorderaxis;
//double BetaSectionMax = 180./LOWORDER;
      int bmax = std::ceil(betamax/sampling);
      double newsamp = betamax/bmax;
      sampling = newsamp;
      //the second time we use round because it should be almost exact anyway, only numerical instability 4ll7 helix
      //this test is also done in FastRotationFunction.cc
      bmax = static_cast<int>(std::round(betamax/sampling));
      PHASER_ASSERT(std::fabs(sampling*bmax-betamax)< DEF_PPT);
    }
    if (dupl_only)
    { //in the frf, the sampling is not uniform because of the projection onto beta sections
      //some places are so finely sampled they are effectivly duplicates,
      //so at a minimum we need to 'cluster' to 0.1
      //this is simply to remove the duplicates
      //or use the sampling/10 to remove more, if this is larger than 0.1
      cluster_angle = std::max(0.1,sampling/10.0);
    }
    cluster_angle = std::max(0.1,cluster_angle);
    return {cluster_angle,sampling};
  }

  std::pair<double,double>
  clustdist(double hires,bool helix,bool coiled_coil, double helixfactor, double sampling_factor,double io_sampling)
  {
    bool finesamp = helix and coiled_coil;
    double default_sampling = hires/sampling_factor;
    if (finesamp) default_sampling /= helixfactor; //because the rotations are finer
    double sampling = default_sampling;
    if (io_sampling > 0)
      sampling = io_sampling;
    PHASER_ASSERT(sampling > 0);
    //print the sampling since this is not in logged keywords above
    double clusfactor(1.5); //AJM this can be changed
    double cluster_distance = sampling*clusfactor;
    cluster_distance = std::max(0.1,cluster_distance);
    return {cluster_distance,sampling};
  }

  double
  sphereOuter(double b,double config_hires)
  {
    //the aim here is to have the lmax within bounds
    int sphere_lmax = std::ceil(scitbx::constants::two_pi*b/config_hires);
    if ((sphere_lmax/2)*2 != sphere_lmax) sphere_lmax++;
    if (sphere_lmax > DEF_CLMN_LMAX) //highest is even no need to check, 100
      b = (DEF_CLMN_LMAX/(scitbx::constants::two_pi/config_hires))-DEF_PPH; //tolerance
    return b;
  }

  int
  calculate_lmax(double b,double hires)
  { //related to above, inverse calculation for same radius
    int sphere_lmax = std::ceil(scitbx::constants::two_pi*b/hires);
    if ((sphere_lmax/2)*2 != sphere_lmax) sphere_lmax++;
    PHASER_ASSERT(sphere_lmax <= DEF_CLMN_LMAX); //because we have configured it with the highest res
    sphere_lmax = std::max(sphere_lmax, DEF_CLMN_LMIN+2);
    return sphere_lmax;
  }

  } //EntryHelper

} //phasertng
