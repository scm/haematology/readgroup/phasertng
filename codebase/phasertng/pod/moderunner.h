//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_moderunner_class__
#define __phasertng_moderunner_class__
#include <string>
#include <scitbx/array_family/shared.h>

namespace phasertng {
namespace pod {

class moderunner {
  public:
    moderunner() {}
    moderunner copy() const
    { //for deepcopy in python
      moderunner other;
      other.pathway_string = pathway_string;
      other.pathway_tag = pathway_tag;
      other.pathlog_string = pathlog_string;
      other.pathlog_tag = pathlog_tag;
      other.subdir = subdir;
      other.dag_cards_filename = dag_cards_filename;
      other.tng_cards_filename = tng_cards_filename;
      other.size = size;
      other.top_tfz = top_tfz;
      other.polarity = polarity;
      other.number_of_poses = number_of_poses;
      other.tncs_info = tncs_info;
      other.sg_info = sg_info;
      other.ccs = ccs;
      other.current_seeks = current_seeks.deep_copy();
      other.message = message;
      other.restart_at_cell_content_scaling = restart_at_cell_content_scaling;
      other.dagcards = dagcards;
      other.tngcards = tngcards;
      return other;
    }

    //below are in ModeRunner
    std::string pathway_string = "";
    std::string pathway_tag = "";
    std::string pathlog_string = "";
    std::string pathlog_tag = "";
    std::string subdir = "";
    std::string dag_cards_filename = "";
    std::string tng_cards_filename = "";
    int size = 0;
    double top_tfz = 0;
    std::string polarity = "";
    int number_of_poses = 0;
    std::string tncs_info = "";
    std::string sg_info = "";
    double ccs = 0;
    //below are in store_solution
    scitbx::af::shared<uuid_t> current_seeks = scitbx::af::shared<uuid_t>();
    std::string message = "";
    bool restart_at_cell_content_scaling = false;
    std::string dagcards = "";
    std::string tngcards = "";

    //getters
    std::string get_pathway_string() { return pathway_string; }
    std::string get_pathway_tag() { return pathway_tag; }
    std::string get_pathlog_string() { return pathlog_string; }
    std::string get_pathlog_tag() { return pathlog_tag; }
    std::string get_subdir() { return subdir; }
    std::string get_dag_cards_filename() { return dag_cards_filename; }
    std::string get_tng_cards_filename() { return tng_cards_filename; }
    int get_size() { return size; }
    double get_top_tfz() { return top_tfz; }
    std::string get_polarity() { return polarity; }
    int get_number_of_poses() { return number_of_poses; }
    std::string get_tncs_info() { return tncs_info; }
    std::string get_sg_info() { return sg_info; }
    double get_ccs() { return ccs; }
    std::string get_message() { return message; }
    bool get_restart_at_cell_content_scaling() { return restart_at_cell_content_scaling; }
    std::string get_dagcards() { return dagcards; }
    std::string get_tngcards() { return tngcards; }

    //setters
    void set_pathway_string(std::string tmp) { pathway_string = tmp; }
    void set_pathway_tag(std::string tmp) { pathway_tag = tmp; }
    void set_pathlog_string(std::string tmp) { pathlog_string = tmp; }
    void set_pathlog_tag(std::string tmp) { pathlog_tag = tmp; }
    void set_subdir(std::string tmp) { subdir = tmp; }
    void set_dag_cards_filename(std::string tmp) { dag_cards_filename = tmp; }
    void set_tng_cards_filename(std::string tmp) { tng_cards_filename = tmp; }
    void set_size(int tmp) { size = tmp; }
    void set_top_tfz(double tmp) { top_tfz = tmp; }
    void set_polarity(std::string tmp) { polarity = tmp; }
    void set_number_of_poses(int tmp) { number_of_poses = tmp; }
    void set_tncs_info(std::string tmp) { tncs_info = tmp; }
    void set_sg_info(std::string tmp) { sg_info = tmp; }
    void set_ccs(double tmp) { ccs = tmp; }
    void set_message(std::string tmp) { message = tmp; }
    void set_restart_at_cell_content_scaling(bool tmp) { restart_at_cell_content_scaling = tmp; }
    scitbx::af::shared<uuid_t> get_current_seeks() { return current_seeks.deep_copy(); }
    void set_current_seeks(scitbx::af::shared<uuid_t> tmp) { current_seeks = tmp.deep_copy(); }
    void set_dagcards(std::string tmp) { dagcards = tmp; }
    void set_tngcards(std::string tmp) { tngcards = tmp; }

};

}} //phasertng

#endif
