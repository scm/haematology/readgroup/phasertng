//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_MtzHandler__class__
#define __phasertng_MtzHandler__class__
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/FileSystem.h>
#include <cmtzlib.h>
#include <mtzdata.h>
#include <array>

namespace phasertng {

  inline bool mtz_isnan(float const& value)
  { return CCP4::ccp4_utils_isnan((union float_uint_uchar *)&value); }

class MtzHandler : public FileSystem
{
  public:
    MtzHandler() {}
    CMtz::MTZ* safe_mtz_get(std::string const&,int);
    void read(FileSystem,int); //file name, read reflections
    void open( //file creator
        FileSystem,
        SpaceGroup&,
        UnitCell&,
        double,
        int,
        af_string);
    ~MtzHandler() { if (mtz and mtz->filein) CMtz::MtzFree(mtz); }

  private:
    CMtz::MTZ* mtz;
    bool       isomorphous;

  public:
    SpaceGroup SG;
    UnitCell   UC;
    double     WAVELENGTH;
    int        NREFL;
    af_string  HISTORY;
    bool       is_isomorphous() { return isomorphous; }

  public:
    CMtz::MTZCOL* mtzH;
    CMtz::MTZCOL* mtzK;
    CMtz::MTZCOL* mtzL;

  public:
    //we want a pointer to the mtz array to avoid reformatting data before loading
    //don't want to have to duplicate (actually triple) data in memory
    CMtz::MTZCOL* addCol(std::string,char,int s=0);
    //no addXtal as we only every have one crystal
    void addSet(std::string);
    CMtz::MTZXTAL* add_HKL_base(double,UnitCell);
    CMtz::MTZCOL* getCol(std::string);
    void Put(std::string);
    int readRefl(float adata[], int logmss[], CMtz::MTZCOL *lookup[], int ncols, int iref);
    void setupReadRefl(sv_string);
    int resetRefl();
    int load_dataset(std::string);

    af_string XtalSetColInfo();
    int Fcount = 0;
    int Icount = 0;
    int FMAPcount = 0;
    int MapCount;
    std::string Flabel,Ilabel,SIGFlabel,SIGIlabel;
    std::string Plabel,Wlabel;
    std::string FREElabel;
    std::array<std::string,4> ISIGIlabel = {{ "","","","" }}; //double brackets required Centos7
    std::array<std::string,4> FSIGFlabel = {{ "","","","" }};
    void find_nonanomalous_data(std::string,std::string,std::string,std::string);
    void find_anomalous_data(std::string,std::string,std::string,std::string,
                             std::string,std::string,std::string,std::string);
    void find_map(std::string);
    void find_fmap_phmap(std::string,std::string,bool);
    void find_freer(std::string);
};

}//phasertng
#endif
