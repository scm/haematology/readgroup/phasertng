import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys,os
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.mode_generator import *
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng import InputCard,Phasertng,PhaserError,enum_out_stream
from phasertng.run.ModeRunner import *
import argparse

def xtricorder(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_xtricorder)

def inner_xtricorder(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="xtricorder")
    initparams = initparams +  "\n" +  build_auto_str("xtricorder")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    #overwrite the phasertng keyword with the xtricorder keyword, which is lower expert level
    if (working_params.xtricorder.analyse_anomalous):
      working_params.phasertng.reflections.read_anomalous = True
    dbdir = working_params.phasertng.suite.database
 #  if (dbdir is not None):
 #    if not os.path.isdir(dbdir):
 #      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    #log the loops
    loop_auto = str("Xtricorder")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout): #database not created if Write() is false
      os.remove(xmlout)

    basetng.input = InputCard()
    basetng.input.parse(user_cards,ignore_unknown_keys)
    ModeRunner(basetng=basetng,mode_list=["walk"])

    #first choice
    hklin = working_params.xtricorder.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) > 0:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    #first choice
    seqin = working_params.xtricorder.seqin
    if seqin is None:
      #second choice is phasertng
      seqin = isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
    if seqin is None and int(basetng.input.size(".phasertng.file_discovery.validated.seq_files.")) > 0:
      #third choice is discovery
      seqin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.seq_files.",0)
    if seqin is not None: #set to the bub requirement
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))

    ModeRunner(basetng=basetng,mode_list=['imtz','data','sga','sgx','aniso','tncso','tncs','feff','twin','cca'])
    read_anomalous = working_params.phasertng.reflections.read_anomalous
    if (working_params.phasertng.reflections.read_anomalous or working_params.xtricorder.analyse_anomalous):
      ModeRunner(basetng=basetng,mode_list=['ccs','ssa'])
    io_srfp = working_params.xtricorder.self_rotation_function
    ModeRunner(basetng=basetng,mode_list=['info'])
    if io_srfp:
      if seqin is not None:
        basetng.input.set_as_str(".phasertng.self_rotation_function.sequence.filename.",str(seqin))
      ModeRunner(basetng=basetng,mode_list=['srf'])
      ModeRunner(basetng=basetng,mode_list=['srfp'])
    basetng.logLoopShut(enum_out_stream.process,loop_auto)

    ModeRunner(basetng=basetng,mode_list=['tree'])
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="mtz file",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  parser.add_argument('-srf',"--srf",dest='srf',action='store_true',help="add self rotation function",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    raise Exception("At least one of either --phil or --input must be specified")
  if (args.phil is None and args.output is None):
    raise Exception("At least one of either --phil or --output must be specified")
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  if (args.input is not None):
    user_phil_str = user_phil_str + "\nphasertng.hklin.filename=\"" + str(args.input)+"\"\n"
  if (args.output is not None):
    print("Output Directory: " + args.output)
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      raise Exception("Directory cannot be opened")
    user_phil_str = user_phil_str + "\nphasertng.suite.database=\"" + str(args.output)+"\"\n"
  if args.srf:
    user_phil_str + "\nphasertng.xtricorder.self_rotation_function=True"
  user_phil_str = user_phil_str.replace("\n\n","\n")
  (error) = xtricorder(user_phil_str=user_phil_str)
  sys.exit(error.exit_code())
