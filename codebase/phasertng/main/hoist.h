#ifndef __phasertng_hoist_class__
#define __phasertng_hoist_class__
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>

namespace phasertng {
namespace hoist {

const std::string WHITESPACE = " \n\r\t\f\v";

namespace algorithm {

inline void trim_left(std::string& str)
{
  size_t start = str.find_first_not_of(WHITESPACE);
  str = (start == std::string::npos) ? "" : str.substr(start);
}

inline void trim_right(std::string& str)
{
  size_t end = str.find_last_not_of(WHITESPACE);
  str = (end == std::string::npos) ? "" : str.substr(0, end + 1);
}

inline void trim(std::string& str)
{
  trim_right(str);
  trim_left(str);
}

inline bool starts_with(const std::string& a, const std::string& b)
{
  return a.size() >= b.size() && a.rfind(b, 0) == 0;
}

inline bool ends_with(const std::string& a, const std::string& b)
{
  return a.size() >= b.size() && 0 == a.compare(a.size()-b.size(), b.size(), b);
}

inline bool contains(const std::string& a, const std::string& b)
{
  return (a.find(b) != std::string::npos);
}

inline void split(std::vector<std::string>& strings,const std::string& str_, const std::string& delimiter)
{
  std::string str = str_;
  if (delimiter == " ")
  {
    trim(str);
    std::string bb = "  ";
    int count(0); //paranoia for infinite loop
    while (contains(str,bb) && count<1000)
    {
      str.replace(str.find(bb),bb.length(),delimiter);
      count++;
    }
  }
  strings.clear();
  std::string::size_type pos = 0;
  std::string::size_type prev = 0;
  while ((pos = str.find(delimiter, prev)) != std::string::npos)
  {
    strings.push_back(str.substr(prev, pos - prev));
    prev = pos + delimiter.size();
  }
  // To get the last substring (or only, if delimiter is not found)
  strings.push_back(str.substr(prev));
}
}

inline void replace_first(std::string& str, std::string substr1, std::string substr2)
{
  if (str.size() < substr1.size()) return;
  size_t index = str.find(substr1);
  if (index != std::string::npos && substr1.length())
    str.replace(index,substr1.length(),substr2);
}

inline void replace_last(std::string& str, std::string substr1, std::string substr2)
{
  if (str.size() < substr1.size()) return;
  size_t index = str.rfind(substr1);
  if (index != std::string::npos && substr1.length())
    str.replace(index,substr1.length(),substr2);
}

inline void replace_all(std::string& str, std::string substr1, std::string substr2)
{
  if (str.size() < substr1.size()) return;
  for (size_t index = str.find(substr1, 0);
       index != std::string::npos && substr1.length();
       index = str.find(substr1, index + substr2.length() ) )
    str.replace(index, substr1.length(), substr2);
}

inline void to_lower(std::string& s)
{
  std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return tolower(c); });
}

inline void to_upper(std::string& s)
{
  std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
}

}}
#endif
