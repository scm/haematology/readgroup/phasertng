//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_cell_class__
#define __phasertng_type_flt_cell_class__
#include <cctbx/uctbx.h>
#include <phasertng/type/optional.h>
#include <scitbx/array_family/tiny_types.h>

namespace phasertng {
namespace type {

namespace af = scitbx::af;

class flt_cell : public optional
{
  cctbx::uctbx::unit_cell dephalt = cctbx::uctbx::unit_cell(af::double6(1,1,1,90,90,90));
  cctbx::uctbx::unit_cell uvalue = cctbx::uctbx::unit_cell(af::double6(1,1,1,90,90,90));
  bool    pminset = false;
  bool    pmaxset = false;
  double  pminval = 0;
  double  pmaxval = 0;

  public:
  const bool&    minset() const { return pminset; }
  const double&  minval() const { return pminval; }
  const bool&    maxset() const { return pmaxset; }
  const double&  maxval() const { return pmaxval; }

  public:
  flt_cell() : optional() {}
  flt_cell(
      const std::string parameter,
      const af::double6 dephalt_ = af::double6(1,1,1,90,90,90)
  );

  void set_value(scitbx::af::double6 other);
  void set_value(const sv_string& vcards,const scitbx::af::double6& other);
  bool is_default() const;
  void set_default();
  cctbx::uctbx::unit_cell value_or_default() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::flt_cell& other) const
  { return uvalue.is_similar_to(other.value_or_default(),0.001,0.001); }

};

}}
#endif
