//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Cell_content_scaling
  void Phasertng::runCCS()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCA))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (cca)");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Reflections"));

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    auto tinyres =  0;
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        tinyres);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_outliers_and_select(REFLECTIONS.get(),
        { rejected::MISSING,
          rejected::WILSONNAT,
          rejected::LOINFONAT,
          rejected::NOMEAN
        });
    logTabArray(out::LOGFILE,selected.logSelected("Tiny Mtz"));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers("Tiny Mtz"));
    }}

    int total_scattering = REFLECTIONS->TOTAL_SCATTERING;
    double io_z = REFLECTIONS->Z;
    if (!input.value__flt_number.at(".phasertng.cell_content_scaling.z.").is_default())
      io_z = input.value__flt_number.at(".phasertng.cell_content_scaling.z.").value_or_default();
    {{
    out::stream where = out::LOGFILE;
    logTab(where,"Total scattering of biological unit = " + itos(total_scattering));
    if (total_scattering == 0)
      throw Error(err::INPUT,"Scattering of biological unit not calculated");
    if (input.value__flt_number.at(".phasertng.cell_content_scaling.z.").is_default())
      throw Error(err::INPUT,"Number in asymmetric unit not set");
    if (!input.value__flt_number.at(".phasertng.cell_content_scaling.z.").value_or_default())
      throw Error(err::INPUT,"Number in asymmetric unit set to zero - check composition");
    logTab(where,"Already scaled = " + btos(REFLECTIONS->WILSON_SCALED));
    }}

    //now apply the total scattering to the mtz file
    //this is permanent change to data elements
    //no (simple) return to original structure factor scale,
    //make the change permanent because cycling will accumulate errors
    //and storing the scale factor slows down all the calculations unnecessarily
      // Scale from ANO mode scales input data to correspond to one electron
      // in a protein of average composition, using BEST curve.
      // Scale up to number of electrons corresponding to composition.
      // ONLY THE OVERALL SCALE is recorded to have been applied by WILSON_SCALED

    dag::Node& NODE = DAGDB.NODES.front();
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Scaling");
    logTab(where,"Number of biological units in asymmetric unit (Z) = " + dtoss(io_z,0));
    //read back in, paranoia self consistency
    if (!REFLECTIONS->WILSON_SCALED)
    {
      REFLECTIONS->Z = input.value__flt_number.at(".phasertng.cell_content_scaling.z.").value_or_default();
      double ztotalscat = REFLECTIONS->get_ztotalscat();
      double KI = ztotalscat*REFLECTIONS->SG.SYMFAC*fn::pow2(REFLECTIONS->WILSON_K_F);
      phaser_assert(!REFLECTIONS->WILSON_SCALED);
      REFLECTIONS->setup_absolute_scale(KI);
      REFLECTIONS->WILSON_SCALED = true; //toggle for only allowing scaling once per data preparation
      //K includes symfac for centring
      logTab(where,"Scale = " + dtos(KI));
      logBlank(where);
    }
    else if (io_z != REFLECTIONS->Z)
    {
      double Iratio = io_z/REFLECTIONS->Z;
      logTab(where,"Ratio new/old Z = " + dtos(Iratio));
      REFLECTIONS->setup_absolute_scale(Iratio); //revert back to 1, only changes the data
      REFLECTIONS->Z = io_z;
      logBlank(where);
    }
    else
    {
      logTab(where,"No change in asymmetric unit Z ");
      logBlank(where);
    }
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::CCS);
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Reflections with Scaling"));

    for (int i = 0; i < DAGDB.NODES.size(); i++)
      DAGDB.NODES[i].CELLZ = REFLECTIONS->Z;
    //write hkl to input, so that it can be added to runEBA mode dag
    //if eca is not run with input hklin, then this will be needed to initialize dag
    //unload_reflections()
    //above is called after every mode, so not necessary here

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());

    //doesn't matter what the filename at termination is
    REFLCOLSMAP.SetFileSystem(DataBase(),reflid.FileOther(other::TINY_MTZ));
    logFileWritten(out::LOGFILE,WriteData(),"Reflections (ccs-tiny)",REFLCOLSMAP);
    //REFLCOLSMAP.generate_free_r_flags();
    if (WriteData())
      REFLCOLSMAP.tiny_write_to_disk(selected.get_selected_array());
  }

} //end namespace phaser
