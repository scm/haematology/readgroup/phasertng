//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_paired.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  flt_paired::flt_paired(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const std::pair<double,double> dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  flt_paired::set_value(std::pair<double,double> other) //allow in place contructor {x,y}
  {
    user = true;
    uvalue = other;
  }

  void
  flt_paired::set_value(const sv_string& vcards,std::pair<double,double> other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_paired::is_default() const
  { return uvalue == dephalt; }

  void
  flt_paired::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_paired::to_python()
  { return unparse_parameter(false); }

  void
  flt_paired::from_python(std::string str)
  {
    sv_string txt = to_words(str);
    set_value({ std::stod(txt[0]),std::stod(txt[1])});
  }

  std::pair<double,double>
  flt_paired::value_or_default() const
  { return user ? uvalue : dephalt; }

  double
  flt_paired::value_or_default_min() const
  {
    std::pair<double,double> tmp = user ? uvalue : dephalt;
    return tmp.first < tmp.second ? tmp.first : tmp.second;
  }

  double
  flt_paired::value_or_default_max() const
  {
    std::pair<double,double> tmp = user ? uvalue : dephalt;
    return tmp.first < tmp.second ? tmp.second : tmp.first;
  }

  std::string
  flt_paired::unparse_parameter(bool phil) const
  {
    if (value_or_default() == std::pair<double,double>( DEF_FLT_FLAG, DEF_FLT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << uvalue.first << " " << uvalue.second;
    return ss.str();
  }

  std::string
  flt_paired::show_default(bool phil) const
  {
    if (dephalt == std::pair<double,double>( DEF_FLT_FLAG, DEF_FLT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << dephalt.first << " " << dephalt.second;
    return is_default() ? None() : ss.str();
  }

}}
