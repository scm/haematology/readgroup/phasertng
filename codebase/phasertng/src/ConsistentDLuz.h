//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ConsistentDLuz_class__
#define __phasertng_ConsistentDLuz_class__
#include <phasertng/main/includes.h>
#include <phasertng/math/vec_ops.h>
#include <phasertng/math/mat_ops.h>
#include <phasertng/math/RealSymmetricPseudoInverse.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>

#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <scitbx/array_family/accessors/c_grid.h>
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;
typedef scitbx::af::versa<double, scitbx::af::c_grid<3> >   versa_grid_double;

namespace phasertng {

//class FourParameterSigmaA;

  class ConsistentDLuz
  {
    private:
      int max_nround = 10;
      double TWOPISQ_ON_THREE = 6.579736267392905; //double is 15 decimal places

    public:
      versa_flex_double pseudoCorMatInv;
      versa_flex_double rhoDelta12;
      sv_double Rms_in;
      sv_double Rms_out;
      sv_double DLuz_in;
      sv_double DLuz_out;
      bool inconsistent = false;

    public:
      ConsistentDLuz(
          const int=10
        );

    public:
      int  setup(
          FourParameterSigmaA,
          sv_double,
          versa_flex_double,
          double
        );
  };

}//phasertng
#endif
