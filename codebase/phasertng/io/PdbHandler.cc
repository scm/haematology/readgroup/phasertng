#include <phasertng/io/PdbHandler.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/Error.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <chrono>

namespace phasertng {

  PdbHandler::PdbHandler(
      SpaceGroup SG,
      UnitCell UC,
      int SOLZ
    )
  {
    addCrystCard(SG,UC,SOLZ);
  }

  void
  PdbHandler::addCrystCard(
      SpaceGroup SG,
      UnitCell UC,
      int SOLZ
    )
  {
    int Z = SG.group().order_z()*SOLZ;
    PHASER_ASSERT(Z >= 1);
    char pdbchar[100];
    sprintf(pdbchar,"%6s%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f %-11s%4i",
        "CRYST1",
        UC.A(),
        UC.B(),
        UC.C(),
        UC.Alpha(),
        UC.Beta(),
        UC.Gamma(),
        SG.CCP4.c_str(),
        Z);
    CRYSTCARD.push_back(std::string(pdbchar));
    for (int i = 0; i < 3; i++)
    {
      char pdbchar[100];
      sprintf(pdbchar,"%5s%d    %10.6f%10.6f%10.6f     %10.5f",
        "SCALE",
        i+1,
        UC.fractionalization_matrix()(i,0),
        UC.fractionalization_matrix()(i,1),
        UC.fractionalization_matrix()(i,2),
        0.0);
      CRYSTCARD.push_back(std::string(pdbchar));
    }
  }

  void
  PdbHandler::VanillaWritePdb(FileSystem PDBOUT)
  {
    PDBOUT.write_array(PdbString());
  }

  af_string
  PdbHandler::PdbString()
  {
    af_string pdbstr;
    char buf[100];
    auto now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    struct tm *parts = std::localtime(&now_c);
    sv_string months = {"JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
    snprintf(buf,100,"%-10s%-40s%d-%s-%d    XXXX","HEADER","PHASERTNG GENERATED",
            parts->tm_mday,months[parts->tm_mon].c_str(),parts->tm_year-100);
    pdbstr.push_back(std::string(buf));
    for (auto item : CRYSTCARD)
      pdbstr.push_back(item);
    for (auto item : REMARK)
      pdbstr.push_back(item);
    int atom_number(1);
    for (unsigned s = 0; s < PDB.size(); s++)
    {
      char buf[16];
      snprintf(buf,16,"MODEL %8d",s+1);
      pdbstr.push_back(std::string(buf));
      for (unsigned a = 0; a < PDB[s].size(); a++)
      {
        std::array<std::string,3> Cards = PDB[s][a].WriteAtomCard(atom_number++);
        for (int i = 0; i < 3; i++)
        {
          //check that the string will always add a return
          if (Cards[i].size())
          {
            PHASER_ASSERT(Cards[i].back() != '\n'); //don't want to add two returns
            pdbstr.push_back(Cards[i]);
          }
        }
      }
      pdbstr.push_back("ENDMDL");
    }
    pdbstr.push_back("END");
    return pdbstr;
  }

  void
  PdbHandler::addRemarkPhaser(std::string REMARK_)
  {
    //remark + number == %10s
    char buf[80];
    snprintf(buf,80,"%6s %2s %s","REMARK",constants::pdb_remark_number.c_str(),REMARK_.c_str());
    REMARK.push_back(std::string(buf));
  }

}//phasertng
