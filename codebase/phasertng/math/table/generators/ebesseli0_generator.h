#ifndef __phasertng_table_ebesseli0_class__
#define __phasertng_table_ebesseli0_class__
#include <vector>
#include <scitbx/constants.h>
#include <phasertng/main/Assert.h>

namespace phasertng {

  inline double Fn_eBesselI0(double arg)
  {
  /* Calculates approximate values for the modified Bessel function
     of the first kind of order zero multiplied by EXP(-ABS(X)).
     Derived from:
     Authors: W. J. Cody and L. Stoltz
     Mathematics and Computer Science Division
     Argonne National Laboratory
     Argonne, IL 60439
  */
    const double LOWERLIM = 15.0;
    const double EPSILON  = 1.0e-15;

    double ax(0),ax2(0),sumn(0),sumd(0);

    // BEGIN
    ax = fabs(arg);

    if (ax < EPSILON)
      return 1;
    else if (ax < LOWERLIM)
    {
      ax2 = ax*ax;
      sumn =      -2.2335582639474375249e+15 +
             ax2*(-5.5050369673018427753e+14 +
             ax2*(-3.2940087627407749166e+13 +
             ax2*(-8.4925101247114157499e+11 +
             ax2*(-1.1912746104985237192e+10 +
             ax2*(-1.0313066708737980747e+08 +
             ax2*(-5.9545626019847898221e+05 +
             ax2*(-2.4125195876041896775e+03 +
             ax2*(-7.0935347449210549190e+00 +
             ax2*(-1.5453977791786851041e-02 +
             ax2*(-2.5172644670688975051e-05 +
             ax2*(-3.0517226450451067446e-08 +
             ax2*(-2.6843448573468483278e-11 +
             ax2*(-1.5982226675653184646e-14 +
             ax2*(-5.2487866627945699800e-18))))))))))))));
       ax2 = ax2-LOWERLIM*LOWERLIM;
      sumd =      -9.7087946179594019126e+14 +
             ax2*( 3.7604188704092954661e+12 +
             ax2*(-6.5626560740833869295e+09 +
             ax2*( 6.5158506418655165707e+06 +
             ax2*(-3.7277560179962773046e+03 + ax2))));
      return (sumn/sumd)*exp(-ax);
    }
    else
    {
      ax2=(1/ax)-(1/LOWERLIM);
      sumn =      -2.1877128189032726730e-06 +
             ax2*( 9.9168777670983678974e-05 +
             ax2*(-2.6801520353328635310e-03 +
             ax2*(-3.7384991926068969150e-03 +
             ax2*( 4.7914889422856814203e-01 +
             ax2*(-2.4708469169133954315e+00 +
             ax2*( 2.9205384596336793945e+00 +
             ax2*(-3.9843750000000000000e-01)))))));
      sumd =      -5.5194330231005480228e-04 +
             ax2*( 3.2547697594819615062e-02 +
             ax2*(-1.1151759188741312645e+00 +
             ax2*( 1.3982595353892851542e+01 +
             ax2*(-6.0228002066743340583e+01 +
             ax2*( 8.5539563258012929600e+01 +
             ax2*(-3.1446690275135491500e+01 + ax2))))));
      return ((sumn/sumd)+3.984375e-01)/sqrt(ax);
    }
    return 0;
  }

namespace table {

class ebesseli0
{
  private:
  // Accuracy about 1 part in 10^7 or better, 0-infinity
  // Quadratic interpolation would use smaller tables, but is slower
  int    tbllen = 0;
  double argcut1 = 0,argcut2 = 0;
  double argfac1 = 0,argfac2 = 0;
  double invArgFac = 0;
  std::vector<std::pair<double,double> > eBesselI0Table1;
  std::vector<std::pair<double,double> > eBesselI0Table2;
  std::vector<std::pair<double,double> > eBesselI0InvTable;

  public:
  ebesseli0(int    tbllen_=4096,
            double argcut1_=4.,
            double argcut2_=20.
           ) :
       tbllen(tbllen_),
       argcut1(argcut1_),
       argcut2(argcut2_)
  {
    argfac1 = double(tbllen/argcut1);
    argfac2 = double(tbllen/argcut2);
    eBesselI0Table1 = geteBesselI0Table(tbllen,argcut1);
    eBesselI0Table2 = geteBesselI0Table(tbllen,argcut2);
    eBesselI0InvTable = geteBesselI0InvTable(tbllen,argcut2);
    invArgFac=(tbllen*argcut2);

   }

   void
   print_tables()
   {
  std::cout << "static constexpr const double eBesselI0Table1[4097][2] = {" << std::endl;
  for (int i = 0; i < eBesselI0Table1.size(); i++)
    std::cout << std::setprecision(17) << "{ " << eBesselI0Table1[i].first << "," << eBesselI0Table1[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr const double eBesselI0Table2[4097][2] = {" << std::endl;
  for (int i = 0; i < eBesselI0Table2.size(); i++)
    std::cout << std::setprecision(17) << "{ " << eBesselI0Table2[i].first << "," << eBesselI0Table2[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr const double eBesselI0InvTable[4097][2] = {" << std::endl;
  for (int i = 0; i < eBesselI0InvTable.size(); i++)
    std::cout << std::setprecision(17) << "{ " << eBesselI0InvTable[i].first << "," << eBesselI0InvTable[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  }

  private:
  std::vector<std::pair<double,double> >
  geteBesselI0Table(int tbllen_, double argmax_)
  {
    std::vector<std::pair<double,double> > eBesselI0Table;
    eBesselI0Table.resize(tbllen_+1);
    double divarg(argmax_/tbllen_);
    for (unsigned i = 0; i < tbllen_+1; i++)
    {
      double x0(Fn_eBesselI0((i  )*divarg));
      double x1(Fn_eBesselI0((i+1)*divarg));
      eBesselI0Table[i].first = x0;
      eBesselI0Table[i].second = x1 - x0;
    }
    return eBesselI0Table;
  }

  std::vector<std::pair<double,double> >
  geteBesselI0InvTable(int tbllen_, double argcut_)
  {
    // sqrt(1/x)eBesselI0(1/x) is nearly linear for small x.
    std::vector<std::pair<double,double> > eBesselI0InvTable;
    eBesselI0InvTable.resize(tbllen_+1);
    double divarg(1./(argcut_*tbllen_));
    double arg0(0),arg1(0),x0(0),x1(0);
    double invSqrt2Pi(1./std::sqrt(scitbx::constants::two_pi));
    for (unsigned i = 0; i < tbllen_+1; i++)
    {
      if (i > 0)
      {
        arg0 = 1./((i  )*divarg);
        x0 = std::sqrt(arg0)*Fn_eBesselI0(arg0);
      }
      else
        x0 = invSqrt2Pi; // Limit as 1/arg->zero, or arg->infinity
      arg1 = 1./((i+1)*divarg);
      x1 = std::sqrt(arg1)*Fn_eBesselI0(arg1);
      eBesselI0InvTable[i].first = x0;
      eBesselI0InvTable[i].second = x1 - x0;
    }
    return eBesselI0InvTable;
  }

};

}}
#endif
