//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Packing.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/Error.h>
#include <phasertng/io/entry/Coordinates.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/io/PdbHandler.h>
#include <mmtbx/geometry/calculator.hpp>
#include <cctbx/sgtbx/site_symmetry.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <phasertng/math/rotation/rotdist.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/io/FileSystem.h>
#include <phasertng/main/jiffy.h>

//#define PHASERTNG_USE_FLOOR

namespace phasertng {

  Packing::Packing(double DISTANCE_FACTOR_) : DISTANCE_FACTOR(DISTANCE_FACTOR_) { }

  void
  Packing::init_node(dag::Node* WORK)
  {
//AJM TODO
//if the clash matrix is already partly defined, then the pose order for the p loop
//MUST match the pose indexing in the pose clash_matrix (first element)
//otherwise the poses and the clash matrix will not be in sync
    NODE = WORK;
    UC = UnitCell(NODE->CELL);
    UC.scale_orthogonal_cell(NODE->DATA_SCALE);
    PHASER_ASSERT(!UC.is_default());
    SG = *NODE->SG;
    CLASH_ATOMS.resize(NODE->POSE.size());
    for (int p = 0 ; p < NODE->POSE.size(); p++)
      CLASH_ATOMS[p] = std::unordered_set<int>();
    sv_ivect3 ntrace(NODE->POSE.size());
    for (int p = 0 ; p < NODE->POSE.size(); p++)
    {
      const auto& entry = NODE->POSE[p].ENTRY;
      ntrace[p] = ivect3(p+1,0,entry->TRACE.size());
    }
    sv_int cvector(NODE->POSE.size()*NODE->POSE.size(),0);
    PACKING_DATA.initialize(cvector,ntrace);
  }

  void
  Packing::move_to_origin(bool move_origin,dvect3 origin,int pref)
  {
    MOVED_TO_ORIGIN = false;
    CLOSEST_TO_ORIGIN_CENTRE = dvect3(0,0,0);
    // Move each symmetry copy to be in the same cell as the origin (most often
    // 0,0,0) and choose the one closest to the origin. Move tNCS-related copies
    // as a group.
    double min_cell = std::min(UC.A(),std::min(UC.B(),UC.C()));
    PHASER_ASSERT(min_cell > 0);
    double overall_min_delta_sqr = std::numeric_limits<double>::max();
    dvect3 fract_origin(0,0,0);
    if (origin != dvect3(0,0,0))
    {
      fract_origin = UC.fractionalization_matrix() * origin;
    }
    std::set<int> firstp;
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      if (pose.TNCS_GROUP.second == 0 or  //not set ?? default is 1,1
          pose.TNCS_GROUP.second == 1 )//set and first tncs copy
        firstp.insert(p);
    }
    for (int p : firstp)
    {
      double min_delta_sqr = std::numeric_limits<double>::max();
      dag::Pose& pose = NODE->POSE[p];
      dmat33 ROT;dvect3 FRACT;
      // finalRT gives operations with respect to molecular transpose, so FRACT
      // is translation of centre from origin. NB: any perturbations of initial
      // rotations and translations are included, so ROT includes effect of ORTHR and
      // FRACT includes effect of ORTHT.
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      int ncell = 0; // Just generate symmetry operators with no cell translations
      af_dvect3 symmetry = SG.symmetry_xyz_cell(FRACT, ncell);
      std::pair<int,dvect3> base_isym_celltrans = {0,dvect3(0,0,0)};
      std::pair<int,dvect3> best_isym_celltrans = {0,dvect3(0,0,0)};
      dmat33 Rsymt;
      for (int isym = 0; isym < symmetry.size(); isym++)
      {
        cctbx::fractional<double> fract_sym = symmetry[isym];
        // Move to cell containing origin
        dvect3 trans_to_target_cell;
        for (int ixyz = 0; ixyz < 3; ixyz++)
        {
          trans_to_target_cell[ixyz] = -std::floor(fract_sym[ixyz] - fract_origin[ixyz]);
          fract_sym[ixyz] += trans_to_target_cell[ixyz];
        }
        dvect3 orth_chk_sym_cell = UC.orthogonalization_matrix()*fract_sym;
        dvect3 vector_from_origin = orth_chk_sym_cell - origin;
        double delta_sqr = vector_from_origin*vector_from_origin;
        if (delta_sqr < min_delta_sqr)
        { //closest to origin
          best_isym_celltrans = {isym, trans_to_target_cell};
          min_delta_sqr = delta_sqr;
          if (delta_sqr < overall_min_delta_sqr)
          { //closest to origin over all poses
            overall_min_delta_sqr = delta_sqr;
            CLOSEST_TO_ORIGIN_CENTRE = orth_chk_sym_cell;
          }
        }
      }
      if (!move_origin and pref>=0)
      { //because these are also used in move_to_compact
        PHASER_ASSERT(pref < NODE->POSE.size());
        dag::Pose& pose = NODE->POSE[pref];
        dmat33 ROT;dvect3 FRACT;
        std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
        CLOSEST_TO_ORIGIN_CENTRE = UC.orthogonalization_matrix()*FRACT;
      }
      if (move_origin and best_isym_celltrans != base_isym_celltrans)
      { //make sure the tncs groups are moved together, so as to maintain translation
        dmat33 Rsymt = SG.rotsym[best_isym_celltrans.first].transpose();
        dmat33 RsymOrth = UC.orthogonalization_matrix()*Rsymt*UC.fractionalization_matrix();
        for (int pp = 0; pp < NODE->POSE.size(); pp++) //this and subsequent
        {
          dag::Pose& ppose = NODE->POSE[pp];
          if (pp == p  or //first, even if not set, this operation etc
             (firstp.count(pp) == 0 and ppose.TNCS_GROUP.first == pose.TNCS_GROUP.first)) //in tncs group
          {
            dmat33 ROTpp;dvect3 FRACTpp;
            std::tie(ROTpp,FRACTpp) = ppose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
            af_dvect3 symmetry_pp = SG.symmetry_xyz_cell(FRACTpp, ncell);
            dvect3 FRACT = symmetry_pp[best_isym_celltrans.first] + best_isym_celltrans.second;
            // Apply the symmetry rotation to the translation perturbation ORTHT
            // The new FRACT and ORTHT should add up to the symmetry copy of the old ones
            dvect3 ORTHT_new = RsymOrth*ppose.ORTHT;
            ppose.FRACT = FRACT - UC.fractionalization_matrix()*ORTHT_new;
            ppose.ORTHT = ORTHT_new;
            // Replace Euler angles stored in pose with the symmetry-related Euler angles
            // The value of ROT that would come from finalRT would include the effect of the ORTHR perturbation
            dmat33 ROT0 = zyz_matrix(ppose.EULER);
            ROT0 = RsymOrth*ROT0;
            ppose.EULER = scitbx::math::euler_angles::zyz_angles(ROT0);
            ppose.ORTHR = dvect3(0,0,0);
            MOVED_TO_ORIGIN = true;
            const dvect3 PT = ppose.ENTRY->PRINCIPAL_TRANSLATION;
            const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
            std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(ppose.EULER,PR);
            dvect3 frac = dag::fract_wrt_in_from_mt(ppose.FRACT,ppose.EULER,PT,NODE->CELL);
            ppose.SHIFT_ORTHT = UC.orthogonalization_matrix()*frac;
          }
        }
      }
    }
  }

  void
  Packing::multiplicity_and_exact_site()
  { //also calculate multiplicity of position of centre in space group
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      //apply multiplicity even if there is no move
      dag::Pose& pose = NODE->POSE[p];
      double min_distance_sym_equiv = 1;//more generous than default of 0.5
      dmat33 ROT;dvect3 FRACT;
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      //the centre of mass is at the origin
      //the point group goes through the centre of mass
      //therefore, fract on special position = point group on special posigion
      cctbx::sgtbx::site_symmetry this_site_sym(
         UC.cctbxUC,SG.group(),FRACT,min_distance_sym_equiv);
      dvect3 FRACT2 = this_site_sym.exact_site();
      if (FRACT2 != FRACT)
      {
        // Only change FRACT if site moves to satisfy site symmetry
        // Set ORTHT to zero because it was part of original FRACT
       // pose.FRACT = dag::fract_wrt_mt_from_in(FRACT,ROT,PT,NODE->CELL);
        pose.ORTHT = dvect3(0,0,0);
      }
      pose.MULT = SG.group().order_z()/this_site_sym.multiplicity();
      //do we need a special position warning per pose rather than overall?
    }
  }

  void
  Packing::move_to_compact(bool move)
  {
    MOVED_TO_COMPACT = false;
    // This assumes that move_to_origin has been run with same value for move.
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      //we have stored the one that is in the original orientation/translation
      //flag is -999 for no match to input orientation/translation
      //no exclusions  //need unit cell translation even in reference case
      {
        dag::Pose& pose = NODE->POSE[p];
        double min_delta_sqr = std::numeric_limits<double>::max();
        dmat33 ROT;dvect3 FRACT;
        std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
        int ncell = 2; // +/- 1 cell would probably be enough, but this is fast
        af_dvect3 symmetry = SG.symmetry_xyz_cell(FRACT, ncell);
        std::pair<int,dvect3> base_isym_cell = {0,dvect3(0,0,0)};
        std::pair<int,dvect3> best_isym_cell = {0,dvect3(0,0,0)};
        for (int isym = 0; isym < symmetry.size(); isym++)
        {
          cctbx::fractional<double> fract_sym_cell = symmetry[isym];
          dvect3 orth_chk_sym_cell = UC.orthogonalization_matrix()*fract_sym_cell;
          dvect3 delta = (orth_chk_sym_cell-CLOSEST_TO_ORIGIN_CENTRE);
          double delta_sqr = delta*delta;
          if (delta_sqr < min_delta_sqr)
          { //closest to origin
            int isym_ori = SG.symmetry_xyz_cell(isym,ncell).first;//recover untranslated symm op number
            best_isym_cell = {isym_ori,fract_sym_cell};
            min_delta_sqr = delta_sqr;
          }
        }
        if (move and best_isym_cell != base_isym_cell)
        {
          dmat33 Rsymt = SG.rotsym[best_isym_cell.first].transpose();
          dmat33 RsymOrth = UC.orthogonalization_matrix()*Rsymt*UC.fractionalization_matrix();
          dvect3 ORTHT_new = RsymOrth*pose.ORTHT;
          FRACT = best_isym_cell.second;
          pose.FRACT = FRACT - UC.fractionalization_matrix()*ORTHT_new;
          pose.ORTHT = ORTHT_new;
          dmat33 ROT0 = zyz_matrix(pose.EULER);
          ROT0 = RsymOrth*ROT0;
          pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT0);
          const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
          std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(pose.EULER,PR);
          pose.ORTHR = dvect3(0,0,0);
          MOVED_TO_COMPACT = true;
          const dvect3 PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
          dvect3 frac = dag::fract_wrt_in_from_mt(pose.FRACT,pose.EULER,PT,NODE->CELL);
          pose.SHIFT_ORTHT = UC.orthogonalization_matrix()*frac;
        }
      }
    }
  }

  void
  Packing::calculate_packing(bool last_only,bool no_cell_translation)
  {
    double min_cell = std::min(UC.A(),std::min(UC.B(),UC.C()));
    PHASER_ASSERT(NODE->POSE.size());

    int q0(0); //by default, start at q=0
    if (last_only and NODE->POSE.size() > 1)
      for(q0 = 0; q0 < NODE->POSE.size(); q0++)
        if (NODE->POSE[q0].TNCS_GROUP.first == NODE->POSE.back().TNCS_GROUP.first)
          break; //first value where this tncs_group matches the final tncs_group

    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      Trace& Tref = pose.ENTRY->TRACE;
      dmat33 ROT;dvect3 FRACT;
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      std::vector<PdbRecord> PDB = Tref.PDB;
      PHASER_ASSERT(PDB.size());
      for (auto& card : PDB)
        card.X = ROT*card.X + UC.orthogonalization_matrix()*FRACT;

      dvect3 ref_centre = ROT*Tref.CENTRE + UC.orthogonalization_matrix()*FRACT;
      double distref = DISTANCE_FACTOR*Tref.SAMPLING_DISTANCE;//more generous, corners of spheres

      std::vector<sv_bool> overlap(NODE->POSE.size(),sv_bool());
      sv_int max_ncells (NODE->POSE.size(),0);

      //check whether any of q or their symmetry copies pass the ball test
      for(int q = 0; q < NODE->POSE.size(); q++)
      {
        dag::Pose& poseq = NODE->POSE[q];
        Trace& Tchk = poseq.ENTRY->TRACE;

        {
#ifdef PHASERTNG_USE_FLOOR
        int nref = std::floor(Tref.MAX_RADIUS/min_cell);
        int nchk = std::floor(Tchk.MAX_RADIUS/min_cell);
#else
        int nref = std::ceil(Tref.MAX_RADIUS/min_cell);
        int nchk = std::ceil(Tchk.MAX_RADIUS/min_cell);
#endif
        //AJM TODO
        //may be floor rather than ceil
        // and could use 3D extents (different in 3 directions)
        // but would probably need to work use fractional extents rather than orthogonal to do this
        max_ncells[q] = nchk+nref; //I think this is right
        }
        if (no_cell_translation)
          max_ncells[q] = 0;

        double distchk = DISTANCE_FACTOR*Tchk.SAMPLING_DISTANCE;//more generous, corners of spheres

        PHASER_ASSERT(Tchk.MAX_RADIUS > 0);
        PHASER_ASSERT(Tref.MAX_RADIUS > 0);
        double distoverlap = Tchk.MAX_RADIUS + Tref.MAX_RADIUS + distref + distchk;
        dmat33 ROT;dvect3 FRACT;
        std::tie(ROT,FRACT) = poseq.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
        dvect3 orth_centre = ROT*Tchk.CENTRE + UC.orthogonalization_matrix()*FRACT;
        dvect3 frac_centre = UC.fractionalization_matrix()*orth_centre;

        bool self_pack(p==q);
        af_dvect3 symmetry = SG.symmetry_xyz_cell(frac_centre,max_ncells[q],self_pack);
        //score each symmetry operator by the possibility of overlap
        overlap[q].resize(symmetry.size(),false);
        for (int isym = 0; isym < symmetry.size(); isym++)
        {
          const dvect3& frac_centre_sym_cell = symmetry[isym];
          dvect3 orth_centre_sym_cell = UC.orthogonalization_matrix()*frac_centre_sym_cell;
          double csqr = (orth_centre_sym_cell-ref_centre)*(orth_centre_sym_cell-ref_centre);
          overlap[q][isym] = csqr < distoverlap*distoverlap;
        }
      }

      bool one_overlap = false;
      sv_bool trace_or_sym_copy_overlaps(NODE->POSE.size(), false); //indexed on q
      for(int q = 0; q < NODE->POSE.size(); q++)
      {
        for (int isym = 0; isym < overlap[q].size(); isym++)
        {
          if (overlap[q][isym])
          {
            one_overlap = true;
            trace_or_sym_copy_overlaps[q] = true;
            break;
          }
        }
      }

      if (one_overlap)
      {
        using namespace mmtbx::geometry::asa::calculator;
        using namespace mmtbx::geometry::asa::calculator::utility;
        ConstRadiusCalculator<
            TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
            double >
          refcalc(
              TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >( PDB ),
              distref
            );
        for (int q = 0; q < NODE->POSE.size(); q++)
        {
          if (trace_or_sym_copy_overlaps[q])
          {
            dag::Pose& poseq = NODE->POSE[q];
            dmat33 ROT;dvect3 FRACT;
            std::tie(ROT,FRACT) = poseq.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
            Trace& Tchk = poseq.ENTRY->TRACE;
            double distchk = DISTANCE_FACTOR*Tchk.SAMPLING_DISTANCE;//more generous, corners of spheres
            bool self_pack(p==q);
            sv_bool atom_clashes(Tchk.size(), false);

            auto sym_operators = SG.symmetry_xyz_cell_operators(max_ncells[q],self_pack);
            for (int isym = 0; isym < sym_operators.size(); isym++)
            {
              if (overlap[q][isym])
              {
                const dmat33& sym_rot_frac = sym_operators[isym].first;
                const dvect3& sym_tra_frac = sym_operators[isym].second;
                for (int a_chk = 0; a_chk < Tchk.size(); a_chk++)
                {
                  const PdbRecord& A = Tchk.PDB[a_chk];
                  if (A.O > 0)
                  {
                    dvect3 orth_chk_atom     = ROT*A.X + UC.orthogonalization_matrix()*FRACT;
                    dvect3 frac_chk_atom     = UC.fractionalization_matrix()*orth_chk_atom;
                    dvect3 frac_chk_atom_sym = sym_rot_frac*frac_chk_atom + sym_tra_frac;
                    dvect3 orth_chk_atom_sym = UC.orthogonalization_matrix()*frac_chk_atom_sym;
                    bool clash = refcalc.is_overlapping_sphere(orth_chk_atom_sym,distchk);
                    if (!atom_clashes[a_chk] and clash)
                    {
                      PACKING_DATA.inc(p,q);
                      CLASH_ATOMS[q].insert(a_chk); //a growing list of the clash sites
                      atom_clashes[a_chk] = true;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    PACKING_DATA.clear_overlap();
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      //double percent = (100.*CLASH_ATOMS[p].size())/static_cast<double>(TRACE[NODE->POSE[p].IDENTIFIER].size());
      dag::Pose& pose = NODE->POSE[p];
      int ntrace = pose.ENTRY->TRACE.size();
      pose.CLASH.index = p + 1;
      pose.CLASH.atoms_clash = CLASH_ATOMS[p].size();
      pose.CLASH.atoms_trace = ntrace;
      PACKING_DATA.add_overlap(CLASH_ATOMS[p].size(), ntrace );
    }
   // NODE->CLASH_WORST = PACKING_DATA.worst_percent();
  }

  bool
  Packing::calculate_special_position_warning()
  {
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      std::string text = pose.ENTRY->POINT_GROUP_SYMBOL;
      sv_string orders;
      //because AJM modified ncs::PointGroup to put spaces between the orders!!
      hoist::algorithm::split(orders, text, (" "));
      std::set<int> subgroups;
      for (auto order : orders)
        for (int i = 2; i <= std::atoi(order.c_str()); i++)
          if (std::atoi(order.c_str())%i == 0)
            subgroups.insert(i);
      if (subgroups.count(pose.MULT))
      {
        return true;
      }
    }
    return false;
  }

  void
  Packing::WritePosePdb(
      FileSystem PDBOUT,
      int p
    )
  {
    PdbHandler pdbhandler(SG,UC,1);
    pdb_chains_2chainid chains; //so that X is included
    pdbhandler.addModel(); //each trace is a different chain, not model
    //if outputting all to the one file, use this loop below
    //for (int p = 0; p < NODE->POSE.size(); p++)
    //p instead entered as parameter
    {
      dag::Pose& pose = NODE->POSE[p];
      const Trace& ptrace = pose.ENTRY->TRACE;
      //std::string Chain = chains.allocate(" A"); //increment starting at A
      int a(1); //renumber
      bool renumber(false);
      dmat33 ROT;dvect3 FRACT;
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      for (int a_chk = 0; a_chk < ptrace.size(); a_chk++)
      {
        PdbRecord card = ptrace.PDB[a_chk];
        card.X = ROT*card.X + UC.orthogonalization_matrix()*FRACT;
       // card.Chain = Chain;
        if (renumber)
        {
          card.AtomNum = a;
          card.ResNum = a;
        }
        if (p < CLASH_ATOMS.size()) //use bfactors because coot can colour by ca+bfac
          card.B = CLASH_ATOMS[p].count(a_chk) ? 500 : 0;
        pdbhandler.addRecord(card);
        a++;
      }
    }
    pdbhandler.VanillaWritePdb(PDBOUT.fstr());
  }

  void
  Packing::WritePdb(FileSystem PDBOUT)
  {
    PdbHandler pdbhandler(SG,UC,1);
    pdb_chains_2chainid chains; //so that X is included
    pdbhandler.addModel(); //each trace is a different chain, not model
    //if outputting all to the one file, use this loop below
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      const Trace& ptrace = pose.ENTRY->TRACE;
      //std::string Chain = chains.allocate(" A"); //increment starting at A
      dmat33 ROT;dvect3 FRACT;
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      int a(1); //renumber
      bool renumber(false);
      for (int a_chk = 0; a_chk < ptrace.size(); a_chk++)
      {
        PdbRecord card = ptrace.PDB[a_chk];
        card.X = ROT*card.X + UC.orthogonalization_matrix()*FRACT;
       // card.Chain = Chain;
        if (renumber)
        {
          card.AtomNum = a;
          card.ResNum = a;
        }
        if (p < CLASH_ATOMS.size()) //use bfactors because coot can colour by ca+bfac
          card.B = CLASH_ATOMS[p].count(a_chk) ? 500 : 0;
        pdbhandler.addRecord(card);
        a++;
      }
    }
    pdbhandler.VanillaWritePdb(PDBOUT.fstr());
  }

  af_string
  Packing::structure_for_refinement(Coordinates& coordinates,bool keep_chains)
  { //in place modification of coordinates
    af_string output;
    coordinates.SG = SG;
    coordinates.UC = UC;
    //set identifiers externally
    pdb_chains_2chainid chains; //so that X is included
    //if outputting all to the one file, use this loop below
    PHASER_ASSERT(NODE->POSE.size());
    double minbfac = NODE->POSE[0].BFAC;
    for (int p = 0; p < NODE->POSE.size(); p++)
      minbfac = std::min(minbfac,NODE->POSE[p].BFAC);
    //minbfac can be negative
    CENTRE_OF_MASS = dvect3(0,0,0);
    double natoms(0);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      const Monostructure& pmono = pose.ENTRY->MONOSTRUCTURE;
      PHASER_ASSERT(pmono.size());
      std::string last_user_chain = "not a chain";
      std::string last_allocate_chain = "not a chain";
      dmat33 ROT;dvect3 FRACT;
      std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      int a(1); //renumber
      bool renumber(false);
      for (int a_chk = 0; a_chk < pmono.size(); a_chk++)
      {
        PdbRecord card = pmono.PDB[a_chk];
        card.ExtraFlags = ivtos(pose.TNCS_GROUP); //we can parse this later
        card.X = ROT*card.X + UC.orthogonalization_matrix()*FRACT;
        CENTRE_OF_MASS += card.X;
        card.B = card.B + pose.BFAC - minbfac; //lowest bfac is the reference for no change, everything else adds
        if (!keep_chains)
        {
          if (card.Chain != last_user_chain)
          {
            std::string newChain = chains.allocate(pmono.PDB[a_chk].Chain);
            output.push_back("Pose #" + ntos(p+1,NODE->POSE.size()) + " Chain \"" + card.Chain + "\"->\"" + newChain + "\" " + pose.IDENTIFIER.str());
            card.Chain = newChain;
            PHASER_ASSERT(!chains.allocate_failed());
            last_user_chain = pmono.PDB[a_chk].Chain;
            last_allocate_chain = card.Chain;
          }
          else
          {
            card.Chain  = last_allocate_chain;
          }
        }
        if (renumber)
        {
          card.AtomNum = a;
          card.ResNum = a;
        }
      //  if (p < CLASH_ATOMS.size())
       //   card.O = CLASH_ATOMS[p].count(a_chk) ? 0 : 1;
        coordinates.AddAtom(card);
        a++;
        natoms++;
      }
    }
    CENTRE_OF_MASS /= natoms;
    output.push_back("");
    PHASER_ASSERT(coordinates.size());
    return output;
  }

  af_string
  Packing::logClashes(std::string description) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Packing Clashes: " + description + "<<");
    output.push_back("Space Group: " + SG.CCP4);
    output.push_back("Special Position Warning: " + chtos(NODE->SPECIAL_POSITION));
    output.push_back("Worst Clash: " + dtos(PACKING_DATA.worst_percent(),6,2) + "%");
    {{
    std::string line;
    line += snprintftos("%2s %c ","",' ');
    for (int q = 0; q < PACKING_DATA.size(); q++)
      line += snprintftos("%6d   ",q+1);
    line += snprintftos("  :  %6s","total");
    output.push_back(line);
    }}
    {{
    for (int p = 0; p < PACKING_DATA.size(); p++)
    {
      std::string line;
      line += snprintftos("%2d %c ",p+1,':');
      for (int q = 0; q < PACKING_DATA.size(); q++)
      {
        line += snprintftos("%6.2f%%  ",PACKING_DATA.percent(p,q));
      }
      line += snprintftos("  :  %6.2f%% ",PACKING_DATA.overlap_percent(p));
      output.push_back(line);
    }
    }}
    output.push_back("");
    return output;
  }

} //phasertng
