//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Clmn_class__
#define __phasertng_Clmn_class__
#include <phasertng/src/Elmn.h>

namespace phasertng {

  class Clmn
  {
    public:
      elmn_array_t clm1m2;

    public:
      Clmn() {}

    public:
      void clmnx(const elmn_array_t&,const elmn_array_t&,const int,const int);
  };

}
#endif
