#ifndef __phasertng_moments_type_enum__
#define __phasertng_moments_type_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace moments {
  enum type {
              DATA,
              ANISOTROPY,
              TNCS
            };
/*
}}
  MakeEnum ( type,
              (DATA)
              (ANISOTROPY)
              (TNCS)
            );
*/
}}
#endif
