//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/NodeList.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/hoist.h>
#include <phasertng/src/SpaceGroup.h>
#include <map>
#include <set>
#include <unordered_set>

typedef  std::vector<dvect3> sv_dvect3;
typedef  std::vector<std::string> sv_string;

namespace phasertng {
namespace dag {

  bool
  NodeList::is_all_turn() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (!node.NEXT.turn_present() and !node.NEXT.gyre_present()) return false;
      //may or may not have pose
      //may or may not have gyre (in addition to turn)
    }
    return true;
  }

  bool
  NodeList::is_all_gyre() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (!node.NEXT.gyre_present()) return false;
      //may or may not have pose
    }
    return true;
  }

  bool
  NodeList::is_all_curl() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (!node.NEXT.TRANSLATED) return false;
      //may or may not have pose
    }
    return true;
  }

  bool
  NodeList::is_all_pose() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (node.NEXT.PRESENT) return false;
      if (!node.POSE.size()) return false;
    }
    return true;
  }

  bool
  NodeList::has_no_turn_curl_pose() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (node.NEXT.PRESENT) return false;
      if (node.POSE.size()) return false;
    }
    return true;
  }

  bool
  NodeList::is_all_full_or_part() const
  {
    if (!node_list.size()) return false;
    for (const auto& node: node_list)
    {
      if (!node.FULL.PRESENT and !node.PART.PRESENT) return false;
      if (node.NEXT.PRESENT) return false;
     // if (node.POSE.size()) return false;
     //can have poses, because may want to recycle to frf after rbm and sbr (which create full)
    }
    return true;
  }

  bool
  NodeList::has_all_modlid_the_same() const
  {
    if (!node_list.size()) return false;
    const auto& first_node = node_list.front();
    for (const auto& node: node_list)
    {
      if (first_node.POSE.size() != node.POSE.size())
        return false;
      for (int p = 0; p < node.POSE.size(); p++)
        if (node.POSE[p].IDENTIFIER != first_node.POSE[p].IDENTIFIER)
          return false;
      if (node.NEXT.PRESENT != first_node.NEXT.PRESENT)
        return false;
      if (node.NEXT.IDENTIFIER != first_node.NEXT.IDENTIFIER)
        return false;
      if (node.FULL.PRESENT != first_node.FULL.PRESENT)
        return false;
      if (node.FULL.IDENTIFIER != first_node.FULL.IDENTIFIER)
        return false;
      if (first_node.used_modlid() != node.used_modlid())
        return false;
    }
    return true;
  }

  af::shared<Identifier>
  NodeList::list_of_modlid() const
  {
    af::shared<Identifier> res;
    auto tmp = used_modlid();
    for (auto item : tmp)
      res.push_back(item);
    return res;
  }

  setmodlid_t
  NodeList::used_modlid() const
  {
    setmodlid_t modlid;
    if (!node_list.size()) return modlid;
    for (const auto& node: node_list)
    {
      for (int h = 0; h < node.HOLD.size(); h++)
        modlid.insert(node.HOLD[h].IDENTIFIER);
      for (int s = 0; s < node.SEEK.size(); s++)
        modlid.insert(node.SEEK[s].IDENTIFIER);
      if (node.NEXT.PRESENT)
        modlid.insert(node.NEXT.IDENTIFIER);
      for (int p = 0; p < node.POSE.size(); p++)
        modlid.insert(node.POSE[p].IDENTIFIER);
      if (node.FULL.PRESENT)
        modlid.insert(node.FULL.IDENTIFIER);
      if (node.PART.PRESENT)
        modlid.insert(node.PART.IDENTIFIER);
    }
    return modlid;
  }

  setmodlid_t
  NodeList::used_modlid_pose(std::string pick) const
  {
    setmodlid_t modlid;
    if (!node_list.size()) return modlid;
    for (const auto& node: node_list)
    {
      for (int p = 0; p < node.POSE.size(); p++)
        modlid.insert(node.POSE[p].IDENTIFIER);
      if (pick == "turn" and node.NEXT.PRESENT)
        modlid.insert(node.NEXT.IDENTIFIER);
      if (pick == "gyre" or pick == "turn" or pick == "curl") //if gyres also defined use these too
        for (int g = 0; g < node.NEXT.ngyre(); g++)
          modlid.insert(node.NEXT.GYRE[g].IDENTIFIER);
      if (pick == "curl" and node.NEXT.PRESENT and node.NEXT.TRANSLATED)
        modlid.insert(node.NEXT.IDENTIFIER);
      if (pick == "seek")
        for (int s = 0; s < node.SEEK.size(); s++)
          modlid.insert(node.SEEK[s].IDENTIFIER);
    }
    return modlid;
  }

  setmodlid_t
  NodeList::used_modlid_full_and_part() const
  {
    setmodlid_t modlid;
    if (!node_list.size()) return modlid;
    for (const auto& node: node_list)
    { //either full molecular replacement or partial substructure
      if (node.FULL.PRESENT) modlid.insert(node.FULL.IDENTIFIER);
      if (node.PART.PRESENT) modlid.insert(node.PART.IDENTIFIER);
    }
    return modlid;
  }

  //frf result has each angle as a separate node
  //linked by the parent
  af_string
  NodeList::add_pose_orientations()
  {
    std::map<uuid_t,std::tuple<uuid_t,sv_dvect3,std::string>> parent_orientations;
    //loop over the genealogies, which will have the same set of poses
    dag::NodeList added_nodes;
    for (const auto& node: node_list)
    {
      //work out if there is a poseid with the turnid, in which case it is interesting
      //and the poseid must be added to the list
      //there could be more than one interesting pose ie, more that one poseid==turnid
      Identifier turnid = node.NEXT.IDENTIFIER;
      if (parent_orientations.count(node.PARENTS.back()))
        continue; //we had already seen this set of poses, so no need to add again
      sv_dvect3 orientations;
      for (int p = 0; p < node.POSE.size(); p++)
      {
        Identifier poseid = node.POSE[p].IDENTIFIER;
        if (poseid == turnid)
        {
          bool same_angle(false);
          double tol_angle(1.0);
          for (int i = 0; i < orientations.size(); i++)
            if (euler_delta_deg(orientations[i],node.POSE[p].EULER) < tol_angle)
              same_angle = true;
          if (!same_angle)
          {
            dag::Node next = node; //copy this node
            //overwrite the copy of the NODE with the pose euler in place of the gyre euler
            //retains the parent information
            next.NEXT.EULER = node.POSE[p].EULER;
            added_nodes.push_back(next);
            orientations.push_back(next.NEXT.EULER); //for logging,unique test
          }
        }
      }
      parent_orientations[node.PARENTS.back()] = std::make_tuple(node.POSE.size(),orientations,node.HALL); //init
    }
    for (const auto& node: added_nodes)
      node_list.push_back(node);
    //generate table of results
    af_string output;
    bool first(true);
    for (const auto& po : parent_orientations)
    {
      int npose; sv_dvect3 orientations; std::string spacegroup;
      std::tie(npose,orientations,spacegroup) = po.second;
      char buf[80];
      if (orientations.size())
      {
        snprintf(buf,80,"%-11s %-13s %4s %-3s %7s %7s %7s",
            "Parent","Space Group","Max#","#","Euler1","Euler2","Euler3");
        if (first)
          output.push_back(std::string(buf));
        first = false;
        std::string pofirststr = std::to_string(po.first);
        snprintf(buf,80,"%-11s %-13s %-4d",
            pofirststr.c_str(),SpaceGroup(spacegroup).CCP4.c_str(),npose);
        output.push_back(std::string(buf)); //%lu very important!
      }
      int i(1);
      for (const auto& euler : orientations)
      {
        snprintf(buf,80,"%11s %13s %4s %-3d % 7.1f % 7.1f % 7.1f",
            "","","",i++,euler[0],euler[1],euler[2]);
        output.push_back(std::string(buf));
      }
    }
    return output;
  }

  void
  NodeList::apply_clear_last_tncs_group_pose(std::string annotation)
  {
    dag::NodeList new_node_list;
    for (const auto& node: node_list)
    {
      dag::Node next = node;
      next.clear_last_tncs_group_pose();
      if (next.ANNOTATION.size())
      {
        std::istringstream ss(next.ANNOTATION);
        std::string word; // for storing each word
        af_string txt;
        while (ss >> word)
        { //break lines at RF each time
          if (word.substr(0,5) == "FIND=" or word.substr(0,5) == "FUSE=" or word.substr(0,4) == "JOG=")
            txt.push_back("");
          txt.back() += " " + word;
        }
        next.ANNOTATION = "";
        for (int t = 0; t < txt.size()-1; t++)
          next.ANNOTATION += txt[t];
        next.ANNOTATION += " " + annotation;
      }
      next.set_star_from_pose();
      new_node_list.push_back(next); //copy
    }
    node_list = new_node_list.node_list;
  }
/*
  void
  NodeList::apply_background_reduction_to_gyre()
  {
    dag::NodeList new_node_list;
    for (const auto& node: node_list)
    {
      dag::Node next = node;
      next.convert_last_pose_to_gyre();
      new_node_list.push_back(next); //copy
    }
    node_list = new_node_list.node_list;
  }

  void
  NodeList::apply_background_reduction_to_curl()
  {
    dag::NodeList new_node_list;
    for (const auto& node: node_list)
    {
      dag::Node next = node;
      next.convert_last_pose_to_curl();
      new_node_list.push_back(next); //copy
    }
    node_list = new_node_list.node_list;
  }
*/

  void
  NodeList::apply_unit_cell(cctbx::uctbx::unit_cell CELL)
  {
    for (auto& node: node_list)
      node.CELL = CELL;
  }

  bool
  NodeList::all_space_groups_the_same() const
  {
    if (!node_list.size()) return false;
    std::string hallref = node_list.front().HALL;
    for (const auto& node: node_list)
    {
      if (node.HALL != hallref) return false;
    }
    return true;
  }

  int
  NodeList::number_of_poses()
  {
    //check that all are first tf
    //returns the first non-zero but should all be the same
    for (const auto& node: node_list)
    {
      if (node.POSE.size())
        return node.POSE.size();
    }
    return 0;
  }

  int
  NodeList::number_of_holds()
  {
    //check that all are first tf
    //returns the first non-zero but should all be the same
    for (const auto& node: node_list)
    {
      if (node.HOLD.size())
        return node.HOLD.size();
    }
    return 0;
  }

  int
  NodeList::number_of_seeks()
  {
    //check that all are first tf
    //returns the first non-zero but should all be the same
    for (const auto& node: node_list)
    {
      if (node.SEEK.size())
        return node.SEEK.size();
    }
    return 0;
  }

  double
  NodeList::top_llg() const
  {
    double top = std::numeric_limits<double>::lowest();
    for (const auto& item : node_list)
      top = std::max(top,item.LLG);
    return top;
  }

  double
  NodeList::low_llg() const
  {
    double low = std::numeric_limits<double>::max();
    for (const auto& item : node_list)
      low = std::min(low,item.LLG);
    return low;
  }

  std::pair<bool,double>
  NodeList::top_novel_turn_llg_packs() const
  {
    if (!node_list.size()) return {false, 0 };
    double top = std::numeric_limits<double>::lowest();
    pod::pakflag pak_flag;
    for (const auto& item : node_list)
      if (!item.turn_in_poses())
        if (pak_flag.pakset(item.PACKS) and pak_flag.packs_YU(item.PACKS))
          top = std::max(top,item.LLG);
    if (top == std::numeric_limits<double>::lowest())
      return { false, top }; //yes return lowest, for (no) pruning
    return { true, top };
  }

  double
  NodeList::top_mapllg() const
  {
    double top = std::numeric_limits<double>::lowest();
    for (const auto& item : node_list)
      top = std::max(top,item.MAPLLG);
    return top;
  }

  double
  NodeList::top_tfz_packs() const
  {
    if (!node_list.size()) return 0;
    double top = std::numeric_limits<double>::lowest();
    bool none_pack(true);
    pod::pakflag pak_flag;
    for (const auto& item : node_list)
      if (pak_flag.pakset(item.PACKS) and pak_flag.packs_YU(item.PACKS))
        { none_pack = false; break; }
    for (const auto& item : node_list)
      if (none_pack or pak_flag.packs_YU(item.PACKS))
        top = std::max(top,item.ZSCORE);
    PHASER_ASSERT(top != std::numeric_limits<double>::lowest());
    return top;
  }

  std::pair<bool,double>
  NodeList::top_llg_packs() const
  {
    double top = std::numeric_limits<double>::lowest();
    if (!node_list.size()) return { false, top };
    pod::pakflag pak_flag;
    for (const auto& item : node_list)
      if (pak_flag.pakset(item.PACKS) and pak_flag.packs_YU(item.PACKS))
        top = std::max(top,item.LLG);
    if (top == std::numeric_limits<double>::lowest())
      return { false, top }; //yes return lowest, for (no) pruning
    return  { true, top };
  }

  double
  NodeList::top_tfz() const
  {
    if (!node_list.size()) return 0;
    double top = std::numeric_limits<double>::lowest();
    for (const auto& item : node_list)
      top = std::max(top,item.ZSCORE);
    return top;
  }

  void
  NodeList::apply_sort_llg()
  {
    if (!node_list.size()) return;
    auto cmp = []( const dag::Node &a, const dag::Node &b )
         { return a.LLG >  //> for reverse sort
                  b.LLG; };
    std::sort( node_list.begin(), node_list.end(),cmp);
  }

  void
  NodeList::apply_sort_mapllg()
  {
    if (!node_list.size()) return;
    auto cmp = []( const dag::Node &a, const dag::Node &b )
         { return a.MAPLLG >  //> for reverse sort
                  b.MAPLLG; };
    std::sort( node_list.begin(), node_list.end(),cmp);
  }

  void
  NodeList::apply_partial_sort_and_maximum_stored_llg(const int io_maxstored)
  {
    //if (io_maxstored == 0) return; //cutoff not set
    if (!node_list.size()) return;
    // lambda function for explict sort on parameter
    auto cmp = []( const dag::Node &a, const dag::Node &b )
         { return a.LLG >  //> for reverse sort
                  b.LLG; };
    if (!io_maxstored or node_list.size() < io_maxstored)
    {
      std::sort( node_list.begin(), node_list.end(), cmp);
    }
    else
    {
      std::partial_sort( node_list.begin(), node_list.begin()+io_maxstored, node_list.end(), cmp);
      node_list.erase( node_list.begin()+io_maxstored, node_list.end());
    }
  }

  void
  NodeList::apply_maximum_stored(const int io_maxstored)
  {
    if (io_maxstored == 0) return; //cutoff not set
    if (!node_list.size()) return;
    if (io_maxstored > node_list.size()) return;
    node_list.erase( node_list.begin()+io_maxstored, node_list.end());
  }

  void
  NodeList::apply_partial_sort_and_maximum_stored_mapllg(const int io_maxstored)
  {
   // if (!io_maxstored == 0) return; //cutoff not set
    if (!node_list.size()) return;
    // lambda function for explict sort on parameter
    auto cmp = []( const dag::Node &a, const dag::Node &b )
         { return a.MAPLLG >  //> for reverse sort
                  b.MAPLLG; };
    if (!io_maxstored or node_list.size() < io_maxstored)
    {
      std::sort( node_list.begin(), node_list.end(), cmp);
    }
    else
    {
      std::partial_sort( node_list.begin(), node_list.begin()+io_maxstored, node_list.end(), cmp);
      node_list.erase(node_list.begin()+io_maxstored,node_list.end());
    }
  }

  double
  NodeList::apply_partial_sort_and_percent_llg(
      const double io_percent,
      const double f_max,
      const double mean)
  {
    int num(0); //counter for erase
    PHASER_ASSERT(f_max > mean);
    double cutoff = (io_percent/100.)*(f_max-mean)+mean;
    for (int i = 0; i < node_list.size(); i++)
    {
      auto& item = node_list[i];
      if (item.LLG >= cutoff) //sort this one to the front
        node_list[num++] = item;
    }
    node_list.erase( node_list.begin()+num, node_list.end());
    return cutoff;
  }

  double
  NodeList::average_llg()
  {
    double sum(0);
    for (int i = 0; i < node_list.size(); i++)
      sum += (node_list[i].LLG);
    return sum/node_list.size();
  }

  double
  NodeList::apply_partial_sort_and_percent_llg_range(
      const std::pair<double,double> io_percent,
      const double f_max,
      const double mean)
  {
    //all pack in this case, frf
    int num(0); //counter for erase
    PHASER_ASSERT(f_max > mean);
    double cutoff = std::min(io_percent.first,io_percent.second);
           cutoff = (cutoff/100.)*(f_max-mean)+mean;
    double cutoff_max = std::max(io_percent.first,io_percent.second);
    double DEF_PPM = 1.0e-06;
    if (cutoff_max > 99) //ie 100
      cutoff_max = top_llg()+DEF_PPM; //may be > 100% because we use top_rf_llg, ignoring pose turns
    else //when we have not selected 100 as the top
      cutoff_max = (cutoff_max/100.)*(f_max-mean)+mean+DEF_PPM; //add tolerance
    for (int i = 0; i < node_list.size(); i++)
    {
      auto& item = node_list[i];
      if (item.LLG <= cutoff_max and item.LLG >= cutoff) //sort this one to the front
        node_list[num++] = item;
    }
    node_list.erase( node_list.begin()+num, node_list.end());
    return cutoff;
  }

  double
  NodeList::apply_partial_sort_and_percent_mapllg(const double io_percent,const double mean)
  {
    int num(0); //counter for erase
    double f_max = top_mapllg();
    PHASER_ASSERT(f_max > mean);
    double cutoff = (io_percent/100.)*(f_max-mean)+mean;
    for (int i = 0; i < node_list.size(); i++)
    {
      auto& item = node_list[i];
      if (item.MAPLLG >= cutoff) //sort this one to the front
        node_list[num++] = item;
    }
    node_list.erase( node_list.begin()+num, node_list.end());
    return cutoff;
  }

  void
  NodeList::flag_tncs_duplicates(dvect3 tncs_vector)
  {
    if (node_list.size() == 0) return;
    auto* WORK = &node_list[0];
    int nmol = WORK->TNCS_ORDER; //same for all
    dmat33 orthmat = WORK->CELL.orthogonalization_matrix();
    if (!(WORK->POSE.size() == 0 and nmol > 2))
      return;
    if (!WORK->NEXT.TRANSLATED)
      return;
    for (int i = 0; i < node_list.size(); i++)
      node_list[i].generic_flag = true;
    for (int i = 0; i < node_list.size(); i++)
    {
      auto fraci = node_list[i].NEXT.FRACT;
      if (!node_list[i].generic_flag) continue;
      for (int j = i+1; j < node_list.size(); j++)
      {
        if (!node_list[j].generic_flag) continue;
        auto fracj = node_list[j].NEXT.FRACT;
        for (double n = -nmol; n <= nmol; n++) //plus and minus shifts
        {
          auto tncsv = fracj + n*tncs_vector;
          auto shift = fraci - tncsv;
          auto orth = orthmat*shift;
          auto dist = std::sqrt(orth*orth);
          if (dist < 1)
          {
            node_list[j].generic_flag = false;
            node_list[j].generic_int2 = i;
            break;
          }
        }
      }
    }
  }

  void
  NodeList::erase_invalid()
  {
    if (!node_list.size()) return;
    size_t last = 0;
    //fill start with interesting, in order
    for (size_t i = 0; i < node_list.size(); i++)
    { if (node_list[i].generic_flag) { node_list[last++] = node_list[i]; } }
    node_list.erase(node_list.begin() + last, node_list.end()); //erase the end, which is not rubbish
  }

  int
  NodeList::llg_precision(boost::logic::tribool map) const
  {
    PHASER_ASSERT(!boost::logic::indeterminate(map));
    if (!size()) return 0;
    double ftop = map ? node_list.front().MAPLLG : node_list.front().LLG;
           ftop = std::max(1.,std::abs(ftop));
    int w = std::floor(std::log10(ftop));
    int p = std::max(4. - w,2.);
    //two decimals bc for sequential rounding we should not see x.48 -> x.5 -> x+1 on conversion to int for annotation versus x
    return p;
  }

  int
  NodeList::llg_width(boost::logic::tribool map) const
  {
    PHASER_ASSERT(!boost::logic::indeterminate(map));
    if (!size()) return 0;
    double ftop = map ? node_list.front().MAPLLG : node_list.front().LLG;
           ftop = std::max(10.,std::abs(ftop));
    int w = std::ceil(std::log10(ftop));
        w = w+1+1+1+1+llg_precision(map);
    return w;
  }

  int
  NodeList::number_of_parents() const
  {
    std::unordered_set<uuid_t> parents;
    for (const auto& node: node_list)
      if (node.PARENTS.size())
        parents.insert(node.PARENTS.back());
    return parents.size();
  }

  bool
  NodeList::multiple_spacegroups() const
  {
    std::unordered_set<std::string> sg;
    for (const auto& node: node_list)
      sg.insert(node.HALL);
    return bool(sg.size() > 1);
  }

  bool
  NodeList::multiple_vrms_values()
  {
    std::set<double> rms;
    for (const auto& node: node_list)
      rms.insert(node.VRMS_VALUE.at(node.NEXT.IDENTIFIER).VAL);
    //values that are withing about 0.1 of each other are probably not significant for llg
    assert(rms.size());
    double rmsend = *rms.rbegin(); //no end()
    double rmstop = *rms.begin();
    return bool((rmsend-rmstop) > 0.1);
  }

  void
  NodeList::set_reflid(Identifier reflid)
  {
    for (int i = 0; i < node_list.size(); i++)
      node_list[i].REFLID = reflid;
  }

  void
  NodeList::set_hall(std::string hall)
  {
    for (int i = 0; i < node_list.size(); i++)
      node_list[i].HALL = hall;
  }

  void
  NodeList::apply_sort_for_search()
  {
    auto cmp = []( const Node &a, const Node &b )
    { //comparitors are for "which is better -> higher in the list"
      //we have stored the array of seek_increases in the generic_str
      //split it out to words and convert back to doubles to get the array
      sv_string astr,bstr;
      hoist::algorithm::split(astr, a.generic_str,(" "));
      hoist::algorithm::split(bstr, b.generic_str,(" "));
      int n = std::min(astr.size(),bstr.size());
      for (int i = 0; i < n; i++)
      {
        double aint = std::atof(astr[i].c_str());
        double bint = std::atof(bstr[i].c_str());
        if (aint != bint)
          return aint > bint; //reverse sort
      }
      if (a.SEEK_COMPONENTS != b.SEEK_COMPONENTS)
        return a.SEEK_COMPONENTS < b.SEEK_COMPONENTS;
      if (a.SEEK.size() != b.SEEK.size())
        return a.SEEK.size() < b.SEEK.size();
      return a.SEEK_COMPONENTS_ELLG > b.SEEK_COMPONENTS_ELLG;
    };
    std::sort(node_list.begin(),node_list.end(),cmp);
    for (int i = 0; i < node_list.size(); i++)
      node_list[i].generic_flag = true;
    for (int i = 0; i < node_list.size(); i++)
    {
      auto& a = node_list[i];
      for (int j = 0; i < j; i++)
      {
        auto& b = node_list[j];
        if (a.SEEK_COMPONENTS == b.SEEK_COMPONENTS)
        {
          int n = a.SEEK_COMPONENTS;
          bool match(true);
          for (int s = 0; s < n; s++)
            if (a.SEEK[s].IDENTIFIER != b.SEEK[s].IDENTIFIER)
              match = false;
          if (match) node_list[i].generic_flag = false;
        }
      }
    }
  }

  bool
  NodeList::is_all_tncs_modelled() const
  {
    for (const auto& node: node_list)
    {
      if (node.TNCS_MODELLED) return true;
    }
    return false;
  }

  std::tuple<std::string,std::string,Identifier>
  NodeList::duplicate_equivalent(int i) const
  {
    //return string because this conversion is already boosted
    for (int k = i+1; k < node_list.size(); k++)
      if (node_list[k].EQUIV == i)
        return std::make_tuple( std::to_string(node_list[k].RFACTOR),std::to_string(node_list[k].ZSCORE), node_list[k].FULL.IDENTIFIER );
    return std::make_tuple( "-999","-999", Identifier() );
  }

}} //phasertng
