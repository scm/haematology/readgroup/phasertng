//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/ElmnData.h>
#include <phasertng/site/FastRotationFunction.h>
#include <phasertng/site/PointsStored.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Sequence.h>

namespace phasertng {

  //Self_rotation_function
  void Phasertng::runSRF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::FEFF))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (feff)");
    if (!(DAGDB.NODES.is_all_pose() or DAGDB.NODES.has_no_turn_curl_pose()))
    throw Error(err::INPUT,"Dag not prepared with correct mode");

    ReflColsMap P1;
    SpaceGroup SG = REFLECTIONS->SG;
    {{
      //change REFLECTIONS to point to P1 above
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Expand Reflections to P1");
      logTab(where,"Original Number of Reflections: " + itos(REFLECTIONS->NREFL));
      P1.copy_and_reformat_data(REFLECTIONS.get()); //this is a copy
      bool change_spacegroup_to_p1(true);
      P1.expand_to_p1(change_spacegroup_to_p1);
      DAGDB.WORK->SG = nullptr; //explicit set here
      DAGDB.reset_entry_pointers(DataBase()); //which resets space group also
      logTab(where,"Expanded Number of Reflections: " + itos(P1.NREFL));
      logTab(where,"Resolution: " + dtos(P1.data_hires(),2));
    }}
    REFLECTIONS->copy_and_reformat_data(&P1);

    ReflColsMap CROSS; //default self-rotation
    ReflectionsPtr CROSSPTR = std::shared_ptr<ReflColsMap>(&CROSS,[](ReflColsMap *) {});
    if (!input.value__path.at(".phasertng.cross_rotation_function.filename.").is_default())
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Cross Rotation Function");
      logTab(where,"Use file prepared with voyager.nacelle");
      FileSystem ref;
      ref.SetFileSystem(input.value__path.at(".phasertng.cross_rotation_function.filename.").value_or_default());
      logTab(where,"Data read from mtz file: " + ref.qfstrq());
      logTab(where,"Read: " + ref.fstr());
      //CROSS.reset(new ReflColsMap()); //this is a copy
      CROSS.SetFileSystem(ref.database,{ref.filepath,ref.filename});
      if (!CROSS.file_exists()) //throws deep within vanilla read core dump
        throw Error(err::FILEOPEN,CROSS.fstr());
      if (CROSS.is_a_directory()) //throws deep within vanilla read core dump
        throw Error(err::FILEOPEN,CROSS.fstr());
      std::set<labin::col> cols = {
      labin::FEFFNAT,
      labin::DOBSNAT,
      labin::RESN,
      labin::TEPS,
      labin::CENT,
      labin::SSQR,
      labin::EPS //,
      //labin::PHSR, //required for it to be reflections
      //labin::BIN //required for it to be reflections
      };
      af_string warning = CROSS.VanillaReadMtz(
          cols //default {} all columns, set by function overload below
             //as we can't use default values in declaration for std::set
      );
      if (warning.size())
        logTabArray(where,warning);
      if (!CROSSPTR->prepared(reflprep::FEFF))
      throw Error(err::INPUT,"Reflections not prepared with correct mode (feff)");
      logTab(where,"Space Group = " + CROSSPTR->SG.CCP4 + " (" + CROSSPTR->SG.HALL + ")");
      logTab(where,"Unit Cell =  " + dvtos(CROSSPTR->UC.cctbxUC.parameters()));
      logTab(where,"Original Number of Reflections: " + itos(CROSSPTR->NREFL));
      bool change_spacegroup_to_p1(true);
      CROSSPTR->expand_to_p1(change_spacegroup_to_p1);
      logTab(where,"Expanded Number of Reflections: " + itos(CROSSPTR->NREFL));
      double cross_hires = CROSSPTR->data_hires();
      logTab(where,"Resolution: " + dtos(cross_hires,2));
    //we want to make sure the data with the lowest resolution as the REFLECTIONS type,
    //so that the resolution from the 'model' aka cross-data is always higher
      if (cross_hires > REFLECTIONS->data_hires())
      throw Error(err::INPUT,"Invert reflections/cross-reflections for resolution limits");
    }
    else
    {
      CROSSPTR->copy_and_reformat_data(&P1);
     // CROSSPTR = std::shared_ptr<ReflColsMap>(&P1,[](ReflColsMap *) {});
    }
    if (Suite::Extra())
    {
      logTabArray(out::TESTING,CROSSPTR->logReflections("Cross"));
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Reference"));
    }

    double srf_radius(0);
    if (!input.value__path.at(".phasertng.self_rotation_function.model.filename.").is_default())
    {
      logUnderLine(out::LOGFILE,"Radius from Model");
      auto fpdb = input.value__path.at(".phasertng.self_rotation_function.model.filename.").value_or_default();
      logTab(out::LOGFILE,"model: " + fpdb);
      Models models;
      models.SetFileSystem(input.value__path.at(".phasertng.self_rotation_function.model.filename.").value_or_default());
      models.read_from_disk(); ///ReadPdb !!!
      models.set_principal_statistics();
      double pdbrad = std::round(models.statistics.MEAN_RADIUS); //which is 90%
      logTab(out::LOGFILE,"Maximum Radius = " + dtos(pdbrad,0) + " A");
      srf_radius = pdbrad;
      double radius_factor = input.value__flt_number.at(".phasertng.self_rotation_function.model.integration_radius_factor.").value_or_default();
      logTab(out::LOGFILE,"Radius factor applied = x" + dtos(radius_factor));
      srf_radius *= radius_factor;
      logTab(out::LOGFILE,"Model Extent Ratio: " + dtos(models.statistics.EXTENT_RATIO,2));
    }
    else if (!input.value__path.at(".phasertng.self_rotation_function.sequence.filename.").is_default())
    {
      logUnderLine(out::LOGFILE,"Radius from Sequence");
      auto fseq = input.value__path.at(".phasertng.self_rotation_function.sequence.filename.").value_or_default();
      logTab(out::LOGFILE,"Sequence: " + fseq);
      bool selenomethionine(false);
      Sequence sequence(selenomethionine);
      sequence.SetFileSystem(fseq);
      sequence.ReadSeq();
      sequence.initialize(true,true);
      logTabArray(out::LOGFILE,sequence.logSequence(""));
      double seqrad = std::round(sequence.minimum_radius_from_molecular_weight());
      logTab(out::LOGFILE,"Molecular Weight = " + dtos(sequence.total_molecular_weight(),1) + " Da");
      logTab(out::LOGFILE,"Radius from Molecular Weight = " + dtos(seqrad,0) + " A");
      srf_radius = seqrad;
      double radius_factor = input.value__flt_number.at(".phasertng.self_rotation_function.sequence.integration_radius_factor.").value_or_default();
      logTab(out::LOGFILE,"Radius factor applied = x" + dtos(radius_factor,0));
      srf_radius *= radius_factor;
    }
    else
    {
      srf_radius = input.value__flt_number.at(".phasertng.self_rotation_function.integration_radius.").value_or_default();
      logUnderLine(out::LOGFILE,"Radius");
      logTab(out::LOGFILE,"Radius input " + dtos(srf_radius) + " A");
    }
#if 0
//do not do this, it gives inferior results
    double mindim = REFLECTIONS->UC.minimum_cell_dimension();
    if (srf_radius > mindim/2.)
    {
      logTab(out::LOGFILE,"Minimum cell dimension restriction applied to radius = half of " + dtos(mindim));
      srf_radius = std::min(srf_radius,mindim/2.);
    }
#endif

    double percent_of_highest_ncs = 75;
    int io_maxstored = input.value__int_number.at(".phasertng.self_rotation_function.maximum_stored.").value_or_default();
    int io_nprint = input.value__int_number.at(".phasertng.self_rotation_function.maximum_printed.").value_or_default();
    double io_percent = input.value__percent.at(".phasertng.self_rotation_function.percent.").value_or_default();
    bool io_use_cluster =  true;
    int io_cluster_back = 10000;
    double io_sampling = 3;
    double io_maxorder = input.value__flt_number.at(".phasertng.self_rotation_function.maximum_order.").value_or_default();
    auto io_reso = REFLECTIONS->resolution(); //0=lores 1=hires
    io_reso[1] = std::max(io_reso[1],
      input.value__flt_number.at(".phasertng.self_rotation_function.resolution.").value_or_default());
    io_reso[1] = std::max(io_reso[1],
      input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    double hires = io_reso[1];
    logTab(out::LOGFILE,"Rotation function resolution: " + dtos(io_reso[0],4) + " " + dtos(hires,4));

    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    bool store_elmn(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR,store_elmn);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        hires,
        DAGDB.resolution_range(),
        hires);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected("Data"));
    if (Suite::Extra())
    {
      logTabArray(out::TESTING,selected.logOutliers("Data"));
    }
    }}
    Selected selected_cross(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected_cross.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected_cross.set_resolution(
        hires,
        DAGDB.resolution_range(),
        hires);
    logTabArray(out::LOGFILE,seltxt);
    selected_cross.flag_standard_outliers_and_select(CROSSPTR.get());
    if (Suite::Extra())
    {
      logTabArray(out::LOGFILE,selected_cross.logSelected("Cross Data"));
      logTabArray(out::TESTING,selected_cross.logOutliers("Cross Data"));
    }
    }}

    phaser_assert(DAGDB.size());
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Angle Convention");
    logTab(where,"Follows ISO-standard convention, same as used by e.g. MOLREP");
    logTab(where,"Rotation by Polar Angles Theta, Phi, Chi:");
    logTab(where,"Theta: angle between rotation axis and Z; zenith (math phi)");
    logTab(where,"Phi  : angle in plane XY between X and projection of rotation axis; azimuth (math theta)");
    logTab(where,"Chi  : rotation angle around rotation axis");
    logBlank(where);
    }}

    {
    out::stream where = out::LOGFILE;
    std::string nN = "";
    logUnderLine(out::LOGFILE,"Self Rotation Function"+nN);
    double sphereOuter = srf_radius; //maximum vector
    sphereOuter = EntryHelper::sphereOuter(sphereOuter,hires); //within lmax limits
    if (sphereOuter < srf_radius)
      logTab(out::LOGFILE,"Radius restricted by Lmax=100");
    logTab(out::LOGFILE,"SRF Radius = " + dtos(sphereOuter/2.,1) + " A");
    logTab(out::LOGFILE,"SRF Point Group = " + SG.pgname());
    logTab(out::LOGFILE,"SRF Percent = " + dtos(io_percent,5,1) + "%");

    int lowsymm(1);
    std::pair<int,char> highsymm = REFLECTIONS->SG.highOrderAxis();
    double cluster_angle(0),sampling(0);
    //integration radius is twice the molecular radius
    int lmax = EntryHelper::calculate_lmax(sphereOuter,hires);
    double helix_sampling_factor(0); bool helix(false),dupl_only(false);
    logTab(out::VERBOSE,"Integration sphere is twice the molecular radius");
    std::tie(cluster_angle,sampling) = EntryHelper::clustang(
        selected.STICKY_HIRES,
        io_sampling,
        helix_sampling_factor,
        highsymm.first,
        sphereOuter,
        helix,
        dupl_only
      );
    double maxorder = input.value__flt_number.at(".phasertng.self_rotation_function.maximum_order.").value_or_default();
    double io_cluster_angle = 360./(maxorder*2);
    cluster_angle = std::max(io_cluster_angle,cluster_angle);

    {{
    logChevron(where,"Elmn Calculation"+nN);
    logTab(where,"Highest symmetry of multiplicity "+itos(highsymm.first)+" around axis "+chtos(highsymm.second));
    logTab(where,"Sampling Angle: " + dtos(sampling,5,2) + " degrees");
    logTab(where,"Cluster Angle:  " + dtos(cluster_angle,5,2) + " degrees");
    logTab(where,"Calculating Elmn with resolution " + dtos(hires));
    logTab(where,"Maximum degree of spherical harmonics " + itos(lmax));
    logTab(where,"Highest symmetry of multiplicity "+itos(highsymm.first)+" around axis "+chtos(highsymm.second));
    }}

    //For the rotating copy the data has no symmetry and is in original indexing ('Z')
    ElmnData elmn_ensemble(lmax,{1,'Z'});
    elmn_ensemble.set_nthreads( NTHREADS,USE_STRICTLY_NTHREADS);

    ElmnData elmn_data(lmax,highsymm);
    elmn_data.set_nthreads( NTHREADS,USE_STRICTLY_NTHREADS);

    if (!DAGDB.NODES.is_all_tncs_modelled())
    {
      logTab(where,"Translational Ncs: Rotational spacegroup changed to P1");
      logBlank(where);
    }

    bool cluster_in_p1(true); //if not best, apply the symmetry to the clustering
    PHASER_ASSERT(REFLECTIONS->SG.NSYMP == 1); // so the cluster_in_p1 is redundant
    FastRotationFunction rotfun(REFLECTIONS.get(),cluster_in_p1);
    rotfun.SG = SG; //copy original space group
    rotfun.set_nthreads(NTHREADS,USE_STRICTLY_NTHREADS);

    {{
    logEllipsisOpen(where,"Calculate elmn#1"+nN);
    //there is no pose data
    pod::FastPoseType blank_ecalcs_sigmaa_pose;
    auto txt1 = elmn_data.ELMNxR2(
        sphereOuter,
        REFLECTIONS.get(),
        selected.get_selected_array(),
        &epsilon,
        DAGDB.WORK->TNCS_ORDER,
        false, //tncs_not_modelled, false because it is in the data!
        hires,
        &blank_ecalcs_sigmaa_pose
      );
    logEllipsisShut(where);
    if (Suite::Extra())
      logTabArray(where,txt1);
    logEllipsisOpen(where,"Calculate elmn#2"+nN);
    auto txt2 = elmn_ensemble.ELMNxR2(
        sphereOuter,
        CROSSPTR.get(),
        selected_cross.get_selected_array(),
        &epsilon,
        DAGDB.WORK->TNCS_ORDER,
        false, //tncs_not_modelled, false because it is in the data!
        hires,
        &blank_ecalcs_sigmaa_pose
      );
    logEllipsisShut(where);
    if (Suite::Extra())
      logTabArray(where,txt2);
    }}

    bool calculate_stats(true);
    {{
    rotfun.rotation_function_data.clear();
    logEllipsisOpen(where,"Scanning the Range of Beta Angles"+nN);
    bool store_full_map(true);
    auto txt = rotfun.calculate_fourier_transforms(
        elmn_data.ELMN,
        elmn_ensemble.ELMN,
        lmax,
        sampling,
        lowsymm,
        calculate_stats,
        store_full_map
      );
    logEllipsisShut(where);
    if (Suite::Extra())
      logTabArray(out::TESTING,txt);
    }}

    {{
    logEllipsisOpen(where,"Merge Beta Sections and Sort"+nN);
    bool clear_data(!calculate_stats);
    rotfun.merge_beta_sections(clear_data);
    logEllipsisShut(where);
    }}

    {{
    if (calculate_stats)
    {
      auto meansig = rotfun.mean_sigma();
      double Rmax = rotfun.top();
      logTab(where,"Mean and Standard Deviation"+nN);
      logTab(where,"Mean Score:      " + dtos(meansig.first,3));
      logTab(where,"Sigma of Scores: " + dtos(meansig.second,3));
      logTab(where,"Highest Score:   " + dtos(Rmax,3));
      logTab(where,"Highest Z-score: " + dtos((Rmax-meansig.first)/meansig.second));
      logBlank(where);
    }
    rotfun.statistics();
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      logTab(where,"Mean and Standard Deviation"+nN);
      logTab(where,snprintftos(
          "%10s %10s %10s %10s %10s %10s",
          "Mean","Sigma","Top","Top-Z","Max","Max-Z"));
      logTab(where,snprintftos(
          "%10.3f %10.3f %10.3f %10.3f %10.3f %10.3f",
          rotfun.f_mean, rotfun.f_sigma, rotfun.f_top, rotfun.z_top, rotfun.f_max, rotfun.z_max));
      //this could be on number of rf not parent, but in the ftf it has to be on id
      //so to use the same object we use the parent as index
      logBlank(where);
    }
    }}

    if (calculate_stats)
    {
      logTab(where,snprintftos(
          "%3s %10s %10s %10s %10s %13s %10s",
          "#","Beta","Mean","Sigma","Top","Z-Top(global)","Points"));
      for (int b = 0; b < rotfun.beta_section.size(); b++)
      {
        AnglesStored& beta_section = *std::next(rotfun.beta_section.begin(),b);
        logTab(where,snprintftos(
            "%3d %10.3f %10.3f %10.3f %10.3f %13.3f %10d",
            b+1,
            beta_section.beta_deg,
            beta_section.f_mean,
            beta_section.f_sigma,
            beta_section.f_top,
            beta_section.z_top, //changed to global value in statistics
            beta_section.rotation_function_data.size()));
      }
      logBlank(where);
    }

    //after mean and standard deviation
    {{
    logEllipsisOpen(where,"Sort and purge by percent"+nN);
    //cluster afterwards
    int before = rotfun.count_sites();
    double cut = rotfun.apply_partial_sort_and_percent_fss(io_percent,rotfun.f_max); //full sort of the surviving peaks
    logEllipsisShut(where);
    logTab(where,snprintftos(
        "%s %.2f/%.2f/%.2f/%.2f",
        "FSS Max/Top/Mean/Cutoff:",rotfun.f_max,rotfun.rotation_function_data[0].value,rotfun.f_mean,cut));
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(rotfun.count_sites()));
    logBlank(where);
    }}

    //axis of rotation to reduce the symmetry
    {{
    dmat33 axisrot;
         if (highsymm.second=='Z') axisrot = dmat33(1,0,0, 0,1,0, 0,0,1);
    else if (highsymm.second=='X') axisrot = dmat33(0,0,1, 1,0,0, 0,1,0);
    else if (highsymm.second=='Y') axisrot = dmat33(0,1,0, 0,0,1, 1,0,0);
    logTab(where,"Reverse highest symmetry of multiplicity "+itos(highsymm.first));
    logTab(where,"around axis " + chtos(highsymm.second));
    logTab(where,"matrix: " + dmtos(axisrot));
    //change to pdb coordinate frame
    for (auto& item : rotfun.rotation_function_data)
    {
      //the point to be searched is wrt the pdb orientation
      dmat33 R = zyz_matrix(item.grid);
      const dmat33 ROT = axisrot*R;
      item.grid = scitbx::math::euler_angles::zyz_angles<double>(ROT);
    }
    logEllipsisOpen(where,"Move to Rotational ASU");
    rotfun.move_to_rotational_asu();
    logEllipsisShut(where);
    }}

    rotfun.clustering_off();

    {{
    logTableTop(where,"Self Rotation Function (before clustering)"+nN,false);
    logTabArray(where,rotfun.logSelfTable(io_nprint,io_maxorder,percent_of_highest_ncs));
    logTableEnd(where);
    }}

    //peaks are sorted by virtue of partial sort in percent cutoff
    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Cluster fast rotation function search peaks"+nN);
    int before = rotfun.count_sites();
    rotfun.cluster(cluster_angle,io_cluster_back);
    rotfun.select_topsites_and_erase();
    bool constrain_sgsymm(true);
    if (constrain_sgsymm)
    {
// it is in P1 so symmetry will not be strictly enforced
// apply the symmetry here and then cluster which will pick out the highest of the p1 peaks
    rotfun.expand_space_group_symmetry();
    rotfun.apply_partial_sort_and_maximum_stored_llg(0); //sort all
    rotfun.cluster(cluster_angle,io_cluster_back);
    rotfun.select_topsites_and_erase(); //select the topsites again to pick the highest symmetry added ones
    }
    logEllipsisShut(where);
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(rotfun.count_sites()));

    logTableTop(where,"Self Rotation Function (after clustering)"+nN,true);
    logTabArray(where,rotfun.logSelfTable(io_nprint,io_maxorder,percent_of_highest_ncs));
    logTableEnd(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge non-point group symmetry"+nN);
    int before = rotfun.count_sites();
    //don't erase the non-integer values, instead display as triangles on the plot
    //eg 1zye which has legitimate Rotn=6.5 (55 degrees) tilt between dodecameric rings
    rotfun.select_point_group_and_erase(io_maxorder);
    rotfun.point_group_classes(percent_of_highest_ncs); //before space group symmetry erased
    logEllipsisShut(where);
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(rotfun.count_sites()));
    }}

    //purging by number done after clustering has been done (or not)
    {{
    int before = rotfun.count_sites();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge by maximum number"+nN);
      rotfun.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored: " + itos(io_maxstored));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(rotfun.count_sites()));
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logTableTop(where,"Self Rotation Function"+nN,true);
    //below calculated before space group symmetry purge, so that one can be aligned with space group
    logTab(where,"Point group symmetries:");
    logTab(where,"Crystallographic " +  SG.pointgroup() + " symmetry");
    bool pg(false);
    for (auto hasX : rotfun.hasX)
    if (hasX.second > 0)
    {
      std::string leader = "Non-crystallographic C" + itos(hasX.first) + "  symmetry: ";
      if (hasX.first >= 10)
        leader = "Non-crystallographic C" + itos(hasX.first) + " symmetry: ";
      logTab(where,leader + "maximum "+ dtos(hasX.second,1) + "%");
      pg = true;
    }
    for (int s = 2; s <= 6; s++)
    {
      std::string leader = "Non-crystallographic D" + itos(s) + "  symmetry: ";
      if (rotfun.hasX22[s] > 0)
        logTab(where,leader + "minimum "+ dtos(rotfun.hasX22[s],1) + "%");
      pg = true;
    }
    if (!pg) logTab(where,"No C or D type non-crystallographic symmetry");
    logTabArray(where,rotfun.logSelfTable(io_nprint,io_maxorder,percent_of_highest_ncs));
    logTableEnd(where);
    }}

    if (WriteFiles())
    {
      logUnderLine(where,"Polar Plots"+nN);
      //the interesting ones
      auto chis = rotfun.peak_chi_sections(io_maxorder);
      int sampling = input.value__int_number.at(".phasertng.self_rotation_function.plot_chi.").value_or_default();
      for (int ichi = 0; ichi <= 180; ichi += sampling)
        if (std::find(chis.begin(), chis.end(), ichi) == chis.end())
          chis.push_back(ichi);
      std::sort(chis.begin(),chis.end());
      int numpts = 2000;
      logChevron(where,"Plotting chi sections with matplotlib"+nN);
      PointsStored stored; //called again internally
      stored.fill_memory_points_on_sphere(numpts);
      double angsamp = scitbx::rad_as_deg(std::atan(stored.SAMP));
      logTab(where,"Number of points " + itos(numpts));
      logTab(where,"Angular sampling " + dtos(angsamp,2) + " degrees");
      FileSystem firstchi;
      for (auto chi : chis)
      {
        //note that chi-00x.txt is hardwired into mode srfp also, change there if changing here
        FileSystem filename(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".chi-"+ntos(chi,360)+".txt"));
        logFileWritten(where,WriteFiles(),"Chi Section",filename);
        if (WriteFiles())
        {
          auto lines = rotfun.plot_of_chi_section(chi,numpts);
          if (not firstchi.filesystem_is_set())
            firstchi = filename;
          filename.write_array(lines);
        }
      }
      logChevron(where,"Plotting self rotation function peaks"+nN);
      //note that peaks.txt is hardwired into mode srfp also, change there if changing here
      FileSystem filename(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".peaks.txt"));
      logFileWritten(where,WriteFiles(),"Mercator Plot Peaks",filename);
      if (WriteFiles())
      {
        auto lines = rotfun.plot_of_peaks();
        filename.write_array(lines);
      }
      {
      Loggraph loggraph;
      loggraph.title = "Top peaks in Self Rotation Function"+nN;
      loggraph.scatter = true;
      loggraph.graph.resize(1);
      loggraph.graph[0] = "RF Number vs LL-gain:AUTO:1,2";
      int i(1);
      for (auto& item : rotfun.rotation_function_data)
      {
        loggraph.data_text += itos(i++) + " " + dtos(item.value,10,2) + "\n";
      }
      logGraph(where,loggraph);
      }
    }
    }

    //clear so that reset without space group expansion in next call to run
    REFLECTIONS = nullptr;

    //logTab(out::LOGFILE,"Generate plots with self rotation function plots mode");
    //files written to PATHWAY.ULTIMATE which does not change with shift_tracker
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
