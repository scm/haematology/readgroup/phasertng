//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved

    //build name required for sequence
    if (Suite::Extra())
      logTabArray(out::TESTING,variance.BIN.logBin("Variance"));

    logUnderLine(out::LOGFILE,"Map Point Group");
    {
      std::string HM = "1"; //default, no symmetry
      if (!input.value__string.at(".phasertng.model_as_map.point_group_symbol.").is_default())
      {
        std::string pgs = input.value__string.at(".phasertng.model_as_map.point_group_symbol.").value_or_default();
        sv_string orders;
        //must have spaces between the orders!!
        hoist::algorithm::split(orders, pgs, (" "));
        bool has_only_digits = true;
        for (auto order :orders)
        {
          bool digits = (order.find_first_not_of( "0123456789" ) == std::string::npos);
          has_only_digits = has_only_digits and digits;
        }
        if (!has_only_digits)
        throw Error(err::INPUT,"Point group of map must contain only numbers (space separated)");
        HM = pgs;
      }
      logTab(out::LOGFILE,"Point Group = " + HM);
      search->POINT_GROUP_SYMBOL = HM;
      sv_double eulers;
      eulers = input.value__flt_numbers.at(".phasertng.model_as_map.point_group_euler.").value_or_default();
      PHASER_ASSERT(eulers.size()%3 == 0);
      for (int i = 0; i < eulers.size(); i+=3)
      {
        dvect3 euler(eulers[i],eulers[i+1],eulers[i+2]);
        search->POINT_GROUP_EULER.push_back(euler);
      }
    }

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Map Trace");
    //will need this info
    sv_string tmp = input.scope_keywords("trace hexgrid");
    for (auto item : tmp)
      logTab(where,item);

    //only calculate trace volume using 5A maps
    //even if the data only extend to lower resolution do the trace analysis at 5A
    //if the map is poor then can only do best resolution available
    double wang_resolution = 5.;
    if (variance.available_hires() > wang_resolution)
      logWarning("Available map resolution is less than recommended resolution for trace calculation " + dtos(wang_resolution));

    {{
    Map wangmap;
    wangmap.copy_to_resolution(variance.UC,variance.MILLER,variance.fcalc(0),wang_resolution);
    wangmap.apply_bfactor(
        input.value__flt_number.at(".phasertng.model_as_map.wang_unsharpen_bfactor.").value_or_default());
    phaser_assert(wangmap.FCALC.size());
    phaser_assert(wangmap.MILLER.size());
    if (Suite::Extra())
    {
      SpaceGroup SG("P 1");
      fft::real_to_complex_3d rfft(SG.type(),wangmap.UC.cctbxUC);
      std::pair<double,double> cell_scale = {1.,1.};
      rfft.calculate_gridding(wangmap.available_hires(cell_scale),cell_scale,false);
      rfft.shannon(2);
      rfft.allocate();
      rfft.backward(wangmap.MILLER,wangmap.FCALC);
      FileSystem wangmapOUT(DataBase(),search->DOGTAG.FileName(".debug.wang.map"));
      logFileWritten(out::TESTING,WriteFiles(),"Debug Wang Map",wangmapOUT);
      wangmapOUT.create_directories_for_file();
      if (WriteFiles()) rfft.WriteMap(wangmapOUT.fstr());
    }
    //below modifies trace
    dvect3 centre(0,0,0); //by definition of mt
    std::tie(trace.SAMPLING_DISTANCE,trace.statistics) = wangmap.wang_trace(
        input.value__flt_number.at(".phasertng.model_as_map.wang_volume_factor.").value_or_default(),
        search->VOLUME,
        centre,
        trace.PDB //returned
        );
    logTab(out::VERBOSE,"Solvent Fraction = " + dtos(wangmap.solvent_fraction(search->VOLUME)));
    }}
    PHASER_ASSERT(trace.size());

    //trace.shift_centre(search->CENTRE);
    trace.set_principal_statistics();
    logTab(where,"Trace centre: " + dvtos(trace.statistics.CENTRE));
    logTab(where,"Map centre: " + dvtos(search->CENTRE));
    dvect3 shift = trace.statistics.CENTRE-search->CENTRE;
    double dist = std::sqrt(shift*shift);
    logTab(where,"Difference: " + dvtos(shift) + " (distance=" + dtos(dist) + " Angstroms)");
    trace.CENTRE = search->CENTRE;
    trace.MAX_RADIUS = trace.statistics.MAX_RADIUS;
    trace.REFLID = REFLECTIONS->reflid();
    trace.MODLID = search->identify();
    trace.TIMESTAMP = TimeStamp();

    if (Suite::Extra())
    {
      trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.wang.pdb"));
      logFileWritten(out::TESTING,WriteFiles(),"Debug Wang Pdb",trace);
      if (WriteFiles()) trace.write_to_disk();
    }

    //now remove surface atoms regardless
    logTab(out::VERBOSE,"Wang Trace surface size = " + itos(trace.PDB.size()));
    double surface_distance(6); //there is no trim_asa for a trace set
    int cycles = trace.trim_surface(surface_distance);
    logTab(out::VERBOSE,"Number of surface trim cycles = "+ itos(cycles));

    if (Suite::Extra())
    {
      trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.wang.trimmed.pdb"));
      logFileWritten(out::TESTING,WriteFiles(),"Debug Wang Pdb",trace);
      if (WriteFiles()) trace.write_to_disk();
    }

    logTab(out::VERBOSE,"Wang Trace trimmed surface size = " + itos(trace.PDB.size()));
    PHASER_ASSERT(trace.PDB.size());
    logTab(out::VERBOSE,"Wang grid diagonal sampling distance = " + dtos(trace.SAMPLING_DISTANCE));
    double wang_distance = trace.SAMPLING_DISTANCE;

    int io_hexgrid_min = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_min();
    int io_hexgrid_max = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_max();
    int io_hexgrid_ncyc = input.value__int_number.at(".phasertng.trace.hexgrid.ncyc.").value_or_default();
    double io_hexgrid_sampling = input.value__flt_number.at(".phasertng.trace.hexgrid.minimum_distance.").value_or_default();

    {{
      double sampling = 0;
      double hexgrid_ntarget = (io_hexgrid_max + io_hexgrid_min)/2.0;
      if (input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").is_default())
      {
        //best estimate up front
        double mapvolume = search->VOLUME*0.74; //Kepler because we are packing spheres
        double voxel = mapvolume/hexgrid_ntarget;
        sampling = std::pow(voxel/(4./3.*scitbx::constants::pi),1./3.);
        sampling *= 2; //diameter, between hexagonal points
        sampling = std::max(io_hexgrid_sampling,sampling);
      }
      else
      {
        sampling = input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").value_or_default();
        io_hexgrid_ncyc = 0; //terminate at end of first loop
      }
      logTab(out::VERBOSE,"Initial estimate of sampling = " + dtos(sampling) + " Angstroms");
      int ncyc(0);
      std::map<double,int> samp_results; //sampling/number
      samp_results[trace.statistics.MEAN_RADIUS] = 1; //a fake limit for upper bound
      Trace hexgrid;
      logTab(out::VERBOSE,"Target =" + dtos(hexgrid_ntarget));
      logEllipsisOpen(where,"Calculate Hexagonal Grid");
      do
      {
        logTab(out::VERBOSE,"Sampling #" + itos(ncyc+1) + ": " + dtos(sampling) + " Angstroms");
        hexgrid.build_hexgrid_box(sampling,trace.statistics.MINBOX,trace.statistics.MAXBOX);
        PHASER_ASSERT(hexgrid.size());
        logTab(out::VERBOSE,"Trace length=" + itos(hexgrid.PDB.size()));
        hexgrid.set_principal_statistics();
        hexgrid.MAX_RADIUS = hexgrid.statistics.MAX_RADIUS;
        hexgrid.MODLID = search->identify();
        hexgrid.REFLID = REFLECTIONS->reflid();
        hexgrid.TIMESTAMP = TimeStamp();
        if (Suite::Extra()) //write here so that asa is in coordinates
        {
          std::string cc = ntos(ncyc+1,99);
          hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.box.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",hexgrid);
          if (WriteFiles()) hexgrid.write_to_disk();
        }
        //trace consists of wang mask, filling volume radius wang_distance each
        sv_double other_radii(trace.PDB.size(),0);
        for (int a = 0; a < trace.PDB.size(); a++)
          other_radii[a] = wang_distance*DEF_HEXFACTOR;
        hexgrid.select_overlapping(trace,other_radii);
        logTab(out::VERBOSE,"Trace length=" + itos(hexgrid.PDB.size()));
        if (Suite::Extra()) //write here so that asa is in coordinates
        {
          std::string cc = ntos(ncyc+1,99);
          hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.overlapping.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",trace);
          if (WriteFiles()) hexgrid.write_to_disk();
        }
        hexgrid.trim_surface();
        logTab(out::VERBOSE,"Trace length=" + itos(hexgrid.PDB.size()));
        if (Suite::Extra()) //write here so that asa is in coordinates
        {
          std::string cc = ntos(ncyc+1,99);
          hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.trimmed.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",trace);
          if (WriteFiles()) hexgrid.write_to_disk();
        }
        double N = hexgrid.size();
        logTab(out::VERBOSE,"Trace length=" + itos(hexgrid.size()));
        samp_results[sampling] = N;
       // double estimated_volume = fn::pow3(sampling)*N*scitbx::constants::pi*(4./(3.*8.));
        if (hexgrid.size() > io_hexgrid_max or hexgrid.size() < io_hexgrid_min)
        {
          //adjustment best guess
          //if N > hexgrid_ntarget (too finely sampled) then increase SAMP by this factor
          double factor = N/hexgrid_ntarget;
          logTab(out::VERBOSE,"Ratio current/target length=" + dtos(factor));
          factor = std::pow(factor,1./3.);
          if (N > io_hexgrid_max)
          {
            logTab(out::VERBOSE,"Too large!");
            sampling *= factor; //widen the spacing
            logTab(out::VERBOSE,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
            for (auto item : samp_results) //sorted on sampling
            {
              //test in order of lowest to highest sampling
              //first one that is larger than samp and has given too many points
              logTab(out::VERBOSE,"Already tried " + dtos(item.first) + "->" + itos(item.second));
              if (sampling < item.first and item.second > io_hexgrid_max) //already know it is too far
              {
                logTab(out::VERBOSE,"But " + dtos(sampling) + "<" + dtos(item.first));
                logTab(out::VERBOSE,"And " + itos(item.second) + ">" + itos(io_hexgrid_max));
                sampling = (item.first+sampling)/2.0;
                logTab(out::VERBOSE,"Sampling reset to half way between two =" + dtos(sampling));
                break; //regardless, we only need to look at the first one with bigger sampling
              }
            }
          }
          else if (N < io_hexgrid_min)
          {
            logTab(out::VERBOSE,"Too small!");
            if (sampling < io_hexgrid_sampling + DEF_PPT) //tolerance for safety
            {
              ncyc = io_hexgrid_ncyc; //to force while to fail
              logTab(out::VERBOSE,"Sampling aborted, at minimum");
              //possibly sent here from condition below
            }
            else if (sampling*factor > io_hexgrid_sampling)
            {
              sampling *= factor;
              logTab(out::VERBOSE,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
              for (auto iter = samp_results.rbegin(); iter != samp_results.rend(); ++iter)
              {
                //test in order of highest to lowest sampling
                //first one that that has given too many points
                logTab(out::VERBOSE,"Already tried " + dtos(iter->first) + "->" + itos(iter->second));
                if (sampling > iter->first and iter->second < io_hexgrid_min)
                {
                  logTab(out::VERBOSE,"But " + dtos(sampling) + ">" + dtos(iter->first));
                  logTab(out::VERBOSE,"And " + itos(iter->second) + "<" + itos(io_hexgrid_max));
                  sampling = (iter->first+sampling)/2.0;
                  logTab(out::VERBOSE,"Sampling reset to half way between two =" + dtos(sampling));
                  PHASER_ASSERT(sampling > io_hexgrid_sampling);
                  break; //regardless
                }
              }
            }
            else
            {
              sampling = io_hexgrid_sampling;
              logTab(out::VERBOSE,"Sampling set to minimum, just generate trace");
            }
          }
        }
        if (samp_results.count(sampling))
          ncyc = io_hexgrid_ncyc; //abort if we have tested this already
        logBlank(out::VERBOSE);
        ncyc++;
      }
      while ((hexgrid.size() > io_hexgrid_max || hexgrid.size() < io_hexgrid_min) and
              ncyc < io_hexgrid_ncyc); //infinite loop trap
      logEllipsisShut(where);
      trace = hexgrid;
      trace.SAMPLING_DISTANCE = sampling; //replace for smaller/larger sampling for packing
      logTab(out::VERBOSE,"Trace size: " + itos(trace.size()));
      logTab(out::VERBOSE,"Trace packing distance: " + dtos(trace.SAMPLING_DISTANCE));
      logBlank(out::VERBOSE);
    }}
    trace.renumber();
    }}
    trace.set_principal_statistics();
    trace.MAX_RADIUS = trace.statistics.MAX_RADIUS;
    trace.CENTRE = trace.statistics.CENTRE;

    double io_fs =     input.value__flt_number.at(".phasertng.model_as_map.solvent_constant.").value_or_default();
    dvect3 io_minbox = input.value__flt_vector.at(".phasertng.model_as_map.extent_minimum.").value_or_default();
    dvect3 io_maxbox = input.value__flt_vector.at(".phasertng.model_as_map.extent_maximum.").value_or_default();

    //decompostion populated same time as variance
    Ensemble& decomposition = search->DECOMPOSITION; //use the decomposition as the Ensemble
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Model Structure Factors for Decomposition");
    logTab(where,"Maximum radius for decomposition = " + dtos(trace.statistics.MAX_RADIUS));
    af_cmplex sfcalc;
    dvect3 range = io_maxbox - io_minbox;
    logTab(out::VERBOSE,"Range " + dvtos(range));
    logTab(out::VERBOSE,"Hires " + dtos(search->HIRES) + " " + dtos(variance.BIN.hires()));
#ifdef PHASERTNG_DEBUG_PHASER_MAPS
    decomposition.UC = variance.UC;
    decomposition.MILLER = variance.MILLER;
    decomposition.ECALC = variance.fcalc(0); //ecalc
#else
    decomposition.UC = UnitCell(range);
    //double sphereOuter = 2.*trace.statistics.MAX_RADIUS; //more correct but inconsistent with assert of mean radius for lmax calculation
    PHASER_ASSERT(search->MEAN_RADIUS);
    double sphereOuter = search->sphereOuter(2.,search->HIRES); // 2*mean_radius, mean_radius estimated from seq assuming sphere
    decomposition.UC.build_decomposition_unit_cell(sphereOuter,search->HIRES);
    std::tie(decomposition.MILLER,decomposition.ECALC) = variance.interpolate(0,
        decomposition.UC,
        io_fs, io_minbox+search->CENTRE, io_maxbox+search->CENTRE, io_cell_scale,
        false);
#endif
    logTab(where,"Unit Cell: " + dvtos(decomposition.UC.cctbxUC.parameters()));
    logTab(out::VERBOSE,"Decomposition Hires " + dtos(decomposition.hires()));
    logTab(out::VERBOSE,"Decomposition Bin " + dtos(variance.BIN.hires()));
    decomposition.REFLID = REFLECTIONS->reflid();
    decomposition.MODLID = search->identify();
    decomposition.TIMESTAMP = TimeStamp();
    decomposition.initialize_bin(variance.BIN);
    decomposition.SIGMAA = variance.SIGMAA;
    decomposition.normalize(); //convert to evalues
    }}

    Interpolation& interpolation = search->INTERPOLATION;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Model Structure Factors for Interpolation");
    Ensemble ensemble;
    ensemble.REFLID = REFLECTIONS->reflid();
    ensemble.MODLID = search->identify();
    ensemble.TIMESTAMP = TimeStamp();
    ensemble.SIGMAA = variance.SIGMAA;
    ensemble.UC = UnitCell(variance.UC); //start with minimal cell
    double boxscale = 3.0;
#ifdef PHASERTNG_DEBUG_PHASER_MAPS
    ensemble.MILLER = variance.MILLER;
    ensemble.ECALC = variance.fcalc(0); //ecalc
#else
    ensemble.UC.build_interpolation_unit_cell(boxscale,search->HIRES);
    std::tie(ensemble.MILLER,ensemble.ECALC) = variance.interpolate(0,
        ensemble.UC,
        io_fs, io_minbox, io_maxbox, io_cell_scale,
        true);
#endif
    logTab(where,"Unit Cell: " + dvtos(ensemble.UC.cctbxUC.parameters()));
    ensemble.initialize_bin(variance.BIN);
    ensemble.normalize(); //convert to evalues
    interpolation.ParseEnsemble(ensemble); //Interpolation()
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Database Entries");

    fcalcs.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::FCALCS_MTZ));
    logFileWritten(out::LOGFILE,WriteFiles(),"Structure Factor",fcalcs);
    if (WriteFiles()) fcalcs.write_to_disk();

    PHASER_ASSERT(models.has_relative_wilson_bfactor());
    models.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MODELS_PDB));
    logFileWritten(out::LOGFILE,WriteFiles(),"Models",models);
    if (WriteFiles()) models.write_to_disk();

    variance.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::VARIANCE_MTZ));
    logFileWritten(out::LOGFILE,WriteFiles(),"Variance",variance);
    if (WriteFiles()) variance.write_to_disk();

    trace.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::TRACE_PDB));
    logFileWritten(out::LOGFILE,WriteFiles(),"Trace",trace);
    if (WriteFiles()) trace.write_to_disk();

    search->MONOSTRUCTURE = trace; //copy, monostructure is base class of trace
    Monostructure& monostructure = search->MONOSTRUCTURE;
    monostructure.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MONOSTRUCTURE_PDB));
    logFileWritten(out::LOGFILE,WriteFiles(),"Monostructure",monostructure);
    if (WriteFiles()) monostructure.write_to_disk();

    decomposition.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::DECOMPOSITION_MTZ));
    logFileWritten(out::LOGFILE,WriteFiles(),"Model Decomposition",decomposition);
    if (WriteFiles()) decomposition.write_to_disk();

    interpolation.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::INTERPOLATION_MTZ));
    logFileWritten(out::LOGFILE,WriteFiles(),"Model Interpolation",interpolation);
    if (WriteFiles()) interpolation.write_to_disk();

    FileSystem filesystem;
    filesystem.SetFileSystem(DataBase(),search->identify().FileDataBase(other::ENTRY_CARDS));
    logFileWritten(out::LOGFILE,WriteCards(),"Entry cards",filesystem);
    auto dagcards = DAGDB.entry_unparse_card(DAGDB.ENTRIES[search->identify()]);
    if (WriteCards()) filesystem.write(dagcards);
    }}
