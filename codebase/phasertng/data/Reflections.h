//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Reflections_class__
#define __phasertng_Reflections_class__
#include <phasertng/data/Header.h>
#include <phasertng/data/Virtual.h>
#include <phasertng/main/pointers.h>
#include <phasertng/src/Bin.h>
#include <phasertng/data/SigmaN.h>
#include <phasertng/data/pdprint.h>
#include <phasertng/io/Loggraph.h>

namespace phasertng {

class Reflections : public Virtual, public Header
{
  public:
    //hardwirded value per data type
    std::set<labin::col>  LABIN_ALLOWED; //contains all the columns allowed, class specific
    std::set<friedel::type>  FRIEDEL; //contains the friedel columns

  //public:
    //void ReadMtz() { VanillaReadMtz(std::set<labin::col>()); }
    //af_string VanillaReadMtz(std::set<labin::col>);
    //void VanillaWriteMtz();
    //void VanillaWriteMtzOriginal();

  public:
    Reflections() : Virtual(),Header() { }
    virtual ~Reflections() {}

  public: //not required for boosting
    bool has_col(labin::col) const;
    bool has_friedel(friedel::type) const;
    bool has_friedel(std::set<friedel::type>) const;
    int  grain(int n) const;
    int  head_thread(int t,int n) const;
    int  tail_thread(int t,int n) const;
    double     data_hires() const;
    double     data_lores() const;
    af::double2 resolution() const;
    void       set_data_resolution();
    int        numbins() const;
    af_double  bin_hires(double) const;
    af_double  bin_lores(double) const;
    af_double  bin_midres(double) const;
    sv_double  bin_midssqr(double) const;
    af_int     numinbin() const;
    Identifier reflid() const;
    pod::pdprint formatting(labin::col,bool=false) const;

  public:
    void   setup_dobs_feff(friedel::type,type::mtzcol,type::mtzcol,double);
    void   setup_dobs_feff_from_map(type::mtzcol,type::mtzcol);
    void   setup_both_plus(type::mtzcol,type::mtzcol);
    void   setup_phsr(type::mtzcol);
    void   setup_ssqr(type::mtzcol);
    void   setup_cent(type::mtzcol);
    void   setup_teps(type::mtzcol,type::mtzcol);//teps,tbin
    void   setup_eps(type::mtzcol);
    void   setup_wll(type::mtzcol);
    Bin    setup_bin(type::mtzcol,int,int,int);
    SigmaN setup_resn_anisobeta(type::mtzcol,type::mtzcol);
    void   setup_absolute_scale(double);
    void   setup_teps_scale();
    void   generate_free_r_flags(type::mtzcol);

  public:
    void         set_data(labin::col,const double,const int);
    af_millnx    get_miller_array() const;
    af_double    get_data_array(labin::col) const;
    af_bool      get_present_array(friedel::type) const;

    SigmaN CalculateSigmaN() const;
    std::pair<af_double,af_int> sigman_with_binning(Bin&);
    double WilsonLL(cctbx::sgtbx::space_group);//sg for sysabs tests
    std::pair<
      std::map<int,std::map<bool,double> >,
      std::map<int,std::pair<double,double> >
    > WilsonSysAbs(char);

  public:
    //these functions use the virtual base class to copy data
    //for python, we need to write specific class to class copy wrappers
    void copy_and_reformat_header(const_ReflectionsPtr);
    void copy_and_reformat_data(const_ReflectionsPtr);
    bool change_to_spacegroup_in_same_pointgroup(SpaceGroup*);
    bool check_and_change_to_spacegroup_in_same_pointgroup(SpaceGroup*);

  public:
    af_string logWilson(std::string) const;
    af_string logLabin(std::string) const;
    af_string logReflections(std::string) const;
    std::pair<af_string,af_string> logBadData(std::string) const; //output,warning
    af_string logNcsEpsn(std::string) const;
    af_string logMtzHeader(std::string) const;
    af_string logMtzDump(std::string,int=-999,millnx=millnx(0,0,0)) const;
    std::pair<af_string,Loggraph> logSignalMeasure(
         const sv_double&,
         const sv_bool&,
         const sv_bool&,
         std::string,
         std::string,
         bool,
         bool=true) const;
    std::pair<sv_double,sv_bool> getIonSigI() const;
    std::tuple<af_string,bool,double> staraniso();

  public:
    bool check_native();
    void set_and_check_anomalous_centric_intensities();
    void set_and_check_anomalous_centric_amplitudes();
    void check_for_negative_f_or_sigf(friedel::type);
    void check_for_negative_or_zero_sigi(friedel::type);
    void move_to_reciprocal_asu();
    af_string unique_under_symmetry_selection();
    void sort_in_resolution_order();
    bool check_sort_in_resolution_order();
    void set_mean_amplitudes_from_anomalous();
    void set_mean_amplitudes_from_map_amplitudes();
    void set_mean_intensities_from_anomalous();
    void set_sigf_to_xtriage_value_if_not_present(friedel::type);
    bool set_french_wilson_flags(friedel::type);
    void reverse_french_wilson_IfromF(friedel::type);
    void reverse_not_french_wilson_IfromF(friedel::type);
    void set_FfromI(friedel::type);
    void set_anomalous_amplitudes_from_mean();
    void set_anomalous_intensities_from_mean();
    friedel::type convert_labin_to_friedel(labin::col) const;
    labin::col    convert_friedel_to_labin(std::string,friedel::type) const;
    double bfactor_minimum();
    virtual void expand_to_p1(bool) { phaser_assert(false); } //ReflColsMap only
    std::string b3tos(const boost::logic::tribool) const;
    std::string get_tncsinfo() const;
    std::string get_crystal() const;

  public: //fast getters
     const double work_reso(const millnx m) const  { return UC.cctbxUC.d(m); }
     const double work_ssqr(const millnx m) const  { return UC.cctbxUC.d_star_sq(m); }
     const bool   work_cent(const millnx m) const  { return SG.group().is_centric((m)); }
     const double work_eps(const millnx m) const   { return SG.group().epsilon(m); }
     const double work_phsr(const millnx m) const  { return SG.group().phase_restriction(m).nearest_valid_phase(0,true); } //true is "in degrees"
     const double work_reso(const int r) const  { return UC.cctbxUC.d(get_miller(r)); }
     const double work_ssqr(const int r) const  { return UC.cctbxUC.d_star_sq(get_miller(r)); }
     const bool   work_cent(const int r) const  { return SG.group().is_centric((get_miller(r))); }
     const double work_eps(const int r) const   { return SG.group().epsilon(get_miller(r)); }
     const double work_phsr(const int r) const  { return SG.group().phase_restriction(get_miller(r)).nearest_valid_phase(0,true); } //true is "in degrees"
};

} //phasertng
#endif
