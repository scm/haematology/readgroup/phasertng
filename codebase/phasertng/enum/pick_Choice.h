#ifndef __phasertng_pick_Choice_enum__
#define __phasertng_pick_Choice_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace pick {

MakeEnum ( Choice,
            (PEAK)
            (HOLE)
         );

}}
#endif
