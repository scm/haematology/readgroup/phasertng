generic_documentation_name = "phasertng"

class voyager_generator():
  def __init__(self):
#note that phasertng included here is a hack so that the same code can be used for writing the documentation for all phasertng as well as the voyager scripts
#phasertng is explicitly removed from the scripts list below
    self.header = {
    "voyager" : "Voyager is software for phasing macromolecular crystal structures and docking structures into cryo-em maps with maximum likelihood methods. It has been developed by Randy Read's group at the Cambridge Institute for Medical Research (CIMR) in the University of Cambridge. It uses a directed acyclic graph framework to track and control protocols.",
    generic_documentation_name : "Phasertng is the part of the Voyager software. Phasertng functionality is divided into \"modes\" which generate each node on the Voyager directed acyclic graph. Keywords are valid for a subset of modes. The nodes can be branch points in phasing and docking protocols. Keywords are valid for a subset of modes. Phasertng is designed for scripting by native handling of the Voyager directed acyclic graph framework. The modes of phasertng that are used in any given protocol are listed in the documentation for that protocol.",
    "scotty" : "Scotty beams the structure direct from the PDB, if a match can be found. The protocol determines if data can me matched to an isomorphous structure in the PDB or a given PDB coordinate file. If so, the data undergo reindexing to match the PDB indexing, expanding the data if required and warning if the data needs to be merged to lower symmetry. The structure then undergoes refinement, including a wide-convergence radius refinement if the R-factor is low, to achieve optimal match of the structure with the data.",
    "changeling" : "Changeling is for identification of a crystal consisting of one of a set of known contaminant structures by rotation function.",
    "bones" : "Bones is for diagnosing the phasing strategies that could be explored by Picard. It can be run before Picard to check the search space so that the search space can be modified before a lengthy Picard is started.",
    "picard" : "Picard is the master molecular replacement solution protocol. The protocol explores space groups within the same pointgroup, different translational non-crystallographic symmetries, and different cell contents, using different error estimations for the models. The search can be configured to also explore subgroups of the spacegroup when twinning is suspected.",
    "nomad" : "Nomad is for building the asymmetric unit contents from the sequence of the biological unit and a directory containing model files, or direct file input.  It can be used to check the models with respect to the biological unit sequence prior to Picard.",
    "riker" : "Riker is secondary to Picard, and is used to refine the coordinates and the temperature factors of a fixed model and use the refined coordinates to bootstrap the molecular replacment placement of further, difficult to place, components. The fixed solution may be for a previous partial solution which needs completion. The list of new models is added sequentially by molecular replacement without permutation. Because Riker refines the coordinates of the partial placement before continuing, which is not part of the Picard protocol, the signal for new placements is increased. This is advantageous in marginal cases where Picard may struggle to place partially disordered or poorly modelled components.",
    "xtricorder" : "Xtricorder is for analysing data quality including translational non-crystallographic symmetry, twinning, self rotation function and information content. The analysis is also performed within other voyager scripts to automatically inform the structure solution pathway. However, the results should be part of data analysis prior to running other voyager scripts.",
    "caldos" : "Caldos is for refining temperature factors of residues for development of conversion protocols.",
    "nacelle" : "Nacelle is for generating a processed reflection  data file. All output is muted except for the processed reflection data, which has additional columns for Feff and the information content. The reflection file contains anisotropic correction, information content per reflection, Feff and Dobs for LLGI target function calculations (the maximum likelihood replacement for French-Wilson amplitudes), and twinning information in the header. These columns are suitable for display in reflection data viewers such as HKLviewer or for other external use.",
    "nanoprobe" : "Nanoprobe is for single atom molecular replacement.",
    "nanite" : "Nanite is for SAD phasing using phassade or hyss or other externally derived substructure for seeding the search.",
    "spock" : "Spock is for MR-SAD, SAD phasing seeded by a partial molecular replacment solution.",
    "exocomp" : "Treewalker is for generating the interactive tree of a phasertng database directory for display in a browser.",
   }

    self.naming = {
    "voyager" : "The name Voyager is used repeatedly for the name of spacecraft throughout Star Trek, and one of these is a Pathfinder-class starship active at the time of Star Trek the Next Generation.",
    "scotty" : "Montgomery Scott aka Scotty is chief engineer on the Enterprise-A, the starship of Star Trek the Original Series, and responsible for teleportation, with \"Beam me up, Scotty\" having entered popular culture as a expression of a desire to be elsewhere.",
    "changeling" : "In Star Trek, the Changelings are shapeshifting lifeforms.",
    "bones" : "Dr Leonard McCoy aka Bones is the doctor on the Enterprise-A, the starship of Star Trek the Original Series.",
    "picard" : "Jean-Luc Picard is commanding officer on the Enterprise-D, the starship of Star Trek the Next Generation.",
    "nomad" : "In Star Trek, Nomad ML-15c is a spacecraft designed to probe for new lifeforms in interstellar space.",
    "riker" : "William Riker is first officer on the Enterprise-D, the starship of Star Trek the Next Generation, following the command of Jean-Luc Picard.",
    "xtricorder" : "In Star Trek, a tricorder is an advanced multi-functional hand-held device to gather and analyse data.",
    "caldos" : "In Star Trek, Caldos is a planet with a long established weather (temperature) control system.",
    "nanoprobe" : "In Star Trek, a nanoprobe is a microscopic device designed to build structures at the molecular level.",
    "nacelle" : "In Star Trek, a nacelle refers to a structure that houses the key components of a spacecraft's propulsion system.",
    "nanite" : "In Star Trek, a nanite is a microscopic device that is built by manipulating atoms, is small enough to enter living cells, and is capable of self-replication.",
    "spock" : "Spock was a half-human/half-Vulcan science officer on Enterprise-A, the starship of Star Trek the Original Series, and was known for his struggle to suppress his human emotions in favor of the Vulcan philosophy of logic, making his rare sad moments particularly impactful.",
    "exocomp" : "In Star Trek, an exocomp was a small robotic device that could be sent into complex systems to analyse and repair them.",
   }

  def scripts(self):
    voyager_scripts = list(self.header.keys())
    voyager_scripts.remove("voyager")
    voyager_scripts.remove(generic_documentation_name)
    return list(voyager_scripts)

  def html(self,pick):
    txt = self.header[pick].replace("\n","<br>\n")
    return txt + "<br><br>\n"

  def text(self,pick):
    txt = self.header[pick]
    return txt + "\n\n"

  #set this to description in the program template the write this with --help
  def wrapped_text(self,pick,nchar=80):
    txt = self.header[pick]
    nam = self.naming[pick]
    import textwrap
    return "\nDescription:\n" + textwrap.fill(txt, nchar) + "\n\nWhy the name?:\n"  + textwrap.fill(nam, nchar) + "\n"

#setup the default dir, this is not a member of the class voyager generator
#but defined here so that it is imported into the program class without another include
def default_voyager_database(params,name):
    import os
    from libtbx.utils import Sorry
    if params.phasertng.suite.database is None:
      defdir = os.path.join(os.getcwd(),"phasertng_" + name)
      params.phasertng.suite.database = defdir
      if os.path.exists(defdir) and os.path.isdir(defdir) and any(os.listdir(defdir)):
        raise Sorry("Default database directory " + defdir + " is not empty\nPlease specify database explicitly or clear directory")
    return params
