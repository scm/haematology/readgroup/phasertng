//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Phasertng_class__
#define __phasertng_Phasertng_class__
#include <autogen/include/InputCard.h>
#include <phasertng/main/Output.h>
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/data/ReflColsMap.h>
#include <phasertng/data/LatticeTranslocation.h>
#include <phasertng/pod/moderunner.h>

namespace phasertng {

//define this to force stacktrace rather than throwing nice errors - no catch
//#define PHASERTNG_DEBUG_THROW_STACKTRACE
//#define PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY

class Phasertng : public Output
{
  //members accessible through boost interface
  public: //reflections in memory
    dag::DagDatabase DAGDB; //the dag and database combined, in c++ format

    //REFLECTIONS pointer to abstract base class of data
    //the workhorse array in any mode is REFLECTIONS
    ReflectionsPtr REFLECTIONS = nullptr; //reformatted data. shared_ptr
    //REFLCOLSMAP is the reservoir of reflection information
    ReflColsMap REFLCOLSMAP; //stored data
    //REFLECTIONS can be of any reflection class type, any data structure
    //REFLCOLSMAP is always in flexible (map) column format (like the mtz file)
    //copy any changes in REFLECTIONS back to the REFLCOLSMAP array at the end of the mode
    //and use this to write out the data
    //note that REFLECTIONS can point to REFLCOLSMAP so that memory is not always doubled
    //both stay in memory and if the same reflection class type is needed in the next mode
    //it is used without having to be instantiated anew
    //temporary changes to the reflections (e.g expand to P1)
    //can take place in a mode by creating a new temporary class and setting REFLECTIONS ptr
    //set REFLECTIONS to nullptr at the end of the mode to force reset for next mode
    InputCard input; //available from python for twilight zone code

  private:
   // const InputCard& input = input; //cast to const for use, prevents changes after parse
    std::string Cards = "";
    int  NTHREADS = 1;
    bool USE_STRICTLY_NTHREADS = true;
    bool OVERWRITE = false; //this has to be stored for multi jobs where flag gets overwritten by restoration of the cards
    std::set<std::string> reflid_can_be_different;

  public: //constructors
    Phasertng() : Output("VOYAGER") { }
    Phasertng(const std::string type) : Output(type) { }
    ~Phasertng() {}

  public://setter
    void  set_nthreads(int n) { NTHREADS = std::max(1,n); }
    void  set_use_strictly_nthreads(bool b) { USE_STRICTLY_NTHREADS = b; }
    void  reset_reflections() { REFLECTIONS  = nullptr; }

  public: //getters
    //call for python boost, to return Error object from python scripts
    Error get_error_object() { return *this; }

  public: //functions
    void run(const std::string);
    bool setup_before_call_to_mode(af_string, const int);
    void cleanup_after_call_to_mode(bool);
    void skipping();

    af_string   cards(bool,bool,std::set<std::string> keywords={});
    af_string   cards_show(bool,bool,std::set<std::string> keywords={},show::type=show::VALUE);
    std::string card_str(bool); //without mode
    std::string hashing();
    std::string hashing_extra();
    std::string pathway_cards();
    std::string phil_str();
    af_string   get_filenames(af_string);
    void        clear_filenames();

    sv_string input_keywords_for_mode(sv_string m)
    { //set to vector conversion for boost output
      std::set<std::string> tmp = input.input_keywords_for_mode(m);
      return sv_string(tmp.begin(),tmp.end());
    }

    sv_string result_keywords_for_mode(sv_string m)
    { //set to vector conversion for boost output
      std::set<std::string> tmp = input.result_keywords_for_mode(m);
      return sv_string(tmp.begin(),tmp.end());
    }

  private: //functions
    void runANISO();
    void runBRF();
    void runBTF();
    void runCCA();
    void runCCS();
    void runDATA();
    void runEATM();
    void runELLG();
    void runEMAP();
    void runEMM();
    void runESM();
    void runETFZ();
    void runEXP1();
    void runFEFF();
    void runFRF();
    void runFRFR();
    void runFTF();
    void runFTFR();
    void runFUSE();
    void runGYRE();
    void runIMAP();
    void runIMTZ();
    void runINFO();
    void runIPDB();
    void runJOG();
    void runMAPZ();
    void runMCC();
    void runMOVE();
    void runMSM();
    void runPAK();
    void runPERM();
    void runPOSE();
    void runPTF();
    void runPUT();
    void runRBGS();
    void runRBM();
    void runRBR();
    void runRELLG();
    void runRMR();
    void runSPR();
    void runSGA();
    void runSGX();
    void runSPAN();
    void runSRF();
    void runSSA();
    void runSSC();
    void runSSD();
    void runSSM();
    void runSSP();
    void runSSR();
    void runTEST();
    void runTFZ();
    void runTNCS();
    void runTNCSO();
    void runTWIN();

  private:
    std::pair<std::string,std::string> load_reflid_to_base(Identifier,out::stream,out::stream);
    bool load_reflid_for_node(Identifier,out::stream);
    void write_reflid(DogTag);
    std::string  setup_reflid_type(Identifier,out::stream,out::stream);
    void unload_dag(out::stream);

  public://boosted
    void load_dag(std::string);
    void load_dag_file(std::string);
    void set_user_input_for_suite(const InputCard&);
    void write_logfiles(bool);
    void get_user_input_for_suite();
    bool load_entry_from_database(out::stream,entry::data,Identifier,bool);
    void append_parse_cards(std::string);
    pod::moderunner get_moderunner();
    void load_moderunner(pod::moderunner,std::string,std::string);
    void load_cards(pod::moderunner);
    LatticeTranslocation apply_interpolation(out::stream,interp::point, //default is no lattice translocation
            std::vector<type::flt_numbers> ltds=std::vector<type::flt_numbers>());
};

}
#endif
