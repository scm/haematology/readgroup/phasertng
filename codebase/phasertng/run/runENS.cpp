//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
    models.MODLID = search->identify();
    models.REFLID = REFLECTIONS->reflid();
    models.TIMESTAMP = TimeStamp();

    if (search->MAP)
      throw Error(err::FATAL,"Model (" + models.REFLID.str() + ") is a map");

    models.set_principal_statistics();
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Ensemble Properties");
    logTab(where,"translation (orth): " + dvtos(search->PRINCIPAL_TRANSLATION,7,4));
    logTab(where,"translation (frac): " + dvtos(REFLECTIONS->UC.fractionalization_matrix()*search->PRINCIPAL_TRANSLATION,7,4));
    logTab(where,"mean_radius: " + dtos(search->MEAN_RADIUS));
    logTab(where,"extent: " + dvtos(models.statistics.EXTENT));
    logTab(where,"volume: " + dtos(search->VOLUME));
    }}
    PHASER_ASSERT(search->MEAN_RADIUS > 0);

    if (models.REFLID != REFLECTIONS->reflid())
      throw Error(err::FATAL,"Model (" + models.REFLID.str() + ") prepared with different reflection file (" + REFLECTIONS->reflid().str() + ")");
    if (models.SERIAL.size() != nmodels)
      throw Error(err::FATAL,"Model has no VRMS estimations");

    config_hires = std::max(config_hires,
          input.value__flt_number.at(".phasertng.molecular_transform.resolution.").value_or_default());

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Ensemble Configuration");
    if (search->ATOM)
    throw Error(err::INPUT,"Cannot build ensemble from single atom");
    logTab(where,"Ensemble configured to resolution " + dtos(config_hires,5,2));
    logTab(where,"Ensemble configured with number of models = " + itos(nmodels));
    PHASER_ASSERT(nmodels);
    search->HIRES = config_hires;
    }}

    if (!models.has_relative_wilson_bfactor())
    throw Error(err::INPUT,"Ensemble Wilson-B not set");
    //if (models.damped())
    //throw Error(err::INPUT,"Ensemble Wilson-B damps B-factors");

    phaser::ncs::PointGroup pg;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Ensemble Point Group");
    if (input.value__boolean.at(".phasertng.point_group.calculate_from_coordinates.").value_or_default())
    {
      SpaceGroup SG("P 1");
      UnitCell UC; //default
      Monostructure refined;
      models.get_best_pdb_cards(
        refined.REFLID,refined.MODLID,refined.TIMESTAMP,refined.PDB);
      auto pdb_file_content = refined.get_pdb_cards();
      const char* const delim = "\n";
      std::ostringstream imploded;
      std::copy(pdb_file_content.begin(), pdb_file_content.end(),
           std::ostream_iterator<std::string>(imploded, delim));
      std::string source_info = models.fstr();
      iotbx::pdb::input ensemble_input(
        source_info.c_str(),
        scitbx::misc::split_lines(
          imploded.str().c_str(),
          imploded.str().size(),
          false,
          true
          ).const_ref()
        ); //throws UNHANDLED ERROR
      phaser::ncs::EnsembleSymmetry ensemble_symmetry;
      ensemble_symmetry.set_min_sequence_coverage(
        input.value__percent.at(".phasertng.point_group.coverage.").value_or_default_fraction());
      ensemble_symmetry.set_min_sequence_identity(
        input.value__percent.at(".phasertng.point_group.identity.").value_or_default_fraction());
      ensemble_symmetry.set_max_rmsd(
        input.value__flt_number.at(".phasertng.point_group.rmsd.").value_or_default());
      ensemble_symmetry.set_angular_tolerance(
        input.value__flt_number.at(".phasertng.point_group.angular_tolerance.").value_or_default());
      ensemble_symmetry.set_spatial_tolerance(
        input.value__flt_number.at(".phasertng.point_group.spatial_tolerance.").value_or_default());
      ensemble_symmetry.set_root(ensemble_input.construct_hierarchy());
      pg = ensemble_symmetry.get_point_group();
      // SYMM_EULER = pg.get_euler_list();
      //int MULT = pg.get_orth_list().size(); //multiplicity
    }
    search->POINT_GROUP_SYMBOL = pg.hermann_mauguin_symbol();
    logTab(where,"Point Group: " + pg.hermann_mauguin_symbol());
    // exclude default case because default is 1 and eulers=000 already loaded in Entry
    for (int i = 1; i < pg.multiplicity(); i++)
    {
      PHASER_ASSERT(pg.get_euler(0) == dvect3(0,0,0)); //paranoia
      logTab(where,"Point Group Euler Angles: " + dvtos(pg.get_euler(i),7,1));
      search->POINT_GROUP_EULER.push_back(pg.get_euler(i));
    }
    }}

    Trace& trace = search->TRACE;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("trace"))
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Ensemble Trace");
      if (!input.value__path.at(".phasertng.trace.filename.").is_default())
        trace.SetFileSystem( input.value__path.at(".phasertng.trace.filename.").value_or_default());
           //this is the read pdb, different from write below
      if (models.is_atom())
      {
        logTab(where,"Trace is single atom");
       // trace.SERIAL = models.SERIAL;
        trace.SAMPLING_DISTANCE = 1.5;
        trace.PDB.push_back(models.PDB[0][0]);
        trace.MODLID = search->identify(); //yes, needed here
        trace.REFLID = REFLECTIONS->reflid();
        trace.TIMESTAMP = TimeStamp();
        trace.MAX_RADIUS = models.statistics.MAX_RADIUS;
        trace.CENTRE = models.statistics.CENTRE;
        PHASER_ASSERT(trace.size());
      }
      else if (trace.filesystem_is_set())
      {
        logTab(where,"Trace from coordinates: " + trace.qfstrq());
        trace.read_from_disk();
        if (trace.has_read_errors())
          throw Error(err::INPUT,trace.read_errors());
        trace.MODLID = search->identify(); //yes, needed here
        trace.REFLID = REFLECTIONS->reflid();
        trace.TIMESTAMP = TimeStamp();
        trace.MAX_RADIUS = models.statistics.MAX_RADIUS;
        trace.CENTRE = models.statistics.CENTRE;
        //AJM TODO fix below so that not hardwired, somehow from trace
        trace.SAMPLING_DISTANCE = 1.5; //or other value if not from coordinates!!
        logTab(where,"Trace number of read atoms: " + itos(trace.size()));
        PHASER_ASSERT(trace.size());
      }
      else
      {
        logTab(where,"Trace from coordinates in conserved core of ensemble");
        logTab(where,"Trace number of model atoms: " + itos(models.PDB[models.mtrace()].size()));
        models.conserved_trace(trace.PDB,trace.SAMPLING_DISTANCE);
        trace.MODLID = search->identify(); //yes, needed here
        trace.REFLID = REFLECTIONS->reflid();
        trace.TIMESTAMP = TimeStamp();
        trace.MAX_RADIUS = models.statistics.MAX_RADIUS;
        trace.CENTRE = models.statistics.CENTRE;
        logTab(where,"Trace number of conserved atoms: " + itos(trace.size()));

        int io_hexgrid_max = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_max();

        sv_string tmp = input.scope_keywords("trace hexgrid");
        for (auto item : tmp)
          logTab(where,item);

        if (Suite::Extra())
        {
          trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.conserved.trace.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Trace",trace);
          if (WriteFiles()) trace.write_to_disk();
        }

        //now remove surface atoms regardless
        if (input.value__boolean.at(".phasertng.trace.trim_asa.").value_or_default())
        {
          out::stream where = out::VERBOSE;
          Trace backup = trace;
          logTab(where,"Remove surface atoms");
          trace.trim_asa();
          logTab(where,"Trace size: " + itos(trace.size()));
          logTab(where,"Remove incomplete sidechains");
          trace.trim_incomplete_sidechains();
          logTab(where,"Trace size: " + itos(trace.size()));
          if (!trace.size())
          {
            logTab(where,"Revert trace to unedited");
            trace = backup;
          }
          if (Suite::Extra())
          {
            trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.asa.trace.pdb"));
            logFileWritten(out::TESTING,WriteFiles(),"Debug Trace",trace);
            if (WriteFiles()) trace.write_to_disk();
          }
        }

        if (trace.size() <= io_hexgrid_max and
            trace.number_of_calphas() == 0) //calpha not possible, hexgrid not necessary
        {
          logTab(where,"Trace is all atoms");
          trace.SAMPLING_DISTANCE = 1.5; //atomic radii sampling distance
          logTab(where,"Trace size: " + itos(trace.size()));
        }
        else if (trace.size() <= io_hexgrid_max and
                 input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("optimal"))
        {
          logTab(where,"Trace is all atoms");
          trace.SAMPLING_DISTANCE = 1.5; //atomic radii sampling distance
          logTab(where,"Trace size: " + itos(trace.size()));
        }
        else if (input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("all"))
        {
          logTab(where,"Trace is all atoms");
          trace.SAMPLING_DISTANCE = 1.5; //atomic radii sampling distance
          logTab(where,"Trace size: " + itos(trace.size()));
        }
        else if (input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("calpha"))
        {
          logTab(where,"Trace is calpha atoms");
          trace.trim_to_calphas();
          trace.SAMPLING_DISTANCE = 3.8; //atomic radii sampling distance
          logTab(where,"Trace size: " + itos(trace.size()));
        }
        else if (trace.number_of_calphas() <= io_hexgrid_max and
                 input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("optimal"))
        {
          logTab(where,"Trace is calpha atoms");
          trace.trim_to_calphas();
          trace.SAMPLING_DISTANCE = 3.8; //atomic radii sampling distance
          logTab(where,"Trace size: " + itos(trace.size()));
        }
        else
        {
          //store these for rapid access
          int io_hexgrid_ncyc = input.value__int_number.at(".phasertng.trace.hexgrid.ncyc.").value_or_default();
          int io_hexgrid_min = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_min();
          double io_hexgrid_min_distance = input.value__flt_number.at(".phasertng.trace.hexgrid.minimum_distance.").value_or_default();
          double sampling = 0;
          double hexgrid_ntarget = (io_hexgrid_max + io_hexgrid_min)/2.0;
          if (input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").is_default())
          {
            //best estimate up front
            double volume;
            {
            //calculate the volume from the sequence, same as for the map
            Sequence sequence(models.SELENOMETHIONINE);
            bool use_unknown(false);
            sequence.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
            sequence.initialize(true,true);
            volume = sequence.volume();
            }
            double mapvolume = volume*0.74; //Kepler because we are packing spheres
            double voxel = mapvolume/hexgrid_ntarget;
            sampling = std::pow(voxel/(4./3.*scitbx::constants::pi),1./3.);
            sampling *= 2; //diameter, between hexagonal points
            sampling = std::max(io_hexgrid_min_distance,sampling);
          }
          else
          {
            sampling = input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").value_or_default();
            io_hexgrid_ncyc = 0; //terminate at end of first loop
          }
          int ncyc(0);
          std::map<double,int> samp_results; //sampling/number
          samp_results[models.statistics.MEAN_RADIUS] = 1; //a fake limit for upper bound
          Trace hexgrid;
          hexgrid.MODLID = search->identify();
          hexgrid.REFLID = REFLECTIONS->reflid();
          hexgrid.TIMESTAMP = TimeStamp();
          hexgrid.MAX_RADIUS = models.statistics.MAX_RADIUS;
          hexgrid.CENTRE = models.statistics.CENTRE;
          logTab(where,"Target = " + dtos(hexgrid_ntarget,0));
          do
          {
            logTab(where,"Sampling #" + itos(ncyc+1) + ": " + dtos(sampling) + " Angstroms");
            hexgrid.build_hexgrid_box(sampling,models.statistics.MINBOX,models.statistics.MAXBOX);
            std::string cc = ntos(ncyc+1,99);
            PHASER_ASSERT(hexgrid.size());
            if (Suite::Extra()) //write here so that asa is in coordinates
            {
              std::string cc = ntos(ncyc+1,99);
              hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.raw.pdb"));
              logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",hexgrid);
              if (WriteFiles()) hexgrid.write_to_disk();
            }
            //trace consists of (conserved) atoms, so use vanderwaals distances
            table::vanderwaals_radii vdw(0);
            sv_double other_radii(trace.PDB.size(),0);
            for (int a = 0; a < trace.PDB.size(); a++)
              other_radii[a] = vdw.get(trace.PDB[a].get_element());
            hexgrid.select_overlapping(trace,other_radii);
            if (Suite::Extra()) //write here so that asa is in coordinates
            {
              std::string cc = ntos(ncyc+1,99);
              hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.clash.pdb"));
              logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",hexgrid);
              if (WriteFiles()) hexgrid.write_to_disk();
            }
            int cycles = hexgrid.trim_surface();
            logTab(out::TESTING,"Number of surface trim cycles = "+ itos(cycles));
            if (Suite::Extra()) //write here so that asa is in coordinates
            {
              hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.buried.pdb"));
              logFileWritten(out::TESTING,WriteFiles(),"Debug Hexgrid",hexgrid);
              if (WriteFiles()) hexgrid.write_to_disk();
            }
            double number_of_hexgrid_points = hexgrid.size();
            logTab(where,"Trace length=" + itos(hexgrid.size()));
            samp_results[sampling] = number_of_hexgrid_points;
           // double estimated_volume = fn::pow3(sampling)*N*scitbx::constants::pi*(4./(3.*8.));
            if (hexgrid.size() > io_hexgrid_max || hexgrid.size() < io_hexgrid_min)
            {
              //adjustment best guess
              //if number_of_hexgrid_points > hexgrid_ntarget (too finely sampled) then increase sampling by this factor
              logTab(where,"Ratio current/target length=" + dtos(number_of_hexgrid_points/hexgrid_ntarget));
              double factor = std::pow(number_of_hexgrid_points/hexgrid_ntarget,1./3.);
              if (number_of_hexgrid_points > io_hexgrid_max)
              {
                logTab(where,"Too large!");
                sampling *= factor; //widen the spacing
                logTab(where,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
                for (auto item : samp_results) //sorted on sampling
                {
                  //test in order of lowest to highest sampling
                  //first one that is larger than samp and has given too many points
                  logTab(where,"Already tried " + dtos(item.first) + "->" + itos(item.second));
                  if (sampling < item.first and item.second > io_hexgrid_max) //already know it is too far
                  {
                    logTab(where,"But " + dtos(sampling) + "<" + dtos(item.first));
                    logTab(where,"And " + itos(item.second) + ">" + itos(io_hexgrid_max));
                    sampling = (item.first+sampling)/2.0;
                    logTab(where,"Sampling reset to half way between two =" + dtos(sampling));
                    break; //regardless, we only need to look at the first one with bigger sampling
                  }
                }
              }
              else if (number_of_hexgrid_points < io_hexgrid_min)
              {
                logTab(where,"Too small!");
                if (sampling < io_hexgrid_min_distance + DEF_PPT) //tolerance for safety
                {
                  ncyc = io_hexgrid_ncyc; //to force while to fail
                  logTab(where,"Sampling aborted, at minimum");
                  //possibly sent here from condition below
                }
                else if (sampling*factor > io_hexgrid_min_distance)
                {
                  sampling *= factor;
                  logTab(where,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
                  for (auto iter = samp_results.rbegin(); iter != samp_results.rend(); ++iter)
                  {
                    //test in order of highest to lowest sampling
                    //first one that that has given too many points
                    logTab(where,"Already tried " + dtos(iter->first) + "->" + itos(iter->second));
                    if (sampling > iter->first and iter->second < io_hexgrid_min)
                    {
                      logTab(where,"But " + dtos(sampling) + ">" + dtos(iter->first));
                      logTab(where,"And " + itos(iter->second) + "<" + itos(io_hexgrid_max));
                      sampling = (iter->first+sampling)/2.0;
                      logTab(where,"Sampling reset to half way between two =" + dtos(sampling));
                      PHASER_ASSERT(sampling > io_hexgrid_min_distance);
                      break; //regardless
                    }
                  }
                }
                else
                {
                  sampling = io_hexgrid_min_distance;
                  logTab(where,"Sampling set to minimum, just generate trace");
                }
              }
            }
            if (samp_results.count(sampling))
              ncyc = io_hexgrid_ncyc; //abort if we have tested this already
            logBlank(where);
            ncyc++;
          }
          while ((hexgrid.size() > io_hexgrid_max || hexgrid.size() < io_hexgrid_min) and
                  ncyc < io_hexgrid_ncyc); //infinite loop trap

          logTab(where,"Hexgrid size: " + itos(hexgrid.size()));
          logTab(where,"Trace size: " + itos(trace.size()));
          if (hexgrid.size() < trace.size())
          {
            trace = hexgrid;
            trace.renumber();//sort out the numbering at the end
          }
          else
          {
            logTab(where,"Hexgrid more finely sampled than atoms, revert to trace");
          }
        }
      }
      trace.POINT_GROUP_EULER.clear();
      for (int i = 0; i < pg.multiplicity(); i++)
        trace.POINT_GROUP_EULER.push_back(pg.get_euler(i));
    }
    trace.MODLID = search->identify();
    trace.REFLID = REFLECTIONS->reflid();
    trace.TIMESTAMP = TimeStamp();
    trace.MAX_RADIUS = models.statistics.MAX_RADIUS;
    trace.CENTRE = models.statistics.CENTRE;

    Monostructure& monostructure = search->MONOSTRUCTURE;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("monostructure"))
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Ensemble Monostructure");
      logTab(where,"Convert best model in ensemble to single representative structure");
      monostructure.MODLID = search->identify();
      monostructure.REFLID = REFLECTIONS->reflid();
      monostructure.TIMESTAMP = TimeStamp();
      int ntrace; double start;
      std::tie(start,ntrace) = models.SERIAL.get_lowest();
/*
      monostructure.SERIAL.resize(1);
      monostructure.SERIAL.vrms_input[0] = models.SERIAL.vrms_input[ntrace];
      monostructure.SERIAL.vrms_start[0] = start;
      monostructure.SERIAL.vrms_lower[0] = models.SERIAL.vrms_lower[ntrace];
      monostructure.SERIAL.vrms_upper[0] = models.SERIAL.vrms_upper[ntrace];
*/
      //make sure identifiers are set externally
      for (int a = 0; a < models.PDB.at(ntrace).size(); a++)
      {
        if (models.PDB.at(ntrace).at(a).O != 0.0)
        {
          monostructure.PDB.push_back(models.PDB.at(ntrace).at(a));
        }
      }
      PHASER_ASSERT(monostructure.PDB.size() > 0);
      monostructure.apply_wilson_bfactor(REFLECTIONS->WILSON_B_F);
      logTab(where,"Best model of ensemble: #" + itos(ntrace+1));
    }

    std::set<entry::data> preparation;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("decomposition"))
      preparation.insert(entry::DECOMPOSITION_MTZ);
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("interpolation"))
      preparation.insert(entry::INTERPOLATION_MTZ);
    for (auto loop : preparation)
    {
      out::stream where = out::LOGFILE;
      loop == entry::DECOMPOSITION_MTZ ?
        logUnderLine(where,"Ensemble Structure Factors for Decomposition"):
        logUnderLine(where,"Ensemble Structure Factors for Interpolation");
      Ensemble ensemble;
      ensemble.REFLID = REFLECTIONS->reflid();
      ensemble.MODLID = search->identify();
      ensemble.TIMESTAMP = TimeStamp();
      ensemble.REFLID = REFLECTIONS->reflid();

      std::pair<double,double> cell_scale = {1.,1.};
      if (loop == entry::INTERPOLATION_MTZ)
        cell_scale = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default();

      ensemble.UC = UnitCell(search->EXTENT);
      if (loop == entry::DECOMPOSITION_MTZ)
      {
        double rf = input.value__flt_number.at(".phasertng.molecular_transform.decomposition_radius_factor.").value_or_default();
        ensemble.SPHERE = search->sphereOuter(rf,config_hires);
        auto text = ensemble.UC.build_decomposition_unit_cell(ensemble.SPHERE,config_hires);
        logTabArray(where,text);
      }
      bool boxwarn(false),memwarn(false);
      if (loop == entry::INTERPOLATION_MTZ)
      {
        double boxscale = input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").value_or_default();
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
        boxscale = 4;
#endif
        auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
        logTab(where,"Boxscale: " + dtos(boxscale));
        logTabArray(where,text);
        //if the boxscale makes the cell really huge then reduce the boxscale
        double excessively_large_cell(600.); //6*100
        auto initboxscale = boxscale;
        while (ensemble.UC.maximum_cell_dimension() > excessively_large_cell and boxscale > 4)
        {
          out::stream extra = out::VERBOSE;
          logTab(extra,"Excessively large cell = " + dtos(excessively_large_cell,0) + "A");
          logTab(extra,"Maximum cell dimension = " + dtos(ensemble.UC.maximum_cell_dimension(),0) + "A");
          boxscale--;
          logTab(extra,"Boxscale reduced due to excessively large cell: " + dtos(boxscale,0));
          ensemble.UC = UnitCell(search->EXTENT); //reset
          auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
          logTabArray(where,text);
        }
        boxwarn = (initboxscale != boxscale);
        try {
          cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
          cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
          fft::real_to_complex_3d rfft(SgInfoP1,ensemble.UC.cctbxUC);
          rfft.shannon(3);
          double savereso; cctbx::uctbx::unit_cell gridding_uc;
          bool interpolate = (loop == entry::INTERPOLATION_MTZ);
          std::tie(savereso,gridding_uc) = ensemble.UC.gridding(
            config_hires,
            cell_scale,
            interpolate,
            1.);
          rfft.calculate_gridding(savereso,gridding_uc);
        }
        catch(...)
        {
          ensemble.UC = UnitCell(search->EXTENT);
          boxscale = 4;
          memwarn = true;
          auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
          logTab(out::VERBOSE,"Boxscale reduced due to memory exhaustion: " + dtos(boxscale,0));
          logTabArray(out::VERBOSE,text);
        }
      }
      if (boxwarn)
        logAdvisory("Boxscale reduced because of excessively large cell");
      if (memwarn)
        logAdvisory("Boxscale reduced because of memory exhaustion");
     // dvect3 max_box = double(CELL_SCALE_MAX)*loop.second;
     // if (getenv("PHASER_STUDY_PARAMS_CELL") != NULL) //because studyparams can go out of range!!
     //    max_box = 4.0*max_box; //for CELL REFINEMENT
      logTab(where,"FFT Unit Cell: " + dvtos(ensemble.UC.GetBox(),2));

      Variance sfcalc;
      //The structure factors below are calculated with the WilsonB subtracted
      //they will be normalized anyway, but the density calculation is affected
      double shannon = 3;
      bool interpolate = (loop == entry::INTERPOLATION_MTZ);
      double savereso; cctbx::uctbx::unit_cell gridding_uc;
      std::tie(savereso,gridding_uc) = ensemble.UC.gridding(
          config_hires,
          cell_scale,
          interpolate,
          1.);
      sfcalc.p1_structure_factor_calculation(
          &models,
          ensemble.UC,
          savereso,
          gridding_uc,
          shannon,
          NTHREADS,
          input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()
          );

      logTab(where,"Setup Bins");
      sfcalc.setup_bins_with_statistically_weighted_bin_width(
          ensemble.UC.GetBox(),
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_min(),
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_max(),
          input.value__int_number.at(".phasertng.bins.molecular_transforms.width.").value_or_default());
      logTab(where,"Cell " + dvtos(ensemble.UC.GetBox()));
      logTab(where,"Number of reflections " + itos(sfcalc.MILLER.size()));
      logTab(where,"Number of bins " + itos(sfcalc.BIN.numbins()));
      logTab(where,"Configure resolution " + dtos(config_hires));
      logTab(where,"Save resolution " + dtos(savereso));
      logTab(where,"Cell scale range " + dtos(cell_scale.first) + "->" + dtos(cell_scale.second));
      logTab(where,"Stored high resolution (accounting for cell scale) " + dtos(sfcalc.FCALC_HIRES));
      logTab(where,"Ensemble Bin hires " + dtos(sfcalc.BIN.hires()));
      if (Suite::Extra())
      {
        logTabArray(out::TESTING,sfcalc.BIN.logBin("sfcalc"));
      }
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
      {{
      out::stream where = out::TESTING;
      logChevron(where,"Top Phaser Functionality Implemented");
      logTab(where,"Variances calculated afresh from structure factors");
      variance.UC = sfcalc.UC;
      variance.p1_structure_factor_calculation(
          &models,
          ensemble.UC,
          savereso,
          gridding_uc,
          shannon,
          input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()
          );
      variance.BIN = sfcalc.BIN;
      variance.REFLID = sfcalc.REFLID;
      //we set the overall lores as the variance lores, which is the most pessimistic
      //because this is the smallest cell
      double SIGA_RMSD = 0;
      double fsol = REFLECTIONS->MAP ?
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
      double bsol = REFLECTIONS->MAP ?
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
      double minb = REFLECTIONS->MAP ?
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();
      FourParameterSigmaA solTerm(SIGA_RMSD,fsol,bsol,minb);

      logTabArray(where,variance.logCalculationOfStatisticalWeighting());
      logUnderLine(where,"Ensemble Weight Calculation");
      {
        variance.NTHREADS = NTHREADS;
        if (Suite::Level() >= where)
        {
          variance.NTHREADS = 1;
          logBlank(where);
          logTab(where,"Output level greater than or equal to " + out::stream2String(where));
          logTab(where,"Switching to 1 thread for model weight calculation to avoid race conditions in logging");
          logBlank(where);
        }
        logEllipsisOpen(where,"Correlation matrices and LuzzatiD by resolution bin:");
        auto output = variance.calculate_modelweights(models.drms_lower(),models.SERIAL.vrms_lower,solTerm);
        logEllipsisShut(where);
        logTabArray(where,output);
        // --- OK, finally happy with the weights
      }
      logTabArray(where,variance.logVariancesAndModelWeightsByResolutionBin());
      logTabArray(where,variance.logVariancesAndModelWeights());
      logChevron(where,"End Phaser Functionality Implemented");
      }}
#endif
      search->LORES = variance.available_lores();
      logTab(where,"Variance Bin hires " + dtos(variance.BIN.hires()));
      if (Suite::Extra())
      {
        logTabArray(out::TESTING,variance.BIN.logBin("variance"));
        logTab(out::TESTING,"Difference " + etos(variance.BIN.hires()-sfcalc.BIN.hires(),10,9));
      }
      PHASER_ASSERT(variance.BIN.hires() <= sfcalc.BIN.hires()+DEF_PPM);

      math::Matrix<double> SigmaP = sfcalc.sigmap_with_binning();
      sfcalc.apply_sigmap(SigmaP);

      //-------------------------------------------------------------------------------------
      // --- OK, finally happy with the weights
      //-------------------------------------------------------------------------------------
      ensemble.MILLER = sfcalc.MILLER; //the binning is for the siga
      ensemble.ECALC.resize(sfcalc.MILLER.size());
      ensemble.initialize_bin(variance.BIN); //the binning is for the siga
      for (int r = 0; r < sfcalc.MILLER.size(); r++)
      {
        int ibin = ensemble.BIN.RBIN[r]; //initialized above
        PHASER_ASSERT(ibin < variance.SIGMAA.size());
        PHASER_ASSERT(ibin < variance.MODELWT.num_rows());
        cmplex WeightedE(0,0);
        for (int m = 0; m < nmodels; m++)
        {

          WeightedE += static_cast<double>(variance.MODELWT(ibin,m))*
                       static_cast<cmplex>(sfcalc.FCALC(m,r));
        //  cmplex WeightedF = WeightedE*std::sqrt(SigmaP[m][ibin]); //back to F from Ecalc
        }
        //remove the Siga part, for multiplication afterwards
        //this is different from phaser, where the Siga weighted Ecalc is stored
        double Siga = variance.SIGMAA[ibin]; // V is really a Covariance now
        PHASER_ASSERT(Siga>0);
        WeightedE /= Siga;
        // take the sqrt here for speed, square for variance
        //this is different from phaser, where the Siga^2 is stored
        ensemble.addData(r,WeightedE,Siga,ibin);
      }
      //now write the data as mtz file
      if (loop == entry::DECOMPOSITION_MTZ)
        search->DECOMPOSITION = ensemble;
      else if (loop == entry::INTERPOLATION_MTZ)
        search->INTERPOLATION.ParseEnsemble(ensemble); //Interpolation()
    }

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Ensemble Generation");
    logTab(where,"Resolution of Ensembles " + dtos(search->HIRES) + " " + dtos(search->LORES));
    logTab(where,snprintftos(
        "%6s %6s:%-15s  %6s %6s %6s %9s:%-15s",
        "Radius","DRMS","[Range]","Model#","Rel-B","iVRMS","VRMS","[Range]"));
    for (int m = 0; m < models.PDB.size(); m++)
    {
      pod::serial& serial = models.SERIAL;
      logTab(where,snprintftos(
         "%6s %6.4f:[% 6.4f/%6.*f] %6d  % 5.1f %6.3f %9.6f:[%6.3f/%6.3f]",
         m ? "" : dtos(search->MEAN_RADIUS,6,1).c_str(),
         serial.drms_first,serial.drms_range.first,
         (serial.drms_range.second >= 10) ? 3 : 4, serial.drms_range.second,
         m+1,
         models.RELATIVE_WILSON_B[m],
         serial.vrms_input[m],
         serial.vrms_start[m],serial.vrms_lower[m],serial.vrms_upper[m]));
    }
    logTab(where,"Trace number of atoms = " + itos(search->TRACE.size()));
    logTab(where,"Monostructure number of atoms = " + itos(search->MONOSTRUCTURE.size()));
    logTab(where,"Decomposition number of reflections = " + itos(search->DECOMPOSITION.nrefl()));
    logTab(where,"Interpolation number of reflections = " + itos(search->INTERPOLATION.nrefl()));
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Database Entries");

    //no write of FCALCS_MTZ
    fcalcs.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::FCALCS_MTZ));
    logFileWritten(out::LOGFILE,WriteData(),"Structure Factor",fcalcs);
    if (WriteData()) //dryrun
      fcalcs.write_to_disk();
    PHASER_ASSERT(models.has_relative_wilson_bfactor());
    models.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MODELS_PDB));
    logFileWritten(out::LOGFILE,WriteData(),"Models",models);
    if (WriteData()) //dryrun
      models.write_to_disk();
    variance.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::VARIANCE_MTZ));
    logFileWritten(out::LOGFILE,WriteData(),"Variance",variance);
    if (WriteData()) //dryrun
      variance.write_to_disk();

    if (trace.in_memory())
    {
      trace.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::TRACE_PDB));
      logFileWritten(out::LOGFILE,WriteFiles(),"Trace",trace);
      if (WriteFiles()) trace.write_to_disk();
    }
    else logTab(where,"Trace not in memory");
    if (monostructure.in_memory())
    {
      monostructure.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MONOSTRUCTURE_PDB));
      logFileWritten(out::LOGFILE,WriteFiles(),"Monostructure",monostructure);
      if (WriteFiles()) monostructure.write_to_disk();
    }
    else logTab(where,"Monostructure not in memory");
    Interpolation& interpolation = search->INTERPOLATION;
    if (interpolation.in_memory())
    {
      interpolation.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::INTERPOLATION_MTZ));
      logFileWritten(out::LOGFILE,WriteFiles(),"Interpolation",interpolation);
      if (WriteFiles()) interpolation.write_to_disk();
    }
    else logTab(where,"Interpolation not in memory");
    Ensemble& decomposition = search->DECOMPOSITION;
    if (decomposition.in_memory())
    {
      decomposition.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::DECOMPOSITION_MTZ));
      logFileWritten(out::LOGFILE,WriteFiles(),"Decomposition",decomposition);
      if (WriteFiles()) decomposition.write_to_disk();
    }
    else logTab(where,"Decomposition not in memory");
    }}

    {{
    FileSystem filesystem;
    filesystem.SetFileSystem(DataBase(),search->identify().FileDataBase(other::ENTRY_CARDS));
    logFileWritten(out::LOGFILE,WriteCards(),"Entry cards",filesystem);
    auto dagcards = DAGDB.entry_unparse_card(DAGDB.ENTRIES[modlid]);
    if (WriteCards()) filesystem.write(dagcards);
    //the files are read if the entry is not in memory, with the filename reconstructed from the modlid
    input.generic_string = dagcards;
    }}

    //set all the output parameters here
    input.value__uuid_number.at(".phasertng.search.id.").set_value(modlid.identifier());
    input.value__string.at(".phasertng.search.tag.").set_value(modlid.tag());
