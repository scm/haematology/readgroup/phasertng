//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pakdat_class__
#define __phasertng_pakdat_class__
#include <phasertng/main/Assert.h>
#include <set>

namespace phasertng {
namespace pod {

    struct pakflag
    {
      pakflag() {}
      const char Y = 'Y';
      const char X = 'X';
      const char Z = 'Z';
      const char O = 'O';
      const char U = 'U';
      const char Q = '?';
      //the default value is unset, then after packing become U unknown if not calculated
      char unset() const { return Q; }
      const std::vector<std::pair<char,std::string>> info = {
          { Y,"Packs" },
          { X,"Clashes" },
          { Z,"High-Tfz-Clash" },
          { O,"High-Tfz-Overlap" },
          { U,"Unknown" } }; // '?' is the not set value
      std::string title(char flag)
      {
        for (auto& item : info)
          if (item.first == flag)
            return item.second;
        return "";
      }
      //functions for comparision with flags
      bool pakset(char item) const { return std::set<char>({Y,X,Z,O,U}).count(item) == 1; }
      void chkset(char item) const { PHASER_ASSERT(pakset(item)); }
      bool packs_YUZ(char item) const { chkset(item); return std::set<char>({Y,U,Z}).count(item) == 1; }
      bool packs_XZO(char item) const { chkset(item); return std::set<char>({X,Z,O}).count(item) == 1; }
      bool packs_YU(char item)  const { chkset(item); return std::set<char>({Y,U}).count(item) == 1; }
      bool packs_YZ(char item)  const { chkset(item); return std::set<char>({Y,Z}).count(item) == 1; }
      bool packs_Y(char item)  const { chkset(item); return std::set<char>({Y}).count(item) == 1; }
      bool packs_Z(char item)  const { chkset(item); return std::set<char>({Z}).count(item) == 1; }
      std::string packs_Y_str() const { return "Y"; }
      std::string packs_YU_str() const { return "Y/U"; }
      std::string packs_XZO_str() const { return "X/Z/O"; }
    };

    struct pakdat {
      bool   fssset = false;
      int    fssnum = 0;
      double fssmax = std::numeric_limits<double>::lowest();
      double fssmaxtfz = std::numeric_limits<double>::lowest();
      double fssmaxclash = -999;
      bool   tfzset = false;
      int    tfznum = 0;
      double tfzmax = std::numeric_limits<double>::lowest();
      double tfzmaxfss = std::numeric_limits<double>::lowest();
      double tfzmaxclash = -999;
      double clash = -999;
      int    count = 0;
      void setpakdat(double fss,double tfz,int n,double c=-999) {
        if (fss > fssmax) { fssmax = std::max(fssmax,fss); fssmaxtfz = tfz; fssset = true; fssnum = n; fssmaxclash = c;}
        if (tfz > tfzmax) { tfzmax = std::max(tfzmax,tfz); tfzmaxfss = fss; tfzset = true; tfznum = n; tfzmaxclash = c;}
        clash = c; //even if negative
      }
    };

}} //phasertng
#endif
