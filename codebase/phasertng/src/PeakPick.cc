//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/PeakPick.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <phasertng/cctbx_project/cctbx/maptbx/structure_factors.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <cctbx/translation_search/symmetry_flags.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/io/FileSystem.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <phasertng/main/jiffy.h>

//#define PHASERTNG_DEBUG_PEAKPICK

namespace phasertng {

  PeakPick::PeakPick(
      SpaceGroup SG_,
      UnitCell UC_,
      double HIRES_,
      versa_grid_double real_map_unpadded_,
      af::int3 GRIDDING_
    ):
    HIRES(HIRES_),
    GRIDDING(GRIDDING_),
    UC(UC_),
    SG(SG_)
  {
    //real_map_unpadded = real_map_unpadded_; // copy array size?
    real_map_unpadded = real_map_unpadded_.deep_copy();
  }

  int
  PeakPick::number_significant(double ZonM,double percent,int minnum)
  {
    //note that sort order is zscore/M
    int count(0);
    pick::Choice h = pick::HOLE;
    pick::Choice p = pick::PEAK;
    double deepest_hole = std::numeric_limits<double>::lowest(); //take everything
    if (SITES[h].size()) deepest_hole = SITES[h][0].zscore_on_multiplicity();
    double maxzscore = 0;
    for (int j = 0; j < SITES[p].size(); j++)
    {
      auto& site = SITES[p][j];
      maxzscore = std::max(maxzscore,site.zscore_on_multiplicity());
      if (j > 0) //check sort on z/m while we are looping
      PHASER_ASSERT(site.zscore_on_multiplicity() <= SITES[p][j-1].zscore_on_multiplicity());
    }
    for (int j = 0; j < SITES[p].size(); j++)
    {
      auto& site = SITES[p][j];
      bool significant = site.zscore_on_multiplicity() >= ZonM;
      bool poverh = site.zscore_on_multiplicity() > deepest_hole;
      bool toppercent = site.zscore_on_multiplicity()/maxzscore > percent; //mean is z=0
      SITES[p][j].peak_over_deepest_hole = poverh; //for printing
      if ((j < minnum) or (poverh and significant and toppercent))
        count++;
    }
    return count;
  }

  int
  PeakPick::number(pick::Choice p)
  {
    return SITES[p].size();
  }

  void PeakPick::calculate(double store_zscore_,bool substructure,bool haveFpart)
  {
    STORE_ZSCORE = store_zscore_;
    //loop over peaks and holes
    //interpolation is different for peaks and holes
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      // --- work out mean and sigma for total search first time through
      cctbx::maptbx::statistics<double> stats =
          af::const_ref<double, af::flex_grid<> >(
              real_map_unpadded.begin(),
              real_map_unpadded.accessor().as_flex_grid());
      STATS[p].mean = stats.mean();
      STATS[p].sigma = stats.sigma();
      STATS[p].max = stats.max();
      STATS[p].min = stats.min();
      PHASER_ASSERT(STATS[p].sigma > 0);

      // --- find symmetry equivalents
      af::c_grid<3> grid_target(GRIDDING[0],GRIDDING[1],GRIDDING[2]);
      cctbx::maptbx::grid_tags<long> tags(grid_target);
      af::ref<long, af::c_grid<3> > tag_array_ref;
      if (!substructure)
      {
        cctbx::sgtbx::search_symmetry_flags sym_flags(true);
        tags.build(SG.type(),sym_flags);
        tag_array_ref =  af::ref<long, af::c_grid<3> >(
            tags.tag_array().begin(),
            af::c_grid<3>(tags.tag_array().accessor()));
      }
      else
      {
        bool isIsotropicSearchModel(false); //true doesn't run
        cctbx::translation_search::symmetry_flags sym_flags(isIsotropicSearchModel,haveFpart);
        tags.build(SG.type(),sym_flags);
        tag_array_ref =  af::ref<long, af::c_grid<3> >(
            tags.tag_array().begin(),
            af::c_grid<3>(tags.tag_array().accessor()));
      }
      af::const_ref<double, af::c_grid_padded<3> > map_const_ref(
          real_map_unpadded.begin(),
          af::c_grid_padded<3>(real_map_unpadded.accessor()));
      phaser_assert(tags.verify(map_const_ref));

      //int peak_search_level(3);
      std::size_t max_peaks(0);
      bool interpolate(true);
      cctbx::maptbx::peak_list<> peak_list(
          map_const_ref,
          tag_array_ref,
          peak_search_level,
          max_peaks,
          interpolate);

      //sorted list coming out does not take account of multiplicity
      PHASER_ASSERT(STATS[p].sigma); //i.e not a flat map

      //optical resolution 0.715*resolution
      double max_bond_dist(10.0),optical_res(0.715*HIRES);
      double separation_dist = std::min(max_bond_dist,optical_res);
      for (int j = 0; j < peak_list.sites().size(); j++)
      {
        peakinfo next;
        next.heights = peak_list.heights()[j]; ///M;
        next.sites = peak_list.sites()[j];
/*
        for (int x = 0; x < 3; x++)
          if (std::fabs(next.sites[x]) < DEF_PPM)
            next.sites[x] = 0; //round off the numbers that are really close to zero
*/
        cctbx::sgtbx::site_symmetry peak_sym(
            UC.cctbxUC,
            SG.group(),
            next.sites,
            separation_dist);
        next.zscore = (next.heights-STATS[p].mean)/STATS[p].sigma; // ZSCORE no M divisor
        next.multiplicity = SG.group().order_z()/peak_sym.multiplicity();
        if (next.zscore > STORE_ZSCORE)
          SITES[p].push_back(next);
        int iZ = std::floor(next.zscore_on_multiplicity());
        if (iZ < 0) break; //necessary for size_t comparator below
        while (iZ >= STATS[p].histogram.size())
          STATS[p].histogram.push_back(0);
        phaser_assert(iZ < STATS[p].histogram.size());
        STATS[p].histogram[iZ]++;
      }
      reverse_sort_on_zscore_over_multiplicity(p);
      for (int j = 0; j < real_map_unpadded.size(); j++)
        real_map_unpadded[j] *= -1; //switch at end
    }
  }

  af_string
  PeakPick::logHistogram(std::string description)
  {
    af_string output;
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      std::string PickChoiceString = pick::Choice2String(static_cast<pick::Choice>(p));
      output.push_back("");
      output.push_back(">>Histogram of Z-scores of " + PickChoiceString + ": " + description + "<<");
      int maxStars(0);
      for (int j = 0; j < STATS[p].histogram.size(); j++)
      {
        maxStars = std::max(maxStars,STATS[p].histogram[j]);
      }
      // the maximum across is 50
      double columns = 50;
      //Can not have less than one star per value, divStarFactor must be at least 1
      double divStarFactor = static_cast<double>(maxStars)/columns;
             divStarFactor = std::max(1.,divStarFactor);
      output.push_back("");
      int grad = static_cast<int>(divStarFactor*10);
      char buf[100];
      snprintf(buf,100,"    %-10d%-10d%-10d%-10d%-10d%-10d", 0,grad,grad*2,grad*3,grad*4,grad*5);
      output.push_back(std::string(buf));
      snprintf(buf,100,"    %-10s%-10s%-10s%-10s%-10s%-10s","|","|","|","|","|","|");
      output.push_back(std::string(buf));
      for (int h = 0; h < STATS[p].histogram.size(); h++)
      {
        std::string stars = h >= 10 ? itos(h) + "   " : " " + itos(h) + "   ";
        for (int star = 0; star < STATS[p].histogram[h]/divStarFactor; star++)
          stars += "*";
        if (STATS[p].histogram[h]) stars += " (" + itos(STATS[p].histogram[h]) + ")";
        output.push_back(stars);
      }
      output.push_back("");
    }
    return output;
  }

  af_string
  PeakPick::logStatistics(std::string description,int io_nprint)
  {
    af_string output;
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      int nsites = SITES[p].size();
      STATS[p].maxZM = std::numeric_limits<double>::lowest();
      for (int j = 0; j < nsites; j++)
        STATS[p].maxZM = std::max(STATS[p].maxZM,SITES[p][j].zscore_on_multiplicity());
    }
    output.push_back(">>Statistics " + description + "<<");
    if (SITES[pick::PEAK].size() && SITES[pick::HOLE].size())
    {
      auto highpeak = STATS[pick::PEAK].maxZM;
      auto deephole = STATS[pick::HOLE].maxZM;
      (highpeak < deephole) ?
        output.push_back("Deepest hole is deeper than highest peak is high"):
        output.push_back("Highest peak is higher than deepest hole is deep");
    }
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      std::string PickChoiceString = pick::Choice2String(static_cast<pick::Choice>(p));
      output.push_back("");
      int nsites = SITES[p].size();
      output.push_back("Statistics for " + PickChoiceString);
      output.push_back("There " + std::string(nsites == 1 ? "is " : "are ") + std::to_string(nsites) + " peak" + std::string(nsites == 1 ? "" : "s"));
      if (!nsites) break;
      //output.push_back("Maximum = " + std::to_string(STATS[p].max));
      //output.push_back("Minimum = " + std::to_string(STATS[p].min));
      //output.push_back("Mean    = "+ std::to_string(STATS[p].mean));
      //output.push_back("Sigma   = "+ std::to_string(STATS[p].sigma));
      output.push_back("Maximum Z/M = " + dtos(STATS[p].maxZM));
      output.push_back("Sites Table: " + PickChoiceString);
      char buf[100];
      snprintf(buf,100,
              "%3s %7s %7s %2s: %-30s",
              "#","Z/M","Z","M","Position (Orthogonal and Fractional)");
      output.push_back(std::string(buf));
      for (int j = 0; j < nsites; j++)
      {
        auto tmp = SITES[p][j];
        dvect3 frac(tmp.sites);
        dvect3 orth = UC.orthogonalization_matrix()*(frac);
        snprintf(buf,100,
          "%-3d %7.2f %7.2f %2d: FRAC %+6.4f %+6.4f %+6.4f   (ORTH % 7.3f % 7.3f % 7.3f)",
          j+1,
          tmp.zscore_on_multiplicity(),
          tmp.zscore,
          tmp.multiplicity,
          frac[0],frac[1],frac[2],
          orth[0],orth[1],orth[2]);
        output.push_back(std::string(buf));
        if (j+1 == io_nprint)
        {
          if (nsites > io_nprint)
            output.push_back("--- etc " + itos(nsites-io_nprint) + " more" );
          break;
        }
      }
    }
    output.push_back("");
    return output;
  }

  void
  PeakPick::WriteMap(FileSystem MAPOUT)
  {
    MAPOUT.create_directories_for_file();
    af::shared<std::string> labels(1);
    labels[0] = "Peak-Pick Map";
    af::int3 gridding_first(0,0,0);
    af::int3 gridding_last = GRIDDING;

    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));

    cctbx::maptbx::copy(real_map_const_ref, real_map_unpadded.ref());
    af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
        real_map_unpadded.begin(), //af::versa<double, af::c_grid<3> > tsmap;
        af::c_grid_padded_periodic<3>(real_map_unpadded.accessor())
      );
    cctbx::sgtbx::space_group SgOpsP1("P1");
    iotbx::ccp4_map::write_ccp4_map_p1_cell(
        MAPOUT.fstr(),
        UC.cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels.const_ref()
      );
  }

  void
  PeakPick::WritePdb(FileSystem PDBOUT)
  {
    PdbHandler pdbhandler(SG,UC,1);
    pdbhandler.addRemarkPhaser("PHASSADE");
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      af::shared<PdbRecord> model;
      std::string PickChoiceString = pick::Choice2String(static_cast<pick::Choice>(p));
      for (int j = 0; j < SITES[p].size(); j++)
      {
        PdbRecord next;
        next.AtomNum = j+1;
        next.AtomName = " CA ";
        next.ResName = "PPK";
        next.Chain = PickChoiceString.substr(0,1), // P or H
        next.ResNum = j+1;
        next.X = UC.orthogonalization_matrix()*SITES[p][j].sites;
        next.O = static_cast<double>(SITES[p][j].zscore_on_multiplicity()),
        next.B = static_cast<double>(SITES[p][j].zscore),
        model.push_back(next);
      }
      pdbhandler.addModel(model);
    }
    pdbhandler.VanillaWritePdb(PDBOUT);
  }

  void
  PeakPick::sort_on_zscore_over_multiplicity(int p)
  {
    auto cmp = []( const peakinfo &a, const peakinfo &b )
    { return a.zscore_on_multiplicity() < b.zscore_on_multiplicity(); };
    std::sort(SITES[p].begin(),SITES[p].end(),cmp); //sort on zscore_on_multiplicity
  }

  void
  PeakPick::reverse_sort_on_zscore_over_multiplicity(int p)
  {
    auto cmp = []( const peakinfo &a, const peakinfo &b )
    { return a.zscore_on_multiplicity() > b.zscore_on_multiplicity(); }; //reverse sort
    std::sort(SITES[p].begin(),SITES[p].end(),cmp); //sort on zscore_on_multiplicity
  }

  void
  PeakPick::purge_by_number(int last)
  {
    pick::Choice p = pick::PEAK;
    SITES[p].erase(SITES[p].begin() + last, SITES[p].end());
    //brutally remove the holes as they are no longer needed, just want peaks now
    p = pick::HOLE;
    SITES[p].clear();
  }

  af_string
  PeakPick::purge_close_contacts(af_dvect3 substructure,double transMax)
  {
    dmat33 orthmat = UC.cctbxUC.orthogonalization_matrix();
    af_string output;
    for (int p = 0; p < pick::ChoiceCount; p++)
    {
      size_t last = 0;
      std::string PickChoiceString = pick::Choice2String(static_cast<pick::Choice>(p));
      int tsize = SITES[p].size();
      for (int s = 0; s < tsize; s++)
      {
        bool close_contact(false);
        auto site = SITES[p][s].sites;
        for (int j = 0; j < substructure.size() and !close_contact; j++)
        {
          for (unsigned isym = 0; isym < SG.group().order_z() and !close_contact; isym++)
          {
            dvect3 sssite = SG.doSymXYZ(isym,substructure[j]);
            dvect3 fracdelta = site - sssite;
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracdelta[j] < -0.5) { fracdelta[j] += 1; }
              while (fracdelta[j] >= 0.5) { fracdelta[j] -= 1; }
            }
            dvect3 delta = orthmat*fracdelta;
            if (delta.length() < transMax)
              close_contact = true;
          }
        }
        if (!close_contact)
          SITES[p][last++] = SITES[p][s];
      }
      //erase rubbish
      SITES[p].erase(SITES[p].begin() + last, SITES[p].end());
      (p == pick::PEAK) ?
        output.push_back("Number of peaks near substructure = " + itos(tsize-last) + " out of " + itos(tsize)):
        output.push_back("Number of holes near substructure = " + itos(tsize-last) + " out of " + itos(tsize));
    }
    return output;
  }
} //end namespace phaser
