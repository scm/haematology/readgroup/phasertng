//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DLUZZATI_CLASS__
#define __PHASER_DLUZZATI_CLASS__

namespace phaser {

inline double DLuzzati(double Ssqr,double vrms)
{
  return std::exp(-two_pi_sq_on_three*vrms*vrms*Ssqr);
}

} // phaser
#endif
