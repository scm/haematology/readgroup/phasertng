//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Edge_class__
#define __phasertng_dag_Edge_class__
#include <phasertng/main/Identifier.h>
#include <set>

namespace phasertng {
namespace dag {

  class Edge
  {
    public:
      Identifier  ULTIMATE;
      Identifier  PENULTIMATE;
    public:
      Edge() {}
      bool is_default() { return ULTIMATE.is_set_valid() and PENULTIMATE.is_set_valid(); }
      void set_default() { ULTIMATE = Identifier(); PENULTIMATE = Identifier(); }
      std::set<Identifier> PROPREANTEPENULTIMATE;

      void shift_edge(const std::string,const std::string);
      void increment(const std::string);
      std::string str() const { return PENULTIMATE.str() + "->" + ULTIMATE.str(); }

    public:
    Edge& operator=(const Edge o) { ULTIMATE = o.ULTIMATE; PENULTIMATE = o.PENULTIMATE; return *this; }
  };

}} //phasertng
#endif
