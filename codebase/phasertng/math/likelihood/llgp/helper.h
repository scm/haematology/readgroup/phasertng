//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_llgp_helper_functions__
#define __phasertng_likelihood_llgp_helper_functions__
#include <complex>
#include <cctbx/miller.h>
#include <scitbx/vec3.h>

typedef std::complex<double> cmplex;
typedef scitbx::vec3<double> dvect3;
typedef cctbx::miller::index<int> millnx;

namespace phasertng { namespace likelihood { namespace llgp {

  double
  DobsSigaSqr(
      double SSQR,double SigaSqr,double FS,double DOBS,double DRMS,double BFAC);

  cmplex
  EcalcTerm(
      double DobsSigaSqr,double EPS,double NSYMP,millnx SymHKL,dvect3 TRA,double symTra);

  double
  EcalcSqrTerm(
      double DobsSigaSqr,double EPS,double NSYMP);

}}} //phasertng
#endif
