# LIBTBX_SET_DISPATCHER_NAME phenix.phasertng

# XXX: imports which rely on compiled modules are postponed (for speed)

import sys, os
import libtbx.load_env
from libtbx import easy_run
from libtbx.utils import Sorry, Usage
from phasertng import *
from phasertng.phil.master_phil_file import *
from phasertng.phil.converter_registry import *


def run_binary (args) :
  exe = ""
  if (os.name == "nt"): exe = ".exe"
  phasertng_path = libtbx.env.under_build(path="phasertng/exe/phasertng"+exe)
  if not os.path.isfile(phasertng_path) :
    phasertng_path = libtbx.env.under_build(path="exe/phasertng"+exe)
  assert os.path.isfile(phasertng_path)
  assert phasertng_path.find('"') < 0
  if "LIBTBX__VALGRIND_FLAG__" in os.environ :
    if not "LIBTBX_VALGRIND" in os.environ :
      raise Sorry("LIBTBX_VALGRIND not defined - exiting.")
    cmd = '%s "%s"' % (os.environ["LIBTBX_VALGRIND"], phasertng_path)
  else :
    cmd = '"'+phasertng_path+'"'
  qargs = []
  for arg in args:
    assert arg.find('"') < 0
    qargs.append('"'+arg+'"')
  return easy_run.call("%s %s" % (cmd, " ".join(qargs)))

def phasertng_wrapper (args) :
  started_phasertng = False
  exit_code = 0
  showdefaults = False
  # AJM these command line args need to match the ones in main.cc in phasertng
  suites =  [ 'ccp4', 'phenix', 'voyager', ]
  flagargs = [ '--version', '--codes', '--change_log',]
  helpargs =  [ '--help', '--options', '--usage',]
  if (len(args) == 0) :
    exit_code = run_binary(args)
    started_phasertng = True
  elif (len(args) > 1) :
    started_phasertng = False
  elif any(args[0] == f for f in flagargs):
    exit_code = run_binary(args)
    started_phasertng = True
  elif any(args[0] == f for f in helpargs):
    started_phasertng = False
  elif args[0].startswith('--suite') :
    level = args[0].strip().split('=')
    if len(level) > 1:
      level = level[1]
      if any(f == level.upper() for f in suites) or any(f == level.lower() for f in suites):
        exit_code = run_binary(args)
        started_phasertng = True
    else:
      started_phasertng = False
  elif args[0].startswith('--show-defaults') or args[0].startswith('--show_defaults') :
    level = args[0].strip().split('=')
    if len(level) > 1:
      level = level[1]
    else:
      level = 0
  # from phasertng import phenix_interface
  # phenix_interface.master_phil().show()
  # Define PHIL scope
    initparams = build_phil_str(program_name="InputCard")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    master_phil.show(attributes_level=int(level))
    started_phasertng = True
  if not started_phasertng :
    usagestr = "\n"
    for u in suites:
      usagestr += "  phenix.phasertng --suite=" + u + "\n"
    for u in flagargs:
      usagestr += "  phenix.phasertng " + u + "\n"
    for u in helpargs:
      usagestr += "  phenix.phasertng " + u + "\n"
    usagestr += "  phenix.phasertng --show-defaults=n [where n is level]\n"
    raise Usage(usagestr)
  sys.exit(exit_code)

if __name__ == "__main__" :
  phasertng_wrapper(sys.argv[1:])
