#include <phasertng/math/likelihood/EELLG_analytical.h>
#include <scitbx/constants.h>
#include <scitbx/array_family/misc_functions.h>

using namespace scitbx;

namespace phasertng {
namespace likelihood {

  double EELLG_analytical::get(double fs, double vrms, double smax,
               double fsol, double bsol, int nref)
  {
    // Analytical eeLLG, if coordinate errors were drawn from a single 3D Gaussian
    // with an overall rmsd of vrms. Optionally accounts for effect of bulk solvent.
    // Optionally multiplies by number of reflections to turn into total eeLLG.
    double pi = scitbx::constants::pi;
    double sqrtpi = std::sqrt(pi);
    double pi_sq = scitbx::constants::pi_sq;
    double smax2 = fn::pow2(smax);
    double vrms2 = fn::pow2(vrms);
    double X0 = 4 * (constants::two_pi_sq/3.) * vrms2 * smax2;
    double eeLLG = 3. / (128 * pi_sq * sqrtpi * fn::pow3(vrms)) *
                   (std::sqrt(6.) * std::erf(std::sqrt(X0)) - 8 * std::exp(-X0) * sqrtpi * vrms * smax);
    if (fsol > 0)
    {
      double dterm1 = 3 * bsol + 16 * pi_sq * vrms2;
      double X1 = smax2 * dterm1 / 6.;
      double dterm2 = dterm1 + 16 * pi_sq * vrms2;
      double X2 = smax2 * dterm2 / 12.;
      eeLLG += 3 * fn::pow2(fsol) *
                (std::erf(std::sqrt(X1)) * std::sqrt(3.*pi/(2*dterm1)) - std::exp(-X1) * smax) / dterm1 +
               12 * fsol * (std::exp(-X2) * smax - std::sqrt(3*pi/dterm2)*std::erf(std::sqrt(X2)))/dterm2;
    }
    eeLLG *= nref * 3. * fn::pow2(fs) / (2 * fn::pow3(smax));
    return eeLLG;
  }
}
}
