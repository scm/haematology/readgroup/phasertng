//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_EFOM_class__
#define __phasertng_EFOM_class__

namespace phasertng {
namespace likelihood {

class EFOM
{
  public:
    EFOM() {}

  public:
    double get(double Eeff, double DobsSigA, bool cent);

  private:
    double centric(double Eeff, double DobsSiga);
    double acentric(double Eeff, double DobsSigA);

};

}}
#endif
