#ifndef __phasertng_math_mst_class__
#define __phasertng_math_mst_class__
#include <scitbx/vec3.h>
#include <vector>
#include <limits>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace math {

inline scitbx::vec3<double> mean_sigma_top(std::vector<double>& v)
{
  if (v.size() < 3) return { 0,0,0 };
  double sum = std::accumulate(v.begin(), v.end(), 0.0);
  double f_mean = sum/ v.size();
  std::vector<double> diff(v.size());
  for (int i = 0; i < v.size(); i++) diff[i] = v[i] - f_mean;
  double sq_sum(0);
  for (int i = 0; i < diff.size(); i++) sq_sum += diff[i]*diff[i] ;
  double f_sigma = std::sqrt(sq_sum / v.size());
  double f_top = std::numeric_limits<double>::lowest();
  for (int i = 0; i < v.size(); i++) f_top = std::max(f_top,v[i] );
 // double z_top = (f_top-f_mean)/f_sigma;
  if (f_sigma < 1.0e-06) return { f_top,1,f_top }; //so that (f_top-f_mean)/sigma = 0 without /0
  return { f_mean, f_sigma, f_top };
}

}}
#endif
