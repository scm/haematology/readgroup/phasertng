//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Hold_class__
#define __phasertng_dag_Hold_class__
#include <phasertng/main/Identifier.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Hold
  { //hold this entry in memory
    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;

    // Overloaded operators
    bool
    operator<( const Hold& rhs ) const
    { //for std::set
      return IDENTIFIER < rhs.IDENTIFIER;
    }

    bool operator==(const Hold& other) const
    { return IDENTIFIER == other.IDENTIFIER; }

  };

}} //phasertng
#endif
