#ifndef __phasertng_table_alogch_class__
#define __phasertng_table_alogch_class__
#include <vector>
#include <cmath>
#include <iostream>

namespace phasertng {
namespace table {

class alogch
{
  private:
    int    tbllen;
    double argcut1,argcut2;
    //derived params
    double argfac1,argfac2;
    double log2 = std::log(2.);
    std::vector<std::pair<double,double> > lnchTable1;
    std::vector<std::pair<double,double> > lnchTable2;

  public:
    alogch();

    alogch(
      int tbllen_= 2048,
      double argcut1_= 2.,
      double argcut2_= 10.
      ) :
      tbllen(tbllen_),
      argcut1(argcut1_),
      argcut2(argcut2_)
    {
      argfac1 = tbllen/argcut1;
      argfac2 = tbllen/argcut2;
      lnchTable1 = getlnchTable(tbllen,argcut1);
      lnchTable2 = getlnchTable(tbllen,argcut2);
   }

   void
   print_tables()
   {
  std::cout << "static constexpr cont double lnchTable1[2049][2] = {" << std::endl;
  for (int i = 0; i < lnchTable1.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnchTable1[i].first << "," << lnchTable1[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr cont double lnchTable2[2049][2] = {" << std::endl;
  for (int i = 0; i < lnchTable2.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnchTable2[i].first << "," << lnchTable2[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
    }

  private:
    std::vector<std::pair<double,double> >
    getlnchTable(int tbllen_, double argmax)
    {
      std::vector<std::pair<double,double> > lnchTable;
      lnchTable.resize(tbllen_+1);
      double darg(argmax/tbllen_);
      double arg0(0),arg1(0),x0(0),x1(0);
      for (unsigned i = 0; i < tbllen_+1; i++)
      {
        arg0 = (i  )*darg;
        arg1 = (i+1)*darg;
        x0 = std::log(std::cosh(arg0));
        x1 = std::log(std::cosh(arg1));
        lnchTable[i].first = x0;
        lnchTable[i].second = x1 - x0;
      }
      return lnchTable;
    }

};

} }
#endif
