//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_fft_complex_to_complex_3d_class__
#define __phasertng_fft_complex_to_complex_3d_class__
#include <phasertng/math/fft/fftbase.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>

typedef scitbx::af::shared<cmplex> af_cmplex;
typedef scitbx::af::shared<millnx> af_millnx;

namespace phasertng { namespace fft {

  class complex_to_complex_3d : public fftbase
  {
    public:
      complex_to_complex_3d() : fftbase() {}
      complex_to_complex_3d(cctbx::sgtbx::space_group_type, cctbx::uctbx::unit_cell);
      bool anomalous_flag  = true;
      scitbx::fftpack::complex_to_complex_3d<double> cfft;
      void allocate();
      //concrete of virtual base
      cctbx::af::c_interval_grid<3>::index_type n_grid() const { return cfft.n(); }

    private:
      af::versa<cmplex, af::c_grid_padded_periodic<3> > complex_map;
      //aka cctbx::xray::sampled_model_density<>::complex_map_type complex_map;

    public:
      af::const_ref<cmplex, scitbx::af::c_grid_padded_periodic<3>>
      backward(af::shared<millnx>,af::shared<cmplex>);

      af_cmplex
      backward(af::shared<cctbx::xray::scatterer<double>>,
               cctbx::xray::scattering_type_registry,
               af_millnx);
  };

}}
#endif
