//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_SPACE_GROUP_NAME_CLASS__
#define __PHASER_SPACE_GROUP_NAME_CLASS__
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>

namespace phaser {

class space_group_name
{
  public:
    std::string HALL;
    std::string CCP4;
    bool        error,hall_error;

    space_group_name(std::string name,bool is_hall)
    {
      hall_error = error = false;
      if (is_hall)
      { //fudge for CCP4 output
        try {
          cctbx::sgtbx::space_group cctbxSG(name,"A1983");
          HALL = name;
        }
        catch (...) { hall_error = true; }

        try {
          cctbx::sgtbx::space_group cctbxSG(name,"A1983");
          std::string HM = cctbxSG.type().universal_hermann_mauguin_symbol();
          if (HM.find("R 3 :H (") != std::string::npos) CCP4 = "R 3";  //R 3 :H (-y+z,x+z,-x+y+z)
          else if (HM.find("R 3 2 :H (") != std::string::npos) CCP4 = "R 3 2";  //change of basis in brackets
          else if (HM.find("R 3 :R") != std::string::npos) CCP4 = "R 3";
          else if (HM.find("R 3 2 :R") != std::string::npos) CCP4 = "R 3 2";
          else if (HM.find("R 3 :H") != std::string::npos) CCP4 = "H 3";
          else if (HM.find("R 3 2 :H") != std::string::npos) CCP4 = "H 3 2";
          else CCP4 = cctbxSG.match_tabulated_settings().universal_hermann_mauguin();
          if (!CCP4.size())
          {
            error = true;
            for (int t = 0; t < HM.size(); t++)
            {
              if (HM[t] == '(') break;
              CCP4 += HM[t]; //for output
            }
          }
        }
        catch (...) { error = true; }
      }
      else
      {
        try {
          if (name == "R3") name = "R 3 :R";
          if (name == "R 3") name = "R 3 :R";
          if (name == "R32") name = "R 3 2 :R";
          if (name == "R 32") name = "R 3 2 :R";
          if (name == "R 3 2") name = "R 3 2 :R";
          if (name == "H3") name = "R 3 :H";
          if (name == "H 3") name = "R 3 :H";
          if (name == "H32") name = "R 3 2 :H";
          if (name == "H 32") name = "R 3 2 :H";
          if (name == "H 3 2") name = "R 3 2 :H";
          cctbx::sgtbx::space_group_symbols Symbols(name,"A1983");
          HALL = Symbols.hall();
          CCP4 = name;
        }
        catch (...) { error = true; }
      }
    }

};

} //phaser

#endif
