//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
// some matrix operations
#ifndef __phasertng_mat_ops__
#define __phasertng_mat_ops__
#include <vector>
#include <phasertng/main/Assert.h>
#include <phasertng/math/Matrix.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>

namespace phasertng {
namespace math
{

template <class T>
Matrix<T> transpose(const Matrix<T> &A)
{
  int M = A.num_rows();
  int N = A.num_cols();

  Matrix<T> S(N,M);
  for (int i=0; i<M; i++)
    for (int j=0; j<N; j++)
      S(j,i) = A(i,j);

  return S;
}

template <typename T>
std::vector<T> mat_vec_mult(const Matrix<T>& A, const std::vector<T>& x)
{
  // Variable names used follow those used in the classic equation Ax = b.
  // b_i = Sum [ A_ij * x_j ], (summing over j).
  // Note, an M*N matrix times an N*1 vector results in a M*1 vector.

  int N = A.num_cols();
  phaser_assert(x.size() == N);
  int M = A.num_rows();

  std::vector<T> b(M);

  T sum(0);
  for (int i=0; i<M; i++)
  {
    sum=0;
    for (int j=0; j<N; j++)
      sum += A(i,j) * x[j];
    b[i] = sum;
  }

  return b;
} // mat_vect_mult

template <typename T>
std::vector<T> neg_mat_vec_mult(const Matrix<T>& A, const std::vector<T>& x)
{
  // Variable names used follow those used in the classic equation Ax = b.
  // b_i = Sum [ A_ij * x_j ], (summing over j).
  // Note, an M*N matrix times an N*1 vector results in a M*1 vector.

  int N = A.num_cols();
  phaser_assert(x.size() == N);
  int M = A.num_rows();

  std::vector<T> b(M);

  T sum(0);
  for (int i=0; i<M; i++)
  {
    sum=0;
    for (int j=0; j<N; j++)
      sum += A(i,j) * x[j];
    b[i] = -sum; // note minus, c.f. mat_vec_mult
  }

  return b;
} // neg_mat_vect_mult

template <typename T>
std::vector<T> mat_vec_mult(const scitbx::af::versa<T, scitbx::af::flex_grid<> > A, const std::vector<T> x)
{
  // Variable names used follow those used in the classic equation Ax = b.
  // b_i = Sum [ A_ij * x_j ], (summing over j).
  // Note, an M*N matrix times an N*1 vector results in a M*1 vector.

  int N = A.accessor().all()[1];
  phaser_assert(x.size() == N);
  int M = A.accessor().all()[0];

  std::vector<T> b(M);

  T sum(0);
  for (int i=0; i<M; i++)
  {
    sum=0;
    for (int j=0; j<N; j++)
      sum += A(i,j) * x[j];
    b[i] = sum;
  }

  return b;
} // mat_vect_mult

template <typename T>
std::vector<T> neg_mat_vec_mult(const scitbx::af::versa<T, scitbx::af::flex_grid<> > A, const std::vector<T> x)
{
  // Variable names used follow those used in the classic equation Ax = b.
  // b_i = Sum [ A_ij * x_j ], (summing over j).
  // Note, an M*N matrix times an N*1 vector results in a M*1 vector.

  int N = A.accessor().all()[1];
  phaser_assert(x.size() == N);
  int M = A.accessor().all()[0];

  std::vector<T> b(M);

  T sum(0);
  for (int i=0; i<M; i++)
  {
    sum=0;
    for (int j=0; j<N; j++)
      sum += A(i,j) * x[j];
    b[i] = -sum; // note -ve sign, c.f. mat_vec_mult(...)
  }

  return b;
} // neg_mat_vect_mult

template <typename T>
Matrix<T> mat_mat_mult(const Matrix<T>& A, const Matrix<T>& B)
{
  // Variable names follow those in the matrix equation AB = C.
  // C_ik = Sum [ A_ij * B_jk] (summing over j).
  // Note, an M*N matrix times an N*P matrix results in a M*P matrix.

  int M = A.num_rows();
  int N = A.num_cols();
  phaser_assert(N == B.num_rows());
  int P = B.num_cols();

  Matrix<T> C(M,P);

  T sum(0);
  for (int i=0; i<M; i++)
  {
    for (int k=0; k<P; k++)
    {
      sum = 0;
      for (int j=0; j<N; j++)
        sum += A(i,j) * B (j,k);
      C(i,k) = sum;
    }
  }

  return C;
} // mat_mat_mult


}} // namespace math
#endif
