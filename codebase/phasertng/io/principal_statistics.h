//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_principal_statistics__class__
#define __phasertng_principal_statistics__class__
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/array_family/shared.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::af::shared<dvect3> af_dvect3;
typedef scitbx::af::shared<int> af_int;

namespace phasertng {

class principal_statistics
{
  public: //members
    double MEAN_RADIUS = 0;
    double MAX_RADIUS = 0;
    double EXTENT_RATIO = 0;
    dvect3 MINBOX = dvect3(0,0,0);
    dvect3 MAXBOX = dvect3(0,0,0);
    dvect3 EXTENT = dvect3(0,0,0);
    dvect3 CENTRE = dvect3(0,0,0);

  void set(af_dvect3);
  void set_atom();
};

} //phasertng
#endif
