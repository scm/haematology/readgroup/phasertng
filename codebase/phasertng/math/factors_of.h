#ifndef __phasertng_factors_of_function__
#define __phasertng_factors_of_function__
#include <vector>

namespace phasertng {

  int factor(int,int,int,bool);
  std::vector<int> factors_of(int); //include original

}//end namespace phaser

#endif

