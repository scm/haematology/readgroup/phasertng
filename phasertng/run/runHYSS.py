import sys
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
import os, os.path

#these are internal parameters not interesting for the user
class runHYSS:

  def run_from_basetng_and_input(self,basetng):
    self.inner_hyss(basetng=basetng)

  def inner_hyss(self, basetng):
 #{{{
    hyss_phil = isNone(basetng.input.get_as_str(".phasertng.substructure.hyss.phil."))
    hyss_command_line = isNone(basetng.input.get_as_str(".phasertng.substructure.hyss.command_line."))
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0):
   #{
      basetng.logTab(enum_out_stream.logfile,"No nodes")
      return
   #}
    import os
    DAGDB.shift_tracker(str(basetng.hashing()),"hyss")
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    root = os.path.join(basetng.DataBase(),subdir)
    wf = basetng.Write()
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(root):
      os.makedirs(root)
    wf = basetng.WriteData()
    if not wf: #stupid option for this mode, so just exit
      basetng.logTab(enum_out_stream.logfile,str("No files written: exit"))
      return
    node = DAGDB.NODES.LIST[0]
    try:
      from phenix.command_line.hyss import run
    except:
      raise ValueError(enum_err_code.input,str("Hyss not installed"))
      return
    command_line = ['phenix.hyss']
    command_line.append(basetng.input.get_as_str(".phasertng.hklin.filename.")[1:-1]) #remove quotes
    command_line.append(basetng.input.get_as_str(".phasertng.substructure.determination.scatterer."))
    fast_debug = False
  # don't use maximum find for hyss, only for phassade, where completion is used for larger substructures
  # nmax = float(basetng.input.get_as_str(".phasertng.substructure.determination.maximum_find."))
    nfind = float(basetng.input.get_as_str(".phasertng.substructure.determination.find."))
    nfind = round(nfind)
  # nfind = round(min(nmax,nfind))
    if fast_debug:
      command_line.append("1")
    else:
      command_line.append(str(nfind))
    command_line.append("wavelength="+basetng.input.get_as_str(".phasertng.reflections.wavelength."))
    command_line.append("nproc="+basetng.input.get_as_str(".phasertng.threads.number."))
   #command_line.append(" output_dir="+root) doesn't work
    def fname(f):
      return FileSystem(basetng.DataBase(),subdir,"hyss" + str(f))
    command_line.append("root="+fname("").fstr())
    if fast_debug:
      command_line.append("rescore="+"correlation")
      command_line.append("phaser_completion=False")
    if hyss_phil is not None:
      command_line.append(hyss_phil)
    if hyss_command_line is not None:
      for command in hyss_command_line.split():
        command_line.append(command)
    basetng.logTab(enum_out_stream.logfile,str(' '.join(command_line)))
    outstr = None #TextIO()
    error_thrown = False
    use_subprocess = True
    try:
      if use_subprocess:
        import subprocess
        basetng.logEllipsisOpen(enum_out_stream.logfile,"Running subprocess")
        result = subprocess.run(command_line, stdout=subprocess.PIPE,text=True)
      else:
        basetng.logEllipsisOpen(enum_out_stream.logfile,"Running")
        run(args=command_line[1:],out=outstr)
    except Exception as e:
        message = "" if str(e) is None else str(e)
        error_thrown = True
       #don't log error yet
       #raise ValueError(enum_err_code.fatal),message)
    #report log first
    basetng.logEllipsisShut(enum_out_stream.logfile)
    basetng.logUnderLine(enum_out_stream.logfile,"Hyss Results")
    if (outstr is not None):
      basetng.logTabArray(enum_out_stream.logfile,outstr.getvalue().split("\n"))
    elif (result is not None):
      for line in result.stdout.split("\n"):
#annoyingly the first of the pdb remark cards starts logging with a space
        line = line.replace(" REMARK","REMARK")
        line = line.replace(" Identifying","Identifying")
        if line.startswith("Showing top") or \
             "Resolution" in line or \
             "best" in line or \
             "kept" in line or \
             line.startswith("p=") or \
             line.startswith("Number of matching sites of top") or \
             ("consensus" in line and \
              "REMARK" not in line and \
              "skip_consensus" not in line):
          basetng.logTab(enum_out_stream.logfile,line)
        else:
          basetng.logTab(enum_out_stream.verbose,line)
    if error_thrown: #now log the error
      basetng.logBlank(enum_out_stream.logfile)
      raise ValueError(enum_err_code.fatal,message)
   #hardwire the files we know are written
    basetng.logBlank(enum_out_stream.logfile)
    if True:
      where = enum_out_stream.summary
      basetng.logUnderLine(where,"Hyss Coordinates")
      with open(fname("_hyss_consensus_model.pdb").fstr(), 'r',encoding="utf8", errors='ignore') as f:
        f_content = f.read()
        assert(len(f_content) > 0)
      for line in f_content.split("\n"):
        basetng.logTab(enum_out_stream.summary,line);
      basetng.logBlank(where)
      basetng.logUnderLine(where,"Hyss File Report")
      basetng.logFileWritten(where,wf,"Anomalous diffs",fname("_anom_diffs.hkl"))
      basetng.logFileWritten(where,wf,"Anomalous diffs",fname("_anom_diffs.ins"))
      basetng.logFileWritten(where,wf,"Consensus model",fname("_hyss_consensus_model.pdb"))
      basetng.logFileWritten(where,wf,"Consensus model",fname("_hyss_consensus_model.pickle"))
      basetng.logFileWritten(where,wf,"Consensus model",fname("_hyss_consensus_model.sdb"))
      basetng.logFileWritten(where,wf,"Consensus model",fname("_hyss_consensus_model.xyz"))
      basetng.logFileWritten(where,wf,"Models         ",fname("_hyss_model.pickle"))
      basetng.input.set_as_str(".phasertng.atoms.filename.",fname("_hyss_consensus_model.pdb").fstr())
      basetng.logBlank(where)
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  cards = None
  if args.cards is not None:
    cards = args.cards.read()
  if directory is not None:
    cards = cards + "\nphasertng suite database = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng.DAGDATABASE.parse_cards(cards)
  basetng = runHYSS().inner_hyss(basetng=basetng)
  print(basetng.summary())
