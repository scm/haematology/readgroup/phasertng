//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Packing_class__
#define __phasertng_Packing_class__
#include <phasertng/main/pointers.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <unordered_set>
#include <set>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <phasertng/math/Matrix.h>
typedef scitbx::af::versa<int, scitbx::af::flex_grid<> > versa_flex_int;


namespace phasertng {

namespace pod {

class packing_data
{
  private:
    //matrix is not symmetric due to difference in sampling!
    versa_flex_int cmatrix;
    int N = 0;
    std::vector< std::pair<int, int> > n_overlap;
    //percent overlap stored as integers, percent = 100*first/second

  public:
    packing_data() {}

  public:
  void initialize(sv_int clashes,sv_ivect3 traces)
  {
    N = std::sqrt(clashes.size());
    assert(fn::pow2(N) == clashes.size());
    cmatrix.resize(scitbx::af::flex_grid<>(N,N));
    int i(0);
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        cmatrix(p,q) = clashes[i++];
    n_overlap.resize(traces.size());
    for (i = 0; i < traces.size(); i++)
    { //convert list of vector elements into index lookup on first element
      //int pose_index = traces[i][0];
      assert(traces[i][0]-1 < n_overlap.size());
//This is the key check. The incoming array in traces, which is in pose order, must match
//the index in the ivect3 first element, or else the packing will not correspond with
//the pose order assert(i == p);
      //percent of clashes for index [0] is [1]/[2]
      n_overlap[i] = { traces[i][1], traces[i][2] };
    }
  }

  //versa_flex_int matrix_as_matrix()
  //{ return cmatrix; }

  sv_int matrix_as_vector() const
  {
    sv_int vec;
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        vec.push_back(cmatrix(p,q));
    return vec;
  }

  bool one_bad(double CUTOFF,sv_bool INCLUDED) const
  {
    if (!INCLUDED.size())
      INCLUDED.resize(N,true);
    assert(INCLUDED.size() == N);
    for (int p = 0; p < N; p++)
    for (int q = 0; q < N; q++)
    if (INCLUDED[p] && INCLUDED[q])
    if (percent(p,q) > CUTOFF)
    { //p and q set
      return true;
    }
    return false;
  }

  std::pair<int,int> worst_ratio() const
  {
    double worst(0);
    std::pair<int,int> ratio = {0,0};
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        if (percent(p,q) > worst)
        {
          worst = percent(p,q);
          ratio = {cmatrix(p,q),n_overlap[q].second};
        }
    return ratio;
  }

  double worst_percent() const
  {
    double worst = 0;
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        worst = std::max(percent(p,q),worst);
    return worst;
  }

  sv_double sorted_percent(int n) const
  {
    sv_double worst;
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        if (percent(p,q) > 0.5) //rounded non-zero only, which are boring
          worst.push_back(percent(p,q));
    std::sort(worst.begin(),worst.end());
    std::reverse(worst.begin(),worst.end());
    if (worst.size() > n) worst.erase(worst.begin()+n,worst.end());
    return worst;
  }

  int extend_matrix(int trace_length)
  {
    versa_flex_int new_cmatrix(scitbx::af::flex_grid<>(N+1,N+1));
    for (int p = 0; p < N+1; p++)
      for (int q = 0; q < N+1; q++)
        new_cmatrix(p,q) = 0;
    for (int p = 0; p < N; p++)
      for (int q = 0; q < N; q++)
        new_cmatrix(p,q) = cmatrix(p,q);
    cmatrix = new_cmatrix.deep_copy();
    N = cmatrix.size();
    n_overlap[N-1] = {0, trace_length};
    return N-1;
  }

  //increment the count in matrix element ij
  void inc(int i,int j)
  { cmatrix(i,j) = cmatrix(i,j) + 1; }

  //increment the count in matrix element ij
  void clear_overlap()
  { n_overlap.clear(); }

  void add_overlap(int i,int j)
  { n_overlap.push_back({i,j}); }

  //calculate the percent of trace points corresponding to matrix element ij
  double percent(int i,int j) const
  {
    double p = 100.*static_cast<double>(cmatrix(i,j))/static_cast<double>(n_overlap[j].second);
    //return (p < 1) ? 0 : p; //cannot return 0->1 as this is interpreted as "not a percent"
    return p;
  }

  //calculate the percent of overlap for entire pose i
  double overlap_percent(int i) const
  {
    double p = 100.*static_cast<double>(n_overlap[i].first)/static_cast<double>(n_overlap[i].second);
    //return (p < 1) ? 0 : p; //cannot return 0->1 as this is interpreted as "not a percent"
    return p;
  }

  //matrix size
  const  int&  size() const
  { return N; }

  math::Matrix<int>
  matrix_as_matrix() const
  {
    std::vector<int> v = matrix_as_vector();
    int  N = size();
    math::Matrix<int> m(N,N,&v[0]);
    return m;
  }
};
}

  class Coordinates; //forward declaration
  class FileSystem; //forward declaration

  class Packing
  {
    private: //members
      dvect3 CLOSEST_TO_ORIGIN_CENTRE = dvect3(0,0,0);

      std::vector<std::unordered_set<int>> CLASH_ATOMS = std::vector<std::unordered_set<int>>();
      double DISTANCE_FACTOR = 1;

    public: //members
      bool MOVED_TO_ORIGIN = false;
      bool MOVED_TO_COMPACT = false;
      dvect3 CENTRE_OF_MASS = dvect3(0,0,0);
      SpaceGroup   SG;
      UnitCell     UC;
      dag::Node*   NODE;
      pod::packing_data PACKING_DATA;

    public:  //constructor
      Packing(double=1); //dfactor
      void init_refine_parameters();
      void init_node(dag::Node*);
      ~Packing() throw() {}

    public:
      void move_to_origin(bool,dvect3,int);
      void multiplicity_and_exact_site();
      void move_to_compact(bool);
      void calculate_packing(bool,bool);

      bool calculate_special_position_warning();
      void WritePosePdb(FileSystem,int);
      void WritePdb(FileSystem);
     // void WriteMonostructure(FileSystem);
      af_string structure_for_refinement(Coordinates&,bool=false);

    public:
      af_string logClashes(std::string) const;
  };

} //phasertng
#endif
