//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/fft/complex_to_complex_3d.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Assert.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>

namespace phasertng { namespace fft {

  complex_to_complex_3d::complex_to_complex_3d(
       cctbx::sgtbx::space_group_type SG_,
       cctbx::uctbx::unit_cell UC_
       ) : fftbase(SG_,UC_)
  {}

  void
  complex_to_complex_3d::allocate()
  {
    if (!memory_allocation_error)
    {
      cfft = scitbx::fftpack::complex_to_complex_3d<double>(gridding);
    }
  }

  af::const_ref<cmplex, scitbx::af::c_grid_padded_periodic<3> >
  complex_to_complex_3d::backward(
      af::shared<millnx> MILLER,
      af::shared<cmplex> COEFFICIENTS)
  {
    if (memory_allocation_error)
      throw Error(err::MEMORY,"FFT array allocation");
    cctbx::sgtbx::space_group      SgOps(SG.group());
    phaser_assert(MILLER.size() == COEFFICIENTS.size());

    bool treat_restricted(false);
    af::c_grid_padded<3> map_grid(cfft.n());
    cctbx::maptbx::structure_factors::to_map<double> complex_map(
        SgOps, //PtNcs requires P1
        anomalous_flag,
        MILLER.const_ref(),
        COEFFICIENTS.const_ref(),
        cfft.n(),
        map_grid,
        conjugate_flag,
        treat_restricted
      );
    af::ref<cmplex, af::c_grid<3> > complex_map_fft_ref(
        complex_map.complex_map().begin(),
        af::c_grid<3>(cfft.n()));

    // --- do the fft
    cfft.backward(complex_map_fft_ref);

    af::const_ref<cmplex, scitbx::af::c_grid_padded_periodic<3> > complex_map_cref(
        complex_map_fft_ref.begin(),
        scitbx::af::c_grid_padded_periodic<3>(cfft.n(), cfft.n()));

    return complex_map_cref;
  }

  af_cmplex
  complex_to_complex_3d::backward(
      af::shared<cctbx::xray::scatterer<double>> xray_scatterers,
      cctbx::xray::scattering_type_registry scattering_type_registry,
      af_millnx selected_to_bin_end_miller)
  {
    if (memory_allocation_error)
      throw Error(err::MEMORY,"FFT array allocation");
    setup_u_base();

    phaser_assert(xray_scatterers.size());
    cctbx::xray::sampled_model_density<> sampled_density(
        UC,
        xray_scatterers.const_ref(),
        scattering_type_registry,
        cfft.n(),
        cfft.n(),
        u_base,
        1.e-8, // wing_cutoff 1.e-8
        -100, // exp_table_one_over_step_size
        true); // force_complex, otherwise all fdp = 0 gives a real map1
    //u_extra = sampled_density.u_extra();

    cctbx::xray::sampled_model_density<double>::complex_map_type density_map =
        sampled_density.complex_map();
    phaser_assert(density_map.size());
    af::ref<std::complex<double>, af::c_grid<3> > density_map_ref(
        &*density_map.begin(), af::c_grid<3>(cfft.n()));
    // --- do the fft
    cfft.backward(density_map_ref);
    // --- extract the complex structure factors
    af::const_ref<std::complex<double>, af::c_grid_padded<3> > density_map_cref(
        reinterpret_cast<std::complex<double>*>(&*density_map.begin()),
        af::c_grid_padded<3>(cfft.n()));
    bool conjugate_flag(false); //overwrite, RefineOCC copy
    cctbx::maptbx::structure_factors::from_map<double> from_map(
        SG.group(),
        sampled_density.anomalous_flag(),
        selected_to_bin_end_miller.const_ref(),
        density_map_cref,
        conjugate_flag);
    af_cmplex Fcalc = from_map.data();
    sampled_density.eliminate_u_extra_and_normalize(
        selected_to_bin_end_miller.const_ref(),
        Fcalc.ref());
    return Fcalc;
  }

}}
