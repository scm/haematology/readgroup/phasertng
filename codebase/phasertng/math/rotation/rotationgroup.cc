#include <phasertng/math/rotation/rotationgroup.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/lnfactorial.h>
#include <limits>

#define PHASERTNG_RECURSIVE_FACTORIAL
#ifdef PHASERTNG_RECURSIVE_FACTORIAL
#include <phasertng/math/rfactorial.h>
#else
#include <phasertng/math/tfactorial.h>
#endif


namespace phasertng {
extern const table::lnfactorial& tbl_lnfac;

double djmn_direct(int& j, int& m, int& n, double& beta)
{
  /* Calculates d-Functions directly
     Written 25.01.2002
     Tested for Stability 25.01.2002
     NOTE TO SELF: Only need for m>=0 and n in range [-m,m]
     use djnm = (-1)^(m-n) djmn = dj-m-n to get to other elements
     THIS STILL NEEDS IMPLEMENTATION
  */

  int t(0);
  double num(0), denom(0), c(0), s(0), res(0);
  res = 0.0;
  for (t=0; t <= 2*j; t++) {
    if (j+m-t<0 || j-n-t<0 || n-m+t<0) continue;
    else {
      num = 0.5*(tbl_lnfac.get(j+m)+tbl_lnfac.get(j-m)+tbl_lnfac.get(j+n)+tbl_lnfac.get(j-n));
      denom = tbl_lnfac.get(j+m-t)+tbl_lnfac.get(j-n-t)+tbl_lnfac.get(t)+tbl_lnfac.get(t+n-m);
 //     num = 0.5*(log(ThrdSafeFact(j+m))+log(ThrdSafeFact(j-m))+log(ThrdSafeFact(j+n))+log(ThrdSafeFact(j-n)));
 //     denom = log(ThrdSafeFact(j+m-t))+log(ThrdSafeFact(j-n-t))+log(ThrdSafeFact(t))+log(ThrdSafeFact(t+n-m));
      c = pow(cos(beta/2),2*j+m-n-2*t);
      s = pow(sin(beta/2),2*t+n-m);
      res += pow(-1.0,t)*c*s*std::exp(num-denom);
    }
  }
  return res;
}

double djmn(int& j, int& m, int& n, double& beta)
{
  return djmn_direct(j, m, n, beta);
}

djmn_recursive_table_t djmn_recursive_table(int& j, double& beta)
{
  double cosbeta = cos(beta/2.0);
  double sinbeta = sin(beta/2.0);
  djmn_recursive_table_t table(scitbx::af::c_grid<2>(2*j+1,2*j+1));
  for (int i=0; i<2*j+1; i++)
      table(i,i) = 1;
  if (beta == 0)  return table;
  double djm_nm(0), djm_n(0), djm_np(0), csb(0);

  double dbl_fac(1.0e15);
  double BIGNUM(std::numeric_limits<double>::max()/dbl_fac);
  double logBIGNUM(log10(BIGNUM));

  for (int m = -j; m <= j; m++) {
    // Calculate d(j,m,j)(beta)
    djm_np = 0.0;
    double powcosbeta(0);
    if (fabs(cosbeta) > 0.01 || fabs(log10(fabs(cosbeta))*(j+m)) < logBIGNUM) powcosbeta = pow(cosbeta,j+m);
    double powsinbeta(0);
    if (fabs(sinbeta) > 0.01 || fabs(log10(fabs(sinbeta))*(j-m)) < logBIGNUM) powsinbeta = pow(sinbeta,j-m);
    csb = powsinbeta*powcosbeta;
    double tmp(std::exp(0.5*(tbl_lnfac.get(2*j)-tbl_lnfac.get(j+m)-tbl_lnfac.get(j-m))));
   // double tmp(sqrt(ThrdSafeFact(2*j))/sqrt(ThrdSafeFact(j+m)*ThrdSafeFact(j-m)));

    if (fabs(csb) > std::numeric_limits<double>::min()*dbl_fac) djm_n = tmp*csb;
    else if (fabs(csb) <= std::numeric_limits<double>::min()*dbl_fac) djm_n = 0;
    if (fabs(djm_n) < std::numeric_limits<double>::min()) djm_n = 0;

    table(j+m,j+j) = djm_n;
    table(j-m,j-j) = djm_n*pow(-1.,j-m);
    table(j+j,j+m) = djm_n*pow(-1.,j-m);
    table(j-j,j-m) = djm_n;

    for (int l = j-1; l >= scitbx::fn::absolute(m); l--) {
      djm_nm = -(sqrt(static_cast<double>((j+l+2)*(j-l-1)))*
                 djm_np+2*(m-(l+1)*cos(beta))*djm_n/sin(beta))
                /sqrt(static_cast<double>((j-l)*(j+l+1)));
      djm_np = djm_n;
      djm_n = djm_nm;
      table(j+m,j+l) = djm_n;
      table(j-m,j-l) = djm_n*pow(-1.,m-l);
      table(j+l,j+m) = djm_n*pow(-1.,m-l);
      table(j-l,j-m) = djm_n;
    }
  }
  return table;
}

double djmn_recursive(int& j, int m, int n, double& beta)
{
  /* Calculates d-Functions from the recurrence relation
     v/(j-n+1)(j+m)' d(j,m,n-1)=-v/(j+n+1)(j-n)' d(j,m,n+1)+2(m-n cos(b))/sin(b) d(j,m,n)
     Written 21.01.2002
     Corrected 28.01.2003
     Tested for stability (up to l=250) on 30.01.2002
     CURRENTLY UNSTABLE FOR LARGISH J

     NOTE TO SELF: Only need for m>=0 and n in range [-m,m]
     use djnm = (-1)^(m-n) djmn = dj-m-n to get to other elements
  */
  if(beta ==0){
    if (m == n) return 1;
    return 0;
  }
  double expftr=1;
  if (n<0) {
    expftr *= pow(-1.,n-m);
    n = -n;
    m = -m;
  }
  if (m>n) {
    expftr *= pow(-1.,m-n);
    int temp = m;
    m = n;
    n = temp;
  }
  if (m<-n) {
    int temp = m;
    m = -n;
    n = -temp;
  }

  double djm_nm(0), djm_n(0), djm_np(0), csb(0);
  djm_np = 0.0;
  csb = pow(sin(beta/2.0),j-m)*pow(cos(beta/2.0),j+m);
#ifdef PHASERTNG_RECURSIVE_FACTORIAL
  const int MAX_FACT = max_rfactorial(); //to avoid overflow
#else
  const int MAX_FACT = max_tfactorial(); //to avoid overflow
#endif
  if (j<=MAX_FACT)
#ifdef PHASERTNG_RECURSIVE_FACTORIAL
    djm_n = sqrt(rfactorial(2*j))/sqrt(rfactorial(j+m)*rfactorial(j-m))*csb;
#else
    djm_n = sqrt(tfactorial(2*j))/sqrt(tfactorial(j+m)*tfactorial(j-m))*csb;
#endif
  else {
    djm_n = exp(0.5*(tbl_lnfac.get(2*j)-tbl_lnfac.get(j+m)-tbl_lnfac.get(j-m)))*csb;
   // djm_n = sqrt(ThrdSafeFact(2*j))/sqrt(ThrdSafeFact(j+m)*ThrdSafeFact(j-m))*csb;
  }
  if (j==n) return expftr*djm_n;
  else {
    for (int l = j; l > n;l--) {
      djm_nm = -(sqrt(static_cast<double>((j+l+1)*(j-l)))*
                 djm_np+2*
                 (m-l*cos(beta))*
                 djm_n/sin(beta))
                /sqrt(static_cast<double>((j-l+1)*(j+l)));
      djm_np = djm_n;
      djm_n = djm_nm;
    }
    return expftr*djm_n;
  }
}

}
