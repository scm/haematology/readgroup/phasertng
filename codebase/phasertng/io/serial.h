//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_serial_class__
#define __phasertng_pod_serial_class__
#include <vector>
#include <utility>
#include <cmath>

typedef std::vector<double> sv_double;

namespace phasertng { namespace pod {

  inline double apply_drms_to_initial(double drms,double initial)
  {
    return std::sqrt(fn::pow2(initial) + drms);
  }

  inline double get_drms_from_initial_for_vrms(double vrms,double initial)
  {
    return fn::pow2(vrms) - fn::pow2(initial);
  }

  inline sv_double vrms_final(double drms,sv_double vrms_start)
  {
    sv_double vrms(vrms_start.size());
    for (int v = 0; v < vrms_start.size(); v++)
      vrms[v] = apply_drms_to_initial(drms,vrms_start[v]);
    return vrms;
  }

class serial
{
  private:
    int precision = 1000000;

  public: //members
    sv_double vrms_input = sv_double(0);
    sv_double vrms_start = sv_double(0); //'start' means the eca has been corrected
    sv_double vrms_upper = sv_double(0);
    sv_double vrms_lower = sv_double(0);
    std::pair<double,double>  drms_range = {0,0};
    double  drms_first = 0;

  serial() {
    vrms_input = sv_double(0);
    vrms_start = sv_double(0);
    vrms_upper = sv_double(0);
    vrms_lower = sv_double(0);
    drms_range.first = drms_range.second = 0;
    drms_first = 0;
  }

  public: //getter
    int size() const
    {
      assert(vrms_input.size() == vrms_start.size());
      assert(vrms_input.size() == vrms_upper.size());
      assert(vrms_input.size() == vrms_lower.size());
      return vrms_input.size();
    }
    const double& drms_upper() const { return drms_range.second; }
    const double& drms_lower() const { return drms_range.first; }

    void resize(int n)
    {
      vrms_input.resize(n,0);
      vrms_start.resize(n,0);
      vrms_upper.resize(n,0);
      vrms_lower.resize(n,0);
    }

    void set_single_default(double guess)
    {
      vrms_input.resize(1,guess);
      vrms_start.resize(1,guess);
      double tol = 1.0e-06; //ppm
      vrms_upper.resize(1,guess+tol); //so doesn't fail < test
      vrms_lower.resize(1,guess-tol); //so doesn't fail > test
    }

    bool add_serial(int i, std::array<double,4> input)
    {
      try {
        if (i >= size()) return false; //success
      } catch(...) { return false; }
      vrms_input[i] = input[0];
      vrms_start[i] = input[1];
      vrms_upper[i] = input[2];
      vrms_lower[i] = input[3];
      return true; //success
    }

    sv_double get_serial(int i) const
    {
      sv_double result(4,0);
      if (i >= size()) return result;
      result[0] = vrms_input[i];
      result[1] = vrms_start[i];
      result[2] = vrms_upper[i];
      result[3] = vrms_lower[i];
      return result;
    }

    sv_double get_drms() const
    {
      sv_double result(3);
      result[0] = drms_range.first;
      result[1] = drms_range.second;
      result[2] = drms_first;
      return result;
    }

    std::pair<double,int> get_lowest() const
    {
      int n = size();
      std::pair<double,int> result = { std::numeric_limits<double>::max(),0 };
      for (int i = 0; i < n; i++)
        if (vrms_start.at(i) < result.first)
          result = { vrms_start.at(i), i };
      return result;
    }

    double get_rms(int i)
    {
      assert(i < size());
      return vrms_input[i];
    }

    bool add_rms(int i, double input)
    {
      try {
        if (i >= size()) return false; //success
      } catch(...) { return false; }
      vrms_input[i] = input;
      vrms_start[i] = 0;
      vrms_upper[i] = 0;
      vrms_lower[i] = 0;
      return true; //success
    }

    bool is_serial() const
    {
      int n = size();
      for (int i = 0; i < n; i++)
      {
        if (vrms_start.at(i) > 0) return true;
        if (vrms_upper.at(i) > 0) return true;
        if (vrms_lower.at(i) > 0) return true;
      }
      return false;
    }

  public: //functions

    void
    setup_drms_first_and_limits()
    {
      //set drms_range.second and lower from VRMS start and RMS upper and lower limits
      //the only ones that need to have a precision set are
      //the vrms_start and drms_upper amd drms_lower because these are the only ones
      //output AND USED through the dag and the models pdb header
      int n = size();
      assert(n>0);
      //do not assert vrms_start is in range because the range of drms may not include 0
      //ie. the starting value may be outside range
      for (int v = 0; v < n; v++)
      {
        vrms_start[v] = std::round(precision * vrms_start[v]) / precision;
      }
      drms_range.first = -std::numeric_limits<double>::max();
      drms_range.second = std::numeric_limits<double>::max();
      for (int v = 0; v < n; v++)
      {
        double drms_lower = (vrms_lower[v]*vrms_lower[v]) - (vrms_start[v]*vrms_start[v]);
        double drms_upper = (vrms_upper[v]*vrms_upper[v]) - (vrms_start[v]*vrms_start[v]);
        drms_range.first = std::max(drms_lower,drms_range.first); //lower contract the range
        drms_range.second = std::min(drms_upper,drms_range.second); //upper contract the range
        drms_range.first =  std::ceil(precision * drms_range.first)/precision; //lower contract
        drms_range.second = std::floor(precision * drms_range.second)/precision; //upper contract
      }
      drms_first = 0;
      drms_first = std::min(drms_range.second,drms_first);
      drms_first = std::max(drms_range.first,drms_first);
    }

    void
    setup_drms_lower(double d)
    {
      int n = size();
      drms_range.first = std::max(drms_range.first,d);
      drms_first = std::max(drms_first,d);
      for (int v = 0; v < n; v++)
      {
        double& drms_lower = drms_range.first;
        double vrms_lower_sqr = drms_lower + (vrms_start[v]*vrms_start[v]);
        //double vrms_start_sqr = drms_lower + (vrms_lower[v]*vrms_lower[v]);
        if (std::sqrt(vrms_lower_sqr) > vrms_lower[v])
        {
          vrms_lower[v] = std::sqrt(vrms_lower_sqr);
          vrms_start[v] = std::max(vrms_lower[v],vrms_start[v]);
          vrms_start[v] = std::ceil(precision * vrms_start[v]) / precision;
        }
      }
    } //bring limits in

    const af_string
    logfile() const
    {
      af_string output;
      const int width(80);
      char buf[width];
      snprintf(buf,width,"Delta rms shift start=%5.2f range=[%5.2f->%5.2f]",
          drms_first,drms_range.first,drms_range.second);
      output.push_back(std::string(buf));
      for (int v = 0; v < size(); v++)
      {
        snprintf(buf,width,"Model #%-3d (input=%5.2f) start=%5.2f range=[%5.2f->%5.2f]",
            v+1,vrms_input[v],vrms_start[v],vrms_lower[v],vrms_upper[v]);
        output.push_back(std::string(buf));
      }
      return output;
    }

};

}} //phasertng

#endif
