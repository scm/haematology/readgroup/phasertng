//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Moving
  void Phasertng::runMOVE()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::LOGFILE,"No poses");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    double lowerB(0),upperB(0);
    Epsilon epsilon;
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    bool do_refine(false);
    //bool do_refine(!REFLECTIONS->MAP); //aka true
    //POSE
    if (do_refine)
    {
    bool store_halfR(false);
    bool store_fullR(true);
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    auto io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();

    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}
    lowerB = REFLECTIONS->bfactor_minimum();
    if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
      lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    upperB = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();
    }

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    if (do_refine)
    {
      for (auto id : used_modlid)
        load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
      DAGDB.apply_interpolation(interp::EIGHT); //for same value as the RBR
    }
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false); //for com shift
    }}

    bool io_move_to_origin = input.value__boolean.at(".phasertng.packing.move_to_origin.").value_or_default();
    bool io_move_to_compact  = input.value__boolean.at(".phasertng.packing.move_to_compact.").value_or_default();
    bool io_move_to_input  = input.value__boolean.at(".phasertng.packing.move_to_input.").value_or_default();
    bool io_move_tncs  = input.value__boolean.at(".phasertng.packing.move_when_tncs_present.").value_or_default();
    int  io_nprint = input.value__int_number.at(".phasertng.packing.maximum_printed.").value_or_default();
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    if (nmol > 1 and !io_move_tncs)
    {
      io_move_to_origin = io_move_to_compact = false; //don't move first solution to origin
    }

    logUnderLine(out::LOGFILE,"Optimize poses for proximity to origin and compactness");
    (io_move_tncs) ?
      logTab(out::VERBOSE,"Move to compact and origin in the presence of tNCS"):
      logTab(out::VERBOSE,"Don't move to compact and origin in the presence of tNCS");
    if (DAGDB.WORK->TNCS_MODELLED)
      logTab(out::VERBOSE,"tNCS in model");
    else if (DAGDB.WORK->TNCS_ORDER > 1)
      logTab(out::VERBOSE,"tNCS in data = " + itos(DAGDB.WORK->TNCS_ORDER));
    if (io_move_to_origin)
      logTab(out::LOGFILE,"First model will be moved closest to origin inside cell");
    if (io_move_to_compact)
      logTab(out::LOGFILE,"Models will be moved closest to centre of mass");
    DAGDB.restart();
    bool no_cell_translation(REFLECTIONS->MAP);
    if (no_cell_translation)
      logTab(out::LOGFILE,"Data is a map, unit cell translations will not be considered");
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      DAGDB.NODES[i].squash_orthogonal_perturbations();
    }

    std::vector<char> moved_to_input(DAGDB.size());
    if (io_move_to_input)
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Move to input position if possible"); //Park
      bool is_a_map(REFLECTIONS->MAP); //cast from tribool
      double dmin = std::max(4.0,REFLECTIONS->data_hires());
      std::string input_sg = REFLECTIONS->iSG.CCP4;
      int count = DAGDB.calculate_duplicate_packs(dmin,is_a_map,input_sg,USE_STRICTLY_NTHREADS,NTHREADS);
      logEllipsisShut(where);
      //the results are in float2 for historic reasons
      if (count > 0)
      {
        (count == 1) ?
          logTab(where,"There is 1 node matching input position"):
          logTab(where,"There are " + itos(count) + " nodes matching input positions");
        int nposes = DAGDB.NODES.number_of_poses();
        if (nposes == 1 and REFLECTIONS->SG.is_p1())
          logTab(where,"First component in P1, all nodes match input");
        else
        {
        for (int i = 0; i < DAGDB.NODES.size(); i++)
          if (DAGDB.NODES[i].generic_float2 >= 0) //flag, crazy
            logTab(where,"Matching input position node #" + itos(i+1) + " reference pose #" + dtoss(DAGDB.WORK->generic_float2+1,0));
        }
      }
    }
    else
    {
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        DAGDB.NODES[i].generic_float2 = -999; //flag, crazy
    }
    //store in reporting array
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      //below is crazy but historical, ListEditingPack uses float2 (from runPAK) set to match value
      //could set generic_flag internal etc but previously used in runPAK
      //we can use generic_int here because we are not using generic_int for clashes
      DAGDB.NODES[i].generic_flag = !(DAGDB.NODES[i].generic_float2 < 0);
      moved_to_input[i] = (DAGDB.NODES[i].generic_float2 < 0)?'N':'Y';
      DAGDB.NODES[i].generic_int = static_cast<int>(DAGDB.NODES[i].generic_float2);
      //also store the input llg values
      DAGDB.NODES[i].generic_float = DAGDB.NODES[i].LLG; //input LLG
    }

    bool mismatch1(false);
    af_string mismatchstr1;
    if (do_refine)
    {
      out::stream progress = out::LOGFILE;
      logBlank(progress);
      logUnderLine(progress,"Rigid Body Rescoring");
      logProgressBarStart(progress,"Rescoring poses",DAGDB.size());
      DAGDB.restart(); int i(0);
      while (!DAGDB.at_end())
      {
        i++;
        dag::Node* node = DAGDB.WORK;
        //rescore with the LLGI target (refinement uses Wilson)
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        //no selection for top peaks, recore them all
        node->reset_parameters(REFLECTIONS->get_ztotalscat());
        node->initKnownMR(REFLECTIONS->data_hires(),upperB,lowerB);
        pod::FastPoseType ecalcs_sigmaa_pose;
        float initLLG0 = node->LLG;
        DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
        if (DAGDB.negvar) logWarning("Negative variance ignored, contact developers");
        //checks that the packing to origin/other packing is working correctly
        if (dtoss(node->LLG,0) != dtoss(initLLG0,0))
        {
          mismatchstr1.push_back("Mismatch 1: #" + itos(i) + " " + dtoss(node->LLG,0) + " " +  dtoss(initLLG0,0));
          mismatch1 = true;
        }
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
    }
    //can now recycle float2
    for (int i = 0; i < DAGDB.NODES.size(); i++)
      DAGDB.NODES[i].generic_float2 = DAGDB.NODES[i].LLG; //fixed LLG

    //identifier for output coordinates, if required
    std::vector<dvect3> com_befor(DAGDB.size());
    std::vector<dvect3> com_after(DAGDB.size());
    std::vector<char> moved_to_origin(DAGDB.size());
    std::vector<char> moved_to_compact(DAGDB.size());
    int k(0);
    int n(1),N(DAGDB.size());
    out::stream progress = out::LOGFILE;
    logBlank(progress);
    logUnderLine(progress,"Component Moving");
    logProgressBarStart(progress,"Moving",DAGDB.size());
    DAGDB.restart();
    bool wf(false);
    while (!DAGDB.at_end())
    {
      std::string nN = nNtos(n++,N);
      out::stream where = (k < io_nprint) ? out::VERBOSE : out::TESTING;
      int NPOSE = DAGDB.WORK->POSE.size();
      Packing packing(input.value__flt_number.at(".phasertng.packing.distance_factor.").value_or_default());
      //get coordinates for trace
      packing.init_node(DAGDB.WORK);
      Coordinates tmp;
      packing.structure_for_refinement(tmp);
      com_befor[k] = packing.CENTRE_OF_MASS;
      if (Suite::Extra() and k < io_nprint)
      {
        where = out::TESTING;
        logUnderLine(where,"Moving"+nN);
        logTabArray(where,DAGDB.WORK->logPose("Before Origin/Compact"+nN,true,true));
      }
      //we are only going to record clashes
      //site symmetry for overlaps
      //move all close to origin
      //move all close to the closest to origin
      dvect3 origin(0,0,0);
      //bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,out::VERBOSE);
      if (REFLECTIONS->MAP)
      {
        origin = REFLECTIONS->MAPORI;
        io_move_to_origin = true;
        logTab(where,"Map Origin"+nN + ": " + dvtos(REFLECTIONS->MAPORI));
      }
      packing.move_to_origin(io_move_to_origin,origin,DAGDB.WORK->generic_int); //function must be called to set internal parameters
      moved_to_origin[k] = (packing.MOVED_TO_ORIGIN ? 'Y':'N');
      if (packing.MOVED_TO_ORIGIN and Suite::Extra() and k < io_nprint)
      {
        where = out::TESTING;
        logTabArray(where,DAGDB.WORK->logPose("Moved to Origin"+nN,true,true));
      }
      packing.multiplicity_and_exact_site();
      if (Suite::Extra() and k < io_nprint and wf)
      {
        for (int p = 0; p < NPOSE; p++)
        {
          std::string cc = ntos(p+1,NPOSE);
          DogTag pakid(DAGDB.PATHWAY.ULTIMATE,DAGDB.WORK->TRACKER.ULTIMATE);
          FileSystem PDBOUT(DataBase(),pakid.FileName("."+DAGDB.WORK->POSE[p].IDENTIFIER.tag() +"." + ntos(p+1,NPOSE) + ".origin.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Move-to-Origin",PDBOUT);
          PDBOUT.create_directories_for_file();
          if (WriteFiles()) packing.WritePosePdb(PDBOUT,p);
        }
      }
      packing.move_to_compact(io_move_to_compact);
      moved_to_compact[k] = (packing.MOVED_TO_COMPACT ? 'Y':'N');
      if (packing.MOVED_TO_COMPACT and Suite::Extra() and k < io_nprint and wf)
      {
        where = out::TESTING;
        logTabArray(where,DAGDB.WORK->logPose("Moved to Compact"+nN,true,true));
      }
      tmp.clear();
      packing.structure_for_refinement(tmp); //same each time
      com_after[k] = packing.CENTRE_OF_MASS;
      k++;
      if (!logProgressBarNext(progress)) break;
    }
    logProgressBarEnd(progress);

    bool mismatch2(false); //the second time
    af_string mismatchstr2;
    if (do_refine)
    {
      out::stream progress = out::LOGFILE;
      logBlank(progress);
      logUnderLine(progress,"Rigid Body Rescoring");
      logProgressBarStart(progress,"Rescoring poses",DAGDB.size());
      DAGDB.restart(); int i(0);
      while (!DAGDB.at_end())
      {
        i++;
        dag::Node* node = DAGDB.WORK;
        //rescore with the LLGI target (refinement uses Wilson)
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        //no selection for top peaks, recore them all
        node->reset_parameters(REFLECTIONS->get_ztotalscat());
        node->initKnownMR(REFLECTIONS->data_hires(),upperB,lowerB);
        pod::FastPoseType ecalcs_sigmaa_pose;
        float initLLG0 = node->LLG;
        DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
        if (DAGDB.negvar) logWarning("Negative variance ignored, contact developers");
        //checks that the packing to origin/other packing is working correctly
        if (dtoss(node->LLG,0) != dtoss(initLLG0,0))
        {
          mismatchstr1.push_back("Mismatch 2: #" + itos(i) + " " + dtoss(node->LLG,0) + " " +  dtoss(initLLG0,0));
          mismatch2 = true;
        }
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
    }

    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      auto& node = DAGDB.WORK;
      if (node->SPECIAL_POSITION == 'Y')
      {
        logWarning("Model(s) with point group symmetry placed on special position(s)");
        break; //once
      }
    }

    for (int i = 0; i < DAGDB.NODES.size(); i++)
      DAGDB.NODES[i].ANNOTATION += " SYM=" + chtos(DAGDB.NODES[i].SPECIAL_POSITION) + std::string((moved_to_input[i]=='Y') ? "*":"");

    {{
    int w = itow(io_nprint);
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP); //REFLECTIONS is nullptr
    out::stream where = out::SUMMARY;
    logTableTop(where,"Moving Summary");
    int nposes = DAGDB.NODES.number_of_poses();
    if (nposes)
    {
      logTab(where,"Background Poses: " + itos(nposes-nmol));
      logTab(where,"Poses: " + itos(nposes));
    }
    logTab(where,"O/C/I Moved-to-Origin/Moved-to-Compact/Moved-to-Input");
    logTab(where,"SYM   Symmetry-Special-Position (*matches input pose)");
    do_refine ?
      logTab(where,snprintftos( "%-*s %5s %2s %11s %11s %11s %12s  %-13s",
             w,"#","O/C/I","SYM","LLG-input","LLG-fixed","LLG-moved","COM-shift(A)","Space Group")):
      logTab(where,snprintftos( "%-*s %5s %2s %11s %12s  %-13s",
             w,"#","O/C/I","SYM","LLG-input","COM-shift(A)","Space Group"));
    DAGDB.restart();
    int i(1);
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      do_refine ?
        logTab(where,snprintftos("%-*d %c/%c/%c %c%c  %11.*f %11.*f %11.*f %12.2f  %-13s",
            w,i,
            moved_to_origin[i-1],moved_to_compact[i-1],moved_to_input[i-1],
            node->SPECIAL_POSITION,(moved_to_input[i-1]=='Y')?'*':' ',
            q,node->generic_float, q,node->generic_float2, q,node->LLG, //only if not a map, no MAPLLG
            dvect3(com_after[i-1]-com_befor[i-1]).length(),
            node->SG->sstr().c_str())):
        logTab(where,snprintftos("%-*d %c/%c/%c %c%c  %11.*f %12.2f  %-13s",
            w,i,
            moved_to_origin[i-1],moved_to_compact[i-1],moved_to_input[i-1],
            node->SPECIAL_POSITION,(moved_to_input[i-1]=='Y')?'*':' ',
            q,REFLECTIONS->MAP ? node->MAPLLG : node->LLG,
            dvect3(com_after[i-1]-com_befor[i-1]).length(),
            node->SG->sstr().c_str()));
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logBlank(where);
    logTableEnd(where);
    }}

    if (false)
    {
      for (auto item : mismatchstr1)
        logAdvisory(item);
      if (mismatch1)
      throw Error(err::DEVELOPER,"Mismatch in LLG calculations between modes");
    }
    for (auto item : mismatchstr2)
      logAdvisory(item);
    if (mismatch2)
    throw Error(err::DEVELOPER,"Mismatch in LLG calculations between modes");
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}  //end namespace phasertng
