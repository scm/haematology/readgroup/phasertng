//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/FastRotationFunction.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/src/Clmn.h>
#include <phasertng/math/rotation/rotationgroup.h>
#include <phasertng/math/four_point_interp.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/copy.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/math/cartesian_as_spherical.h>
#include <phasertng/math/spherical_as_cartesian.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/main/jiffy.h>
#include <future>

namespace phasertng {

  // Phaser's definition of the Euler angles assumes R_z(gamma)R_y(beta)R_z(alpha)
  // Sakurai uses R_z(alpha)R_y(beta)R_z(gamma)

  FastRotationFunction::FastRotationFunction(
      const_ReflectionsPtr REFLECTIONS_,
      bool cluster_in_p1) :
    AnglesStored(),REFLECTIONS(std::move(REFLECTIONS_))
  {
    clustering_sg = REFLECTIONS->SG;
    if (cluster_in_p1)
      clustering_sg = SpaceGroup("P 1");
    int NSYMP = clustering_sg.group().order_p();
    dmat33 fracmat = REFLECTIONS->UC.fractionalization_matrix();
    dmat33 orthmat = REFLECTIONS->UC.orthogonalization_matrix();
    orth_rotsymtr_frac.resize(NSYMP); //class
    for (unsigned isym = 0; isym < NSYMP; isym++)
      orth_rotsymtr_frac[isym] = orthmat*clustering_sg.rotsym[isym].transpose()*fracmat;
  }

// A bug in GCC 4.2 compels us to introduce this helper function,
// the contents of which all used to reside in get_FRF()
// AJM phasertng - carried over from phaser, probably unnecessary now but harmless
// and kept for code tracking
  scitbx::fftpack::real_to_complex_3d<double>
  FastRotationFunction::DoRfftStuff(
      const elmn_array_t& CLM1M2,
      const double bSAMPLING,
      const int LMAX,
      cctbx::maptbx::structure_factors::to_map<double>& to_map
    )
  {
    double betaDEG = static_cast<double>(bSAMPLING);
    double betaRAD =  scitbx::deg_as_rad(betaDEG);
    double cosbeta = std::cos(betaRAD/2.);
    af_millnx  pseudo_miller;
    af_cmplex  pseudo_sf;

   // Calculate the rotated Smm'(beta) from the Clmm' and Dlmm'(beta) by summing over the l values
   // and storing it in the FFT array.

    cmplex rot(0);
    int l(0);
    double beta = 2*std::acos(cosbeta);
    cmplex zero(0,0);

#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    int max_index = static_cast<int>(CLM1M2.size()); //max_index is lmax/2
#else
    pseudo_miller.reserve(CLM1M2.accessor().size_1d());
    pseudo_sf.reserve(CLM1M2.accessor().size_1d());
    //scitbx::af::versa<size_t,scitbx::af::c_grid<2>> index(scitbx::af::c_grid<2>(LMAX+2,LMAX+2));
    //af_double iindex(LMAX);
    int max_index = LMAX/2;
    for (int l_index = 0; l_index < max_index; l_index++)
    {
      int l=2*l_index+2;
      int twolplus1 = 2*l+1;
     // iindex[l_index] = twolplus1;
      for (int m1 = 0; m1 < twolplus1; m1++)
      {
      //  index(l_index,m1) = twolplus1;
      }
    }
    cctbx::af::tiny<std::size_t, 3> dimensions = CLM1M2.accessor();
#endif
    PHASER_ASSERT(max_index > 2);

    for (int l_index = 0; l_index < max_index; l_index++)
    {
      l = 2*l_index+2;
      djmn_recursive_table_t table = djmn_recursive_table(l,beta);
      int twolplus1 = 2*l+1;
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
      int clmn_l_index_size = CLM1M2[l_index].size();
#else
      int clmn_l_index_size = twolplus1; //iindex[l_index];
#endif
      for (int m1_index = 0; m1_index < twolplus1; m1_index++)
      {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
        int clmn_l_index__m1_index_size = CLM1M2[l_index][m1_index].size();
#else
        int clmn_l_index__m1_index_size = twolplus1; //index(l_index,m1_index);
#endif
        for (int m2_index = 0; m2_index < twolplus1; m2_index++)
        {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
          const cmplex& clmn_l_index__m1_index_m2_index = CLM1M2[l_index][m1_index][m2_index];
#else
          const cmplex& clmn_l_index__m1_index_m2_index = CLM1M2(l_index,m1_index,m2_index);
#endif
          if ((l_index < max_index) and
              (m1_index < clmn_l_index_size) and
              (m2_index < clmn_l_index__m1_index_size) and
              clmn_l_index__m1_index_m2_index != zero)
          {
            int m1l = m1_index-l;
            int m2l = m2_index-l;
            rot = clmn_l_index__m1_index_m2_index*table(m1_index,m2_index);
            // This MUST be exactly one asymmetric unit!
            // phaser r621 "Added the 000 reflection" mll>0->m1l>=0
            if (m2l > 0 or (m2l == 0 and m1l >= 0))
            {
              pseudo_miller.push_back(miller::index<>(0,m1l,m2l));
              pseudo_sf.push_back(rot);
            }
          }
        }
      }
    }
    phaser_assert(pseudo_sf.size());
    cctbx::sgtbx::space_group cctbxSG("P 1"); // Hall symbol
    scitbx::fftpack::real_to_complex_3d<double> rfft(gridding);

    cctbx::af::flex_grid<> map_grid(scitbx::af::adapt(rfft.n_complex()));
    bool conjugate_flag = true;
    bool anomalous_flag = false;
    to_map = cctbx::maptbx::structure_factors::to_map<double>(
      cctbxSG,
      anomalous_flag,
      pseudo_miller.const_ref(),
      pseudo_sf.const_ref(),
      rfft.n_real(),
      map_grid,
      conjugate_flag);

    rfft.backward(scitbx::af::ref<cmplex,
         scitbx::af::c_grid<3> >(
             to_map.complex_map().begin(),
             scitbx::af::c_grid<3>(to_map.complex_map().accessor().all())
         )
      );
    return rfft;
  }

  af_string
  FastRotationFunction::calculate_fourier_transforms(
      const elmn_array_t& DATAELMN,
      const elmn_array_t& SEARCHELMN,
      const int LMAX,
      const double SAMPLING,
      const int LOWORDER,
      const bool calculate_stats,
      const bool store_full_map
    )
  {
    af_string output;
    phaser_assert(DEF_CLMN_LMIN < LMAX);
    phaser_assert(DATAELMN.size());
    phaser_assert(SEARCHELMN.size());
    Clmn CLMN;
    CLMN.clmnx(DATAELMN,SEARCHELMN,DEF_CLMN_LMIN,LMAX);
    phaser_assert(SAMPLING);
    PHASER_ASSERT(180%LOWORDER == 0);
    double BetaSectionMax = 180./LOWORDER;
    BetaSectionMax = std::min(180.,BetaSectionMax); //paranoia
    output.push_back("Highest beta angle = " + dtos(BetaSectionMax));
    //the second time we use round because it should be almost exact anyway, only numerical instability 4ll7 helix
    int bmax = static_cast<int>(std::round(BetaSectionMax/SAMPLING)); //stored for printing
    output.push_back("Highest beta angle by bmax = " + dtos(SAMPLING*bmax));
    //note that the sampling should be defined so that BetaSectionMax us exactly divisible by it (EntryHelper)
    //if below fails it is a numerical stability problem with the definition of the sampling/reverse calculation
    PHASER_ASSERT(std::fabs(SAMPLING*bmax-BetaSectionMax)< DEF_PPT);
    bmax++; // this is the size, because our for loops are < bmax and we WANT the edge, eg beta=180

    int lmax = LMAX; //2*static_cast<int>(CLMN.clm1m2.size())+2;
    int min_grid = 2* std::max(bmax, lmax); //number of steps
    int max_prime = 5;
    int mandatory_factor = 1;
    int amax = scitbx::fftpack::adjust_gridding(
                   min_grid,
                   max_prime,
                   mandatory_factor
                );
    int cmax = amax;
    gridding = ivect3(1,amax,cmax);
    if (store_full_map)
    {
      ivect3 mapgrid(amax,bmax,cmax);
      frfmap = af::versa<double, af::c_grid<3>>(af::c_grid<3>(mapgrid));
    }

    // claim necessary memory in advance to avoid vector's greedy memory grabber
    beta_section.resize(bmax);
    int b(0);
    for (auto& item : beta_section)
    {
      item.allocate_memory(b,SAMPLING);
      item.fill_memory(b,SAMPLING);
      b++;
    }
    //the b angles have different number of associated alpha,gamma samplings, which means that the
    //parallel for loops do not have equal numbers of angles on each thread. Randomize to even it out.
    const int nthreads = (bmax <= NTHREADS) ? bmax : NTHREADS;

    std::vector<sv_int> thread_b(nthreads);

    int number_of_beta(0);
    for (auto& item : beta_section)
      number_of_beta += item.count_sites();

    if (bmax <= NTHREADS) // extremely unlikely in practice
    {
      for (int n = 0; n < nthreads; n++)
      {
        thread_b[n].push_back(n);
      }
    }
    else // the expected case
    {
      //integer division, result will always be >= 1 because bmax > nthreads
      const int grain = number_of_beta / nthreads;
      //thread balancing code below, since clusters are different sizes
      std::vector<std::pair<int,int>> count(bmax);
      sv_bool used(bmax,false);
      int i(0);
      for (auto& item : beta_section)
      {
        count[i] = { item.count_sites(),i};
        i++;
      }
      auto cmp = []( const std::pair<int,int> &a, const std::pair<int,int> &b )
      { if (a.first == b.first) return a.second < b.second;
        return a.first >  //> for reverse sort
               b.first; };
      if (NTHREADS > 1) //keep the order otherwise
        std::sort(count.begin(),count.end(),cmp); //sorted on size large to small
      //this is so that the smallest ones are at the end and can be safely added to complete final
      //thread (because used=false) without overloading the last thread
      sv_int cumulative(nthreads);
      for (int n = 0; n < nthreads; n++)
      {
        for (int j = 0; j < bmax; j++)
        {
          if (!used[j])
          {
            int i = count[j].second;
            int size = count[j].first;
            if ( (size >= grain) or //these are overflows, fill the entire thread
                ((size  + cumulative[n]) < grain) or //fill up until limit
                 (n == nthreads-1)) //fill up final thread with stragglers, regardless
            {
              cumulative[n] += size;
              thread_b[n].push_back(i);
              used[j] = true;
            }
          }
        }
      }
      for (int i = 0; i < bmax; i++)
      {
        PHASER_ASSERT(used[i]);
        std::string txt("#" + itos(i) + " beta=" + itos(count[i].second) + " #angles=" + itos(count[i].first));
        output.push_back(txt);
      }
      int total(0);
      output.push_back("Load balancing");
      for (int n = 0; n < nthreads; n++)
      {
        output.push_back("Thread #" + itos(n) + " cumulative load " + itos(cumulative[n]));
        total += cumulative[n];
      }
      PHASER_ASSERT(total == number_of_beta);
    }

    {{
    // launch threads for all but the first batch of reflections
    std::vector< std::future< void > > results;
    std::vector< std::packaged_task< void(
            const sv_int,
            const elmn_array_t&,
            const double,
            const int,
            const bool,
            const bool
            )> > packaged_tasks;
    std::vector<std::thread> threads;
    int nthreads_1 = nthreads-1;
    results.reserve(nthreads_1);
    if (USE_STRICTLY_NTHREADS)
    {
      packaged_tasks.reserve(nthreads_1);
      threads.reserve(nthreads_1);
    }
    for (int t = 0; t < nthreads_1; t++)
    {
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.emplace_back( std::bind( &FastRotationFunction::parallel_beta_section, this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3,
            std::placeholders::_4,
            std::placeholders::_5,
            std::placeholders::_6
            ));
        results.push_back(packaged_tasks[t].get_future());
        // create the thread (and set it off running)
        threads.emplace_back(std::move(packaged_tasks[t]),
            thread_b[t],
            CLMN.clm1m2,
            SAMPLING,
            LMAX,
            calculate_stats,
            store_full_map
            );
      }
      else
      {
        results.push_back( std::async( std::launch::async, &FastRotationFunction::parallel_beta_section, this,
            thread_b[t],
            CLMN.clm1m2,
            SAMPLING,
            LMAX,
            calculate_stats,
            store_full_map
            ));
      }
    }

    parallel_beta_section(
            thread_b[nthreads_1],
            CLMN.clm1m2,
            SAMPLING,
            LMAX,
            calculate_stats,
            store_full_map
            );

    for (int t = 0; t < nthreads_1; t++)
    {
      if (USE_STRICTLY_NTHREADS) threads[t].join(); //terminate the threads
    }
    }}

    int count(0);
    for (auto& item : beta_section)
      count += item.rotation_function_data.size();
    output.push_back(itos(count)+" grid points sampled.");
    //Number of grid points sampled is dependent on grid sampling precision
    // to many significant figures, can't compare with phaser
    output.push_back("Evaluating for beta between 0 and " + dtos((bmax-1)*SAMPLING) + " degrees (inclusive)");
    output.push_back("Angle resolution requires "+itos(bmax)+" steps");
    return output;
  }

  void
  FastRotationFunction::parallel_beta_section(
      const sv_int thread_b,
      const elmn_array_t& CLMN,
      const double SAMPLING,
      const int LMAX,
      const bool calculate_stats,
      const bool store_full_map
    )
  {
    for (int b = 0; b < thread_b.size(); b++)
      calculate_beta_section(thread_b[b],CLMN,SAMPLING,LMAX,calculate_stats,store_full_map);
  }

  void
  FastRotationFunction::calculate_beta_section(
      const int b,
      const elmn_array_t& CLMN,
      const double SAMPLING,
      const int LMAX,
      const bool calculate_stats,
      const bool store_full_map
    )
  {
    AnglesStored& this_beta_section = *std::next(beta_section.begin(),b); //store offset
    double betaDEG = static_cast<double>(SAMPLING*b);

    cctbx::maptbx::structure_factors::to_map<double> to_map;
    scitbx::fftpack::real_to_complex_3d<double> rfft = DoRfftStuff(CLMN,betaDEG,LMAX,to_map);
    //rfft.n_real() == gridding
    //rfft.m_real() padded
    scitbx::af::const_ref< double, scitbx::af::c_grid_padded_periodic<3> >
        beta_section_map(
            reinterpret_cast<double*>(to_map.complex_map().begin()),
            scitbx::af::c_grid_padded_periodic<3>(
            rfft.m_real(),
            rfft.n_real())
        );

    if (store_full_map)
    {
      PHASER_ASSERT(gridding[0] == 1);
      for (int y = 0; y < gridding[1]; y++)
      for (int z = 0; z < gridding[2]; z++)
      {
        frfmap(y,b,z) = beta_section_map(1,y,z);
      }
    }

    this_beta_section.beta_deg = betaDEG;
    // --- work out mean and sigma for total search
    if (calculate_stats)
    {
      cctbx::maptbx::statistics<double> stats(
          af::const_ref<double, af::flex_grid<> >(
              beta_section_map.begin(),
              beta_section_map.accessor().as_flex_grid()));
      this_beta_section.f_mean = stats.mean();
      this_beta_section.f_sigma = stats.sigma();
      this_beta_section.f_top = stats.max();
      this_beta_section.z_top = (stats.max()-stats.mean())/stats.sigma();
    }

    four_point_interpolation<double> interpolate(beta_section_map);
    for (int b_ang = 0; b_ang < this_beta_section.rotation_function_data.size(); b_ang++)
    {
      Site& item = this_beta_section.rotation_function_data[b_ang]; //store offset
      double alpha = item.grid[0];
      double gamma = item.grid[2];
      double interp = interpolate(alpha, gamma);
      item.value = interp;
      dvect3 euler = { -360*alpha,betaDEG,-360*gamma };
      item.grid = euler;
    }

    bool add_highest_to_list(true);
    //add the highest value on the beta section to the list regardless of the values
    //in the pseudo-hexagonal grid sampling
    //this may be higher than the interpolated value above
    //this addition is not in phaser
    if (add_highest_to_list)
    {
      int amax = gridding[1];
      int cmax = gridding[2];
      Site maxsite(std::numeric_limits<double>::lowest());
      for (int a = 0; a < amax; a++)
      for (int c = 0; c < cmax; c++)
      {
        const double& v = beta_section_map(0,a,c);
        if ( v > maxsite.value )
        {
          maxsite = Site( { -360*double(a)/double(amax), betaDEG, -360*double(c)/double(cmax) }, v);
        }
      }
      this_beta_section.rotation_function_data.push_back(maxsite);
    }

    bool add_all_to_list(false);
    //add the highest value on the beta section to the list regardless of the values
    //in the pseudo-hexagonal grid sampling
    //this may be higher than the interpolated value above
    //this addition is not in phaser
    if (add_all_to_list)
    {
      int amax = gridding[1];
      int cmax = gridding[2];
      Site maxsite(std::numeric_limits<double>::lowest());
      for (int a = 0; a < amax; a++)
      for (int c = 0; c < cmax; c++)
      {
        const double& v = beta_section_map(0,a,c);
        Site next( { -360*double(a)/double(amax), betaDEG, -360*double(c)/double(cmax) }, v);
        this_beta_section.rotation_function_data.push_back(next);
      }
    }
  }

  int FastRotationFunction::total_number()
  {
    int numb(0);
    for (auto& item : beta_section)
      numb += item.count_sites();
    return numb;
  }

  void FastRotationFunction::merge_beta_sections(bool clear_data)
  {
    //AJM TO DO merge a partial sort of beta sections
    //transfer data to the base (combined) rotation_function_data and exit
    rotation_function_data.clear();
    for (auto& item : beta_section)
    {
      //base class contains rotation_function_data
      rotation_function_data.insert(
          rotation_function_data.end(),
          item.rotation_function_data.begin(),
          item.rotation_function_data.end());
      if (clear_data)
        item.rotation_function_data.clear();
      //item.rotation_function_data.shrink_to_fit(); //c++11 standard function
    }
    if (clear_data)
      beta_section.clear();
   // beta_section.shrink_to_fit();
  }

  bool FastRotationFunction::statistics()
  {
    phaser_assert(rotation_function_data.size());
    std::tie(f_mean,f_sigma) = mean_sigma(); //calculate mean and sigma for merged beta sections
    PHASER_ASSERT(f_sigma>0);
    f_top = top();
    z_top = (f_top-f_mean)/f_sigma;
    bool newmax(false);
    if (false)
    { //this means that the max is always the top, assumes frf on different scales
      f_max = f_top;
      z_max = z_top;
    }
    else
    { //this makes it same as translation function, with fmax changing
      if (f_max < f_top)
      {
        newmax = true;
        f_max = f_top;
        z_max = z_top;
      }
    }
    //reset the sigmas with the merged sigma values
    for (auto& this_beta_section : beta_section)
    {
      this_beta_section.z_top = (this_beta_section.f_top-f_mean)/f_sigma;
    }
    return newmax;
  }

  void FastRotationFunction::move_to_rotational_asu()
  {
    // Returns a list of unique angles for the space group
    for (auto& item : rotation_function_data)
    {
      //zyz_matrix defined in degrees
      dmat33 rmat = zyz_matrix(item.grid);
      item.grid = regularize_angle( item.grid );
      for (int isym = 1; isym < clustering_sg.NSYMP; isym++)
      {
        dmat33 rotmat = orth_rotsymtr_frac[isym]*rmat;
        dvect3 sym_euler = scitbx::math::euler_angles::zyz_angles(rotmat);
        // If beta is smaller, save this orientation.
        sym_euler = regularize_angle( sym_euler );
        if (sym_euler[1] < item.grid[1])
        {
          item.grid = sym_euler;
        }
        //New beta is the same
        //Make decision on alpha
        // If new alpha is smaller, save this orientation.
        else if (sym_euler[0] < item.grid[0])
        {
          item.grid = sym_euler;
        }
        //New beta and alpha are the same
        //Make decision on gamma
        //If new gamma is smaller, save this orientation.
        else if (sym_euler[2] < item.grid[2])
        {
          item.grid = sym_euler;
        }
      }
    }
  }

  void
  FastRotationFunction::clustering_off()
  {
    int i(0);
    for (auto& item : rotation_function_data)
      item.clear_topsite(i++);
  }

  void
  FastRotationFunction::cluster(const double CLUSTER_ANG_,int cluster_back)
  {
    int max_cluster_back = rotation_function_data.size()-1;
    cluster_back = std::min(cluster_back,max_cluster_back);
    PHASER_ASSERT(CLUSTER_ANG_>0);
    CLUSTER_ANG = CLUSTER_ANG_;
    for (auto& item : rotation_function_data)
      item.set_topsite_false();

    for (int rot = 1; rot < rotation_function_data.size(); rot++)
      PHASER_ASSERT(rotation_function_data[rot-1].value >= rotation_function_data[rot].value);

    for (int rot = 0; rot < rotation_function_data.size(); rot++)
    {
      //reference_rmat = item.getMatrix();
      Site& item = rotation_function_data[rot]; //offset ref
      dmat33 reference_rmat = zyz_matrix(item.grid);
      //Look backwards as you are most likely to find a
      //site to cluster with close by in the list,not
      //at the top of the list each time (list in order)
      int rot_prev = cluster_combine(reference_rmat,rot,cluster_back);
      if (rot_prev < 0)
      {
        item.set_topsite_true(); // no hits found,this site must be unique
        item.ifTopNumInGroup = 1; //first member of its own cluster
        item.clusterNum = rot; //unique,so the cluster angle so far is the angle in the FRF
      }
      else
      {
        Site& item_prev = rotation_function_data[rot_prev]; //offset ref
        item.clusterNum = item_prev.clusterNum;
        rotation_function_data[item.clusterNum].ifTopNumInGroup++;
      }
    }
  }

  int
  FastRotationFunction::cluster_combine(dmat33 matRef,int nprev,int cluster_back)
  {
    int rot_prev(-999); //result
    int minimum_nprev_for_parallel(2000); //must be 2000 to make it worthwhile AJM TODO run tests
    int dynamic_nthreads = std::ceil(nprev/static_cast<double>(minimum_nprev_for_parallel));
    //when nprev hits 2001 it goes down to 1000 on each thread, not 2000 and 1
    cluster_back = std::min(nprev,cluster_back);
    dynamic_nthreads = std::min(NTHREADS,dynamic_nthreads);
    if (NTHREADS == 1)
    {
      int beg = std::max(0,nprev-minimum_nprev_for_parallel);
      int end = nprev-1;
      rot_prev = cluster_parallel(matRef,beg,end);
    }
    else if (nprev <= minimum_nprev_for_parallel)// because the granularity is too small
    {
      int beg = 0;
      int end = nprev-1;
      rot_prev = cluster_parallel(matRef,beg,end);
    }
    else
    {
      std::vector< std::future< int > > results;
      std::vector< std::packaged_task< int(dmat33,int,int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = dynamic_nthreads-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      int grain = (cluster_back/dynamic_nthreads)+1; //granularity of threading, +1 makes the last thread the shortest
      int start = (nprev-cluster_back); //end of thread
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = std::max(0,start+t*grain); //start of thread reflection
        int end = std::min(start+(t+1)*grain,nprev)-1; //end of thread
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&FastRotationFunction::cluster_parallel, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              matRef,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &FastRotationFunction::cluster_parallel, this,
              matRef,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
       int beg = start+nthreads_1*grain; //start of thread reflection
       int end = std::min(start+(nthreads_1+1)*grain,nprev)-1; //end of thread
       rot_prev = cluster_parallel(
              matRef,
              beg,
              end);
       //rot_prev will remain -999 if nothing was found
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        if (result >= 0)
          rot_prev = std::min(result,rot_prev); //highest in list, as per forwards search
      }
    }
    return rot_prev;
  }

  int
  FastRotationFunction::cluster_parallel(dmat33 matRef,int beg,int end)
  {
    for (int rot_prev = beg; rot_prev <= end ; rot_prev++)
    {
      Site& item_prev = rotation_function_data[rot_prev]; //offset ref
      dmat33 prev_rotmat = zyz_matrix(item_prev.grid);
      for (int isym = 0; isym < clustering_sg.NSYMP; isym++)
      {
        dmat33 sym_prev_rotmat = prev_rotmat;
        if (isym > 0) //identity op if zero
        {
          sym_prev_rotmat = orth_rotsymtr_frac[isym]*prev_rotmat;
        }
        if (matrix_delta_deg(sym_prev_rotmat,matRef) < CLUSTER_ANG)
        {
          return rot_prev;
        }
      }
    }
    return -999;
  }

  double
  FastRotationFunction::apply_partial_sort_and_percent_fss(double io_percent,double f_novel_max,bool do_sort)
  {
    PHASER_ASSERT(f_sigma != 0); //stats not calculated
    //rotations not sorted yet
    int num(0); //counter for erase
    double cutoff = (io_percent/100.)*(f_novel_max-f_mean)+f_mean;
    for (int rot = 0; rot < rotation_function_data.size(); rot++)
    {
      auto& item = rotation_function_data[rot];
      //f_max is the max over all rotation functions, not just this one
      if (item.value >= cutoff) //sort this one to the front
        rotation_function_data[num++] = item;
    }
    rotation_function_data.erase(
        rotation_function_data.begin()+num,
        rotation_function_data.end());
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (do_sort)
      std::sort( //now sort properly
        rotation_function_data.begin(),
        rotation_function_data.end(),cmp);
    return cutoff;
  }

  void
  FastRotationFunction::apply_partial_sort_for_print(const int io_nprint)
  {
    if (io_nprint == 0) return; //cutoff not set
    if (!rotation_function_data.size()) return;
    // lambda function for explict sort on parameter
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (io_nprint < rotation_function_data.size())
    {
      std::partial_sort(
        rotation_function_data.begin(),
        rotation_function_data.begin()+io_nprint,
        rotation_function_data.end(),
        cmp);
    }
    else
    {
      std::sort(
        rotation_function_data.begin(),
        rotation_function_data.end(), cmp);
    }
  }

  void
  FastRotationFunction::apply_partial_sort_and_maximum_stored_llg(const int io_maxstored)
  {
    // lambda function for explict sort on parameter
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (!io_maxstored or io_maxstored >= rotation_function_data.size())
    {
      std::sort(
        rotation_function_data.begin(),
        rotation_function_data.end(), cmp);
    }
    else if (io_maxstored < rotation_function_data.size())
    {
      std::partial_sort(
        rotation_function_data.begin(),
        rotation_function_data.begin()+io_maxstored,
        rotation_function_data.end(),
        cmp);
      rotation_function_data.erase(
        rotation_function_data.begin()+io_maxstored,
        rotation_function_data.end());
    }
  }

  void
  FastRotationFunction::apply_nth_element_maximum_stored_llg(const int io_maxstored)
  {
    if (io_maxstored == 0) return; //cutoff not set
    if (!rotation_function_data.size()) return;
    // lambda function for explict sort on parameter
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (io_maxstored < rotation_function_data.size())
    {
      std::nth_element(
        rotation_function_data.begin(),
        rotation_function_data.begin()+io_maxstored,
        rotation_function_data.end(),
        cmp);
      rotation_function_data.erase(
        rotation_function_data.begin()+io_maxstored,
        rotation_function_data.end());
    }
  }

  af_string
  FastRotationFunction::logTableHeader(int nprint) const
  {
    af_string output;
    int z = rotation_function_data.size();
    output.push_back("There " + std::string(z==1?"is ":"are ") + itos(z) + " angle" + std::string(z==1?"":"s"));
/*
    bool clustered = z ? rotation_function_data[0].clustered() : false;
    (clustered) ?
      output.push_back("Peaks are clustered"):
      output.push_back("Peaks are not clustered");
    if (clustered)
    {
      int numTop(0); //have these been clustered? if not, they will all be isTop
      for (auto& item : rotation_function_data)
        if (item.is_topsite()) { numTop++; } //at least one is not a topsite
      output.push_back("There " + std::string(numTop==1?"is ":"are ") + itos(numTop) + " cluster" + std::string(numTop==1?"":"s"));
    }
*/
    output.push_back("Top " + itos(nprint) + " reported");
    return output;
  }

  af_string
  FastRotationFunction::logFastTable(
      const int nprint, //maximum number to print
      const bool AllSites,
      dmat33 PR
    ) //regardless of whether it is a top site in cluster or not
  {
    af_string output  = logTableHeader(nprint);
    bool clustered = rotation_function_data.size() ? rotation_function_data[0].clustered() : false;
    int w = itow(nprint);
    int ww = itow(rotation_function_data.size(),5);
    if (!rotation_function_data.size()) return output;
    output.push_back("--------");
    output.push_back("RFZ     fast search z-score");
    output.push_back("FSS     fast search score");
    output.push_back("FSS%    fast search score percent of top peak wrt mean");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(rotation_function_data.size());
    int q = std::max(4. - std::floor(std::log10(std::max(1.,f_max))),1.);
    output.push_back(snprintftos(
        "%-*s %33s %6s %10s %5s %*s %5s",
        w,"#","Polar wrt original [Axis]:[Angle]","RFZ","FSS","FSS%",ww,"#Peak","#Grp"));
    int i(1);
    for (auto& item : rotation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered selection below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      dmat33 rotmat;
      std::tie(std::ignore,rotmat) = //not dag::print_euler_wrt_in_from_mt(item.grid,PR);
           turn_best_sym(i,PR); //which finds the best amongst the symmetry copies
      if (AllSites or item.is_topsite())
      {
        double f_top = top(); //this value is independent of what came before
       // double f_top = f_max; //this value would depend on what has come before
       // Not possible to print the percent reported to annotation, since overall f_max not known here
        double pscore = 100.*(item.value-f_mean)/(f_top-f_mean);
        double zscore = (item.value-f_mean)/f_sigma;
        PHASER_ASSERT(pscore <= (100. + DEF_PPB));
        output.push_back(snprintftos(
        "%-*i %s %6.2f %10.*f %5.1f %*s %5s",
            w,i,
            dag::print_polar(rotmat).c_str(),
            zscore,q,item.value,pscore,
            ww,topstr.c_str(),
            grpstr.c_str()));
        if (i == nprint) break;
        i++;
      }
    }
    int more = rotation_function_data.size()-nprint;
    if (rotation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(more) + " more");
    output.push_back("");
    return output;
  }

  af_string
  FastRotationFunction::logSlowTable(
      const int nprint, //maximum number to print
      const bool AllSites,
      dmat33 PR
    ) //regardless of whether it is a top site in cluster or not
  {
    af_string output = logTableHeader(nprint);
    bool clustered = rotation_function_data.size() ? rotation_function_data[0].clustered() : false;
    int w = itow(nprint);
    if (!rotation_function_data.size()) return output;
    output.push_back("--------");
    output.push_back("LLG     log-likelihood gain");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(rotation_function_data.size());
    double ftop = std::abs(top());
    int q = std::max(4. - std::floor(std::log10(std::max(1.,ftop))),1.);
    int ww = itow(rotation_function_data.size(),5);
    output.push_back(snprintftos(
        "%-*s %33s %10s %*s %5s",
        w,"#","Polar wrt original [Axis]:[Angle]","LLG",ww,"#Peak","#Grp"));
    int i(1);
    for (auto& item : rotation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered selection below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      dmat33 rotmat;
      std::tie(std::ignore,rotmat) = dag::print_euler_wrt_in_from_mt(item.grid,PR);
      if (AllSites or item.is_topsite())
      {
        output.push_back(snprintftos(
        "%-*i %s % 10.*f %*s %5s",
            w,i,
            dag::print_polar(rotmat).c_str(),
            q,item.value,
            ww,topstr.c_str(),
            grpstr.c_str()));
        if (i == nprint) break;
        i++;
      }
    }
    if (rotation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(rotation_function_data.size()-nprint) + " more");
    output.push_back("");
    return output;
  }

  af_string
  FastRotationFunction::logTable(
      const int nprint, //maximum number to print
      const bool AllSites,
      dmat33 PR
    ) //regardless of whether it is a top site in cluster or not
  {
    af_string output = logTableHeader(nprint);
    bool clustered = rotation_function_data.size() ? rotation_function_data[0].clustered() : false;
    int w = itow(nprint);
    if (!rotation_function_data.size()) return output;
    output.push_back("--------");
    output.push_back("LLG     log-likelihood gain");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(rotation_function_data.size());
    double ftop = std::abs(top());
    int q = std::max(4. - std::floor(std::log10(std::max(1.,ftop))),1.);
    int ww = itow(rotation_function_data.size(),5);
    output.push_back(snprintftos(
        "%-*s %s %10s %*s %5s",
        w,"#",
        dag::header_polar().c_str(),
        "LLG",ww,"#Peak","#Grp"));
    int i(1);
    for (auto& item : rotation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered selection below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      dmat33 rotmat;
      std::tie(std::ignore,rotmat) = dag::print_euler_wrt_in_from_mt(item.grid,PR);
      if (AllSites or item.is_topsite())
      {
        output.push_back(snprintftos(
        "%-*i %s % 10.*f %*s %5s",
            w,i,
            dag::print_polar(rotmat).c_str(),
            q,item.value,
            ww,topstr.c_str(),
            grpstr.c_str()));
        if (i == nprint) break;
        i++;
      }
    }
    if (rotation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(rotation_function_data.size()-nprint) + " more");
    output.push_back("");
    return output;
  }

  double
  FastRotationFunction::top_novel_llg(dag::Node node) //copy
  {
    double top = std::numeric_limits<double>::lowest();
    double fmax_tol = f_max-1.0e-06;
    for (auto& item : rotation_function_data)
    {
      //since we are going to select by percent,
      //the lowest has to be at least the mean
      if (item.value >= f_mean)
      {
        node.NEXT.EULER = item.grid; //in copy
        if (!node.turn_in_poses())
        {
          top = std::max(top,item.value);
          //if the top peak is not in the pose list then we don't need to search down any further
          //peaks may not be sorted in order
          if (item.value >= fmax_tol)
            break;
        }
      }
      else
      {
        top = std::max(top,item.value);
      }
    }
    if (top == std::numeric_limits<double>::lowest()) //eg no rotations, avoid phaser_assert
      return f_max;
    PHASER_ASSERT(top > f_mean); //will also be caught later, flat map
    return top;
  }

  std::pair<dvect3,dmat33>
  FastRotationFunction::turn_best_sym(int i,dmat33 PR) //copy
  {
    dvect3 ineuler,mteuler;
    dmat33 MROT = zyz_matrix(rotation_function_data[i].grid);
    std::tuple<int, int, int> iii = std::make_tuple(100000,100000,100000); //outside range
    for (int isym = 0; isym < REFLECTIONS->SG.NSYMP; isym++)
    {
      dmat33 Rsym = REFLECTIONS->UC.orth_Rtr_frac(REFLECTIONS->SG.rotsym[isym])*MROT;
      dvect3 esym = scitbx::math::euler_angles::zyz_angles(Rsym);
      esym = regularize_angle(esym);
      dvect3 esymin = dag::euler_wrt_in_from_mt(esym,PR);
      esymin = regularize_angle(esymin); //must be here so that it is in positive range
      std::tuple<int, int, int> iii_sym = std::make_tuple(
           static_cast<int>(std::round(esymin[1]*100)), //pph accuracy, beta first
           static_cast<int>(std::round(esymin[0]*100)),
           static_cast<int>(std::round(esymin[2]*100)) );
      if (iii_sym < iii)
      {
        iii = iii_sym;
        ineuler = esymin;
        mteuler = esym;
      }
    }
    ineuler = regularize_angle(ineuler);
    MROT = zyz_matrix(ineuler);
    return {mteuler,MROT};
  }

#include <phasertng/site/FastSelfRotationFunction.cpp>
} //phasertng
