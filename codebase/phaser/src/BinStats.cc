//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/BinStats.h>
#include <phasertng/main/Error.h>

namespace phaser {

void BinStats::reset(Bin& bin)
{
  NUM_bin.resize(bin.numbins());
  FOM_bin.resize(bin.numbins());
  HiRes_bin.resize(bin.numbins());
  LoRes_bin.resize(bin.numbins());
  for (int s = 0; s < bin.numbins(); s++)
  {
    NUM_bin[s] = 0;
    FOM_bin[s] = 0;
    HiRes_bin[s] = bin.HiRes(s);
    LoRes_bin[s] = bin.LoRes(s);
  }
}

floatType BinStats::LoRes(int ibin)
{ if (ibin < 0) ibin = 0; if (ibin >= numbins()) ibin = numbins()-1; return LoRes_bin[ibin]; }

floatType BinStats::HiRes(int ibin)
{ if (ibin < 0) ibin = numbins()-1; if (ibin >= numbins()) ibin = numbins()-1; return HiRes_bin[ibin]; }

int BinStats::numbins() const
{ assert(NUM_bin.size() == FOM_bin.size()); return NUM_bin.size(); }

int BinStats::Num(int s)
{ assert(s < numbins()); return NUM_bin[s]; }

floatType BinStats::FOM(int s)
{ assert(s < numbins()); return (NUM_bin[s] > 0) ? FOM_bin[s]/NUM_bin[s] : 0; }

int BinStats::Num()
{
  int sum(0);
  for (int s = 0; s < NUM_bin.size(); s++) sum += NUM_bin[s];
  return sum;
}

floatType BinStats::FOM()
{
  if (Num())
  {
    floatType num = Num();
    floatType fom(0);
    for (int s = 0; s < FOM_bin.size(); s++) fom += FOM_bin[s];
    return fom/num;
  }
  return 0;
}

void BinStats::addFOM(int s,floatType rFOM,int revFactor)
{
  assert(s < numbins());
  assert(revFactor);
  NUM_bin[s] += revFactor;
  FOM_bin[s] += rFOM/revFactor;
}

floatType BinStats::getBinNum(int s)           { assert(s < numbins()); return NUM_bin[s]; }
void BinStats::setBinNum(int s, floatType v)   { assert(s < numbins()); NUM_bin[s] = v; }

floatType BinStats::getBinFOM(int s)           { assert(s < numbins()); return FOM_bin[s]; }
void BinStats::setBinFOM(int s, floatType v)   { assert(s < numbins()); FOM_bin[s] = v; }

} //phaser
