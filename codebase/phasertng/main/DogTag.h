//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_main_DogTag_class__
#define __phasertng_main_DogTag_class__
#include <phasertng/main/Identifier.h>

namespace phasertng {

  class DogTag
  {
    public:
      Identifier  PATHWAY;
      Identifier  IDENTIFIER;
      bool shortened = false; //flag to return whether tag shortened, not parsed
      DogTag() {}
      DogTag(Identifier a,Identifier b) { PATHWAY = a; IDENTIFIER = b; shortened = false; }
      DogTag(const DogTag& o)
        : PATHWAY(o.PATHWAY), IDENTIFIER(o.IDENTIFIER), shortened(o.shortened) {}

    void initialize(std::pair<std::pair<uuid_t,std::string>,std::pair<uuid_t,std::string>>);
    void initialize(std::vector<uuid_t>,std::vector<std::string>);

    const std::string tag_unparse() const;
    const std::string id_unparse() const;
    const bool is_set_valid() const;
    Identifier identify() { return IDENTIFIER; }
    const Identifier identify() const { return IDENTIFIER; }
             //IDENTIFIER //old parameter
    std::string str() { return IDENTIFIER.str(); }
    std::string str_str() { return PATHWAY.str() + "/" + IDENTIFIER.str(); }
    uuid_t identifier() { return IDENTIFIER.identifier(); }
    std::string tag() { return IDENTIFIER.tag(); }
    const std::string str() const { return IDENTIFIER.str(); }
    const std::string string() const { return IDENTIFIER.string(); }
    void set_invalid() { PATHWAY.set_invalid(); IDENTIFIER.set_invalid(); }

    std::pair<std::string,std::string> FileName(std::string);
    std::pair<std::string,std::string> FileEntry(entry::data);
    std::pair<std::string,std::string> FileOther(other::data);
    std::string extra_file_stem_ext(std::string);
    std::string entry_data_stem_ext(entry::data);
    std::string other_data_stem_ext(other::data);
    std::string database_subdir();

    DogTag& operator=(const DogTag o) { PATHWAY = o.PATHWAY; IDENTIFIER = o.IDENTIFIER; return *this; }
    bool operator!=( const DogTag& rhs) const { return PATHWAY != rhs.PATHWAY or IDENTIFIER != rhs.IDENTIFIER; }
    bool operator==( const DogTag& rhs) const { return PATHWAY == rhs.PATHWAY and IDENTIFIER == rhs.IDENTIFIER; }
    bool operator<( const DogTag& rhs) const {
    //the test of quality in std::set is not all the same, different from ==
      return IDENTIFIER < rhs.IDENTIFIER; }
  };

  struct DogTagHash
  {
    template <typename T>
    std::size_t operator()(T t) const
    {
      return static_cast<std::size_t>(t.identify().identifier());
    }
  };

} //phasertng
#endif
