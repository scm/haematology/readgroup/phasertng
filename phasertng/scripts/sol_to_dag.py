import sys
from libtbx.phil import parse
from phasertng.phil.node_phil_file import node_separator
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.cards_to_phil import cards_to_phil
import argparse
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.cards_to_phil import cards_to_phil

def sol_to_dag(filein=None,fileout=None,database=None,unitcell=None,spacegroup=None,calphas=None):

    indent = "  "
    dbinfo = database.split(",")
    entries = ""
    tags = []
    params = ""
    for db in dbinfo:
      dbstr = ""
      params = db.split(":")
      pdbout = str(params[1])
      assert(len(params) == 2)
      if True:
        from iotbx.pdb import hierarchy
        pdb_in = hierarchy.input(params[1])
        crystal_symmetry = pdb_in.input.crystal_symmetry()
        # pdbset selection = "not hetero" default
        sel_cache = pdb_in.hierarchy.atom_selection_cache()
        if calphas is True:
          working_sel = sel_cache.selection("name ca and (altloc ' ' or altloc A)")
        else:
          working_sel = sel_cache.selection("(altloc ' ' or altloc A)")
        pdb_in_sel = pdb_in.hierarchy.select(working_sel)
        #write pdb after init of fileroot
        pdbout = str(params[1].replace("data/",""))  + ".trace.pdb"
        fullpdbout = "/tmp/PHASERTNG/" + pdbout
        f = open(fullpdbout, "w")
        #the remark cards are not written out with as_pdb_string
        #they have to be added at the start manually from list,adding returns at the end of each line
        #otherwise the serial records on the pdb file will not be carried through after atom selection
        #REMARK PHASER ENSEMBLE MODEL SERIAL <int> RMS <float>
        for item in pdb_in.input.remark_section():
          if item[:1] != "\n":
            item += "\n"
          f.write(item )
        if calphas is True:
          f.write("REMARK PHASER TRACE SAMPLING DISTANCE 3.8\n")
        else:
          f.write("REMARK PHASER TRACE SAMPLING DISTANCE 1.5\n")
        f.write(pdb_in_sel.as_pdb_string( crystal_symmetry=crystal_symmetry,anisou=False,sigatm=False,siguij=False))
        f.close()
      dbstr += node_scope
      dbstr += ".entry {\n"
      dbstr += "  identifier = 1\n"
      dbstr += "  tag = \"" + params[0] + "\"\n"
      dbstr += "  models = \"" + pdbout + "\"\n"
      dbstr += "  interpolation = \"" + pdbout + "\"\n"
      dbstr += "  decomposition = \"" + pdbout + "\"\n"
      dbstr += "  variance = \"" + pdbout + "\"\n"
      dbstr += "  trace = \"" + pdbout + "\"\n"
      dbstr += "  filepath = \"/tmp/PHASERTNG/\"\n"
      dbstr += "}\n"
      tags.append(params[0])
      # add the node and database keywords to the master phil file cf user phil
      # these will be the output phil
      initparams = build_node_str()
      assert(len(initparams))
      master_phil = parse(initparams,converter_registry=converter_registry)
      user_phil = parse(dbstr,converter_registry=converter_registry)
      working_params = master_phil.fetch(source=user_phil)
  #   print("as_str")
  #   print(working_params.as_str())
      cards = phil_to_cards(user_phil_str=working_params.as_str(),program_name="node")
  #   print("phil to cards")
  #   print cards
      phil = cards_to_phil(cards=cards, keyword_list=["node"])
  #   print("cards to phil")
  #   print(phil)
      for line in phil.splitlines():
        dagscope = node_scope + ".node"
        if dagscope not in line:
          entries += indent*3 + line + "\n"
        elif dagscope + ".entry" in line:
          entries += indent*2 + line.replace(dagscope+".entry","entry") + "\n"

    initparams = build_node_str()
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
   #modified_phil = master_phil.format(python_object=working_params)
  # master_phil.show()
  # print("---entries")
  # print entries

    allcardstr = ""
    alldagstr = ""
    philarray = []
    if filein is not None:
      with open(filein, 'r') as f:
        assert(unitcell is not None)
        solstr = f.read()
        solstr = solstr.replace("SOLUTION SET","SOLU SET")
        #delete resolution line
        solstr = solstr.split("SOLU SET")
        for sol in solstr:
          if "SOLU" in sol: #not an empty string
            print("SOLU SET " + sol)  # add back in split for the print
            dagstr  = indent*1 + "node {\n"
            lines = sol.splitlines()
            dagstr += indent*2 + "annotation = " + lines[0] + "\n"
            dagstr += indent*2 + "unitcell = " + unitcell.replace(","," ") + "\n"
            vrms = 0
            for line in lines:
              if "SPAC" in  line:
                words = line.split()
                dagstr += indent*2 + "spacegroup = "
                for i in range(2,len(words)):
                  dagstr +=  words[i] + " "
                dagstr += "\n"
              if "6DIM" in line: #not an empty string
                dagstr += indent*2 + "pose {\n"
                words = line.split()
                assert(len(words) >= 13)
                dagstr += indent*3 + "identifier = 1\n"
                dagstr += indent*3 + "tag = " + words[3] + "\n"
                dagstr += indent*3 + "euler = " + words[5] + " " + words[6] + " " + words[7] + "\n"
                dagstr += indent*3 + "fractional = " + words[9] + " " + words[10] + " " + words[11] + "\n"
                dagstr += indent*3 + "bfactor = " + words[13] + "\n"
                dagstr += indent*3 + "multiplicity = 1\n"
                dagstr += indent*3 + "tncs_group = 1\n"
                dagstr += indent*3 + "overlap = 0 0 0\n"
                dagstr += indent*3 + "nclus = 0\n"
                dagstr += indent*2 + "}\n"
              if "VRMS" in line: #not an empty string
                words = line.split()
                assert(len(words) >= 5)
                drms = words[5]
            #drms
            dagstr += entries
            dagstr += indent + "}\n"
      #     print dagstr
            alldagstr += dagstr
            #create the dagstr as the node only, then add the outer_scope for parsing
            alldagstr += str(node_separator) + "\n"
            check_phil_will_parse = True
            if check_phil_will_parse:
              scopedagstr = node_scope + " {\n" + dagstr + "}\n"
              phaser_phil = parse(scopedagstr,converter_registry=converter_registry)
     #        master_phil.fetch(source=phaser_phil).show()
              working_params = master_phil.fetch(source=phaser_phil).extract()
              philarray.append(working_params)
  #           print phil_to_cards(user_phil=working_params,program_name="node")
              allcardstr += phil_to_cards(user_phil=working_params,program_name="node")
              allcardstr += str(node_separator) + "\n"
    else:
            assert(unitcell is not None)
            assert(spacegroup is not None)
            dagstr  = indent*1 + "node {\n"
            dagstr += indent*2 + "annotation = \"origin\"\n"
            dagstr += indent*2 + "unitcell = " + unitcell.replace(","," ") + "\n"
            dagstr += indent*2 + "spacegroup = " + spacegroup + "\n"
            for tag in tags:
                dagstr += indent*2 + "pose {\n"
                dagstr += indent*3 + "identifier = 1\n"
                dagstr += indent*3 + "tag = " + tag + "\n"
                dagstr += indent*3 + "euler = 0 0 0\n"
                dagstr += indent*3 + "fractional = 0 0 0\n"
                dagstr += indent*3 + "bfactor = 0.\n"
                dagstr += indent*3 + "multiplicity = 1\n"
                dagstr += indent*3 + "tncs_group = 1\n"
                dagstr += indent*3 + "overlap = 0 0 0\n"
                dagstr += indent*3 + "nclus = 0\n"
                dagstr += indent*2 + "}\n"
            #drms
            dagstr += entries
            dagstr += indent + "}\n"
      #     print dagstr
            alldagstr += dagstr
            #create the dagstr as the node only, then add the outer_scope for parsing
            alldagstr += str(node_separator) + "\n"
            check_phil_will_parse = True
            if check_phil_will_parse:
              scopedagstr = node_scope + " {\n" + dagstr + "}\n"
              phaser_phil = parse(scopedagstr,converter_registry=converter_registry)
    #         master_phil.fetch(source=phaser_phil).show()
              working_params = master_phil.fetch(source=phaser_phil).extract()
              philarray.append(working_params)
  #           print phil_to_cards(user_phil=working_params,program_name="node")
              allcardstr += phil_to_cards(user_phil=working_params,program_name="node")
              allcardstr += str(node_separator) + "\n"

    print("Dag for packing:")
    print("----------------")
    print(allcardstr)
    print("----------------")
    with open(fileout, 'w') as fo:
    # don't add outer scope, node list is different from input scope
      fo.write(allcardstr)
    print(fileout)
    return


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-f',"--filein",help="filein",required=False)
  parser.add_argument('-o',"--fileout",help="fileout",required=True)
  parser.add_argument('-db',"--database",help="database",required=True)
  parser.add_argument('-uc',"--unitcell",help="unitcell",required=True)
  parser.add_argument('-sg',"--spacegroup",help="spacegroup",required=False)
  parser.add_argument('-ca',"--calphas",help="calphas (default true)",default=True,required=False)
  args = parser.parse_args()
  sol_to_dag(filein=args.filein,
             fileout=args.fileout,
             database=args.database,
             unitcell=args.unitcell,
             spacegroup=args.spacegroup,
             calphas=args.calphas,
             )
  sys.exit(0)
