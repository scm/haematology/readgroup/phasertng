#ifndef __phasertng__pNormal__function__
#define __phasertng__pNormal__function__
#include <cassert>
#include <cmath>
#include <scitbx/constants.h>

// Common probability distribution

namespace phasertng {

inline double pNormal(double& x, double& mu, double& sigma)
{
  // Returns probability density for a variable x which has a normal
  // distribution with mean mu and standard deviation sigma
  assert(sigma > 0);
  double xmu(x - mu);
  double xmu2 = xmu*xmu;
  return std::exp(-xmu2/(2*sigma*sigma)) /
    (sigma*std::sqrt(scitbx::constants::two_pi));
}

}

#endif
