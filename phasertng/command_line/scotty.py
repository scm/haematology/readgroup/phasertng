# LIBTBX_SET_DISPATCHER_NAME phasertng.scotty

from iotbx.cli_parser import run_program
from phasertng.programs import scotty

if (__name__ == '__main__'):
  try:
    run_program(scotty.Program)
  except Exception as e:
    print(str(e))
