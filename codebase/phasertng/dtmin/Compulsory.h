//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_mintbx_Compulsory_class__
#define __phasertng_mintbx_Compulsory_class__
#include <phasertng/dtmin/includes.h>

typedef std::vector<bool> sv_bool;
typedef std::vector<std::string> sv_string;
typedef std::vector<double> sv_double;

namespace phasertng {
namespace dtmin {

class Compulsory
{
  public:
    Compulsory() { }
    virtual ~Compulsory() {}

  public:
    // Set each macrocycle in set_macrocycle_protocol(...)
    sv_bool
    refine_parameter_; // Bit mask of which parameters are being refined this macrocycle.

    // Number of parameters being refined this macrocycle.
    // Calculated from refine_parameter_, is the total number of true in refine_parameter_.
    int
    nmp = 0;

    // Total number of parameters.
    // Calculated from refine_parameter_, is the size of refine_parameter_.
    int
    total_number_of_parameters_ = 0;

  public:
    // Must set refine_parameter_, nmp, total_number_of_parameters_.
    virtual void
    set_macrocycle_protocol(const sv_string) = 0;

    // Return vector of names of the parameters being refined this macrocycle.
    virtual sv_string
    macrocycle_parameter_names() = 0;

    // current values of the parameters being refined this macrocycle.
    virtual sv_double
    get_macrocycle_parameters() = 0;

    virtual void
    set_macrocycle_parameters(sv_double) = 0;

    // large shifts for parameters
    virtual sv_double
    macrocycle_large_shifts() = 0;

    // target function, smaller better (minimization!)
    virtual TargetType
    target() = 0;

    // returns target and gradient
    virtual TargetGradientType
    target_gradient() = 0;

    // returns target and gradient and hessian
    virtual TargetGradientHessianType
    target_gradient_hessian() = 0;

};

}} //phasertng

#endif
