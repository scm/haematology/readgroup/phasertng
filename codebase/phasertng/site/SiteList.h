//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserveL
#ifndef __phasertng_SiteList_class__
#define __phasertng_SiteList_class__
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/tiny_types.h>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>

using namespace scitbx;
typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::af::shared<dvect3> af_dvect3;

namespace phasertng {

class SiteList
{
  public:
    SiteList() {}
    virtual ~SiteList() {}

  public:
    virtual std::size_t count_sites() = 0;
    virtual void        restart(unsigned=0) = 0;
    virtual bool        at_end() = 0;
    virtual dvect3      next_site() = 0;
    virtual af_dvect3   all_sites() = 0;
    virtual double      point_distance() = 0;
};

}
#endif
