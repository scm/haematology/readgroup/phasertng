//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PattersonFunction_class__
#define __phasertng_PattersonFunction_class__
#include <phasertng/pod/frequencies.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/FileSystem.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>

namespace phasertng {
typedef af::versa<double, af::c_grid<3> >   versa_grid_double;

  class PattersonFunction : public FileSystem
  {
    private:
      bool   ORIGIN_REMOVED = false;
      int r1 = 0;
      int r2 = 0;
      double random_check1 = 0;
      double random_check2 = 0;
      af_millnx MILLER_RES_LIMITS;
      af_cmplex IOBS_EQUIVALENT;
      af_double SIGIOBS_EQUIVALENT;

    private:
      //have to store both, don't know why
      //af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref;
      versa_grid_double real_map_unpadded;
      UnitCell UC;

    public:
      double Patterson_hires = 0;
      double Patterson_max = 0;
      double Patterson_top = 0;
      double Patterson_mean = 0;
      double Patterson_sigma = 0;
      dvect3 Patterson_topsite = {0,0,0};

      af::int3 Patterson_gridding;
      bool   insufficient_data = false;
      bool   insufficient_resolution = false;
      af_double Patterson_heights_percent;
      af_double Patterson_zscore;
      af_dvect3 Patterson_sites;
      af_int Patterson_multiplicity;

      std::vector<pod::frequencies> frequency_peaks;
      SpaceGroup SG; // space group of crystal
      SpaceGroup DERIVED_SG; // space group without translational components
      SpaceGroup PATT_SG; // Patterson space group

    public:
      PattersonFunction() {}

      //pseudo-named arguments
      dvect3 site(int i)
      { return dvect3(std::fabs(Patterson_sites[i][0]) < 1.0e-06 ? 0 : Patterson_sites[i][0],
                      std::fabs(Patterson_sites[i][1]) < 1.0e-06 ? 0 : Patterson_sites[i][1],
                      std::fabs(Patterson_sites[i][2]) < 1.0e-06 ? 0 : Patterson_sites[i][2]); }
      double& zscore(int i)
      { return Patterson_zscore[i]; }
      double& percent(int i)
      { return Patterson_heights_percent[i]; }

    public:
      af_string
      calculate
      (
        cctbx::sgtbx::space_group,
        cctbx::uctbx::unit_cell,
        af::shared<millnx>,
        af_cmplex,
        af_double,
        bool, //origin removed
        bool, //use multiplicity
        double, //mapcut_percent
        int, //min reflections
        double, //min lores
        ivect3 = ivect3(0,0,0)
      );

      std::pair<versa_grid_double,af::int3>
      calculate_substructure(
        UnitCell,// UC,
        SpaceGroup,// SG,
        bool,// haveFpart,
        af_cmplex, //Fpart
        af_millnx,// Fmiller
        af_double,// mI
        double //LLconst
      );

      void WriteMap(FileSystem);
      void WriteMap(FileSystem,versa_grid_double);
      void WritePdb(FileSystem,double);
      void WritePdb(FileSystem,double,af_dvect3);
      void WriteMtz(FileSystem);

      af_string logStatistics(double,std::string,int,double,double=0);
      af_string logStatsExtra();
      af_string logFrequencies(std::string);
      double eight_point_interpolation_percent(dvect3);
      int number_reported(double);

      std::pair<af_dvect3,versa_grid_double>
         constellation(bool,std::size_t,af_dvect3);
      bool harker(dvect3);
      std::pair<bool,double> origin(dvect3,double);
      void erase_origin(double);
  };

}
#endif
