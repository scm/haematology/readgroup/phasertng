//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_OnlyOnDisk_class__
#define __phasertng_OnlyOnDisk_class__
#include <phasertng/io/EntryBase.h>

namespace phasertng {

class OnlyOnDisk :  public EntryBase
{
  public:
    OnlyOnDisk(): EntryBase() {} //for class member declaration
    bool in_memory() { return false; }
    void read_from_disk() { }
};

} //phasertng
#endif
