#ifndef __phasertng__bessel_ratio_function__
#define __phasertng__bessel_ratio_function__

namespace phasertng {

inline double besselI1_on_besselI0(double& V)
{
// ----------------------------------------------------------------
// RETURNS BESRAT = A(K) FOR K = ABS(V), WHERE A(K) IS THE EXPECTED
// MODULUS OF THE MEAN VECTOR SUM OF UNIT VECTORS SAMPLED FROM THE
// VON MISES DISTRIBUTION OF DIRECTIONS IN 2D WITH PARAMETER = K.
// A(V) = THE RATIO OF MODifIED BESSEL FUNCTIONS OF THE FIRST KIND
// OF ORDERS 1 AND 0, I.E., A(V) = I1(V)/I0(V).
// ----------------------------------------------------------------
//
//  ADJUST TO S DECIMAL DIGIT PRECISION BY SETTING DATA CONSTANTS -
//     C1 = (S+9.0-8.0/S)*0.0351
//     C2 = ((S-5.0)**3/180.0+S-5.0)/10.0
//     CX = S*0.5 + 11.0
//  FOR S IN RANGE (5,14).  THUS FOR S = 9.3 :
      const double C1(0.613);
      const double C2(0.475);
      const double CX(15.65);
//
      double Y = 0.0;
      double X = std::fabs(V);
      if (X <= CX)
      {
//
//  FOR SMALL X, RATIO = X/(2+X*X/(4+X*X/(6+X*X/(8+ ... )))
      int N = static_cast<int>((X+16.0-16.0/(X+C1+0.75))*C1);
      X *= 0.5;
      double XX = X*X;
      for (int J = 1; J <= N; J++)
        Y = XX/(double(N-J+2)+Y);
      return X/(1.0+Y);
      }
//
//  FOR LARGE X, RATIO = 1-2/(4X-1-1/(4X/3-2-1/(4X/5-2- ... )))
      int N = static_cast<int>((68.0/X+1.0)*C2) + 1;
      X *= 4.0;
      double XX(N*2+1);
      for (int J = 1; J <=30; J++)
      {
        Y = XX/((-2.0-Y)*XX+X);
        XX = XX - 2.0;
      }
      return 1.0 - 2.0/(X-1.0-Y);
}

}

#endif
