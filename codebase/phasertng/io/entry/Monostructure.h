//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Monostructure_class____
#define __phasertng_Monostructure_class____
#include <phasertng/main/Identifier.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/io/EntryBase.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/principal_statistics.h>
#include <phasertng/io/principal_components.h>
#include <set>

namespace phasertng {

//Monostructure are a set of points masquerading as atoms in a pdb file for refinement
  class Monostructure : public EntryBase
  {
    protected:
      std::set<std::string> pdb_errors = {};

    public: //members
      Identifier MODLID;
      Identifier REFLID;
      std::string TIMESTAMP = "";
      //must be a vector for using mmtbx types for packing etc
      std::vector<PdbRecord> PDB = std::vector<PdbRecord>(0);
      sv_string REMARKS = sv_string(0);
   //   pod::serial SERIAL = pod::serial();
      SpaceGroup SG = SpaceGroup("P 1"); //default= P1
      UnitCell UC; //default= 1 1 1 90 90 90
      principal_statistics statistics;
      principal_components components;

    public: //getters
      std::string read_errors();
      bool        has_read_errors() { return pdb_errors.size() > 0; }
      void        erase_invalid();
      int         size() const  { return PDB.size(); }
      af_dvect3   get_atom_list() const;

    public: //constructors
      Monostructure() : EntryBase() {}
      Monostructure(af_string pdblines) : EntryBase() { SetPdb(pdblines); }
      bool in_memory() { return PDB.size(); }
      void read_from_disk() { ReadPdb(); }
      void write_to_disk() { WritePdb(SG,UC); }

    public:
      void       ReadPdb();
      void       WritePdb(SpaceGroup,UnitCell);
      af_string  get_pdb_cards();
      void       GetPdbHandler(PdbHandler&);
      void       SetPdb(af_string);

 //convert to Models to get scattering etc
      int    nresidues();
      void   set_principal_statistics();
      void   set_principal_components();
      void   shift_centre(dvect3);

    public:
      std::pair<int,bool>  calculate_segment_identifiers(const int,const int);
      std::vector<PdbRecord> select_segment(const int,int&);
      void apply_wilson_bfactor(double);
  };

}
#endif
