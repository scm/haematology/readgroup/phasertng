#ifndef __phasertng_table_cos_sin_lin_interp_class__
#define __phasertng_table_cos_sin_lin_interp_class__
#include <scitbx/constants.h>
#include <complex>
#include <vector>


namespace phasertng { namespace table {

  class cos_sin_lin_interp_table
  {
    public:
      static cos_sin_lin_interp_table & getInstance() {
      // see https://stackoverflow.com/questions/3533503/using-singleton-in-different-classes
      // on how to ensure we only have one instance of this class during execution
        return inst_;
      }
      static cos_sin_lin_interp_table inst_; // initialised in cos_sin.cc

      std::complex<double>
      get(double unary_angle) const;

      std::complex<double> operator()(double unary_angle) const {
        return get(unary_angle);
      }

    private:
      cos_sin_lin_interp_table(int n_points = 4000);
      // make sure copy and assignment also only return an address
      cos_sin_lin_interp_table(const cos_sin_lin_interp_table&);
      cos_sin_lin_interp_table& operator=(const cos_sin_lin_interp_table&);

      int n_points_;
      int n_points_4_;
      std::vector<double> values_;
      std::vector<double> diffvalues_;
  };


}} // namespace cctbx::math
#endif
