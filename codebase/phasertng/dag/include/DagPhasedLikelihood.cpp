//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/math/likelihood/llgp/function.h>
#include <phasertng/math/likelihood/llgp/helper.h>
#include <future>
#include <phasertng/src/SpaceGroup.h>

//#define PHASERTNG_DEBUG_WORKING_NODE
//#define PHASERTNG_FAST_INTERPV

namespace phasertng {
namespace dag {

  double
  DagDatabase::work_node_phased_likelihood(//of working node
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS
    )
  {
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    WORK->MAPLLG = combined_phased_tgh(
        REFLECTIONS,
        SELECTED,
        NTHREADS,
        USE_STRICTLY_NTHREADS
      );
    WORK->MAPLLG *= -1; //we don't want the target -llg, which is only for refinement
    WORK->MAPLLG *= REFLECTIONS->oversampling_correction();
    return WORK->MAPLLG;
  }

  double
  DagDatabase::combined_phased_tgh(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS
    )
  {
    double Target = 0;
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    sv_cmplex ecalcs_pose; sv_double sigmaa_pose;
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      Target = parallel_combined_phased_tgh(
              REFLECTIONS,
              SELECTED,
              beg,
              end
              );
    }
    else
    {
      std::vector<std::future< double > > results;
      std::vector<std::packaged_task< double(
              const_ReflectionsPtr,
              sv_bool,
              int,
              int
              )>> packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&DagDatabase::parallel_combined_phased_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              REFLECTIONS,
              SELECTED,
              beg,
              end
              );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &DagDatabase::parallel_combined_phased_tgh, this,
              REFLECTIONS,
              SELECTED,
              beg,
              end
              ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      Target = parallel_combined_phased_tgh(
              REFLECTIONS,
              SELECTED,
              beg,
              end
              );
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        double thread_llg = result;
        Target += thread_llg;
      }
    }
    return Target;
  }

  double
  DagDatabase::parallel_combined_phased_tgh(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      int beg,
      int end
    )
  {
    double Target = 0;
    const SpaceGroup& SG = REFLECTIONS->SG;

    //collect the terms that will not change
    auto pose_terms = WORK->pose_terms_interpolate();

    //cmplex TwoPiI = scitbx::constants::two_pi*cmplex(0,1);
    likelihood::llgp::tgh_flags flags;
    flags.target(true).gradient(false).hessian(false);
    flags.variances(false);

    likelihood::llgp::function LLGP;
    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const millnx& miller = REFLECTIONS->get_miller(r);
        /* Handling of dobs equivalent needs to be generalised
           For superposition of template density on target density, dobs set to 1
           For docking an atomic model into cryoEM density, dobs equivalent should
           be Cref, i.e. the estimated correlation between the map and the
           true structure (computed e.g. from sqrt(2*FSC/(1+FSC)))
        */
        //const int&    bin  =   REFLECTIONS->get_int(labin::BIN,r);
        const double& dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double& feff =   REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double& resn =   REFLECTIONS->get_flt(labin::RESN,r);
        const double& phif =  scitbx::deg_as_rad(REFLECTIONS->get_flt(labin::MAPPH,r));
        const double& rssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
        const bool&   cent =   REFLECTIONS->get_bln(labin::CENT,r);
        const double& eps  =   REFLECTIONS->get_flt(labin::EPS,r);
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        //first pass calculate total EC and total variance
        //for likelihood target, need summed EC and variance value
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);
        sv_double DobsSigaSqr_pose(WORK->POSE.size());
        af_cmplex FCpose(WORK->POSE.size(),{0.,0.});
        for (int p = 0; p < WORK->POSE.size(); p++)
        {
          const auto& pose = WORK->POSE[p];
          const auto& entry = pose.ENTRY;
          //int b = entry->INTERPOLATION.BIN.get_bin_ssqr(rssqr);
          const double ssqr = rssqr; //entry->INTERPOLATION.BIN.MidSsqr(b);
          //use the reference to avoid lookup on [p] each time
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac = WORK->POSE[p].BFAC;
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = WORK->DRMS_SHIFT[pose.IDENTIFIER].VAL;
//in DagDatabase::work_node_phased_likelihood
          double thisDobsSigaSqr = likelihood::llgp::DobsSigaSqr(
              ssqr, SigaSqr, fs, dobs, drms, bfac);
          DobsSigaSqr_pose[p] = thisDobsSigaSqr;
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
            cmplex iTarget = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
//in DagDatabase::work_node_phased_likelihood
            cmplex scalefac = likelihood::llgp::EcalcTerm(
                thisDobsSigaSqr, eps, SG.NSYMP,
                SymHKL, pose_terms[p].TRA, rsym.traMiller[isym]);
            cmplex thisE = scalefac * iTarget;
            DobsSigaEcalc += thisE;
            FCpose[p] += thisE;
          }
          DobsSigaSqr += thisDobsSigaSqr;
        }
        //====
        //LLGP calculation
        //====
        double V = 1. - DobsSigaSqr;
        cmplex eobs = feff/resn * std::exp(cmplex(0.,1.)*phif);
        LLGP.target_gradient_hessian(DobsSigaSqr, eobs, DobsSigaEcalc, V, cent, flags);
        Target -= LLGP.LL; // Already log-likelihood-gain, so no wll term
      } //selected
    } //reflections
    return Target;
  }

}} //phasertng
