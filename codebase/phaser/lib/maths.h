//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MATH__
#define __PHASER_MATH__
#include <cmath>

namespace phaser {

  double    lookup_sigesq_cent(double);
  double    lookup_sigesq_acent(double);

}//end namespace phaser

#endif
