#ifndef __phasertng_subgroups__
#define __phasertng_subgroups__
#include <cctbx/sgtbx/space_group_type.h>
#include <set>
#include <string>

namespace phasertng {

  std::string
  remove_hall_translations(std::string);

  std::vector<cctbx::sgtbx::space_group_type>
  subgroups(cctbx::sgtbx::space_group_type,bool,std::set<std::string>&);

  std::vector<cctbx::sgtbx::space_group_type>
  groups_sysabs(cctbx::sgtbx::space_group_type);


  std::vector<cctbx::sgtbx::space_group_type>
  space_groups_in_same_point_group(
      cctbx::sgtbx::space_group_type,
      std::string,
      std::set<std::string>);

  class sorted_space_group_type : public cctbx::sgtbx::space_group_type
  {
    public:
    sorted_space_group_type(cctbx::sgtbx::space_group_type name)
    {
      cctbx::sgtbx::space_group_type::operator=(name);
    //  *(static_cast<cctbx::sgtbx::space_group_type*>(this)) = name;
    }

    bool operator<( const sorted_space_group_type& rhs ) const
    {
      int order_a = group().order_z();
      int order_b = rhs.group().order_z();
      return order_a == order_b ?
             hall_symbol() < rhs.hall_symbol() : order_a < order_b ;
    }
  };

  class reverse_sorted_space_group_type : public cctbx::sgtbx::space_group_type
  {
    public:
    reverse_sorted_space_group_type(cctbx::sgtbx::space_group_type name)
    {
      cctbx::sgtbx::space_group_type::operator=(name);
    //  *(static_cast<cctbx::sgtbx::space_group_type*>(this)) = name;
    }

    bool operator<( const reverse_sorted_space_group_type& rhs ) const
    {
      int order_a = group().order_z();
      int order_b = rhs.group().order_z();
      return order_a == order_b ?
             hall_symbol() > rhs.hall_symbol() : order_a > order_b ;
    }
  };

  std::set<sorted_space_group_type>
  subgroups_sysabs(cctbx::sgtbx::space_group_type,bool=false);

  std::vector<cctbx::sgtbx::space_group_type>
  sad_space_groups(cctbx::sgtbx::space_group_type,std::string,std::set<std::string>);

}//phasertng
#endif

