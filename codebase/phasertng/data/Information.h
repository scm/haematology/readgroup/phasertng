//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
// Class to perform various information calculations given a reference to a set
// of reflections
#ifndef __phasertng_Information_class__
#define __phasertng_Information_class__
#include <phasertng/main/includes.h>
#include <phasertng/main/pointers.h>

namespace phasertng {

namespace likelihood { namespace isad { class sigmaa; }}

  class Information
  {
    public:
      Information(const_ReflectionsPtr REFLECTIONS_,
                  const sv_bool& SELECTED_,
                  int NTHREADS_,
                  bool USE_STRICTLY_NTHREADS_) :
          REFLECTIONS(REFLECTIONS_),
          SELECTED(SELECTED_),
          NTHREADS(NTHREADS_),
          USE_STRICTLY_NTHREADS(USE_STRICTLY_NTHREADS_) {}

      const_ReflectionsPtr  REFLECTIONS;
      const sv_bool  SELECTED;
      int NTHREADS;
      bool USE_STRICTLY_NTHREADS;
      ///output:
      sv_double INFO;
      sv_bool   INFO_PRESENT;
      sv_double EINFO;
      sv_bool   EINFO_PRESENT;
      sv_double SINFO;
      sv_bool   SINFO_PRESENT;
      sv_double SELLG;
      sv_bool   SELLG_PRESENT;
      sv_double SEFOM;
      sv_bool   SEFOM_PRESENT;

      void calculate_info();
      void calculate_einfo();
      void calculate_mapinfo();
      void calculate_sinfo(likelihood::isad::sigmaa);
      void calculate_sellg(likelihood::isad::sigmaa, double, double);

      void partial_info_calculation(int, int);
      void partial_einfo_calculation(int, int);
      void partial_sinfo_calculation(likelihood::isad::sigmaa, int, int);
      void partial_sellg_calculation(likelihood::isad::sigmaa, int, int);
  };

} // phasertnclass Information

#endif
