import iotbx.phil
import os.path
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from phasertng.phil.master_phil_file import *
from phasertng.phil.master_node_file import *
from phasertng.phil.converter_registry import *
from phasertng.scripts.OrderedSet import *
from phasertng.scripts.OrderedDict import *

def phil_to_cards(user_phil_str=None,
                  user_phil=None,
                  ignore_params=[],
                  diff=True,
                  program_name=None,
                  keyword_list=None,
                  debugger=False):
  if (debugger):
    print("start phil_to_cards")
# if program_name is None and keyword_list is None:
#   raise Exception("either program_name or keyword_list must be given")

  #guess whether the use is wanting the phasertng or phaserdag master phil
  #if diff=True is used, which is the default then it doesn't matter
  if (diff and program_name is not None): #we can concatenate, as one complete set will be the default and excluded
    master_params = build_node_str() + build_phil_str(program_name=program_name,keyword_list=keyword_list)
  else:
    #both must not be none, odd logic of multiple overwrites below
    master_params = build_node_str() #does not need other keywords
    if (program_name is not None or keyword_list is not None):
      master_params = build_phil_str(program_name=program_name,keyword_list=keyword_list)
    #but...
    if user_phil is not None and str(node_scope) in dir(user_phil):
      master_params = build_node_str() #overwrite
    elif user_phil_str is not None:
      extract = parse(user_phil_str,converter_registry=converter_registry).extract()
      if str(node_scope) in dir(extract):
        master_params = build_node_str() #overwrite
  master_phil = parse(master_params,converter_registry=converter_registry)
# if user_phil is not None:
#   phil_scope = master_phil.format(python_object=user_phil)  # converts extract to scope
#   user_phil_str = phil_scope.as_str(expert_level=maximum_expert_level, attributes_level=0) # converts scope to str
# user_phil = parse(user_phil_str,converter_registry=converter_registry)
# working_phil = master_phil.fetch(source=user_phil)
# AJM above has the problem that e.g. macaniso goes in as default + uservalue not just uservalue
  multiple_keywords = defaultdict(list)
  for e in master_phil.all_definitions():
    singleline = e.parent.multiple
    #special case so that doesn't have to be multiple!
    extract_attribute_error = False
    if (getattr(e.object, "style", extract_attribute_error) is not extract_attribute_error):
      styletype = str(getattr(e.object, "style"))
      if "multiple" in styletype:
         singleline = True
    if (singleline): #special case so that doesn't have to be multiple!
      tester = ".".join(e.path.split('.')[:-1])
      trailing = ".".join(e.path.split('.')[-1:])
      multiple_keywords[tester].append(dict({ trailing: None }))

#AJM all_definitions lists all input parameters and all

  if user_phil is not None:
    if (debugger):
      print("user_phil is not None")
    # user_phil does not contain type information!! must use master_phil.format()
    #libtbx.phil.scope_extract type
    working_phil = master_phil.format(python_object=user_phil)
    #libtbx.phil.scope type
    if (diff):
      working_phil = master_phil.fetch_diff(source=working_phil) #changed only
    else:
      working_phil = master_phil.fetch(source=working_phil) #all keywords
    if (debugger):
      working_phil.show()
  else:
    if (debugger):
      print("user_phil is None")
    user_phil = parse(user_phil_str,converter_registry=converter_registry)
    #libtbx.phil.scope type
    if (diff):
      working_phil = master_phil.fetch_diff(source=user_phil) #changed only
    else:
      working_phil = master_phil.fetch(source=user_phil) #all keywords
    if (debugger):
      working_phil.show()

  #find the keywords in the user phil, ignore all the others in the master phil
  user_params = set()
  for e in working_phil.all_definitions():
    keyword = ".".join(e.path.split(".",2)[1:2]) + "." # e.g. "phasertng.data." - note trailing dot
    user_params.add(keyword)
  if (debugger):
    for u in user_params: print("user params",u)

  #find the keywords in the user phil, ignore all the others in the master phil
  cards = ""
  for e in working_phil.all_definitions():
    if (debugger):
      print("e.path",e.path)
      print(e.path.split(".",2)[1:2])
    keyword = ".".join(e.path.split(".",2)[1:2]) + "."
    if (debugger):
      print("keyword",keyword)
    if not (keyword in ignore_params) and keyword in user_params:
      valuetype = str(getattr(e.object, "type"))
      if (debugger):
        print("valuetype",valuetype)
      if True:
        uservalue = ""
        for word in e.object.words:
          uservalue += str(word) + " "
        uservalue = uservalue[:-1]
        if (debugger):
          print("uservalue",uservalue)
        starred = False
        if valuetype.startswith("choice") and not valuetype.startswith("choices"):
          uservalue = ""
          #choice should have default *
          for word in e.object.words:
             if word.value.startswith("*"):
               starred = True
              #uservalue = word.value.replace("*","").lower()
               uservalue += word.value.replace("*","") + " "
          if (debugger):
            print("choice uservalue",uservalue)
        else:
          starred = True
          if (debugger):
            print("starred")
        if starred:
          if valuetype.startswith("atom_selection") or valuetype.startswith("path"):
            if (debugger):
              print("startswith",valuetype.startswith("atom_selection"),valuetype.startswith("path"))
            if (uservalue == "None"):
              uservalue = ""
            elif (uservalue[1:] == '""' and uservalue[-2:] == '""'):
              uservalue = uservalue[1:-1]
            elif (uservalue[0:] != '"' and uservalue[-1:] != '"'):
              uservalue = '"' + uservalue + '"'
          if (debugger):
            print("starred uservalue",uservalue)
          if str(uservalue) == "True" or str(uservalue) == "False":
            uservalue = "ON" if str(uservalue) == "True" else "OFF"
          if (debugger):
            print("final uservalue",uservalue)
         #line = e.path.replace(phil_scope,"").lower().replace("."," ")
         #line = e.path.replace(phil_scope,"").replace("."," ")
         #line = e.path.replace(node_scope,"").replace("."," ")
          line = e.path.replace("."," ")
          line += " " + str(uservalue) + "\n"
          if (debugger):
            print("line",line)
          if not e.parent.multiple:
              if (debugger):
                print("not multiple")
              if str(uservalue) != "None" and len(uservalue):
                if line[0] == ' ':
                  line = line[1:]
                cards += line
          elif e.parent.multiple:
              if (debugger):
                print("multiple")
            # allow None, because we always want to fill the dictionary
              for key in multiple_keywords:
                if e.path.startswith(key):
                  m = key
                  subkey = e.path.replace(m,"").replace("."," ")[1:]
                  for val in multiple_keywords[key]:
                    for k in val:
                      if k == subkey:
                      # this trimming is not needed but it removes blank spaces
                      # after and before quotes at bookends of strings " P 32 "-> "P 32"
                      # which is needed for the getQuotedString function in
                      # CCP4base which has optional quotes - forcing quotes fixes problem
                      # while uservalue[0] == '"' and uservalue[1] == ' ':
                      #   uservalue = uservalue[:1] + uservalue[2:] # remove index 1
                      # while uservalue[len(uservalue)-1] == '"' and uservalue[len(uservalue)-2] == ' ':
                      #   i = len(uservalue)-2 # remove index i
                      #   uservalue = uservalue[:i] + uservalue[i+1:]
                        val[k] = uservalue
                        if (debugger):
                          print("m",m, "subkey",subkey,"uservalue",uservalue, uservalue == str(None))
                      # note that usevalue = None it is recorded as str(None) not null-None
                      # so that it can be differentiated from null-None which is reserved
                      # to flag an empty dictionary entry for the sub-keyword (and hence for
                      # setting found_all and writing)
                  found_all = True
                  found_any_None = False
                  # two conditions must be met for printing
                  # all values must be not None and all must have been set
                  for val in multiple_keywords[key]:
                    for k in val:
                      if val[k] is None:
                        found_all = False
                      if val[k] == str(None):
                        found_any_None = True
                      if (debugger):
                        print("key",key,"k",k,"found_all",found_all,"found_any_none",found_any_None)
                  if found_all:
                   #parent_key = m.replace(phil_scope,"").replace("."," ")[1:]
                   #parent_key = m.replace(node_scope,"").replace("."," ")[1:]
                    parent_key = m.replace("."," ")
                    multi_keys = parent_key
                    for val in multiple_keywords[key]:
                      for k in val:
                        multi_keys += " " + k + " " + val[k]
                        val[k] = None #reset
                   #print multi_keys if they all have values
                    if not found_any_None:
                      cards += multi_keys + "\n"

 #print cards
 #return cards
  #add outer scope at the end, fails with missing p in phasertng if naive approach above
  #this is because the algorithm can only handle one level of nesting
  lines = cards.splitlines()
  tngcards = ""
  for line in lines:
 #  tngcards += phil_scope + " "
    tngcards += line + "\n"
  if (debugger):
    print("----cards---")
    print(tngcards)
  return tngcards

def phil_to_cards_change_mode(phil_str=None,mode=None):
  phaser_phil = parse(phil_str,converter_registry=converter_registry)
  initparams = build_phil_str(program_name="InputCard")
  assert(len(initparams))
  master_phil = parse(initparams,converter_registry=converter_registry)
  working_params = master_phil.fetch(source=phaser_phil).extract()
  working_params.phasertng.mode = [ mode ]
  cards = phil_to_cards(user_phil=working_params,ignore_params=[],program_name="InputCard")
  return cards

if __name__ == "__main__":
  user_params = '''
phaserdag.node.tncs_order = 1
phaserdag.node.tncs_rms = 0
phaserdag.node.total_scattering = 0
phaserdag.node.z = 1
phaserdag.node.total_molecular_weight = 0
phaserdag.node.annotation = None
phaserdag.node.spacegroup = "Hall: P 2ac 2ab (x,y,z)"
phaserdag.node.unitcell = 73.58 38.73 23.19 90.00 90.00 90.00
phaserdag.node.twinned = False
phaserdag.node.clash_matrix = None
phaserdag.node.clash_worst = 0
phaserdag.node.special_position_warning = False
phaserdag.node.llg = 0
phaserdag.node.zscore = 0
phaserdag.node.rfactor = 0
phaserdag.node.information_content = 0
phaserdag.node.expected_information_content = 0
phaserdag.node.sad_information_content = 0
phaserdag.node.sad_expected_llg = 0
phaserdag.node.parents = None
phaserdag.node.rellg = 0
phaserdag.node.data_scale = 1
phaserdag.node.hklin {
 tag = "toxd"
 identifier = 7882652528575457769
}
'''
  cards =  phil_to_cards(user_phil_str = user_params,ignore_params=[],program_name="InputCard")
  print("nodes:")
  print(cards)

  user_params = '''
phasertng.anisotropy.beta = -0.0001408570982 0.0004448552612 0.0001771374106 -8.606961904e-20 -1.437636302e-19 -2.731111732e-19
phasertng.anisotropy.delta_bfactor = 2.860072362
phasertng.anisotropy.direction_cosines = 0 1 0 0 0 1 1 0 0
phasertng.dag.cards.filename = "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.cards"
phasertng.dag.phil.filename = "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.phil"
phasertng.data.number_of_reflections = 3161
phasertng.data.resolution_available = 2.300205241 36.79100037
phasertng.data.unitcell = 73.58200073 38.73300171 23.18899918 90 90 90
phasertng.hklin.filename = "../../tutorial/data/toxd/toxd.mtz"
phasertng.hklin.identifier = 15968090302689210158
phasertng.hklin.tag = "toxd"
phasertng.labin.aniso = ANISO
phasertng.labin.fnat = FTOXD3
phasertng.suite.database = "/tmp/PHASERTNG/toxd"
phasertng.suite.level = logfile
phasertng.tncs.order = 1
phasertng.tncs.parity = 0 0 0
phasertng.tncs.unique = False
phasertng.tncs.vector = 0 0 0
phasertng.tncs_analysis.indicated = False
phasertng.tncs_analysis.top_non_origin = 0
phasertng.tncs_analysis.result {
 order = 1
 vector = 0 0 0
 parity = 0 0 0
 unique = False
}
'''
  cards =  phil_to_cards(user_phil_str = user_params,program_name="InputCard")
  print("cards:")
  print(cards)

  user_params = '''
phasertng {
anisotropy.beta = -0.0001408570982 0.0004448552612 0.0001771374106 -8.606961904e-20 -1.437636302e-19 -2.731111732e-19
anisotropy.delta_bfactor = 2.860072362
anisotropy.direction_cosines = 0 1 0 0 0 1 1 0 0
dag.cards.filename = "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.cards"
dag.phil.filename = "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.phil"
data.number_of_reflections = 3161
data.resolution_available = 2.300205241 36.79100037
data.unitcell = 73.58200073 38.73300171 23.18899918 90 90 90
hklin.filename = "../../tutorial/data/toxd/toxd.mtz"
hklin.identifier = 15968090302689210158
hklin.tag = "toxd"
labin.aniso = ANISO
labin.fnat = FTOXD3
suite.database = "/tmp/PHASERTNG/toxd"
suite.level = logfile
tncs.order = 1
tncs.parity = 0 0 0
tncs.unique = False
tncs.vector = 0 0 0
tncs_analysis.indicated = False
tncs_analysis.top_non_origin = 0
tncs_analysis.result {
 order = 1
 vector = 0 0 0
 parity = 0 0 0
 unique = False
}
}
'''
  cards =  phil_to_cards(user_phil_str = user_params,ignore_params=[],program_name="InputCard")
  print("cards:")
  print(cards)
