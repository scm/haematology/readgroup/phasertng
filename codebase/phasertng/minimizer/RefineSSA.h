//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineSSA_class__
#define __phasertng_RefineSSA_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phasertng/math/likelihood/pNormal.h>
#include <phasertng/math/likelihood/pLogNormal.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <complex>
typedef std::complex<double> cmplex;
typedef std::vector<cmplex> sv_cmplex;

namespace phasertng {

class RefineSSA : public dtmin::RefineBase
{
  public:
    double nascat; // # of fully-occupied anom scatterers needed for covar
    double dBscat; // Difference from Wilson B of average anomalous scatterer
    bool nascat_set; // Was nascat set explicitly?
    double fraction_weak = 0; //logging
    double high_resolution = 0; //logging
    double rmszrhoFF = 0;

  public:
    double SIGMA_ATOMS = 0;
    double SIGMA_RHOPM = 0;
    bool   NOT_CONSISTENT = false;
    bool   NO_SIGNAL = false;

  private: //members
    dag::Node*           NODE;
    int    NBINS = 0;
    bool   REFINE_RHOFF = false;
    bool   REFINE_RHOPM = false;
    bool   FIRST_CALL = true;
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;

    //Composition-based covariance
    sv_cmplex ascat_bin; // Symmetry & WilsonB-weighted scattering factor for major anomalous scatterer

    std::string ANOMTYPE; // Identity of strongest anomalous scatterer

    TargetGradientHessianType
    parallel_combined_tgh(
      bool, //gradient
      bool, //hessian
      int, //beginning
      int//end
    );

    TargetGradientHessianType
    combined_tgh( bool, bool, bool);

  public: //members
    const_ReflectionsPtr REFLECTIONS;
    SelectedPtr          SELECTED;
    ScatterersPtr        SCATTERERS;
    sv_double            sca_null;
    //DagDatabasePtr       DAGDB;

  public:  //constructor
    RefineSSA() : dtmin::RefineBase() {}
    RefineSSA(
        const_ReflectionsPtr,
        SelectedPtr,
        ScatterersPtr,
        double,double);
      //  DagDatabasePtr);
    ~RefineSSA() throw() {}

  public:
    void init_node(dag::Node*);

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string reject_outliers();
    af_string cleanup_parameters();
    af_string finalize_parameters();

  public:
    af_string logSigmaA(std::string);
    double absrhoFFfromNanom(double,cmplex,cmplex,double);
    double nAnomMax(double,cmplex,cmplex);
    std::pair<double,double> nAnomFromAbsrhoFF(double,double,cmplex,cmplex);

  private:
    double likelihood();

  private:
    //reflectionwise arrays to be calculated in parallel_combined_tgh and
    //binned in combined_tgh
    sv_double dLL_by_dabsrhoFF;
    sv_double d2LL_by_dabsrhoFF2;
    sv_double dLL_by_drhopm;
    sv_double d2LL_by_drhopm2;
    sv_double d2LL_by_dabsrhoFF_by_drhopm;

  public:
    void set_threading(bool,int);
};

} //phasertng
#endif
