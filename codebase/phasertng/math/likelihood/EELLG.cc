#include <phasertng/math/likelihood/EELLG.h>

namespace phasertng {
namespace likelihood {

  double EELLG::get(double SigaSqr)
  {
    return (SigaSqr*SigaSqr)/2.;
  }
}
}
