//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved/
// some vector operations
#ifndef __phasertng_vec_ops__
#define __phasertng_vec_ops__
#include <vector>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace math
{

template <typename T>
std::vector<T> vec_plus(const std::vector<T>& A, const std::vector<T>& B)
{
  int N = A.size();
  phaser_assert( N == B.size() );

  std::vector<T> tmp(N);

  for (int i=0; i<N; i++)
    tmp[i] = A[i] + B[i];

  return tmp;
} // vec_plus

template <typename T>
std::vector<T> vec_minus(const std::vector<T>& A, const std::vector<T>& B)
{
  int N = A.size();
  phaser_assert(N == B.size());

  std::vector<T> tmp(N);

  for (int i=0; i<N; i++)
    tmp[i] = A[i] - B[i];

  return tmp;
} // vec_minus

template <typename T>
std::vector<T> vec_mult(const std::vector<T>& A, const std::vector<T>& B)
{
  // Element wise vector multiplication of two vectors of the same size
  int N = A.size();
  phaser_assert(N == B.size());

  std::vector<T> tmp(N);

  for (int i=0; i<N; i++)
    tmp[i] = A[i] * B[i];

  return tmp;
} // vec_mult

template <typename T>
T dot_prod(const std::vector<T>& A, const std::vector<T>& B)
{
  int N = A.size();
  phaser_assert(N == B.size());

  T sum = 0;
  for (int i=0; i<N; i++)
    sum += A[i] * B[i];

  return sum;
} // dot_prod

template <typename T>
bool IsVecZero(const std::vector<T>& v)
{
  bool is_zero = true;
  for (int i=0; i<v.size(); i++)
  {
    if (std::fabs(v[i]) > 1E-10)
    {
      is_zero = false;
      break;
    }
  }
  return is_zero;
} // isVecZero

}} // namespace math
#endif
