//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/llgi/function.h>
#include <scitbx/math/bessel.h>
#include <phasertng/main/Assert.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/alogchI0.h>
#include <phasertng/math/table/alogch.h>

namespace phasertng {
extern const table::alogchI0& tbl_alogchI0;
extern const table::alogch& tbl_alogch;
}

typedef std::complex<double> cmplex;
using namespace scitbx;

namespace phasertng { namespace likelihood { namespace llgi {

  void
  function::target_gradient_hessian(
      const double& EOBS,
      const cmplex& DobsSigaEcalc, //this sums individual SigaEcalc from components
      const double& teps,   //tNCS epsilon
      const double& V_,
      const bool&   CENT,
      const likelihood::llgi::tgh_flags& flags
    )
  {
    LL = 0;
    dLL_by_dReEC = 0;
    dLL_by_dImEC = 0;
    dLL_by_dV = 0;
    d2LL_by_dReEC2 = 0;
    d2LL_by_dImEC2 = 0;
    d2LL_by_dReEC_dImEC = 0;
    d2LL_by_dReEC_dV = 0;
    d2LL_by_dImEC_dV = 0;
    d2LL_by_dV2 = 0;
    V = V_;

    if (V <= 0) {
      return; // V will be negative and function::negative_variance() will be false
    }

    double EOBS2 = fn::pow2(EOBS);
    double EC = std::abs(DobsSigaEcalc);
    double EC2 = fn::pow2(EC);
    double X = 2.0*EOBS*EC/V;
    //In following, note that V=teps-DobsSigaSqr, so DobsSigaSqr=teps-V
    //target
    //double wll = std::log(teps)+(EOBS2/teps);
    double wll = (EOBS2/teps); //speed, use divisor below log(a/b) instead of second log
           wll = CENT ? wll/2.0 : wll;
    LL = -(std::log(V/teps)+(EOBS2 + EC2)/V);
    if (CENT)
    {
      X /= 2.0;
      LL = LL/2.0 + tbl_alogch.get(X);
    }
    else LL += tbl_alogchI0.get(X); //acentric
    LL += wll;
    //gradient
    if (flags.gradient())
    {
     //derivatives
      double bessTerm = CENT ? std::tanh(X) : scitbx::math::bessel::i1_over_i0(X);
      phaser_assert(std::abs(X) < 710.5); // limit for function
      double bessTerm2 = CENT ? fn::pow2(1/std::cosh(X)) : fn::pow2(bessTerm);
      // NB: sech = 1/cosh
      double ReEC = DobsSigaEcalc.real();
      double ImEC = DobsSigaEcalc.imag();
      //always calculate EC terms, needed for all
      double dEC_by_dReEC = 0;
      double dEC_by_dImEC = 0;
      double V2 = fn::pow2(V);
      if (EC > 0)
      {
        dEC_by_dReEC = ReEC/EC;
        dEC_by_dImEC = ImEC/EC;
      }
      double dLL_by_dEC = CENT ? (1/V)*(EOBS*bessTerm - EC) : (2/V)*(EOBS*bessTerm - EC);
      dLL_by_dReEC = dLL_by_dEC*dEC_by_dReEC;
      dLL_by_dImEC = dLL_by_dEC*dEC_by_dImEC;
      //numerical limits mean that V > 0 does not imply V2 > 0
      if (flags.variances() and V2 > 0)
      {
        dLL_by_dV = CENT ?
                (EOBS2 + EC2 - V - 2*EOBS*EC*bessTerm)/(2*V2) :
                (EOBS2 + EC2 - V - 2*EOBS*EC*bessTerm)/V2;
      }
      if (flags.hessian())
      {
        double V3 = fn::pow3(V);
        double V4 = fn::pow4(V);
        double EC3 = EC * EC2;
        double EC4 = EC * EC3;
        if (EC2 > 0 && EC3 > 0 && EC4 > 0)
        {
          double ReEC2 = ReEC * ReEC;
          double ImEC2 = ImEC * ImEC;
          double ReECImEC = ReEC * ImEC;
          if (CENT)
          {
            double commonTerm1 = bessTerm2*EOBS2/EC2/V2;
            double commonTerm2 = -bessTerm*EC*EOBS/EC4/V;
            double commonTerm3 = bessTerm*EC*EOBS/EC2/V - 1/V;
            d2LL_by_dReEC2 = commonTerm1*ReEC2 + commonTerm2*ReEC2 + commonTerm3;
            d2LL_by_dImEC2 = commonTerm1*ImEC2 + commonTerm2*ImEC2 + commonTerm3;
            d2LL_by_dReEC_dImEC = commonTerm1*ReECImEC + commonTerm2*ReECImEC;
          }
          else
          {
            double denom = EC4*V2;
            double commonTerm5 = -2*EC2*(EC2-bessTerm*EC*EOBS)*V;
            double commonTerm6 = -4*EOBS*((-1 + bessTerm2)*EC2*EOBS+bessTerm*EC*V);
            d2LL_by_dReEC2 = (commonTerm5 + commonTerm6*ReEC2)/denom;
            d2LL_by_dImEC2 = (commonTerm5 + commonTerm6*ImEC2)/denom;
            d2LL_by_dReEC_dImEC = commonTerm6*ReECImEC/denom;
          }
        }
        if (flags.variances() and V3 > 0 and V4 > 0)
        {
          d2LL_by_dV2 = CENT ?
              (2*bessTerm2*EC2*EOBS2 + V*(-2*EC2 + 4*bessTerm*EC*EOBS - 2*EOBS2 + V))/(2*V4) :
              (4*EOBS2*EC2/V4 - 2*(EC2+EOBS2)/V3 + 1/V2 + 2*EC*EOBS*bessTerm/V3 - 4*EOBS2*EC2*bessTerm2/V4);
          double d2LL_by_dV_dEC = CENT ?
              EC/V2 - EC*EOBS2*bessTerm2/V3 - EOBS*bessTerm/V2 :
              2*EC/V2 - 4*EC*EOBS2*(1.-bessTerm2)/V3;
          d2LL_by_dReEC_dV = d2LL_by_dV_dEC * dEC_by_dReEC; //ReEC/EC
          d2LL_by_dImEC_dV = d2LL_by_dV_dEC * dEC_by_dImEC; //ImEC/EC
        }
      }
    }
  }

}}} //phasertng
