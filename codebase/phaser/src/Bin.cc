//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Bin.h>
#include <phasertng/main/Error.h>
#include <math.h>

namespace phaser {

Bin::Bin(data_bins DAT) : data_bins(DAT)
{
  SMAX = SMIN = SDELTA = NUMBINS = FnSShell = 0;
}

bool Bin::not_setup()
{ return data_bins::not_setup() && !SMAX && !SMIN && !SDELTA && !NUMBINS && !FnSShell; }

//initialization functions
void Bin::setup(double reso1,double reso2,int nrefl)
{
  assert(MINBINS >= 0);
  assert(MAXBINS > 0);
  assert(WIDTH > 0);
  double hires = std::min(reso1,reso2);
  double lores = std::max(reso1,reso2);
  SMAX = (hires>0) ? 1/hires : 1./9999;
  SMIN = (lores>0) ? 1/lores : 1./10000;
  assert(nrefl);
  if (MINBINS == MAXBINS)
    NUMBINS = MINBINS;
  else
    NUMBINS = std::max(MINBINS,std::min((nrefl/WIDTH),MAXBINS));
  assert(NUMBINS > 0);
  SDELTA = (SMAX-SMIN)/NUMBINS;
  FnSShell = CUBIC.FnSShell(SMIN,SMAX,NUMBINS);
}

//set functions
void Bin::set_minbins(int m)
{ assert(m > 0); MINBINS = m; MAXBINS = std::max(MINBINS,MAXBINS); }

void Bin::set_maxbins(int m)
{ assert(m > 0); MAXBINS = m; MINBINS = std::min(MAXBINS,MINBINS); }

void Bin::set_width(int w)
{ WIDTH = w; assert(WIDTH > 1); }

bool Bin::set_numbins(int nbins)
{
  assert(nbins > 0);
  NUMBINS = nbins;
  SDELTA = (SMAX-SMIN)/NUMBINS;
  FnSShell = CUBIC.FnSShell(SMIN,SMAX,NUMBINS);
  return (NUMBINS >= MINBINS && NUMBINS <= MAXBINS);
}

void Bin::set_cubic(double A, double B, double C)
{
  CUBIC = Cubic(A,B,C);
  FnSShell = CUBIC.FnSShell(SMIN,SMAX,NUMBINS);
  assert(Cubic(A,B,C).is_valid());
}

double Bin::LoRes(double ibin)
{ return 1/CUBIC.InvFnS(ibin*FnSShell + CUBIC.FnS(SMIN)); }

float1D Bin::LoRes_array()
{
  float1D array;
  for (int ibin = 0; ibin < numbins(); ibin++)
    array.push_back(LoRes(ibin));
  return array;
}

double Bin::HiRes(double ibin)
{ return 1/CUBIC.InvFnS((ibin+1)*FnSShell + CUBIC.FnS(SMIN)); }

double Bin::MidRes(double ibin)
{
// the middle of the resolution range is the weighted mean
  double loS = 1/LoRes(ibin);
  double hiS = 1/HiRes(ibin);
  if (CUBIC.is_default())
    return 1/(sqrt((loS*loS+hiS*hiS)/2));
  double wtAverageS = CUBIC.InvFnS((CUBIC.FnS(loS) + CUBIC.FnS(hiS))/2);
  return 1/wtAverageS;
}

unsigned Bin::get_bin(double s)
{
  assert(FnSShell);
  int rbin((CUBIC.FnS(s) - CUBIC.FnS(SMIN))/FnSShell);
  if (rbin < 0) return 0;
  if (rbin >= NUMBINS) return NUMBINS-1;
  return rbin;
}

}
