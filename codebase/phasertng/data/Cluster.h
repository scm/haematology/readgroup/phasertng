#ifndef __phasertng_data_Cluster_class__
#define __phasertng_data_Cluster_class__
#include <phasertng/io/FileSystem.h>
#include <vector>
#include <utility>
#include <string>
#include <tuple>
#include <map>
#include <scitbx/vec3.h>
#include <cctbx/eltbx/xray_scattering/gaussian.h>

typedef scitbx::vec3<double> dvect3;

namespace phasertng {

class Cluster : public FileSystem
{
  struct structure_t { std::string element; dvect3 site; double twopi_ra; };
  public:
  std::vector<structure_t> structure;
  std::map<std::string,cctbx::eltbx::xray_scattering::gaussian> ff;
  bool        is_not_cluster = true;

  public:
    Cluster(std::string xx = "",std::string filename="") : FileSystem(filename) {
      if (xx == "SS") setup_disulphide();
      if (xx == "TX") setup_ta6br12();
      if (xx == "XX") ReadPdb();//filesystem
      //else default is not a cluster
      //or could read a file for XX
      set_twopi_ra_and_ff();
    }
    void set_twopi_ra_and_ff();
    void ReadPdb();

  public:
  std::map<std::string,int> composition() const;
  std::vector<std::tuple<std::string,std::string,double> > twopi_ra_rb() const;
  double vanderwaals() const;
  double debye(double);
  double scattering(double);
  void setup_ta6br12();
  void setup_disulphide();
};

} //phasertng
#endif
