//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
//
// The base class for any refine object to be refined by the MInimizer class
//
// Note that all vectors and matrices that the member functions
// deal with have dimensions N and N x N
//
// jargon:
// refine: to change a parameter to  the target function (e.g.
//   refined pars are those that are being refined in the macrocyle)

#ifndef __phasertng_mintbx_RefineBase_class__
#define __phasertng_mintbx_RefineBase_class__
#include <phasertng/dtmin/Compulsory.h>
#include <phasertng/dtmin/Optional.h>
#include <phasertng/dtmin/Logging.h>
#include <tuple>

namespace phasertng {
namespace dtmin {

//this is an abstract base class by virtue of inheritance from Compulsory, which is abstact

class RefineBase : public Compulsory, public Optional, public Logging
{
  public:
    RefineBase() { refine_parameter_.clear(); }
    virtual ~RefineBase() {}

  public:
    HessianType
    previous_h_inverse_approximation_;// for bfgs

  public:
    sv_double
    get_reparameterized_parameters();

    void
    set_reparameterized_parameters(sv_double);

    sv_double
    reparameterized_large_shifts();

    std::vector<dtmin::Bounds>
    reparameterized_bounds();

    // reparameterizeedf() is nonsensical
    TargetGradientType
    reparameterized_target_gradient();

    // used by studyParams where we don't want to fix the hessian
    TargetGradientHessianType
    reparameterized_target_gradient_hessian();

    // used by everything else, (the hessian has been 'adjusted' to remove -ve diagonal terms)
    std::pair<TargetGradientHessianType,bool>
    reparameterized_adjusted_target_gradient_hessian();

    sv_double
    damped_shift(double,sv_double,sv_double,sv_double,sv_double);

    //debugging
    void   study_parameters();

    std::pair<TargetType,GradientType>
    finite_difference_gradient(double);

    std::pair<double,HessianType>
    finite_difference_hessian_by_gradient(double,bool);

    std::pair<double,HessianType>
    finite_difference_curvatures_by_gradient(double,bool);

    std::pair<double,HessianType>
    finite_difference_hessian_by_function(double,bool);

    std::pair<double,HessianType>
    finite_difference_curvature_by_function(double,bool);

    std::string to_sci_format(const double);

    TargetGradientHessianType
    local_linear_restraint(std::vector<double>, std::vector<double>,
                           dtmin::Bounds, double, DiagonalType);

  public:
    std::pair<double, sv_double> maximum_distance(sv_double, sv_double);

  protected:
    // Eliminate non-positive curvatures from matrix. Return true if any curvatures were non-positive.
    std::pair<HessianType, bool>
    adjust_hessian(versa_flex_double);
    std::tuple<double,double,double>
    adaptive_restraint_infinite(double,double,double,double);
};

}} //phasertng

#endif
