import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError,SpaceGroup
from phasertng import enum_out_stream,enum_other_data
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.scripts.picard_jiffies import *
from phasertng.voyager.picard import polish
from phasertng.run.ModeRunner import *
import argparse

def riker(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_riker)

def inner_riker(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  debug = False
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="riker")
    initparams = initparams +  "\n" +  build_auto_str("riker")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    #master_phil.show() #libtbx.phil.scope object
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    if dbdir is None:
      print("")
      raise ValueError(enum_err_code.input,"No dag cards database")

    #phil_to_cards selects out only the phasertng cards with the program name
  # working_params.phasertng.reflections.read_anomalous = False
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    #can ignore dryrun above as it doesn't make a difference

    loop_auto = str("Riker")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout):
      os.remove(xmlout)

    ModeRunner(basetng=basetng,mode_list=['chk'])

    #first check that there are some files, or else we don't fail until auto_search called
    #nb auto search calls this function again internally
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,"",ignore_unknown_keys)
    DAGDB = basetng.DAGDATABASE

    walk = ModeRunner(basetng=basetng,mode_list=['walk']).solution()

    #first choice
    hklin = working_params.riker.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) == 1:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is None:
      raise ValueError(enum_err_code.fatal,"No reflections")

    #first choice
    seqin = working_params.riker.seqin
    if seqin is None:
      #second choice is phasertng
      seqin = isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
    if seqin is None and int(basetng.input.size(".phasertng.file_discovery.validated.seq_files.")) == 1:
      #third choice is discovery
      seqin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.seq_files.",0)
  # if seqin is None:
  #   raise ValueError(enum_err_code.fatal,"No biological unit sequence")

    #this is the fixed model which will go into put_solution
    if working_params.riker.fixed is None:
      if int(basetng.input.size(".phasertng.file_discovery.validated.pdb_files.")) == 1:
        working_params.riker.fixed  = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",0)

    if working_params.riker.fixed is None:
      raise ValueError(enum_err_code.fatal,"No fixed model")

    #must input any additional pdb files directly because we need the order
    found_pdb = working_params.riker.xyzin

    #show bub for information only
    basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))
    if seqin is not None:
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    basetng.input.resize(".phasertng.biological_unit_builder.model.",len(found_pdb)+1)
    basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(working_params.riker.fixed),int(0))
    for i,found in enumerate(found_pdb):
      basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(found),int(i+1))
    ModeRunner(basetng=basetng,mode_list=['bub'])

    basetng.load_moderunner(walk,user_cards,"")
    basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))
    basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    aniso1 = ModeRunner(basetng=basetng,mode_list=['imtz','data','sgx','aniso']).solution()
    tfzobj = tfzscore() #does not depend on the input, values from research

    entries_dagcards = ""
   #{{{
    #generate ensembles
    #ensembles has to be done after the aniso, for the scaling
    loop_ens = "Generate models"

    basetng.logLoopOpen(enum_out_stream.process,loop_ens,len(found_pdb)+1,"")
    fixed_pdb = working_params.riker.fixed
    #we can just change the pathway tracker because pathlog carries the logfile info
    basetng.logLoopNext(enum_out_stream.process,loop_ens,os.path.basename(fixed_pdb))
    basetng.input.set_as_str(".phasertng.model.filename.",fixed_pdb)
    ModeRunner(basetng=basetng,mode_list=['esm'])
    fixed_tag = basetng.input.get_as_str(".phasertng.search.tag.")
    fixed_ide = basetng.input.get_as_str(".phasertng.search.id.")
    entries_dagcards = entries_dagcards + basetng.input.generic_string + node_separator
    basetng.load_moderunner(aniso1,user_cards,"")

    for i,found in enumerate(found_pdb):
      basetng.logLoopNext(enum_out_stream.process,loop_ens,os.path.basename(found))
     #ifile = os.path.join(found)
      ifile = found
      basetng.input.set_as_str(".phasertng.model.filename.",ifile)
      ModeRunner(basetng=basetng,mode_list=['esm'])
      entries_dagcards = entries_dagcards + basetng.input.generic_string + node_separator
      tag = basetng.input.get_as_str(".phasertng.search.tag.")
      ide = basetng.input.get_as_str(".phasertng.search.id.")
      found_pdb[i] = { "ifile": found, "tag" : tag, "ide":ide}
      basetng.load_moderunner(aniso1,user_cards,"")
    basetng.logLoopShut(enum_out_stream.process,loop_ens)
   #}}}

    if isNone(basetng.input.get_as_str(".phasertng.biological_unit.model.filename.")) is None:
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))
    ModeRunner(basetng=basetng,mode_list=['tncso','tncs','feff','cca','ccs'])
    #we add the holds to the nodes here after the cca
    #put the ensembles in the database
    basetng.input.generic_int = ord('A') #0
    basetng.input.generic_string = entries_dagcards
    ModeRunner(basetng=basetng,mode_list=['join'])
    basetng.input.set_as_str(".phasertng.search.tag.",str(fixed_tag))
    basetng.input.set_as_str(".phasertng.search.id.",str(fixed_ide))
    basetng.input.set_as_str(".phasertng.put_solution.clear.",str(True))
    #no move because not separate chains
    refpoint = ModeRunner(basetng=basetng,mode_list=['put','rbr','rbm']).solution()
    strR = str(round(DAGDB.NODES.LIST[0].LLG))
    basetng.logLoopInfo(enum_out_stream.process,"LLG after rigid body refinement = " + strR)
    rundict = dict()
    software = working_params.riker.control.xref_software if working_params.riker.control.use_xref else ['phenix']
    for selection in ['refmac','phenix']:
   #{{{
      if selection in software:
     #{{{
        #branches from last refpoint
        basetng.load_moderunner(refpoint,user_cards,"")
        basetng.input.set_as_str(".phasertng.refinement.method.",selection)
        hires = float(basetng.input.get_as_str(".phasertng.notifications.hires."))
        basetng.input.set_as_str(".phasertng.refinement.resolution.",str(hires))
        if working_params.riker.control.use_xref:
          ModeRunner(basetng=basetng,mode_list=['xref'])
        else:
          ModeRunner(basetng=basetng,mode_list=['rfac'])
        strR = "%.2f" % round(DAGDB.NODES.LIST[0].RFACTOR,2)
        basetng.logLoopInfo(enum_out_stream.process,"R-factor after refinement = " + strR)
        rundict[selection] = strR
        n_first = len(found_pdb)
        loop_poses1 = "Find Additional Poses"
        basetng.logLoopOpen(enum_out_stream.process,loop_poses1,n_first,"Expected number of additional poses="+str(n_first))
        for i,found in enumerate(found_pdb):
       #{{{
          ifile = found["ifile"]
          tag = found["tag"]
          ide = found["ide"]
          basetng.logLoopNext(enum_out_stream.process,loop_poses1,"Pose #" + str(DAGDB.NODES.number_of_poses()+1))
          basetng.input.set_as_str(".phasertng.search.tag.",str(tag))
          basetng.input.set_as_str(".phasertng.search.id.",str(ide))
        # ModeRunner(basetng=basetng,mode_list=['find'])
          first = ModeRunner(basetng=basetng,mode_list=['frf','frfr','gyre','ftf','ftfr','pose','pak']).solution()
          if (int(DAGDB.size()) > 0):
         #{{{
         #  first_reached_tfz = False
            first_reached_tfz = tfzobj.reached_tfz(first)
            if first_reached_tfz: #do few cycles with vrms
              basetng.logLoopInfo(enum_out_stream.process,"High TFZ but no contrast in base; quick refinement including vrms")
              basetng.input.set_as_str(".phasertng.macrbr.ncyc.","5 0 0")
              basetng.input.set_as_def(".phasertng.macrbr.macrocycle1.")
            else: #don't refine vrms
              basetng.logLoopInfo(enum_out_stream.process,"Low TFZ and no contrast in base; quick refinement excluding vrms")
              basetng.input.set_as_str(".phasertng.macrbr.ncyc.","5 0 0")
              basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.","rotn tran")
            first = ModeRunner(basetng=basetng,mode_list=['rbr','tfz']).solution()
            first_reached_tfz = tfzobj.reached_tfz(first)
            #now if it has reached good values
            if first_reached_tfz:
                basetng.logLoopInfo(enum_out_stream.process,msg + "polishing")
                basetng,results,first = \
                  polish(basetng,results,working_params,user_cards,"",progress_callback)
            else:
              basetng.logLoopInfo(enum_out_stream.process,"No TFZ signal in base after rbr")
            basetng = store_mrsolution(first,current_seeks,basetng)
         #}}}
         #branch from first, in case of a side trip
          basetng.load_moderunner(first,user_cards,"")
       #}}}
        basetng.logLoopShut(enum_out_stream.process,loop_poses1)
     #}}}
   #}}}

    for k,v in rundict.items():
      basetng.logLoopInfo(enum_out_stream.process,"R-factor after wide search and " + k + " refinement = " + v)
    basetng.logLoopShut(enum_out_stream.process,loop_auto)

    ModeRunner(basetng=basetng,mode_list=['tree'])
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="directory path walked for data files",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is None and args.output is None):
    print("At least one of either --phil or --output must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  verbose = False
  if (args.input is not None):
    if not (os.path.exists(args.input)):
      print("Directory cannot be opened",str(os.path.exists(args.input)))
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndirectory=\"" + str(args.input)+"\"\n"
  if (args.output is not None):
    if not (os.path.exists(args.output)):
      print("Directory cannot be opened",str(os.path.exists(args.input)))
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndatabase=\"" + str(args.input)+"\"\n"
  if (args.phil is not None or (args.input is not None and args.output is not None)):
    riker( user_phil_str=user_phil_str)
  else:
    print("command line arguments insufficient",flush=True)
    sys.exit(0)
