//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_vector_class__
#define __phasertng_type_flt_vector_class__
#include <phasertng/type/optional.h>
#include <scitbx/vec3.h>

namespace phasertng {
namespace type {
typedef scitbx::vec3<double>                dvect3;

class flt_vector : public optional
{
  dvect3  dephalt;
  dvect3  uvalue;
  bool    pminset = false;
  bool    pmaxset = false;
  double  pminval = 0.;
  double  pmaxval = 0.;

  public:
  const bool& minset() const { return pminset; }
  const double&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const double&  maxval() const { return pmaxval; }

  flt_vector() : optional() {}
  flt_vector(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const dvect3 dephalt_ = { DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG }
  );

  void set_value(dvect3 other);
  void set_value(double a, double b, double c);
  void set_value(int i, double a);
  void set_value(const sv_string& vcards,const dvect3& other);
  bool is_default() const;
  void set_default();
  dvect3 value_or_default() const;
  double value_or_default(int i) const;
  dvect3 value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::flt_vector& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
