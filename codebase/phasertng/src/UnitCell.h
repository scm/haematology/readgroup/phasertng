//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_UnitCell_class__
#define __phasertng_UnitCell_class__
#include <phasertng/main/includes.h>
#include <cctbx/uctbx.h>

namespace phasertng {

  class UnitCell
  {
    private:
      dvect3 original_orthogonal_cell = {0,0,0}; //flag undefined value

    public: //members
      cctbx::uctbx::unit_cell cctbxUC;

    public: //getters
      const dmat33& fractionalization_matrix() const { return cctbxUC.fractionalization_matrix(); }
      const dmat33& orthogonalization_matrix() const { return cctbxUC.orthogonalization_matrix(); }
      const af::double6 get_cell() const { return cctbxUC.parameters(); }
      const dvect3 GetBox() const { return dvect3(A(),B(),C()); }
      const double& A() const { return cctbxUC.parameters()[0]; }
      const double& B() const { return cctbxUC.parameters()[1]; }
      const double& C() const { return cctbxUC.parameters()[2]; }
      const double& Alpha() const { return cctbxUC.parameters()[3]; }
      const double& Beta() const  { return cctbxUC.parameters()[4]; }
      const double& Gamma() const { return cctbxUC.parameters()[5]; }

    public: //constructors
      UnitCell(const af::double6 = af::double6(1,1,1,90,90,90));
      UnitCell(const cctbx::uctbx::unit_cell);
      UnitCell(const dvect3);
      virtual ~UnitCell(){}

    public:
      double aStar() const;
      double bStar() const;
      double cStar() const;
      double cosAlpha() const;
      double cosBeta() const;
      double cosGamma() const;
      double cosAlphaStar() const;
      double cosBetaStar() const;
      double cosGammaStar() const;
      dvect3 HKLtoVecS(const int,const int,const int) const;
      bool is_default() const;
      bool is_orthogonal() const;
      double extend_resolution(double,std::pair<double,double>) const;
      double limit_resolution(double,std::pair<double,double>) const;
      std::pair<double,cctbx::uctbx::unit_cell>
         gridding(double,std::pair<double,double>,bool,double);
      void   scale_orthogonal_cell(double);
      void   reset_orthogonal_cell();
      bool   is_orthogonal();
      double resolution(double,std::pair<double,double>);
      dmat33 orth_Rtr_frac(dmat33) const;
      double maximum_gfunction_radius() const { return std::min(std::min(A(),B()),C())/2.; }
      double maximum_cell_dimension() const { return std::max(std::max(A(),B()),C()); }
      double minimum_cell_dimension() const { return std::min(std::min(A(),B()),C()); }

    public: //functions returning output
      af_string build_minimum_unit_cell(double);
      af_string build_variance_unit_cell(double,double);
      af_string build_decomposition_unit_cell(double,double);
      af_string build_interpolation_unit_cell(double,double);
      std::string str(std::string="") const;
      int width() const;
  };

} //phasertng
#endif
