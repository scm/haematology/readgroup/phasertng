//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_typeuuid_includes__
#define __phasertng_typeuuid_includes__
#include <vector>
#include <cstddef>
#include <cstdint>
#include <iso646.h>

namespace phasertng {

//ullint type is needed since input from python can be unlimited precision
typedef unsigned long long int  ullint; //unsigned uint64_t minimum, possibly bigger
//typedef uint64_t uuid_t; //20 digits
typedef uint32_t uuid_t; //10 digits
//constexpr int uuid_w = 20; //width of the uuid_t
constexpr int uuid_w = 10; //width of the uuid_t
typedef std::vector<uuid_t> sv_uuid;

} //phasertng
#endif
