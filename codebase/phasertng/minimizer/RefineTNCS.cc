//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/minimizer/RefineTNCS.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/main/jiffy.h>
#include <future>

namespace phasertng {

  RefineTNCS::RefineTNCS(
      const_ReflectionsPtr  REFLECTIONS_,
      SelectedPtr  SELECTED_,
      EpsilonPtr   EPSILON_)
    : RefineBase(),
      REFLECTIONS(REFLECTIONS_),
      SELECTED(SELECTED_),
      EPSILON(EPSILON_)
  {
    phaser_assert(REFLECTIONS->has_col(labin::INAT));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    //these may need to be altered as user information may not be complete e.g gfnrad
    //also have to check these are within bounds, in the case of the minimizer
    INIT_ORDER = EPSILON->TNCS_ORDER;
    INIT_VECTOR = EPSILON->TNCS_VECTOR;
    {//put gfnrad within bounds
    INIT_GFNRAD = EPSILON->TNCS_GFNRAD;
    if (INIT_GFNRAD < EPSILON->MIN_GFNRAD) INIT_GFNRAD = EPSILON->MIN_GFNRAD+DEF_PPM;
    if (INIT_GFNRAD > EPSILON->MAX_GFNRAD) INIT_GFNRAD = EPSILON->MAX_GFNRAD-DEF_PPM;
    EPSILON->MIN_GFNRAD = std::max(EPSILON->MIN_GFNRAD,0.5*INIT_GFNRAD);
    EPSILON->MAX_GFNRAD = std::min(EPSILON->MAX_GFNRAD,1.5*INIT_GFNRAD);
    EPSILON->TNCS_GFNRAD = INIT_GFNRAD;
    }
    {//check rmsd in bounds
    INIT_RMS = EPSILON->TNCS_RMS;
    PHASER_ASSERT(INIT_RMS >= EPSILON->MIN_RMS);
    PHASER_ASSERT(INIT_RMS <= EPSILON->MAX_RMS);
    }
    {//clear tbin and start again regardless
    EPSILON->VARBINS.resize(REFLECTIONS->numbins(),1.0);
    arrayTEPS.resize(REFLECTIONS->NREFL,1.);
    }
    //this must be done afte rthe gfn rad has been set
    EPSILON->apply_spacegroup_unitcell(REFLECTIONS->SG,REFLECTIONS->UC);
    bool halfR(false);
    EPSILON->initialize(halfR);
    //don't store arrays
    reject_outliers();
    setup_wll();
  }

  void
  RefineTNCS::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineTNCS::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(stolower(item));
    EPSILON->REFINE_ROT = (protocol.find("rot") != protocol.end());
    EPSILON->REFINE_GFN = (protocol.find("gfn") != protocol.end());
    EPSILON->REFINE_TRA = (protocol.find("tra") != protocol.end());
    EPSILON->REFINE_RMSD = (protocol.find("rmsd") != protocol.end());
    EPSILON->REFINE_FS = (protocol.find("fs") != protocol.end());
    EPSILON->REFINE_BINS = (protocol.find("bins") != protocol.end());
    if (EPSILON->REFINE_BINS)
      EPSILON->REFINE_FS = false; //highly correlated, bounds are also correlated (getMaxDistSpecial)
    if (EPSILON->TNCS_ANGLE == dvect3(0,0,0))  //e.g. no perturbation in tncs order 2 case
      EPSILON->REFINE_ROT = false; //because it won't go anywhere anyway
    if (protocol.find("off") != protocol.end()) //overwrite
      EPSILON->REFINE_ROT = EPSILON->REFINE_GFN = EPSILON->REFINE_TRA = EPSILON->REFINE_RMSD = EPSILON->REFINE_FS = EPSILON->REFINE_BINS = false;

    refine_parameter_.clear();
    phaser_assert(refine_parameter_.size() == 0);
    for (int rot = 0; rot < 3; rot++)
      EPSILON->REFINE_ROT ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    EPSILON->REFINE_GFN ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    for (int tra = 0; tra < 3; tra++)
      EPSILON->REFINE_TRA ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    EPSILON->REFINE_RMSD ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    EPSILON->REFINE_FS ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      EPSILON->REFINE_BINS ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();
  }

  sv_string
  RefineTNCS::macrocycle_parameter_names()
  {
    sv_string macrocycle_parameter_names(nmp);
    int i(0),m(0);
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Rotation X";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Rotation Y";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Rotation Z";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Gfunction Radius";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Translation X";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Translation Y";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Translation Z";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Tncs Related RmsD";
    if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Tncs Related Fs";
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++]) macrocycle_parameter_names[i++] = "Bins Variance #" + std::to_string(bin+1);
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return macrocycle_parameter_names;
  }

  sv_double
  RefineTNCS::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    for (int rot = 0; rot < 3; rot ++)
      if (refine_parameter_[m++])
        pars[i++] = EPSILON->TNCS_ANGLE[rot];
    if (refine_parameter_[m++])
      pars[i++] = EPSILON->TNCS_GFNRAD;
    for (int tra = 0; tra < 3; tra ++)
      if (refine_parameter_[m++])
        pars[i++] = EPSILON->TNCS_VECTOR[tra];
    if (refine_parameter_[m++])
      pars[i++] = EPSILON->TNCS_RMS;
    if (refine_parameter_[m++])
      pars[i++] = EPSILON->TNCS_FS;
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++])
        pars[i++] = EPSILON->VARBINS[bin];
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineTNCS::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot++)
      if (refine_parameter_[m++])
        EPSILON->TNCS_ANGLE[rot] = newx[i++];
    if (refine_parameter_[m++])
      EPSILON->TNCS_GFNRAD = newx[i++];
    for (int tra = 0; tra < 3; tra++)
      if (refine_parameter_[m++])
        EPSILON->TNCS_VECTOR[tra] = newx[i++];
    if (refine_parameter_[m++])
      EPSILON->TNCS_RMS = newx[i++];
    if (refine_parameter_[m++])
      EPSILON->TNCS_FS = newx[i++];
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++])
        EPSILON->VARBINS[bin] = newx[i++];
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineTNCS::macrocycle_large_shifts()
  {
    sv_double largeShifts(nmp);
    double traShift(REFLECTIONS->data_hires()/64.);
    double rotShift(traShift/scitbx::deg_as_rad(EPSILON->TNCS_GFNRAD));
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot++)
      if (refine_parameter_[m++])
        largeShifts[i++] = rotShift/scitbx::deg_as_rad(INIT_GFNRAD); //ROT
    if (refine_parameter_[m++])
      largeShifts[i++] = 0.1*INIT_GFNRAD;//1.; //GFNRAD
    if (refine_parameter_[m++])
      largeShifts[i++] = traShift/REFLECTIONS->UC.A(); //TRA
    if (refine_parameter_[m++])
      largeShifts[i++] = traShift/REFLECTIONS->UC.B(); //TRA
    if (refine_parameter_[m++])
      largeShifts[i++] = traShift/REFLECTIONS->UC.C(); //TRA
    if (refine_parameter_[m++])
      largeShifts[i++] = 0.1; //RMS
    if (refine_parameter_[m++])
      largeShifts[i++] = 0.02; //FS
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++])
        largeShifts[i++] = 0.05; //BIN
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineTNCS::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    double minfs = min_fs();
    double maxfs = 1.;
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot ++)
      if (refine_parameter_[m++])
        bounds[i++].on(-20, 20); //ROT
    if (refine_parameter_[m++])
      bounds[i++].on(EPSILON->MIN_GFNRAD,EPSILON->MAX_GFNRAD);
    for (int tra = 0; tra < 3; tra ++)
      if (refine_parameter_[m++])
        bounds[i++].off(); //TRA
    if (refine_parameter_[m++])
      bounds[i++].on(EPSILON->MIN_RMS, EPSILON->MAX_RMS); //RMS //same value as in phil file
    if (refine_parameter_[m++])
      bounds[i++].on(minfs, maxfs); //FS
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++]) //cannot refine fs and bin at the same time!
      {
        //bin same scale as fs
        double minvarbin = std::max(EPSILON->MIN_VARBIN,minfs/EPSILON->TNCS_FS);
        double maxvarbin = std::min(EPSILON->MAX_VARBIN,maxfs/EPSILON->TNCS_FS);
        bounds[i++].on(minvarbin,maxvarbin);
      }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineTNCS::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot ++)
      if (refine_parameter_[m++])
        repar[i++].off(); //ROT
    if (refine_parameter_[m++])
      repar[i++].off(); //on(20); //GFN
    for (int tra = 0; tra < 3; tra ++)
      if (refine_parameter_[m++])
        repar[i++].off(); //TRA
    if (refine_parameter_[m++])
      repar[i++].off(); //RMS
    if (refine_parameter_[m++])
      repar[i++].off(); //on(0.9); //FS
    for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      if (refine_parameter_[m++])
        repar[i++].off(); //BIN
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineTNCS::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    //store the llg without restraints
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    return Target;
  }

  TargetType
  RefineTNCS::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    LLG = -REFLECTIONS->oversampling_correction()*(Tuple.Target);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineTNCS::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    LLG = -REFLECTIONS->oversampling_correction()*(Tuple.Target);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineTNCS::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    LLG = -REFLECTIONS->oversampling_correction()*(Tuple.Target);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  af_string
  RefineTNCS::initial_statistics()
  { //always the same, very boring
    return logNcsEpsn("Initial");
  }

  af_string
  RefineTNCS::current_statistics()
  {
    return logNcsEpsn("Current");
  }

  af_string
  RefineTNCS::final_statistics()
  {
    return logNcsEpsn("Final");
  }

  af_string
  RefineTNCS::reject_outliers()
  {
    sv_double epsnSigmaN(REFLECTIONS->NREFL,0);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const double resn = REFLECTIONS->get_flt(labin::RESN,r);
      const double teps = arrayTEPS[r];
      epsnSigmaN[r] = fn::pow2(resn)*teps;
    }
    SELECTED->flag_outliers_and_select(REFLECTIONS,
        SELECTED->minimal_criteria(),
        epsnSigmaN
      );
    //now call llg() to set the TNCS parameters in sync with the selected list
    //could be done by calling setup_parameters after reject_outliers in main but this is safer
    //likelihood(); //aka setup_parameters
   //no point in logging these because WILSON is not selected
    //(SELECTED->logOutliers(out::VERBOSE,"Anisotropy Refinement"));
    //(out::LOGFILE,SELECTED->logSelected("Anisotropy Refinement"));
    PHASER_ASSERT(SELECTED->nsel());
    return af_string();
  }

  af_string
  RefineTNCS::finalize_parameters()
  {
    for (int i = 0; i < 3; i++)
    {
      if (std::fabs(EPSILON->TNCS_VECTOR[i]) < DEF_PPM)
      {
        EPSILON->TNCS_VECTOR[i] = 0;
      }
    }
    return af_string();
  }

  af_string
  RefineTNCS::logNcsEpsn(std::string description,bool verbose)
  {
    af_string txt;
    txt = EPSILON->logEpsilon(description,verbose);

    double max_teps(-std::numeric_limits<double>::max());
    double min_teps(std::numeric_limits<double>::max());
    double mean_teps(0.);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const double teps = arrayTEPS[r];
      max_teps = std::max(teps,max_teps);
      min_teps = std::min(teps,min_teps);
      mean_teps += teps;
    }
    txt.push_back("Maximum tncs-epsn factor =   " + std::to_string(max_teps));
    txt.push_back("Minimum tncs-epsn factor =   " + std::to_string(min_teps));
    txt.push_back("Average tncs-epsn factor =   " + std::to_string(mean_teps/REFLECTIONS->NREFL));
    txt.push_back("");

    if (verbose)
    {
      int nrefl(10);
      for (bool highest : { false, true })
      {
        if (highest)
          txt.push_back("First " + std::to_string(nrefl) + " highest tncs-epsn factor reflections:");
        else
          txt.push_back("First " + std::to_string(nrefl) + " lowest tncs-epsn factor reflections:");
        std::vector<std::pair<double,int> > sort_ncs_epsfac(REFLECTIONS->NREFL);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          const double  teps = arrayTEPS[r];
          sort_ncs_epsfac[r] = std::pair<double,int>(teps,r);
        }
        auto cmp = []( const std::pair<double,int> &a, const std::pair<double,int> &b )
        { if (a.first == b.first) return a.second < b.second;
          return a.first < b.first; };
        std::sort(sort_ncs_epsfac.begin(),sort_ncs_epsfac.end(),cmp);
        if (highest)
          std::reverse(sort_ncs_epsfac.begin(),sort_ncs_epsfac.end());
        txt.push_back(snprintftos(
                 "%-3s %-3s %-3s %-8s %10s %12s %12s",
                 "H","K","L","(refl#)","tncs-epsn","I","SIGI"));
        for (int countr = 0; countr < nrefl; countr++)
        {
          int r = sort_ncs_epsfac[countr].second;
          const millnx& miller = REFLECTIONS->get_miller(r);
          const double& teps = arrayTEPS[r];
          const double& iobs = REFLECTIONS->get_flt(labin::INAT,r);
          const double& sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
          txt.push_back(snprintftos(
                   "% 3d % 3d % 3d (%6d) %10.4f %12.3f %12.3f",
                   miller[0],
                   miller[1],
                   miller[2],
                   r+1,
                   teps,
                   iobs,
                   sigi));
        }
        txt.push_back("");
      }
    }
    return txt;
  }

  TargetGradientHessianTncsType
  RefineTNCS::parallel_combined_tgh(
      const bool& do_gradient,
      const bool& do_hessian,
      const int& beg,
      const int& end
    ) const
  {
    TargetGradientHessianType tgh(nmp,false);
    Epsilon copy_of_epsilon = *EPSILON;
    bool halfR(false);
    copy_of_epsilon.initialize(halfR);
    int parallel_size(end-beg);
    sv_double beg_end_teps(parallel_size);
    double ONE(1);
    for (int r = beg; r < end; r++)
    {
      int rbeg = r-beg;
      if (SELECTED->get_selected(r))
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const millnx miller = work_row->get_miller();
        const double iobs = work_row->get_inat();
        const double sigi = work_row->get_siginat();
        const double resn = work_row->get_resn();
        const double wll  = labin_wll[r];
        const double ssqr = work_row->get_ssqr();
        const bool   cent = work_row->get_cent();
        const int&   s =    work_row->get_bin();
        copy_of_epsilon.calculate_reflection_parameters(miller,ssqr,s);
        PHASER_ASSERT(copy_of_epsilon.refl_TEPS > 0);

        double dLL_by_dEps(0);
        double d2LL_by_dEps2(0);
        //Use chain rule to get derivatives with respect to log-likelihood
        double epsnSigmaN = fn::pow2(resn)*copy_of_epsilon.refl_TEPS;
        double grad(0);
        double hess(0);
        // for French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2, ignore errors
        double prob = cent ?
            pintensity.pIobsCen_grad_hess_by_dSN(iobs,epsnSigmaN,sigi,do_gradient,grad,do_hessian,hess) :
            pintensity.pIobsAcen_grad_hess_by_dSN(iobs,epsnSigmaN,sigi,do_gradient,grad,do_hessian,hess);
        double dLL_by_dEpsnSigmaN(0);
        double d2LL_by_dEpsnSigmaN2(0);
        if (prob < std::numeric_limits<double>::min())
        {
          prob = std::numeric_limits<double>::min();
          dLL_by_dEpsnSigmaN = d2LL_by_dEpsnSigmaN2 = 0.;
        }
        else
        {
          if (do_gradient or do_hessian)
          {
            dLL_by_dEpsnSigmaN = -grad/prob;
            if (do_hessian)
            {
              d2LL_by_dEpsnSigmaN2 = -(hess/prob - fn::pow2(grad/prob));
            }
          }
        }
        if (do_gradient or do_hessian)
        {
          double dEpsnSigmaN_by_dEps = epsnSigmaN/copy_of_epsilon.refl_TEPS;
          dLL_by_dEps = dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dEps;
          if (do_hessian)
          {
            d2LL_by_dEps2 = d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dEps);
          }
        }
        // Accumulate minus log-likelihood-gain over null hypothesis of no tNCS
        tgh.Target -= (std::log(prob) - wll);

        if (do_gradient or do_hessian)
        {
          int i(0),m(0);
          if (refine_parameter_[m])
          {
            for (int rot = 0; rot < 3; rot++)
            {
              tgh.Gradient[i+rot] += dLL_by_dEps*copy_of_epsilon.dEps_by_dRot[rot];
              if (do_hessian)
              {
                if (tgh.Diagonal)
                  tgh.Hessian(i+rot,i+rot) += (d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dRot[rot]) +
                    dLL_by_dEps*copy_of_epsilon.d2Eps_by_dRot2(rot,rot));
                else
                {
                  for (int rot2 = 0; rot2 < 3; rot2++)
                    tgh.Hessian(i+rot,i+rot2) += (d2LL_by_dEps2*copy_of_epsilon.dEps_by_dRot[rot]*copy_of_epsilon.dEps_by_dRot[rot2] +
                      dLL_by_dEps*copy_of_epsilon.d2Eps_by_dRot2(rot,rot2));
                }
              }
            }
            i += 3; // Increment at end to use for indexing in inner loop
          }
          m += 3; // Increment after all 3 rotation angles
          if (refine_parameter_[m++])
          {
            tgh.Gradient[i] += dLL_by_dEps*copy_of_epsilon.dEps_by_dGfn;
            if (do_hessian)
            {
              tgh.Hessian(i,i) += (d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dGfn) +
                        dLL_by_dEps*copy_of_epsilon.d2Eps_by_dGfn2);
            }
            i++;
          }
          for (int tra = 0; tra < 3; tra++)
          {
            if (refine_parameter_[m++])
            {
              tgh.Gradient[i] += dLL_by_dEps*copy_of_epsilon.dEps_by_dTra[tra];
              if (do_hessian)
              {
                tgh.Hessian(i,i) += (d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dTra[tra]) +
                                dLL_by_dEps*copy_of_epsilon.d2Eps_by_dTra2[tra]);
              }
              i++;
            }
          }
          if (refine_parameter_[m++])
          {
            tgh.Gradient[i] += dLL_by_dEps*copy_of_epsilon.dEps_by_dRms;
            if (do_hessian)
            {
              tgh.Hessian(i,i) += (d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dRms) +
                               dLL_by_dEps*copy_of_epsilon.d2Eps_by_dRms2); //AJM bug?
            }
            i++;
          }
          if (refine_parameter_[m++]) //REFINE_FS
          {
            tgh.Gradient[i] += dLL_by_dEps*copy_of_epsilon.dEps_by_dFs;
            if (do_hessian)
            {
              // double d2Drms_by_dFs2(0);
              tgh.Hessian(i,i) += d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dFs);
                              // + dLL_by_dEps*dEps_by_dDrms*d2Drms_by_dFs2;
            }
            i++;
          }
          for (int bin = 0; bin < copy_of_epsilon.VARBINS.size(); bin++)
          {
            if (refine_parameter_[m++])
            {
              if (s == bin)
              {
                tgh.Gradient[i] += dLL_by_dEps*copy_of_epsilon.dEps_by_dBin;
                if (do_hessian)
                {
                  // double d2Drms_by_dBin2(0);
                  tgh.Hessian(i,i) += d2LL_by_dEps2*fn::pow2(copy_of_epsilon.dEps_by_dBin);
                }
              }
              i++;
            }
          }
        }
        //store the teps, but don't use this array as working array
        beg_end_teps[rbeg] = copy_of_epsilon.refl_TEPS;
      }
      else
      {
        //store the current teps, but don't use this array as working array
        beg_end_teps[rbeg] = ONE;
      }
    }

    return { tgh, beg_end_teps};
  }

  TargetGradientHessianType
  RefineTNCS::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    TargetGradientHessianType tgh(nmp,false);

    //calculate target, gradients and hessians without restraints
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    EPSILON->flags(do_gradient,do_hessian,tgh.Diagonal);
    // Construct ncsRmat and its derivatives, then transpose for subsequent calcs
    // EPSILON->initialize(false);
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      std::tie(tgh,arrayTEPS) = parallel_combined_tgh(
             do_gradient,
             do_hessian,
             beg,
             end );
    }
    else
    {
      std::vector< std::future< TargetGradientHessianTncsType > > results;
      std::vector< std::packaged_task< TargetGradientHessianTncsType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector<std::thread> threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back( std::bind( &RefineTNCS::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4
             ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end);
        }
        else
        {
          results.push_back( std::async( std::launch::async, &RefineTNCS::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      //this last thread will have the smallest number of reflections
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      int grain =  REFLECTIONS->grain(NTHREADS);
      sv_double beg_end_teps(grain);
      std::tie(tgh, beg_end_teps) = parallel_combined_tgh(
           do_gradient,
           do_hessian,
           beg,
           end);
      for (int r = beg; r < end; r++)
      {
        int rbeg = r-beg;
        arrayTEPS[r] = beg_end_teps[rbeg];
      }
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        TargetGradientHessianType result; sv_double result2;
        std::tie(result,result2) = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        for (int r = beg; r < end; r++)
        {
          int rbeg = r-beg;
          arrayTEPS[r] = result2[rbeg];
        }
      }
    }

    //add restraint terms
    bool apply_harmonic_restraints(
        SIGMA_ROT or SIGMA_TRA or SIGMA_GFNRAD or SIGMA_RMSD or SIGMA_FS or SIGMA_BINS);
    if (apply_harmonic_restraints and
        do_restraint and
        refine_parameter_.size()) //for llg(), refine_parameter_ not set
    {
      int i(0),m(0);
      for (int rot = 0; rot < 3; rot++) //rot
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_ROT)
          {
            tgh.Target += fn::pow2(EPSILON->TNCS_ANGLE[rot]/SIGMA_ROT)/2.;
            if (do_gradient)
              tgh.Gradient[i] += EPSILON->TNCS_ANGLE[rot]/fn::pow2(SIGMA_ROT);
            if (do_hessian)
              tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_ROT);
          }
          i++;
        }
      }
      if (refine_parameter_[m++])
      {
        if (SIGMA_GFNRAD)
        {
          double sigma = SIGMA_GFNRAD*INIT_GFNRAD;
          tgh.Target += fn::pow2((EPSILON->TNCS_GFNRAD-INIT_GFNRAD)/sigma)/2.;
          if (do_gradient)
            tgh.Gradient[i] += (EPSILON->TNCS_GFNRAD-INIT_GFNRAD)/fn::pow2(sigma);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(sigma);
        }
        i++;
      }
      for (int tra = 0; tra < 3; tra++) //tra
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_TRA)
          {
            tgh.Target += fn::pow2(EPSILON->TNCS_VECTOR[tra]-INIT_VECTOR[tra]/SIGMA_TRA)/2.;
            if (do_gradient)
              tgh.Gradient[i] += (EPSILON->TNCS_VECTOR[tra]-INIT_VECTOR[tra])/fn::pow2(SIGMA_TRA);
            if (do_hessian)
              tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_TRA);
          }
          i++;
        }
      }
      if (refine_parameter_[m++])
      {
        if (SIGMA_RMSD)
        {
          tgh.Target += fn::pow2((EPSILON->TNCS_RMS-INIT_RMS)/SIGMA_RMSD)/2.;
          if (do_gradient)
            tgh.Gradient[i] += (EPSILON->TNCS_RMS-INIT_RMS)/fn::pow2(SIGMA_RMSD);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_RMSD);
        }
        i++;
      }
      if (refine_parameter_[m++])
      {
        if (SIGMA_FS)
        {
          double diff(EPSILON->TNCS_FS-1.0);
          tgh.Target += fn::pow2(diff/SIGMA_FS)/2.;
          if (do_gradient)
            tgh.Gradient[i] += diff/fn::pow2(SIGMA_FS);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_FS);
        }
        i++;
      }
      for (int bin = 0; bin < EPSILON->VARBINS.size(); bin++)
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_BINS)
          {
            double binfac(EPSILON->VARBINS[bin]);
            double logbin(std::log(binfac));
            tgh.Target += fn::pow2(logbin/SIGMA_BINS)/2.;
            if (do_gradient)
              tgh.Gradient[i] += logbin/(binfac*fn::pow2(SIGMA_BINS));
            if (do_hessian)
              tgh.Hessian(i,i) += (1.-logbin)/fn::pow2(binfac*SIGMA_BINS);
          }
          i++;
        }
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    bool apply_smoothing_restraint(SIGMA_SMOOTH > 0);
    if (apply_smoothing_restraint and
        do_restraint and
        refine_parameter_.size()) //for llg(), refine_parameter_ not set
    {
      int i(0),m(0);
      for (int rot = 0; rot < 3; rot++) //rot
        if (refine_parameter_[m++]) i++;
      if (refine_parameter_[m++]) i++; //gfn
      for (int tra = 0; tra < 3; tra++) //tra
        if (refine_parameter_[m++]) i++;
      if (refine_parameter_[m++]) i++; //rmsd
      if (refine_parameter_[m++]) i++; //fs
      const double varbinwt(1./(2*fn::pow2(SIGMA_SMOOTH)));
      const int offset_vb(i); // Get offset for number of refined variables before bins
      if (refine_parameter_[m++]) i++; //s=0

      for (int s = 1; s < EPSILON->VARBINS.size()-1; s++) // restraints over inner bins
      {
        if (refine_parameter_[m++])
        {
          const double vmean = (EPSILON->VARBINS[s-1] + EPSILON->VARBINS[s+1])/2.;
          const double delta = EPSILON->VARBINS[s] - vmean;
          double tsmooth = varbinwt*fn::pow2(delta);
          tgh.Target += tsmooth;
          const int si = offset_vb+s;
          const int sip1 = si+1;
          const int sim1 = si-1;
          if (do_gradient)
          {
            const double varbinwtdelta = varbinwt*delta;
            tgh.Gradient[sim1] -= varbinwtdelta;
            tgh.Gradient[si]   += 2.*varbinwtdelta;
            tgh.Gradient[sip1] -= varbinwtdelta;
          }
          if (do_hessian)
          {
            tgh.Hessian(sim1,sim1) += varbinwt/2.;
            tgh.Hessian(si  ,si  ) += 2.*varbinwt;
            tgh.Hessian(sip1,sip1) += varbinwt/2.;
          }
          i++;
        }
      }
      if (refine_parameter_[m++]) i++; //s=last
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    return tgh;
  }

  double
  RefineTNCS::min_fs() { return std::sqrt(0.25*(1.0/INIT_ORDER)); } //sqrt(x<1)

  std::tuple<Epsilon,sv_double,double>
  RefineTNCS::parameters() { return std::make_tuple(*EPSILON,arrayTEPS,LLG); }

  void
  RefineTNCS::setup_wll()
  {
    labin_wll.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      //below is necessary
      if (REFLECTIONS->get_present(friedel::NAT,r)) //not "selected", as not selected yet
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
        const bool   cent = REFLECTIONS->work_cent(miller);
        const double inat = REFLECTIONS->get_flt(labin::INAT,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        double pWilson = cent ?
          pintensity.pIobsCen(inat,fn::pow2(resn),sigi) :
          pintensity.pIobsAcen(inat,fn::pow2(resn),sigi);
        pWilson = std::max(std::numeric_limits<double>::min(),pWilson);
        pWilson = std::log(pWilson);
        labin_wll[r] = pWilson;
      }
    }
  }
} //phasertng
