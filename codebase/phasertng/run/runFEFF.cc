//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <scitbx/math/basic_statistics.h>

namespace phasertng {

  //Cell_content_scaling
  void Phasertng::runFEFF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::ANISO))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (aniso)");
    //if tncs has not been run then hotwire here to make the TNCS_PRESENT false, with default order 1
    if (!REFLECTIONS->prepared(reflprep::TNCS))
    {
      logWarning("No tncs defined: tncs assumed not present");
      REFLECTIONS->TNCS_PRESENT = false;
    }
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");

    //Calculate DOBS and FEFF for current state of data and write to mtz
    //ReflRows must contain empty fields for Dobs and Feff
    for (auto f : {friedel::NAT,friedel::POS,friedel::NEG})
    {
      if (!REFLECTIONS->has_friedel(f)) continue;
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Feff and Dobs Calculation " + friedel::type2String(f));
      std::string ext = stolower(friedel::type2String(f));
      std::string key_dobs = ".phasertng.labin.dobs" + ext + "."; //input value_or_default :comment required for check keywords
      std::string key_feff = ".phasertng.labin.feff" + ext + "."; //input value_or_default :comment required for check keywords
      if (REFLECTIONS->MAP)
      { //should never have friedel pos/neg but you never know
        logTab(where,"Feff and Dobs Assignment for Map");
        bool has_mapdobs(REFLECTIONS->has_col(labin::MAPDOBS));
        if (has_mapdobs)
          logTab(where,"Dobs assignment from input");
        else
          logTab(where,"Dobs assiged to 1 because not present in input");
        REFLECTIONS->setup_dobs_feff_from_map(
          input.value__mtzcol.at(key_dobs).value_or_default(),
          input.value__mtzcol.at(key_feff).value_or_default());
      }
      else
      {
        REFLECTIONS->setup_dobs_feff( f,
          input.value__mtzcol.at(key_dobs).value_or_default(),
          input.value__mtzcol.at(key_feff).value_or_default(),
          input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default()
         );
      }
      //histogram data
      af_double dobs_array,feff_array;
      labin::col labin_dobs = REFLECTIONS->convert_friedel_to_labin("DOBS",f);
      labin::col labin_feff = REFLECTIONS->convert_friedel_to_labin("FEFF",f);
      input.value__mtzcol.at(key_dobs).set_value(input.value__mtzcol.at(key_dobs).name());
      input.value__mtzcol.at(key_feff).set_value(input.value__mtzcol.at(key_feff).name());
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const bool present = REFLECTIONS->get_present(f,r);
        if (present)
        {
          const double dobs = REFLECTIONS->get_flt(labin_dobs,r);
          const double feff = REFLECTIONS->get_flt(labin_feff,r);
          dobs_array.push_back(dobs);
          feff_array.push_back(feff);
        }
      }
      if (dobs_array.size())
      {
      out::stream where = out::LOGFILE;
      std::string title = "Dobs " + friedel::type2String(f);
      bool logscale(true); int bins(10); double mindobs(0); double maxdobs(1);
      logHistogram(where,dobs_array,bins,logscale,title,mindobs,maxdobs,false);
      //no histogram for Feff
      logBlank(where);
      }
      else logTab(out::LOGFILE,"No data");
      if (feff_array.size())
      {
      out::stream where = out::LOGFILE;
      std::string title = "Feff " + friedel::type2String(f);
      bool logscale(true); int bins(10);
      logHistogram(where,feff_array,bins,logscale,title);
      //no histogram for Feff
      logBlank(where);
      }
      double sigfigs(100); //2
      if (dobs_array.size())
      {
      //we want to round away the numerical instablility, for previous job identification
      out::stream where = out::SUMMARY;
      double mean(scitbx::math::basic_statistics<double>(dobs_array.const_ref()).mean);
      mean = std::round(mean * sigfigs) / sigfigs;
      double stddev(scitbx::math::basic_statistics<double>(dobs_array.const_ref()).biased_standard_deviation);
      stddev = std::round(stddev * sigfigs) / sigfigs;
      double skew(scitbx::math::basic_statistics<double>(dobs_array.const_ref()).skew);
      skew = std::round(skew * sigfigs) / sigfigs;
      logTab(where,stoupper(ext) +" Dobs: (Mean/Stddev/Skew) " + dtos(mean,3) + "/" + dtos(stddev,3) + "/" + dtos(skew,3));
      }
      if (feff_array.size())
      {
      out::stream where = out::SUMMARY;
      double mean(scitbx::math::basic_statistics<double>(feff_array.const_ref()).mean);
      mean = std::round(mean * sigfigs) / sigfigs;
      double stddev(scitbx::math::basic_statistics<double>(feff_array.const_ref()).biased_standard_deviation);
      stddev = std::round(stddev * sigfigs) / sigfigs;
      double skew(scitbx::math::basic_statistics<double>(feff_array.const_ref()).skew);
      skew = std::round(skew * sigfigs) / sigfigs;
      logTab(where,stoupper(ext) +" Feff: (Mean/Stddev/Skew) " + dtos(mean,3) + "/" + dtos(stddev,3) + "/" + dtos(skew,3));
      }
    }

    DAGDB.shift_tracker(hashing(),input.running_mode);

    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::FEFF);
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Reflections with Dobs/Feff"));

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //end namespace phaser
