#ifndef __phasertng_other_data_enum__
#define __phasertng_other_data_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace other { //note this is boosted as enum_entry_data
//must use different namespace because UNDEFINED is used in both
MakeEnum ( data,
           (UNDEFINED)
           (DATA_MTZ) //the reflections, not in Entry but referenced in database REFLID
           (TINY_MTZ)
           (REFINE_MTZ)
           (CONCISE_LOG)
           (SUMMARY_LOG)
           (LOGFILE_LOG)
           (VERBOSE_LOG)
           (PATHWAY_CARDS)
           (RESULT_CARDS)
           (IO_EFF)
           (DAG_CARDS)
           (DAG_HTML)
           (DAG_PHIL)
           (ENTRY_CARDS)
           (LOGGRAPHS_LOG)
           (SOLUTIONS_LOG)
         );

struct EnumHash
{
  template <typename T>
  std::size_t operator()(T t) const
  {
    return static_cast<std::size_t>(t);
  }
};
}}
#endif
