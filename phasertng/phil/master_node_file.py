#There is no automatic code generation from this file
#This is only used for cards to phil and phil to cards
#The parse and unparse functions are hand-coded for speed

#style = multiple makes non-multiple keywords appear on the one line
#because they are specifically added to the list in phil_to_cards.py and cards_to_phil


node_scope = "phaserdag" #shorter saves space
node_separator = "===="

node_implemented = {

"pathway" : '''
  pathway
    .style = multiple
  {
    ultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
    penultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
  }
''' ,

"pathlog" : '''
  pathlog
    .style = multiple
  {
    ultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
    penultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
  }
''' ,

"tracker" : '''
  tracker
    .style = multiple
  {
    ultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
    penultimate
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for node
      tag = None
        .type = str
        .short_caption = Short tag identifying node
    }
  }
''' ,

"node" : '''
  node
  {
    zscore = 0
      .type = float
      .short_caption = LLG Z-score
    percent = 0
      .type = float
      .short_caption = Percent score from search
    cellz = 1
      .type = float
      .short_caption = Cell Scaling Z
    rfactor = 100
      .type = float(value_min=0,value_max=100)
      .short_caption = R-factor
      .help = R-factor, without solvent correction terms, maximum value restricted to 100%.
    llg = 0
      .type = float
      .short_caption = LLG value
    fss = None
      .type = bool
      .short_caption = LLG value is a fast search score
    mapllg = 0
      .type = float
      .short_caption = Phased LLG value
    hklin
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for data
      tag = None
        .type = str
        .short_caption = Short tag identifying model
    }
    composition
      .multiple = True
    {
      element = None
        .type = str
        .help = Atom type in the composition of biological unit in the asymmetric unit.
        .short_caption = Element type
      number = None
        .type = int(value_min=0)
        .help = Number of atoms of element type in the composition of biological unit in the asymmetric unit.
        .short_caption = Number of atoms of element type
    }
    annotation = None
      .type = str
      .short_caption = Text annotation
    spacegroup = None
      .type = str
      .short_caption = Space group Hall symbol
    hermann_mauguin = None
      .type = str
      .short_caption = Space group HM symbol
    unitcell = None
      .type = unit_cell
      .short_caption = Unit cell dimensions
    tncs_order = 1
      .type = int
      .short_caption = Tncs order used
    tncs_vector =  0 0 0
      .type = floats(size=3)
      .short_caption = Tncs vector used
    tncs_present_in_model = False
      .type = bool
      .short_caption = Tncs duplications are present in the search model and do not need to be applied at structure factor calculation or pose steps
    tncs_indicated = None
      .type = bool
      .short_caption = Tncs indicated
    twinned = None
      .type = bool
      .short_caption = Twinning indicated
    twinop = None
      .type = strings
      .short_caption = The twinning operator(s) if twinned and the operator(s) determined in refinement
    packs = *? Y X Z O U
      .type = choice
      .short_caption = Packs according to packing criteria, for carried high tfz solutions
    clash_worst = 0
      .type = float(value_min=0,value_max=100)
      .short_caption = Worst packing percent
    special_position = None
      .type = bool
      .short_caption = One of the molecules with point group symmetry is on a special position
    spans = None
      .type = bool
      .short_caption = The poses span the unit cell, filling space
      .short_caption = Total substructure expected llg
    information_content = 0
      .type = float
      .short_caption = Total data information content
    expected_information_content = 0
      .type = float
      .short_caption = Total data information content
    sad_information_content = 0
      .type = float
      .short_caption = Total sad substructure information content
    sad_expected_llg = 0
      .type = float
    parents = None
      .type = uuids(value_min=0)
      .short_caption = Identifier linking nodes from same parent
      .help = Identifier linking nodes from same parent. Identifies penultimate and ultimate for background of each rotation function performed.
    rellg = 0
      .type = float
      .short_caption = Relative expected llg
      .help = Relative expected llg with respect to perfectly imperfect model
    signal_resolution = 0
      .type = float
      .short_caption = Resolution of data to which there is a signal
    signal_resolution_phased = 0
      .type = float
      .short_caption = Resolution of phased data to which there is a signal
    seek_resolution = 0
      .type = float
      .short_caption = Resolution of data required to reach expected llg target in the case of a single component being easy to place (over target ellg)
    seek_resolution_phased = 0
      .type = float
      .short_caption = Resolution of data required to reach expected map llg target in the case of a single component being easy to place (over target ellg)
    seek_components = 0
      .type = int
      .short_caption = Number of components from seek list required to reach expected llg target
    seek_components_ellg = 0
      .type = float
      .short_caption = Total of the seek components expected llg
    seek_complete = False
      .type = bool
      .short_caption = All seek components have been found
    data_scale = 1.0
      .type = float
      .short_caption = Scale factor for data when data is from electron microscopy grid
    pose
      .multiple = True
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
      euler = None
        .type = floats(size=3)
        .short_caption = Euler angles for pose
      rmat = 1 0 0  0 1 0  0 0 1
        .type = floats(size=9)
        .short_caption = Matrix rotation relative to initial orientation
      rxyz = 0 0 0
        .type = floats(size=3)
        .short_caption = Rotation shift in degrees relative to pose euler angle rotation
      fractional = None
        .type = floats(size=3)
        .short_caption = Fractional coordinates for pose
      shift_orthogonal = 0 0 0
        .type = floats(size=3)
        .short_caption = Orthogonal shift relative to initial poisition
      txyz = 0 0 0
        .type = floats(size=3)
        .short_caption = Translation shift in Angstroms relative to pose fractional translation
      bfactor = 0
        .type = float(value_min=-999,value_max=999)
        .short_caption = B-factor for pose
      m = 1
        .type = int(value_min=1)
        .short_caption = Multiplicity factor for pose
      tncs_group = 1 1
        .type = ints(size=2,value_min=1)
        .short_caption = Integer identifying tncs group and index of pose in group
      clash = 0 0 0
        .type = ints(size=3,value_min=0)
        .short_caption = Clash information for this pose where percent overlap is second divided by third
        .help = Three integers describing clashes. First is the index into the clash matrix, second is the number of atoms clashing in this pose of model, and last is the number of atoms in the trace. The fraction of this pose overlapped is calculated from the second divided by the third.
      aellg = 0
        .type = float
        .short_caption = Analytical expected llg
      tfz = 0
        .type = float
        .short_caption = Translation function z-score of placement
    }
    next
      .style = multiple
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
      euler = None
        .type = floats(size=3)
        .short_caption = Euler angles for orientation
      rmat = 1 0 0  0 1 0  0 0 1
        .type = floats(size=9)
        .short_caption = Matrix rotation relative to initial orientation
      rxyz = 0 0 0
        .type = floats(size=3)
        .short_caption = Rotation shift in degrees relative to euler angle
      translated = False
        .type = bool
        .short_caption = Translation is included in current knowledge of solution
      fractional = None
        .type = floats(size=3)
        .short_caption = Translation without resolving orientation (half-rotation) and strict tncs assumed
      shift_orthogonal = 0 0 0
        .type = floats(size=3)
        .short_caption = Orthogonal shift relative to initial poisition
      tfz = 0
        .type = float
        .short_caption = Translation function z-score of placement
    }
    gyre
      .multiple = True
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
      rxyz = 0 0 0
        .type = floats(size=3)
        .short_caption = Rotation shift in degrees around orthogonal xyz
      txyz = 0 0 0
        .type = floats(size=3)
        .short_caption = Orthogonal shift in angstroms along orthogonal xyz
      rmat = 1 0 0  0 1 0  0 0 1
        .type = floats(size=9)
        .short_caption = Matrix rotation relative to initial orientation
    }
    seek
      .multiple = True
    {
      id = None
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
      star = None
        .type = str
        .short_caption = Flag for search component
    }
    hold
      .multiple = True
    {
      id = None
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
    }
    full
      .style = multiple
    {
      present = False
        .type = bool
        .short_caption = Full is current knowledge of solution, concurrent with poses
      id = None
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying substructure
        .help =  Tag can be used in place of id to define model, provided unique.
      fs = None
        .type = float
        .short_caption = Fraction scattering
    }
    part
      .style = multiple
    {
      present = False
        .type = bool
        .short_caption = Part is current knowledge of solution, displacing poses
      id = None
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying substructure
        .help =  Tag can be used in place of id to define model, provided unique.
    }
    variance
      .multiple = True
    {
      id = 0
        .type = uuid(value_min=0)
        .short_caption = Identifier for model
      tag = None
        .type = str
        .short_caption = Short tag identifying model
        .help =  Tag can be used in place of id to define model, provided unique.
      drms = 0
        .type = float
        .short_caption = Vrms shift factor for model
      vrms = 0
        .type = float
        .short_caption = Vrms for model. If model is an ensemble, this is the first vrms value.
      cell = 1
        .type = float(value_min=0.8,value_max=1.2)
        .short_caption = Vrms shift factor for model
    }
    sigmaa
      .style = multiple
    {
      midssqr = None
        .type = float
      sigman = None
        .type = float
      sigmap = None
        .type = float
      sigmaq = None
        .type = float
      sigmah = None
        .type = float
      absrhoff = None
        .type = float
      sigmapp = None
        .type = floats(size=2)
        .short_caption = complex number
      sigmaqq = None
        .type = floats(size=2)
        .short_caption = complex number
      sigmahh = None
        .type = floats(size=2)
        .short_caption = complex number
      fdelta = None
        .type = float
      rhopm = None
        .type = float
    }
  }
''' ,

"entry" : '''
  entry
  .multiple = True
  {
    id = 0
      .type = uuids(value_min=0)
      .short_caption = Identifier for model
    tag = None
      .type = strings
      .short_caption = Short tag identifying model
      .help =  Tag can be used in place of id to define model, provided unique.
    atom = False
      .type = bool
      .short_caption = Ensemble is an atom
    helix = False
      .type = bool
      .short_caption = Ensemble is a helix
    map = False
      .type = bool
      .short_caption = Ensemble is a map
    scattering = 0
      .type = int
      .short_caption = Effective scattering of model
    mw = 0
      .type = float
      .short_caption = Effective molecular weight of model
    pt = 0 0 0
      .type = floats(size=3)
      .short_caption = Principal Translation Vector
    pr = 1 0 0 0 1 0 0 0 1
      .type = floats(size=9)
      .short_caption = Principal Orientation Matrix
    radius = 0
      .type = float
      .short_caption = Molecular mean radius
    volume = 0
      .type = float
      .short_caption = Molecular volume
    centre = 0 0 0
      .type = floats(size=3)
      .short_caption = Centre of coordinates
    extent = 0 0 0
      .type = floats(size=3)
      .short_caption = Extents
    pg_symbol = None
      .type = str
      .short_caption = Point group symbol for model
      .help = Point group for an model that is (in itself) an oligomer
    pg_euler = None
      .type = floats
      .short_caption = Point group euler angles for model
      .help = Point group euler angles for an model that is (in itself) an oligomer, \
              read in groups of three
    drms = 0 0
      .type = floats(size=2)
      .short_caption = Range of drms values for refinement
      .help = Best and worst rmsd possible between any model in model and target after refinement. Gives limits for vrms. The default miniumun-rmsd is one twentieth high resolution limit for model configuration and the default maximun-rmsd is one sixth of mean radius of model.
    vrms = None
      .type = floats
      .short_caption = Initial values of vrms
    hires = 0
      .type = float
      .short_caption = High resolution to which model is configured
    lores = 0
      .type = float
      .short_caption = Low resolution limit for interpolation
    data = 1 2 3 4 5 6 7 8 9 10 11
      .type = choice(multi=True)
      .short_caption = Data structures filled in database models_pdb trace_pdb monostructure_pdb substructure_pdb coordinates_pdb fcalcs_mtz interpolation_mtz decomposition_mtz variance_mtz density_mtz gradient_mtz
  }
''' ,

"reflections" : '''
  reflections
  .multiple = True
  {
    id = 0
      .type = uuids(value_min=0)
      .short_caption = Pathway and id number for reflections
    tag = None
      .type = strings
      .short_caption = Short tag for reflections
  }
''' ,


} # end of implemented dictionary

def build_node_str():
  master_node = node_scope + " {\n"
  master_node += node_implemented["pathway"] + "\n"
  master_node += node_implemented["pathlog"] + "\n"
  master_node += node_implemented["tracker"] + "\n"
  master_node += node_implemented["node"] + "\n"
  master_node += node_implemented["entry"] + "\n"
  master_node += "}\n"
  return master_node

def build_graph_str():
  master_node = node_scope + " {\n"
  master_node += node_implemented["pathway"] + "\n"
  master_node += node_implemented["pathlog"] + "\n"
  master_node += node_implemented["tracker"] + "\n"
#add the node zscore explicitly as this is used to generate the size of the node circles
  master_node += '''
  node.zscore = 0
      .type = float
      .short_caption = LLG Z-score
  '''
  master_node += "}\n"
  return master_node

if __name__ == "__main__":
  print(build_node_str())
