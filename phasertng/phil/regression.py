import iotbx.phil
import copy
import os.path
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from master_phil_file import *
from converter_registry import *
from phasertng.scripts.OrderedSet import *
from phasertng.scripts.OrderedDict import *
from phasertng.phil.master_phil_file import build_phil_str


def rreplace(s, old, new, occurrence):
  li = s.rsplit(old, occurrence)
  return new.join(li)

class Regression(object):
    def __init__(self, ok=False, test=None, truth=None,regression=None, tolerance=None, difference=None,same_exit_status=True):
        self.ok = ok
        self.same_exit_status = same_exit_status
        self.test = test
        self.truth = truth
        self.regression = regression
        self.tolerance = tolerance
        self.difference = difference

    def __str__(self):
      if self.ok: return "OK"
      errorstring = ""
      if self.test  is not None: errorstring += "\ttest  = " + str(self.test ) + "\n"
      if self.truth is not None: errorstring += "\ttruth = " + str(self.truth) + "\n"
      if self.regression is not None: errorstring += "\tregression = " + str(self.regression) + "\n"
      if self.tolerance is not None: errorstring += "\ttolerance = " + str(self.tolerance) + "\n"
      if self.difference is not None: errorstring += "\tdifference = " + str(self.difference) + "\n"
      return errorstring

    def OK(self):
     return self.ok

class KeyWords(object):
    def __init__(self, words, regression, value_is_number, is_error):
        self.words = words
        self.regression = regression
        self.value_is_number = value_is_number
        self.is_error = is_error

    def __str__(self):
      return " words=" + str(words) + \
             " is_error=" + str(is_error)

def found_all(multiple_keywords_basekey):
    foundall = True
    for val in multiple_keywords_basekey:
      for k in val:
        if val[k] is None:
          foundall = False
    return foundall

def reset_all(multiple_keywords_basekey):
    for val in multiple_keywords_basekey:
      for k in val:
        val[k] = None #reset
    return multiple_keywords_basekey

def regression_type(e):
    regression_tol = None
    extract_attribute_error = False
    if (getattr(e.object, "style", extract_attribute_error) is not extract_attribute_error):
      styletype = str(getattr(e.object, "style"))
      #if the style information contains the regression information
      #otherwise this keyword is ignored, and not included in regression tests
      if "tng:regression:" in styletype:
        #the style line contains a list of SPACE separated keywords
        attr_list = styletype.split(' ')
        regression_tol = None # type defaults
        for a in attr_list: #first loop selects size=2
          if a.startswith("tng:regression:"):
            regression_tol = a.replace("tng:regression:","")
            if len(regression_tol) == 0:
              # for example "tng:regression:" with missing parameter
              raise Exception("No regression test tolerance "+e.path)
    return regression_tol

def regression_test(regression_tol,value_is_number,test_words,truth_words):
    #all regression tests
    test_str =','.join([str(i) for i in test_words])
    truth_str =','.join([str(i) for i in truth_words])
    if regression_tol == "match" and (test_str != truth_str):
      return Regression(test=test_str,truth=truth_str,regression=regression_tol)
    elif regression_tol == "match" and (test_str == truth_str):
      return Regression(ok=True)
    elif regression_tol.endswith("%") and \
         regression_tol.replace('.','',1).replace('%','').isdigit() and \
         value_is_number:
      tol = float(regression_tol[:1])/100 # the fraction change for the test
      for i in range(len(test_words)):
        truth = float(truth_words[i].value)
        test = float(test_words[i].value)
        diff = abs(truth-test)/truth
        if diff > tol:
          return Regression(test=test_str,truth=truth_str,regression=regression_tol,tolerance=tol,difference=diff)
        else:
          return Regression(ok=True)
    elif regression_tol.replace('.','',1).isdigit() and value_is_number:
      tol = float(regression_tol) # the change for the test
      for i in range(len(test_words)):
        truth = float(truth_words[i].value)
        test = float(test_words[i].value)
        diff = abs(truth-test)
        if diff > tol:
          return Regression(test=test_str,truth=truth_str,regression=regression_tol,tolerance=tol,difference=diff)
        else:
          return Regression(ok=True)
    elif regression_tol == "ppm"  and value_is_number:
      tol = float(1./1000000.) # the change for the test
      for i in range(len(test_words)):
        truth = float(truth_words[i].value)
        test = float(test_words[i].value)
        diff = abs(truth-test)/truth
        if diff > tol:
          return Regression(test=test_str,truth=truth_str,regresion=regression_tol,tolerance=tol,difference=diff)
        else:
          return Regression(ok=True)
    elif regression_tol == "ppt"  and value_is_number:
      tol = float(1./1000.) # the change for the test
      for i in range(len(test_words)):
        truth = float(truth_words[i].value)
        test = float(test_words[i].value)
        diff = abs(truth-test)/truth
        if diff > tol:
          return Regression(test=test_str,truth=truth_str,regression=regression_tol,tolerance=tol,difference=diff)
        else:
          return Regression(ok=True)
    elif regression_tol == "pph"  and value_is_number:
      tol = float(1./100.) # the change for the test
      for i in range(len(test_words)):
        truth = float(truth_words[i].value)
        test = float(test_words[i].value)
        diff = abs(truth-test)/truth
        if diff > tol:
          return Regression(test=test_str,truth=truth_str,regression=regression_tol,tolerance=tol,difference=diff)
        else:
          return Regression(ok=True)
    else:
      raise Exception("Regression test not recognised")
    return Regression(ok=True)

def regression_test_multiples(phil1,phil1_count_basekey,phil2,multiple_keywords,Errors,loop):
  for e in phil1.all_definitions(): # phil1 values
    #print(e.path)
    # this loop through we are going to collect lots of interesting things
    # in the dict, not just "not none"
    multiple_phil1 = dict()
    regression_tol = regression_type(e)
    if regression_tol is None:
      continue
    ebasekey = ".".join(e.path.split('.')[:-1]) if e.parent.multiple else e.path
    etrailing = ".".join(e.path.split('.')[-1:]) if e.parent.multiple else ""
    #.type is always present or code won't compile etc, no need to check for extract_attribute_error
    valuetype = str(getattr(e.object, "type"))
    value_is_number = (valuetype.startswith("float") or valuetype.startswith("int"))
    ewords = []
    for word in e.object.words:
      ewords.append(word)
    for val in multiple_keywords[ebasekey]:
      for k in val:
        if k == etrailing:
          is_error = True # default is look for match, because not found
          val[k] = KeyWords(ewords, regression_tol, value_is_number, is_error)
    if found_all(multiple_keywords[ebasekey]):
      multiple_phil1 = copy.deepcopy(multiple_keywords[ebasekey])
      multiple_keywords[ebasekey] = reset_all(multiple_keywords[ebasekey])

      #loop over phil2 to find correspondances between phil2 and phil1
      #if e.path is not a multiple keyword, then can add error immediately
      #if e.path is a multiple keyword then have to find out if it us used once or more
      #if only used once, and there is a mismatch, then can add error immediately
      # but if used more than once and there are no matches, then throw a different error
      # that does not specify which phil1 value failed (because it is undefined)

      if phil1_count_basekey[ebasekey] > 1 :
        #finding the correspondences is more complicated!
        # you have to fill the multiple array first
        multiple_phil2 = dict()
        for t in phil2.all_definitions(): # phil2 values
          tbasekey = ".".join(t.path.split('.')[:-1]) if t.parent.multiple else t.path
          ttrailing = ".".join(t.path.split('.')[-1:]) if t.parent.multiple else ""
          twords = []
          for word in t.object.words:
            twords.append(word)
          for val in multiple_keywords[tbasekey]:
            for k in val:
              if k == ttrailing:
                val[ttrailing] = twords # not KeyWords
          if found_all(multiple_keywords[tbasekey]):
            multiple_phil2 = copy.deepcopy(multiple_keywords[tbasekey])
            multiple_keywords[tbasekey] = reset_all(multiple_keywords[tbasekey])
            is_error = False
            if (tbasekey == ebasekey): # all full for subkeys, base is equal
              for val in multiple_phil1:
                for k in val:
                  is_error = is_error or val[k].is_error
              if is_error:
                for val in multiple_phil1:
                  for k in val:
                    regression_tol = val[k].regression
                    value_is_number = val[k].value_is_number
                    phil1_words = val[k].words
                    for val2 in multiple_phil2: #awkward lookup
                      for k2 in val2: #awkward lookup
                        if (k2 == k): #awkward lookup
                          phil2_words = val2[k2] #lookup into phil2
                    Test = regression_test(regression_tol,value_is_number,phil1_words,phil2_words)
                    val[k].is_error = not Test.OK()
        #after look through all base classes
        if len(multiple_phil2) and len(multiple_phil2):
          is_error = False
          cards = "["
          for val in multiple_phil1:
            for k in val:
              is_error = is_error or val[k].is_error
              phil1_str =','.join([str(i) for i in val[k].words])
              cards += k + "=" + phil1_str + " "
          cards = cards[:-1]
          cards += "]"
          if is_error:
            message = "missing from truth" if loop else "missing from test"
            Errors[ebasekey].append(Regression(test=cards,regression=message))
  return Errors


def regression(test_phil_str=None,truth_phil_str=None):
  Errors = defaultdict(list) # returned list of Regression errors, indexed on parameter that fails

  master_params = build_phil_str("InputCard")
  master_phil = parse(master_params,converter_registry=converter_registry)
  #create the phil from the truth
  truth_phil = parse(truth_phil_str,converter_registry=converter_registry)
  truth_phil = master_phil.fetch(source=truth_phil)
  #create the phil from the test output
  test_phil = parse(test_phil_str,converter_registry=converter_registry)
  test_phil = master_phil.fetch(source=test_phil)

  #if the jobs have different exit status, then exit with special Regression status
  # with flag same_exit_status set to false and
  # with the error messages from truth and test recorded in the test and truth strings
  truth_params = truth_phil.extract()
  test_params = test_phil.extract()
  if (test_params.phasertng.notifications.success != truth_params.phasertng.notifications.success):
     truth_msg= truth_params.phasertng.notifications.error.type
     if truth_params.phasertng.notifications.error.message is not None:
       truth_msg=truth_msg + ": " + truth_params.phasertng.notifications.error.message
     test_msg=str(test_params.phasertng.notifications.error.type)
     if test_params.phasertng.notifications.error.message is not None:
       test_msg=test_msg + ": " + test_params.phasertng.notifications.error.message
     Errors["phasertng.notifications"].append(
            Regression(test=test_msg,
                       truth=truth_msg,
                       regression="notifications",
                       same_exit_status=False))
     return ( False, Errors )

  #create a phil that is just the differences
  # as a first pass to simplify the analysis
  test_phil = master_phil.fetch_diff(source=test_phil)
  truth_phil = master_phil.fetch_diff(source=truth_phil)
  print("==== test ====")
  print(test_phil.as_str())
  print("=====")
  print("")
  print("==== truth ===")
  print(truth_phil.as_str())
  print("=====")
  #fetch_diff on truth_phil_fetch_diff(source=test_phil) does not handle multiples

  Errors = defaultdict(list)
  #test after sorted into order or master_phil, equality not necessarily true on input
  #shortcut for trivial equality
  if test_phil.as_str() == truth_phil.as_str():
    return ( True, Errors )

  #find ALL the keywords that are multiple
  #must use master
  multiple_keywords = defaultdict(list)
  for e in master_phil.all_definitions():
    basekey = ".".join(e.path.split('.')[:-1]) if e.parent.multiple else e.path
    trailing = ".".join(e.path.split('.')[-1:]) if e.parent.multiple else ""
    multiple_keywords[basekey].append(dict({ trailing: None }))

  test_count_basekey = dict()
  for basekey in multiple_keywords:
    test_count_basekey[basekey] = 0
  for e in test_phil.all_definitions(): # test values
    basekey = ".".join(e.path.split('.')[:-1]) if e.parent.multiple else e.path
    trailing = ".".join(e.path.split('.')[-1:]) if e.parent.multiple else ""
    for val in multiple_keywords[basekey]:
      for v in val:
        if v == trailing:
           val[trailing] = "not none"
    if found_all(multiple_keywords[basekey]):
      test_count_basekey[basekey] = test_count_basekey[basekey] +1
      multiple_keywords[basekey] = reset_all(multiple_keywords[basekey])

  #assumes all reset here, will cause spurious regression fails if not

  truth_count_basekey = dict()
  for basekey in multiple_keywords:
    truth_count_basekey[basekey] = 0
  for t in truth_phil.all_definitions(): # truth values
    basekey = ".".join(t.path.split('.')[:-1]) if t.parent.multiple else t.path
    trailing = ".".join(t.path.split('.')[-1:]) if t.parent.multiple else ""
    for val in multiple_keywords[basekey]:
      for v in val:
        if v == trailing:
           val[trailing] = "not none"
    if found_all(multiple_keywords[basekey]):
      truth_count_basekey[basekey] = truth_count_basekey[basekey] +1
      multiple_keywords[basekey] = reset_all(multiple_keywords[basekey])

  # if the number of entries for the multiples is different between the test and truth
  #then add error
  #still check for which truth items are not in test
  #  throws a "missing" error
  #still check for which test items are not in truth
  #  throws a "addition" error
  for t in master_phil.all_definitions(): # truth values
    basekey = ".".join(t.path.split('.')[:-1]) if t.parent.multiple else t.path
    trailing = ".".join(t.path.split('.')[-1:]) if t.parent.multiple else ""
    if (truth_count_basekey[basekey] != test_count_basekey[basekey]):
      if len(Errors[basekey]) == 0: # first error type accumulated!!
        #if no test for empty Error above, get one error entry for each subkey
        message = "("+ str(test_count_basekey[basekey]) + "/" + str(truth_count_basekey[basekey]) + ")"
        if t.parent.multiple:
          message = "different number of scopes " + message
          Errors[basekey].append(Regression(regression=message))
        else:
          message = "missing scope " + message
          Errors[basekey].append(Regression(regression=message))

  for e in test_phil.all_definitions(): # test values
    #print(e.path)
    # this loop through we are going to collect lots of interesting things
    # in the dict, not just "not none"
    multiple_test = dict()
    regression_tol = regression_type(e)
    if regression_tol is None:
      continue
    ebasekey = ".".join(e.path.split('.')[:-1]) if e.parent.multiple else e.path
    etrailing = ".".join(e.path.split('.')[-1:]) if e.parent.multiple else ""
    #.type is always present or code won't compile etc, no need to check for extract_attribute_error
    valuetype = str(getattr(e.object, "type"))
    value_is_number = (valuetype.startswith("float") or valuetype.startswith("int"))
    ewords = []
    for word in e.object.words:
      ewords.append(word)
    #loop over truth to find correspondances between truth and test
    #if e.path is not a multiple keyword, then can add error immediately
    #if e.path is a multiple keyword then have to find out if it us used once or more
    #if only used once, and there is a mismatch, then can add error immediately
    # but if used more than once and there are no matches, then throw a different error
    # that does not specify which test value failed (because it is undefined)

    if test_count_basekey[ebasekey] == 1 and truth_count_basekey[ebasekey] == 1 :
      #we don't care if it is a multiple, just check the full paths
      #and throw error if there is a mismatch
      for t in truth_phil.all_definitions(): # truth values
        #print(t.path)
        if t.path == e.path:
          #make a string of everything after the = (multiple parameters)
          test_str =','.join([str(i) for i in e.object.words])
          truth_str =','.join([str(i) for i in t.object.words])
          if (test_str != truth_str):
            if (len(e.object.words) != len(t.object.words)):
              #same path should have the same number of words with (size=n) set
              # conversion to phil will fail if size!=n, however if no size set
              # parameter lists can be different sizes - allowed by the phil  and can
              # make it to this point
              # this must be true for the rest of the loop to be happy, so continue if fails
              Errors[e.path].append(Regression(test=test_str,truth=truth_str,regression="word length different"))
            else:
              twords = []
              for word in t.object.words:
                twords.append(word)
              Test = regression_test(regression_tol,value_is_number,ewords,twords)
              if not Test.OK():
                Errors[e.path].append(Test)

  #test and inverted test
  Errors = regression_test_multiples(test_phil,test_count_basekey,truth_phil,multiple_keywords,Errors,True)
  Errors = regression_test_multiples(truth_phil,truth_count_basekey,test_phil,multiple_keywords,Errors,False)

  if len(Errors):
   return ( False, Errors )
  return  ( True, Errors )

if __name__ == "__main__":
  truth_phil_str = '''
 phasertng.reflections.filename = $PHENIX/modules/phasertng/tutorial/data/beta_blip/beta_blip_P3121.mtz
 phasertng.reflections.resolution = 3
 phasertng.reflections.resolution_available = 10 5
 phasertng.anisotropy.sharpening_bfactor = 11
 phasertng.reflections.anomalous = True
 phasertng.reflections.reflid = 2341234125
 phasertng.matthews {
   vm = 1040
   probability = 0.50
   z = 1
 }
 phasertng.matthews {
   vm = 1000
   probability = 0.50
   z = 2
 }
 phasertng.matthews {
   vm = 1100
   probability = 0.55
   z = 2
 }
 phasertng.matthews {
   vm = 1140
   probability = 0.55
   z = 2
 }
'''
  test_phil_str = '''
 phasertng.reflections.filename = beta_blip_P3221.mtz
 phasertng.reflections.resolution = 3.1
 phasertng.reflections.resolution_available = 10 6
 phasertng.reflections.anomalous = False
 phasertng.anisotropy.sharpening_bfactor = 10
 phasertng.reflections.reflid = 2341234125
 phasertng.matthews {
   vm = 1000
   probability = 0.50
   z = 2
 }
 phasertng.matthews {
   vm = 1100
   probability = 0.55
   z = 2
 }
 phasertng.matthews {
   vm = 1130
   probability = 0.55
   z = 2
 }
'''
  test_phil_str = '''
 phasertng.notifications.success = False
 phasertng.notifications.error.code = 65
 phasertng.notifications.error.type = fatal
 phasertng.notifications.error.message = "regression test failure"
'''
  ok, status = regression(test_phil_str = test_phil_str, truth_phil_str = truth_phil_str)
  if not ok:
    for key,val in status.items():
      for v in val:
        print("Error: ", key, "=>\n", str(v))
  else:
    print("OK")
