//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/path.h>

namespace phasertng {
namespace type {

  path::path(
      const std::string parameter,
      const std::string dephalt_,
      const std::string extension_
  ) :
     optional(parameter),
     uvalue(dephalt_),
     dephalt(dephalt_),
     extension(extension_)
  { }

  std::string path::ext() { return extension; } //ext in master_phil_file

  //input from quoted strings
  void
  path::set_value(std::string other)
  {
    if (!other.size()) { set_default(); return; }
    user = true;
//stored unquoted - filename cannot start with quote
    if (other[0] != '"')
    {
      uvalue = other;
    }
    else
    {
      std::string unquoted = other;
      while (unquoted.size() && unquoted[0] == '"') //remove nested "
      {
        std::string tmp = "";
        for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
          tmp += std::string(1,unquoted[i]);
        unquoted = tmp;
      }
      //phaser_assert(!unquoted.size() || unquoted[0] != '"');
      uvalue = unquoted;
    }
  }

  void
  path::set_value(const sv_string& vcards,const std::string& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  path::is_default() const
  { return (uvalue == dephalt or uvalue == None()); }

  void
  path::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  path::to_python()
  { return unparse_parameter(false); }

  void
  path::from_python(std::string str)
  { set_value(str); }

  std::string
  path::value_or_default() const  // unquoted
  {
    std::string out = !is_default() ? uvalue : dephalt;
    return out;
  }

  std::string
  path::show_default(bool phil) const
  {
    std::string vv = dephalt.size() ? dephalt : None();
    return std::string("\"" + vv + "\"");
  }

  std::string
  path::unparse_parameter(bool phil) const
  {
    std::string v = value_or_default();
    std::string vv = v.size() ? v : None();
    return std::string("\"" + vv + "\"");
  }

}}
