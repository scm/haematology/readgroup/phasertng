#! /usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################################
#  Copyright and authorship belong to:                               #
#  @author: Massimo Sammito                                         #
#  @email: msacri@ibmb.csic.es / massimo.sammito@gmail.com          #
#####################################################################
#####################################################################
#                              LICENCE                              #
#                                                                   #
#                                                                   #
#####################################################################

# System imports
from future.standard_library import install_aliases

install_aliases()
import sys, math, os, copy, logging
from functools import reduce  # forward compatibility for Python 3
import operator
import shutil, collections
import mmtbx.utils
import mmtbx.model
import iotbx.pdb.mmcif
import cctbx
import scitbx
from cctbx.array_family import flex
from cctbx.uctbx.determine_unit_cell import NCDist
from iotbx.file_reader import any_file
from phasertng.cctbx_project.mmtbx.scaling import pair_analyses
from phasertng import enum_err_code


def ma_selection(ma):
   # AJM selection is not perfect, since you can have an f without a sigma
   # however, the amplitudes and intensities can be mislabelled eg fom
  #return ma.is_xray_data_array() and ma.sigmas() is not None and ma.size() > 10
  #print(ma.info().labels,ma.size(),ma.observation_type(),ma.is_xray_data_array(),ma.sigmas())
   return ma.size() > 10 and \
          ma.observation_type() is not None and \
          ma.is_xray_data_array() and \
     (len(ma.info().labels)>0 and ma.info().labels[0]!='FOM') # special case where FOM is registered as mtz type 'F'
   # AJM TODO don't know how to add miller arrays that are not as above
   # looks like you have to use add column with flex array of data
   # there might be an easier way (?)

def ma_mtz_labels(millers,k,ma):
  def labin(v):
    labels = copy.deepcopy(v.info().labels)
    label = labels[0]
    if len(labels) > 1 and  labels[1].startswith("_refln."):
      label = labels[1] # 0 is the crystal, 1 is the first column label
    label = label.strip()
    if label.endswith("(+)"):
      label = label.rstrip("(+)")
    if label.endswith("plus"):
      label = label.rstrip("plus")
    label = label.strip()
    maxlabwidth = int(20) # maximum is 30, will throw error on add_miller_array
    label = label.replace("_refln.intensity_","_refln.I_")
    label = label.replace("_refln.","") #eg _refln.F_meas
    if int(len(label)) > maxlabwidth:
      label = label[:maxlabwidth]
    return [label,labels]
  full_list = []
  for i,v in enumerate(millers): #k is external i
    full_list.append(labin(v)[0]) #for count
  current_list = full_list[:k]
  label,labels = labin(ma)
  if (full_list.count(label) > 1):
    label = label+str(current_list.count(label)+1) # assure uniqueness
  return label,labels


def g6_form(v):
    g6 = [0,0,0,0,0,0]

    # NOTE: G6 form defined as: [a^2, b^2, c^2, 2bccos(alpha), 2accos(beta), 2abcos(gamma)]
    g6[0] = v[0] ** 2
    g6[1] = v[1] ** 2
    g6[2] = v[2] ** 2
    g6[3] = 2 * v[1] * v[2] * math.cos(v[3])
    g6[4] = 2 * v[0] * v[2] * math.cos(v[4])
    g6[5] = 2 * v[0] * v[1] * math.cos(v[5])

    return g6

try :
    class Intensity_Correlation(object):
        """
        An Intensity correlation is a task that takes as input a dataset and

        Attributes:
            :param data1:
            :type data1:
            :param mtz1obj:
            :type mtz1obj:
            :param millers1:
            :type millers1:
            :param crystal1:
            :param unit_cell1:
            :param spg1:
            :param num1:
            :param laue1:
            :param num_laue1:

        """
        def __init__(self, data1, high_resolution,matches_cut_off,logger=None):
            self.high_resolution = high_resolution
            self.matches_cut_off = matches_cut_off
            self.data1 = data1
            if logger is None:
                logging.basicConfig(format='%(message)s',
                                    datefmt='%H:%M:%S',
                                    level=logging.DEBUG)
                self.logger = logging.getLogger("IC")
            else:
                self.logger = logger
            self.logger.setLevel(logging.ERROR) # turn off output
          # self.logger.setLevel(logging.INFO) # turn on output

            self.mtz1obj = any_file(self.data1)
            self.crystal1 = self.mtz1obj.crystal_symmetry()


            #AJM TODO
            #if the reference is a pdb file then there is NO REINDEXING
            self.niggli = None
            if self.mtz1obj.file_type == 'pdb':
                phil2 = mmtbx.programs.fmodel.master_phil
                xray_structure = self.mtz1obj.file_object.xray_structure_simple()
                self.crystal1 = self.mtz1obj.file_object.crystal_symmetry()
                params2 = phil2.extract()
                params2.high_resolution = self.high_resolution
                params2.low_resolution = 100
                params2.output.type = "real"
                f_model = mmtbx.utils.fmodel_from_xray_structure(
                    xray_structure = xray_structure,
                    f_obs          = None,
                    add_sigmas     = False,
                    params         = params2)
                mtz_dataset = f_model.f_model.as_mtz_dataset(column_root_label="FMODEL")
             #  array = f_model.f_model.as_amplitude_array()
                miller_arrays = mtz_dataset.mtz_object().as_miller_arrays()
            else:
                #have to move reference to niggli cell for doing the cc check
                miller_arrays = self.mtz1obj.file_object.as_miller_arrays()
             #  ncop = reindex.reindexing_operators(self.crystal1,self.crystal1.niggli_cell())
                mtz_dataset = None
                for k,ma in enumerate(miller_arrays):
                  if ma_selection(ma):
                    try:
                      label,labels = ma_mtz_labels(miller_arrays,k,ma)
                      cb_op = ma.change_of_basis_op_to_niggli_cell()
                      ma=ma.average_bijvoet_mates()
                      if not mtz_dataset:
                        mtz_dataset = ma.as_mtz_dataset(column_root_label=label)
                      else:
                        #2evr fails passing just ma to add_miller_array
                        mtz_dataset.add_miller_array(miller_array=ma, column_root_label=label)
                    except Exception as e:
                      message = "Cannot add miller array to reference " + str(labels)
                      self.logger.info(message)
                      self.logger.error(e)
                    # raise ValueError(enum_err_code.input,message)
                      continue
                if (mtz_dataset is None):
                  raise ValueError(enum_err_code.input,"No readable F/I data in file")
                mtz_object = mtz_dataset.mtz_object()
            ##  mtz_object.change_basis_in_place(cb_op=cb_op) ##
                miller_arrays = mtz_object.as_miller_arrays()
                self.crystal1 = self.crystal1.niggli_cell()
            #   mtz_object.show_summary()
            #   print(cb_op.as_hkl())

            self.millers1 = []

            labin = 1
            for k,ma in enumerate(miller_arrays):
                if ma_selection(ma):
                  try:
                    r = ma.d_max_min()[1]
                    label,labels = ma_mtz_labels(miller_arrays,k,ma)
                    before = ma.size()
                    maset = ma.resolution_filter(d_min=self.high_resolution)
                    maset.set_info(ma.info())
                    ma = maset
                    self.logger.info("resolution filter 1: " + str(before) + "-> " + str(ma.size()))
                    self.millers1.append({ "ma" : ma, "reso": r, "labels": labels, "labin": labin})
                    labin = labin + 1
                  except:
                    continue

            self.crystal1 = self.mtz1obj.crystal_symmetry()#2qcw requires
            self.unit_cell1 = self.crystal1.unit_cell().parameters()
            self.spg1 = self.crystal1.space_group_info().symbol_and_number()
            self.num1 = self.crystal1.space_group_number()
            self.laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().lookup_symbol()
            self.num_laue1 = self.crystal1.space_group().build_derived_laue_group().info().type().number()
            maxres = self.high_resolution
            maxdelta = 3.0

            #reference set is a loop over datasets and a loop over settings
            #check here to see if the data are actually lower resolution than CC request
            data_high_resolution = 10000.
            for mi1 in self.millers1:
                   data_high_resolution = min(data_high_resolution,mi1["reso"])
            self.high_resolution = max(self.high_resolution,data_high_resolution)

        def print_info_data(self):
            self.logger.info("1 Spacegroup: "+str(self.spg1))
            self.logger.info("1 Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell1)))
            self.logger.info("1 Laue: "+str(self.laue1)+" Num: "+str(self.num_laue1))

            if hasattr(self, "data2"):
                self.logger.info("2 Spacegroup: "+str(self.spg2))
                self.logger.info("2 Unit cell: "+str(cctbx.uctbx.unit_cell(self.unit_cell2)))
                self.logger.info("2 Laue: "+str(self.laue2)+" Num: "+str(self.num_laue2))

        def try_as_cif(self,data2):
          import iotbx.cif
          cif_file = iotbx.cif.reader(input_string=str(data2),strict=False)
          ma = cif_file.as_miller_arrays()
          cs = ma[0].crystal_symmetry()
          ma = cif_file.as_miller_arrays(crystal_symmetry=cs)
          return cs, ma,

        def compare(self, pdbid, data2, calculate_fmodel, thresh,verbose=False):
            self.logger.setLevel(logging.ERROR) # turn off output
          # self.logger.setLevel(logging.INFO) # turn on output
            self.data2 = data2
            self.millers2 = []
            self.niggli2 = None
            self.logger.info("calculate fmodel "+str(calculate_fmodel))
            if calculate_fmodel:
                try:
                   #str around self.data2 is essential or else 144d.cif is read as ncs type (!)
                   self.mtz2obj = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(data2)))
                except Exception as e:
                   message = "Cannot read coordinate file"
                   self.logger.info(message)
                   self.logger.error(e)
                   raise ValueError(enum_err_code.input,message)
                phil2 = mmtbx.programs.fmodel.master_phil
                xray_structure = self.mtz2obj.xray_structure_simple()
                self.crystal2 = self.mtz2obj.crystal_symmetry()
                params2 = phil2.extract()
                params2.high_resolution = self.high_resolution
                params2.low_resolution = 100
                params2.output.type = "real"
                f_model = mmtbx.utils.fmodel_from_xray_structure(
                    xray_structure = xray_structure,
                    f_obs          = None,
                    add_sigmas     = False,
                    params         = params2)
                mtz_dataset = f_model.f_model.as_mtz_dataset(column_root_label="FMODEL")
            #   array = f_model.f_model.as_amplitude_array()
                miller_arrays = mtz_dataset.mtz_object().as_miller_arrays()
                for ma in miller_arrays:
                  self.niggli2 = ma.change_of_basis_op_to_niggli_cell()
            else:
              # cifile = str(hash(str(pdbid + ".tmp.cif"))) + ".tmp.cif"
              # with open(cifile, "w") as fg:
              #   fg.write(data2)
              #   self.mtz2obj = any_file(str(cifile))
              #   os.remove(cifile) #delete after writing because this is a tmp file
              # self.crystal2 = self.mtz2obj.crystal_symmetry()
                #may be problem with space group
                self.crystal2, miller_arrays =  self.try_as_cif(data2)
             #  ncop = reindex.reindexing_operators(self.crystal2,self.crystal2.niggli_cell())
                mtz_dataset = None
                try:
                  for k,ma in enumerate(miller_arrays):
                    if ma_selection(ma):
                      try:
                        labels = None
                        label,labels = ma_mtz_labels(miller_arrays,k,ma)
                        cb_op = ma.change_of_basis_op_to_niggli_cell()
                        maset=ma.average_bijvoet_mates()
                        maset.set_info(ma.info())
                        ma = maset
                        if not mtz_dataset:
                          mtz_dataset = ma.as_mtz_dataset(column_root_label=label)
                        else:
                          mtz_dataset.add_miller_array(miller_array=ma,column_root_label=label)
                      except Exception as e:
                        message = "Testing miller array skipped: " + str(labels)
                        self.logger.info(message)
                        self.logger.error(e)
                      # raise ValueError(enum_err_code.input,message)
                        continue
                except Exception as e:
                  raise ValueError(enum_err_code.input,"No selected data in file")
                if mtz_dataset is None:
                  raise ValueError(enum_err_code.input,"No selected data in file")
                mtz_object = mtz_dataset.mtz_object()
            ##  mtz_object.change_basis_in_place(cb_op=cb_op) ##
                miller_arrays = mtz_object.as_miller_arrays()
                self.crystal2 = self.crystal2.niggli_cell()
             #  mtz_object.show_summary()
                self.niggli2 = cb_op

            labin = 1
            for k,ma in enumerate(miller_arrays):
                if ma_selection(ma):
                  try:
                    labels = None
                    r = ma.d_max_min()[1]
                    before = ma.size()
                    label,labels = ma_mtz_labels(miller_arrays,k,ma)
                    self.logger.info("label info 2: " + str(labels) + "-> " + str(label))
                    maset = ma.resolution_filter(d_min=self.high_resolution)
                    maset.set_info(ma.info())
                    ma = maset
                    self.logger.info("resolution filter 2: " + str(before) + "-> " + str(ma.size()))
                    self.millers2.append({ "ma" : ma, "reso": r, "labels": labels, "labin": labin})
                    labin = labin + 1
                  except:
                        message = "Testing miller2 array skipped: " + str(labels)
                        self.logger.info(message)
                        self.logger.error(e)
                      # raise ValueError(enum_err_code.input,message)
                        continue
            self.unit_cell2 = self.crystal2.unit_cell().parameters()
            self.cell2 = self.crystal2.unit_cell()
            self.spg2 = self.crystal2.space_group_info().symbol_and_number()
            self.num2 = self.crystal2.space_group_number()
            self.num_laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().number()
            self.laue2 = self.crystal2.space_group().build_derived_laue_group().info().type().lookup_symbol()

            self.print_info_data()

            # IS = []
            # IP = []
         #  self.miller_indexed = None
            self.cc = None
            self.operation = None
            self.crystal_match = None
            self.niggli_match = None
            maxcc = 0
         #  self.all_millers_indexed = []
            result = []
            for i, mi1 in enumerate(self.millers1):
                # if i>0: break
                self.reso = mi1["reso"]
                self.logger.info("  --------------")
                self.logger.info("  : "+str(mi1["labels"]))
                self.logger.info("  Resolution: "+str(round(mi1["reso"],3)))
                m1d = mi1["ma"].map_to_asu()
                for mi2 in self.millers2:
                    try:
                      from phasertng.scripts.jiffy import TextIO
                      outstr = TextIO()
                      self.reference_analyses = pair_analyses.reindexing(
                               mi1["ma"],
                               mi2["ma"],
                               file_name=self.data2,
                               out=outstr)
                      lines = outstr.getvalue().split("\n")
                      for line in lines:
                        self.logger.info(str(line.lstrip()))
                      nops = len(self.reference_analyses.nice_cb_ops)
                      self.logger.info("+++++++++")
                      self.logger.info(": "+str(mi2["labels"]))
                      self.logger.info("Resolution: "+str(round(mi2["reso"],3)))
                      self.logger.info("Number of ops " + str(nops))
                      for o in range(nops):
                            # if t>0: break
                            self.logger.info("reindexing op "+str(o+1))
                            self.logger.info("reindexing op "+str(self.reference_analyses.nice_cb_ops[o].as_hkl()))
                            cc = 0
                            if self.reference_analyses.matches[o] > self.matches_cut_off: # this is the matches_cut_off in pair_analyses.py
                              cc = self.reference_analyses.cc_values[o]
                            self.logger.info(str(cc))
                            self.logger.info(str(mi2["labels"]))
                            self.logger.info(str(round(mi2["reso"],2)))
                            self.logger.info(str("number cc " + str(self.reference_analyses.number_in_cc[o])))
                            result.append(
                                {
                                 "miller1":i+1,
                                 "miller2":mi2["labin"],
                                 "cc":self.reference_analyses.cc_values[o],
                                 "matches":self.reference_analyses.matches[o],
                                 "number_in_cc":self.reference_analyses.number_in_cc[o],
                                 "nref": self.reference_analyses.set_a.size(), # selection1.size(),
                                 "labels":mi2["labels"],
                                 "reso":mi2["reso"],
                                 "labin":mi2["labin"],
                                 "sg":self.spg2,
                                 "niggli":self.niggli2,
                                 "reindex":self.reference_analyses.nice_cb_ops[o].as_hkl()}) #op["hkl"]})
                            self.logger.info("len results " + str(len(result)))
                            self.logger.info("    +++++++++")
                            if cc >= maxcc:
                                self.cc = cc
                                self.operation = self.reference_analyses.nice_cb_ops[o].as_hkl() # op["hkl"]
                                self.crystal_match = self.crystal2
                                self.niggli_match = self.niggli2
                                maxcc = cc
                    except:
                            # import traceback
                            # print(sys.exc_info())
                            # traceback.print_exc(file=sys.stdout)
                            continue
                self.logger.info("  --------------")

            if maxcc < thresh:
                return False, result
            else:
                return True, result

        def match_reflections(self, ma1, ma2):
            I1 = ma1.data()
            H1 = ma1.indices()
            I2 = ma2.data()
            H2 = ma2.indices()

            lookup = {}
            for i, h in enumerate(H1):
                lookup[h] = i
            selection1 = flex.size_t()
            selection2 = flex.size_t()

            for i, h in enumerate(H2):
                if h in lookup:
                    selection1.append(lookup[h])
                    selection2.append(i)

            return selection1, I1, selection2, I2
except:
    pass
