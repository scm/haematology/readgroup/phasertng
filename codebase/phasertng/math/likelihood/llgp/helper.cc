//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/llgp/helper.h>
#include <scitbx/constants.h>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
}

using namespace scitbx;

namespace phasertng { namespace likelihood { namespace llgp {

  double
  DobsSigaSqr(
      double SSQR,double SigaSqr,double FS,double DOBS,double DRMS,double BFAC)
  {
    double thisDobsSigaSqr(0);
    double minusSsqrOn2 = -SSQR/2.0;
    double minusSsqrTwoPiSqrOn3 = -(constants::two_pi_sq/3.0)*SSQR;
    thisDobsSigaSqr = SigaSqr;
    thisDobsSigaSqr *= FS; //SCATTERING[modlid]/(TS);
    thisDobsSigaSqr *= fn::pow2(DOBS);
    thisDobsSigaSqr *= std::exp(2*minusSsqrTwoPiSqrOn3*DRMS); //Vrms-shift term squared
    thisDobsSigaSqr *= std::exp(minusSsqrOn2*BFAC); //this is I/F
    return thisDobsSigaSqr;
  }

  std::complex<double>
  EcalcTerm(
      double DobsSigaSqr,double EPS,double NSYMP,millnx SymHKL,dvect3 TRA,double symTra)
  {
    double scalefac = DobsSigaSqr;
           scalefac *= EPS;
           scalefac /= NSYMP;
           scalefac = std::sqrt(scalefac);
    double dphi_arg = SymHKL*TRA;
           dphi_arg += symTra;
    cmplex scaled_dphi = tbl_cos_sin.get(dphi_arg);
    scaled_dphi *= scalefac;
    return scaled_dphi;
  }

  double
  EcalcSqrTerm(
      double DobsSigaSqr,double EPS,double NSYMP)
  {
    double scalefac = DobsSigaSqr;
           scalefac *= EPS;
           scalefac /= NSYMP;
           //scalefac = std::sqrt(scalefac); //Ecalc term
    return scalefac;
  }

}}} //phasertng
