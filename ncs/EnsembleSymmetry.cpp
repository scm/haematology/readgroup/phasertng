#include "EnsembleSymmetry.h"
#include "ReferenceChain.h"
#include <cassert>

#include <scitbx/math/superpose.h>

using namespace iotbx::pdb::hierarchy;

namespace phaser {  namespace ncs {

//
// class EnsembleSymmetry
//

// Constructors
EnsembleSymmetry::EnsembleSymmetry(
        float min_coverage,
        float min_identity,
        float max_rmsd,
        double angular_tolerance,
        double spatial_tolerance )
    : assemblies_up_to_date_( false ),
        min_seq_coverage_( min_coverage ),
        min_seq_identity_( min_identity ),
        max_rmsd_( max_rmsd ),
        angular_( angular_tolerance ),
        spatial_( spatial_tolerance )
{
}


// Destructor
EnsembleSymmetry::~EnsembleSymmetry()
{
}

// Calculation
PointGroup
EnsembleSymmetry::get_point_group()
{
    const std::vector< NCSAssembly >& assemblies = get_assemblies();

    // No checkable symmetry
    if ( assemblies.size() == 0 )
    {
        return PointGroup::from_operators(
            std::vector< NCSOperator >(),
            angular_,
            spatial_
            );
    }

    // Transform assemblies to centre-of-mass
    scitbx::vec3< double > centre = get_centre_of_assembly();
    std::vector< NCSSymmetry > symmetries;

    for (
        std::vector< NCSAssembly >::const_iterator asit = assemblies.begin();
        asit != assemblies.end();
        ++asit
        )
    {
        const std::vector< NCSOperator >& ops = asit->get_operators();
        std::vector< NCSOperator > transformed;

        for (
            std::vector< NCSOperator >::const_iterator opit = ops.begin();
            opit != ops.end();
            ++opit
            )
        {
            const scitbx::mat3< double >& rot = opit->get_rotation();
            scitbx::vec3< double > tr = opit->get_translation() + rot * centre
                - centre;
            transformed.push_back( NCSOperator( rot, tr ) );
        }

        symmetries.push_back(
            NCSSymmetry( transformed, angular_, spatial_ )
            );
    }

    // Determine point group of assemblies
    std::vector< PointGroup > pgs;

    for (
        std::vector< NCSSymmetry >::const_iterator it = symmetries.begin();
        it != symmetries.end();
        ++it
        )
    {
        pgs.push_back(
            PointGroup::from_operators(
                it->get_operators(),
                angular_,
                spatial_
                )
            );
    }

    // Determine common operators
    std::vector< NCSOperator > common_ops = pgs[0].get_operators();

    for (
        std::vector< PointGroup >::const_iterator it = pgs.begin() + 1;
        it != pgs.end();
        ++it
        )
    {
        common_ops = it->intersect( common_ops );
    }

    // Find candidate point group
    return PointGroup::from_operators( common_ops, angular_, spatial_ );
}

// Accessors
const root&
EnsembleSymmetry::get_root() const
{
    return root_;
}


const std::vector< NCSAssembly >&
EnsembleSymmetry::get_assemblies()
{
    if ( !assemblies_up_to_date_ )
    {
        determine_assemblies();
    }

    return assemblies_;
}


float
EnsembleSymmetry::get_min_sequence_coverage() const
{
    return min_seq_coverage_;
}


float
EnsembleSymmetry::get_min_sequence_identity() const
{
    return min_seq_identity_;
}


float
EnsembleSymmetry::get_max_rmsd() const
{
    return max_rmsd_;
}


double
EnsembleSymmetry::get_angular_tolerance() const
{
    return angular_;
}


double
EnsembleSymmetry::get_spatial_tolerance() const
{
    return spatial_;
}

// Setters
void
EnsembleSymmetry::set_root(const root& root)
{
    assemblies_up_to_date_ = false;
    root_ = root;
}


void
EnsembleSymmetry::set_min_sequence_coverage(float value)
{
    assemblies_up_to_date_ = false;
    min_seq_coverage_ = value;
}


void
EnsembleSymmetry::set_min_sequence_identity(float value)
{
    assemblies_up_to_date_ = false;
    min_seq_identity_ = value;
}


void
EnsembleSymmetry::set_max_rmsd(float value)
{
    assemblies_up_to_date_ = false;
    max_rmsd_ = value;
}


void
EnsembleSymmetry::set_angular_tolerance(double value)
{
    angular_ = value;
}


void
EnsembleSymmetry::set_spatial_tolerance(double value)
{
    spatial_ = value;
}

// Private methods
void
EnsembleSymmetry::determine_assemblies()
{
  // Initialize static members of class
  const MacromoleculeType unknown('X');

    std::vector< model > models = root_.models();
    assemblies_.clear();

    if ( models.size() == 0 )
    {
        return;
    }

    std::vector< ReferenceChain > references;
    const size_t MIN_COMMON_LENGTH = 10;
    const size_t MIN_ATOMS_FOR_SUPERPOSITION = 3;

    for (
        std::vector< chain >::const_iterator it = models[0].chains().begin();
        it != models[0].chains().end();
        ++it
        )
    {
        MacromoleculeType mmt;// = MacromoleculeType::get_chain_type( *it );
        mmt.get_chain_type( *it );

        if ( mmt.get_type_name() == unknown.get_type_name() )
        {
            continue;
        }

        bool found = false;
        assert( references.size() == assemblies_.size() );

        for ( size_t index = 0; index < assemblies_.size(); ++index )
        {
            const ReferenceChain& ref = references[ index ];
            const MacromoleculeType ref_mmt = ref.get_macromolecule_type();

            if ( mmt.get_type_name() != ref_mmt.get_type_name() )
            {
                continue;
            }

            std::pair< std::string, long > match_info =
                ref.determine_numbering_shift( *it );

            if ( match_info.first.size() < MIN_COMMON_LENGTH )
            {
                continue;
            }

            ChainAlignment alignment = ChainAlignment(
                ref.get_chain(),
                *it,
                match_info.second
                );

            if ( alignment.get_sequence_coverage() < min_seq_coverage_
                || alignment.get_sequence_identity() < min_seq_identity_ )
            {
                continue;
            }

            const std::set< iotbx::pdb::str4 >& names =
                ref_mmt.get_backbone_atom_names();
            std::vector< std::pair< atom, atom > > atom_alignment =
                alignment.get_atom_alignment();
            scitbx::af::shared< scitbx::vec3< double > > first_coords;
            scitbx::af::shared< scitbx::vec3< double > > second_coords;

            for (
                std::vector< std::pair< atom, atom > >::const_iterator at =
                    atom_alignment.begin();
                at != atom_alignment.end();
                ++at
                )
            {
                assert( at->first.data->name == at->second.data->name );

                if ( names.find( at->first.data->name ) != names.end() )
                {
                    first_coords.push_back( at->first.data->xyz );
                    second_coords.push_back( at->second.data->xyz );
                }
            }

            assert( first_coords.size() == second_coords.size() );

            if ( first_coords.size() < MIN_ATOMS_FOR_SUPERPOSITION )
            {
                continue;
            }

            scitbx::math::superpose::least_squares_fit< double >
                superposition( second_coords, first_coords );

            if ( max_rmsd_ < superposition.other_sites_rmsd() )
            {
                continue;
            }

            assemblies_[ index ].insert(
                NCSOperator(
                    superposition.get_rotation(),
                    superposition.get_translation()
                    ),
                *it
                );

            found = true;
            break;
        }

        if ( ! found )
        {
            references.push_back( ReferenceChain( *it, mmt ) );
            assemblies_.push_back( NCSAssembly( *it, angular_, spatial_ ) );
        }
    }

    assemblies_up_to_date_ = true;
}


scitbx::vec3< double >
EnsembleSymmetry::get_centre_of_assembly() const
{
    scitbx::af::shared< atom > atoms = root_.atoms();

    if ( atoms.size() == 0 )
    {
        return scitbx::vec3< double >( 0, 0, 0 );
    }

    //scitbx::af::const_ref< atom > atoms_ref = atoms.const_ref();
    std::vector< scitbx::vec3< double > > atom_coordinates;

    for (
            scitbx::af::const_ref< atom >::const_iterator it = atoms.begin();
            it != atoms.end();
            ++it )
    {
        atom_coordinates.push_back( it->data->xyz );
    }

    return scitbx::af::mean( scitbx::af::make_const_ref( atom_coordinates ) );
}

}   } // namespace phaser::ncs
