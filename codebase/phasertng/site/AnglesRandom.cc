//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/AnglesRandom.h>

namespace phasertng {

  AnglesRandom::AnglesRandom(int nrand_,unsigned seed_) :
      SiteList(),
      Size(nrand_)
  {
    restart(seed_);
  }

  void
  AnglesRandom::restart(unsigned seed_)
  {
    //reset generator
    generator = scitbx::random::mersenne_twister(seed_);
    inext = 0;
  }

  bool
  AnglesRandom::at_end()
  {
    return inext >= Size;
  }

  dvect3
  AnglesRandom::next_site()
  {
    //Generate random angles through sampling of quaternions
    dmat33 random_rotmat = generator.random_double_r3_rotation_matrix();
    dvect3 Site = scitbx::math::euler_angles::zyz_angles(random_rotmat);
    inext++;
    return Site;
  }

  af_dvect3
  AnglesRandom::all_sites()
  {
    af_dvect3 siteList;
    for (int i = 0; i < Size; i++)
    {
      //Generate random angles through sampling of quaternions
      dmat33 random_rotmat = generator.random_double_r3_rotation_matrix();
      dvect3 Site = scitbx::math::euler_angles::zyz_angles(random_rotmat);
      inext++;
      siteList.push_back(Site);
    }
    return siteList;
  }

}// phasertng
