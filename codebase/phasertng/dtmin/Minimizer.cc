//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/math/RealSymmetricPseudoInverse.h>
#include <phasertng/main/Output.h>
#include <phasertng/math/vec_ops.h>
#include <phasertng/math/mat_ops.h>
#include <tuple>

namespace phasertng {
namespace dtmin {

//-----------------------------------------------------------------------
// printf - printf to string
//-----------------------------------------------------------------------
Minimizer::Minimizer(OutputPtr output) : output_(output)
{
  func_count_ = 0;
  write_debuggg = true; //but hidden behind compilation flag
  write_testing = (output_->Level() >= out::TESTING);
  if (write_testing) write_logfile = true;
}

void
Minimizer::set_logfile_level(out::stream where)
{
  write_logfile = (output_->Store() >= where);
  logfile = where;
}

void
Minimizer::run(
    RefineBase&                  TARGET,
    const std::vector<sv_string> PROTOCOL,
    const std::vector<int>       NCYC,
    const std::string            MINIMIZER,
    const bool                   STUDY_PARAMS,
    const double                 SMALL_TARGET)
{
  // Run the minimization for refine object TARGET
  iteration_limit_reached = false;

  check_input(PROTOCOL, NCYC, MINIMIZER);

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTabArray(debuggg,TARGET.initial_statistics());
  }
#endif

  for (int macro_cycle = 0; macro_cycle < PROTOCOL.size(); macro_cycle++)
  {
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"MACROCYCLE #" + std::to_string(macro_cycle+1) + " OF " + std::to_string(PROTOCOL.size()));
    }
#endif

    TARGET.set_macrocycle_protocol(PROTOCOL[macro_cycle]);
    if (TARGET.nmp == 0)
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"No parameters to refine this macrocycle. Skipping.");
        output_->logBlank(debuggg);
      }
#endif
      continue; // skip this macrocycle
    }

    {
    auto txt = TARGET.setup_parameters();
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTabArray(debuggg,txt);
    }
#endif
    }

    parameter_names_ = TARGET.macrocycle_parameter_names();
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"Parameters:");
      for (int i = 0; i < parameter_names_.size(); i++)
        output_->logTab(debuggg,"Refined Parameter #:" + std::to_string(i+1) + " " + parameter_names_[i]);
    }
#endif

    {
    auto txt = TARGET.reject_outliers();
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTabArray(debuggg,txt);
    }
#endif
    }

    if (write_logfile)
      output_->logEllipsisOpen(logfile,"Performing Optimisation");
    PHASER_ASSERT(TARGET.total_number_of_parameters_ > 0);

    bool calc_new_hessian(true);           // for bfgs, to be done in next cycle
    bool queue_new_hessian(false);         // for bfgs, flag to recalculate instead of converging
    int ncyc_since_hessian_calc(0);        // for bfgs
    sv_double previous_x(TARGET.nmp);      // for bfgs
    sv_double previous_g(TARGET.nmp);      // for bfgs
    TARGET.previous_h_inverse_approximation_.resize(af::flex_grid<>(TARGET.nmp,TARGET.nmp)); // for bfgs
    std::string termination_reason("");


    double f(0.),initial_f(0.),previous_f(0.),required_decrease(0.);
    const double EPS(1.E-10), FTOL(1.E-6), TWO(2.0);
    bool too_small_shift(false), too_small_decrease(false), zero_gradient(false);

    output_->logTab(testing,"Target");
    f = initial_f = previous_f = TARGET.target();
    if (write_testing)
    {
      output_->logBlank(testing);
      output_->logTab(testing,"Optimisation statistics, macrocycle #" + std::to_string(macro_cycle+1));
      output_->logTab(testing,snprintftos("Cycle %-4s %15s %18s %18s","","end-this-cycle","change-from-start","change-from-last"));
      output_->logTab(testing,snprintftos("Cycle#%-4d %15.2f %18s %18s",0,-initial_f,"",""));
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTabArray(debuggg,TARGET.current_statistics());
      }
#endif
    }

    ///////////////////////////////////////////////// MINIMIZATION CYCLES ////////////////////////////////
    int cycle = 0;
    if (NCYC[macro_cycle]-1 > 0)
    {
    for (;; cycle++)
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"MACROCYCLE #" + std::to_string(macro_cycle+1) + ": CYCLE #" + std::to_string(cycle+1));
      }
#endif
      PHASER_ASSERT(check_in_bounds(TARGET,parameter_names_));

      too_small_shift = too_small_decrease = zero_gradient = false; // reset
      required_decrease = FTOL*(std::fabs(f)+std::fabs(previous_f)+EPS)/TWO; // to not converge this cycle
      required_decrease = std::max(SMALL_TARGET,required_decrease);
      previous_f = f;

      if (MINIMIZER == "bfgs")
      {
        std::tie(f,ncyc_since_hessian_calc,queue_new_hessian,too_small_shift,zero_gradient,previous_x,previous_g) = \
            bfgs(
                 TARGET,
                 calc_new_hessian,         // input: tells bfgs to calculate a new hessian
                 queue_new_hessian,        // modified: updated if new hessian will be needed eventually
                 ncyc_since_hessian_calc,  // modified: reset or incremented by bfgs
                 required_decrease,        // input: for the line search
                 previous_x,               // input: for bfgs to update h_inverse_approximation
                 previous_g);              // input and modified: for bfgs to update h_inverse_approximation
#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTabArray(debuggg,format_hessian(TARGET.previous_h_inverse_approximation_, parameter_names_));
        }
#endif
      }
      else if (MINIMIZER == "newton")
        std::tie(f, too_small_shift) = \
            newton(TARGET,
                   required_decrease);    // for the line search

#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTabArray(debuggg,TARGET.current_statistics());
      }
#endif
      if (write_testing)
      {
        double change(f-previous_f);
        if (std::abs(change) < 1.0e-11) change = -1.0e-11; //fudge instability in print limit
        if (std::abs(change) > 0.01) //print more decimal places
        output_->logTab(testing,snprintftos("Cycle#%-4d %15.2f %18.2f %18.2f",cycle+1,-f,-(f-initial_f),-change));
        else
        output_->logTab(testing,snprintftos("Cycle#%-4d %15.2f %18.2f %18.10f",cycle+1,-f,-(f-initial_f),-change));
      }

      too_small_decrease = (previous_f - f < required_decrease);

      std::tie(termination_reason, calc_new_hessian) =
      check_termination_criteria(
                          too_small_shift,
                          too_small_decrease,
                          zero_gradient,
                          queue_new_hessian,
                          ncyc_since_hessian_calc,
                          cycle,
                          NCYC[macro_cycle]);

      //string is empty for continuation
      if (termination_reason.size() != 0)
      {
        break;
      }

    }
    }
    ///////////////////////////////////////////////// END OF MINIMIZATION CYCLES /////////////////////////

    if (write_logfile)
      output_->logEllipsisShut(logfile);
    if (cycle == NCYC[macro_cycle]-1)
    {
      iteration_limit_reached = true;
      if (write_logfile)
        output_->logTab(logfile,"--- Iteration limit reached at cycle " + std::to_string(NCYC[macro_cycle]) + " ---");
    }
    else
    {
      iteration_limit_reached = false;
      if (write_logfile)
        output_->logTab(logfile,"--- Convergence before iteration limit (" + std::to_string(NCYC[macro_cycle]) +") at cycle " + std::to_string(cycle+1) + " ---");
    }
    if (write_logfile)
    {
      output_->logTab(logfile,"Start-this-macrocycle End-this-macrocycle Change-this-macrocycle");
      output_->logTab(logfile,snprintftos("%13.2f %21.2f %21.2f",-initial_f,-f,-(f-initial_f)));
      output_->logBlank(logfile);
    }
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTabArray(debuggg,TARGET.current_statistics());
    }
#endif

    // study behaviour of function, gradient and curvature as parameters varied around minimum
    // using mathematica file studyParams_short.nb
    if (STUDY_PARAMS and NCYC[macro_cycle] > 0)
    {
      TARGET.study_parameters();
      if (NCYC[macro_cycle] == 0)
        if (write_logfile)
          output_->logTab(logfile,"No study parameters because no cycles of refinement");
    }

    {
    auto txt = TARGET.cleanup_parameters(); //clean up ready for next macrocycle
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTabArray(debuggg,txt);
    }
#endif
    }
  } // end of macrocycle loop

  auto txt = TARGET.finalize_parameters();
#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTabArray(debuggg,txt);
    output_->logTabArray(debuggg,TARGET.final_statistics());
  }
#endif
}

void
Minimizer::check_input(
    std::vector<sv_string> PROTOCOL,
    std::vector<int>       NCYC,
    std::string            MINIMIZER)
{
  // Check that the PROTOCOL, NCYC and MINIMSER arguments are valid

  if (PROTOCOL.size() == 0)
    throw Error(err::FATAL,"No PROTOCOL for refinement");

  if (PROTOCOL.size() != NCYC.size())
    throw Error(err::FATAL,"Number of macrocyles in ncyc does not match number in protocol");

  bool some_refinement = false;
  for (int macro_cycle = 0; macro_cycle < PROTOCOL.size(); macro_cycle++)
  {
    if (PROTOCOL[macro_cycle].size() != 0)
    {
      some_refinement = true;
      break;
    }
  }

  if (some_refinement == false)
  {
    if (write_logfile)
    {
      output_->logTab(logfile,"No refinement of parameters");
      output_->logBlank(logfile);
    }
    return;
  }

  if (MINIMIZER != "bfgs" and MINIMIZER != "newton")
    throw Error(err::FATAL,"MINIMIZER value: \"" + MINIMIZER + "\" is invalid");

  for (auto ncyc : NCYC)
  if (ncyc < 0)
    throw Error(err::FATAL,"The maximum number of microcycles per macrocycle must be non-negative");
}

bool
Minimizer::check_in_bounds(RefineBase& TARGET,sv_string parameter_names)
{
  auto bounds(TARGET.reparameterized_bounds());
  if (bounds.size() == 0)
    return true;
  else
  {
    auto x(TARGET.get_reparameterized_parameters());
    auto macrocycle_large_shifts(TARGET.reparameterized_large_shifts());
    double TOL = 1.e-03; // Tolerance for deviation in bound as fraction of LargeShift
    PHASER_ASSERT(x.size() == macrocycle_large_shifts.size());
    PHASER_ASSERT(x.size() == bounds.size());
    for (int i=0; i<x.size(); i++)
    {
      double thistol = macrocycle_large_shifts[i]*TOL;
      if (  ( bounds[i].lower_bounded() && (x[i] < (bounds[i].lower_limit() - thistol) ) )
          | ( bounds[i].upper_bounded() && (x[i] > (bounds[i].upper_limit() + thistol) ) )
        )
      {
        throw Error(err::DEVELOPER,"Parameter #"  + std::to_string(i+1)  +
          " (" + parameter_names[i] +
          ") (Lower: " + on_off(bounds[i].lower_bounded()) +
          " " + std::to_string(bounds[i].lower_limit()) +
          ") (Value: " + std::to_string(x[i]) +
          ") (Upper: " + on_off(bounds[i].upper_bounded()) +
          " " + std::to_string(bounds[i].upper_limit()) + ")");
        return false;
      }
    }
    return true;
  }
}

//<f, ncyc_since_hessian_calc, queue_new_hessian, too_small_shift, zero_gradient, x, g>
std::tuple<double, int, bool, bool, bool, sv_double, sv_double>
Minimizer::bfgs(
    RefineBase& TARGET,
    bool        calc_new_hessian,         // tells bfgs to calculate a new hessian
    bool        queue_new_hessian,        // set when new hessian should be calculated instead of converging
    int         ncyc_since_hessian_calc,  // reset or incremented by bfgs
    double      required_decrease,        // for the line search and used to decide whether to retry the newton step
    sv_double&  previous_x,               // for bfgs to update h_inverse_approximation
    sv_double&  previous_g)               // for bfgs to update h_inverse_approximation
{
  // Move the refinable parameters by one bfgs step

  bool too_small_shift(false);
  bool zero_gradient(false);
  sv_double x(TARGET.get_reparameterized_parameters());
  sv_double g(TARGET.nmp);
  versa_flex_double   h_inverse_approximation(af::flex_grid<>(TARGET.nmp,TARGET.nmp), 0.); // for bfgs
  const double min_dx_over_esd(0.1);
  double minimum_distance = 1.E-05;
  double f(0.);

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,"== BFGS ==");
  }
#endif

  if (calc_new_hessian)
  {
    ncyc_since_hessian_calc = 0;
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"== Newton Step ==");
      output_->logTab(debuggg,"BFGS: Gradient and New Hessian");
      output_->logEllipsisOpen(debuggg,"BFGS: Calculating Gradient and New Hessian");
    }
#endif

    TargetGradientHessianType f_g_h; bool queue_new_hessian;
    std::tie(f_g_h,queue_new_hessian) = TARGET.reparameterized_adjusted_target_gradient_hessian();
    double             starting_f = f_g_h.Target;
                                g = f_g_h.Gradient;
    versa_flex_double           h = f_g_h.Hessian.deep_copy(); // does this need to be a deep copy ???
    bool      is_hessian_diagonal = f_g_h.Diagonal;
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      if (queue_new_hessian)
        output_->logTab(debuggg, "BFGS: Hessian not pos def: restart with new Hessian");
      output_->logEllipsisShut(debuggg);
      output_->logBlank(debuggg);
      output_->logTabArray(debuggg,format_vector( "BFGS::Newton: Gradient", g, parameter_names_));
      output_->logBlank(debuggg);
      output_->logTabArray(debuggg,format_hessian( h, parameter_names_));
      output_->logBlank(debuggg);
    }
#endif

    if (math::IsVecZero(g))
    {
      queue_new_hessian = false; // Already at minimum so no point in new Hessian
      zero_gradient = true;
      return std::make_tuple(starting_f, ncyc_since_hessian_calc, too_small_shift, queue_new_hessian, zero_gradient, x, g);
    }

    // Set up to repeat perturbed newton step if insufficient progress
    double hiiRMS = hessian_diag_rms(h);
    int filtered_last(0);
    int maxtry = std::min(3,TARGET.nmp);
    f = starting_f;
    previous_x = x;
    double rs(0.); // Repeat only while required shift is small compared to initial step
    for (int ntry=0; (ntry < maxtry) && (starting_f-f < required_decrease)
      && (filtered_last < TARGET.nmp) && (rs < 1.); ntry++)
    {
      if (is_hessian_diagonal)
        for (int i=0; i<g.size(); i++)
          h_inverse_approximation(i,i) = 1/(h(i,i)+ntry*hiiRMS);
      else
      {
        int min_to_filter(0);
        if (ntry>0)
        {
          min_to_filter = filtered_last + std::max(1,TARGET.nmp/10);
          if (min_to_filter >= TARGET.nmp)
            min_to_filter = std::min(filtered_last+1, TARGET.nmp-1);
        }
        int filtered(0);
        h_inverse_approximation = math::RealSymmetricPseudoInverse(h,filtered,min_to_filter); // h_inverse_approximation is now the inverse

#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          if (filtered)
            output_->logTab(debuggg,snprintftos("filtered %i small eigenvectors",filtered));
          output_->logTab(debuggg,"Pseudoinverse Hessian");
          output_->logTabArray(debuggg,format_hessian(h_inverse_approximation,parameter_names_));
        }
#endif
        filtered_last = filtered;
      }

      sv_double p = math::neg_mat_vec_mult(h_inverse_approximation, g);
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTabArray(debuggg,format_vector("BFGS: Parameters",x,parameter_names_));
        output_->logTabArray(debuggg,format_vector("BFGS::Newton: Search Vector",p,parameter_names_));
        output_->logBlank(debuggg);
      }
#endif

      // Figure out how far linesearch has to go to shift at least one parameter
      // by (minimum shift/esd) * esd, using inverse Hessian to estimate covariance matrix
      rs = required_shift(min_dx_over_esd, p, h_inverse_approximation);
      double line_search_distance;
      std::tie(f, line_search_distance) = line_search(TARGET,x,f,g,p,1.,minimum_distance,required_decrease);
      if (line_search_distance < rs) too_small_shift = true;
    }
    TARGET.previous_h_inverse_approximation_ = h_inverse_approximation;
  }
  else
  {
    ncyc_since_hessian_calc += 1;

#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"BFGS: Gradient");
      output_->logEllipsisOpen(debuggg,"BFGS: Calculating Gradient");
    }
#endif
    auto f_g = TARGET.reparameterized_target_gradient();
    f = f_g.Target;
    g = f_g.Gradient;

    if (math::IsVecZero(g))
    {
      queue_new_hessian = false;
      zero_gradient = true;
      return std::make_tuple(f, ncyc_since_hessian_calc, queue_new_hessian, too_small_shift, zero_gradient, x, g);
    }

    sv_double dx(math::vec_minus(x, previous_x));
    sv_double dg(math::vec_minus(g, previous_g));
    sv_double Hdg(math::mat_vec_mult(TARGET.previous_h_inverse_approximation_, dg));
    sv_double u(TARGET.nmp);
    double dx_dot_dg(math::dot_prod(dx,dg));
    double dg_dot_Hdg(math::dot_prod(dg,Hdg));

    if (dx_dot_dg <= 0)  // Shouldn't happen, but we're cutting corners on line search convergence
    {                    // i.e. Wolfe condition considering new gradient
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"BFGS: dx_dot_dg <= 0: restart with new Hessian");
      }
#endif
      queue_new_hessian = true;
      too_small_shift = true;
      return std::make_tuple(f, ncyc_since_hessian_calc, queue_new_hessian, too_small_shift, zero_gradient, x, g);
    }
    PHASER_ASSERT(dg_dot_Hdg);
    // BFGS update of the approximation to the inverse hessian
    // see Numerical Recipes in Fortran, Second Ed. p. 420
    // h_inverse_approximation contains the approximation to the inverse hessian
    for (int i = 0; i < g.size(); i++)
      u[i] = dx[i]/dx_dot_dg - Hdg[i]/dg_dot_Hdg;
    for (int i = 0; i < g.size(); i++)
      for (int j = 0; j < g.size(); j++)
        h_inverse_approximation(i,j) = TARGET.previous_h_inverse_approximation_(i,j) + dx[i]*dx[j]/dx_dot_dg - Hdg[i]*Hdg[j]/dg_dot_Hdg + dg_dot_Hdg*u[i]*u[j];

#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logBlank(debuggg);
      output_->logTabArray(debuggg,format_hessian(h_inverse_approximation, parameter_names_));
      output_->logBlank(debuggg);
    }
#endif
    sv_double p = math::neg_mat_vec_mult(h_inverse_approximation, g);
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTabArray(debuggg,format_vector("BFGS: Parameters",x,parameter_names_));
      output_->logTabArray(debuggg,format_vector("BFGS: Search Vector",p,parameter_names_));
      output_->logBlank(debuggg);
    }
#endif

    //Check for non-negative grad before attempting linesearch
    if (math::dot_prod(p,g) >= 0.)
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg, "BFGS: p.g >= 0: restart with new Hessian");
      }
#endif
      queue_new_hessian = true;
      too_small_shift = true;
    }
    else
    {
      // Figure out how far linesearch has to go to shift at least one parameter
      // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
      double rs = required_shift(min_dx_over_esd, p, h_inverse_approximation);
      double line_search_distance;
      std::tie(f, line_search_distance) = line_search(TARGET,x,f,g,p,1,minimum_distance,required_decrease);
      if (line_search_distance < rs) too_small_shift = true;
      if (line_search_distance < minimum_distance)
      {
        queue_new_hessian = true;
#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTab(debuggg, "BFGS: distance < minimum: restart with new Hessian");
        }
#endif
      }
    }

    TARGET.previous_h_inverse_approximation_ = h_inverse_approximation;
  }
  return std::make_tuple(f, ncyc_since_hessian_calc, queue_new_hessian, too_small_shift, zero_gradient, x, g);
}

//<f, too_small_shift>
std::pair<double, bool>
Minimizer::newton(
    RefineBase& TARGET,
    double      required_decrease)
{
  // Move the refinable parameters by one newton step

  bool too_small_shift(false);
  double f(0.);
  double minimum_distance(1.E-05);
  sv_double x(TARGET.get_reparameterized_parameters());
  const double min_dx_over_esd(0.1);
  // ESD is the estimated standard deviation. Why we are considering standard deviations at this
  // point in the code may not be immediately obvious.
  // The naming only really makes snse when we are minimising a log likelihood function
  // If the likelihood can locally be approxmated by a
  // multivatriate normal distribution, then the log likelihood may be approximated by a multidimensional
  // quadratic with its matrix being the inverse of the covariance matrix of the distribution.


#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,"==NEWTON==");
    output_->logTab(debuggg,"Newton: Gradient and Hessian");
    output_->logEllipsisOpen(debuggg,"Newton: Calculating the gradient and hessian");
  }
#endif

  TargetGradientHessianType f_g_h;
  std::tie(f_g_h,std::ignore) = TARGET.reparameterized_adjusted_target_gradient_hessian();
                              f = f_g_h.Target;
  sv_double                   g = f_g_h.Gradient;
  versa_flex_double           h = f_g_h.Hessian;
  bool      is_hessian_diagonal = f_g_h.Diagonal;

  versa_flex_double h_inv = h; // (shallow copy)

  if (math::IsVecZero(g))
  {
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"Newton: Zero gradient: exiting");
    }
#endif
    return std::make_pair(f, too_small_shift);
  }

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logEllipsisShut(debuggg);
    output_->logBlank(debuggg);
    output_->logTab(debuggg,"Newton: After adjusting negative diagonals, but before Inverse");
    output_->logTabArray(debuggg,format_vector("Newton: Gradient",g,parameter_names_));
    output_->logBlank(debuggg);
    output_->logTabArray(debuggg,format_hessian(h,parameter_names_));
  }
#endif

  // invert hessian
  if (is_hessian_diagonal)
    for (int i = 0; i < g.size(); i++)
      h(i,i) = 1/h(i,i);
  else
  {
    int filtered(0);
    h_inv = math::RealSymmetricPseudoInverse(h,filtered);
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      if (filtered)
        output_->logTab(debuggg,snprintftos("Newton: filtered %i negative or small eigenvectors",filtered));
      output_->logTab(debuggg,"Newton: After Computing Pseudo-Inverse");
      output_->logTabArray(debuggg,format_hessian(h_inv,parameter_names_));
    }
#endif
  }

  // search vector p found from the newton equation: p = - h_inv * g
  sv_double p(math::neg_mat_vec_mult(h_inv, g));

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTabArray(debuggg,format_vector("Newton: Parameters",x,parameter_names_));
    output_->logTabArray(debuggg,format_vector("Newton: Search Vector",p,parameter_names_));
    output_->logBlank(debuggg);
  }
#endif

  // Figure out how far linesearch has to go to shift at least one parameter
  // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
  double rs = required_shift(min_dx_over_esd, p, h_inv);
  double line_search_distance;
  std::tie(f,line_search_distance) = line_search(TARGET,x,f,g,p,1.,minimum_distance,required_decrease);
  if (line_search_distance < rs) too_small_shift = true;
#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,snprintftos("Newton Function %10.6f", f));
  }
#endif
  return std::make_pair(f, too_small_shift);
}

// <f, line_search_distance>
std::pair<double, double>
Minimizer::line_search(
    RefineBase&          TARGET,
    sv_double            oldx,
    double               f,
    sv_double            g,
    sv_double&           p,
    double               starting_distance,
    double               minimum_distance,
    double               required_decrease
  )
{
  func_count_ = 0;

  sv_double x(TARGET.nmp);

  const double ZERO(0.0),HALF(0.5),ONE(1.0),TWO(2.0),FIVE(5.0);
  const double GOLDEN((std::sqrt(FIVE)+ONE)/TWO);
  const double GOLDFRAC((GOLDEN-ONE)/GOLDEN);
  const double WOLFEC1(1.E-4); // 1.E-4 suggested in Nocedal & Wright
  const double DTOL(1.e-5);
  double grad(math::dot_prod(p,g));

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,"Determining Stepsize");
    output_->logTab(debuggg,snprintftos("f(  Start  ) = %12.6f", f));
    output_->logTab(debuggg,snprintftos("|"));
  }
#endif

  // Put distance each parameter can move before it hits a bound in dist array. If all parameters are
  // bounded in search direction, need to know max_dist we can move before hitting ultimate bound.
  auto max_dist_pair = TARGET.maximum_distance(oldx,p);
  double          max_dist = max_dist_pair.first;
  sv_double dist = max_dist_pair.second;
  if (starting_distance > max_dist)
  {
    if (max_dist > ZERO)
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"To avoid exceeding bounds for all parameters, starting distance reduced from " + std::to_string(starting_distance) + " to " + std::to_string(max_dist));
      }
#endif
      starting_distance = max_dist;
    }
    else
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"All parameters have hit a bound -- end search");
      }
#endif
      double line_search_distance(0.);
      return std::make_pair(f, line_search_distance);
    }
  }

  // Get macrocycle_large_shifts for damping in shift_parameters
  sv_double macrocycle_large_shifts(TARGET.reparameterized_large_shifts());

  double fk(0),flo(0),fhi(0),dk(0),dlo(0),dhi(0);

  //Sample first test point
#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,"Unit distance = " + std::to_string(starting_distance));
  }
#endif

  dk  = starting_distance;
  fk = shift_score(TARGET, dk, x, oldx, p, dist, macrocycle_large_shifts);

  const double WOLFEFRAC(HALF); // 0.5 slightly better in tests of 0,0.25,0.5,1
  if ((dk >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
      && (fk < f+WOLFEC1*dk*grad)) // Wolfe condition for first step
  {
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"Satisfied Wolfe condition for first step");
      output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -fk, dk));
      output_->logTab(debuggg,"Bracketing (A) took " + std::to_string(func_count_) + " function evaluations");
    }
#endif
    return std::make_pair(fk, dk);
  }

  dlo = ZERO;
  flo = f;

  if (f <= fk) { // First step is too big
    while (f <= fk) {
      // Shrink shift until we find a value lower than starting point
      // fit quadratic to grad and f at 0 and dk, choose its minimum (but not too small)
      if (dk < minimum_distance) { // Give up if shift too small
        TARGET.set_reparameterized_parameters(oldx);  // Reset x before returning

#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTab(debuggg,"Line search: Shift too small");
        }
#endif
        return std::make_pair(f, ZERO);
      }
      fhi = fk;
      dhi = dk;
      double denom(TWO*(grad*dk+(f-fk)));
      if (denom < 0)
      {
        double xdk = std::min(0.99,std::max(0.1,grad*dk/denom));
        dk *= xdk;
#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTab(debuggg,"Line search: fraction of step = "  + std::to_string(xdk));
        }
#endif
      }
      else // unlikely but possible for vanishingly small gradient
      {
        dk *= 0.1;
#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTab(debuggg,"Line search: vanishingly small gradient");
        }
#endif
      }
      fk = shift_score(TARGET, dk, x, oldx, p, dist, macrocycle_large_shifts);
    }
    if (fk < f+WOLFEC1*dk*grad) {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Satisfied Wolfe condition after backtracking");
        output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -fk, dk));
        output_->logTab(debuggg,"Bracketing (B) took " + std::to_string(func_count_) + " function evaluations");
      }
#endif
      return std::make_pair(fk, dk);
    }
  }
  else { // First step goes down.
    if (dk >= max_dist) { // First step already hit ultimate bound
      dhi = max_dist;
      fhi = fk;
    }
    else {
      dhi = std::min(max_dist,(ONE+GOLDEN)*dk);
      fhi = shift_score(TARGET, dhi, x, oldx, p, dist, macrocycle_large_shifts);
      if ((fhi <= fk)
          && (dhi >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
          && (fhi < f+WOLFEC1*dhi*grad)) { // Wolfe condition for first step
#ifdef PHASERTNG_DEBUG_MINIMIZER
        if (write_debuggg)
        {
          output_->logTab(debuggg,"Satisfied Wolfe condition for extended step");
          output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -fhi, dhi));
          output_->logTab(debuggg,"Bracketing (C) took " + std::to_string(func_count_) + " function evaluations");
        }
#endif
        return std::make_pair(fhi, dhi);
      }
    }
    if (flo > fk && fk >= fhi) { // Still not coming up at second step
      while (fk >= fhi && dhi < max_dist) {
        flo = fk;
        dlo = dk;

        fk = fhi;
        dk = dhi;

        dhi = std::min(max_dist,dk + GOLDEN*(dk-dlo));
        fhi = shift_score(TARGET, dhi, x, oldx, p, dist, macrocycle_large_shifts);
        if ((fhi <= fk)
            && (dhi >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
            && (fhi < f+WOLFEC1*dhi*grad)) { // Wolfe condition for first step
#ifdef PHASERTNG_DEBUG_MINIMIZER
          if (write_debuggg)
          {
            output_->logTab(debuggg,"Satisfied Wolfe condition for further extended step");
            output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -fhi, dhi));
            output_->logTab(debuggg,"Bracketing (D) took " + std::to_string(func_count_) + " function evaluations");
          }
#endif
          return std::make_pair(fhi, dhi);
        }
      }
      if (dhi >= max_dist && fk >= fhi) {
        // Reached boundary without defining bracket.
        // Use finite differences to test if still going down.
        // If so, stop this line search.  Otherwise, we've verified bracket.
        dk = (1.-DTOL)*dhi;
        fk = shift_score(TARGET, dk, x, oldx, p, dist, macrocycle_large_shifts);
        if (fk >= fhi) {
          shift_parameters(TARGET,dhi,x,oldx,p,dist,macrocycle_large_shifts);
#ifdef PHASERTNG_DEBUG_MINIMIZER
          if (write_debuggg)
          {
            output_->logTab(debuggg,"Reached boundary without defining bracket");
          }
#endif
          return std::make_pair(fhi, dhi);
        }
      }
    }
    if (dk >= WOLFEFRAC && fk < f+WOLFEC1*dk*grad) {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Satisfied Wolfe condition for bigger than initial step");
        output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -fk, dk));
        output_->logTab(debuggg,"Bracketing (E) took " + std::to_string(func_count_) + " function evaluations");
      }
#endif
      shift_parameters(TARGET,dk,x,oldx,p,dist,macrocycle_large_shifts);  // Set to current best before returning
      return std::make_pair(fk, dk);
    }
  }

  //===================================  End of bracketing

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,"Bracketing (F) took " + std::to_string(func_count_) + " function evaluations");
    output_->logTab(debuggg,"xlow= " + std::to_string(dlo) + ", xhigh= " + std::to_string(dhi));
  }
#endif
  double dmid(dk);
  double fmid(fk);

  // BISECTING/INTERPOLATION STARTS HERE (we now know that the value is between dlo and dhi)

  // let tolerance be small but not too close to zero as to ensure our algorithm tries a new value
  // distinctly different from existing one
  const double XTOL(0.01);

  double a(0), b(0), c(0);
  double stepsize = std::min(dhi-dmid,dmid-dlo);
  double lastlaststepsize(0);
  double laststepsize = stepsize*TWO; // permit first step to actually happen
  double d1(0),f1(0),d2(0),f2(0);

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,snprintftos("Line Search |"));
  }
#endif
  for (int i=0; i<20; i++) // fall-back upper limit on evaluations in interpolation stage
  {
    if (quadratic_coefficients(flo, fmid, fhi, dlo, dmid, dhi, a, b, c)) // Make sure there will be quadratic with a minimum
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Quadratic interpolation used" );
      }
#endif
      dk = -b/(TWO*c); // where the derivative of parabolic curve is zero
      dk = std::max(dk,dlo); // remain within high and low interval
      dk = std::min(dk,dhi); // (Paranoia: fmid < an end should be enough)
    }
    else
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"No quadratic interpolation possible, taking interval midpoint..." );
      }
#endif
      c = ZERO;
      dk = dmid;
    }

    lastlaststepsize = laststepsize;
    laststepsize = stepsize;
    stepsize = std::fabs(dk-dmid);

#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,snprintftos(" stepsize= %12.6f, laststepsize= %12.6f,  ",stepsize, laststepsize));
    }
#endif
    if (c > ZERO  // i.e. the interval has a local minimum, not a local maximum so proceed.
        && dk-dlo > XTOL*(dhi-dlo) // New minimum is sufficiently far from previous points
        && dhi-dk > XTOL*(dhi-dlo) // to make quadratic convergence probable.
        && std::fabs(dk-dmid) > XTOL*(dhi-dlo)
        && stepsize <= HALF*lastlaststepsize) { // Steps are decreasing sufficiently in size
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Use quadratic step");
      }
#endif
      fk = shift_score(TARGET, dk, x, oldx, p, dist, macrocycle_large_shifts);
    }
    else {  // Golden search instead
      dk = (dhi-dmid >= dmid-dlo) ? dmid + GOLDFRAC*(dhi-dmid) : dmid - GOLDFRAC*(dmid-dlo);
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Use golden search step");
      }
#endif
      fk = shift_score(TARGET, dk, x, oldx, p, dist, macrocycle_large_shifts);
    }
    if (dk < dmid) { // Get data sorted by size of shift
      d1 = dk;
      f1 = fk;
      d2 = dmid;
      f2 = fmid;
    }
    else {
      d1 = dmid;
      f1 = fmid;
      d2 = dk;
      f2 = fk;
    }
    if (fk < fmid) { // Keep current best at dmid
      dmid = dk;
      fmid = fk;
    }

#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,snprintftos("="));
      output_->logTab(debuggg,snprintftos("[%12.6f,%12.6f,%12.6f,%12.6f]",dlo,d1,d2,dhi));
      output_->logTab(debuggg,snprintftos("[    ----    ,%12.6f,%12.6f,    ----    ]",f1,f2));
      if (f1 < f2)
        output_->logTab(debuggg,snprintftos("          <        ^^^^        >                     "));
      else
        output_->logTab(debuggg,snprintftos("                       <       ^^^^       >          "));
    }
#endif

    // convergence tests
    double convergence_decrease = std::max(required_decrease,-WOLFEC1*dmid*grad);
    if (fmid < f - convergence_decrease) {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Stop linesearch: good enough for next cycle");
      }
#endif
      break;
    }
    else if ((d2-d1)/d2 < 0.01) {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Stop linesearch: fractional change in stepsize < 0.01");
      }
#endif
      break;
    }
    // Not converged, so prepare for next loop
    if (f1 < f2) {
      dhi = d2;
      fhi = f2;
    }
    else {
      dlo = d1;
      flo = f1;
    }
  }
  // converged or limit in steps

  if (f < fmid) // Shouldn't happen, but catch possibility that function didn't improve
    TARGET.set_reparameterized_parameters(oldx);
  else {
    shift_parameters(TARGET,dmid,x,oldx,p,dist,macrocycle_large_shifts);  // Make sure best shift has been applied.
    f = fmid;
  }

#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logBlank(debuggg);
    output_->logTab(debuggg,"This line search took " + std::to_string(func_count_) + " function evaluations");
    output_->logBlank(debuggg);
    output_->logTab(debuggg,snprintftos("Final LLG: %12.6f distance: %12.6f ", -f, dmid));
    output_->logTab(debuggg,"End distance = " +std::to_string(dmid));
    output_->logTab(debuggg,"Prediction ratio = " +std::to_string(dmid/starting_distance));
    output_->logBlank(debuggg);
  }
#endif
  return std::make_pair(f, dmid);
}

double
Minimizer::shift_score(
    RefineBase& TARGET,
    double      a,
    sv_double&  x,
    sv_double&  oldx,
    sv_double&  p,
    sv_double&  dist,
    sv_double&  macrocycle_large_shifts,
    bool        bcount)
{
  shift_parameters(TARGET,a,x,oldx,p,dist,macrocycle_large_shifts);
  double f(TARGET.target());
  if (bcount)
    func_count_++;
#ifdef PHASERTNG_DEBUG_MINIMIZER
  if (write_debuggg)
  {
    output_->logTab(debuggg,snprintftos("f(%9.6f) = %10.6f", a, f));
  }
#endif
  return f;
}

void
Minimizer::shift_parameters(
    RefineBase& TARGET,
    double      a,
    sv_double&  x,
    sv_double&  oldx,
    sv_double&  p,
    sv_double&  dist,
    sv_double&  macrocycle_large_shifts
  )
{
  x = TARGET.damped_shift(a,oldx,p,dist,macrocycle_large_shifts);
  TARGET.set_reparameterized_parameters(x);
}

// <termination_reason, calc_new_hessian>
std::pair<std::string, bool>
Minimizer::check_termination_criteria(
    bool  too_small_shift,
    bool  too_small_decrease,
    bool  zero_gradient,
    bool  queue_new_hessian,
    int   ncyc_since_hessian_calc,
    int   cycle,
    int   ncyc
  )
{
  // Test whether we should terminate the current macrocycle
  //
  // Return a string containing the reason to terminate (empty if there is none)
  // Also return a bool, calc_new_hessian if it is determined that
  // the next microcycle in a bfgs minimization should start with a new hessian.
  // (naturally this only makes sense if we are not terminating the macrocycle
  //  this time.)

  bool calc_new_hessian(false);

  if (cycle == ncyc-1)
    return std::make_pair("cycle limit", calc_new_hessian);

  if (zero_gradient)
  {
#ifdef PHASERTNG_DEBUG_MINIMIZER
    if (write_debuggg)
    {
      output_->logTab(debuggg,"Zero gradient: exiting");
    }
#endif
    return std::make_pair("zero_gradient", calc_new_hessian);
  }
  if ( too_small_shift xor too_small_decrease ) // Only one of two stopping criteria triggered
  { // Carry on but only recalculate Hessian if it wasn't done last cycle
    if (ncyc_since_hessian_calc != 0)
    {
      calc_new_hessian = true; // tell bfgs to calculate a new hessian next time rather than doing the bfgs update to the inverse hessian approximation
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"Recalculate Hessian and carry on");
      }
#endif
    }
    return std::make_pair("", calc_new_hessian);
  }
  if ( too_small_shift and too_small_decrease ) // Both primary stopping criteria triggered
  {
    if ( queue_new_hessian and (ncyc_since_hessian_calc != 0) ) // queue_new_hessian is only touched by bfgs
    {
#ifdef PHASERTNG_DEBUG_MINIMIZER
      if (write_debuggg)
      {
        output_->logTab(debuggg,"New Hessian queued: recalculate  and carry on");
      }
#endif
      calc_new_hessian = true;
      return std::make_pair("", calc_new_hessian);
    }
    else
    {
      if (write_testing)
      {
        output_->logBlank(testing);
        output_->logTab(testing,"---Convergence of Macrocycle---");
        output_->logBlank(testing);
      }
      return std::make_pair("converged", calc_new_hessian);
    }
  }
  // No stopping or restarting criteria triggered
  return std::make_pair("",calc_new_hessian);
}

double
Minimizer::hessian_diag_rms(versa_flex_double h)
{
  // Calculate root-mean-square of the hessian diagonal entries

  double hiiRMS(0.);
  int N = h.accessor().all()[0];
  PHASER_ASSERT(N != 0); // paranoia
  for (int i=0; i<N; i++)
    hiiRMS += fn::pow2(h(i,i));
  hiiRMS = std::sqrt(hiiRMS/N);
  return hiiRMS;
}

inline double
Minimizer::required_shift(
    double            min_dx_over_esd,
    sv_double         p,
    versa_flex_double h_inv)
{
    // Figure out how far linesearch has to go to shift at least one parameter
    // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
    // DHS: this was rather tricky to get my head around. The following is an attempt
    // at a graphical explanation:

    // Outer box is the esd estimated using sqrt(h(i,i))
    // Inner box is min_dx_over_esd * esd  ( = min_dx_over_esd * sqrt(h(i,i)))

    // We want to find the minimum multiple of p (the arrow/vector thing in the diagram)
    // needed to just hit the inner box (in this case it will be less than 1 and hit the 2nd parameter's limit first)
    //
    // -------------------------------------
    // |                    ^              |
    // |                   /               |
    // |      -----------------------      |
    // |      |          /          |      |
    // |      -----------------------      |
    // |                                   |
    // |                                   |
    // -------------------------------------

  double rs(0.);
  for (int i = 0; i < p.size(); i++) {
    if (h_inv(i,i) > 0)
      rs = std::max(rs,std::fabs(p[i])/std::sqrt(h_inv(i,i)));
  }
  if (rs > 0)
    rs = min_dx_over_esd/rs;
  return rs;
}

bool
Minimizer::quadratic_coefficients(
    double  y1,
    double  y2,
    double  y3,
    double  x1,
    double  x2,
    double  x3,
    double& a,
    double& b,
    double& c)
{
  // Find the coefficients to a quadratic specified by three pointy on the quadratic
  //
  // Given three points in the x-y plane get the coefficients of the corresponding
  // parabolic equation y(x) = a + b*x + c*x^2 that passes through them all

  a = b = c = 0.0;

  double det = x2*x3*x3 - x3*x2*x2 - x1*(x3*x3 - x2*x2) + x1*x1*(x3 - x2);
  if (std::fabs(det) < std::numeric_limits<double>::epsilon()*3) // too close to zero within machine precision
    return false;

  double invdet = 1.0/det;

  //   a = (y1*(x2*x3*x3 - x2*x2*x3) + y2*(x1*x3*x3 - x3*x1*x1) + y3*(x1*x2*x2 - x2*x1*x1))*invdet; // correct but unstable
  b = (y1*(x2*x2 - x3*x3) + y2*(x3*x3 - x1*x1) + y3*(x1*x1 - x2*x2))*invdet;
  c = (y1*(x3 -x2) + y2*(x1 - x3) + y3*(x2 -x1))*invdet;
  a = y1 - (b*x1 +c*x1*x1);

  if (c == 0)
    return false;
  else
    return true;
}

  af_string
  Minimizer::format_vector(
     std::string what,
     sv_double& vec,
     sv_string macrocycle_parameter_names)
  {
    af_string lines;
    lines.push_back(what);
    for (int i = 0; i < vec.size(); i++)
    {
      std::string pname = (i < macrocycle_parameter_names.size()) ? macrocycle_parameter_names[i] : "";
      lines.push_back(snprintftos(
          "%3d [% 10.4g] %s",
          i+1,vec[i],pname.c_str()));
    }
    lines.push_back("");
    return lines;
  }

  af_string
  Minimizer::format_hessian(
     af::versa<double, af::flex_grid<> > Hessian,
     sv_string macrocycle_parameter_names)
  {
    af_string lines;
    int max_dim(5); //very generous but not infinite
    PHASER_ASSERT(Hessian.accessor().is_square_matrix());
    int n_rows = Hessian.ref().accessor().all()[0];
    int n_cols = Hessian.ref().accessor().all()[1];
    bool is_diagonal(true);
    for (int i = 0; i < n_rows; i++)
    for (int j = 0; j < n_cols; j++)
    if (i != j and (Hessian(i,j) != 0))
    {
      is_diagonal = false;
      break;
    }

    if (!is_diagonal)
    {
      lines.push_back("Matrix");
      if (n_rows > max_dim)
        lines.push_back("Matrix is too large to write to log: size = "+std::to_string(n_rows));
      for (int i = 0; i < n_rows and i+1 < max_dim; i++)
      {
        std::string hess;
        std::string line;
        for (int j = 0; j < n_cols and j+1 < max_dim; j++)
        {
          {
          std::stringstream s;
          s.setf(std::ios_base::showpos);
          s.setf(std::ios_base::scientific);
          s.precision(1); //number of decimal places
          s.width(3); //width
          s << Hessian(i,j);
          hess = s.str();
          }
          line += (Hessian(i,j)) ? hess + " " : " ---0--- " ;
        }
        line = line.substr(0,line.length()-1);
        (n_rows >= max_dim) ?
          lines.push_back(snprintftos(
              "%3d [%s etc...]",
              i+1,line.c_str())) :
          lines.push_back(snprintftos(
              "%3d [%s]",
              i+1,line.c_str()));
      }
      if (n_rows >= max_dim)
        lines.push_back(" etc...");
      lines.push_back("");
    }
    lines.push_back("Matrix Diagonals");
    for (int i = 0; i < n_rows; i++)
    {
      std::string pname = (i < macrocycle_parameter_names.size()) ? macrocycle_parameter_names[i] : "";
      lines.push_back(snprintftos(
          "%3d [% 10.4g] %s",
          i+1,Hessian(i,i),pname.c_str()));
    }
    lines.push_back("");
    return lines;
  }

}} //phasertng
