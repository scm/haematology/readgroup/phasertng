//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/HandEP.h>

namespace phaser {

map_str_int HandEP::getAtomNum()
{
  map_str_int mapa;
  for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
      mapa[atoms[a].SCAT.scattering_type] = 0;
  for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
      mapa[atoms[a].SCAT.scattering_type]++;
  return mapa;
}

af::shared<xray::scatterer<double> > HandEP::getAtoms()
{
  af::shared<xray::scatterer<double> > atoms_selected;
  for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
      atoms_selected.push_back(atoms[a].SCAT);
  return atoms_selected;
}

std::map<std::string,cctbx::eltbx::fp_fdp> HandEP::getFpFdp()
{
  std::map<std::string,cctbx::eltbx::fp_fdp> FpFdp;
  for (int t = 0; t < AtomFp.size(); t++)
  {
    std::string atomtype = t2atomtype[t];
    cctbx::eltbx::fp_fdp fpfdp(AtomFp[t],AtomFdp[t]);
    FpFdp[atomtype] = fpfdp;
  }
  return FpFdp;
}

} //end namespace phaser
