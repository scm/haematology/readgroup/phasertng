//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_boolean_class__
#define __phasertng_type_boolean_class__
#include <phasertng/type/optional.h>
#include <boost/logic/tribool.hpp>

namespace phasertng {
namespace type {

class boolean : public optional
{
  boost::logic::tribool dephalt = boost::logic::indeterminate;
  boost::logic::tribool uvalue = boost::logic::indeterminate;

  public:
  boolean() : optional() {}
  boolean(
      const std::string parameter,
      const boost::logic::tribool dephalt_=boost::logic::indeterminate
  );
  ~boolean() {}

  void set_value(bool other);
  void set_value(const sv_string& vcards,const bool& other);
  boost::logic::tribool value_or_default_tribool() const;
  char value_or_default_char() const; // Y U N
  bool value_or_default() const;
  bool is_default() const;
  void set_default();
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::boolean& other) const
  {
    bool isdephalt(boost::logic::indeterminate(dephalt)); //Windows
    bool isuserval(boost::logic::indeterminate(uvalue)); //Windows
    if (isdephalt and isuserval) return true;
    else if (isdephalt or isuserval) return false;
    return bool(value_or_default_tribool()) == bool(other.value_or_default_tribool());
  }

};

}}
#endif
