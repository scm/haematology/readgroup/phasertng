//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/math/likelihood/EELLG.h>
#include <phasertng/math/likelihood/EELLG_analytical.h>
#include <phasertng/data/Selected.h>
#include <unordered_set>
#include <phasertng/math/tfactorial.h>

namespace phasertng {

  //Expected_llg_for_search
  void Phasertng::runPERM()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->get_ztotalscat())
    throw Error(err::INPUT,"Total scattering not set");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.all_modlid(); //because we only have tags for lookup, so don't know full identifiers (yet)
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::VARIANCE_MTZ,id,false);
    }}

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    bool store_elmn(false);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR,store_elmn);
    auto& array_G_DRMS = epsilon.array_G_DRMS;

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    //below for self documenting code, indices into af::double4
    const int imap(3),islow(2),imidl(1),ifast(0);
    int io_ellg(0);
    double io_ellg_target(0), io_mapellg_target(0);
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Target ellg");
    (REFLECTIONS->TWINNED) ?
       logTab(where,"Twinning indicated: expected ellg target quadrupled"):
       logTab(where,"Twinning not indicated: expected ellg target NOT quadrupled");
    io_ellg_target = input.value__flt_number.at(".phasertng.expected.ellg.target.").value_or_default();
    io_ellg_target *= (REFLECTIONS->TWINNED) ? 4.0 : 1.0;
    logTab(where,"eLLG Target: " + dtos(io_ellg_target,2));
    if (REFLECTIONS->MAP)
    {
      io_ellg = imap;
      logTab(where,"Map data: eLLG (amplitudes) and mapeLLG (phased) will be calculated");
      io_mapellg_target = input.value__flt_number.at(".phasertng.expected.mapllg.target.").value_or_default();
      logTab(where,"Map eLLG Target: " + dtos(io_mapellg_target,2));
    }
    else
    {
      if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("ellg"))
      {
        io_ellg = islow;
        logTab(where,"eLLG, eeLLG and aeLLG values will be calculated");
      }
      else if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("eellg"))
      {
        io_ellg = imidl;
        logTab(where,"eeLLG and aeLLG values will be calculated");
      }
      else if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("aellg"))
      {
        io_ellg = ifast;
        logTab(where,"aeLLG values will be calculated");
      }
    }
    }}

    auto tags = input.value__strings.at(".phasertng.permutations.tags_in_search_order.").value_or_default();

    if (!DAGDB.NODES.number_of_seeks())
    {
      logTab(out::LOGFILE,"No current seeks");
      //we can only add seeks to ONE node with seeks using constituents,
      //for multiple nodes with different seeks, for sorting, need to add through python
      //directly to the dagdatabase
      std::vector<sv_string> searches;
      //use the constituents if they are set, otherwise default to the search tag
      if (!input.size(".phasertng.permutations.tags."))
      {
        searches.resize(1);
        if (input.value__string.at(".phasertng.search.tag.").is_default())
          throw Error(err::INPUT,"Constituents for ellg not set");
        auto tag = input.value__string.at(".phasertng.search.tag.").value_or_default();
        searches[0].push_back(tag);
      }
      else
      {
        searches.resize(input.size(".phasertng.permutations.tags."));
        for (int i = 0; i < input.size(".phasertng.permutations.tags."); i++)
        {
          //the searches are built from tags and z, and z could be in tags also (z=1)
          //so accumulate the tags in a map and then divide out the nmol per tag
          auto permtags = input.array__strings.at(".phasertng.permutations.tags.").at(i).value_or_default();
          int mz = input.value__int_number.at(".phasertng.permutations.matthews_z.").value_or_default();
          std::map<std::string,int> mapsearch;
          logTab(out::LOGFILE,"Matthews Z=" + itos(mz));
          for (int j = 0; j < permtags.size(); j++)
            permtags[j] = Identifier(0,permtags[j]).tag();
          for (int j = 0; j < permtags.size(); j++)
          {
            //the processed predicted model tags are shortened
            mapsearch[permtags[j]] = 0;
            logTab(out::LOGFILE,"Input tag #" + itos(j+1) + "=" +  permtags[j]);
          }
          for (int j = 0; j < permtags.size(); j++)
            for (int z = 0; z < mz; z++)
              mapsearch[permtags[j]]++;
          int ntag = permtags.size()*mz;
          if (ntag%nmol != 0)
          {
            logWarning("Input tags not divisible by tncs order");
            input.clear(".phasertng.expected.subdir.");
            input.clear(".phasertng.expected.filenames_in_subdir.");
            DAGDB.shift_tracker(hashing(),input.running_mode);
            return;
          }
          for (auto& tsearch : mapsearch)
            for (int z = 0; z < tsearch.second/nmol; z++)
              searches[i].push_back(tsearch.first);
        }
      }
      auto orignode = DAGDB.NODES.back();
      for (int i = 0; !tags.size() and i < searches.size(); i++)
      {
        if (i > 0) //use the first node with ccs
          DAGDB.NODES.push_back(orignode);
        auto& node = DAGDB.NODES.back();
        sv_string search = searches[i];
        node.seek_cumulative_ellg = scitbx::af::double4(0,0,0,0);
        for (auto tag : search)
        {
          dag::Seek seek;
          seek.set_missing();
          seek.IDENTIFIER.initialize_tag(tag);
          auto entry = DAGDB.lookup_entry_tag(seek.IDENTIFIER,DataBase()); //will be in HOLD
          //if this has had to be created from a de-novo read, we will need to load the variance data
          if (entry->VARIANCE.in_memory())
            load_entry_from_database(out::VERBOSE,entry::VARIANCE_MTZ,entry->identify(),false);
          seek.IDENTIFIER = entry->identify();
          seek.seek_increases = 0;
          node.SEEK.push_back(seek);
          auto modlid = seek.IDENTIFIER;
          //might already be present from HOLD
          if (!node.DRMS_SHIFT.count(modlid))  node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0);
          if (!node.VRMS_VALUE.count(modlid))  node.VRMS_VALUE[modlid] = dag::node_t(modlid,0);
          if (!node.CELL_SCALE.count(modlid))  node.CELL_SCALE[modlid] = dag::node_t(modlid,1);
        }
      }
      if (tags.size())
      {
        auto& node = DAGDB.NODES.back();
        node.seek_cumulative_ellg = scitbx::af::double4(0,0,0,0);
        for (auto tag : tags)
        {
          dag::Seek seek;
          seek.set_missing();
          seek.IDENTIFIER.initialize_tag(tag);
          auto entry = DAGDB.lookup_entry_tag(seek.IDENTIFIER,DataBase()); //will be in HOLD
          seek.IDENTIFIER = entry->identify();
          seek.seek_increases = 0;
          node.SEEK.push_back(seek);
          auto modlid = seek.IDENTIFIER;
          //might already be present from HOLD
          if (!node.DRMS_SHIFT.count(modlid))  node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0);
          if (!node.VRMS_VALUE.count(modlid))  node.VRMS_VALUE[modlid] = dag::node_t(modlid,0);
          if (!node.CELL_SCALE.count(modlid))  node.CELL_SCALE[modlid] = dag::node_t(modlid,1);
        }
      }
    }
    DAGDB.restart();

    logBlank(out::VERBOSE);
    logTabArray(out::VERBOSE,DAGDB.WORK->logSeek("Initial"));

    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      DAGDB.WORK->set_star_from_pose();
      DAGDB.WORK->HALL = REFLECTIONS->SG.HALL;
      DAGDB.WORK->CELL = REFLECTIONS->UC.cctbxUC;
      DAGDB.reset_work_entry_pointers(DataBase());
    }

    DAGDB.restart();
    if (!DAGDB.NODES.number_of_seeks())
    {
      logTab(out::LOGFILE,"No seek entries in Dag");
    }
    else
    {
      //make sure the reflections are sorted low resolution to high resolution
      //this should be done as part of the data preparation step
      if (!REFLECTIONS->check_sort_in_resolution_order())
        throw Error(err::INPUT,"Reflection file not sorted by resolution, aborting");

      likelihood::mapELLG likelihood_emap;
      likelihood::ELLG likelihood_ellg;
      likelihood::EELLG likelihood_eellg;
      likelihood::EELLG_analytical likelihood_aellg;

      //the average vrms is a horrible approximation, but is good for af2 models
      //which don't have ensembles and are all the same rmsd regardless of target
      sv_double average_vrms_final(DAGDB.size(),0.);
      int maxnseek(0);
      {{
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Average VRMS");
      DAGDB.restart();
      int m(0);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK; //pointer
        phaser_assert(node->SEEK.size());
        sv_double average_vrms(node->SEEK.size());
        maxnseek = std::max(maxnseek,int(node->SEEK.size()));
        for (int s = 0; s < node->SEEK.size(); s++)
        {
          auto& seek = node->SEEK[s];
          auto& entry = seek.ENTRY; //pointer ok
          auto  modlid = entry->identify();
          sv_double vrms_final = pod::vrms_final(
            node->DRMS_SHIFT[modlid].VAL,entry->VRMS_START);
          for (int n = 0; n < vrms_final.size(); n++) //over all in the ensemble
            average_vrms[s] += vrms_final[n]/vrms_final.size();
        }
        logTab(where,"Node " + nNtos(m+1,DAGDB.size()));
        for (int s = 0; s < node->SEEK.size(); s++)
        {
          logTab(where,"Seek #" + ntos(s+1,node->SEEK.size()) + "  VRMS:[average=" + dtos(average_vrms[s]) + "]");
          average_vrms_final[m] += average_vrms[s]/static_cast<double>(node->SEEK.size());
        }
        logTab(where,"Average VRMS: " + dtos(average_vrms_final[m],7,4));
        m++;
      }
      }}

      logUnderLine(out::LOGFILE,"Signal Resolution");
      double io_sigfac = input.value__flt_number.at(".phasertng.expected.ellg.signal_resolution_factor.").value_or_default();
      for (int m = 0; m < DAGDB.size(); m++)
      { // Resolution for the interesting signal, can't go past data resolution
        // Note that this is overwritten for eLLG and mapeLLG, where Dobs is accounted for
        DAGDB.WORK->SIGNAL_RESOLUTION = io_sigfac*average_vrms_final[m];
        logTab(out::LOGFILE,"Average vrms over models =  " + dtos(average_vrms_final[m],7,4));
        logTab(out::LOGFILE,"Signal resolution factor =  " + dtos(io_sigfac,7,4));
        logTab(out::LOGFILE,"Signal resolution wrt vrms = "  + dtos(DAGDB.WORK->SIGNAL_RESOLUTION,2));
        if (DAGDB.WORK->SIGNAL_RESOLUTION < REFLECTIONS->data_hires())
          logTab(out::LOGFILE,"Signal Resolution extends beyond data resolution, no data resolution truncation");
        else
          logAdvisory("Signal Resolution truncates data resolution");
        DAGDB.WORK->SIGNAL_RESOLUTION = std::max(REFLECTIONS->data_hires(),DAGDB.WORK->SIGNAL_RESOLUTION);
      }

      {{
      //check that the scattering is all ok first
      DAGDB.restart();
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Check Scattering");
      double ztotalscat = REFLECTIONS->get_ztotalscat();
      logTab(where,"Total Scattering = " + dtoss(REFLECTIONS->get_totalscat(),2));
      logTab(where,"Total Scattering Z = " + dtoss(REFLECTIONS->Z,2));
      logTab(where,"Total Scattering with Z applied = " + dtos(ztotalscat,0));
      while (!DAGDB.at_end())
      {
        logTabArray(out::LOGFILE,DAGDB.seek_info_array());
        //scattering_seek includes nmol
        double scattot(0);
        logTab(where,"Cumulative Scattering:");
        for (int s = 0; s < DAGDB.WORK->SEEK.size(); s++)
        {
          auto& node = DAGDB.WORK;
          std::string info = "Seek Scattering #" + ntos(s*nmol+1,node->SEEK.size());
          std::string tncs_info = (nmol>1) ? ("->" + ntos(s*nmol+nmol,node->SEEK.size())) : "";
          int scat = node->SEEK[s].ENTRY->SCATTERING;
          scattot += nmol*scat;
          (s == 0) ?
            logTab(where,info + tncs_info + " = " +  itos(nmol*scat)):
            logTab(where,info + tncs_info + " = " +  itos(nmol*scat) + " [=" + dtos(scattot,0) + "]");
        }
        if (DAGDB.WORK->fraction_scattering_seek(ztotalscat) > 1.0)
        {
//this is the most important point where the scattering of the sequence is checked against the actual model scattering
          throw Error(err::INPUT,"Seek scattering is higher than total scattering:\nCheck composition sequence matches biological unit\nCheck composition sequence is higher than model sequence");
        }
      }
      }}


      std::map<Identifier,double> seek_fp;
      bool calculate_sigmaa(io_ellg == imidl or
                            io_ellg == islow or
                            io_ellg == imap);
      for (auto& item : DAGDB.ENTRIES)
      {
        dag::Entry* entry = &item.second;
        seek_fp[entry->identify()] = entry->fraction_scattering(REFLECTIONS->get_ztotalscat());
        seek_fp[entry->identify()] *= nmol;
      }

      if (Suite::Extra())
      {
        for (auto& entry : DAGDB.ENTRIES)
          logTabArray(out::TESTING,entry.second.VARIANCE.BIN.logBin("variance"));
      }

      int nrefl(0);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
        if (selected.get_selected(r) and REFLECTIONS->get_present(friedel::NAT,r))
          nrefl++;
      sv_bool present(REFLECTIONS->NREFL);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
        present[r] = REFLECTIONS->get_present(friedel::NAT,r);

      int io_absmax =  //unique max, determined by permutations
        input.value__int_paired.at(".phasertng.expected.ellg.maximum_first_search.").value_or_default_max();
      io_absmax = std::min(io_absmax,maxnseek);
      int io_absmin = 1;
      if (maxnseek >= 3)
        io_absmin =  //permute up to three regardless of whether target is reached
          input.value__int_paired.at(".phasertng.expected.ellg.maximum_first_search.").value_or_default_min();
      if (io_absmin > io_absmax) io_absmin = io_absmax;
      dag::NodeList output_dag;
      bool at_least_one_reached_ellg_target(false);
      for (int io_maxunique = io_absmin; io_maxunique <= io_absmax; io_maxunique++)
      {
        dag::NodeList loop_dag; //reset
        logUnderLine(out::LOGFILE,"Permutation Loop");
        logTab(out::LOGFILE,"Maximum number in permutation: " + itos(io_maxunique));

        int n(1),N(DAGDB.size());
        scitbx::af::double4 zero(0.,0.,0.,0.); //fast medium slow map ellg
        //where there are no ensembles and the vrms are uniformly low
        //seek increases in indexed on seek number but not all will be filled
      //std::unordered_set<std::string> seek_models_unique;
        std::set<std::string> seek_models_unique;
        int maxlen_seek_models_unique(0);
        DAGDB.restart();
        DAGDB.reset_entry_pointers(DataBase());
        struct perminfo { int nseek; int total; int unique; sv_int counts; sv_int target; };
        std::vector<perminfo> permutation_info(DAGDB.size());
        //at_least_one_reached_ellg_target = false;
        logEllipsisOpen(out::LOGFILE,"Expected LLG Calculations");
        while (!DAGDB.at_end())
        {
          std::string nN = nNtos(n++,N);
          int m(n-2);
          out::stream where = out::VERBOSE;
          logUnderLine(where,"Expected LLG Calculation" + nN);
          auto cmp = [](const dag::Seek &a, const dag::Seek &b) { return a.IDENTIFIER.identifier() < b.IDENTIFIER.identifier(); };
          std::map<dag::Seek,int,decltype(cmp)> Pkn(cmp);
          std::map<dag::Seek,int,decltype(cmp)> Pkn_leftover(cmp);
          for (int s = 0; s < DAGDB.WORK->SEEK.size(); s++)
            Pkn[DAGDB.WORK->SEEK[s]] = 0;
          for (int s = 0; s < DAGDB.WORK->SEEK.size(); s++)
          {
            //only allow up to the maximum for the first search
            if (Pkn[DAGDB.WORK->SEEK[s]] < io_maxunique)
              Pkn[DAGDB.WORK->SEEK[s]]++;
            else
              Pkn_leftover[DAGDB.WORK->SEEK[s]]++;
            //we must consider at least all of the same identifier if it appears multiple times
            //in the search for the first component before signal is achieved
            //else one contaminating bad component could terminate the search
          //  maxunique = std::max(maxunique,Pkn[DAGDB.WORK->SEEK[s]]);
            //but have deleted this as the number of permutations can be astronomical
          }
  //short seeks mixes up the first few unique combinations in order to see what works for the
          logTab(where,"Seeks: " + itos(DAGDB.WORK->SEEK.size()));
          for (auto k : Pkn)
            logTab(where,"Counter: Identifier " +  k.first.IDENTIFIER.string() + " #" + itos(k.second));
          sv_int divisor(DAGDB.WORK->SEEK.size(),true);
          for (auto k : Pkn)
            for (int s = 1; s < DAGDB.WORK->SEEK.size(); s++)
              if (k.second % s != 0)
                divisor[s] = false;
          int maxs(1);
          for (int s = 1; s < DAGDB.WORK->SEEK.size(); s++)
             if (divisor[s])
               maxs = s;
          logTab(where,"Maximum divisor=" + itos(maxs));
          logTab(where,"Maximum number of first search models=" + itos(io_maxunique));
          std::vector<dag::Seek> short_Seek;
          while (short_Seek.size() < io_maxunique)
          {
            for (auto k : Pkn)
              for (int s = 0; s < k.second/maxs; s++)
               // for (int n = 0; n < nmol; n++)
                  short_Seek.push_back(k.first);
          }
          auto orig_Seek = DAGDB.WORK->SEEK;
          if (short_Seek.size() > DAGDB.WORK->SEEK.size())
            short_Seek = DAGDB.WORK->SEEK;
          if (tags.size())
            short_Seek = DAGDB.WORK->SEEK;
          logTab(where,"Short unique=" + itos(short_Seek.size()));
          //run to the end
          int npermutations = 0;
          if (!tags.size())
          {
            std::sort(short_Seek.begin(), short_Seek.end(),cmp);
            do { npermutations++; } while (std::next_permutation(short_Seek.begin(), short_Seek.end(),cmp));
            std::sort(short_Seek.begin(), short_Seek.end(),cmp);
          }
          else npermutations = 1;
          permutation_info[m].nseek = DAGDB.WORK->SEEK.size();
          permutation_info[m].total = static_cast<int>(npermutations);
          permutation_info[m].unique = 0;
          permutation_info[m].counts = sv_int(DAGDB.WORK->SEEK.size(),0); //not short_Seeks
          permutation_info[m].target = sv_int(DAGDB.WORK->SEEK.size(),0); //not short_Seeks
          int permutation = 1;
          out::stream progress = out::VERBOSE;
          logProgressBarStart(progress,"Permuting"+nN,npermutations,1,false);
          do
          {
            std::string pP = nNtos(permutation++,npermutations);
            DAGDB.WORK->SEEK = short_Seek;
            if (Suite::Extra())
            {
              logTab(out::TESTING,"Permutation" + pP);
              logTabArray(out::TESTING,DAGDB.WORK->logSeek("Permutation" + pP));
            }
            //append the ones that are missing
            auto tmp = Pkn;
            for (int s = 0; s < short_Seek.size(); s++)
              tmp[short_Seek[s]]--;
            for (auto k : tmp)
              for (int s = 0; s < k.second; s++)
                DAGDB.WORK->SEEK.push_back(k.first);
            for (auto k : Pkn_leftover)
              for (int s = 0; s < k.second; s++)
                DAGDB.WORK->SEEK.push_back(k.first);
            //phaser_assert(DAGDB.WORK->SEEK.size() == orig_Seek.size());
            dag::Node newnode = *DAGDB.WORK; //pointer
            dag::Node* node = &newnode;
            std::string this_seek_models_unique;
            {{
              bool unique = true;
             // DAGDB.WORK->generic_flag = true;
              int s(0);
              if (Suite::Extra())
                logTab(out::TESTING,"Max length = " + itos(maxlen_seek_models_unique));
              maxlen_seek_models_unique = std::max(maxlen_seek_models_unique,static_cast<int>(short_Seek.size()));
              for (s = 0; s < maxlen_seek_models_unique; s++)
              {
                this_seek_models_unique += node->SEEK[s].IDENTIFIER.string()+" "; //build up a string starting from 0
                unique = !seek_models_unique.count(this_seek_models_unique); //has the start been seen before?
                if (Suite::Extra())
                  logTab(out::TESTING,"This seek permutation up to " + itos(s+1) + " " + this_seek_models_unique + " unique = " + btos(unique));
                if (!unique)
                { //yes the start as been seen before, don't need to know any more
                  break;
                }
              }
              if (!unique)
              {
                if (Suite::Extra())
                  logTab(out::TESTING,"This seek permutation duplicates previous search up to point of reaching ellg target (" + itos(s+1) + ")");
                //do not push_back this newnode into loop_dag
                if (!logProgressBarNext(progress)) break;
                continue; //skip the calculations
              }
              else
              {
                if (Suite::Extra())
                { //interesting
                  logTab(out::TESTING,"This seek permutation does not duplicate previous search up to point of reaching ellg target");
                }
                permutation_info[m].unique++;
              }
            }}
            node->SEEK_COMPONENTS = nmol; //minimum, even if target is not reached
            node->SEEK_COMPONENTS_ELLG = 0;
            node->SEEK_RESOLUTION = 0;
            node->SEEK_RESOLUTION_PHASED = 0;
            //llg is added to total for each seek component
            //first placement is NMOL copies, which is seek position nmol-NMOL
            bool reached_target(false);
            sv_double ellgdata(REFLECTIONS->NREFL,0.);
            std::vector<scitbx::af::double4> seek_ellg(node->SEEK.size(),zero);
            if (Suite::Extra())
            {
              (io_ellg == imap)?
                logTab(out::TESTING,snprintftos("%4s %14s %16s %16s",
                      "Seek","Resolution","eLLG (unphased)","mapeLLG (phased)")):
                logTab(out::TESTING,snprintftos("%4s %14s %14s %14s %14s",
                      "Seek","Resolution","Fast (aellg)","Medium (eellg)","Slow (ellg)"));
            }
            for (int s = 0; s < node->SEEK.size(); s++)
            {
              if (reached_target) //for speed
                break;
              if (s > short_Seek.size())
                break;
              double FP = 0;
              for (int ss = 0; ss <= s; ss++)
              {
                FP += seek_fp[node->SEEK[ss].IDENTIFIER];
                PHASER_ASSERT(FP <= 1);
              }

              af_double Siga;
              if (calculate_sigmaa)
              {
                Siga.resize(REFLECTIONS->NREFL);
                auto& variance = node->SEEK[s].ENTRY->VARIANCE;
                for (int r = 0; r < REFLECTIONS->NREFL; r++)
                {
                  Siga[r] = std::sqrt(variance.rSigaSqr[r]*FP);
                }
              }

              if (io_ellg == ifast or io_ellg == imidl or io_ellg == islow)
              {
                double smax = 1. / REFLECTIONS->data_hires();
                double expected_LLGI = likelihood_aellg.get(FP, average_vrms_final[m], smax)*nrefl;
                       expected_LLGI *= REFLECTIONS->oversampling_correction();
                node->seek_cumulative_ellg[ifast] = std::max(1.,expected_LLGI);
                //average over all vrms in ensemble (not great approx)
                if (Suite::Extra())
                  logTab(out::TESTING,snprintftos("%4d %14s %14.2f %14s %14s",
                                 s+1,"",node->seek_cumulative_ellg[ifast],"",""));
              }

              if (io_ellg == imidl or io_ellg == islow)
              {
                bool hit(false);
                for (int r = 0; r < REFLECTIONS->NREFL; r++)
                {
                  if (selected.get_selected(r) and present[r])
                  {
                    const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);

                    double SigaSqr = fn::pow2(Siga[r]);
                    double expected_LLGI = likelihood_eellg.get(SigaSqr);
                 //        expected_LLGI *= REFLECTIONS->oversampling_correction();
                    ellgdata[r] += expected_LLGI;
                    node->seek_cumulative_ellg[imidl] += expected_LLGI;
                    if (s == 0 and
                        !reached_target and
                        node->seek_cumulative_ellg[imidl] > io_ellg_target)
                    {
                      hit = true;
                      reached_target = true;
                      at_least_one_reached_ellg_target = true;
                      node->SEEK_RESOLUTION = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                      node->SEEK_COMPONENTS = s*nmol+nmol;
                      node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[imidl]);
                      if (Suite::Extra())
                        logTab(out::TESTING,snprintftos("%4d %14.2f %14s %14s %14s",
                                 s+1,node->SEEK_RESOLUTION,"","",""));
                      std::string add_seek_models_unique = "";
                      for (int ss = 0; ss < node->SEEK_COMPONENTS/nmol; ss++)
                        add_seek_models_unique += node->SEEK[ss].IDENTIFIER.string()+" ";
                      maxlen_seek_models_unique = std::max(maxlen_seek_models_unique,node->SEEK_COMPONENTS/nmol+1);
                      int c = node->SEEK_COMPONENTS/nmol-1;
                      PHASER_ASSERT(c < permutation_info[m].counts.size());
                      PHASER_ASSERT(c >= 0);
                      permutation_info[m].counts[c]++;
                      permutation_info[m].target[c]++;
                      seek_models_unique.insert(add_seek_models_unique);
                      if (Suite::Extra())
                        logTab(out::TESTING,"Add seek permutation unique " + add_seek_models_unique);
                    }
                  }
                }
                if (hit) //overwrite with all data value
                {
                  node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[imidl]);
                }
                if (Suite::Extra())
                  logTab(out::TESTING,snprintftos("%4d %14s %14s %14.2f %14s",
                                 s+1,"","",node->seek_cumulative_ellg[imidl],""));
              }

              if (io_ellg == islow or io_ellg == imap)
              {
                bool hit = false;
               // reached_target = false;
                // Keep track of the cumulative increase of eLLG with resolution
                std::vector<std::pair<double,double> > cumulative_ellg_by_reso;
                int nref = REFLECTIONS->NREFL;
                double nref_by_100(nref / 100.);
                int next_percentile = 1;
                for (int r = 0; r < nref; r++)
                {
                  if (selected.get_selected(r) and present[r])
                  {
                    const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
                    const double feff = work_row->get_feffnat();
                    const double dobs = work_row->get_dobsnat();
                    const double resn = work_row->get_resn();
                    const double teps = work_row->get_teps();
                    const double ssqr = work_row->get_ssqr();
                    const bool cent = work_row->get_cent();
                    double DobsSiga = dobs*Siga[r];
                    double Eeff = feff/(resn*std::sqrt(teps));
                    double expected_LLGI = likelihood_ellg.get(Eeff,DobsSiga,cent);
                           expected_LLGI *= REFLECTIONS->oversampling_correction();
                    ellgdata[r] += expected_LLGI;
                    node->seek_cumulative_ellg[islow] += expected_LLGI;
                    if (r/nref_by_100 > next_percentile or r == nref)
                    {
                      cumulative_ellg_by_reso.push_back({ssqr, node->seek_cumulative_ellg[islow]});
                      next_percentile = floor(r/nref_by_100) + 1;
                    }

                    if (s == 0 and
                        !reached_target and
                        node->seek_cumulative_ellg[islow] > io_ellg_target)
                    {
                      hit = true;
                      reached_target = true;
                      at_least_one_reached_ellg_target = true;
                      node->SEEK_RESOLUTION = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                      node->SEEK_COMPONENTS = std::min(int(node->SEEK.size()*nmol),s*nmol+nmol);
                      node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[islow]);
                      if (Suite::Extra())
                        logTab(out::TESTING,snprintftos("%4d %14.2f %14s %14s %14s",
                                 s+1,node->SEEK_RESOLUTION,"","",""));
                      std::string add_seek_models_unique = "";
                      for (int ss = 0; ss < node->SEEK_COMPONENTS/nmol; ss++)
                        add_seek_models_unique += node->SEEK[ss].IDENTIFIER.string()+" ";
                      maxlen_seek_models_unique = std::max(maxlen_seek_models_unique,node->SEEK_COMPONENTS/nmol+1);
                      int c = node->SEEK_COMPONENTS/nmol-1;
                      permutation_info[m].counts[c]++;
                      permutation_info[m].target[c]++;
                      seek_models_unique.insert(add_seek_models_unique);
                      if (Suite::Extra())
                        logTab(out::TESTING,"Add seek permutation unique " + add_seek_models_unique);
                    }
                  }
                }
                if (hit)
                {
                  node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[islow]);
                }
                // Define signal resolution as resolution that achieves 99% of total eLLG
                double signal_resolution = REFLECTIONS->data_hires();
                double signal_eLLG = 0.99 * node->seek_cumulative_ellg[islow];
                for (int b = 0; b < cumulative_ellg_by_reso.size(); b++)
                {
                  if (cumulative_ellg_by_reso[b].second >= signal_eLLG)
                  {
                    signal_resolution = 1. / std::sqrt(cumulative_ellg_by_reso[b].first);
                    break;
                  }
                }
                if (Suite::Extra())
                {
                  (io_ellg == imap) ?
                    logTab(out::TESTING,snprintftos("%4d %14s %16.2f %14s",
                                  s+1,"",node->seek_cumulative_ellg[islow],"")):
                    logTab(out::TESTING,snprintftos("%4d %14s %14s %14s %14.2f",
                                  s+1,"","","",node->seek_cumulative_ellg[islow]));
                }
                DAGDB.WORK->SIGNAL_RESOLUTION = std::max(signal_resolution,DAGDB.WORK->SIGNAL_RESOLUTION);
              }

              if (io_ellg == imap) // mapeLLG
              {
                reached_target = false;
                // Keep track of the cumulative increase of eLLG with resolution
                std::vector<std::pair<double,double> > cumulative_mapellg_by_reso;
                int nref = REFLECTIONS->NREFL;
                double nref_by_100(nref / 100.);
                int next_percentile = 1;
                for (int r = 0; r < nref; r++)
                {
                  if (selected.get_selected(r) and present[r])
                  {
                    const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
                    const double feff = work_row->get_feffnat();
                    const double dobs = work_row->get_dobsnat();
                    const double resn = work_row->get_resn();
                    const double teps = work_row->get_teps();
                    const double ssqr = work_row->get_ssqr();
                    const bool cent = work_row->get_cent();
                    double DobsSiga = dobs*Siga[r];
                    double Eeff = feff/(resn*std::sqrt(teps));
                    double expected_LLG = likelihood_emap.get(Eeff,DobsSiga,cent);
                           expected_LLG *= REFLECTIONS->oversampling_correction();
                    ellgdata[r] += expected_LLG;
                    node->seek_cumulative_ellg[imap] += expected_LLG;
                    if (r/nref_by_100 > next_percentile or r == nref)
                    {
                      cumulative_mapellg_by_reso.push_back({ssqr, node->seek_cumulative_ellg[imap]});
                      next_percentile = floor(r/nref_by_100) + 1;
                    }

                    if (s == 0 and
                        !reached_target and
                        node->seek_cumulative_ellg[imap] > io_mapellg_target)
                    {
                      reached_target = true;
                      at_least_one_reached_ellg_target = true;
                      node->SEEK_RESOLUTION_PHASED = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                      // node->SEEK_COMPONENTS_PHASED = s+nmol;
                      // node->SEEK_COMPONENTS_ELLG_PHASED = node->seek_cumulative_ellg[imap];
                      if (Suite::Extra())
                        logTab(out::TESTING,snprintftos("%4d %14.2f %14s %14s %14s",
                                 s+1,node->SEEK_RESOLUTION_PHASED,"","",""));
                    }
                  }
                }
                // Define signal resolution as resolution that achieves 99% of total eLLG
                double signal_resolution = REFLECTIONS->data_hires();
                double signal_eLLG = 0.99 * node->seek_cumulative_ellg[imap];
                for (int b = 0; b < cumulative_mapellg_by_reso.size(); b++)
                {
                  if (cumulative_mapellg_by_reso[b].second >= signal_eLLG)
                  {
                    signal_resolution = 1. / std::sqrt(cumulative_mapellg_by_reso[b].first);
                    break;
                  }
                }
                if (Suite::Extra())
                  logTab(out::TESTING,snprintftos("%4d %14s %16s %16.2f",
                                 s+1,"","",node->seek_cumulative_ellg[imap]));
                DAGDB.WORK->SIGNAL_RESOLUTION_PHASED = signal_resolution;
              }

              if (s == 0) //this is the minimum even if it is not target ellg
              {
                maxlen_seek_models_unique = std::max(maxlen_seek_models_unique,1);
                if (io_ellg == imap)
                {
                  node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[islow]);
                }
                else
                {
                  node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[io_ellg]);
                }
              }
              if (io_ellg == imap)
              {
                seek_ellg[s][io_ellg] = std::max(1.,node->seek_cumulative_ellg[islow]);
              }
              else
              {
                seek_ellg[s][io_ellg] = std::max(1.,node->seek_cumulative_ellg[io_ellg]);
              }
              double increase = (s == 0)  ?
                         seek_ellg[s][io_ellg] :
                         seek_ellg[s][io_ellg] - seek_ellg[s-1][io_ellg];
              node->SEEK[s].seek_increases = std::max(1.,increase);
              if (!reached_target and
                 (s == node->SEEK.size()-1 or //last
                  s == io_maxunique-1 or //first search has too many components for permuting
                  increase > io_ellg_target))
              {
                reached_target = true;
                at_least_one_reached_ellg_target = true;
                node->SEEK_RESOLUTION = 0; //na
                if (increase > io_ellg_target)
                  node->SEEK_COMPONENTS = std::min(int(node->SEEK.size()*nmol),s*nmol+nmol);
                else if (s == io_maxunique-nmol)
                  node->SEEK_COMPONENTS = std::min(int(node->SEEK.size()*nmol),io_maxunique); //multiples of nmol
                else
                  node->SEEK_COMPONENTS = node->SEEK.size()*nmol;
                node->SEEK_COMPONENTS_ELLG = std::max(1.,node->seek_cumulative_ellg[io_ellg]);
                std::string add_seek_models_unique = "";
                for (int ss = 0; ss < node->SEEK_COMPONENTS/nmol; ss++)
                {
                  add_seek_models_unique += node->SEEK[ss].IDENTIFIER.string()+" ";
                }
                maxlen_seek_models_unique = std::max(maxlen_seek_models_unique,node->SEEK_COMPONENTS+nmol);
                int c = node->SEEK_COMPONENTS/nmol-1;
                permutation_info[m].counts[c]++;
                if (increase > io_ellg_target)
                  permutation_info[m].target[c]++;
                seek_models_unique.insert(add_seek_models_unique);
                if (Suite::Extra())
                  logTab(out::TESTING,"Add seek permutation unique " + add_seek_models_unique);
              }
            }
            if (Suite::Extra() and io_ellg == imap)
            {
              af_string table; Loggraph loggraph;
              std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
                  ellgdata,present,selected.get_selected_array(),"Expected LLGI","eLLGI",false);
              logTabArray(out::TESTING,table);
              logGraph(out::TESTING,loggraph);
            }
            loop_dag.push_back(newnode);
            if (!logProgressBarNext(progress)) break;
          }
          while (std::next_permutation(short_Seek.begin(), short_Seek.end(),cmp) and npermutations > 1);
          logProgressBarEnd(progress);
        }
        logEllipsisShut(out::LOGFILE);

        {{
        out::stream where = out::SUMMARY;
        logTableTop(where,"Permutations");
        logTab(where,"Maximum depth = " + itos(io_maxunique));
        logTab(where,"unique  unique permutations to point of achieving ellg target");
        logTab(where,"depth   unique permutations per number of components");
        logTab(where,"target  unique permutations per number of components reaching ellg target");
        size_t maxseek(0);
        for (int m = 0; m < permutation_info.size(); m++)
          maxseek = std::max(maxseek,permutation_info[m].counts.size());
        sv_int header(maxseek,0);
        size_t w = itos(header.size()).size();
        for (int m = 0; m < permutation_info.size(); m++)
          for (int i = 0; i < permutation_info[m].counts.size(); i++)
            w = std::max(w,itos(permutation_info[m].counts[i]).size());
        for (int i = 0; i < header.size(); i++) header[i] = i+1;
        int cw = std::max(6,int(ivtos(header,w).size()));
        logTab(where,snprintftos(
             "%-8s %7s %12s %6s   : %*s : %*s :",
             "i#",
             "seeking",
             "permutations",
             "unique",
             cw,"depth",
             cw,"target"
             ));
        for (int m = 0; m < permutation_info.size(); m++)
        {
          logTab(where,snprintftos(
             "%-8d %7d %12d %6d   : %*s : %*s :",
             m+1,
             permutation_info[m].nseek,
             permutation_info[m].total,
             permutation_info[m].unique,
             cw,ivtos(permutation_info[m].counts,w).c_str(),
             cw,ivtos(permutation_info[m].target,w).c_str()
             ));
        }
        logTableEnd(where);
        }}
        output_dag = loop_dag;
        //if (at_least_one_reached_ellg_target) break;
        //no, we want to keep going in case there is one good one that turns out not to be good
      }

      if (!at_least_one_reached_ellg_target)
        logWarning("No permutations reached ellg target");

      {{
      out::stream where = out::LOGFILE;
      logChevron(where,"Expand seek by permutations");
      int before = DAGDB.size();
      DAGDB.NODES = output_dag;
      logTab(where,"Number before expanding permutations: " + itos(before));
      logTab(where,"Number after expanding permutations:  " + itos(DAGDB.size()));
      }}

      {{
      out::stream where = out::LOGFILE;
      DAGDB.restart();
      int m(0);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK;
        node->generic_int = m++; //original index for lookup in table
        for (int i = 0; i < node->SEEK.size(); i++)
          node->generic_str += dtos(node->SEEK[i].seek_increases) + " ";
      }
      if (DAGDB.size() > 1)
      {
        logEllipsisOpen(where,"Sort into search order");
        DAGDB.NODES.apply_sort_for_search();
        logEllipsisShut(where);
      }
      }}

//the seeks have already divided out the tncs

/*
      auto tags = input.value__strings.at(".phasertng.permutations.tags_in_search_order.").value_or_default();
      if (tags.size())
      {
        out::stream where = out::LOGFILE;
        logEllipsisOpen(where,"Store the searches according to input search order");
        int before = DAGDB.size();
        DAGDB.restart();
        while (!DAGDB.at_end())
        {
          bool& generic_flag = DAGDB.WORK->generic_flag;
          generic_flag = (ntag == DAGDB.WORK->SEEK.size());
          if (generic_flag)
            for (int i = 0; i < ntag; i++)
              generic_flag = generic_flag and DAGDB.WORK->SEEK[i].IDENTIFIER.tag() == tags[i];
        }
        DAGDB.NODES.erase_invalid();
        logEllipsisShut(where);
        logTab(where,"Number until purge: " + itos(before));
        logTab(where,"Number after purge: " + itos(DAGDB.size()));
        DAGDB.restart();
      }
*/

      int nmax = input.value__int_number.at(".phasertng.expected.ellg.maximum_stored.").value_or_default();
      if (nmax > 0 and DAGDB.size() > nmax) //flag for use all
      {
        out::stream where = out::LOGFILE;
        logEllipsisOpen(where,"Store the searches that are most likely to generate a solution");
        int before = DAGDB.size();
        DAGDB.restart();
        int m(0);
        while (!DAGDB.at_end())
        {
          DAGDB.WORK->generic_flag = bool(m < nmax);
          m++;
        }
        DAGDB.NODES.erase_invalid();
        logEllipsisShut(where);
        logTab(where,"Maximum stored = " + itos(nmax));
        logTab(where,"Number until purge: " + itos(before));
        logTab(where,"Number after purge: " + itos(DAGDB.size()));
      }

      if (DAGDB.size())
      {{
      out::stream where = out::SUMMARY;
      logTableTop(where,"Model eLLG");
      size_t p = 8; //work out width of seek-llg column
      size_t q = 12; //work out width of llg-increase column
      for (int m = 0; m < DAGDB.NODES.size(); m++)
      {
        size_t w = dtos(DAGDB.NODES[m].SEEK[0].seek_increases,0).size();
        p = std::max(p,w);
        q = std::max(q,w);
        double seek_total = DAGDB.NODES[m].SEEK[0].seek_increases;
        for (int i = 1; i < DAGDB.NODES[m].SEEK.size(); i++)
        {
          seek_total += DAGDB.NODES[m].SEEK[i].seek_increases;
          w = dtos(seek_total,0).size();
          p = std::max(p,w);
          std::string llgstr = "+" + dtos(DAGDB.NODES[m].SEEK[i].seek_increases,0) + " (=" + dtos(seek_total,0) + ")";
          q = std::max(q,llgstr.size());
        }
      }
      size_t t = 2; //formatting
      t = std::max(t,itos(DAGDB.size()).size());
      logTab(where,"Target eLLG = " + dtos(io_ellg_target,10,2));
      logTab(where,"Data Resolution = " + dtos(REFLECTIONS->data_hires(),7,4));
      logTab(where,"Seek Multiplicity = " + itos(nmol));
      logTab(where,"Seek Number = " + itos(DAGDB.WORK->SEEK.size()));
      logTab(where,snprintftos(
          //   1   2    3   4     5    6   7    8   9   10 11
           "%-*s %-*s %6s %6s %6s %12s %11s %8s %*s %3s %-*s seek",
           t,"o#", //1
           t,"i#", //2
           " eLLG", //3
           "eeLLG", //4
           "aeLLG", //5
           "seek-reso", //6
           "signal-reso", //7
           "seek-num", //8
           p,"seek-llg", //9
           "#", //10
           q,"llg-increase" //11
           ));
      DAGDB.restart();
      int mm(1);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK;
        auto& vslow = node->seek_cumulative_ellg[islow];
        auto& vmidl = node->seek_cumulative_ellg[imidl];
        auto& vfast = node->seek_cumulative_ellg[ifast];
        auto& vmap = node->seek_cumulative_ellg[imap];
        vslow = vslow ? std::max(1.,node->seek_cumulative_ellg[islow]) : 0;
        vmidl = vmidl ? std::max(1.,node->seek_cumulative_ellg[imidl]) : 0;
        vfast = vfast ? std::max(1.,node->seek_cumulative_ellg[ifast]) : 0;
        vmap  = vmap  ? std::max(1.,node->seek_cumulative_ellg[imap])  : 0;
        int m = node->generic_int; //original sort order
        int s(nmol);
        double seek_total = node->SEEK[0].seek_increases;
        std::string dslow = vslow>99999 ? ">99999":dtos(node->seek_cumulative_ellg[islow],6,0);
        std::string dmidl = vmidl>99999 ? ">99999":dtos(node->seek_cumulative_ellg[imidl],6,0);
        std::string dfast = vfast>99999 ? ">99999":dtos(node->seek_cumulative_ellg[ifast],6,0);
        std::string dseek = node->SEEK_COMPONENTS_ELLG>99999 ? ">99999":dtos(node->SEEK_COMPONENTS_ELLG,6,0);
        logTab(where,snprintftos(
          //     1   2    3   4     5    6   7    8   9    10  11
               "%-*d %-*d %6s %6s %6s %12s %11s %8s %*s %3d %-*s %s",
               t,mm++,
               t,m+1,
               vslow ? dslow.c_str() : "--na--",
               vmidl ? dmidl.c_str() : "--na--",
               vfast ? dfast.c_str() : "--na--",
               node->SEEK_RESOLUTION ? dtos(node->SEEK_RESOLUTION,11,2).c_str() : "(--all--)",
               node->SIGNAL_RESOLUTION ? dtos(node->SIGNAL_RESOLUTION,11,2).c_str() : "(--all--)",
               itos(node->SEEK_COMPONENTS).c_str(),
               p,node->SEEK_COMPONENTS_ELLG ? dseek.c_str() : "--na--",
               s,
               q,dtos(node->SEEK[0].seek_increases,0).c_str(),
               node->SEEK[0].IDENTIFIER.str().c_str()
               ));
        for (int i = 1; i < node->SEEK.size() and i < node->SEEK_COMPONENTS/nmol; i++)
        {
          s += nmol;
          seek_total += node->SEEK[i].seek_increases;
          std::string llgstr = "+" + dtos(node->SEEK[i].seek_increases,0) + " (=" + dtos(seek_total,0) + ")";
          logTab(where,snprintftos(
               "%-*s %-*s %6s %6s %6s %12s %11s %8s %*s %3d %-*s %s",
               t,"",t,"","","","","","","",
               p,"",
               s,
               q,llgstr.c_str(),
               node->SEEK[i].IDENTIFIER.str().c_str()
               ));
        }
      }
      logTableEnd(where);
      }}

      double minres = 3.0;
      if (DAGDB.size())
      {{
      out::stream where = out::LOGFILE;
      DAGDB.restart();
      int modified(0);
      int signaled(0);
      logTab(where,"Minimum seek resolution: " + dtos(minres,2));
      logTab(where,"Check all permutation have minimum seek resolution: " + dtos(DAGDB.WORK->SEEK_RESOLUTION,2));
      logTab(where,"Check seek resolution is greater than signal resolution: " + dtos(DAGDB.WORK->SIGNAL_RESOLUTION,2));
      logEllipsisOpen(where,"Checking");
      while (!DAGDB.at_end())
      {
        if (DAGDB.WORK->SEEK_RESOLUTION > minres)
        {
          modified++;
          DAGDB.WORK->SEEK_RESOLUTION = minres;
        }
        if (DAGDB.WORK->SEEK_RESOLUTION < DAGDB.WORK->SIGNAL_RESOLUTION)
        {
          signaled++;
          DAGDB.WORK->SEEK_RESOLUTION = DAGDB.WORK->SIGNAL_RESOLUTION;
        }
      }
      logEllipsisShut(where);
      if (modified > 0)
        logTab(where,"Seek resolution(s) set to minimum: "+itos(modified)+" of "+itos(DAGDB.size()));
      else
        logTab(where,"Seek resolution(s) already have minimum");
      if (signaled > 0)
        logTab(where,"Seek resolution(s) set to signal resolution: "+itos(signaled)+" of "+itos(DAGDB.size()));
      }}
    }

    DAGDB.shift_tracker(hashing(),input.running_mode);
    input.clear(".phasertng.expected.subdir.");
    input.clear(".phasertng.expected.filenames_in_subdir.");
    if (DAGDB.size())
    { //these files are used for bones and scripting so there is a generous write condition
      //write_xml is the condition for bones
      bool write_perm = (WriteFiles() or WriteCards() or WriteXml());
      DAGDB.restart();
      int m(0);
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Dag Files per Node");
      while (!DAGDB.at_end())
      {
        Identifier ultimate = DAGDB.PATHWAY.ULTIMATE;
        std::string counter = ntos(m+1,DAGDB.size());
        FileSystem fileout(DataBase(),ultimate.FileName("."+counter+".dag.cards"));
        DAGDB.unparse_card_node(fileout,m++);
        logFileWritten(out::LOGFILE,write_perm,"Dag Node "+counter,fileout);
        input.push_back(".phasertng.expected.subdir.",fileout.filepath);
        input.push_back(".phasertng.expected.filenames_in_subdir.",fileout.filename);
      }
    }
  }

} //phasertng
