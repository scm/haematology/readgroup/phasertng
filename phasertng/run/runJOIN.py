import sys
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from phasertng.scripts.picard_jiffies import *
from phasertng.phil.mode_generator import isNone
from phasertng.phil.master_node_file import node_separator
import os, os.path

#these are internal parameters not interesting for the user
class runJOIN:

  def match_current_seeks(self,modedata,current_seeks) -> bool:
    tmp = modedata.get_current_seeks()
    if len(tmp) != len(current_seeks): return False
    return all(cs == tmp[i] for i, cs in enumerate(current_seeks))

  def has_preliminary_match_solution(self,basetng,current_seeks):
   #use this to check if there is at least something interesting in the list for identical match
    refltncsinfo = str(basetng.REFLECTIONS.get_tncsinfo())
    reflcellz = int(basetng.REFLECTIONS.Z)
    reflcryst = basetng.REFLECTIONS.get_crystal()
    if basetng.mrsolution is None: return False
    if len(basetng.mrsolution) == 0: return False
    if int(basetng.DAGDATABASE.size()) == 0: return False
    for k,v in basetng.mrsolution.items():
        if self.match_current_seeks(v,list(current_seeks)): #list comparator, scitbx_stl_vector_ext.unsigned
          if str(v.tncs_info) == refltncsinfo: #no size
            if v.sg_info == reflcryst:
              if not basetng.restart_at_cell_content_scaling or (basetng.restart_at_cell_content_scaling and int(v.ccs) == reflcellz):
                  return True
    return False

  def subset_of_mrsolution(self,basetng,full_search):
    rundict = None
    nbest = int(0)
    refltncsinfo = str(basetng.REFLECTIONS.get_tncsinfo())
    reflcellz = int(basetng.REFLECTIONS.Z)
    reflcryst = basetng.REFLECTIONS.get_crystal()
    tfzobj = tfzscore()
    for k,v in basetng.mrsolution.items():
        if tfzobj.reached_certain_tfz(v): #this means that the current_seek is the full solution
          if str(v.get_tncs_info()) == refltncsinfo:
            if v.get_sg_info() == reflcryst:
              #this v.ccs must be from a v.restart_at_cell_content_scaling
              raccs = basetng.restart_at_cell_content_scaling and v.get_restart_at_cell_content_scaling()
              if not basetng.restart_at_cell_content_scaling or (raccs and int(v.get_ccs()) == reflcellz):
                  plist = list(v.get_current_seeks()) #this will be the current pose list also
                  npart = int(len(plist))
                  for ident in full_search:
                    if ident in plist:
                      plist.remove(ident) #removes first occurrence
                  if int(len(plist)) == 0: #does the check match the reference identically up to this point?
                    if (npart > nbest): #first of the highest number of matches amongst the partial solutions available
                      rundict = copy.deepcopy(v)
                    # rundict.dagcards = v.dagcards #explicit copy here of uncopied member
                      nbest = copy.deepcopy(npart)
                     #no break, get best npart
    return rundict

  def match_identical_solution(self,basetng,current_seeks):
    rundict = None
    refltncsinfo = str(basetng.REFLECTIONS.get_tncsinfo())
    reflcellz = int(basetng.REFLECTIONS.Z)
    reflcryst = basetng.REFLECTIONS.get_crystal()
    z = int(basetng.REFLECTIONS.Z)
    if basetng.mrsolution is None: return
    for k,v in basetng.mrsolution.items():
        if self.match_current_seeks(v,list(current_seeks)): #list comparator, scitbx_stl_vector_ext.unsigned
          if str(v.get_tncs_info()) == refltncsinfo: #no size
            if v.get_sg_info() == reflcryst:
              restart_at_cell_content_scaling = basetng.restart_at_cell_content_scaling and v.restart_at_cell_content_scaling
              if not basetng.restart_at_cell_content_scaling or (restart_at_cell_content_scaling and int(v.ccs) == reflcellz):
                  rundict = copy.deepcopy(v)
              #   rundict.dagcards = v.dagcards #explicit copy here of uncopied member
                  break
    return rundict

  def print_previous(self,mrsolution):
   #{{{
      msg = "Previous solution summary"
      if (mrsolution) is None:
        return msg + "\nNo current solutions"
      nsolution = len(mrsolution)
      if nsolution == 0:
        msg = msg + "\nNo current solutions"
      elif nsolution == 1:
        msg = msg + "\n1 current solution"
      elif nsolution >= 2:
        msg = msg + "\n" + str(nsolution) + " current solutions"
      if nsolution > 0:
        msg = msg + "\nCurrent solutions (ordered):"
        for k,rundict in mrsolution.items():
            rundict_current_seeks = rundict.get_current_seeks()
            if (len(rundict_current_seeks) > 0): # because should come from store_mrsolution
              txt = ' '.join(str(x) for x in rundict_current_seeks)
              ncsstr = " /tncs=" +  str(rundict.get_tncs_info())
              ccsstr = "/z=" +  str(int(rundict.get_ccs()))
              restartccsstr = "*" if rundict.get_restart_at_cell_content_scaling() else ""
              sgstr = "/" +  str(rundict.get_sg_info()) #starts PG eg PG121
              pathway = "[" +  str(rundict.get_pathway_string()) + "] "
              msg = msg + "\n---"+rundict.get_message()[:8] + pathway + txt + ncsstr + ccsstr + restartccsstr+ sgstr
      # for k in mrsolution.keys(): # equivalently
      #   msg = msg + "\n---"+k
      #cca, tncs
      return msg
   #}}}

  def print_current(self,basetng):
   #{{{
    refltncsinfo = str(basetng.REFLECTIONS.get_tncsinfo())
    reflcellz = int(basetng.REFLECTIONS.Z)
    reflcryst = basetng.REFLECTIONS.get_crystal()
    msg = "Current seeks:"
    current_seeks = basetng.DAGDATABASE.list_of_current_seeks()
    txt = ' '.join(str(x) for x in current_seeks)
    ncsstr = " /tncs=" +  str(refltncsinfo)
    ccsstr = "/z=" +  str(int(reflcellz))
    sgstr = "/" +  str(reflcryst) #starts PG eg PG121
    msg = msg + "\n---"+ txt + ncsstr + ccsstr + sgstr
    return msg
   #}}}

  def run_from_basetng_and_input(self,basetng):
    self.inner_join(basetng=basetng)

  def inner_join(self,
          basetng,
          ):
  #{{{
    # this is a generic script that can be used to
    # A) merge information from different entries
    # B) print the results of a loosely formatted string on input.generic_string
    # C) add your own here!

    DAGDB = basetng.DAGDATABASE
  # assert(DAGDB.size())  #or else the hashing is not specific #so add the generic string instead
  # DAGDB.shift_tracker(str(basetng.hashing()+basetng.input.generic_string),"join")
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    wf = basetng.Write()
    io_directory = os.path.join(basetng.DataBase(),subdir)
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(io_directory):
        os.makedirs(io_directory)

    basetng.logTab(enum_out_stream.logfile,"Join type #"+str(chr(basetng.input.generic_int)))
    if basetng.input.generic_int == ord('C'):
   #{{{
      cards = basetng.input.generic_string.split(node_separator)
      basetng.logUnderLine(enum_out_stream.summary,"Join Entries to Database")
      for card in cards:
        if len(card):
          basetng.logChevron(enum_out_stream.logfile,"Entry Information")
          basetng.logTab(enum_out_stream.logfile,card)
          basetng.logBlank(enum_out_stream.logfile)
          DAGDB.append_entry_hold(card)
      basetng.logChevron(enum_out_stream.summary,"Entry Summary")
      lines = DAGDB.logEntries()
      for line in lines:
        basetng.logTab(enum_out_stream.summary,str(line))
      basetng.logBlank(enum_out_stream.summary)
   #}}}
    elif (basetng.input.generic_int == 1):
   #{{{
      #deleted, now runPOOL
      pass
   #}}}
    elif (basetng.input.generic_int == 2):
   #{{{
      #unused
      pass
   #}}}
    elif (basetng.input.generic_int == 3):
   #{{{
      #see runFIND.py
      pass
   #}}}
    elif (basetng.input.generic_int == ord('A')): #4
   #{{{
        #results are reported in runRFAC.py using same generic_int == 4
        basetng.logUnderLine(enum_out_stream.summary,"Join to results of previous refinement")
        nposes = DAGDB.NODES.number_of_poses()
        basetng.logTab(enum_out_stream.summary,"Number of poses: " + str(nposes))
        basetng.logTab(enum_out_stream.summary,"Identifier of match: " + basetng.input.generic_string)
        basetng.logBlank(enum_out_stream.summary)
   #}}}
    elif (basetng.input.generic_int == ord('D')): #5
   #{{{
        basetng.logUnderLine(enum_out_stream.summary,"Join to previous largest high tfz subset solution")
        where = enum_out_stream.verbose
        assert(DAGDB.size() > 0)

        full_search = DAGDB.list_of_all_seeks()
        basetng.logTab(enum_out_stream.logfile,"All seeks:")
        basetng.logTab(enum_out_stream.logfile,"---"+' '.join(str(x) for x in full_search))

        msg = self.print_previous(basetng.mrsolution)
        basetng.logTabArray(enum_out_stream.logfile,msg.split("\n"))
        node = DAGDB.NODES.LIST[0]
        seek_components = copy.deepcopy(DAGDB.NODES.number_of_poses())
        cellz = copy.deepcopy(node.CELLZ)
        reflid = [node.REFLID.identifier(),node.REFLID.tag()]

        #find subset_of_mrsolution
        rundict = self.subset_of_mrsolution(basetng,full_search)
        if rundict is not None:
          basetng.logTab(enum_out_stream.summary,"Previous result will be restored:")
          basetng.logTab(enum_out_stream.summary,rundict.get_message())
          rundict_current_seeks = rundict.get_current_seeks()
          txt = ' '.join(str(x) for x in rundict_current_seeks)
          msg = rundict.get_message()[:8]
          basetng.logTab(enum_out_stream.summary,"---" + msg + "[" + str(rundict.get_pathway_string()) + "] " + txt)
          #basetng.input.set_as_str(".phasertng.dag.cards.subdir.",str(rundict.get_subdir()))
         # basetng.input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",str(rundict.get_dag_cards_filename()))
          #adjust the seeks
          seeks = []
          for card in DAGDB.NODES.LIST[0].SEEK:
            #can't copy Identifier, not boosted enough
            seeks.append(Identifier(card.IDENTIFIER.identifier(),card.IDENTIFIER.tag()))
          assert(len(seeks)>0)
          uid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          pid = [DAGDB.PATHWAY.PENULTIMATE.identifier(),DAGDB.PATHWAY.PENULTIMATE.tag()]
          basetng.logTab(enum_out_stream.logfile,"Load " + rundict.get_dag_cards_filename())
          basetng.load_cards(rundict) #or could read the file
        # pid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          DAGDB.PATHWAY.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHWAY.PENULTIMATE = Identifier(pid[0],pid[1])
          DAGDB.PATHLOG.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHLOG.PENULTIMATE = Identifier(pid[0],pid[1])
          DAGDB.clear_seeks_add_seeks(seeks)
          basetng.input.generic_string = "subset"
          reflidid = Identifier(reflid[0],reflid[1])
          DAGDB.set_join(seek_components,cellz,reflidid)
          DAGDB.reset_seek_components_from_pose() #to poses, replace what is set above because high tfz
          full_search = DAGDB.list_of_all_seeks()
          basetng.logTab(enum_out_stream.logfile,"New all seeks:")
          basetng.logTab(enum_out_stream.logfile,"---"+' '.join(str(x) for x in full_search))
        else:
          basetng.logTab(enum_out_stream.summary,"No previous result will be restored")
          basetng.input.generic_string = ""
          DAGDB.clear_cards() #hack to get end of the line really fast

        if rundict is not None:
          #complete the first lot
          n_first = int(DAGDB.number_of_seek_loops_first())
          basetng.logBlank(enum_out_stream.logfile)
          basetng.logTab(enum_out_stream.logfile,"Number remaining to place first = " + str(n_first))
          n_final = int(DAGDB.number_of_seek_loops_final())
          basetng.logTab(enum_out_stream.logfile,"Number remaining to place = " + str(n_final))
   #}}}
    elif (basetng.input.generic_int == ord('E')): #6
   #{{{
     #this should fail as etfz was the last mode, in which case the current seeks not set
     #and the file has be read in the input here rather than loaded in picard
        basetng.logUnderLine(enum_out_stream.summary,"Join to previous identical solution before signal")
        where = enum_out_stream.verbose
        assert(DAGDB.size() > 0)

        current_seeks = DAGDB.list_of_current_seeks()
        basetng.logTab(enum_out_stream.logfile,"Current seeks:")
        basetng.logTab(enum_out_stream.logfile,"---"+' '.join(str(x) for x in current_seeks))

        full_search = DAGDB.list_of_all_seeks()
        basetng.logTab(enum_out_stream.logfile,"All seeks:")
        basetng.logTab(enum_out_stream.logfile,"---"+' '.join(str(x) for x in full_search))

        msg = self.print_previous(basetng.mrsolution)
        basetng.logTabArray(enum_out_stream.logfile,msg.split("\n"))
        node = DAGDB.NODES.LIST[0]
        seek_components = copy.deepcopy(node.SEEK_COMPONENTS)
        cellz = copy.deepcopy(node.CELLZ)
        reflid = [node.REFLID.identifier(),node.REFLID.tag()]

        rundict = None
        #screen first for just existence of current seeks in list
        #this is then repeated below, but is shown explicitly here
        #function can be used to prescreen/exclude runJOIN=6
        if self.has_preliminary_match_solution(basetng,current_seeks):
          rundict = self.match_identical_solution(basetng,current_seeks)
        if rundict is not None:
          basetng.logTab(enum_out_stream.summary,"Previous result will be restored:")
          basetng.logTab(enum_out_stream.summary,rundict.get_message())
          rundict_current_seeks = rundict.get_current_seeks()
          txt = ' '.join(str(x) for x in rundict_current_seeks)
          #note that failure does not mean end of the line if it is "before signal"
          msg = rundict.get_message()[:8]
          basetng.logTab(enum_out_stream.summary,"---" + msg + "[" + str(rundict.get_pathway_string()) + "] " + txt)
          seeks = []
          for card in DAGDB.NODES.LIST[0].SEEK:
            #can't copy Identifier, not boosted enough
            seeks.append(Identifier(card.IDENTIFIER.identifier(),card.IDENTIFIER.tag()))
          assert(len(seeks)>0)
          uid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          pid = [DAGDB.PATHWAY.PENULTIMATE.identifier(),DAGDB.PATHWAY.PENULTIMATE.tag()]
        # pid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          basetng.logTab(enum_out_stream.logfile,"Load " + rundict.get_dag_cards_filename())
          basetng.load_cards(rundict) #or could read the file
          DAGDB.PATHWAY.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHWAY.PENULTIMATE = Identifier(pid[0],pid[1])
          DAGDB.PATHLOG.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHLOG.PENULTIMATE = Identifier(pid[0],pid[1])
          #there will not have been a subset found at this point because if there was = "after signal"
          #no subset means seek order is the same
          #copy the seek components which may be longer that the ones in the matched set
          DAGDB.clear_seeks_add_seeks(seeks)
          reflidid = Identifier(reflid[0],reflid[1])
          #for before signal, we copy the number required from the input
          DAGDB.set_join(seek_components,cellz,reflidid)
          basetng.logTab(enum_out_stream.logfile,"Set components = " + str(seek_components))
          basetng.logTab(enum_out_stream.logfile,"Set reflid = " + str(reflidid.str()))
          basetng.logTab(enum_out_stream.logfile,"Set cell Z = " + str(cellz))
          basetng.input.generic_string = "restored"
        else:
          basetng.logTab(enum_out_stream.summary,"No previous result will be restored")
          basetng.input.generic_string = ""
          DAGDB.clear_cards() #hack to get end of the line really fast
   #}}}
    elif (basetng.input.generic_int == ord('F')): #7
   #{{{
     #this should fail as etfz was the last mode, in which case the current seeks not set
     #and the file has be read in the input here rather than loaded in picard
        basetng.logUnderLine(enum_out_stream.summary,"Join to previous identical solution after signal")
        where = enum_out_stream.verbose
     #  basetng.logTab(where,"pickle file of current solutions: " + basetng.mrsolution['pickle'])
        assert(DAGDB.size() > 0)
        DAGDB.seek_next() # this is required to increment seek_search otherwise we look for what is already placed

        current_seeks = DAGDB.list_of_current_seeks()
        msg = self.print_current(basetng)
        basetng.logTabArray(enum_out_stream.logfile,msg.split("\n"))

        full_search = DAGDB.list_of_all_seeks()
        basetng.logTab(enum_out_stream.logfile,"All seeks:")
        basetng.logTab(enum_out_stream.logfile,"---"+' '.join(str(x) for x in full_search))

        msg = self.print_previous(basetng.mrsolution)
        basetng.logTabArray(enum_out_stream.logfile,msg.split("\n"))
        node = DAGDB.NODES.LIST[0]
        seek_components = copy.deepcopy(node.SEEK_COMPONENTS)
        cellz = copy.deepcopy(node.CELLZ)
        reflid = [node.REFLID.identifier(),node.REFLID.tag()]
        assert(int(DAGDB.size()) > 0)

        #match identical solutions
        rundict = None
        #screen first for just existence of current seeks in list
        #this is then repeated below, but is shown explicitly here
        #function can be used to prescreen/exclude runJOIN=6
        if self.has_preliminary_match_solution(basetng,current_seeks):
          rundict = self.match_identical_solution(basetng,current_seeks)
        if rundict is not None:
          assert(int(DAGDB.size()) > 0)
          basetng.logTab(enum_out_stream.summary,"Previous result will be restored:")
          basetng.logTab(enum_out_stream.summary,rundict.get_message())
          rundict_current_seeks = rundict.get_current_seeks()
          txt = ' '.join(str(x) for x in rundict_current_seeks)
          #note that failure does not mean end of the line if it is "before signal"
          msg = rundict.get_message()[:8]
          basetng.logTab(enum_out_stream.summary,"---" + msg + "[" + str(rundict.get_pathway_string()) + "] " + txt)
          seeks = []
          for card in DAGDB.NODES.LIST[0].SEEK:
            #can't copy Identifier, not boosted enough
            seeks.append(Identifier(card.IDENTIFIER.identifier(),card.IDENTIFIER.tag()))
          assert(len(seeks)>0)
          uid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          pid = [DAGDB.PATHWAY.PENULTIMATE.identifier(),DAGDB.PATHWAY.PENULTIMATE.tag()]
          basetng.logTab(enum_out_stream.logfile,"Load " + rundict.get_dag_cards_filename())
          basetng.load_cards(rundict) # this resets the InputBase and running_mode
        # pid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
          DAGDB.PATHWAY.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHWAY.PENULTIMATE = Identifier(pid[0],pid[1])
          DAGDB.PATHLOG.ULTIMATE = Identifier(uid[0],uid[1])
          DAGDB.PATHLOG.PENULTIMATE = Identifier(pid[0],pid[1])
          #for the second search if there is a failure then we just kill the return
          #seek list may be longer
          DAGDB.clear_seeks_add_seeks(seeks)
          if msg.strip() == "Failure":
            DAGDB.clear_cards() #hack to get end of the line really fast
            assert(int(DAGDB.size()) == 0)
          elif msg.strip() == "Deadend":
            assert(int(DAGDB.size()) == 0)
          elif msg.strip() == "Partial":
            assert(int(DAGDB.size()) > 0)
          else:
            assert(False) #paranoia
          reflidid = Identifier(reflid[0],reflid[1])
          #for before signal, we copy the number required from the input
          DAGDB.set_join(seek_components,cellz,reflidid)
          DAGDB.reset_seek_components_from_pose() #to poses, replace what is set above because high tfz
          lines = DAGDB.seek_info_all().split('\n')
          for line in lines:
            basetng.logTab(enum_out_stream.logfile,line)
          basetng.logTab(enum_out_stream.logfile,"Set components = " + str(seek_components))
          basetng.logTab(enum_out_stream.logfile,"Set reflid = " + str(reflidid.str()))
          basetng.logTab(enum_out_stream.logfile,"Set cell Z = " + str(cellz))
          basetng.input.generic_string = "restored"
        else:
          basetng.logTab(enum_out_stream.summary,"No previous result will be restored")
          basetng.input.generic_string = ""
          DAGDB.clear_cards() #hack to get end of the line really fast
   #}}}
    #elif (basetng.input.generic_int == 8): # 8==I which is hard to read in the graph display, so skip #8
    elif (basetng.input.generic_int == ord('H')): #9
   #{{{
      basetng.logUnderLine(enum_out_stream.summary,"B-factors")
      cards = basetng.input.generic_string.split("\n")
      basetng.logTabArray(enum_out_stream.summary,cards)
   #}}}
    elif (basetng.input.generic_int == 10):
   #{{{
      from phasertng.scripts.pdbset import pdbset
      basetng.logUnderLine(enum_out_stream.summary,"Perturb coordinates")
      model = isNone(basetng.input.get_as_str(".phasertng.pdbout.filename."))
      basetng.logTab(enum_out_stream.logfile,"Angle limit:" + str(basetng.perturbation.angle_limit))
      basetng.logTab(enum_out_stream.logfile,"Distance limit:" + str(basetng.perturbation.distance_limit))
      filenames,filetags,output = pdbset(
            xyzin=model,
            stem=model,
            preset="perturb_chains",
            tout=False,
            angle_limit=basetng.perturbation.angle_limit,
            distance_limit=basetng.perturbation.distance_limit,
            seed=basetng.perturbation.random_seed)
      cards = output.split("\n")
      basetng.logTabArray(enum_out_stream.summary,cards)
      wf = basetng.WriteData()
      if False: #only if all perturbation in one pdb file
     #{
        assert(len(filenames) == 1)
        basetng.logUnderLine(enum_out_stream.logfile,"Database Entries")
        node = DAGDB.NODES.LIST[0]
        modlid = node.TRACKER.ULTIMATE
        DAGDB.add_entry_copy_header(modlid,node.FULL.IDENTIFIER)
        subdir = DAGDB.lookup_modlid(modlid).database_subdir()
        if wf and not FileSystem().flat_filesystem() and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
            os.makedirs(os.path.join(basetng.DataBase(),subdir))
        pdbout = modlid.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
        newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
        basetng.logFileWritten(enum_out_stream.logfile,wf,"Coordinates",newpdbname)
        if (wf):
          f = open(newpdbname.fstr(), "w")
          f.write(filenames[0])
          f.close()
      # else:
      #   lines = iotbx.file_reader.any_file(iopdb.fstr()).as_pdb_string()
      #   for line in lines:
      #     basetng.input.generic_string = basetng.input.generic_string + line + node_separator
        basetng.input.set_as_str(".phasertng.pdbout.filename.",newpdbname.fstr())
        basetng.input.set_as_str(".phasertng.pdbout.pdbid.",newpdbname.fstr())
        basetng.input.set_as_str(".phasertng.pdbout.id.",str(modlid.identifier()))
        basetng.input.set_as_str(".phasertng.pdbout.tag.",modlid.tag())
        #since this file is written directly, we should load into memory so that dag records it as being present
        basetng.load_entry_from_database(enum_out_stream.logfile,enum_entry_data.coordinates_pdb,modlid,False)
        basetng.logBlank(enum_out_stream.logfile)
     #}
      else:
     #{
        nchain = len(filenames)
        basetng.logTab(enum_out_stream.summary,"Number of chains " + str(nchain))
        for i,pdbstr in enumerate(filenames):
          basetng.input.generic_string = basetng.input.generic_string + pdbstr + node_separator
        basetng.input.generic_string = basetng.input.generic_string[:-len(node_separator)]
        assert(nchain == len(basetng.input.generic_string.split(node_separator)))
     #}
   #}}}
    elif (basetng.input.generic_int == ord('G')): #11
   #{{{
      from phasertng.scripts.pdbset import pdbset
      basetng.logUnderLine(enum_out_stream.summary,"Split residues")
      model = isNone(basetng.input.get_as_str(".phasertng.model.filename."))
      if (model is None):
        raise ValueError(enum_err_code.fatal,"No pdb file")
      basetng.logTab(enum_out_stream.summary,"Pdb file: " + str(model))
      filenames,filetags,output = pdbset(
            xyzin=model,
            stem=model,
            preset="split_residues",
            tout=False)
      cards = output.split("\n")
      basetng.logTabArray(enum_out_stream.summary,cards)
      nres = len(filenames)
      basetng.logTab(enum_out_stream.summary,"Number of residues " + str(nres))
      if (nres == 0):
        raise ValueError(enum_err_code.fatal,"No residues")
      basetng.logUnderLine(enum_out_stream.verbose,"Pdb files in memory")
      for i,pdbstr in enumerate(filenames):
        basetng.logTab(enum_out_stream.verbose,str(pdbstr))
        basetng.input.generic_string = basetng.input.generic_string + pdbstr + node_separator
      basetng.input.generic_string = basetng.input.generic_string[:-len(node_separator)]
      assert(nres == len(basetng.input.generic_string.split(node_separator)))
   #}}}
    elif (basetng.input.generic_int == ord('B')): # 8==I which is hard to read in the graph display, so skip #8
   #{{{
      basetng.logUnderLine(enum_out_stream.summary,"Polish solution")
      basetng.logTab(enum_out_stream.summary,"Solutions will be improved for evalution by refinement protocols")
      cards = basetng.input.generic_string.split("\n")
      basetng.logTabArray(enum_out_stream.summary,cards)
   #}}}
   #because we may have reloaded a mode and modified, shift the trackers here
    DAGDB.shift_tracker(str(basetng.hashing()+basetng.input.generic_string),"join")
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--dagfile",help="dag cards file",type=argparse.FileType('r'),required=True)
  parser.add_argument('-s',"--subdir",help="dag cards subdir",type=argparse.FileType('r'),required=True)
  parser.add_argument('-c',"--cards",help="input cards file",type=argparse.FileType('r'),required=True)
  parser.add_argument('-t',"--type",help="join type",type=int,required=True)
  parser.add_argument('-p',"--pickle",help="pickle file of solution",type=str,required=True)
  args = parser.parse_args()
  cards = None
  if args.cards is not None:
    cards = args.cards.read()
  dagcards = None
  if args.dagcards is not None:
    dagcards = args.dagcards.read()
  basetng = Phasertng("VOYAGER")
  basetng.DAGDATABASE.parse_cards(dagcards) #or could read the file
  ignore_unknown_keys = True
  basetng.input.parse(cards,ignore_unknown_keys)
  basetng.input.set_as_str(".phasertng.dag.cards.subdir.",str(args.subdir))
  basetng.input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",str(args.dagfile))
  basetng.input.generic_int = args.type
  basetng.input.generic_string = "before signal" #hardwire
  import pickle
  basetng.mrsolution = pickle.load(open(args.pickle, "rb"))
  runJOIN().inner_join(basetng=basetng)
  print(basetng.verbose())
  print(basetng.DAGDATABASE.size())
