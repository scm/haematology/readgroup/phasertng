//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Substructure_class__
#define __phasertng_Substructure_class__
#include <phasertng/io/entry/Atoms.h>

namespace phasertng {

  class Substructure : public Atoms
  {
    public:
      //gradient and hessian are only resized if needed
      std::vector<pod::gradient::anom::scatterer> GRAD;
      std::vector<pod::hessian::anom::scatterer>  HESS;
      std::map<std::string,double> dReFHpos_by_dFdp;
      std::map<std::string,double> dReFHneg_by_dFdp;
      std::map<std::string,double> dImFHpos_by_dFdp;
      std::map<std::string,double> dImFHneg_by_dFdp;
      std::map<std::string,double> d2ReFHpos_by_dFdp2;
      std::map<std::string,double> d2ReFHneg_by_dFdp2;
      std::map<std::string,double> d2ImFHpos_by_dFdp2;
      std::map<std::string,double> d2ImFHneg_by_dFdp2;
      bool REFINE_SITE = false;
      bool REFINE_BFAC = false;
      bool REFINE_OCC = false;
      bool REFINE_FDP = false;

    public: //constructors
      Substructure() : Atoms() {}
      Substructure(af_string pdblines) : Atoms(pdblines) {}

    public:
      std::pair<cmplex,cmplex>
      calcAtomicDataSum(
          const millnx&,
          const double,
          const double,
          const_ScatterersPtr,
          bool,
          bool
      );

  };

} //phasertng
#endif
