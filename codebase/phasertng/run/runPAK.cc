//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/pod/pakdat.h>

namespace phasertng {

  //Packing
  void Phasertng::runPAK()
  {
    pod::pakflag pak_flag;
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::LOGFILE,"No poses");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::TRACE_PDB,id,false);
    }}

    int  io_nprint = input.value__int_number.at(".phasertng.packing.maximum_printed.").value_or_default();
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    if (DAGDB.WORK->TNCS_MODELLED)
      logTab(out::VERBOSE,"tNCS in model");
    else if (DAGDB.WORK->TNCS_ORDER > 1)
      logTab(out::VERBOSE,"tNCS in data = " + itos(DAGDB.WORK->TNCS_ORDER));
    DAGDB.restart();
    bool no_cell_translation(REFLECTIONS->MAP);
    if (no_cell_translation)
      logTab(out::LOGFILE,"Data is a map, unit cell translations will not be considered");

    //identifier for output coordinates, if required
    int k(0);
    int n(1),N(DAGDB.size());
    out::stream progress = out::LOGFILE;
    logBlank(progress);
    logProgressBarStart(progress,"Packing",DAGDB.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      std::string nN = nNtos(n++,N);
      out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
      logUnderLine(where,"Packing"+nN);
      int NPOSE = DAGDB.WORK->POSE.size();
      Packing packing(input.value__flt_number.at(".phasertng.packing.distance_factor.").value_or_default());
      //get coordinates for trace
      packing.init_node(DAGDB.WORK);
      if (Suite::Extra())
      {
        logTabArray(out::TESTING,DAGDB.WORK->logPose("Initial"));
      }
      //we are only going to record clashes
      //site symmetry for overlaps
      //move all close to origin
      //move all close to the closest to origin
      dvect3 origin(0,0,0);
      bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,out::VERBOSE);
      if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str());
      bool io_move_to_origin = false;
      if (REFLECTIONS->MAP)
      {
        origin = REFLECTIONS->MAPORI;
        io_move_to_origin = true;
        logTab(where,"Map Origin: " + dvtos(REFLECTIONS->MAPORI));
      }
      packing.move_to_origin(io_move_to_origin,origin,-999); //function must be called to set internal parameters
      if (Suite::Extra() and packing.MOVED_TO_ORIGIN)
      {
        logTabArray(out::TESTING,DAGDB.WORK->logPose("Moved to Origin"));
      }
      packing.multiplicity_and_exact_site();
      auto pakid = DogTag(DAGDB.PATHWAY.ULTIMATE,DAGDB.WORK->TRACKER.ULTIMATE);
      if (Suite::Extra())
      {
        for (int p = 0; p < NPOSE; p++)
        {
          std::string cc = ntos(p+1,NPOSE);
          FileSystem PDBOUT(DataBase(),pakid.FileName("."+DAGDB.WORK->POSE[p].IDENTIFIER.tag() +"." + ntos(p+1,NPOSE) + ".origin.pdb"));
          logFileWritten(out::TESTING,WriteFiles(),"Debug Move-to-Origin",PDBOUT);
          PDBOUT.create_directories_for_file();
          if (WriteFiles()) packing.WritePosePdb(PDBOUT,p);
        }
      }

      //there are optimizations in this routine, expand reference to cell + a bit
      bool last_only(false);
      packing.calculate_packing(last_only,no_cell_translation);
      DAGDB.WORK->clash_matrix = packing.PACKING_DATA.matrix_as_matrix();
      DAGDB.WORK->CLASH_WORST = packing.PACKING_DATA.worst_percent();
      DAGDB.WORK->generic_int = packing.PACKING_DATA.worst_ratio().first;
      DAGDB.WORK->generic_int2 = packing.PACKING_DATA.worst_ratio().second;
      DAGDB.WORK->generic_str = dvtos(packing.PACKING_DATA.sorted_percent(10),0);
      if (Suite::Extra())
      {
        for (int p = 0; p < NPOSE; p++)
        {
          std::string cc = ntos(p+1,NPOSE);
          FileSystem PDBOUT(DataBase(),pakid.FileName("."+DAGDB.WORK->POSE[p].IDENTIFIER.tag() +"." + ntos(p+1,NPOSE) + ".clash.pdb"));
          PDBOUT.create_directories_for_file();
          logFileWritten(out::TESTING,WriteFiles(),"Debug Packing",PDBOUT);
          if (WriteFiles()) packing.WritePosePdb(PDBOUT,p);
        }
      }

      //don't test coordinates for overlap any more as this is not general with maps
      //as it requires the trace to retain the pointgroup symmetry
      //and as it requires the trace to maintain the chain identities of the monomers
      //just look at the point group
      //if the point group has a symmetry operator the same as the site symmetry
      //then this *could* but not necessarily be a site on a special position obeying symmetry
      //then flag this one as one that you may want to look at even if it clashes
      DAGDB.WORK->SPECIAL_POSITION = packing.calculate_special_position_warning() ? 'Y' : 'N';
      if (input.value__boolean.at(".phasertng.packing.clash_coordinates.").value_or_default())
      {
        for (int p = 0; p < NPOSE; p++)
        {
          std::string cc = ntos(p+1,NPOSE);
          FileSystem coords(DataBase(),pakid.FileName(cc+".packing.pdb"));
          coords.create_directories_for_file();
          logFileWritten(out::LOGFILE,true,"Packing Clash Coordinate",coords);
          if (WriteFiles())
            packing.WritePosePdb(coords,p);
        }
      }
      logBlank(where);
      logTabArray(where,packing.logClashes(""));
      if (!logProgressBarNext(progress)) break;
      k++;
    }
    logProgressBarEnd(progress);

    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      if (DAGDB.WORK->SPECIAL_POSITION == 'Y')
      {
        logWarning("Model(s) with point group symmetry placed on special position(s)");
        break; //once
      }
    }

    {{
    out::stream where = out::VERBOSE;
    logTableTop(where,"Packing Clashes");
    int nposes = DAGDB.NODES.number_of_poses();
    if (nposes) //DAGDB.size == 0
    {
      logTab(where,"Background Poses: " + itos(nposes-nmol));
      logTab(where,"Poses: " + itos(nposes));
    }
    logTab(where,"* Special Position");
    logTab(where,"Worst clash is reported");
    logTab(where,"--------");
    int w = itow(io_nprint);
    int i(1);
    logTab(where,snprintftos(
        "%-*s %-13s %12s %11s  %c %10s %7s",
        w,"#","Space Group","Worst-Clash%","Fraction",'*',"LLG","TFZ"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP); //REFLECTIONS is nullptr
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      std::string frac = (DAGDB.WORK->CLASH_WORST == 0) ?
          "n/a" : std::string(itos(node->generic_int) + "/" + itos(node->generic_int2));
      logTab(where,snprintftos(
          "%-*d %-13s %11.2f%% %11s  %c % 10.*f %7.2f",
          w,i,
          node->SG->sstr().c_str(),
          node->CLASH_WORST,
          frac.c_str(),
          node->SPECIAL_POSITION,
          q,REFLECTIONS->MAP ? node->MAPLLG : node->LLG,
          node->ZSCORE
        ));
      bool add_blank(false);
      logTabArray(where,node->logPose("",add_blank));
      if (node->CLASH_WORST > 0) //only if there are clashes that need reporting
        logTabArray(where,node->logClash());
      logTab(where,"--------");
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    double io_worst_clash = input.value__percent.at(".phasertng.packing.percent.").value_or_default();
    bool io_keep_hightfz = (input.value__boolean.at(".phasertng.packing.keep_high_tfz.").value_or_default());
    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Packing Flags");
    double io_hightfz = input.value__flt_number.at(".phasertng.packing.high_tfz.").value_or_default();
    double io_overlap = input.value__percent.at(".phasertng.packing.overlap.").value_or_default();
    DAGDB.restart();
    int count(0),noverlap(0);
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      node->generic_flag = false; //for purge
      if (node->CLASH_WORST <= (io_worst_clash+DEF_PPM))
      {
        node->PACKS = pak_flag.Y;
        node->generic_flag = true; //don't erase
      }
      else if (io_keep_hightfz and node->ZSCORE >= io_hightfz)
      {
        if (node->CLASH_WORST >= io_overlap)
        {
          node->PACKS = pak_flag.O;
          noverlap++;
        }
        else
        {
          node->PACKS = pak_flag.Z;
          node->generic_flag = true; //don't erase
        }
      }
      else
      {
        node->PACKS = pak_flag.X;
        count++;
      }
      node->ANNOTATION += " PAK=" + dtoss(node->CLASH_WORST,0) + "%/" +chtos(node->PACKS);
    }
    logEllipsisShut(where);
    {
      out::stream where = out::SUMMARY;
      logTab(where,"Worst clash for packing check = " + dtos(io_worst_clash,2) + "%");
      logTab(where,"Keep high TFZ despite clashes = " + btos(io_keep_hightfz));
      if (io_keep_hightfz)
      {
        logTab(where,"High TFZ for keeping if clash = " + dtos(io_hightfz,2));
        logTab(where,"But reject overlap with clash > " + dtos(io_overlap,2) + "%");
        logTab(where,"Number High TFZ Rescued:    "  + itos(count));
        logTab(where,"Number High TFZ Overlapped: "  + itos(noverlap));
      }
    }
    logBlank(where);
    }}

    int count_packs(0);
    for (auto& node : DAGDB.NODES)
      if (pak_flag.packs_YZ(node.PACKS)) count_packs++;
    {{
    int w = itow(io_nprint);
    int ww = itow(DAGDB.size());
    out::stream where = out::LOGFILE;
    logTableTop(where,"Packing Results");
    for (auto item : pak_flag.info)
      logTab(where,chtos(item.first) + " " + item.second);
    int nposes = DAGDB.NODES.number_of_poses();
    if (nposes) //DAGDB.size == 0
    {
      logTab(where,"Background Poses: " + itos(nposes-nmol));
      logTab(where,"Poses: " + itos(nposes));
    }
    logTab(where,"Number packing: " + itos(count_packs));
    logTab(where,"* Special Position");
    logTab(where,"--------");
    logTab(where,snprintftos(
        "%*s %*s Packs %7s %11s * %9s %7s   %-13s  %s",
        w,"#",ww,"##","Worst%","Fraction","LLG","TFZ","Space Group","Ten-Worst-Clashes(%)"));
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP); //REFLECTIONS is nullptr
    int i(1);
    int kk = 0;
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      logTab(where,snprintftos(
          "%*d %*s %c%4s %6.2f%% %11s %c % 9.*f %7.2f   %-13s  %s",
          w,i,
          ww,pak_flag.packs_YZ(node->PACKS) ? itos(kk+1).c_str() : "--",
          node->PACKS,"",
          node->CLASH_WORST,
          std::string(itos(node->generic_int) + "/" + itos(node->generic_int2)).c_str(),
          node->SPECIAL_POSITION,
          q,REFLECTIONS->MAP ? node->MAPLLG : node->LLG,
          node->ZSCORE,
          node->SG->sstr().c_str(),
          node->generic_str.c_str()
        ));
      dvect3& eulermt = node->POSE.back().EULER;
      logTab(out::VERBOSE,snprintftos(
          "%*s %*s R=[% 6.4f % 6.4f % 6.4f] T=[ % 7.4f % 7.4f % 7.4f]",
          w,"",ww+10,"",eulermt[0],eulermt[1],eulermt[2],
          node->POSE.back().FRACT[0],node->POSE.back().FRACT[1],node->POSE.back().FRACT[2]));

      if (pak_flag.packs_YZ(node->PACKS)) kk++;
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logBlank(where);
    logTableEnd(where);
    }}


    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by packing cutoff");
    int before = DAGDB.size();
    auto before_list = DAGDB.NODES;
    DAGDB.NODES.erase_invalid();
    logEllipsisShut(where);
    logTab(where,"Percent cutoff:      "  + dtos(io_worst_clash,5,1) + "%");
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(DAGDB.size()));
    input.value__boolean.at(".phasertng.packing.reverted.").set_value(false);
    if (DAGDB.NODES.size() == 0)
    {
      logWarning("No poses passed packing test");
      if (input.value__boolean.at(".phasertng.packing.revert_on_failure.").value_or_default())
      {
        logTab(where,"Dag reverted to remove last tNCS group placement");
        DAGDB.NODES = before_list;
        DAGDB.NODES.apply_clear_last_tncs_group_pose("RF=>PAK=NONE");
        DAGDB.reset_entry_pointers(DataBase());
        input.value__boolean.at(".phasertng.packing.reverted.").set_value(true);
        return;
      }
     // else { DAGDB.revert_to_base(before_list[0]); }
    }
    }}

    int io_maxstored = input.value__int_number.at(".phasertng.packing.maximum_stored.").value_or_default();

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge by maximum stored");
      DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored: " + itos(io_maxstored));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(DAGDB.NODES.size()));
    }
    }}

    for (std::string selection : sv_string({pak_flag.title(pak_flag.Y),pak_flag.title(pak_flag.Z)}))
    {{ //packs, rescued
    int w = itow(io_nprint);
    int ww = itow(DAGDB.size());
    out::stream where = out::SUMMARY;
    logTableTop(where,"Packing Summary: " + selection);
    int nposes = DAGDB.NODES.number_of_poses();
    if (nposes) //DAGDB.size == 0
    {
      logTab(where,"Background Poses: " + itos(nposes-nmol));
      logTab(where,"Poses: " + itos(nposes));
    }
    logTab(where,"* Special Position");
    logTab(where,"--------");
    logTab(where,snprintftos(
        "%*s %*s %5s %7s %11s %3s %9s %7s %-13s  %s",
        w,"#",ww,"##","Packs","Worst%","Fraction","SYM","LLG","TFZ","Space Group","Ten-Worst-Clashes(%)"));
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP); //REFLECTIONS is nullptr
    int i(1);
    int kk = 0;
    int kkk = 0;
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      bool condition(false); //but will pick out one of the loop conditions
      if (selection == pak_flag.title(pak_flag.Y))
        condition = pak_flag.packs_Y(node->PACKS);
      else if (selection == pak_flag.title(pak_flag.Z))
        condition = pak_flag.packs_Z(node->PACKS);
      if (condition)
        logTab(where,snprintftos(
          "%*d %*s %c%4s %6.2f%% %11s  %c  % 9.*f %7.2f %-13s  %s",
          w,i,
          ww,pak_flag.packs_YZ(node->PACKS)? itos(kk+1).c_str() : "--",
          node->PACKS,"",
          node->CLASH_WORST,
          std::string(itos(node->generic_int) + "/" + itos(node->generic_int2)).c_str(),
          node->SPECIAL_POSITION,
          q,REFLECTIONS->MAP ? node->MAPLLG : node->LLG,
          node->ZSCORE,
          node->SG->sstr().c_str(),
          node->generic_str.c_str()
        ));
      dvect3& eulermt = node->POSE.back().EULER;
      if (condition)
        logTab(out::VERBOSE,snprintftos(
          "%*s %*s R=[% 6.4f % 6.4f % 6.4f] T=[ % 7.4f % 7.4f % 7.4f]",
          w,"",ww+10,"",eulermt[0],eulermt[1],eulermt[2],
          node->POSE.back().FRACT[0],node->POSE.back().FRACT[1],node->POSE.back().FRACT[2]));

      if (pak_flag.packs_YZ(node->PACKS)) kk++; //counts the output
      if (condition) kkk++;
      if (kkk == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      if (pak_flag.packs_YZ(node->PACKS)) i++;
    }
    if (kkk == 0) logTab(where,"No poses");
    logBlank(where);
    logTableEnd(where);
    }}

    for (int n = 0; n < DAGDB.size(); n++)
      DAGDB.NODES[n].generic_str = "";
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}  //end namespace phasertng
