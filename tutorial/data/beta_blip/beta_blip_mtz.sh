#! /bin/bash
mtz="beta_blip_P3221.mtz"
cns="beta_blip_P3221.cns"
if [ -f $mtz ]; then
  echo $mtz " exists"
fi
if ! [ -f $mtz ]; then
  phenix.reflection_file_converter $cns --mtz=$mtz
fi
if ! [ -f $mtz ]; then
  convert2mtz -hklin $cns -mtzout $mtz -cell 75.11,75.11,133.31,90,90,120 -spacegroup P3221
fi
if ! [ -f $mtz ]; then
  echo $mtz " file does not exist"
fi
