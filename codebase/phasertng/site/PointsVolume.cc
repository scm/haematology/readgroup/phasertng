//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Assert.h>
#include <phasertng/site/PointsVolume.h>
#include <scitbx/math/euler_angles.h>
#include <iso646.h>

namespace phasertng {

  PointsVolume::PointsVolume(double SAMP_,dvect3 BOX_) :
      SiteList(),
      BOX(BOX_),SAMP(SAMP_)
  {
    restart();
    Size = 0;
    while (!at_end()) { Size++; }
    restart(); //reset
  }

  void
  PointsVolume::restart(unsigned seed_)
  {
    //count the number of points
    //stored
    orthMin = dvect3(0,0,0);
    orthMax = BOX;
    orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
    //leave in state ready for start
    orthSite = orthMin;
    inext = 0;
  }

  bool
  PointsVolume::at_end()
  {
    //calculations done in terms of orthogonal sampling, output as fractional
    if (inext == 0)
    {
      inext++;
      Site = BOX;
      return false;
    }

    do
    {
      orthSite[0] += orthSamp[0];
      if (orthSite[0] < orthMax[0])
      {
      }
      else
      {
        orthSite[1] += orthSamp[1];
        if (orthSite[1] < orthMax[1])
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
        }
        else
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          orthSite[2] += orthSamp[2];
          if (orthSite[2] < orthMax[2])
          {
            off1 = 1.0 - off1;
            off0 = 3.0/4.0;
            orthSite[1] = orthMin[1] + off1 * orthSamp[1];
            off0 = 1.0 - off0;
            orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          }
          else { break; } //out of region
        }
        off0 = 1.0 - off0;
      }
    }
    while (false);

    bool accepted_point(true);
    if (accepted_point)
    {
      Site = orthSite;
      inext++;
      return false;
      //leaving orthSite where it was, ready for next do loop iteration
    }
    restart();
    return true;
  }

  af_dvect3
  PointsVolume::all_sites()
  {
    af_dvect3 traList;
    traList.push_back(BOX);
    orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
    off1 = 5.0/6.0;
    for (orthSite[2] = orthMin[2];
         orthSite[2] < orthMax[2];
         orthSite[2] += orthSamp[2])
    {
      off1 = 1.0 - off1;
      off0 = 3.0/4.0;
      for (orthSite[1] = orthMin[1] + off1 * orthSamp[1];
           orthSite[1] < orthMax[1];
           orthSite[1] += orthSamp[1])
      {
        off0 = 1.0 - off0;
        for (orthSite[0] = orthMin[0] + off0 * orthSamp[0];
             orthSite[0] < orthMax[0];
             orthSite[0] += orthSamp[0])
        {
          if (true)
          {
            traList.push_back(orthSite);
          }
        }
      }
    }
    return traList;
  }

}// phasertng
