//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/lib/maths.h>
//#include <phaser/lib/aniso.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/Integration.h>
#include <cctbx/adptbx.h>
#include <cctbx/eltbx/sasaki.h>
#include <cctbx/eltbx/fp_fdp.h>

// Protocol related code

namespace phaser {

void RefineSAD::setProtocol(protocolPtr p)
{
  //make sure the initial atomic parameters are within the ranges allowed by
  //the upper and lower limits, or else all hell breaks loose
  //AJM now done in ssr

//count the parameters
  npars_all = 0;
//-- sigmaa --
  npars_all += 4*bin.numbins(); //SA SB SP SD
  nsigmaa_all = npars_all; //marker for #sigmaA parameters
//-- scales --
  if (input_atoms) npars_all += 2; //ScaleK ScaleU
  if (input_partial) npars_all += 2; //PartK PartU
  nscales_all = npars_all; //marker for #scale parameters
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
    npars_all += 3; //XYZ,O,B
  npars_all += AtomFdp.size(); //Fdp

//count the refined parameters
//-- sigmaa --
  refinePar = getRefineMask(p);
  int m(0);
  npars_ref = 0;
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) npars_ref++; //SA
    if (refinePar[m++]) npars_ref++; //SB
    if (refinePar[m++]) npars_ref++; //SP
    if (refinePar[m++]) npars_ref++; //SD
  }
  nsigmaa_ref = npars_ref; //marker for #sigmaA parameters
//-- scales --
  if (input_atoms)
  {
    if (refinePar[m++]) npars_ref++; //ScaleK
    if (refinePar[m++]) npars_ref++; //ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) npars_ref++; //PartK
    if (refinePar[m++]) npars_ref++; //PartU
  }
  nscales_ref = npars_ref; //marker for #scale parameters
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    if (refinePar[m++]) npars_ref+= atoms[a].XTRA.n_xyz; //XYZ
    if (refinePar[m++]) npars_ref++; //O
    if (refinePar[m++])
    {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) npars_ref++; //B (i.e. u_iso)
      else npars_ref+=atoms[a].XTRA.n_adp; //AnisoB (i.e. u_star)
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++]) npars_ref++; //Fdp

  dL_by_dPar.clear();
  d2L_by_dPar2.clear();
  dL_by_dPar.resize(npars_ref);
  d2L_by_dPar2.resize(npars_ref);
}

std::vector<double> RefineSAD::getRefinePars()
{
  std::vector<double> pars(npars_ref);
  int i(0),m(0);
//-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) pars[i++] = DphiA_bin[s];
    if (refinePar[m++]) pars[i++] = DphiB_bin[s];
    if (refinePar[m++]) pars[i++] = SP_bin[s];
    if (refinePar[m++]) pars[i++] = SDsqr_bin[s];
  }
//-- scale --
  if (input_atoms)
  {
    if (refinePar[m++]) pars[i++] = ScaleK;
    if (refinePar[m++]) pars[i++] = ScaleU;
  }
  if (input_partial)
  {
    if (refinePar[m++]) pars[i++] = PartK;
    if (refinePar[m++]) pars[i++] = PartU;
  }
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    if (refinePar[m++]) {
      scitbx::af::small<floatType,3> indep_params = site_sym_table.get(a).site_constraints().independent_params(atoms[a].SCAT.site);
      for (int x = 0; x < atoms[a].XTRA.n_xyz; x++) pars[i++] = indep_params[x];
    }
    if (refinePar[m++]) pars[i++] = atoms[a].SCAT.occupancy;
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) { pars[i++] = atoms[a].SCAT.u_iso; //U
      } else //u_star
      {
        scitbx::af::small<floatType,6> u_indep = site_sym_table.get(a).adp_constraints().independent_params(atoms[a].SCAT.u_star);
        for (int n = 0; n < atoms[a].XTRA.n_adp; n++) pars[i++] = u_indep[n];
      }
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++]) pars[i++] = AtomFdp[t]; //fp

  assert(m == npars_all);
  assert(i == npars_ref);
  return pars;
}

bool1D RefineSAD::getRefineMask(protocolPtr p)
{
  FIX_ATOMIC = !input_atoms || (p->FIX_XYZ && p->FIX_OCC && p->FIX_BFAC && p->FIX_FDP);
  FIX_SIGMAA = (p->FIX_SD && p->FIX_SP && p->FIX_SA && p->FIX_SB);
  FIX_SCALES = (p->FIX_K && p->FIX_B && p->FIX_PARTK && p->FIX_PARTU);

  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
//--sigmaa--
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    p->FIX_SA ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    p->FIX_SB ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    p->FIX_SP ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    p->FIX_SD ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
//-- scale --
  if (input_atoms)
  {
    p->FIX_K ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    p->FIX_B ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  if (input_partial)
  {
    p->FIX_PARTK ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    p->FIX_PARTU ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    if (atoms[a].SCAT.label == "px")
    {
      refineMask.push_back(REFINE_OFF); //xyz
      refineMask.push_back(REFINE_ON);  //occ
      refineMask.push_back(REFINE_OFF);  //Bfac
    }
    else
    {
      (p->FIX_XYZ || atoms[a].XTRA.fix_site) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
      (p->FIX_OCC || atoms[a].XTRA.fix_occ) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
      (p->FIX_BFAC || atoms[a].XTRA.fix_bfac) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    (p->FIX_FDP || input_fix_fdp[t]) ?
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);

  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

std::string RefineSAD::whatAmI(int& ipar)
{
  int i(0),m(0);
//-- sigmaa  --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": SA Bin #"+ std::to_string(s+1);
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": SB Bin #"+ std::to_string(s+1);
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": SP Bin #"+ std::to_string(s+1);
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": SD Bin #"+ std::to_string(s+1);
  }
  assert(m == nsigmaa_all);
  assert(i == nsigmaa_ref);
//-- scale  --
  if (input_atoms)
  {
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": Overall Occupancy Scale";
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": Overall B-factor";
  }
  if (input_partial)
  {
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": Partial Structure Scale";
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": Partial Structure B-factor";
  }
  assert(m == nscales_all);
  assert(i == nscales_ref);
//-- atomic  --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    if (refinePar[m++]) {
      int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
      if (M == 1)
      {
        if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " X";
        if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " Y";
        if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " Z";
      }
      else
      for (int x = 0; x < atoms[a].XTRA.n_xyz; x++)
        if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " independent direction #" + std::to_string(x+1);
    }
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " O";
    if (refinePar[m++]) {
    if (!atoms[a].SCAT.flags.use_u_aniso_only())
    {
      if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " IsoB";
    } else
    {
      for (int n = 0; n < atoms[a].XTRA.n_adp; n++)
      if (i++ == ipar) return std::to_string(ipar+1) + ": Atom #" +std::to_string(a+1) + " AnisoB" +std::to_string(n+1);
    }
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++]) if (i++ == ipar) return std::to_string(ipar+1) + ": AtomType " + t2atomtype[t] + " F\"" ;

  assert(m == npars_all);
  assert(i == npars_ref);
  return "refined parameter number " + std::to_string(ipar+1) + " not identified";
}

af_string RefineSAD::rejectOutliers(bool verbose)
{
  calcIntegrationPoints();
  calcOutliers();
  return logOutliers(verbose);
}

void RefineSAD::calcOutliers()
{
  OUTLIER.SAD.clear();
  bool1D outlier(NREFL,false);
  for (int o = 0; o < OUTLIER.ANO.size(); o++)
    outlier[OUTLIER.ANO[o].refl] = true;
  if (OUTLIER.REJECT)
  {
    floatType sqrtTWO(sqrt(2.0));
    floatType maxFloatType(std::numeric_limits<floatType>::max());
    floatType minFloatType(std::numeric_limits<floatType>::min());
    floatType maxExpArg(std::min(std::log(maxFloatType),-std::log(minFloatType)));

    for (unsigned r = 0; r < NREFL; r++)
    if (!outlier[r] && !bad_data[r])
    {
      probTypeSAD outlier(r);
      unsigned s = rbin[r];
      floatType epsnSigmaN = epsn[r]*SN_bin[s];
      floatType expArg(0);
      if (cent[r])
      {
        outlier.probPos = scitbx::math::erfc((POS.F[r]/sqrt(epsnSigmaN))/sqrtTWO);
      }
      else if (both[r])
      {
        expArg = fn::pow2(POS.F[r])/epsnSigmaN;
        outlier.probPos = (expArg < maxExpArg) ? std::exp(-expArg) : 0.;
        expArg = fn::pow2(NEG.F[r])/epsnSigmaN;
        outlier.probNeg = (expArg < maxExpArg) ? std::exp(-expArg) : 0.;
        bool negative(NEG.F[r]>POS.F[r]);
        floatType FOsmall = negative ? POS.F[r] : NEG.F[r];
        floatType FOlarge = negative ? NEG.F[r] : POS.F[r];
        floatType deltaH(std::abs(FHpos[r]-FHneg[r]));
        double VarFOpos = fn::pow2(POS.SIGF[r]);
        double VarFOneg = fn::pow2(NEG.SIGF[r]);
        floatType sigmadH(std::sqrt(sigPlus[r]+VarFOneg+VarFOpos));
        floatType varSmall = negative ? VarFOpos : VarFOneg;
        varSmall = sigDsqr[r] + varSmall;
        if (deltaH < FOlarge - FOsmall - 2.2*sigmadH) // smaller circle completely inside larger with significant gap
        {
          // Compute probability that larger observation is that large or larger
          floatType Fmax(std::max(FOsmall+deltaH+5*sigmadH,FOlarge+3*sigmadH));
          int ndiv(24);
          floatType Fstep((Fmax-FOlarge)/ndiv);
          outlier.probCon = 0.;
          for (int idiv = 0; idiv <= ndiv; idiv++)
          {
            floatType F = FOlarge + idiv*Fstep;
            floatType Fprob = acentricReflProbSAD2(r,negative,F)*Fstep;
            if (idiv == 0 || idiv == ndiv) Fprob /= 3.; // Simpson's rule: endpoints
            else if (idiv % 2) Fprob *= 4./3.; // odd
            else               Fprob *= 2./3.; // even
            outlier.probCon += Fprob;
          }
        }
        else if (deltaH > FOsmall + FOlarge + 2.2*sigmadH) // circles completely outside each other -- unlikely
        {
          // Compute probability that larger observation is that small or smaller
          // F+ and F- must be greater than zero, so use sigmadH/1000 as semi-arbitrary minimum value.
          floatType Fmin(std::max(std::min(deltaH-FOsmall-5*sigmadH,FOlarge-3*sigmadH),sigmadH/1000.));
          int ndiv(24);
          floatType Fstep((FOlarge-Fmin)/ndiv);
          outlier.probCon = 0.;
          for (int idiv = 0; idiv <= ndiv; idiv++)
          {
            floatType F = Fmin + idiv*Fstep;
            floatType Fprob = acentricReflProbSAD2(r,negative,F)*Fstep;
            if (idiv == 0 || idiv == ndiv) Fprob /= 3.; // Simpson's rule: endpoints
            else if (idiv % 2) Fprob *= 4./3.; // odd
            else               Fprob *= 2./3.; // even
            outlier.probCon += Fprob;
          }
        }
        else
        {
          outlier.probCon = 1.; // arbitrary value when circles (at least nearly) cross
        }
      } //both
      else if (plus[r])
      {
        expArg = fn::pow2(POS.F[r])/epsnSigmaN;
        outlier.probPos = (expArg < maxExpArg) ? std::exp(-expArg) : 0.;
      }
      else
      {
        expArg = fn::pow2(NEG.F[r])/epsnSigmaN;
        outlier.probNeg = (expArg < maxExpArg) ? std::exp(-expArg) : 0.;
      }
      if (outlier.probLow() <= OUTLIER.PROB)
      {
        OUTLIER.SAD.push_back(outlier);
      }
    }//reflections
    for (int o = 0; o < OUTLIER.SAD.size(); o++)
      outlier[OUTLIER.SAD[o].refl] = true;
    for (int r = 0; r < NREFL; r++)
    {
      //check partial map extends to resolution limit
      bool phase_info_present((!input_partial) || (input_partial && FPpos[r] != cmplxType(0)));
      selected[r] = !bad_data[r] && working[r] && !outlier[r] && phase_info_present;
    }
  }
  wilson_llg = 0;
  if (wilson_llg_refl.size() == NREFL) //if has been initialized
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
      wilson_llg += wilson_llg_refl[r];
  }
}

} //phaser
