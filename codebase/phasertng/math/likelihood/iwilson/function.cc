//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/iwilson/function.h>
#include <scitbx/array_family/misc_functions.h>
#include <cmath>
#include <iso646.h>

using namespace scitbx;

namespace phasertng { namespace likelihood { namespace iwilson {

  void
  function::target_gradient_hessian(
      const double& EOBS,
      const double& teps,
      const double& V_,
      const bool&   CENT,
      const likelihood::iwilson::tgh_flags& flags
    )
  {
    LL = 0;
    dLL_by_dV = 0;
    d2LL_by_dV2 = 0;
    V = V_;

    if (V <= 0) {
      return; // V will be negative and function::negative_variance() will be false
    }

    double EOBS2 = fn::pow2(EOBS);
    //target
    //double wll = (std::log(teps)+EOBS2/teps);
    double wll = (EOBS2/teps); //one log function below
           wll = CENT ? wll/2.0 : wll;
    LL = -(std::log(V/teps)+(EOBS2)/V);
    if (CENT) { LL /= 2.0; }
    LL += wll;
    //gradient
    if (flags.gradient())
    {
     //derivatives
      double V2 = fn::pow2(V);
      if (flags.variances() and V2 > 0)
      {
        dLL_by_dV = CENT ?
                (EOBS2 - V)/(2*V2) :
                (EOBS2 - V)/V2;
      }
      if (flags.hessian())
      {
        double V3 = fn::pow3(V);
        double V4 = fn::pow4(V);
        if (flags.variances() and V3 > 0 and V4 > 0)
        {
          d2LL_by_dV2 = CENT ? //yes these can be simplified but here in same form as llgi
              (V*(- 2*EOBS2 + V))/(2*V4) :
              (- 2*(EOBS2)/V3 + 1/V2);
        }
      }
    }
  }

}}} //phasertng
