#include <phasertng/math/table/ebesseli1.h>
#include <phasertng/main/Assert.h>
#include <cmath>

namespace phasertng {
namespace table {

constexpr const double table::ebesseli1::eBesselI1Table1[4097][2];
constexpr const double table::ebesseli1::eBesselI1Table2[4097][2];
constexpr const double table::ebesseli1::eBesselI1InvTable[4097][2];
  ebesseli1::ebesseli1()
  {
    argfac1 = double(tbllen/argcut1);
    argfac2 = double(tbllen/argcut2);
    invArgFac=(tbllen*argcut2);
  }

  double
  ebesseli1::get(double arg) const
  {
    phaser_assert(arg >= 0);
    if (arg < argcut1)
    {
      double fracTableArg = argfac1*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return eBesselI1Table1[index][0] + (fracTableArg-index)*eBesselI1Table1[index][1];
    }
    else if (arg < argcut2)
    {
      double fracTableArg = argfac2*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return eBesselI1Table2[index][0] + (fracTableArg-index)*eBesselI1Table2[index][1];
    }
    else
    {
      double ONE(1);
      double invArg(ONE/arg);
      double fracTableArg = invArgFac*invArg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return std::sqrt(invArg)*(eBesselI1InvTable[index][0] + (fracTableArg-index)*eBesselI1InvTable[index][1]);
    }
    return 0;
  }

  ebesseli1 ebesseli1::inst_;
}}
