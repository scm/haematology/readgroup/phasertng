#include <phasertng/data/Reflections.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/jiffy.h>

namespace phasertng {

  af_string
  Reflections::logLabin( std::string description) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Reflection Column Labels: " + description + "<<");
    output.push_back(snprintftos( "Labin : %8s   %-10s", "--data--", "--label--"));
    for (auto item : this->LABIN)
      output.push_back(snprintftos(
          "Labin : %8s = %-10s",
          std::string(labin::col2String(item)).c_str(),
          this->MTZCOLNAME.at(item).c_str()));
    output.push_back("Space Group: " + this->SG.CCP4);
    output.push_back("Unit Cell:   " + this->UC.str());
    output.push_back("Resolution : " + dtos(this->data_hires()));
    return output;
  }

  af_string
  Reflections::logWilson(std::string description) const
  {
    af_string output;
    output.push_back(">>Wilson log(likelihood) " + description + "<<");
    if (this->has_col(labin::WLL))
    {
      double totalLL(0);
      for (int r = 0; r < this->NREFL; r++)
        totalLL += this->get_flt(labin::WLL,r);
      output.push_back("log(likelihood) = " + dtos(totalLL,2));
    }
    else output.push_back("Not available");
    return output;
  }

  af_string
  Reflections::logReflections( std::string description ) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Reflections: " + description + "<<");
    sv_int rcount(2,0);
    for (int r = 0; r < this->NREFL; r++)
    {
      bool cent = this->work_cent(r);
      cent ? rcount[0]++ : rcount[1]++;
    }
    output.push_back("Number of reflections:          " + itos(this->NREFL));
    output.push_back("Number of centric reflections:  " + itos(rcount[0]));
    output.push_back("Number of acentric reflections: " + itos(rcount[1]));
    output.push_back("Data is anomalous:  " + b3tos(ANOMALOUS));
    output.push_back("Translational sym:  " + itos(this->TNCS_ORDER));
    output.push_back("Header: ");
    auto header = get_history();
    for (auto h : header)
      output.push_back(h);
    std::set<labin::col> printcol;
    printcol.insert(labin::H);
    printcol.insert(labin::K);
    printcol.insert(labin::L);
    std::set<labin::col> boring = { labin::CENT, labin::SSQR, labin::BOTH, labin::PLUS, labin::PHSR, labin::EPS, labin::BIN, labin::TBIN, labin::WLL, labin::FREE };
    for (auto item : this->LABIN)
      if (!boring.count(item))
        printcol.insert(item);
    std::map<labin::col, pod::pdprint> format;
    for (auto item : printcol)
    {
      bool use_generic(true);
      format[item] = this->formatting(item,use_generic);
    }
    for (std::string refl : {"all","centric","acentric"} )
    {
      output.push_back("Range: " + refl);
      {{
      std::string line("     ");
      for (auto item : printcol)
      {
        std::string label = labin::col2String(item); //this->MTZCOLNAME.at(item)
        hoist::replace_all(label,"NAT","");
        hoist::replace_all(label,"POS","(+)");
        hoist::replace_all(label,"NEG","(-)");
        line += snprintftos("%*s ", format[item].pd, label.c_str());
      }
      output.push_back(line);
      }}
      {{
      for (bool maximum : { false,true } )
      {
        std::string line;
        line += snprintftos("%s" ,maximum ? "max: " : "min: ");
        for (auto item : printcol)
        {
          if (format[item].empty[refl])
          {
            line += snprintftos("%*s ", format[item].pd, "---");
          }
          else if (format[item].boolean) //onerange (cent)
          {
            line += snprintftos("%*s ",
                    format[item].pd,
                    maximum ? ((format[item].mm[refl].second == 1) ? "1" : "0") :
                               ((format[item].mm[refl].first  == 1) ? "1" : "0")
                               );
          }
          else
          {
            line += snprintftos("%*.*f ",
                    format[item].pd, format[item].d,
                    maximum ? format[item].mm[refl].second : format[item].mm[refl].first
                    );
          }
        }
        output.push_back(line);
      }
      }}
      {{ //memory
        std::string line;
        line += snprintftos("%s" ,"num: ");
        for (auto item : printcol)
        {
          if (this->MTZCOLTYPE.at(item) == 'H')
          {
            line += snprintftos("%*s ", format[item].pd, "---");
          }
          else
          {
            line += snprintftos("%*d ", format[item].pd, format[item].npres[refl]);
          }
        }
        output.push_back(line);
      }} //memory
    }
    return output;
  }

  af_string
  Reflections::logMtzDump(
      std::string description,
      int DUMP_R,
      millnx DUMP_MILLER
    ) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>MtzDump Reflections: " + description + "<<");
    auto header = get_history();
    for (auto h : header) output.push_back(h);
    sv_int rcount(2,0);
    std::set<labin::col> printcol;
    for (auto item : this->LABIN)
      printcol.insert(item);
    std::map<labin::col, pod::pdprint> format;
    for (auto item : printcol)
      format[item] = this->formatting(item);
    if (this->NREFL == 0)
    {
      output.push_back("No Reflections");
      return output;
    }
    // here is the selection for the mtz dump
    std::set<int> pick_reflections;
    int nprint = 20;
    if (DUMP_R >= 0 && DUMP_R < this->NREFL)
    {
      output.push_back("Dump Reflection");
      pick_reflections.insert(DUMP_R);
    }
    if (DUMP_MILLER != millnx(0,0,0))
    {
      output.push_back("Dump Miller");
      for (int r = 0; r < this->NREFL; r++)
      {
        const millnx& miller = this->get_miller(r);
        if (miller == DUMP_MILLER)
          pick_reflections.insert(r);
      }
    }
    if (!pick_reflections.size())
    { //fill with half centric/half acentric if possible
      output.push_back("Dump First Centric and Acentric Reflection(s)");
      //look for centrics first, as there may not be any centrics at all
      for (int r = 0; r < this->NREFL and pick_reflections.size() < nprint/2; r++)
      {
        bool centric = this->work_cent(r);;
        if (centric)
          pick_reflections.insert(r);
      }
      //then acentrics
      for (int r = 0; r < this->NREFL and pick_reflections.size() < nprint; r++)
      {
        bool centric = this->work_cent(r);
        if (!centric)
          pick_reflections.insert(r);
      }
    }
    //header
    {{
    std::string line;
    line += snprintftos("#%6c ",' ');
    line += snprintftos("%5s ","H");
    line += snprintftos("%5s ","K");
    line += snprintftos("%5s ","L");
    for (auto item : printcol)
      line += snprintftos("%*s%c ", format[item].pd, this->MTZCOLNAME.at(item).c_str(),' ');
    output.push_back(line);
    }}
    for (auto r : pick_reflections)
    {
      const millnx& miller = this->get_miller(r);
      std::string line;
      line += snprintftos("#%-6d ",r);
      line += snprintftos("%5d ",miller[0]);
      line += snprintftos("%5d ",miller[1]);
      line += snprintftos("%5d ",miller[2]);
      for (auto item : printcol)
      {
        friedel::type f = convert_labin_to_friedel(item);
        if (format[item].boolean) //onerange (cent)
        {
          line += this->get_present(f,r) ?
              snprintftos("%*d  ", format[item].pd, this->get_bln(item,r)):
              snprintftos("%*s  ", format[item].pd,"n/a");
        }
        else if (this->MTZCOLTYPE.at(item) == 'I')
        {
          line += this->get_present(f,r) ?
              snprintftos("%*d  ", format[item].pd, this->get_int(item,r)):
              snprintftos("%*s  ", format[item].pd,"n/a");
        }
        else
        {
          line += this->get_present(f,r) ?
              snprintftos("%*.*f  ",
              format[item].pd, format[item].d-1, //-1 so there is room for the minus sign
              this->get_flt(item,r) ): snprintftos("%*s  ", format[item].pd, "n/a");
        }
      }
      output.push_back(line);
    }
    return output;
  }

  std::pair<af_string,Loggraph>
  Reflections::logSignalMeasure(
      const sv_double& data,
      const sv_bool& present,
      const sv_bool& selected,
      std::string description,
      std::string short_description,
      bool is_info,
      bool is_cumulative
    ) const
  {
    af_string output;
    PHASER_ASSERT(data.size() == NREFL);
    PHASER_ASSERT(this->has_col(labin::BIN));
    int numbins = this->numbins();
    sv_double signal_bin(numbins,0.);
    sv_int    n_bin(numbins,0);
    double signal_tot(0.);
    int    n_tot(0);
    std::set<double> signalLimits = { 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.02, 0.05, 0.1, 0.2, 0.05, 1., 2., 5., 10., 20., 50. };
    std::map<double,int> signalCount;
    for (auto item : signalLimits)
      signalCount[item] = 0;
    double maxSignal(0);
    millnx maxSignalMiller;
    for (int r = 0; r < this->NREFL; r++)
    {
      if (present[r] and selected[r])
      {
        const int&     s = this->get_int(labin::BIN,r);
        const millnx&  miller = this->get_miller(r);
        n_tot++;
        n_bin[s]++;
        double thisSignal = data[r];
        if (thisSignal > maxSignal)
        {
          maxSignal = thisSignal;
          maxSignalMiller = miller;
        }
        for (auto item : signalLimits)
          if (thisSignal > item)
            signalCount[item]++;
        signal_tot += thisSignal;
        signal_bin[s] += thisSignal;
      }
    }
    output.push_back("");
    output.push_back(">>Signal in Data: " + description + "<<");
    PHASER_ASSERT(n_tot >= 0); //nan
    if (signal_tot != 0); //nan
    output.push_back("Estimate of " + short_description + " depends on:");
    output.push_back("---accuracy of experimental sigmas");
    output.push_back("---accuracy of anisotropy and tNCS parameters");
    output.push_back("---status of twinning (which reduces information)");
    output.push_back("accuracy of null hypothesis refinement");
    if (is_info)
    {
      output.push_back("Estimated total information = " +
          dtos(signal_tot) + " bits, or " + dtos(signal_tot*std::log(2.)) +
          " nats in " + itos(n_tot) + " reflections");
    }
    else if (is_cumulative)
    {
      output.push_back("Estimated total " + short_description + " = " +
          dtos(signal_tot) + " in " + itos(n_tot) + " reflections");
    }

    output.push_back("Maximum " + short_description + " = " + dtos(maxSignal) + " for hkl " + ivtos(maxSignalMiller));
    if (n_tot)
    {
      output.push_back("---average of " + dtos(signal_tot/n_tot) + " per reflection");
      output.push_back("Number of reflections exceeding threshold:");
      output.push_back("  threshold     number");
      for (auto item : signalLimits)
        output.push_back(dtos(item,11,6) + " " + itos(signalCount[item],10));
      output.push_back("     total " + itos(NREFL));
    }

    Loggraph loggraph;
    loggraph.title = description;
    loggraph.scatter = false;
    loggraph.graph.resize(1);
    loggraph.data_labels = is_cumulative ? "1/d^2 Mean Cumulative" :
                                           "1/d^2 Mean";
    sv_double hires_bin(numbins,std::numeric_limits<double>::max());
    sv_double lores_bin(numbins,0);
    double cumulative_signal(0);
    for (int r = 0; r < this->NREFL; r++)
    {
      const int&     s = this->get_int(labin::BIN,r);
      double reso = 1.0/std::sqrt(this->get_flt(labin::SSQR,r));
      if (reso < hires_bin[s])
        hires_bin[s] = reso;
      if (reso > lores_bin[s])
        lores_bin[s] = reso;
    }
    for (unsigned s = 0; s < numbins; s++)
    {
      if (n_bin[s])
      {
        double loS = 1/lores_bin[s]; //from Bin calculation, so that it is the same
        double hiS = 1/hires_bin[s];
        double midres = 1/(std::sqrt((loS*loS+hiS*hiS)/2));
        cumulative_signal += signal_bin[s];
        loggraph.data_text += dtos(1.0/fn::pow2(midres),5,4) + " " +
                              dtos(signal_bin[s]/n_bin[s]) + " ";
        if (is_cumulative)
          loggraph.data_text += dtos(cumulative_signal) + " ";
        loggraph.data_text += "\n";
      }
    }
    loggraph.graph.resize(2);
    loggraph.graph[0] = short_description + " per reflection vs resolution:AUTO:1,2";
    if (is_cumulative)
      loggraph.graph[1] = "Cumulative " + short_description + " vs resolution:AUTO:1,3";
    return { output, loggraph };
  }

  std::pair<sv_double,sv_bool>
  Reflections::getIonSigI() const
  {
    sv_double IonSIGI(NREFL,0);
    sv_bool present(NREFL,true);
    for (int r = 0; r < this->NREFL; r++)
      if (this->get_flt(labin::SIGINAT,r) > 0)
        IonSIGI[r] = this->get_flt(labin::INAT,r)/this->get_flt(labin::SIGINAT,r);
      else present[r] = false;
    return { IonSIGI,present };
  }

  af_string
  Reflections::logMtzHeader( std::string description ) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Mtz Header:" + description + "<<");
    output.push_back("HIRES/LORES = " + dtos(this->RESOLUTION[1]) + "/" + dtos(this->RESOLUTION[0]));
    output.push_back("NREFL = " + itos(this->NREFL));
    output.push_back("SG = " + this->SG.CCP4 + " [ " + this->SG.HALL + " ]");
    output.push_back("UC = " + this->UC.str());
    output.push_back("WAVELENGTH = " + dtos(this->WAVELENGTH));
    output.push_back("OVERSAMPLING = " + dtos(this->OVERSAMPLING));
    output.push_back("MAPORI = " + dvtoss(this->MAPORI,3)); //origin relative to original map
    for (auto ID : this->REFLPREP)
      output.push_back("REFLPREP = " + reflprep::flag2String(ID)); //reflection preparation
    output.push_back("REFLID = " + REFLID.str()); //reflection idenitifier
    output.push_back("MAP = " + b3tos(this->MAP));
    output.push_back("TNCS PRESENT = " + b3tos(this->TNCS_PRESENT));
    output.push_back("TWINNED = " + b3tos(this->TWINNED));
    output.push_back("ANOMALOUS = " + b3tos(this->ANOMALOUS));
    output.push_back("SHARPENING = " + dtos(this->SHARPENING));
    output.push_back("WILSON_K_F = " + dtos(this->WILSON_K_F));
    output.push_back("WILSON_B_F = " + dtos(this->WILSON_B_F));
    output.push_back("TNCS_ORDER = " + itos(this->TNCS_ORDER));
    output.push_back("TNCS_ANGLE = " + dvtoss(this->TNCS_ANGLE,3));
    output.push_back("TNCS_VECTOR = " + dvtoss(this->TNCS_VECTOR,3));
    if (ANISO_PRESENT)
    output.push_back("ANISO_DIRECTION_COSINES = " + dmtos(this->ANISO_DIRECTION_COSINES,6,4));
    output.push_back("TNCS_RMS = " + dtos(this->TNCS_RMS));
    output.push_back("TNCS_FS = " + dtos(this->TNCS_FS));
    output.push_back("TNCS_GFNRAD = " + dtos(this->TNCS_GFNRAD));
    output.push_back("TOTAL_SCATTERING = " + itos(this->TOTAL_SCATTERING));
    output.push_back("Z = " + dtos(this->Z));
    return output;
  }

  std::pair<af_string,af_string>
  Reflections::logBadData( std::string description ) const
  { //memory
    af_string warning;
    // THE FORWARDSLASH CANNOT BE USED IN MTZ LABELS - NO (+/-) allOWED!!
    // (note ISYM/M is a special case)
    std::map<friedel::type,std::string> npnstr =
        {{friedel::NAT,""}, {friedel::POS,"(+)"} ,{friedel::NEG,"(-)"}};
    af_string output;
    output.push_back("");
    output.push_back(">>Bad Data: " + description + "<<");
    output.push_back("Number of reflections = " + itos(NREFL));
    output.push_back("Data as Intensities = " + b3tos(this->INTENSITIES));
    output.push_back("Data as French-Wilson = " + b3tos(this->FRENCH_WILSON));
    output.push_back("Number of reflections recorded as zero, absent(?), and/or negative");
    for (auto f : { friedel::NAT,friedel::POS,friedel::NEG} )
    {
      labin::col labinI = convert_friedel_to_labin("I",f);
      labin::col labinF = convert_friedel_to_labin("F",f);
      labin::col labinSIGI = convert_friedel_to_labin("SIGI",f);
      labin::col labinSIGF = convert_friedel_to_labin("SIGF",f);

      std::map<std::string,int> count;
      //tag starting with x if it is for info only
      count["F=0"] = 0;
      count["F=?"] = 0;
      count["F=(0&?)"] = 0;
      count["SIGF=0"] = 0;
      count["I<0"] = 0;
      count["I=0"] = 0;
      count["I=?"] = 0;
      count["SIGI=0"] = 0;
      count["xI=(0&?)"] = 0; //flag  x for OK below

      if ( this->has_col(labinF) or this->has_col(labinI))
      {
        output.push_back("---");
        //if or condition above, must have both, developer error otherwise
        PHASER_ASSERT(this->has_col(labinF));
        PHASER_ASSERT(this->has_col(labinI));
        bool allsigi0=true;
        for (int r = 0; r < this->NREFL; r++)
        { //frenchwilson
          if (this->get_flt(labinSIGI,r) != 0)
            allsigi0 = false;
        }
        for (int r = 0; r < this->NREFL; r++)
        {
          if (this->get_flt(labinF,r) == 0) count["F=0"]++;
          if (this->get_flt(labinSIGF,r) == 0) count["SIGF=0"]++;
          if (this->get_flt(labinI,r) < 0) count["I<0"]++;
          if (this->get_flt(labinI,r) == 0) count["I=0"]++;
          if (!this->get_present(f,r))
          {
             count["F=?"]++;
             if (this->get_flt(labinF,r) == 0)
               count["F=(0&?)"]++;
             count["I=?"]++;
             if (this->get_flt(labinI,r) == 0)
               count["xI=(0&?)"]++;
          }
          else if (!allsigi0)
          {
            if (this->get_flt(labinSIGI,r) == 0 and this->get_flt(labinI,r) != 0)
              count["I!=0 & SIGI=0"]++;
          }
        }
        std::string labF = this->MTZCOLNAME.at(labinF);
        std::string labI = this->MTZCOLNAME.at(labinI);
        std::string labFS = this->MTZCOLNAME.at(labinSIGF);
        std::string labIS = this->MTZCOLNAME.at(labinSIGI);
        std::string ext = friedel::type2String(f);
        size_t w = std::max(labF.size(),static_cast<size_t>(itow(NREFL)));
        w = std::max(w,labI.size());
        w = std::max(w,labFS.size());
        w = std::max(w,labIS.size());
        output.push_back(snprintftos(
           "F%3s: %*s=0 %*s=? %*s=(0&?) %*s=0&F!=0",ext.c_str(),
           w,labF.c_str(),
           w,labF.c_str(),
           w,labF.c_str(),
           w,labFS.c_str()
        ));
        output.push_back(snprintftos(
           "F%3s: %*d %*d %*d %*d",ext.c_str(),
           w+2,count["F=0"],
           w+2,count["F=?"],
           w+6,count["F=(0&?)"],
           w+7,count["SIGF=0"]
        ));
        output.push_back("---");
        output.push_back(snprintftos(
           "I%3s: %*s=0 %*s=? %*s=(0&?) %*s=0&I!=0 %*s<0 ",ext.c_str(),
           w,labI.c_str(),
           w,labI.c_str(),
           w,labI.c_str(),
           w,labI.c_str(),
           w,labIS.c_str()
        ));
        output.push_back(snprintftos(
           "I%3s: %*d %*d %*d %*d %*d",ext.c_str(),
           w+2,count["I=0"],
           w+2,count["I=?"],
           w+6,count["I=(0&?)"],
           w+7,count["SIGI=0"],
           w+2,count["I<0"]
        ));
      }
      for (auto c : count)
      {
        double perc = 100*static_cast<double>(c.second)/static_cast<double>(NREFL);
        if (!MAP and perc > 50 and c.first[0] != 'x') //flag x which is ok
          warning.push_back("Over half are bad reflections (" + dtos(perc,2) + "%) : check data " + c.first);
      }
    }
    output.push_back("---");
    output.push_back("");
    return { output, warning };
  }

std::string Reflections::b3tos(const boost::logic::tribool i) const
{
  if (boost::logic::indeterminate(i)) return "undefined";
  return i ? "true ":"false";
}
} //phasertng
