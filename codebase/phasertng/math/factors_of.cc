#include <phasertng/math/factors_of.h>
#include <math.h>
#include <cstdlib>
#include <algorithm>

namespace phasertng {

int factor(int number, int prime, int reqd, bool go_up)
{
  /*
  Returns the a number that is factorizable by max_num_factors numbers <= prime.  Number is also divisible by reqd
  If (go_up) the number is the next largest, otherwise it is the next smallest.
  */

  int inc(0);
  if (go_up)
  {
    while (number % reqd != 0)
      number++;
    inc = reqd;
  }
  else
  {
    while (number % reqd != 0)
      number--;
    inc = -reqd;
  }

  if (number == 0)
    return inc;

  int ntmp(number);
  int num_factors(0);
  int max_num_factors(100);

  while (num_factors < max_num_factors)
  {
    for (int fac = 2; fac <= prime; fac++)
    {
      while ( ntmp == (ntmp/fac)*fac )
      {
        ntmp = ntmp/fac;
      }
    }
    if (ntmp == 1) break;
    number += inc;
    ntmp = number;
    num_factors++;
  }
  return number;
}

std::vector<int> factors_of(int NUM)
{
  std::vector<int> facvec;
  for (int f = 2; f <= NUM/2; f++)
  {
    int rem = std::div(NUM,f).rem;
    if (rem == 0)
      if (std::find(facvec.begin(),facvec.end(),f) == facvec.end())
        facvec.push_back(f);
  }
  facvec.push_back(1);
  return facvec;
}

} //phaser
