//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dtmin/RefineBase.h>
#include <fstream>

//#define PHASERTNG_STUDYPARAMS_GRADIENT_ONLY

namespace phasertng {
namespace dtmin {

  sv_double
  RefineBase::get_reparameterized_parameters()
  {
    std::vector<dtmin::Reparams> repar(reparameterize());
    sv_double reparameterized_x(get_macrocycle_parameters()); // initialise with the un-reparameterized x

    if (repar.size() != 0)
      for (int i = 0; i < nmp; i++)
        if (repar[i].reparamed)
          reparameterized_x[i] = std::log(reparameterized_x[i] + repar[i].offset);

    return reparameterized_x;
  }

  void
  RefineBase::set_reparameterized_parameters(sv_double reparameterized_x)
  {
    // given get_reparameterized_parameters, find the original x and call set_macrocycle_parameters(x) set x in refineXYZ
    sv_double x(reparameterized_x);
    std::vector<dtmin::Reparams> repar(reparameterize());

    if (repar.size() != 0)
      for (int i=0; i<repar.size(); i++)
        if (repar[i].reparamed)
          x[i] = std::exp(x[i]) - repar[i].offset;

    set_macrocycle_parameters(x);
  }

  sv_double
  RefineBase::reparameterized_large_shifts()
  {
    //  derivation of the formula used:
    // 1d unreparameterized:
    //                         ls
    //                      |------>|
    //    0                 x       |
    //  --|-----------------|-------|---->
    //
    // reparameterized:
    //            rls
    //          |---->|
    //    0    rx     |
    //  --|-----|-----|------------------>
    //
    //  rx = log(x + offset)
    //
    //  rx + rls = log(x + ls + offset)
    //
    //  => rls = log(x + ls + offset) - log(x + offset)
    //
    //         = log(1 + ls/(x + offset) )
    //
    //        ~= ls/(x + offset) for small ls/(x + offset)
    //
    sv_double x(get_macrocycle_parameters());
    sv_double reparameterized_ls(macrocycle_large_shifts());
    std::vector<dtmin::Reparams> repar(reparameterize());

    if (repar.size() != 0)
      for (int i = 0; i < nmp; i++)
        if (repar[i].reparamed)
          reparameterized_ls[i] = reparameterized_ls[i]/(x[i]+repar[i].offset);

    return reparameterized_ls;
  }

  std::vector<dtmin::Bounds>
  RefineBase::reparameterized_bounds()
  {
    std::vector<dtmin::Bounds> reparameterized_bounds(bounds());
    std::vector<dtmin::Reparams> repar(reparameterize());

    if (reparameterized_bounds.size() == 0)  //no bounds case
      return reparameterized_bounds;
    else if (repar.size() == 0)  //bounds but no repar case
      return reparameterized_bounds;
    else  //bounds and repar case
    {
      for (int i = 0; i < nmp; i++)
      {
        if (repar[i].reparamed)
        {
          if (reparameterized_bounds[i].lower_bounded())
          {
            double lower = reparameterized_bounds[i].lower_limit() + repar[i].offset;
            assert(lower>0);
            reparameterized_bounds[i].lower_on( std::log(lower));
          }
          if (reparameterized_bounds[i].upper_bounded())
          {
            double upper = reparameterized_bounds[i].upper_limit() + repar[i].offset;
            assert(upper>0);
            reparameterized_bounds[i].upper_on( std::log(upper) );
          }
        }
      }
      return reparameterized_bounds;
    }
  }

  TargetGradientType
  RefineBase::reparameterized_target_gradient()
  {
    // When we are finding the gradient of f with respect to the reparameterized
    // parameters rx, we can pretend that we started from rx, calculated x
    // and then calculated f. In reality we started from x then calculated rx.
    //
    // x -> rx  (reparameterized x)
    //  \-> f
    //
    // i.e.  ' rx -> x -> f '
    //
    // rx(x) = log(x+c)  => x(rx) = e^rx - c
    //                     dx/drx = e^rx = x + c
    //
    // we want:  df(x(rx))   df   dx    df
    //           --------- = -- * --- = -- * (x+c)
    //              drx      dx   drx   dx

    sv_double x(get_macrocycle_parameters());
    TargetGradientType reparameterized_f_g(target_gradient());
    std::vector<dtmin::Reparams> repar(reparameterize());

    if (repar.size() != 0)
      for (int i = 0; i < nmp; i++)
        if (repar[i].reparamed)
          reparameterized_f_g.Gradient[i] *= (x[i]+repar[i].offset);

    return reparameterized_f_g; // dont need to do anything to f
  }

  TargetGradientHessianType
  RefineBase::reparameterized_target_gradient_hessian()
  {
    // Returns the function, the reparameterized gradient,
    // the reparameterized hessian and a flag to tell if
    // the hessian is diagonal
    //
    // Probably easiest to understand if you write out the equation,
    //   all small d's are partial differentials
    //   rx means reparameterized x
    //   c_i is the reparmaterisation offset used in rx_i(x_i) = log(x_i + c_i)
    //
    //                    d^2 f         d      df     dx_j
    //reparedHessian_ij = ----------- = ----- (---- * -----)
    //                    drx_i drx_j   drx_i  dx_j   drx_j
    //
    //  d^2 f      dx_j     df   d^2x_j
    //= ----------*-----  + ----*-----------
    //  drx_i dx_j drx_j    dx_j drx_i drx_j
    //
    //  d^2 f     dx_i  dx_j    df   d^2 x_j
    //= ---------*-----*----- + ----*-----------
    //  dx_i dx_j drx_i drx_j   dx_j drx_i drx_j
    //
    //  d^2f                                         df
    //= --------- * (x_i + c_i) * (x_j + c_j) + diag(----*(x_j + c_j) )
    //  dx_i dx_j                                    dx_j

    sv_double x(get_macrocycle_parameters());
    std::vector<dtmin::Reparams> repar(reparameterize());

    auto tgh = target_gradient_hessian(); // Note reparameterized_g and reparameterized_h are not reparameterized yet, that is what this function does
    double             f = tgh.Target;
    sv_double          reparameterized_g = tgh.Gradient; // will be populated with the unreparameterized gradient and ones requiring reparing will be modified accordingly
    versa_flex_double  reparameterized_h = tgh.Hessian; // will be populated with the unreparameterized hessian, and ones requiring reparing will be modified accordingly
    bool               is_hessian_diagonal = tgh.Diagonal;

    if (repar.size() != 0)
    {
      // reparameterize the hessian first because we need the unreparameterized gradient for its calculation
      for (int i = 0; i < nmp; i++)
        for (int j = 0; j < nmp; j++)
          if (repar[i].reparamed || repar[j].reparamed)
          {
            double dxidyi(1.),dxjdyj(1.);
            if (repar[i].reparamed) dxidyi = x[i]+repar[i].offset;
            if (repar[j].reparamed) dxjdyj = x[j]+repar[j].offset;
            reparameterized_h(i,j) *= dxidyi*dxjdyj;
            if (i==j) reparameterized_h(i,i) += reparameterized_g[i]*dxidyi; //dxidyi=d2xidyi2  // note at this point 'reparameterized_g' is not actually reparameterized
          }
      // repar the gradient as per reparameterized_target_gradient()
      for (int i = 0; i < nmp; i++)
        if (repar[i].reparamed)
          reparameterized_g[i] *= (x[i]+repar[i].offset);
    }

    TargetGradientHessianType result;
    result.Target = f;
    result.Gradient = reparameterized_g;
    result.Hessian = reparameterized_h;
    result.Diagonal = is_hessian_diagonal;
    return result;
  }

  std::pair<TargetGradientHessianType,bool>
  RefineBase::reparameterized_adjusted_target_gradient_hessian()
  {
    // Returns the function, the reparameterized gradient,
    // the reparameterized and adjusted* hessian, a flag to tell if the hessian is diagonal
    // and a flag to say whether the hessian was adjusted or not.
    //
    // *By adjusted we mean that the hessian had non-positive curvatures adjusted

    auto rtgh = reparameterized_target_gradient_hessian();
    double              f = rtgh.Target;
    sv_double           reparameterized_g = rtgh.Gradient;
    versa_flex_double   reparameterized_h = rtgh.Hessian;
    bool                is_hessian_diagonal = rtgh.Diagonal;

    // adjust the hessian
    bool queue_new_hessian;
    auto adjusted_hessian_pair(adjust_hessian(reparameterized_h));
    reparameterized_h        = adjusted_hessian_pair.first;
    queue_new_hessian = adjusted_hessian_pair.second;

    TargetGradientHessianType result;
    result.Target = f;
    result.Gradient = reparameterized_g;
    result.Hessian = reparameterized_h;
    result.Diagonal = is_hessian_diagonal;
    return {result, queue_new_hessian};
  }

  sv_double
  RefineBase::damped_shift(
      double    a,            // multiple of p we would like to shift x by
      sv_double old_x,        // the starting position
      sv_double p,            // the search vector
      sv_double dist,         // vector of the positive multiple of p required to
                              // hit a bound. If the variable is unbounded then the value is -1
      sv_double ls)           // vector of what is deemed to be a large shift for each parameter
  {
    // The overall idea is to shift x by a multiple of p, but avoid crossing boundaries
    // and dampen any shifts of parameters that would be greater than their large_shift value.

    sv_double x(nmp);
    auto bounds(reparameterized_bounds());
    for (int i = 0; i < nmp; i++)
    {
      double thisdist( (dist[i]<0.) ? a : std::min(a,dist[i]) ); // neg dist (-1.) means outside of bound (should never happen) or unbounded.
      double rawshift(thisdist*p[i]); // Undamped shift
      double relshift(std::fabs(rawshift)/ls[i]);
      double dampfac = (relshift <= 0.005) ? 1. : 2.*std::tanh(relshift/2.)/relshift;
      double shift = dampfac*rawshift;
      x[i] = old_x[i] + shift; // Shift parameter only up to its bound, damped to max of 2*largeShift
    }

     // correct for the possibility of minutely stepping over a boundary
    if (bounds.size() != 0)
    {
      for (int i = 0; i < nmp; i++)
      {
        if (bounds[i].lower_bounded() && x[i] < bounds[i].lower_limit())
          x[i] = bounds[i].lower_limit();
        if (bounds[i].upper_bounded() && x[i] > bounds[i].upper_limit())
          x[i] = bounds[i].upper_limit();
      }
    }
    return x;
  }

  void
  RefineBase::study_parameters()
  {
    // Vary refined parameters and print out function, gradient and curvature
    sv_string parameter_names(macrocycle_parameter_names());
    std::string filename("studyParams");
    sv_double x(get_reparameterized_parameters()),old_x(get_reparameterized_parameters()),g(nmp);
    sv_double unrepar_x(get_macrocycle_parameters()), unrepar_oldx(nmp);
    std::vector<dtmin::Bounds> bounds(reparameterized_bounds());
    sv_double ls(reparameterized_large_shifts());
    bool bounds_present = bounds.size() == 0 ? false : true;

    puts("\n");
    puts("Study behaviour of parameters near current values");

    double fmin(target()); // so called because it should be at the minimum

    // First check that function values from F, FG and FGH agree
    auto reparameterized_f_g = reparameterized_target_gradient();
    double gradLogLike = reparameterized_f_g.Target;
    g                  = reparameterized_f_g.Gradient;
    if (std::abs(fmin-gradLogLike) >= 0.1)
    {
      puts("Likelihoods from target() and reparameterized_f_g() disagree");
      puts(std::string("Likelihood from              target(): " + std::to_string(-fmin)).c_str());
      puts(std::string("Likelihood from reparameterized_f_g(): " + std::to_string(-gradLogLike)).c_str());
    }
    auto f_g_h = reparameterized_target_gradient_hessian();
    double  hessLogLike = f_g_h.Target;
    versa_flex_double h = f_g_h.Hessian;
    bool    is_diagonal = f_g_h.Diagonal;
    if (std::abs(fmin-hessLogLike) >= 0.1)
    {
      puts("Likelihoods from target() and reparameterized_f_g_h() disagree");
      puts(std::string("Likelihood from                target(): " + std::to_string(-fmin)).c_str());
      puts(std::string("Likelihood from reparameterized_f_g_h(): " + std::to_string(-hessLogLike)).c_str());
    }
    assert(std::abs(fmin-gradLogLike) < 0.1);
    assert(std::abs(fmin-hessLogLike) < 0.1);
    fmin = gradLogLike; // Use function value from gradient for consistency below

    std::ofstream outstream;
    outstream.open("studyWhatAmI");

    for (int i = 0; i < nmp; i++)
    {
      outstream << " \"" << parameter_names[i] << " \"\n";
    }
    outstream.close();

    const int LOGFILE_WIDTH = 90;
    int ndiv(6);
    sv_double dxgh(nmp); // Finite diff shift for grad & hess
    for (int i = 0; i < nmp; i++)
    {
      std::ofstream outstream;
      if (i+1 < 10)
        outstream.open(std::string(filename+".0"+std::to_string(i+1)).c_str());
      else
        outstream.open(std::string(filename+"."+std::to_string(i+1)).c_str());
      puts(std::string("\nRefined Parameter #:" + std::to_string(i+1) + " " + macrocycle_parameter_names()[i]).c_str());
      puts(std::string("Centered on: " + to_sci_format(old_x[i])).c_str());
      double xmin = old_x[i] - 2.*ls[i];
      dxgh[i] = ls[i]/50; // Small compared to exploration of function value
      if (bounds_present && bounds[i].lower_bounded())
      {
        xmin = std::max(xmin,bounds[i].lower_limit()+dxgh[i]); // Allow room for FD calcs
        puts(std::string("Lower limit: " + to_sci_format(bounds[i].lower_limit())).c_str());
      }
      else // "Unbounded" parameters may have limits depending on correlations
      {
        sv_double g_Fake(nmp,0.);
        g_Fake[i] = ls[i]/10.; // Put in range of plausible shift
        double max_dist(maximum_distance(old_x,g_Fake).first);
        double fmax(std::numeric_limits<double>::max());
        double ftol(std::numeric_limits<double>::epsilon());
        double specialLimit = (max_dist < fmax-ftol) ? old_x[i]-max_dist*g_Fake[i] : -fmax;
        if (specialLimit > xmin)
        {
          xmin = specialLimit+dxgh[i];
          puts(std::string("Special lower limit: "+ to_sci_format(specialLimit)).c_str());
        }
      }
      double xmax(old_x[i] + 2.*ls[i]);
      if (bounds_present && bounds[i].upper_bounded())
      {
        xmax = std::min(xmax,bounds[i].upper_limit()-dxgh[i]);
        puts(std::string("Upper limit: "+ to_sci_format(bounds[i].upper_limit())).c_str());
      }
      else
      {
        sv_double g_Fake(nmp,0.);
        g_Fake[i] = -ls[i]/10.;
        double max_dist(maximum_distance(old_x,g_Fake).first);
        double fmax(std::numeric_limits<double>::max());
        double ftol(std::numeric_limits<double>::epsilon());
        double specialLimit = (max_dist < fmax-ftol) ? old_x[i]-max_dist*g_Fake[i] : fmax;
        if (specialLimit < xmax)
        {
          xmax = specialLimit-dxgh[i];
          puts(std::string("Special upper limit: "+ to_sci_format(specialLimit)).c_str());
        }
      }
      puts(std::string("Large shift: "+ to_sci_format(ls[i])).c_str());
#ifdef PHASERTNG_STUDYPARAMS_GRADIENT_ONLY
      bool gradient_only(true);
#else
      bool gradient_only(false);
#endif
      if (gradient_only)
      {
      puts(" parameter   func-min    gradient    FD grad     grad-diff   grad-ratio");
      }
      else
      {
      puts("                                                                      FD curv    FD curv");
      puts(" parameter   func-min    gradient   curvature  crv*lrg^2   FD grad   from func  from grad grad-diff hess-diff");
      }
      double dx = (xmax-xmin)/ndiv;
      for (int j = 0; j <= ndiv; j++)
      {
        x[i] = xmin + j*dx;
        set_reparameterized_parameters(x);
        auto f_g = reparameterized_target_gradient();
        gradLogLike = f_g.Target;
        g           = f_g.Gradient;
        double df = gradLogLike - fmin;
        f_g_h = reparameterized_target_gradient_hessian();
            h = f_g_h.Hessian.deep_copy();
        double thisx = x[i]; // Save before finite diffs
        x[i] = x[i] + dxgh[i];
        set_reparameterized_parameters(x);

        f_g = reparameterized_target_gradient();
        double fplus = f_g.Target;
        auto   gplus = f_g.Gradient;
        x[i] = x[i] - 2*dxgh[i]; // Other side of original x
        set_reparameterized_parameters(x);

        f_g = reparameterized_target_gradient();
        double fminus = f_g.Target;
        auto   gminus = f_g.Gradient;

        double fdgrad = (fplus - fminus)/(2.*dxgh[i]);
        double fdh_from_F = (fplus+fminus-2*gradLogLike)/fn::pow2(dxgh[i]);
        double fdh_from_g = (gplus[i] - gminus[i])/(2.*dxgh[i]);

        x[i] = thisx; // Restore

        const int len = LOGFILE_WIDTH*2;
        char c_str[len]; //just have to be greater than string length below
        double ddg = fdgrad-g[i]; //since fdgrad is the gold standard
        double ddh = fdh_from_g-h(i,i);
        double lrg = h(i,i)*fn::pow2(ls[i]);
        if (gradient_only)
        {
        double rat = g[i] == 0 ? 0 : fdgrad/g[i];
        snprintf(c_str,len,
                 "%+.4e %+.4e %+.4e %+.4e %+10.3f %+10.5f",
                   x[i],   df, g[i],fdgrad,ddg,rat);
        puts(c_str);
        outstream << x[i] << " " << df << " " << g[i] << " " << fdgrad << " " << ddg << " " << rat << std::endl;
        }
        else
        {
        snprintf(c_str,len,
                 "%+.4e %+.3e %+.3e %+.4e %+.2e %+.3e %+.3e %+.3e %+.3e %+.3e",
                   x[i],   df, g[i],h(i,i), lrg,fdgrad,fdh_from_F,fdh_from_g,ddg,ddh);
        puts(c_str);
        outstream << x[i] << " " << df << " " << g[i] << " " << h(i,i) << " " << fdgrad << " " << fdh_from_g << "   " << ddg << "   " << ddh << std::endl;
        }
      }
      x[i] = old_x[i];
      outstream.close();
    }

    // Finite difference tests for off-diagonal Hessian elements
    if (!is_diagonal)
    {
      bool first(true);
      set_reparameterized_parameters(x);
      auto f_g = reparameterized_target_gradient();

      f_g_h = reparameterized_target_gradient_hessian();
      h = f_g_h.Hessian.deep_copy();
      for (int i = 0; i < nmp-1; i++)
      for (int j = i+1; j < nmp; j++)
      {
        if (h(i,j) != 0.) // Hessian may be sparse
        {
          if (first)
          {
            puts("\n");
            puts("Off-diagonal Hessian elements at current values");
            puts("Parameter #s   Hessian     FD from grad");
            first = false;
          }
          x[j] = x[j] + dxgh[j];
          set_reparameterized_parameters(x);

          f_g = reparameterized_target_gradient();
          auto gplus = f_g.Gradient;
          x[j] = x[j] - 2*dxgh[j]; // Other side of original x
          set_reparameterized_parameters(x);

          f_g = reparameterized_target_gradient();
          auto gminus = f_g.Gradient;
          double fdhess = (gplus[i] - gminus[i])/(2.*dxgh[j]);
          char c_str[LOGFILE_WIDTH];
          snprintf(c_str,LOGFILE_WIDTH,"%4i %4i    %+.4e   %+.4e",
            i+1,j+1,h(i,j),fdhess);
          puts(c_str);
          x[j] = old_x[j];
        }
      }
    }
    puts("\n");
    set_reparameterized_parameters(old_x);
  }

  std::pair<TargetType, GradientType>
  RefineBase::finite_difference_gradient(
      double     frac_large)
  {
    // Fraction of large shift to use for each type of function should be
    // investigated by numerical tests, varying by, say, factors of two.
    // Gradient is with respect to original parameters, so no reparameterisation is done
    // the scheme is g_i(x) ~= (f(x + sz_i) - f(x))/sz_i, unless we are at an upper bound,
    // in which case the scheme is g_i(x) ~= (f(x) - f(x - sz_i))/sz_i
    GradientType Gradient(nmp);

    std::vector<dtmin::Bounds> bounds(RefineBase::bounds());
    bool bounds_present = (bounds.size() == 0) ? false : true;
    double f(0.),fplus(0.),fminus(0.),sz(0.);
    sv_double x(get_macrocycle_parameters());
    sv_double old_x(x);
    sv_double ls(macrocycle_large_shifts());
    set_macrocycle_parameters(x); // paranoia
    f = target();
    for (int i = 0; i < nmp; i++)
    {
      sz = frac_large*ls[i];

      x[i] = old_x[i] + sz;
      if (bounds_present && bounds[i].upper_bounded() && x[i] > bounds[i].upper_limit())
      {
        x[i] = old_x[i] - sz;
        if (bounds[i].lower_bounded() && x[i] < bounds[i].lower_limit())
          // if this part of the code is reached it means that x+sz is greater than the
          // upper bound, and x-sz is lower than the lower bound, so you should reduce
          // either the large shift for the relevant parameter or frac_large. (Or change
          // your bounds)
          assert(false);
        set_macrocycle_parameters(x);
        fminus = target();
        Gradient[i] = (f - fminus)/sz;
      }
      else
      {
        set_macrocycle_parameters(x);
        fplus = target();
        Gradient[i] = (fplus - f)/sz;
      }

      x[i] = old_x[i];
    }
    set_macrocycle_parameters(old_x);
    return { f, Gradient };
  }

  std::pair<double,HessianType>
  RefineBase::finite_difference_hessian_by_gradient(
      double            frac_large,
      bool              do_repar)
  {
    HessianType Hessian;
    // Fraction of large shift to use for each type of function should be
    // investigated by numerical tests, varying by, say, factors of two.
    // Computes a finite difference approximation to the hessian using gradient function calls
    // the scheme is H_ij(x) ~=
    if (do_repar)
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0.),sz(0.);
      sv_double g(nmp),gplus(nmp);
      auto rtg = reparameterized_target_gradient();
      f = rtg.Target;
      g = rtg.Gradient;
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // resize and zero;

      for (int i=0; i<nmp; i++)
      {
        sz = frac_large*ls[i];
        x[i] = old_x[i] + sz;
        set_reparameterized_parameters(x);
        gplus = reparameterized_target_gradient().Gradient; // fplus not needed
        x[i] = old_x[i];
        for (int j = 0; j < nmp; j++)
        {
          if (i == j)
            Hessian(i,i) = (gplus[i] - g[i])/sz;
          else
            Hessian(i,j) = Hessian(j,i) += (gplus[j] - g[j])/(2*sz);
        }
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
    else
    {
      sv_double x(get_macrocycle_parameters());
      sv_double old_x(x);
      sv_double ls(macrocycle_large_shifts());
      double f(0.),sz(0.);
      sv_double g(nmp),gplus(nmp);
      auto tg = target_gradient();
      f = tg.Target;
      g = tg.Gradient;
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // resize and zero;

      for (int i=0; i<nmp; i++)
      {
        sz = frac_large*ls[i];
        x[i] = old_x[i] + sz;
        set_macrocycle_parameters(x);
        gplus = target_gradient().Gradient; // fplus not needed
        x[i] = old_x[i];
        for (int j = 0; j < nmp; j++)
        {
          if (i == j)
            Hessian(i,i) = (gplus[i] - g[i])/sz;
          else
            Hessian(i,j) = Hessian(j,i) += (gplus[j] - g[j])/(2*sz);
        }
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
  }

  std::pair<double,HessianType>
  RefineBase::finite_difference_curvatures_by_gradient(
      double frac_large,
      bool   do_repar)
  {
    HessianType Hessian;
    // Fraction of large shift to use for each type of function should be
    // investigated by numerical tests, varying by, say, factors of two.
    // This routine is inefficient, costing as much as finiteGDiffHessian
    // to compute full Hessian, but may be useful for testing purposes.
    if (do_repar)
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0.),sz(0.);
      sv_double g(nmp),gplus(nmp);
      auto rtg = reparameterized_target_gradient();
      f = rtg.Target;
      g = rtg.Gradient;
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // resize and zero;

      for (int i=0; i<nmp; i++)
      {
        sz = frac_large*ls[i];
        x[i] = old_x[i] + sz;
        set_reparameterized_parameters(x);
        gplus = reparameterized_target_gradient().Gradient; // fplus not needed
        x[i] = old_x[i];
        Hessian(i,i) = (gplus[i] - g[i])/sz;
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
    else
    {
      sv_double x(get_macrocycle_parameters());
      sv_double old_x(x);
      sv_double ls(macrocycle_large_shifts());
      double f(0.),sz(0.);
      sv_double g(nmp),gplus(nmp);
      auto tg = target_gradient();
      f = tg.Target;
      g = tg.Gradient;
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // resize and zero;

      for (int i=0; i<nmp; i++)
      {
        sz = frac_large*ls[i];
        x[i] = old_x[i] + sz;
        set_macrocycle_parameters(x);
        gplus = target_gradient().Gradient; // fplus not needed
        x[i] = old_x[i];
        Hessian(i,i) = (gplus[i] - g[i])/sz;
      }
      set_macrocycle_parameters(old_x);
      return { f, Hessian };
    }
  }

  std::pair<double,HessianType>
  RefineBase::finite_difference_hessian_by_function(
      double frac_large,
      bool   do_repar)
  {
    HessianType Hessian;
    // Fraction of large shift to use for each type of function should be
    // investigated by numerical tests, varying by, say, factors of two.
    if (do_repar)
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0),fplusi(0),fminusi(0),fplusij(0),fplusj(0),szi(0),szj(0);
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // make certain that it is the correct size, and zero;
      f = target();
      for (int i=0; i<nmp; i++)
      {
        szi = frac_large*ls[i];

        x[i] = old_x[i] - szi;
        set_reparameterized_parameters(x);
        fminusi = target();

        x[i] = old_x[i] + szi;
        set_reparameterized_parameters(x);
        fplusi = target();

        x[i] = old_x[i];

        Hessian(i,i) = (fplusi - 2*f + fminusi)/(szi*szi);

        for (int j = i+1; j < nmp; j++)
        {
          x[i] = old_x[i] + szi;
          szj = frac_large*ls[j];
          x[j] = old_x[j] + szj;
          set_reparameterized_parameters(x);
          fplusij = target();

          x[i] = old_x[i];
          set_reparameterized_parameters(x);
          fplusj = target();

          x[j] = old_x[j];
          Hessian(i,j) = Hessian(j,i) = (fplusij - fplusi - fplusj + f)/(szi*szj);
        }
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
    else
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0),fplusi(0),fminusi(0),fplusij(0),fplusj(0),szi(0),szj(0);
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // make certain that it is the correct size, and zero;
      f = target();
      for (int i=0; i<nmp; i++)
      {
        szi = frac_large*ls[i];

        x[i] = old_x[i] - szi;
        set_reparameterized_parameters(x);
        fminusi = target();

        x[i] = old_x[i] + szi;
        set_reparameterized_parameters(x);
        fplusi = target();

        x[i] = old_x[i];

        Hessian(i,i) = (fplusi - 2*f + fminusi)/(szi*szi);

        for (int j = i+1; j < nmp; j++)
        {
          x[i] = old_x[i] + szi;
          szj = frac_large*ls[j];
          x[j] = old_x[j] + szj;
          set_reparameterized_parameters(x);
          fplusij = target();

          x[i] = old_x[i];
          set_reparameterized_parameters(x);
          fplusj = target();

          x[j] = old_x[j];
          Hessian(i,j) = Hessian(j,i) = (fplusij - fplusi - fplusj + f)/(szi*szj);
        }
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
  }

  std::pair<double,HessianType>
  RefineBase::finite_difference_curvature_by_function(
      double frac_large,
      bool   do_repar)
  {
    HessianType Hessian;
    // Fraction of large shift to use for each type of function should be
    // investigated by numerical tests, varying by, say, factors of two.
    if (do_repar)
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0),fplusi(0),fminusi(0),szi(0);
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // make certain that it is the correct size, and zero;
      f = target();
      for (int i=0; i<nmp; i++)
      {
        szi = frac_large*ls[i];

        x[i] = old_x[i] - szi;
        set_reparameterized_parameters(x);
        fminusi = target();

        x[i] = old_x[i] + szi;
        set_reparameterized_parameters(x);
        fplusi = target();

        x[i] = old_x[i];

        Hessian(i,i) = (fplusi - 2*f + fminusi)/(szi*szi);
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
    else
    {
      sv_double x(get_reparameterized_parameters());
      sv_double old_x(x);
      sv_double ls(reparameterized_large_shifts());
      double f(0),fplusi(0),fminusi(0),szi(0);
      Hessian.resize(af::flex_grid<>(nmp,nmp), 0.); // make certain that it is the correct size, and zero;
      f = target();
      for (int i=0; i<nmp; i++)
      {
        szi = frac_large*ls[i];

        x[i] = old_x[i] - szi;
        set_reparameterized_parameters(x);
        fminusi = target();

        x[i] = old_x[i] + szi;
        set_reparameterized_parameters(x);
        fplusi = target();

        x[i] = old_x[i];

        Hessian(i,i) = (fplusi - 2*f + fminusi)/(szi*szi);
      }
      set_reparameterized_parameters(old_x);
      return { f, Hessian };
    }
  }

  std::string
  RefineBase::to_sci_format(const double f)
  {
    // Format a double as a string in scientific notation.
    // Seems to do the equivalent of str(f) in python.
    std::string str;
    std::stringstream s;
    //s.setf(std::ios_base::showpos); //to show the + sign if positive (default no + sign)
    s.setf(std::ios_base::scientific);
    s.precision(10); //number of decimal places
    s.width(4); //width
    s << f;
    return s.str();
  }

  std::pair<double, sv_double >
  RefineBase::maximum_distance(sv_double x, sv_double p)
  {
    // Return
    //   1) the multiple of the search vector p that x can be shifted by before
    //      hitting the furthest bound. Used to define the limit for line search.
    //   2) vector of allowed shifts for each parameter, which can be used to
    //      decide which parameters can't be moved along the search direction.

    double max_dist(0.), ZERO(0.);
    sv_double dist(nmp, -1.);  // multiple of p allowed before hitting box bound, or -1. used to indicate can't move, i.e. at a bound
    std::vector<dtmin::Bounds> bounds(reparameterized_bounds());
    sv_bool bounded(nmp, false); // given to maximum_distance_special

    if (bounds.size() != 0)
    {
      for (int i=0; i<nmp; i++)
      {
        //if (p[i] == ZERO && (bounds[i].lower_bounded() || bounds[i].upper_bounded() ))
        //  bounded[i] = true;
        if ((p[i] < ZERO && bounds[i].lower_bounded()) || (p[i] > ZERO && bounds[i].upper_bounded()))
        {
          bounded[i] = true;
          double limit = (p[i] < ZERO) ? bounds[i].lower_limit() : bounds[i].upper_limit();
          dist[i] = (limit-x[i])/p[i]; // if we are in bounds will positive, on bounds, is 0.
        }
        max_dist = std::max(max_dist,dist[i]);
      }
    }

    if (bounds.size() == 1 && bounded[0]) //there can't be any correlations
      return { max_dist, dist };
    else
      // The rationale behind maximum_distance_special() is to provide a way to impose non-linear
      // equality constraints.
      // Correlated, i.e. equality constrained parameters are checked here, updating
      // bounded and dist if relevant.
      // If correlated parameters are also limited by overall upper and lower bounds,
      // those can be set as well for the normal limit checks above.
      max_dist = std::max(max_dist,maximum_distance_special(x,p,bounded,dist,max_dist));

    // If any parameters are unbounded, search can carry on essentially indefinitely
    for (int i=0; i < nmp; i++)
    {
      if (bounded[i] == false) {max_dist = 1000000.; break;}
    }
    return { max_dist, dist };
  }

  std::pair<versa_flex_double, bool>
  RefineBase::adjust_hessian(versa_flex_double h)
  {
    // Eliminate non-positive curvatures (diagonal elements of the hessian).
    // Return true if any curvatures were non-positive.
    //
    // Determine mean factor by which curvatures differ from 1/largeShift^2
    // from parameters with positive curvatures.  This gives the average
    // curvature that would be obtained if all the parameters were scaled
    // so that their largeShift values were one.
    // Expected curvatures (consistent with relative largeShift values) are
    // computed from this and used to replace non-positive curvatures.
    // Corresponding off-diagonal terms are set to zero.
    // RJR Note 31/5/06: tried setting a minimum fraction of the expected
    // curvature, but this degraded convergence and depended too much on
    // accurate estimation of largeShift.
    // DHS Note 30/4/19: if this looks hacky, that's because it is. This
    // looks to be a fix implemented when only diagonal hessians were
    // considered as it is still possible to have negative eigen values
    // after this step for non-diagonal hessians.
    // To see this consider the case [1 3] which has eigen values (4,-2)
    //                               [3 1]

    double meanfac(0.),ZERO(0.);
    bool queue_new_hessian(false);
    int npos(0); //number of positive diagonal hessian entries
    sv_double ls(reparameterized_large_shifts());
    for (int i = 0; i < nmp; i++)
    {
      if (h(i,i) > ZERO)
      {
        npos++;
        meanfac += h(i,i)*fn::pow2(ls[i]);
      }
    }
    if (npos)
    {
      meanfac /= npos;
      for (int i = 0; i < nmp; i++)
      {
        if (h(i,i) <= ZERO)
        {
          h(i,i) = meanfac/fn::pow2(ls[i]);
          for (int j = i+1; j < nmp; j++)
            h(i,j) = h(j,i) = ZERO;
        }
      }
    }
    else // Fall back on diagonal matrix based on macrocycle_large_shifts shift values
    {
      for (int i = 0; i < nmp; i++)
      {
        h(i,i) = 100./fn::pow2(ls[i]);
        for (int j = 0; j < nmp; j++)
          if (i != j) h(i,j) = h(j,i) = ZERO;
      }
    }
    if (npos != nmp ) queue_new_hessian = true;
    return { h, queue_new_hessian };
  }

  TargetGradientHessianType
  RefineBase::local_linear_restraint(sv_double x, sv_double y,
                                     dtmin::Bounds ybounds, double sigmay,
                                     DiagonalType is_diagonal)
  {
    // Restrain parameters y as a function of underlying variable x to lie on smooth
    // curve by assuming that the function is locally approximately linear.
    // Generate target for each point by interpolating between neighbours,
    // or extrapolating for the endpoints, keeping within bounds if present.
    // Contribution to total likelihood based on Gaussian with standard deviation
    // given by sigmay (which is doubled for extrapolated targets).
    // Generate off-diagonal Hessian elements if relevant.
    int nx = x.size();
    assert(y.size() == nx);
    sv_double R(nx, 0.);
    TargetGradientHessianType tgh(nx,is_diagonal);
    if (nx > 2) // No linear restraints possible for less than 3 points
    {
      for (int np = 0; np < nx; np++)
      {
        int n1, n2; // Elements used for interpolation or extrapolation of target
        double this_sigmay = (np == 0 || np == nx - 1) ? 2 * sigmay : sigmay;
        double vary = fn::pow2(this_sigmay);
        if (np == 0) // Extrapolate back
        {
          n1 = 1;
          n2 = 2;
        }
        else if (np == nx - 1) // Extrapolate forward
        {
          n1 = nx - 3;
          n2 = nx - 2;
        }
        else // Interpolate
        {
          n1 = np - 1;
          n2 = np + 1;
        }
        double w1 = (x[n2] - x[np]) / (x[n2] - x[n1]);
        double w2 = 1. - w1;
        double ytarget = w1 * y[n1] + w2 * y[n2];
        if (ybounds.lower_bounded())
          ytarget = std::max(ytarget, ybounds.lower_limit());
        if (ybounds.upper_bounded())
          ytarget = std::min(ytarget, ybounds.upper_limit());
        double dy = y[np] - ytarget;
        tgh.Target += fn::pow2(dy) / (2. * vary);
        tgh.Gradient[np] += dy / vary;
        tgh.Gradient[n1] -= w1 * dy / vary;
        tgh.Gradient[n2] -= w2 * dy / vary;
        tgh.Hessian(np,np) += 1 / vary;
        tgh.Hessian(n1,n1) += fn::pow2(w1) / vary;
        tgh.Hessian(n2,n2) += fn::pow2(w2) / vary;
        if (!is_diagonal)
        {
          tgh.Hessian(n1, np) -= w1 / vary;
          tgh.Hessian(np, n1) -= w1 / vary;
          tgh.Hessian(n2, np) -= w2 / vary;
          tgh.Hessian(np, n2) -= w2 / vary;
          tgh.Hessian(n1, n2) += w1 * w2 / vary;
          tgh.Hessian(n2, n1) += w1 * w2 / vary;
        }
      }
    }
    return tgh;
  }

  std::tuple<double,double,double>
  RefineBase::adaptive_restraint_infinite(double k,double c, double xo,double x)
  {
    assert(c>0);
    double c2 = fn::pow2(c);
    double c4 = fn::pow4(c);
    double expterm = std::exp(-fn::pow2(x-xo)/(2*c2));
    double target = k*(1-expterm);
    double gradient = k*((x-xo)/c2)*expterm;
    double hessian = k*(expterm/c2 - expterm*(x-xo)*(x-xo)/c4);
    return std::make_tuple(target,gradient,hessian);
  }

}} // namespace phasertng
