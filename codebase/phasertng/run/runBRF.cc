//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/AnglesUnique.h>
#include <phasertng/site/AnglesAround.h>
#include <phasertng/site/AnglesRandom.h>
#include <phasertng/site/FastRotationFunction.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Brute_rotation_function
  void Phasertng::runBRF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!(DAGDB.NODES.is_all_pose() or DAGDB.NODES.has_no_turn_curl_pose()))
    throw Error(err::INPUT,"Dag not prepared with correct mode");

    Identifier identifier(
        input.value__uuid_number.at(".phasertng.search.id.").value_or_default(),
        input.value__string.at(".phasertng.search.tag.").value_or_default());
    auto search = DAGDB.lookup_entry_tag(identifier,DataBase());
    logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
    if (!search->identify().is_set_valid())
      throw Error(err::INPUT,"Search identifier not defined");

    logTab(out::VERBOSE,"Clear the full and partial entry data");
    DAGDB.clear_frf_entries(search->identify());

    bool io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").is_default() ? false : input.value__boolean.at(".phasertng.coiled_coil.").value_or_default(); //default None

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    used_modlid.insert(search->identify());
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    unsigned random_seed = 0;

    //initialize tNCS parameters from reflections and calculate correction terms
    //BRF
    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    double resolution = io_hires;// ? io_hires : DAGDB.WORK->SIGNAL_RESOLUTION;
    double signal_resolution = input.value__flt_number.at(".phasertng.brute_rotation_function.signal_resolution.").value_or_default();

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    SpaceGroup rf_spacegroup("P 1");
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Space Group");
    if (nmol > 1)
    {
      logTab(where,"Translational Ncs");
      logTab(where,"Rotation function space group will be P1");
      rf_spacegroup = SpaceGroup("P 1");
    }
    else
    {
      logTab(where,"No Translational Ncs");
      logTab(where,"Rotation function will be in space group of data");
      rf_spacegroup = REFLECTIONS->SG;
    }
    }}

    std::pair<int,char> highsymm = { 1, 'Z' };
    double cluster_angle(0),sampling(0);
    std::tie(cluster_angle,sampling) = search->clustang(
        selected.STICKY_HIRES,
        input.value__flt_number.at(".phasertng.brute_rotation_function.sampling.").value_or_default(),
        io_coiled_coil,
        false,//there are no duplicates because of the hexagonal grid spacing, only cluster or not cluster
        input.value__flt_number.at(".phasertng.brute_rotation_function.helix_factor.").value_or_default(),
        highsymm.first
      );

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Sampling");
    double io_helix_factor = input.value__flt_number.at(".phasertng.brute_rotation_function.helix_factor.").value_or_default();
    if (io_helix_factor != 1)
    {
      logTab(where,"All-helical (helix factor gridding): " + btos(search->HELIX));
      logTab(where,"Coiled-coil (helix factor gridding): " + btos(io_coiled_coil));
      logTab(where,"Helix factor: " + dtos(io_helix_factor));
    }
    logTab(where,"Sampling Angle: " + dtos(sampling,5,2) + " degrees");
    logTab(where,"Cluster Angle: " + dtos(cluster_angle,9,7) + " degrees");
    }}

    std::unique_ptr<SiteList> rotList; //virtual base class, next() and site()
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Rotation Function Angle Generation");
    if (input.value__choice.at(".phasertng.brute_rotation_function.volume.").value_or_default_equals("around"))
    {
      dmat33 ROTin = input.value__flt_matrix.at(".phasertng.brute_rotation_function.shift_matrix.").value_or_default();
      double range = input.value__flt_number.at(".phasertng.brute_rotation_function.range.").value_or_default();
      dvect3 axis; double angle;
      std::tie(axis,angle) = axis_and_angle_deg(ROTin);
      logTab(where,"Rotations around polar axis " + dvtos(axis) + " angle " + dtos(angle));
      logTab(where,"Rotations range " + dtos(range));
      logTab(where,"Rotations sampling " + dtos(sampling));
      dmat33 ROTmt = ROTin*search->PRINCIPAL_ORIENTATION.transpose();
      dvect3 eulermt = scitbx::math::euler_angles::zyz_angles(ROTmt);
      PHASER_ASSERT(sampling > 0);
      if (range == 0) range = sampling*1.1; //just add a tolerance for =
      rotList.reset(new AnglesAround(sampling,eulermt,range));
    }
    else if (input.value__choice.at(".phasertng.brute_rotation_function.volume.").value_or_default_equals("unique"))
    {
      logTab(where,"Unique rotations");
      rotList.reset(new AnglesUnique(sampling,rf_spacegroup,REFLECTIONS->UC));
    }
    else if (input.value__choice.at(".phasertng.brute_rotation_function.volume.").value_or_default_equals("random"))
    {
      int number_of_random_points  = input.value__int_number.at(".phasertng.brute_rotation_function.nrand.").value_or_default();
      rotList.reset(new AnglesRandom(number_of_random_points,random_seed++));
      logTab(where,"Random angles (#" + itos(number_of_random_points) + ")");
    }
    else throw Error(err::DEVELOPER,"No Angles type defined");
    logTab(where,"Rotation Type : " + std::string(typeid(rotList).name()));
    logTab(where,"Number of rotations: " + itos(rotList->count_sites()));
    logBlank(where);
    }}

    int io_maxstored = input.value__int_number.at(".phasertng.brute_rotation_function.maximum_stored.").value_or_default();
    int io_nprint = input.value__int_number.at(".phasertng.brute_rotation_function.maximum_printed.").value_or_default();
    bool generate_only = input.value__boolean.at(".phasertng.brute_rotation_function.generate_only.").value_or_default();

    double best_rf(std::numeric_limits<double>::lowest());
    //if this is not by reference it coredumps
    DAGDB.apply_space_group_expansion({REFLECTIONS->SG.type()});
    DAGDB.reset_entry_pointers(DataBase()); //this expands the parents as well
    DAGDB.NODES.apply_unit_cell(REFLECTIONS->UC.cctbxUC);
    dag::NodeList output_dag;

    Identifier parent;
    parent.initialize_from_text(svtos(DAGDB.WORK->logNode())+hashing());//same for same job
    parent.initialize_tag("brf"+ntos(1,DAGDB.size()));

    int n(1),N(DAGDB.size());
    DAGDB.restart();
    logProgressBarStart(out::SUMMARY,"Rotation Function",DAGDB.size());
    while (!DAGDB.at_end()) //iterates over WORK nodes
    {
      auto& work = DAGDB.WORK;
      std::string nN = nNtos(n++,N);
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Brute Rotation Function" + nN);
      //modifiy the spacegroup and unitcell here in place
      logTab(where,"Search: " + search->identify().str());
      work->initialize_turn({0,0,0},search);
      DAGDB.reset_work_entry_pointers(DataBase());
      logTabArray(where,work->logNode("Search"));
      int c(1);
      int C(rotList->count_sites());
      FastRotationFunction rotfun(REFLECTIONS.get(),nmol > 1);
      rotfun.set_nthreads(NTHREADS,USE_STRICTLY_NTHREADS);
      if (generate_only)
      logProgressBarStart(where,"Generating angles",C);
      else
      logProgressBarStart(where,"Scoring angles",C);
      rotList->restart();
      pod::FastPoseType ecalcs_sigmaa_pose;
      while (!rotList->at_end())
      {
        std::string cC = nNtos(c++,C);
        work->NEXT.EULER = rotList->next_site();
        if (generate_only)
        {
          work->LLG = 0;
          if (signal_resolution)
            work->SIGNAL_RESOLUTION = signal_resolution;
        }
        else
        {
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
        }
        rotfun.rotation_function_data.push_back(Site(work->NEXT.EULER,work->LLG));
        rotfun.rotation_function_data.back().value = work->LLG;
        if (!generate_only and work->LLG > best_rf)
        {
          best_rf = work->LLG;
          logProgressBarAgain(where,"Best llg = " + dtos(work->LLG) + cC );
        }
        logProgressBarNext(where);
      }
      logProgressBarEnd(where);

      if (!generate_only)
      {
        {{
        out::stream where = out::SUMMARY;
        rotfun.full_sort();
        int cluster_back = input.value__int_number.at(".phasertng.brute_rotation_function.cluster_back.").value_or_default();
        rotfun.cluster(cluster_angle,cluster_back);
        bool AllSites(true); //not just topsites
        logTableTop(where,"Brute Rotation Function" +nN);
        dmat33 PR = search->PRINCIPAL_ORIENTATION;
        logTabArray(where,rotfun.logSlowTable(io_nprint,AllSites,PR));
        logTableEnd(where);
        }}

        {{
        Loggraph loggraph;
        loggraph.title = "Top peaks in Brute Rotation Function" ;
        loggraph.scatter = true;
        loggraph.graph.resize(1);
        loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
        int v(1);
        for (auto& item : rotfun.rotation_function_data)
        {
          loggraph.data_text += itos(v++) + " " +
                                  dtos(item.value,10,2) + " " +
                                  "\n";
        }
        logGraph(where,loggraph);
        }}

        {{ //purge regardless
        out::stream where = out::LOGFILE;
        logEllipsisOpen(where,"Purge by cluster");
        int before = rotfun.count_sites();
        rotfun.select_topsites_and_erase();
        logEllipsisShut(where);
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(rotfun.count_sites()));
        }}
      }

      {{
      logEllipsisOpen(where,"Append to DAG");
      //add the growing list to the DAG, note that this will not be sorted overall
      for (auto& item : rotfun.rotation_function_data)
      {
        dag::Node next = *work; //deep copy converted seek to turn
        dmat33 PR = search->PRINCIPAL_ORIENTATION;
        next.NEXT.EULER = item.grid;
        next.LLG = item.value;
        if (signal_resolution)
          next.SIGNAL_RESOLUTION = signal_resolution;
        next.FSS = 'Y';
        next.ANNOTATION += " FIND="+search->identify().tag();
        next.ANNOTATION += " RF=" + dtoi(item.value);
        if (!next.PARENTS.size())
          next.PARENTS = {0,0}; //resize and init
        next.PARENTS[0] = next.PARENTS[1]; //front
        next.PARENTS[1] = parent.identifier(); //back
        //store the euler with the lowest b amongst symmetry
        std::tie(std::ignore,next.NEXT.SHIFT_MATRIX) =
            dag::print_euler_wrt_in_from_mt(item.grid,PR);
        output_dag.push_back(next);
      }
      logEllipsisShut(where);
      }}
      parent.increment("brf"+ntos(n-1,DAGDB.size()));
      if (!logProgressBarNext(out::SUMMARY)) break;
    }
    logProgressBarEnd(out::SUMMARY);

    // cannot purge by percent use the to do this

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by maximum stored");
    int before = output_dag.size();
    if (!generate_only and io_maxstored != 0) //store all with no scoring, 6D search
      output_dag.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
    logEllipsisShut(where);
    if (io_maxstored == 0)
    logTab(where,"Maximum stored: all");
    else
    logTab(where,"Maximum stored:      " + itos(io_maxstored));
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(output_dag.size()));
    }}

    DAGDB.NODES = output_dag; //overwrite
    DAGDB.reset_entry_pointers(DataBase());
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
