#include <phasertng/math/french_wilson.h>
#include <scitbx/constants.h>
#include <scitbx/math/erf.h>
#include <phasertng/math/table/ParabolicCylinderD.h>

namespace phasertng {
extern const table::ParabolicCylinderD& tbl_pcf;
namespace math {

  double
  french_wilson::expectEFWacen(double eosq, double sigesq)
  {
  /* Acentric: Compute French & Wilson posterior expected value of E, from the
     normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
  */

    const double CROSSOVER1(-12.5), CROSSOVER2(18.);
    const double SQRT2(std::sqrt(2.));

    double ee;
    double x((eosq-scitbx::fn::pow2(sigesq))/sigesq);
    double xsqr(scitbx::fn::pow2(x));

    if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
      ee = std::sqrt(-scitbx::constants::pi*sigesq/x) *
           (-916620705. + xsqr *
           (91891800.   + xsqr *
           (-11531520.  + xsqr *
           (1935360.    + xsqr *
           (-491520.    + xsqr * 262144.))))) /
           (-495452160. + xsqr *
           (55050240.   + xsqr *
           (-7864320.   + xsqr *
           (1572864.    + xsqr *
           (-524288.    + xsqr * 524288.)))));
    else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
      ee = std::sqrt(sigesq) *
           (-45045. + 32.*xsqr *
           (-315.    + 8.*xsqr *
           (-15.    - 16.*xsqr + 128.*scitbx::fn::pow2(xsqr)))) /
           (32768*std::pow(x,7.5));
    else // Moderate arguments: analytical integral
      ee = std::sqrt(sigesq/2.) * std::exp(-xsqr/4.) *
           tbl_pcf.get(-1.5,-x)/ scitbx::math::erfc(-x/SQRT2);

    return ee;
  }

  double
  french_wilson::expectEsqFWacen(double eosq, double sigesq)
  {
  /* Acentric: Compute French & Wilson posterior expected value of E^2, from the
     normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
  */

    const double CROSSOVER1(-8.9), CROSSOVER2(5.7);
    const double SQRT2BYPI(std::sqrt(2./scitbx::constants::pi));
    const double SQRT2(std::sqrt(2.));

    double eesq((eosq-scitbx::fn::pow2(sigesq))); // Default for significantly positive x
    double x(eesq/(SQRT2*sigesq));
    double xsqr(scitbx::fn::pow2(x));

    if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
      eesq *= (-135135 + xsqr *
              (20790   + xsqr *
              (-3780   + xsqr *
              (840     + xsqr *
              (-240    + xsqr *
              (96      - xsqr * 64)))))) /
              (-135135 + xsqr *
              (20790   + xsqr *
              (-3780   + xsqr *
              (840     + xsqr *
              (-240    + xsqr *
              (96      + xsqr *
              (-64     + xsqr * 128)))))));
    else if (x <= CROSSOVER2) // Moderate arguments: analytical integral
      eesq += SQRT2BYPI * sigesq / (std::exp(xsqr) * scitbx::math::erfc(-x));

    return eesq;
  }

  double
  french_wilson::expectEFWcen(double eosq, double sigesq)
  {
  /* Centric: Compute French & Wilson posterior expected value of E, from the
     normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
  */

    const double CROSSOVER1(-17.5), CROSSOVER2(17.5);
    const double SQRTPI(std::sqrt(scitbx::constants::pi));

    double pcdratio,ee;
    double x(sigesq/2.-eosq/sigesq);
    double xsqr(scitbx::fn::pow2(x));

    if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
      pcdratio = (1024.*SQRTPI*std::pow(-x,6.5)) /
                 (3465. + xsqr *
                 (840.  + xsqr *
                 (384.  + xsqr * 1024.)));
    else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
      pcdratio = (3440640. + xsqr *
                 (-491520. + xsqr *
                 (98304.   + xsqr *
                 (-32768.  + xsqr * 32768.)))) /
                 (675675.  + xsqr *
                 (-110880. + xsqr *
                 (26880.   + xsqr *
                 (-12288.  + xsqr * 32768.)))) / std::sqrt(x);
    else // Moderate arguments: analytical integral
      pcdratio = tbl_pcf.get(-1.,x)/ tbl_pcf.get(-0.5,x);

    ee = std::sqrt(sigesq/scitbx::constants::pi)*pcdratio;

    return ee;
  }

  double
  french_wilson::expectEsqFWcen(double eosq, double sigesq)
  {
  /* Centric: Compute French & Wilson posterior expected value of E^2, from the
     normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
  */

    const double CROSSOVER1(-17.5), CROSSOVER2(17.5);

    double pcdratio,eesq;
    double x(sigesq/2.-eosq/sigesq);
    double xsqr(scitbx::fn::pow2(x));

    if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
      pcdratio = (45045. + xsqr *
                 (10080. + xsqr *
                 (3840.  + xsqr *
                 (4096.  - xsqr * 32768.)))) /
                 (x *
                 (55440. + xsqr *
                 (13440. + xsqr *
                 (6144.  + xsqr * 16384.))));
    else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
      pcdratio = (11486475. + xsqr *
                 (-1441440. + xsqr *
                 (241920.   + xsqr *
                 (-61440.   + xsqr * 32768.)))) /
                 (x *
                 (675675.   + xsqr *
                 (-110880.  + xsqr *
                 (26880.    + xsqr *
                 (-12288.   + xsqr * 32768.)))));
    else // Moderate arguments: analytical integral
      pcdratio = tbl_pcf.get(-1.5,x)/ tbl_pcf.get(-0.5,x);

    eesq = sigesq*pcdratio/2.;

    return eesq;
  }

  double
  french_wilson::expectEFW(double eosq, double sigesq, bool centric)
  {
    if (sigesq <= 0.) // Apparently no measurement error
    {
      assert(eosq>=0.); // Can only allow zero sigma for non-negative I
      return std::sqrt(eosq);
    }
    double eEFW;
    if (centric) eEFW = expectEFWcen(eosq,sigesq);
    else         eEFW = expectEFWacen(eosq,sigesq);
    return eEFW;
  }

  double
  french_wilson::expectEsqFW(double eosq, double sigesq, bool centric)
  {
    if (sigesq <= 0.)
    {
      assert(eosq>=0.);
      return eosq;
    }
    double eEsqFW;
    if (centric) eEsqFW = expectEsqFWcen(eosq,sigesq);
    else         eEsqFW = expectEsqFWacen(eosq,sigesq);
    return eEsqFW;
  }

  bool
  french_wilson::is_FrenchWilsonF( af::shared<double> F, af::shared<double> SIGF, af::shared<bool> CENT)
  {
    int nviolations(0);
    int NREFL(F.size());
    for (int r = 0; r < NREFL; r++)
    {
      if (F[r] <= 0. || SIGF[r] <= 0.) return false; // French-Wilson always positive
      double SIGFoverF(SIGF[r]/F[r]);
      // After French-Wilson, SIGF/F has a maximum for centrics and acentrics,
      // obtained as SIGI -> infinity
      if (SIGFoverF > 1) return false; // Don't expect any big violations
      double maxrat = CENT[r] ? 0.756 : 0.523; // Slightly generous to be safe
      if (SIGFoverF > maxrat) nviolations ++;
    }
    double fracviol(double(nviolations)/NREFL);
    // std::cout << "Fraction of amplitude FW violations: " << fracviol << std::endl;
    return (fracviol <= 0.005);
  }

  bool
  french_wilson::is_FrenchWilsonI( af::shared<double> I, af::shared<double> SIGI, af::shared<bool> CENT)
{
  int nviolations(0);
  int NREFL(I.size());
  for (int r = 0; r < NREFL; r++)
  {
    if (I[r] <= 0. || SIGI[r] <= 0.) return false; // French-Wilson always positive
    double SIGIoverI(SIGI[r]/I[r]);
    // After French-Wilson, SIGI/I has a maximum for centrics and acentrics,
    // obtained as original SIGI -> infinity
    if (SIGIoverI > 2.0) return false; // Don't expect any big violations
    double maxrat = CENT[r] ? 1.415 : 1.001; // Slightly generous again
    if (SIGIoverI > maxrat) nviolations ++;
  }
  double fracviol(double(nviolations)/NREFL);
  // std::cout << "Fraction of intensity FW violations: " << fracviol << std::endl;
  return (fracviol <= 0.005);
}

}}
