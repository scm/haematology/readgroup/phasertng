import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys,os
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.mode_generator import *
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng import InputCard,Phasertng,PhaserError,enum_out_stream
from phasertng.run.ModeRunner import *
import argparse

def spock(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_spock)

def inner_spock(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="spock")
    initparams = initparams +  "\n" +  build_auto_str("spock")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    assert(dbdir is not None)
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    run_silent = False
    if run_silent:
      #permanently turn off all the file writing, and only output the non-database file
      dryrun_cards = silent_cards("logfile")
    else:
      dryrun_cards = ""

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    #log the loops
    loop_auto = str("Spock")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout):
      os.remove(xmlout)
    ModeRunner(basetng=basetng,mode_list=['chk'])

    #first check that there are some files, or else we don't fail until auto_search called
    #nb auto search calls this function again internally
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,"",ignore_unknown_keys)
    DAGDB = basetng.DAGDATABASE

    walk = ModeRunner(basetng=basetng,mode_list=['walk']).solution()

    #first choice
    hklin = working_params.spock.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) == 1:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    seqin = working_params.spock.seqin
    if seqin is None:
      #second choice is phasertng
      seqin = isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
      n = basetng.input.size(".phasertng.file_discovery.validated.seq_files.")
    if seqin is None and int(n) > 0:
      #third choice is discovery
      seqin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.seq_files.",0)
    if seqin is not None: #set to the bub requirement
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))

    xyzin = working_params.spock.xyzin
    if xyzin is None:
      n = basetng.input.size(".phasertng.file_discovery.validated.pdb_files.")
      if int(n) == 1:
        ifile = isNone(basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",int(0)))
        basetng.input.set_as_str(".phasertng.model.filename.",str(ifile)) #first in list
    else:
      basetng.input.set_as_str(".phasertng.model.filename.",str(xyzin)) #only one

    mapin = working_params.spock.mapin
    if xyzin is None and mapin is None:
      raise ValueError(enum_err_code.input,"No model specified")
    if seqin is None and mapin is None:
      raise ValueError(enum_err_code.input,"No model sequence specified")

    basetng.input.set_as_str(".phasertng.reflections.read_anomalous.",str(True))
  # basetng.input.set_as_str(".phasertng.tncs.order.",str(1)) #no tncs forced
    ModeRunner(basetng=basetng,mode_list=['imtz'])
    anomalous = basetng.input.get_as_str(".phasertng.data.anomalous.")
    if anomalous == "False":
      basetng.logLoopInfo(enum_out_stream.process,"No anomalous data")
      raise ValueError(enum_err_code.input,"No anomalous data")
    wavelength = basetng.input.get_as_str(".phasertng.reflections.wavelength.")
    aniso1 = ModeRunner(basetng=basetng,mode_list=['data','aniso','feff','cca','ccs','eatm','ssa']).solution()
  # don't fail if no anomalous signal, because mr-sad doesn't need it

    if xyzin is not None:
      basetng.logLoopInfo(enum_out_stream.process,"MR-SAD phasing from input partial structure from coordinates")
      basetng.input.set_as_str(".phasertng.atoms.filename.",xyzin)
      basetng.input.set_as_str(".phasertng.model.filename.",xyzin)
      basetng.input.set_as_str(".phasertng.model.single_atom.use.","False")
      basetng.input.set_as_str(".phasertng.molecular_transform.preparation.","interpolation monostructure")
      ModeRunner(basetng=basetng,mode_list=['esm','put','rbm','ssr'])
    elif mapin is not None:
      basetng.logLoopInfo(enum_out_stream.process,"MR-SAD phasing from input partial structure from density")
      basetng.input.set_as_str(".phasertng.model_as_map.filename.",mapin)
      basetng.input.set_as_str(".phasertng.model_as_map.sequence.filename.",seqin)
      basetng.input.set_as_str(".phasertng.model_as_map.require_box.","False") #phased asu content for put
      ModeRunner(basetng=basetng,mode_list=['msm','put','rbm','ssr'])
    else:
      raise ValueError(enum_err_code.input,"No map and no model")

    pdbin = isNone(basetng.input.get_as_str(".phasertng.pdbin.filename."))
    if pdbin is not None:
      basetng.logLoopInfo(enum_out_stream.process,"Reference structure provided for correlation analysis")
      ModeRunner(basetng=basetng,mode_list=['sscc'])

    tracker = basetng.DAGDATABASE.PATHWAY.ULTIMATE.identifier()
    #emit the node for the srf to fill the gui with the node
    if progress_callback is not None:
      progress_callback.emit(tracker)
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="directory path walked for data files",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  if (args.input is not None):
    if not (os.path.exists(args.input)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndirectory=\"" + str(args.input)+"\"\n"
  if (args.output is not None):
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndatabase=\"" + str(args.output)+"\"\n"
  user_phil_str = user_phil_str.replace("\n\n","\n")
  spock( user_phil_str=user_phil_str)
