from __future__ import division
from phasertng import *
import copy, os

o = Output("PHENIX")
r = Reflections(0)
#set_history defines what is in the mtz file for reading
r.set_history(["PHASER ID 000000001 toxd",
 "PHASER TIMESTAMP Fri_Dec__1_23:00:00_2023",
 "PHASER PREPARATION INIT",
 "PHASER LABIN FNAT/FOBS/F SIGFNAT/SIGMA/Q",
 "PHASER DATA INTENSITIES F FRENCH-WILSON T  ANO F MAP F EVALUES F",
])
data = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../data/toxd/toxd.mtz")
r.set_filename(data)
r.read_from_disk()
rr = copy.deepcopy(r)
print(r.get_hall())
print(r.get_cell())
