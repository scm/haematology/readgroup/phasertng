//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_SADELLG_class__
#define __phasertng_SADELLG_class__
#include <array>
#include <phasertng/math/likelihood/isad/sigmaa.h>

namespace phasertng {
namespace likelihood {

class SADeLLG
{
  public:
    SADeLLG() {}

  public:
    std::array<double,3>
    get(const int& s, const double& RESN, const double& TEPS,
      const double& DOBSPOS, double FEFFPOS, const double& DOBSNEG, double FEFFNEG,
      likelihood::isad::sigmaa ISAD_SIGMAA, const bool& BOTH, const bool& PLUS, const bool& CENT);

  private:
    std::array<double,3>
    acentric(const int& s, const double& RESN, const double& TEPS,
      const double& DOBSPOS, double FEFFPOS, const double& DOBSNEG, double FEFFNEG,
      likelihood::isad::sigmaa ISAD_SIGMAA);

};

}}
#endif
