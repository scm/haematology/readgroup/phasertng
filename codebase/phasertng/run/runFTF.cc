//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/subgroups.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/src/Packing.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <phasertng/pod/pakdat.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>
#include <cctbx/eltbx/tiny_pse.h>

namespace phasertng {

  //Fast_translation_function
  void Phasertng::runFTF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_turn() and
        !input.value__boolean.at(".phasertng.fast_translation_function.single_atom.use.").value_or_default())
    throw Error(err::INPUT,"Dag not prepared with correct mode (rf failed?)");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete, skipping");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    DAGDB.clear_packing(); //kept until now, as we need background packing for frfr purge step

    if (input.value__boolean.at(".phasertng.fast_translation_function.single_atom.use.").value_or_default())
    {
      logUnderLine(out::SUMMARY,"Single Atom Search");
      auto scatterer = input.value__string.at(".phasertng.fast_translation_function.single_atom.scatterer.").value_or_default();
      cctbx::eltbx::tiny_pse::table cctbxAtom;
      try { cctbxAtom = cctbx::eltbx::tiny_pse::table(scatterer); }
      catch (std::exception const& err) { throw Error(err::INPUT,"Search identifier not single atom"); }
      logTab(out::SUMMARY,"Single atom: " + scatterer + " [" + itos(cctbxAtom.atomic_number()) + "]");
      Identifier identifier; //no uuid
      identifier.initialize_tag(scatterer);
      auto search = DAGDB.lookup_entry_tag(identifier,DataBase());
      logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
      if (!search->identify().is_set_valid())
        throw Error(err::INPUT,"Single atom  not defined");
      if (!search->ATOM)
        throw Error(err::INPUT,"Search must be for single atom");
      dvect3 self(0,0,0);
      Identifier parent;
      parent.initialize_from_text(svtos(DAGDB.WORK->logNode())+hashing());//same for same job
      parent.initialize_tag("sa"+ntos(1,DAGDB.size()));
      for (int i = 0; i < DAGDB.NODES.size(); i++)
      {
        auto& next = DAGDB.NODES[i];
        next.initialize_turn(self,search);
        next.ANNOTATION += " RF=*";
        if (!next.PARENTS.size())
          next.PARENTS = {0,0}; //resize and init
        next.PARENTS[0] = next.PARENTS[1]; //front
        next.PARENTS[1] = parent.identifier(); //back
      }
      logTab(out::SUMMARY,"Single atom prepared for translation function");
    }

    bool io_use_packing = input.value__boolean.at(".phasertng.fast_translation_function.packing.use.").value_or_default();
    bool io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").is_default() ? false : input.value__boolean.at(".phasertng.coiled_coil.").value_or_default(); //default None

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn"); //gyre included if present
    for (auto id : used_modlid)
    {
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
      if (io_use_packing)
        load_entry_from_database(where,entry::TRACE_PDB,id,false);
    }
    }}
    DAGDB.apply_interpolation(interp::FOUR); //previously interpolations for ft fft were interp::EIGHT

    //need to shift tracker here for write_maps
    //but above not required because we are now using pathway and numberer ftf maps

    dmat33 orthmat = REFLECTIONS->UC.orthogonalization_matrix();

    double io_worst_clash = input.value__percent.at(".phasertng.packing.percent.").value_or_default();
    bool io_use_cluster = input.value__boolean.at(".phasertng.fast_translation_function.cluster.").value_or_default();
    bool io_report_cluster = input.value__boolean.at(".phasertng.fast_translation_function.report_cluster.").value_or_default();
    double io_hightfz = input.value__flt_number.at(".phasertng.packing.high_tfz.").value_or_default();
    bool io_keep_hightfz = (input.value__boolean.at(".phasertng.packing.keep_high_tfz.").value_or_default());
    double io_overlap = (input.value__percent.at(".phasertng.packing.overlap.").value_or_default());
    double io_stop_rfp = (input.value__percent.at(".phasertng.fast_translation_function.stop.rf_percent_under.").value_or_default());
    double io_stop_tfz = (input.value__flt_number.at(".phasertng.fast_translation_function.stop.best_packing_tfz_over.").value_or_default());
    double io_stop_rfz = (input.value__percent.at(".phasertng.fast_translation_function.stop.rf_zscore_under.").value_or_default());

    pod::pakflag pak_flag;
    std::map<char,std::string> map_flag;
    int h(0);
    for (auto item : pak_flag.info) map_flag[item.first] = item.second;
    for (auto item : pak_flag.info) h = std::max(h,int(item.second.size()));
    std::string P = "P ";
    for (auto item : pak_flag.info) P += chtos(item.first) + "/";
    P.back() = ' ';
    for (auto item : pak_flag.info) P += item.second + "/";
    P.pop_back();
    //determine if this is the first tf or not
    auto tf2 = DAGDB.NODES.number_of_poses();
    auto tf1 = !tf2;

    auto io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    {{
    if (io_use_packing)
    {
      logTab(out::LOGFILE,"Packing constraint will be applied to top peak: "+dtos(io_worst_clash,2)+"%");
      logTab(out::LOGFILE,"Traces loaded");
    }
    else
    {
      logTab(out::LOGFILE,"Packing constraint will not be applied to top peak");
      logTab(out::LOGFILE,"Traces not loaded");
    }
    io_report_cluster = io_use_cluster or io_report_cluster;
    }}

    //check that search_uuid is not an atom

    //calculate the default sampling
    double io_percent = input.value__percent.at(".phasertng.fast_translation_function.percent.").value_or_default();
    double cluster_distance,sampling;
    //peak selection on tfz means that when it comes to the final purge, the cutoff is in terms of tfz not the raw values
    //this should mean that there are fewer peaks selected in the case of wrong space group
    ///if peak_selection_on_tfz=true 1bos=19 poses
    ///if peak_selection_on_tfz=false 1bos=20 poses
    double peak_selection_on_tfz = false;//DAGDB.NODES[0].TNCS_INDICATED == 'Y';
    {{
    std::tie(cluster_distance,sampling) = EntryHelper::clustdist(
      selected.STICKY_HIRES,
      DAGDB.WORK->NEXT.ENTRY->HELIX,
      io_coiled_coil,
      input.value__flt_number.at(".phasertng.fast_translation_function.helix_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.fast_translation_function.sampling_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.fast_translation_function.sampling.").value_or_default());
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Selection and Sampling");
    logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
    double io_helix_factor = input.value__flt_number.at(".phasertng.fast_translation_function.helix_factor.").value_or_default();
    if (io_helix_factor != 1)
    {
      logTab(where,"All-helical (helix factor gridding): " + btos(DAGDB.WORK->NEXT.ENTRY->HELIX));
      logTab(where,"Coiled-coil (helix factor gridding): " + btos(io_coiled_coil));
      logTab(where,"Helix factor: " + dtos(io_helix_factor));
    }
    logTab(where,"Sampling Distance: " + dtos(sampling,5,2) + " Angstroms");
    logTab(where,"Cluster Distance:  " + dtos(cluster_distance,5,2) + " Angstroms");
    if (peak_selection_on_tfz)
      logTab(where,"Peak selection on tfz rather than llg");
    logBlank(where);
    }}

    int nstart = DAGDB.size();
    {{
    out::stream where = out::LOGFILE;
    if (tf1)
    {
      //find all the spacegroups that will be used
      logUnderLine(where,"Space Groups");
      logTab(where,"Space groups from list of alternatives");
      std::string selection = input.value__choice.at(".phasertng.sgalt.selection.").value_or_default();
      auto spcgrps = input.value__strings.at(".phasertng.sgalt.spacegroups.").value_or_default();
      logTab(where,"Choice: " + selection);
      std::set<std::string> sglist;
      for (auto sg : spcgrps)
      {
        SpaceGroup tmp(sg);
        sglist.insert(tmp.HALL);
        logTab(out::VERBOSE,snprintftos("List %-13s  [ %-s ]",tmp.CCP4.c_str(),tmp.HALL.c_str()));
      }
      std::vector<cctbx::sgtbx::space_group_type>  sgalternative =
          space_groups_in_same_point_group( REFLECTIONS->SG.type(),selection,sglist);
      //replace the DAGDB with a list that has been expanded by the spacegroup alternatives
      if (sgalternative.size() == 0)
        throw Error(err::INPUT,"No space groups in selection in data space group point group");
      logTableTop(where,"Space Group Alternatives");
      if (DAGDB.NODES[0].TNCS_INDICATED == 'Y')
        logTab(where,"Pseudo-translations and twinning may mean systematic absences do not correspond with space group");
      for (const auto& sg : sgalternative)
      {
        SpaceGroup tmp("Hall: " + sg.hall_symbol());
        logTab(where,snprintftos("%-13s  [ %-s ]",tmp.CCP4.c_str(),tmp.HALL.c_str()));
      }
      logTableEnd(where);
      DAGDB.apply_space_group_expansion(sgalternative); //this expands the parents as well
      DAGDB.reset_entry_pointers(DataBase()); //this expands the parents as well
      //PARENTS[0] is the original frf parent
      //PARENTS[1] is the new parent unique by node
      DAGDB.reset_entry_pointers(DataBase());
      int nfinal = DAGDB.size();
      if (nfinal > nstart)
        logTab(where,"Number of nodes expanded from " + itos(nstart) + " to " + itos(nfinal) + " by space group");
      else
      {
        logTab(where,"Number of nodes not expanded by space group");
        logTab(where,"Space Group retained or changed");
      }
    }
    else
    {
      logUnderLine(where,"Space Groups");
      std::map<std::string,int> sglist;
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        sglist[DAGDB.NODES[i].SG->CCP4] = 0;
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        sglist.at(DAGDB.NODES[i].SG->CCP4)++;
      for (const auto& sg : sglist)
      {
        SpaceGroup tmp(sg.first);
        logTab(where,snprintftos("%-13s  [ %-s ] #nodes=%-d",tmp.CCP4.c_str(),tmp.HALL.c_str(),sg.second));
      }
    }
    if (tf2 and
        input.value__boolean.at(".phasertng.fast_translation_function.add_pose_orientations.").value_or_default() and
        !input.value__boolean.at(".phasertng.fast_translation_function.single_atom.use.").value_or_default()) //all 000 anyway
    {
      logUnderLine(where,"Add Pose Orientations");
      logTab(where,"Orientation list expanded from list of current poses");
      int before = DAGDB.size();
      logEllipsisOpen(where,"Expanding");
      auto txt = DAGDB.NODES.add_pose_orientations();
      logEllipsisShut(where);
      logTab(where,"Number until addition: " + itos(before));
      logTab(where,"Number after addition: " + itos(DAGDB.size()));
      if (txt.size())
      {
        out::stream where = out::VERBOSE;
        logTableTop(where,"Added Orientations");
        logTabArray(where,txt);
        logTableEnd(where);
      }
    }
    }}

    //initialize tNCS parameters from reflections and calculate correction terms
    //FTF
    bool store_halfR(true);
    bool store_fullR(tf2);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    int io_nprint = input.value__int_number.at(".phasertng.fast_translation_function.maximum_printed.").value_or_default();
    int io_maxstored = input.value__int_number.at(".phasertng.fast_translation_function.maximum_stored.").value_or_default();
    double io_lowtfz = input.value__flt_number.at(".phasertng.fast_translation_function.packing.low_tfz.").value_or_default();

    if (io_use_packing)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Fast Translation Packing");
      logTab(where,"Signal in fast translation function will be checked for packing");
      logTab(where,"Worst clash for packing check = " + dtos(io_worst_clash,2) + "%");
      logTab(where,"Lowest TFZ for packing check  = " + dtos(io_lowtfz,2));
      logTab(where,"Keep high TFZ despite clashes = " + btos(io_keep_hightfz));
      if (io_keep_hightfz)
      {
        logTab(where,"High TFZ for keeping if clash = " + dtos(io_hightfz,2));
        logTab(where,"But reject overlap with clash > " + dtos(io_overlap,2) + "%");
      }
      logTab(where,P);
    }

    int numtfs = DAGDB.size();

    std::map<char,pod::pakdat> mappakdat;
    for (auto item : pak_flag.info) mappakdat[item.first] = pod::pakdat();
    dag::NodeList output_dag;
    {{
    if (tf1 and REFLECTIONS->SG.is_p1())
    {
      out::stream where = out::LOGFILE;
      logTab(where,"First Translation Function in P1");
      logTab(where,"Score will be LLG");
      logTab(where,"Z-score will be (retained) from FRF");
      logBlank(where);
      logProgressBarStart(where,"Scoring positions",DAGDB.NODES.size(),1,false);
      DAGDB.restart(); //very important to call this to set WORK
      int n(1),N(DAGDB.size());
      while (!DAGDB.at_end())
      { //in place conversion
        std::string nN = nNtos(n++,N);
        dag::Node* work = DAGDB.WORK;
        dag::Node  node = *DAGDB.WORK; //copy
        work->convert_turn_to_curl();
        node.convert_turn_to_curl();
        DAGDB.reset_work_entry_pointers(DataBase());
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.work_node_likelihood( //interp::FOUR
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        //next.ZSCORE = (item.value - trafun.f_mean)/trafun.f_sigma; //f_sigma is zero
        node.NUM = n-1;
        double peak_height = 100-(n-1)*DEF_PPB; //sort order faking
        node.generic_float = peak_height; //percent, lowest_mean
        node.PACKS = pak_flag.U;
        if (io_use_packing)
        {
          double distance_factor(0.5); //lower than trace distance sampling
          double zscore = work->ZSCORE;
          if (zscore > io_lowtfz)
          {
            Packing packing(distance_factor);
            //we can mess with work because we have already copied to node
            work->convert_curl_to_pose(REFLECTIONS->TNCS_VECTOR);
            //have to have work in DagDatabase so that pointers to entries and sgs can be found
            packing.init_node(DAGDB.WORK); //must be linked with entries
            packing.multiplicity_and_exact_site();
            bool last_only(true); //doesn't matter, first component
            bool no_cell_translation(REFLECTIONS->MAP);
            packing.calculate_packing(last_only,no_cell_translation);
            double worst = packing.PACKING_DATA.worst_percent();
            if (worst <= io_worst_clash) //= very important for zero
            {
              mappakdat[pak_flag.Y].setpakdat(peak_height,zscore,n-1,worst);
              node.PACKS = pak_flag.Y;
            }
            else
            {
              if (io_keep_hightfz and zscore >= io_hightfz)
              {
                if (worst > io_overlap)
                {
                  mappakdat[pak_flag.O].setpakdat(peak_height,zscore,n-1,worst);
                  node.PACKS = pak_flag.O;
                }
                else
                {
                  mappakdat[pak_flag.Z].setpakdat(peak_height,zscore,n-1,worst);
                  node.PACKS = pak_flag.Z;
                }
              }
              else
              {
                mappakdat[pak_flag.X].setpakdat(peak_height,zscore,n-1,worst);
                node.PACKS = pak_flag.X;
              }
            }
          }
          else
          {
            mappakdat[pak_flag.U].setpakdat(peak_height,zscore,n-1);
            node.PACKS = pak_flag.U;
          }
        }
        output_dag.push_back(node); //with all the modifications to packing etc
        logProgressBarNext(where);
      }
      logProgressBarEnd(where);
    }
    else //not first translation function in p1
    {
      pod::parent_mean_sigma_top stats;
      bool haveFpart = tf2;
      logUnderLine(out::VERBOSE,"Fast Translation Setup");
      DAGDB.restart(); //very important to call this to set WORK
      FastTranslationFunction trafun(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon
          );
      trafun.allocate_memory(REFLECTIONS.get(),sampling,haveFpart);
      logTabArray(out::VERBOSE,trafun.logSetup());
      logTab(out::VERBOSE,"Has partial structure: " + btos(haveFpart));
      logTab(out::VERBOSE,"Gridding: " + ivtos(trafun.gridding));

      logUnderLine(out::LOGFILE,"Fast Translation Speed");
      logTab(out::LOGFILE,"Search abandoned if:");
      logTab(out::LOGFILE,"++(Current) Best packing TFZ > " + dtoss(io_stop_tfz,2) + "z");
      logTab(out::LOGFILE,"++Rotation function FSS < " + dtoss(io_stop_rfp,1) + "%");
      //set an extra criteria for stopping based on problematic data with high rfz
      logTab(out::LOGFILE,"++Rotation function RFZ < " + dtoss(io_stop_rfz,2) + "z");
      double lowestperc = std::numeric_limits<double>::max();
      for (int i = 0; i < DAGDB.size(); i++) lowestperc = std::min(lowestperc,DAGDB.NODES[i].PERCENT);
      double lowestrfz = std::numeric_limits<double>::max();
      for (int i = 0; i < DAGDB.size(); i++) lowestrfz = std::min(lowestrfz,DAGDB.NODES[i].ZSCORE);
      logTab(out::LOGFILE,"Lowest FSS in rotation list = " + dtos(lowestperc,2) + "%");
      logTab(out::LOGFILE,"Lowest RFZ in rotation list = " + dtos(lowestrfz,2) + "z");

      int n(1),N(DAGDB.size());
      DAGDB.restart();
      std::string bestnNf = nNtos(n,N);
      std::string bestnNz = nNtos(n,N);
      out::stream progress = out::LOGFILE;
      logUnderLine(progress,"Fast Translation Function");
      logTab(progress,("Background Poses: " + itos(DAGDB.NODES.number_of_poses())));
      logProgressBarStart(progress,"Fast Translation Functions",DAGDB.size(),1);
      auto set_of_parents = DAGDB.set_of_parents();
      bool trafun_top_packs = true;
      bool hasbest= false;
      double besttfzpaks(0);
      uuid_t bestparent(0);
      std::map<int,Loggraph> loggraph_list;
      bool breakit(false);
      for (auto parent : set_of_parents)
      {
        if (breakit) break;
        //group the turns by background so that the ecalcs_sigmaa_pose is done first time only
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.restart(); //very important
        while (!DAGDB.at_end())
        {
          dag::Node* work = DAGDB.WORK;
          //only break if we have tested all the space groups in the set,
          //because high tfz in related space group can be a false flag
          if (work->PARENTS.back() != parent)
            continue;
          if (breakit) break;
          auto& turn = work->NEXT;
          std::string nN = nNtos(n++,N);
          out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
          if ((n-2) ==  io_nprint)
            logTab(out::VERBOSE,"--- etc " + itos(DAGDB.size()-io_nprint) + " more translation function");
          {{
          logUnderLine(where,"Fast Translation Function" + nN);
          //reflections stored as pointer, so below changes FTF reflections object
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work->SG);
          if (diffsg)
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          logTab(where,"Space Group: " + work->SG->CCP4);
          logTab(where,"Fast Rotation Function: " + dtos(work->PERCENT,1) + "%/" +dtos(work->ZSCORE,1) + "z");
          logTab(where,"High packing TFZ = " + std::string(hasbest?dtos(besttfzpaks,2):"None"));
          if (false and Suite::Extra())
            logTabArray(out::TESTING,work->logNode(nN+""));

          if (work->PERCENT < io_stop_rfp and
              besttfzpaks >= io_stop_tfz and //check overall is going to pack
              work->ZSCORE < io_stop_rfz) //this rotation is still very interesting
         //    and (bestparent != work->PARENTS[0] or work->PARENTS[0] == 0)) //not same sgalt group
          {
            logTab(where,"Found high packing tfz for fss limit (for parent), skip");
            //increment the progress bar checking time
            if (!logProgressBarNext(progress,1)) breakit = true;
            //don't break because the nodes are not in rf fss order, just skip over these
            continue;
          }

          if (!ecalcs_sigmaa_pose.precalculated)
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa( //interp::FOUR
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS);
          DAGDB.work_node_likelihood( //interp::FOUR
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose); //if not the first parent node, then this will be precalculated
          logTab(where,"Turn angle: " + dag::print_polar(turn.SHIFT_MATRIX));
          logTabArray(where,work->logAnnotation("Known components"));
          }}
          trafun.translation_function_data.clear();
          work->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
          trafun.calculate_letf1(REFLECTIONS.get(),work); //previously interp::EIGHT
          if (trafun.negvar) logWarning("Negative variance ignored, contact developers");

          {{
          logTab(where,"Mean and Standard Deviation");
          int p = DAGDB.NODES.llg_precision(true);
          int q = DAGDB.NODES.llg_width(true);
          trafun.statistics();
          logTab(where,snprintftos(
              "%*s %*s %*s %10s",
              q,"Mean",q,"Sigma",q,"Top","Top-Z"));
          logTab(where,snprintftos(
              "%*.*f %*.*f %*.*f %10.3f",
              q,p,trafun.f_mean,q,p,trafun.f_sigma,q,p,trafun.f_top,trafun.z_top));
          logBlank(where);
          (trafun.tf_max()->f == std::numeric_limits<double>::lowest()) ?
            logTab(where, "Current Max-Top = n/a "):
            logTab(where, "Current Max-Top = " + dtos(trafun.tf_max()->f,2));
          stats.mst[n-2] = dvect3(trafun.f_mean,trafun.f_sigma,trafun.f_top); //map
          }}

          {{ //need the mean to do below, can't short circuit the statistics calculation
          double tolerance = 5;
          double io_percent_tol = std::max(0.0,io_percent-DEF_PPM-tolerance);
          //fast bailout without peak picking and mean truncation if the max is lower than the f_max
          //however need to leave some room because interpolation may increase the peak height
          //always need some tolerance for numerical instablities
          double ptol = 100*(trafun.f_top-trafun.f_mean)/(trafun.tf_max()->f-trafun.f_mean);
          if (trafun.tf_max()->f != std::numeric_limits<double>::lowest() and
              ptol <= (io_percent_tol) and
              trafun.z_top < io_hightfz) //extra condition, reuse io_hightfz
          {
            logBlank(where);
            logChevron(where,"No values over percent cutoff (" + dtos(io_percent_tol,2) + ") with current Max-Top"+nN);
            logTab(where,"Current Best LLG: LLG=" + dtos(trafun.tf_max()->f,2) + " TFZ=" + dtos(trafun.tf_max()->z,2) + " " + bestnNf);
            logTab(where,"Current Best TFZ: LLG=" + dtos(trafun.tz_max()->f,2) + " TFZ=" + dtos(trafun.tz_max()->z,2) + " " + bestnNz);
            if (!logProgressBarNext(progress,1)) breakit = true;
            continue;
          }
          }}

          //peak search from map, but only store the ones that are over the percent
          //the percent cutoff is used here as only the fast search has a mean and stddev
          //continuous shifts and score is slow if done on all peaks
          af_double histogram;
          {{ //packing section
          phaser_assert(turn.IDENTIFIER.is_set_valid());
          DAGDB.reset_work_entry_pointers(DataBase());
          dvect3 PT = turn.ENTRY->PRINCIPAL_TRANSLATION; //used below
          //below makes sites wrt input
          af_double peak_heights; af_dvect3 peak_sites;
          histogram = trafun.peak_search(peak_heights,peak_sites);
          //could do fast bailout without mean cutoff if the max is lower than the f_max
          //but this is fast anyway and will bail below
          af_char peak_packing(peak_heights.size(),pak_flag.U);

          //ok now start the process of packing
          int bmc = peak_heights.size();
          logEllipsisOpen(where,"Apply Mean Cutoff");
          //since store by percent, can't store lower than mean
          trafun.apply_mean_cutoff(peak_heights,peak_sites);
          logEllipsisShut(where);
          logTab(where,"Number until purge: " + itos(bmc));
          logTab(where,"Number after purge: " + itos(peak_heights.size()));

          int num_peaks(peak_sites.size()); //store original size for logging
          af_dvect3 fail_tra; //previous failures, for cases of polar axes, speed, don't check equivalents
          if (io_use_packing and peak_sites.size())
          {
            int ntfz(0); //count for packing
            for (int t = 0; t < peak_sites.size(); t++)
            {
              double zscore = (peak_heights[t]-trafun.f_mean)/trafun.f_sigma;
              if (zscore >= io_lowtfz) ntfz++;
            }
            if (!ntfz)
              logTab(where,"No packing: There are no peaks above lowest tfz for packing check");
            else
              logTab(where,"There are peaks above lowest tfz for packing check = "+ itos(ntfz));
            if (peak_heights[0] < trafun.tf_max()->f)
            {
              double zscore = (peak_heights[0]-trafun.f_mean)/trafun.f_sigma;
              mappakdat[pak_flag.U].setpakdat(peak_heights[0],zscore,n-1);
              logTab(where,"But no packing: Top below overall maximum peak height");
            }
            else if (ntfz)
            {
              //progress bar where must be out::TESTING to report inner testing output
              out::stream testing = out::TESTING;
              dag::Node copy_work = *DAGDB.WORK; //deep copy, entries not init
              double distance_factor(0.5); //lower than trace distance sampling
              Packing packing(distance_factor);
              af_string packing_output;
              logProgressBarStart(testing,"Packing top positions",ntfz,2,false);
              for (int i = 0; i < peak_sites.size(); i++)
              {
                double zscore = (peak_heights[i]-trafun.f_mean)/trafun.f_sigma;
                if (peak_heights[i] < trafun.tf_max()->f)
                {
                  if (Suite::Extra())
                  {
                    std::string tmp = " FTF=" + dtos(peak_heights[i],2);
                    tmp += " FTF-max=" + dtos(trafun.tf_max()->f,2);
                    tmp += " TFZ=" + dtos(zscore,1);
                    packing_output.push_back("Packing Stop on LLG: Peak #" + itos(i+1) + " below overall maximum peak height"+tmp);
                  }
                  mappakdat[pak_flag.U].setpakdat(peak_heights[i],zscore,n-1);
                  break; //because it doesn't matter
                }
                if (zscore < io_lowtfz)
                { //if first tf, so that top tf not set, then might run down whole list packing (slow)
                  //instead terminate when the tfz gets too boring
                  //maybe a later tf will give a high f_max that packs
                  //in other words, we don't want to pack the top one regardless of tfz
                  //if there is no signal, the exact value of the highest peak is irrelevant (much for muchness)
                  if (Suite::Extra())
                    packing_output.push_back("Packing Stop on TFZ: Peak #" + itos(i+1) + " below TFZ=" +dtos(io_lowtfz,1));
                  mappakdat[pak_flag.U].setpakdat(peak_heights[i],zscore,n-1);
                  break; //because it doesn't matter
                }
                bool novel; dvect3 TRA; int nv;
                std::tie(novel,TRA) = trafun.coordinates(peak_sites[i],fail_tra,turn.EULER,PT,nv);
                if (!novel)
                {
                  if (Suite::Extra())
                    packing_output.push_back("Packing not novel: Peak #" + itos(i+1) + " " + dvtos(TRA,7,3) + " FTF=" + dtos(peak_heights[i],2) + "=#" + itos(nv+1)); //wrt in
                  //packing set to not packs, although it may be z or o instead
                  peak_packing[i] = pak_flag.X;
                  mappakdat[pak_flag.X].setpakdat(peak_heights[i],zscore,n-1);
                  logProgressBarNext(testing,2);
                  continue; //will be same as previous packed (i.e. won't pack)
                }
                dag::Node work = copy_work; //change the work to an external node, not internal NODES
                work.convert_turn_to_pose(peak_sites[i],REFLECTIONS->TNCS_VECTOR); //wrt mt
                DAGDB.WORK = &work; //change the work to an external node, not internal NODES
                //have to have work in DagDatabase so that pointers to entries and sgs can be found
                DAGDB.reset_work_entry_pointers(DataBase());
                packing.init_node(DAGDB.WORK); //must be linked with entries
                packing.multiplicity_and_exact_site();
                bool last_only(true); //assume that this has already been through a packing test
                bool no_cell_translation(REFLECTIONS->MAP);
                packing.calculate_packing(last_only,no_cell_translation);
                double worst = packing.PACKING_DATA.worst_percent();
                if (Suite::Extra())
                  packing_output.push_back("Packing Test: Peak #" + itos(i+1) + " Clashes " + dtos(worst,2)+"%"+nN);
                if (Suite::Extra())
                {
                  std::string tmp = "Testing Rescore: Peak #" ;
                  tmp += itos(i+1,7,false,false) + " tra=(";
                  tmp += dvtos(TRA,7,3); //wrt in
                  tmp += ")   Clashes " + dtos(worst,5,1) + "%";
                  tmp += " FTF=" + dtos(peak_heights[i],2);
                  tmp += " TFZ=" + dtos(zscore,2);
                  pod::FastPoseType ecalcs_sigmaa_pose;
                  DAGDB.work_node_likelihood( //very slow! //interp::FOUR
                    REFLECTIONS.get(),
                    selected.get_selected_array(),
                    &epsilon,
                    NTHREADS,
                    USE_STRICTLY_NTHREADS,
                    &ecalcs_sigmaa_pose);
                  tmp += " LLG=" + dtos(DAGDB.WORK->LLG,1);
                  packing_output.push_back(tmp+nN);
                }
                if (worst <= io_worst_clash) //= very important for zero
                {
                  logProgressBarNext(testing,2);
                    packing_output.push_back("Packing Success: Break at Peak #" + itos(i+1) + " packs"+nN);
                  peak_packing[i] = pak_flag.Y;
                  mappakdat[pak_flag.Y].setpakdat(peak_heights[i],zscore,n-1,worst);
                  break; //top_packs at last value
                }
                else
                {
                  if (io_keep_hightfz and zscore >= io_hightfz)
                  {
                    if (worst >= io_overlap)
                    { //never kept, just too awful and screws selection stats up completely
                      peak_packing[i] = pak_flag.O;
                      mappakdat[pak_flag.O].setpakdat(peak_heights[i],zscore,n-1,worst);
                      if (Suite::Extra())
                        packing_output.push_back("Peak #" + itos(i+1) + " has high tfz (but overlapped)");
                    }
                    else
                    {
                      peak_packing[i] = pak_flag.Z;
                      mappakdat[pak_flag.Z].setpakdat(peak_heights[i],zscore,n-1,worst);
                      if (Suite::Extra())
                        packing_output.push_back("Peak #" + itos(i+1) + " has high tfz (kept)");
                    }
                  }
                  else //user option not to keep
                  {
                    peak_packing[i] = pak_flag.X; //reiterate, same as if not handled differently
                    mappakdat[pak_flag.X].setpakdat(peak_heights[i],zscore,n-1,worst);
                    if (Suite::Extra())
                      packing_output.push_back("Peak #" + itos(i+1) + " has high tfz (not kept)");
                  }
                }
                fail_tra.push_back(TRA); //wrt in
                if (Suite::Extra())
                  packing_output.push_back("Append to Fail List: Peak #" + itos(i+1) + " Fail #" +itos(fail_tra.size())+nN);
                if (i == peak_sites.size()-1) //about to exit
                { //only possible if they also all have high tfz (paranoia)
                  if (Suite::Extra())
                    packing_output.push_back("Packing Fail: All peaks fail packing"+nN);
                  break; //because it doesn't matter
                }
                if (Suite::Extra())
                  logProgressBarNext(testing,2);
              }
              logProgressBarEnd(testing,2);
              if (Suite::Extra())
                logTabArray(where,packing_output);
              DAGDB.reset_work_pointer();
              DAGDB.reset_work_entry_pointers(DataBase());
            }
          }
          logEllipsisOpen(where,"Peak search and purge by percent");
          if (peak_selection_on_tfz)
          {
            trafun.convert_heights_to_zscores(peak_heights);
            stats.mst[n-2] = dvect3(trafun.f_mean,trafun.f_sigma,trafun.f_top); //map
          }
          if (Suite::Extra())
          {{
          out::stream where = out::TESTING;
          logTab(where,"Fast Translation Function (before percent cutoff with packing)"+nN);
          logTabArray(where,trafun.log_peaks(3,peak_heights,peak_sites,peak_packing));
          }}
          auto txt = trafun.apply_percent_cutoff_with_packing(peak_heights,peak_sites,io_percent,
                         peak_packing);
          trafun.continuous_shifts_and_store(peak_heights,peak_sites,turn.EULER,PT,peak_packing);
          logEllipsisShut(where);
          logTabArray(where,txt);
          logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
          if (Suite::Extra())
          {{
          out::stream where = out::TESTING;
          logTab(where,"Fast Translation Function (after continuous shifts)"+nN);
          logTabArray(where,trafun.log_peaks(3,peak_heights,peak_sites,peak_packing));
          }}
          logTab(where,"Number until purge: " + itos(num_peaks));
          logTab(where,"Number after purge: " + itos(trafun.translation_function_data.size()));
          if (fail_tra.size() == 0)
            logTab(where,"No peaks failed pack (flag="+pak_flag.packs_XZO_str()+") - top peaks pack or unknown (flag="+pak_flag.packs_YU_str()+")");
          else if (fail_tra.size() == 1)
            logTab(where,"Top 1 peak failed pack (flag="+pak_flag.packs_XZO_str()+")");
          else if (fail_tra.size() > 1)
            logTab(where,"Top " + itos(fail_tra.size()) + " peaks failed pack (flag="+pak_flag.packs_XZO_str()+")");
          if (Suite::Extra())
          {
            auto tp = trafun.top_packs(pak_flag.Y);
            logTab(out::TESTING,"Top packs: "  + btos(tp.first)+ " #" + itos(tp.second+1) + "/" + itos(trafun.translation_function_data.size()));
          }
          bool has_one_pak;int j;
          std::tie(has_one_pak,j) = trafun.top_packs(pak_flag.Y);
          if (io_use_packing and num_peaks and has_one_pak and j != 0)
          {
            trafun_top_packs = false;
          }
          }}

          if (trafun.count_sites() == 0)
          {
         // out::stream where = out::VERBOSE;
            logBlank(where);
         //No packing peaks
            logChevron(where,"No translation function peaks over cutoff"+nN);
            logTab(where,"Current Best LLG: LLG=" + dtos(trafun.tf_max()->f,2) +
                         " TFZ=" + dtos(trafun.tf_max()->z,2) + "/" + chtos(trafun.tf_max()->paks) + " " + bestnNf);
            logTab(where,"Current Best TFZ: LLG=" + dtos(trafun.tz_max()->f,2) +
                         " TFZ=" + dtos(trafun.tz_max()->z,2) + "/" + chtos(trafun.tz_max()->paks) + " " +  bestnNz);
            if (!logProgressBarNext(progress,1)) breakit = true;
            continue;
          }

          if (Suite::Extra())
          {
            out::stream where = out::TESTING;
            logHistogram(where,histogram,20,true,"Fast Translation Function Peak Heights (Z)",0,0,false);
          }

          {{
          if (trafun.tf_max()->newmax and trafun.tz_max()->newmax)
            bestnNf = bestnNz = nN;
          else if (trafun.tf_max()->newmax)
            bestnNf = nN;
          else if (trafun.tz_max()->newmax)
            bestnNz = nN;
          if (trafun.tf_max()->newmax and trafun.tz_max()->newmax)
            logProgressBarAgain(progress,"New Best LLG=" + dtos(trafun.tf_max()->f,2) +
                                " and TFZ=" + dtos(trafun.tz_max()->z,2) + "/" + chtos(trafun.tz_max()->paks) + " " +
                                " " + work->sg_info() +
                                nN,1);
          else if (trafun.tf_max()->newmax)
            logProgressBarAgain(progress,"New Best LLG=" + dtos(trafun.tf_max()->f,2) +
                                " (TFZ=" + dtos(trafun.tf_max()->z,2) + "/" + chtos(trafun.tf_max()->paks) + ") " +
                                " " + work->sg_info() +
                                nN,1);
          else if (trafun.tz_max()->newmax)
            logProgressBarAgain(progress,"New Best TFZ=" + dtos(trafun.tz_max()->z,2) +
                                 "/" + chtos(trafun.tz_max()->paks) + " (LLG=" + dtos(trafun.tz_max()->f,2) + ") " +
                                " " + work->sg_info() +
                                nN,1);
          }}

          {{
         // out::stream where = out::VERBOSE;
          logBlank(where);
          logTab(where,"Current Best LLG: LLG=" + dtos(trafun.tf_max()->f,2) +
                       " (TFZ=" + dtos(trafun.tf_max()->z,2) + "/" + chtos(trafun.tf_max()->paks) + ") " +
                        bestnNf);
          logTab(where,"Current Best TFZ: LLG=" + dtos(trafun.tz_max()->f,2) +
                       " (TFZ=" + dtos(trafun.tz_max()->z,2) + "/" + chtos(trafun.tz_max()->paks) + ") " +
                        bestnNz);
          }}

          // trafun.full_sort(); //search puts them in order
          trafun.clustering_off(); //reset all to top

          {{
          //out::stream where = out::VERBOSE;
          bool AllSites(false);
          int phaser_nprint(3);
          logTableTop(where,"Fast Translation Function (before clustering)"+nN,false);
          logTab(where,("Background Poses: " + itos(DAGDB.NODES.number_of_poses())));
          //item.grid is wrt in
          logTabArray(where,trafun.logFastTable(phaser_nprint,AllSites));
          logTableEnd(where);
          }}

          if (input.value__boolean.at(".phasertng.fast_translation_function.write_maps.").value_or_default())
          {
            int nn = n-1; //because n is incremented
            std::string cc = ntos(nn,DAGDB.size());
            FileSystem mapout(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(cc+".translation_function.map"));
            logFileWritten(out::LOGFILE,WriteFiles(),"FTF Map",mapout);
            if (WriteFiles()) trafun.WriteMap(mapout);
          }

          {{
         // out::stream where = out::VERBOSE;
          if (!io_report_cluster)
          {
            logBlank(where);
            logTab(where,"No clustering, speed enhancement");
            logBlank(where);
          }
          else
          {
            logEllipsisOpen(where,"Cluster fast translation function search peaks");
            //if the sampling is not inflated, then each point has no neighbours!!!
            trafun.NTHREADS = NTHREADS;
            trafun.USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS;
            int cluster_back = input.value__int_number.at(".phasertng.fast_translation_function.cluster_back.").value_or_default();
            trafun.cluster(cluster_distance,cluster_back);
            logEllipsisShut(where);
            if (io_use_cluster)
            {
              {{
              logEllipsisOpen(where,"Purge by cluster");
              int before = trafun.count_sites();
              trafun.select_topsites_and_erase();
              logEllipsisShut(where);
              logTab(where,"Number until purge: " + itos(before));
              logTab(where,"Number after purge: " + itos(trafun.count_sites()));
              }}

              {{
              bool AllSites(false);
              int phaser_nprint(3);
              logTableTop(where,"Fast Translation Function (after clustering)"+nN,false);
              //item.grid is wrt in
              logTabArray(where,trafun.logFastTable(phaser_nprint,AllSites));
              logTableEnd(where);
              }}
            }
            else
            {
              logBlank(where);
              logTab(where,"Clustering performed but no clustering purge");
              logBlank(where);
            }
          }
          }}

          {{
          int before = trafun.count_sites();
          if (before > io_maxstored)
          {
           // out::stream where = out::VERBOSE;
            logEllipsisOpen(where,"Purge by maximum number");
            trafun.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
            logEllipsisShut(where);
            if (io_maxstored == 0)
            logTab(where,"Maximum stored: all");
            else
            logTab(where,"Maximum stored:     " + itos(io_maxstored));
            logTab(where,"Number until purge: " + itos(before));
            logTab(where,"Number after purge: " + itos(trafun.count_sites()));
            double minperc = std::numeric_limits<double>::max();
            for (auto& item : trafun.translation_function_data)
            {
              double perc = 100.*(item.value-trafun.f_mean)/(trafun.tf_max()->f-trafun.f_mean);
              minperc = std::min(minperc,perc);
            }
            logTab(where,"Minimum percent after purge:  " + dtos(minperc,2) + "%");
            logProgressBarAgain(progress,"Purge by number=" + itos(io_maxstored) +
                                ": FTF>" + dtos(minperc,2) + "%",1);
          }
          }}

          {{
          Loggraph loggraph;
          loggraph.title = "Top peaks in Fast Translation Function "+nN ;
          loggraph.scatter = true;
          loggraph.graph.resize(1);
          loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
          int i(1);
          for (auto& item : trafun.translation_function_data)
          {
            loggraph.data_text += itos(i++) + " " + dtos(item.value,10,2) + "\n";
          }
          loggraph_list[n] = loggraph; //n for sorting only
          //logGraph(where,loggraph);
          }}

          {{
         // out::stream where = out::VERBOSE;
          logEllipsisOpen(where,"Append to DAG");
          //add the growing list to the DAG, note that this will not be sorted overall
          DAGDB.WORK->convert_turn_to_curl();
          DAGDB.reset_work_entry_pointers(DataBase());
          int i(0);
          for (auto& item : trafun.translation_function_data)
          {
            //no selection for top peaks, recore them all
            dag::Node next = *DAGDB.WORK; //deep copy so that the dag node is replicated
            //current values are gyre values
            next.generic_str = dtos(next.LLG,0)+"/"+dtos(next.PERCENT,0)+"%/"+dtos(next.ZSCORE,0)+"z"; //frf
            double val = item.value;
            double perc = 100.*(val-trafun.f_mean)/(trafun.tf_max()->f-trafun.f_mean);
            double zsr = (val - trafun.f_mean)/trafun.f_sigma;
            //std::string annotation = dtoi(val)+"/"+dtoi(perc)+"%/"+dtos(std::floor(zsr),0)+"z";
            std::string annotation = dtoi(perc)+"%/"+dtos(std::floor(zsr),0)+"z";
            std::string peak = "";
            if (io_report_cluster)
            { //there will not be a purge of the clustering, so clusterNum is still valid in the list
              double pkval = trafun.translation_function_data[item.clusterNum].value;
              double pkperc = 100.*(pkval-trafun.f_mean)/(trafun.tf_max()->f-trafun.f_mean);
              double pkzsr = (pkval - trafun.f_mean)/trafun.f_sigma;
              PHASER_ASSERT(val <= pkval);
              if (val < pkval)
                peak = "^"+dtoi(pkval)+"/"+dtoi(pkperc)+"%/"+dtos(pkzsr,1)+"Z";
            }
            next.LLG = val; //fast search score FSS
            next.PERCENT = perc; //fast search score FSS
            next.FSS = 'Y'; //fast search score FSS
            next.PACKS = io_use_packing ? item.packs : pak_flag.unset();
            //store the packing information for mode ftfr top_llg_packs, but don't use for mode pak
            //mode pak reuse would require storing special position information etc, which is too complicated
            mappakdat[item.packs].count++;
            next.ZSCORE = zsr;
            if (next.PACKS == pak_flag.Y and zsr > besttfzpaks)
            {
              hasbest = true;
              besttfzpaks = zsr;
              bestparent = next.PARENTS[0];
            }
            next.generic_float = perc;
            //recreate the annotation for information
            next.ANNOTATION += " TF="+annotation+peak+"/"+pak_flag.packs_Y_str();
            next.NUM = n-1;
            dag::Next& curl = next.NEXT;
            const dvect3 PT = turn.ENTRY->PRINCIPAL_TRANSLATION;
            const dmat33 PR = turn.ENTRY->PRINCIPAL_ORIENTATION;
            auto fract = dag::fract_wrt_mt_from_in(item.grid,turn.EULER,PT,next.CELL);
            //when there is a gyre present, the identifier for the curl will be the identifier for the turn
            //if gyres are present, and we are using the gyre information, then we need to ignore curl identifier
            auto shift_ortht = orthmat*item.grid; //external set
            next.convert_turn_to_curl(fract,shift_ortht,zsr); //internal set
            output_dag.push_back(next); //pointer to last in list
            i++;
          }
          logEllipsisShut(where);
          }}
          if (!logProgressBarNext(progress,1)) breakit = true;
        } //dagdb
      }
      logProgressBarEnd(progress,1);

      for (const auto& loggraph : loggraph_list)
      {
        out::stream where = out::VERBOSE;
        logGraph(where,loggraph.second);
      }

      if (!trafun_top_packs)
        logAdvisory("Top peak in (at least one) fast translation function did not pack");

      logUnderLine(out::LOGFILE,"Merged Results");
      if (stats.size() > 1) //e.g. not tf1
      {
        out::stream where = out::VERBOSE;
        stats.calculate_histogram_mean();
        logHistogram(where,stats.histdat,10,false,"TF means",stats.histmin,stats.histmax);
        stats.calculate_histogram_sigma();
        logHistogram(where,stats.histdat,10,false,"TF sigmas",stats.histmin,stats.histmax);
      }

      auto orig_output_dag = output_dag;
      double cut(-999);
      {{
      out::stream where = out::LOGFILE;
      double lowest_mean = stats.merged_lowest_mean();
      double average_mean = stats.merged_average_mean();
      //we purge using the lowest mean so as not to miss any
      //we report with the average mean as this is more realistic
      double top = std::numeric_limits<double>::lowest();
      for (auto& item : output_dag)
      {
        if (!io_use_packing or pak_flag.packs_YU(item.PACKS))
          top = std::max(top,item.LLG);
      }
      logTab(where,"Top-All/Lowest-Mean/Average-Mean: " + dtos(top,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(average_mean,2));
      logEllipsisOpen(where,"Purge by merged percent");
      int before = output_dag.size();
      output_dag.apply_partial_sort_and_percent_llg(io_percent,top,lowest_mean);
      logEllipsisShut(where);
      logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
      cut = (io_percent/100.)*(top-lowest_mean)+lowest_mean;
      logTab(where,"Cutoff FSS: " + dtos(cut,2));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(output_dag.size()));
      double minperc = std::numeric_limits<double>::max();
      for (auto& node : output_dag)
        minperc = std::min(minperc,node.generic_float);
      logTab(where,"FSS percent after purge: " + dtos(minperc,2) + "%");
      double minperc2 = std::numeric_limits<double>::max();
      double minperc3 = std::numeric_limits<double>::max();
      for (auto& item : output_dag)
      { //reset the generic float using the merged mean and top
        item.generic_float2 = item.generic_float;
        item.generic_float = 100*(item.LLG-lowest_mean)/(top-lowest_mean);
        double perc2 = 100*(item.LLG-average_mean)/(top-average_mean);
        double perc3 = item.generic_float;
        minperc2 = std::min(minperc2,perc2);
        minperc3 = std::min(minperc3,perc3);
      }
      logTab(out::VERBOSE,"Merged av-mean percent after purge: " + dtos(minperc2,2) + "%");
      logTab(out::VERBOSE,"Merged lo-mean percent after purge: " + dtos(minperc3,2) + "%");
      }}

      if (io_keep_hightfz)
      {
        out::stream where = out::LOGFILE;
        int before = output_dag.size();
        logEllipsisOpen(where,"Rescue high tfz");
        int count(0);
        for (int i = 0; i < orig_output_dag.size(); i++)//with reference to the original
        {
          dag::Node node = orig_output_dag[i];
          if (node.LLG < cut and node.ZSCORE > io_hightfz)
          {
            output_dag.push_back(node);
            count++;
          }
        }
        logEllipsisShut(where);
        logTab(where,"TFZ cutoff: " + dtos(io_hightfz,5,1));
        logTab(where,"Number High TFZ Rescued:    "  + itos(count));
        logTab(where,"Number until purge: " + itos(before));
        logTab(where,"Number after purge: " + itos(output_dag.size()));
        logBlank(where);
      }
    }
    }} //P1 or not P1

    logUnderLine(out::LOGFILE,"Final Results");

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by maximum stored");
    int before = output_dag.size();
    output_dag.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
    logEllipsisShut(where);
    if (io_maxstored == 0)
    logTab(where,"Maximum stored: all");
    else
    logTab(where,"Maximum stored:     " + itos(io_maxstored));
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(output_dag.size()));
      double minperc2 = std::numeric_limits<double>::max();
    double minperc = std::numeric_limits<double>::max();
    for (auto& node : output_dag)
      minperc2 = std::min(minperc2,node.generic_float2);
    for (auto& node : output_dag)
      minperc = std::min(minperc,node.generic_float);
    logTab(where,"FSS percent after purge:  " + dtos(minperc2,2) + "%");
    logTab(out::VERBOSE,"Merged lo-mean percent after purge:  " + dtos(minperc,2) + "%");
    }}

    {{
    out::stream where = out::LOGFILE;
    Loggraph loggraph;
    loggraph.title = "Top peaks in Fast Translation Function";
    loggraph.scatter = true;
    loggraph.graph.resize(1);
    loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
    int i(1);
    for (auto& item : output_dag)
    {
      loggraph.data_text += itos(i++) + " " + dtos(item.LLG,10,2) + "\n";
    }
    logGraph(where,loggraph);
    }}

    //just replace the node list
    DAGDB.NODES = output_dag;
    DAGDB.reset_entry_pointers(DataBase());
    DAGDB.shift_tracker(hashing(),input.running_mode);

    bool at_least_one_does_not_pack(false);
    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      at_least_one_does_not_pack = at_least_one_does_not_pack or (io_use_packing and pak_flag.packs_XZO(node.PACKS));
      at_least_one_does_not_pack = true; //because we always want the hightfz column in the results
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    int w = std::max(2,itow(io_nprint));
    size_t maxwstr = std::string("RF/GYRE").size();
    for (int i = 0; i < DAGDB.NODES.size(); i++)
      maxwstr = std::max(maxwstr,DAGDB.NODES[i].generic_str.size());
    out::stream where = out::LOGFILE;
    logTableTop(where,"Fast Translation Function Results");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using llg here
    int q = DAGDB.NODES.llg_width(false); //even if it is a map, we are using llg here
    int cellw = REFLECTIONS->UC.width();
    if (DAGDB.size())
    {
      logTab(where,P);
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %*s %s  %s  %s %*s %5s %c %7s %-13s %*s %*s",
          w,"#",
          maxwstr,"RF/GYRE",
          dag::header_polar().c_str(),
          dag::header_fract().c_str(),
          dag::header_ortht(cellw).c_str(),
          q,"FSS",
          "FSS%",'P',"TFZ","Space Group",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      dmat33 fracmat = node->CELL.fractionalization_matrix();
      dag::Next& curl = node->NEXT;
      logTab(where,snprintftos(
        "%-*d %*s %s  %s  %s % *.*f %5.1f %c %7.2f %-13s %*s %*s",
        w,i,
        maxwstr,node->generic_str.c_str(),
        dag::print_polar(curl.SHIFT_MATRIX).c_str(),
        dag::print_fract(fracmat*curl.SHIFT_ORTHT).c_str(),
        dag::print_ortht(curl.SHIFT_ORTHT,cellw).c_str(),
        q,p,node->LLG,
        node->generic_float, //100% for first in p1, otherwise value from tf
        node->PACKS,
        node->ZSCORE,
        node->SG->sstr().c_str(),
        unique_search ? 0:uuid_w,unique_search ? "":curl.IDENTIFIER.string().c_str(),
        unique_search ? 0:uuid_w,unique_search ? "":node->REFLID.string().c_str()
        ));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
      logTab(where,snprintftos(
        "%*s %5s EULER=[% 6.1f % 6.1f % 6.1f] FRACT=[% 7.4f % 7.4f % 7.4f]",
        w,"","",curl.EULER[0],curl.EULER[1],curl.EULER[2],
        curl.FRACT[0],curl.FRACT[1],curl.FRACT[2]));
      logTab(where,snprintftos(
        "%*s %5s ORTHR={%+6.3f %+6.3f %+6.3f}",
        w,"", "",curl.ORTHR[0],curl.ORTHR[1],curl.ORTHR[2]));
#endif
      logTab(where,"---");
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Fast Translation Function Summary");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    DAGDB.restart(); //very important to call this to set WORK
    logTab(where,"Last placed: " + DAGDB.WORK->NEXT.IDENTIFIER.str());
    logTab(where,"Percent cutoff: " + dtos(io_percent,5,1) + "%");
    logTab(where,P);
    std::set<std::string> ensembles;
    for (auto& node : DAGDB.NODES)
      ensembles.insert(node.NEXT.IDENTIFIER.str());
    int j(1);
    for (auto ens : ensembles)
      logTab(where,"Model #" + itos(j++) + " " +  ens);
    int p = DAGDB.NODES.llg_precision(false); //stored in llg
    int minw = std::string("HighTfz").size();
    int q = std::max(minw,DAGDB.NODES.llg_width(false));
    int z = DAGDB.size();
    int ww = itow(nstart,3);
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"There " + std::string(numtfs==1?"is ":"are ") + itos(numtfs) + " translation function" + std::string(numtfs==1?"":"s"));
    if (DAGDB.size())
    {
      std::string line1 = snprintftos("%-*s   ",ww,"TF");
      sv_string header = { "Top----", "Second-", "Third--" };
      if (at_least_one_does_not_pack)
        header = { "HighTfz", "Top----","Second-", "Third--" };
      for (auto item : header)
         line1 += snprintftos("%*s-%5s-%5s-%c ",q,item.c_str(),"-----","-----",'-');
      logTab(where,line1);
      std::string line2 = snprintftos("%-*s   ",ww,"#");
      for (int j = 0; j < header.size(); j++)
        line2 += snprintftos("%*s %5s %5s %c ",q,"(LLG)","(%)","(Z)",'P');
      line2 += snprintftos("%-13s  %*s %*s","Space Group",
                          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
                          unique_search ? 0:uuid_w,unique_search ? "":"reflid");
      logTab(where,line2);
    }
    int last_interesting_line(0);
    af_string tabletxt;
    for (int itf = 0; itf < numtfs; itf++) //original size of DAGDB
    {
      int j(0);
      std::string line = snprintftos("%-*d   ",ww,itf+1);
      Identifier reflid,modlid;
      bool first(at_least_one_does_not_pack);
      std::string tfsg = "";
      for (auto& node : DAGDB.NODES)
      {
        if (node.NUM == itf+1)
        {
          tfsg = node.SG->sstr();
          reflid = node.REFLID; //all the reflid for same itf will be the same
          modlid = node.NEXT.IDENTIFIER; //all the reflid for same itf will be the same
          double perc(node.generic_float > 1000 ? 999.9 : node.generic_float);//see above
          bool not_packing = !io_use_packing or (io_use_packing and pak_flag.packs_XZO(node.PACKS));
          int zw = (node.ZSCORE < 100) ? 2 : 1;
          if (first and not_packing)
          { //put the clashing one out first in the row
            line += snprintftos("%*.*f %5.1f %5.*f %c ",q,p,node.LLG,perc,zw,node.ZSCORE,node.PACKS);
            //second one must pack
            //this one is not included in the j count
          }
          else if (first and !not_packing)
          {
            line += snprintftos("%*s %5s %5s %c ",q,"---","---","---",'-');
            line += snprintftos("%*.*f %5.1f %5.*f %c ",q,p,node.LLG,perc,zw,node.ZSCORE,node.PACKS);
            j++; //count the number that are in this TF#
          }
          else if (!not_packing)//second and subsequent
          {
            line += snprintftos("%*.*f %5.1f %5.*f %c ",q,p,node.LLG,perc,zw,node.ZSCORE,node.PACKS);
            j++; //count the number that are in this TF#
          }
          first = false;
          if (j==3) break;
        }
      }
      if (!first or j > 0) last_interesting_line = itf;
      if (first) line += snprintftos("%*s %5s %5s %c ",q,"---","---","---",'-');
      if (j<1)   line += snprintftos("%*s %5s %5s %c ",q,"---","---","---",'-');
      if (j<2)   line += snprintftos("%*s %5s %5s %c ",q,"---","---","---",'-');
      if (j<3)   line += snprintftos("%*s %5s %5s %c ",q,"---","---","---",'-');
      line += snprintftos("%-13s  %*s %*s",
                  tfsg.c_str(),
                  unique_search ? 0:uuid_w,unique_search ? "":modlid.string().c_str(),
                  unique_search ? 0:uuid_w,unique_search ? "":reflid.string().c_str());
      tabletxt.push_back(line);
    }
    int t1 = tabletxt.size();
    tabletxt.erase(tabletxt.begin()+last_interesting_line+1,tabletxt.end()); //remove the masses of boring lines at the end
    int t2 = tabletxt.size();
    if (t2 < t1)
      tabletxt.push_back("--- etc " + itos(t1-t2) + " more empty lines");
    logTabArray(where,tabletxt);
    logTableEnd(where);
    }}

    //convert the packing flag to Y/N/?
    std::map<char,int> newcounts;
    for (auto item : pak_flag.info) newcounts[item.first] = 0;
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      if (DAGDB.WORK->PACKS == pak_flag.unset()) //leftover from init
        DAGDB.WORK->PACKS = pak_flag.U;
      newcounts[DAGDB.WORK->PACKS]++;
    }

    if (io_use_packing)
    {
    out::stream where = out::SUMMARY;
    logTableTop(where,"Fast Translation Function Packing Summary");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"Includes information about peaks excluded from peak list");
    if (io_keep_hightfz)
    logTab(where,"Low/High TFZ = " + dtos(io_hightfz,2));
    logTab(where,P);
    double maxfss = std::numeric_limits<double>::lowest();
    for (auto& item : mappakdat)
    {
      pod::pakdat& dat = item.second;
      maxfss = std::max(maxfss,dat.fssset ? dat.fssmax : maxfss);
      maxfss = std::max(maxfss,dat.tfzset ? dat.tfzmaxfss : maxfss);
    }
    phaser_assert(mappakdat.size());
    int minw = std::string("Max-FSS").size();
    int w = std::max(minw,DAGDB.NODES.llg_width(false)); //even if it is a map, we are using llg here
    logTab(where,snprintftos("%-*s %6s %6s | %*s %*s %7s %5s | %*s %*s %7s %5s |",
        h,"Packing","#all","#out",
        w,"Max-FSS",w+2,"=(TFZ)","Clash","TF#",
        w,"Max-TFZ",w+2,"=(FSS)","Clash","TF#"));
    for (auto item : pak_flag.info)
    {
      auto& dat = mappakdat.at(item.first);
      std::string fssstr =  dat.fssset ? dtos(dat.fssmax,2) : "--n/a--";
      std::string tfzstr =  dat.tfzset ? dtos(dat.tfzmax,2) : "--n/a--";
      std::string fssstrtfz =  dat.fssset ? "("+dtos(dat.fssmaxtfz,2)+")" : "--n/a--";
      std::string tfzstrfss =  dat.tfzset ? "("+dtos(dat.tfzmaxfss,2)+")" : "--n/a--";
      std::string fssstrclash =  dat.fssset and (dat.fssmaxclash >= 0) ? dtos(dat.fssmaxclash,1)+"%" : "--n/a--";
      std::string tfzstrclash =  dat.tfzset  and (dat.tfzmaxclash >= 0) ? dtos(dat.tfzmaxclash,1)+"%" : "--n/a--";
      logTab(where,snprintftos("%-*s %6s %6s | %*s %*s %7s %5s | %*s %*s %7s %5s |",
          h,map_flag.at(item.first).c_str(),
          (dat.count > 999999) ? ">1.0e7": itos(dat.count).c_str(),
          (newcounts[item.first] > 999999) ? ">1.0e7": itos(newcounts[item.first]).c_str(),
          w,fssstr.c_str(),
          w+2,fssstrtfz.c_str(),
          fssstrclash.c_str(),
          dat.fssset ? itos(dat.fssnum).c_str(): "",
          w,tfzstr.c_str(),
          w+2,tfzstrfss.c_str(),
          tfzstrclash.c_str(),
          dat.tfzset ? itos(dat.tfznum).c_str(): ""
        ));
    }
    logTableEnd(where);
    }

    {{
    out::stream where = out::TESTING;
    if (Suite::Store() >= where)
    {
      int n(1),N(DAGDB.size());
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        if (n > 1) break;
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Fast Translation Function Node" +nN);
        logTabArray(where,DAGDB.WORK->logNode(nN+""));
      }
    }
    }}
  }

}//phasertng
