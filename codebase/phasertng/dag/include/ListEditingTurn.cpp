//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/dag/DuplicateData.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/euler_angles.h>
#include <future>

namespace phasertng {
namespace dag {

  //ListEditing
  int
  DagDatabase::calculate_duplicate_turns(
      double dmin,
      bool has_tncs,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = size();
    // Compare solutions to find duplicates further down list
    for (int k = 0; k < NODES.size(); k++)
    {
      auto& item = NODES[k];
      item.generic_flag = true;
      item.EQUIV = k;
      item.generic_int = 0; //top peak, symmetry as is
    }

    HallDuplicateData map_data2;
    bool is_atom(false);
    for (int k = 0; k < tsize; k++)
    {
      dag::Node& node_k = NODES[k];
      if (!map_data2.count(node_k.HALL))
        map_data2[node_k.HALL] = dag::DuplicateData(node_k,false); //AJM to do pose for single atom?
    }

    for (int k = 0; k < tsize; k++)
    {
      duplicate_turns_combine(map_data2,dmin,has_tncs,k,USE_STRICTLY_NTHREADS,NTHREADS);
    }

    //after paralled section, when all KEEP is flagged
    for (int k = 0; k < NODES.size(); k++)
      NODES[k].NUM = -999;
    //first pass for known
    int na(1);
    for (int k = 0; k < NODES.size(); k++)
      if (NODES[k].generic_flag)
        NODES[k].NUM = na++; //output number equivalent
    bool all_numbered(true);
    int count_loop(0);
    do
    {
      all_numbered = true;
      for (int k = 0; k < NODES.size(); k++)
        if (!NODES[k].generic_flag) //if this is equivalent to something
          NODES[k].NUM = NODES[NODES[k].EQUIV].NUM; //may be negative
      for (int k = 0; k < NODES.size(); k++)
        if (NODES[k].NUM < 0)
          all_numbered = false; //flag that we are not yet done
      PHASER_ASSERT(count_loop++ < 100); //infinite loop check
    }
    while (!all_numbered);

    int count(0);
    for (int k = 0; k < NODES.size(); k++)
    {
      if (!NODES[k].generic_flag) count++;
    }
    return count;
  }

  void
  DagDatabase::duplicate_turns_combine(
      HallDuplicateData map_data2,
      double dmin,
      bool has_tncs,
      int k,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = NODES.size();
    int nchk = tsize - k;
    int minimum_nchk_for_parallel(1000); //must be this to make it worthwhile AJM TODO run tests
    if (NTHREADS == 1)
    {
      int beg = k+1;
      int end = tsize;
      duplicate_turns_parallel(map_data2,dmin,has_tncs,k,beg,end);
    }
    else if (nchk < minimum_nchk_for_parallel)// because the granularity is too small
    {
      int beg = k+1;
      int end = tsize;
      duplicate_turns_parallel(map_data2,dmin,has_tncs,k,beg,end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(HallDuplicateData,double,bool,int,int,int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      int grain = (nchk/NTHREADS)+1; //granularity of threading, +1 makes the last thread the shortest
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = k+1 + t*grain; //start of thread reflection
        int end = std::min(k+1 + (t+1)*grain,tsize)-1; //end of thread
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind( &DagDatabase::duplicate_turns_parallel, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              map_data2,
              dmin,
              has_tncs,
              k,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &DagDatabase::duplicate_turns_parallel, this,
              map_data2,
              dmin,
              has_tncs,
              k,
              beg,
              end  ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
       int beg = k+1 + nthreads_1*grain; //start of thread reflection
       int end = std::min(k+1 + (nthreads_1+1)*grain,tsize)-1; //end of thread
       duplicate_turns_parallel(
              map_data2,
              dmin,
              has_tncs,
              k,
              beg,
              end  );
       //rot_prev will remain -999 if nothing was found
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
      }
    }
  }

  void
  DagDatabase::duplicate_turns_parallel(
      HallDuplicateData map_data2,
      double dmin,
      bool has_tncs,
      int k,
      int beg,int end)
  {
    dag::Node& node_k = NODES[k];
    DuplicateData& data2 = map_data2[node_k.HALL];
    int nmodels = 1;
    Identifier comp1;
    comp1 = node_k.NEXT.IDENTIFIER;
    double maprotMax;
    maprotMax = 2.0*std::atan(dmin/(4.*node_k.NEXT.ENTRY->MEAN_RADIUS));
    for (int k_chk = beg; k_chk < end; k_chk++) //check those further down the list
    {
      dag::Node& node_k_chk = NODES[k_chk];
      if (node_k.PARENTS != node_k_chk.PARENTS) continue;
      if (!node_k_chk.generic_flag) continue;
      // skip comparison if different space groups
      bool different_space_group(node_k_chk.HALL != node_k.HALL);
      if (different_space_group) continue;
      // skip comparison if different composition
      Identifier comp2;
      comp2 = node_k_chk.NEXT.IDENTIFIER;
      if (comp1 != comp2) continue;
      auto& gyre = node_k.NEXT;
      auto& gyre_chk = node_k_chk.NEXT;
      bool matched(false);
      if (gyre.IDENTIFIER == gyre_chk.IDENTIFIER)
      {
        double rotMax = maprotMax;
        dmat33 solR_first = gyre.finalR();
        dmat33 solR_chk = gyre_chk.finalR();
        sv_dvect3 get_euler = gyre.ENTRY->POINT_GROUP_EULER; //will be the same
        int multiplicity(get_euler.size());
        int nsymp = has_tncs ? 1 : data2.SG.NSYMP;
        for (int isym = 0; isym < nsymp and !matched; isym++)
        {
          UnitCell uc(node_k.CELL);
          dmat33 solR_chk_sym = uc.orth_Rtr_frac(data2.SG.rotsym[isym])*solR_chk;
          for (int ensym = 0; ensym < multiplicity and !matched; ensym++)
          {
            dmat33 ensymR = zyz_matrix(get_euler[ensym]);
            dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
            double rotdist = matrix_delta_rad(solR_first,solR_chk_sym_ensym);
            if (rotdist < rotMax)
            {
              node_k_chk.generic_int = isym;
              matched = true;
            }
          } // ensym
        } // isym
      }
      if (matched)
      {
        node_k_chk.generic_flag = false;
        node_k_chk.EQUIV = k;
        //replace the kept TFZ with the TFZ highest from those found to be equivalent
        node_k.ZSCORE = std::max(node_k_chk.ZSCORE,node_k.ZSCORE);
      }
    } //k_chk
  }

}}
