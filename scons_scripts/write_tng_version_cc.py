from __future__ import print_function
from __future__ import division

import libtbx.load_env
import time
import sys, os, os.path
import subprocess, re

#this file has to be in the same directory as SConscript

def write_tng_version_cc(target, source, env):
  #dict = env_base.Dictionary()
  sys.stdout.write( "phasertng: Write version file " + env.version_cc)
  d = os.path.dirname(env.version_cc)
  if (not os.path.exists(d)):
    os.mkdir(d)
  # if git repo is present ...
  gitrevnumber = "" ###1
  githash = "" ###2
  gittotalcommits = "" ###3
  gitshorthash = "" ###4
  gitdatetime = "" ###5
  gitbranch = "" ###6
  gittagname = "" ###7
  try:
    # get hash, short hash and author timestamp of current revision in this branch
    timeargs = ["git", "rev-list", "HEAD", "-n", "1", "--pretty=tformat:%h %ad", "--date=iso"]
    process = subprocess.Popen(timeargs, cwd=env.phasertng_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (out, err) = process.communicate()
    out = out.decode(encoding='utf-8', errors='strict') # for python3
    #sys.stdout.write( out+ "  " + err+"\n")
    if process.returncode == 0:
      s = out.split()
      githash = s[1]   ###2
      gitshorthash = s[2]   ###4
      gitdatetime = " ".join(s[3:6])   ###5
      # Construct the current revision number as the total count of revisions in this branch.
      # Assuming the same repo is used with always the same branch this number should be sequential.
      revargs = ["git", "rev-list", "--count", "HEAD"]
      process = subprocess.Popen(revargs, cwd=env.phasertng_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict') # for python3
      gitrevnumber = out.split()[0]   ###1
      # get total number of all commits in all branches
      revargs = ["git", "rev-list", "--count", "--all", "HEAD"]
      process = subprocess.Popen(revargs, cwd=env.phasertng_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict') # for python3
      gittotalcommits = out.split()[0]   ###3
      # get name of current branch
      branchargs = ["git", "rev-parse", "--abbrev-ref", "HEAD"]
      process = subprocess.Popen(branchargs, cwd=env.phasertng_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict') # for python3
      gitbranch = out.split()[0] ###6
      gittagname = 'gitrev_' + str(gitrevnumber) ###7
      sys.stdout.write( "\nGit commit hash:"+ githash+"\n")
      sys.stdout.write( "Git commit date:"+ gitdatetime+"\n")
      sys.stdout.write( "Current branch:"+ gitbranch+"\n")
      sys.stdout.write( "Number of commits of current branch: " +  gitrevnumber+"\n")
      sys.stdout.write( "Total number of commits of all branches: " + gittotalcommits+"\n")
  except OSError as e:
    gitrevnumber = ""

  program= open(str(env.phasertng_path + "/PROGRAM"), "r")
  programstr = program.read().replace("\n","")
  program.close()
  version = open(str(env.phasertng_path + "/VERSION"), "r")
  versionstr = version.read().replace("\n","")
  version.close()
  if versionstr == "":
    versionstr = "0.0.0"
  fullversion = programstr + "-" + versionstr

  changelog = open(str(env.phasertng_path + "/CHANGELOG"), "r")
  changelogstr = changelog.read()
  changelogstr = changelogstr.replace('\n', '\\n').replace('\r', '\\n')
  changelog.close()
 # ddmmyy = time.strftime("%d/%m/%Y")[:6] + time.strftime("%d/%m/%Y")[8:10]
  yyyymmdd = time.strftime("%Y/%m/%d")

  versionccstr = """
// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS
// EDITS WILL BE OVERWRITTEN!
#include <phasertng/main/Version.h>

namespace phasertng {

  std::string Version::version_date()       { return \"%s\"; }
  std::string Version::yyyymmdd()           { return \"%s\"; }
  std::string Version::program_name()       { return \"%s\"; }
  std::string Version::version_number()     { return \"%s\"; }
  std::string Version::full_name()          { return \"%s\"; }
  std::string Version::change_log()         { return \"\\n%s\"; }
  std::string Version::git_revision()       { return \"%s\"; }
  scitbx::af::shared<std::string> Version::git_info()
  {
    std::string gitshorthash =     \"%s\";
    std::string gitrevnumber =     \"%s\";
    std::string gittotalcommits =  \"%s\";
    std::string gittagname =       \"%s\";
    std::string githash =          \"%s\";
    std::string gitdatetime =      \"%s\";
    std::string gitbranch =        \"%s\";
    scitbx::af::shared<std::string> gitinfo;
    if (gitrevnumber.size())
    {
      gitinfo.push_back("git revision: " + gitshorthash);
      gitinfo.push_back("git SHA-1: " + githash);
      gitinfo.push_back("git commit at: " + gitdatetime);
      gitinfo.push_back("git commits: " + gitrevnumber);
      gitinfo.push_back("git branch: " + gitbranch);
      gitinfo.push_back("git tag name: " + gittagname);
    }
    return gitinfo;
  }

} //phasertng
  """ %(
        time.asctime(time.localtime()),
        yyyymmdd,
        programstr,
        versionstr,
        fullversion,
        changelogstr,
        gitshorthash, #git_revision
        gitshorthash, #git_revision
        gitrevnumber,
        gittotalcommits,
        gittagname,
        githash,
        gitdatetime,
        gitbranch,
        )

  versionCCfile = open(env.version_cc, "w")
  versionCCfile.write(versionccstr)
  versionCCfile.flush() # attempting to circumvent strange race conditions that may cause access denied error
  os.fsync(versionCCfile.fileno())
  versionCCfile.close()
  del versionCCfile

# Now generate header file for the Windows resource file, phasertng.res, that holds version
# numbers which will be visible in Windows explorer
  # replace dots with commas
  fullversion = versionstr.replace('.',",")

  hstr = """
// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS
// EDITS WILL BE OVERWRITTEN!
// Version number convention on Windows is <major version>.<minor version>.<build number>.<revision>
// First 3 numbers are extracted from version.h and revision number from the git database.
// These numbers are used in phasertng.rc2 which in turn is included by phasertng.rc when building on Windows.
#define FILEVER          %s
#define PRODUCTVER       %s
#define STRFILEVER       \"%s\\0\"
#define STRPRODUCTVER    \"%s\\0\"
""" %(fullversion, fullversion, fullversion, fullversion)

# versresHfile = open(str(libtbx.env.under_build("phasertng/versionno.h")), "w")
  versresHfile = open(str(os.path.join(env.autogen,"versionno.h")), "w")
  versresHfile.write(hstr)
  versresHfile.flush()
  os.fsync(versresHfile.fileno())
  versresHfile.close()
  del versresHfile

  constructvar = '$SHCXXCOM'
  # Windows: embed version info as a resource into executable
  if env.compiler == "win32_cl":
    # Manually compose resource compiler command and then execute it immediately as to avoid delayed compilation
    rccomvar = env.subst('$RCCOM') + " " + \
      os.path.join(env.phasertng_path, "scons_scripts", "phasertng.rc")
    env.Execute(rccomvar)
  ofile = os.path.splitext(env.version_cc)[0] + env.objsufx
  # Hack to avoid recompiling version files unless phasertng exectutable will be linked:
  # Manually compose compiler command and then execute it immediately as to avoid delayed compilation
  compstr = env.subst(constructvar) + " " + env.version_cc
  # on Windows
  compstr = compstr.replace(" /Fo ", " /Fo" + ofile + " ")
  #on unix
  compstr = compstr.replace(" -o ", " -o" + ofile + " ")
  # Object() would compile the version file object after attempting to build phasertng executable.
  # A build of phasertng would then at best have included an outdated version file object.
  # By using Execute() for immediate compilation we ensure the compiled version file object is up to date.
  r = 1
  cnt = 0
  while r:
    time.sleep(1.0)
    r = env.Execute(compstr)
    if r:
      print("trying again")
      cnt += 1
    if cnt > 10:
      break
  if r: #
    print("Failed compiling", env.version_cc)
    Exit(r)
  time.sleep(1.0)

#---write Version.cc
