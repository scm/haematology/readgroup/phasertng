//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Identifier.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/jiffy.h>
#include <random>
#include <chrono>
#include <sstream>
//#include <cmtzlib.h>
//#include <mtzdata.h>
#include <boost/functional/hash.hpp>

//typedef std::chrono::seconds duration_t;
//typedef std::chrono::duration<int, std::centi> duration_t; //hundredth of a second
//typedef std::chrono::duration<int, std::deci> duration_t; //tenth of a second
typedef std::chrono::nanoseconds duration_t; //requires 64 bits

namespace phasertng {

  void Identifier::initialize_from_ullint(ullint ULLINT)
  {
    if (ULLINT < 0) //paranoia
      throw Error(err::INPUT,"identifier less than zero");
    if (ULLINT >= std::numeric_limits<uuid_t>::max()) //paranoia
      throw Error(err::INPUT,"identifier out of range for compiled source/operating system");
    number_value = static_cast<uuid_t>(ULLINT);
  }

  void Identifier::initialize_from_text(std::string eg_tag)
  {
    boost::hash<std::string> string_hash;
    std::size_t hash_value = string_hash(eg_tag); //return type for boost hash
    initialize_from_random(static_cast<ullint>(hash_value));
  }

  void Identifier::initialize_from_string(std::string ULLINTSTR)
  {
    if (!ULLINTSTR.size())
      throw Error(err::INPUT,"identifier string not set");
    if (ULLINTSTR[0] == '-')
      throw Error(err::INPUT,"identifier negative");
    for (int j = 0; j < ULLINTSTR.size(); j++)
      if (!isdigit(ULLINTSTR[j]))
        throw Error(err::INPUT,"identifier string is not a number");
    ullint convert;
    try {
      convert = std::stoull(ULLINTSTR);
    }
    catch (...)
    { throw Error(err::INPUT,"identifier string to number conversion failed, out of range"); }
    initialize_from_ullint(convert);
  }

  void Identifier::parse_string(std::string ULLINTSTR,std::string TAG)
  {
    initialize_from_string(ULLINTSTR);
    std::string tag_value = TAG.size() ? TAG : string();
    initialize_tag(tag_value);
  }

  void Identifier::initialize_from_random(ullint seed)
  {
    std::random_device rd;     //Get a random seed from the OS entropy device, or whatever
    std::mt19937_64 eng; //Use the 64-bit Mersenne Twister 19937 generator
    seed ? eng.seed(seed) : eng.seed(rd()); //seed it with entropy.
    //Define the distribution, by default it goes from 0 to MAX
    //disallow 0, and make sure it is interesting enough as a minimum
    ullint minval = std::pow(10, uuid_w-1); //first digit is non-zero
    //make sure there is always room for some incrementing with a buffer
    ullint maxval = std::numeric_limits<uuid_t>::max()-10000;
    std::uniform_int_distribution<uuid_t> distr(minval,maxval);
    //Generate random numbers
    number_value = distr(eng);
  }

  bool Identifier::is_valid(ullint ULLINT) const
  {
    //default is 0, not set
    return (ULLINT < std::numeric_limits<uuid_t>::max() and ULLINT != 0);
  }

  void Identifier::increment(const std::string TAG)
  {
    //default is 0, not set
    if (number_value < std::numeric_limits<uuid_t>::max()-1)
      number_value++;
    else //should throw error because if we have checked there is room this should not happen
         //however it is a back up option
      number_value = 1;
    initialize_from_ullint(number_value);
    initialize_tag(TAG);
  }

  std::string Identifier::str() const
  //{ return tag() + "[" + string() + "]"; }
  { return string() + "-" + tag(); }

  std::string Identifier::string() const
  {
    if (!is_set_valid()) return "---"; // For print, other checks fail
    //int wsize = uuid_w-1; //this is wrong (shorter than necessary for size_t, 9) but backwardly compat
    int wsize = uuid_w; //10 digit codes ok for size_t
    std::string buffer(wsize+1,'\0');
    int len = snprintf(&buffer[0], wsize+1, "%0*lld",wsize,
        static_cast<long long>(number_value)); // Using %lld for long long integers
    if (len < 0 || len > wsize+1) {
        // Handle error: snprintf failed or buffer was too small
        return "---";  // Or another error handling mechanism
    }
    buffer.resize(len);
    return buffer;
  }

  const uuid_t Identifier::identifier() const
  { return number_value; }

  std::string Identifier::stem() const
  //{ return tag() + "." + string(); }
  //{ return tag() + "[" + string() + "]"; } //bad idea
  { return string() + "-" + tag(); }

  void Identifier::unstem(const std::string text)
  { //for python graph format where index must be a string
    Identifier def;
    if (def.stem() == text) return;
    std::string ULLINTSTR,TAG;
    int j;
    for (j = 0; j < text.size(); j++)
    {
      if (isdigit(text[j]))
        ULLINTSTR += text[j];
      if (text[j] == '-') break; //same as above
    }
    for (int i = j+1; i < text.size(); i++)
      TAG += text[i];
    PHASER_ASSERT(ULLINTSTR.size());
    initialize_from_string(ULLINTSTR);
    PHASER_ASSERT(TAG.size());
    initialize_tag(TAG);
    PHASER_ASSERT(is_set_valid());
  }

  const std::string Identifier::tag() const
  {
    //increment separator must be alphanumeric or underscore
    //should not be a dot, because it is used for making directories
    return tag_value;
  }

  const std::string Identifier::tag_unparse() const
  {
    if (is_set_valid()) return "\"" + tag() + "\""; //stoq
    return "None"; //which should be the default tag anyway
  }

  const std::string Identifier::id_unparse() const
  {
    if (is_set_valid()) return string();
    return "None"; //which should be the default tag anyway
  }

  bool Identifier::initialize_tag(std::string TAG)
  {
    //tag must not look like a directory separator, because it is used in the filenames!
    //e.g. replace "\" "/" "." " " "\t" with _
    //TAG may be messed up by passage through phil
    if (!TAG.length()) return false;
    hoist::algorithm::trim(TAG); //trim whitespaces
    std::string unquoted_str; //trim the quotes on the ends
    bool quoted(TAG.length() >= 2 and TAG.front() == '"' and TAG.back() == '"');
    for (int i = 0; i < TAG.length(); i++)
      if (!(quoted and (i == 0 or i == TAG.length()-1)))
        unquoted_str += TAG[i];
    TAG = unquoted_str;
    hoist::algorithm::trim(TAG); //trim the whitespace inside the quotes
    for (int i = 0; i < TAG.length(); i++)
      if (!isalnum(TAG[i]) and TAG[i] != '-')
        TAG[i] = '_'; //also replaces underscore with underscore
    //don't change the tag because then it won't work as a filename //AJM??
    //for the process_predicted_model special case, remove the constants in the filenames
    if (hoist::algorithm::contains(TAG,"predicted_"))
    if (hoist::algorithm::contains(TAG,"cycle_"))
    if (hoist::algorithm::contains(TAG,"processed_"))
    {
    hoist::replace_last(TAG,"predicted_","");
    hoist::replace_last(TAG,"cycle_","");
    hoist::replace_last(TAG,"processed_","");
    }
    //make sure that the tag cannot exceed the mtz record length, even if for coordinates
    //since coordinates also have maps written from them!
    std::string leader(constants::remark_tag + constants::remark_identifier + string() + " ");
    std::string strhistory(leader + TAG);
    bool shortened = false;
    int len(4); // in case it is a year
    int MTZRECORDLENGTH(80);
    if (strhistory.size() > int(MTZRECORDLENGTH))
    { //first try shortening all the uuint identifiers in the string to first len numbers only
      std::string tag;
      int i(0);
      for (int j = 0; j < TAG.size(); j++)
      {
        if (isdigit(TAG[j]))
        {
          if (i < len) tag += TAG[j];
          i++;
        }
        else
        {
          i = 0;
          tag += TAG[j];
        }
      }
      TAG = tag;
      shortened = true;
    }
    //repeat analysis with shortened string
    strhistory = (leader + TAG);
    if (strhistory.size() > int(MTZRECORDLENGTH))
    { //desperation
      int len = int(MTZRECORDLENGTH)-leader.size()-string().size()-2;
      //by appending the string() of the identifier, we guarantee unique tag
      TAG = TAG.substr(0,len)+"_"+ string(); //this will be shorter, and unique
      std::string line(leader + TAG);
      PHASER_ASSERT(line.size() < int(MTZRECORDLENGTH));
      shortened = true;
    }
    tag_value = TAG;
    return shortened;
  }

  bool Identifier::is_set_valid() const
  //{ return is_valid(number_value); } //default is 0, not set
  { return (number_value != 0); }

  std::string
  Identifier::entry_data_stem_ext(entry::data text)
  {
    std::string ext = "." + stolower(entry::data2String(text));
    hoist::replace_all(ext,"_","."); //the entry::data underscore into a dot ext filetype
    //start all directories with the leading string of the name of the program in lower case
    return stem()+ext;
  }

  std::string
  Identifier::other_data_stem_ext(other::data text)
  {
    std::string ext = "." + stolower(other::data2String(text));
    hoist::replace_all(ext,"_","."); //the entry::data underscore into a dot ext filetype
    //start all directories with the leading string of the name of the program in lower case
    if (is_set_valid())
      return stem()+ext;
    return tag()+ext; //this is for the lookup, only return the tag-ext not ----tag-ext when id not set
  }

  std::string
  Identifier::database_subdir()
  { return stem(); }

  std::string
  Identifier::extra_file_stem_ext(std::string ext)
  {
//   if (ext[0] != '.') ext = "." + ext; //for when the stem must be derived from the identifier
    return (stem() + ext);
  }

  std::pair<std::string,std::string>
  Identifier::FileName(std::string ext)
  { return { database_subdir(), extra_file_stem_ext(ext) }; }

  std::pair<std::string,std::string>
  Identifier::FileEntry(entry::data dat)
  { return { database_subdir() , entry_data_stem_ext(dat) }; }

  std::pair<std::string,std::string>
  Identifier::FileOther(other::data dat)
  { return { database_subdir() , other_data_stem_ext(dat) }; }

  std::string
  Identifier::reflections_and_models()
  { return "reflections_and_models"; }

  std::pair<std::string,std::string>
  Identifier::FileDataBase(other::data dat)
  { return { reflections_and_models() , other_data_stem_ext(dat) }; }

  void
  Identifier::initialize_node_tag()
  { tag_value = "pathway"; }

  void
  Identifier::initialize(std::pair<uuid_t,std::string> id)
  { number_value = id.first; tag_value = id.second; }

}//phasertng
