import sys, os, os.path
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng.voyager.picard import *

#wrapper for picard.dryrun

def bones(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_bones)

def inner_bones(basetng,user_phil_str,progress_callback=None):
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="picard")
    autoparams = initparams + "\n" + build_auto_str("picard")
    master_auto = parse(autoparams,converter_registry=converter_registry)
    #fetch_diff important to only get the differences from the defaults, otherwise defaults->set
    working_auto = master_auto.fetch_diff(sources=[user_phil]).extract()
    modified_phil = master_auto.format(python_object=working_auto)
    #replace the text scope bones with picard and reinterpret below
    outstr = TextIO()
    modified_phil.show(out=outstr)
    showstr = outstr.getvalue()
    showstr = showstr.replace("bones {","picard {")
    user_phil = parse(showstr,converter_registry=converter_registry)
    autoparams = initparams + "\n" + build_auto_str("picard")
    master_auto = parse(autoparams,converter_registry=converter_registry)
    #fetch_diff important to only get the differences from the defaults, otherwise defaults->set
    dryrun_phil = parse("picard.dryrun = True",converter_registry=converter_registry)
    working_auto = master_auto.fetch_diff(sources=[user_phil,dryrun_phil]).extract()
    modified_phil = master_auto.format(python_object=working_auto)
    #replace the text scope bones with picard and reinterpret below
    outstr = TextIO()
    modified_phil.show(out=outstr)
    showstr = outstr.getvalue()
    basetng = inner_picard(basetng,showstr,progress_callback)
  except ValueError as e:
    raise e
  except Exception as e:
    raise e
  return basetng

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  assert(args.phil is not None)
  user_phil_str = args.phil.read()
  user_phil_str = user_phil_str + "\npicard.dryrun = True\n"
  bones( user_phil_str=user_phil_str)
