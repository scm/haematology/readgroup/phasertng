#ifndef __phasertng_table_d2gfunction_of_dssqr2_class__
#define __phasertng_table_d2gfunction_of_dssqr2_class__
#include <scitbx/constants.h>
#include <scitbx/array_family/shared.h>

// Assumption for this code is that G-function will be used when approximating
// the shape of a molecule as a sphere, in which case it doesn't make much sense
// to go past the first zero of the function.  Another approach should be used
// in other circumstances.

namespace phasertng {
namespace table {

class d2gfunction_by_dssqr2
{
  private:
  // Good Accuracy, zero to first root (return zero for larger args)
  int    tbllen;
  double argcut;
  double argfac;
  scitbx::af::shared<std::pair<double,double> > d2Gfunc_by_dRSsqr2Table;

  public:
  d2gfunction_by_dssqr2(int tbllen_=8192,
                    double argcut_=0.51143712) :
          tbllen(tbllen_),
          argcut(argcut_)
  {
    argfac = double(tbllen/argcut);
    d2Gfunc_by_dRSsqr2Table = getd2Gfunc_by_dRSsqr2Table(tbllen,argcut);
  }

  double
  get(double arg) const
  {
    if (arg < argcut)
    {
      double fracTableArg = argfac*arg;
      int index = static_cast<int>(fracTableArg);
      return d2Gfunc_by_dRSsqr2Table[index].first +
        (fracTableArg-index)*d2Gfunc_by_dRSsqr2Table[index].second;
    }
    return Fn_d2Gfunc_by_dRSsqr2(arg);
  }

  double
  Fn_d2Gfunc_by_dRSsqr2(double rsSqr) const
  {
    // 2nd derivative of G-function wrt rs^2.
    static double SMALL(0.1);
    double twoPiRS = scitbx::constants::two_pi*std::sqrt(rsSqr);
    if (twoPiRS >= SMALL)
      return (3*twoPiRS*(fn::pow2(twoPiRS)-15.)*std::cos(twoPiRS) +
        9*(5.-2*fn::pow2(twoPiRS))*std::sin(twoPiRS))
        / (fn::pow2(2*rsSqr)*fn::pow3(twoPiRS));
    else
      return fn::pow2(scitbx::constants::two_pi_sq)/35. -
        rsSqr*fn::pow3(scitbx::constants::two_pi_sq)/315.;
  }

  scitbx::af::shared<std::pair<double,double> >
  getd2Gfunc_by_dRSsqr2Table(int tbllen, double argmax)
  {
    scitbx::af::shared<std::pair<double,double> > d2Gfunc_by_dRSsqr2Table;
    d2Gfunc_by_dRSsqr2Table.resize(tbllen+1);
    double divarg(argmax/tbllen);
    for (unsigned i = 0; i < tbllen+1; i++)
    {
      double x0(Fn_d2Gfunc_by_dRSsqr2((i  )*divarg));
      double x1(Fn_d2Gfunc_by_dRSsqr2((i+1)*divarg));
      d2Gfunc_by_dRSsqr2Table[i].first = x0;
      d2Gfunc_by_dRSsqr2Table[i].second = x1 - x0;
    }
    return d2Gfunc_by_dRSsqr2Table;
  }

};

}}
#endif
