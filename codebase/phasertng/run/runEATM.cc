//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/math/likelihood/EELLG.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Expected_llg_for_atom
  void Phasertng::runEATM()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain one node");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Guide to eLLG values");
    logTab(where,"eLLG   Top solution correct?");
    logTab(where,"<25    :no");
    logTab(where,"25-36  :unlikely");
    logTab(where,"36-49  :possibly");
    logTab(where,"49-64  :probably");
    logTab(where,">64    :yes");
    logBlank(where);
    }}

    likelihood::ELLG ELLG;
    likelihood::EELLG EELLG;

    bool USE_ELLG = true;

    //make sure the reflections are sorted low resolution to high resolution
    //this should be done as part of the data preparation step
    logUnderLine(out::LOGFILE,"Single Atom eLLG Calculation");
    logTab(out::LOGFILE,"Resolution = " + dtos(REFLECTIONS->data_hires()));
    logBlank(out::LOGFILE);
    if (!REFLECTIONS->check_sort_in_resolution_order())
      throw Error(err::INPUT,"Reflection file not sorted by resolution, aborting");

    double io_target = input.value__flt_number.at(".phasertng.expected.atom.target.").value_or_default();
    sv_string io_elements = input.value__strings.at(".phasertng.expected.atom.scatterer.").value_or_default_unique();
    sv_double io_bfactors = input.value__flt_numbers.at(".phasertng.expected.atom.bfactor.").value_or_default_unique();
    std::reverse(io_bfactors.begin(), io_bfactors.end());
    double fsol = 0;
    double bsol = 0;
    double minb = 0;
    FourParameterSigmaA solTerm(
        input.value__flt_number.at(".phasertng.expected.atom.rmsd.").value_or_default(),
        fsol,bsol,minb);
    if (!io_elements.size())
      throw Error(err::FATAL,"No element: check input");
    if (!io_bfactors.size())
      throw Error(err::FATAL,"No Bfactor: check input");

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }
    for (auto item : DAGDB.WORK->COMPOSITION)
      scatterers.insert(item.first);
    for (auto sa_element : io_elements)
      scatterers.insert(sa_element);
    logTabArray(out::LOGFILE,scatterers.logScatterers("Composition"));

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    double hires_all_data(0);
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    class single_atom_result_t
    {
      public:
      single_atom_result_t(
      int number_,
      std::pair<double,double> minmaxFP_,
      std::string sa_element_,
      double Bfac_,
      double eLLG_,
      double eeLLG_) :
      number(number_),
      minmaxFP(minmaxFP_),
      sa_element(sa_element_),
      Bfac(Bfac_),
      eLLG(eLLG_),
      eeLLG(eeLLG_)
      {}
      int number;
      std::pair<double,double> minmaxFP;
      std::string sa_element;
      double Bfac;
      double eLLG;
      double eeLLG;
    };
    std::vector<single_atom_result_t> results;
    int max_search_number(5);
    int min_search_number(max_search_number);
    for (auto sa_element : io_elements)
    {
      for (auto Bfac : io_bfactors) //more ordered
      {
        //Calculation assumes that there is no change in the total scattering
        //caused by the change in the B-factor of a single atom
        //This calculation will not work changing the B-factor of larger components
        logTab(out::VERBOSE,"Results for: " + sa_element + " " + dtos(Bfac));
        for (int number = 1; number <= max_search_number; number++)
        {
          double eLLG(0),eeLLG(0);
          af_double bin_reso = REFLECTIONS->bin_midres(0);
          af_double Siga(bin_reso.size());
          std::pair<double,double> minmaxFP;
          //calculate the resolution dependent parameters by bin, for speed
          bool negvar = false;
          bool firstr(true);
          for (int r = 0; r < REFLECTIONS->NREFL; r++)
          {
            if (selected.get_selected(r))
            {
              const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
              const double dobs = work_row->get_dobsnat();
              const double ssqr = work_row->get_ssqr();
              double sigmaa = solTerm.get(ssqr);
              double bratio = std::exp(-2*(Bfac)*ssqr/4.0);
              double scatX = fn::pow2(scatterers.fo(sa_element,ssqr));
              double scatP(0);
              for (auto item : DAGDB.WORK->COMPOSITION)
                scatP += item.second*fn::pow2(scatterers.fo(item.first,ssqr));
              double fs = scatX/scatP;
              double fp = number*fs*bratio;
              double Siga = sigmaa*std::sqrt(fp);
              if (firstr) minmaxFP.first = minmaxFP.second = fp;
              else minmaxFP.second = std::max(minmaxFP.second,fp);
              eeLLG += EELLG.get(fn::pow2(Siga));
              firstr = false;
              if (USE_ELLG)
              {
                const double feff = work_row->get_feffnat();
                const double resn = work_row->get_resn();
                const bool   cent = work_row->get_cent();
                double DobsSiga = dobs*Siga;
                double Eeff = feff/resn;
                try {
                eLLG += ELLG.get(Eeff,DobsSiga,cent);
                }
                catch(...)
                {
                  logTab(out::VERBOSE,"fp range: " + dtos(minmaxFP.first) + " " + dtos(minmaxFP.second));
                  negvar = true;
                  break;
                }
              }
            }
          }
          if (!negvar)
            results.push_back(
              single_atom_result_t(number,minmaxFP,sa_element,Bfac,eLLG,eeLLG));
          if (eeLLG > io_target or eLLG > io_target)
          {
            min_search_number = std::min(number,min_search_number);
            break;
          }
        }
      }
    }
    logTab(out::VERBOSE,"Number of results: " + itos(results.size()));

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Single Atom eLLG");
    std::string last_element = "";
    for (int i = 0; i < results.size(); i++)
    {
      if (last_element != results[i].sa_element)
      {
        logBlank(where);
        logTab(where,snprintftos(
            "Table Atomtype: %-s",
            results[i].sa_element.c_str()));
        logTab(where,snprintftos(
            "%-8s  %-19s  %-6s  %12s %12s  %-12s",
            "Atomtype","fs for lores->hires","B-factor","eLLG  ","eeLLG  ","number"));
        last_element = results[i].sa_element;
      }
      double ellg =  results[i].eLLG;
      double eellg = results[i].eeLLG;
      logTab(where,snprintftos(
          "%8s  %7.5f ->%9.5f  %6.2f  %12s %12s  %-12d",
          results[i].sa_element.c_str(),
          results[i].minmaxFP.first,
          results[i].minmaxFP.second,
          results[i].Bfac,
          (ellg > DEF_PPT ? dtos(ellg,12,3).c_str() : "(< 0.001)"),
          USE_ELLG ? (eellg > DEF_PPM ? dtos(eellg,12,3).c_str() : "(< 0.001)") : "---",
          results[i].number
         ));
    }
    logBlank(where);
    logTableEnd(where);
    }}
    input.value__int_number.at(".phasertng.expected.atom.minimum_search_number.").set_value(min_search_number);
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
