from __future__ import division
from phasertng import Phasertng

#load the reflections
tng = Phasertng("PHENIX")
cards = '''
phasertng mode imtz data aniso tncso tncs feff cca ccs esm frf gyre ftf pose rbr rbm
phasertng hklin filename "../data/toxd/toxd.mtz"
phasertng suite database "toxd_results"
phasertng biological_unit sequence filename "../data/toxd/toxd.seq"
phasertng model filename "../data/toxd/toxd.pdb"
phasertng model tag "toxd_auto"
phasertng search tag "toxd_auto"
'''
tng.run(cards)
