//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/main/jiffy.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/hoist.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/src/SpaceGroup.h>

namespace phasertng {
namespace dag {

  void
  Node::convert_curl_to_pose(const dvect3 tncs_frac)
  {
    const dmat33 orthmat = CELL.orthogonalization_matrix();
    //find the next index for the tncs group
    std::set<int> tncs_groups;
    for (int p = 0; p < POSE.size(); p++)
      tncs_groups.insert(POSE[p].TNCS_GROUP.first);
    int next_tncs_group_number(1);
    while (tncs_groups.size() and tncs_groups.count(next_tncs_group_number))
    next_tncs_group_number++;
    int start = next_tncs_group_number;
    int nmol = TNCS_MODELLED ? 1 : TNCS_ORDER;
    if (tncs_frac == dvect3(0,0,0)) nmol = 1;
    for (int n = 0; n < nmol; n++)
    {
      next_tncs_group_number = start;
      if (NEXT.TRANSLATED and !NEXT.ngyre())
      {
        dag::Pose new_pose;
        new_pose.IDENTIFIER = NEXT.IDENTIFIER;
        new_pose.ENTRY = NEXT.ENTRY;
        new_pose.EULER = NEXT.EULER;
        new_pose.ORTHR = NEXT.ORTHR;
        new_pose.ORTHT = {0,0,0};
        new_pose.FRACT = NEXT.FRACT + double(n)*tncs_frac;
        new_pose.SHIFT_MATRIX = NEXT.SHIFT_MATRIX;
        new_pose.SHIFT_ORTHT = NEXT.SHIFT_ORTHT + double(n)*orthmat*tncs_frac;
        new_pose.TNCS_GROUP = { next_tncs_group_number, n+1 };
        new_pose.TFZ = NEXT.TFZ; //not set
        POSE.push_back(new_pose);
      }
      for (int g = 0; g < NEXT.ngyre(); g++)
      {
        dag::Gyre gyre = NEXT.GYRE[g];
        dag::Pose new_pose;
        new_pose.IDENTIFIER = gyre.IDENTIFIER;
        new_pose.ENTRY = gyre.ENTRY;
        new_pose.EULER = NEXT.EULER;
        new_pose.ORTHR = gyre.ORTHR;
        new_pose.ORTHT = gyre.ORTHT;
        new_pose.FRACT = NEXT.FRACT + double(n)*tncs_frac;
        new_pose.SHIFT_MATRIX = NEXT.SHIFT_MATRIX;
        new_pose.SHIFT_ORTHT = NEXT.SHIFT_ORTHT + double(n)*orthmat*tncs_frac;
        new_pose.TNCS_GROUP = { next_tncs_group_number, n+1 };
        new_pose.TFZ = NEXT.TFZ; //not set
        POSE.push_back(new_pose);
        next_tncs_group_number++; //next set of tncs vectors
      }
    }
    NEXT.TRANSLATED = false;
    NEXT.PRESENT = false;
    NEXT.GYRE.clear();

    PHASER_ASSERT(SG);
    bool use_perturbations(false);
    if (use_perturbations)
    { //put the perturbations into the euler angles and fract
      for (int p = 0; p < POSE.size(); p++)
      {
        dag::Pose& pose = POSE[p];
        dmat33 ROT = zyz_matrix(pose.EULER);
        //apply xyz rotation perturbation to molecular transform orientation
        dmat33 Rperturb = pose.perturbed(pose.ORTHR);
        dmat33 ROTperturb = ROT * Rperturb; //new euler
        pose.EULER = scitbx::math::euler_angles::zyz_angles(ROTperturb);
        dvect3 iOrthT = CELL.orthogonalization_matrix() * pose.FRACT;
               iOrthT += pose.ORTHT;
        pose.FRACT = CELL.fractionalization_matrix()*iOrthT;
        //ORTHR ORTHT have been incorporated in EULER and FRACT
        pose.ORTHR = dvect3(0,0,0);
        pose.ORTHT = dvect3(0,0,0);
      }
    }
  }

  bool
  Node::turn_in_poses() const
  {
    dmat33 TROT = zyz_matrix(NEXT.EULER);
    double rotMax(1);
    for (int p = 0; p < POSE.size(); p++)
    {
      if (POSE[p].IDENTIFIER == NEXT.IDENTIFIER)
      {
        for (int isym = 0; isym < SG->NSYMP; isym++)
        {
          dmat33 MROT = zyz_matrix(POSE[p].EULER);
          dmat33 Rsymt = SG->rotsym[isym].transpose();
          MROT = (CELL.orthogonalization_matrix()*Rsymt*CELL.fractionalization_matrix())*MROT;
          double rotdist = scitbx::rad_as_deg(matrix_delta_rad(TROT,MROT));
          if (rotdist < rotMax)
            return true;
        }
      }
    }
    return false;
  }

  void
  Node::clear_last_tncs_group_pose()
  {
    //delete all the poses in the last tncs group
    //converting them to curl and turn
    int last_tncs_group_number = POSE.back().TNCS_GROUP.first;
    std::vector<dag::Pose> remaining_poses;
    NEXT.TRANSLATED = false;
    for (int p = 0; p < POSE.size(); p++)
    {
      dag::Pose& pose = POSE[p];
      if (pose.TNCS_GROUP.first != last_tncs_group_number)
      {
        remaining_poses.push_back(pose);
      }
    }
    POSE = remaining_poses;
    //reset entries afterwards
  }

  void
  Node::convert_turn_to_pose(const dvect3 frac, const dvect3 tncs_frac)
  {
    const dmat33 orthmat = CELL.orthogonalization_matrix();
    //find the next index for the tncs group
    std::set<int> tncs_groups;
    for (int p = 0; p < POSE.size(); p++)
      tncs_groups.insert(POSE[p].TNCS_GROUP.first);
    int next_tncs_group_number(1);
    while (tncs_groups.size() and tncs_groups.count(next_tncs_group_number))
    next_tncs_group_number++;
    int nmol = TNCS_MODELLED ? 1 : TNCS_ORDER;
    for (int n = 0; n < nmol; n++)
    {
      if (NEXT.PRESENT and !NEXT.ngyre())
      {
        dag::Pose new_pose;
        new_pose.IDENTIFIER = NEXT.IDENTIFIER;
        new_pose.ENTRY = NEXT.ENTRY;
        new_pose.EULER = NEXT.EULER;
        new_pose.ORTHR = NEXT.ORTHR;
        new_pose.ORTHT = {0,0,0};
        new_pose.FRACT = frac + double(n)*tncs_frac;
        new_pose.SHIFT_MATRIX = NEXT.SHIFT_MATRIX;
        new_pose.SHIFT_ORTHT = {0,0,0}; //not set
        new_pose.TNCS_GROUP = { next_tncs_group_number, n+1 };
        new_pose.TFZ = 0; //not set
        POSE.push_back(new_pose);
      }
      for (int g = 0; g < NEXT.ngyre(); g++)
      {
        dag::Gyre gyre = NEXT.GYRE[g];
        dag::Pose new_pose;
        new_pose.IDENTIFIER = gyre.IDENTIFIER;
        new_pose.ENTRY = gyre.ENTRY;
        new_pose.EULER = NEXT.EULER;
        new_pose.ORTHR = gyre.ORTHR;
        new_pose.ORTHT = {0,0,0};
        new_pose.FRACT = frac + double(n)*tncs_frac;
        new_pose.SHIFT_MATRIX = NEXT.SHIFT_MATRIX;
        new_pose.SHIFT_ORTHT = {0,0,0}; //not set
        new_pose.TNCS_GROUP = { next_tncs_group_number, n+1 };
        new_pose.TFZ = 0; //not set
        POSE.push_back(new_pose);
        next_tncs_group_number++;
      }
    }
    NEXT.PRESENT = false;
  }

  void
  Node::convert_turn_to_gyre(setmodlid_t searches)
  {
    for (auto& search : searches)
    {
      const Identifier& modlid = search;
      dag::Gyre new_gyre;
      new_gyre.IDENTIFIER = modlid;
      new_gyre.ENTRY = nullptr; //if search is not in seek set then this will not be set
      for (int s = 0; s < SEEK.size(); s++)
        if (SEEK[s].IDENTIFIER == modlid)
          new_gyre.ENTRY = SEEK[s].ENTRY;
      if (!new_gyre.ENTRY)
        for (int h = 0; h < HOLD.size(); h++)
          if (HOLD[h].IDENTIFIER == modlid)
            new_gyre.ENTRY = HOLD[h].ENTRY;
      PHASER_ASSERT(new_gyre.ENTRY); //!nullptr
      NEXT.GYRE.push_back(new_gyre);
      //entry must be in the seek already
    }
  }

  void
  Node::convert_turn_to_curl(const dvect3 frac,const dvect3 shift_ortht,double tfz)
  {
    NEXT.TRANSLATED = true;
    NEXT.FRACT = frac; //wrt mt
    NEXT.SHIFT_ORTHT = shift_ortht; //wrt in
    if (tfz >= 0) NEXT.TFZ = tfz; //default flag -999 for no change
  }

  void
  Node::initialize_turn(const dvect3 euler,dag::Entry* modlid)
  {
    NEXT.ENTRY = modlid;
    NEXT.PRESENT = true;
    NEXT.IDENTIFIER = modlid->identify();
    NEXT.EULER = euler;
    PHASER_ASSERT(NEXT.ENTRY);
    PHASER_ASSERT(SG);
    {
      auto& node = NEXT;
      double shift_angle = std::numeric_limits<double>::max();
      dvect3 shift_axis(0,0,0);
      for (int isym = 0; isym < SG->NSYMP; isym++)
      {
        dmat33 Rsymt = SG->rotsym[isym].transpose();
        dmat33 MROT = zyz_matrix(node.EULER);
        MROT = (CELL.orthogonalization_matrix()*Rsymt*CELL.fractionalization_matrix())*MROT;
        dvect3 axis; double angle;
        std::tie(axis,angle) = axis_and_angle_deg(MROT);
        if (angle < shift_angle)
          node.SHIFT_MATRIX = MROT;
      }
    }
    PHASER_ASSERT(NEXT.ENTRY);
  }

  std::string
  Node::str() const
  {
    auto txt = logNode("");
    std::string text;
    for (int i = 0; i < txt.size(); i++)
      text += txt[i] + "\n";
    return text;
  }

  void
  Node::add_header(af_string output,std::string description) const
  {
    if (description.size())
      output.push_back("Title: " + description);
    if (LLG != 0)
      output.push_back("LLG: " + dtos(LLG,2));
    if (MAPLLG != 0)
      output.push_back("Map LLG: " + dtos(MAPLLG,2));
    if (ZSCORE != 0)
      output.push_back("Z-score: " + dtos(ZSCORE,2));
    if (SG)
      output.push_back("Space Group: " + sg_info());
    for (auto& item : PARENTS)
      output.push_back("Parent node: " + std::to_string(item));
    output.push_back("Composition Z: " + std::to_string(CELLZ));
    std::string compstr;
    for (auto& item : COMPOSITION)
      compstr += item.first + "/" + itos(item.second) + " ";
    if (compstr.size())
    {
      compstr.pop_back();
      output.push_back("Composition: " + compstr);
    }
    if (POSE.size())
    {
      (CLASH_WORST == 0) ?
        output.push_back("Worst Clash: 0%"):
        output.push_back("Worst Clash: " + dtos(CLASH_WORST,6,2) + "%");
      output.push_back("tNCS indicated: " + chtos(TNCS_INDICATED));
      if (TNCS_INDICATED)
      output.push_back("tNCS order:vector = " + tncs_info());
      output.push_back("Twinned: " + chtos(TWINNED) + std::string(TWINOP.size() ? " with " + TWINOP : ""));
      output.push_back("Packs: " + chtos(PACKS));
      output.push_back("Special Position: " + chtos(SPECIAL_POSITION));
      output.push_back("Spans: " + chtos(SPANS));
    }
    //output.push_back("Tracker:" + TRACKER.str());
  }

  void
  Node::add_annotation(af_string output) const
  {
    if (!ANNOTATION.size()) return;
    std::istringstream ss(ANNOTATION);
    std::string word; // for storing each word
    // Traverse through all words
    // while loop till we get
    // strings to store in string word
    af_string txt;
    while (ss >> word)
    { //break lines at RF each time
      if (word.size())
      {
        if (txt.size() == 0)
          txt.push_back(word);
        else if (word.substr(0,5) == "FIND=" or word.substr(0,5) == "FUSE=" or word.substr(0,4) == "JOG=")
          txt.push_back(word);
        else
          txt.back() += " " + word;
      }
    }
    output.push_back("Annotation:");
    for (int t = 0; t < txt.size(); t++)
      output.push_back(txt[t]);
    output.push_back("------");
  }

  void
  Node::add_pose(af_string output) const
  {
    if (POSE.size())
    {
      for (int p = 0; p < POSE.size(); p++)
      {
        const dag::Pose& pose = POSE[p];
        output.push_back(snprintftos(
            "%s BFAC=%-7.2f MULT=%-2i TNCS-GRP=%d(#%d)",
            std::string("Pose #"+ntos(p+1,POSE.size())+" ("+pose.IDENTIFIER.str()+")").c_str(),
            pose.BFAC,pose.MULT,pose.TNCS_GROUP.first,pose.TNCS_GROUP.second));
        output.push_back(snprintftos(
            "  R(% 7.2f % 7.2f % 7.2f) wrt mt",
            pose.EULER[0],pose.EULER[1],pose.EULER[2]));
        double ang = pose.delta_angle();
        output.push_back(snprintftos(
            " +R{%+7.3f(x) %+7.3f(y) %+7.3f(z)} wrt mt | Angle[%5.*f]",
            pose.ORTHR[0],pose.ORTHR[1],pose.ORTHR[2],
            (ang >= 10) ? 2:3,ang));
        if (pose.SHIFT_MATRIX != dmat33()) output.push_back(snprintftos(
            " R=%s wrt in",dag::print_polar(pose.SHIFT_MATRIX).c_str()));
        output.push_back(snprintftos(
            "  T(% 7.3f % 7.3f % 7.3f)",
            pose.FRACT[0],pose.FRACT[1],pose.FRACT[2]));
        double sft = pose.delta_shift();
        output.push_back(snprintftos(
            " +T{%+7.3f(x) %+7.3f(y) %+7.3f(z)} wrt mt | Shift[%5.*f]",
            pose.ORTHT[0],pose.ORTHT[1],pose.ORTHT[2],
            (sft >= 10) ? 2:3,sft));
        output.push_back(snprintftos(
            " T=%s wrt in",dag::print_ortht(pose.SHIFT_ORTHT).c_str()));
        output.push_back("------");
      }
    }
  }

  void
  Node::add_curl(af_string output) const
  {
    if (NEXT.PRESENT and NEXT.TRANSLATED)
    {
      output.push_back("Curl (" + NEXT.IDENTIFIER.str() + ")");
      output.push_back(snprintftos(
          "  R(% 7.2f % 7.2f % 7.2f) wrt mt",
          NEXT.EULER[0],NEXT.EULER[1],NEXT.EULER[2]));
      double ang = NEXT.delta_angle();
      output.push_back(snprintftos(
          " +R{%+7.3f(x) %+7.3f(y) %+7.3f(z)} wrt mt | Angle[%5.*f]",
          NEXT.ORTHR[0],NEXT.ORTHR[1],NEXT.ORTHR[2],
          (ang >= 10) ? 2:3,ang));
      if (NEXT.SHIFT_MATRIX != dmat33()) output.push_back(snprintftos(
          " R=%s wrt in",dag::print_polar(NEXT.SHIFT_MATRIX).c_str()));
      output.push_back(snprintftos(
          "  T(% 7.3f % 7.3f % 7.3f) wrt mt",
          NEXT.FRACT[0],NEXT.FRACT[1],NEXT.FRACT[2]));
      output.push_back(snprintftos(
          " T=%s wrt in",dag::print_ortht(NEXT.SHIFT_ORTHT).c_str()));
      output.push_back("------");
    }
  }

  void
  Node::add_gyre(af_string output) const
  {
    if (NEXT.ngyre())
    {
      for (int g = 0; g < NEXT.ngyre(); g++)
      {
        const dag::Gyre& gyre = NEXT.GYRE[g];
        output.push_back("Gyre #" + ntos(g+1,NEXT.ngyre()) + " (" + gyre.IDENTIFIER.str() + ")");
        output.push_back(snprintftos(
            "  R(% 7.2f % 7.2f % 7.2f) wrt mt",
            NEXT.EULER[0],NEXT.EULER[1],NEXT.EULER[2]));
        double ang = gyre.delta_angle();
        output.push_back(snprintftos(
            " +R{%+7.3f(x) %+7.3f(y) %+7.3f(z)} wrt mt | Angle[%5.*f]",
            gyre.ORTHR[0],gyre.ORTHR[1],gyre.ORTHR[2],
            (ang >= 10) ? 2:3,ang));
        if (NEXT.SHIFT_MATRIX != dmat33()) output.push_back(snprintftos(
            " R=%s wrt in",dag::print_polar(NEXT.SHIFT_MATRIX).c_str()));
      }
      output.push_back("------");
    }
  }

  void
  Node::add_turn(af_string output) const
  {
    if (NEXT.PRESENT)
    {
      output.push_back("Turn (" + NEXT.IDENTIFIER.str() + ")");
    //  auto EULER = euler_wrt_in_from_mt(NEXT.EULER,NEXT.ENTRY->PRINCIPAL_ORIENTATION);
      output.push_back(snprintftos(
            "  R(% 7.2f % 7.2f % 7.2f) wrt mt",
            NEXT.EULER[0],NEXT.EULER[1],NEXT.EULER[2]));
      double ang = NEXT.delta_angle();
      output.push_back(snprintftos(
            " +R{%+7.3f(x) %+7.3f(y) %+7.3f(z)} wrt mt | Angle[%5.*f]",
            NEXT.ORTHR[0],NEXT.ORTHR[1],NEXT.ORTHR[2],
            (ang >= 10) ? 2:3,ang));
      if (NEXT.SHIFT_MATRIX != dmat33()) output.push_back(snprintftos(
            " R=%s wrt in",dag::print_polar(NEXT.SHIFT_MATRIX).c_str()));
      output.push_back("------");
    }
  }

  void
  Node::add_seek(af_string output) const
  {
    if (SEEK.size())
    {
      for (int s = 0; s < SEEK.size(); s++)
      {
        std::string txt = "Seek #" + ntos(s+1,SEEK.size()) + " (" + SEEK[s].IDENTIFIER.str() + ")" + " flag:" + std::string(1,SEEK[s].STAR);
        if (SEEK[s].seek_increases > 0)
           txt += " eLLG+" + dtos(SEEK[s].seek_increases,0);
        output.push_back(txt);
      }
      output.push_back("------");
    }
  }

  void
  Node::add_entries(af_string output,std::set<Identifier> print,bool summary) const
  {
    //set of modlid to be printed, each entry is erased after print so that each only printed once
    while (print.size())
    {
      dag::Entry* ENTRY = nullptr;
      //cheap algorithm
      //keeps overwriting the link multiple times as long as there are entries in print
      //have to loop over all the possible places for the entry, unfortunately, in data structure
      for (int s = 0; s < SEEK.size() and !ENTRY; s++)
        if (print.count(SEEK[s].IDENTIFIER))
          ENTRY = SEEK[s].ENTRY;
      for (int p = 0; p < POSE.size() and !ENTRY; p++)
        if (print.count(POSE[p].IDENTIFIER))
          ENTRY = POSE[p].ENTRY;
      if (print.count(NEXT.IDENTIFIER) and !ENTRY)
        ENTRY = NEXT.ENTRY;
      for (int g = 0; g < NEXT.ngyre() and !ENTRY; g++)
        if (print.count(NEXT.GYRE[g].IDENTIFIER))
          ENTRY = NEXT.GYRE[g].ENTRY;
      if (print.count(FULL.IDENTIFIER) and !ENTRY)
        ENTRY = FULL.ENTRY;
      if (ENTRY == nullptr)
        break; //because the pointers have not been set, infinite loop
      if (ENTRY != nullptr) //not nullptr
      {
        output.push_back(std::string("Entry: (" + ENTRY->DOGTAG.str() + ")"));
        output.push_back(std::string("Data: " + ENTRY->get_data_choice_selection_str()));
        double Da = ENTRY->MOLECULAR_WEIGHT;
        double kDa = Da/1000.;
        auto id = ENTRY->identify();
        PHASER_ASSERT(DRMS_SHIFT.count(id));
        output.push_back(snprintftos(
            "drms-shift[%+6.3f]=vrms-value[%6.3f] cell-scale[%7.4f]",
            DRMS_SHIFT.at(id).VAL,
            VRMS_VALUE.at(id).VAL,
            CELL_SCALE.at(id).VAL
         ));
        if (!summary)
        { //selection is alternative to assert
          output.push_back(snprintftos(
              "drms-range[%+7.3f:%+7.3f] vrms-start[%s]",
              ENTRY->DRMS_MIN,ENTRY->DRMS_MAX,
              dvtos(ENTRY->VRMS_START,6,3).c_str()
            ));
          output.push_back(snprintftos(
              "molecular-weight[%s%s] scattering[%se] mean-radius[%sA] point-group[%s]",
              (kDa>10) ? dtos(kDa,3).c_str() : dtos(Da,3).c_str(),
              (kDa>10) ? "kDa":"Da",
              itos(ENTRY->SCATTERING).c_str(),
              dtos(ENTRY->MEAN_RADIUS,1).c_str(),
              ENTRY->POINT_GROUP_SYMBOL.size() ? ENTRY->POINT_GROUP_SYMBOL.c_str() : "0"
            ));
          output.push_back("PT: orth[" + dvtos(ENTRY->PRINCIPAL_TRANSLATION,7,1) + "]=frac[" +
              dvtos(CELL.fractionalization_matrix()*ENTRY->PRINCIPAL_TRANSLATION,7,3) + "]");
          output.push_back("PR: [" + dmtos(ENTRY->PRINCIPAL_ORIENTATION,7,3) + "]");
        }
        output.push_back("------");
        print.erase(ENTRY->identify());//so only prints once
      }
    }
    output.push_back("");
  }

  af_string
  Node::logNode(std::string description) const
  {
    af_string output;
    add_header(output,description);
    add_annotation(output);
    output.push_back(separator());
    if (POSE.size() or NEXT.ngyre() or NEXT.PRESENT or SEEK.size() or FULL.PRESENT)
    {
      output.push_back("(Next/Gyre/Pose/Full/Seek) (identifier/tag)");
      output.push_back("------");
    }
    else
    {
      output.push_back("No (Next/Gyre/Pose/Full/Seek) (identifier/tag)");
    }
    add_pose(output);
    add_curl(output);
    add_gyre(output);
    add_turn(output);
    if (FULL.PRESENT)
    {
      output.push_back("Full (" + FULL.IDENTIFIER.str() + ")");
      output.push_back("------");
    }
    add_seek(output);
    output.push_back(separator());
    add_entries(output,used_modlid(),false);
    output.push_back(separator());
    return output;
  }

  std::string
  Node::sg_info() const
  { std::string sg = SG->CCP4; hoist::replace_all(sg," ",""); return sg; }

  std::string
  Node::tncs_info() const
  {
    if (TNCS_ORDER == 1) return  "1:(---None,---None,---None)";
    return itos(TNCS_ORDER)+":("+dtos(TNCS_VECTOR[0],7,4,true)+","+dtos(TNCS_VECTOR[1],7,4,true)+","+dtos(TNCS_VECTOR[2],7,4,true)+")";
  }

  std::string
  Node::pose_info() const
  {
    std::string txt;
    //make the minimium b-factor 0 as in the coordinate file
    double minbfac = POSE[0].BFAC;
    for (int p = 0; p < POSE.size(); p++)
      minbfac = std::min(minbfac, POSE[p].BFAC);
    for (int p = 0; p < POSE.size(); p++)
      txt += dtos(POSE[p].BFAC-minbfac,5,1) + " ";
    txt.pop_back();
    return txt;
#if 0
    std::string txt;
    //just the composition not the order
    std::map<std::string,int> poseset;
    for (int p = 0; p < POSE.size(); p++)
      poseset[POSE[p].IDENTIFIER.string()] = 0;
    for (int p = 0; p < POSE.size(); p++)
      poseset[POSE[p].IDENTIFIER.string()]++;
    for (auto item : poseset)
      txt += "[" + item.first + "]*" + itos(item.second) + "+";
    txt.pop_back();
    return txt;
#endif
#if 0
    //or in order
    if (POSE.size()) txt = POSE[0].IDENTIFIER.tag();
    int npose = 1;
    for (int p = 1; p < POSE.size(); p++)
    {
      if (POSE[p].IDENTIFIER == POSE[p-1].IDENTIFIER)
      {
        npose++;
      }
      else
      {
        txt += "*" + itos(npose) + "+" + POSE[p].IDENTIFIER.tag();
        npose = 1;
      }
    }
    txt += "*" + itos(npose);
    return txt;
#endif
  }

  af_string
  Node::logAnnotation(std::string description,bool add_blank) const
  {
    af_string output;
    if (description.size())
      output.push_back("Title: " + description);
    add_annotation(output);
    POSE.size() ?
      output.push_back("Poses: " + itos(POSE.size())):
      output.push_back("Poses: None");
    if (POSE.size())
    {
      std::string tags = "Tags: ";
      int p = 0;
      while (p < POSE.size())
      {
        auto firstid = POSE[p].IDENTIFIER;
        tags += firstid.tag() + "*";
        int count(0);
        for (; p < POSE.size(); p++)
          if (firstid != POSE[p].IDENTIFIER)
            break;
          else count++;
        tags += itos(count) + " ";
      }
      output.push_back(tags);
    }
    if (add_blank) output.push_back("");
    return output;
  }

  af_string
  Node::logPose(std::string description,bool add_blank,bool extra) const
  {
    af_string output;
    if (description.size())
      output.push_back("Title: " + description);
    add_annotation(output);
    if (!POSE.size())
    {
      output.push_back("Poses: None");
    }
    else
    {
      for (int p = 0; p < POSE.size(); p++)
      {
        const dag::Pose& pose = POSE[p];
        double ang = pose.delta_angle();
        double sft = pose.delta_shift();
        std::string shift = "";
        if (ang or sft) shift = "("+dtos(ang,(ang >= 10) ? 1:2)+"o/"+dtos(sft,(sft >= 10) ? 1:2)+"A)";
        dvect3 fract = CELL.fractionalization_matrix() * pose.SHIFT_ORTHT;
        output.push_back(snprintftos(
            "%s R=%s T=%s/%s B=%-6.1f %s %s",
            std::string("Pose #"+ntos(p+1,POSE.size())).c_str(),
            dag::print_polar(pose.SHIFT_MATRIX).c_str(),
            dag::print_fract(fract).c_str(),
            dag::print_ortht(pose.SHIFT_ORTHT).c_str(),
            pose.BFAC, //pose.MULT,pose.TNCS_GROUP.first,
            shift.c_str(),
            pose.IDENTIFIER.str().c_str()));
        if (extra)
        {
        output.push_back(snprintftos(
            "  EULER=[% 6.1f % 6.1f % 6.1f] FRACT=[% 7.3f % 7.3f % 7.3f] wrt mt",
            pose.EULER[0],pose.EULER[1],pose.EULER[2],
            pose.FRACT[0],pose.FRACT[1],pose.FRACT[2]));
        output.push_back(snprintftos(
            "  ORTHR={%+6.3f %+6.3f %+6.3f} ORTHT={%+7.3f %+7.3f %+7.3f} wrt mt",
            pose.ORTHR[0],pose.ORTHR[1],pose.ORTHR[2],
            pose.ORTHT[0],pose.ORTHT[1],pose.ORTHT[2]));
        }
      }
    }
    if (add_blank)
      output.push_back("");
    return output;
  }

  af_string
  Node::logSeek(std::string description) const
  {
    af_string output;
    if (description.size())
      output.push_back("Title: " + description);
    add_seek(output);
    return output;
  }

  af_string
  Node::logTurn(std::string description) const
  {
    af_string output;
    if (description.size())
      output.push_back("Title: " + description);
    add_annotation(output);
    output.push_back("Background Poses: " + itos(POSE.size()));
    output.push_back(separator());
    add_turn(output);
    add_entries(output,used_modlid_gyre(),true);
    return output;
  }

  af_string
  Node::logGyre(std::string description) const
  {
    af_string output;
    if (description.size())
      output.push_back("Title: " + description);
    add_annotation(output);
    output.push_back("Background Poses: " + itos(POSE.size()));
    output.push_back(separator());
    add_gyre(output);
    add_entries(output,used_modlid_gyre(),true);
    return output;
  }

  af_string
  Node::logClash() const
  {
    af_string output;
    double s = POSE.size()+1; //must leave room for indices as well as values
    int w = std::ceil(std::log10(std::max(s,1.))); //width of indices
    for (int p = 0; p < POSE.size(); p++)
    {
      for (int q = 0; q < POSE.size(); q++)
      {
        int ww = std::ceil(std::log10(std::max(clash_matrix(p,q)+1,1)));
        w = std::max(w,ww); //width of values
      }
    }
    for (int p = 0; p < POSE.size(); p++)
    {
      if (p == 0)
      {
        std::string line = "[#]" + std::string(w-1,' ') + "-- ";
        for (int q = 0; q < POSE.size(); q++)
          line += "" + itos(q+1,w,true,false) + " ";
        line += "[ Overlap        ]";
        output.push_back(line);
      }
      std::string line = "--" + itos(p+1,w,true,false) + "-- ";
      for (int q = 0; q < POSE.size(); q++)
        line += clash_matrix(p,q) == 0 ?
                std::string(w-1,' ') + ". ":
                itos(clash_matrix(p,q),w,true,false) + " ";
      const auto& clash = POSE[p].CLASH;
      std::string tag = POSE[p].IDENTIFIER.tag();
      double overlap = clash.overlap_percent();
      line += snprintftos(
          " [%4d/%4d =%4.*f%%] %s",
          clash.atoms_clash,
          clash.atoms_trace,
          (overlap < 1 and overlap > 0) ? 1:0,overlap,
          tag.c_str());
      output.push_back(line);
    }
    return output;
  }

  af_string
  Node::logPairwiseClash(size_t nbackground) const
  {
    af_string output;
    if (nbackground > 0)
      output.push_back("Constituents for fusing");
    double s = POSE.size()+1; //must leave room for indices as well as values
    int w = std::ceil(std::log10(std::max(s,1.))); //width of indices
    for (int p = 0; p < POSE.size(); p++)
    {
      for (int q = 0; q < POSE.size(); q++)
      {
        int ww = std::ceil(std::log10(std::max(clash_matrix(p,q)+1,1)));
        w = std::max(w,ww); //width of values
      }
    }
    size_t phead[4] = { 0,0,nbackground,nbackground };
    size_t ptail[4] = { nbackground,nbackground,POSE.size(),POSE.size() };
    size_t qhead[4] = { 0,nbackground,0,nbackground };
    size_t qtail[4] = { nbackground,POSE.size(),nbackground,POSE.size() };
    for (auto loop : {0,1,2,3})
    {
      if (loop == 0) output.push_back("Background-Background:");
      if (loop == 1) output.push_back("Background-Fusing:");
      if (loop == 2) output.push_back("Fusing-Background:");
      if (loop == 3) output.push_back("Fusing-Fusing:");
      for (int p = phead[loop]; p < ptail[loop]; p++)
      {
        if (p == phead[loop])
        {
          std::string line = "[#]" + std::string(w-1,' ') + "-- ";
          for (int q = qhead[loop]; q < qtail[loop]; q++)
            line += "" + itos(q+1,w,true,false) + " ";
          output.push_back(line);
        }
        std::string line = "--" + itos(p+1,w,true,false) + "-- ";
        for (int q = qhead[loop]; q < qtail[loop]; q++)
          line += clash_matrix(p,q) == 0 ?
                  std::string(w-1,' ') + ". ":
                  itos(clash_matrix(p,q),w,true,false) + " ";
        output.push_back(line);
      }
      output.push_back("");
    }
    return output;
  }

  std::vector<dag::Term>
  Node::pose_terms_interpolate(dvect3 fract) const
  {
    std::vector<dag::Term> terms(POSE.size());
    for (int p = 0; p < POSE.size(); p++)
    {
      const dag::Pose& pose = POSE[p];
      dmat33 ROT = zyz_matrix(pose.EULER);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = pose.perturbed(pose.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dmat33 R = ROTperturb * pose.ENTRY->INTERPOLATION.UC.cctbxUC.orthogonalization_matrix();
      dmat33 Q1 = CELL.fractionalization_matrix() * R;
      dmat33 Q1tr = Q1.transpose();

      dvect3 iOrthT = CELL.orthogonalization_matrix() * (pose.FRACT + fract);
      iOrthT += pose.ORTHT;
      dvect3 TRA = CELL.fractionalization_matrix()*iOrthT;

      dmat33 ROTtr = ROT.transpose();
      dmat33 Rtr_Dtr = ROTtr * CELL.fractionalization_matrix().transpose();

      terms[p] = dag::Term(Q1tr,TRA,Rtr_Dtr);
    }
    return terms;
  }

  void
  Node::cleanUp()
  {
    for (int p = 0; p < POSE.size(); p++)
    {
      dag::Pose& pose = POSE[p];
      dmat33 ROT = zyz_matrix(pose.EULER);
      //apply xyz rotation perturbation to molecular transform orientation
      dmat33 Rperturb = pose.perturbed(pose.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb; //new euler
      pose.EULER = scitbx::math::euler_angles::zyz_angles(ROTperturb);
      dvect3 iOrthT = CELL.orthogonalization_matrix() * pose.FRACT;
             iOrthT += pose.ORTHT;
      pose.FRACT = CELL.fractionalization_matrix()*iOrthT;
      //ORTHR ORTHT have been incorporated in EULER and FRACT
      pose.ORTHR = dvect3(0,0,0);
      pose.ORTHT = dvect3(0,0,0);
    }
  }

  dag::Term
  Node::curl_terms_interpolate() const
  {
    dag::Term terms;
    if (NEXT.TRANSLATED)
    {
      dmat33 ROT = zyz_matrix(NEXT.EULER);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = NEXT.perturbed(NEXT.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dmat33 R = ROTperturb * NEXT.ENTRY->INTERPOLATION.UC.cctbxUC.orthogonalization_matrix();
      dmat33 Q1 = CELL.fractionalization_matrix() * R;
      dmat33 Q1tr = Q1.transpose();

      dvect3 iOrthT = CELL.orthogonalization_matrix() * NEXT.FRACT;
      dvect3 TRA = CELL.fractionalization_matrix()*iOrthT;

      dmat33 ROTtr = ROT.transpose();
      dmat33 Rtr_Dtr = ROTtr * CELL.fractionalization_matrix().transpose();

      terms = dag::Term(Q1tr,TRA,Rtr_Dtr);
    }
    return terms;
  }

  std::vector<dag::Term>
  Node::gyre_curl_terms_interpolate() const
  {
    //the translation is held in NEXT
    std::vector<dag::Term> terms(NEXT.ngyre());
    for (int c = 0; c < NEXT.ngyre(); c++)
    {
      const dag::Gyre& curl = NEXT.GYRE[c];
      dmat33 ROT = zyz_matrix(NEXT.EULER);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = curl.perturbed(curl.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dmat33 R = ROTperturb * curl.ENTRY->INTERPOLATION.UC.cctbxUC.orthogonalization_matrix();
      dmat33 Q1 = CELL.fractionalization_matrix() * R;
      dmat33 Q1tr = Q1.transpose();

      dvect3 iOrthT = CELL.orthogonalization_matrix() * NEXT.FRACT;
      iOrthT += curl.ORTHT;
      dvect3 TRA = CELL.fractionalization_matrix()*iOrthT;

      dmat33 ROTtr = ROT.transpose();
      dmat33 Rtr_Dtr = ROTtr * CELL.fractionalization_matrix().transpose();

      terms[c] = dag::Term(Q1tr,TRA,Rtr_Dtr);
    }
    return terms;
  }

  std::vector<dag::Term>
  Node::gyre_terms_interpolate() const
  {
    std::vector<dag::Term> terms(NEXT.ngyre());
    for (int g = 0; g < NEXT.ngyre(); g++)
    {
      const dag::Gyre& gyre = NEXT.GYRE[g];
      dmat33 ROT = zyz_matrix(NEXT.EULER);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = gyre.perturbed(gyre.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dmat33 R = ROTperturb * gyre.ENTRY->INTERPOLATION.UC.cctbxUC.orthogonalization_matrix();
      dmat33 Q1 = CELL.fractionalization_matrix() * R;
      dmat33 Q1tr = Q1.transpose();

      dvect3 iOrthT = {0,0,0};
      if (gyre.SHIFT == dvect3(0,0,0))
        iOrthT += gyre.ORTHT;
      dvect3 TRA = CELL.fractionalization_matrix()*iOrthT;

      dmat33 ROTtr = ROT.transpose();
      dmat33 Rtr_Dtr = ROTtr * CELL.fractionalization_matrix().transpose();

      terms[g] = dag::Term(Q1tr,TRA,Rtr_Dtr);
    }
    return terms;
  }

  dag::Term
  Node::turn_terms_interpolate() const
  {
    dag::Term terms;
    if (NEXT.PRESENT)
    {
      dmat33 ROT = zyz_matrix(NEXT.EULER);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = NEXT.perturbed(NEXT.ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dmat33 R = ROTperturb * NEXT.ENTRY->INTERPOLATION.UC.cctbxUC.orthogonalization_matrix();
      dmat33 Q1 = CELL.fractionalization_matrix() * R;
      dmat33 Q1tr = Q1.transpose();

      dvect3 TRA = {0,0,0};

      dmat33 ROTtr = ROT.transpose();
      dmat33 Rtr_Dtr = ROTtr * CELL.fractionalization_matrix().transpose();

      terms = dag::Term(Q1tr,TRA,Rtr_Dtr);
    }
    return terms;
  }

  std::set<Identifier>
  Node::used_modlid_pose() const
  { std::set<Identifier> used;
    for (auto item : POSE) used.insert(item.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid_curl() const
  { std::set<Identifier> used;
    if (NEXT.TRANSLATED) used.insert(NEXT.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid_gyre() const
  { std::set<Identifier> used;
    for (auto item : NEXT.GYRE) used.insert(item.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid_turn() const
  { std::set<Identifier> used;
    if (NEXT.PRESENT) used.insert(NEXT.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid_seek() const
  { std::set<Identifier> used;
    for (auto item : SEEK) used.insert(item.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid_full() const
  { std::set<Identifier> used;
    if (FULL.PRESENT) used.insert(FULL.IDENTIFIER);
    if (PART.PRESENT) used.insert(PART.IDENTIFIER);
    return used; }

  std::set<Identifier>
  Node::used_modlid(bool use_seek) const
  {
    std::set<Identifier> used;
    for (auto item : HOLD) used.insert(item.IDENTIFIER);
    if (use_seek) for (auto item : SEEK) used.insert(item.IDENTIFIER);
    if (NEXT.PRESENT) used.insert(NEXT.IDENTIFIER);
    for (auto item : NEXT.GYRE) used.insert(item.IDENTIFIER);
    for (auto item : POSE) used.insert(item.IDENTIFIER);
    if (FULL.PRESENT) used.insert(FULL.IDENTIFIER);
    if (PART.PRESENT) used.insert(PART.IDENTIFIER);
    return used;
  }

  double
  Node::scattering()
  {
    double totalscat(0);
    if (FULL.PRESENT)
    {
      //replace the value?? full is just full?
      totalscat += FULL.ENTRY->SCATTERING;
    }
    //hack, the others takes precedence
    if (POSE.size() or NEXT.PRESENT or NEXT.ngyre())
    {
      totalscat = 0;
    }
    for (int p = 0; p < POSE.size(); p++)
    {
      totalscat += POSE[p].ENTRY->SCATTERING;
    }
    if (NEXT.PRESENT and !NEXT.ngyre()) //because the curl will have the turn identifier
    {
      totalscat += NEXT.ENTRY->SCATTERING;
    }
    for (int g = 0; g < NEXT.ngyre(); g++)
    {
      totalscat += NEXT.GYRE[g].ENTRY->SCATTERING;
    }
    return totalscat;
  }

  double
  Node::fraction_scattering(double ztotalscat)
  {
    return scattering()/ztotalscat;
  }

  double
  Node::scattering_seek()
  {
    double totalscat(0);
    int nmol = TNCS_MODELLED ? 1 : TNCS_ORDER;
    for (int s = 0; s < SEEK.size(); s++)
    {
      totalscat += SEEK[s].ENTRY->SCATTERING;
    }
    return totalscat*nmol;
  }

  double
  Node::fraction_scattering_seek(double ztotalscat)
  {
    PHASER_ASSERT(ztotalscat);
    return scattering_seek()/ztotalscat;
  }

  void
  Node::set_star_from_pose()
  {
    for (int s = 0; s < SEEK.size(); s++)
      SEEK[s].set_missing();
    int nmol = TNCS_MODELLED ? 1 : TNCS_ORDER;
    for (int p = 0; p < POSE.size(); p+=nmol)
    { //find this pose in the seek list
      for (int s = 0; s < SEEK.size(); s++)
      {
        if (SEEK[s].IDENTIFIER == POSE[p].IDENTIFIER and SEEK[s].is_missing())
        {
          SEEK[s].set_found();
          break; //because we have found one, next cycle of pose may find another (the next)
        }
      }
    }
    //sort the found seeks to the start of the list
    //seek order may have fused ones non-continuous, random order
    std::stable_sort(SEEK.begin(),SEEK.end());
    //operator< defined on star
    SEEK_COMPLETE = true;
    for (int s = 0; s < SEEK.size(); s++)
    {
      if (SEEK[s].is_missing())
      {
        SEEK_COMPLETE = false;
        break;
      }
    }
  }

  void
  Node::reset_parameters(double ztotalscat)
  {
    for (int p = 0; p < POSE.size(); p++)
      POSE[p].FS = POSE[p].ENTRY->SCATTERING/ztotalscat;
    for (int g = 0; g < NEXT.ngyre(); g++)
      NEXT.GYRE[g].FS = NEXT.GYRE[g].ENTRY->SCATTERING/ztotalscat;
    if (NEXT.IDENTIFIER.is_set_valid())
      NEXT.FS = NEXT.ENTRY->SCATTERING/ztotalscat;
    if (FULL.IDENTIFIER.is_set_valid())
      FULL.FS = FULL.ENTRY->SCATTERING/ztotalscat;

    for (int p = 0; p < POSE.size(); p++)
      POSE[p].DRMS_SHIFT = DRMS_SHIFT[POSE[p].IDENTIFIER].VAL;
    for (int g = 0; g < NEXT.ngyre(); g++)
      NEXT.GYRE[g].DRMS_SHIFT = DRMS_SHIFT[NEXT.GYRE[g].IDENTIFIER].VAL;
    if (NEXT.IDENTIFIER.is_set_valid())
      NEXT.DRMS_SHIFT = DRMS_SHIFT[NEXT.IDENTIFIER].VAL;
    if (FULL.IDENTIFIER.is_set_valid())
      FULL.DRMS_SHIFT = DRMS_SHIFT[FULL.IDENTIFIER].VAL;
  }

  void
  Node::cleanup_drms_shift_cell_scale()
  {
    typedef mapnode_t::const_iterator citer;
    auto used = used_modlid(); //uuid
    for (citer iter = DRMS_SHIFT.cbegin(); iter != DRMS_SHIFT.cend() ; )
      iter = used.count(iter->first) ? std::next(iter) : DRMS_SHIFT.erase(iter);
    for (citer iter = VRMS_VALUE.cbegin(); iter != VRMS_VALUE.cend() ; )
      iter = used.count(iter->first) ? std::next(iter) : VRMS_VALUE.erase(iter);
    for (citer iter = CELL_SCALE.cbegin(); iter != CELL_SCALE.cend() ; )
      iter = used.count(iter->first) ? std::next(iter) : CELL_SCALE.erase(iter);
  }

  bool
  Node::initKnownMR(double testreso,double upperB,double lowerB)
  {
  //----------- Bfactors
    // Make sure that B-factor-corrected scattering at resolution limit does not exceed total
    // In refinement this should be kept under control by getMaxDistSpecial, but solutions
    // provided manually as input or assembled in amalgamation step may violate this constraint.
    // If so, increase B-factors of all placed components.
    double fracmodeled(0);
    double kssqr = -2*fn::pow2(1/testreso)/4;
    for (int p = 0; p < POSE.size(); p++)
    {
      fracmodeled += POSE[p].FS;
      POSE[p].BFAC = std::max(POSE[p].BFAC,lowerB);
      POSE[p].BFAC = std::min(POSE[p].BFAC,upperB);
    }
    if (NEXT.PRESENT)
    {
      //the next component does not have a BFAC, ie it is zero
      //but this component affects the bfac of the poses through the fs
      fracmodeled += NEXT.FS;
    }
    double fracleft(1.-fracmodeled);
    bool overshot;
    int adjusted = false;
    do
    {
      overshot = false;
      double fracmodeled_B(0);
      for (int p = 0; p < POSE.size(); p++)
        fracmodeled_B += POSE[p].FS*std::exp(kssqr*POSE[p].BFAC);
      double tolB = DEF_PPT;
      double fracleftmin = fracleft*std::exp(kssqr*(upperB-tolB)); // Minimum B-weighted scattering left
      double fracleft_B = 1.-fracmodeled_B;
      if (fracleft_B < fracleftmin)
      { // What deltaB is needed to leave enough scattering at high resolution?
        double deltaB = 2.*fn::pow2(testreso)*std::log(fracmodeled_B/(1.-fracleftmin));
        for (int p = 0; p < POSE.size(); p++)
        {
          double Bfac(POSE[p].BFAC);
          POSE[p].BFAC = (Bfac+deltaB);
        }
        //put upper B back in range if it overshot
        for (int p = 0; p < POSE.size(); p++)
        {
          if (POSE[p].BFAC > upperB-0.9*tolB) // Keep reasonably well within limits
          {
            POSE[p].BFAC = (upperB-tolB);
            adjusted = true;
            overshot = true;
          }
        }
      }
    } while (overshot); // Run through again if upperB limit restricted correction
    return adjusted;
  }

  void
  Node::pose_clear()
  {
    NEXT.TRANSLATED = false;
    NEXT.GYRE.clear();
    POSE.clear();
    FULL.PRESENT = NEXT.PRESENT = PART.PRESENT = false;
    FULL.IDENTIFIER = NEXT.IDENTIFIER = PART.IDENTIFIER = Identifier();
  }

  void
  Node::hold_clear()
  {
    HOLD.clear();
  }

  bool
  Node::is_all_full() const
  {
    if (!FULL.PRESENT) return false;
    if (NEXT.PRESENT) return false;
   // if (node.POSE.size()) return false;
   //can have poses, because may want to recycle to frf after rbm and sbr (which create full)
    return true;
  }

  bool
  Node::turn_is_novel() const
  { //if a modlid is shared in gyre and in pose, then don't refine vrms
    //because the vrms from the pose will be more accurate
    auto used_pose = used_modlid_pose();
    auto used_turn = used_modlid_turn();
    for (const auto& modlid : used_turn)
      if (used_pose.count(modlid))
        return false;
    return true;
  }

  void
  Node::set_cell(af::double6 uc)
  { //convert fractional coordinates to new cell upon setting cell
    cctbx::uctbx::unit_cell newcell(uc);
    for (int p = 0; p < POSE.size(); p++)
    {
      POSE[p].FRACT = CELL.orthogonalization_matrix()*POSE[p].FRACT;
      POSE[p].FRACT = newcell.fractionalization_matrix()*POSE[p].FRACT;
    }
    if (NEXT.TRANSLATED)
    {
      NEXT.FRACT = CELL.orthogonalization_matrix()*NEXT.FRACT;
      NEXT.FRACT = newcell.fractionalization_matrix()*NEXT.FRACT;
    }
    CELL = newcell;
  }

  double
  Node::search_resolution(double io_hires) const
  {
    double res1 = io_hires ? io_hires : SIGNAL_RESOLUTION;
    double res2 = io_hires ? io_hires : SEEK_RESOLUTION;
    return std::max(res1,res2);
  }

  af_string
  Node::logSigmaa(std::string description) const
  {
    af_string output;
    output.push_back("SAD Variance Parameters: " + description);
    output.push_back(snprintftos(
        "  %3s %9s %9s %9s %9s %9s %9s %9s %9s %9s",
        "#","(MidRes)","MidSsqr","SigmaN","SigmaP","SigmaQ","SigmaH","|rhoFF|","fDelta","rhopm"));
    for (int s = 0; s < SADSIGMAA.MIDSSQR.size(); s++)
      output.push_back(snprintftos(
        "  %3d (%7.2f) %9.4g %9.4g %9.4g %9.4g %9.4g %9.4g %9.6f %9.6f",
        s+1, 1./std::sqrt(SADSIGMAA.MIDSSQR[s]), SADSIGMAA.MIDSSQR[s],
        SADSIGMAA.SigmaN_bin[s],
        SADSIGMAA.SigmaP_bin[s],
        SADSIGMAA.SigmaQ_bin[s],
        SADSIGMAA.SigmaH_bin[s],
        SADSIGMAA.absrhoFF_bin[s],
        SADSIGMAA.fDelta_bin[s],
        SADSIGMAA.rhopm_bin[s]
    ));
    output.push_back("");
    return output;
  }

  std::string
  Node::shift_str(std::string select)
  {
    PHASER_ASSERT(NEXT.ngyre() > 0);
    sv_string txt(3);
    for (auto& item : txt)
      item += ":{";
    for (int g = 0; g < NEXT.ngyre(); g++)
    {
      dag::Gyre& gyre = NEXT.GYRE[g];
      auto drms = DRMS_SHIFT[gyre.IDENTIFIER].VAL;
      auto vrms = VRMS_VALUE[gyre.IDENTIFIER].VAL;
      phaser_assert(CELL_SCALE.count(gyre.IDENTIFIER));
      txt[0] = dtos(gyre.delta_angle(),5,(gyre.delta_angle() >= 10) ? 2 : 3) + " ";
      txt[1] += dtos(drms,5,2,true) + " ";
      txt[2] += dtos(vrms,5,(vrms >= 10) ? 2 : 3) + " ";
    }
    if (NEXT.ngyre()) for (auto& item : txt) item.pop_back();
    if (NEXT.ngyre()) for (auto& item : txt) item += "}";
    if (NEXT.ngyre())
      return "(" + txt[0] + ") (" + txt[1] + ")=(" + txt[2] + ")";
    return "(" + txt[0] + ")  " + txt[1] + "=" + txt[2] + "";
  }

  std::string
  Node::tncs_vector() const
  { //for boosting to python: returns string, which can be passed back/manipulated
    return dvtos(TNCS_VECTOR,8,5);
  }

  void
  Node::squash_orthogonal_perturbations()
  {
    dmat33 orthmat = CELL.orthogonalization_matrix();
    dmat33 fracmat = CELL.fractionalization_matrix();
    dmat33 ROT;dvect3 FRACT;
    for (int p = 0; p < POSE.size(); p++)
    {
      dag::Pose& pose = POSE[p];
      const dvect3& PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
      const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
      std::tie(ROT,FRACT) = pose.finalRT(orthmat,fracmat);
      pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT);
      pose.FRACT = FRACT;
      std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(pose.EULER,PR);
      pose.SHIFT_ORTHT = orthmat*dag::fract_wrt_in_from_mt(pose.FRACT,ROT,PT,CELL);
      pose.ORTHR = dvect3(0,0,0);
      pose.ORTHT = dvect3(0,0,0);
    }
  }

}} //phasertng
