#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/math/table/alogchI0.h>
#include <phasertng/math/table/alogch.h>

namespace phasertng {
extern const table::alogchI0& tbl_alogchI0;
extern const table::alogch& tbl_alogch;
namespace likelihood {

  double ELLG::get(double Eeff, double DobsSigA, bool cent)
  {
    // These should ideally include effect of tNCS, which would involve
    // accounting for the perturbation of the Rice function variance term.
    if (DobsSigA < 0.01)
      return 0.; // Value would be around 10^-6 at cutoff
    return cent ?centric(Eeff,DobsSigA) :acentric(Eeff,DobsSigA);
  }

  double ELLG::centric(double Eeff, double DobsSigA)
  {
    // Centric: integrate over possible values of Ecalc using trapezoidal rule
    // Integrand contains a Gaussian term with mean of DobsSigA*Eeff
    // and standard deviation of sqrt(1-DobsSigA^2), so integrate from
    // zero to mean plus 6 standard deviations.
    // For probability-weighted integral, p(Ec;Eeff)=p(Ec)*exp(LLGI)
    // 200 points is sufficient to obtain result to about 8 decimal point accuracy
    double eLLG(0.);
    double dosaSqr(DobsSigA*DobsSigA);
    double V(1.-dosaSqr);
    if (V < 0) throw Error(err::FATAL,"Negative variance"); //eg b-factor too negative
    double sigma(std::sqrt(V));
    double Ecmax(DobsSigA*Eeff + nsigma*sigma);
    double dEc(Ecmax/(npts-1));
    double EOsqr(Eeff*Eeff);
    for (int i=0; i < npts; i++)
    {
      double wt = 2;
      if (i==0 || i==npts-1) wt = 1; // Half-weight at ends
      double Ecalc = i*dEc;
      double ECsqr = (Ecalc*Ecalc);
      double pECen = SQRT2BYPI*std::exp(-ECsqr/2);
      double X = DobsSigA*Eeff*Ecalc/V;
      double reflLL = -(std::log(V)+dosaSqr*(EOsqr + ECsqr)/V)/2. + tbl_alogch.get(X);
      eLLG += wt*pECen*std::exp(reflLL)*reflLL;
    }
    eLLG *= dEc/2;
    return eLLG;
  }

  double ELLG::acentric(double Eeff, double DobsSigA)
  {
    // Acentric: integrate over possible values of Ecalc using midpoint rule, as
    // value at zero argument is zero because of p(E) for acentric.
    // Integrand contains a Gaussian term with mean of DobsSigA*Eeff
    // and standard deviation of sqrt((1-DobsSigA^2)/2), so integrate from
    // zero to mean plus 6 standard deviations. 200 points is sufficient
    // to obtain result to about 8 decimal point accuracy
    double eLLG(0.);
    double dosaSqr(DobsSigA*DobsSigA);
    double V(1.-dosaSqr);
    if (V < 0) throw Error(err::FATAL,"Negative variance"); //eg b-factor too negative
    double sigma(std::sqrt(V/2.));
    double Ecmax(DobsSigA*Eeff + nsigma*sigma);
    double dEc(Ecmax/npts);
    double EOsqr(Eeff*Eeff);
    for (int i=0; i < npts; i++)
    {
      double Ecalc = (i+0.5)*dEc;
      double ECsqr = (Ecalc*Ecalc);
      double pEAcen = 2.*Ecalc*std::exp(-ECsqr);
      double X = 2.*DobsSigA*Eeff*Ecalc/V;
      double reflLL = -(std::log(V)+dosaSqr*(EOsqr + ECsqr)/V) + tbl_alogchI0.get(X);
      eLLG += pEAcen*std::exp(reflLL)*reflLL;
    }
    eLLG *= dEc;
    return eLLG;
  }

}
}
