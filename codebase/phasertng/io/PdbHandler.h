//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PdbHandler_class__
#define __phasertng_PdbHandler_class__
#include <phasertng/main/includes.h>
#include <phasertng/io/PdbRecord.h>
#include <phasertng/io/FileSystem.h>

namespace phasertng {

class SpaceGroup;
class UnitCell;

class PdbHandler
{
  private: //members
    sv_string CRYSTCARD;
    sv_string REMARK;
    std::vector<af::shared<PdbRecord> > PDB;

  public: //constructors
    PdbHandler() {}
    PdbHandler(SpaceGroup,UnitCell,int);
    ~PdbHandler() {}

  public:
    void
    addCrystCard(SpaceGroup,UnitCell,int);

    void
    addModel()
    { PDB.push_back(af::shared<PdbRecord>()); }

    void
    addModel(af::shared<PdbRecord> MODEL)
    { PDB.push_back(MODEL); }

    void
    addRecord(PdbRecord RECORD_)
    {
      if (!PDB.size()) addModel();
      PDB.back().push_back(RECORD_);
    }

    void
    addVanillaRemark(std::string REMARK_)
    {
      REMARK.push_back(REMARK_);
    }

    void
    addRemarkPhaser(std::string REMARK_);

  public:
    void VanillaWritePdb(FileSystem);

    af_string PdbString();
};

} //phaser
#endif
