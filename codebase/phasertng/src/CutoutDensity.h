//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_CutoutDensity_class__
#define __phasertng_CutoutDensity_class__
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <scitbx/array_family/accessors/c_grid.h>
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;
typedef scitbx::af::versa<double, scitbx::af::c_grid<3> >   versa_grid_double;

namespace phasertng {

  class FileSystem;

  class CutoutDensity
  {
    public:
      af_millnx MILLER_RES_LIMITS;
      //af_cmplex FMAP;
      af_cmplex FCUT;

    private:
      //have to store both, don't know why
      //af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref;
      af::versa<double, af::c_grid<3> > real_map_unpadded;
      UnitCell UC;
      SpaceGroup SG; // space group of crystal

    public:
      double cutout_density_hires = 0;
      af::int3 cutout_density_gridding;

    public:
      CutoutDensity() {}

    public:
      af_string
      calculate
      (
        cctbx::sgtbx::space_group,
        cctbx::uctbx::unit_cell,
        af::shared<millnx>,
        af_cmplex,
        std::pair<dvect3,dvect3>
      );

      void WriteMtz(FileSystem);
  };

}
#endif
