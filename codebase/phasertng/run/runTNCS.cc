//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Hexagonal.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineTNCS.h>
#include <phasertng/math/xyzRotMatDeg.h>
#include <phasertng/data/Information.h>
#include <phasertng/data/Selected.h>
#include <phasertng/math/xyzRotMatDeg.h>

namespace phasertng {

  //tNCS_correction_refinement
  void Phasertng::runTNCS()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::ANISO))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (aniso)");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");

    dvect3 io_tncs_vector =  input.value__flt_vector.at(".phasertng.tncs.vector.").value_or_default();
    int    io_tncs_order = input.value__int_number.at(".phasertng.tncs.order.").value_or_default();
    ivect3 io_tncs_parity = input.value__int_vector.at(".phasertng.tncs.parity.").value_or_default();

    bool tncs_order_one = false;
    if (input.value__int_number.at(".phasertng.tncs.order.").is_default())
      throw Error(err::INPUT,"tNCS order not set");
    else if (io_tncs_order == 1)
      tncs_order_one = true;
    else if (input.value__flt_vector.at(".phasertng.tncs.vector.").is_default())
      throw Error(err::INPUT,"tNCS vector not set");

    if (REFLECTIONS->MAP)
      logWarning("tNCS correction using Amplitudes from Map not Intensity data");

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.mactncs.resolution.").value_or_default());
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected("Before tNCS refinement"));
    if (Suite::Extra())
        logTabArray(out::TESTING,selected.logOutliers("Before tNCS refinement"));

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(out::SUMMARY,"tNCS Refinement");
    logTab(out::SUMMARY,"tNCS order  = " + itos(io_tncs_order));
    logTab(out::SUMMARY,"tNCS vector = " + dvtos(io_tncs_vector));
    logTab(out::SUMMARY,"tNCS parity = " + ivtos(io_tncs_parity));

    if (!tncs_order_one)
    { //findROT
      Selected best_selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
      auto seltxt = best_selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = best_selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
      logTabArray(where,seltxt);
      best_selected.flag_standard_outliers_and_select(REFLECTIONS.get());

      cctbx::sgtbx::space_group cctbxPattSG(REFLECTIONS->SG.group().build_derived_patterson_group());
      cctbx::sgtbx::space_group_type PattInfo(cctbxPattSG);
      SpaceGroup PATT("Hall: " + PattInfo.hall_symbol());
      cctbx::sgtbx::site_symmetry this_site_sym(REFLECTIONS->UC.cctbxUC,cctbxPattSG,io_tncs_vector);
      int M = PATT.group().order_z()/this_site_sym.multiplicity();
      double hires = REFLECTIONS->data_hires();
      bool   TNCS_PERTURB =  input.value__boolean.at(".phasertng.tncs.refine.perturb_special_position.").value_or_default();
      if (M > 1 && TNCS_PERTURB)
      {
        io_tncs_vector = this_site_sym.exact_site(); // Put on exact site before perturbing
        logUnderLine(where,"Special Position Perturbation");
        logTab(where,"tNCS vector on special position with M = " + itos(M));
        logTab(where,"tNCS vector = " + dvtos(io_tncs_vector));
        logTab(where,"Starting point for refinement will be shifted from special position.");
        scitbx::vec3<std::string> direction("X","Y","Z");
        dvect3 perturb(hires/6/REFLECTIONS->UC.A(),
                       hires/6/REFLECTIONS->UC.B(),
                       hires/6/REFLECTIONS->UC.C());
        io_tncs_vector -= perturb;
        for (int tra = 0; tra < 3; tra++)
        {
          logTab(where,"tNCS vector has been perturbed in " + direction[tra]);
        }
        logTab(where,"tNCS vector perturbed by (orthogonal angstroms): " + dvtos(REFLECTIONS->UC.orthogonalization_matrix()*(perturb),5,3));
        logTab(where,"tNCS vector has been set to " + dvtos(io_tncs_vector));
        input.value__flt_vector.at(".phasertng.tncs.vector.").set_value(io_tncs_vector);
        double RMSD = input.value__flt_number.at(".phasertng.tncs.rmsd.").value_or_default();
        logTab(where,"tNCS rmsd " + dtos(RMSD));
        logBlank(where);
      }

      double gfunction_radius = input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").value_or_default();
      if (input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").is_default())
      {
        double mol_volume(REFLECTIONS->UC.cctbxUC.volume()/(2*io_tncs_order)/REFLECTIONS->SG.group().order_z());
             //25% of asu volume for tncs order=2
        gfunction_radius = std::pow(3*mol_volume/4/scitbx::constants::pi,1./3.);
        logTab(where,"Molecular Radius for G-function = " + dtos(gfunction_radius) + " Angstroms");
        //set on input so that it will be used subsequently
      }
      const UnitCell& UC = REFLECTIONS->UC;
      double max_gfunction_radius = UC.maximum_gfunction_radius(); //not maxval(), not set
      double min_gfunction_radius = input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").minval();
             max_gfunction_radius = std::max(max_gfunction_radius,min_gfunction_radius);
      if (gfunction_radius < min_gfunction_radius)
      { //1p9i
        logTab(where,"Minium Molecular Radius for G-function = " + dtos(gfunction_radius) + " Angstroms");
        logWarning("Molecular Radius for G-function less than minimum");
      }
      gfunction_radius = std::min(max_gfunction_radius,gfunction_radius);
      gfunction_radius = std::max(min_gfunction_radius,gfunction_radius);
      input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").set_value(gfunction_radius);

      //set up the refinement to start from different orthogonal perturbation angles around
      //the unperturbed point (0,0,0)
      //often get convergence to the same angle from all the perturbations
      sv_dvect3 tncs_sampling_angle;
      if (io_tncs_order > 2 ||
          (!input.value__flt_number.at(".phasertng.tncs.refine.rotation_sampling.").is_default() &&
            input.value__flt_number.at(".phasertng.tncs.refine.rotation_sampling.").value_or_default() == 0))
      {
        logTab(where,"No sampling of of tNCS rotational differences");
        tncs_sampling_angle.push_back(dvect3(0,0,0));
      }
      else //need to set up a list
      {
        double rotation_sampling = 0; //for print in results, default is for no sampling
        if (!input.value__flt_number.at(".phasertng.tncs.refine.rotation_sampling.").is_default())
        {
          rotation_sampling = input.value__flt_number.at(".phasertng.tncs.refine.rotation_sampling.").value_or_default();
        }
        else
        {
          rotation_sampling = std::atan(hires/(4.0*gfunction_radius));
          rotation_sampling = scitbx::rad_as_deg(rotation_sampling);
        }
        logTab(where,"Sampling = " + dtos(rotation_sampling));
        input.value__flt_number.at(".phasertng.tncs.refine.rotation_sampling.").set_value(rotation_sampling);

        double rotational_range = 0; //for print in results, default is for no sampling
        // Rotations around xyz are nearly orthogonal, so use machinery for
        // ORTHOGONAL hexagonal grid generation to generate hexagonal set of rotations.
        // rather than euler angle hexagonal grigs
        UnitCell box(dvect3(1.,1.,1.));
        Hexagonal rotlist(box);
        if (!input.value__flt_number.at(".phasertng.tncs.refine.rotation_range.").is_default())
        {
          logTab(where,"Use input range");
          rotational_range = input.value__flt_number.at(".phasertng.tncs.refine.rotation_range.").value_or_default();
        }
        else
        {
          logTab(where,"Use default range");
          rotational_range = 1.1*rotation_sampling;
        }
        logTab(where,"Range = " + dtos(rotational_range));
        input.value__flt_number.at(".phasertng.tncs.refine.rotation_range.").set_value(rotational_range);

        rotlist.setup_around_point(dvect3(0,0,0),rotation_sampling,rotational_range);
        //select all
        for (int a = 0; a < rotlist.num_sites(); a++)
          if (rotlist.next_site(a)[0] >= 0) //hemisphere
            tncs_sampling_angle.push_back(rotlist.next_site(a));
        //deselect to just list if this is requested (yes, by default)
        logTab(where,"Number of perturbation angles in list: " + itos(tncs_sampling_angle.size()));
        if (input.value__int_numbers.at(".phasertng.mactncs.perturbation.").size())
        {
          sv_int study_perturbation_angles = input.value__int_numbers.at(".phasertng.mactncs.perturbation.").value_or_default();
          logTab(where,"Selecting perturbation angles from list: " + ivtos(study_perturbation_angles));
          sv_dvect3 study_angle;
          for (auto a : study_perturbation_angles)
          {
            phaser_assert(a < tncs_sampling_angle.size());
            study_angle.push_back(tncs_sampling_angle[a]);
          }
          tncs_sampling_angle = study_angle;
        }
        (tncs_sampling_angle.size() == 1) ?
          logTab(where,"There is 1 tNCS rotational sampling perturbation angle"):
          logTab(where,"There are " + itos(tncs_sampling_angle.size()) + " tNCS rotational perturbation angles");
        logBlank(where);
      }

      //now setup to do minimization over tncs_sampling_angle angles
      //below will store the results of each miniimizet
      std::vector<std::tuple<Epsilon,sv_double,double> > refined_epsilon(tncs_sampling_angle.size());

      int best_angle_number(0);
      double bestLLG = std::numeric_limits<double>::lowest(); //LLG, highest is the best

      tncs_sampling_angle.size() == 1 ?
        logTab(out::LOGFILE,"There is 1 tNCS parameter set to refine"):
        logTab(out::LOGFILE,"There are " + itos(tncs_sampling_angle.size()) + " tNCS parameter sets to refine");
    //  logProgressBarStart(out::LOGFILE,"Refining tNCS parameters",tncs_sampling_angle.size());
      logBlank(out::LOGFILE);

      for (int a = 0; a < tncs_sampling_angle.size(); a++)
      {
        logUnderLine(out::LOGFILE,"TNCS REFINEMENT #" + itos(a+1) + " OF " + itos(tncs_sampling_angle.size()));
        if (io_tncs_order == 2)
        logTab(out::LOGFILE,"Rotation angle perturbation: " + dvtos(tncs_sampling_angle[a]));

        Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
        auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
        logTabArray(out::VERBOSE,seltxt);
        seltxt = selected.set_resolution(
          REFLECTIONS->data_hires(),
          DAGDB.resolution_range(),
          input.value__flt_number.at(".phasertng.resolution.").value_or_default());
        logTabArray(out::VERBOSE,seltxt);

        //binwise min and max set internally
        //this mode generates binwise variances which are stored in reflections as bin(=s)/tbin(=var)
        //they are stored per reflection in file then converted to VARBIN[s]
        Epsilon epsilon;
        epsilon.TNCS_ANGLE = tncs_sampling_angle[a];
        epsilon.parse(
        input.value__int_number.at(".phasertng.tncs.order.").value_or_default(),
        input.value__flt_vector.at(".phasertng.tncs.vector.").value_or_default(),
        input.value__flt_number.at(".phasertng.tncs.rmsd.").value_or_default(),
        input.value__flt_number.at(".phasertng.tncs.rmsd.").minval(),
        input.value__flt_number.at(".phasertng.tncs.rmsd.").maxval(),
        input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").value_or_default(),
        input.value__flt_number.at(".phasertng.tncs.gfunction_radius.").minval(),
        REFLECTIONS->UC.maximum_gfunction_radius()
        );

        RefineTNCS refn(REFLECTIONS.get(),
            &selected,
            &epsilon);
        refn.SIGMA_ROT = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.rot.").value_or_default();
        refn.SIGMA_TRA = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.tra.").value_or_default();
        refn.SIGMA_GFNRAD = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.gfn.").value_or_default();
        refn.SIGMA_RMSD = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.rmsd.").value_or_default();
        refn.SIGMA_FS = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.fs.").value_or_default();
        refn.SIGMA_BINS = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.binwise_unity.").value_or_default();
        refn.SIGMA_SMOOTH = input.value__flt_number.at(".phasertng.mactncs.restraint_sigma.smooth.").value_or_default();
        refn.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);

        dtmin::Minimizer minimizer(this);
        bool refine_off =
          input.value__choice.at(".phasertng.mactncs.macrocycle1.").value_or_default_equals("off") and
          input.value__choice.at(".phasertng.mactncs.macrocycle2.").value_or_default_equals("off") and
          input.value__choice.at(".phasertng.mactncs.macrocycle3.").value_or_default_equals("off");
        refine_off = refine_off or
          input.value__choice.at(".phasertng.mactncs.protocol.").value_or_default_equals("off");

        if (refine_off)
        {
          logTab(where,"Refinement off");
        }
        //and then run as normal
        {
          std::vector<sv_string> macro = {
            input.value__choice.at(".phasertng.mactncs.macrocycle1.").value_or_default_multi(),
            input.value__choice.at(".phasertng.mactncs.macrocycle2.").value_or_default_multi(),
            input.value__choice.at(".phasertng.mactncs.macrocycle3.").value_or_default_multi()
          };
          std::vector<int> ncyc = refine_off ? sv_int({0,0,0}) : sv_int({
            input.value__int_vector.at(".phasertng.mactncs.ncyc.").value_or_default(0),
            input.value__int_vector.at(".phasertng.mactncs.ncyc.").value_or_default(1),
            input.value__int_vector.at(".phasertng.mactncs.ncyc.").value_or_default(2)
          });
          auto method = input.value__choice.at(".phasertng.mactncs.minimizer.").value_or_default();
          logProtocol(out::LOGFILE,macro,ncyc,method);
          if (io_tncs_order > 2) //no refinement of angles!!
          {
            //change default refinement to fix rotation - remove rotation refinement
            for (int p = 0; p < macro.size(); p++)
            {
              sv_string::iterator iter = macro[p].begin();
              while (iter != macro[p].end())
              {
                 if (*iter == "rot")
                   iter = macro[p].erase(iter);
                 else iter++;
              }
            }
          }
          minimizer.run(
            refn,
            macro,
            ncyc,
            method,
            input.value__boolean.at(".phasertng.mactncs.study_parameters.").value_or_default(),
            1.0
            );
        }
        refined_epsilon[a] = refn.parameters();
        double LLG = std::get<2>(refined_epsilon[a]);
        if (LLG > bestLLG)
        {
          best_angle_number = a;
          bestLLG = LLG;
          best_selected = *refn.SELECTED;
        }
        //logProgressBarNext(out::LOGFILE);
      }
      //logProgressBarEnd(out::LOGFILE);


      {{
      out::stream where = out::SUMMARY;
      logTableTop(where,"Results of Refinements from Perturbed Starting Rotations");
      logTab(where,snprintftos(
          "#  (%-21s) %-8s",
          "Initial Orth Rot\'n", "( Angle)"));
      logTab(where,snprintftos(
          "#  (%-21s) %-8s %8s   (%-23s) %-5s %-5s %11s",
          "Refined Orth Rot\'n",
          "( Angle)",
          "G-fn-Rad",
          "Refined-Translation",
          "Drms",
          "Dfs",
          "LLG"
         ));
      for (int a = 0; a < tncs_sampling_angle.size(); a++)
      {
        af_double hires = REFLECTIONS->bin_hires(0);
        af_double lores = REFLECTIONS->bin_lores(0);
        af_int numinbin = REFLECTIONS->numinbin();
        xyzRotMatDeg rotmat;
        double startANGL(0),finalANGL(0);
        startANGL = rotmat.xyzAngle(tncs_sampling_angle[a]);
        Epsilon& EPSILON = std::get<0>(refined_epsilon[a]);
        double& LLG = std::get<2>(refined_epsilon[a]);
        finalANGL = rotmat.xyzAngle(EPSILON.TNCS_ANGLE);
        logTab(where,snprintftos(
            "%-2i  (%+6.2f %+6.2f %+6.2f) (%6.2f)",
            a+1,
            tncs_sampling_angle[a][0],
            tncs_sampling_angle[a][1],
            tncs_sampling_angle[a][2],
            startANGL
          ));
        logTab(where,snprintftos(
            "    (%+6.2f %+6.2f %+6.2f) (%6.2f) %8.2f   (%+7.4f %+7.4f %+7.4f) %5.3f %5.3f %11s %c",
            EPSILON.TNCS_ANGLE[0],
            EPSILON.TNCS_ANGLE[1],
            EPSILON.TNCS_ANGLE[2],
            finalANGL,
            EPSILON.TNCS_GFNRAD,
            EPSILON.TNCS_VECTOR[0],
            EPSILON.TNCS_VECTOR[1],
            EPSILON.TNCS_VECTOR[2],
            EPSILON.TNCS_RMS,
            EPSILON.TNCS_FS,
            etos(LLG,6,4).c_str(),
            a == best_angle_number ? '*':' '
          ));
        logTab(out::VERBOSE,snprintftos(
            "%40s%15s%27s",
            "Bin","(Resolution)","(Bin-wise Scale)"));
        for (int s = 0; s < EPSILON.VARBINS.size(); s++) //log the bins
          logTab(out::VERBOSE,snprintftos(
              "%37s#%-4d %6.3f-%6.3f (%6d)    %+10.8f",
               "",
               s+1,
               hires[s],
               lores[s],
               numinbin[s],
               EPSILON.VARBINS[s]));
      }
      logTableEnd(where);
      }}

      Epsilon&  best_epsilon = std::get<0>(refined_epsilon[best_angle_number]);
      sv_double& best_teps = std::get<1>(refined_epsilon[best_angle_number]);
      input.value__flt_vector.at(".phasertng.tncs.vector.").set_value(best_epsilon.TNCS_VECTOR);
      input.value__flt_number.at(".phasertng.tncs.rmsd.").set_value(best_epsilon.TNCS_RMS);

      //transfer to reflections data structure
      REFLECTIONS->TNCS_ORDER = best_epsilon.TNCS_ORDER;
      REFLECTIONS->TNCS_ANGLE = best_epsilon.TNCS_ANGLE;
      REFLECTIONS->TNCS_GFNRAD = best_epsilon.TNCS_GFNRAD;
      REFLECTIONS->TNCS_VECTOR = best_epsilon.TNCS_VECTOR;
      REFLECTIONS->TNCS_RMS = best_epsilon.TNCS_RMS;
      REFLECTIONS->TNCS_FS = best_epsilon.TNCS_FS;
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        int s = REFLECTIONS->get_int(labin::BIN,r);
        REFLECTIONS->set_flt(labin::TEPS,best_teps[r],r);
        phaser_assert(best_epsilon.VARBINS.at(s) != 0);
        REFLECTIONS->set_flt(labin::TBIN,best_epsilon.VARBINS.at(s),r);
      }

      { //scope
      out::stream where(out::LOGFILE);
      logUnderLine(where,"Histogram of Best Epsilon Factors");
     // REFLECTIONS->logNcsEpsn(out::LOGFILE,"Final",this);

      double ticks(io_tncs_order); //for high orders make it divisible by the order e.g 4
      if (io_tncs_order == 2) ticks = 10;
      if (io_tncs_order == 3) ticks = 6;
      int div(io_tncs_order*ticks+1);
      sv_int histogram(div,0);
      int   maxStars(0);
      double  min_epsfac(0),max_epsfac(0),mean_epsfac(0),stdev_epsfac(0);
      { //memory
      sv_double v;
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        if (best_selected.get_selected(r))
        {
          const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
          v.push_back(teps);
          min_epsfac = r ? std::min(teps,min_epsfac) : teps;
          max_epsfac = r ? std::max(teps,max_epsfac) : teps;
          size_t iZ = std::max(0.0,round(teps*ticks));
                 iZ = std::min(iZ,histogram.size()-1);
          histogram[iZ]++;
          maxStars = std::max(maxStars,histogram[iZ]);
        }
      }
      double sum = std::accumulate(v.begin(), v.end(), double(0.0));
      mean_epsfac = sum / v.size();
      sv_double diff(v.size());
      for (int i = 0; i < v.size(); i++) diff[i] = v[i] - 1.0; //deviation about 1
      double sq_sum(0);
      for (int i = 0; i < diff.size(); i++) sq_sum += diff[i]*diff[i] ;
      stdev_epsfac = std::sqrt(sq_sum / v.size());
      } //memory
      double columns = 50;
      double divStarFactor = maxStars/columns;
      int grad = divStarFactor*ticks; // maxStars/5
      logBlank(where);
      logTab(where,"Mid EpsFac Bin Value");
      logTab(where,snprintftos(
          " |    %-10d%-10d%-10d%-10d%-10d%-10d\n",
          0,grad,grad*2,grad*3,grad*4,grad*5));
      logTab(where,snprintftos(
          " V    %-10s%-10s%-10s%-10s%-10s%-10s\n",
          "|","|","|","|","|","|"));
      for (int h = 0; h < div; h++)
      {
        std::string stars;
        stars += dtos(h/ticks,2) + " ";
        for (int star = 0; star < histogram[h]/divStarFactor; star++)
          stars += "*";
        if (histogram[h]) stars += " (" + itos(histogram[h]) + ")";
        logTab(where,stars);
      }
      logBlank(where);
      logTab(where,"Variance about 1: " + dtos(stdev_epsfac));
      }//scope
      {//memory
      Loggraph loggraph;
      double segment(20);
      loggraph.title = "Modulation factors histogram";
      loggraph.scatter = false;
      loggraph.graph.resize(1);
      loggraph.data_labels = "epsilon count";
      loggraph.graph.resize(1);
      loggraph.graph[0] = "Count vs epsilon:AUTO:1,2";
      int div(io_tncs_order*segment+1); //fine sampling for loggraph output
      sv_int histogram(div,0);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        if (best_selected.get_selected(r))
        {
          const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
          size_t iZ = std::max(0.0,round(teps*segment));
                 iZ = std::min(iZ,histogram.size()-1);
          histogram[iZ]++;
        }
      }
      for (int h = 0; h < div; h++)
      {
        loggraph.data_text += dtos(h/segment) + " " +
                              itos(histogram[h]) + " " +
                              "\n";
      }
      logGraph(out::LOGFILE,loggraph);
      }//scope
      REFLECTIONS->TNCS_PRESENT = true; //not indeterminate

      if (Suite::Extra())
        logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Data"));
    }
    else
    {
      logTab(out::SUMMARY,"tNCS not defined: no refinement");
      REFLECTIONS->TNCS_PRESENT = false; //not indeterminate
      REFLECTIONS->TNCS_ORDER = 1;
      REFLECTIONS->TNCS_VECTOR = dvect3(0,0,0);
     // REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.teps.").value_or_default());
     // REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.tbin.").value_or_default());
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        REFLECTIONS->set_flt(labin::TEPS,1.0,r);
        REFLECTIONS->set_flt(labin::TBIN,1.0,r);
      }
    }
    logBlank(out::SUMMARY);

    }}

    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected("After tNCS refinement"));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers("After tNCS refinement"));

    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::TNCS);
    }}

    //change the node
    for (auto& node : DAGDB.NODES)
    {
      node.TNCS_ORDER = io_tncs_order;
      node.TNCS_VECTOR = io_tncs_vector;
    }

    {{
      out::stream where = out::LOGFILE;
      bool io_tncs_modelled = input.value__boolean.at(".phasertng.tncs.present_in_model.").value_or_default();
      if (io_tncs_modelled)
        logTab(where,"tNCS flagged as present in model");
      else
        logTab(where,"tNCS flagged as not present in model");
      for (auto& node : DAGDB.NODES)
      {
        node.TNCS_MODELLED = io_tncs_modelled;
        node.TNCS_PRESENT_IN_MODEL = io_tncs_modelled ? 'Y' : 'N'; //from '?'
      }
      logBlank(where);
    }}

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
