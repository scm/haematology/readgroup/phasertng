import sys,shutil
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.master_phil_file import uuid_w
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.mode_generator import isNone
from iotbx.pdb import crystal_symmetry_from_pdb as from_pdb
import os, os.path
from phasertng.cctbx_project.mmtbx.ncs import tncs
from phasertng.scripts.ncs_from_pdb import convert_tncsresults_to_dict
from phasertng.scripts.twin_results import get_twin_results

#these are internal parameters not interesting for the user
class runRFAC:

  def get_iomtz(self,basetng,node,f_obs_label):
      DAGDB = basetng.DAGDATABASE
      try: #from density
       #subdir = node.FULL.IDENTIFIER.database_subdir()
        subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
        entfile = node.REFLID.other_data_stem_ext(enum_other_data.tiny_mtz)
        iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
        if not os.path.exists(iomtz.fstr()): #from reflections
          subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
          entfile = node.REFLID.other_data_stem_ext(enum_other_data.data_mtz)
          iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
      except Exception as e:
        try: #from reflections
          subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
          entfile = node.REFLID.other_data_stem_ext(enum_other_data.data_mtz)
          iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
        except Exception as e: #from original data
           wf = basetng.WriteFiles()
           if not wf:
             basetng.logAdvisory("Coordinate refine requires files on disk")
             return None, None
           else:
             raise e
        if f_obs_label is None: #because we have not set explicitly eg from __main__
          f_obs_label = reflid_label #backup to the original label
      iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
      return iomtz,f_obs_label


  def defaults(self,log, silent):
    if (not silent):
      print("Default params:\n", file=log)
    master_params_str="""
f_obs_label = None
  .type = str
r_free_flags_label = None
  .type = str
scattering_table = wk1995  it1992  *n_gaussian  neutron
  .type = choice
high_resolution = None
  .type = float
twin_law = None
  .type = str
n_bins = 20
  .type = int
"""
    parsed = iotbx.phil.parse(master_params_str)
    if (not silent):
      parsed.show(prefix="  ", out=log)
      print(file=log)
    return parsed

  def reflection_file_server(self,crystal_symmetry, reflection_files):
    return reflection_file_utils.reflection_file_server(
      crystal_symmetry=crystal_symmetry,
      force_symmetry=True,
      reflection_files=reflection_files,
      err=StringIO())

  def run_from_basetng_and_input(self,basetng):
    io_tncs_correction=(basetng.input.get_as_str(".phasertng.refinement.tncs_correction."))
    io_tncs_correction = bool(io_tncs_correction.strip() == "True")
    self.inner_rfactor(basetng=basetng,
      reflid_label=basetng.input.get_as_str(".phasertng.labin.inat."),
      io_top_files=basetng.input.get_as_str(".phasertng.refinement.top_files."),
      tncs_correction=io_tncs_correction,
      high_resolution=basetng.input.get_as_str(".phasertng.refinement.resolution."))

  def report_rfactors(self,basetng):
    DAGDB = basetng.DAGDATABASE
    if (basetng.input.generic_int == 4):
   #{{{
        #see runFIND.py
        io_top_files=basetng.input.get_as_str(".phasertng.refinement.top_files.")
        basetng.logUnderLine(enum_out_stream.summary,"Report previous refinement")
        basetng.logTab(enum_out_stream.summary,"Results of a previous refinement with identical poses")
        nposes = DAGDB.NODES.number_of_poses()
        basetng.logTab(enum_out_stream.summary,"Poses: " + str(nposes))
       #bring in the nodes that are refined
        refined_dag =  basetng.input.generic_string
        posedag = DAGDB.cards() #original
        firstnodes = posedag.split(node_separator)
        if len(firstnodes[-1]) < 10: # 1 is actually enough, dud final line
          firstnodes = firstnodes[:-1]
        assert(len(firstnodes) == int(DAGDB.size()))
        n = min(len(firstnodes),int(io_top_files))
        basetng.logTab(enum_out_stream.summary,"Maximum number of nodes = " + str(n))
        underline = True
        basetng.logTableTop(enum_out_stream.summary,"R-factor Results",underline)
        basetng.logTab(enum_out_stream.summary,'{:5} {:>10} {:30}'.format("#","R-factor","Identifier/Filename"))
        zscores = []
        rfactors = []
        unique = []
        for i in range(n):
          assert(i < len(firstnodes))
          #take this dag first nodes and add the refined solutions from external
          allnodes = firstnodes[i] + node_separator + refined_dag
          DAGDB.clear_cards()
          DAGDB.parse_cards(allnodes)
          DAGDB.calculate_duplicate_poses(3.0,False,True,1) # have to find the duplicate again here
          rfactor,zscore,identifier = DAGDB.NODES.duplicate_equivalent(0) # the top (current) has a duplicate in input
          if float(rfactor) > 0 and float(rfactor) < 99:
            basetng.logTab(enum_out_stream.summary,'{:5} {:10.2f} {:s}'.format(str(i+1),float(rfactor),identifier.str()))
            rfactors.append(float(rfactor))
            zscores.append(float(zscore)) #for scale of graph_report.html
          else:
            basetng.logTab(enum_out_stream.summary,'{:5} {:>10} {:s}'.format(str(i+1),"---","---"))
            unique.append(firstnodes[i])
            rfactors.append(float(0))
            zscores.append(float(0)) #for scale of graph_report.html
        if False:
          #so this will be shorter than max refined if there are known R-factors
          #replace the nodes with the nodes that don't have the R-factors calculated yet
          allnodes = ""
          for i in range(len(unique)):
            allnodes = unique[i] + node_separator
        DAGDB.clear_cards()
        DAGDB.parse_cards(posedag) #go back to riginal
        #store the rfactor on the node
        for i in range(len(rfactors)):
          assert(i < int(DAGDB.NODES.size()))
          node = DAGDB.NODES.LIST[i];
          node.ANNOTATION = node.ANNOTATION + " R=" +str(rfactors[i]) #DUPLICATE RFACTOR
          node.ZSCORE = float(zscores[i]) #for scale of graph_report.html
          node.RFACTOR = float(rfactors[i])
          DAGDB.NODES.set_node(i,node) # then clear and overwrite below
        basetng.logTableEnd(enum_out_stream.summary)
        basetng.logBlank(enum_out_stream.summary)
   #}}}

  def inner_rfactor(self,
          basetng,
          reflid_label,
          io_top_files,
          tncs_correction,
          high_resolution,
          f_obs_label=None):
 #{
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0):
   #{
      basetng.logTab(enum_out_stream.logfile,"No nodes")
      return
   #}
    DAGDB.shift_tracker(str(basetng.hashing()),"rfac")
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    wf = basetng.Write()
    io_directory = os.path.join(basetng.DataBase(),subdir)
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(io_directory):
        os.makedirs(io_directory)

    if (basetng.input.generic_int == 4):
      self.report_rfactors(basetng)
      return

   #if (!(DAGDB.NODES.is_all_pose() or DAGDB.NODES.has_no_turn_gyre_curl_pose()))
    basetng.logTab(enum_out_stream.logfile,"Phenix R-factor calculation with bulk-solvent terms")
    basetng.logTab(enum_out_stream.logfile,"Resolution for calculation = " + str(high_resolution))
    basetng.logUnderLine(enum_out_stream.verbose,"Seek Information")
    lines = DAGDB.seek_info_all().split('\n')
    for line in lines:
      basetng.logTab(enum_out_stream.verbose,line)
    basetng.logBlank(enum_out_stream.verbose)
    #the fact that all the files start in the one directory assures that the filenames are unique
    #end hence the tags will be unique, when based on the file stem
    #not first so don't initialize for twilight zone
    io_top_files = int(io_top_files)
    top_files = min(len(DAGDB.NODES.LIST),io_top_files)
    rfactors = []
    for i,node in enumerate(DAGDB.NODES.LIST):
      node.RFACTOR = 100.
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
    node = basetng.DAGDATABASE.NODES.LIST[0]
    iomtz,f_obs_label = self.get_iomtz(basetng,node,f_obs_label)
    if iomtz is None:
      return
    if not os.path.exists(iomtz.fstr()):
      raise ValueError(enum_err_code.fileopen,iomtz.fstr())
    twinned = bool(node.TWINNED == "Y")
    twin_results = get_twin_results(basetng,twinned,iomtz)
    twin_results.TWINNED = twinned #add to class
    twin_results.io_sigtwinfrac=float(0.20) #add to class
    for i,node in enumerate(DAGDB.NODES.LIST):
   #{
      if ("SYM=N*" not in node.ANNOTATION): #analysis only, the ones matching input. SYM=Y* also but rare
        if (i >= io_top_files and io_top_files > 0):
          continue #because there might be another one that is input interesting
      basetng.logUnderLine(enum_out_stream.logfile,"Phenix R-factor #" + str(i+1) + " of " + str(top_files) + " [of " + str(len(DAGDB.NODES.LIST)) + " total]")
      if not node.FULL.PRESENT:
        basetng.logTab(enum_out_stream.logfile,"No coordinates (\"full\" not present in node), skipping")
        DAGDB.NODES.set_node(i,node) #python not iterate by reference
        continue
      subdir = DAGDB.lookup_modlid(node.FULL.IDENTIFIER).database_subdir()
      entfile = node.FULL.IDENTIFIER.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
      iopdb = FileSystem(basetng.DataBase(),subdir,entfile)
      basetng.logTabArray(enum_out_stream.logfile,node.logAnnotation("",True))
      basetng.logTabArray(enum_out_stream.verbose,node.logNode(""))
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iopdb.qfstrq())
      if not os.path.exists(iopdb.fstr()):
        basetng.logTab(enum_out_stream.logfile,"File does not exist")
        basetng.logTab(enum_out_stream.verbose,iopdb.fstr())
        DAGDB.NODES.set_node(i,node) #python not iterate by reference
        continue
      #can either call the original reflection file here or the density file
      #if calling reflid then the f_obs_label as to be passed because it
      # cannot be deduced automatically
      #whereas it is deduced from the density file (fewer data items)
      iomtz,f_obs_label = self.get_iomtz(basetng,node,f_obs_label)
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iomtz.qfstrq())
      if f_obs_label is not None:
        basetng.logTab(enum_out_stream.logfile,"Column Label: " + str(f_obs_label))
      if not os.path.exists(iomtz.fstr()):
        raise ValueError(enum_err_code.fileopen,iomtz.fstr())
      #find the actual resolution of the data
      mtz_obj = iotbx.mtz.object(iomtz.fstr())
      mtzres = sorted(mtz_obj.max_min_resolution())[0]
      hires = max(float(high_resolution),float(mtzres))
      basetng.logTab(enum_out_stream.logfile,"Space Group: " + str(node.sg_info()))
      cmd_cs = from_pdb.extract_from(file_name = iopdb.fstr())
      node.RFACTOR = 0
      DAGDB.NODES.set_node(i,node) #python not iterate by reference

      try:
     #{
        rwork,rfree,tncsresults,best_twin_law = self.get_rfactors(basetng,
                               f_obs_label=f_obs_label,
                               tncs_correction=tncs_correction,
                               high_resolution=hires,
                               twin_results=twin_results,
                               cmd_cs=cmd_cs,
                               iopdb = iopdb,
                               iomtz = iomtz,
                               )
        node.RFACTOR = rwork
        if (float(node.RFACTOR) < 100):
          node.ANNOTATION = node.ANNOTATION + " R="+str(round(node.RFACTOR))+"%"
          DAGDB.add_entry_copy_header(node.TRACKER.ULTIMATE,node.FULL.IDENTIFIER)
          node.FULL.IDENTIFIER = node.TRACKER.ULTIMATE
          DAGDB.NODES.set_node(i,node) #python not iterate by reference
        DAGDB.NODES.set_node(i,node) #python not iterate by reference
        basetng.logBlank(enum_out_stream.logfile)
        twinlaw = "N" if best_twin_law is None else "Y"
        if (best_twin_law is not None): #hkl
          basetng.logChevron(enum_out_stream.logfile,"Best Twin Law")
          basetng.logTab(enum_out_stream.logfile,"Twin law = " + best_twin_law)
        basetng.logTab(enum_out_stream.logfile,"R-work = " + "{:.2f}".format(node.RFACTOR))
        basetng.logTab(enum_out_stream.verbose,"R-work/R-free = " + "{:.2f}".format(node.RFACTOR) + " " + "{:.2f}".format(rfree))
        basetng.logTab(enum_out_stream.logfile,"Identifier = " + str(node.TRACKER.ULTIMATE.str()))
        basetng.logBlank(enum_out_stream.logfile)
        rfactors.append(
                        {'rwork':float(node.RFACTOR),
                         'rfree':float(rfree),
                         'llg':float(node.LLG),
                         'ident': node.TRACKER.ULTIMATE.str(),
                         'info' : str(node.pose_info()),
                         'tncs_info' : str(node.tncs_info()),
                         'twinned' : str(twinlaw),
                         'hires' : float(hires),
                         'sg_info' : str(node.sg_info())})
        basetng.logBlank(enum_out_stream.logfile)
        self.print_tncs(basetng,tncsresults)
        wf = basetng.WriteData()
        if True:
       #{
          basetng.logChevron(enum_out_stream.logfile,"Database Entries")
          modlid = node.TRACKER.ULTIMATE
          subdir = DAGDB.lookup_modlid(modlid).database_subdir()
          if wf and not FileSystem().flat_filesystem() and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
              os.makedirs(os.path.join(basetng.DataBase(),subdir))
          pdbout = modlid.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
          newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Coordinates",newpdbname)
          if (wf):
            shutil.copyfile(iopdb.fstr(), newpdbname.fstr()) #straight copy of unmodified coordinates
        # else:
        #   lines = iotbx.file_reader.any_file(iopdb.fstr()).as_pdb_string()
        #   for line in lines:
        #     basetng.input.generic_string = basetng.input.generic_string + line + node_separator
          basetng.input.set_as_str(".phasertng.pdbout.filename.",newpdbname.fstr())
          basetng.input.set_as_str(".phasertng.pdbout.pdbid.",newpdbname.fstr())
          basetng.input.set_as_str(".phasertng.pdbout.id.",str(modlid.identifier()))
          basetng.input.set_as_str(".phasertng.pdbout.tag.",modlid.tag())
          #since this file is written directly, we should load into memory so that dag records it as being present
          basetng.load_entry_from_database(enum_out_stream.logfile,enum_entry_data.coordinates_pdb,modlid,False)
         #basetng.logBlank(enum_out_stream.logfile)
          filesystem = FileSystem(basetng.DataBase(),modlid.reflections_and_models(),modlid.other_data_stem_ext(enum_other_data.entry_cards))
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Entry cards",filesystem)
          dagcards = DAGDB.entry_unparse_modlid(modlid)
          if (wf):
           filesystem.write(dagcards);
       #}
     #}
      except Exception as e:
        basetng.logWarning(str(e))
   #}
    if len(rfactors) > 0:
   #{
      basetng.logTableTop(enum_out_stream.summary,"R-factor Summary",True)
      basetng.logTab(enum_out_stream.summary,"Poses: " + str(DAGDB.NODES.number_of_poses()))
      width = int(uuid_w)+len("-rfac-0001") #pessimistic
    #R-free not valid as there is no refinement!
      basetng.logTab(enum_out_stream.summary,
           '{:8s}'.format("R-work") + " " +
        #  '{:8s}'.format("R-free") + " " +
           '{:>10s}'.format("LLG") + "  " +
           '{:{w}s}'.format("Identifier",w=width) + " " +
           '{:13s}'.format("Space Group") + " " +
           '{:s}'.format("B-factors"))
      for item in sorted(rfactors,key = lambda i: i['rwork']):
        rdict = item
        basetng.logTab(enum_out_stream.summary,
           '{:8.2f}'.format(rdict['rwork']) + " " +
        #  '{:8.2f}'.format(rdict['rfree']) + " " +
           '{:10.0f}'.format(rdict['llg']) + "  " +
           '{:{w}s}'.format(rdict['ident'],w=width) + " " +
           '{:13s}'.format(rdict['sg_info']) + " " +
           '{:s}'.format(' '.join(rdict['info'].split())))
      basetng.logTableEnd(enum_out_stream.summary)
   #}
    if len(rfactors) > 0:
   #{
      wf = basetng.WriteFiles()
      if (wf):
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        tmpfile = str(enum_other_data.solutions_log).replace("_",".")
        flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
        if not flat and wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
          os.makedirs(os.path.join(basetng.DataBase(),subdir))
        newlogname = FileSystem(basetng.DataBase(),subdir,tmpfile)
        basetng.logFileWritten(enum_out_stream.logfile,wf,"R-factors",newlogname)
        sel = "Phenix"
        #if this is changed change runPREF and runTREE (col = 16) also
        with open(newlogname.fstr(), "w") as f:
          for rdict in sorted(rfactors,key = lambda i: i['rwork']):
            line =("Solution: " +
              sel + ' R-factor = {:5.2f}'.format(float(rdict["rwork"])) +
              " dmin = {:5.2f}".format(float(rdict["hires"])) +
              ' poses = {:2d}'.format(DAGDB.NODES.number_of_poses()) +
              ' sg = {:<7} '.format(rdict["sg_info"]) +
              " tncs = " + str(rdict["tncs_info"]) +
              " twin = " + str(rdict["twinned"]) +
              " " + str(subdir) +
              " " + str(rdict["ident"]))
            basetng.logTab(enum_out_stream.logfile,line)
            f.write(line+"\n")
   #}
 #}

  def rfactor(self,
          args,
          f_obs_label = None,
          cmd_cs = None,
          reflid_label = None,
          tncs_correction = True,
          high_resolution = None,
          twin_law = None,
          outstr = TextIO()):
        # log = sys.stdout):
    parsed = self.defaults(log=outstr, silent=True)
    #
    processed_args = utils.process_command_line_args(
      args = args,
      cmd_cs=cmd_cs,
      log = outstr,
      master_params = parsed,
      suppress_symmetry_related_errors = True)
    params = processed_args.params.extract()
    if(f_obs_label is not None):
      params.f_obs_label = f_obs_label
    #
    reflection_files = processed_args.reflection_files
    if(len(reflection_files) == 0):
      raise ValueError(enum_err_code.input,"No reflection file found")
    crystal_symmetry = processed_args.crystal_symmetry
    if(crystal_symmetry is None):
      raise ValueError(enum_err_code.input,"No crystal symmetry found")
    if(len(processed_args.pdb_file_names) == 0):
      raise ValueError(enum_err_code.input,"No PDB file found")
    pdb_file_names = processed_args.pdb_file_names
    #
    rfs = self.reflection_file_server(
      crystal_symmetry = crystal_symmetry,
      reflection_files = reflection_files)
    parameters = extract_xtal_data.data_and_flags_master_params().extract()
    if(params.f_obs_label is not None):
      parameters.labels = params.f_obs_label
    if(params.r_free_flags_label is not None):
      parameters.r_free_flags.label = params.r_free_flags_label
    if (params.high_resolution is not None):
      parameters.high_resolution = float(params.high_resolution)
    if (high_resolution is not None):
      parameters.high_resolution = float(high_resolution)
    if (twin_law is not None):
      parameters.twin_law = str(twin_law)
  # rfactor code has NO TNCS CORRECTION AJM TODO
  # if (tncs_correction is not None):
  #   parameters.tncs_correction = bool(tncs_correction)
    parameters.r_free_flags.generate = False #will already be present in tiny.mtz
    determine_data_and_flags_result = extract_xtal_data.run(
      reflection_file_server  = rfs,
      parameters              = parameters,
    # data_parameter_scope    = "refinement.input.xray_data",
    # flags_parameter_scope   = "refinement.input.xray_data.r_free_flags",
    # data_description        = "X-ray data",
      keep_going              = True)
    # log                     = outstr)
    f_obs = determine_data_and_flags_result.f_obs
    # Data
    f_obs.show_comprehensive_summary(prefix="  ", f = outstr)
    # R-free-flags
    r_free_flags = determine_data_and_flags_result.r_free_flags
    test_flag_value = determine_data_and_flags_result.test_flag_value
    # Model
    pdb_combined = iotbx.pdb.combine_unique_pdb_files(
      file_names=pdb_file_names)
    raw_records = pdb_combined.raw_records
    try:
      pdb_inp = iotbx.pdb.input(source_info = None,
                                lines       = flex.std_string(raw_records))
    except ValueError as e :
      raise ValueError(enum_err_code.fatal,"Model format (PDB or mmCIF) error:\n%s" % str(e))
    model = mmtbx.model.manager(
      model_input      = pdb_inp,
      crystal_symmetry = crystal_symmetry,
      log              = outstr)
    #
    scattering_table = params.scattering_table
    exptl_method = pdb_inp.get_experiment_type()
    if(exptl_method is not None) and (exptl_method.is_neutron()):
      scattering_table = "neutron"
    model.setup_scattering_dictionaries(
      scattering_table = scattering_table,
      d_min            = f_obs.d_min())
    #
    # Model vs data
    #
    fmodel = mmtbx.f_model.manager(
      xray_structure = model.get_xray_structure(),
      f_obs          = f_obs,
      r_free_flags   = r_free_flags,
      twin_law       = params.twin_law)
    fmodel.update_all_scales(update_f_part1=True)
   #fmodel.update_all_scales(update_f_part1=True,fast=True,optimize_mask=True)
    fmodel.show(log=outstr, show_header=False, show_approx=False)
  # print("  r_work: %6.4f"%fmodel.r_work(), file=outstr)
  # if(test_flag_value is not None):
  #   print("  r_free: %6.4f"%fmodel.r_free(), file=outstr)
  # else:
  #   print("  r_free: None", file=outstr)
  # print(file=outstr)
    n_outl = f_obs.data().size() - fmodel.f_obs().data().size()
  # print("  Number of F-obs outliers:", n_outl, file=outstr)

    # tNCS epsilons
    tncsresults = None
    if tncs_correction:
      epsilons = None
      epsilons = fmodel.f_obs().epsilons().data().as_double()
      tncs_outstr = TextIO()
     #we have a local copy of tncs.py because the cctbx_project one has problems
      tncsresults = tncs.compute_eps_factor(
          f_obs               = fmodel.f_obs(),
          pdb_hierarchy       = model.get_hierarchy(),
          log  = tncs_outstr, #suppress output and show results later
          reflections_per_bin = 150)
      tncsresults.outstr = tncs_outstr
    return float(fmodel.r_work()), float(fmodel.r_free()), tncsresults

  def get_rfactors(self,
          basetng,
          f_obs_label = None,
          cmd_cs = None,
          reflid_label = None,
          tncs_correction = True,
          high_resolution = None,
          twin_results = None,
          iopdb = None,
          iomtz = None,
          ):
        # log = sys.stdout):
      if int(basetng.DAGDATABASE.size()) == 0:
        return None,None,None,None
      outstr = TextIO()
    # iomtz,f_obs_label = get_iomtz(basetng,node,f_obs_label)
      if True:
     #{
        rwork,rfree,tncsresults = self.rfactor(
                                 f_obs_label=f_obs_label,
                                 tncs_correction=tncs_correction,
                                 high_resolution=high_resolution,
                                 outstr=outstr,
                                 cmd_cs=cmd_cs,
                                 twin_law=None,
                                 args=[iopdb.fstr(),iomtz.fstr()])
        if rwork == 0:
          rwork = 100.
          rfree = 100.
          return rwork,rfree
        lines = outstr.getvalue().split("\n")
        interesting_lines = ["Number of Miller indices",
                             "Resolution range",
                             "Resolution    Compl Nwork",
                             "Completeness"]
        for line in lines:
          lline = line.lstrip();
          for interesting in interesting_lines:
            if lline.startswith(interesting):
              basetng.logTab(enum_out_stream.verbose,str(lline))
          if len(lline) > 0 and lline[0].isdigit():
            basetng.logTab(enum_out_stream.verbose,str(line)) #include left spaces for layout
        rwork = 100*float("{:.4f}".format(rwork))
        rfree = 100*float("{:.4f}".format(rfree))
     #  basetng.logTab(enum_out_stream.logfile,str("Twin law=(h,k,l)")) #include left spaces for layout
        basetng.logTab(enum_out_stream.logfile,"R-work/R-free = " + "{:.2f}".format(rwork) + " " + "{:.2f}".format(rfree))
        best_rwork = rwork
        best_rfree = rfree
        best_twin = None
     #}
      if bool(twin_results.TWINNED) and int(twin_results.n_twin_laws) > 0:
     #{
        for ii in range(twin_results.n_twin_laws):
       #{
          if (twin_results.twin_summary.murray_rust_alpha[ii] > float(twin_results.io_sigtwinfrac)):
         #{
            line = "Twin law=(%s) alpha=%5.3f"%(twin_results.twin_summary.twin_laws[ii],
                                         twin_results.twin_summary.murray_rust_alpha[ii])
            twin_law = twin_results.twin_summary.twin_laws[ii]
            basetng.logChevron(enum_out_stream.logfile,str("Twin Law")) #include left spaces for layout
            basetng.logTab(enum_out_stream.logfile,str(line)) #include left spaces for layout
            rwork,rfree,tncsresults = self.rfactor(
                                     f_obs_label=f_obs_label,
                                     tncs_correction=tncs_correction,
                                     high_resolution=high_resolution,
                                     outstr=outstr,
                                     cmd_cs=cmd_cs,
                                     twin_law=twin_law,
                                     args=[iopdb.fstr(),iomtz.fstr()])
            if rwork == 0:
              rwork = 100.
              rfree = 100.
              return rwork,rfree
            lines = outstr.getvalue().split("\n")
            interesting_lines = ["Number of Miller indices",
                                 "Resolution range",
                                 "Resolution    Compl Nwork",
                                 "Completeness"]
            for line in lines:
              lline = line.lstrip();
              for interesting in interesting_lines:
                if lline.startswith(interesting):
                  basetng.logTab(enum_out_stream.verbose,str(lline))
              if len(lline) > 0 and lline[0].isdigit():
                basetng.logTab(enum_out_stream.verbose,str(line)) #include left spaces for layout
            rwork = 100*float("{:.4f}".format(rwork))
            rfree = 100*float("{:.4f}".format(rfree))
            basetng.logTab(enum_out_stream.logfile,"R-work/R-free = " + "{:.2f}".format(rwork) + " " + "{:.2f}".format(rfree))
            if rwork < best_rwork:
              best_rwork = rwork
              best_rfree = rfree
              best_twin = twin_law
         #}
       #}
     #}
      return best_rwork,best_rfree,tncsresults,best_twin

  def print_tncs(self,basetng,tncsresults):
      DAGDB = basetng.DAGDATABASE
      if tncsresults is None:
        basetng.logTab(enum_out_stream.verbose,"No tncs correction applied")
      if tncsresults is not None:
        wf = basetng.WriteFiles()
        flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
        if not flat and wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
            os.makedirs(os.path.join(basetng.DataBase(),subdir))
        if wf:
            lines = tncsresults.outstr.getvalue()
            logout = DAGDB.PATHWAY.ULTIMATE.extra_file_stem_ext(".tncs_pairs.log")
            newlogname = FileSystem(basetng.DataBase(),subdir,logout)
            basetng.logFileWritten(enum_out_stream.logfile,wf,"tNCS Pairs",newlogname)
            with open(newlogname.fstr(), "w") as f:
              f.write("PHASERTNG TNCS PAIRS\n")
              f.write("--------------------\n")
              f.write(lines)
        if wf:
            showsummary = TextIO()
            tncsresults.show_summary(log=showsummary)
            lines = showsummary.getvalue()
            logout = DAGDB.PATHWAY.ULTIMATE.extra_file_stem_ext(".tncs_show.log")
            newlogname = FileSystem(basetng.DataBase(),subdir,logout)
            basetng.logFileWritten(enum_out_stream.logfile,wf,"tNCS Epsilons",newlogname)
            with open(newlogname.fstr(), "w") as f:
              basetng.logTab(enum_out_stream.verbose,newlogname.fstr())
              f.write("PHASERTNG TNCS\n")
              f.write("--------------\n")
              f.write(lines)
              f.close()
        tncsdict = convert_tncsresults_to_dict(tncsresults.ncs_groups)
        for k,v in tncsdict.items():
          basetng.logTab(enum_out_stream.logfile,k + ":" + str(v))
        if (tncsresults.epsfac is not None):
          basetng.logTab(enum_out_stream.logfile,"tNCS eps factor: min:%6.4f max:%6.4f mean:%6.4f"%\
            tncsresults.epsfac.min_max_mean().as_tuple())
          assert(len(tncsresults.epsfac)>2)
          nprint=3
          for r in range(nprint):
            basetng.logTab(enum_out_stream.verbose,"r=%6d teps=%6.4f"%(r,tncsresults.epsfac[r]))
          basetng.logTab(enum_out_stream.verbose,"...")
          for r in reversed(range(nprint)):
            rr = len(tncsresults.epsfac)-r-1
            basetng.logTab(enum_out_stream.verbose,"r=%6d teps=%6.4f"%(rr,tncsresults.epsfac[rr]))

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  basetng = Phasertng("VOYAGER")
  parser.add_argument('-p',"--pdb",help="pdb file",required=False)
  parser.add_argument('-m',"--mtz",help="mtz file",required=False)
  parser.add_argument('-r',"--resolution",type=float,required=False,default=3)
  parser.add_argument('-f',"--f_obs_label",help="column label for data, can be amplitude or intensity",required=False)
  parser.add_argument('-d',"--db",help="database file",type=argparse.FileType('r'),default=None,required=False)
  args = parser.parse_args()
  if args.db is None:
    cmd_cs = from_pdb.extract_from(file_name = args.pdb)
    r_work,r_free,tncsresults = runRFAC().rfactor(
                            args=[args.mtz,args.pdb],
                            cmd_cs = cmd_cs,
                            f_obs_label=args.f_obs_label,
                            tncs_correction=True,
                            high_resolution=args.resolution)
    print("R-work/R-free = ", "{:.2f}".format(100*r_work),"/", "{:.2f}".format(100*r_free))
  else:
    cards = args.db.read()
    basetng.DAGDATABASE.parse_cards(cards)
    runRFAC().inner_rfactor(basetng=basetng,
                            reflid_label="INAT",
                            tncs_correction=True,
                            high_resolution=args.resolution)
