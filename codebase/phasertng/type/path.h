//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_path_class__
#define __phasertng_type_path_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class path : public optional
{
  std::string uvalue = "";
  std::string dephalt = "";
  std::string extension = "";

  public:
  path() : optional() {}
  path(
      const std::string parameter,
      const std::string dephalt_ = "" ,
      const std::string extension_ = ""
  );
  std::string ext();
  void set_value(std::string other);
  void set_value(const sv_string& vcards,const std::string& other);
  bool is_default() const;
  void set_default();
  std::string value_or_default() const;  // unquoted
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);
  std::string unparse_parameter(bool phil) const;

  bool operator==(const type::path& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
