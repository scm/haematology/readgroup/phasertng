//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/pod/scatterer.h>
#include <phasertng/minimizer/RefineSADI.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/dag/Node.h>
#include <phasertng/main/jiffy.h>

namespace phasertng {

  RefineSADI::RefineSADI(
      const_ReflectionsPtr  REFLECTIONS_,
      SelectedPtr SELECTED_,
      ScatterersPtr SCATTERERS_)
    : RefineBase(),
      REFLECTIONS(std::move(REFLECTIONS_)),
      SELECTED(std::move(SELECTED_)),
      SCATTERERS(std::move(SCATTERERS_))
  {
    NBINS = REFLECTIONS->numbins();
    //pick out the coefficients
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;
   // dmat6 u_iso_coeffs = cctbx::adptbx::u_iso_as_u_star(cctbxUC,1.0);
    dmat6 u_star_coeffs;
    for (int n = 0; n < 6; n++)
    {
      dmat6 pick(n==0?1:0,n==1?1:0,n==2?1:0,n==3?1:0,n==4?1:0,n==5?1:0);
      u_star_coeffs[n] = cctbx::adptbx::u_star_as_u_iso(cctbxUC,pick);
      dBeq_by_du_star[n] = cctbx::adptbx::u_as_b(u_star_coeffs[n]);
    }
    HAS_PART = REFLECTIONS->has_col(labin::FCNAT);
    integration_points.resize(REFLECTIONS->NREFL,fixed_number_of_integration_steps);
    for (auto item : SCATTERERS->FP_FDP)
    {
      INITIAL_FDP[item.first] = item.second.fdp();
    }
    SADI.resize(REFLECTIONS->NREFL);
  }

  void
  RefineSADI::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineSADI::set_sigma_sphericity(double SIGMA_SPHERICITY_)
  {
    SIGMA_SPHERICITY = SIGMA_SPHERICITY_;
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;
    if (SIGMA_SPHERICITY)
    { //SetupSphericity
      double sigmaSphericity(SIGMA_SPHERICITY*fn::pow2(REFLECTIONS->data_hires())); //Restrict relative correction at resolution limit
      double QUARTER(1./4.);
      double aStar = cctbxUC.reciprocal_parameters()[0];
      double bStar = cctbxUC.reciprocal_parameters()[1];
      double cStar = cctbxUC.reciprocal_parameters()[2];
      //Convert isotropic equivalent as in largeShifts
      dmat6 sigmaSphericityBeta;
      sigmaSphericityBeta[0] = QUARTER*sigmaSphericity*aStar*aStar;
      sigmaSphericityBeta[1] = QUARTER*sigmaSphericity*bStar*bStar;
      sigmaSphericityBeta[2] = QUARTER*sigmaSphericity*cStar*cStar;
      sigmaSphericityBeta[3] = QUARTER*sigmaSphericity*aStar*bStar;
      sigmaSphericityBeta[4] = QUARTER*sigmaSphericity*aStar*cStar;
      sigmaSphericityBeta[5] = QUARTER*sigmaSphericity*bStar*cStar;
      u_star_sphericity = cctbx::adptbx::beta_as_u_star(sigmaSphericityBeta);
    }
    else
    {
      dmat6 sigmaSphericityBeta(0,0,0,0,0,0);
      u_star_sphericity = cctbx::adptbx::beta_as_u_star(sigmaSphericityBeta);
    }
  }

  void
  RefineSADI::set_fixed_number_of_integration_steps(int n)
  {
    fixed_number_of_integration_steps = n;
    integration_points.resize(REFLECTIONS->NREFL,fixed_number_of_integration_steps);
  }

  void
  RefineSADI::init_node(dag::Node* work)
  {
    NODE = work;
    atoms = Substructure(NODE->FULL.ENTRY->SUBSTRUCTURE.GetPdb());
    atoms.process(SCATTERERS);
    phaser_assert(REFLECTIONS->NREFL);
    PHASER_ASSERT(NODE->SADSIGMAA.numbins());
    sv_double midssqr = REFLECTIONS->bin_midssqr(0);
    PHASER_ASSERT(midssqr.size());
    for (int b = 0; b < NBINS; b++)
    {
      PHASER_ASSERT(std::fabs(midssqr[b] - NODE->SADSIGMAA.MIDSSQR[b]) < DEF_PPM);
    }
    atoms.GRAD.resize(atoms.size());
    atoms.HESS.resize(atoms.size());
    NODE->SADSIGMAA.fDelta_bin.resize(NODE->SADSIGMAA.numbins());//,0.001);
    for (unsigned s = 0; s < NODE->SADSIGMAA.numbins(); s++)
    {
      auto& SigmaH_bin = NODE->SADSIGMAA.SigmaH_bin[s];
      auto& sigmaHH_bin = NODE->SADSIGMAA.sigmaHH_bin[s];
      NODE->SADSIGMAA.fDelta_bin[s] = 0.001;
      PHASER_ASSERT(NODE->SADSIGMAA.fDelta_bin[s] > 0);
      SigmaH_bin = 0.;
      sigmaHH_bin = {0,0};
      double& ssqr_bin = NODE->SADSIGMAA.MIDSSQR[s];
      for (unsigned a = 0; a < atoms.XRAY.size(); a++)
      {
        pod::xray::scatterer& A = atoms.XRAY[a];
        pod::xtra::scatterer& X = atoms.XTRA[a];
        if (!X.rejected)
        {
          std::string t = A.scattering_type;
          //int t = atomtype2t[t];
         // int mult = site_sym_table.get(a).multiplicity();
          double atom_u_iso = (A.flags.use_u_aniso_only()) ?
              cctbx::adptbx::u_star_as_u_iso(REFLECTIONS->UC.cctbxUC,A.u_star) :
              A.u_iso;
          double isoBeqterm = std::exp(-ssqr_bin*cctbx::adptbx::u_as_b(atom_u_iso)*0.25);
          double iso2Beqterm = fn::pow2(isoBeqterm);
          double atomocc(A.occupancy);
          const double& fdp = SCATTERERS->fdp(t);
          double occfofp(atomocc*SCATTERERS->fo_plus_fp(t,ssqr_bin));
          double occfpp(atomocc*fdp);
          double occfabsSqr(fn::pow2(occfofp) + fn::pow2(occfpp));
          cmplex occfSqr(fn::pow2(cmplex(occfofp,occfpp)));
          // To include effect of anisotropy, could use the following two lines inside symm loop:
          // if (A.flags.use_u_aniso_only())
          // anisoB = cctbx::adptbx::debye_waller_factor_u_star(rsym.rotMiller[r][isym],A.u_star);
          double multB2(X.multiplicity*iso2Beqterm);
          SigmaH_bin += multB2*occfabsSqr;
          sigmaHH_bin += multB2*occfSqr;
       // PHASER_ASSERT(!has_partial_structure); // Have to fix this code for partial structure
          /* if (has_partial_structure)
          {
            double debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
            double part_scale = fn::pow2(PartK*std::exp(-ssqr[r]*debye_PartB));
            SigmaH_bin += part_scale*SigmaH_bin_part[s];
            sigmaHH_bin += part_scale*sHH_bin_part[s];
          } */
          SigmaH_bin = std::min(SigmaH_bin,0.9*NODE->SADSIGMAA.SigmaN_bin[s]);
        } //end  loop over atoms
      } //end  loop over atoms
      //bounds limits
      double sigman = NODE->SADSIGMAA.SigmaN_bin[s];
      SigmaH_bin = std::min(std::max(0.000001*sigman, SigmaH_bin),0.999999*sigman);//SigmaH
    }
    //AJM TODO setup FP from COORDINATES
  }

  void
  RefineSADI::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(item);
   // Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    REFINE_SIGA = (protocol.find("siga") != protocol.end());
    REFINE_PART = (protocol.find("part") != protocol.end());
    atoms.REFINE_SITE = (protocol.find("site") != protocol.end());
    atoms.REFINE_BFAC = (protocol.find("bfac") != protocol.end());
    atoms.REFINE_OCC = (protocol.find("occ") != protocol.end());
    atoms.REFINE_FDP = (protocol.find("fdp") != protocol.end());
    if (protocol.find("off") != protocol.end()) //overwrite
    {
      REFINE_SIGA = REFINE_PART = false;
      atoms.REFINE_SITE = false;
      atoms.REFINE_BFAC = false;
      atoms.REFINE_OCC = false;
      atoms.REFINE_FDP = false;
    }
    //set flags on ATOM for derivatives and hessian

    refine_parameter_.clear();
    PHASER_ASSERT(refine_parameter_.size() == 0);
    for (int b = 0; b < NBINS; b++)
    {
      REFINE_SIGA ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
      REFINE_SIGA ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
      REFINE_SIGA ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    }
    REFINE_PART ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    REFINE_PART ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        for (int x = 0; x < X.n_xyz; x++)
        {
          atoms.REFINE_SITE ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
        }
        atoms.REFINE_OCC ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
        if (!A.flags.use_u_aniso_only())
        {
          atoms.REFINE_BFAC ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
        }
        else
        {
          for (int n = 0; n < X.n_adp; n++)
          {
            atoms.REFINE_BFAC ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
          }
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
    {
      atoms.REFINE_FDP ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    }

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i])
        nmp++;
    total_number_of_parameters_ = refine_parameter_.size();
  }

  sv_string
  RefineSADI::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      if (refine_parameter_[m++]) names[i++] = "SigmaH Bin #"+ std::to_string(b+1);
      if (refine_parameter_[m++]) names[i++] = "fDelta Bin #"+ std::to_string(b+1);
      if (refine_parameter_[m++]) names[i++] = "rhopm  Bin #" + std::to_string(b+1);
    }
    if (refine_parameter_[m++]) names[i++] = "Partial Structure Scale";
    if (refine_parameter_[m++]) names[i++] = "Partial Structure B-factor";
   // Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        //  int M = SG.NSYMM/atoms.multiplicity;
        if (X.multiplicity == 1)
        {
          if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " Direction=X";
          if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " Direction=Y";
          if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " Direction=Z";
        }
        else
        {
          for (int x = 0; x < X.n_xyz; x++)
            if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " Direction=" + std::to_string(x+1);
        }
        if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " O";
        if (!A.flags.use_u_aniso_only())
        {
          if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " IsoB";
        }
        else
        {
          for (int n = 0; n < X.n_adp; n++)
            if (refine_parameter_[m++]) names[i++] = "Atom #" +std::to_string(a+1) + " AnoB" + std::to_string(n+1);
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
    {
      if (refine_parameter_[m++]) names[i++] = "AtomType " + item.first + " F\"" ;
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineSADI::get_macrocycle_parameters()
  {
    sv_double pars(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      PHASER_ASSERT(NODE->SADSIGMAA.SigmaH_bin[b]>0);
      if (refine_parameter_[m++]) pars[i++] = NODE->SADSIGMAA.SigmaH_bin[b];
      if (refine_parameter_[m++]) pars[i++] = NODE->SADSIGMAA.fDelta_bin[b];
      if (refine_parameter_[m++]) pars[i++] = NODE->SADSIGMAA.rhopm_bin[b];
    }
    double& PartK = NODE->FULL.ENTRY->COORDINATES.PARTK;
    double& PartU = NODE->FULL.ENTRY->COORDINATES.PARTU;
    if (refine_parameter_[m++]) pars[i++] = PartK;
    if (refine_parameter_[m++]) pars[i++] = PartU;
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        auto constrained = atoms.site_independent_params(a,A.site);
        for (int x = 0; x < X.n_xyz; x++)
          if (refine_parameter_[m++]) pars[i++] = constrained[x];
        if (refine_parameter_[m++]) pars[i++] = A.occupancy;
        if (!A.flags.use_u_aniso_only())
        {
          if (refine_parameter_[m++]) pars[i++] = A.u_iso;
        }
        else
        {
          auto constrained = atoms.adp_independent_params(a,A.u_star);
          for (int n = 0; n < X.n_adp; n++)
            if (refine_parameter_[m++]) pars[i++] = constrained[n];
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
      if (refine_parameter_[m++])
        pars[i++] = item.second.fdp();
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineSADI::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      if (refine_parameter_[m++])
        NODE->SADSIGMAA.SigmaH_bin[b] = newx[i++];
      if (refine_parameter_[m++])
        NODE->SADSIGMAA.fDelta_bin[b] = newx[i++];
      if (refine_parameter_[m++])
        NODE->SADSIGMAA.rhopm_bin[b] = newx[i++];
    }
    double& PartK = NODE->FULL.ENTRY->COORDINATES.PARTK;
    double& PartU = NODE->FULL.ENTRY->COORDINATES.PARTU;
    if (refine_parameter_[m++])
      PartK = newx[i++];
    if (refine_parameter_[m++])
      PartU = newx[i++];
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        scitbx::af::small<double,3> x_indep(X.n_xyz);
        for (int x = 0; x < X.n_xyz; x++)
        {
          if (refine_parameter_[m++])
          {
            x_indep[x] = newx[i++];
       // if (atoms.REFINE_SITE) //need this extra criteria!
            atoms.set_site_independent_params(a,x_indep); //overwrite
          }
        }
        if (refine_parameter_[m++])
          A.occupancy = newx[i++];
        if (!A.flags.use_u_aniso_only())
        {
          //if (refine_parameter_[m++]) A.u_iso = cctbx::adptbx::b_as_u(newx[i++]);
          if (refine_parameter_[m++]) A.u_iso = newx[i++];
        }
        else
        {
          scitbx::af::small<double,6> u_indep(X.n_adp);
          for (int n = 0; n < X.n_adp; n++)
          {
            if (refine_parameter_[m++])
            {
              u_indep[n] = newx[i++];
         // if (atoms.REFINE_BFAC) //need this extra criteria!
              atoms.set_adp_independent_params(a,u_indep); //overwrite
            }
          }
        }
      }
    }
    for (auto& item : SCATTERERS->FP_FDP)
    {
      if (refine_parameter_[m++])
      {
        SCATTERERS->set_fdp(item.first,newx[i++]); //overwrite
      }
    }

    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
  }

  std::vector<dtmin::Bounds>
  RefineSADI::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      double sigman = NODE->SADSIGMAA.SigmaN_bin[b];
      if (refine_parameter_[m++]) bounds[i++].on(0.000001*sigman, 0.999999*sigman);//SigmaH
      if (refine_parameter_[m++]) bounds[i++].on(0.000001, 0.999999);//fDelta
      if (refine_parameter_[m++]) bounds[i++].on(0., 0.99);//rhopm
    }
    if (refine_parameter_[m++]) bounds[i++].lower_on(0.05);//PartK
    if (refine_parameter_[m++]) bounds[i++].on(-cctbx::adptbx::b_as_u(UPPER_B), cctbx::adptbx::b_as_u(UPPER_B));//PartU
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        for (int x = 0; x < X.n_xyz; x++)
          if (refine_parameter_[m++]) bounds[i++].off(); //XYZ
        if (refine_parameter_[m++]) bounds[i++].lower_on(LOWER_O); //OCC
        if (!A.flags.use_u_aniso_only()) //BFAC
        {
          if (refine_parameter_[m++])
            bounds[i++].on(-cctbx::adptbx::b_as_u(UPPER_B), cctbx::adptbx::b_as_u(UPPER_B)); //B (i.e. u_iso)
        }
        else
        {
          for (int n = 0; n < X.n_adp; n++)
            if (refine_parameter_[m++]) bounds[i++].off(); //AnisoB (i.e. u_star)
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
      if (refine_parameter_[m++]) bounds[i++].lower_on(LOWER_FDP);//FDP
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineSADI::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      if (refine_parameter_[m++]) repar[i++].off(); //SigmaH
     // if (refine_parameter_[m++]) repar[i++].on(0.000001); //fDelta
      if (refine_parameter_[m++]) repar[i++].off(); //fDelta
      if (refine_parameter_[m++]) repar[i++].off(); //rhopm
    }
    if (refine_parameter_[m++]) repar[i++].off();
    if (refine_parameter_[m++]) repar[i++].off();
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        for (int x = 0; x < X.n_xyz; x++)
          if (refine_parameter_[m++]) repar[i++].off();
        if (refine_parameter_[m++]) repar[i++].off();
        if (!A.flags.use_u_aniso_only())
        {
          //if (refine_parameter_[m++]) repar[i++].on(cctbx::adptbx::b_as_u(5.0));
          if (refine_parameter_[m++]) repar[i++].off();
        }
        else
        {
          for (int n = 0; n < X.n_adp; n++)
            if (refine_parameter_[m++]) repar[i++].off();
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
      if (refine_parameter_[m++]) repar[i++].off();
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  sv_double
  RefineSADI::macrocycle_large_shifts()
  {
    sv_double largeShifts(nmp);
    double largeXshift = REFLECTIONS->data_hires()/10.0; //in Angstroms
    cctbx::fractional<double> largeXYZshift;
    const UnitCell& UC = REFLECTIONS->UC;
    largeXYZshift[0] = largeXshift/UC.A(); //X, fractional
    largeXYZshift[1] = largeXshift/UC.B(); //Y, fractional
    largeXYZshift[2] = largeXshift/UC.C(); //Z, fractional
    double largeBshift = 2.0;
    double largeUshift = cctbx::adptbx::b_as_u(largeBshift);
    dmat6 largeABshift;
    largeABshift[0] = largeBshift/fn::pow2(UC.A());
    largeABshift[1] = largeBshift/fn::pow2(UC.B());
    largeABshift[2] = largeBshift/fn::pow2(UC.C());
    largeABshift[3] = largeBshift/(UC.A()*UC.B());
    largeABshift[4] = largeBshift/(UC.A()*UC.C());
    largeABshift[5] = largeBshift/(UC.B()*UC.C());
    dmat6 largeADPshift = cctbx::adptbx::beta_as_u_star(largeABshift);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      if (refine_parameter_[m++]) largeShifts[i++] = 0.25*NODE->SADSIGMAA.SigmaH_bin[b];//0.001 //SigmaH
      if (refine_parameter_[m++]) largeShifts[i++] = std::max(0.25*NODE->SADSIGMAA.fDelta_bin[b],0.0001);//fDelta
      if (refine_parameter_[m++]) largeShifts[i++] = std::max(0.25*NODE->SADSIGMAA.rhopm_bin[b],0.001); //rhopm
    }
    if (refine_parameter_[m++]) largeShifts[i++] = 0.05; //PartK
    if (refine_parameter_[m++]) largeShifts[i++] = largeUshift; //PartU
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    for (int a = 0; a < atoms.size(); a++)
    {
      pod::xray::scatterer& A = atoms.XRAY[a];
      pod::xtra::scatterer& X = atoms.XTRA[a];
      if (!X.rejected)
      {
        auto constrained = atoms.site_independent_params(a,largeXYZshift);
        for (int x = 0; x < X.n_xyz; x++)
           if (refine_parameter_[m++]) largeShifts[i++] = constrained[x];
        if (refine_parameter_[m++]) largeShifts[i++] = std::max(0.075,0.15*A.occupancy); // occupancy
        if (!A.flags.use_u_aniso_only())
        {
          if (refine_parameter_[m++]) largeShifts[i++] = largeUshift; //B (i.e. u_iso)
        }
        else
        {
          auto constrained = atoms.adp_independent_params(a,largeADPshift);
          for (int n = 0; n < X.n_adp; n++)
            if (refine_parameter_[m++]) largeShifts[i++] = constrained[n];
        }
      }
    }
    for (auto item : SCATTERERS->FP_FDP)
      if (refine_parameter_[m++]) largeShifts[i++] = std::max(0.07*item.second.fdp(),0.01);
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return largeShifts;
  }

  double
  RefineSADI::Rfactor()
  {
    likelihood::rfactor::function rfactor;
    return rfactor.get();
  }

  TargetType
  RefineSADI::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    //store the llg without restraints
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don' want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineSADI::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineSADI::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineSADI::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  af_string
  RefineSADI::initial_statistics()
  {
    af_string out1 = NODE->logSigmaa("Current");
    af_string out2 = atoms.logAtoms("Current");
    for (auto& line:out2)
      out1.push_back(line);
    return out1;
  }

  af_string
  RefineSADI::current_statistics()
  {
    af_string out1 = NODE->logSigmaa("Current");
    af_string out2 = atoms.logAtoms("Current");
    for (auto& line:out2)
      out1.push_back(line);
    return out1;
  }

  af_string
  RefineSADI::final_statistics()
  {
    af_string out1 = NODE->logSigmaa("Final");
    af_string out2 = atoms.logAtoms("Final");
    for (auto& line:out2)
      out1.push_back(line);
    return out1;
  }

  af_string
  RefineSADI::finalize_parameters()
  {
    NODE->FULL.ENTRY->SUBSTRUCTURE = atoms; //base class copy
    return af_string();
  }

  af_string
  RefineSADI::reject_outliers()
  {
    sv_double epsnSigmaN(0); // empty to trigger internal calculation
    SELECTED->flag_outliers_and_select(REFLECTIONS,
      { rejected::RESO,
        rejected::MISSING,
        rejected::SYSABS,
        //rejected::WILSONNAT,
        rejected::WILSONPOS,
        rejected::WILSONNEG,
        // rejected::LOINFO,
        rejected::NOANOM,
        rejected::BADANOM,
      },
      epsnSigmaN,
      NODE->SADSIGMAA
    );
    return af_string();
  }

  af_string
  RefineSADI::setup_parameters()
  {
    calculate_number_of_integration_steps();
    return af_string();
  }

  void
  RefineSADI::calculate_number_of_integration_steps()
  {
    //set number of integration points each macrocycle
    //also calculate FC
    if (fixed_number_of_integration_steps > 0) return;
    //double& PartK = NODE->FULL.ENTRY->COORDINATES.PARTK;
    //double& PartU = NODE->FULL.ENTRY->COORDINATES.PARTU;
   // Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    likelihood::isad::function ISAD(&NODE->SADSIGMAA);

    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      integration_points[r] = 0;
      if (SELECTED->get_selected(r) and
          REFLECTIONS->get_bln(labin::BOTH,r))
      {
        const int& s = REFLECTIONS->get_int(labin::BIN,r);
        const double& ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double& eps = REFLECTIONS->get_flt(labin::EPS,r);
        const millnx& miller = REFLECTIONS->get_miller(r);
        std::pair<cmplex,cmplex> fa = atoms.calcAtomicDataSum(miller,ssqr,eps,SCATTERERS,false,false);
        //double debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
        //cmplex scale = PartK*std::exp(-SSQR*debye_PartB);
        //double solTerm = SIGMAA.get(SSQR);
        cmplex kfp(0,0);
        //if (HAS_FPART) kfp += solTerm*scale*R->get_FC();
        cmplex fhpos = fa.first + kfp;
        cmplex fhneg = fa.second + kfp;
        //FA written to FCpos not FC, which is where partial structure is
        const double& teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double& resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double& dobspos = REFLECTIONS->get_flt(labin::DOBSPOS,r);
        const double& feffpos = REFLECTIONS->get_flt(labin::FEFFPOS,r);
        const double& dobsneg = REFLECTIONS->get_flt(labin::DOBSPOS,r);
        const double& feffneg = REFLECTIONS->get_flt(labin::FEFFNEG,r);
        // Calculation of SIGMAPOS and SIGMANEG below may have to be updated
        // to work with all parameterisations of iSAD
        double SigmaN = NODE->SADSIGMAA.SigmaP_bin[s] + NODE->SADSIGMAA.SigmaQ_bin[s];
        double SigmaH = NODE->SADSIGMAA.SigmaH_bin[s];
        const double& sigmaneg = (SigmaN - fn::pow2(dobsneg)*SigmaH);
        PHASER_ASSERT(sigmaneg > 0);
        double absrhoFF = NODE->SADSIGMAA.absrhoFF_bin[s];
        double rhopm = NODE->SADSIGMAA.rhopm_bin[s];
        double absrhoFFobs = dobspos*dobsneg*absrhoFF +
          rhopm*std::sqrt((1.-fn::pow2(dobspos))*(1.-fn::pow2(dobsneg)));
        cmplex sigmaFF = NODE->SADSIGMAA.sigmaPP_bin[s] + NODE->SADSIGMAA.sigmaQQ_bin[s];
        cmplex rhoFFobs = absrhoFFobs*sigmaFF/SigmaN;
        cmplex sigmaPhi = rhoFFobs*SigmaN - dobspos*dobsneg*NODE->SADSIGMAA.sigmaHH_bin[s];
        double sigmaPhiSqr = std::norm(sigmaPhi);
        const double& sigmapos = (SigmaN - fn::pow2(dobspos)*SigmaH)
          - sigmaPhiSqr/sigmaneg;
        integration_points[r] = ISAD.calculate_integration_points(resn, teps,
          dobspos, feffpos, fhpos, dobsneg, feffneg, fhneg, sigmapos, sigmaneg);
      }
    }
  }

  TargetGradientHessianType
  RefineSADI::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    //initialization
    do_gradient = (do_gradient || do_hessian);
    TargetGradientHessianType tgh(nmp);
    GradientType Curvature(nmp,0);

    double& PartK = NODE->FULL.ENTRY->COORDINATES.PARTK;
    double& PartU = NODE->FULL.ENTRY->COORDINATES.PARTU;
 //   Substructure& atoms = NODE->FULL.ENTRY->SUBSTRUCTURE;
    likelihood::isad::function ISAD(&NODE->SADSIGMAA);
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;

    //ISAD: warning
    //ReflectionPtr does not copy data in memory unless it is column-wise data
    //Warning! This is not thread-safe if col data is used
    //AJM TODO change ReflectionPtr to smart pointer and create new row object for each
    //reflection when there is column data used
    //Keep ReflRows as the default for REFLECTIONS and there is no problem
    bool fa_grad = true; //(do_gradient or dossc);
    bool fa_hess = true; //do_hessian;
    likelihood::isad::tgh_flags flags;
    flags.target(true).phsprob(false).gradient(fa_grad).hessian(fa_hess);

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SADI.size()) SADI[r] = 0; //zero for the unselected miller indices
      if (SELECTED->get_selected(r))
      {
        //Information that is not reflection-wise or constant added to sad function here
        //These terms are not reflection-wise so should only be transferred to reflection
        //information here
        //These terms need to be summed before ISAD anyway so need new memory
        //could be in external function, for fft
        const millnx& miller = REFLECTIONS->get_miller(r);
        const double& ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const bool&   cent = REFLECTIONS->get_bln(labin::CENT,r);
        const bool&   both = REFLECTIONS->get_bln(labin::BOTH,r);
        const int&    s = REFLECTIONS->get_int(labin::BIN,r);
        const double& eps = REFLECTIONS->get_flt(labin::EPS,r);
        const double& teps = REFLECTIONS->get_flt(labin::TEPS,r);
        double debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
        cmplex scale = PartK*std::exp(-ssqr*debye_PartB);
        cmplex kfp = FP.size() ? scale*FP[r] : cmplex(0,0);
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);
        std::pair<cmplex,cmplex> fa = atoms.calcAtomicDataSum(
                                          miller,ssqr,eps,SCATTERERS,fa_grad,fa_hess);
        cmplex fhpos = fa.first + kfp;
        cmplex fhneg = fa.second + kfp;
        likelihood::isad::tgh TGH;
        if (both)
        {
          const double& dobspos = REFLECTIONS->get_flt(labin::DOBSPOS,r);
          const double& feffpos = REFLECTIONS->get_flt(labin::FEFFPOS,r);
          const double& dobsneg = REFLECTIONS->get_flt(labin::DOBSPOS,r);
          const double& feffneg = REFLECTIONS->get_flt(labin::FEFFNEG,r);
          TGH = ISAD.tgh_acentric(
             s, eps,  teps, dobspos, feffpos, fhpos, dobsneg, feffneg, fhneg,
             flags,integration_points[r]);
        }
        else if (cent)
        {
          const double& dobs = REFLECTIONS->get_flt(labin::DOBSPOS,r);
          const double& feff = REFLECTIONS->get_flt(labin::FEFFPOS,r);
          const double& phsr = REFLECTIONS->get_flt(labin::PHSR,r);
          const cmplex& fh = fhpos;
          TGH = ISAD.tgh_centric(
             s, eps, teps, dobs, feff, fh, phsr,
             flags);
        }
        else
        {
          const bool& plus = REFLECTIONS->get_bln(labin::PLUS,r);
          const double& dobs = plus ? REFLECTIONS->get_flt(labin::DOBSPOS,r) : REFLECTIONS->get_flt(labin::DOBSNEG,r);
          const double& feff = plus ? REFLECTIONS->get_flt(labin::FEFFPOS,r) : REFLECTIONS->get_flt(labin::FEFFNEG,r);
          const cmplex& fh = plus ? fhpos : fhneg;
          TGH = ISAD.tgh_singleton(
             plus, s, eps, teps, dobs, feff, fh,
             flags);
        }
        tgh.Target += TGH.LogLike;
        tgh.Target -= REFLECTIONS->get_flt(labin::WLL,r); //null hypothesis offset

        // Note that FHneg is complex conjugate of H-, and dL refers to minus log-likelihood
        //calculculate in targetFn
        if (SADI_ATOMTYPE.size())
        {
          PHASER_ASSERT(r < SADI.size());
          const double& fp = SCATTERERS->fp(SADI_ATOMTYPE);
          const double& fdp = SCATTERERS->fdp(SADI_ATOMTYPE);
          SADI[r] = { -(fp *(TGH.dLL_by_dReFHpos + TGH.dLL_by_dReFHneg)+
                       fdp*(TGH.dLL_by_dImFHpos - TGH.dLL_by_dImFHneg)),
                     -(fdp*(-TGH.dLL_by_dReFHpos + TGH.dLL_by_dReFHneg)+
                       fp *(TGH.dLL_by_dImFHpos + TGH.dLL_by_dImFHneg)) };
        }

        if (do_gradient)
        {
/*
          //intermediate results
          sv_double dSigmaH_by_dO(atoms.size(),0);
          sv_double dSigmaH_by_dBeq(atoms.size(),0);
          std::map<std::string,double> dSigmaH_by_dFdp; //initialize below
          for (auto item : SCATTERERS->FP_FDP)
          {
            const std::string& t = item.first;
            dSigmaH_by_dFdp[t] = 0;
          }
*/
          //add partial scattering, which is real

          int m(0),i(0); //gradient
          int mm(0),ii(0); //curvatures

          for (int b = 0; b < NBINS; b++)
          {
            bool bin_match(b == s);
            if (refine_parameter_[m++])
              tgh.Gradient[i++] += bin_match ? TGH.dLL_by_dSigmaH : 0.;
            if (refine_parameter_[m++])
              tgh.Gradient[i++] += bin_match ? TGH.dLL_by_dfDelta : 0.;
            if (refine_parameter_[m++])
              tgh.Gradient[i++] += bin_match ? TGH.dLL_by_drhopm : 0;
          }

          //use one flag for selection, rather than intercalating with do_gradient above
          if (do_hessian)
          {
            for (int b = 0; b < NBINS; b++)
            {
              bool bin_match(b == s);
              if (refine_parameter_[mm++])
                Curvature[ii++] += bin_match ? TGH.d2LL_by_dSigmaH2 : 0.;
              if (refine_parameter_[mm++])
                Curvature[ii++] += bin_match ? TGH.d2LL_by_dfDelta2 : 0.;
              if (refine_parameter_[mm++])
                Curvature[ii++] += bin_match ? TGH.d2LL_by_drhopm2 : 0;
            }
          }

          PHASER_ASSERT(PartK); //lower bounded >0.05
          double dReFHpos_by_dPartK = std::real(kfp)/PartK;
          double dReFHneg_by_dPartK = std::real(kfp)/PartK;
          double dImFHpos_by_dPartK = std::imag(kfp)/PartK;
          double dImFHneg_by_dPartK = std::imag(kfp)/PartK;
          double  b_as_u_term(0.25*scitbx::constants::eight_pi_sq);
          double gradfac = -ssqr*b_as_u_term;
          double dReFHpos_by_dPartU = gradfac*std::real(kfp);
          double dReFHneg_by_dPartU = gradfac*std::real(kfp);
          double dImFHpos_by_dPartU = gradfac*std::imag(kfp);
          double dImFHneg_by_dPartU = gradfac*std::imag(kfp);
          if (refine_parameter_[m++]) //PartK
          {
            tgh.Gradient[i++] +=
                TGH.dLL_by_dReFHpos*dReFHpos_by_dPartK +
                TGH.dLL_by_dImFHpos*dImFHpos_by_dPartK +
                TGH.dLL_by_dReFHneg*dReFHneg_by_dPartK +
                TGH.dLL_by_dImFHneg*dImFHneg_by_dPartK;
          }
          if (refine_parameter_[m++]) //PartU
          {
            tgh.Gradient[i++] +=
                TGH.dLL_by_dReFHpos*dReFHpos_by_dPartU +
                TGH.dLL_by_dImFHpos*dImFHpos_by_dPartU +
                TGH.dLL_by_dReFHneg*dReFHneg_by_dPartU +
                TGH.dLL_by_dImFHneg*dImFHneg_by_dPartU;
          }

          if (do_hessian)
          {
            double hessfac = fn::pow2(gradfac);
            double d2ReFHpos_by_dPartU2 = hessfac*std::real(kfp);
            double d2ReFHneg_by_dPartU2 = hessfac*std::real(kfp);
            double d2ImFHpos_by_dPartU2 = hessfac*std::imag(kfp);
            double d2ImFHneg_by_dPartU2 = hessfac*std::imag(kfp);
            if (refine_parameter_[mm++]) //PartK
            {
              Curvature[ii++] +=
                  fn::pow2(dReFHpos_by_dPartK)*TGH.d2LL_by_dReFHpos2 +
                  fn::pow2(dImFHpos_by_dPartK)*TGH.d2LL_by_dImFHpos2 +
                  fn::pow2(dReFHneg_by_dPartK)*TGH.d2LL_by_dReFHneg2 +
                  fn::pow2(dImFHneg_by_dPartK)*TGH.d2LL_by_dImFHneg2 +
                  2*dReFHpos_by_dPartK*dImFHpos_by_dPartK*TGH.d2LL_by_dReFHpos_dImFHpos +
                  2*dReFHpos_by_dPartK*dReFHneg_by_dPartK*TGH.d2LL_by_dReFHpos_dReFHneg +
                  2*dReFHpos_by_dPartK*dImFHneg_by_dPartK*TGH.d2LL_by_dReFHpos_dImFHneg +
                  2*dReFHneg_by_dPartK*dImFHneg_by_dPartK*TGH.d2LL_by_dReFHneg_dImFHneg +
                  2*dReFHneg_by_dPartK*dImFHpos_by_dPartK*TGH.d2LL_by_dImFHpos_dReFHneg +
                  2*dImFHpos_by_dPartK*dImFHneg_by_dPartK*TGH.d2LL_by_dImFHpos_dImFHneg;
            }
            if (refine_parameter_[mm++]) //PartU
            {
              Curvature[ii++] +=
                  d2ImFHneg_by_dPartU2*TGH.dLL_by_dImFHneg +
                  d2ReFHneg_by_dPartU2*TGH.dLL_by_dReFHneg +
                  d2ImFHpos_by_dPartU2*TGH.dLL_by_dImFHpos +
                  d2ReFHpos_by_dPartU2*TGH.dLL_by_dReFHpos +
                  dImFHneg_by_dPartU*(dImFHneg_by_dPartU*TGH.d2LL_by_dImFHneg2 +
                                      dReFHneg_by_dPartU*TGH.d2LL_by_dReFHneg_dImFHneg +
                                      dImFHpos_by_dPartU*TGH.d2LL_by_dImFHpos_dImFHneg +
                                      dReFHpos_by_dPartU*TGH.d2LL_by_dReFHpos_dImFHneg) +
                  dReFHneg_by_dPartU*(dImFHneg_by_dPartU*TGH.d2LL_by_dReFHneg_dImFHneg +
                                      dReFHneg_by_dPartU*TGH.d2LL_by_dReFHneg2 +
                                      dImFHpos_by_dPartU*TGH.d2LL_by_dImFHpos_dReFHneg +
                                      dReFHpos_by_dPartU*TGH.d2LL_by_dReFHpos_dReFHneg) +
                  dImFHpos_by_dPartU*(dImFHneg_by_dPartU*TGH.d2LL_by_dImFHpos_dImFHneg +
                                      dReFHneg_by_dPartU*TGH.d2LL_by_dImFHpos_dReFHneg +
                                      dImFHpos_by_dPartU*TGH.d2LL_by_dImFHpos2 +
                                      dReFHpos_by_dPartU*TGH.d2LL_by_dReFHpos_dImFHpos) +
                  dReFHpos_by_dPartU*(dImFHneg_by_dPartU*TGH.d2LL_by_dReFHpos_dImFHneg +
                                      dReFHneg_by_dPartU*TGH.d2LL_by_dReFHpos_dReFHneg +
                                      dImFHpos_by_dPartU*TGH.d2LL_by_dReFHpos_dImFHpos +
                                      dReFHpos_by_dPartU*TGH.d2LL_by_dReFHpos2);
            }
          }

          for (int a = 0; a < atoms.size(); a++)
          {
            pod::xray::scatterer& A = atoms.XRAY[a];
            pod::xtra::scatterer& X = atoms.XTRA[a];
            pod::gradient::anom::scatterer& G = atoms.GRAD[a];
            //pod::hessian::anom::scatterer& H = atoms.HESS[a];
            if (!X.rejected)
            {
              af::small<double,3> constrained;
              if (atoms.REFINE_SITE)
              {
                cctbx::fractional<double> x_grad(0,0,0);
                for (int x = 0; x < 3; x++)
                {
                  x_grad[x] = TGH.dLL_by_dReFHpos*G.dReFHpos_by_dX[x] +
                              TGH.dLL_by_dImFHpos*G.dImFHpos_by_dX[x] +
                              TGH.dLL_by_dReFHneg*G.dReFHneg_by_dX[x] +
                              TGH.dLL_by_dImFHneg*G.dImFHneg_by_dX[x];
                }
                constrained = atoms.site_independent_gradients(a,x_grad);
              }
              for (int x = 0; x < X.n_xyz; x++) //XYZ
              {
                if (refine_parameter_[m++])
                  tgh.Gradient[i++] += constrained[x];
              }
              if (refine_parameter_[m++]) //OCC
              {
                tgh.Gradient[i++] +=
                  TGH.dLL_by_dReFHpos * G.dReFHpos_by_dO +
                  TGH.dLL_by_dImFHpos * G.dImFHpos_by_dO +
                  TGH.dLL_by_dReFHneg * G.dReFHneg_by_dO +
                  TGH.dLL_by_dImFHneg * G.dImFHneg_by_dO +
                  0 ;
                  //TGH.dLL_by_dSigmaH*dSigmaH_by_dO[a];
              }
              if (!A.flags.use_u_aniso_only()) //BFAC
              {
                if (refine_parameter_[m++])
                {
                  tgh.Gradient[i++] +=
                    TGH.dLL_by_dReFHpos * G.dReFHpos_by_dIB +
                    TGH.dLL_by_dImFHpos * G.dImFHpos_by_dIB +
                    TGH.dLL_by_dReFHneg * G.dReFHneg_by_dIB +
                    TGH.dLL_by_dImFHneg * G.dImFHneg_by_dIB +
                    0 ;
                    //TGH.dLL_by_dSigmaH*dSigmaH_by_dBeq[a];
                }
              }
              else
              {
                af::small<double,6> constrained;
                if (atoms.REFINE_BFAC)
                {
                  for (int n = 0; n < 6; n++)
                  {
                    dmat6 u_star_grad(0,0,0,0,0,0);
                    u_star_grad[n] =
                      TGH.dLL_by_dReFHpos * G.dReFHpos_by_dAB[n] +
                      TGH.dLL_by_dImFHpos * G.dImFHpos_by_dAB[n] +
                      TGH.dLL_by_dReFHneg * G.dReFHneg_by_dAB[n] +
                      TGH.dLL_by_dImFHneg * G.dImFHneg_by_dAB[n] +
                      0 ;
                      //TGH.dLL_by_dSigmaH * dSigmaH_by_dBeq[a] * dBeq_by_du_star[n];
                    constrained = atoms.adp_independent_gradient(a,u_star_grad);
                  }
                }
                //do the contraints
                for (int n = 0; n < X.n_adp; n++)
                  if (refine_parameter_[m++])
                    tgh.Gradient[i++] += constrained[n];
              }
            }
          }

          if (do_hessian)
          {
            for (int a = 0; a < atoms.size(); a++)
            {
              pod::xray::scatterer& A = atoms.XRAY[a];
              pod::xtra::scatterer& X = atoms.XTRA[a];
              pod::gradient::anom::scatterer& G = atoms.GRAD[a];
              pod::hessian::anom::scatterer& H = atoms.HESS[a];
              if (!X.rejected)
              {
                dvect3 x_diag_hess(0,0,0);
                if (atoms.REFINE_SITE)
                {
                  for (int x = 0; x < 3; x++)
                  {
                    x_diag_hess[x] =
                      H.d2ImFHneg_by_dX2[x]*TGH.dLL_by_dImFHneg +
                      H.d2ReFHneg_by_dX2[x]*TGH.dLL_by_dReFHneg +
                      H.d2ImFHpos_by_dX2[x]*TGH.dLL_by_dImFHpos +
                      H.d2ReFHpos_by_dX2[x]*TGH.dLL_by_dReFHpos +
                      G.dImFHneg_by_dX[x]*(G.dImFHneg_by_dX[x]*TGH.d2LL_by_dImFHneg2 +
                                           G.dReFHneg_by_dX[x]*TGH.d2LL_by_dReFHneg_dImFHneg +
                                           G.dImFHpos_by_dX[x]*TGH.d2LL_by_dImFHpos_dImFHneg +
                                           G.dReFHpos_by_dX[x]*TGH.d2LL_by_dReFHpos_dImFHneg) +
                      G.dReFHneg_by_dX[x]*(G.dImFHneg_by_dX[x]*TGH.d2LL_by_dReFHneg_dImFHneg +
                                           G.dReFHneg_by_dX[x]*TGH.d2LL_by_dReFHneg2 +
                                           G.dImFHpos_by_dX[x]*TGH.d2LL_by_dImFHpos_dReFHneg +
                                           G.dReFHpos_by_dX[x]*TGH.d2LL_by_dReFHpos_dReFHneg) +
                      G.dImFHpos_by_dX[x]*(G.dImFHneg_by_dX[x]*TGH.d2LL_by_dImFHpos_dImFHneg +
                                           G.dReFHneg_by_dX[x]*TGH.d2LL_by_dImFHpos_dReFHneg +
                                           G.dImFHpos_by_dX[x]*TGH.d2LL_by_dImFHpos2 +
                                           G.dReFHpos_by_dX[x]*TGH.d2LL_by_dReFHpos_dImFHpos) +
                      G.dReFHpos_by_dX[x]*(G.dImFHneg_by_dX[x]*TGH.d2LL_by_dReFHpos_dImFHneg +
                                           G.dReFHneg_by_dX[x]*TGH.d2LL_by_dReFHpos_dReFHneg +
                                           G.dImFHpos_by_dX[x]*TGH.d2LL_by_dReFHpos_dImFHpos +
                                           G.dReFHpos_by_dX[x]*TGH.d2LL_by_dReFHpos2);
                  }
                }
                if (X.n_xyz == 3) //XYZ
                {
                  for (int x = 0; x < 3; x++)
                    if (refine_parameter_[mm++])
                      Curvature[ii++] += x_diag_hess[x];
                }
                else //XYZ
                {
                  af_double  hess6(6,0); //upper triangle matrix (symmetric)
                  int d[3] = {0,3,5};  //index to unconstrained diagonal elements
                  for (int x = 0; x < 3; x++)
                    hess6[d[x]] = x_diag_hess[x];
                  //return array n_xyz*n_xyz/2 t i.e upper triangle
                  af::small<double,6> constrained = atoms.site_independent_curvatures(a,hess6);
                  sv_int c(X.n_xyz,0); //index to constrained diagonal elements
                  if (X.n_xyz == 2)
                    c[1] = 2;
                  for (int x = 0; x < X.n_xyz; x++)
                    if (refine_parameter_[mm++])
                      Curvature[ii++] += constrained[c[x]];
                }
                if (refine_parameter_[mm++]) //OCC
                {
                  Curvature[ii++] +=
                    fn::pow2(G.dReFHpos_by_dO)*TGH.d2LL_by_dReFHpos2 +
                    fn::pow2(G.dImFHpos_by_dO)*TGH.d2LL_by_dImFHpos2 +
                    fn::pow2(G.dReFHneg_by_dO)*TGH.d2LL_by_dReFHneg2 +
                    fn::pow2(G.dImFHneg_by_dO)*TGH.d2LL_by_dImFHneg2 +
                    2*G.dReFHpos_by_dO*G.dImFHpos_by_dO*TGH.d2LL_by_dReFHpos_dImFHpos +
                    2*G.dReFHpos_by_dO*G.dReFHneg_by_dO*TGH.d2LL_by_dReFHpos_dReFHneg +
                    2*G.dReFHpos_by_dO*G.dImFHneg_by_dO*TGH.d2LL_by_dReFHpos_dImFHneg +
                    2*G.dReFHneg_by_dO*G.dImFHneg_by_dO*TGH.d2LL_by_dReFHneg_dImFHneg +
                    2*G.dReFHneg_by_dO*G.dImFHpos_by_dO*TGH.d2LL_by_dImFHpos_dReFHneg +
                    2*G.dImFHpos_by_dO*G.dImFHneg_by_dO*TGH.d2LL_by_dImFHpos_dImFHneg;
                }
                if (!A.flags.use_u_aniso_only()) //BFAC
                {
                  if (refine_parameter_[mm++])
                  {
                    Curvature[ii++] +=
                        H.d2ImFHneg_by_dIB2*TGH.dLL_by_dImFHneg +
                        H.d2ReFHneg_by_dIB2*TGH.dLL_by_dReFHneg +
                        H.d2ImFHpos_by_dIB2*TGH.dLL_by_dImFHpos +
                        H.d2ReFHpos_by_dIB2*TGH.dLL_by_dReFHpos +
                        G.dImFHneg_by_dIB*(G.dImFHneg_by_dIB*TGH.d2LL_by_dImFHneg2 +
                                           G.dReFHneg_by_dIB*TGH.d2LL_by_dReFHneg_dImFHneg +
                                           G.dImFHpos_by_dIB*TGH.d2LL_by_dImFHpos_dImFHneg +
                                           G.dReFHpos_by_dIB*TGH.d2LL_by_dReFHpos_dImFHneg) +
                        G.dReFHneg_by_dIB*(G.dImFHneg_by_dIB*TGH.d2LL_by_dReFHneg_dImFHneg +
                                           G.dReFHneg_by_dIB*TGH.d2LL_by_dReFHneg2 +
                                           G.dImFHpos_by_dIB*TGH.d2LL_by_dImFHpos_dReFHneg +
                                           G.dReFHpos_by_dIB*TGH.d2LL_by_dReFHpos_dReFHneg) +
                        G.dImFHpos_by_dIB*(G.dImFHneg_by_dIB*TGH.d2LL_by_dImFHpos_dImFHneg +
                                           G.dReFHneg_by_dIB*TGH.d2LL_by_dImFHpos_dReFHneg +
                                           G.dImFHpos_by_dIB*TGH.d2LL_by_dImFHpos2 +
                                           G.dReFHpos_by_dIB*TGH.d2LL_by_dReFHpos_dImFHpos) +
                        G.dReFHpos_by_dIB*(G.dImFHneg_by_dIB*TGH.d2LL_by_dReFHpos_dImFHneg +
                                           G.dReFHneg_by_dIB*TGH.d2LL_by_dReFHpos_dReFHneg +
                                           G.dImFHpos_by_dIB*TGH.d2LL_by_dReFHpos_dImFHpos +
                                           G.dReFHpos_by_dIB*TGH.d2LL_by_dReFHpos2);
                  }
                }
                else //BFAC
                {
                  af_double  hess21(21,0); //upper triangle matrix (symmetric)
                  if (atoms.REFINE_BFAC)
                  {
                    for (int n = 0; n < 6; n++)
                    {
                      hess21[hess21_diagonal[n]] +=
                         H.d2ReFHpos_by_dAB2[n]*TGH.dLL_by_dReFHpos +
                         H.d2ImFHpos_by_dAB2[n]*TGH.dLL_by_dImFHpos +
                         H.d2ReFHneg_by_dAB2[n]*TGH.dLL_by_dReFHneg +
                         H.d2ImFHneg_by_dAB2[n]*TGH.dLL_by_dImFHneg +
                         G.dReFHpos_by_dAB[n]*(G.dReFHpos_by_dAB[n]*TGH.d2LL_by_dReFHpos2 +
                                               G.dImFHpos_by_dAB[n]*TGH.d2LL_by_dReFHpos_dImFHpos +
                                               G.dReFHneg_by_dAB[n]*TGH.d2LL_by_dReFHpos_dReFHneg +
                                               G.dImFHneg_by_dAB[n]*TGH.d2LL_by_dReFHpos_dImFHneg) +
                         G.dImFHpos_by_dAB[n]*(G.dReFHpos_by_dAB[n]*TGH.d2LL_by_dReFHpos_dImFHpos +
                                               G.dImFHpos_by_dAB[n]*TGH.d2LL_by_dImFHpos2 +
                                               G.dReFHneg_by_dAB[n]*TGH.d2LL_by_dImFHpos_dReFHneg +
                                               G.dImFHneg_by_dAB[n]*TGH.d2LL_by_dImFHpos_dImFHneg) +
                         G.dReFHneg_by_dAB[n]*(G.dReFHpos_by_dAB[n]*TGH.d2LL_by_dReFHpos_dReFHneg +
                                               G.dImFHpos_by_dAB[n]*TGH.d2LL_by_dImFHpos_dReFHneg +
                                               G.dReFHneg_by_dAB[n]*TGH.d2LL_by_dReFHneg2 +
                                               G.dImFHneg_by_dAB[n]*TGH.d2LL_by_dReFHneg_dImFHneg) +
                         G.dImFHneg_by_dAB[n]*(G.dReFHpos_by_dAB[n]*TGH.d2LL_by_dReFHpos_dImFHneg +
                                               G.dImFHpos_by_dAB[n]*TGH.d2LL_by_dImFHpos_dImFHneg +
                                               G.dReFHneg_by_dAB[n]*TGH.d2LL_by_dReFHneg_dImFHneg +
                                               G.dImFHneg_by_dAB[n]*TGH.d2LL_by_dImFHneg2);
                    }
                  }
                  //return array n_adp*n_adp/2 elements i.e upper triangle
                  auto constrained = atoms.site_independent_curvatures(a,hess21);
                  sv_int icd(X.n_adp,0); //index to constrained diagonal elements
                       if (X.n_adp == 1) icd = {0};
                  else if (X.n_adp == 2) icd = {0,2};
                  else if (X.n_adp == 3) icd = {0,3,5};
                  else if (X.n_adp == 4) icd = {0,4,7,9};
                  else if (X.n_adp == 5) icd = {0,5,9,12,14};
                  else if (X.n_adp == 6) icd = {0,6,11,15,18,21};
                  for (int n = 0; n < X.n_adp; n++)
                    if (refine_parameter_[mm++])
                      Curvature[ii++] += constrained[icd[n]];
                }
              } //loop over atoms
            }
          }

          for (auto item : SCATTERERS->FP_FDP) //FDP
          {
            const std::string& t = item.first;
            if (refine_parameter_[m++])
            {
              tgh.Gradient[i++] +=
                TGH.dLL_by_dReFHpos * atoms.dReFHpos_by_dFdp[t] +
                TGH.dLL_by_dImFHpos * atoms.dImFHpos_by_dFdp[t] +
                TGH.dLL_by_dReFHneg * atoms.dReFHneg_by_dFdp[t] +
                TGH.dLL_by_dImFHneg * atoms.dImFHneg_by_dFdp[t] +
                0 ;
                //TGH.dLL_by_dSigmaH*dSigmaH_by_dFdp[t];
            }
          }
          if (do_hessian)
          {
            for (auto item : SCATTERERS->FP_FDP) //FDP
            {
              const std::string& t = item.first;
              if (refine_parameter_[mm++])
              {
                PHASER_ASSERT(ii < Curvature.size());
                PHASER_ASSERT(atoms.dReFHneg_by_dFdp.count(t));
                Curvature[ii++] +=
                  fn::pow2(atoms.dReFHpos_by_dFdp[t])*TGH.d2LL_by_dReFHpos2 +
                  fn::pow2(atoms.dImFHpos_by_dFdp[t])*TGH.d2LL_by_dImFHpos2 +
                  fn::pow2(atoms.dReFHneg_by_dFdp[t])*TGH.d2LL_by_dReFHneg2 +
                  fn::pow2(atoms.dImFHneg_by_dFdp[t])*TGH.d2LL_by_dImFHneg2 +
                  2*atoms.dReFHpos_by_dFdp[t]*atoms.dImFHpos_by_dFdp[t]*TGH.d2LL_by_dReFHpos_dImFHpos +
                  2*atoms.dReFHpos_by_dFdp[t]*atoms.dReFHneg_by_dFdp[t]*TGH.d2LL_by_dReFHpos_dReFHneg +
                  2*atoms.dReFHpos_by_dFdp[t]*atoms.dImFHneg_by_dFdp[t]*TGH.d2LL_by_dReFHpos_dImFHneg +
                  2*atoms.dReFHneg_by_dFdp[t]*atoms.dImFHneg_by_dFdp[t]*TGH.d2LL_by_dReFHneg_dImFHneg +
                  2*atoms.dReFHneg_by_dFdp[t]*atoms.dImFHpos_by_dFdp[t]*TGH.d2LL_by_dImFHpos_dReFHneg +
                  2*atoms.dImFHpos_by_dFdp[t]*atoms.dImFHneg_by_dFdp[t]*TGH.d2LL_by_dImFHpos_dImFHneg;
              }
            }
          }
        } //gradient
      } //selected
    } //reflections

    if (false and do_restraint and refine_parameter_.size())
    {
      int m(0),i(0);
      for (int b = 0; b < NBINS; b++)
      {
        if (refine_parameter_[m++]) i++; //SigmaH
        if (refine_parameter_[m++]) i++; //fDelta
        if (refine_parameter_[m++]) i++; //rhopm
      }
      if (refine_parameter_[m++]) i++; //PartK
      if (refine_parameter_[m++]) i++; //PartB
      for (int a = 0; a < atoms.size(); a++)
      {
        pod::xray::scatterer& A = atoms.XRAY[a];
        pod::xtra::scatterer& X = atoms.XTRA[a];
        if (!X.rejected)
        {
          for (int x = 0; x < X.n_xyz; x++)
          {
            if (refine_parameter_[m++]) i++; //XYZ
          }
          if (refine_parameter_[m++]) i++; //OCC
          if (!A.flags.use_u_aniso_only()) //BFAC
          {
            double sigmaSqr = fn::pow2(SIGMA_WILSON_U);
            double udiff = (A.u_iso-WILSON_U);
            if (SIGMA_WILSON_U)
            {
              PHASER_ASSERT(sigmaSqr > 0);
              tgh.Target += fn::pow2(udiff)/sigmaSqr/2;
            }
            if (refine_parameter_[m++])
            {
              if (do_gradient and SIGMA_WILSON_U)
              {
                tgh.Gradient[i] += udiff/sigmaSqr;
                if (do_hessian)
                  Curvature[i] += 1/sigmaSqr;
              }
              i++;
            }
          }
          else //BFAC
          {
            dmat6 u_star_grad = {0,0,0,0,0,0};
            af_double  hess21(21,0); //upper triangle matrix (symmetric)
            if (SIGMA_SPHERICITY)
            { //add penalty even if not refined
              cctbx::adptbx::factor_u_star_u_iso<double> factor(cctbxUC,A.u_star);
              for (int n = 0; n < 6; n++)
              {
                double sigmaSqr = fn::pow2(u_star_sphericity[n]);
                PHASER_ASSERT(sigmaSqr > 0);
                tgh.Target += fn::pow2(factor.u_star_minus_u_iso[n])/sigmaSqr/2.;
              }
              if (do_gradient)
              {
                for (int ni = 0; ni < 6; ni++)
                {
                  for (int n = 0; n < 6; n++)
                  {
                    double du_by_dustarI = u_star_coeffs[ni]*u_iso_coeffs[n];
                    double dustar_by_dustarI = (n == ni) ? 1. : 0.;
                    double udiff = (dustar_by_dustarI-du_by_dustarI);
                    double sigmaSqr = fn::pow2(u_star_sphericity[n]);
                    u_star_grad[ni] += factor.u_star_minus_u_iso[n]*udiff/sigmaSqr;
                  }
                }
                if (do_hessian)
                {
                  int ni_nj(0);
                  for (int ni = 0; ni < 6; ni++)
                  {
                    for (int nj = ni; nj < 6; nj++,ni_nj++) //only calculate upper triangle
                    {
                      for (int n = 0; n < 6; n++)
                      {
                        double dBetaIso_by_dBetaAnoI = u_star_coeffs[ni]*u_iso_coeffs[n];
                        double dBetaAno_by_dBetaAnoI = (n == ni) ? 1. : 0.;
                        double dBetaIso_by_dBetaAnoJ = u_star_coeffs[nj]*u_iso_coeffs[n];
                        double dBetaAno_by_dBetaAnoJ = (n == nj) ? 1. : 0.;
                        // tgh.Hessian at this point refers to LL, not minusLL, so subtract
                        hess21[ni_nj] += (dBetaAno_by_dBetaAnoJ-dBetaIso_by_dBetaAnoJ) *
                                         (dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
                                         fn::pow2(u_star_sphericity[n]);
                      }
                    }
                  }
                }
              }
            }
            if (SIGMA_WILSON_U)
            {
              double u_iso = cctbx::adptbx::u_star_as_u_iso(cctbxUC,A.u_star);
              double udiff = (u_iso-WILSON_U);
              double sigmaSqr = fn::pow2(SIGMA_WILSON_U);
              PHASER_ASSERT(sigmaSqr > 0);
              tgh.Target += fn::pow2(udiff)/sigmaSqr/2.;
              if (do_gradient)
              {
                for (int n = 0; n < 6; n++)
                {
                  u_star_grad[n] += u_star_coeffs[n]*udiff/sigmaSqr;
                }
                for (int n = 0; n < 6; n++)
                {
                  hess21[hess21_diagonal[n]] += fn::pow2(u_star_coeffs[n])/sigmaSqr;
                }
              }
            }
            //return array n_adp*n_adp/2 elements i.e upper triangle
            auto constrained_grad = atoms.adp_independent_gradient(a,u_star_grad);
            auto constrained_hess = atoms.site_independent_curvatures(a,hess21);
            sv_int icd(X.n_adp,0); //index to constrained diagonal elements
                 if (X.n_adp == 1) icd = {0};
            else if (X.n_adp == 2) icd = {0,2};
            else if (X.n_adp == 3) icd = {0,3,5};
            else if (X.n_adp == 4) icd = {0,4,7,9};
            else if (X.n_adp == 5) icd = {0,5,9,12,14};
            else if (X.n_adp == 6) icd = {0,6,11,15,18,21};
            // load arrays
            for (int n = 0; n < X.n_adp; n++)
            {
              if (refine_parameter_[m++])
              {
                if (do_gradient)
                {
                  tgh.Gradient[i] += constrained_grad[n];
                  if (do_hessian)
                    Curvature[i] += constrained_hess[icd[n]];
                }
                i++;
              }
            }
          }
        }
      }
      for (auto item : SCATTERERS->FP_FDP) //FDP
      {
        double& initial_fdp = INITIAL_FDP.at(item.first);
        double sigmaSqr = fn::pow2(SIGMA_FDP*initial_fdp);
        double fdpdiff = std::fabs(item.second.fdp() - initial_fdp);
        if (SIGMA_FDP and initial_fdp > 0) //Hydrogen fdp=0
        {
          PHASER_ASSERT(sigmaSqr > 0);
          tgh.Target += fn::pow2(fdpdiff)/sigmaSqr/2.;
        }
        if (refine_parameter_[m++])
        {
          if (do_gradient and SIGMA_FDP and initial_fdp)
          {
            tgh.Gradient[i] += fdpdiff/sigmaSqr;
            if (do_hessian)
              Curvature[i] += 1/sigmaSqr;
          }
          i++;
        }
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    } //do_restraint

    //convert diagonal hessian to 2D
    if (do_hessian)
      for (int i = 0; i < nmp; i++)
        tgh.Hessian(i,i) = Curvature[i]; //sum all refls
    return tgh;
  }

} //phasertng
