//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_pdprint_class__
#define __phasertng_pod_pdprint_class__
#include <map>
#include <utility>

namespace phasertng {
namespace pod {

class pdprint
{
  public: //members
    int p = 0;
    int d = 0;
    int pd = 0;
    bool boolean = true;
    std::map<std::string,std::pair<double,double> > mm;
    std::map<std::string,int> npres;
    std::map<std::string,bool> empty;

  public: //constructor
    pdprint() {}

  public:
    pdprint(
        af_double flt,
        int minpd
      )
    {
      minpd = std::max(5,minpd);
      mm["ALL"] = { std::numeric_limits<double>::max(), -std::numeric_limits<double>::max() };
      for (int r = 0; r < flt.size(); r++)
      {
        double& dat = flt[r];
        mm["ALL"].first = std::min(mm["ALL"].first,dat);
        mm["ALL"].second = std::max(mm["ALL"].second,dat);
      }
      if (mm["ALL"].first == std::numeric_limits<double>::max())
      {
        p = minpd;
        d = 0;
        pd = minpd;
        empty["ALL"] = true;
      }
      else
      {
        mm["ALL"].second = std::abs(mm["ALL"].second);
        p = (mm["ALL"].second != 0) ? (int) log10 ((double) mm["ALL"].second) + 1 : 1;
        if (p < 0) { d = minpd-1; p = 1; }
        else d = (p <= 5) ? (5-p) : 0;
        if (d == 0) { d++; p++; }  //show floats as floats not ints
        pd = p + d + 2; //1 for decimal point, one for sign (phase, intensity)
        p = std::max(p,minpd);
        pd = std::max(pd,minpd);
      }
    }
};

} //pod
} //phasertng
#endif
