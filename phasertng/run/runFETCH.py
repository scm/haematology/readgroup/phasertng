import sys,os,copy,json
import argparse
import iotbx
import cctbx
import cctbx.uctbx.determine_unit_cell
from iotbx.file_reader import any_file
import traceback
import iotbx.pdb.fetch
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.scripts.jiffy import cleanup_hierarchy
from phasertng.scripts.jiffy import format_sequence
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.scripts.jiffy import generate_list_of_pdb_hierarchy_by_chain
from phasertng.phil.master_node_file import node_separator
from scitbx.array_family import flex
from cctbx.crystal import reindex
from cctbx import crystal
import iotbx.pdb
from cctbx.array_family import flex
from cctbx.sgtbx import change_of_basis_op
from six.moves import cStringIO as StringIO
import phasertng.scripts.simple_tools as simple_tools
import phasertng.scripts.system_utility as system_utility
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
from phasertng.phil.master_phil_file import *

class runFETCH:

  def run_from_basetng_and_input(self,basetng):
 #{{{
    self.inner_fetch(
      basetng=basetng,
      io_tute_force_match = isNone(basetng.input.get_as_str(".phasertng.pdbout.pdbid.")),
      io_mirrors = basetng.input.get_as_str(".phasertng.mirrors.").strip().split(),
      )
 #}}}

  #crazy names from first implementation
  def inner_fetch(self,
          basetng,
          io_tute_force_match,
          io_mirrors,
         ):
 #{{{
    def ispdb(name):
      return (name is not None) and len(name) != 4
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0):
      basetng.logTab(enum_out_stream.logfile,"No nodes")
      return
    DAGDB.shift_tracker(str(basetng.hashing()),"fetch")
    #fetch node will always be connected to the root
    if (io_tute_force_match is None):
      raise ValueError(enum_err_code.input,"fetch pdbid or file not defined")

    can_download = None
    z = None
    if (ispdb(io_tute_force_match)):
   #{
      basetng.logTab(enum_out_stream.summary,"Load file into memory: "+io_tute_force_match)
      pdbid = os.path.splitext(os.path.basename(io_tute_force_match))[0] # include the ext
      title = pdbid
      try:
        datap = iotbx.pdb.input(io_tute_force_match).as_pdb_string()
        can_download = True
      except:
        can_download = False
   #}
    else:
   #{
      pdbid = io_tute_force_match
     #basetng.logTab(enum_out_stream.logfile, "title: " + pdbid_title)
      cannot_download = False
      mirrors = io_mirrors
      for mirror in mirrors:
        if can_download: break
        try:
          basetng.logTab(enum_out_stream.summary,"Fetch pdb [" + pdbid.lower() + "] from " + mirror + "...")
          datap = iotbx.pdb.fetch.fetch(pdbid.lower(),
                         entity="model_pdb",
                         mirror=mirror,
                         ).read().decode(system_utility.get_encoding())
          can_download = True
        except Exception as e:
          basetng.logTab(enum_out_stream.logfile,"Error: " + str(e))
          can_download = False #if this is the last time through, this sticks
      for mirror in mirrors:
        if can_download: break
        try:
          basetng.logTab(enum_out_stream.summary,"Fetch cif [" + pdbid.lower() + "] from " + mirror + "...")
          datap = iotbx.pdb.fetch.fetch(pdbid.lower(),
                         entity="model_cif",
                         mirror=mirror,
                         ).read().decode(system_utility.get_encoding())
          can_download = True
        except Exception as e:
          basetng.logTab(enum_out_stream.logfile,"Error: " + str(e))
          can_download = False #if this is the last time through, this sticks
   #}
    #use ebi to get the title
    try:
       pdburl = "https://www.ebi.ac.uk/pdbe/api/pdb/entry/summary/" + str(pdbid.lower())
       summary = json.loads(system_utility.request_url(pdburl, "www.ebi.ac.uk"))[pdbid.lower()][0]
       title = summary["title"]
    except Exception:
       title = pdbid

    if can_download is None or not can_download:
      basetng.logTab(enum_out_stream.summary, "Download unsuccessful")
    else:
   #{
      basetng.logTab(enum_out_stream.summary, "Download successful")
      basetng.logTab(enum_out_stream.summary,"Title: " + title)
      basetng.logBlank(enum_out_stream.summary)
      datap_lines=flex.split_lines(str(datap))
      pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=datap_lines)
      basetng.input.set_as_str(".phasertng.pdbout.deposited_title.",str(title))
      where = enum_out_stream.logfile
      basetng.logUnderLine(where,"Prepare structure")
      for line in flex.split_lines(str(pdb_input.crystal_symmetry())):
        basetng.logTab(where, line)
      basetng.logTab(where,"Cleanup structure to single model of first chain with no hetero atoms")
      pdbih = pdb_input.construct_hierarchy()
      nchains,unknown,pdbih = cleanup_hierarchy(pdbih)
      if (nchains == 0):
          basetng.logWarning("Cleanup of structure resulted in no chains")
          basetng.logTab(where,"Reverting to full structure")
          pdbih = pdb_input.construct_hierarchy()
      basetng.logTab(where,"Sequence from structure")
      hlist = generate_list_of_pdb_hierarchy_by_chain(pdbih)
      if (len(hlist)) == 0:
        raise ValueError(enum_err_code.input,"Bad pdb file")
      seqdata = ""
      class BreakLoopException(Exception):
        pass
      try:
        for ha in hlist:
          for chain in ha.models()[0].chains():
            basetng.logTab(where,"Chain: \'" + str(chain.id) + "\'" + " residues=" + str(len(chain.as_sequence())))
            lines = ''.join(chain.as_sequence())
            lines = format_sequence(lines)
            for line in lines:
              basetng.logTab(where,line)
            seqdata += ">" +pdbid + " chain " + str(chain.id) + "\n"
            seqdata += chain.as_padded_sequence() + "\n"
         #  if len(chain.id) > 2:
         #    seqdata += "New chain " + str(chain.id[2:]) + "\n"
         #    chain.id[2:]
            raise BreakLoopException
      except BreakLoopException:
        pass  # Handle the exception (just pass to exit the loops)
      basetng.logTab(where,"Number of atoms in pdb entry: " + str(len(pdbih.atoms())))
      basetng.logTab(where,"Number of atoms in first chain: " + str(len(chain.atoms())))
      for chain in hlist[0].chains(): #only one
        if len(chain.id) > 2:
          chain.id = "A" #allowed pdb chain.id if cif is larger
       #  aka lines = [line.replace("AAA","A") for line in lines] #eg for 8ouc
      pdb_or_mmcif_string = str(hlist[0].as_pdb_or_mmcif_string(target_format="pdb"))
      debug = False
      if debug:
        print(str(pdb_or_mmcif_string))
      def determine_format(data):
          try:
              iotbx.pdb.input(source_info=None, lines=data)
              return "PDB"
          except Exception:
              pass
          return "Unknown"
      if determine_format(pdb_or_mmcif_string) != "PDB":
        raise ValueError(enum_err_code.input,"Pdb format error")
      lines=flex.split_lines(pdb_or_mmcif_string)
      ident = abs(hash(pdbid)) % (10 ** 8) # 8 digit number
      ent = ""
      if (len(lines) == 0):
        raise ValueError(enum_err_code.input,"No atoms in pdb file")
      for line in lines:
        ent = ent + line + node_separator
      basetng.input.set_as_def(".phasertng.pdbout.filename.") # in the generic string
      basetng.input.set_as_str(".phasertng.pdbout.id.",str(ident))
      basetng.input.set_as_str(".phasertng.pdbout.tag.",str(pdbid))
      basetng.input.generic_int = 1 # flag
      basetng.input.generic_string = str(ent) #don't put this in the input for unparsing, it is massive
      assert(len(basetng.input.generic_string))
      where = enum_out_stream.verbose
      basetng.logTab(where,"Pdb:")
      basetng.logTab(where,lines[0])
      basetng.logTab(where,"...")
      basetng.logTab(where,lines[-1])

      wf = basetng.WriteData()
      if wf:
        where = enum_out_stream.logfile
        basetng.logUnderLine(where,"Write Pdb Files")
        assert(len(pdbid))
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        pdbdir = os.path.join(basetng.DataBase(),subdir)
        if not FileSystem().flat_filesystem() and not os.path.exists(pdbdir):
            os.makedirs(pdbdir)
        if pdbid.endswith(".pdb"):
          basename = os.path.basename(pdbid)[:-4]
        else:
          basename = pdbid
        basename = basename+"_"+chain.id.strip()
        newpdbname = os.path.join(pdbdir,basename + ".pdb")
        basetng.logTab(where,"File: " + newpdbname)
        hlist[0].write_pdb_file(newpdbname)
        basetng.input.set_as_str(".phasertng.pdbout.filename.",str(newpdbname))
   #}
    return basetng
 #}}}

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-o',"--output",help="database directory for output",required=True)
  parser.add_argument('-f',"--force",dest='force',default=None,help="force match with pdbid",required=False)
  args = parser.parse_args()
  basetng = Phasertng("VOYAGER")
  basetng.SetDataBase(args.output)
  runFETCH().inner_fetch(
        basetng=basetng,
        io_tute_force_match=(args.force),
        io_mirrors=['rscb','pdbe','pdbj'],
        )
  print(basetng.logfile())
