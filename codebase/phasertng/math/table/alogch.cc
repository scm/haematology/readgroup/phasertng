#include <phasertng/math/table/alogch.h>
#include <cmath>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace table {

constexpr const double table::alogch::lnchTable1[2049][2];
constexpr const double table::alogch::lnchTable2[2049][2];
  alogch::alogch()
  {
    argfac1 = tbllen/argcut1;
    argfac2 = tbllen/argcut2;
  }

  double
  alogch::get(double arg) const
  {
    double absArg(std::fabs(arg));
    if (absArg < argcut1)
    {
      double tableArg = absArg*argfac1;
      int index = static_cast<int>(tableArg);
      phaser_assert(index < tbllen1);
      return lnchTable1[index][0] + (tableArg-index)*lnchTable1[index][1];
    }
    else if (absArg < argcut2)
    {
      double tableArg = absArg*argfac2;
      int index = static_cast<int>(tableArg);
      phaser_assert(index < tbllen1);
      return lnchTable2[index][0] + (tableArg-index)*lnchTable2[index][1];
    }
    else
      return (absArg-log2);
  }

  alogch alogch::inst_;

}}
