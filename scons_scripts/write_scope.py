from __future__ import print_function
from __future__ import division

import libtbx.load_env
import sys, os, os.path, shutil
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
from master_phil_file import phil_scope
from master_node_file import node_scope,node_separator

def write_scope_cc(source, target, env):
  sys.stdout.write( "phasertng: Writing scope function file " + env.scope_cc + "\n")
  f1 = open(env.scope_cc, "w")
  f1.write("// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
  f1.write("// EDITS WILL BE OVERWRITTEN!\n")
  f1.write("#include <autogen/include/Scope.h>\n")
  f1.write("namespace phasertng {\n")
  f1.write("const std::string PHIL_SCOPE() { return \"" + phil_scope + "\"; }\n")
  f1.write("const std::string NODE_SCOPE() { return \"" + node_scope + "\"; }\n")
  f1.write("const std::string NODE_SEPARATOR() { return \"" + node_separator + "\"; }\n")
  f1.write("}\n")
  f1.close()
  print("phasertng: Copying " + str(env.scope_cc))
  f = "Scope.cc"
  assert(env.scope_cc.endswith(f))
  verbose = False
  if verbose:
    print("phasertng: " +
                os.path.join(env.autogen,f) + " --> " +
                os.path.join(env.dist_path,"codebase","autogen",f))
  shutil.copy(os.path.join(env.autogen,f),
                os.path.join(env.dist_path,"codebase","autogen",f))

if __name__ == "__main__":
  source = ""
  target = ""
  os.scope_cc  = "Scope.cc"
  os.autogen  = "."
  os.dist_path  = ".."
  write_scope_cc(source,target,env=os)
