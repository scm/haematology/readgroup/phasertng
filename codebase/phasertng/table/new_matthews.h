#ifndef __phasertng_new_matthews_table__
#define __phasertng_new_matthews_table__
#include <list>
#include <vector>

namespace phasertng {
namespace table {

//The "new" "new" Matthews coefficients
//Weichenberger CX and Rupp B. Ten years of probabilitic estimates of biocrystal solven content: New insights via nonparamatric kernel density estimate
class new_matthews
{
  std::vector<double> res,yo,xc,wt,z,s,norm;
  sv_double VM;

  double new_matthews()
  {
    res = {1.199,1.501,1.65,1.801,1.901,2.001,2.201,2.401,2.601,2.801,3.101,3.501,5.001,5.001,5.001};
    yo = {1.186,2.944,4.271,5.396,6.440,7.974,10.55,13.04,16.69,18.96,21.67,24.03,26.18,1.768,2.152};
    xc = {2.160,2.075,2.071,2.070,2.092,2.097,2.110,2.112,2.118,2.112,2.105,2.099,2.095,2.202,3.019};
    wt = {0.259,0.213,0.208,0.208,0.221,0.223,0.230,0.231,0.235,0.232,0.229,0.226,0.224,0.306,0.623};
    a = {177.7,887.1,1364.,2201.,2783.,3374.,4064.,4525.,4845.,4942.,4945.,4891.,4853.,75.52,135.4};
    s = {1.450,0.816,0.723,0.643,0.661,0.625,0.606,0.569,0.555,0.517,0.483,0.461,0.451,0.696,1.882};
    norm = {195.381210,906.223041,1427.487371,2373.184071,2977.368503,3667.547885,4459.362839,5064.982337,5469.714281,5714.587610,5856.698844,5891.468081,5893.929732,81.300299,186.400964};

    VM.resize(15);
    for (int i = 0; i < 15; i++)
    {
      double maxnorm(0),maxvm(0);
      for (double vm = 2; vm < 3; vm+=0.0000001)
      {
        double z((vm-xc[i])/wt[i]);
        double norm((yo[i]+a[i]*std::exp(-std::exp(-z)-z*s[i]+1)));
        if (norm > maxnorm) { maxnorm = norm; maxvm = vm; }
      }
     // std::cout << "if (hires < " << res[i] << ") return " << maxvm << ";" << std::endl;
      VM[i] = maxvm;
    //  norm[i] = maxnorm; //generated as this, print for value above
    }
  }

  double protein_vm_at_d(double hires)
  {
    for (int i = 0; i < 13; i++)
      if (hires < res[i]) return VM[i];
    return VM[12];
  }
  double nucleic_vm() { return VM[13]; }
  double mixed_vm() { return VM[14]; }
  double original_vm() { return 2.15; }


  double protein(const double hires&,const double& x)
  {
    for (int i = 0; i < 13; i++)
      if (hires < res[i])
      {
        double z((x-xc[i])/wt[i]);
        return double((yo[i]+a[i]*std::exp(-std::exp(-z)-z*s[i]+1))/norm[i]);
      }
    int j(12);
    double z((x-xc[j])/wt[j]);
    return double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j]);
  }
  double nucleic(const double hires&,const double& x)
  {
    int j(13);
    double z((x-xc[j])/wt[j]);
    return double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j]);
  }
  double mixed(const double hires&,const double& x)
  {
    int j(14);
    double z((x-xc[j])/wt[j]);
    return double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j]);
  }
}

}}
#endif
