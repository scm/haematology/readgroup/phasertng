#ifndef __phasertng_spherical_as_cartesian_function__
#define __phasertng_spherical_as_cartesian_function__
#include <scitbx/vec3.h>

typedef scitbx::vec3<double> dvect3;

namespace phasertng {

class spherical_as_cartesian
{
  double x=0;
  double y=0;
  double z=0;

  public:
  spherical_as_cartesian(dvect3 v,bool degrees)
  {
    //phi is the azimuthal angle
    //theta is zenith angle
    double& radius = v[0];
    double& phi = v[1]; //azimuth
    double& theta = v[2]; //zenith
    if (degrees)
    {
      phi = scitbx::deg_as_rad(phi);
      theta = scitbx::deg_as_rad(theta);
    }
    x=radius*std::cos(phi)*std::sin(theta);
    y=radius*std::sin(phi)*std::sin(theta);
    z=radius*std::cos(theta);
  }

  dvect3 axis()
  { return dvect3(x,y,z); }
};

}
#endif
