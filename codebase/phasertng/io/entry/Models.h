//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Models_class____
#define __phasertng_Models_class____
#include <phasertng/main/includes.h>
#include <phasertng/io/EntryBase.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/io/principal_statistics.h>
#include <phasertng/io/principal_components.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/io/serial.h>
#include <phasertng/pod/composition.h>
#include <unordered_set>
#include <unordered_map>

namespace phasertng {

  class Models  : public EntryBase
  {
    protected:
      std::unordered_set<std::string> pdb_errors = {};

    public: //members
      Identifier  MODLID;
      Identifier  REFLID;
      std::string TIMESTAMP = "";
      sv_double   RELATIVE_WILSON_B = sv_double(0);
      std::vector<std::vector<PdbRecord>> PDB;
      pod::serial SERIAL = pod::serial();
      int         COUNT_SSBOND = 0;
      bool        SELENOMETHIONINE = false;
      bool        USE_MSE = false;
      sv_string   REMARKS = sv_string(0);
      UnitCell    INPUT_CELL;
      principal_statistics statistics;
      principal_components components;

    public: //virtual base
      bool in_memory() { return PDB.size(); }
      void read_from_disk() { ReadPdb(); }
      void write_to_disk() { WritePdb(); }

    public: //constructors
      Models() : EntryBase() {}
      Models(af_string pdblines) : EntryBase() { SetPdb(pdblines); }

    public:
      void       ReadPdb();
      void       SetPdb(af_string);
      af_string  get_pdb_cards();
      void       WritePdb();
      void       GetPdbHandler(PdbHandler&);

    public:
      bool                  has_read_errors() const { return pdb_errors.size() > 0; }
      bool                  has_relative_wilson_bfactor() const { return RELATIVE_WILSON_B.size(); }
      bool                  is_set() const { return bool(PDB.size() > 0); }
      std::string           read_errors() const;
      bool                  is_atom() const;
      bool                  is_helix() const;
      bool                  bfactors_damped_after_relative_wilson_b() const;
      std::unordered_set<std::string> set_of_atomtypes() const;
      int                   mtrace() const;
      const sv_double&      card_vrms() const;
      void                  move_and_align_to_principal_axes(dmat33,dvect3);
      composition_t         composition(int);
      sv_int                scattering();
      sv_double             molecular_weight();
      void                  conserved_trace(std::vector<PdbRecord>&,double&) const;
      void                  get_best_pdb_cards(Identifier&,Identifier&,std::string&,std::vector<PdbRecord>&) const;
      void                  convert_rmsd_to_bfac();
      double                trim(double);
 //     void                  sequence(int,Sequence&);
  //    double                volume(int);
 //     composition::type     composition_type(int);
      bool                  is_random_coil(double);
      void                  reorient_on_centre(dmat33,dvect3,dvect3);
      void                  recentre(dvect3);
      void                  renumber();
    //  af_dvect3             get_atom_list();
      void                  set_principal_statistics();
      void                  set_principal_components();
      double                maximum_radius();
  };
}
#endif
