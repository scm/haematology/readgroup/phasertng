//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/minimizer/RefineSPIN.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/main/Error.h>
#include <phasertng/math/xyzRotMatDeg.h>
#include <phasertng/math/rotation/rotdist.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <phasertng/math/Matrix.h>
#include <phasertng/math/Tensor.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>
#include <unordered_map>
#include <set>
#include <future>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

  RefineSPIN::RefineSPIN(
      const_ReflectionsPtr REFLECTIONS_,
      sv_bool  SELECTED_,
      const_EpsilonPtr  EPSILON_)
    : RefineBase(),
      REFLECTIONS(REFLECTIONS_),
      SELECTED(SELECTED_),
      EPSILON(EPSILON_)
  {
  }

  void
  RefineSPIN::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineSPIN::init_node(dag::Node* work,pod::FastPoseType& ecalcs_sigmaa_pose_)
  {
    negvar = false;
    NODE = work;
    ecalcs_sigmaa_pose = ecalcs_sigmaa_pose_;
    PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
    phaser_assert(ecalcs_sigmaa_pose.size() == REFLECTIONS->NREFL);
    if (NODE->HALL.size() and NODE->HALL != REFLECTIONS->SG.HALL)
    throw Error(err::INPUT,"Node space group is not that of data");
    const auto& entry = NODE->NEXT.ENTRY;
    dvect3 maxperp =
        { std::max(entry->INTERPOLATION.UC.B(),entry->INTERPOLATION.UC.C()),
          std::max(entry->INTERPOLATION.UC.C(),entry->INTERPOLATION.UC.A()),
          std::max(entry->INTERPOLATION.UC.A(),entry->INTERPOLATION.UC.B()) } ;
    for (int rot = 0; rot < 3; rot++)
    {
      LARGE_ROTN[rot] = 4*scitbx::rad_as_deg(REFLECTIONS->data_hires()/maxperp[rot]);
    }
  }

  void
  RefineSPIN::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::unordered_set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(item);
    REFINE_ROTN = (protocol.find("rotn") != protocol.end());
    REFINE_VRMS = (protocol.find("vrms") != protocol.end());
    REFINE_VRMS = REFINE_VRMS and NODE->turn_is_novel();
    if (protocol.find("off") != protocol.end())
       REFINE_ROTN = REFINE_VRMS = false;

    bool REFINE_ON(true),REFINE_OFF(false);
    refine_parameter_.clear();
    for (int rot = 0; rot < 3; rot++)
      REFINE_ROTN ? refine_parameter_.push_back(REFINE_ON) : refine_parameter_.push_back(REFINE_OFF);
    REFINE_VRMS ? refine_parameter_.push_back(REFINE_ON) : refine_parameter_.push_back(REFINE_OFF);

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();

    //make sure parameters are within bounds
    auto x = get_macrocycle_parameters();
    auto bound = bounds();
    if (x.size() == bound.size())
    {
      for (int i = 0; i < x.size(); i++)
      {
        if (bound[i].upper_bounded() and (x[i] > bound[i].upper_limit() ))
        {
          x[i] = bound[i].upper_limit();
        }
        if (bound[i].lower_bounded() and (x[i] < bound[i].lower_limit() ))
        {
          x[i] = bound[i].lower_limit();
        }
      }
    }
    set_macrocycle_parameters(x);
  }

  sv_string
  RefineSPIN::macrocycle_parameter_names()
  {
    if (!refine_parameter_.size()) return sv_string();
    sv_string names(nmp);
    int i(0),m(0);
    if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation X ";
    if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation Y ";
    if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation Z ";
    auto modlid = NODE->NEXT.IDENTIFIER;
    if (refine_parameter_[m++]) names[i++] = "Drms for " + modlid.str();
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineSPIN::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    auto& turn = NODE->NEXT;
    for (int rot = 0; rot < 3; rot++)
      if (refine_parameter_[m++])
        pars[i++] = turn.ORTHR[rot];
    if (refine_parameter_[m++])
    {
      auto modlid = NODE->NEXT.IDENTIFIER;
      pars[i++] = NODE->DRMS_SHIFT[modlid].VAL;
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineSPIN::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    auto& turn = NODE->NEXT;
    for (int rot = 0; rot < 3; rot++)
    {
      if (refine_parameter_[m++]) //ROT
      {
        turn.ORTHR[rot] = newx[i++];
      }
    }
    if (refine_parameter_[m++]) //VRMS
    {
      auto modlid = NODE->NEXT.IDENTIFIER;
      NODE->DRMS_SHIFT.at(modlid).VAL = newx[i++];
    }
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineSPIN::macrocycle_large_shifts()
  {
    double large_vrms = REFLECTIONS->data_hires()/20.0;
    sv_double largeShifts(nmp);
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot++)
    {
      if (refine_parameter_[m++])
      {
        largeShifts[i++] = LARGE_ROTN[rot];
      }
    }
    if (refine_parameter_[m++])
      largeShifts[i++] = large_vrms; //VRMS
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineSPIN::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    for (int rot = 0; rot < 3; rot++)
      if (refine_parameter_[m++])
        bounds[i++].on(-9.99, 9.99); // Any sensible perturbation will be much smaller
    auto entry = NODE->NEXT.ENTRY;
    if (refine_parameter_[m++])
      bounds[i++].on(entry->DRMS_MIN, entry->DRMS_MAX); //VRMS
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineSPIN::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    sv_double pars(nmp);
    for (int rot = 0; rot < 3; rot++) //ROT
      if (refine_parameter_[m++])
        repar[i++].off();
    if (refine_parameter_[m++]) //VRMS
      repar[i++].off();
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineSPIN::likelihood()
  {
    bool do_gradient(false), do_hessian(false), do_restraint(false);
    TargetGradientHessianType Tuple = combined_tgh(do_gradient,do_hessian,do_restraint);
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don' want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineSPIN::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineSPIN::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*TargetGradientType(Tuple);
  }

  TargetGradientHessianType
  RefineSPIN::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineSPIN::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    //initialization
    do_gradient = (do_gradient or do_hessian);
    TargetGradientHessianType tgh(nmp,true);
    bool do_grad_fc(true); //may want to change for no-fc gradient refinement
    bool do_hess_fc(true); //may want to change for no-fc gradient refinement

    // tNCS case: curvatures (particularly vrms) should take account of correlations between
    // tNCS-related copies.  For now, rely on BFGS algorithm to compensate, but it would be better
    // to deal with this explicitly.
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;

    //AJM TODO look at the speed of this and set pointers between map lookups
    //and std::vectors indexed on p
    //and make this part of set protocol or init step so it is not done each time!!!

    //Gradient
    dvect3 dLL_by_dRotn(0,0,0);
    double dLL_by_dDrms(0);
    //Hessian
    dvect3 d2LL_by_dRotn2(0,0,0);
    double d2LL_by_dDrms2(0);

    auto turn_terms = NODE->turn_terms_interpolate();
    cvect3  turn_cos_sin;
    auto& turn = NODE->NEXT;
    for (int dir = 0; dir < 3; dir++)
      turn_cos_sin[dir] = tbl_cos_sin.get(turn.ORTHR[dir]/360.);

    int NSYMP = REFLECTIONS->SG.group().order_p();
    likelihood::iwilson::tgh_flags flags;
    flags.target(true).gradient(do_gradient).hessian(do_hessian);
    likelihood::iwilson::function IWILSON;
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::FEFFNAT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));
    phaser_assert(REFLECTIONS->has_col(labin::SSQR));
    phaser_assert(REFLECTIONS->has_col(labin::EPS));
   // PHASER_ASSERT(NODE->POSE.size() == 0);

    //NSYMP is the largest rsym.unique will get, when there are duplicates it will be fewer
    math::Matrix<cmplex> dEC_isym_by_dRotn(NSYMP,3,{0.,0.});
    math::Matrix<cmplex> d2EC_isym_by_dRotn2(NSYMP,3,{0.,0.});
    math::Matrix<cmplex> dEC_isym_by_dTran(NSYMP,3,{0.,0.});
    math::Matrix<cmplex> d2EC_isym_by_dTran2(NSYMP,3,{0.,0.});

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    xyzRotMatDeg rotmat;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const millnx& miller = work_row->get_miller();
        const double  teps = work_row->get_teps();
        const double& resn = work_row->get_resn();
        const double& dobs = work_row->get_dobsnat();
        const double& feff = work_row->get_feffnat();
        const double& ssqr = work_row->get_ssqr();
        const double& eps =  work_row->get_eps();
        const bool&   cent = work_row->get_cent();
        const double  g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1; //halfR=true
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);

        //result of summation
        //first pass calculate EC and V
        //for likelihood target, need summed EC and V value
        //no fixed components allowed

        //This loop should be separate if the InterpV is separate from the InterpE
        //since most of the time is in the interpolation
        //if not, then this loop can be combined below
        double DobsSigaSqr(0);
        double DobsSigaSqr_turn(0);
       // double scale_ssqr = ssqr*(CELL[modlid]/NODE->GRID);
        auto& turn = NODE->NEXT;
        const auto& entry = turn.ENTRY;
        dmat33 orthtr = entry->INTERPOLATION.UC.orthogonalization_matrix().transpose();
        const auto& modlid = entry->identify();
        double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
        double bfac(0);
        double fs = REFLECTIONS->fracscat(entry->SCATTERING);
        double drms = NODE->DRMS_SHIFT[modlid].VAL;
        double thisDobsSigaSqr = likelihood::iwilson::DobsSigaSqr(
          ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
        DobsSigaSqr_turn = thisDobsSigaSqr;
        if (nmol > 1)
        { //halfR = true
          double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
          if (tncs_modelled) Vterm /= nmol;
          thisDobsSigaSqr *= Vterm;
        }
        DobsSigaSqr += thisDobsSigaSqr; //one per pose, sum the squares
        DobsSigaSqr += fn::pow2(ecalcs_sigmaa_pose.sigmaa[r]);

        af_cmplex EC_isym(rsym.unique,{0.,0.});
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
        {
          const millnx& SymHKL = rsym.rotMiller[isym];
          dvect3 RotSymHKL = turn_terms.Q1tr*SymHKL;
          cmplex iTarget; cvect3 iGradient; cmat33 iHessian;
          std::tie(iTarget,iGradient,iHessian) =
              entry->INTERPOLATION.callTGH(RotSymHKL,do_grad_fc,do_hess_fc);
          cmplex scalefac = likelihood::iwilson::EcalcTerm(
              DobsSigaSqr_turn,eps,NSYMP,
              SymHKL,turn_terms.TRA,rsym.traMiller[isym]);
          cmplex thisE = scalefac*iTarget;
          cmplex ptncs_scat = cmplxONE; //imol=0
          if (nmol > 1 and tncs_not_modelled)
          { //halfR = true
            const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
            for (double imol = 1; imol < nmol; imol++)
              ptncs_scat += std::exp(TWOPII*imol*theta);
            ptncs_scat *= (nmol == 2) ? matrix_G(r,isym) : 1;
          }
          thisE *= ptncs_scat;
          EC_isym[isym] += thisE;
          //no G function correction (pfun), models in separate (real) rotations
          if (do_gradient or do_hessian)
          {
            cvect3 dthisE = ptncs_scat * scalefac * iGradient;
            cmat33 hthisE = ptncs_scat * scalefac * iHessian;
            if (REFINE_ROTN)
            {
              for (int rot = 0; rot < 3; rot++)
              {
                dmat33 dQ1tr_by_dRotn = turn_terms.Rtr_Dtr; //CELL in here somewhere?? AJM fix
                for (int dir = 2; dir >= 0; dir--)
                {
                  bool do_grad(dir==rot);
                  dQ1tr_by_dRotn = rotmat.grad_tr(dir,turn_cos_sin[dir],do_grad)*dQ1tr_by_dRotn;
                }
                dQ1tr_by_dRotn = orthtr * dQ1tr_by_dRotn;
                dvect3  dQ1tr_h_by_dRotn_p = dQ1tr_by_dRotn*SymHKL;
                dEC_isym_by_dRotn(isym,rot) = dthisE*dQ1tr_h_by_dRotn_p;
                if (do_hessian)
                {
                  dmat33 d2Q1tr_by_dRotn2 = turn_terms.Rtr_Dtr;
                  for (int dir = 2; dir >= 0; dir--)
                  {
                    bool do_hess(dir==rot);
                    d2Q1tr_by_dRotn2 = rotmat.hess_tr(dir,turn_cos_sin[dir],do_hess)*d2Q1tr_by_dRotn2;
                  }
                  d2Q1tr_by_dRotn2 = orthtr * d2Q1tr_by_dRotn2;
                  dvect3 d2Q1tr_h_by_dRotn2_p = d2Q1tr_by_dRotn2 * SymHKL;
                  d2EC_isym_by_dRotn2(isym,rot) =
                      dQ1tr_h_by_dRotn_p*(hthisE*dQ1tr_h_by_dRotn_p) +
                      dthisE*d2Q1tr_h_by_dRotn2_p;
                }
              }
            }
          } //gradient/hessian
          //EC_isym is summed over all turns, for one symmetry operator
        }
        double sum_Esqr_search(0);
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          sum_Esqr_search += std::norm(EC_isym[isym]);

        sum_Esqr_search += std::norm(ecalcs_sigmaa_pose.sigmaa[r]*ecalcs_sigmaa_pose.ecalcs[r]);
        //use wilson approximation even when there is a placed component

        //====
        //IWILSON calculation
        //====
        double V = teps - DobsSigaSqr + sum_Esqr_search;
        double eobs = feff/resn;
        IWILSON.target_gradient_hessian(eobs, teps, V, cent, flags);
        tgh.Target -= IWILSON.LL;
        NODE->LLG = -tgh.Target; //always at last value without restraints

        if (V <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        static std::mutex mutex;
        static int counter(0);
        if (V <= 0)
        {
          const std::lock_guard<std::mutex> lock(mutex);
          counter++;
          std::cout << "\n---------- " << counter <<std::endl;
          std::cout << "Debugging negative variance" << std::endl;
          std::cout << "V=" + std::to_string(IWILSON.variance()) << std::endl;
          std::cout << "reflection #" + std::to_string(r) << std::endl;
          std::cout << "teps="+ std::to_string(teps) << std::endl;
          std::cout << "g_drms="+ std::to_string(g_drms) << std::endl;
          std::cout << "DobsSigaSqr="+ std::to_string(DobsSigaSqr) << std::endl;
          std::cout << "sum_Esqr_search="+ std::to_string(sum_Esqr_search) << std::endl;
          std::cout << "ssqr="+ std::to_string(ssqr) << std::endl;
          std::cout << "reso="+ std::to_string(1/std::sqrt(ssqr)) << std::endl;
          std::cout << "npose="+ std::to_string(NODE->POSE.size()) << std::endl;
          std::cout << "eps="+ std::to_string(eps) << std::endl;
          std::cout << "nsymp="+ std::to_string(NSYMP) << std::endl;
          if (nmol > 2)
          std::cout << "Vterm ="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (nmol == 2)
          std::cout << "Vterm ="+ std::to_string(array_Gsqr_Vterm[r]) << std::endl;
          std::cout << "DobsSigaSqr_turn ="+ std::to_string(DobsSigaSqr_turn) << std::endl;
          std::string eg = std::to_string(turn.EULER[0]) + " "+
                           std::to_string(turn.EULER[1]) + " "+
                           std::to_string(turn.EULER[2]);
          std::cout << "Euler Angle =" + eg << std::endl;
                      eg = std::to_string(turn.ORTHR[0]) + " "+
                           std::to_string(turn.ORTHR[1]) + " "+
                           std::to_string(turn.ORTHR[2]);
          std::cout << "Euler Shift =" + eg << std::endl;
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            std::cout << "Ecalc isym="+ std::to_string(isym) + " " + std::to_string(std::abs(EC_isym[isym])) << std::endl;
          }
          std::cout << "Eobs="+ std::to_string(eobs) << std::endl;
          std::cout << "dobs="+ std::to_string(dobs) << std::endl;
          std::cout << "centric="+ std::string(cent?"Y":"N") << std::endl;
          std::cout << "----------" << std::endl;
          PHASER_ASSERT(!IWILSON.negative_variance());
        }//debug
#endif

        if (do_gradient or do_hessian)
        {
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double EA_sym = std::real(EC_isym[isym]);
            double EB_sym = std::imag(EC_isym[isym]);
            {
              if (REFINE_ROTN)
              {
                for (int rot = 0; rot < 3; rot++)
                {
                  double dEA_by_dRotn = dEC_isym_by_dRotn(isym,rot).real();
                  double dEB_by_dRotn = dEC_isym_by_dRotn(isym,rot).imag();
                  double dV_by_dRotn = 2*EA_sym*dEA_by_dRotn + 2*EB_sym*dEB_by_dRotn;
                  dLL_by_dRotn[rot] += IWILSON.dLL_by_dV*dV_by_dRotn;
                  if (do_hessian)
                  {
                    double d2EA_by_dRotn2 = d2EC_isym_by_dRotn2(isym,rot).real();
                    double d2EB_by_dRotn2 = d2EC_isym_by_dRotn2(isym,rot).imag();
                    double d2V_by_dRotn2 = 2*fn::pow2(dEA_by_dRotn) + 2*fn::pow2(dEB_by_dRotn) +
                                           2*EA_sym*d2EA_by_dRotn2 + 2*EB_sym*d2EB_by_dRotn2;
                    d2LL_by_dRotn2[rot] +=
                        IWILSON.d2LL_by_dV2*fn::pow2(dV_by_dRotn) +
                        IWILSON.dLL_by_dV*d2V_by_dRotn2;
                  }
                }
              }
            }
          }
          if (REFINE_VRMS)
          {
            double minusSsqrTwoPiSqrOn3 = -2*DEF_TWOPISQ_ON_THREE*ssqr;
            double dV_by_dDrms(0);
            double d2V_by_dDrms2(0);
            double Vconst = minusSsqrTwoPiSqrOn3;
            double Vconst2 = fn::pow2(Vconst);
            double& totvar_known_s = DobsSigaSqr_turn;
            dV_by_dDrms = -Vconst*totvar_known_s;
            d2V_by_dDrms2 = -Vconst2*totvar_known_s;
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              double EA = std::real(EC_isym[isym])*std::real(EC_isym[isym]);
              double EB = std::imag(EC_isym[isym])*std::imag(EC_isym[isym]);
              dV_by_dDrms += Vconst*(EA+EB);//sumEsqr term
              if (do_hessian)
              {
                d2V_by_dDrms2 += Vconst2*(EA+EB);
              }
            }
            dLL_by_dDrms += IWILSON.dLL_by_dV*dV_by_dDrms;
            d2LL_by_dDrms2 += IWILSON.d2LL_by_dV2*fn::pow2(dV_by_dDrms) +
                              IWILSON.dLL_by_dV*d2V_by_dDrms2;
          }
        } //gradient or hessian
      } //selected
    } //reflections

    if (do_gradient)
    {
      int m(0),i(0);
      for (int rot = 0; rot < 3; rot++)
      {
        if (refine_parameter_[m++])
        {
          tgh.Gradient[i] = -dLL_by_dRotn[rot];
          if (do_hessian)
            tgh.Hessian(i,i) = -d2LL_by_dRotn2[rot];
          i++;
        }
      }
      if (refine_parameter_[m++])
      {
        tgh.Gradient[i] = -dLL_by_dDrms;
        if (do_hessian)
          tgh.Hessian(i,i) = -d2LL_by_dDrms2;
        i++;
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    return tgh;
  }

  TargetGradientHessianType
  RefineSPIN::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
    //initialization
    do_gradient = (do_gradient or do_hessian);
    TargetGradientHessianType tgh(nmp,true);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< TargetGradientHessianType > > results;
      std::vector< std::packaged_task< TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineSPIN::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineSPIN::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
             do_gradient,
             do_hessian,
             beg,
             end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
      }
    }

    //subtract restraint terms
    //double f = Target;
    if (do_restraint)
    {
      int m(0),i(0);
      auto& turn = NODE->NEXT;
      for (int rot = 0; rot < 3; rot++)
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_ROT)
          {
            tgh.Target += fn::pow2(turn.ORTHR[rot]/SIGMA_ROT)/2.;
            if (do_gradient)
              tgh.Gradient[i] -= turn.ORTHR[rot]/fn::pow2(SIGMA_ROT);
            if (do_hessian)
              tgh.Hessian(i,i) -= 1.0/fn::pow2(SIGMA_ROT);
          }
          i++;
        }
      }
      if (refine_parameter_[m++])
        i++; //VRMS AJM restrain to input
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }
    //negate target here for minimization
    return tgh;
  }

  af_string
  RefineSPIN::initial_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logTurn("Initial");
  }

  af_string
  RefineSPIN::current_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logTurn("Current");
  }

  af_string
  RefineSPIN::final_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logTurn("Final");
  }

  af_string
  RefineSPIN::finalize_parameters()
  {
    auto& turn = NODE->NEXT;
    auto initial = turn.ENTRY->VRMS_START[0];
    auto modlid = turn.IDENTIFIER;
    phaser_assert(NODE->DRMS_SHIFT.count(modlid));
    phaser_assert(NODE->VRMS_VALUE.count(modlid));
    phaser_assert(NODE->CELL_SCALE.count(modlid));
    auto drms = NODE->DRMS_SHIFT[modlid].VAL;
    NODE->VRMS_VALUE[modlid].VAL = pod::apply_drms_to_initial(drms,initial);
    af_string output;
    output.push_back("Calculated shifted vrms values");
    return output;
  }
} //phasertng
