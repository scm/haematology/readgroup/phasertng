//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/math/likelihood/llgi/function.h>
#include <phasertng/math/likelihood/llgi/helper.h>
#include <future>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/math/table/cos_sin.h>

//#define PHASERTNG_DEBUG_WORKING_NODE
//#define PHASERTNG_FAST_INTERPV

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
namespace dag {

  LikelihoodDataType
  DagDatabase::work_node_likelihood_for_tfz(//of working node
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      pod::FastTfzType* stored,
      dvect3 fract
    )
  {
    PHASER_ASSERT(WORK->HALL.size());
    PHASER_ASSERT(WORK->HALL == REFLECTIONS->SG.HALL);
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    PHASER_ASSERT(WORK->fraction_scattering(ztotalscat)<=1);
    WORK->FSS = 'Y';
    std::tie(WORK->LLG) = combined_tgh_for_tfz(
        REFLECTIONS,
        SELECTED,
        EPSILON,
        NTHREADS,
        USE_STRICTLY_NTHREADS,
        ecalcs_sigmaa_pose,
        stored,
        fract
      );
    WORK->LLG *= -1; //we don' want the -llg, which is only for refinement
    WORK->LLG *= REFLECTIONS->oversampling_correction();
    return std::make_tuple(WORK->LLG);
  }

  LikelihoodDataType
  DagDatabase::combined_tgh_for_tfz(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      pod::FastTfzType* stored,
      dvect3 fract
    )
  {
    double Target = 0;
    PHASER_ASSERT(NTHREADS> 0);
    PHASER_ASSERT(REFLECTIONS->NREFL > 0);
    PHASER_ASSERT(REFLECTIONS->NREFL > NTHREADS);
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      std::tie(Target) = parallel_combined_tgh_for_tfz(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              stored,
              fract
              );
    }
    else
    {
      std::vector<std::future< LikelihoodDataType > > results;
      std::vector<std::packaged_task< LikelihoodDataType(
              const_ReflectionsPtr,
              sv_bool,
              const_EpsilonPtr,
              int,
              int,
              pod::FastPoseType*,
              pod::FastTfzType*,
              dvect3
              )>> packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&DagDatabase::parallel_combined_tgh_for_tfz, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6,
              std::placeholders::_7,
              std::placeholders::_8
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              stored,
              fract
              );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &DagDatabase::parallel_combined_tgh_for_tfz, this,
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              stored,
              fract
              ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      std::tie(Target) = parallel_combined_tgh_for_tfz(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              stored,
              fract
              );
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        double thread_llg = std::get<0>(result);
        Target += thread_llg;
      }
    }
    return std::make_tuple(Target);
  }

  LikelihoodDataType
  DagDatabase::parallel_combined_tgh_for_tfz(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int beg,
      int end,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      pod::FastTfzType* stored,
      dvect3 fract
    )
  {
    double Target = 0;

    // tNCS case: curvatures (particularly vrms) should take account of correlations between
    // tNCS-related copies.  for now, rely on BFGS algorithm to compensate, but it would be better
    // to deal with this explicitly.

    auto pose_terms = WORK->pose_terms_interpolate();
    auto selected_group = WORK->POSE.back().TNCS_GROUP.first;
    int pstart(0);
    for (int p = 0; p < WORK->POSE.size(); p++)
    {
      const auto& pose = WORK->POSE[p];
      if (pose.TNCS_GROUP.first != selected_group)
        pstart++;
    }

    int nmol = WORK->TNCS_ORDER;
    likelihood::llgi::tgh_flags flags;
    flags.target(true).gradient(false).hessian(false);
    likelihood::llgi::function LLGI;
    PHASER_ASSERT(stored->DobsSigaSqr.size() == REFLECTIONS->NREFL);

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const millnx  miller = REFLECTIONS->get_miller(r);
        const double& Dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double& Feff =   REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double  teps =   REFLECTIONS->get_flt(labin::TEPS,r);
        const double& resn =   REFLECTIONS->get_flt(labin::RESN,r);
        const bool&   cent =   REFLECTIONS->get_bln(labin::CENT,r);
        const double& ssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
        const double& eps  =   REFLECTIONS->get_flt(labin::EPS,r);
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);

        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);

        if (ecalcs_sigmaa_pose->ecalcs.size())
        {
          //initialize the main parameters from above
          DobsSigaEcalc = ecalcs_sigmaa_pose->ecalcs[r]*Dobs;
          DobsSigaSqr = fn::pow2(ecalcs_sigmaa_pose->sigmaa[r]);
        }

        phaser_assert(WORK->POSE.size());
        { //pose loop in iEcalc
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            double dphi_arg = SymHKL*fract;
                   dphi_arg += rsym.traMiller[isym];
            cmplex dphi = tbl_cos_sin.get(dphi_arg);
            cmplex thisE = dphi * stored->Ecalc_isym(r,isym);
            DobsSigaEcalc += thisE;
          }
          //g term has been applied to stored value
          DobsSigaSqr += stored->DobsSigaSqr[r]; //all the same because all same pose identifier
        }
        phaser_assert(Dobs > 0);

        //the translation of the gyre is known, but orientation is not resolved
        //half way between pose and gyre
        //translation function rescoring

        //====
        //LLGI calculation
        //====
        double V = teps - DobsSigaSqr;
        double eobs = Feff/resn;
        LLGI.target_gradient_hessian(eobs, DobsSigaEcalc, teps, V, cent, flags);
        Target -= LLGI.LL; // This is already log-likelihood-gain so no wll term required

#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        static std::mutex mutex;
        static int counter(0);
        if (V <= 0)
        {
          const std::lock_guard<std::mutex> lock(mutex);
          //threadsafe
          //lock_guard for cleanup of incomplete mutex.lock() with mutex.unlock()
          counter++;
          std::cout << "\n-------------------- " << counter<< std::endl;
          std::cout << "DagLikelihoodTfz Debugging reflection #" << r << std::endl;
          std::cout << "V=" <<  LLGI.variance() << std::endl;
          std::cout << "LL="  << LLGI.LL << std::endl;
          std::cout << "Miller=(" << miller[0]<<","<<miller[1]<<","<<miller[2]<<")"<< std::endl;
          std::cout << "Eobs="<< eobs << std::endl;
          std::cout << "|Ecalc|=" << std::abs(DobsSigaEcalc) << std::endl;
          std::cout << "DobsSigaSqr=" << DobsSigaSqr<< std::endl;
          std::cout << "Centric=" << cent << std::endl;
          std::cout << "scattering="<< REFLECTIONS->get_ztotalscat() << std::endl;
          std::cout << "rssqr="<< ssqr << std::endl;
          std::cout << "reso="<< (1/std::sqrt(ssqr)) << std::endl;
          std::cout << "tncs-epsfac="<< teps << std::endl;
          if (EPSILON->array_G_Vterm.size())
          std::cout << "tncs-g_vterm="<< EPSILON->array_G_Vterm[r] << std::endl;
          if (EPSILON->array_Gsqr_Vterm.size())
          std::cout << "tncs-gsqr_vterm="<< EPSILON->array_Gsqr_Vterm[r] << std::endl;
          std::cout << "Dobs="<< Dobs << std::endl;
          auto output = WORK->logNode("Debugger");
          for (auto line : output)
            std::cout << line << std::endl;
          PHASER_ASSERT(!LLGI.negative_variance());
          std::cout << "--------------------" << std::endl;
          PHASER_ASSERT(V > 0);
        }//debug
#endif
      } //selected
    } //reflections
    return std::make_tuple(Target);
  }

  pod::FastTfzType
  DagDatabase::fast_terms_for_tfz(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON
    )
  {
    const SpaceGroup* SG = &REFLECTIONS->SG;

    //we will only use the rotational part of the pose_terms here
    auto pose_terms = WORK->pose_terms_interpolate();

    int nmol = WORK->TNCS_ORDER;
    double nsymp = SG->group().order_p();

    int pstart(0);
    for (int p = 0; p < WORK->POSE.size(); p++)
    {
      const auto& pose = WORK->POSE[p];
      if (pose.TNCS_GROUP.first != WORK->POSE.back().TNCS_GROUP.first)
        pstart++;
    }

    pod::FastTfzType stored;
    stored.resize(REFLECTIONS->NREFL,nsymp);

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx  miller = REFLECTIONS->get_miller(r);
        const double& Dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double& ssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
        const double& eps  =   REFLECTIONS->get_flt(labin::EPS,r);
        const double  g_drms = (nmol > 1) ? EPSILON->array_G_DRMS[r] : 1;
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);


        PHASER_ASSERT(WORK->POSE.size());
        {
          double last_thisDobsSigaSqr = 0;
          for (int p = pstart; p < WORK->POSE.size(); p++)
          {
            const auto& pose = WORK->POSE[p];
            const auto& entry = pose.ENTRY;
            double thisDobsSigaSqr = 0;
            if (p == pstart)
            {
              double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
              double bfac = pose.BFAC;
              double fs = REFLECTIONS->fracscat(entry->SCATTERING);
              double drms = WORK->DRMS_SHIFT[pose.IDENTIFIER].VAL;
              thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                  ssqr, SigaSqr, fs, Dobs, drms, bfac, g_drms);
              last_thisDobsSigaSqr = thisDobsSigaSqr;
            }
            else
            {
              thisDobsSigaSqr = last_thisDobsSigaSqr;
            }
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
              cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                  thisDobsSigaSqr,eps,nsymp, //use thisDobsSigaSqr before tncs var correction
                  SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]
                  );
              stored.Ecalc_isym(r,isym) += scalefac*iEcalc;
            }
            if (nmol > 1)
            { //halfR = false
              thisDobsSigaSqr *= EPSILON->array_G_Vterm[r];
            }
            stored.DobsSigaSqr[r] += thisDobsSigaSqr; //one per pose, includes Dobs
          }
        }
      } //selected
    } //reflections
    return stored;
  }

}} //phasertng
