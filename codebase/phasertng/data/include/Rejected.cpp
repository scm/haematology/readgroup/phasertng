//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Selected.h>
#include <phasertng/math/likelihood/isad/sigmaa.h>
#include <phasertng/math/likelihood/pIntensity.h>
#include <phasertng/math/likelihood/pBijvoet.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <future>

namespace phasertng {

//#define PHASERTNG_DEBUG_SELECTED_CLASS
  void
  Selected::flag_outliers_and_select(
      const_ReflectionsPtr reflections,
      const std::set<rejected::code> CRITERIA_,
      sv_double epsnSigmaN,
      likelihood::isad::sigmaa isad_sigmaa
    )
  {
    PHASER_ASSERT(STICKY_HIRES != 0);
    //This function flags all the outliers regardless of the criteria
    //and then selects the reflections based on the criteria at the end
    //This is so that you can see the outliers in the different criteria
    //where ever in the code the function is called
    //even if you are not rejecting them as outliers
    CRITERIA = CRITERIA_;
    PROBABILITIES = ProbType();
    //all function require CENT SSQR and BIN
    //call functions that do not need epsnSigmaN or anomalous terms
    //one function per criteria
#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "functions that do not need epsnSigmaN or anomalous terms" <<std::endl;
#endif
    {{
    bool use_reso(CRITERIA.count(rejected::RESO));
    bool use_sysabs(CRITERIA.count(rejected::SYSABS));
    bool use_missing(CRITERIA.count(rejected::MISSING));
    bool use_nomean(CRITERIA.count(rejected::NOMEAN));
    bool use_noanom(CRITERIA.count(rejected::NOANOM));
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(reflections->NREFL);
      auto reflprobs = flag_fixed_criteria(
          reflections,
          use_reso,
          use_sysabs,
          use_missing,
          use_nomean,
          use_noanom,
          beg,
          end);
      append_probabilities(reflprobs);
    }
    else
    {
      std::vector< std::future< ProbType > > results;
      std::vector< std::packaged_task< ProbType(
              const_ReflectionsPtr,
              bool,
              bool,
              bool,
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = reflections->head_thread(t,NTHREADS);
        int end = reflections->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind( &Selected::flag_fixed_criteria, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6,
              std::placeholders::_7,
              std::placeholders::_8
            ));
          results.push_back(packaged_tasks[t].get_future());
          threads.emplace_back(std::move(packaged_tasks[t]),
              reflections,
              use_reso,
              use_sysabs,
              use_missing,
              use_nomean,
              use_noanom,
              beg,
              end );
        }
        else
        {
          results.push_back(std::async(std::launch::async, &Selected::flag_fixed_criteria, this,
              reflections,
              use_reso,
              use_sysabs,
              use_missing,
              use_nomean,
              use_noanom,
              beg,
              end));
        }
      }

      {
      int beg = reflections->head_thread(nthreads_1,NTHREADS);
      int end = reflections->tail_thread(nthreads_1,NTHREADS);
      auto reflprobs = flag_fixed_criteria(
          reflections,
          use_reso,
          use_sysabs,
          use_missing,
          use_nomean,
          use_noanom,
          beg,
          end);
      append_probabilities(reflprobs);
      }

      for (int t = 0; t < nthreads_1; t++)
      {
        auto reflprobs = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join();
        append_probabilities(reflprobs);
      }
    }
    }}
#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "create sigmaN " <<std::endl;
#endif
    //create the epsnSigmaN array from the reflection list if the values weren't passed
    if (epsnSigmaN.size() == 0)
    {
      if (reflections->has_col(labin::RESN)) //these must be present
      {
        bool hasTEPS = reflections->has_col(labin::TEPS);
        epsnSigmaN.resize(reflections->NREFL);
        for (int r = 0; r < reflections->NREFL; r++)
        {
          double this_teps = hasTEPS ? reflections->get_flt(labin::TEPS,r) : 1;
          double resn  = reflections->get_flt(labin::RESN,r);
          epsnSigmaN[r] = fn::pow2(resn)*this_teps;
        }
      }
      else epsnSigmaN = sv_double(reflections->NREFL,1); //e.g. if not yet normalized
    }
    //call outlier functions dependent on above

#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "functions that need sigmaN " <<std::endl;
#endif
    bool use_wilsonnat(CRITERIA.count(rejected::WILSONNAT));
    bool use_wilsonpos(reflections->ANOMALOUS and CRITERIA.count(rejected::WILSONPOS));
    bool use_loinfonat(CRITERIA.count(rejected::LOINFONAT));
    bool use_loinfopos(reflections->ANOMALOUS and CRITERIA.count(rejected::LOINFOPOS));
    //not selecting on wilson will mean that the feff/dobs calculation is not valid
    //can only use this for anisotropy correction
    if (use_wilsonnat or use_wilsonpos or use_loinfonat or use_loinfopos)
    {{
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(reflections->NREFL);
      auto reflprobs = flag_epsnsigman_criteria(
          reflections,
          epsnSigmaN,
          use_wilsonnat,
          use_wilsonpos,
          use_loinfonat,
          use_loinfopos,
          beg,
          end);
      append_probabilities(reflprobs);
    }
    else
    {
      std::vector< std::future< ProbType > > results;
      std::vector< std::packaged_task< ProbType(
              const_ReflectionsPtr,
              sv_double,
              bool,
              bool,
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = reflections->head_thread(t,NTHREADS);
        int end = reflections->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind( &Selected::flag_epsnsigman_criteria, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6,
              std::placeholders::_7,
              std::placeholders::_8
             ));
          results.push_back(packaged_tasks[t].get_future());
          threads.emplace_back(std::move(packaged_tasks[t]),
              reflections,
              epsnSigmaN,
              use_wilsonnat,
              use_wilsonpos,
              use_loinfonat,
              use_loinfopos,
              beg,
              end); }
        else
        {
          results.push_back(std::async(std::launch::async, &Selected::flag_epsnsigman_criteria, this,
              reflections,
              epsnSigmaN,
              use_wilsonnat,
              use_wilsonpos,
              use_loinfonat,
              use_loinfopos,
              beg,
              end));
        }
      }

      int beg = reflections->head_thread(nthreads_1,NTHREADS);
      int end = reflections->tail_thread(nthreads_1,NTHREADS);
      auto reflprobs = flag_epsnsigman_criteria(
          reflections,
          epsnSigmaN,
          use_wilsonnat,
          use_wilsonpos,
          use_loinfonat,
          use_loinfopos,
          beg,
          end);
      append_probabilities(reflprobs);

      for (int t = 0; t < nthreads_1; t++)
      {
        auto reflprobs = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join();
        append_probabilities(reflprobs);
      }
    }
    }}

#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "functions that need anomalous " <<std::endl;
#endif
    {{
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(reflections->NREFL);
      auto reflprobs = flag_badanom(
              reflections,
              epsnSigmaN,
              isad_sigmaa,
              beg,
              end);
      append_probabilities(reflprobs);
    }
    else
    {
      std::vector< std::future< ProbType > > results;
      std::vector< std::packaged_task< ProbType(
              const_ReflectionsPtr,
              sv_double,
              likelihood::isad::sigmaa,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = reflections->head_thread(t,NTHREADS);
        int end = reflections->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(
              &Selected::flag_badanom, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5
            ));
          results.push_back(packaged_tasks[t].get_future());
          threads.emplace_back(std::move(packaged_tasks[t]),
              reflections,
              epsnSigmaN,
              isad_sigmaa,
              beg,
              end);
        }
        else
        {
          results.push_back(std::async(std::launch::async,&Selected::flag_badanom,this,
              reflections,
              epsnSigmaN,
              isad_sigmaa,
              beg,
              end));
        }
      }

      {
      int beg = reflections->head_thread(nthreads_1,NTHREADS);
      int end = reflections->tail_thread(nthreads_1,NTHREADS);
      auto reflprobs = flag_badanom(
              reflections,
              epsnSigmaN,
              isad_sigmaa,
              beg,
              end);
      append_probabilities(reflprobs);
      }

      for (int t = 0; t < nthreads_1; t++)
      {
        auto reflprobs = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join();
        append_probabilities(reflprobs);
      }
    }
    }}
#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "Final probabilities " << PROBABILITIES.size() << std::endl;
#endif
    //now flag the outliers based on the criteria selected
    SELECTED.resize(reflections->NREFL,true); //note that resize does not init if not actual resize
    for (int r = 0; r < SELECTED.size(); r++)
      SELECTED[r] = true;
    for (auto refl : PROBABILITIES) //set of rejected::code
    {
      for (auto code : refl.second.CODES) //set of rejected::code
      {
        if (CRITERIA.count(code))
        {
          PHASER_ASSERT(refl.first < SELECTED.size());
          SELECTED[refl.first] = false;
        }
      }
    }
#ifdef PHASERTNG_DEBUG_SELECTED_CLASS
std::cout << "Final selection " << nsel() << "/" << reflections->NREFL << std::endl;
#endif
    {{
    resolution_selected = std::numeric_limits<double>::max(); //store for logging
    for (int r = 0; r < SELECTED.size(); r++)
    if (SELECTED[r])
    {
      const millnx  miller =  reflections->get_miller(r);
      const double  resolution = reflections->UC.cctbxUC.d(miller);
      resolution_selected = std::min(resolution_selected,resolution);
    }
    }}
    if (nsel() == 0)
      throw Error(err::INPUT,"No reflections selected");
  }

  ProbType
  Selected::flag_fixed_criteria(
      const_ReflectionsPtr reflections,
      bool use_reso,
      bool use_sysabs,
      bool use_missing,
      bool use_nomean,
      bool use_noanom,
      int beg,
      int end) const
  {
    ProbType reflprobs;
    bool has_I(reflections->has_col(labin::INAT));
    bool has_F(reflections->has_col(labin::FNAT));
    bool has_IPOS(reflections->has_col(labin::IPOS));
    bool has_FPOS(reflections->has_col(labin::FPOS));
    bool has_INEG(reflections->has_col(labin::INEG));
    bool has_FNEG(reflections->has_col(labin::FNEG));
    if (!(use_reso or use_sysabs or use_missing or use_nomean or use_noanom)) return reflprobs;

    //if (use_nomean)
    //{
    //  PHASER_ASSERT(has_I or has_F);
    //  PHASER_ASSERT(!boost::logic::indeterminate(reflections->INTENSITIES));
    //}
    for (int r = beg; r < end; r++)
    {
      const millnx  miller =  reflections->get_miller(r);
      const double  resolution = reflections->UC.cctbxUC.d(miller);
      if (use_reso)
      {
        //tolerance to include all at resolution limit even with instability in resolution calculation
        //(the very highest resolution (hires defining) reflection may be excluded without tol)
        if (resolution < STICKY_HIRES-DEF_PPM or resolution > STICKY_LORES+DEF_PPM)
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::RESO); else
            reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::RESO)); //creates
        }
      }
      if (use_sysabs)
      {
        if (reflections->SG.group().is_sys_absent(miller))
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::SYSABS); else
            reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::SYSABS)); //creates
        }
      }
      if (use_missing and !reflections->MAP)
      {
        boost::logic::tribool NaNnat = boost::logic::indeterminate;
        boost::logic::tribool NaNpos = boost::logic::indeterminate;
        boost::logic::tribool NaNneg = boost::logic::indeterminate;
        if (has_I)    NaNnat = !reflections->get_flt(labin::INAT,r);
        if (boost::logic::indeterminate(NaNnat) and has_F)
                      NaNnat = !reflections->get_flt(labin::FNAT,r); //if no labin
        if (has_IPOS) NaNpos = !reflections->get_flt(labin::IPOS,r);
        if (boost::logic::indeterminate(NaNpos) and has_FPOS)
                      NaNpos = !reflections->get_flt(labin::FPOS,r); //if no labin
        if (has_INEG) NaNneg = !reflections->get_flt(labin::INEG,r);
        if (boost::logic::indeterminate(NaNneg) and has_FNEG)
                      NaNneg = !reflections->get_flt(labin::FNEG,r); //if no labin
        if (boost::logic::indeterminate(NaNnat)) NaNnat = true; //if no labin
        if (boost::logic::indeterminate(NaNpos)) NaNpos = true;
        if (boost::logic::indeterminate(NaNneg)) NaNneg = true;
        if (NaNnat && NaNpos && NaNneg)
        {
          //flag MISSING if there is no anomalous or non-anomalous data for this reflection
          //reflection in mtz is just a free-r-flag?
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::MISSING); else
            reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::MISSING)); //creates
        }
      }
      if (use_nomean and
          reflections->has_friedel(friedel::NAT))
      { //less of a case now, because the mean is generated from the anomalous
        //flagged if conversions fail
        const bool pnat = reflections->get_present(friedel::NAT,r);
        if (!pnat)
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::NOMEAN); else
            reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::NOMEAN)); //creates
        }
      }
      //flag NOANOM if there is no anomalous data for this reflection
      if (use_noanom and
          reflections->has_friedel({friedel::POS,friedel::NEG}))
      {
        const bool both = reflections->get_bln(labin::BOTH,r);
        const bool cent =  reflections->work_cent(miller);
        const bool plus = reflections->get_bln(labin::PLUS,r);
        const bool ppos = reflections->get_present(friedel::POS,r);
        const bool pneg = reflections->get_present(friedel::NEG,r);
        if (!ppos and !pneg)
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::NOANOM); else
            reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::NOANOM)); //creates
        }
        else if (reflections->INTENSITIES and has_IPOS and has_INEG)
        {
          const double sigipos = reflections->get_flt(labin::SIGIPOS,r);
          const double sigineg = reflections->get_flt(labin::SIGINEG,r);
          if ((both and sigipos == 0 and sigineg == 0) or
              (cent and sigipos == 0) or
              (!cent and !both and plus and sigipos == 0) or
              (!cent and !both and !plus and sigineg == 0))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::NOANOM); else
              reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::NOANOM)); //creates
          }
        }
        else if (!reflections->INTENSITIES and has_FPOS and has_FNEG)
        {
          const double fpos = reflections->get_flt(labin::FPOS,r);
          const double fneg = reflections->get_flt(labin::FNEG,r);
          if ((both and fpos == 0 and fneg == 0) or
              (cent and fpos == 0) or
              (!cent and !both and plus and fpos == 0) or
              (!cent and !both and !plus and fneg == 0) or
              (!ppos and !pneg))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::NOANOM); else
              reflprobs.emplace(r,pod::probtype(miller,resolution,rejected::NOANOM)); //creates
          }
        }
      }
    }
    return reflprobs;
  }

  ProbType
  Selected::flag_epsnsigman_criteria(
      const_ReflectionsPtr reflections,
      const sv_double epsnSigmaN,
      bool use_wilsonnat,
      bool use_wilsonpos,
      bool use_loinfonat,
      bool use_loinfopos,
      int beg,
      int end) const
  {
    ProbType reflprobs;
    if (epsnSigmaN.size() != reflections->NREFL) return reflprobs;
    if (!reflections->MAP)
    {
      math::pIntensity pintensity;
      use_wilsonnat = use_wilsonnat and reflections->has_col(labin::INAT);
      use_wilsonpos = use_wilsonpos and reflections->has_col(labin::IPOS) and reflections->has_col(labin::INEG);
      use_loinfonat = use_loinfonat and reflections->has_col(labin::INAT);
      use_loinfopos = use_loinfopos and reflections->has_col(labin::IPOS) and reflections->has_col(labin::INEG);
      if (!(use_wilsonnat or use_wilsonpos or use_loinfonat or use_loinfopos)) return reflprobs;

      //hardwire the default for speed
      double sigesqr_cent,sigesqr_acent;
      std::tie(sigesqr_cent,sigesqr_acent) = sigesqr_values();
      for (int r = beg; r < end; r++)
      {
        const millnx  miller =  reflections->get_miller(r);
        const bool    cent =  reflections->work_cent(miller);
        const double  ssqr = reflections->work_ssqr(miller);
        const double  reso = 1./std::sqrt(ssqr);
        const double  esn   = epsnSigmaN[r];
        // For French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2
        // Safer to reject F-W outliers without allowance for measurement error
        if (esn == 0) continue; //prerequisites have been setup
        if (reflections->MAP) continue; //no wilson for map
        if (use_wilsonnat and reflections->get_present(friedel::NAT,r))
        {
          double eosqr   = reflections->get_flt(labin::INAT,r)/esn;
          double sigesqr = reflections->get_flt(labin::SIGINAT,r)/esn;
          double prob = pintensity.probability(cent,eosqr,sigesqr);
          if (prob <= OUT_PROB)
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::WILSONNAT); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::WILSONNAT)); //creates
            reflprobs[r].set_probability(friedel::NAT,prob);
          }
        }
        if (use_wilsonpos and reflections->get_present(friedel::POS,r))
        {
          double eosqr   = reflections->get_flt(labin::IPOS,r)/esn;
          double sigesqr = reflections->get_flt(labin::SIGIPOS,r)/esn;
          double prob = pintensity.probability(cent,eosqr,sigesqr);
          if (prob <= OUT_PROB)
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::WILSONPOS);
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::WILSONPOS)); //creates
            reflprobs[r].set_probability(friedel::POS,prob);
          }
        }
        if (use_wilsonpos and reflections->get_present(friedel::NEG,r))
        {
          double eosqr   = reflections->get_flt(labin::INEG,r)/esn;
          double sigesqr = reflections->get_flt(labin::SIGINEG,r)/esn;
          double prob = pintensity.probability(cent,eosqr,sigesqr);
          if (prob <= OUT_PROB)
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::WILSONNEG); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::WILSONNEG)); //creates
            reflprobs[r].set_probability(friedel::NEG,prob);
          }
        }
        if (use_loinfonat and reflections->get_present(friedel::NAT,r))
        {
          double sigesqr = reflections->get_flt(labin::SIGINAT,r)/esn;
          if ( (cent and (sigesqr > sigesqr_cent) ) or (!cent and (sigesqr > sigesqr_acent) ))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::LOINFONAT); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFONAT)); //creates
            reflprobs[r].set_sigessqr(sigesqr);
          }
        }
        if (use_loinfopos and reflections->get_present(friedel::POS,r))
        {
          double sigesqr =  reflections->get_flt(labin::SIGIPOS,r)/esn;
          if ( (cent and (sigesqr > sigesqr_cent) ) or (!cent and (sigesqr > sigesqr_acent) ))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::LOINFOPOS); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFOPOS)); //creates
            reflprobs[r].set_sigessqr(sigesqr);
          }
        }
        if (use_loinfopos and reflections->get_present(friedel::NEG,r))
        {
          double sigesqr = reflections->get_flt(labin::SIGINEG,r)/esn;
          if ( (cent and (sigesqr > sigesqr_cent) ) or (!cent and (sigesqr > sigesqr_acent) ))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::LOINFOPOS); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFOPOS)); //creates
            reflprobs[r].set_sigessqr(sigesqr);
          }
        }
      }
    }
    else
    {
      likelihood::mapELLG mapELLG;
      //use_loinfonat = use_loinfonat and reflections->has_col(labin::FEFFNAT);
      use_loinfonat = use_loinfonat and reflections->has_col(labin::FNAT);
      if (!use_loinfonat) return reflprobs;
      double log2 = std::log(2.);
      for (int r = beg; r < end; r++)
      {
        const millnx  miller =  reflections->get_miller(r);
        const double  ssqr = reflections->work_ssqr(miller);
        const double  reso = 1./std::sqrt(ssqr);
        const double  resn = std::sqrt(epsnSigmaN[r]);
        double Emean = reflections->get_flt(labin::FEFFNAT,r)/resn;
        double dobs = reflections->get_flt(labin::DOBSNAT,r);
        double prob = mapELLG.get(Emean,dobs,false)/log2;
        if (prob <= OUT_INFO)
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::LOINFONAT); else
            reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFONAT)); //creates
          reflprobs[r].set_probability(friedel::NAT,prob);
        }
      }
    }
    return reflprobs;
  }

  ProbType
  Selected::flag_badanom(
      const_ReflectionsPtr reflections,
      const sv_double epsnSigmaN,
      likelihood::isad::sigmaa isad_sigmaa,
      int beg, int end) const
  {
    ProbType reflprobs;
    math::pBijvoet pbijvoet;
    //flag BADANOM if Bijvoet mates are inconsistent
    if (epsnSigmaN.size() != reflections->NREFL) return reflprobs;
    if (isad_sigmaa.size() == 0) return reflprobs;
    for (int r = beg; r < end; r++)
    {
      const millnx  miller =  reflections->get_miller(r);
      const double  ssqr = reflections->work_ssqr(miller);
      const double  reso = 1./std::sqrt(ssqr);
      const int  s = reflections->get_int(labin::BIN,r);
      if (reflections->get_bln(labin::BOTH,r) and
          reflections->get_present(friedel::POS,r) and
          reflections->get_present(friedel::NEG,r))
      {
        const double  feffpos = reflections->get_flt(labin::FEFFPOS,r);
        const double  dobspos = reflections->get_flt(labin::DOBSPOS,r);
        const double  feffneg = reflections->get_flt(labin::FEFFNEG,r);
        const double  dobsneg = reflections->get_flt(labin::DOBSNEG,r);
        const double  absrhoff = isad_sigmaa.absrhoFF_bin[s];
        const double  rhopm = isad_sigmaa.rhopm_bin[s];
        double sqrt_epsnSigmaN = std::sqrt(epsnSigmaN[r]); // Sqrt of expected I
        double probs = pbijvoet.pSmallerFeffGivenOther(feffpos,dobspos,feffneg,dobsneg,sqrt_epsnSigmaN,absrhoff,rhopm);
        double probl = pbijvoet.pLargerFeffGivenOther( feffpos,dobspos,feffneg,dobsneg,sqrt_epsnSigmaN,absrhoff,rhopm);
        double probmin = std::min(probs,probl);
        if (probmin <= OUT_PROB)
        {
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::BADANOM)); //creates
        }
      }
    }
    return reflprobs;
  }

}//phasertng
