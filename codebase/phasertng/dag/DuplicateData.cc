//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DuplicateData.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/dag/Node.h>
#include <cctbx/sgtbx/search_symmetry.h>

namespace phasertng {
namespace dag {

  DuplicateData::DuplicateData(dag::Node& node_k,bool is_atom)
  {
    cctbx::sgtbx::space_group_symbols Symbol(node_k.SG->HALL);
    cctbx::sgtbx::space_group SgOps(Symbol.hall(),"A1983");
    cctbx::sgtbx::space_group_type SgInfo(SgOps);
    //add centre of symmetry so that other hand identified as duplicate
    if (is_atom and !SgInfo.is_enantiomorphic()) SgOps.expand_inv(cctbx::sgtbx::tr_vec(0,0,0));
    SgInfo = cctbx::sgtbx::space_group_type(SgOps);
    cctbx::sgtbx::structure_seminvariants semis(SgOps);
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
    SG = SpaceGroup(*node_k.SG);
    Euclid = SpaceGroup("Hall: " + ssym.subgroup().type().hall_symbol());
    // Repeat setup work for reference setting.
    cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
    cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
    cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
    cctbx::sgtbx::search_symmetry reference_ssym(flags,reference_space_group_type,reference_semis);
    cb_op_r_to_reference_setting = SgInfo.cb_op().c().r().as_double();
    cb_op_r_from_reference_setting = SgInfo.cb_op().c_inv().r().as_double();
    isshift = reference_ssym.continuous_shift_flags();
  }

  sv_dmat33
  DuplicateData::Euclid_Rmat(cctbx::uctbx::unit_cell CELL)
  {
    UnitCell uc(CELL);
    sv_dmat33 Euclid_orthRtrfrac;
    Euclid_orthRtrfrac.resize(Euclid.group().order_z());
    dmat33 orthmat = uc.orthogonalization_matrix();
    dmat33 fracmat = uc.fractionalization_matrix();
    for (int isym = 0; isym < Euclid.group().order_z(); isym++)
    {
      dmat33 Rotsym_chk = Euclid.rotsym[isym];
      Euclid_orthRtrfrac[isym] = orthmat*Rotsym_chk.transpose()*fracmat;
    }
    return Euclid_orthRtrfrac;
  }

}}
