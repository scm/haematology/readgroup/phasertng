//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_assert_class__
#define __phasertng_assert_class__
#include <string>
#include <cstdlib>
#include <cstdio>
#include <exception>
#include <cassert>
#include <iso646.h>

#define PHASER_NOT_IMPLEMENTED() ::phasertng::error(__FILE__, __LINE__, \
              "Not implemented.")

//#define DEBUG_USE_PHASER_ASSERT this is a -D option in development build in SCons
#ifndef  DEBUG_USE_PHASER_ASSERT
//phaser_assert is used in the math functions and type functions
//for speed in compilation, set phaser_assert to assert and use NDEBUG compiler flag
#define phaser_assert(int) assert(int)
//for slower code, use PHASER_ASSERT which is derived from std::exception
//#define phaser_assert(bool) PHASER_ASSERT(bool)
//#define PHASER_ASSERT(int) assert(int)
#else
#define phaser_assert(bool) PHASER_ASSERT(bool)
#endif

#define PHASER_ASSERT(bool) \
  if (!(bool)) throw ::phasertng::error(__FILE__, __LINE__,\
    "Consistency check (" # bool ") failed.")

namespace phasertng { //global namespace

class error : public std::exception
{
  protected:
    std::string msg_;

  public:
    explicit
    error(std::string const& msg) throw()
    { msg_ = std::string("Program Error: ") + msg; }

    virtual const char* what() const throw()
    { return msg_.c_str(); }

    virtual ~error() throw() {}

    error(const char* file,
          long line,
          std::string const& msg = "",
          bool internal = true) throw()
    {
      const char *s = "";
      if (internal) s = " internal";
      char buf[64];
      std::string sfile(file);
      std::string::size_type i = sfile.rfind("/"); //unix file separator
      sfile.erase(sfile.begin(),sfile.begin()+i+1);
      std::string::size_type j = sfile.rfind("\\"); //microsoft file separator
      sfile.erase(sfile.begin(),sfile.begin()+j+1);
      std::sprintf(buf, "%ld", line);
      msg_ =   std::string("Program") + s + " error in source file "
                + sfile + " (line " + buf + ")\n";
      if (msg.size()) msg_ += "*** "+ msg + " ***";
      if (internal) msg_ += "\nPlease email this log file and relevant data to cimr-phaser@lists.cam.ac.uk";
    }

};

} //phasertng
#endif
