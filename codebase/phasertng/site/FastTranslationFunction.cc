//(c) 2000-2024 Cambridge University Technical Services LtdS
//All rights reserved
#include <phasertng/site/FastTranslationFunction.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/main/Error.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/dag/Node.h>
#include <phasertng/io/PdbHandler.h>
#include <cctbx/translation_search/fast_nv1995.h>
#include <cctbx/translation_search/fast_terms.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <phasertng/cctbx_project/cctbx/miller/asu.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/sgtbx/rot_mx_info.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <phasertng/cctbx_project/cctbx/maptbx/structure_factors.h>
#include <phasertng/math/RiceWoolfson.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/math/likelihood/llgi/helper.h>
#include <phasertng/math/likelihood/llgp/helper.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/pod/pakdat.h>
#include <phasertng/dag/Entry.h>
#include <future>
#include <phasertng/math/table/sim.h>

namespace phasertng {
extern const table::sim& tbl_sim;
}

namespace phasertng {

  FastTranslationFunction::FastTranslationFunction(
      const_ReflectionsPtr REFLECTIONS,
      sv_bool SELECTED_,
      const_EpsilonPtr EPSILON_
      ) :
        PointsStored(),
        SELECTED(SELECTED_),
        EPSILON(EPSILON_)
  {
    UC = REFLECTIONS->UC;
  }

  void
  FastTranslationFunction::setup(
      const_ReflectionsPtr REFLECTIONS,
      sv_bool SELECTED_,
      const_EpsilonPtr EPSILON_
      )
  {
    translation_function_data.clear();
    SELECTED = SELECTED_;
    EPSILON = EPSILON_;
    UC = REFLECTIONS->UC;
  }

  void
  FastTranslationFunction::allocate_memory(
      const_ReflectionsPtr REFLECTIONS,
      const double sampling,
      const bool haveFpart_
    )
  {
    haveFpart = haveFpart_;
    bool isIsotropicSearchModel(false);
    int max_prime(5);
    bool assert_shannon_sampling(true);
    d_min = std::numeric_limits<double>::max();
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
      if (SELECTED[r])
        d_min = std::min(d_min,UC.cctbxUC.d(REFLECTIONS->get_miller(r)));
    double resolution_factor(sampling/d_min);
    resolution_factor = std::min(0.5,resolution_factor);
    sym_flags = cctbx::translation_search::symmetry_flags(isIsotropicSearchModel,haveFpart);
    gridding = cctbx::maptbx::determine_gridding(
        UC.cctbxUC,
        d_min,
        resolution_factor,
        sym_flags,
        REFLECTIONS->SG.type(),
        af::int3(1,1,1),
        max_prime,
        assert_shannon_sampling
        );
    af::c_grid<3> grid_target(gridding);
    try {
      tags = cctbx::maptbx::grid_tags<long>(grid_target);
    }
    catch (std::exception const& err) {
       throw Error(err::MEMORY,"Can not allocate memory for ftf: reduce resolution or sampling");
    }
  }

  af_string
  FastTranslationFunction::logSetup()
  {
    af_string output;
    output.push_back("use_space_group_symmetry: " + std::string(sym_flags.use_space_group_symmetry()?"true":"false"));
    output.push_back("use_normalizer_k2l: " + std::string(sym_flags.use_normalizer_k2l()?"true":"false"));
    output.push_back("use_seminvariants: " +  std::string(sym_flags.use_seminvariants()?"true":"false"));
    if (haveFpart) output.push_back("Using Fpart");
    else output.push_back("No Fpart");
    af::tiny<bool, 3> isshift;
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::space_group SgOps = SG.group();
    cctbx::sgtbx::structure_seminvariants semis(SgOps);
    cctbx::sgtbx::search_symmetry ssym(flags,SG.type(),semis);
    if (ssym.continuous_shifts_are_principal())
      isshift = ssym.continuous_shift_flags();
    if (!haveFpart) //maps have origin
    {
      if (isshift[0]) output.push_back("Continuous shift in X - translation set to 0");
      if (isshift[1]) output.push_back("Continuous shift in Y - translation set to 0");
      if (isshift[2]) output.push_back("Continuous shift in Z - translation set to 0");
      if (!isshift[0] and !isshift[1] and !isshift[2]) output.push_back("No continuous shifts in space group");
    }
    else output.push_back("No continuous shifts");
    return output;
  }

  void
  FastTranslationFunction::setup_spacegroup(
      dag::Node* NODE)
  {
    //in same point group, REFLECTIONS is const
    SG = *NODE->SG;
    if (!sg_f_max.count(SG))
    {
      sg_f_max[SG] = bestdata();
      sg_z_max[SG] = bestdata();
    }
  }

  void
  FastTranslationFunction::calculate_letf1(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    //in same point group, REFLECTIONS is const
    setup_spacegroup(NODE);
    cctbx::sgtbx::space_group SgOps = SG.group();
    bool FriedelFlag(false);

    af_millnx MillerIndicesSelP1; af_cmplex P1Fcalc;
    std::tie(MillerIndicesSelP1,P1Fcalc) = NODE->NEXT.ngyre() ?
           this->calculate_letf1_p1_gyre_terms(REFLECTIONS,NODE):
           this->calculate_letf1_p1_turn_terms(REFLECTIONS,NODE);

    cctbx::translation_search::fast_terms<double> fast_terms(
        gridding,
        !FriedelFlag,
        MillerIndicesSelP1.const_ref(),
        P1Fcalc.const_ref());
    MillerIndicesSelP1.clear();
    P1Fcalc.clear();
    // Ralf's fast_terms.summation multiplies amplitudes by number of
    // lattice centering operations, but we only consider primitive
    // symmetry operators, so have to compensate.

    af_millnx MillerIndicesSel; double LLconst; af_double mI; af_cmplex Fpart;
    std::tie(MillerIndicesSel,LLconst,mI,Fpart) = NODE->NEXT.ngyre() ?
       this->calculate_sg_gyre_terms(REFLECTIONS,NODE):
       this->calculate_sg_turn_terms(REFLECTIONS,NODE);

    bool squared_flag(false);
    af::versa<double, af::c_grid<3> > tsmap_ =
    fast_terms.summation(
        SgOps,
        MillerIndicesSel.const_ref(),
        mI.const_ref(),
        Fpart.const_ref(),
        squared_flag).fft().accu_real_copy();
    tsmap = tsmap_.deep_copy();
    for (int i = 0; i < tsmap.size(); i++)
      tsmap[i] = tsmap[i] + LLconst;
    //build the tags hwere as there may have been a change of space group!!
    tags.build(SG.type(),sym_flags);
  }

  void
  FastTranslationFunction::calculate_ptf(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    setup_spacegroup(NODE);
    cctbx::sgtbx::space_group SgOps = SG.group();
    //in same point group, REFLECTIONS is const
    cctbx::sgtbx::space_group SgOpsCent("P 1");
    SgOpsCent.expand_conventional_centring_type(SgOps.conventional_centring_type_symbol());

    //already in space group P1
    af_millnx MillerIndicesSelP1; af_cmplex coeffs;
    double LLconst;
    std::tie(MillerIndicesSelP1, LLconst, coeffs) = this->calculate_ptf_p1_terms(REFLECTIONS,NODE);
    scitbx::fftpack::real_to_complex_3d<double> rfft(gridding);
    bool anomalous_flag(false);
    bool conjugate_flag(true);
    af::c_grid_padded<3> map_grid(rfft.n_complex());
    cctbx::maptbx::structure_factors::to_map<double> gradmap(
        SgOpsCent,
        anomalous_flag,
        MillerIndicesSelP1.const_ref(),
        coeffs.const_ref(),
        rfft.n_real(),
        map_grid,
        conjugate_flag);
    af::ref<cmplex, af::c_grid<3> > gradmap_fft_ref(
        gradmap.complex_map().begin(),
        af::c_grid<3>(rfft.n_complex()));
    rfft.backward(gradmap_fft_ref);
    af::versa<double, af::flex_grid<> > real_map(
        gradmap.complex_map().handle(),
        af::flex_grid<>(af::adapt((rfft.m_real())))
        .set_focus(af::adapt(rfft.n_real())));
    cctbx::maptbx::unpad_in_place(real_map);
    //deep copy to keep tsmap in memory
    af::versa<double, af::c_grid<3> > tsmap_(real_map, af::c_grid<3>(rfft.n_real()));
    tsmap = tsmap_.deep_copy();
    for (int i = 0; i < tsmap.size(); i++)
      tsmap[i] = REFLECTIONS->oversampling_correction() * (tsmap[i] + LLconst);
    cctbx::sgtbx::search_symmetry_flags sym_flags(true,1); //not the stored ones
    tags.build(SgOpsCent.type(),sym_flags);
  }

  std::tuple<af_millnx,af_cmplex>
  FastTranslationFunction::calculate_letf1_p1_turn_terms(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    af_millnx MillerIndicesSelP1; af_cmplex P1Fcalc;

    auto turn_terms = NODE->turn_terms_interpolate();
    setup_spacegroup(NODE);

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;

    double nsymp = SG.group().order_p();
    phaser_assert(REFLECTIONS->has_col(labin::EPS));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));

    if (nmol > 1) phaser_assert(EPSILON->array_G_DRMS.size() == REFLECTIONS->NREFL);
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    pod::rsymhkl rsym(SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double eps = REFLECTIONS->get_flt(labin::EPS,r);
        const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        phaser_assert(eps > 0);
        phaser_assert(ssqr > 0);
        phaser_assert(dobs >= 0);
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        double DobsSigaSqr_turn;
        {
          const auto& entry = NODE->NEXT.ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          DobsSigaSqr_turn = thisDobsSigaSqr;
        }
        //symm first, we are going to build p1 array
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
        {
          cmplex iECsym(0,0);
          const millnx& SymHKL = rsym.rotMiller[isym];
          const auto& entry = NODE->NEXT.ENTRY;
          dvect3 RotSymHKL = turn_terms.Q1tr*SymHKL;
          cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
          //original int P1EPS(1);
//FastTranslationFunction::calculate_letf1_p1_terms(NODE)
          cmplex scalefac = likelihood::llgi::EcalcTerm(
              DobsSigaSqr_turn,eps,nsymp,
              RotSymHKL,turn_terms.TRA,0); //works, molecule in p1, just the hkl is expanded
          cmplex thisE = scalefac*iEcalc;
          if (nmol > 1 and tncs_not_modelled)
          { //halfR = true
            const double theta  = SymHKL*REFLECTIONS->TNCS_VECTOR;
            cmplex ptncs_scat = cmplxONE; //imol=0
            for (double imol = 1; imol < nmol; imol++)
              ptncs_scat += std::exp(TWOPII*imol*theta);
            thisE *= ptncs_scat;
            thisE *= (nmol == 2) ? matrix_G(r,isym) : 1.;
          }
          iECsym += thisE;
          MillerIndicesSelP1.push_back(SymHKL);
          P1Fcalc.push_back(iECsym);
        }
        double DobsSigaSqr(0);
        {
          double thisDobsSigaSqr = DobsSigaSqr_turn;
          if (nmol > 1)
          { //halfR = true;
            double  Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
        }
      }
    }
    return std::make_tuple(MillerIndicesSelP1, P1Fcalc );
  }

  std::tuple<af_millnx,af_cmplex>
  FastTranslationFunction::calculate_letf1_p1_gyre_terms(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    af_millnx MillerIndicesSelP1; af_cmplex P1Fcalc;

    auto gyre_terms = NODE->gyre_terms_interpolate();
    setup_spacegroup(NODE);

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;

    double nsymp = SG.group().order_p();
    phaser_assert(REFLECTIONS->has_col(labin::EPS));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));

    if (nmol > 1) phaser_assert(EPSILON->array_G_DRMS.size() == REFLECTIONS->NREFL);
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    pod::rsymhkl rsym(SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double eps = REFLECTIONS->get_flt(labin::EPS,r);
        const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        phaser_assert(eps > 0);
        phaser_assert(ssqr > 0);
        phaser_assert(dobs >= 0);
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        sv_double DobsSigaSqr_gyre(NODE->NEXT.ngyre());
        for (int g = 0; g < NODE->NEXT.ngyre(); g++)
        {
          const auto& entry = NODE->NEXT.GYRE[g].ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          DobsSigaSqr_gyre[g] = thisDobsSigaSqr;
        }
        //symm first, we are going to build p1 array
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
        {
          cmplex iECsym(0,0);
          const millnx& SymHKL = rsym.rotMiller[isym];
          for (int g = 0; g < NODE->NEXT.ngyre(); g++)
          {
            const auto& entry = NODE->NEXT.GYRE[g].ENTRY;
            dvect3 RotSymHKL = gyre_terms[g].Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            if (NODE->NEXT.GYRE[g].SHIFT != dvect3(0,0,0))
            { //this puts the molecule at exactly the refined position, so that 000 will be highest
              //for the tfz== calculation
              dvect3 fract  = NODE->NEXT.GYRE[g].SHIFT;
                     fract += UC.fractionalization_matrix()*NODE->NEXT.GYRE[g].ORTHT;
                 //don't add the offset for ortht
              const double phsft = SymHKL*fract;
              iEcalc *= std::exp(TWOPII*phsft);
            }
            //original int P1EPS(1);
//  FastTranslationFunction::calculate_letf1_p1_terms(NODE)
            cmplex scalefac = likelihood::llgi::EcalcTerm(
                DobsSigaSqr_gyre[g],eps,nsymp,
                RotSymHKL,gyre_terms[g].TRA,0); //works, molecule in p1, just the hkl is expanded
                //SymHKL,gyre_terms[g].TRA,symmetry[isym].second,
            cmplex thisE = scalefac*iEcalc;
            if (nmol > 1 and tncs_not_modelled)
            { //halfR = true
              const double theta  = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1.;
            }
            iECsym += thisE;
          } //gyres
          //gyres have fixed relative phase for each symmetry component
          MillerIndicesSelP1.push_back(SymHKL);
          P1Fcalc.push_back(iECsym);
        }
        double DobsSigaSqr(0);
        for (int g = 0; g < NODE->NEXT.ngyre(); g++)
        {
          double thisDobsSigaSqr = DobsSigaSqr_gyre[g];
          if (nmol > 1)
          { //halfR = true; //for gyres
            double  Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
        }
      }
    }
    return std::make_tuple(MillerIndicesSelP1, P1Fcalc );
  }

  std::tuple<af_millnx,double,af_cmplex>
  FastTranslationFunction::calculate_ptf_p1_terms(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    af_millnx MillerIndicesSel; af_cmplex Fcalc; af_cmplex Fphi;
    af_millnx MillerIndicesSelP1; af_cmplex FcalcP1; af_cmplex FphiP1;

    auto turn_terms = NODE->turn_terms_interpolate();

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;

    double nsymp = SG.group().order_p();
    phaser_assert(REFLECTIONS->has_col(labin::EPS));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::MAPF));
    phaser_assert(REFLECTIONS->has_col(labin::MAPPH));

    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;
    if (nmol == 2) phaser_assert(array_Gsqr_Vterm.size() == REFLECTIONS->NREFL);
    if (nmol > 2) phaser_assert(array_G_Vterm.size() == REFLECTIONS->NREFL);

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    double LLconst(0.);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double phmap = REFLECTIONS->get_flt(labin::MAPPH, r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double eps = REFLECTIONS->get_flt(labin::EPS,r);
        phaser_assert(eps > 0);
        phaser_assert(ssqr > 0);
        phaser_assert(dobs >= 0);
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        double DobsSigaSqr(0);
        double bfac = 0.; // NODE->POSE[0].BFAC; // Come back here if we build up bigger solutions
        double wtdEcSqr(0);
        double DobsSigaSqr_turn;
        {
          const auto& entry = NODE->NEXT.ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double thisDobsSigaSqr = likelihood::llgp::DobsSigaSqr(
              ssqr, SigaSqr, fs, dobs, drms, bfac);
          DobsSigaSqr_turn = thisDobsSigaSqr;
          if (nmol > 1)
          { //halfR = true; //for gyres
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
        }
        //symm first, we are going to build p1 array
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
        {
          cmplex iECsym(0,0);
          const millnx& SymHKL = rsym.rotMiller[isym];
          {
            const auto& entry = NODE->NEXT.ENTRY;
            dvect3 RotSymHKL = turn_terms.Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            // abs(scalefac) is Dobs*SigA for P1 case
            cmplex scalefac = likelihood::llgp::EcalcTerm(
                DobsSigaSqr_turn,eps,nsymp,
                RotSymHKL,turn_terms.TRA,0);
            cmplex thisE = scalefac*iEcalc;
            if (nmol > 1 and tncs_not_modelled)
            {
              const double theta  = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1.;
            }
            iECsym += thisE;
          } //gyres
          //gyres have fixed relative phase for each symmetry component
          wtdEcSqr = std::norm(iECsym);
          FcalcP1.push_back(iECsym);
        }
        MillerIndicesSel.push_back(miller);
        double eeff = feff / resn;
        // Scale Fphi to include 1/variance for LLGP
        // This would be 2/variance but FFT includes both Friedel mates so that
        // imaginary component cancels and real component is doubled
        double var = 1. - DobsSigaSqr;
        double scale = 1. / var;
        Fphi.push_back(std::polar(scale * eeff, scitbx::deg_as_rad(phmap)));
        LLconst -= (DobsSigaSqr * fn::pow2(eeff) + wtdEcSqr) / var + std::log(var);
      }
    }
    phaser_assert(MillerIndicesSel.size() != 0);
    cctbx::sgtbx::space_group SgOps = SG.group();
    bool FriedelFlag(false);
    miller::expand_to_p1_complex<double> hklFphi(SgOps,!FriedelFlag,MillerIndicesSel.const_ref(),Fphi.const_ref());
    FphiP1 = hklFphi.data;
    MillerIndicesSelP1 = hklFphi.indices;
    int nsel = FcalcP1.size();
    PHASER_ASSERT(nsel);
    af_cmplex coeffs(nsel);
    for (int r = 0; r < nsel; r++)
    {
      // For LLGP likelihood, 1/var scale is in FphiP1 and dobs*siga is in FcalcP1
      coeffs[r]= std::conj(FcalcP1[r])*FphiP1[r];
    }
    return std::make_tuple(MillerIndicesSelP1, LLconst, coeffs);
  }

  std::tuple<af_millnx,double,af_double,af_cmplex>
  FastTranslationFunction::calculate_sg_gyre_terms(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    af_millnx MillerIndicesSel; double LLconst(0); af_double mI; af_cmplex Fpart;

    auto pose_terms = NODE->pose_terms_interpolate();
    auto gyre_terms = NODE->gyre_terms_interpolate();

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;

    double nsymp = SG.group().order_p();
    double C1(0);

    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;
    if (nmol > 1) phaser_assert(array_G_DRMS.size() == REFLECTIONS->NREFL);
    if (nmol == 2) phaser_assert(array_Gsqr_Vterm.size() == REFLECTIONS->NREFL);
    if (nmol > 2) phaser_assert(array_G_Vterm.size() == REFLECTIONS->NREFL);

    pod::rsymhkl rsym(SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double eps =  REFLECTIONS->get_flt(labin::EPS,r);
        const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);
        const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);
        for (int p = 0; p < NODE->POSE.size(); p++)
        {
          const auto& entry = NODE->POSE[p].ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double bfac = NODE->POSE[p].BFAC;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
               thisDobsSigaSqr,eps,nsymp,
               SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac*iEcalc;
            DobsSigaEcalc += thisE;
          }
          if (nmol > 1)
          { //halfR = false
            double Vterm = array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes DOBS
        }
        double sum_Esqr_search(0);
        af_cmplex ECsym(rsym.unique,{0.,0.});
        for (int g = 0; g < NODE->NEXT.ngyre(); g++)
        {
          const auto& entry = NODE->NEXT.GYRE[g].ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            cmplex& iECsym = ECsym[isym];
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = gyre_terms[g].Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
                thisDobsSigaSqr,eps,nsymp,
                SymHKL,gyre_terms[g].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac*iEcalc;
            if (nmol > 1)
            {
              const double theta  = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1.;
            }
            iECsym += thisE;
          } //gyres
          if (nmol > 1)
          { //bool halfR = true //for gyres, different from phaser
            //thisDobsSigaSqr *= array_Gsqr_Vterm[r];
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
          //gyres have fixed relative phase for each symmetry component
        }
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          sum_Esqr_search += std::norm(ECsym[isym]);
        cmplex EM_known = DobsSigaEcalc;
        double phasedEsqr = std::norm(EM_known);
        double eImove = phasedEsqr + sum_Esqr_search; // <Imove> knowing molecular transforms
        double sqrt_eImove = std::sqrt(eImove);
        PHASER_ASSERT(eImove >= 0);
        double C(teps - DobsSigaSqr);

        if (C <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        auto V = C;// consistent alias
        static int counter(0);
        if (C <= 0)
        {
          counter++;
          std::cout << "\n---------- " << counter <<std::endl;
          std::cout << "FastTranslationFunction Debugging reflection #" + std::to_string(r) << std::endl;
          std::cout << "V="+ std::to_string(V) << std::endl;
          std::cout << "Miller=" <<miller[0] << " " << miller[1] << " " << miller[2] << std::endl;
          std::cout << "Centric="+ std::string(cent?"Y":"N") << std::endl;
          std::cout << "ssqr="+ std::to_string(ssqr) << std::endl;
          std::cout << "reso="+ std::to_string(1/std::sqrt(ssqr)) << std::endl;
          std::cout << "DobsSigaSqr="+ std::to_string(DobsSigaSqr) << std::endl;
          std::cout << "tncs="+ std::to_string(teps) << std::endl;
          std::cout << "tncs-g_drms="+ std::to_string(g_drms) << std::endl;
          std::cout << "tncs-epsfac="+ std::to_string(teps) << std::endl;
          if (array_G_Vterm.size())
          std::cout << "tncs-g_vterm="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (array_Gsqr_Vterm.size())
          std::cout << "tncs-gsqr_vterm="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (array_G_DRMS.size())
          std::cout << "tncs-g_drms="+ std::to_string(array_G_DRMS[r]) << std::endl;
          std::cout << "----------" << std::endl;
        }//debug
        PHASER_ASSERT(V > 0);
#endif
        //will get here if (V < 0) without checks
        if (C > 0) //last ditch to avoid coredump for 1byu with first bu seq res A (for hetatm) without water inflation (or first residue R)
        {
          double eobs = feff/resn;
          double X = 2.*eobs*sqrt_eImove/C;
          double weight = cent ? 2.0 : 1.0;
          X = X/weight;
          double rh = cent ? std::tanh(X) : tbl_sim.get(X);
          double B1 = (sqrt_eImove > 0) ?
                        (rh*eobs/sqrt_eImove - 1)/(weight*C):
                        (eobs*eobs/C - 1)/(weight*C); // Limit as sqrt_eImove -> 0
          //Compute LL for eImove, leaving out constants that are left out of Rice()
          double eLL = cent ? logRelWoolfson(eobs,sqrt_eImove,C) : logRelRice(eobs,sqrt_eImove,C);
          double C1r = eLL - B1*eImove;
          C1 += C1r;
          double wll = -(std::log(teps)+fn::pow2(eobs)/teps);
                 wll = cent ? wll/2.0 : wll;
          LLconst += (C1r - wll);
          mI.push_back(B1);
          Fpart.push_back(EM_known);
          MillerIndicesSel.push_back(miller);
        }
      }
    }
    return std::make_tuple(MillerIndicesSel, LLconst, mI, Fpart );
  }

  std::tuple<af_millnx,double,af_double,af_cmplex>
  FastTranslationFunction::calculate_sg_turn_terms(
      const_ReflectionsPtr REFLECTIONS,
      dag::Node* NODE)
  {
    af_millnx MillerIndicesSel; double LLconst(0); af_double mI; af_cmplex Fpart;

    auto pose_terms = NODE->pose_terms_interpolate();
    auto turn_terms = NODE->turn_terms_interpolate();

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;

    double nsymp = SG.group().order_p();
    double C1(0);

    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;
    if (nmol > 1) phaser_assert(array_G_DRMS.size() == REFLECTIONS->NREFL);
    if (nmol == 2) phaser_assert(array_Gsqr_Vterm.size() == REFLECTIONS->NREFL);
    if (nmol > 2) phaser_assert(array_G_Vterm.size() == REFLECTIONS->NREFL);

    pod::rsymhkl rsym(SG.NSYMP);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
        const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double eps =  REFLECTIONS->get_flt(labin::EPS,r);
        const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);
        const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);
        for (int p = 0; p < NODE->POSE.size(); p++)
        {
          const auto& entry = NODE->POSE[p].ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double bfac = NODE->POSE[p].BFAC;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
               thisDobsSigaSqr,eps,nsymp,
               SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac*iEcalc;
            DobsSigaEcalc += thisE;
          }
          if (nmol > 1)
          { //halfR = false
            double Vterm = array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes DOBS
        }
        double sum_Esqr_search(0);
        af_cmplex ECsym(rsym.unique,{0.,0.});
        {
          const auto& entry = NODE->NEXT.ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[entry->identify()].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            cmplex& iECsym = ECsym[isym];
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = turn_terms.Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
                thisDobsSigaSqr,eps,nsymp,
                SymHKL,turn_terms.TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac*iEcalc;
            if (nmol > 1)
            {
              const double theta  = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1.;
            }
            iECsym += thisE;
          }
          if (nmol > 1)
          { //bool halfR = true //for gyres, different from phaser
            //thisDobsSigaSqr *= array_Gsqr_Vterm[r];
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
          //gyres have fixed relative phase for each symmetry component
        }
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          sum_Esqr_search += std::norm(ECsym[isym]);
        cmplex EM_known = DobsSigaEcalc;
        double phasedEsqr = std::norm(EM_known);
        double eImove = phasedEsqr + sum_Esqr_search; // <Imove> knowing molecular transforms
        double sqrt_eImove = std::sqrt(eImove);
        PHASER_ASSERT(eImove >= 0);
        double C(teps - DobsSigaSqr);

        if (C <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        auto V = C;// consistent alias
        static int counter(0);
        if (C <= 0)
        {
          counter++;
          std::cout << "\n---------- " << counter <<std::endl;
          std::cout << "FastTranslationFunction Debugging reflection #" + std::to_string(r) << std::endl;
          std::cout << "V="+ std::to_string(V) << std::endl;
          std::cout << "Miller=" <<miller[0] << " " << miller[1] << " " << miller[2] << std::endl;
          std::cout << "Centric="+ std::string(cent?"Y":"N") << std::endl;
          std::cout << "ssqr="+ std::to_string(ssqr) << std::endl;
          std::cout << "reso="+ std::to_string(1/std::sqrt(ssqr)) << std::endl;
          std::cout << "DobsSigaSqr="+ std::to_string(DobsSigaSqr) << std::endl;
          std::cout << "tncs="+ std::to_string(teps) << std::endl;
          std::cout << "tncs-g_drms="+ std::to_string(g_drms) << std::endl;
          std::cout << "tncs-epsfac="+ std::to_string(teps) << std::endl;
          if (array_G_Vterm.size())
          std::cout << "tncs-g_vterm="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (array_Gsqr_Vterm.size())
          std::cout << "tncs-gsqr_vterm="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (array_G_DRMS.size())
          std::cout << "tncs-g_drms="+ std::to_string(array_G_DRMS[r]) << std::endl;
          std::cout << "----------" << std::endl;
        }//debug
        PHASER_ASSERT(V > 0);
#endif
        double eobs = feff/resn;
        double X = 2.*eobs*sqrt_eImove/C;
        double weight = cent ? 2.0 : 1.0;
        X = X/weight;
        double rh = cent ? std::tanh(X) : tbl_sim.get(X);
        double B1 = (sqrt_eImove > 0) ?
                      (rh*eobs/sqrt_eImove - 1)/(weight*C):
                      (eobs*eobs/C - 1)/(weight*C); // Limit as sqrt_eImove -> 0
        //Compute LL for eImove, leaving out constants that are left out of Rice()
        double eLL = cent ? logRelWoolfson(eobs,sqrt_eImove,C) : logRelRice(eobs,sqrt_eImove,C);
        double C1r = eLL - B1*eImove;
        C1 += C1r;
        double wll = -(std::log(teps)+fn::pow2(eobs)/teps);
               wll = cent ? wll/2.0 : wll;
        LLconst += (C1r - wll);
        mI.push_back(B1);
        Fpart.push_back(EM_known);
        MillerIndicesSel.push_back(miller);
      }
    }
    return std::make_tuple(MillerIndicesSel, LLconst, mI, Fpart );
  }

  void
  FastTranslationFunction::statistics()
  {
    cctbx::maptbx::statistics<double> stats(
        af::const_ref<double, af::flex_grid<> >(
        tsmap.begin(),
        tsmap.accessor().as_flex_grid()));
    f_mean = stats.mean();
    f_sigma = stats.sigma();
    PHASER_ASSERT(f_sigma > 0);
    f_top = stats.max();
    z_top = (f_top-f_mean)/f_sigma;
    f_min = stats.min();
    z_min = (f_min-f_mean)/f_sigma;
    //do not set f_max here, because
    // a) it has to be the peak maximum (interpolated maximum)
    // b) it has to pack
  }

  af_double
  FastTranslationFunction::peak_search(
      af_double& peak_heights,af_dvect3& peak_sites)
  {
    af_double histogram;
    af::const_ref<double, af::c_grid_padded<3> > tsmap_const_ref(
        tsmap.begin(),
        af::c_grid_padded<3>(tsmap.accessor()));
    af::ref<long, af::c_grid<3> > tag_array_ref(
        tags.tag_array().begin(),
        af::c_grid<3>(tags.tag_array().accessor()));
    if (!tags.verify(tsmap_const_ref))
    throw Error(err::FATAL,"invalid symmetry flags");
    int peak_search_level = 1;
    std::size_t max_peaks = 0;
    bool interpolate = true;
    cctbx::maptbx::peak_list<> peak_list(
        tsmap_const_ref, tag_array_ref,
        peak_search_level, max_peaks, interpolate);
    peak_sites = peak_list.sites().deep_copy();
    peak_heights = peak_list.heights().deep_copy(); //always positive values
    histogram = peak_heights.deep_copy();
    for (auto& item : histogram)
    {
      //item = 100*(item - f_mean) / (peak_heights.front() - f_mean); //for percent
      item = (item - f_mean)/ f_sigma;
    }
    phaser_assert(peak_sites.size());
    phaser_assert(peak_heights.size());
    return histogram;
  }

  void
  FastTranslationFunction::apply_mean_cutoff(
      af_double& peak_heights,af_dvect3& peak_sites)
  {
    if (f_sigma != 0)
    { //only store the ones that are greater than the mean
      int num(0);
      for (num = 0; num < peak_heights.size(); num++)
      {
        if (peak_heights[num] < f_mean)
          break; //num at last point
      }
      if (num < peak_heights.size()) //should be impossible
      {
        peak_sites.erase( peak_sites.begin()+num, peak_sites.end());
        peak_heights.erase( peak_heights.begin()+num, peak_heights.end());
      }
    }
    phaser_assert(peak_sites.size());
    phaser_assert(peak_heights.size());
  }

  af_string
  FastTranslationFunction::apply_percent_cutoff(
      af_double peak_heights,af_dvect3 peak_sites,double io_percent)
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    //set with top peak from interpolation
    af_string output;
    //sort all the hightfz or top_packs to the front
    //first few if any will be high tfz no packing
    //then first highest packing
    int num(0);
    //hightfz solutions will be
    for (int i = 0; i < peak_sites.size(); i++)
    {
        peak_sites[num] = peak_sites[i];
        peak_heights[num] = peak_heights[i];
        num++;
    }
    //the ones at the top that don't pack are sorted to the end
    //there are many non-packing ones also at the end (less than top_packs condition)
    peak_sites.erase( peak_sites.begin()+num, peak_sites.end());
    peak_heights.erase( peak_heights.begin()+num, peak_heights.end());
    f_max->newmax = false;
    z_max->newmax = false;
    if (!peak_sites.size())
    { //absolutely NONE of the peaks packed, after very long packing job!
      //since we don't normally pack everything '?' this is unlikely
      output.push_back("No peaks");
      return output;
    }
    int top_packing(0);
    if (peak_heights[top_packing] > f_max->f)
    { //only replace z_max if f_max is the highest
      f_max->f = std::max(f_max->f,peak_heights[top_packing]);
      f_max->z = (f_max->f - f_mean) / f_sigma;
      f_max->newmax = true;
    }
    double peak_heights0 = peak_heights[0];
    double z =  (peak_heights0 - f_mean) / f_sigma;
    if (z > z_max->z)
    { //only replace z_max if f_max is the highest z
      z_max->f = peak_heights0;
      z_max->z = z;
      z_max->newmax = true;
    }
    //f_max is the top overall, not just the top this fast translation function
    const double& tol = std::numeric_limits<double>::epsilon();
    for (num = 1; num < peak_heights.size(); num++)
      PHASER_ASSERT(peak_heights[num] <= peak_heights[num-1] + tol); //paranoia check for sort
    double cutoff = (io_percent/100.)*(f_max->f-f_mean)+f_mean;
    if (io_percent and f_sigma != 0)
    {
      int num(0);
      for (num = 0; num < peak_heights.size(); num++)
      {
        if (peak_heights[num] < cutoff)
          break; //num at last point
      }
      if (num < peak_heights.size()) //run-off theoretically possible
      {
        peak_sites.erase( peak_sites.begin()+num, peak_sites.end());
        peak_heights.erase( peak_heights.begin()+num, peak_heights.end());
      }
    }
    output.push_back(snprintftos(
          "%s %.2f/%.2f/%.2f",
          "FSS Top/Mean/Cutoff:",peak_heights0,f_mean,cutoff));
    return output;
  }

  void
  FastTranslationFunction::convert_heights_to_zscores(
      af_double peak_heights)
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    for (int i = 0; i < peak_heights.size(); i++)
    {
      peak_heights[i] = (peak_heights[i]- f_mean) / f_sigma;
    }
    f_top = z_top;
    f_mean = 0;
    f_sigma = 1;
    f_min = z_min;
  }

  af_string
  FastTranslationFunction::apply_percent_cutoff_with_packing(
      af_double peak_heights,af_dvect3 peak_sites,double io_percent,
      af_char peak_packing)
  {
    pod::pakflag pak_flag;
    auto f_max = tf_max();
    auto z_max = tz_max();
    phaser_assert(peak_packing.size());
    //set with top peak from interpolation
    af_string output;
    //sort all the hightfz or top_packs to the front
    //first few if any will be high tfz no packing
    //then first highest packing
    int num(0);
    for (int i = 0; i < peak_sites.size(); i++)
    {
      bool allowed = pak_flag.packs_YUZ(peak_packing[i]);
      if (allowed) //sort this one to the front
      {
        peak_sites[num] = peak_sites[i];
        peak_heights[num] = peak_heights[i];
        peak_packing[num] = peak_packing[i];
        num++;
      }
    }
    //the ones at the top that don't pack are sorted to the end
    //there are many non-packing ones also at the end (less than top_packs condition)
    output.push_back("Purge " + itos(peak_heights.size()-num) + " low-tfz-clash or overlap");
    peak_sites.erase( peak_sites.begin()+num, peak_sites.end());
    peak_heights.erase( peak_heights.begin()+num, peak_heights.end());
    peak_packing.erase( peak_packing.begin()+num, peak_packing.end());
    f_max->newmax = false;
    z_max->newmax = false;
    if (!peak_sites.size())
    { //absolutely NONE of the peaks packed, after very long packing job!
      //since we don't normally pack everything '?' this is unlikely
      output.push_back("No peaks");
      return output;
    }
    int top_packing(0);
    if (peak_packing.size())
    {
      for (int i = 0; i < peak_sites.size(); i++)
      {
        if (pak_flag.packs_YU(peak_packing[i]))
        {
          top_packing = i;
          break;
        }
      }
      output.push_back("Top successful or unknown (not high-tfz) packing is peak #" + itos(top_packing+1));
    }
    if (peak_heights[top_packing] > f_max->f)
    { //only replace z_max if f_max is the highest
      f_max->f = std::max(f_max->f,peak_heights[top_packing]);
      f_max->z = (f_max->f - f_mean) / f_sigma;
      f_max->newmax = true;
      f_max->paks = peak_packing[top_packing];
    }
    double peak_heights0 = peak_heights[0];
    double z =  (peak_heights0 - f_mean) / f_sigma;
    if (z > z_max->z)
    { //only replace z_max if f_max is the highest z
      z_max->f = peak_heights0;
      z_max->z = z;
      z_max->newmax = true;
      z_max->paks = peak_packing[0];
    }
    //f_max is the top overall, not just the top this fast translation function
    const double& tol = std::numeric_limits<double>::epsilon();
    for (num = 1; num < peak_heights.size(); num++)
      PHASER_ASSERT(peak_heights[num] <= peak_heights[num-1] + tol); //paranoia check for sort
    double cutoff = (io_percent/100.)*(f_max->f-f_mean)+f_mean;
    if (io_percent and f_sigma != 0)
    {
      int num(0);
      for (num = 0; num < peak_heights.size(); num++)
      {
        if (peak_heights[num] < cutoff)
          break; //num at last point
      }
      if (num < peak_heights.size()) //run-off theoretically possible
      {
        peak_sites.erase( peak_sites.begin()+num, peak_sites.end());
        peak_heights.erase( peak_heights.begin()+num, peak_heights.end());
        peak_packing.erase( peak_packing.begin()+num, peak_packing.end());
      }
    }
    std::string txt = "FSS Packs(" + pak_flag.packs_YU_str();
    txt += ") This-Top/All-Top-Packs/Mean/Cutoff:";
    output.push_back(snprintftos(
          "%s %.2f/%.2f/%.2f/%.2f",txt.c_str(),peak_heights0,f_max->f,f_mean,cutoff));
    return output;
  }

  af_string
  FastTranslationFunction::log_peaks(int io_print,
      af_double peak_heights,af_dvect3 peak_sites,
      af_char peak_packing)
  {
    af_string output;
    pod::pakflag pak_flag;
    auto f_max = tf_max();
    double tmptop = f_max->f;
    if (peak_packing.size())
    {
      for (int i = 0; i < peak_sites.size(); i++)
      {
        if (pak_flag.packs_YU(peak_packing[i]))
        {
          tmptop = std::max(tmptop,peak_heights[i]);
          break;
        }
      }
      output.push_back("Top successful or unknown (not high-tfz) packing llg = " + dtos(tmptop,1));
    }
    for (int i = 0; i < peak_sites.size() and i < io_print; i++)
    {
      double perc = 100.*(peak_heights[i]-f_mean)/(tmptop-f_mean);
      if (i < peak_packing.size())
        output.push_back(itos(i+1,2) + " " + dtos(perc,5,1) + "% " +
                       dtos(peak_heights[i],7,1) + " T=(" +
                       dvtos(peak_sites[i],7,3) + ") P=" +
                       chtos(peak_packing[i]));
      else
        output.push_back(itos(i+1,2) + " " + dtos(perc,5,1) + "% " +
                       dtos(peak_heights[i],7,1) + " T=(" +
                       dvtos(peak_sites[i],7,3) + ")");
    }
    if (output.size() == 0) output.push_back("No peaks");
    return output;
  }

  std::pair<af::tiny<bool, 3>,bool>
  FastTranslationFunction::continuous_shifts()
  {
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::space_group SgOps = SG.group();
    cctbx::sgtbx::structure_seminvariants semis(SgOps);
    cctbx::sgtbx::search_symmetry ssym(flags,SG.type(),semis);
    af::tiny<bool, 3> isshift;
    if (ssym.continuous_shifts_are_principal())
      isshift = ssym.continuous_shift_flags();
    else isshift.fill(false);
    return { isshift, ssym.continuous_shifts_are_principal() };
  }

  void
  FastTranslationFunction::continuous_shifts_and_store(
      af_double peak_heights,af_dvect3 peak_sites,dvect3 eulermt,dvect3 PT,af_char peak_packing)
  {
    //here we convert the peak_sites wrt mt
    //to translation_function_data Sites which are wrt in
    bool no_cell_translation(false);
    af::tiny<bool, 3> isshift; bool continuous_shifts_are_principal;
    std::tie(isshift,continuous_shifts_are_principal) = continuous_shifts();
    //don't use perturbations, which are applied to molecular transform directly
    translation_function_data.resize(peak_sites.size());
    dvect3 ORI = {0,0,0}; //wrt in
    int itra(0);
    for (int i = 0; i < peak_sites.size(); i++)
    { //TRA wrt in
      dvect3 TRA = dag::fract_wrt_in_from_mt(peak_sites[i],eulermt,PT,UC.cctbxUC);
      if (!haveFpart and continuous_shifts_are_principal)
      {
        // Move so center of mass on polar axis is at zero
        if (isshift[0]) TRA[0] = ORI[0];
        if (isshift[1]) TRA[1] = ORI[1];
        if (isshift[2]) TRA[2] = ORI[2];
        Site other(TRA,peak_heights[i]);
        if (peak_packing.size())
           other.packs = peak_packing[i];
        // this looks for the really boring cases where the values have collapsed to a plane
        //ignores the values (?) as they should be irrelevant by definition
        auto data_end = translation_function_data.begin()+itra;
        auto iter = std::find_if( translation_function_data.begin(), data_end,
             [&other](const Site& x) { return ((x.grid-other.grid)*(x.grid-other.grid) < DEF_PPM);});
        if (iter == data_end)
        {
          translation_function_data[itra++] = other;
        }
      }
      else
      {
        Site other(TRA,peak_heights[i]);
        if (peak_packing.size())
           other.packs = peak_packing[i];
        translation_function_data[itra++] = other;
      }
    }
    translation_function_data.erase(
        translation_function_data.begin()+itra,
        translation_function_data.end());
  }

  std::pair<bool,dvect3>
  FastTranslationFunction::coordinates(dvect3 peak_site,af_dvect3 previous,dvect3 eulermt,dvect3 PT,int& i)
  {
    af::tiny<bool, 3> isshift; bool continuous_shifts_are_principal;
    std::tie(isshift,continuous_shifts_are_principal) = continuous_shifts();
    //don't use perturbations, which are applied to molecular transform directly
    dvect3 ORI = {0,0,0}; //wrt in
    dvect3 TRA = dag::fract_wrt_in_from_mt(peak_site,eulermt,PT,UC.cctbxUC);
    i = -999;
    if (!haveFpart and continuous_shifts_are_principal)
    {
      if (isshift[0]) TRA[0] = ORI[0];
      if (isshift[1]) TRA[1] = ORI[1];
      if (isshift[2]) TRA[2] = ORI[2];
      // this looks for the really boring cases where the values have collapsed to a plane
      //ignores the values (?) as they should be irrelevant by definition
      auto iter = std::find_if( previous.begin(), previous.end(),
           [&TRA](const dvect3& x) { return ((x-TRA)*(x-TRA) < DEF_PPM) ;});
      if (iter == previous.end())
      {
        return { true, TRA };
      }
      else
      {
        i = std::distance(previous.begin(), iter); //not a novel site
        return { false, TRA }; //not a novel site
      }
    }
    return { true, TRA }; //novel
  }

  void
  FastTranslationFunction::apply_partial_sort_and_maximum_stored_llg(const int io_maxstored)
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    if (io_maxstored == 0) return; //cutoff not set
    if (!translation_function_data.size()) return;
    // lambda function for explict sort on parameter
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (translation_function_data.size() < io_maxstored)
    {
      std::sort(
        translation_function_data.begin(),
        translation_function_data.end(), cmp);
    }
    else
    {
      std::partial_sort(
        translation_function_data.begin(),
        translation_function_data.begin()+io_maxstored,
        translation_function_data.end(),
        cmp);
      translation_function_data.erase(
        translation_function_data.begin()+io_maxstored,
        translation_function_data.end());
    }
  }

  double
  FastTranslationFunction::apply_partial_sort_and_percent_fss(const double io_percent)
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    PHASER_ASSERT(f_sigma != 0); //stats not calculated
    //translations not sorted yet
    int num(0); //counter for erase
    double cutoff = (io_percent/100.)*(f_max->f-f_mean)+f_mean;
    for (int tra = 0; tra < translation_function_data.size(); tra++)
    {
      auto& item = translation_function_data[tra];
      //f_max is the max over all translation functions, not just this one
      if (item.value >= cutoff) //sort this one to the front
        translation_function_data[num++] = item;
    }
    translation_function_data.erase(
        translation_function_data.begin()+num,
        translation_function_data.end());
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    std::sort(
        translation_function_data.begin(),
        translation_function_data.end(), cmp);
    return cutoff;
  }

  void
  FastTranslationFunction::clustering_off()
  {
    int i(0);
    for (auto& item : translation_function_data)
      item.clear_topsite(i++);
  }

  void
  FastTranslationFunction::cluster(const double CLUSTER_DIST,int cluster_back)
  {
    int max_cluster_back = translation_function_data.size()-1;
    cluster_back = std::min(cluster_back,max_cluster_back);
    //bool use_symmetry(true);
   // bool use_cell_translation(true); //necessary
    phaser_assert(CLUSTER_DIST>0);
    //must sort before clustering
    //now square it so don't have to keep taking sqrt(distance) for comparison
    DIST2 = fn::pow2(CLUSTER_DIST);
    dvect3 box = UC.GetBox(); //orthogonal
    double cellEdge = 3*CLUSTER_DIST; //orthogonal
    cellBoundMin = dvect3(cellEdge/box[0],cellEdge/box[1],cellEdge/box[2]); //fractional
    cellBoundMax = dvect3(1-cellBoundMin[0],1-cellBoundMin[1],1-cellBoundMin[2]); //fractional
    //assume all sites are NOT unique to start with
    for (auto& item : translation_function_data) //all tra
      item.set_topsite_false();
    const dmat33 orthmat = UC.orthogonalization_matrix();
    double NSYMZ = SG.group().order_z();
    rotsym_tr.resize(NSYMZ); //class
    phaser_assert(SG.rotsym.size() == NSYMZ);
    for (unsigned isym = 0; isym < NSYMZ; isym++)
      rotsym_tr[isym] =  SG.rotsym[isym].transpose();

    for (int tra = 0; tra < translation_function_data.size(); tra++) //all tra
    {
      Site& item = translation_function_data[tra]; //offset ref
      dvect3 orthRef = orthmat*item.grid;
      //Look backwards as you are most likely to find a
      //site to cluster with close by in the list,not
      //at the top of the list each time (list in order)
      int tra_prev = cluster_combine(orthRef,tra,cluster_back);
      if (tra_prev < 0)
      {
        item.set_topsite_true(); // no hits found,this site must be unique
        item.ifTopNumInGroup = 1; //first member of its own cluster
        item.clusterNum = tra; //unique,so the cluster position so far is the position in the FTF
      }
      else
      {
        Site& item_prev = translation_function_data[tra_prev]; //offset ref
        item.clusterNum = item_prev.clusterNum;
        translation_function_data[item.clusterNum].ifTopNumInGroup++;
      }
    }
  }

  int
  FastTranslationFunction::cluster_combine( dvect3 orthRef,int nprev,int cluster_back)
  {
    int tra_prev(-999); //result
    int minimum_nprev_for_parallel(2000); //must be 2000 to make it worthwhile AJM TODO run tests
    int dynamic_nthreads = std::ceil(nprev/static_cast<double>(minimum_nprev_for_parallel));
    //when nprev hits 2001 it goes down to 1000 on each thread, not 2000 and 1
    cluster_back = std::min(nprev,cluster_back);
    dynamic_nthreads = std::min(NTHREADS,dynamic_nthreads);
    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = nprev-1;
      tra_prev = cluster_parallel(orthRef, beg, end);
    }
    else if (nprev < minimum_nprev_for_parallel)// because the granularity is too small
    {
      int beg = 0;
      int end = nprev-1;
      tra_prev = cluster_parallel(orthRef, beg, end);
    }
    else
    {
      std::vector< std::future< int > > results;
      std::vector< std::packaged_task< int(dvect3,int,int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = dynamic_nthreads-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      int grain = (cluster_back/dynamic_nthreads)+1; //granularity of threading, +1 makes the last thread the shortest
      int start = (nprev-cluster_back); //end of thread
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = std::max(0,start+t*grain); //start of thread reflection
        int end = std::min(start+(t+1)*grain,nprev)-1; //end of thread
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&FastTranslationFunction::cluster_parallel, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              orthRef,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &FastTranslationFunction::cluster_parallel, this,
              orthRef,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
       int beg = nthreads_1*grain; //start of thread reflection
       int end = std::min((nthreads_1+1)*grain,nprev)-1; //end of thread
       tra_prev = cluster_parallel(
              orthRef,
              beg,
              end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        if (result >= 0)
          tra_prev = std::min(result,tra_prev); //highest in the list, fowards search
      }
    }
    return tra_prev;
  }

  int
  FastTranslationFunction::cluster_parallel( dvect3 orthRef,int beg, int end)
  {
    const dmat33 orthmat = UC.orthogonalization_matrix();
    for (int tra_prev = beg; tra_prev <= end; tra_prev++)
    {
      Site& item_prev = translation_function_data[tra_prev]; //offset ref
    //Look at the sites not considering symmetry first
    //as you are most likely not going to need symmetry to
    //find a neighbouring site
      for (unsigned isym = 0; isym < rotsym_tr.size(); isym++)
      {
        dvect3 fracPrev = rotsym_tr[isym]*item_prev.grid + SG.trasym[isym];
        dvect3 orthPrev = orthmat*fracPrev;
        double delta = (orthPrev-orthRef)*(orthPrev-orthRef);
        if (delta < DIST2)
        {
          return tra_prev;
        }
        { //only test the ones at the boundaries, this is a big speedup
          dvect3 cellMin(0,0,0);
          dvect3 cellMax(0,0,0);
          dvect3 cellTrans(0,0,0);
          if (fracPrev[0] > cellBoundMax[0]) cellMin[0] = -1;
          if (fracPrev[0] < cellBoundMin[0]) cellMax[0] = 1;
          if (fracPrev[1] > cellBoundMax[1]) cellMin[1] = -1;
          if (fracPrev[1] < cellBoundMin[1]) cellMax[1] = 1;
          if (fracPrev[2] > cellBoundMax[2]) cellMin[2] = -1;
          if (fracPrev[2] < cellBoundMin[2]) cellMax[2] = 1;
          for (cellTrans[0] = cellMin[0]; cellTrans[0] <= cellMax[0]; cellTrans[0]++)
          for (cellTrans[1] = cellMin[1]; cellTrans[1] <= cellMax[1]; cellTrans[1]++)
          for (cellTrans[2] = cellMin[2]; cellTrans[2] <= cellMax[2]; cellTrans[2]++)
          if (cellTrans != dvect3(0,0,0)) //already tested above
          {
            dvect3 fracPrevCell = fracPrev+cellTrans;
            dvect3 orthPrev = orthmat*fracPrevCell;
            delta = (orthPrev-orthRef)*(orthPrev-orthRef);
            if (delta < DIST2)
            {
              return tra_prev;
            }
          } //cells
        }
      } //symm
    }
    return -999;
  }

  af_string
  FastTranslationFunction::logTableHeader(int nprint)
  {
    af_string output;
    int z = translation_function_data.size();
    output.push_back("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    output.push_back("Top " + itos(nprint) + " reported");
    return output;
  }

  af_string
  FastTranslationFunction::logFastTable(
      const int nprint,
      const bool AllSites
      )
  {
    af_string output = logTableHeader(nprint);
    bool clustered = translation_function_data.size() ? translation_function_data[0].clustered() : false;
    if (!translation_function_data.size()) return output;
    output.push_back("--------");
    output.push_back("TFZ     fast search z-score");
    output.push_back("FSS     fast search score");
    output.push_back("FSS%    fast search score percent of current top peak wrt mean");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(translation_function_data.size());
    int q = precision();
    int w = itow(nprint);
    int ww = itow(translation_function_data.size(),5);
    auto f_max = tf_max();
    auto z_max = tz_max();
    output.push_back("Top FSS: " + dtos(f_max->f,q));
    output.push_back(snprintftos(
        "%-*s (%7s %7s %7s) (%7s %7s %7s) %6s %10s %5s %*s %5s",
        w,"#","FracX","FracY","FracZ", "OrthX","OrthY","OrthZ","TFZ","FSS","FSS%",ww,"#Peak","#Grp"));
    const dmat33& orthmat = UC.orthogonalization_matrix();
    int i(1);
    for (auto item : translation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered below
      if (!clustered) topstr = grpstr = "";
      dvect3 fract = item.grid;
      dvect3 orth = orthmat*fract;
      if (AllSites or item.is_topsite())
      {
       // Not possible to print the percent reported to annotation, since overall f_max not known here
        double pscore = 100.*(item.value-f_mean)/(f_max->f-f_mean);
        double zscore = (item.value-f_mean)/f_sigma;
       // PHASER_ASSERT(pscore <= (100. + DEF_PPB)); //top may not be packing top
        output.push_back(snprintftos(
            "%-*i (% 7.3f % 7.3f % 7.3f) (% 7.1f % 7.1f % 7.1f) %6.2f %10.*f %5.1f %*s %5s",
            w,i,
            fract[0],fract[1],fract[2],
            orth[0],orth[1],orth[2],
            zscore,
            q,item.value,
            pscore,
            ww,topstr.c_str(),
            grpstr.c_str()));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
        output.push_back(snprintftos(
          "%*s FRACT=[% 6.3f % 6.3f % 6.3f]",
          w,"",
          item.grid[0],item.grid[1],item.grid[2]));
#endif
        if (i == nprint) break;
        i++;
      }
    }
    if (translation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(translation_function_data.size()-nprint) + " more");
    output.push_back("");
    return output;
  }

  af_string
  FastTranslationFunction::logJogTable(
      const int nprint,
      const bool AllSites,
      const dvect3 ref_fract,
      dvect3 euler,
      dvect3 pt
      )
  {
    af_string output = logTableHeader(nprint);
    if (!translation_function_data.size()) return output;
    dvect3 ref_ortht = UC.orthogonalization_matrix()*ref_fract;
    output.push_back("Pose translation (" + dvtos(ref_fract) + ") (" + dvtos(ref_ortht) + ")");
    output.push_back("--------");
    output.push_back("TFZ     fast search z-score");
    output.push_back("FSS%    fast search score percent of current top peak wrt mean");
    output.push_back("--------");
    phaser_assert(translation_function_data.size());
    int w = itow(nprint);
    output.push_back(snprintftos(
        "%-*s %6s %5s %11s",
        w,"#","TFZ","FSS%","Distance(A)"));
    int i(1);
    for (auto item : translation_function_data)
    {
      double mindist = std::numeric_limits<double>::max();
      dvect3 minvect(0,0,0); //closest site by cell translation
      for (int i = -1; i < 2; i++)
      for (int j = -1; j < 2; j++)
      for (int k = -1; k < 2; k++)
     // int i = 0;
     // int j = 0;
      //int k = 0;
      {
        dvect3 item_fract = item.grid + dvect3(i,j,k);
        //output.push_back("orig " + dvtos(item_fract));
        //item_fract = dag::fract_wrt_mt_from_in(item_fract,euler,pt,UC.cctbxUC);
       // output.push_back("wrt in " + dvtos(item_fract));
        dvect3 orth = UC.orthogonalization_matrix()*item_fract;
        dvect3 vect = orth-ref_ortht;
        double distance = std::sqrt(vect*vect);
        if (distance < mindist)
        {
          mindist = distance;
          minvect = item_fract;
        }
      }
      auto f_max = tf_max();
      auto z_max = tz_max();
      if (AllSites or item.is_topsite())
      {
       // Not possible to print the percent reported to annotation, since overall f_max not known here
        double pscore = 100.*(item.value-f_mean)/(f_max->f-f_mean);
        double zscore = (item.value-f_mean)/f_sigma;
       // PHASER_ASSERT(pscore <= (100. + DEF_PPB));
       // dvect3 minorth = UC.orthogonalization_matrix()*minvect;
        output.push_back(snprintftos(
            "%-*i %6.2f %5.1f %8.2f",
            w,i,
            zscore,
            pscore,
            mindist
            ));
        if (i == nprint) break;
        i++;
      }
    }
    if (translation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(translation_function_data.size()-nprint) + " more");
    output.push_back("");
    return output;
  }

  af_string
  FastTranslationFunction::logSlowTable(
      const int nprint,
      const bool AllSites
      )
  {
    af_string output = logTableHeader(nprint);
    if (!translation_function_data.size()) return output;
    bool clustered = translation_function_data.size() ? translation_function_data[0].clustered() : false;
    output.push_back("--------");
    output.push_back("LLG     log-likelihood gain");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(translation_function_data.size());
    int q = precision();
    int w = itow(nprint);
    int ww = itow(translation_function_data.size(),5);
    output.push_back(snprintftos(
        "%-*s (%7s %7s %7s) (%7s %7s %7s) %10s %*s %8s",
        w,"#","FracX","FracY","FracZ", "OrthX","OrthY","OrthZ","LLG",ww,"#Peak","#Grp"));
    int i(1);
    const dmat33 orthmat = UC.orthogonalization_matrix();
    for (auto item : translation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered selection below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      dvect3 orth = orthmat*item.grid;
      if (AllSites or item.is_topsite())
      {
        output.push_back(snprintftos(
            "%-*i (% 7.3f % 7.3f % 7.3f) (% 7.1f % 7.1f % 7.1f) %10.*f %*s %8s",
            w,i,
            item.grid[0],item.grid[1],item.grid[2],
            orth[0],orth[1],orth[2],
            q,item.value,
            ww,topstr.c_str(),
            grpstr.c_str()));
        if (i == nprint) break;
        i++;
      }
    }
    output.push_back("");
    return output;
  }

  af_string
  FastTranslationFunction::logTable(
      const int nprint,
      const bool AllSites
      )
  {
    af_string output = logTableHeader(nprint);
    if (!translation_function_data.size()) return output;
    bool clustered = translation_function_data.size() ? translation_function_data[0].clustered() : false;
    output.push_back("--------");
    output.push_back("LLG     log-likelihood gain");
    output.push_back("#Peak   unclustered peak number");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    phaser_assert(translation_function_data.size());
    const dmat33& orthmat = UC.orthogonalization_matrix();
    int q = precision();
    int w = itow(nprint);
    int ww = itow(translation_function_data.size(),5);
    output.push_back(snprintftos(
        "%-*s (%6s %6s %6s) (%6s %6s %6s) %10s %*s %8s",
        w,"#","FracX","FracY","FracZ", "OrthX","OrthY","OrthZ","LLG",ww,"#Peak","#Grp"));
    int i(1);
    for (auto item : translation_function_data)
    {
      std::string topstr = item.is_topsite() ? itos(item.clusterNum+1) : "--"; //clustered selection below
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      dvect3 fract = item.grid;
      dvect3 orth = orthmat*fract;
      if (AllSites or item.is_topsite())
      {
        output.push_back(snprintftos(
            "%-*i (% 6.3f % 6.3f % 6.3f) (% 6.1f % 6.1f % 6.1f) %10.*f %*s %8s",
            w,i,
            fract[0],fract[1],fract[2],
            orth[0],orth[1],orth[2],
            q,item.value,
            ww,topstr.c_str(),
            grpstr.c_str()));
        if (i == nprint) break;
        i++;
      }
    }
    output.push_back("");
    return output;
  }

  void
  FastTranslationFunction::WriteMap(FileSystem mapout)
  {
    af::shared<std::string> labels(1);
    labels[0] = "(Cheshire) fast translation function map";
    af::int3 gridding_first(0,0,0);
    af::int3 gridding_last = gridding;

    af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
        tsmap.begin(),
        af::c_grid_padded_periodic<3>(tsmap.accessor()));

    cctbx::sgtbx::space_group SgOpsP1("P1");
    iotbx::ccp4_map::write_ccp4_map_p1_cell(
        mapout.fstr(),
        UC.cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels.const_ref()
      );
  }

  int
  FastTranslationFunction::precision()
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    return  std::max(4. - std::floor(std::log10(std::max(1.,f_max->f))),1.);
  }

  int
  FastTranslationFunction::width()
  {
    auto f_max = tf_max();
    auto z_max = tz_max();
    double llg = std::max(10.,std::abs(f_max->f));
    return  std::log10(llg)+1+1+1+precision(); //width+sign+decimal+afterdecimal
  }

  std::pair<bool,int>
  FastTranslationFunction::top_packs(char flag)
  {
    for (int j = 0; j < translation_function_data.size(); j++)
      if (translation_function_data[j].packs == flag)
        return {true,j};
    return {false,0};
  }

#if 1
  //return the best per spacegroup, new method
  bestdata* FastTranslationFunction::tf_max()
  { return &sg_f_max.at(SG); }
  bestdata* FastTranslationFunction::tz_max()
  { return &sg_z_max.at(SG); }
#else
  //return the best overall, the original method
  bestdata* FastTranslationFunction::tf_max()
  {
    PHASER_ASSERT(sg_f_max.size());
    auto tmp = &sg_f_max.begin()->second;
    for (auto& item : sg_f_max)
      if (item.second.f > tmp->f)
        tmp = &item.second;
    return tmp;
  }
  bestdata* FastTranslationFunction::tz_max()
  {
    PHASER_ASSERT(sg_z_max.size());
    auto tmp = &sg_z_max.begin()->second;
    for (auto& item : sg_z_max)
      if (item.second.z > tmp->z)
        tmp = &item.second;
    return tmp;
  }
#endif

} //phasertng
