#import iotbx.phil
import sys, os, os.path
import libtbx.phil
from libtbx.phil import *
from iotbx.phil import *

#---Converters

class uuid_converters(number_converters_base):

  phil_type = "uuid"
  #we have to make sure that the python can understand the integer
  #is an identifier
  #in python 2 this means isinstance(result, (int,long)
  #I think in python 3 we can just leave it as original cctbx code
  #which uses isinstance(result, int)
  #because integers are of unlimited size

  #below will be OK for python 3
  #def _value_from_words(self, words, path):
     #return int_from_words(words=words, path=path)

  #python 2
  def _value_from_words(self, words, path):
    #return int_from_words(words=words, path=path)
    result = number_from_words(words=words, path=path)
    if (result is None or result is Auto):
      return result
    if sys.version_info[0] == 2:
      if (isinstance(result, (int,long))): return result
    else:
      if (isinstance(result, (int))): return result #no long in python3
    raise RuntimeError(
      'Error interpreting %s=%s as a long long integer %s' % (
        path, str_from_words(words), words[0].where_str()))

  def _value_as_str(self, value):
    return "%d" % value

class uuids_converters(numbers_converters_base):

  phil_type = "uuids"
  #we have to make sure that the python can understand the integer
  #is an identifier

  #python 2
  def _value_from_number(self, number, words, path):
    if sys.version_info[0] == 2:
      if (isinstance(number, (int,long))): return number
    else:
      if (isinstance(number, (int))): return number #no long in python3
    raise RuntimeError(
      'Error interpreting %s=%s as a long long integer %s' % (
        path, str_from_words(words), words[0].where_str()))

  def _value_as_str(self, value):
    return "%d" % value

class mtzcol_converters():

  phil_type = "mtzcol"

  def __init__(self,
      type=None,
      name=None):
    self.type = type
    self.name = name
    chars = set('HJFDQGLKMEPWABYIR')
    assert(any((c in chars) for c in self.type))

  def __str__(self):
    kwds = []
    if (self.type is not None):
      kwds.append("type=\"" + str(self.type)+"\"")
    if (self.name is not None):
      kwds.append("name=\"" + str(self.name)+"\"")
    if (len(kwds) != 0):
      return self.phil_type + "(" + ", ".join(kwds) + ")"
    return self.phil_type

  def from_words(self, words, master):
    return libtbx.phil.str_from_words(words=words)

  def as_words(self, python_object, master):
    if (python_object is None):
      return [tokenizer.word(value="None")]
    return [tokenizer.word(value=python_object, quote_token='"')]

class scatterer_converters(str_converters):

  phil_type = "scatterer"

class scatterers_converters(strings_converters):

  phil_type = "scatterers"

class filesystem_converters(str_converters):

  phil_type = "filesystem"

  def __str__(self): return self.phil_type

  def from_words(self, words, master):
    path = str_from_words(words=words)
    if path not in (None, Auto):
      path = os.path.expanduser(path)
    return path

  def __init__(self,
      ext="txt", #mtz,pdb,fa,dag
      directory=False):
    self.ext = ext
    self.directory = directory

  def __setstate__(self, state):
    # When unpickling ensure directory is set. Entire function is for
    # backwards-compatibility purposes only. 20180308
    self.ext = "txt"
    self.directory = True
    for key, value in state.items():
      setattr(self, key, value)

  def __str__(self):
    kwds = []
    if (self.directory): #not the default
      kwds.append("directory=True")
    if (len(kwds) != 0):
      return self.phil_type + "(" + ", ".join(kwds) + ")"
    return self.phil_type

# def as_words(self, python_object, master):
#   if (python_object is None):
#     return [tokenizer.word(value="None")]
#   if (python_object is Auto) or (type(python_object) == type(Auto)) :
#     return [tokenizer.word(value="Auto")]
#   return [tokenizer.word(value=self._value_as_str(value=python_object))]

#{
class choices_converters(object):
  """
  Converter for a list of predefined choices
  """

  phil_type = "choices"

  def __init__(self, **choices):
    self.choices = choices

  def __str__(self):
    return "%s( %s )" % (
      self.phil_type,
      ", ".join( "%s = %s" % ( k, v ) for ( k, v ) in self.choices.items() ),
      )

  def as_words(self, python_object, master):
    return libtbx.phil.strings_as_words( python_object )

  def from_words(self, words, master):
    if libtbx.phil.is_plain_none( words = words ):
      return []
    results = []
    for ( index, word ) in enumerate( words, start = 1 ):
      value = word.value
      values = word.value.split('+')
      for value in values:
        if value not in self.choices:
          from libtbx.utils import Sorry
          raise Sorry(
                  "Not a possible choice for %s: %s (setting '%s', index %s)%s\n" % (
            master.full_path(),
            value,
            " ".join(w.value for w in words),
            index,
            word.where_str(),
          )
                  + "  Possible choices are:\n"
                  + "    " + "\n    ".join(sorted(self.choices, key=self.choices.get))
          )
        results.append( value )
    return results

def definition_choices(options):
  return "%s( %s )" % (
    choices_converters.phil_type,
    ", ".join( "%s = %s" % ( k, i ) for ( i, k ) in enumerate( options ) ),
    )

def help_choices(options):
  return ", ".join( options )
#}

additional_converters=[
  uuid_converters,
  uuids_converters,
  mtzcol_converters,
  space_group_converters,
  unit_cell_converters,
  atom_selection_converters,
  filesystem_converters,
  scatterer_converters,
  scatterers_converters,
  choices_converters,
  ]

converter_registry = libtbx.phil.extended_converter_registry(
  additional_converters=additional_converters
#   ,base_registry = iotbx.phil.default_converter_registry,
   )
