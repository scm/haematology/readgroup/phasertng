#include <scitbx/math/eigensystem.h>
#include <scitbx/array_family/shared.h>
#include <tntbx/include/tnt_array1d.h>
#include <tntbx/include/tnt_array2d.h>
#include <tntbx/include/jama_eig.h>
#include "tnt_fmat.h"
#include <cassert>

namespace TNT {

// solve symmetric eigenvalue problem (return eigenvalues and eigenvectors)
template <class T>
class SolveUpperSymmetric
{
  TNT::Vector<T> eigen_values_;
  TNT::Fortran_Matrix<T> eigen_vectors_;

  public:
  SolveUpperSymmetric(const TNT::Fortran_Matrix<T>& A)
  {
  //input matrix A replaced with eigenvectors
    TNT::Subscript N = A.num_rows();
    scitbx::af::versa<T, scitbx::af::c_grid<2> >  Aversa(scitbx::af::c_grid<2>(N,N));
    for (int i = 0; i < N; i++)
      for (int j = 0; j < N; j++)
        Aversa(i,j) = A(i+1,j+1);
    scitbx::math::eigensystem::real_symmetric<T> eigens(Aversa.ref());
    eigen_vectors_ = TNT::Fortran_Matrix<T>(N,N,eigens.vectors().begin());
    eigen_values_ = TNT::Vector<T>(N,eigens.values().begin());
  }
  const TNT::Fortran_Matrix<T>& eigen_vectors() const { return eigen_vectors_; }
  const TNT::Vector<T>&  eigen_values() const  { return eigen_values_; }

  TNT::Fortran_Matrix<T> eigen_vectors() { return eigen_vectors_; }
  TNT::Vector<T>  eigen_values()  { return eigen_values_; }
};

template <class T>
class UpperSymmetricPseudoInverse
{
  private:
  TNT::Fortran_Matrix<T> inverse_matrix_;

  public:
  UpperSymmetricPseudoInverse(const TNT::Fortran_Matrix<T>& M,
                             int& filtered,
                             const int min_to_filter=0)
  {
    TNT::Subscript N(M.num_rows());
    SolveUpperSymmetric<T> solve(M);
    const TNT::Fortran_Matrix<T>& eigenvecs = solve.eigen_vectors();
    const TNT::Vector<T>& eigenvals = solve.eigen_values();
  // Set condition number for range of eigenvalues, depending on variable type
    T evCondition(std::min(1.e9,0.01/std::numeric_limits<T>::epsilon()));
    T evMax(eigenvals(1)); //scitbx, largest first
    T evMin = evMax/evCondition;
    T ZERO(0);
    TNT::Fortran_Matrix<T> lambdaInv(N,N,ZERO);
    for (int i = 1; i <= N; i++)
    {
      const T& ev = eigenvals(i);

      if ( ev > evMin )
        lambdaInv(i,i) = 1.0 / ev;

      else
        lambdaInv(i,i) = ZERO;
    }
    if (min_to_filter)
    {
      assert (min_to_filter < N);
      for (int i=N-min_to_filter+1; i <= N; i++)
        lambdaInv(i,i) = ZERO;
    }
    inverse_matrix_ = eigenvecs * lambdaInv * TNT::transpose(eigenvecs);
    //count number filtered
    filtered = 0;
    for (int i = 1; i <= N; i++)
      if (eigenvals(i) <= evMin || i > N-min_to_filter) filtered++;
//#define __DEBUG_PSEUDOINVERSE__
#ifdef __DEBUG_PSEUDOINVERSE__
    // Check that pseudoinverse obeys Moore-Penrose conditions
    // (Golub & van Loan, p. 257)
    TNT::Fortran_Matrix<T> testmat = M * inverse_matrix_ * M;
    T maxdev(0),maxelement(0);
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
      {
        maxelement = std::max(maxelement,std::abs(M(i,j)));
        maxdev = std::max(maxdev,std::abs(M(i,j)-testmat(i,j)));
      }
    std::cout << "Moore-Penrose condition 1, maximum relative error: " << maxdev/maxelement << "\n";
    testmat = inverse_matrix_ * M * inverse_matrix_;
    maxdev = 0.; maxelement = 0;
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
      {
        maxelement = std::max(maxelement,std::abs(inverse_matrix_(i,j)));
        maxdev = std::max(maxdev,std::abs(inverse_matrix_(i,j)-testmat(i,j)));
      }
    std::cout << "Moore-Penrose condition 2, maximum relative error: " << maxdev/maxelement << "\n";
    testmat = M * inverse_matrix_;
    maxdev = 0.; maxelement = 0;
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
      {
        maxelement = std::max(maxelement,std::abs(testmat(i,j)));
        maxdev = std::max(maxdev,std::abs(testmat(i,j)-testmat(j,i)));
      }
    std::cout << "Moore-Penrose condition 3, maximum relative error: " << maxdev/maxelement << "\n";
    testmat = inverse_matrix_ * M;
    maxdev = 0.; maxelement = 0;
    for (int i = 1; i <= N; i++)
      for (int j = 1; j <= N; j++)
      {
        maxelement = std::max(maxelement,std::abs(testmat(i,j)));
        maxdev = std::max(maxdev,std::abs(testmat(i,j)-testmat(j,i)));
      }
    std::cout << "Moore-Penrose condition 4, maximum relative error: " << maxdev/maxelement << "\n";
#endif
  }

  const TNT::Fortran_Matrix<T>& inverse_matrix() const { return inverse_matrix_; }
  TNT::Fortran_Matrix<T> inverse_matrix() { return inverse_matrix_; }
};

}
