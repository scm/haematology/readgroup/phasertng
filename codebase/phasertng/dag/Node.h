//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Node_class__
#define __phasertng_dag_Node_class__
#include <phasertng/dag/Pose.h>
#include <phasertng/dag/Gyre.h>
#include <phasertng/dag/Next.h>
#include <phasertng/dag/Seek.h>
#include <phasertng/dag/Hold.h>
#include <phasertng/dag/Full.h>
#include <phasertng/dag/Part.h>
#include <phasertng/dag/Term.h>
#include <phasertng/dag/Edge.h>
#include <phasertng/pod/pakdat.h>
#include <phasertng/math/likelihood/isad/sigmaa.h>
#include <phasertng/main/Identifier.h>
#include <cctbx/uctbx.h>
#include <unordered_map>
#include <map>
#include <set>
#include <string>
#include <phasertng/math/Matrix.h>
#include <scitbx/array_family/shared.h>

typedef scitbx::af::shared<std::string> af_string;
typedef std::set<phasertng::Identifier> setmodlid_t;

namespace phasertng {

  class SpaceGroup;

namespace dag {

//#define PHASERTNG_DEBUG_EULER_FRACT

  class node_t {
    public:
    node_t() {}
    node_t(Identifier a,double b):ID(a),VAL(b) {}
    Identifier ID;
    double     VAL;
  };

typedef std::map<Identifier,node_t> mapnode_t;

  class Node
  {
    public: //members
      Edge        TRACKER; //this is not unparsed to node keyword, instead it is unparsed in DagDatabase

    public:
      //internal for c++ dagdatabase
      SpaceGroup* SG = nullptr;

    public: //members
      double      ZSCORE = 0;
      Identifier  REFLID;
      std::string ANNOTATION = "";
      std::map<std::string,int> COMPOSITION;
      int         TNCS_ORDER = 1;
      dvect3      TNCS_VECTOR = dvect3(0,0,0);
      bool        TNCS_MODELLED = false; //this is boolean flag for below, in sync but not unparsed
      char        TNCS_PRESENT_IN_MODEL = '?'; //this is unparsed
      std::string HALL = "Hall:  P 1 (x,y,z)"; //default SpaceGroup
      //HM
      cctbx::uctbx::unit_cell CELL = cctbx::uctbx::unit_cell(af::double6(1,1,1,90,90,90));
      double      CELLZ = 1;
      double      CLASH_WORST = 0;
      char        TNCS_INDICATED = '?'; //flag for not set
      char        TWINNED = '?';
      char        PACKS = pod::pakflag().unset(); // '?'
      char        SPECIAL_POSITION = '?';
      char        SPANS = '?'; //Y N ? and U if map
      char        FSS = '?';
      std::string TWINOP = ""; //whitespace separated operators, parsed as list
      double      LLG = 0;
      double      MAPLLG = 0;
      double      PERCENT = 0;
      double      RFACTOR = 0;
      double      INFORMATION_CONTENT = 0;
      double      EXPECTED_INFORMATION_CONTENT = 0;
      double      SAD_INFORMATION_CONTENT = 0;
      double      SAD_EXPECTED_LLG = 0;
      double      RELLG = 0;
      double      SIGNAL_RESOLUTION = 0;
      double      SIGNAL_RESOLUTION_PHASED = 0;
      int         SEEK_COMPONENTS = 0;
      double      SEEK_RESOLUTION = 0;
      double      SEEK_RESOLUTION_PHASED = 0;
      double      SEEK_COMPONENTS_ELLG = 0;
      bool        SEEK_COMPLETE = false;
      double      DATA_SCALE = 1;
      sv_uuid     PARENTS; //does not have tag, so not an Identifier
      //anything added here must also add MANUALLY
      // parse in DagDatabase
      // and unparse in Node
      // Also in master_node_file.py
      std::vector<dag::Hold>       HOLD = std::vector<dag::Hold>(0);
      std::vector<dag::Seek>       SEEK = std::vector<dag::Seek>(0);
      dag::Next                    NEXT;
      std::vector<dag::Pose>       POSE = std::vector<dag::Pose>(0);
      dag::Full                    FULL;
      dag::Part                    PART;
      likelihood::isad::sigmaa     SADSIGMAA;
      mapnode_t                    DRMS_SHIFT; //can access either by map on id or iterate on second
      mapnode_t                    VRMS_VALUE; //can access either by map on id or iterate on second
      mapnode_t                    CELL_SCALE; //can access either by map on id or iterate on second

    public:
      Node() { SG = nullptr; }
      ~Node() noexcept { SG = nullptr; }

    public:
      void set_star_from_pose();
      std::string separator() const { return "======"; }

   public:
      std::set<Identifier>  used_modlid(bool=true) const;
      std::set<Identifier>  used_modlid_pose() const;
      std::set<Identifier>  used_modlid_curl() const;
      std::set<Identifier>  used_modlid_gyre() const;
      std::set<Identifier>  used_modlid_turn() const;
      std::set<Identifier>  used_modlid_seek() const;
      std::set<Identifier>  used_modlid_full() const;

    public:
      //helper functions for likelihood code
      std::vector<dag::Term> pose_terms_interpolate(dvect3=dvect3(0,0,0)) const;
      std::vector<dag::Term> gyre_terms_interpolate() const;
      std::vector<dag::Term> gyre_curl_terms_interpolate() const;
      dag::Term curl_terms_interpolate() const;
      dag::Term turn_terms_interpolate() const;

      void convert_curl_to_pose(const dvect3);
      void convert_turn_to_curl(const dvect3=dvect3(0,0,0),const dvect3=dvect3(0,0,0),double=-999); //flag for don't change
      void convert_turn_to_pose(const dvect3,const dvect3);
      void convert_turn_to_gyre(setmodlid_t);
      void initialize_turn(const dvect3,dag::Entry*);
      void clear_last_tncs_group_pose();
      double scattering();
      double scattering_seek();
      double fraction_scattering(double);
      double fraction_scattering_seek(double);
      double next_seek();
      void cleanUp();
      void cleanup_drms_shift_cell_scale();
      bool turn_in_poses() const;
      bool turn_is_novel() const;
      std::string shift_str(std::string);
      void squash_orthogonal_perturbations();

    private:
      void add_header(af_string,std::string) const;
      void add_annotation(af_string) const;
      void add_gyre(af_string) const;
      void add_pose(af_string) const;
      void add_curl(af_string) const;
      void add_turn(af_string) const;
      void add_seek(af_string) const;
      void add_entries(af_string,std::set<Identifier>,bool) const;
    public:
      std::string str() const;
      std::string pose_info() const;
      std::string sg_info() const;
      std::string tncs_info() const;
      af_string logNode(std::string="") const;
      af_string logAnnotation(std::string="",bool=true) const;
      af_string logPose(std::string="",bool=true,bool=false) const;
      af_string logTurn(std::string="") const;
      af_string logGyre(std::string="") const;
      af_string logSeek(std::string="") const;
      af_string logClash() const;
      af_string logPairwiseClash(size_t) const;
      af_string logSigmaa(std::string="") const;

    public: //flags for sorting,purging,reordering etc
      int  NUM = 0; //RF number, TF number, original sort order number
      int  EQUIV = 0;
      bool last_pose = false; //for the pose case of precalculated+last
      double generic_float = 0;
      double generic_float2 = 0;
      int  generic_int = 0;
      int  generic_int2 = 0;
      bool generic_flag = true;
      bool generic_bool = true;
      std::string generic_str = "";
      scitbx::af::double4 seek_cumulative_ellg = scitbx::af::double4(0,0,0,0);
      math::Matrix<int> clash_matrix;

    void clear_additional()
    {
      NUM = EQUIV = generic_float = generic_int = generic_int2 = 0;
      generic_flag = generic_bool = true;
      generic_str = "";
      seek_cumulative_ellg = scitbx::af::double4(0,0,0,0);
      clash_matrix = math::Matrix<int>();
    }

    friend std::ostream& operator<<(std::ostream &os, const dag::Node &obj);

    bool is_default() const
    {
      if (ANNOTATION.size()) return false;
      if (REFLID.is_set_valid()) return false;
      if (COMPOSITION.size()) return false;
      if (HOLD.size()) return false;
      if (POSE.size()) return false;
      if (SEEK.size()) return false;
      if (NEXT.PRESENT) return false;
      if (FULL.PRESENT) return false;
      if (PART.PRESENT) return false;
      return true;
    }

    public: //python
      void set_cell(af::double6 uc);
      void hold_clear();
      void pose_clear();
      void reset_parameters(double);
      bool is_all_full() const;
      bool initKnownMR(double,double,double); //for fixing the B-factors as in phaser
      double search_resolution(double) const;
      std::string tncs_vector() const;
  };

  inline std::ostream& operator<<(std::ostream &os, const dag::Node& obj)
  { auto tmp = obj.logNode(); for (auto t : tmp) os << t << std::endl; ; return os; }

}} //phasertng
#endif
