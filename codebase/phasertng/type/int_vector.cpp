//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/int_vector.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  int_vector::int_vector(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const ivect3 dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void int_vector::set_value(ivect3 other)
  {
    user = true;
    uvalue = other;
  }

  void int_vector::set_value(int a, int b, int c)
  {
    user = true;
    uvalue = ivect3(a,b,c);
  }

  void int_vector::set_value(int i, int a)
  {
    user = true;
    uvalue[i] = a;
  }

  void int_vector::set_value(const sv_string& vcards,const ivect3& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  int_vector::is_default() const
  { return uvalue == dephalt; }

  void
  int_vector::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  int_vector::to_python()
  { return unparse_parameter(false); }

  void
  int_vector::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    for (int i = 0; i < 3; i++)
      uvalue[i] = std::stoi(txt[i]);
  }

  ivect3
  int_vector::value_or_default() const
  { return user ? uvalue : dephalt; }

  int
  int_vector::value_or_default(int i) const
  {
    if (i>2 or i<0) return 0;
    return user ? uvalue[i] : dephalt[i];
  }

  ivect3
  int_vector::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  int_vector::unparse_parameter(bool phil) const
  {
    if (value_or_default() == ivect3(DEF_INT_FLAG,DEF_INT_FLAG,DEF_INT_FLAG))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 3; i++)
      ss << value_or_default()[i] << " ";
    return ss.str();
  }

  std::string
  int_vector::show_default(bool phil) const
  {
    if (dephalt == ivect3(DEF_INT_FLAG,DEF_INT_FLAG,DEF_INT_FLAG))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 3; i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
