//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/LatticeTranslocation.h>
#include <phasertng/main/Error.h>

namespace phasertng {

  void
  LatticeTranslocation::parse(std::vector<type::flt_numbers>& iltds)
  {
    DISORDER.clear(); //will be size=1 for no ltd ie. [0 0 0 1]
    HAS_LTD = false;
    //convert input type to standard vector type
    std::vector<std::vector<double>> ltds;
    for (int i = 0; i < iltds.size(); i++)
    {
      auto vecocc = iltds.at(i).value_or_default();
      if (vecocc.size() == 4) //default None is length 0
        ltds.push_back(vecocc);
    }
    if (ltds.size() > 0)
    {
      //see if the  0 0 0 is in the list
      bool zerotld(false);
      double zeroocc(0); //may not be 1
      double sumocc = 0;
      for (auto nums : ltds)
      {
        if (nums[0]==0 and nums[1]==0 and nums[2]==0)
        { //000 needs to be the highest occ
          zerotld = true;
          zeroocc = nums[3];
        }
        else
        {
          HAS_LTD = true;
        }
        std::pair<dvect3,double> disorder = {{nums[0],nums[1],nums[2]},nums[3]};
        DISORDER.push_back(disorder);
        sumocc += nums[3];
      }
      if (sumocc != 1) //even if not ltd, could be ltd with only eg  0 0 0 0.5
        throw Error(err::INPUT,"Lattice translocation disorder occupancies do not sum to 1");
      if (HAS_LTD)
      {
        if (!zerotld)
          throw Error(err::INPUT,"Lattice translocation disorder without occupancy for zero translation");
        for (int i = 0; i < DISORDER.size(); i++)
          if (zeroocc < DISORDER[i].second)
            throw Error(err::INPUT,"Lattice translocation disorder occupancy higher than zero translation");
        if (DISORDER.size())
        { //reverse sort on the occupancy, high to low
          typedef std::pair<dvect3,double> vecocc;
          auto cmp = [](const vecocc& a, const vecocc& b) { return a.second > b.second; };
          std::sort(DISORDER.begin(), DISORDER.end(), cmp);
        }
      }
      else DISORDER.clear();
    }
  }

} //phasertng
