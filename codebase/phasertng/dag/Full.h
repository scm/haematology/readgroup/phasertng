//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Full_class__
#define __phasertng_dag_Full_class__
#include <phasertng/main/Identifier.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Full
  {//the monostructures of the poses merged into one set of coordinates
    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      bool        PRESENT = false;
      double      FS = 0;

    public: //internal variable only, not parsed/unparsed
      double      DRMS_SHIFT = 0;
  };

}} //phasertng
#endif
