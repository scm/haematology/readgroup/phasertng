#include <phasertng/main/Assert.h>
#include <phasertng/math/slope_and_intercept.h>
#include <scitbx/array_family/misc_functions.h>

using namespace scitbx;

namespace phasertng {

  double
  correlation_coefficient(versa_grid_double xdata,versa_grid_double ydata)
  {
    double sumx(0),sumy(0),sumx2(0),sumxy(0),sumy2(0);
    int ndata = xdata.size();
    if (ndata == 1) return 0;
    phaser_assert(ndata);
    phaser_assert(ydata.size() == ndata);
    for (int i = 0; i < ndata; i++)
    {
      sumx += xdata[i];
      sumy += ydata[i];
      sumx2 += fn::pow2(xdata[i]);
      sumy2 += fn::pow2(ydata[i]);
      sumxy += xdata[i]*ydata[i];
    }
    double denom = (ndata*sumx2-fn::pow2(sumx))*
                   (ndata*sumy2-fn::pow2(sumy));
    phaser_assert(denom > 0);
    double cc = (ndata*sumxy-(sumx*sumy))/std::sqrt(denom);
    return cc;
  }

  std::pair<double,double>
  slope_and_intercept(af_double xdata,af_double ydata,af_double ysigma)
  {
    double sumw(0),sumwx(0),sumwy(0),sumwx2(0),sumwxy(0);
    int ndata = xdata.size();
    phaser_assert(ndata);
    phaser_assert(ydata.size() == ndata);
    phaser_assert(!ysigma.size() or (ysigma.size() == ndata));
    for (int i = 0; i < ndata; i++)
    {
      double weight = (ysigma.size()) ? 1./fn::pow2(ysigma[i]) : 1.;
      sumw += weight;
      sumwx += weight*xdata[i];
      sumwx2 += weight*fn::pow2(xdata[i]);
      sumwy += weight*ydata[i];
      sumwxy += weight*xdata[i]*ydata[i];
    }
    if (ndata == 1)
      return {0,ydata[0]};
    double slope =  (sumw*sumwxy-(sumwx*sumwy))/(sumw*sumwx2-fn::pow2(sumwx));
    double intercept = (sumwy - slope*sumwx)/sumw;
    return { slope, intercept };
  }

  std::pair<double,double>
  wilson_slope_and_intercept(af_double DATA,af_double SSQR,af_double WEIGHT)
  {
    // Version specialised to fit Wilson plot with lower weight at low resolution
    double sumw(0),sumwx(0),sumwy(0),sumwx2(0),sumwxy(0);
    int countr(0);
    int NUMBINS = DATA.size();
    phaser_assert(SSQR.size() == NUMBINS);
    phaser_assert(!WEIGHT.size() or (WEIGHT.size() == NUMBINS));
    for (int s = 0; s < NUMBINS; s++)
    {
      if (DATA[s] > 0.) // Skip empty bins or ones with negative mean intensity
      {
        double log_bin = std::log(DATA[s]);
        double weight = (s < WEIGHT.size()) ? WEIGHT[s] : 1.;
        // Down-weight data below 5A
        if (SSQR[s] < 0.04)
          weight = weight * fn::pow2(SSQR[s]/0.04);
        if (SSQR[s] < 0.009)
          weight = 0; // Ignore data below 10.5409
        else
          countr++;
        sumw += weight;
        sumwx += weight*SSQR[s];
        sumwx2 += weight*fn::pow2(SSQR[s]);
        sumwy += weight*log_bin;
        sumwxy += weight*SSQR[s]*log_bin;
      }
    }
    if (countr <= 2)
      return {0,0};
    double slope =  (sumw*sumwxy-(sumwx*sumwy))/(sumw*sumwx2-fn::pow2(sumwx));
    double intercept = (sumwy - slope*sumwx)/sumw;
    return { slope, intercept };
  }

} //phaser
