//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dtmin_reparams_class__
#define __phasertng_dtmin_reparams_class__
#include <cmath>

namespace phasertng {
namespace dtmin {

class Reparams
{
  public: //members
    bool   reparamed = false;
    double offset = 0;

  public: //constructor
    Reparams() {}
    Reparams(const bool& reparamed_,const double& offset_) :
       reparamed(reparamed_),offset(offset_) {}

  public: //functions
    void off()        { reparamed = false; offset = 0; }
    void on(double f) { reparamed = true; offset = f; }

    double param(const double& par) const
    { return reparamed ? std::log(par + offset) : par; }

    double unparam(const double& par) const
    { return reparamed ? std::exp(par) - offset : par; }

    double gradient(const double& par,const double& grad) const
    { return reparamed ? grad*(par + offset) : par; }
};

}}
#endif
