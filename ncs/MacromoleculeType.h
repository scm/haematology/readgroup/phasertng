#ifndef PHASER_NCS_MACROMOLECULETYPE_H
#define PHASER_NCS_MACROMOLECULETYPE_H

#include <iotbx/pdb/hierarchy.h>

#include <vector>
#include <map>
#include <set>
#include <boost/assign/list_of.hpp>

namespace phaser {  namespace ncs {

class MacromoleculeType
{
    private:
        std::vector< char > one_letter_codes_;
        std::vector< std::string > three_letter_codes_;
        char unknown_one_letter_;
        std::string unknown_three_letter_;
        size_t min_chain_length_;
        std::set< iotbx::pdb::str4 > backbone_atom_names_;
        std::string type_name_;

    public:
        MacromoleculeType(
            const std::vector< char >& one_letter_codes,
            const std::vector< std::string >& three_letter_codes,
            char unknown_one_letter,
            const std::string& unknown_three_letter,
            size_t min_chain_length,
            const std::set< iotbx::pdb::str4 >& backbone_atom_names,
            const std::string& type_name
            );

        //constructor defaults to protein, for python calls
        MacromoleculeType() :
           unknown_one_letter_( 'X' ),
           unknown_three_letter_( "UNK" ),
           min_chain_length_(5),
           backbone_atom_names_( boost::assign::list_of
             ( " CA " )( " N  " )( " C  " )( " O  " ).
             convert_to_container<std::set<iotbx::pdb::str4> >() ),
           type_name_ ( "protein" )
           {
             three_letter_codes_ = boost::assign::list_of
                 ( "ALA" )( "CYS" )( "ASP" )( "GLU" )( "PHE" )
                 ( "GLY" )( "HIS" )( "ILE" )( "LYS" )( "LEU" )( "MSE" )( "MET" )
                 ( "ASN" )( "PRO" )( "GLN" )( "ARG" )( "SER" )( "THR" )( "VAL" )
                 ( "TRP" )( "TYR" ).
                 convert_to_container<std::vector<std::string> >();
             one_letter_codes_ = boost::assign::list_of
                  ( 'A' )( 'C' )( 'D' )( 'E' )( 'F' )( 'G' )( 'H' )
                  ( 'I' )( 'K' )( 'L' )( 'M' )( 'M' )( 'N' )( 'P' )( 'Q' )( 'R' )( 'S' )
                  ( 'T' )( 'V' )( 'W' )( 'Y' ).
                  convert_to_container<std::vector<char> >();
             backbone_atom_names_ = boost::assign::list_of
               ( " CA " )( " N  " )( " C  " ) ( " O  " ).
               convert_to_container<std::set<iotbx::pdb::str4> >();
           };
        MacromoleculeType(char X) : //non-default constructor
           unknown_one_letter_( 'X' ),
           unknown_three_letter_( "UNK" ),
           min_chain_length_(0),
           type_name_ ( "unknown" )
           {
           };
        ~MacromoleculeType();

        // Factory function
        //const MacromoleculeType get_chain_type(
         //   const iotbx::pdb::hierarchy::chain& chain
         //   );
         void get_chain_type(
            const iotbx::pdb::hierarchy::chain& chain
            );

        // Accessors
        const std::set< iotbx::pdb::str4 >& get_backbone_atom_names() const;
        const std::string& get_type_name() const;
        char get_unknown_one_letter() const;

        // Translation
        std::map< std::string, char > get_three_to_one() const;
        std::string residue_group_sequence(
            const std::vector< iotbx::pdb::hierarchy::residue_group >& rgs
            ) const;

        // Conformance
        bool recognize_chain(
            const iotbx::pdb::hierarchy::chain& chain,
            double confidence = 0.9
            ) const;
        size_t count_recognized_residues(
            const std::vector< iotbx::pdb::hierarchy::residue_group >& rgs
            ) const;

        // Predefined types
        //static const MacromoleculeType protein;
        //static const MacromoleculeType unknown;
        //static const std::vector< MacromoleculeType > known_types;
};

}  } // namespace phaser::ncs

#endif /*PHASER_NCS_MACROMOLECULETYPE_H*/
