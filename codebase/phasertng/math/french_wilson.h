#ifndef __phasertng_math_french_wilson_class__
#define __phasertng_math_french_wilson_class__
#include <scitbx/array_family/shared.h>

using namespace scitbx;

namespace phasertng {
namespace math {

class french_wilson
{
  public:
    french_wilson() {}

  public:
    double expectEFWacen(double eosq, double sigesq);
    double expectEsqFWacen(double eosq, double sigesq);
    double expectEFWcen(double eosq, double sigesq);
    double expectEsqFWcen(double eosq, double sigesq);
    double expectEFW(double eosq, double sigesq, bool centric);
    double expectEsqFW(double eosq, double sigesq, bool centric);

    bool
    is_FrenchWilsonF(
        af::shared<double> F,
        af::shared<double> SIGF,
        af::shared<bool> CENT);

    bool
    is_FrenchWilsonI(
        af::shared<double> I,
        af::shared<double> SIGI,
        af::shared<bool> CENT);
};

}}
#endif
