//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_LatticeTranslocation_class__
#define __phasertng_LatticeTranslocation_class__
#include <phasertng/main/includes.h>
#include <phasertng/type/flt_numbers.h>

namespace phasertng {

  class LatticeTranslocation
  {
    public:
      std::vector<std::pair<dvect3,double>> DISORDER;
      bool HAS_LTD = false;

    LatticeTranslocation() {}
    void parse(std::vector<type::flt_numbers>&); //separate from constructor because it throws
  };

}
#endif
