//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_strings_class__
#define __phasertng_type_strings_class__
#include <phasertng/type/optional.h>
#include <set>
#include <string>

namespace phasertng {
namespace type {

typedef std::vector<std::string> sv_string;

class strings : public optional
{
  sv_string dephalt = sv_string(0);
  sv_string uvalue = sv_string(0);

  public:
  strings() : optional() {}
  strings(const std::string parameter,
          const sv_string dephalt_ = {}
         );

  void set_value(sv_string others);
  void push_back(const std::string& other);
  void set_value(const sv_string& vcards,const sv_string& others);
  bool is_default() const;
  void set_default();
  sv_string value_or_default() const;
  std::string value_or_default(size_t i) const;
  sv_string value_or_default_unique() const;
  std::set<std::string> value_or_default_set() const;
  std::set<std::string> value_or_default_set_upper() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::strings& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
