#ifndef __phasertng_io_jiffy_functions__
#define __phasertng_io_jiffy_functions__
#include <phasertng/main/includes.h>

namespace phasertng {

std::string stoq(const std::string&);
std::string itos(const int);
std::string itos(const size_t);
std::string itos(const int,const int,bool=true,bool=false); //rj, sign
std::string dtoi(const double);
std::string dtos(const double);
std::string dtoss(const double,int); //to stream, integer conversion
std::string dtos(const double,int,int,bool,bool=false,bool=false); //sign, trailing, rj
std::string dtos(const double,int,int); //width, precision
std::string dtos(const double,int); //precision
std::string gtos(const double,int);
std::string etos(const double,int,int,bool=false);
std::string dvtos(const dvect3&);
std::string dvtos(const dvect3&,int,int);
std::string dvtos(const dvect3&,int,int,bool,char);
std::string dvtos(const dvect3&,int);
std::string dvtoss(const dvect3&,int); //to stream, integer conversion
std::string dvtos(const scitbx::af::double6&,int=7,int=2);
std::string dvtos(const af_double&);
std::string dvtos(const sv_double&);
std::string dvtos(const af_double&,int,int);
std::string dvtos(const sv_double&,int,int);
std::string dvtos(const sv_double&,int);
std::string dvtos(const sv_double&,int,std::string);
std::string dvtos(const std::pair<double,double>&);
std::string ivtos(const std::pair<int,int>&);
std::string ivtos(const ivect3&);
std::string ivtos(const sv_int&);
std::string ivtos(const sv_int&,std::string);
std::string ivtos(const sv_int&,int,bool=false,bool=false);
std::string ivtos(const af_int);
std::string svtos(const sv_string&);
std::string svtos(const af_string);
std::string dmtos(const dmat33&,int,int);
std::string dmtos(const dmat33&,int);
std::string dmtos(const dmat33&);
std::string dmtos(const dmat6&);
std::string dmtoss(const dmat33&,int); //to stream, integer conversion
std::string ntos(const int,const int);
std::string btos(const bool);
std::string btoch(const bool);
std::string otos(const char); //optional to string
std::string bvtos(const sv_bool&);
std::string btos5(const bool);
std::string on_off(const bool);
std::string chtos(char);
std::string ctos(const cmplex);
std::string ctos(const cmplex,int,bool);
std::string ctos(const cmplex,int,int,bool,bool);
std::string cmtos(const cmat33&,int);
std::string stolower(std::string);
std::string stoupper(std::string);
std::string nNtos(const int,const int);
std::string snprintftos(const char*,...);
std::string afstringtos(af_string);
int itow(int,int=2);

}//end namespace phasertng

#endif
