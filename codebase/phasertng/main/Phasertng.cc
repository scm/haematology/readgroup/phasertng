//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/io/FileSystem.h>
#include <ccp4io/libccp4/ccp4/ccp4_errno.h>
#include <boost/functional/hash.hpp>
#include <thread>
#include <phasertng/data/ReflRowsData.h>
#include <phasertng/data/ReflRowsAnom.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>
#include <phasertng/math/table/alogchI0.h>
#include <phasertng/math/table/alogch.h>
#include <phasertng/math/table/sim.h>
#include <phasertng/math/table/lnfactorial.h>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/math/table/ParabolicCylinderD.h>
#include <phasertng/math/table/sphbessel.h>


//#define PHASERTNG_DEBUG_HASHING
//#define PHASERTNG_DEBUG_INPUTOUTPUT

namespace phasertng {

const table::ebesseli0& tbl_ebesseli0 = table::ebesseli0::getInstance();
const table::ebesseli1& tbl_ebesseli1  = table::ebesseli1::getInstance();
const table::alogchI0& tbl_alogchI0  = table::alogchI0::getInstance();
const table::alogch& tbl_alogch  = table::alogch::getInstance();
const table::sim& tbl_sim  = table::sim::getInstance();
const table::lnfactorial& tbl_lnfac  = table::lnfactorial::getInstance();
const table::cos_sin_lin_interp_table& tbl_cos_sin = table::cos_sin_lin_interp_table::getInstance();
const table::ParabolicCylinderD& tbl_pcf  = table::ParabolicCylinderD::getInstance();
const table::sphbessel& tbl_sphbessel  = table::sphbessel::getInstance();

  void
  Phasertng::run(std::string Cards_)
  {
    //for python version of this code see TwilightZone.py
    clear_memory(); //so that base class doesn't fill memory when called from python
    //clear the input, but keep track of any input parameters that have already been used
    //this is useful for getting .style = tng:modes:labin+hklin etc
    std::multimap<std::string,std::string>  input_parameters = input.input_parameters;
    std::multimap<std::string,std::string>  result_parameters = input.result_parameters;
    CCP4::ccp4_liberr_verbosity(0);
    Output::clear_storage();

    Cards = Cards_;
    auto tmpint = input.generic_int; //or else it is wiped
    auto tmpstr = input.generic_string; //or else it is wiped
    input = InputCard();
    input.generic_int = tmpint; //or else it is wiped
    input.generic_string = tmpstr; //or else it is wiped
    input.parse(Cards); //no errors thrown
    //set suite before logging anything
    Phasertng::set_user_input_for_suite(input); //no errors thrown
    //log the banner, which contains no information
    logProgramStart(out::CONCISE);
    //now we can run something that might fail
    sv_string multi = input.value__choice.at(".phasertng.mode.").value_or_default_multi();

//PHASERTNG_DEBUG_THROW_STACKTRACE in Phasertng.h
#ifndef PHASERTNG_DEBUG_THROW_STACKTRACE
    try {
#endif
      for (int imode = 0; imode < multi.size(); imode++)
      {
        StartModeTime();
        if (imode > 0)
        {
          Cards = Cards_; //reset the results if we have started again
          input = InputCard(); //clear
          input.parse(Cards); //no errors thrown
        }
        af_string pymulti(multi.size()); //inelegant conversion for python boosting which uses af_string
        for (int i = 0; i < multi.size(); i++) pymulti[i] = multi[i];
        bool job_has_been_run_previously = Phasertng::setup_before_call_to_mode(pymulti,imode); //input errors thrown

        logSectionHeader(out::SUMMARY,"RUN MODE " + stoupper(input.running_mode));
        if (job_has_been_run_previously)
        {
          Phasertng::skipping();
        }
        else
        {
          logTab(out::PROCESS,"Running...");
          auto pymodes = input.python_modes();
          //Revert the parameters to those at start of run, to exclude all those set
          // but not used
          input.input_parameters = input_parameters;
          input.result_parameters = result_parameters;
          if (input.running_mode == "chk")
          {
            logTab(out::SUMMARY,"Checked input");
          }
          else if (input.running_mode == "aniso") runANISO();
          else if (input.running_mode == "brf")   runBRF();
          else if (input.running_mode == "btf")   runBTF();
          else if (input.running_mode == "cca")   runCCA();
          else if (input.running_mode == "ccs")   runCCS();
          else if (input.running_mode == "data")  runDATA();
          else if (input.running_mode == "eatm")  runEATM();
          else if (input.running_mode == "ellg")  runELLG();
          else if (input.running_mode == "emap")  runEMAP();
          else if (input.running_mode == "emm")   runEMM();
          else if (input.running_mode == "esm")   runESM();
          else if (input.running_mode == "etfz")  runETFZ();
          else if (input.running_mode == "exp1")  runEXP1();
          else if (input.running_mode == "feff")  runFEFF();
          else if (input.running_mode == "frf")   runFRF();
          else if (input.running_mode == "frfr")  runFRFR();
          else if (input.running_mode == "ftf")   runFTF();
          else if (input.running_mode == "ftfr")  runFTFR();
          else if (input.running_mode == "fuse")  runFUSE();
          else if (input.running_mode == "gyre")  runGYRE();
          else if (input.running_mode == "imap")  runIMAP();
          else if (input.running_mode == "imtz")  runIMTZ();
          else if (input.running_mode == "info")  runINFO();
          else if (input.running_mode == "ipdb")  runIPDB();
          else if (input.running_mode == "jog")   runJOG();
          else if (input.running_mode == "mapz")  runMAPZ();
          else if (input.running_mode == "mcc")   runMCC();
          else if (input.running_mode == "move")  runMOVE();
          else if (input.running_mode == "msm")   runMSM();
          else if (input.running_mode == "pak")   runPAK();
          else if (input.running_mode == "perm")  runPERM();
          else if (input.running_mode == "pose")  runPOSE();
          else if (input.running_mode == "ptf")   runPTF();
          else if (input.running_mode == "put")   runPUT();
          else if (input.running_mode == "rbgs")  runRBGS();
          else if (input.running_mode == "rbm")   runRBM();
          else if (input.running_mode == "rbr")   runRBR();
          else if (input.running_mode == "rellg") runRELLG();
          else if (input.running_mode == "rmr")   runRMR();
          else if (input.running_mode == "sga")   runSGA();
          else if (input.running_mode == "sgx")   runSGX();
          else if (input.running_mode == "span")  runSPAN();
          else if (input.running_mode == "spr")   runSPR();
          else if (input.running_mode == "srf")   runSRF();
          else if (input.running_mode == "ssa")   runSSA();
          else if (input.running_mode == "ssd")   runSSD();
          else if (input.running_mode == "ssm")   runSSM();
          else if (input.running_mode == "ssp")   runSSP();
          else if (input.running_mode == "ssr")   runSSR();
          else if (input.running_mode == "test")  runTEST();
          else if (input.running_mode == "tfz")   runTFZ();
          else if (input.running_mode == "tncs")  runTNCS();
          else if (input.running_mode == "tncso") runTNCSO();
          else if (input.running_mode == "twin")  runTWIN();
          else if (pymodes.count(input.running_mode))
                logTab(out::SUMMARY,"Only available from python");
          else throw Error(err::DEVELOPER,"The Twilight Zone");
          //because you might be looking for a python mode
        }

        the_twilight_zone:
        Phasertng::cleanup_after_call_to_mode(job_has_been_run_previously);

        //essential for when cleanup is within the mode loop in c++
        //superfluous when called from python with single modes only
        {{
        bool write_mode(true);
        bool write_suite(true);
        //clear the cards and restart next loop
        Cards_ = "";
        af_string cardline = cards(write_mode,write_suite); //internal, keep mode
        PHASER_ASSERT(cardline.size());
        for (auto card : cardline)
          Cards_ += card + "\n";
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
        if (Suite::Extra())
        {
          out::stream where = out::TESTING;
          logUnderLine(where,"Recycling");
          logTabArray(where,cardline);
          logBlank(where);
        }
#endif
        }}
      }
      logProgramEnd(out::CONCISE);
#ifndef PHASERTNG_DEBUG_THROW_STACKTRACE
    }
    catch (Error err) {
      logError(out::CONCISE,err);
      if (WriteLog()) write_logfiles(false); //bailout information, not nice though
      logTrailer(out::SUMMARY);
    }
    catch (std::bad_alloc const& err) {
      logError(out::CONCISE,Error(err::MEMORY,err.what()));
      if (WriteLog()) write_logfiles(false); //bailout information, not nice though
      logTrailer(out::SUMMARY);
    }
    catch (std::out_of_range const& err) {
      logError(out::CONCISE,Error(err::UNKNOWN,err.what()));
      if (WriteLog()) write_logfiles(false); //bailout information, not nice though
      logTrailer(out::SUMMARY);
    }
    catch (std::exception const& err) {
      //trick to find out that this is an assert not another unhandled
      const std::string message = err.what();
      bool passert(message.rfind("Program internal error", 0) == 0);
      if (passert)
        logAssert(out::CONCISE,message);
      else
        logError(out::CONCISE,Error(err::UNHANDLED,message));
      if (WriteLog()) write_logfiles(false); //bailout information, not nice though
      logTrailer(out::SUMMARY);
    }
    catch (...) {
      logError(out::CONCISE,Error(err::UNKNOWN));
      if (WriteLog()) write_logfiles(false); //bailout information, not nice though
      logTrailer(out::SUMMARY);
    }
#endif
    logTab(out::PROCESS,"Exit Code: " + itos(exit_code()));
    input.value__boolean.at(".phasertng.notifications.success.").set_value(this->success());
    std::string type = this->error_type(); hoist::to_lower(type);
    input.value__choice.at(".phasertng.notifications.error.type.").set_value(type);
    input.value__string.at(".phasertng.notifications.error.message.").set_value(this->error_message());
    input.value__int_number.at(".phasertng.notifications.error.code.").set_value(this->exit_code());
    input.value__flt_number.at(".phasertng.notifications.hires.").set_value(REFLCOLSMAP.data_hires());
  } //the_end

  bool
  Phasertng::setup_before_call_to_mode(af_string multi,int imode)
  {
    Output::clear_mode_storage();
    input.running_mode = multi[imode];
    //override the writing of all files except xml here, with global flag in Suite
    Suite::suite_running_mode_chk = (multi[imode] == "chk");
    Output::set_imode(imode); //indices for multi-mode warnings etc
    input.value__string.at(".phasertng.notifications.git_revision.").set_value(git_revision());
    std::string autotitle = looptitle();
    if (autotitle.size()) //regardless of input title, picard overwrites whatever you say
      input.set_as_str(".phasertng.title.",autotitle);
    logHeaderTitle(out::CONCISE,input.header(),
        input.value__string.at(".phasertng.title.").value_or_default());
    logTab(out::PROCESS,"Running mode: " + input.running_mode + " -of- " + svtos(multi));

    if (imode == 0) //has been incremented
    {
      double cpus = input.value__flt_number.at(".phasertng.time.cumulative.cpu.").value_or_default();
      double wall = input.value__flt_number.at(".phasertng.time.cumulative.wall.").value_or_default();
      set_cumulative_wall_seconds(wall);
      set_cumulative_cpus_seconds(cpus);
    }

    logSectionHeader(out::SUMMARY,"INPUT");

#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      logUnderLine(where,"Cards");
      logKeywords(where,Cards); //right place to be visible
     // if (failure()) throw(*this); //parse, set_user_input
      //input includes all keywords, even output ones
      bool phil(false);
      std::string interpreter = input.unparse(show::VALUE,phil);
      logUnderLine(where,"User input");
      interpreter.size() ? logKeywords(where,interpreter):
                           logKeywords(where,"[NONE]");
      logBlank(where);
      logUnderLine(where,"All input");
      interpreter = input.unparse(show::VALUEORDEFAULT,phil);
      logKeywords(where,interpreter);
      logBlank(where);
      if (input.ADDITIONAL_TOKENS.size())
      {
        logBlank(where);
        std::string warning = "Tokens were ignored\n";
        for  (auto tokens : input.ADDITIONAL_TOKENS) warning += tokens + "\n";
        warning.pop_back();
        logWarning(warning);
      }
    }
#endif

    input.analyse(); //store all errors
    //includes clear incompatible linkage arrays, which will make sense of the keywords

    FILEOUT.clear(); //reset the file list for each mode

#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      logUnderLine(where,"Input Keywords for Mode");
      std::set<std::string> keyset = input.input_keywords_for_mode({input.running_mode});
      for (auto item:keyset)
        logTab(where,"relevant keyword from master phil: "+item);
      if (!keyset.size())
        logTab(where,"None");
    }
#endif

    {{
    out::stream where = out::CONCISE;
    logUnderLine(where,"Non-default Input Keywords");
    bool write_mode(false);
    bool write_suite(true);
    std::set<std::string> keyset = input.input_keywords_for_mode({input.running_mode});
    auto text = cards(write_mode,write_suite,keyset);
    logTab(where,"phasertng mode " + input.running_mode); //hack! in case of multi-modes
    text.size() ?
      logTabArray(where,text) :
      logTab(where,"(None)");
    }}

    if (input.has_errors()) //mainly fileopen
      logWarning("Input has errors"); //for now, see below for throw
    //but don't throw here because the database directory has not been created, and multi-mode jobs will write to wrong dir etc

    //must hardwire optional keywords in here
    //value only known at this point
    bool check_io(input.running_mode != "chk" and Write()); //formerly WriteFiles());
    bool check_chk(input.running_mode == "chk" and DataBase().size()); //check with chk if DataBase is set (not default)
    if (check_io or check_chk)
    {
      out::stream where = check_chk ? out::LOGFILE : out::VERBOSE;
      logUnderLine(where,"Database");
      logTab(where,"database: " + DataBase());
      FileSystem dbdir(DataBase(),{"",""});
      dbdir.prepare_and_check_database();
      if (DataBase() != dbdir.fstr()) //full path expansion
        logTab(where,"database: " + dbdir.fstr());
      logTab(where,"database: OK");
    }

    bool io_overwrite = input.value__boolean.at(".phasertng.overwrite.").value_or_default();
    if (imode == 0) OVERWRITE = io_overwrite;

    if (input.running_mode == "chk") //and return
    { //this is a special case to throw here
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Keywords");
      for (auto error : input.ERRORS)
      {
        std::string message;
        for (auto item : error.second)
          message += item + " : ";
        *(static_cast<Error *>(this)) = Error(error.first,message);
        throw Error(error.first,message);
      }
      //bail out here, regardless of error status
      if (!input.has_errors())
        logTab(where,"keyword input: OK");
      return false; //status job_has_been_run_previously
    }

    if (input.running_mode != "tree")
    {
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Directed Acyclic Graph");
    FileSystem  dagfile;
    if (!input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").is_default() and
        !input.value__path.at(".phasertng.dag.cards.subdir.").is_default())
    {
      auto subdir = input.value__path.at(".phasertng.dag.cards.subdir.").value_or_default();
      auto dagname = input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").value_or_default();
      dagfile.SetFileSystem(DataBase(),{subdir,dagname});
      logTab(where,"Dag for read: " + dagfile.qfstrq() + "");
    }
    else
    {
      logTab(where,"Dag for read: None");
    }
    if (DAGDB.filesystem_is_set())
    {
      logTab(where,"(Dag on disk: " + DAGDB.qfstrq() + ")");
    }
    if (!DAGDB.size())
    {
      logTab(where,"Dag not in memory");
      if (!input.value__path.at(".phasertng.dag.cards.subdir.").is_default() and
          !input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").is_default())
      {
        logTab(where,"Dag read from file");
        auto subdir = input.value__path.at(".phasertng.dag.cards.subdir.").value_or_default();
        auto dagname = input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").value_or_default();
        DAGDB.SetFileSystem(DataBase(),{subdir,dagname});
        DAGDB.parse_card();
        DAGDB.restart(); //this is very important because it sets the WORK pointer
      }
    }
    else if (DAGDB.size())
    {
      logTab(where,"Dag in memory");
      if (!input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").is_default() or
          !input.value__path.at(".phasertng.dag.cards.subdir.").is_default())
      {
        logTab(out::VERBOSE,"Compare stems " + DAGDB.stem() + " versus " + dagfile.stem());
        bool samedag = (DAGDB.stem() == dagfile.stem());
        samedag ?
          logTab(where,"Dag requested on input and in memory (on disk) are the same"):
          logAdvisory("Dag requested on input and in memory (on disk) are different");
        //for em_placement, the script is bad and the names are not kept in sync with what is on the disk
        //so the default is false because these scripts are easier to make
        bool read_file(input.value__boolean.at(".phasertng.dag.read_file.").value_or_default());
        if (samedag)
          logTab(where,"Dag unchanged");
        else if (!read_file and !samedag)
          logAdvisory("Dag in memory is retained (not replaced)");
        else if (read_file and !samedag)
        {
          logAdvisory("Dag in memory is replaced (not retained)");
          auto subdir = input.value__path.at(".phasertng.dag.cards.subdir.").value_or_default();
          auto dagname = input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").value_or_default();
          DAGDB.SetFileSystem(DataBase(),{subdir,dagname});
          DAGDB.NODES.clear(); //don't clear the entries because they must be retained in memory if data files not written
          DAGDB.parse_card();
        }
      }
      DAGDB.restart(); //this is very important because it sets the WORK pointer
    }
    logTab(where,"Number of Nodes = " + itos(DAGDB.size()));
    where = out::VERBOSE;
    logTab(where,"Number of Parents = " + itos(DAGDB.set_of_parents().size()));
    logTab(where,"Number of GrandParents = " + itos(DAGDB.set_of_grandparents().size()));
    logTab(where,"Number of Model Entries = " + itos(DAGDB.ENTRIES.size()));
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra())
    for (auto item : DAGDB.ENTRIES)
    {
      auto where = out::TESTING;
      auto entry = &item.second;
      logTab(where,"memory: " + item.first.str());
      logTab(where,"memory models " + btos(entry->MODELS.in_memory()));
      logTab(where,"memory fcalcs " + btos(entry->FCALCS.in_memory()));
      logTab(where,"memory imodels " + btos(entry->IMODELS.in_memory()));
      logTab(where,"memory ifcalcs " + btos(entry->IFCALCS.in_memory()));
      logTab(where,"memory decomposition " + btos(entry->DECOMPOSITION.in_memory()));
      logTab(where,"memory interpolation " + btos(entry->INTERPOLATION.in_memory()));
      logTab(where,"memory variance " + btos(entry->VARIANCE.in_memory()));
      logTab(where,"memory trace " + btos(entry->TRACE.in_memory()));
      logTab(where,"memory monostructure " + btos(entry->MONOSTRUCTURE.in_memory()));
      logTab(where,"memory substructure " + btos(entry->SUBSTRUCTURE.in_memory()));
      logTab(where,"memory coordinates " + btos(entry->COORDINATES.in_memory()));
      logTab(where,"memory density " + btos(entry->DENSITY.in_memory()));
      logTab(where,"memory gradient " + btos(entry->GRADIENT.in_memory()));
    }
#endif
    logTab(where,"Number of Data  Entries = " + itos(DAGDB.REFLIDS.size()));
    //entries may not be loaded so below is nonsense
    // logTab(where,"Number of Entries = " + itos(DAGDB.ENTRIES.size()));
    auto set_of_entries = DAGDB.set_of_entries();
    for (auto entry : set_of_entries)
      logTab(where,"Model: " + DAGDB.ENTRIES[entry].DOGTAG.str_str());
    for (auto entry : DAGDB.REFLIDS)
      logTab(where,"Data:  " + entry.second.str_str());
    //previously, tested for database for certain modes,
    //but will now test search modlid separately
    DAGDB.reset_entry_pointers(DataBase());
    //Change the node
    //Here we reset any parameters that are stored and utilized from the dag
    //the parameters can be set here rather than forcing separate edit of dag
    //this is a bit of a hack
    if (!input.value__boolean.at(".phasertng.dag.tncs_present_in_model.").is_default())
    { //can be set with tncs present_in_model or altered here on the dag file. Default is None.
      logEllipsisOpen(out::VERBOSE,"Set dag global parameters");
      //currently there is only one, the tncs present in model flag
      bool io_tncs_modelled = input.value__boolean.at(".phasertng.dag.tncs_present_in_model.").value_or_default();
      logTab(out::VERBOSE,"tNCS present in model = " + btos(io_tncs_modelled));
      for (auto& node : DAGDB.NODES)
        node.TNCS_MODELLED = io_tncs_modelled;
      logEllipsisShut(out::VERBOSE);
    }
    if (!input.value__int_number.at(".phasertng.dag.maximum_stored.").is_default())
    {
      int maxstor = input.value__int_number.at(".phasertng.dag.maximum_stored.").value_or_default();
      if (DAGDB.size() > maxstor)
      {
        DAGDB.NODES.apply_maximum_stored(maxstor);
        maxstor == 1 ?
          logTab(where,"Dag truncated on input to 1 node"):
          logTab(where,"Dag truncated on input to " + itos(maxstor) + " nodes");
        logTab(where,"Truncated Number of Nodes = " + itos(DAGDB.size()));
        logTab(where,"Truncated Number of Parents = " + itos(DAGDB.set_of_parents().size()));
        logTab(where,"Truncated Number of GrandParents = " + itos(DAGDB.set_of_grandparents().size()));
      }
    }
    }

#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra() and DAGDB.size())
    {
      out::stream where = out::TESTING;
      logUnderLine(where,"Input Dag Nodes");
      auto text = DAGDB.unparse_card_summary();
      logTabArray(where,text);
      logBlank(where);
    }
#endif

    if (DAGDB.ENTRIES.size() and WriteData()) //if we are not writing, then keep all in memory
    {
      out::stream where = out::VERBOSE;
      logTab(where,"Clear memory of unused entries");
      std::set<Identifier> fullset;
      for (auto& item : DAGDB.ENTRIES)
        fullset.insert(item.first);
      int e1 = DAGDB.ENTRIES.size();
      DAGDB.clear_unused_entries();
      int e2 = DAGDB.ENTRIES.size();
      for (auto& item : DAGDB.ENTRIES)
        fullset.erase(item.first);
      logTab(where,"Number of entries cleared = " + itos(e1-e2));
      for (auto& item : fullset)
        logTab(where,"Entry cleared: " + item.str());
      if (e2 < e1)
        logAdvisory("Unused entries cleared from memory");
    }

    //if not set this will be initialized at the end
    bool job_has_been_run_previously(false);
    if (input.running_mode != "tree")
    { //only do once for whole database
      std::string pathtag = input.running_mode;
      if (pathtag == "join") //joinA, joinB etc to differentiate between types
        pathtag += chtos(input.generic_int);
      bool consecutive_numbering(true);
      if (!input.value__boolean.at(".phasertng.dag.consecutive.").value_or_default())
      //unparse the input, take only the control cards, and hash it (excluding dag filename)
        DAGDB.PATHWAY.shift_edge(hashing(),pathtag);
      else
        DAGDB.PATHWAY.increment(pathtag);
      //if (!input.value__uuid_number.at(".phasertng.dag.id.").is_default())
      //  DAGDB.PATHWAY.ULTIMATE.initialize_from_ullint(
      //     input.value__uuid_number.at(".phasertng.dag.id.").value_or_default());
      //copy the pathway to the pathlog, which will be the case by default
      //patch the pathlog when 'put' is used to patch solutions in picard
      DAGDB.PATHLOG = DAGDB.PATHWAY;
      FileSystem ultimate(DataBase(),{ DAGDB.PATHWAY.ULTIMATE.database_subdir(), ""});
/*
      if (!ultimate.filesystem_is_set())
        logTab(out::VERBOSE,"Detect Previous Job: None (Database not set)");
      else
*/
      if (Write()) //if no output is requested, then assume we are not interested about anything on disk
      {
        //don't overwrite here
        out::stream where = out::SUMMARY;
        logUnderLine(where,"Detect Previous Job");
        logTab(where,"Job Mode: \"" + input.running_mode + "\"");
        job_has_been_run_previously = ultimate.filepath_exists();
        logTab(where,"Job Identifier: " + DAGDB.PATHWAY.ULTIMATE.str() + "<-" + DAGDB.PATHWAY.PENULTIMATE.str());
        logTab(where,"Job Directory:  " + ultimate.qfstrq());
        logTab(where,"Was an identical previous job detected?:  " +
             std::string(job_has_been_run_previously ? "Yes":"No"));
        logTab(where,"Will any previous job be overwritten?:    " +
             std::string(OVERWRITE ? "Yes":"No"));
        //store overwrite with job flag for use of single boolean below
        job_has_been_run_previously = job_has_been_run_previously and !OVERWRITE;
        if (job_has_been_run_previously)
        {
          //clear the reflections which will be in the wrong state
          REFLECTIONS = nullptr; //must be done before the reflections are read in again
          REFLCOLSMAP.NREFL = 0; //flag for reading again!!!!
          try
          { //skip to the results
            auto cards =
              input.value__boolean.at(".phasertng.dag.consecutive.").value_or_default() ? other::RESULT_CARDS : other::PATHWAY_CARDS;
            FileSystem FILENAME(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(cards));
            logTab(where,"Cards file: reading from " + FILENAME.qfstrq());
            std::fstream infile;
            infile.open(FILENAME.fstr().c_str(),std::fstream::in);
            if (!infile.is_open())
              throw Error(err::FILEOPEN,FILENAME.fstr());
            bool phil(false);

            std::string interpreter = "";
            std::set<std::string> keyset = input.input_keywords_for_mode({input.running_mode});
            keyset.erase("dag"); //because we are not interested in the filename, data could be stored DAGDB, use the dag itself
            auto lines = cards_show(false,true,keyset,show::VALUEORDEFAULT);
            for (auto& line : lines)
            {
              hoist::algorithm::trim(line);
              if (!hoist::algorithm::ends_with(line," None") and !hoist::algorithm::contains(line," None "))
                interpreter += line + "\n";
            }
            input = InputCard(); //clear
            try {
              std::stringstream ss;
              ss << infile.rdbuf();
              infile.close();
              std::string restoration = ss.str() + "\n";
              //need to update with NEW information eg the xmlout card including non-set
              //the order should not matter here
                          restoration += interpreter;
              input.parse(ss.str()); //no errors thrown
              input.value__boolean.at(".phasertng.overwrite.").set_value(OVERWRITE);
#ifdef PHASERTNG_DEBUG_HASHING
            logTab(where,"Restoration: \n"+ss.str());
            logTab(where,"Cards: \n"+Cards);
#endif
            }
            catch (...)
            {
              throw Error(err::STDIN,FILENAME.fstr());
            }
            //convert state to output state and skip to the end
            input.running_mode = multi[imode]; //because already incremented
            DAGDB.SetFileSystem(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::DAG_CARDS));
            logTab(where,"Dag file: reading from " + DAGDB.qfstrq());
            std::fstream ifile;
            ifile.open(DAGDB.fstr().c_str(),std::fstream::in);
            if (!ifile.is_open())
              throw Error(err::FILEOPEN,DAGDB.fstr());
            std::stringstream sss;
            sss << ifile.rdbuf();
            ifile.close();
            DAGDB.parse_cards(sss.str());
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
            if (Suite::Extra() and DAGDB.size())
              logTabArray(out::TESTING,DAGDB.NODES[0].logNode("First node"));
#endif
            logTab(where,"Job Restore Success");
          }
          catch (std::exception const& err) {
            logAdvisory(err.what());
            job_has_been_run_previously = false;
            logTab(where,"Job Restore Failed");
          }
        }
      }
    }

    //after keywords header has been printed
    //after the output directory has been created and opened
    if (input.has_errors()) //mainly fileopen
    {
      for (auto error : input.ERRORS)
      {
        std::string message;
        for (auto item : error.second)
          message += item + " : ";
        *(static_cast<Error *>(this)) = Error(error.first,message);
        // throw(*this); //this will throw ERROR message here
        //above has compilation error with unique_ptr
        throw Error(error.first,message);
      }
    }

    if (input.input_keywords_for_mode({input.running_mode}).count("reflid"))
    if (!job_has_been_run_previously)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Reflections");
      Identifier reflid;
      { //  may also need to change the reflections if it comes from input
        if (DAGDB.size())
        { //to testing because this may be completely irrelevant eg with rfac mode
          reflid = DAGDB.NODES.front().REFLID;
          logTab(out::VERBOSE,"Reflections identifier from database: " + reflid.str());
        }
        if (input.value__boolean.at(".phasertng.reflid.force_read.").value_or_default())
        {
          if (input.value__uuid_number.at(".phasertng.reflid.id.").is_default() and DAGDB.size())
          {
            reflid = DAGDB.NODES.front().REFLID;
            logTab(where,"Reflections identifier from dag: " + reflid.str());
          }
          else
          {
            reflid.initialize_from_ullint(input.value__uuid_number.at(".phasertng.reflid.id.").value_or_default());
            reflid.initialize_tag(input.value__string.at(".phasertng.reflid.tag.").value_or_default());
            logTab(where,"Reflections identifier from input: " + reflid.str());
          }
        }
      }
      //here is the call to the reflection setup
      auto reflections_initialize = setup_reflid_type(reflid,out::LOGFILE,out::VERBOSE);
      //-------
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
      if (Suite::Extra())
      {
        out::stream where = out::TESTING;
        logTabArray(where,REFLCOLSMAP.logLabin("Reflections Read"));
        logTabArray(where,REFLCOLSMAP.logReflections("Reflections Read"));
      }
#endif
      if (reflections_initialize == "ok")
        logTab(out::SUMMARY,"Reflections already initialized and formatted");
      else if (reflections_initialize == "changed")
        logTab(out::SUMMARY,"Reflections initialized and formatted");
      else if (reflections_initialize == "none")
        logTab(out::SUMMARY,"Reflections not required");
      else if (reflections_initialize == "invalid")
        logTab(out::SUMMARY,"No reflections");
      else if (reflections_initialize == "initialized")
        logTab(out::SUMMARY,"Reflections initialized");
      else  //reformatted and type
        logTab(out::SUMMARY,"Reflections " + reflections_initialize);
      //don't use work iterator as this may be in a loop itself
      PHASER_ASSERT(reflid_can_be_different.size());
      if (!reflid_can_be_different.count(input.running_mode) and
           DAGDB.size())// and DAGDB.NODES.front().REFLID.is_set_valid())
      {
        out::stream where = out::LOGFILE;
        reflid = DAGDB.NODES.front().REFLID;
        logTab(where,"Reflections identifier from dag: " + reflid.str());
        for (int i = 1; i < DAGDB.NODES.size(); i++)
        {
          if (reflid != DAGDB.NODES[i].REFLID)
          {
            logTab(where,"Reflections identifier from dag: " + DAGDB.NODES[i].REFLID.str());
            throw Error(err::INPUT,"Reflections identifier not consistent in all dag nodes");
          }
        }
      }
      if (REFLCOLSMAP.REFLID.is_set_valid())
      {
        out::stream where = out::LOGFILE;
        logTab(where,"Space Group = " + REFLCOLSMAP.SG.CCP4 + " (" + REFLCOLSMAP.SG.HALL + ")");
        logTab(where,"Unit Cell =   " + dvtos(REFLCOLSMAP.UC.cctbxUC.parameters()));
        if (REFLCOLSMAP.Z)
          logTab(where,"Unit Cell Z = " + dtos(REFLCOLSMAP.Z,(std::round(REFLCOLSMAP.Z)==REFLCOLSMAP.Z)?0:2));
        logTab(where,"Resolution =  " + dtos(REFLCOLSMAP.data_hires(),2));
        if (boost::logic::indeterminate(REFLCOLSMAP.TNCS_PRESENT))
          logTab(where,"tNCS Order =   None");
        else if (REFLCOLSMAP.TNCS_ORDER > 1)
          logTab(where,"tNCS Order =  " + itos(REFLCOLSMAP.TNCS_ORDER) + "/" + dvtos(REFLCOLSMAP.TNCS_VECTOR));
        else if (REFLCOLSMAP.TNCS_ORDER == 1)
          logTab(where,"tNCS Order =  " + itos(REFLCOLSMAP.TNCS_ORDER));
      }
    }

    if (!job_has_been_run_previously)
    {
      if (input.input_keywords_for_mode({input.running_mode}).count("threads"))
      {
        this->set_nthreads(input.value__int_number.at(".phasertng.threads.number.").value_or_default());
        this->set_use_strictly_nthreads(input.value__boolean.at(".phasertng.threads.strictly.").value_or_default());
        out::stream where = out::LOGFILE;
        logBlank(where);
        logTab(where,"Number of threads: " + itos(NTHREADS));
        logTab(where,"Strictly number of threads: " + btos(USE_STRICTLY_NTHREADS));
        logBlank(where);
      }
    }
    DAGDB.restart(); //make sure WORK is set before we get going

    if (Write()) //if no output is requested, then assume we are not interested about anything on disk
    if (input.running_mode != "tree")
    {
      out::stream where = out::PROCESS;
      std::string subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir();
      logTab(where,"Directory: "+FileSystem(DataBase(),{subdir,""}).fstr());
    }
    return job_has_been_run_previously;
  }

  void Phasertng::skipping()
  {
    out::stream where = out::LOGFILE;
    FileSystem FILENAME(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::LOGFILE_LOG));
    logTab(out::PROCESS,"Skipping...");
    logTab(where,"See " + FILENAME.qfstrq());
      //goto the_twilight_zone; //skipping unload reflecions
  }

  void
  Phasertng::cleanup_after_call_to_mode(bool job_has_been_run_previously)
  {
    if (input.running_mode == "chk") return;
    if (input.running_mode == "tree") return;

    //for the tree nodes we don't write the dag

    logSectionHeader(out::SUMMARY,"RESULTS");
    logTab(out::SUMMARY,"Dag size = " + itos(DAGDB.size()));
    if (DAGDB.NODES.top_tfz()) //looks at node.ZSCORE which may be rf or tf
    {
      (DAGDB.WORK->NEXT.PRESENT) ? //turn or gyre
        logTab(out::SUMMARY,"Dag top-RFZ = " + dtos(DAGDB.NODES.top_tfz(),2)):
        logTab(out::SUMMARY,"Dag top-TFZ = " + dtos(DAGDB.NODES.top_tfz(),2));
    }
    if (DAGDB.NODES.top_tfz_packs() and !DAGDB.WORK->NEXT.PRESENT)
    {
      logTab(out::SUMMARY,"Dag top-TFZ which packs or with unknown packing = " + dtos(DAGDB.NODES.top_tfz_packs(),2));
    }
    logBlank(out::SUMMARY);

    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      logUnderLine(where,"Suite Configuration");
      //this is the output that is general to all modes
      //the specialist data structures output by each mode are written internally to each mode
      logTab(where,"Suite: " + Suite::suite);
      logTab(where,"Kill file:   " + std::string(Suite::KillFile().size() ? Suite::KillFile() : "(None)"));
      logTab(where,"Kill time:   " + std::string((Suite::KillTime() > 0) ? dtos(Suite::KillTime(),2) : "(None)"));
      logTab(where,"Loggraphs:   " + btos(Suite::Loggraphs()));
      logTab(where,"Write files: " + btos(Suite::WriteFiles()));
      logTab(where,"Write data:  " + btos(Suite::WriteData()));
      logTab(where,"Write cards: " + btos(Suite::WriteCards()));
      logTab(where,"Write phil:  " + btos(Suite::WritePhil()));
      logTab(where,"Write log:   " + btos(Suite::WriteLog()));
      logTab(where,"Write xml:   " + btos(Suite::WriteXml()));
      logTab(where,"Level:       " + out::stream2String(Suite::Level()));
      logTab(where,"Store:       " + out::stream2String(Suite::Store()));
      logBlank(where);
    }

    //shift all the trackers to a new tracking number
    //the new penultimate one is inherited from the node it was derived from
    //DAGDB.NODES.shift(input.running_mode);
    // do the above for each run-job

    if (!job_has_been_run_previously)
       //needs to be done even if dagdb is empty
    {
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
      if (Suite::Extra() and DAGDB.size())
      {
        out::stream where = out::TESTING;
        logUnderLine(where,"Output Dag Nodes");
        auto text = DAGDB.unparse_card_summary();
        logTabArray(where,text);
        logBlank(where);
      }
#endif
      out::stream where = out::VERBOSE;
      logTab(where,"Number of Nodes = " + itos(DAGDB.size()));
      logTab(where,"Number of Parents = " + itos(DAGDB.set_of_parents().size()));
      logTab(where,"Number of GrandParents = " + itos(DAGDB.set_of_grandparents().size()));
      logTab(where,"Number of Model Entries = " + itos(DAGDB.ENTRIES.size()));
#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
      if (Suite::Extra())
      for (auto item : DAGDB.ENTRIES)
      {
        auto where = out::TESTING;
        auto entry = &item.second;
        logTab(where,"memory: " + item.first.str());
        logTab(where,"memory models " + btos(entry->MODELS.in_memory()));
        logTab(where,"memory fcalcs " + btos(entry->FCALCS.in_memory()));
        logTab(where,"memory imodels " + btos(entry->IMODELS.in_memory()));
        logTab(where,"memory ifcalcs " + btos(entry->IFCALCS.in_memory()));
        logTab(where,"memory decomposition " + btos(entry->DECOMPOSITION.in_memory()));
        logTab(where,"memory interpolation " + btos(entry->INTERPOLATION.in_memory()));
        logTab(where,"memory variance " + btos(entry->VARIANCE.in_memory()));
        logTab(where,"memory trace " + btos(entry->TRACE.in_memory()));
        logTab(where,"memory monostructure " + btos(entry->MONOSTRUCTURE.in_memory()));
        logTab(where,"memory substructure " + btos(entry->SUBSTRUCTURE.in_memory()));
        logTab(where,"memory coordinates " + btos(entry->COORDINATES.in_memory()));
        logTab(where,"memory density " + btos(entry->DENSITY.in_memory()));
        logTab(where,"memory gradient " + btos(entry->GRADIENT.in_memory()));
      }
#endif
      logTab(where,"Number of Data  Entries = " + itos(DAGDB.REFLIDS.size()));
      if (Write()) unload_dag(out::LOGFILE);
    }

    if (!job_has_been_run_previously)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Result Files");// + input.running_mode + "));
      Identifier id(DAGDB.PATHWAY.ULTIMATE);
      std::string subdir = id.database_subdir();
      std::string tngname = id.other_data_stem_ext(other::RESULT_CARDS);
      FileSystem resultcards(DataBase(),{subdir, tngname});
      logFileWritten(out::LOGFILE,WriteCards(),"Result Cards",resultcards);
      if (WriteCards())
      {
        //bool write_mode(false); //not for chaining
        //bool write_suite(true); //not for chaining
        //resultcards.write_array(cards(write_mode,write_suite));
        bool without_mode(true); //see ModeRunner.py
        resultcards.write(card_str(without_mode));
        input.value__path.at(".phasertng.notifications.result.subdir.").set_value(subdir);
        input.value__path.at(".phasertng.notifications.result.filename_in_subdir.").set_value(tngname);
      }

      if (Suite::called_from_executable)
      {
      FileSystem SCRIPT(DataBase(),{"","cards.script"});
      //write the file that we know where to go looking for scripting data
      //scripting is nested!
      //use @ <filestem.script> to read into executable
      logFileWritten(out::LOGFILE,WriteCards(),"Script",SCRIPT);
      if (WriteCards()) SCRIPT.write("@ \"" + resultcards.fstr() + "\"");
      }

      FileSystem resultphil(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::IO_EFF));
      logFileWritten(out::LOGFILE,WritePhil(),"Phil",resultphil);
      if (WritePhil()) resultphil.write(phil_str());

      //this file required for job restoration
      FileSystem Restoration(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::PATHWAY_CARDS));
      logFileWritten(out::LOGFILE,WriteCards(),"Restoration",Restoration);
      if (WriteCards()) Restoration.write(pathway_cards());
    }

    //now add the inner of all loops, the running mode loop, and write the file
    {
      out::stream where = out::PROCESS;
      std::string tmode = stoupper(input.running_mode) + "__";
      add_loopxml(input.header(),DAGDB.PATHWAY.ULTIMATE.database_subdir() + " Dag size=" + itos(DAGDB.size()));
      std::string xmlout = input.value__string.at(".phasertng.graph_report.filestem.").value_or_default() + ".xml";
      FileSystem XML(DataBase(),{"",xmlout});
      logFileWritten(where,WriteXml(),"Loop",XML);
      if (WriteXml()) XML.write(loopxml());
    }

    {{
    out::stream where = out::CONCISE;
    logUnderLine(where,"All Files Written");
    FILEOUT.size() ?
      logTab(where,"Files as written: $DAGDATABASE="+DataBase()):
      logTab(where,"No files written");
    for (auto item : FILEOUT)
      logTab(where,FileSystem(item).qfstrq());
    }}

    {{ //advisories
    out::stream where = out::CONCISE;
    logAdvisories(where);
    input.process_warnings(".phasertng.notifications.advisory.",advisories());
    }}

    {{ //warnings
    out::stream where = out::CONCISE;
    logWarnings(where);
    input.process_warnings(".phasertng.notifications.warning.",warnings());
    }}

#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      logUnderLine(where,"Result Keywords for Mode");
      std::set<std::string> keyset = input.result_keywords_for_mode({input.running_mode});
      for (auto item:keyset)
        logTab(where,"relevant keyword from master phil: "+item);
      if (!keyset.size()) logTab(where,"(None)");
      logBlank(where);
    }
#endif

    //we need to set the overwrite to the input because it could be different on the stored cards file
    //overwrite can be changed without changing hash
    {{
    double cpu = get_mode_cpus_seconds();
    double wall = get_mode_wall_seconds();
    input.value__flt_number.at(".phasertng.time.mode.cpu.").set_value(cpu);
    input.value__flt_number.at(".phasertng.time.mode.wall.").set_value(wall);
    double cpus = get_cumulative_cpus_seconds();
    double walls = get_cumulative_wall_seconds();
    input.value__flt_number.at(".phasertng.time.cumulative.cpu.").set_value(cpus);
    input.value__flt_number.at(".phasertng.time.cumulative.wall.").set_value(walls);
    }}

#ifdef PHASERTNG_DEBUG_INPUTOUTPUT
    if (Suite::Extra())
    {
      //auto keywords = input.result_keywords_for_mode(multi);
      bool write_mode(false);
      bool write_suite(false);
      auto keywords = input.result_keywords_for_mode({input.running_mode});
      keywords.erase("time");
      out::stream where = out::TESTING;
      logUnderLine(where,"Non-default Result Keywords");
      keywords.size() ?
        logTabArray(where,cards(write_mode,write_suite,keywords)):
        logTab(where,"(None)");
    }
#endif

    //if (WriteFiles()) WriteLog is internal
    if (WriteLog()) write_logfiles(job_has_been_run_previously); //also called if error thrown
    logTrailer(out::CONCISE);
  }

  void
  Phasertng::write_logfiles(bool job_has_been_run_previously)
  {
    if (input.running_mode == "chk") return;
    if (!DAGDB.PATHWAY.ULTIMATE.is_set_valid()) return;
    out::stream where = out::LOGFILE;
    logBlank(where);
    //fake trailer for adding to written files, not output at this point
    //timings in the trailer will be before the logfile write
    std::unordered_map<other::data,out::stream,other::EnumHash> lookup;
    if (Suite::Store() >= out::CONCISE) lookup[other::CONCISE_LOG] = out::CONCISE;
    if (Suite::Store() >= out::SUMMARY) lookup[other::SUMMARY_LOG] = out::SUMMARY;
    if (Suite::Store() >= out::LOGFILE) lookup[other::LOGFILE_LOG] = out::LOGFILE;
    if (Suite::Store() >= out::VERBOSE) lookup[other::VERBOSE_LOG] = out::VERBOSE;
    for (auto logfile : lookup)
    {
      FileSystem FILENAME(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(logfile.first));
      if (WriteLog() and !job_has_been_run_previously)
      {
        add_trailer(out::CONCISE);
        logFileWritten(where,true,"Output",FILENAME);
        FILENAME.write(Print(logfile.second));
        subtract_trailer();
      }
      else
      {
        if (job_has_been_run_previously)
          logTab(where,"Output: not overwritten " + FILENAME.qfstrq());
        else
          logFileWritten(where,false,"Output",FILENAME);
      }
      //now add the trailer to the output stream, stored for python extraction
    }
    if (LOGGRAPHS.size())
    {
      bool wf = (WriteLog() and !job_has_been_run_previously);
      FileSystem FILENAME(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::LOGGRAPHS_LOG));
      logFileWritten(where,wf,"Loggraph",FILENAME);
      if (wf)
      {
        af_string all_loggraphs;
        for (auto loggraph : LOGGRAPHS) //loggraph map by imode
          for (auto item : loggraph.second)
            all_loggraphs.push_back(item);
        FILENAME.write_array(all_loggraphs);
      }
    }
  }

  std::string
  Phasertng::setup_reflid_type(
      Identifier reflid,
      out::stream where1,
      out::stream where2)
  {
    logTab(where2,"Setup reflections");
    //set member here
    reflid_can_be_different = { "frf","frfr","gyre","ptf","pak","move","rmr","pose" };
    std::string reflections_initialize = "ok";
    std::set<std::string> SetOfReflNone = { "chk","test" }; //add to check keywords exclusions also
    for (auto item : input.python_modes())
      SetOfReflNone.insert(item);
    SetOfReflNone.erase("find");
    std::set<std::string> SetOfReflInit = { "imtz","ipdb","imap" };
    std::set<std::string> SetOfReflColsMap = { "data","sgx","ccs","rmr","mapz","rellg","feff","ptf","msm","ssp","emm","put","jog","pak","move","span","ssd","ssr","exp1","find" };
    std::set<std::string> SetOfReflRowsData = { "twin","rbm","eatm","emap","esm","ellg","perm","etfz","frf","srf","frfr","pose","fuse","brf","gyre","ftf","ftfr","btf","spr","rbgs","rbr","tfz","mcc" };
    std::set<std::string> SetOfReflRowsAnom = { "aniso","tncs","tncso","info","sga","cca","ssa","ssm" };
    //map for checking that there a no multiple entries
    std::vector<std::set<std::string> > MapOfReflType = {
       SetOfReflNone,
       SetOfReflInit,
       SetOfReflColsMap,
       SetOfReflRowsData,
       SetOfReflRowsAnom,
    };
    //check for not set
    bool found_reflection_type_for_mode(false);
    std::string invalid_mode;
    for (int i = 0; i < MapOfReflType.size(); i++)
      for (auto Mode1 : MapOfReflType[i])
      {
        if (Mode1 == input.running_mode)
          found_reflection_type_for_mode = true;
        else if (!input.value__choice.at(".phasertng.mode.").check_value(Mode1))
          invalid_mode = Mode1;
      }
    if (invalid_mode.size())
      throw Error(err::DEVELOPER,"mode invalid \"" + invalid_mode + "\" in Phasertng.cc");
    if (!found_reflection_type_for_mode)
      throw Error(err::DEVELOPER,"Reflection class typeid not defined for mode " + input.running_mode + ", add mode name to a definition set in Phasertng.cc ");
    //check for duplicates
    for (int i = 0; i < MapOfReflType.size(); i++)
      for (int j = i+1; j < MapOfReflType.size(); j++)
        for (auto Mode1 : MapOfReflType[i])
          for (auto Mode2 : MapOfReflType[j])
            if (Mode1 == Mode2)
              throw Error(err::DEVELOPER,"Reflection class typeid duplicated for task \"" + Mode1 + "\" in Phasertng.cc");

    if (REFLECTIONS and REFLECTIONS.get()) //no typeid for nullptr
    {
      auto& r = *REFLECTIONS.get();
      std::string ReflType = typeid(r).name();
      logTab(where2,"Start Reflections typeid: " + ReflType);
    }
    else
    {
      logTab(where2,"Start Reflections typeid: None");
    }
    if (SetOfReflNone.count(input.running_mode))
    {
      logTab(where1,"Reflections: None");
      reflections_initialize = "none";
    }
    else if (SetOfReflInit.count(input.running_mode))
    {
      //don't load_reflid_to_base, the mode will do the setup
      REFLCOLSMAP = ReflColsMap();
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      //shared pointer copy,don't delete when the reference count goes to zero
      logTab(where1,"Reflections: initialize");
      reflections_initialize = "initialized";
    }
    else if (!reflid.is_set_valid()) //before we try to load anything
    {
      //set the reflections to base, even though not useful
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      logTab(where1,"Reflections: no valid identifier");
      reflections_initialize = "invalid";
    }
    else if (SetOfReflColsMap.count(input.running_mode))
    {
      std::string ReflType; phaser_assert(reflections_initialize == "ok"); //paranoia for reinit internally
      std::tie(ReflType,reflections_initialize) = load_reflid_to_base(reflid,where1,where2);
      if (ReflType != typeid(ReflColsMap).name())
      {
        REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
        //shared pointer copy,don't delete when the reference count goes to zero
        //REFLECTIONS.reset(new ReflColsMap()); //this would be a copy
        //REFLECTIONS->copy_and_reformat_data(&REFLCOLSMAP); //this would be a copy
        logTab(where2,"Reflections: shared base type");
      }
    }
    else if (SetOfReflRowsData.count(input.running_mode))
    {
      std::string ReflType; phaser_assert(reflections_initialize == "ok"); //paranoia for reinit internally
      std::tie(ReflType,reflections_initialize) = load_reflid_to_base(reflid,where1,where2);
      if (ReflType != typeid(ReflRowsData).name())
      {
        REFLECTIONS.reset(new ReflRowsData());
        REFLECTIONS->copy_and_reformat_data(&REFLCOLSMAP);
        logTab(where2,"Reflections: copy base and reformat");
        reflections_initialize = "reformatted";
      }
    }
    else if (SetOfReflRowsAnom.count(input.running_mode))
    {
      std::string ReflType; phaser_assert(reflections_initialize == "ok"); //paranoia for reinit internally
      std::tie(ReflType,reflections_initialize) = load_reflid_to_base(reflid,where1,where2);
      if (ReflType != typeid(ReflRowsAnom).name())
      {
        REFLECTIONS.reset(new ReflRowsAnom());
        REFLECTIONS->copy_and_reformat_data(&REFLCOLSMAP);
        logTab(where2,"Reflections: copy base and reformat");
        reflections_initialize = "reformatted";
      }
    }
    if (REFLECTIONS and REFLECTIONS.get())
    {
      auto& r = *REFLECTIONS.get();
      std::string ReflType = typeid(r).name();
      logTab(where2,"Final Reflections typeid: " + ReflType);
    }
    else
    {
      logTab(where2,"Final Reflections typeid: None");
    }
    logBlank(where2);
    return reflections_initialize;
  }

  bool
  Phasertng::load_reflid_for_node(Identifier reflid,out::stream where)
  {
    PHASER_ASSERT(reflid_can_be_different.size());
    PHASER_ASSERT(reflid_can_be_different.count(input.running_mode));
    if (REFLCOLSMAP.REFLID == reflid)
      return false;
    REFLECTIONS = nullptr;// to force reset of REFLECTIONS
    setup_reflid_type(reflid,out::VERBOSE,out::VERBOSE);
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    return true;
  }

  std::pair<std::string,std::string>
  Phasertng::load_reflid_to_base(Identifier reflid,out::stream where1,out::stream where2)
  {
    std::string reflections_initialize = "ok"; //important! same as above, so don't have to pass
    logTab(where2,"Reflections:");
    if (!reflid.is_set_valid())
      throw Error(err::INPUT,"Reflections identifier and tag not set in dag or input");
    logTab(where1,"Current Reflections Identifier: " + REFLCOLSMAP.reflid().str());
    logTab(where1,"Request Reflections Identifier: " + reflid.str());
    if (REFLCOLSMAP.NREFL > 0 and REFLCOLSMAP.reflid() == reflid)
    {
      logTab(where2,"Reflections in memory " + REFLCOLSMAP.reflid().str());
      if (input.value__boolean.at(".phasertng.reflid.force_read.").value_or_default())
      {
        DogTag dogtag = DAGDB.lookup_reflid(reflid);
        logTab(where1,"Force Read " + dogtag.str_str());
        REFLCOLSMAP.SetFileSystem(DataBase(),dogtag.FileOther(other::DATA_MTZ));
        logTab(where2,"(Reflections on disk: " + REFLCOLSMAP.fstr() + ")");
        std::fstream infile;
        infile.open(REFLCOLSMAP.fstr().c_str(),std::fstream::in);
        if (!infile.is_open())
           throw Error(err::FILEOPEN,REFLCOLSMAP.fstr());
        af_string warning = REFLCOLSMAP.VanillaReadMtz(
            {} //default {} all columns, set by function overload below
               //as we can't use default values in declaration for std::set
        );
        //we have to reset the reflections also,
        // or else the change of reflections is not picked up by setup_reflid_type
        REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
        if (warning.size())
          logTabArray(where1,warning);
      }
    }
    else
    {
      if (REFLCOLSMAP.NREFL == 0)
        logTab(where1,"Reflections not in memory");
      else if (REFLCOLSMAP.reflid() != reflid)
      {
        logTab(where1,"Load Reflections Identifier: " + reflid.str());
        logTab(where1,"Change of reflections required");
      }
      DogTag dogtag = DAGDB.lookup_reflections(reflid,DataBase());
      logTab(where1,"Read " + dogtag.str_str());
      REFLCOLSMAP.SetFileSystem(DataBase(),dogtag.FileOther(other::DATA_MTZ));
      logTab(where2,"(Reflections on disk: " + REFLCOLSMAP.qfstrq() + ")");
      std::fstream infile;
      infile.open(REFLCOLSMAP.fstr().c_str(),std::fstream::in);
      if (!infile.is_open())
         throw Error(err::FILEOPEN,REFLCOLSMAP.fstr());
      af_string warning = REFLCOLSMAP.VanillaReadMtz(
          {} //default {} all columns, set by function overload below
             //as we can't use default values in declaration for std::set
      );
      //we have to reset the reflections also,
      // or else the change of reflections is not picked up by setup_reflid_type
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      if (warning.size())
        logTabArray(where1,warning);
      reflections_initialize = "changed";
    }
    logTab(where2,"Number of reflections:  " + itos(REFLCOLSMAP.NREFL));
    if (REFLECTIONS) //no typeid for nullptr
    { //here if the reflections have been maintained in memory, otherwise REFLECTIONS == nullptr
      if (REFLECTIONS.get())
      {
        auto& r = *REFLECTIONS.get();
        logTab(where2,"Reflections typeid: " + std::string(typeid(r).name()));
      }
    }
    logTab(where2,"Loaded");
    if (REFLECTIONS and REFLECTIONS.get()) //no typeid for nullptr
    {
      auto& r = *REFLECTIONS.get();
      std::string ReflType = typeid(r).name();
      return { ReflType, reflections_initialize };
    }
    return { "", reflections_initialize };
  }

/*
  void
  Phasertng::load_reflections(
        out::stream where,
        bool force_read
      )
  { load_reflections(where,{},force_read); }
*/

  void
  Phasertng::load_dag(std::string text)
  { //wrapper for python access
    //make sure it is clear if calling from python
    DAGDB.SetFileSystem("text");
    DAGDB.parse_cards(text);
    DAGDB.restart(); //this is very important because it sets the WORK pointer
  }

  void
  Phasertng::load_dag_file(std::string dagfile_)
  { //wrapper for python access
    //make sure it is clear if calling from python
    DAGDB.SetFileSystem(dagfile_);
    DAGDB.parse_card();
  }

  void
  Phasertng::unload_dag(out::stream where)
  {
    if (!DAGDB.PATHWAY.ULTIMATE.is_set_valid()) return;
    logUnderLine(where,"Dag Files");
    input.value__path.at(".phasertng.dag.cards.subdir.").set_default();
    input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").set_default();
    Identifier id(DAGDB.PATHWAY.ULTIMATE);
    auto subdir =  id.database_subdir();
    auto dagname = id.other_data_stem_ext(other::DAG_CARDS);
    DAGDB.SetFileSystem(DataBase(),{subdir,dagname});
    logFileWritten(where,WriteCards(),"Directed Acyclic Graph Cards", DAGDB);
    if (WriteCards())
    {
      DAGDB.unparse_card();
      input.value__path.at(".phasertng.dag.cards.subdir.").set_value(subdir);
      input.value__path.at(".phasertng.dag.cards.filename_in_subdir.").set_value(dagname);
    }
    //these are the cookies for the graph analysis
    FileSystem daghtml(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::DAG_HTML));
    logFileWritten(where,WriteXml(),"Directed Acyclic Graph Html", daghtml);
    if (WriteXml())
    {
      DAGDB.unparse_html(daghtml);
    }

    input.value__path.at(".phasertng.dag.phil.filename.").set_default();
    FileSystem dagphil(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileOther(other::DAG_PHIL));
    logFileWritten(where,WritePhil(),"Directed Acyclic Graph Phil",dagphil);
    if (WritePhil())
    {
      DAGDB.unparse_phil(dagphil);
      input.value__path.at(".phasertng.dag.phil.filename.").set_value(dagphil.fstr());
    }
    input.value__int_vector.at(".phasertng.dag.counters.").set_value(
         ivect3(DAGDB.size(),DAGDB.set_of_parents().size(),DAGDB.set_of_grandparents().size()));
  }

  void //python!
  Phasertng::append_parse_cards(std::string cards)
  { //call from python!
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Append dag nodes");
    bool check_io(Write());
    if (check_io)
    { //from python, this may not have been set on new basetng object
      //due to bad Phasertng object tracking
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Database");
      logTab(where,"database: " + DataBase());
      FileSystem dbdir(DataBase(),{"",""});
      dbdir.prepare_and_check_database();
      if (DataBase() != dbdir.fstr()) //full path expansion
        logTab(where,"database: " + dbdir.fstr());
      logTab(where,"database: OK");
    }
    logTab(where,"Initial dag statistics");
    logTab(where,"Number of Nodes = " + itos(DAGDB.size()));
    logTab(where,"Number of Parents = " + itos(DAGDB.set_of_parents().size()));
    logTab(where,"Number of GrandParents = " + itos(DAGDB.set_of_grandparents().size()));
    logTab(where,"Number of Model Entries = " + itos(DAGDB.ENTRIES.size()));
    logTab(where,"Number of Data  Entries = " + itos(DAGDB.REFLIDS.size()));
    if (DAGDB.NODES.size() and cards.size() and cards.find(NODE_SEPARATOR()) != 0)
      cards += NODE_SEPARATOR() + "\n";
    //we also have to patch up the pathway information
    auto oldway = DAGDB.PATHWAY;
    auto oldlog = DAGDB.PATHLOG;
    DAGDB.append_parse_cards(cards);
    DAGDB.PATHWAY = oldway;
    DAGDB.PATHLOG = oldlog;
    int len = std::min(static_cast<int>(cards.size()-1),100);
    DAGDB.PATHWAY.shift_edge(cards.substr(0,len),"append");
    DAGDB.PATHLOG.shift_edge(cards.substr(0,len),"append");
    logTab(where,"Final dag statistics");
    logTab(where,"Number of Nodes = " + itos(DAGDB.size()));
    logTab(where,"Number of Parents = " + itos(DAGDB.set_of_parents().size()));
    logTab(where,"Number of GrandParents = " + itos(DAGDB.set_of_grandparents().size()));
    logBlank(where);
    logTab(where,"Appended Pathway: " + DAGDB.PATHWAY.str());
    logTab(where,"Appended Pathlog: " + DAGDB.PATHLOG.str());
    logTab(where,"Number of Model Entries = " + itos(DAGDB.ENTRIES.size()));
    logTab(where,"Number of Data  Entries = " + itos(DAGDB.REFLIDS.size()));
    //the unloading must be called from Phasertng
    if (Write()) unload_dag(where);
  }

  std::string
  Phasertng::phil_str()
  {
    bool phil(true);
    return input.unparse(show::VALUE,phil);
  }

  af_string
  Phasertng::cards(bool write_mode,bool write_suite,std::set<std::string> keywords)
  {
    return cards_show(write_mode,write_suite,keywords,show::VALUE); //original
  }

  af_string
  Phasertng::cards_show(bool write_mode,bool write_suite,std::set<std::string> keywords,show::type SHOWTYPE)
  {
    //write the output phil file in cards text format
    bool default_all = true;
    af_string ccp4_cards;
    if (!default_all && !keywords.size()) return ccp4_cards;
    bool phil_format(false);
    std::string text = input.unparse(SHOWTYPE,phil_format);
    sv_string lines;
    hoist::algorithm::split(lines, text, ("\n"));
    for (auto& item : lines)
    {
      hoist::algorithm::trim(item);
      bool is_mode = (item.find(PHIL_SCOPE()+" mode ")==0 or item.find(PHIL_SCOPE()+".mode")==0); // != model
      bool is_suite = (item.find(PHIL_SCOPE()+" suite ")==0 or item.find(PHIL_SCOPE()+".suite ")==0);
      if ((write_mode and is_mode) or
          (write_suite and is_suite) or
          (!is_mode and !is_suite))
      {
        if (!keywords.size()) //true of no keywords set
        {
          ccp4_cards.push_back(item);
        }
        else if (phil_format)
        {
          for (auto keyword : keywords) //not by reference
          {
            hoist::algorithm::trim(keyword);
            keyword = PHIL_SCOPE() + "." + keyword + "."; //final space to avoid eg tncs tncs_analysis
            if (!item.find(keyword)) //found at the start
            {
              ccp4_cards.push_back(item);
              break; //only insert the keyword once
            }
          }
        }
        else
        {
          for (auto keyword : keywords) //not by reference
          {
            hoist::replace_all(keyword,SCOPE_SEPARATOR(),std::string(" "));
            hoist::algorithm::trim(keyword);
            keyword = PHIL_SCOPE() + " " + keyword + " "; //final space to avoid eg tncs tncs_analysis
            if (!item.find(keyword)) //found at the start
            {
              ccp4_cards.push_back(item);
              break; //only insert the keyword once
            }
          }
        }
      }
    }
    return ccp4_cards;
  }

  std::string
  Phasertng::card_str(bool without_mode)
  {
    return input.card_str(without_mode);
  }

  af_string
  Phasertng::get_filenames(af_string dot_ext_list)
  {
    //helper function for python
    af_string result;
    for (auto ext : dot_ext_list)
    {
      for (auto fileout : FILEOUT)
      {
        std::string filename = FileSystem(fileout).fstr(); //without the DATABASE substitution
        if (hoist::algorithm::ends_with(filename,ext))
          result.push_back(filename);
      }
    }
    return result;
  }

  void
  Phasertng::clear_filenames()
  { FILEOUT.clear(); }

  void Phasertng::set_user_input_for_suite(const InputCard& input_)
  {
    //input_ so that parameters picked up in check keywords
    set_defaults_for_suite(); //since user_value is only if there is a change from defaults
    try {
      if (!input_.value__choice.at(".phasertng.suite.level.").is_default())
        Suite::SetLevel( input_.value__choice.at(".phasertng.suite.level.").value_or_default());
      if (!input_.value__choice.at(".phasertng.suite.store.").is_default())
        Suite::SetStore( input_.value__choice.at(".phasertng.suite.store.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_phil.").is_default())
        Suite::SetWritePhil( input_.value__boolean.at(".phasertng.suite.write_phil.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_cards.").is_default())
        Suite::SetWriteCards( input_.value__boolean.at(".phasertng.suite.write_cards.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_files.").is_default())
        Suite::SetWriteFiles( input_.value__boolean.at(".phasertng.suite.write_files.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_data.").is_default())
        Suite::SetWriteData( input_.value__boolean.at(".phasertng.suite.write_data.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_density.").is_default())
        Suite::SetWriteData( input_.value__boolean.at(".phasertng.suite.write_density.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_log.").is_default())
        Suite::SetWriteLog( input_.value__boolean.at(".phasertng.suite.write_log.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.write_xml.").is_default())
        Suite::SetWriteXml( input_.value__boolean.at(".phasertng.suite.write_xml.").value_or_default());
      if (!input_.value__path.at(".phasertng.suite.kill_file.").is_default())
        Suite::SetKillFile( input_.value__path.at(".phasertng.suite.kill_file.").value_or_default());
      if (!input_.value__flt_number.at(".phasertng.suite.kill_time.").is_default())
        Suite::SetKillTime( input_.value__flt_number.at(".phasertng.suite.kill_time.").value_or_default());
      if (!input_.value__boolean.at(".phasertng.suite.loggraphs.").is_default())
        Suite::SetLoggraphs( input_.value__boolean.at(".phasertng.suite.loggraphs.").value_or_default());
      if (!input_.value__path.at(".phasertng.suite.database.").is_default())
        Suite::SetDataBase( input_.value__path.at(".phasertng.suite.database.").value_or_default());
    }
    catch (const std::out_of_range& oor)
    { std::cerr << oor.what() << std::endl; }
    if (!WriteFiles()) SetSilent();
    Suite::check_store_level();
  }

  bool
  Phasertng::load_entry_from_database(
      out::stream where,
      entry::data entry_data_enum,
      Identifier identifier,
      bool optional) //if optional, error will not be thrown if not present in database
  {
    std::string entry_data_str = entry::data2String(entry_data_enum);
    if (!identifier.is_set_valid())
    {
      std::string msg = "Invalid identifier for load entry";
      throw Error(err::INPUT,msg);
    }
    auto entry = DAGDB.lookup_entry_tag(identifier,DataBase());
    logTab(where,"Entry ID: " + entry->DOGTAG.str_str());
    EntryBase* filled;
    //could use std::map<entry::data,EntryBase*> and create new here
         if (entry_data_enum == entry::MODELS_PDB)        filled = &entry->MODELS;
    else if (entry_data_enum == entry::FCALCS_MTZ)        filled = &entry->FCALCS;
    else if (entry_data_enum == entry::IMODELS_PDB)       filled = &entry->IMODELS;
    else if (entry_data_enum == entry::IFCALCS_MTZ)       filled = &entry->IFCALCS;
    else if (entry_data_enum == entry::DECOMPOSITION_MTZ) filled = &entry->DECOMPOSITION;
    else if (entry_data_enum == entry::INTERPOLATION_MTZ) filled = &entry->INTERPOLATION;
    else if (entry_data_enum == entry::VARIANCE_MTZ)      filled = &entry->VARIANCE;
    else if (entry_data_enum == entry::TRACE_PDB)         filled = &entry->TRACE;
    else if (entry_data_enum == entry::MONOSTRUCTURE_PDB) filled = &entry->MONOSTRUCTURE;
    else if (entry_data_enum == entry::SUBSTRUCTURE_PDB)  filled = &entry->SUBSTRUCTURE;
    else if (entry_data_enum == entry::COORDINATES_PDB)   filled = &entry->COORDINATES;
    else if (entry_data_enum == entry::DENSITY_MTZ)       filled = &entry->DENSITY;
    else if (entry_data_enum == entry::GRADIENT_MTZ)      filled = &entry->GRADIENT;
    else
    {
      throw Error(err::DEVELOPER,"Unknown load enumeration");
    }
    filled->SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry_data_enum));
    bool ok(false); //should only be returned if failure to load and optional
    if (filled->in_memory())
    {
      logTab(where,"Entry " + entry_data_str + " in memory");
      ok = true; //true in that entry is in database
    }
    else
    {
      if (!filled->filesystem_is_set())
      {
        throw Error(err::FILESET,"dag database entry " + entry_data_str);
      }
      logTab(where,"Entry file: " + filled->qfstrq());
      if (!optional and !filled->file_exists()) //errors not thrown from deeper in code without -g option, core dump
      {
        throw Error(err::FILEOPEN,filled->qfstrq());
      }
      if (filled->file_exists())
      {
        try {
          filled->read_from_disk(); //fileopen errors thrown here
          //very important to reset the pointers to the entries in each of the nodes
        }
        catch (Error& err)
        { //this may core dump instead if not in debug mode
          logTab(where,"Entry file not available/readable");
          if (!optional) throw; //original error at original location
        }
      }
      DAGDB.reset_entry_pointers(DataBase());
      ok = true;
    }
    //special case, make the RBIN array in the interpolation match the reflection array loaded
    if (ok and entry_data_enum == entry::INTERPOLATION_MTZ)
    { //this stores the SigmaA squared per reflid reflection
      //for fast lookup in all likelihood calculations
      auto ssqr_array = REFLCOLSMAP.get_data_array(labin::SSQR);
      entry->INTERPOLATION.setup_sigmaa_sqr(ssqr_array);
    }
    if (ok and entry_data_enum == entry::VARIANCE_MTZ) //ellg
    { //this stores the SigmaA squared per reflid reflection
      //for fast lookup in all likelihood calculations
      auto ssqr_array = REFLCOLSMAP.get_data_array(labin::SSQR);
      entry->VARIANCE.setup_sigmaa_sqr(ssqr_array);
    }
    return ok;
  }

  std::string
  Phasertng::pathway_cards()
  {
    std::string ccp4_cards;
    bool write_mode(false);
    bool write_suite(true); //keep the current settings
    //the cards that we want to keep this run through
    std::set<std::string> keyset = input.result_keywords_for_mode({input.running_mode});
    //now carry the others that we may need later
//   std::set<std::string> ikeyset = input.input_keywords_for_mode(input.parameter<type::choice>(".phasertng.mode.")->all_choices());
//    ikeyset.erase(input.running_mode); //but not this mode
//    keyset.insert(ikeyset.begin(), ikeyset.end());
    keyset.erase("notifcations");
    keyset.erase("overwrite");
    keyset.erase("dag"); //because we are not interested in the filename, data could be stored DAGDB, use the dag itself
    keyset.erase("time");
    keyset.erase("threads");
    keyset.erase("labin"); //shared io AJM TODO needs separate io
    auto lines = cards(write_mode,write_suite,keyset);
    for (auto& line : lines)
    {
      //this removes the lines that are defaults with no values
      //defaults that set the value are retained
      //eg mactncs protocol off/on
      hoist::algorithm::trim(line);
      if (!hoist::algorithm::ends_with(line," None") and !hoist::algorithm::contains(line," None "))
        ccp4_cards += line + "\n";
    }
#ifdef PHASERTNG_DEBUG_HASHING
    logTab(out::LOGFILE,"Pathway string: \"" + ccp4_cards + "\"");
#endif
    return ccp4_cards;
  }

  void
  Phasertng::write_reflid(DogTag reflid)
  {
   //writes the mtz file
   //write the cookie in the database directory for lookup
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Database Entries");
    REFLCOLSMAP.store_new_and_changed_data(REFLECTIONS.get());
    REFLCOLSMAP.SetFileSystem(DataBase(),reflid.FileOther(other::DATA_MTZ));
    logFileWritten(out::LOGFILE,WriteData(),"Reflections (" + input.running_mode + ")",REFLCOLSMAP);
    if (WriteData())
    {
      REFLCOLSMAP.write_to_disk();
      if (WriteCards())
      { //this puts the file in the database directory where it can be read from the reflid alone
        FileSystem filesystem;
        filesystem.SetFileSystem(DataBase(),reflid.identify().FileDataBase(other::ENTRY_CARDS));
        logFileWritten(out::LOGFILE,WriteCards(),"Reflid cards",filesystem);
        auto dagcards = DAGDB.reflid_unparse_card(DAGDB.REFLIDS[reflid.identify()]);
        if (WriteCards()) filesystem.write(dagcards);
      }
    }
    else { DAGDB.REFLIDS.erase(reflid.IDENTIFIER); }
    DAGDB.NODES.set_reflid(reflid.IDENTIFIER);
  }

  std::string
  Phasertng::hashing()
  {
    std::string ccp4_cards;
    bool phil_format(false);
    bool write_mode(false);
    bool write_suite(false);
    std::set<std::string> keyset = input.input_keywords_for_mode({input.running_mode});
    keyset.erase("notifications");
    keyset.erase("overwrite");
    keyset.erase("dag"); //because we are not interested in the filename, data could be stored DAGDB, use the dag itself
    keyset.erase("time");
    keyset.erase("threads");
    keyset.erase("labin"); //shared io AJM TODO needs separate io
    keyset.erase("entry"); //because we are not interested in the filename
    auto lines = cards_show(write_mode,write_suite,keyset,show::VALUEORDEFAULT);
    for (auto& line : lines)
    {
      hoist::algorithm::trim(line);
      if (!hoist::algorithm::ends_with(line," None") and !hoist::algorithm::contains(line," None "))
        ccp4_cards += line + "\n";
    }
    //we have to add some information from the database here because the run also depend on this!!!
    //don't use the pathway information, since how we got here is not important
    ccp4_cards += "#" + std::to_string(DAGDB.size()) + "#";
    if (DAGDB.size())
    {
      auto& nhead = DAGDB.NODES.front().TRACKER;
      ccp4_cards += "Pose:";
      for (auto& pose : DAGDB.NODES.front().POSE)
        ccp4_cards += pose.IDENTIFIER.string() + "+";
      ccp4_cards.pop_back();
      ccp4_cards += "/Seek:";
      for (auto& seek : DAGDB.NODES.front().SEEK)
        ccp4_cards += seek.IDENTIFIER.string() + "+";
      ccp4_cards.pop_back();
      ccp4_cards += "/";
      ccp4_cards += nhead.ULTIMATE.str() + nhead.PENULTIMATE.str();
      //and the end in case someone truncates the database, probably only need size though
      auto& ntail = DAGDB.NODES.back().TRACKER;
      ccp4_cards += ntail.ULTIMATE.str() + ntail.PENULTIMATE.str();
    }
    //add the running mode too, in case the keywords refer to a different (boring) mode e.g no keywords at all
    ccp4_cards += input.running_mode;
    ccp4_cards += REFLCOLSMAP.SG.HALL;;
    auto hist = REFLCOLSMAP.get_history();
    for (auto item : hist)
      ccp4_cards += item;
#ifdef PHASERTNG_DEBUG_HASHING
    logTab(out::LOGFILE,"Hashing string: \n\"" + ccp4_cards + "\"");
#endif
    return ccp4_cards;
  }

  std::string
  Phasertng::hashing_extra()
  {
    boost::hash<std::string> string_hash;
    size_t hash_value = string_hash(hashing()); //return type for boost hash
    return std::to_string(hash_value); //because hashing will not parse directly
  }

  pod::moderunner
  Phasertng::get_moderunner()
  {
    pod::moderunner result;
    result.pathway_string = DAGDB.PATHWAY.ULTIMATE.string();
    result.pathway_tag = DAGDB.PATHWAY.ULTIMATE.tag();
    result.pathlog_string = DAGDB.PATHLOG.ULTIMATE.string();
    result.pathlog_tag = DAGDB.PATHLOG.ULTIMATE.tag();
    result.subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir();
    result.size = DAGDB.size();
    result.top_tfz = DAGDB.NODES.top_tfz();
    result.polarity = DAGDB.polarity();
    result.number_of_poses = DAGDB.NODES.number_of_poses();
    if (REFLECTIONS)
    {
      result.tncs_info = REFLECTIONS->get_tncsinfo();
      result.sg_info = REFLECTIONS->get_crystal();
      result.ccs = REFLECTIONS->Z;
    }
    if (WriteCards()) //changeling, in memory running
    {
      result.dag_cards_filename = input.get_as_str(".phasertng.dag.cards.filename_in_subdir.");
      result.tng_cards_filename = input.get_as_str(".phasertng.notifications.result.filename_in_subdir."); //ignore tng:input
    }
    else
    {
      bool without_mode(true);
      result.tngcards = card_str(without_mode); //possibly large
      result.dagcards = DAGDB.cards(); //possibly huge string in python sizes
      //it is important to also set these to empty because tngcards can be empty anyway
      result.dag_cards_filename = "";
      result.tng_cards_filename = "";
    }
    return result;
  }

  void
  Phasertng::load_moderunner(pod::moderunner result,std::string user_cards,std::string dryrun_cards)
  {
    //called from python
    std::string subdir = result.subdir;
    hoist::replace_first(subdir,"\"","");
    hoist::replace_last(subdir,"\"","");
    std::string dagname = result.dag_cards_filename;
    hoist::replace_first(dagname,"\"","");
    hoist::replace_last(dagname,"\"","");
    std::string tngname = result.tng_cards_filename;
    hoist::replace_first(tngname,"\"","");
    hoist::replace_last(tngname,"\"","");
    if (dagname.size() > 0)
    {
      DAGDB.set_filesystem(DataBase(),subdir,dagname);
      DAGDB.parse_card(); //using filesystem internal
      DAGDB.restart(); //this is very important because it sets the WORK pointer
      input.set_as_str(".phasertng.dag.cards.subdir.",subdir);
      input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",dagname);
    }
    else
    {
      DAGDB.parse_cards(result.dagcards);
      DAGDB.restart(); //this is very important because it sets the WORK pointer
    }
    if (tngname.size() > 0)
    {
      FileSystem fs(DataBase(),subdir,tngname);
      std::ifstream infile(fs.fstr().c_str(), std::ios::in | std::ios::binary);
      std::stringstream buffer;
      buffer << infile.rdbuf();  // Read file content into the stringstream
      std::string tngcards = buffer.str();
      infile.close();
      input = InputCard();
      bool ignore_unknown_keywords = false;
      input.parse_multi(tngcards,user_cards,dryrun_cards,ignore_unknown_keywords);
      input.set_as_str(".phasertng.notifications.result.subdir.",subdir);
      input.set_as_str(".phasertng.notifications.result.filename_in_subdir.",tngname);
    }
    else
    {
      input = InputCard();
      bool ignore_unknown_keywords = false;
      input.parse_multi(result.tngcards,user_cards,dryrun_cards,ignore_unknown_keywords);
    }
  }

  void
  Phasertng::load_cards(pod::moderunner result)
  {
    input.set_as_str(".phasertng.dag.cards.subdir.",result.subdir);
    input.set_as_str(".phasertng.dag.cards.filename_in_subdir.",result.dag_cards_filename);
    std::string subdir = result.subdir;
    hoist::replace_first(subdir,"\"","");
    hoist::replace_last(subdir,"\"","");
    std::string dagname = result.dag_cards_filename;
    hoist::replace_first(dagname,"\"","");
    hoist::replace_last(dagname,"\"","");
    std::string tngname = result.tng_cards_filename;
    hoist::replace_first(tngname,"\"","");
    hoist::replace_last(tngname,"\"","");
    DAGDB.set_filesystem(DataBase(),subdir,dagname);
    DAGDB.parse_card(); //using filesystem internal
    DAGDB.restart(); //this is very important because it sets the WORK pointer
  }

  LatticeTranslocation
  Phasertng::apply_interpolation(out::stream where,interp::point interpolation,std::vector<type::flt_numbers> iltds)
  {
    DAGDB.apply_interpolation(interpolation);
    LatticeTranslocation LT;
    if (iltds.size())
    {
      LT.parse(iltds); //separate because it throws, not constructor
      if (LT.HAS_LTD)
      {
        DAGDB.apply_interpolation(interpolation,LT);
        logTab(where,"Entries have Lattice Translocation Disorder applied");
        for (int ltd = 0; ltd < LT.DISORDER.size(); ltd++)
          logTab(where,"#" + itos(ltd) + //note that index 0 is the 0 0 0, so report as #0
              " displacement=[" + dvtos(LT.DISORDER[ltd].first,6,4) + "]" +
              " occupancy=" + dtos(LT.DISORDER[ltd].second,2));
      }
    }
    else
      logTab(where,"No Lattice Translocation Disorder applied");
    return LT; //after processing we can reuse this
  }

} //phasertng
