//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <iotbx/pdb/hierarchy.h>
#include <iotbx/pdb/input.h>
#include <ncs/EnsembleSymmetry.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/math/slope_and_intercept.h>
#include <phasertng/src/ConsistentDLuz.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <scitbx/misc/split_lines.h>

namespace phasertng {

  //Ensemble_preparation
  void Phasertng::runEMM()
  {
#include <phasertng/run/runECA.cpp>
#include <phasertng/run/runEBA.cpp>
#include <phasertng/run/runEWA.cpp>
#include <phasertng/run/runENS.cpp>
  }

} //phasertng
