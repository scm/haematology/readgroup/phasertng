//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <ccp4_errno.h>
#include <library_file.h>
#include <phasertng/io/MtzHandler.h>
#include <list>
//#include <boost/sort/sort.hpp> //AJM BOOST 1.70

namespace phasertng {

  //Read_mtz_file
  void Phasertng::runIMTZ()
  {
    if (DAGDB.size() >= 2)
    throw Error(err::INPUT,"Dag must contain none or one nodes");

    logUnderLine(out::LOGFILE,"Data Preparation");

    FileSystem HKLIN;
    if (input.value__path.at(".phasertng.hklin.filename.").is_default())
    {
      if (!DAGDB.size())
        throw Error(err::FILESET,"hklin filename");
      auto reflid = DAGDB.NODES.front().REFLID;
      if (!reflid.is_set_valid())
        throw Error(err::FILESET,"hklin filename");
      auto dogtag = DAGDB.lookup_reflid(reflid);
      logTab(out::LOGFILE,"Current Reflections Identifier: " + reflid.str());
      HKLIN.SetFileSystem(DataBase(),dogtag.FileOther(other::DATA_MTZ));
    }
    else
    {
      HKLIN.SetFileSystem(input.value__path.at(".phasertng.hklin.filename.").value_or_default());
    }
    logTab(out::LOGFILE,"Data read from mtz file: " + HKLIN.qfstrq());

    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      auto labels = input.labin(true);
      logTab(where,"Labels:" + itos(labels.size()));
      for (auto item : labels)
      {
        logTab(where,item.parameter() + "/" + item.name() + "/default=" + btos(item.is_not_set()));
      }
    }

    {//memory
    if (!HKLIN.file_exists())
      throw Error(err::FILEOPEN,HKLIN.fstr());
    if (HKLIN.is_a_directory())
      throw Error(err::FILEOPEN,HKLIN.fstr());
    int read_reflections(1);
    MtzHandler mtzIn;
    mtzIn.read(HKLIN,read_reflections);

    //read the history and setup up parsing of cards to see what is there already
    logTabArray(out::VERBOSE,mtzIn.HISTORY);
    //we want to reset column data or cyclic write-read fails
    //keep other bits of the header that might store e.g. oversampling e.g. em_placement
    af_string edited;
    for (auto line : mtzIn.HISTORY)
      //most important to remove the labin keywords since these are maps and not overwritten
      if (!hoist::algorithm::contains(line,REFLECTIONS->remark_labin) and
          !hoist::algorithm::contains(line,REFLECTIONS->remark_identifier) and
          !hoist::algorithm::contains(line,REFLECTIONS->remark_preparation) and
          !hoist::algorithm::contains(line,REFLECTIONS->remark_data))
      edited.push_back(line);
    mtzIn.HISTORY = edited;
    REFLECTIONS->SetHistory(mtzIn.HISTORY);

    if (Suite::Extra())
      logTabArray(out::TESTING,mtzIn.XtalSetColInfo());

    //find defaults depending on what used to be mode, now has to be something else
    //always read non-anomalous data
    {{
    out::stream where = out::VERBOSE;
    logTab(where,"Set user/default I/F");
    std::string inat = //default is I(+)
      input.value__mtzcol.at(".phasertng.labin.inat.").name();
    std::string siginat =
      input.value__mtzcol.at(".phasertng.labin.siginat.").name();
    std::string fnat =
      input.value__mtzcol.at(".phasertng.labin.fnat.").name();
    std::string sigfnat =
      input.value__mtzcol.at(".phasertng.labin.sigfnat.").name();
    logTab(where,"default/user intensity=" + inat + "/" + siginat);
    logTab(where,"default/user amplitude=" + fnat + "/" + sigfnat);
    mtzIn.find_nonanomalous_data(fnat,sigfnat,inat,siginat);
    if (mtzIn.Ilabel.size())
    { //intensity takes precedence
    //set_value to the default makes the return is_not_set() false
      input.value__mtzcol.at(".phasertng.labin.inat.").set_value(mtzIn.Ilabel);
      if (!mtzIn.SIGIlabel.size())
        throw Error(err::INPUT,"Data provided as intensities (I) without sigmas (SIGI)");
      input.value__mtzcol.at(".phasertng.labin.siginat.").set_value(mtzIn.SIGIlabel);
      //clear any input because amplitudes must not read in preference to intensities
      //(also amplitudes may not have been found)
      input.value__mtzcol.at(".phasertng.labin.fnat.").set_default();
      input.value__mtzcol.at(".phasertng.labin.sigfnat.").set_default();
    }
    else if (mtzIn.Flabel.size())
    { //don't read the f's if i's are present, they may be FW-amplitudes
      input.value__mtzcol.at(".phasertng.labin.fnat.").set_value(mtzIn.Flabel);
      if (mtzIn.SIGFlabel.size())
        input.value__mtzcol.at(".phasertng.labin.sigfnat.").set_value(mtzIn.SIGFlabel);
      //clear the intensities because because not found
      input.value__mtzcol.at(".phasertng.labin.inat.").set_default();
      input.value__mtzcol.at(".phasertng.labin.siginat.").set_default();
    }
    }}

    //only read the map if a map is requested
    if (input.value__boolean.at(".phasertng.reflections.read_map.").value_or_default())
    if (mtzIn.MapCount) // and therefore it is worth looking for one
    {
      out::stream where = out::VERBOSE;
      logTab(where,"Set user/default FMAP");
      std::string mapf =
        input.value__mtzcol.at(".phasertng.labin.mapf.").name();
      std::string mapph =
        input.value__mtzcol.at(".phasertng.labin.mapph.").name();
      std::string mapdobs =
        input.value__mtzcol.at(".phasertng.labin.mapdobs.").name();
      logTab(where,"default/user map=" + mapf + "/" + mapph + "/" + mapdobs);
      mtzIn.find_map(mapf);
      if (mtzIn.Flabel.size())
      { //read the map in anyway
        input.value__mtzcol.at(".phasertng.labin.mapf.").set_value(mtzIn.Flabel);
        input.value__mtzcol.at(".phasertng.labin.mapph.").set_value(mtzIn.Plabel);
        if (mtzIn.Wlabel.size())
          input.value__mtzcol.at(".phasertng.labin.mapdobs.").set_value(mtzIn.Wlabel);
        //clear the amplitudes and intensities because mapf outranks both (has a phase)
        input.value__mtzcol.at(".phasertng.labin.fnat.").set_default();
        input.value__mtzcol.at(".phasertng.labin.sigfnat.").set_default();
        input.value__mtzcol.at(".phasertng.labin.inat.").set_default();
        input.value__mtzcol.at(".phasertng.labin.siginat.").set_default();
      }
      else //clear the input values because they weren't found
      {
        input.value__mtzcol.at(".phasertng.labin.mapf.").set_default();
        input.value__mtzcol.at(".phasertng.labin.mapph.").set_default();
        input.value__mtzcol.at(".phasertng.labin.mapdobs.").set_default();
      }
      //If there are more than one I or F on the file it can't decide which to use
      //force the user to choose
      //If it is a map only then the default can go through
    }

    std::list<bool> read_anomalous_loop = { false, true };
    if (input.value__boolean.at(".phasertng.reflections.read_anomalous.").value_or_default())
      read_anomalous_loop.pop_front();
    for (auto this_read_anomalous_loop : read_anomalous_loop)
    {
      if (this_read_anomalous_loop) //try again even if the user has not set this
      {
        out::stream where = out::VERBOSE;
        std::string ipos =
          input.value__mtzcol.at(".phasertng.labin.ipos.").name();
        std::string sigipos =
          input.value__mtzcol.at(".phasertng.labin.sigipos.").name();
        std::string ineg =
          input.value__mtzcol.at(".phasertng.labin.ineg.").name();
        std::string sigineg =
          input.value__mtzcol.at(".phasertng.labin.sigineg.").name();
        std::string fpos =
          input.value__mtzcol.at(".phasertng.labin.fpos.").name();
        std::string sigfpos =
          input.value__mtzcol.at(".phasertng.labin.sigfpos.").name();
        std::string fneg =
          input.value__mtzcol.at(".phasertng.labin.fneg.").name();
        std::string sigfneg =
          input.value__mtzcol.at(".phasertng.labin.sigfneg.").name();
        logTab(where,"default/user intensity=" + ipos + "/" + sigipos + "/" + ineg + "/" + sigineg);
        logTab(where,"default/user amplitude=" + fpos + "/" + sigfpos + "/" + fneg + "/" + sigfneg);
        mtzIn.find_anomalous_data(fpos,sigfpos,ipos,sigipos,
                                  fneg,sigfneg,ineg,sigineg);
        logTab(where,"#I(+) columns = " + itos(mtzIn.Icount));
        logTab(where,"#F(+) columns = " + itos(mtzIn.Fcount));
        bool Ivalid(mtzIn.ISIGIlabel[0].size() and mtzIn.ISIGIlabel[2].size()); //not the sigmas
        bool Fvalid(mtzIn.FSIGFlabel[0].size() and mtzIn.FSIGFlabel[2].size()); //not the sigmas
        logTab(where,"found I(+) user/default = " + btos(Ivalid));
        logTab(where,"found F(+) user/default = " + btos(Fvalid));
        if (Ivalid)
        { //set to the first entry in the list
          //set_value to the default makes the return is_not_set false
          logTab(where,"Set user/default IPOS/SIGFPOS/INEG/SIGINEG");
          input.value__mtzcol.at(".phasertng.labin.ipos.").set_value(mtzIn.ISIGIlabel[0]);
          if (!mtzIn.ISIGIlabel[1].size())
            throw Error(err::INPUT,"Data provided as intensities (I(+)) without sigmas (SIGI(+))");
          input.value__mtzcol.at(".phasertng.labin.sigipos.").set_value(mtzIn.ISIGIlabel[1]);
          input.value__mtzcol.at(".phasertng.labin.ineg.").set_value(mtzIn.ISIGIlabel[2]);
          if (!mtzIn.ISIGIlabel[3].size())
            throw Error(err::INPUT,"Data provided as intensities (I(-)) without sigmas (SIGI(-))");
          input.value__mtzcol.at(".phasertng.labin.sigineg.").set_value(mtzIn.ISIGIlabel[3]);
          //clear the amplitides because we have intensities
          input.value__mtzcol.at(".phasertng.labin.fpos.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigfpos.").set_default();
          input.value__mtzcol.at(".phasertng.labin.fneg.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigfneg.").set_default();
          //delete the mean data, as it must match the anomalous data, so generate below
          input.value__mtzcol.at(".phasertng.labin.fnat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigfnat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.inat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.siginat.").set_default();
          //delete the map data because we have asked for anomalous data
          input.value__mtzcol.at(".phasertng.labin.mapf.").set_default();
          input.value__mtzcol.at(".phasertng.labin.mapph.").set_default();
          input.value__mtzcol.at(".phasertng.labin.mapdobs.").set_default();
        }
        else if (Fvalid)
        { //don't read the f's if i's are present, they may be FW-amplitudes
          //set to the first entry in the list
          //set_value to the default makes the return is_not_set false
          logTab(where,"Set user/default FPOS/SIGFPOS/FNEG/SIGFNEG");
          input.value__mtzcol.at(".phasertng.labin.fpos.").set_value(mtzIn.FSIGFlabel[0]);
          if (mtzIn.FSIGFlabel[1].size())
            input.value__mtzcol.at(".phasertng.labin.sigfpos.").set_value(mtzIn.FSIGFlabel[1]);
          input.value__mtzcol.at(".phasertng.labin.fneg.").set_value(mtzIn.FSIGFlabel[2]);
          if (mtzIn.FSIGFlabel[3].size())
            input.value__mtzcol.at(".phasertng.labin.sigfneg.").set_value(mtzIn.FSIGFlabel[3]);
          //the intensities were not found, so clear
          input.value__mtzcol.at(".phasertng.labin.ipos.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigipos.").set_default();
          input.value__mtzcol.at(".phasertng.labin.ineg.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigineg.").set_default();
          //delete the mean data, as it must match the anomalous data, so generate below
          input.value__mtzcol.at(".phasertng.labin.fnat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.sigfnat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.inat.").set_default();
          input.value__mtzcol.at(".phasertng.labin.siginat.").set_default();
          //delete the map data because we have asked for anomalous data
          input.value__mtzcol.at(".phasertng.labin.mapf.").set_default();
          input.value__mtzcol.at(".phasertng.labin.mapph.").set_default();
          input.value__mtzcol.at(".phasertng.labin.mapdobs.").set_default();
        }
        //anomalous data, even if requested is not compulsory
        //result of attempt to find data is returned on data_anomalous flags below
        if (mtzIn.Icount > 1 or mtzIn.Fcount > 1)
        {
          logAdvisory("Multiple anomalous datasets");
        }
      }

      {{//Free R flag reading, if present
      out::stream where = out::VERBOSE;
      logTab(where,"Set user/default FreeR_flag");
      std::string freer = "";
      if (!input.value__mtzcol.at(".phasertng.labin.free.").is_not_set())
        freer = input.value__mtzcol.at(".phasertng.labin.free.").name();
      mtzIn.find_freer(freer);
      if (mtzIn.Fcount == 0)
        logAdvisory("Free R flag column not found");
      else if (mtzIn.FREElabel.size()) //default or selected
        input.value__mtzcol.at(".phasertng.labin.free.").set_value(mtzIn.FREElabel);
      else if (mtzIn.Fcount > 1)
        logAdvisory("Multiple Free R flag datasets");
      }}

      //report the change in labins after all the analysis
      {{
      out::stream where = out::LOGFILE;
      auto labels = input.labin(false);
      logBlank(where);
      std::string header = this_read_anomalous_loop ? "Anomalous" : "Non-anomalous";
      logUnderLine(where,"Mtz Columns: " + header);
      for (auto item : labels)
      { //labels are in order of enumeration
        logTab(where,snprintftos(
            "labin %7s = %-7s",
            item.labin_col().c_str(),
            item.name().c_str()));
      }
      if (labels.size() == 0)
        logTab(where,"None");
      logBlank(where);
      }}

      {{
      sv_bool final_read_cols = {
          input.value__mtzcol.at(".phasertng.labin.fnat.").is_not_set(),
          input.value__mtzcol.at(".phasertng.labin.inat.").is_not_set(),
          input.value__mtzcol.at(".phasertng.labin.mapf.").is_not_set(),
          input.value__mtzcol.at(".phasertng.labin.fpos.").is_not_set(),
          input.value__mtzcol.at(".phasertng.labin.ipos.").is_not_set()
         };
      if (std::count(final_read_cols.begin(),final_read_cols.end(),false) > 1)
      { //paranoia check, only one read
        throw Error(err::DEVELOPER,"Labin for read is not unique");
      }
      if (std::count(final_read_cols.begin(),final_read_cols.end(),false) == 0)
      {
        if (this_read_anomalous_loop) //second and final attempt
          throw Error(err::INPUT,"No default data F/I/FMAP/FPOS/IPOS set, specify columns in input");
      }
      else
      {
        break; //out of at the first attempt of this_read_anomalous_loop
      }
      }}
    }//default backup read_anomalous

  // UNITCELL,WAVELENGTH,SPACEGROUP
    {{
    out::stream where = out::VERBOSE;
    std::string CELLCOL="";
    {
       //look to ANOMALOUS data first to get the cell and wavelength!
           if (!input.value__mtzcol.at(".phasertng.labin.ipos.").is_not_set())
        CELLCOL = input.value__mtzcol.at(".phasertng.labin.ipos.").name();
      else if (!input.value__mtzcol.at(".phasertng.labin.fpos.").is_not_set())
        CELLCOL = input.value__mtzcol.at(".phasertng.labin.fpos.").name();
      else if (!input.value__mtzcol.at(".phasertng.labin.inat.").is_not_set())
        CELLCOL = input.value__mtzcol.at(".phasertng.labin.inat.").name();
      else if (!input.value__mtzcol.at(".phasertng.labin.fnat.").is_not_set())
        CELLCOL = input.value__mtzcol.at(".phasertng.labin.fnat.").name();
      else if (!input.value__mtzcol.at(".phasertng.labin.mapf.").is_not_set())
        CELLCOL = input.value__mtzcol.at(".phasertng.labin.mapf.").name();
      if (!CELLCOL.size()) throw Error(err::INPUT,"No reference data for unit cell");
    }
    logTab(where,"Reference column for unit cell: " + CELLCOL);
    logTab(where,"Reference column for wavelength : " + CELLCOL);
    {
      mtzIn.load_dataset(CELLCOL);
      REFLECTIONS->UC = mtzIn.UC;
      REFLECTIONS->WAVELENGTH = mtzIn.WAVELENGTH;
    }
    where = out::LOGFILE;
    logTab(where,"Mtz Wavelength: " + dtos(REFLECTIONS->WAVELENGTH));
    if (!input.value__flt_number.at(".phasertng.reflections.wavelength.").is_default() )
    {
      double wavelength = input.value__flt_number.at(".phasertng.reflections.wavelength.").value_or_default();
      logTab(where,"Input Wavelength: " + dtos(wavelength));
      if (REFLECTIONS->WAVELENGTH and wavelength != REFLECTIONS->WAVELENGTH)
        logWarning("Wavelength in mtz file and user input are different " + dtos(REFLECTIONS->WAVELENGTH) + " cf " + dtos(wavelength));
      REFLECTIONS->WAVELENGTH = wavelength;
      input.value__flt_number.at(".phasertng.reflections.wavelength.").set_value(REFLECTIONS->WAVELENGTH);
    }
    input.value__flt_number.at(".phasertng.reflections.wavelength.").set_value(REFLECTIONS->WAVELENGTH);
    {
      REFLECTIONS->SG = mtzIn.SG;
      REFLECTIONS->iSG = mtzIn.SG;
    }
    }}

    {
      double oversampling = input.value__flt_number.at(".phasertng.reflections.oversampling.").value_or_default();
      if (!input.value__flt_number.at(".phasertng.reflections.oversampling.").is_default())
      {
        if (oversampling != REFLECTIONS->OVERSAMPLING)
        {
          out::stream where = out::LOGFILE;
          logTab(where,"Mtz Oversampling: " + dtos(REFLECTIONS->OVERSAMPLING));
          logTab(where,"Input Oversampling: " + dtos(oversampling));
          logWarning("Oversampling in mtz file and user input are different " + dtos(REFLECTIONS->OVERSAMPLING) + " cf " + dtos(oversampling));
        }
        REFLECTIONS->OVERSAMPLING = oversampling;
      }
      //if the oversampling has come from the mtz file, make sure this difference is recorded in phil
      input.value__flt_number.at(".phasertng.reflections.oversampling.").set_value(REFLECTIONS->OVERSAMPLING);
    }
    {
      dvect3 mapori = input.value__flt_vector.at(".phasertng.reflections.map_origin.").value_or_default();
      if (!input.value__flt_vector.at(".phasertng.reflections.map_origin.").is_default())
      {
        if (REFLECTIONS->MAPORI != mapori)
        {
          out::stream where = out::LOGFILE;
          logTab(where,"Mtz origin relative to original map: " + dvtoss(REFLECTIONS->MAPORI,3));
          logTab(where,"Input origin                       : " + dvtoss(mapori,3));
          logWarning("Map origin in mtz file and user input are different");
        }
        REFLECTIONS->MAPORI = mapori;
      }
      //if the mapori has come from the mtz file, make sure this difference is recorded in phil
      input.value__flt_vector.at(".phasertng.reflections.map_origin.").set_value(REFLECTIONS->MAPORI);
    }

    logTab(out::LOGFILE,"Space Group:  " + REFLECTIONS->SG.CCP4);
    logTab(out::LOGFILE,"Unit Cell:    " + dvtos(REFLECTIONS->UC.cctbxUC.parameters()));
    logTab(out::LOGFILE,"Wavelength:   " + dtos(REFLECTIONS->WAVELENGTH));
    PHASER_ASSERT(REFLECTIONS->OVERSAMPLING);
    logTab(out::LOGFILE,"Oversampling: " + dtos(REFLECTIONS->OVERSAMPLING));
    logTab(out::LOGFILE,"Map origin:   " + dvtoss(REFLECTIONS->MAPORI,3));

    {{
  // DATA
    mtzIn.resetRefl();

    int ncols = 3; //hkl
    auto labels = input.labin(false);
    for (auto item : labels)
    {
      ncols++;
    }
    PHASER_ASSERT(ncols > 3);
    af::versa<bool, af::c_grid<2> > bad_friedel(af::c_grid<2>(mtzIn.NREFL,friedel::typeCount));
    std::vector<float> adata_(ncols);
    float* adata = &*adata_.begin();
    sv_int logmss_(ncols);
    int* logmss = &*logmss_.begin();
    std::vector<CMtz::MTZCOL*> lookup_(ncols);
    CMtz::MTZCOL** lookup = &*lookup_.begin();
    int n(0);
    std::map<labin::col,int> column; //all REFLECTIONS-> not just the set ones
    sv_string hkl = {"H","K","L"};
    for (int i = 0; i < hkl.size(); i++)
    {
      lookup[n] = mtzIn.getCol(hkl[i]);
      column[labin::col2Enum(hkl[i])] = n; //these are special, will need for knowing which to read
      n++;
    }
    for (auto item : labels)
    {
      lookup[n] = mtzIn.getCol(item.name().c_str());
      column[item.enumeration()] = n; //these are special, will need for knowing which to read
      std::string A(item.type_str()),B(1,lookup[n]->type[0]);
      if (A != B)
      throw Error(err::INPUT,"Mtz column type for " + item.labin_col() + " not \'" + A + "\' (\'" + B + "\')");
      n++;
    }

    //setup all that is possible on first pass through mtz file
    sv_bool readMtzRefl(mtzIn.NREFL,true);
    PHASER_ASSERT(mtzIn.NREFL);
    { //memory
      double MTZ_HIRES = std::numeric_limits<double>::max();
      double MTZ_LORES = -std::numeric_limits<double>::max();
      double READ_RESO = input.value__flt_number.at(".phasertng.reflections.resolution.").value_or_default();
      bool trigger_resolution_exclusion(false);
      bool trigger_f000_exclusion(false);
      bool trigger_negative_or_zero_sigi(false);
      bool trigger_negative_sigf(false);
      bool trigger_negative_amplitudes(false);
      //these are the columns that are checkable on input
      //must be positive/nonzero
      std::set<labin::col> sigicols = { labin::SIGINAT, labin::SIGIPOS, labin::SIGINEG };
      std::set<labin::col> sigfcols = { labin::SIGFNAT, labin::SIGFPOS, labin::SIGFNEG };
      std::set<labin::col> fcols =    { labin::FNAT, labin::FPOS, labin::FNEG };
      for (int mtzr = 0; mtzr < mtzIn.NREFL ; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H(adata[0]);
        int K(adata[1]);
        int L(adata[2]);
        millnx HKL(H,K,L);
        double resolution = REFLECTIONS->UC.cctbxUC.d(HKL);
        MTZ_HIRES = std::min(resolution,MTZ_HIRES);
        MTZ_LORES = std::max(resolution,MTZ_LORES);
        bool is_all_nan_or_bad = true;
        bool one_bad = false;
        bad_friedel(mtzr,0) = bad_friedel(mtzr,1) = bad_friedel(mtzr,2) = false;
        for (int n = 3; n < ncols; n++)
        {
          bool read_this_col = !mtz_isnan(adata[n]);
          //THIS READS REFLECTIONS FOR WHICH **ANY** DATA ARE !NAN
          //do not read negative or zero sigma intensity data
          //e.g. if there are low numbers of significiant figures
          //because this can't be handled in the DOBS calculation
          //Warn if these are found
          labin::col col = labels[n-3].enumeration();
          if (read_this_col and sigicols.count(col) and adata[n] <= 0)
          {
            read_this_col = false;
            one_bad = true;
            trigger_negative_or_zero_sigi = true;
          }
          if (read_this_col and sigfcols.count(col) and adata[n] < 0)
          {
            read_this_col = false;
            one_bad = true;
            trigger_negative_sigf = true;
          }
          if (read_this_col and fcols.count(col) and adata[n] < 0)
          {
            read_this_col = false;
            one_bad = true;
            trigger_negative_amplitudes = true;
          }
          if (read_this_col) is_all_nan_or_bad = false;
          friedel::type f = REFLECTIONS->convert_labin_to_friedel(col);
          int intf = static_cast<int>(f);
          bad_friedel(mtzr,intf) = bad_friedel(mtzr,intf) or !read_this_col;
        }
        if (one_bad and Suite::Extra())
        {
          std::string line = "Bad: " + ivtos(HKL) + " ";
          for (int n = 3; n < ncols; n++)
            line += labin::col2String(labels[n-3].enumeration()) + " " + dtos(adata[n]) + " ";
          logTab(out::TESTING,line);
        }
        if (is_all_nan_or_bad)
        {
          readMtzRefl[mtzr] = false;
        }
        if (resolution < READ_RESO) //after bad
        {
          readMtzRefl[mtzr] = false;
          trigger_resolution_exclusion = true;
        }
        //not f000/F000
        if (H == 0 and K == 0 && L == 0) //after bad
        {
          readMtzRefl[mtzr] = false;
          trigger_f000_exclusion = true;
        }
      }
      input.value__flt_paired.at(".phasertng.data.resolution_available.").set_value(
          { MTZ_HIRES, MTZ_LORES });
      input.value__flt_cell.at(".phasertng.data.unitcell.").set_value(
           REFLECTIONS->UC.cctbxUC.parameters());
      if (trigger_negative_or_zero_sigi)
        logWarning("Reflections excluded from mtz file by negative or zero sigmas for intensities");
      if (trigger_negative_sigf)
        logWarning("Reflections excluded from mtz file by negative sigmas for amplitudes");
      if (trigger_negative_amplitudes)
        logWarning("Reflections excluded from mtz file by negative amplitudes");
      if (trigger_resolution_exclusion)
        logWarning("Reflections excluded from mtz file by resolution");
      if (trigger_f000_exclusion)
        logWarning("Reflection with Miller index [0,0,0] excluded from mtz file");
    } //memory

    int NREFL(0);
    for (int mtzr = 0; mtzr < readMtzRefl.size(); mtzr++)
    {
      if (readMtzRefl[mtzr])
      {
        NREFL++;
      }
    }
    PHASER_ASSERT(NREFL > 0);
    input.value__int_number.at(".phasertng.data.number_of_reflections.").set_value(NREFL);
    REFLECTIONS->setup_miller(NREFL);
    for (auto item : labels)
    {
      if (!item.is_not_set())
      {
        REFLECTIONS->setup_mtzcol(item);
      }
    }

    logEllipsisOpen(out::LOGFILE,"Reading Mtz File");
    {//memory
      //reset position in file
      mtzIn.resetRefl();

    //read the miller indices
      int r(0);
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        if (readMtzRefl[mtzr]) //must call MtzReadRefl before deciding
        {
          int H(adata[column[labin::H]]);
          int K(adata[column[labin::K]]);
          int L(adata[column[labin::L]]);
          millnx HKL(H,K,L);
          REFLECTIONS->set_miller(HKL,r);
          int n(3);
          for (auto item : labels)
          {
            if (!item.is_not_set())
            {
              bool is_BLN(item.type() == 'B'); //currently no boolean read
              bool is_INT(item.type() == 'I');
              bool is_FLT(!is_BLN and !is_INT);
              labin::col col = item.enumeration();
              friedel::type f = REFLECTIONS->convert_labin_to_friedel(col);
              if (is_FLT) REFLECTIONS->set_flt(col,0.,r);
              if (is_INT) REFLECTIONS->set_int(col,0,r); //free is an integer
              if (is_BLN) REFLECTIONS->set_bln(col,false,r); //not used
              REFLECTIONS->set_present(f,false,r); //flagged not present
              if (!mtz_isnan(adata[n]) and !bad_friedel(mtzr,f))
              {
                if (is_FLT) REFLECTIONS->set_flt(col,static_cast<double>(adata[n]),r);
                if (is_INT) REFLECTIONS->set_int(col,static_cast<int>(adata[n]),r); //free is an integer
                if (is_BLN) REFLECTIONS->set_bln(col,static_cast<bool>(adata[n]),r); //not used
                REFLECTIONS->set_present(f,true,r);
              }
              n++;
            }
          }
          r++;
        }
      }
    }//memory
    logEllipsisShut(out::LOGFILE);
    }}

    logTab(out::LOGFILE,"Number of reflections read/mtz: " + itos(REFLECTIONS->NREFL) + "/" + itos(mtzIn.NREFL));
    for (auto f : { friedel::NAT,friedel::POS,friedel::NEG} )
    {
      if (REFLECTIONS->has_friedel(f))
      {
        auto v = REFLECTIONS->get_present_array(f);
        int num = std::count(v.begin(), v.end(), true);
        logTab(out::LOGFILE,"Number of reflections present/read: " + itos(num) + "/" + itos(REFLECTIONS->NREFL));
      }
    }
    REFLECTIONS->set_data_resolution();
    }//memory

    {{
    REFLECTIONS->MAP = false;
    if (!input.value__mtzcol.at(".phasertng.labin.mapf.").is_not_set())
      REFLECTIONS->MAP = true;
    }}
    {{
    REFLECTIONS->EVALUES =
      input.value__boolean.at(".phasertng.reflections.evalues.").value_or_default();
    }}
    {{
    REFLECTIONS->INTENSITIES = false;
    if (!input.value__mtzcol.at(".phasertng.labin.inat.").is_not_set())
      REFLECTIONS->INTENSITIES = true;
    if (!input.value__mtzcol.at(".phasertng.labin.ipos.").is_not_set())
      REFLECTIONS->INTENSITIES = true;
    }}
    {{
    REFLECTIONS->ANOMALOUS = false;
    if (!input.value__mtzcol.at(".phasertng.labin.ipos.").is_not_set() or
        !input.value__mtzcol.at(".phasertng.labin.fpos.").is_not_set())
    {
      //if the data are not really anomalous (+ and - the same)
      //then return anomalous true (for processing to NAT)
      //warn user but don't do anything else
      //run check_anomalous again where anomalous signal is required (later)
      REFLECTIONS->ANOMALOUS = true;
      if (REFLECTIONS->check_native())
        logWarning("Anomalous data has no anomalous signal");
    }
    }}

    REFLECTIONS->set_data_resolution();

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Data Read Summary");
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->MAP));
    if (REFLECTIONS->MAP)
    {
      logTab(where,"Data provided as a map");
      //don't throw warning here, throw in data mode
    }
    input.value__boolean.at(".phasertng.data.map.").set_value(bool(REFLECTIONS->MAP));
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->ANOMALOUS));
    (REFLECTIONS->ANOMALOUS) ?
      logTab(where,"Anomalous data have been provided") :
      logTab(where,"Non-anomalous data have been provided");
    input.value__boolean.at(".phasertng.data.anomalous.").set_value(bool(REFLECTIONS->ANOMALOUS));
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->INTENSITIES));
    (REFLECTIONS->INTENSITIES) ?
      logTab(where,"Data provided as intensities") :
      logTab(where,"Data provided as amplitudes");
    //don't throw warning here, throw in data mode
    input.value__boolean.at(".phasertng.data.intensities.").set_value(bool(REFLECTIONS->INTENSITIES));
    for (auto f : REFLECTIONS->FRIEDEL)
    {
      if (f != friedel::UNDEFINED)
      {
        logTab(where,"Friedel type provided: " + friedel::type2String(f));
        PHASER_ASSERT(REFLECTIONS->has_friedel(f));
      }
    }
    }}

    if (REFLECTIONS->WAVELENGTH == 0)
    {
      if (REFLECTIONS->ANOMALOUS)
        if (input.value__boolean.at(".phasertng.reflections.read_anomalous.").value_or_default())
          throw Error(err::INPUT,"Wavelength not set for anomalous data");
  //only throw if the user needs anomalous data, not if just going to be non-anomalous in the end (mr from +/-)
      REFLECTIONS->WAVELENGTH = DEF_WAVELENGTH; //else
    }

    //initialize the dag with first node if not set on input
    //create the reflid from the node tracker
    DogTag reflid;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Reflection Identifier");
    //this is a bit messed up for coding because initialize the reflid
    //from the tracker and not from the pathway or tag or something
    if (DAGDB.size() == 0)
    {
      DAGDB.initialize_first_node_from_hash_tag(hashing(),input.running_mode);
    }
    std::string tag;
    if (!input.value__string.at(".phasertng.hklin.tag.").is_default())
    {
      tag = input.value__string.at(".phasertng.hklin.tag.").value_or_default();
      logTab(where,"Tag from input");
    }
    else
    {
      tag = HKLIN.stem(); //default
      logTab(where,"Tag from filename stem");
    }
    reflid =  DAGDB.initialize_reflection_identifier(tag);
    if (reflid.shortened)
      logTab(where,"Filename too long for tag, tag modified from " + tag);
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::INIT);
    }}

    //set the reflection values on the node
    dag::Node& node = DAGDB.NODES.front();
    node.CELL = REFLECTIONS->UC.cctbxUC;
    node.HALL = REFLECTIONS->SG.HALL;
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->MAP));
    node.SG = nullptr;
    node.REFLID = reflid.identify();

    logTabArray(out::VERBOSE,REFLECTIONS->logReflections("Data"));
    if (Suite::Extra()) //mtzdump is expensive
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Data"));

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"History");
    logBlank(where);
    logTabArray(where,REFLECTIONS->get_history());
    }}

    PHASER_ASSERT(REFLECTIONS->NREFL > 0); //check this is the type
    PHASER_ASSERT(REFLCOLSMAP.NREFL > 0); //check this is the type
    //don't shift, just initialize above
    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
