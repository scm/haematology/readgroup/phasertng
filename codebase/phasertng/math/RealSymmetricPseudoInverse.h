//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
//
// Find the pseudo-inverse of a real, symmetric matrix A.
// See https://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_inverse
//
// Doesn't quite copmute the pseudo-inverse as it 'filters' (i.e. set to zero)
// small and negative eigen values before copmuting the inverse.

#ifndef __phasertng_real_symmetric_pseudo_inverse__
#define __phasertng_real_symmetric_pseudo_inverse__
#include <scitbx/array_family/versa_matrix.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <scitbx/matrix/eigensystem.h>
#include <cmath>  // for std::sqrt
#include <limits> // for std::numeric_limits<double>::epsilon()
#include <phasertng/main/Assert.h>


using namespace scitbx::af;
// af is 'array family', a cctbx set of boostable data structures
// documentation is pretty sparse, when trying to use it one typically looks at the source code in
// cctbx_project/scitbx/array_family/, especially for constructors,
// as well as searching for other places in phenix that the code has been used.
// In this function we use versa, c_grid, flex_grid, matrix_multiply, matrix_transpose

namespace phasertng {
namespace math
{

inline versa<double, flex_grid<> >
RealSymmetricPseudoInverse(
    const versa<double, flex_grid<> > A_,   // Real symmetric matrix to find pseudo inverse of.
    int& filtered,                // Number of eigen values that were too small and hence were filtered before computation of the pseudo-inverse, set by this function.
    const int min_to_filter = 0 ) // Minimum number of eigen values to filter out before computing the inverse.
{
  const int N(A_.accessor().all()[0]); // The number of rows of A_ (and hence cols because square matrix)
  versa<double, c_grid<2> > A(A_.deep_copy(), c_grid<2>(N,N)); // Convert accessor to c_grid, deep_copy to avoid changing the original when scaling

  // Scale diagonals to 1, to be undone at the end. This makes eigenvalue filtering scale invariant.
  std::vector<double> A_scale(N);
  for (int i = 0; i < N; i++)
  {
    phaser_assert(A(i,i) > 0);
    A_scale[i] = 1/std::sqrt(A(i,i));
  }
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      A(i,j) = A_scale[i]*A_scale[j]*A(i,j);

  // Call the scitbx routine to find eigenvalues and eigenvectors
  scitbx::matrix::eigensystem::real_symmetric<double> eigen(A.ref());

  // Set condition number for range of eigenvalues,
  const double evCondition(std::min(1.e9,0.01/std::numeric_limits<double>::epsilon()));
  const double evMax(eigen.values()[0]);   // (Eigen values are stored in descending order.)
  const double evMin = evMax/evCondition;
  const double ZERO(0.);
  versa<double, c_grid<2> > lambdaInv(c_grid<2>(N,N), ZERO);

  filtered = 0;
  for (int i = N-1; i >= 0; i--) // going backwards is the least awkward way I could find to do this
  {
    if ( eigen.values()[i] < evMin || filtered < min_to_filter )
    {
      lambdaInv(i,i) = ZERO;
      filtered++;
    }
    else
      lambdaInv(i,i) = 1. / eigen.values()[i];
  }

  // Compute the pseudo inverse
  // The formula from wikipedia is (A^-1) = (Q)(lam^-1)(Q^T)
  // where Q has eigen vectors as columns
  // eigen.vectors() has eigenvectors as rows
  // let V be eigen.vectors(). The formula is now: (A^-1) = (V^T)(lam^-1)(V)

  versa<double, c_grid<2> > lambdaInvEigen = matrix_multiply( lambdaInv.ref(), eigen.vectors().ref() );
  versa<double, c_grid<2> > eigenT = matrix_transpose( eigen.vectors().ref() );
  versa<double, c_grid<2> > C = matrix_multiply( eigenT.ref(), lambdaInvEigen.ref() );
  versa<double, flex_grid<> > inverse_matrix( C , C.accessor().as_flex_grid() );

  // Rescale the matrix
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      inverse_matrix(i,j) = A_scale[i]*A_scale[j]*inverse_matrix(i,j);

  ////////////////////////////////////////////// debug stuff ///////////////////////////////////////////////////
  //#define __DEBUG_PSEUDOINVERSE__
  #ifdef __DEBUG_PSEUDOINVERSE__
  // Check that pseudoinverse obeys Moore-Penrose conditions
  // (Golub & van Loan, p. 257, see also the wikipedia page)
  versa<double, c_grid<2> > testmat = matrix_multiply( matrix_multiply(A.ref(), C.ref()).ref(), A.ref());
  double maxdev(0),maxelement(0);
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      maxelement = std::max(maxelement,std::abs(A(i,j)));
      maxdev = std::max(maxdev,std::abs(A(i,j)-testmat(i,j)));
    }
  std::cout << "Moore-Penrose condition 1, maximum relative error: " << maxdev/maxelement << "\n";
  testmat = matrix_multiply( matrix_multiply( C.ref(), A.ref()).ref(), C.ref());
  maxdev = 0.; maxelement = 0;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      maxelement = std::max(maxelement,std::abs(C(i,j)));
      maxdev = std::max(maxdev,std::abs(C(i,j)-testmat(i,j)));
    }
  std::cout << "Moore-Penrose condition 2, maximum relative error: " << maxdev/maxelement << "\n";
  testmat = matrix_multiply(A.ref(), C.ref());
  maxdev = 0.; maxelement = 0;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      maxelement = std::max(maxelement,std::abs(testmat(i,j)));
      maxdev = std::max(maxdev,std::abs(testmat(i,j)-testmat(j,i)));
    }
  std::cout << "Moore-Penrose condition 3, maximum relative error: " << maxdev/maxelement << "\n";
  testmat = matrix_multiply(C.ref(), A.ref());
  maxdev = 0.; maxelement = 0;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
    {
      maxelement = std::max(maxelement,std::abs(testmat(i,j)));
      maxdev = std::max(maxdev,std::abs(testmat(i,j)-testmat(j,i)));
    }
  std::cout << "Moore-Penrose condition 4, maximum relative error: " << maxdev/maxelement << "\n";
  #endif
/////////////////////////////////////////// end of debug stuff ///////////////////////////////////////

  return inverse_matrix;
}

}} // namespace math
#endif
