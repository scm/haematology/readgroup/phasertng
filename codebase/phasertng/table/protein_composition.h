#ifndef __phasertng_table_protein_composition_class__
#define __phasertng_table_protein_composition_class__
#include <map>

namespace phasertng {
namespace table {

class protein_composition
{
  private:
    //$=(not used in the standard)
    //%=(not used in the standard)
    std::map<char,std::map<std::string,double>> protein = {
      { 'A', { {"H", 5}, {"C", 3}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'C', { {"H", 5}, {"C", 3}, {"N",1}, {"O",1}, {"S",1}, {"SE",0}, {"P",0}, {"SS",0} } },
      { '%', { {"H", 5}, {"C", 3}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0.5} } },
      { 'D', { {"H", 4}, {"C", 4}, {"N",1}, {"O",3}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'E', { {"H", 6}, {"C", 5}, {"N",1}, {"O",3}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'F', { {"H", 9}, {"C", 9}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'G', { {"H", 3}, {"C", 2}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'H', { {"H", 8}, {"C", 6}, {"N",3}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'I', { {"H",11}, {"C", 6}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'K', { {"H",13}, {"C", 6}, {"N",2}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'L', { {"H",11}, {"C", 6}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'M', { {"H", 9}, {"C", 5}, {"N",1}, {"O",1}, {"S",1}, {"SE",0}, {"P",0}, {"SS",0} } },
      { '$', { {"H", 9}, {"C", 5}, {"N",1}, {"O",1}, {"S",0}, {"SE",1}, {"P",0}, {"SS",0} } },
      { 'N', { {"H", 6}, {"C", 4}, {"N",2}, {"O",2}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
      { 'P', { {"H", 7}, {"C", 4}/*wrong*/, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
#else
      { 'P', { {"H", 7}, {"C", 5}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
#endif
      { 'Q', { {"H", 8}, {"C", 5}, {"N",2}, {"O",2}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'R', { {"H",13}, {"C", 6}, {"N",4}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'S', { {"H", 5}, {"C", 3}, {"N",1}, {"O",2}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'T', { {"H", 7}, {"C", 4}, {"N",1}, {"O",2}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'V', { {"H", 9}, {"C", 5}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'W', { {"H",10}, {"C",11}, {"N",2}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'Y', { {"H", 9}, {"C", 9}, {"N",1}, {"O",2}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
//and the null entry, for modified residues, alanine at minimum
      { 'U', { {"H", 5}, {"C", 3}, {"N",1}, {"O",1}, {"S",0}, {"SE",1}, {"P",0}, {"SS",0} } },
      { 'J', { {"H",11}, {"C", 6}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { '-', { {"H", 5}, {"C", 3}, {"N",1}, {"O",1}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'B', { {"H", 5.0}, {"C", 4}, {"N",1.5}, {"O",2.5}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
      { 'Z', { {"H", 7.0}, {"C", 5}, {"N",1.5}, {"O",2.5}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} } },
    };
    //B=ASX (standard)
    //Z=GLX (standard)
    //X=unknown (standard)
    //$=(not used in the standard)
//to get disulphides, look at the SSBOND records
//         1         2          3        4         5         6         7         8
//12345678901234567890123456789012345678901234567890123456789012345678901234567890
//SSBOND   1 CYS A    6    CYS A  127                          1555   1555  2.03
    std::map<std::string, char> single_letter_code = {
      { "ALA", 'A' },
      { "CYS", 'C' }, //no single letter code for disulphide, need to read SSBOND records in pdb
      { "ASP", 'D' },
      { "GLU", 'E' },
      { "PHE", 'F' },
      { "GLY", 'G' },
      { "HIS", 'H' },
      { "ILE", 'I' },
      { "LYS", 'K' },
      { "LEU", 'L' },
      { "MET", 'M' }, { "MSE", '$' },
      { "ASN", 'N' },
      { "PRO", 'P' },
      { "GLN", 'Q' },
      { "ARG", 'R' },
      { "SER", 'S' },
      { "THR", 'T' },
      { "VAL", 'V' },
      { "TRP", 'W' },
      { "TYR", 'Y' },
//allowed modified residues read from pdb files as HETATM
      { "ASX", 'B' }, //true but no 1-1 mapping to a composition
      { "GLX", 'Z' }, //see 1H34.fa no 1-1 mapping to a composition
      { "SEC", 'U' }, //see 1GP1.fa
      { "XLE", 'J' },
  //  { "XAA", 'X' }, //true but no mapping to a composition
//allowed modified residues read from pdb files as HETATM, but with no distinct single letter mapping
                      { "DDE", '-' },
                      { "CSO", '-' },
                      { "CME", '-' },
                      { "TPO", '-' },
                      { "KYN", '-' },
                      { "SEP", '-' },
                      { "MHA", '-' },
                      { "HIE", '-' },
                      { "HID", '-' },
                      { "ACE", '-' },
                      { "NH2", '-' },
                      { "PYL", '-' },
                      { "B3B", '-' },
                      { "NLE", '-' },
                      { "MPT", '-' }
    };
    std::map<std::string,double> empty;

  public:
  protein_composition(){}

  bool
  valid_residue(const char& r) const
  { return (protein.find(r) != protein.end()); }

  bool
  valid_residue(const std::string& r) const
  { return (single_letter_code.count(r)); }

  std::map<std::string,double>
  composition(const char& r) const
  { return valid_residue(r) ? protein.at(r) : empty; }

  int
  number_of_nonhydrogen(const std::string& resname) const
  { //test valid first
    char r = single_letter_code.at(resname);
    auto comp = protein.at(r);
    int n(0);
    for (auto c : comp)
      if (c.first != "H")
        n += c.second;
    return n;
  }

  char
  single_letter(std::string residue) const  { return single_letter_code.at(residue); }

  char SeMet() const { return '$'; }
  char SSbond() const { return '%'; }
};

}}
#endif
