import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys,os
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.mode_generator import *
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng import InputCard,Phasertng,PhaserError,enum_out_stream
from phasertng.run.ModeRunner import *
import argparse

def exocomp(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_exocomp)

def inner_exocomp(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="exocomp")
    initparams = initparams +  "\n" +  build_auto_str("exocomp")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    assert(dbdir is not None)
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    loop_auto = str("Exocomp")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    #chk does not contribute to the DAG
    ModeRunner(basetng=basetng,mode_list=['chk'])

    #first check that there are some files, or else we don't fail until auto_search called
    #nb auto search calls this function again internally
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    #basetng.input.set_as_str(".phasertng.graph_report.filestem.",working_params.exocomp.output)
    ModeRunner(basetng=basetng,mode_list=['tree'])

    node_number = basetng.DAGDATABASE.PATHWAY.ULTIMATE.identifier()
    #emit the node for the srf to fill the gui with the node
    if progress_callback is not None:
      progress_callback.emit(node_number)
    #change the default
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  user_phil = args.phil.read()
  (error) = exocomp(user_phil_str=user_phil)
  sys.exit(error.exit_code())
