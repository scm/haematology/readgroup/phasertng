//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_pdbrecord_class__
#define __phasertng_pod_pdbrecord_class__
#include <phasertng/main/includes.h>
#include <phasertng/io/hybrid_36_c.h>
#include <array>

namespace phasertng {

class PdbRecord
{
  public:
    bool        TER = false; //so that TER can be inline with model atom records
 //   std::string ATOMorHETATMandNUM; //convert to hybrid36 immediately!!
    bool        Hetatm = false;
    int         AtomNum = 1;
    std::string AtomName = " C  "; //CA means they are all joined up in coot
    std::string AltLoc = " ";
    std::string ResName = "UNK";
    std::string InsCode = " ";
    std::string Chain = " A";
    int         ResNum = 0;
    dvect3      X = {0,0,0};
    double      O = 1;
    double      B = 0;
    dmat6       AnisoU = {0,0,0,0,0,0}; //cartesian
    bool        FlagAnisoU = false;
    int         Segment = -999; //flag for not present
    std::string Charge = "";
    std::string get_element() const;
    void        set_element(std::string);

  private:
    std::string Element2 = " C"; //private so we can fix the length at 2

  public:
    std::string Element = "C"; //the phaser scatterer type - converter_registry list
    bool        valid_cluster = true;
    bool        valid_element = true;
    int         generic_int = 0; //carry generic information
    std::string ExtraFlags = ""; //flags added externally, for example fix/refine flags

  public:
    PdbRecord(bool TER_=false) : TER(TER_) {}
    PdbRecord(dvect3 X_,double O_,double B_) : X(X_),O(O_),B(B_) {} //mini constructor

  public:
    std::pair<char,char> chain_pair() const;
    bool at_xplor_infinity() const;
    bool is_water() const;
    bool is_backbone() const;
    bool is_polyala() const;
    bool is_calpha() const;
    bool is_palpha() const;
    bool is_nucleic_trace() const; //more spacefilling than P
    bool is_protein_trace() const; //more spacefilling than CA
    std::pair<int,int> tncs_group() const;

  private:
    std::string hybrid36Num5(int n) const;
    std::string hybrid36Num4(int n) const;

  public:
    std::string Card(int ATOMNUM=-999,int RESNUM=-999) const;
    std::array<std::string,3> WriteAtomCard(int ATOMNUM=-999,int RESNUM=-999) const;
    void ReadAtomCard(std::string buffer);
    bool ReadAnisouCard(std::string buffer);

  public: //function
    bool operator<(const PdbRecord &right) const
    {
      if (TER) return true;
      return AtomNum < right.AtomNum;
    }
};

  struct PdbRecord_getter
  {
    public:
      typedef dvect3 value_type;
      value_type operator ()(PdbRecord const& card) const // const added
      { return card.X; }
  };

} //phasertng
#endif
