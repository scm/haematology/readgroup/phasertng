//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/minimizer/RefineRB.h>
#include <phasertng/data/Reflections.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <future>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/math/xyzRotMatDeg.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/data/Epsilon.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

//#define PHASERTNG_DEBUG_REFINERB

  RefineRB::RefineRB(
      const_ReflectionsPtr  REFLECTIONS_,
      sv_bool  SELECTED_,
      const_EpsilonPtr  EPSILON_)
    : RefineBase(),
      REFLECTIONS(std::move(REFLECTIONS_)),
      SELECTED(SELECTED_),
      EPSILON(std::move(EPSILON_))
  {
    phaser_assert(REFLECTIONS->NREFL == SELECTED.size());
  }

  void
  RefineRB::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineRB::init_node(dag::Node* work)
  {
    negvar = false;
    NODE = work;
    phaser_assert(NODE->HALL.size());
    PHASER_ASSERT(NODE->HALL == REFLECTIONS->SG.HALL);
    phaser_assert(NODE->REFLID == REFLECTIONS->REFLID);
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    PHASER_ASSERT(NODE->fraction_scattering(ztotalscat)<=1);
    //the parameters should already be in range from the check before the refinement
    NODE->reset_parameters(ztotalscat);
    NODE->initKnownMR(REFLECTIONS->data_hires(),BFAC_MAX,BFAC_MIN);
    TNCS_GROUP_POSES.clear(); //map on tgroup
    TNCS_GROUP_BFAC.clear(); //map on tgroup
    TNCS_GROUP_MODLID.clear(); //map on tgroup
    TNCS_GROUP_FRACSCAT.clear(); //map on tgroup
    LARGE_ROTN.resize(NODE->POSE.size());
    std::map<Identifier,double> entries;
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (!entries.count(modlid))
      {
        USED_MODLID.insert(p);
        entries[modlid] = NODE->POSE[p].ENTRY->SCATTERING;
      }
    }
    //the parameters should already be in range from the check before the refinement
    NODE->reset_parameters(REFLECTIONS->get_ztotalscat());
    bool thisadjusted = NODE->initKnownMR(REFLECTIONS->data_hires(),BFAC_MAX,BFAC_MIN);
    adjusted = adjusted or thisadjusted;
    TNCS_GROUP_POSES.clear(); //map on tgroup
    TNCS_GROUP_BFAC.clear(); //map on tgroup
    TNCS_GROUP_MODLID.clear(); //map on tgroup
    TNCS_GROUP_FRACSCAT.clear(); //map on tgroup
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      //NODE->POSE[p].BFAC = 0; //reset!! because range problems if things change re resolution etc
      const Interpolation& interpolation = NODE->POSE[p].ENTRY->INTERPOLATION;
      dvect3 maxperp =
          { std::max(interpolation.UC.B(),interpolation.UC.C()),
            std::max(interpolation.UC.C(),interpolation.UC.A()),
            std::max(interpolation.UC.A(),interpolation.UC.B())};
      for (int rot = 0; rot < 3; rot++)
      {
        // Define large shift as enough to move from one RLP at dmin to the next
        // with Shannon sampling, assuming angle small enough that sin(angle)=angle
        LARGE_ROTN[p][rot] = 2*scitbx::rad_as_deg(REFLECTIONS->data_hires()/maxperp[rot]);
      }
      int t = NODE->POSE[p].TNCS_GROUP.first;
      if (TNCS_GROUP_POSES.count(t))
      {
        TNCS_GROUP_POSES[t].insert(p);
      }
      else
      {
        TNCS_GROUP_POSES[t] = {p};
      }
      phaser_assert(TNCS_GROUP_POSES.size());
      if (TNCS_GROUP_BFAC.count(t))
      {
        double B1 = TNCS_GROUP_BFAC[t];
        double B2 = NODE->POSE[p].BFAC;
        if (B1 != B2)
        throw Error(err::INPUT,"Bfactors do not obey tncs [" + std::to_string(B1) + ":" + std::to_string(B2) + "]");
        TNCS_GROUP_BFAC[t] = B1;
      }
      else
      {
        TNCS_GROUP_BFAC[t] = NODE->POSE[p].BFAC;
      }
      PHASER_ASSERT(TNCS_GROUP_BFAC.size());
      if (TNCS_GROUP_MODLID.count(t))
      {
        auto M1 = TNCS_GROUP_MODLID[t];
        auto M2 = NODE->POSE[p].IDENTIFIER;
        if (M1 != M2)
        throw Error(err::INPUT,"Identifiers do not obey tncs [" + M1.str() + ":" + M2.str() + "]");
        TNCS_GROUP_MODLID[t] = M1;
      }
      else
      {
        TNCS_GROUP_MODLID[t] = NODE->POSE[p].IDENTIFIER;
      }
      if (TNCS_GROUP_FRACSCAT.count(t))
      { //we need fracscat because groups may have different numbers of tncs related components (mixed tncs)
        auto entry = entries[TNCS_GROUP_MODLID[t]];
        TNCS_GROUP_FRACSCAT[t] += REFLECTIONS->fracscat(entry);
      }
      else
      {
        TNCS_GROUP_FRACSCAT[t] = 0;
        auto entry = entries[TNCS_GROUP_MODLID[t]];
        TNCS_GROUP_FRACSCAT[t] += REFLECTIONS->fracscat(entry);
      }
      PHASER_ASSERT(TNCS_GROUP_MODLID.size());
      PHASER_ASSERT(TNCS_GROUP_FRACSCAT.size());
    }
    for (auto fracscat : TNCS_GROUP_FRACSCAT)
    {
      PHASER_ASSERT(fracscat.second > 0);
      PHASER_ASSERT(fracscat.second <= 1);
    }

    //make sure the starting values are within limits
    //make sure all the map elements with index modlid are filled with values
    for (const auto& p : USED_MODLID)
    {
      auto entry = NODE->POSE[p].ENTRY;
      auto modlid = NODE->POSE[p].IDENTIFIER;
      double& drms_shift = NODE->DRMS_SHIFT[modlid].VAL; //alias
      thisadjusted = drms_shift > entry->DRMS_MAX or drms_shift < entry->DRMS_MIN;
      adjusted = adjusted or thisadjusted;
      drms_shift = std::max(entry->DRMS_MIN,drms_shift);
      drms_shift = std::min(entry->DRMS_MAX,drms_shift);
      Interpolation& interpolation = entry->INTERPOLATION;
      auto& cell_scale = NODE->CELL_SCALE[modlid].VAL;
      thisadjusted = cell_scale > CELL_MAX or cell_scale < CELL_MIN;
      adjusted = adjusted or thisadjusted;
      cell_scale = std::max(CELL_MIN,cell_scale);
      cell_scale = std::min(CELL_MAX,cell_scale);
      interpolation.UC.reset_orthogonal_cell(); //set at instantiation
      interpolation.UC.scale_orthogonal_cell(cell_scale); //apply cell scale
      double maxcell = std::max({interpolation.UC.A(), interpolation.UC.B(), interpolation.UC.C()});
      LARGE_CELL[modlid] = REFLECTIONS->data_hires() / maxcell;
    }
    for (auto item : NODE->DRMS_SHIFT)
      INITIAL_DRMS[item.first] = item.second.VAL;
    for (auto item : NODE->CELL_SCALE)
      INITIAL_CELL[item.first] = item.second.VAL;
    ecalcs_sigmaa_pose.precalculated = false;
    //don't set the ecalcs_sigmaa_pose here because it requires DAGDB
    //which slows down the compilation
    //so calculate this externally if LAST_ONLY is used
    if (LAST_ONLY)
      PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
    start_likelihood = -999; //flag
    final_likelihood = -999; //flag
  }

  void
  RefineRB::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(stolower(item));
    for (auto item : protocol)
      if (item != "" and
          item != "off" and
          item != "rotn" and
          item != "tran" and
          item != "bfac" and
          item != "vrms" and
          item != "cell" and
          item != "last"
        )
        throw Error(err::FATAL,"Protocol flag not recognised: " + item);
    REFINE_ROTN = (protocol.find("rotn") != protocol.end());
    REFINE_TRAN = (protocol.find("tran") != protocol.end());
    REFINE_BFAC = (protocol.find("bfac") != protocol.end());
    REFINE_VRMS = (protocol.find("vrms") != protocol.end());
    REFINE_CELL = (protocol.find("cell") != protocol.end());
    if (protocol.find("off") != protocol.end())
      REFINE_ROTN = REFINE_TRAN = REFINE_BFAC = REFINE_VRMS = REFINE_CELL = false;

    const SpaceGroup& SG = REFLECTIONS->SG;
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::structure_seminvariants semis(SG.group());
    cctbx::sgtbx::search_symmetry ssym(flags,SG.type(),semis);
    af::tiny< bool, 3 > isshift(false,false,false);
    if (ssym.continuous_shifts_are_principal())
      isshift = ssym.continuous_shift_flags();
    int tgrp = NODE->POSE.back().TNCS_GROUP.first;
    refine_parameter_.clear();
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      bool refine(true);
      if (LAST_ONLY and NODE->POSE[p].TNCS_GROUP.first != tgrp) //overwrite
        refine = false;
      for (int rot = 0; rot < 3; rot++)
        REFINE_ROTN ? refine_parameter_.push_back(refine) : refine_parameter_.push_back(false);
      for (int tra = 0; tra < 3; tra++)
        (REFINE_TRAN and !(p==0 and isshift[tra])) ?
         refine_parameter_.push_back(refine) : refine_parameter_.push_back(false);
    }
    for (int tgrp = 0; tgrp < TNCS_GROUP_BFAC.size(); tgrp++)
    {
      REFINE_BFAC ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    }
    for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
    {
      REFINE_VRMS ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
      REFINE_CELL ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    }

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();

    //make sure parameters are within bounds
    auto x = get_macrocycle_parameters();
    if (false)
    {
    //apply a small random perturbation to the parameters between macrocycles
    //pseudo-stochastic gradient descent
    //currently not used
    //perturb up by 1 part per billion and then apply upper bound if necessary
      for (int i = 0; i < x.size(); i++)
      {
        x[i] += x[i]*DEF_PPB; //random perturbation
      }
    }
    auto bound = bounds();
    if (x.size() == bound.size())
    {
      for (int i = 0; i < x.size(); i++)
      {
        if (bound[i].upper_bounded() and (x[i] > bound[i].upper_limit() ))
        {
          x[i] = bound[i].upper_limit();
        }
        if (bound[i].lower_bounded() and (x[i] < bound[i].lower_limit() ))
        {
          x[i] = bound[i].lower_limit();
        }
      }
    }
    set_macrocycle_parameters(x);
  }

  sv_string
  RefineRB::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    int i(0),m(0);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      if (refine_parameter_[m++]) names[i++] = "Rotation X Pose " + std::to_string(p+1);
      if (refine_parameter_[m++]) names[i++] = "Rotation Y Pose " + std::to_string(p+1);
      if (refine_parameter_[m++]) names[i++] = "Rotation Z Pose " + std::to_string(p+1);
      if (refine_parameter_[m++]) names[i++] = "Translation X Pose " + std::to_string(p+1);
      if (refine_parameter_[m++]) names[i++] = "Translation Y Pose " + std::to_string(p+1);
      if (refine_parameter_[m++]) names[i++] = "Translation Z Pose " + std::to_string(p+1);
    }
    for (auto group : TNCS_GROUP_BFAC)
    {
      if (refine_parameter_[m++]) names[i++] = "Bfactor Tncs Group " + std::to_string(group.first);
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (refine_parameter_[m++]) names[i++] = "Drms " + modlid.str();
      if (refine_parameter_[m++]) names[i++] = "Cell " + modlid.str();
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineRB::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      for (int rot = 0; rot < 3; rot++)
        if (refine_parameter_[m++])
          pars[i++] = NODE->POSE[p].ORTHR[rot];
      for (int tra = 0; tra < 3; tra++)
        if (refine_parameter_[m++])
          pars[i++] = NODE->POSE[p].ORTHT[tra];
    }
    for (auto group : TNCS_GROUP_BFAC)
    {
      if (refine_parameter_[m++])
      {
        pars[i++] = group.second;
      }
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (refine_parameter_[m++])
        pars[i++] = NODE->DRMS_SHIFT[modlid].VAL;
      if (refine_parameter_[m++])
        pars[i++] = NODE->CELL_SCALE[modlid].VAL;
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineRB::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      for (int rot = 0; rot < 3; rot++)
      {
        if (refine_parameter_[m++]) //ROT
        {
          NODE->POSE[p].ORTHR[rot] = newx[i++];
        }
      }
      for (int tra = 0; tra < 3; tra++)
      {
        if (refine_parameter_[m++]) //TRA
        {
          NODE->POSE[p].ORTHT[tra] = newx[i++];
        }
      }
    }
    for (auto& group : TNCS_GROUP_BFAC)
    {
      if (refine_parameter_[m++]) //BFAC
        group.second = newx[i++];
      //now transfer all the bfactors to poses
      for (auto p : TNCS_GROUP_POSES[group.first]) //the poses in this group
        NODE->POSE[p].BFAC = group.second;
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (refine_parameter_[m++]) //DRMS
      {
        NODE->DRMS_SHIFT[modlid].VAL = newx[i++];
      }
      if (refine_parameter_[m++]) //CELL
      {
        double cell_scale = newx[i++];
        auto entry = NODE->POSE[p].ENTRY;
        //cell scale is refined per modlid BUT is constrained to be the same for all
        NODE->CELL_SCALE[modlid].VAL = cell_scale;
        entry->INTERPOLATION.UC.scale_orthogonal_cell(cell_scale); //apply cell scale
      }
    }

    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineRB::macrocycle_large_shifts()
  {
    sv_double largeShifts(nmp);
    int m(0),i(0);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      for (int rot = 0; rot < 3; rot++) //ROT
        if (refine_parameter_[m++])
          largeShifts[i++] = LARGE_ROTN[p][rot];
      for (int tra = 0; tra < 3; tra++) //TRA
        if (refine_parameter_[m++])
          largeShifts[i++] = REFLECTIONS->data_hires()/6.0;
    }
    for (int tgrp = 0; tgrp < TNCS_GROUP_BFAC.size(); tgrp++)
    {
      if (refine_parameter_[m++])
        largeShifts[i++] = 4.0; //BFAC
    }
    //if too large, molecule will get washed out and won't come back from limits
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (refine_parameter_[m++])
        largeShifts[i++] = REFLECTIONS->data_hires()/6.0; //DRMS
      if (refine_parameter_[m++])
        largeShifts[i++] = LARGE_CELL[modlid]; //CELL
       //Large change in cell scale moves RLP to next at dmin assuming Shannon sampling
    }
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineRB::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    double data_hires = REFLECTIONS->data_hires();
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      for (int rot = 0; rot < 3; rot++) //ROT
        if (refine_parameter_[m++]) i++;
      for (int tra = 0; tra < 3; tra++) //TRA
        if (refine_parameter_[m++]) i++;
    }
    for (auto bfac : TNCS_GROUP_BFAC)
    {
      double thisfs = TNCS_GROUP_FRACSCAT[bfac.first]; //aka t
      PHASER_ASSERT(thisfs > 0);
      PHASER_ASSERT(thisfs <= 1);
      if (refine_parameter_[m++])
      {
        double bfac_min(0);
        // Limit Bfac to lowest possible value that would not exceed total scattering power
        // if applied to smallest component, by assuming Bfac for everything else could be
        // at the maximum possible value.
        // Rely on maximum_distance_special to tighten this up if necessary.
        if (thisfs < 1.)
        {
          double ssqmax = 1. / fn::pow2(data_hires);
          double logterm = ((1.-(1.-thisfs)*std::exp(-BFAC_MAX*ssqmax/2))/thisfs);
          PHASER_ASSERT(logterm > 0);
          bfac_min = -2 * std::log(logterm) / ssqmax;
        }
        bfac_min = std::max(bfac_min, BFAC_MIN);
        bounds[i++].on(bfac_min, BFAC_MAX); //BFAC
      }
    }
    for (const auto& p : USED_MODLID)
    {
      auto entry = NODE->POSE[p].ENTRY;
      if (refine_parameter_[m++])
        bounds[i++].on(entry->DRMS_MIN, entry->DRMS_MAX); //DRMS
      if (refine_parameter_[m++])
        bounds[i++].on(CELL_MIN, CELL_MAX); //CELL
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineRB::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    sv_double pars(nmp);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      for (int rot = 0; rot < 3; rot++) //ROT
        if (refine_parameter_[m++])
          repar[i++].off();
      for (int tra = 0; tra < 3; tra++) //TRA
        if (refine_parameter_[m++])
          repar[i++].off();
    }
    for (int tgrp = 0; tgrp < TNCS_GROUP_BFAC.size(); tgrp++)
    {
      if (refine_parameter_[m++]) //BFAC
        repar[i++].off();
    }
    for (const auto& p : USED_MODLID)
    {
      if (refine_parameter_[m++]) //DRMS
      {
        auto entry = NODE->POSE[p].ENTRY;
        // Use offset that makes lowest possible value slightly positive before log
        double offset = -(entry->DRMS_MIN - (entry->DRMS_MAX - entry->DRMS_MIN) / 100);
        repar[i++].on(offset);
      }
      if (refine_parameter_[m++]) //CELL
        repar[i++].off();
    }

    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineRB::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    //store the llg without restraints
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don't want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineRB::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineRB::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineRB::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  af_string
  RefineRB::initial_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logNode("Initial");
  }

  af_string
  RefineRB::current_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logNode("Current");
  }

  af_string
  RefineRB::final_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logNode("Final");
  }

  double
  RefineRB::maximum_distance_special(
      sv_double& x,
      sv_double& p,
      sv_bool& bounded,
      sv_double& dist,
      double& start_distance)
  // Return maximum multiple of gradient that can be shifted before hitting *furthest possible* bound
  // for "special" parameters, i.e. B-factors influencing total scattering.
  // Calculations done as if OFAC is 1, which is conservative when OFAC is being refined.
  // However, this would have to be changed if upper bound for OFAC were > 1.
  // Set dist for any parameters that are found to have new distance limits
  {
//#define PHASERTNG_DEBUG_MAXDISTSPECIAL
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "maximum_distance_special " << std::endl;
    std::cout << "search vector p " << p[0] << " " << p[1] << " " << p[2] << std::endl;
    std::cout << "dist " << dist[0] << " " << dist[1] << " " << dist[2] << std::endl;
    std::cout << "start dist " << (start_distance) << std::endl;
    for (auto group : TNCS_GROUP_BFAC)
      std::cout << "bfac " << group.second << std::endl;
#endif
    if (!REFINE_BFAC)
      return 0;
    sv_double localp = p;

    // deltai and index keep track of which parameters are potentially affected by correlations
    // Value of 0 for deltai indicates parameter that is pushing up against its bounds
    sv_double deltai;
    sv_int index;
    double fracmodeled(0.); // Fraction modeled of scattering at rest
    bool bdecreasing(false);
    { //memory
      int i(0), m(0);
      for (int p = 0; p < NODE->POSE.size(); p++)
      {
        fracmodeled += REFLECTIONS->fracscat(NODE->POSE[p].ENTRY->SCATTERING);
        for (int rot = 0; rot < 3; rot++)
          if (refine_parameter_[m++])
            i++;
        for (int tra = 0; tra < 3; tra++)
          if (refine_parameter_[m++])
            i++;
      }
      for (int tgrp = 0; tgrp < TNCS_GROUP_BFAC.size(); tgrp++)
      {
        if (refine_parameter_[m++])
        {
          //check the ensemble
          if ((p[i] >= 0 and x[i] >= BFAC_MAX) or // x + d*p i.e. has run up against upper limit
              (p[i] <= 0 and x[i] <= BFAC_MIN))   // x + d*p i.e. has run up against lower limit
          {
            deltai.push_back(0);
            localp[i] = 0;
          }
          else
            deltai.push_back(p[i]);
          index.push_back(i);
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "localp " << (localp[i]) << std::endl;
#endif
          if (localp[i] < 0)
            bdecreasing = true;
          i++; //BFAC
        }
      }
      for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
      {
        if (refine_parameter_[m++])
          i++; //DRMS
        if (refine_parameter_[m++])
          i++; //CELL
      }
      phaser_assert(i == nmp);
      phaser_assert(m == total_number_of_parameters_);
    } //end memory
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "deltai " << deltai[0] << " " << deltai[1] << " " << deltai[2] << std::endl;
    std::cout << "index " << index[0] << " " << index[1] << " " << index[2] << std::endl;
    std::cout << "bdecreasing " << std::string(bdecreasing?"Y":"N") << std::endl;
#endif
    if (!bdecreasing)
      return 0; // No danger if no B-factors are decreasing

    double fracleft(1.-fracmodeled); // Fraction of unmodeled scattering at rest
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "fracmodeled " << (fracmodeled) << std::endl;
    std::cout << "fracleft " << (fracleft) << std::endl;
#endif
    double kssqr = -2*fn::pow2(1/REFLECTIONS->data_hires())/4;
    double fracleftmin = fracleft*std::exp(kssqr*BFAC_MAX);
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "fracleftmin " << (fracleftmin) << std::endl;
#endif

    const double MAX_SHIFT(std::numeric_limits<double>::max()/2.);
    double max_step(MAX_SHIFT);
    for (int s = 0; s < deltai.size(); s++)
      if (deltai[s] != 0) //i.e. 0 == flag for running up against bounds
      { //don't include the ones that are up against limits in test for max_step
        int ii = index[s];
        max_step = std::min(max_step,dist[ii]);
        //limits for dist have already been determined
        //this limit will be less than this
      }
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "max_step " << (max_step) << std::endl;
#endif
    PHASER_ASSERT(max_step >= 0);

    //B-factor special restraint
    double distance(0),step(max_step);
    //limits now between distance and distance+step
    //doesn't matter if this goes outside minB and maxB, bound limit applied separately
    sv_double newx = x;
    int nloop(0);

    double lastgooddist(0.);
    sv_double largeShifts = macrocycle_large_shifts();

    if (step) //otherwise distance=0 and use 0 as the limit (max_step=0 above)
    {
      for (;;)
      {
        distance += step;
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "----nloop " << (nloop) << " step " << step << " distance " << distance << std::endl;
#endif
        if (distance != MAX_SHIFT)
        {
          newx = damped_shift(distance,x,localp,dist,largeShifts);
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "damped shift" << std::endl;
#endif
        }
        else
        {
          for (int ii = 0; ii < x.size(); ii++)
          {
            if (localp[ii] == 0)
            {
              newx[ii] = x[ii]; //don't shift ones against the bounds
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "no shift " << newx[ii] << std::endl;
#endif
            }
            else if (std::fabs(localp[ii]) < 1)
            {
              newx[ii] = x[ii] + distance*localp[ii]; //very large but not infinite shift
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "very large shift " << newx[ii] << std::endl;
#endif
            }
            else if (localp[ii] > 0)
            {
              newx[ii] = MAX_SHIFT; //shift to positive bound
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "shift to positive bound " << newx[ii] << std::endl;
#endif
            }
            else if (localp[ii] < 0)
            {
              newx[ii] = -MAX_SHIFT; //shift to negative bound
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "shift to negative bound " << newx[ii] << std::endl;
#endif
            }
          }
        }
        double fracmodeled_B(0.);
        { //memory
          int i(0),m(0);
          for (int p = 0; p < NODE->POSE.size(); p++)
          {
            for (int rot = 0; rot < 3; rot++)
              if (refine_parameter_[m++])
                i++;
            for (int tra = 0; tra < 3; tra++)
              if (refine_parameter_[m++])
                i++;
          }
          double this_models_bfac;
          for (auto group : TNCS_GROUP_BFAC) //cannot modify std::set in place in loop
          {
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "newx B=" << (newx[i]) << std::endl;
#endif
            if (refine_parameter_[m++]) //BFAC
            {
              this_models_bfac = newx[i++]; //BFAC
              this_models_bfac = std::max(this_models_bfac,BFAC_MIN);
              this_models_bfac = std::min(this_models_bfac,BFAC_MAX);
            }
            else
            {
              this_models_bfac = group.second;
            }
            // because they must all be the same ensemble!
            double fracscat = TNCS_GROUP_FRACSCAT[group.first]; //aka t
            fracmodeled_B += fracscat*std::exp(kssqr*this_models_bfac);
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "fracmodeled_B " << (fracmodeled_B) << std::endl;
#endif
          }
          for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
          {
            if (refine_parameter_[m++])
              i++; //DRMS
            if (refine_parameter_[m++])
              i++; //CELL
          }
          phaser_assert(i == nmp);
          phaser_assert(m == total_number_of_parameters_);
        } //end memory
        double fracleft_B(1.-fracmodeled_B);
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "fracleft_B " << (fracleft_B) << std::endl;
#endif
        if ((fracleft_B < fracleftmin)) // Too much scattering at high res
        {
          distance -= step;
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "Too much scattering at high res: step " << step << " distance " << distance << std::endl;
#endif
        }
        else
        {
          lastgooddist = distance;
          if (distance >= max_step)
          {
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "No problems at limit imposed by uncorrelated limits " << distance << std::endl;
#endif
            break; // No problems at limit imposed by uncorrelated limits
          }
        }
        if (nloop++ > 1000)
        {
          distance = 0; // Paranoia: don't touch correlated parameters if limit could not be determined
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "don't touch correlated parameters if limit could not be determined " << distance << std::endl;
#endif
          break;
        }
        step /= 2.;
        if (step < 1.e-8*distance) // Is step still big enough to make a difference?
        {
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "step still big enough to make a difference " << (distance) << std::endl;
#endif
          break;
        }
      }
    }
    distance = lastgooddist;
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "last good distance " << (distance) << std::endl;
#endif

    for (int s = 0; s < deltai.size(); s++)
    {
      if (deltai[s] != 0) // 0 == flag up against limits
      {
        int ii = index[s];
        distance = std::min(distance,dist[ii]); //in case limit is lower
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "limit is lower " << (distance) << std::endl;
#endif
      }
    }
    // Set distance for all B-factors moving to smaller values to the distance determined
    // (B-factors that are increasing won't create variance problems)
    for (int s = 0; s < deltai.size(); s++)
    {
      if (deltai[s] != 0) // 0 == flag
      {
        int ii = index[s];
        if (p[ii] < 0)
        {
          dist[ii] = distance;
          if (distance < MAX_SHIFT)
          {
            bounded[ii] = true;
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << ii << " bounded " << std::endl;
#endif
          }
        }
      }
    }
#ifdef PHASERTNG_DEBUG_MAXDISTSPECIAL
    std::cout << "distance " << (distance) << std::endl;
    std::cout << "returned  " << (std::max(0.0,distance)) << std::endl;
#endif
    return std::max(0.0,distance);
  }

  TargetGradientHessianType
  RefineRB::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,true);
    if (!NODE->POSE.size()) //e.g. default for fast rotation function fixed components
    return tgh;

    // tNCS case: curvatures (particularly vrms) should take account of correlations between
    // tNCS-related copies.  for now, rely on BFGS algorithm to compensate, but it would be better
    // to deal with this explicitly.
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;
    const SpaceGroup& SG = REFLECTIONS->SG;

    //Gradient
    sv_dvect3          dLL_by_dRotn(NODE->POSE.size());
    sv_dvect3          dLL_by_dTran(NODE->POSE.size());
    std::map<Identifier,double> dLL_by_dDrms;
    std::map<Identifier,double> dLL_by_dCell;
    //Hessian
    sv_dvect3          d2LL_by_dRotn2(NODE->POSE.size());
    sv_dvect3          d2LL_by_dTran2(NODE->POSE.size());
    std::map<Identifier,double> d2LL_by_dDrms2;
    std::map<Identifier,double> d2LL_by_dCell2;

    std::map<int,double>  dLL_by_dBfac; //tncs index, this is max possible
    std::map<int,double>  d2LL_by_dBfac2; //tncs index, this is max possible

    //collect the terms that will not change
    auto pose_terms = NODE->pose_terms_interpolate();
    sv_cvect3  pose_cos_sin(NODE->POSE.size());
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      auto& pose = NODE->POSE[p];
      for (int dir = 0; dir < 3; dir++)
        pose_cos_sin[p][dir] = tbl_cos_sin.get(pose.ORTHR[dir]/360.);
    }

    //initialize gradients and hessians
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dLL_by_dRotn[p] = {0,0,0};
      dLL_by_dTran[p] = {0,0,0};
      d2LL_by_dRotn2[p] = { 0,0,0 };
      d2LL_by_dTran2[p] = { 0,0,0 };
    }
    for (auto group : TNCS_GROUP_BFAC)
    {
      dLL_by_dBfac[group.first] = 0;
      d2LL_by_dBfac2[group.first] = 0;
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      dLL_by_dDrms[modlid] = 0;
      dLL_by_dCell[modlid] = 0;
      d2LL_by_dDrms2[modlid] = 0;
      d2LL_by_dCell2[modlid] = 0;
    }

    cmplex TwoPiI = scitbx::constants::two_pi*cmplex(0,1);
    bool do_fc_grad(do_gradient); //may want to change for no-fc gradient refinement
    bool do_fc_hess(do_hessian); //may want to change for no-fc gradient refinement
    likelihood::llgi::function LLGI;
    likelihood::llgi::tgh_flags flags;
    flags.target(true).gradient(do_gradient).hessian(do_hessian);
    flags.variances(REFINE_BFAC or REFINE_VRMS);

    //first pass over reflections to calculate total EC and total SIGA
    //for likelihood target, need summed EC and SIGA value
    //NSYMP is the largest rsym.unique will get, when there are duplicates it will be fewer

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;

    pod::rsymhkl rsym(SG.NSYMP);
    xyzRotMatDeg rotmat;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const millnx& miller = work_row->get_miller();
        const double& dobs = work_row->get_dobsnat();
        const double& feff = work_row->get_feffnat();
        const double& resn = work_row->get_resn();
        const double  teps = work_row->get_teps();
        const bool&   cent = work_row->get_cent();
        const double& rssqr = work_row->get_ssqr();
        const double& eps =  work_row->get_eps();
        const double  g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        SG.symmetry_hkl_primitive(miller,eps,rsym);
        //first pass calculate total EC and total SIGA
        //for likelihood target, need summed EC and SIGA value
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);
        sv_double DobsSigaSqr_pose(NODE->POSE.size());
        af_cmplex FCpose(NODE->POSE.size(),{0.,0.});
        double last_thisDobsSigaSqr = 0;
        Identifier last_id;
        int last_tncsgrp = -999;
        //AJM TODO also speed up the lookup for multiple copies not in same tncs group
        //by doing last for SigSqr only
        if (ecalcs_sigmaa_pose.precalculated)
        {
          phaser_assert(r < ecalcs_sigmaa_pose.ecalcs.size());
          phaser_assert(r < ecalcs_sigmaa_pose.sigmaa.size());
          DobsSigaEcalc = ecalcs_sigmaa_pose.ecalcs[r]*dobs;
          DobsSigaSqr = fn::pow2(ecalcs_sigmaa_pose.sigmaa[r]);
          //sum_Esqr_search not set
        }
        for (int p = pfirst; p < NODE->POSE.size(); p++)
        {
          const auto& pose = NODE->POSE[p];
          const auto& entry = pose.ENTRY;
          const double ssqr = rssqr; //entry->INTERPOLATION.BIN.MidSsqr(b);
          double thisDobsSigaSqr = 0;
          if (pose.IDENTIFIER == last_id and last_tncsgrp == pose.TNCS_GROUP.first)
          { //bfactors the same in the tncs group
            thisDobsSigaSqr = last_thisDobsSigaSqr;
          }
          else
          {
            double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
            double bfac = pose.BFAC;
            double fs = REFLECTIONS->fracscat(entry->SCATTERING);
            double drms = NODE->DRMS_SHIFT[pose.IDENTIFIER].VAL;
            thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
            last_thisDobsSigaSqr = thisDobsSigaSqr;
            last_id = pose.IDENTIFIER;
            last_tncsgrp = pose.TNCS_GROUP.first;
          }
          DobsSigaSqr_pose[p] = thisDobsSigaSqr;
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
            cmplex iTarget = entry->INTERPOLATION.callPt(RotSymHKL); //EightPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
               thisDobsSigaSqr,eps,SG.NSYMP,
               SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac * iTarget;
            DobsSigaEcalc += thisE;
            FCpose[p] += thisE;
          }
          if (nmol > 1)
          { //halfR = false
            double Vterm = array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr;
        }
        //====
        //LLGI calculation
        //====
        double V = teps - DobsSigaSqr;
        double eobs = feff/resn;
        LLGI.target_gradient_hessian(eobs, DobsSigaEcalc, teps, V, cent, flags);
        tgh.Target -= LLGI.LL; //Already log-likelihood-gain so no need for WilsonLL

#ifdef PHASERTNG_DEBUG_REFINERB
if (r < 10)
{
std::cout << "debugging sum: r=" << r << " cent=" << cent << " feff=" << feff << " resn=" << resn << " E=" << eobs << " DobsSiga=" << std::sqrt(DobsSigaSqr) << " eps=" << eps << " Ecalc=" << DobsSigaEcalc << " V=" << V << " hkl=" << miller[0] << " " << miller[1] << " " << miller[2] <<  " llg=" << tgh.Target << std::endl;
}
#endif

        if (V <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        static std::mutex mutex;
        static int counter(0);
        if (V <= 0)
        {
          const std::lock_guard<std::mutex> lock(mutex);
          //threadsafe
          //lock_guard for cleanup of incomplete mutex.lock() with mutex.unlock()
          counter++;
          std::cout << "\n---------- " << counter <<std::endl;
          std::cout << "RefineRB Debugging reflection #" + std::to_string(r) << std::endl;
          std::cout << "V="+ std::to_string(LLGI.variance()) << std::endl;
          std::cout << "Miller=" << miller[0] << " " << miller[1] << " " << miller[2] << std::endl;
          std::cout << "|Eobs|="+ std::to_string(std::abs(eobs)) << std::endl;
          std::cout << "|Ecalc|=" + std::to_string(std::abs(DobsSigaEcalc)) << std::endl;
          std::cout << "Dobs="+ std::to_string(dobs) << std::endl;
          std::cout << "Centric="+ std::string(cent?"Y":"N") << std::endl;
          std::cout << "scattering="+ std::to_string(REFLECTIONS->get_ztotalscat()) << std::endl;
          std::cout << "reso="+ std::to_string(1/std::sqrt(rssqr)) << std::endl;
          for (auto group : TNCS_GROUP_BFAC)
            std::cout << "Bfactor Tncs Group #" + std::to_string(group.first) + "=" + std::to_string(group.second) << std::endl;
          for (auto drms : NODE->DRMS_SHIFT)
            std::cout << "Drms modlid #" + drms.second.ID.str() +"="+ std::to_string(drms.second.VAL) << std::endl;
          for (auto cell : NODE->CELL_SCALE)
            std::cout << "Cell modlid #" + cell.second.ID.str() +"="+ std::to_string(cell.second.VAL) << std::endl;
          std::cout << "tncs-g_drms="+ std::to_string(g_drms) << std::endl;
          std::cout << "tncs-epsfac="+ std::to_string(teps) << std::endl;
          if (array_G_Vterm.size())
          std::cout << "tncs-g_vterm="+ std::to_string(array_G_Vterm[r]) << std::endl;
          if (array_G_DRMS.size())
          std::cout << "tncs-g_drms="+ std::to_string(array_G_DRMS[r]) << std::endl;
          std::cout << "----------" << std::endl;
          PHASER_ASSERT(V > 0);
        }//debug
#endif

        if (do_gradient or do_hessian)
        {
          const double& dLL_by_dReEC = LLGI.dLL_by_dReEC;
          const double& dLL_by_dImEC = LLGI.dLL_by_dImEC;
          const double& d2LL_by_dReEC2 = LLGI.d2LL_by_dReEC2;
          const double& d2LL_by_dImEC2 = LLGI.d2LL_by_dImEC2;
          const double& d2LL_by_dReEC_dImEC = LLGI.d2LL_by_dReEC_dImEC;
          const double& dLL_by_dV = LLGI.dLL_by_dV;
          const double& d2LL_by_dV2 = LLGI.d2LL_by_dV2;
          const double& d2LL_by_dReEC_dV = LLGI.d2LL_by_dReEC_dV;
          const double& d2LL_by_dImEC_dV = LLGI.d2LL_by_dImEC_dV;

          //second pass of interpolation
          cmplex Ecalc_check = {0,0};
          if (ecalcs_sigmaa_pose.precalculated)
          {
            phaser_assert(r < ecalcs_sigmaa_pose.ecalcs.size());
            phaser_assert(r < ecalcs_sigmaa_pose.sigmaa.size());
            Ecalc_check = ecalcs_sigmaa_pose.ecalcs[r]*dobs;
            //sum_Esqr_search not set
          }
          for (int p = pfirst; p < NODE->POSE.size(); p++)
          {
            auto& pose = NODE->POSE[p];
            auto& entry = pose.ENTRY;
            //int b = entry->INTERPOLATION.BIN.get_bin_ssqr(rssqr);
            const double ssqr = rssqr; //entry->INTERPOLATION.BIN.MidSsqr(b);
            //function of p and r
            //summed over isym
            cvect3 dEC_by_dRotn({0,0},{0,0},{0,0});
            cvect3 dEC_by_dTran({0,0},{0,0},{0,0});
            cvect3 d2EC_by_dRotn2({0,0},{0,0},{0,0});
            cvect3 d2EC_by_dTran2({0,0},{0,0},{0,0});
            cmplex dEC_by_dCell(0,0);
            cmplex d2EC_by_dCell2(0,0);

            const dmat33& Rtr_Dtr = pose_terms[p].Rtr_Dtr;
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
              cmplex iTarget; cvect3 iGradient; cmat33 iHessian;
              std::tie(iTarget, iGradient, iHessian) =  entry->INTERPOLATION.callTGH(RotSymHKL,do_fc_grad,do_fc_hess);
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                 DobsSigaSqr_pose[p],eps,SG.NSYMP,
                 SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
              cmplex thisE =  scalefac * iTarget;
              cvect3 dthisE = scalefac * iGradient;
              cmat33 hthisE = scalefac * iHessian; // Note that this finite difference is rather approximate!
              Ecalc_check += thisE;

              { //memory for grads in symmetry loop
              if (REFINE_ROTN)
              {
                const auto& UC = entry->INTERPOLATION.UC;
                for (int rot = 0; rot < 3; rot++)
                {
                  dmat33 dQ1tr_by_dRotn = Rtr_Dtr;
                  for (int dir = 2; dir >= 0; dir--)
                  {
                    bool do_grad(dir==rot);
                    dQ1tr_by_dRotn = rotmat.grad_tr(dir,pose_cos_sin[p][dir],do_grad)*dQ1tr_by_dRotn;
                  }
                  dQ1tr_by_dRotn = UC.orthogonalization_matrix().transpose() * dQ1tr_by_dRotn;
                  dvect3 dQ1tr_h_by_dRotn_rp = dQ1tr_by_dRotn * SymHKL;
                  dEC_by_dRotn[rot] += dthisE*dQ1tr_h_by_dRotn_rp;
                  if (do_hessian)
                  {
                    dmat33 d2Q1tr_by_dRotn2 = Rtr_Dtr;
                    for (int dir = 2; dir >= 0; dir--)
                    {
                      bool do_hess(dir==rot);
                      d2Q1tr_by_dRotn2 = rotmat.hess_tr(dir,pose_cos_sin[p][dir],do_hess)*d2Q1tr_by_dRotn2;
                    }
                    d2Q1tr_by_dRotn2 = UC.orthogonalization_matrix().transpose() * d2Q1tr_by_dRotn2;
                    dvect3 d2Q1tr_h_by_dRotn2_rp = d2Q1tr_by_dRotn2 * SymHKL;
                    d2EC_by_dRotn2[rot] +=
                        dQ1tr_h_by_dRotn_rp*(hthisE*dQ1tr_h_by_dRotn_rp) +
                        dthisE*d2Q1tr_h_by_dRotn2_rp;
                  }
                }
              }

              if (REFINE_TRAN)
              {
                for (int tra = 0; tra < 3; tra++)
                {
                  dvect3 dTRA_by_du(tra==0 ? 1 : 0, tra==1 ? 1 : 0, tra==2 ? 1 : 0);
                  dvect3 dRotSymHKL(SymHKL[0], SymHKL[1], SymHKL[2]);
                  cmplex C = TwoPiI*(dRotSymHKL*(cctbxUC.fractionalization_matrix()*dTRA_by_du));
                  dEC_by_dTran[tra] += C*thisE;
                  if (do_hessian)
                  {
                    d2EC_by_dTran2[tra] += fn::pow2(C)*thisE;
                  }
                }
              }

              if (REFINE_CELL)
              {
                const auto& modlid = pose.IDENTIFIER;
                const double& cell_scale = NODE->CELL_SCALE[modlid].VAL;
                dmat33 dQ1tr_by_dCell;
                const dmat33& Q1tr = pose_terms[p].Q1tr;
                for (int q = 0; q < Q1tr.size(); q++)
                {
                  dQ1tr_by_dCell[q] = Q1tr[q]/cell_scale;
                }
                dvect3 dQ1tr_h_by_dCell_rp = dQ1tr_by_dCell*SymHKL;
                dEC_by_dCell += dthisE*dQ1tr_h_by_dCell_rp; //scalar = vector.vector
                if (do_hessian)
                {
                  d2EC_by_dCell2 += dQ1tr_h_by_dCell_rp*(hthisE*dQ1tr_h_by_dCell_rp);
                }
              }

              } //memory for grads in symmetry
            } //loop over symmetry

            { //gradients not requiring symmetry
            if (REFINE_ROTN)
            {
              for (int rot = 0; rot < 3; rot++)
              {
                double dReEC_by_dRotn_rp = dEC_by_dRotn[rot].real();
                double dImEC_by_dRotn_rp = dEC_by_dRotn[rot].imag();
                dLL_by_dRotn[p][rot] +=
                    dLL_by_dReEC*dReEC_by_dRotn_rp +
                    dLL_by_dImEC*dImEC_by_dRotn_rp;
                if (do_hessian)
                {
                  double d2ReEC_by_dRotn2_rp = d2EC_by_dRotn2[rot].real();
                  double d2ImEC_by_dRotn2_rp = d2EC_by_dRotn2[rot].imag();
                  d2LL_by_dRotn2[p][rot] +=
                      d2LL_by_dReEC2*fn::pow2(dReEC_by_dRotn_rp) +
                      d2LL_by_dImEC2*fn::pow2(dImEC_by_dRotn_rp) +
                      dLL_by_dReEC*d2ReEC_by_dRotn2_rp +
                      dLL_by_dImEC*d2ImEC_by_dRotn2_rp +
                      2*d2LL_by_dReEC_dImEC*dReEC_by_dRotn_rp*dImEC_by_dRotn_rp;
                }
              }
            }

            if (REFINE_TRAN)
            {
              for (int tra = 0; tra < 3; tra++)
              {
                double dReEC_by_dTran_rp = dEC_by_dTran[tra].real();
                double dImEC_by_dTran_rp = dEC_by_dTran[tra].imag();
                dLL_by_dTran[p][tra] +=
                    dLL_by_dReEC*dReEC_by_dTran_rp +
                    dLL_by_dImEC*dImEC_by_dTran_rp;
                if (do_hessian)
                {
                  double d2ReEC_by_dTran2_rp = d2EC_by_dTran2[tra].real();
                  double d2ImEC_by_dTran2_rp = d2EC_by_dTran2[tra].imag();
                  d2LL_by_dTran2[p][tra] +=
                      d2LL_by_dReEC2*fn::pow2(dReEC_by_dTran_rp) +
                      d2LL_by_dImEC2*fn::pow2(dImEC_by_dTran_rp) +
                      dLL_by_dReEC*d2ReEC_by_dTran2_rp +
                      dLL_by_dImEC*d2ImEC_by_dTran2_rp +
                      2*d2LL_by_dReEC_dImEC*dReEC_by_dTran_rp*dImEC_by_dTran_rp;
                }
              }
            }

            if (REFINE_BFAC)
            {
              double Bconst = -ssqr/4.0;
              double dReEC_by_dBfac = Bconst*FCpose[p].real();
              double dImEC_by_dBfac = Bconst*FCpose[p].imag();
              double thisDobsSigaSqr = DobsSigaSqr_pose[p];
              if (nmol > 1)
              {
                double Vterm = array_G_Vterm[r];
                if (tncs_modelled) Vterm /= nmol;
                thisDobsSigaSqr *= Vterm;
              }
              double dV_by_dBfac = -2*Bconst*thisDobsSigaSqr; //teps-thisV
              double dLL_by_dBfac_rp =
                  dLL_by_dReEC*dReEC_by_dBfac +
                  dLL_by_dImEC*dImEC_by_dBfac +
                  dLL_by_dV*dV_by_dBfac;
              int t = pose.TNCS_GROUP.first;
              dLL_by_dBfac[t] += dLL_by_dBfac_rp;
              if (do_hessian)
              {
                double d2ReEC_by_dBfac2 = Bconst*dReEC_by_dBfac;
                double d2ImEC_by_dBfac2 = Bconst*dImEC_by_dBfac;
                double d2V_by_dBfac2  = 2*Bconst*dV_by_dBfac;
                double d2LL_by_dBfac2_rp =
                    d2ReEC_by_dBfac2*dLL_by_dReEC +
                    fn::pow2(dReEC_by_dBfac)*d2LL_by_dReEC2 +
                    d2ImEC_by_dBfac2*dLL_by_dImEC +
                    2*dReEC_by_dBfac*dImEC_by_dBfac*d2LL_by_dReEC_dImEC +
                    fn::pow2(dImEC_by_dBfac)*d2LL_by_dImEC2 +
                    d2V_by_dBfac2*dLL_by_dV +
                    2*dReEC_by_dBfac*dV_by_dBfac*d2LL_by_dReEC_dV +
                    2*dImEC_by_dBfac*dV_by_dBfac*d2LL_by_dImEC_dV +
                    fn::pow2(dV_by_dBfac)*d2LL_by_dV2;
                int t = pose.TNCS_GROUP.first;
                d2LL_by_dBfac2[t] += d2LL_by_dBfac2_rp;
              }
            }

            if (REFINE_VRMS)
            {
              double Vconst = -DEF_TWOPISQ_ON_THREE*ssqr;
              double dReEC_by_dDrms = Vconst*FCpose[p].real();
              double dImEC_by_dDrms = Vconst*FCpose[p].imag();
              double thisDobsSigaSqr = DobsSigaSqr_pose[p];
              if (nmol > 1)
              {
                double Vterm = array_G_Vterm[r];
                if (tncs_modelled) Vterm /= nmol;
                thisDobsSigaSqr *= Vterm;
              }
              double dV_by_dDrms = -2*Vconst*thisDobsSigaSqr; // -thisV
              double dLL_by_dDrms_rp =
                  dLL_by_dReEC*dReEC_by_dDrms +
                  dLL_by_dImEC*dImEC_by_dDrms +
                  dLL_by_dV*dV_by_dDrms;
              const auto modlid = pose.IDENTIFIER;
              dLL_by_dDrms[modlid] += dLL_by_dDrms_rp;
              if (do_hessian)
              {
                double d2ReEC_by_dDrms2 = Vconst*dReEC_by_dDrms;
                double d2ImEC_by_dDrms2 = Vconst*dImEC_by_dDrms;
                double d2V_by_dDrms2 = 2*Vconst*dV_by_dDrms;
                double d2LL_by_dDrms2_rp =
                    d2ReEC_by_dDrms2*dLL_by_dReEC +
                    d2ImEC_by_dDrms2*dLL_by_dImEC +
                    d2V_by_dDrms2 *  dLL_by_dV +
                    fn::pow2(dReEC_by_dDrms)*d2LL_by_dReEC2 +
                    fn::pow2(dImEC_by_dDrms)*d2LL_by_dImEC2 +
                    fn::pow2(dV_by_dDrms) *  d2LL_by_dV2 +
                    2*dReEC_by_dDrms*dImEC_by_dDrms*d2LL_by_dReEC_dImEC +
                    2*dReEC_by_dDrms*dV_by_dDrms *  d2LL_by_dReEC_dV +
                    2*dImEC_by_dDrms*dV_by_dDrms *  d2LL_by_dImEC_dV;
                d2LL_by_dDrms2[modlid] += d2LL_by_dDrms2_rp;
              }
            }

            if (REFINE_CELL)
            {
              double dReEC_by_dCell_rp = dEC_by_dCell.real();
              double dImEC_by_dCell_rp = dEC_by_dCell.imag();
              double dLL_by_dCell_rp =
                  dLL_by_dReEC*dReEC_by_dCell_rp +
                  dLL_by_dImEC*dImEC_by_dCell_rp;
              dLL_by_dCell[pose.IDENTIFIER] += dLL_by_dCell_rp;
              if (do_hessian)
              {
                double d2ReEC_by_dCell2_rp = d2EC_by_dCell2.real()/2.0;
                double d2ImEC_by_dCell2_rp = d2EC_by_dCell2.imag()/2.0;
                double d2LL_by_dCell2_rp =
                    d2ReEC_by_dCell2_rp*dLL_by_dReEC +
                    fn::pow2(dReEC_by_dCell_rp)*d2LL_by_dReEC2 +
                    d2ImEC_by_dCell2_rp*dLL_by_dImEC +
                    2*dReEC_by_dCell_rp*dImEC_by_dCell_rp*d2LL_by_dReEC_dImEC +
                    fn::pow2(dImEC_by_dCell_rp)*d2LL_by_dImEC2;
                d2LL_by_dCell2[pose.IDENTIFIER] += d2LL_by_dCell2_rp/2.0;
              }
            }

            } //gradients and hessians not inside symmetry loop
          } //pose
          PHASER_ASSERT(std::norm(DobsSigaEcalc-Ecalc_check) < DEF_PPB); //paranoia
        } //gradient or hessian
      } //selected
    } //reflections

    //load terms into tgh.Gradient/Hessian arrays
    if (do_gradient)
    {
      int m(0),i(0);
      for (int p = 0; p < NODE->POSE.size(); p++)
      {
        for (int rot = 0; rot < 3; rot++)
          if (refine_parameter_[m++])
          { tgh.Gradient[i++] = -dLL_by_dRotn[p][rot]; }
        for (int tra = 0; tra < 3; tra++)
          if (refine_parameter_[m++])
          { tgh.Gradient[i++] = -dLL_by_dTran[p][tra]; }
      }
      for (auto group : TNCS_GROUP_BFAC)
        if (refine_parameter_[m++])
        { tgh.Gradient[i++] = -dLL_by_dBfac[group.first]; }
      for (const auto& p : USED_MODLID)
      {
        auto modlid = NODE->POSE[p].IDENTIFIER;
        if (refine_parameter_[m++])
        { tgh.Gradient[i++] = -dLL_by_dDrms[modlid]; }
        if (refine_parameter_[m++])
        { tgh.Gradient[i++] = -dLL_by_dCell[modlid]; }
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    if (do_hessian)
    {
      int m(0),i(0);
      for (int p = 0; p < NODE->POSE.size(); p++)
      {
        for (int rot = 0; rot < 3; rot++)
          if (refine_parameter_[m++])
          { tgh.Hessian(i,i) = -d2LL_by_dRotn2[p][rot]; i++; }
        for (int tra = 0; tra < 3; tra++)
          if (refine_parameter_[m++])
          { tgh.Hessian(i,i) = -d2LL_by_dTran2[p][tra]; i++; }
      }
      for (auto group : TNCS_GROUP_BFAC)
        if (refine_parameter_[m++])
        { tgh.Hessian(i,i) = -d2LL_by_dBfac2[group.first]; i++; }
      for (const auto& p : USED_MODLID)
      {
        auto modlid = NODE->POSE[p].IDENTIFIER;
        if (refine_parameter_[m++])
        { tgh.Hessian(i,i) = -d2LL_by_dDrms2[modlid]; i++; }
        if (refine_parameter_[m++])
        { tgh.Hessian(i,i) = -d2LL_by_dCell2[modlid]; i++; }
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }
    return tgh;
  }

  TargetGradientHessianType
  RefineRB::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,true);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< TargetGradientHessianType > > results;
      std::vector< std::packaged_task< TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineRB::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineRB::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
      }
    }

    //record the start and final likelihood values without restraint terms
    if (start_likelihood == -999) //flag for no set, first call
      start_likelihood = tgh.Target; //for printing
    final_likelihood = tgh.Target; //for printing, finishes at final call

    //add restraint terms
    if (do_restraint)
    {
      add_restraint(tgh,do_gradient,do_hessian);
    }
    //Gradient and tgh.Hessian already defined for minimization (negative)
    //negate Target here for minimization
    return tgh;
  }

  void
  RefineRB::add_restraint(
      TargetGradientHessianType& tgh,
      bool do_gradient,
      bool do_hessian
    )
  {
    int m(0),i(0);
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      for (int rot = 0; rot < 3; rot++)
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_ROT)
          {
            tgh.Target += fn::pow2(pose.ORTHR[rot]/SIGMA_ROT)/2.;
            if (do_gradient)
              tgh.Gradient[i] += pose.ORTHR[rot]/fn::pow2(SIGMA_ROT);
            if (do_hessian)
              tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_ROT);
          }
          i++;
        }
      }
      for (int tra = 0; tra < 3; tra++)
      {
        if (refine_parameter_[m++])
        {
          if (SIGMA_TRA)
          {
            tgh.Target += fn::pow2(pose.ORTHT[tra]/SIGMA_TRA)/2.;
            if (do_gradient)
              tgh.Gradient[i] += pose.ORTHT[tra]/fn::pow2(SIGMA_TRA);
            if (do_hessian)
              tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_TRA);
          }
          i++;
        }
      }
    }
    for (auto group : TNCS_GROUP_BFAC)
    {
      if (refine_parameter_[m++])
      {
        if (SIGMA_BFAC)
        { //restrained to 0 because all normalized to data
          tgh.Target += fn::pow2(group.second/SIGMA_BFAC)/2.;
          if (do_gradient)
            tgh.Gradient[i] += group.second/fn::pow2(SIGMA_BFAC);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_BFAC);
        }
        i++; //BFAC
      }
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->POSE[p].IDENTIFIER;
      if (refine_parameter_[m++]) //DRMS
      {
        if (SIGMA_DRMS)
        {
          double  drms_shift = NODE->DRMS_SHIFT[modlid].VAL; //alias
          double  drms_init = INITIAL_DRMS[modlid];
          double  drms = drms_shift-drms_init;
          tgh.Target += fn::pow2(drms/SIGMA_DRMS)/2.;
          if (do_gradient)
            tgh.Gradient[i] += drms/fn::pow2(SIGMA_DRMS);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_DRMS);
        }
        i++; //DRMS
      }
      if (refine_parameter_[m++]) //CELL
      {
        if (SIGMA_CELL)
        {
          double  cell_shift = NODE->CELL_SCALE[modlid].VAL; //alias
          double  cell_init = INITIAL_CELL[modlid];
          double  cell = cell_shift-cell_init;
          tgh.Target += fn::pow2(cell/SIGMA_CELL)/2.;
          if (do_gradient)
            tgh.Gradient[i] += cell/fn::pow2(SIGMA_CELL);
          if (do_hessian)
            tgh.Hessian(i,i) += 1.0/fn::pow2(SIGMA_CELL);
        }
        i++; //CELL
      }
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
  }

  af_string
  RefineRB::logBfactors(std::string title)
  {
    af_string lines;
    lines.push_back(title);
    int len(10),n(4),l(1);
    char buf[100];
    snprintf(buf,100,"segments %-*d->%*d ",n,1,n,len);
    std::string line(buf);
    bool added(false);
    for (int tgrp = 0; tgrp < TNCS_GROUP_BFAC.size(); tgrp++)
    {
      snprintf(buf,100,"%6.2f ",TNCS_GROUP_BFAC[tgrp]);
      line += std::string(buf);
      added = true;
      if (!((tgrp+1)%len)) //lists 10 long
      {
        lines.push_back(line);
        l+=len;
        snprintf(buf,100,"segments %-*d->%*d ",n,l,n,l+len-1);
        line = std::string(buf);
        added = false;
      }
    }
    if (added) lines.push_back(line);
    return lines;
  }

  af_string
  RefineRB::finalize_parameters()
  {
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      auto initial = NODE->POSE[p].ENTRY->VRMS_START[0];
      auto modlid = NODE->POSE[p].IDENTIFIER;
      phaser_assert(NODE->DRMS_SHIFT.count(modlid));
      phaser_assert(NODE->VRMS_VALUE.count(modlid));
      phaser_assert(NODE->CELL_SCALE.count(modlid));
      auto drms = NODE->DRMS_SHIFT[modlid].VAL;
      NODE->VRMS_VALUE[modlid].VAL = pod::apply_drms_to_initial(drms,initial);
    }
    af_string output;
    output.push_back("Calculated shifted vrms values");
    return output;
  }

} //phasertng
