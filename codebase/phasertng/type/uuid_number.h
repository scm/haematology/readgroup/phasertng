//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_uuid_number_class__
#define __phasertng_type_uuid_number_class__
#include <phasertng/type/optional.h>
#include <phasertng/main/uuid.h> //for uuid_t

namespace phasertng {
namespace type {

class uuid_number : public optional
{
  uuid_t dephalt = 0;
  uuid_t uvalue = 0;
  bool  pminset = false;
  bool  pmaxset = false;
  //mantissa 2^53 is max range for double to integer conversion exactly
  uuid_t pminval = 0;
  uuid_t pmaxval = 0;

  public:
  const bool& minset() const { return pminset; }
  const uuid_t& minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const uuid_t& maxval() const { return pmaxval; }

  uuid_number() : optional() {}
  uuid_number(
      const std::string parameter,
      const bool pminset_, const uuid_t pminval_,
      const bool pmaxset_, const uuid_t pmaxval_,
      const uuid_t  dephalt_ = DEF_UUID_FLAG
  );

  void set_value(uuid_t  other);
  void set_value(const sv_string& vcards,const uuid_t & other);
  bool is_default() const;
  void set_default();
  uuid_t value_or_default() const;
  uuid_t value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::uuid_number& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
