//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineRB.h>
#include <phasertng/math/rotation/rotdist.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Rigid_body_refinement
  void Phasertng::runRBR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::LOGFILE,"No poses");
      DAGDB.clear_cards(); //no result
      return;
    }

    int z(1);
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      node->generic_int2 = z++;
    }

    if (!REFLECTIONS->prepared(reflprep::FEFF))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (feff)");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    //dag nodes are already loaded in Phasertng.cc

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    bool optional(false);
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,optional);
    }}
    DAGDB.apply_interpolation(interp::FOUR); //changed to EIGHT for refinement
    if (false) //uncomment this to get the experimental tricubic refinement
    {
      DAGDB.apply_interpolation(interp::TRICUBIC); //experimental
      logTab(out::LOGFILE,"Experiemental tricubic interpolation for target, gradient, hessian");
      logBlank(out::LOGFILE);
    }

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    int io_nprint = input.value__int_number.at(".phasertng.macrbr.maximum_printed.").value_or_default();

    //initialize tNCS parameters from reflections and calculate correction terms
    //POSE
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    auto io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    logUnderLine(out::LOGFILE,"Seek Information");
    logTabArray(out::LOGFILE,DAGDB.seek_info_array());

    bool refine_off =
        input.value__choice.at(".phasertng.macrbr.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macrbr.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macrbr.macrocycle3.").value_or_default_equals("off");
    refine_off = refine_off or
        input.value__choice.at(".phasertng.macrbr.protocol.").value_or_default_equals("off");

    double lowerB = REFLECTIONS->bfactor_minimum();
    if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
      lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    double upperB = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();


    if (refine_off)
    {
      out::stream progress = out::LOGFILE;
      logBlank(progress);
      logUnderLine(progress,"Rigid Body Rescoring");
      logProgressBarStart(progress,"Rescoring poses",DAGDB.size());
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        //rescore with the LLGI target (refinement uses Wilson)
        //no selection for top peaks, recore them all
        pod::FastPoseType ecalcs_sigmaa_pose;
        bool diffsg =  REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        dag::Node* node = DAGDB.WORK;
        node->ANNOTATION += " RBR=" + dtoi(node->LLG);
        node->generic_float = node->LLG;
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
    }
    else
    {
      //when the dag nodes are entered
      out::stream where = out::LOGFILE;
      //hack below to remove rotn refinement when there is single atom molecular replacement
      std::vector<std::set<std::string>> macrocycle(3);
      macrocycle[0] = input.value__choice.at(".phasertng.macrbr.macrocycle1.").value_or_default_multi_set();
      macrocycle[1] = input.value__choice.at(".phasertng.macrbr.macrocycle2.").value_or_default_multi_set();
      macrocycle[2] = input.value__choice.at(".phasertng.macrbr.macrocycle3.").value_or_default_multi_set();
      for (int i = 0; i < 3 and DAGDB.WORK->POSE[0].ENTRY->ATOM; i++)
        macrocycle[i].erase("rotn");
      std::vector<sv_string> macro = {
        sv_string(macrocycle[0].begin(), macrocycle[0].end()),
        sv_string(macrocycle[1].begin(), macrocycle[1].end()),
        sv_string(macrocycle[2].begin(), macrocycle[2].end())
      };
      sv_int ncyc = {
        input.value__int_vector.at(".phasertng.macrbr.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macrbr.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macrbr.ncyc.").value_or_default(2)
      };
      auto method = input.value__choice.at(".phasertng.macrbr.minimizer.").value_or_default();
      auto& work = DAGDB.WORK;
      logProtocol(out::LOGFILE,macro,ncyc,method);
      if (work->POSE[0].ENTRY->ATOM) //single atom molecular replacement
        logTab(out::LOGFILE,"Single atom molecular replacement, no rotation refinement");
      logTab(out::LOGFILE,"B-factor range: " + dtos(lowerB,2) + " " + dtos(upperB,2));
      if (DAGDB.NODES.is_all_tncs_modelled())
        logTab(where,"Ignoring tNCS");
      if (input.value__boolean.at(".phasertng.macrbr.last_only.").value_or_default())
        (nmol > 1) ?
          logTab(where,"Only refining last poses added (a tNCS group)"):
          logTab(where,"Only refining last pose added");
      logUnderLine(where,"Pose Refinement");
      logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
      int n(1),N(DAGDB.size());
      DAGDB.restart(); //very important to call this to set WORK
      out::stream progress = out::LOGFILE;
      logProgressBarStart(progress,"Refining Poses",DAGDB.size());
      double topllg = std::numeric_limits<double>::lowest();
      bool adjusted(false);
      while (!DAGDB.at_end())
      {
        auto& work = DAGDB.WORK;
        std::string nN = nNtos(n++,N);
        out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
        logUnderLine(where,"Rigid Body Refinement" +nN);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        work->reset_parameters(REFLECTIONS->get_ztotalscat());
        bool adjusted = work->initKnownMR(
                     REFLECTIONS->data_hires(),upperB,lowerB);
        if (adjusted)
          logAdvisory("B-factors adjusted for variance checks");
        if (Suite::Extra())
        {
          logTabArray(out::TESTING,work->logNode("Known components"));
          logBlank(out::TESTING);
        }
        logTab(where,"Start");
        RefineRB refine(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon);
        refine.SIGMA_ROT = input.value__flt_number.at(".phasertng.macrbr.restraint_sigma.rotation.").value_or_default();
        refine.SIGMA_TRA = input.value__flt_number.at(".phasertng.macrbr.restraint_sigma.translation.").value_or_default();
        refine.SIGMA_BFAC = input.value__flt_number.at(".phasertng.macrbr.restraint_sigma.bfactor.").value_or_default();
        refine.SIGMA_DRMS = input.value__flt_number.at(".phasertng.macrbr.restraint_sigma.drms.").value_or_default();
        refine.SIGMA_CELL = input.value__flt_number.at(".phasertng.macrbr.restraint_sigma.cell.").value_or_default();
        refine.BFAC_MIN = lowerB;
        refine.BFAC_MAX = upperB;
        refine.CELL_MIN = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_min();
        refine.CELL_MAX = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_max();
        refine.LAST_ONLY = input.value__boolean.at(".phasertng.macrbr.last_only.").value_or_default();
        if (refine.LAST_ONLY)
        {
          refine.pfirst = 0;
          int tgrp = work->POSE.back().TNCS_GROUP.first;
          for (int p = 0; p < work->POSE.size(); p++)
            if (work->POSE[p].TNCS_GROUP.first != tgrp) //overwrite
              refine.pfirst = p; //store the last included in the lastonly_pose
          DAGDB.apply_interpolation(interp::FOUR); //before work_ecalcs_sigmaa
          refine.ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              refine.pfirst); //here the pfirst is the plast used for the storage
          refine.ecalcs_sigmaa_pose.precalculated = true;
          phaser_assert(refine.ecalcs_sigmaa_pose.size() == REFLECTIONS->NREFL);
        }
        refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
        DAGDB.apply_interpolation(interp::EIGHT); //before init_node
        refine.init_node(work);

        dtmin::Minimizer minimizer(this);
        minimizer.set_logfile_level(out::TESTING);
        minimizer.run(
          refine,
          macro,
          ncyc,
          method,
          input.value__boolean.at(".phasertng.macrbr.study_parameters.").value_or_default(),
          input.value__flt_number.at(".phasertng.macrbr.small_target.").value_or_default()
        );
        work->generic_float = -refine.start_likelihood; //interp::EIGHT
        double llg = -refine.final_likelihood; //likelihood(); //store llg in node //interp::EIGHT
        work->LLG = llg;
        adjusted = adjusted or refine.adjusted;
        if (refine.negvar) logWarning("Negative variance ignored, contact developers");
        work->ANNOTATION += " RBR=" + dtoi(llg);
        work->generic_bool = minimizer.convergence();
        logTabArray(where,work->logPose("Refined"));
        if (input.value__boolean.at(".phasertng.macrbr.study_parameters.").value_or_default())
        {
          throw Error(err::DEVELOPER,"Study parameters: break after first refinement");
        }
        if (llg > topllg)
        {
          topllg = llg;
          logProgressBarAgain(progress,"New Best LLG=" + dtos(topllg,2)+nN);
        }
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
      if (adjusted) logWarning("At least one B-factor, rms or cell adjusted for range");
    }

    {{
    out::stream where = out::VERBOSE;
    for (int i = 0; i < DAGDB.NODES.size(); i++)
      DAGDB.NODES[i].generic_int = i+1;
    logEllipsisOpen(where,"Sort");
    DAGDB.NODES.apply_sort_llg();
    logEllipsisShut(where);
    }}

    if (true) //!refine_off) //AJM TODO? do anyway, just in case
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Flagging duplicates");
      bool is_a_map(REFLECTIONS->MAP); //cast from tribool
      int dupl = DAGDB.calculate_duplicate_poses(selected.STICKY_HIRES,is_a_map,USE_STRICTLY_NTHREADS,NTHREADS);
      logEllipsisShut(where);
      if (dupl == 1)
        logTab(where,"Duplicates: There is " + itos(dupl) + " out of " + itos(DAGDB.size()));
      else
        logTab(where,"Duplicates: There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
    } //generic_float overwritten
    else
    {
      for (int i = 0; i < DAGDB.NODES.size(); i++)
      {
        DAGDB.NODES[i].EQUIV = i;
        DAGDB.NODES[i].generic_flag = true;
      }
    }


    for (auto& node : DAGDB.NODES)
    { //load shifts wrt original
      dmat33 orthmat = node.CELL.orthogonalization_matrix();
      dmat33 fracmat = node.CELL.fractionalization_matrix();
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
        const dvect3& PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
        dmat33 ROTmt; dvect3 FRACT;
        //node.DATA_SCALE not implemented
        std::tie(ROTmt,FRACT) = node.POSE[p].finalRT(orthmat,fracmat);
        dvect3 eulermt = scitbx::math::euler_angles::zyz_angles(ROTmt);
        std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(eulermt,PR);
        dvect3 fract = dag::fract_wrt_in_from_mt(FRACT,ROTmt,PT,node.CELL);
        dmat33 orthmat = node.CELL.orthogonalization_matrix();
        pose.SHIFT_ORTHT = orthmat*fract;
      }
    }

    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    out::stream where = out::LOGFILE;
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::max(3,itow(io_nprint2)); //-n/a-
    logTableTop(where,"Rigid Body Refinement Results");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    int cellw = REFLECTIONS->UC.width();
    if (DAGDB.size())
    {
      logTab(where,"#r      Refined rank of the peak");
      logTab(where,"*/&     Unique/Duplicate");
      logTab(where,"#i/#o   Input/Output rank of the peak");
      logTab(where,"=/<     Converged/Not-Converged");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s%c %-*s %*s %*s  %10s %10s%c %7s  %-13s %*s",
          w,"#r",' ',
          ww,"#i",
          ww,"#o",
          ww,"#=",
          "LLG0","LLG",' ',"TFZ","Space Group",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
      logTab(where,snprintftos(
          "%*s %2s  %s  %s  %s (%5s/%-5s) %7s %6s=%-5s %*s",
          w+1+(ww+1)*3,"Final","#",
          dag::header_polar().c_str(),
          dag::header_fract().c_str(),
          dag::header_ortht(cellw).c_str(),
          "Angle","Shift","Bfactor","DRMS","VRMS",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid"));
    }
    logTab(where,"--------");
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      logTab(where,snprintftos(
          "%*i%c %-*i %*s %*s  %10s %10s%c %7s  %-13s %*s",
          w,i,node.generic_flag ? '*': '&',
          ww,node.generic_int2,
          ww,node.generic_flag ? itos(node.NUM).c_str() : "",
          ww,node.generic_flag ? "n/a" : itos(node.EQUIV+1).c_str(),
          dtos(node.generic_float,q).c_str(),
          dtos(node.LLG,q).c_str(),
          node.generic_bool ? '=' : '<',
          dtos(node.ZSCORE,2).c_str(),
          node.SG->sstr().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()));
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        double ang = pose.delta_angle();
        double sft = pose.delta_shift();
        int da = ang >= 10 ? 2 : 3;
        int ds = sft >= 10 ? 2 : 3;
        double initial = pose.ENTRY->VRMS_START[0]; //alphafold esm
        double drms = node.DRMS_SHIFT[pose.IDENTIFIER].VAL;
        double vrms = pod::apply_drms_to_initial(drms,initial);
        dmat33 fracmat = node.CELL.fractionalization_matrix();
        logTab(where,snprintftos(
            "%*s %2s  %s  %s  %s (%5.*f/%5.*f) %+7.1f %+6.2f=%05.3f %*s",
            w+1+(ww+1)*3,"",
            ntos(p+1,node.POSE.size()).c_str(),
            dag::print_polar(pose.SHIFT_MATRIX).c_str(),
            dag::print_fract(fracmat*pose.SHIFT_ORTHT).c_str(),
            dag::print_ortht(pose.SHIFT_ORTHT,cellw).c_str(),
            da,ang,
            ds,sft,
            pose.BFAC,
            drms,vrms,
            unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
        logTab(where,snprintftos(
          "%12s EULER=[% 6.1f % 6.1f % 6.1f] FRACT=[% 7.4f % 7.4f % 7.4f]",
           "",pose.EULER[0],pose.EULER[1],pose.EULER[2],
          pose.FRACT[0],pose.FRACT[1],pose.FRACT[2]));
      logTab(where,snprintftos(
          "%12s ORTHR={%+6.3f %+6.3f %+6.3f} ORTHT={%+7.3f %+7.3f %+7.3f}",
          "",pose.ORTHR[0],pose.ORTHR[1],pose.ORTHR[2],
             pose.ORTHT[0],pose.ORTHT[1],pose.ORTHT[2]));
#endif
      }
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    double io_purge = input.value__boolean.at(".phasertng.macrbr.purge_duplicates.").value_or_default();
    if (io_purge)
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge duplicates");
      int before = DAGDB.size();
      DAGDB.NODES.erase_invalid();
      logEllipsisShut(where);
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(DAGDB.size()));
    }

    int io_maxstored = input.value__int_number.at(".phasertng.macrbr.maximum_stored.").value_or_default();

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge by maximum stored");
      DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored:     " + itos(io_maxstored));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(DAGDB.NODES.size()));
    }
    }}

    {{
    out::stream where = out::SUMMARY;
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int2,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::max(3,itow(io_nprint2)); //-n/a-
    logTableTop(where,"Rigid Body Refinement Summary");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#o #i   Output/Input rank of the peak");
      logTab(where,"=/<     Converged/Not-Converged");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %*s %10s %10s  %7s   (%11s) %7s %18s",
          ww,"", ww,"","","","","  average  ","highest","(all pose average)"));
      logTab(where,snprintftos(
          "%-*s %*s %10s %10s%c %7s %3s (%5s/%-5s) %7s %6s=%-5s %5s %-13s %*s %*s",
          ww,"#o",
          ww,"#i",
          "LLG0","LLG",' ',"TFZ","PAK","Angle","Shift","Bfactor","DRMS","VRMS","CELL","Space Group",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    logTab(where,"--------");
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      dag::Pose& pose = node.POSE.back();
      double ang(0),sft(0),drms(0),bfa(-1000),vrms(0),cell(0),nref(0);
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        ang += pose.delta_angle();
        sft += pose.delta_shift();
        bfa = std::max(bfa,pose.BFAC);
        drms += node.DRMS_SHIFT[pose.IDENTIFIER].VAL/node.POSE.size();
        vrms += node.VRMS_VALUE[pose.IDENTIFIER].VAL/node.POSE.size();
        cell += node.CELL_SCALE[pose.IDENTIFIER].VAL/node.POSE.size();
        if (pose.delta_angle() != 0 or pose.delta_shift() != 0)
          nref++;
      }
      if (nref) ang /= nref;
      if (nref) sft /= nref;
      int da = ang >= 10 ? 2 : 3;
      int ds = sft >= 10 ? 2 : 3;
      logTab(where,snprintftos(
          "%*s %*i %10s %10s%c %7s  %c  (%5.*f/%5.*f) %+7.1f %+6.2f=%05.3f %5.3f %-13s %*s %*s",
          ww,node.generic_flag ? itos(node.NUM).c_str() : "",
          ww,node.generic_int2,
          dtos(node.generic_float,q).c_str(),
          dtos(node.LLG,q).c_str(),
          node.generic_bool ? '=' : '<',
          dtos(node.ZSCORE,2).c_str(),
          node.PACKS,
          da,ang,
          ds,sft,
          bfa,drms,vrms,cell,
          node.SG->sstr().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":pose.IDENTIFIER.string().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);

    if (input.generic_int == 9)
    { //output for b-factor refinement of residues
      logChevron(out::LOGFILE,"Store B-factors");
      input.generic_string = "";
      for (int p = 0; p < DAGDB.NODES[0].POSE.size(); p++)
      {
        auto b = DAGDB.NODES[0].POSE[p].BFAC;
        input.generic_string += dtos(b) +" ";
      }
    }

    if (input.value__boolean.at(".phasertng.macrbr.cleanup.").value_or_default())
    {
      logEllipsisOpen(out::LOGFILE,"Cleanup parameters");
      for (int i = 0; i < DAGDB.size(); i++)
        DAGDB.NODES[i].cleanUp();
      logEllipsisShut(out::LOGFILE);
    }

    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        if (entry.second.INTERPOLATION.Warnings.size())
          logWarning(entry.second.INTERPOLATION.Warnings);
    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        entry.second.INTERPOLATION.Warnings = "";
  }

} //phasertng
