//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/ConsistentDLuz.h>

namespace phasertng {

  ConsistentDLuz::ConsistentDLuz(
      const int max_nround_
    ) :
      max_nround(max_nround_)
  { }

  int
  ConsistentDLuz::setup(
      FourParameterSigmaA solTerm,
      sv_double RMSlist,
      af::versa<double, af::flex_grid<> > CorMat,
      double midSsqr
    )
  {
    Rms_in = RMSlist;
    // Massage DLuz values computed from user RMSD to be consistent with correlation matrix at chosen resolution
    int nmodels = Rms_in.size();
    PHASER_ASSERT(nmodels>0);
    sv_double DLuz(nmodels);
    for (int m = 0; m < nmodels; m++)
    {
      solTerm.set_rmsd(RMSlist[m]); //fsol/bsol are stored
      DLuz[m] = solTerm.get(midSsqr);
    }

    DLuz_in = DLuz;
    // Massaging of DLuz values considers one pair at a time, and adjusting a later pair for consistency
    // might break the consistency of an earlier pair.  Perhaps a linear programming algorithm could satisfy all
    // constraints simultaneously, but carrying out the adjustment iteratively converges quickly.
    // Any remaining minor issues will be dealt with in the subsequent quadratic optimisation step.
    for (int nround = 0; nround < max_nround; nround++)
    {
      int num_updated(0);
      sv_double DLuzMin(nmodels),DLuzMax(nmodels);
      for (int n = 0; n < nmodels; n++)
      {
        DLuzMin[n] = DLuzMax[n] = DLuz[n];
      }
      //make rhoDelta12 for return
      af::versa<double, af::flex_grid<> > n_rhoDelta12(af::flex_grid<>(nmodels,nmodels));
      for (int i = 0; i < nmodels; i++) //limit nmodel for initialization of nmodel,nmodel
      {
        n_rhoDelta12(i,i) = 1.0;
        for (int j = i+1 ; j < nmodels; j++)
        {
          // Avoid problems with very small and negative correlation matrix values: replace with something
          // around the standard deviation of a zero correlation computed from about 2000 samples.
          double cmfloor = std::max(CorMat(i,j),0.02); // 0.0158 rounded up
          n_rhoDelta12(i,j) = (cmfloor - DLuz[i]*DLuz[j]) /
                                 std::sqrt((1.-fn::pow2(DLuz[i]))*(1.-fn::pow2(DLuz[j])));
          n_rhoDelta12(j,i) = n_rhoDelta12(i,j);
        }
      }
      for (int i = 0; i < nmodels-1; i++) //limit different as nmodel,nmodel not considered
      {
        for (int j = i+1 ; j < nmodels; j++)
        {
          double cmfloor = std::max(CorMat(i,j),0.02);  // as above
          if (n_rhoDelta12(i,j) < 0.) // Implied negative error correlation: reduce both DLuz values to make zero
          {
            double sascale = std::sqrt(cmfloor/(DLuz[i]*DLuz[j]));
            DLuzMin[i] = std::min(DLuzMin[i],sascale*DLuz[i]);
            DLuzMin[j] = std::min(DLuzMin[j],sascale*DLuz[j]);
            inconsistent = true;
          }
          if (n_rhoDelta12(i,j) > cmfloor)
          {
            // Implied error correlation can be reduced by making Luzzati-D values closer, keeping sum constant
            double sa1(DLuz[i]),sa2(DLuz[j]),rho12(cmfloor);
            double sasum(sa1+sa2);
            double sasum2(fn::pow2(sasum));
            double v12(1.-fn::pow2(rho12));
            double sqrtarg(0.);
            if (rho12 < 0.9999)
            {
              sqrtarg = std::max(0., (sasum2 - rho12*(4. + rho12*(4. + sasum2)) +
                                  4.*rho12*std::sqrt(fn::pow2(1. + rho12) - v12*sasum2)) / v12);
            }
            double sashift = (std::max(sa1,sa2)-std::min(sa1,sa2)-std::sqrt(sqrtarg))/2.;
            if (DLuz[i] < DLuz[j])
            {
              DLuzMax[i] = std::max(DLuzMax[i],DLuz[i] + sashift);
              DLuzMin[j] = std::min(DLuzMin[j],DLuz[j] - sashift);
            }
            else
            {
              DLuzMin[i] = std::min(DLuzMin[i],DLuz[i] - sashift);
              DLuzMax[j] = std::max(DLuzMax[j],DLuz[j] + sashift);
            }
          }
        }
      }
      for (int n = 0; n < nmodels; n++)
      {
        if ((DLuz[n] != DLuzMin[n]) ||
            (DLuz[n] != DLuzMax[n]))
        {
          // If conflicting choices about increasing or decreasing, choose to decrease D (increase RMSD)
          DLuz[n] = (DLuzMin[n] < DLuz[n]) ? DLuzMin[n] : DLuzMax[n];
          num_updated++;
        }
      }
      if (nround == 0) rhoDelta12 = n_rhoDelta12;
      if (num_updated == 0) break;
    }

    // Change DLuz to maximise the conditional variance subject to quadratic restraints to the incoming values.
    // Iterate if necessary, until the conditional variance of the ensemble structure factor is at least as large
    // as expected if all the models had independent errors.
    double conditionalVariance(-1.),condVarIndependent(0.);
    int whilecount(-1);
    double DLuzInvVar(1./fn::pow2(0.15)); // Rough estimate for 20% uncertainty in RMSD
    int filtered(0);
    pseudoCorMatInv = math::RealSymmetricPseudoInverse(CorMat,filtered);
    //numerical stability correction
    for (int i = 0; i < nmodels; i++)
    {
      for (int j = 0; j < nmodels; j++)
      {
        if (fabs(pseudoCorMatInv(i,j)) < 1.0e-08)
        {
          pseudoCorMatInv(i,j) = 0.;
        }
      }
    }

    //tolerance for numerical stability required so that case of nmodel=1 degenerates correctly
    //while (conditionalVariance < condVarIndependent)
    DLuz_out = DLuz;
    double tol = 1.0e-06;
    while ((condVarIndependent-conditionalVariance) > tol) //tolerance for numerical stability
    {
      whilecount++;
      scitbx::af::versa<double, scitbx::af::flex_grid<> > augmented_pCMI = pseudoCorMatInv.deep_copy();
      sv_double wtdDLuz(nmodels);
      for (int i = 0; i < nmodels; i++)
      {
        augmented_pCMI(i,i) += DLuzInvVar;
        wtdDLuz[i] = DLuz[i]*DLuzInvVar;
      }
      filtered = 0;
      DLuz = math::mat_vec_mult( math::RealSymmetricPseudoInverse(augmented_pCMI,filtered), wtdDLuz);
      conditionalVariance = 1. - math::dot_prod( math::mat_vec_mult(pseudoCorMatInv, DLuz), DLuz);
      // Find conditional variance that would apply if all the models had independent errors.
      // We will assume that model errors can be positively but not negatively correlated, in which
      // case the conditional variance should not be less than this value.
      af::versa<double, af::flex_grid<> > CorMatIndependent(af::flex_grid<>(nmodels,nmodels));
      for (int i = 0; i < nmodels; i++)
      {
        for (int j = 0; j < nmodels; j++)
        {
          CorMatIndependent(i,j) = (i==j) ? 1. : DLuz[i]*DLuz[j];
        }
      }
      filtered = 0;
      const af::versa<double, af::flex_grid<> >
          pseudoCorMatIndepInv =
              math::RealSymmetricPseudoInverse(CorMatIndependent,filtered);
      condVarIndependent = 1. -  math::dot_prod( math::mat_vec_mult(pseudoCorMatIndepInv, DLuz), DLuz);
      if ((condVarIndependent-conditionalVariance) > tol) //tolerance for numerical stability
        DLuz_out = DLuz;
    }

    // Update RMSD values to agree with updated DLuz values
    Rms_out.resize(nmodels);
    for (int m = 0; m < nmodels; m++)
    {
      if (DLuz_out[m] > 0)
      {
        double ratio(DLuz_in[m]/DLuz_out[m]);
        PHASER_ASSERT(ratio > 0);
        double thisDVRMS = std::log(DLuz_in[m]/DLuz_out[m])/(TWOPISQ_ON_THREE*midSsqr);
        double rmssqr(fn::pow2(RMSlist[m]) + thisDVRMS);
        PHASER_ASSERT(rmssqr>=0);
        Rms_out[m] = std::sqrt(rmssqr);
      }
      else
      {
        PHASER_ASSERT(DLuz_in[m] == 0);
        Rms_out[m] = RMSlist[m];
      }
    }
    return whilecount;
  }

}
