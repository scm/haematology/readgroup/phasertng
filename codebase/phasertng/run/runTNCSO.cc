//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Selected.h>
#include <phasertng/pod/data_nmol.h>
#include <phasertng/data/PattersonFunction.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/math/factors_of.h>
#include <phasertng/site/Site.h>

namespace phasertng {

     std::pair<double,int> erase_the_low_or_origin_ones(double patterson_cutoff, bool erase_origin,
            af_double& peak_heights_percent,
            af_dvect3& peak_sites,
            af_double& peak_zscore,
            std::vector<std::vector<pod::data_nmol> >& expanded_non_origin_peaks,
            sv_bool& origin);

  //tNCS_modulation_analysis
  void Phasertng::runTNCSO()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");
    if (!REFLECTIONS->prepared(reflprep::ANISO))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (aniso)");

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    double hires_all_data(0);
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_outliers_and_select(REFLECTIONS.get(),
        selected.hires_criteria());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    //float  tests_peak = 95; //Minimum acceptable percent of origin for commensurate modulation.
    float  tests_peak = 20; //Minimum acceptable percent of origin for commensurate modulation.
    float  tests_tolf = 0.02; //The maximum fractional difference (in x, y, or z) between index*vector and an exact unit cell repeat, as a measure of the tolerance from perfect commensurate modulation.
    float  tests_tolo = 5.0;//The length of the vector difference between index*vector and a unit cell repeat in orthogonal Angstroms, as a measure of the tolerance in orthogonal angstroms from perfect commensurate modulation.
    float  tests_patt = 10; //The percent of the Patterson origin peak height used for the frequency analysis, and determining if all vectors are present
    float  tests_miss = 100*(1/2.); //Percentage of a complete set of vectors allowed to be missing from the complete set and still allow the commensurate modulation.
                                    //allow any number of missing

    if (REFLECTIONS->MAP)
      logWarning("tNCS analysis using Amplitudes from Map rather than Intensities");

    pod::vector_data_nmol analysis;
    bool indicated = false;

    input.value__boolean.at(".phasertng.tncs_analysis.insufficient_data.").set_value(false);
    input.value__boolean.at(".phasertng.tncs_analysis.insufficient_resolution.").set_value(false);
    input.value__boolean.at(".phasertng.tncs_analysis.coiled_coil.").set_value(false);
    char io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").value_or_default_char();
    double io_origin_distance = input.value__flt_number.at(".phasertng.tncs_order.patterson.origin_distance.").value_or_default();
    double io_coilcoil_distance = 16;

    //io_patterson_percent is the minimum height for the TNCS vector height
    //mapcut_patt_percent is used for the map cutoff before fft and for the missing peaks in
    //the commensurate test. High and mapcut numbers could be different but
    //mapcut_patt_percent must be < io_patterson_percent
    //otherwise the fft will cut off all the interesting peaks before frequency anaysis (oops)
    //so mapcut is not entered directly, but as a percent of io_patterson_percent i.e. io_tests_patt_percent
    //io_tests_patt_percent is the fraction of io_patterson_percent to take for fft filtering
    //the noise level is set to tests percent of the percent cutoff for the selection
    double mapcut_patt_percent;
    double io_patterson_percent = input.value__percent.at(".phasertng.tncs_order.patterson.percent.").value_or_default_percent();
    {{
    if (tests_patt < 0.5*io_patterson_percent)
      mapcut_patt_percent = tests_patt; //maximum value is default 10
    else mapcut_patt_percent = 0.5*io_patterson_percent;
    logTab(out::TESTING,"mapcut_patt_percent=" + dtos(mapcut_patt_percent));
    }}

    // Change default Patterson limits if only low-resolution data
    PattersonFunction patterson;
    bool origin_removed(false);
    {{
    double patt_hires = std::max(REFLECTIONS->data_hires(),
        input.value__flt_paired.at(".phasertng.tncs_order.patterson.resolution.").value_or_default_min());
    double patt_lores = std::min(REFLECTIONS->data_lores(),
        input.value__flt_paired.at(".phasertng.tncs_order.patterson.resolution.").value_or_default_max());
    af::shared<millnx> miller_res_limits;
    af_cmplex    Iobs;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const millnx miller = REFLECTIONS->get_miller(r);
      const double iobs = REFLECTIONS->get_flt(labin::INAT,r);
      const double& ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
      if (REFLECTIONS->get_present(friedel::NAT,r) and iobs > 0)
      {
        phaser_assert(ssqr > 0);
        double reso = 1/std::sqrt(ssqr);
        if (selected.get_selected(r) and
             reso >= patt_hires and
             reso <= patt_lores)
        {
          miller_res_limits.push_back(miller);
          double phase(0);
          Iobs.push_back(cmplex(iobs,phase));
        }
      }
    }
    if (!miller_res_limits.size()) //get desperate, use all the reflections that are present
    {
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double iobs = REFLECTIONS->get_flt(labin::INAT,r);
        if (selected.get_selected(r)) //but not that desperate
        {
          miller_res_limits.push_back(miller);
          double phase(0);
          Iobs.push_back(cmplex(iobs,phase));
        }
      }
    }
    af_double SigIobs(Iobs.size(),0.01);
    bool use_multiplicity(false);
    ivect3 max_frequency(std::floor(REFLECTIONS->UC.A()/io_origin_distance),
                         std::floor(REFLECTIONS->UC.B()/io_origin_distance),
                         std::floor(REFLECTIONS->UC.C()/io_origin_distance));
    logTab(out::VERBOSE,"Maximum frequency " + ivtos(max_frequency));
    logEllipsisOpen(out::LOGFILE,"Calculate Patterson");
    logTab(out::LOGFILE,"Resolution = " + dtos(patt_hires,1) + "A - " + dtos(patt_lores,1) + "A");
    auto text = patterson.calculate(
        REFLECTIONS->SG.group(),
        REFLECTIONS->UC.cctbxUC,
        miller_res_limits,
        Iobs,
        SigIobs,
        origin_removed,
        use_multiplicity,
        mapcut_patt_percent,
        input.value__int_number.at(".phasertng.tncs_order.minimum_number_of_reflections.").value_or_default(),
        input.value__flt_number.at(".phasertng.tncs_order.minimum_resolution.").value_or_default(),
        max_frequency //maximum frequency that divides into origin-distance sections
      );
    logEllipsisShut(out::LOGFILE);
    logTabArray(out::VERBOSE,text);
    logTab(out::VERBOSE,"Number of reflections for Pattersion: " + itos(miller_res_limits.size()));
    if (patterson.insufficient_data)
    {
      logWarning("Too few reflections for Patterson analysis");
      input.value__boolean.at(".phasertng.tncs_analysis.insufficient_data.").set_value(patterson.insufficient_data);
    }
    else if (patterson.insufficient_resolution)
    {
      logWarning("Resolution too low for Patterson analysis");
      input.value__boolean.at(".phasertng.tncs_analysis.insufficient_resolution.").set_value(patterson.insufficient_resolution);
    }
    else if (REFLECTIONS->UC.A() < io_origin_distance or
        REFLECTIONS->UC.B() < io_origin_distance or
        REFLECTIONS->UC.C() < io_origin_distance)
    {
      logWarning("Unit Cell dimensions smaller than origin Patterson vector distance");
      patterson.insufficient_data = true; //reuse the same flag as from Patterson
      input.value__boolean.at(".phasertng.tncs_analysis.insufficient_data.").set_value(patterson.insufficient_data);
    }
    }}
    int io_nprint = 100; // ie 'all'
    logTabArray(out::VERBOSE,patterson.logStatsExtra());
    logTabArray(out::LOGFILE,patterson.logStatistics(mapcut_patt_percent,"tNCS",io_nprint,io_origin_distance,io_patterson_percent));
    out::stream extra = out::TESTING;
    logTabArray(extra,patterson.logFrequencies("tNCS"));
    af_double peak_heights_percent = patterson.Patterson_heights_percent.deep_copy();
    af_double peak_zscore = patterson.Patterson_zscore.deep_copy();
    af_dvect3 peak_sites = patterson.Patterson_sites.deep_copy();
    //note that peaks over mapcut do not meed to be maintained in the list here
    //because we are going to interpolate into the patterson at the corect positions for the vectorset
    //ie recalculate the values, not rely on the peaks

    class tncsdat
    { public:
      tncsdat(int a,dvect3 b,ivect3 c={0,0,0},bool d=false):order(a),vector(b),parity(c),unique(d) {};
      int order = 1; dvect3 vector = {0,0,0}; ivect3 parity = {0,0,0}; bool unique = false;
      std::string str() { return  itos(order) + " vector=" + dvtos(vector) + " parity=" + ivtos(parity) + " unique=" + btos(unique); }
    };
    std::vector<tncsdat> tncsdata;
    if (patterson.insufficient_resolution or patterson.insufficient_data)
    {
      tncsdata.push_back(tncsdat(1,{0.,0.,0.},{0,0,0},true));
      logTab(out::SUMMARY,"No analysis of Patterson");
      logBlank(out::LOGFILE);
    }
    else if (!peak_heights_percent.size())
    {
      tncsdata.push_back(tncsdat(1,{0.,0.,0.},{0,0,0},true));
      logTab(out::SUMMARY,"No peaks in Patterson");
      logBlank(out::LOGFILE);
    }
    else if (peak_heights_percent.size() == 1)
    {
      tncsdata.push_back(tncsdat(1,{0.,0.,0.},{0,0,0},true));
      logTab(out::SUMMARY,"No non-origin peaks in Patterson");
      logTab(out::LOGFILE,"Only origin peak in Patterson");
      logBlank(out::LOGFILE);
    }
    else if (peak_heights_percent.size() > 2)
    {
      logTab(out::LOGFILE,"Patterson Top (All) = " + dtos(patterson.Patterson_top,5,1) + "%");
      (peak_heights_percent.size() == 1) ?
          logTab(out::LOGFILE,"There was 1 peak"):
          logTab(out::LOGFILE,"There were " + itos(peak_heights_percent.size()) + " peaks");
      logBlank(out::LOGFILE);

      //Remove non-origin peaks, same as xtriage
      int order_z = patterson.DERIVED_SG.group().order_z();
      sv_bool origin(peak_sites.size(),false); //keep as flag, because we will cluster to these

      std::vector<std::vector<pod::data_nmol> > expanded_non_origin_peaks(peak_sites.size());
      pod::data_nmol template_nmol(
          io_patterson_percent,
          tests_peak,
          tests_tolf,
          tests_tolo,
          tests_miss,
          mapcut_patt_percent,
          io_origin_distance //this is the separation distance for the commensurate modulations
        );

      //now erase the low ones to mapcut
      double top_non_origin_high(-999); int norigin(0);
      std::tie(top_non_origin_high,norigin) = erase_the_low_or_origin_ones(mapcut_patt_percent,false,
             peak_heights_percent,
             peak_sites,
             peak_zscore,
             expanded_non_origin_peaks, //not set
             origin); //not set
      logTab(out::LOGFILE,"Patterson Cutoff = " + dtos(mapcut_patt_percent,2) + "%");
      (top_non_origin_high > 0) ?
          logTab(out::LOGFILE,"Patterson Top (over cutoff) = " + dtos(top_non_origin_high,2) + "%"):
          logTab(out::LOGFILE,"Patterson Top (over cutoff) = (none)");
      (expanded_non_origin_peaks.size() == 1) ?
          logTab(out::VERBOSE,"There was 1 peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(expanded_non_origin_peaks.size()) + " peaks over cutoff");
      (norigin == 1) ?
          logTab(out::VERBOSE,"There was 1 non-origin peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(norigin) + " non-origin peaks over cutoff");
      logBlank(out::LOGFILE);

      //replace the peak_sites with the site that is closest to the origin
      //flag origin
      bool has_coilcoil_peaks(false);
      sv_double origin_dist(peak_sites.size(),std::numeric_limits<double>::max());
      sv_int minsym(peak_sites.size(),0);
      int cellTrans(1);
      if (peak_sites.size())
      {
        bool print_header(true);
        out::stream where = out::LOGFILE;
        //always at least one (the origin)
        for (int i = 0; i < peak_sites.size(); i++)
        {
          const dvect3& peak_site_i(peak_sites[i]);
          dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(peak_site_i);
          //init distance to origin with the input peak_site
          origin_dist[i] = std::sqrt(std::fabs((orth_i)*(orth_i)));
          af_dvect3 symmetry = patterson.DERIVED_SG.symmetry_xyz_cell(peak_site_i,cellTrans);
          for (int isym = 0; isym < symmetry.size(); isym++)
          {
            const dvect3& fracCell_i = symmetry[isym];
            dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(fracCell_i);
            double dist_to_origin(std::sqrt(std::fabs((orth_i)*(orth_i))));
            if (dist_to_origin < origin_dist[i])
            {
              origin_dist[i] = dist_to_origin;
              logTab(out::TESTING,"Replace #" + itos(i+1) + " " + dvtos(peak_sites[i],6,4) + " with #" + itos(isym) + " " + dvtos(fracCell_i,6,4));
              peak_sites[i] = fracCell_i; //replace with closest to origin
              minsym[i] = isym; //replace with closest to origin
            }
            if (dist_to_origin < io_origin_distance)
            {
              origin[i] = true;
            }
            if (dist_to_origin < io_coilcoil_distance)
            {
              has_coilcoil_peaks = true;
            }
          }
        }
      }

      //print all the peaks for od peaks also near origin
      if (has_coilcoil_peaks)
      {
        out::stream where = out::LOGFILE;
        logTableTop(where,"Coiled-Coil (Origin) Patterson Peaks");
        logTab(where,"Unit Cell: " + REFLECTIONS->UC.str());
        logTab(where,"Sorted by Height");
        logTab(where,"Distance < " + dtos(io_origin_distance,2) + " Angstroms");
        logTab(where,"#   Height Distance   Vector");
        for (int i = 0; i < peak_sites.size(); i++)
        {
          af_dvect3 symmetry = patterson.DERIVED_SG.symmetry_xyz_cell(peak_sites[i],cellTrans);
          //print with the symmetry because we need to see the full sphere
          //if the cell is less than half the origin distance then symmetry related peaks will
          //also be within the origin distance. There is no requirement for unique
          for (int isym = 0; isym < symmetry.size(); isym++)
         // if (isym == minsym[i])
          {
            const dvect3& fracCell_i = symmetry[isym];
            dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(fracCell_i);
            double dist_to_origin(std::sqrt(std::fabs((orth_i)*(orth_i))));
            //rather than using the isym of the min, we use the lowest distance and pick all the same distance
            if (dist_to_origin < io_coilcoil_distance)
            if (dist_to_origin < (origin_dist[i] +DEF_PPT))
            {
              logTab(where,snprintftos(
                  "%-3i %5.1f%c %6.1f%c:   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
                    i+1,
                    peak_heights_percent[i],'%',
                    dist_to_origin,'A',
                    fracCell_i[0],fracCell_i[1],fracCell_i[2],
                    orth_i[0],orth_i[1],orth_i[2]));
              if (!origin_removed and i == 0) break; //special case
            }
          }
        }
        logTableEnd(where);
      }

      //init expanded_non_origin_peaks
      if (peak_sites.size())
      {
        for (int i = 0; i < peak_sites.size(); i++)
        {
          dvect3 frac_j = peak_sites[i];
          for (int n = 0; n < 3; n++)
          {
            while (frac_j[n] <  0) frac_j[n]++;
            while (frac_j[n] >= 1-DEF_PPT) frac_j[n]--;
          }
          //template_nmol has frac_j
          template_nmol.setup(2,frac_j,peak_heights_percent[i],origin_dist[i]);
          expanded_non_origin_peaks[i].push_back(template_nmol);
        }
      }

      //check for coiled coil
      //checked against viewing Thomas test1 test set Pattersons manually
      {{ //now apply the coiled coil flags or not depending on user
      out::stream where = out::SUMMARY;
      logUnderLine(where,"Coiled-coils");
      bool  this_tncs_analysis_coiled_coil(false); //default
      if (expanded_non_origin_peaks.size())
      {
        out::stream where = out::LOGFILE;
        if (true) //do the test
        {
          //Site is co-opted here because it has operator< and can be used for std::set
          std::set<Site> coilcoil05,coilcoil10,coilcoil15;
          PHASER_ASSERT(io_coilcoil_distance == 16);
          //bin the peaks into 4-8, 9-12, 13-16 Angstrom sets, then test for distance between peaks in bins (avoid eg perpendicular peaks)
          for (int i = 0; i < expanded_non_origin_peaks.size(); i++)
          {
            const dvect3& fracPrev = expanded_non_origin_peaks[i][0].vector_frac;
            af_dvect3 symmetry = patterson.DERIVED_SG.symmetry_xyz_cell(fracPrev,cellTrans);
            for (int isym = 0; isym < symmetry.size(); isym++)
            {
              const dvect3& fracPrevCell = symmetry[isym];
              dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(fracPrevCell);
              double dist_to_origin = std::sqrt(std::fabs(orth_i*orth_i)); //numerical instability
              //pick those with the minimum distance only
              if (dist_to_origin < io_coilcoil_distance)
              if (dist_to_origin < (origin_dist[i]+DEF_PPT))
              {
                if (dist_to_origin > 4 and dist_to_origin < 8)
                { //put a test for height of peak in here also (Ira!)
                  coilcoil05.insert(Site(orth_i,peak_heights_percent[i],i));
                  logTab(out::TESTING,"Coil: pk=" + itos(i+1) + ",isym=" + itos(isym) + " orth=" + dvtos(orth_i,6,2) + " dist_to_origin=" + dtos(dist_to_origin,2));
                  logTab(out::TESTING,"Coil: peak05");
                }
                else if (dist_to_origin > 9 and dist_to_origin < 12)
                { //put a test for height of peak in here also (Ira!)
                  coilcoil10.insert(Site(orth_i,peak_heights_percent[i],i));
                  logTab(out::TESTING,"Coil: pk=" + itos(i+1) + ",isym=" + itos(isym) + " orth=" + dvtos(orth_i,6,2) + " dist_to_origin=" + dtos(dist_to_origin,2));
                  logTab(out::TESTING,"Coil: peak10");
                }
                else if (dist_to_origin > 13 and dist_to_origin < 16)
                { //put a test for height of peak in here also (Ira!)
                  coilcoil15.insert(Site(orth_i,peak_heights_percent[i],i));
                  logTab(out::TESTING,"Coil: pk=" + itos(i+1) + ",isym=" + itos(isym) + " orth=" + dvtos(orth_i,6,2) + " dist_to_origin=" + dtos(dist_to_origin,2));
                  logTab(out::TESTING,"Coil: peak15");
                }
              }
            }
          }
          bool crossvec1(false),crossvec2(false);
          std::set<Site> coil05,coil10,coil15;
          logTab(out::TESTING,"Coil: 5/10/15 "+itos(coilcoil05.size())+" "+itos(coilcoil10.size())+" "+itos(coilcoil15.size()));
          for (auto site05 : coilcoil05)
          {
            for (auto site10 : coilcoil10)
            {
              auto diff1 = site10.grid-site05.grid;
              double length1 = std::sqrt(std::fabs(diff1*diff1)); //numerical stability
              logTab(out::TESTING,"Coil1: orth=" + dvtos(site05.grid,6,2) + " orth=" + dvtos(site10.grid,6,2) + " diff1=" + dvtos(diff1,6,2) + " length=" + dtos(length1,2));
              if (length1 > 4 and length1 < 7)
              {
                if (site05.grid[0] < 0) site05.grid *= -1.; //remove centrosymmetry
                if (site10.grid[0] < 0) site10.grid *= -1.; //remove centrosymmetry
                coil05.insert(site05);
                coil10.insert(site10);
                crossvec1 = true;
                logTab(out::TESTING,"Coil1: " + btos(crossvec1));
              }
            }
          }
          for (auto site10 : coilcoil10)
          {
            for (auto site15 : coilcoil15)
            {
              auto diff2 = site10.grid-site15.grid;
              double length2 = std::sqrt(std::fabs(diff2*diff2)); //numerical stability for small value
              logTab(out::TESTING,"Coil2: orth=" + dvtos(site10.grid,6,2) + " orth=" + dvtos(site15.grid,6,2) + " diff2=" + dvtos(diff2,6,2) + " length=" + dtos(length2,2));
              if (length2 > 4 and length2 < 7)
              {
                if (site15.grid[0] < 0) site15.grid *= -1.; //remove centrosymmetry
                if (site10.grid[0] < 0) site10.grid *= -1.; //remove centrosymmetry
                coil15.insert(site15);
                coil10.insert(site10);
                crossvec2 = true;
                logTab(out::TESTING,"Coil2: " + btos(crossvec2));
              }
            }
          }
          //if (crossvec1 and crossvec2) //at least one in each category
          //if (crossvec1) //just the first
          if (crossvec1 or crossvec2) //at least one in one category
          {
            logTab(where,"Coiled-coil detected");
            logTab(where,"Peaks associated with coiled-coil will be excluded from further analysis");
            logWarning("Possible coiled coil or other regular structure");
            this_tncs_analysis_coiled_coil = true;
            logTableTop(where,"Coiled Coil Peaks");
            logTab(where,"Height Distance   Vector");
            for (auto site05 : coil05)
            {
              dvect3 frac_i(REFLECTIONS->UC.fractionalization_matrix()*site05.grid);
              dvect3 orth_i(site05.grid);
              logTab(where,snprintftos(
                "%5.1f%c %6.1f :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
                site05.value,'%',
                std::sqrt(std::fabs((orth_i)*(orth_i))), //numerical stability for small values
                frac_i[0],frac_i[1],frac_i[2],
                orth_i[0],orth_i[1],orth_i[2]));
            }
            for (auto site10 : coil10)
            {
              dvect3 frac_i(REFLECTIONS->UC.fractionalization_matrix()*site10.grid);
              dvect3 orth_i(site10.grid);
              logTab(where,snprintftos(
                "%5.1f%c %6.1f :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
                site10.value,'%',
                std::sqrt(std::fabs((orth_i)*(orth_i))), //numerical stability for small values
                frac_i[0],frac_i[1],frac_i[2],
                orth_i[0],orth_i[1],orth_i[2]));
            }
            for (auto site15 : coil15)
            {
              dvect3 frac_i(REFLECTIONS->UC.fractionalization_matrix()*site15.grid);
              dvect3 orth_i(site15.grid);
              logTab(where,snprintftos(
                "%5.1f%c %6.1f :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
                site15.value,'%',
                std::sqrt(orth_i*orth_i),
                frac_i[0],frac_i[1],frac_i[2],
                orth_i[0],orth_i[1],orth_i[2]));
            }
            logTableEnd(where);
          }
          else
          {
            logTab(where,"No coiled-coil detected");
          }
          logBlank(where);
        }
        else
        {
          logTab(where,"No coiled-coil test undertaken");
        }
      }
      if (io_coiled_coil != 'U')
      {
        logTab(where,"Coiled-coil as specified by user: " + btos(io_coiled_coil == 'Y'));
      }
      else
      {
        logTab(where,"Coiled-coil as specified by analysis: " + btos(this_tncs_analysis_coiled_coil));
      // the coiled coil flag is for the user only, we do not set it here because it is too unreliable
      //  input.value__boolean.at(".phasertng.coiled_coil.").set_value(this_tncs_analysis_coiled_coil);
      }
      logBlank(where);
      }}

      //now erase the low ones
      //we don't need the peaks between patterson_percent and mapcut because we will interpolate directly
      std::tie(top_non_origin_high,norigin) = erase_the_low_or_origin_ones(io_patterson_percent,false,
             peak_heights_percent,
             peak_sites,
             peak_zscore,
             expanded_non_origin_peaks,
             origin);
      logTab(out::LOGFILE,"Patterson Cutoff = " + dtos(io_patterson_percent,2) + "%");
      (top_non_origin_high > 0) ?
          logTab(out::LOGFILE,"Patterson Top (over cutoff) = " + dtos(top_non_origin_high,2) + "%"):
          logTab(out::LOGFILE,"Patterson Top (over cutoff) = (none)");
      (expanded_non_origin_peaks.size() == 1) ?
          logTab(out::VERBOSE,"There was 1 peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(expanded_non_origin_peaks.size()) + " peaks over cutoff");
      (norigin == 1) ?
          logTab(out::VERBOSE,"There was 1 non-origin peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(norigin) + " non-origin peaks over cutoff");
      logBlank(out::LOGFILE);

      //now erase the non-origin ones
      //call a second time to get the statistics separately - this could all be one call
      std::tie(top_non_origin_high,norigin) = erase_the_low_or_origin_ones(io_patterson_percent,true,
             peak_heights_percent,
             peak_sites,
             peak_zscore,
             expanded_non_origin_peaks,
             origin);
      logTab(out::VERBOSE,"Erase Origin");
      (top_non_origin_high > 0) ?
          logTab(out::VERBOSE,"Patterson Top (non-origin over cutoff) = " + dtos(top_non_origin_high,2) + "%"):
          logTab(out::VERBOSE,"Patterson Top (non-origin over cutoff) = (none)");
      (expanded_non_origin_peaks.size() == 1) ?
          logTab(out::VERBOSE,"There was 1 peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(expanded_non_origin_peaks.size()) + " peaks over cutoff");
      (norigin == 1) ?
          logTab(out::VERBOSE,"There was 1 non-origin peak over cutoff") :
          logTab(out::VERBOSE,"There were " + itos(norigin) + " non-origin peaks over cutoff");
      logBlank(out::VERBOSE);

      if (expanded_non_origin_peaks.size() == 0)
      {
        logTab(out::LOGFILE,"There were no interesting non-origin Patterson peaks");
        logBlank(out::LOGFILE);
      }
      else
      {
        int ncs_peaks = expanded_non_origin_peaks.size();
        out::stream where = out::LOGFILE;
        //first table on height
        logTableTop(where,"Patterson Peaks");
        logTab(where,"Sorted by Height");
        logTab(where,"Height Distance   Vector");
        for (int i = 0; i < expanded_non_origin_peaks.size(); i++)
        {
          dvect3 frac_i(expanded_non_origin_peaks[i][0].vector_frac);
          dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(frac_i);
          logTab(where,snprintftos(
              "%5.1f%c %6.1f :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
              expanded_non_origin_peaks[i][0].vector_height,'%',
              expanded_non_origin_peaks[i][0].vector_distance,
              frac_i[0],frac_i[1],frac_i[2],
              orth_i[0],orth_i[1],orth_i[2]));
        }
        logTableEnd(where);
        //extend with symmetry
        bool extend_with_symmetry(false);
        if (extend_with_symmetry)
        {
          double tol=1.0e-06;
          for (int i = 0; i < ncs_peaks; i++)
          {
            sv_dvect3 symset;
            for (int isym = 0; isym < order_z; isym++)  //not identity
            {
              dvect3 frac_i = patterson.DERIVED_SG.doSymXYZ(isym,expanded_non_origin_peaks[i][0].vector_frac);
              for (int n = 0; n < 3; n++)
              {
                while (frac_i[n] < 0) frac_i[n]++;
                while (frac_i[n] >= 1-DEF_PPT) frac_i[n]--;
              }
              bool duplicate(false);
              for (int s = 0; s < symset.size(); s++)
              {
                dvect3 frac_j = symset[s];
                for (int n = 0; n < 3; n++)
                {
                   while (frac_j[n] < 0) frac_j[n]++;
                   while (frac_j[n] >= 1-DEF_PPT) frac_j[n]--;
                }
                if (std::fabs(frac_i[0]-frac_j[0]) < tol and
                    std::fabs(frac_i[1]-frac_j[1]) < tol and
                    std::fabs(frac_i[2]-frac_j[2]) < tol)
                { duplicate = true; }
              }
              if (!duplicate)
              {
                symset.push_back(frac_i);
              }
            }
            for (int s = 0; s < symset.size(); s++)
            {
              double min_origin_dist(std::numeric_limits<double>::max());
              dvect3 min_frac_i;
              dvect3 cellTrans;
              for (cellTrans[0] = -0; cellTrans[0] <= 0; cellTrans[0]++)
              for (cellTrans[1] = -0; cellTrans[1] <= 0; cellTrans[1]++)
              for (cellTrans[2] = -0; cellTrans[2] <= 0; cellTrans[2]++)
              {
                dvect3 frac_i = symset[s]+cellTrans;
                dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(frac_i);
                double origin_dist(std::sqrt((std::fabs((orth_i)*(orth_i)))));
                if (origin_dist < min_origin_dist)
                {
                  min_origin_dist = origin_dist;
                  min_frac_i = frac_i;
                }
              }
              template_nmol.setup(2,min_frac_i,expanded_non_origin_peaks[i][0].vector_height,min_origin_dist);
              expanded_non_origin_peaks[i].push_back(template_nmol);
            }
          }
          //second table on distance
          logUnderLine(where,"Patterson Peaks Expanded by Symmetry");
          logTab(where,"Sorted by distance from origin in unit cell [(0,0,0),(1,1,1)]");
          logTab(where,"Height Distance   Vector");
          for (int i = 0; i < expanded_non_origin_peaks.size(); i++)
          {
            std::sort(expanded_non_origin_peaks[i].begin(),expanded_non_origin_peaks[i].end(),pod::sort_nmol_distance);
            dvect3 frac_i(expanded_non_origin_peaks[i][0].vector_frac);
            dvect3 orth_i = REFLECTIONS->UC.orthogonalization_matrix()*(frac_i);
            logTab(where,snprintftos(
                "%5.1f%c %5.1f  :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)",
                expanded_non_origin_peaks[i][0].vector_height,'%',
                expanded_non_origin_peaks[i][0].vector_distance,
                frac_i[0],frac_i[1],frac_i[2],
                orth_i[0],orth_i[1],orth_i[2]));
          }
          logBlank(where);
        }
      }

     //bool tncs_vector_set(false);
      if (!expanded_non_origin_peaks.size())
      {
        out::stream where = out::SUMMARY;
        logBlank(where);
        logTableTop(where,"tNCS Order Ranking");
        logTab(where,"No tNCS found in Patterson");
        logTableEnd(where);
        analysis.data.push_back(template_nmol); //for python
      }
      else if (expanded_non_origin_peaks.size())
      {
       // cctbx::sgtbx::site_symmetry this_site_sym(REFLECTIONS->UC,patterson.DERIVED_SG.group,tncs_vector);
       // int M = patterson.DERIVED_SG.group().order_z()/this_site_sym.multiplicity();
       // if (M > 1) tncs_vector = this_site_sym.exact_site(); // Put on exact site before perturbing

      //edit the map so that only the peaks are present, delete all the noise
        int maxi(0);
        std::vector<pod::frequencies> frequency_peaks = patterson.frequency_peaks;
        double io_freq_cutoff = input.value__percent.at(".phasertng.tncs_order.frequency_cutoff.").value_or_default_fraction();
        double topstrength(0);
        if (frequency_peaks.size())
          topstrength = frequency_peaks[0].strength;
        if (topstrength > 0)
        {
          for (maxi = 0; maxi < frequency_peaks.size(); maxi++)
          {
            frequency_peaks[maxi].strength /= topstrength;
            frequency_peaks[maxi].strength *= 100;
            if (frequency_peaks[maxi].strength < io_freq_cutoff)
            {
               break;
            }
          }
        }
        logTab(extra,"Frequency removed for low strength =" + itos(maxi) + " (< " + dtos(io_freq_cutoff) +")");
        frequency_peaks.erase(frequency_peaks.begin()+maxi,frequency_peaks.end());
        logTab(extra,"Frequency peaks =" + itos(frequency_peaks.size()));

        //miller index duplicate search
        if (true)
        {//memory scope
          sv_bool top_site(frequency_peaks.size(),true);
          for (int i = 0; i < frequency_peaks.size(); i++)
          {
            for (int i_prev = 0; i_prev < i; i_prev++)
            {
              ivect3 diff_miller = frequency_peaks[i_prev].index-frequency_peaks[i].index;
              if (diff_miller == ivect3(0,0,0))
              {
                top_site[i] = false;
              }
            }
          }
          for (int i = frequency_peaks.size()-1; i >= 0; i--)
          {
            if (!top_site[i])
            {
              frequency_peaks.erase(frequency_peaks.begin()+i);
            }
          }
        }//memory scope

        if (true) //delete the frequencies that are mixed values or boring (0 or 1)
        {
          for (int i = frequency_peaks.size()-1; i >= 0; i--)
          {
            bool match(true);
            for (int s = 0; s < 3; s++)
            {
              int nmol = frequency_peaks[i].index[s];
              if (nmol)
              {
                if (frequency_peaks[i].nmol == 0)
                  frequency_peaks[i].nmol = nmol;
                else
                  match = (match and (frequency_peaks[i].nmol == nmol));
              }
            }
            if (!match or frequency_peaks[i].nmol <= 1)
            {
              logTab(extra,"Erase Frequency mixed/boring =" + ivtos(frequency_peaks[i].index));
              frequency_peaks.erase(frequency_peaks.begin()+i);
            }
            else
            {
              logTab(extra,"Keep Frequency =" + ivtos(frequency_peaks[i].index));
            }
          }
          logTab(extra,"Frequency peaks =" + itos(frequency_peaks.size()));
        }

        for (int i = 0; i < frequency_peaks.size(); i++)
        {
          logTab(extra,"Frequency = " + ivtos(frequency_peaks[i].index) + " strength=" + dtos(frequency_peaks[i].strength));
        }
        logBlank(extra);

       //do below even if there are no frequency peaks
       //peaks without corresponding frequencies will be orphan non-commensurates
        {
          analysis.data.clear();
          //need to find the relationship between the frequency peaks and the vectors in the Patterson
          //frequency peaks will relate to commensurate modulations
          //left overs will be designated TNCSO 2
          std::set<std::string> vector_sets;
          sv_bool used_expanded_non_origin_peaks(expanded_non_origin_peaks.size(),false);
          for (int p = 0; p < expanded_non_origin_peaks.size(); p++)
          {
            phaser_assert(expanded_non_origin_peaks[p].size());
            logTab(extra,"Non Origin peak #" + itos(p+1));
          //  pod::data_nmol best_analysis = expanded_non_origin_peaks[p][0];
           // dvect3 orthvec = REFLECTIONS->UC.orthogonalization_matrix()*(expanded_non_origin_peaks[p][0].vector_frac);
           // best_analysis.shortdist = std::sqrt(orthvec*orthvec);
           // logTab(extra,"Initial best");
            //logTab(extra,best_analysis.log_nmol_analysis());
            for (int q = 0; q < expanded_non_origin_peaks[p].size(); q++)
            {
              logTab(extra,"Non Origin peak expanded #" + itos(q+1) + " " + dvtos(expanded_non_origin_peaks[p][q].vector_frac));
              for (int i = 0; i < frequency_peaks.size(); i++)
              {
                sv_int factors = factors_of(frequency_peaks[i].nmol); //includes maxTNCSO
                logTab(extra,"Freq peak #" + itos(i) + "=" + ivtos(frequency_peaks[i].index) + " factors = " + ivtos(factors));
                //sv_int factors(1,frequency_peaks[0].nmol); //includes maxTNCSO
                for (int f = 0; f < factors.size(); f++)
                {
                  pod::data_nmol this_analysis = expanded_non_origin_peaks[p][q]; //inits to max, 1
                  this_analysis.order = std::round(((1./factors[f])*frequency_peaks[i].nmol));
                  for (int s = 0; s < 3; s++)
                  {
                    this_analysis.freq.index[s] = (1./factors[f])*frequency_peaks[i].index[s];
                  }
                  this_analysis.freq.strength = frequency_peaks[i].strength;
                  this_analysis.freq.distance = frequency_peaks[i].distance;
                  bool good(true);//make sure all non-zero elements are multiplied
                  logTab(extra,"factor #" + itos(f) + " freq = " + ivtos(this_analysis.freq.index));
                  logTab(extra,"factor #" + itos(f) + " vector = " + dvtos(this_analysis.vector_frac));
                  for (int s = 0; s < 3; s++)
                  {
                    bool frac0(std::fabs(this_analysis.vector_frac[s]) < DEF_PPT);
                    bool frac1(std::fabs(this_analysis.vector_frac[s]-1) < DEF_PPT);
                    bool cond1(this_analysis.freq.index[s] and (frac0 or frac1));
                    bool cond2(!this_analysis.freq.index[s] and !(frac0 or frac1));
                    logTab(extra,"index = " + itos(s+1) + " freq>0 and vec=1,0 " + btos(cond1) + " freq=0 and !(vec=0,1) " + btos(cond2) + " vec=0==" + btos(frac0) + " vec=1==" + btos(frac1));
                    if (cond1 or cond2)
                    {
                      good = false;
                    }
                  }
                  logTab(extra,"factor #" + itos(f) + " good = " + btos(good));
                  if (!good) break;
                  logTab(extra,"good - continue to this analysis");

                  {{
                  dvect3 vtolfrac(0,0,0);
                  for (int s = 0; s < 3; s++)
                  {
                    //vtolfrac should be close to cell translation if commensurate
                    //but not necessarily first, eg if vector_frac is 2 of 7 (hyp1)
                    vtolfrac[s] = this_analysis.order*this_analysis.vector_frac[s];
                    //frac[s] = this_analysis.freq[s]*this_analysis.vector_frac[s];
                    vtolfrac[s] = std::fabs(vtolfrac[s] - std::round(vtolfrac[s]));
                    //how far out for whatever cell translation
                    double cell_multiplicity = std::max(1.,std::round(vtolfrac[s])); //2 for above example
                    vtolfrac[s] /= cell_multiplicity; //how far out for first cell
                  }
                  dvect3 vtolorth = REFLECTIONS->UC.orthogonalization_matrix()*(vtolfrac);
                  this_analysis.tolfrac = std::sqrt(vtolfrac*vtolfrac);
                  this_analysis.tolorth = std::sqrt(vtolorth*vtolorth);
                  }}
                  //now store the extra values that will be used for commensurate tests
                  //correct vector so that is an exact cell divisor, ie tol is zero
                  phaser_assert(this_analysis.order > 1);
                  for (int repeat = 1; repeat < this_analysis.order; repeat++)
                  {
                    dvect3 frac = double(repeat)*this_analysis.exact_vector();
                    for (int s = 0; s < 3; s++)
                    {
                      while (frac[s] < 0) frac[s]++;
                      while (frac[s] >= 1-DEF_PPT) frac[s]--;
                    }
                    dvect3 orth = REFLECTIONS->UC.orthogonalization_matrix()*(frac);
                    double dist = std::sqrt(orth*orth);
                    this_analysis.shortdist = std::min(dist,this_analysis.shortdist); //initialized to max
                    double mapperc = patterson.eight_point_interpolation_percent(frac);
                    //see of there is a peak within cooee of the vector multiples
                    for (int j = 0; j < peak_sites.size(); j++) //truncated to interesting
                    {
                      double height_j = peak_heights_percent[j];
                      dvect3 site_j = peak_sites[j];
                      if (height_j > mapperc) //interpolation may be better already
                      {
                        for (int isym = 0; isym < order_z; isym++)
                        {
                          dvect3 frac_j = patterson.DERIVED_SG.doSymXYZ(isym,site_j);
                          //first try  without cell translations first
                          dvect3 fracCell_j = frac_j;
                          dvect3 orth_j = REFLECTIONS->UC.orthogonalization_matrix()*(fracCell_j);
                          double dist_to_orth(std::sqrt(std::fabs((orth_j-orth)*(orth_j-orth))));
                          if (dist_to_orth < template_nmol.TEST_TOLO and height_j > mapperc)
                          {
                            mapperc = height_j;
                            goto end_loop; //because peaks are sorted highest first, this will be the best on offer
                          }
                          dvect3 cellTrans;
                          for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
                          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
                          for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
                          if (cellTrans != dvect3(0,0,0))//above
                          {
                            dvect3 fracCell_j = frac_j + cellTrans;
                            dvect3 orth_j = REFLECTIONS->UC.orthogonalization_matrix()*(fracCell_j);
                            double dist_to_orth(std::sqrt(std::fabs((orth_j-orth)*(orth_j-orth))));
                            if (dist_to_orth < template_nmol.TEST_TOLO and height_j > mapperc)
                            {
                              mapperc = height_j;
                              goto end_loop; //because peaks are sorted highest first
                            }
                          }
                        }
                      }
                    }
                    end_loop:
                    this_analysis.add_vector(frac,mapperc);
                  }
                  logTab(extra,"This analysis");
                  logTab(extra,this_analysis.log_nmol_analysis());
                  if (this_analysis.shortdist > 0)
                    //commensurate test below tests shordist > io_origin_distance,
                    //shortdist > zero is absolute minimum, don't even store
                  {
                    bool commensurate = (this_analysis.commensurate());
                    if (!commensurate)
                      this_analysis.hack_vectorset(); //transfer vector_frac to vectorset
                    std::stringstream s;
                    s.precision(3);
                    s << "[";
                    auto sorted_vectorset = this_analysis.vectorset;
                    std::sort(sorted_vectorset.begin(),sorted_vectorset.end());
                    for (auto& v : sorted_vectorset)
                    {
                      dvect3 vec = v.vec;
                      for (int i = 0; i < 3; i++) if (std::fabs(vec[i]) < DEF_PPT) vec[i] = 0;
                      s << "[(" << vec[0] << "," << vec[1] << "," << vec[2] << "),";
                      vec = REFLECTIONS->UC.orthogonalization_matrix()*(vec);
                      for (int i = 0; i < 3; i++) if (std::fabs(vec[i]) < DEF_PPT) vec[i] = 0;
                      s << "(" << vec[0] << "," << vec[1] << "," << vec[2] << "),";
                      s << v.pk << "], ";
                    }
                    s << "]";
                    std::string vectorset_str = s.str();
                    //selection string to only print interesting things to the phil files
                    //if the vector set is the same,
                    // and the results of the commensurate/noncommensurte/nmol analysis are the same
                    // then ignore ones that are sorted lower down the list
                    std::string new_string_test = itos(this_analysis.order) + vectorset_str;
                    if (this_analysis.order != 2) //because 2 commensurate and nonc are the same, in effect
                      new_string_test += btos(commensurate);
                    bool new_vector_set = vector_sets.find(new_string_test) == vector_sets.end();
                    //only output the ones for which the vector sets are different
                    //already sorted with optimal first
                    vector_sets.insert(new_string_test);
                    if (new_vector_set)
                    {
                      dvect3 frac = this_analysis.vector_frac;
                      for (int i = 0; i < 3; i++) if (std::fabs(frac[i]) < DEF_PPT) frac[i] = 0;
                      analysis.data.push_back(this_analysis);
                      indicated = true;
                      if (this_analysis.order == 2)
                        used_expanded_non_origin_peaks[p] = true;
                      logTab(extra,"New vector set");
                      logBlank(extra);
                    }
                  }
                  else
                    logTab(extra,"Short distance");
                }
              }
            }
          }


          logTab(extra,"Add other orphans as order 2"); //eg Exmerge with p1 data
          for (int p = 0; p < used_expanded_non_origin_peaks.size(); p++)
          {
            if (!used_expanded_non_origin_peaks[p])
            {
              logTab(extra,"Add peak not used with order 2: " + itos(p+1));
              pod::data_nmol this_analysis = expanded_non_origin_peaks[p][0];
              this_analysis.order = 2;
              analysis.data.push_back(this_analysis);
              indicated = true;
            }
          }

          std::sort(analysis.data.begin(),analysis.data.end(),pod::sort_data_nmol());
          std::reverse(analysis.data.begin(),analysis.data.end());

          {//memory
          out::stream where = out::VERBOSE;
          logChevron(where,"Translational NCS Analysis: All");
          logTabArray(where,analysis.logTncsAnalysis());
          } //memory

          //remove the centres of symmetry due to space group expansion
          //eg hyp1 with c2 -> p1
          //order 2 comes out as the top
#if 0
          //No! 3it5, p21 with 0.5 0 0.5 looks like centre of symmetry and removed
          int m(0);
          for (int n = 0; n < analysis.data.size(); n++)
            if (!(analysis[n].max_peak_in_complete_set() > 100-DEF_PPM and analysis[n].order == 2))
              analysis[m++] = analysis[n];
          if (m < analysis.data.size())
          {
            logChevron(out::LOGFILE,"Peaks at centre of symmetry deleted from analysis");
            logTabArray(out::LOGFILE,analysis.logTncsAnalysis(m));
            analysis.data.erase(analysis.data.begin()+m,analysis.data.end());
            logWarning("Analysis shows centre of symmetry in the data");
          }
#endif

          for (int n = 0; n < analysis.data.size(); n++)
          { //zeroed
            for (int x = 0; x < 3; x++)
              if (std::fabs(analysis[n].vector_frac[x]) < DEF_PPM)
                analysis[n].vector_frac[x] = 0;
          }


          out::stream where(out::LOGFILE);
          {
            logUnderLine(where,"tNCS Vector and Multiplicity Analysis");
            logBlank(where);
            logTab(where,"tNCS order from Patterson spatial frequency analysis");
            {
            logTab(where,"Commensurate modulation conditions:");
            logTab(where,"Commensturate modulation indicated if (1 and (2 or (3 and 4))):");
            logTab(where,"(1) Complete set of modulation vectors:");
            logTab(where,"More than " + dtos(template_nmol.TEST_MISS) + "% of tNCS vectors for modulation");
            logTab(where,"are present in the Patterson");
            logTab(where,"(2) Strong modulation:");
            logTab(where,"Indicator is strong when > " + itos(int(template_nmol.TEST_PEAK)));
            logTab(where,"(3) Good fractional precision:");
            logTab(where,"The maximum fractional difference (in x, y, or z) between the freq*tNCS-vector");
            logTab(where,"and an exact unit cell repeat");
            logTab(where,"Indicator is strong when < " + dtos(template_nmol.TEST_TOLF));
            logTab(where,"(4) Good orthogonal precision:");
            logTab(where,"The length of the vector difference between freq*tncs-vector and a unit cell");
            logTab(where,"repeat in orthogonal Angstroms");
            logTab(where,"Indicator is strong when tolorth < " + dtos(template_nmol.TEST_TOLO));
            }
            logBlank(where);
            logTableTop(where,"Translational NCS Analysis");
            logTabArray(where,analysis.logTncsAnalysis());
            if (true)
            {
              //erase duplicate TNCSOs in analysis if they are commensurate
              //TNCSO = 2 is for non-commensurate
              //  Previously commented not do do this, see 3teq, different commensurate modulations, but this is now false
              //  Do do this!! see 3ak5
              for (int n = 0; n < analysis.data.size(); n++)
              {
                analysis.data[n].flag = true;//the one of interest
                for (int m = 0; m < n; m++) //look above in list for match
                {
                  if (analysis.data[n].order == analysis.data[m].order and
                      analysis.data[n].vector_height == analysis.data[m].vector_height and
                      analysis.data[n].vector_frac == analysis.data[m].vector_frac)
                  {
                    analysis.data[n].flag = false;
                  }
                }
              }
              int num(0);
              for (int n = 0; n < analysis.data.size(); n++)
                if (analysis.data[n].flag) //sort this one to the front
                  analysis.data[num++] = analysis.data[n];
              analysis.data.erase( analysis.data.begin()+num, analysis.data.end());
            }
            for (int n = 0; n < analysis.data.size(); n++)
            {
              ivect3 parity = analysis[n].freq.index;
              if (parity == ivect3(0,0,0)) parity = ivect3(2,2,2);
              //this is where the top peak in the final table is swapped out for the shortest vector for results
              analysis[n].set_vector_frac_to_shortest_in_vectorset(); //replace the vector for the top peak
              //eg hyp1 p422, top peak's vector_frac is 2*shortest
              tncsdata.push_back(tncsdat(analysis[n].order,analysis[n].vector_frac,parity,true));
            }
            logTableEnd(where);
            logTab(where,"Highest ranked tNCS set as default for refinement");
      //decision time for TNCSO
            if (analysis.data.size() >= 1)//strong modulation, or top and only top is commensurate
            {
              out::stream where = out::SUMMARY;
              logTableTop(where,"tNCS Order Ranking");
              logTab(where,"tNCS present");
              //tncs_vector_set = true; //so that it finds ROT next
              logTab(where,snprintftos(
                  "%4s %12s %23s %23s",
                  "Rank","tNCS order","tNCS vector:fractional","tNCS vector:orthogonal"));
              for (int i = 0; i < analysis.data.size(); i++)
              {
                dvect3 orth = REFLECTIONS->UC.orthogonalization_matrix()*analysis[i].vector_frac;
                for (int v = 0; v < 3; v++) if (std::fabs(orth[v]) < DEF_PPT) orth[v] = 0;
                logTab(where,snprintftos(
                    "%4i %6s%-6i %23s %23s",
                    i+1,"",
                    analysis[i].order,
                    dvtos(analysis[i].vector_frac,7,4).c_str(),
                    dvtos(orth,7,1).c_str() ));
              }
              logTableEnd(where);
              logTab(where,"No tNCS also possible");
              logAdvisory("If structure solution fails consider other tNCS");
            }
          }
        }
      }//non origin peaks

    //always add nmol=1 as an option, last in the list
    //but store the top vector, for use in alternative modulation correction
    //AJM TODO allow more than one vector with order=1
      tncsdata.push_back(tncsdat(1,dvect3(0,0,0),{0,0,0},true));
      //below might be for putting highest vector in for other modulations
     // tncsdata.push_back(tncsdat(1,analysis[0].vector_frac,{0,0,0},true));
    }

    //if comparing this list with a tNCS calculated from a pdb file, we will need
    //all the symmetry related copies
    //but if we are correcting the data for tncs, we don't want all the symmetry related tncs
    bool check_for_unique(true);
    if (check_for_unique)
    {
      for (int tra = 0; tra < tncsdata.size(); tra++) //all tra
      {
        tncsdata[tra].unique = true;
        logTab(extra,"#" + itos(tra) + " unique");
        if (tncsdata[tra].order > 1)
        {
          dvect3 orthRef = REFLECTIONS->UC.orthogonalization_matrix()*tncsdata[tra].vector;
          for (int prev = 0; prev < tra; prev++)
          {
            if (tncsdata[tra].order == tncsdata[prev].order)
            {
              const dvect3& fracPrev = tncsdata[prev].vector;
              af_dvect3 symmetry = patterson.DERIVED_SG.symmetry_xyz_cell(fracPrev,1);
              for (int isym = 0; isym < symmetry.size(); isym++)
              {
                const dvect3& fracCell_i = symmetry[isym];
                dvect3 orthPrev = REFLECTIONS->UC.orthogonalization_matrix()*(fracCell_i);
                double delta = std::sqrt((orthPrev-orthRef)*(orthPrev-orthRef));
                if (delta < 1.0)
                {
                  tncsdata[tra].unique = false; // no hits found,this site must be unique
                  logTab(extra,"#=" + itos(prev) + " set not unique");
                }
              } //symm
            }//order
          } //previous
        }//order > 1
        logTab(extra,"#" + itos(tra) + " " + btos(tncsdata[tra].unique));
      } //vector
    }

    //clear the arrays
    input.array__int_number.at(".phasertng.tncs_analysis.result.order.").clear();
    input.array__flt_vector.at(".phasertng.tncs_analysis.result.vector.").clear();
    input.array__int_vector.at(".phasertng.tncs_analysis.result.parity.").clear();
    input.array__boolean.at(".phasertng.tncs_analysis.result.unique.").clear();

    phaser_assert(tncsdata.size() > 0); //something is set
    int n = tncsdata.size();
    auto Append = [](tncsdat x, InputCard& input) //Cool! Inline function! but does not throw!!
    {
      input.push_back(".phasertng.tncs_analysis.result.order.",x.order);
      input.push_back(".phasertng.tncs_analysis.result.vector.",x.vector);
      input.push_back(".phasertng.tncs_analysis.result.parity.",x.parity);
      input.push_back(".phasertng.tncs_analysis.result.unique.",x.unique);
    }; //Append is actually a variable, so don't forget the ;
    auto Result = [](tncsdat x, InputCard& input) //Cool! Inline function! but does not throw!!
    {
      input.value__int_number.at(".phasertng.tncs.order.").set_value(x.order);
      input.value__flt_vector.at(".phasertng.tncs.vector.").set_value(x.vector);
      input.value__int_vector.at(".phasertng.tncs.parity.").set_value(x.parity);
      input.value__boolean.at(".phasertng.tncs.unique.").set_value(x.unique);
    }; //Result is actually a variable, so don't forget the ;
    logChevron(out::LOGFILE,"Selection of default");
    auto iorder = input.value__int_number.at(".phasertng.tncs.order.").value_or_default();
    auto ivector = input.value__flt_vector.at(".phasertng.tncs.vector.").value_or_default();
    auto isel = input.value__int_number.at(".phasertng.tncs_order.selection_by_rank.").value_or_default();
    if (!input.value__int_number.at(".phasertng.tncs_order.selection_by_rank.").is_default() and
        isel >= n)
    {
      throw Error(err::FATAL,"tncs selection_by_rank out of range");
    }
    //tncs order is 1 or tncs_order > 1 and vector also set
    if ( (!input.value__int_number.at(".phasertng.tncs.order.").is_default() and
          !input.value__flt_vector.at(".phasertng.tncs.vector.").is_default()) or
          input.value__int_number.at(".phasertng.tncs.order.").value_or_default() == 1)
    { //do not include order 1 when the user is very very specific, and includes the vector
      out::stream where = out::LOGFILE;
      if (input.value__int_number.at(".phasertng.tncs.order.").value_or_default() == 1)
      {
        input.value__flt_vector.at(".phasertng.tncs.vector.").set_value(dvect3(0,0,0));
      }
      //reset ivector
      ivector = input.value__flt_vector.at(".phasertng.tncs.vector.").value_or_default();
      logTab(where,"tNCS input");
      ivect3 iparity(1,1,1);
      if (input.value__int_vector.at(".phasertng.tncs.parity.").is_default())
        logWarning("tNCS parity not set on input");
      else
        iparity = input.value__int_vector.at(".phasertng.tncs.parity.").value_or_default();
      bool iunique(true);
      if (input.value__boolean.at(".phasertng.tncs.unique.").is_default())
        logWarning("tNCS unique not set on input");
      else
        iunique = input.value__boolean.at(".phasertng.tncs.unique.").value_or_default();
      tncsdat x(iorder,ivector,iparity,iunique);
      Append(x,input);
      Result(x,input);
      logTab(where,"tNCS:" + x.str());
      logBlank(where);
    }
    else
    {
      out::stream where = out::LOGFILE;
      bool at_least_one_added = false;
      auto io_include_no_tncs = input.value__boolean.at(".phasertng.tncs_order.include_no_tncs.").value_or_default();
      auto user_tncs = !input.value__int_number.at(".phasertng.tncs.order.").is_default();
      for (int i = 0; i < n; i++)
      {
        auto x = tncsdata[i];
        if (io_include_no_tncs or (!io_include_no_tncs and x.order != 1) or n==1)
        {
          if (!user_tncs or (user_tncs and x.order == iorder))
          {
            at_least_one_added = true;
            Append(x,input);
          }
        }
      }
      if (!at_least_one_added)
      {
        for (int i = 0; i < n; i++)
        {
          auto x = tncsdata[i];
          if (io_include_no_tncs or (!io_include_no_tncs and x.order != 1) or n==1)
          {
            at_least_one_added = true;
            Append(x,input);
          }
        }
      }
      PHASER_ASSERT(at_least_one_added);
      for (int i = 0; i < n; i++)
      {
        auto x = tncsdata[i];
        if (!input.value__int_number.at(".phasertng.tncs_order.selection_by_rank.").is_default() and
                  i+1 == isel)
        { //rank selection takes precedence, only look for selection if it is defined
          Result(x,input);
          logTab(where,"Add: " + x.str());
          break; //select the one and break, in range check already done
        }
        else if (!input.value__int_number.at(".phasertng.tncs.order.").is_default() and
                  input.value__flt_vector.at(".phasertng.tncs.vector.").is_default() and
                  x.order == input.value__int_number.at(".phasertng.tncs.order.").value_or_default())
        { //pick the highest rank vector if only the order is defined
          Result(x,input);
          logTab(where,"Add: " + x.str());
          break;
        }
        else if (input.value__int_number.at(".phasertng.tncs.order.").is_default())
        { //else append all of them (order 1 special, dealt with above)
          Result(x,input); //each time overwrite to make the the default the top rank
          logTab(where,"Add: " + x.str());
          break;
        }
      }
    }
    int number_of_tncs = input.size(".phasertng.tncs_analysis.result.order.");
    PHASER_ASSERT(number_of_tncs > 0);
    input.value__int_number.at(".phasertng.tncs_analysis.number.").set_value(number_of_tncs);
    {{
    out::stream where = out::VERBOSE;
    logTab(where,"Default tNCS:");
    logTab(where,"order: " +itos(input.value__int_number.at(".phasertng.tncs.order.").value_or_default()));
    logTab(where,"vector: " +dvtos(input.value__flt_vector.at(".phasertng.tncs.vector.").value_or_default()));
    logTab(where,"parity: " +ivtos(input.value__int_vector.at(".phasertng.tncs.parity.").value_or_default()));
    logTab(where,"unique: " +btos(input.value__boolean.at(".phasertng.tncs.unique.").value_or_default()));
    input.value__boolean.at(".phasertng.tncs_analysis.indicated.").set_value(indicated);
    DAGDB.NODES[0].TNCS_INDICATED = indicated ? 'Y' : 'N';
    logTab(where,"tNCS indicated: " + btos(indicated));
    logTab(where,"Result tNCS:");
    for (int n = 0; n < number_of_tncs; n++)
    {
      logTab(where,"tNCS #" +itos(n+1));
      logTab(where,"order:  " + input.get_as_str_i(".phasertng.tncs_analysis.result.order.",n));
      logTab(where,"vector: " + input.get_as_str_i(".phasertng.tncs_analysis.result.vector.",n));
      logTab(where,"parity: " + input.get_as_str_i(".phasertng.tncs_analysis.result.parity.",n));
      logTab(where,"unique: " + input.get_as_str_i(".phasertng.tncs_analysis.result.unique.",n));
    }
    logBlank(where);
    }}

    logUnderLine(out::LOGFILE,"Files");
    if (!patterson.insufficient_data and
        !patterson.insufficient_resolution)
    {
      //these files are effectively log files, not database files, so put in pathway dir
      FileSystem MAPOUT(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".patterson.map"));
      FileSystem PDBOUT(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".patterson.pdb"));
      logFileWritten(out::LOGFILE,WriteFiles(),"Patterson Map",MAPOUT);
      logFileWritten(out::LOGFILE,WriteFiles(),"Patterson Vector",PDBOUT);
      if (WriteFiles())
      {
        patterson.WriteMap(MAPOUT);
        patterson.WritePdb(PDBOUT,io_patterson_percent);
      }
    }
    //DAGDB.set_reflid(reflid);
    //must change something every time, so shift can see change in unparse
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

      std::pair<double,int> erase_the_low_or_origin_ones(double patterson_cutoff,bool erase_origin,
            af_double& peak_heights_percent,
            af_dvect3& peak_sites,
            af_double& peak_zscore,
            std::vector<std::vector<pod::data_nmol> >& expanded_non_origin_peaks,
            sv_bool& origin)
      {
        int j(0);
        phaser_assert(peak_heights_percent.size() == peak_sites.size());
        phaser_assert(peak_heights_percent.size() == peak_zscore.size());
        phaser_assert(peak_heights_percent.size() == expanded_non_origin_peaks.size());
        phaser_assert(peak_heights_percent.size() == origin.size());
        for (int i = 0; i < peak_heights_percent.size(); i++)
        {
          if ((peak_heights_percent[i] >= patterson_cutoff) and
              (!erase_origin or (erase_origin and !origin[i])))
          {
            peak_heights_percent[j] = peak_heights_percent[i];
            peak_sites[j] = peak_sites[i];
            peak_zscore[j] = peak_zscore[i];
            expanded_non_origin_peaks[j] = expanded_non_origin_peaks[i];
            origin[j] = origin[i];
            j++;
          }
        }
        peak_heights_percent.erase(peak_heights_percent.begin()+j,peak_heights_percent.end());
        peak_sites.erase(peak_sites.begin()+j,peak_sites.end());
        peak_zscore.erase(peak_zscore.begin()+j,peak_zscore.end());
        expanded_non_origin_peaks.erase(expanded_non_origin_peaks.begin()+j,expanded_non_origin_peaks.end());
        origin.erase(origin.begin()+j,origin.end()); //BUGFIX after Ira, replicate trunk correction
        double tncs_analysis__percent__top_non_origin_high = -999;
        int norigin(0);
        bool set_top_non_origin_high(false);
        for (int i = 0; i < peak_sites.size(); i++)
        {
          if (!origin[i])
          {
            norigin++;
            if (!set_top_non_origin_high)
            {
              tncs_analysis__percent__top_non_origin_high = peak_heights_percent[i];
              set_top_non_origin_high = true;
            }
          }
        }
        return { tncs_analysis__percent__top_non_origin_high, norigin };
      }

} //end namespace phaser
