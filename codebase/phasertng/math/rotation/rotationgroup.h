#ifndef __phasertng_rotationgroup__
#define __phasertng_rotationgroup__
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>
  typedef scitbx::af::versa<double, scitbx::af::c_grid<2> >   djmn_recursive_table_t;

namespace phasertng {

  double djmn(int&,int&,int&,double&);
  djmn_recursive_table_t   djmn_recursive_table(int&,double&);
  double djmn_recursive(int&,int,int,double&);

}//end namespace phasertng

#endif
