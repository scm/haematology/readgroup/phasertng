#! /bin/bash
mtz="toxd.mtz"
cns="toxd.cns"
if [ -f $mtz ]; then
  echo $mtz " exists"
fi
if ! [ -f $mtz ]; then
  phenix.reflection_file_converter $cns --mtz=$mtz
fi
if ! [ -f $mtz ]; then
  pdb="toxd.pdb"
  convert2mtz -hklin $cns -mtzout $mtz -pdbin $pdb
fi
if ! [ -f $mtz ]; then
  echo $mtz " file does not exist"
fi
