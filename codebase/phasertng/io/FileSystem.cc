//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/FileSystem.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/hoist.h>
#include <csignal>
#include <cstdlib>

//The filesystem class needs to be able to make directories
//because the job restoration process looks for the directories to determine if job has been run
//If we don't want the file restoration, we can just use the flat database directory
//this does not require the boost::filesystem (c++11)/std::experimental::filesystem(c++14)/std::filesystem(c++17)
//Switch between flat and subdir type database structures with PHASERTNG_USE_FLAT_FILESYSTEM


//below can also be set with a compilation flag in SConscript
//#define PHASERTNG_USE_FLAT_FILESYSTEM

#ifndef PHASERTNG_USE_FLAT_FILESYSTEM

#if BOOST_VERSION < 104600
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#elif BOOST_VERSION < 105000
#include <boost/filesystem/v3/operations.hpp>
#include <boost/filesystem/v3/fstream.hpp>
#else
#include <boost/filesystem/exception.hpp>
#include <boost/filesystem/directory.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#endif
//#include <experimental/filesystem>
//#include <filesystem>

#else

#include <cassert>
#include <iostream>
#include <string>
#include <cstring>
#ifdef _WIN32
#include <windows.h>
#else
#include <dirent.h>
#include <sys/types.h>
#include <errno.h>
#endif

#endif

namespace phasertng {

#ifndef PHASERTNG_USE_FLAT_FILESYSTEM
//below for c++11
using namespace boost::filesystem;
namespace fs = boost::filesystem;

//below for c++14
//using namespace std::experimental::filesystem;
//namespace fs = std::experimental::filesystem;

//below for c++17
//using namespace std::filesystem;
//namespace fs = std::filesystem;

#else

//include the file for the findFileWithSuffix code that is OS dependent
//for flat file structure and no requirement for boost or c++14 or c++17
#include <phasertng/io/FileSystem_CXX11.cpp>

namespace fs {
  class path
  {
    public:
    std::string str;
    path(std::string str_="") :str(str_) {}
    path(const fs::path& str_) :str(str_.str) {}
    std::string string() const { return str; }
    fs::path parent_path()
    { sv_string sv;
      hoist::algorithm::split(sv,str,"/");
      std::string p;
      for (int i= 0;i<sv.size()-1;i++) p+=sv[i]+"/";
      p.pop_back();
      return fs::path(p);
    }
    fs::path stem() {
      sv_string sv;
      hoist::algorithm::split(sv,str,"/");
      sv_string st;
      hoist::algorithm::split(st,sv.back(),".");
      std::string p; //the filename may have multiple dots before .pdb
      for (int i= 0;i<st.size()-1;i++) p+=st[i]+".";
      p.pop_back();
      return fs::path(p);
    }
    // division
    fs::path operator/(fs::path const& rhs) const
    { if (str == "") return fs::path(rhs.str); return fs::path(str + "/" + rhs.str); }
    fs::path operator+(fs::path const& rhs) const
    { if (str == "") return fs::path(rhs.str); return fs::path(str + "_" + rhs.str); } //no subdir
  };
  bool exists(const fs::path& f)
  {
     std::fstream ifs;
    ifs.open(f.str, std::fstream::in);
    bool o=ifs.is_open();
    ifs.close();
    return o;
  }
  bool is_directory(const fs::path& f) { return true; } //can't do it
  bool create_directories(const fs::path& f) { return true; } //can't do it
  bool remove(const fs::path& f) { return true; } //can't do it
}//fs

#endif

  bool FileSystem::None(std::string f) const
  { return !f.size() or f == "None"; }

  bool FileSystem::flat_filesystem()
  { //for python, directory creation flag
#ifdef PHASERTNG_USE_FLAT_FILESYSTEM
    return true;
#else
    return false;
#endif
  }

  std::string
  FileSystem::fstr() const
  {
    if (!None(database) and !None(filepath) and !None(filename))
    {
#ifdef PHASERTNG_USE_FLAT_FILESYSTEM
      fs::path fullpath = fs::path(filepath) + fs::path(filename); //no subdirectories!
#else
      fs::path fullpath = fs::path(filepath) / fs::path(filename);
#endif
      fullpath = fs::path(database) / fullpath;
      return fullpath.string();
    }
    else if (!None(database) && !None(filename))
    {
      fs::path fullpath = fs::path(database) / fs::path(filename);
      return fullpath.string();
    }
    else if (!None(database) && !None(filepath))//path to job results
    {
      fs::path fullpath = fs::path(database) / fs::path(filepath);
      return fullpath.string();
    }
    else if (!None(filepath) && !None(filename))//relative path to job results
    {
      fs::path fullpath = fs::path(filepath) / fs::path(filename);
      return fullpath.string();
    }
    else if (!None(filename))
    {
      return filename;
    }
    else if (!None(database))
    {
      return database;
    }
    throw Error(err::DEVELOPER,"empty filename");
    return "";
  }

  FileSystem::FileSystem(std::string database_, std::pair<std::string,std::string> ident )
  { SetFileSystem(database_,ident); }

  FileSystem::FileSystem(std::string filename_)
  { SetFileSystem("",{"",filename_}); }

  void FileSystem::set_filename(std::string filename_) //python
  { SetFileSystem("",{"",filename_}); }

  FileSystem::FileSystem(std::string database,std::string subdir, std::string fname)
  { SetFileSystem(database,{subdir,fname}); }

  FileSystem::FileSystem(std::tuple<std::string,std::string,std::string> tmp)
  { SetFileSystem(std::get<0>(tmp),{ std::get<1>(tmp),std::get<2>(tmp) }); }

  void
  FileSystem::SetFileSystem(std::string database_, std::pair<std::string,std::string> ident)
  {
    std::string sep;
#ifdef _WIN32 // Check if we're on Windows
        sep = "\\";
#else // Assuming Unix-like systems
        sep = "/";
#endif
    while (hoist::algorithm::ends_with(database_,sep)) {  database_.pop_back(); }
    database = full_path(database_);
    if (database.size())
      while (hoist::algorithm::starts_with(ident.first,sep)) { ident.first.erase(0,1); }
    while (hoist::algorithm::ends_with(ident.first,sep)) { ident.first.pop_back(); }
    filepath = ident.first;
    if (database.size() or filepath.size())
      while (hoist::algorithm::starts_with(ident.second,sep)) { ident.second.erase(0,1); }
    while (hoist::algorithm::ends_with(ident.second,sep)) { ident.second.pop_back(); }
    filename = ident.second;
  }

  void
  FileSystem::SetFileSystem(std::string filename_)
  { SetFileSystem("",{"",filename_}); }

  std::string
  FileSystem::qfstrq() const
  {
    std::string dbpath;
#ifdef _WIN32 // Check if we're on Windows
        dbpath = "%DAGDATABASE%";
#else // Assuming Unix-like systems
        dbpath = "$DAGDATABASE";
#endif
    std::string notquoted = fstr();
    hoist::replace_first(notquoted,fs::path(database).string(),dbpath);
    return "\""+notquoted+"\"";
  }

  bool
  FileSystem::filesystem_is_set() const
  { return !None(filename); }

  bool
  FileSystem::file_exists()
  {
    fs::path filesystem(fstr());
    return fs::exists(filesystem);
  }

  //catch the segfaults that may occur for unknown reason when creating a directory
  void signal_handler(int signal) {
    std::cerr << "Caught signal " << signal << ", segmentation fault occurred!" << std::endl;
    std::cerr << "Your system does not support c++ coded directory creation" << std::endl;
    std::cerr << "To continue using phasertng please recompile with create_directories = False in SConscript" << std::endl;
    std::exit(1);
    //cannot throw error, must exit
  }

  bool
  FileSystem::filepath_exists() //careful
  { //this is the second point that the file access is required, after checking database
#ifndef _WIN32
    signal(SIGSEGV, signal_handler); //subvert normal segmentation faults
#endif
#ifndef PHASERTNG_USE_FLAT_FILESYSTEM
    fs::path f(fstr());
    return fs::is_directory(f);
#else
    return false; //for job restoration, can't be done with flat file system
#endif
#ifndef _WIN32
    signal(SIGSEGV, SIG_DFL); //revert to normal segmentation faults
#endif
  }

  bool
  FileSystem::is_a_directory() //careful
  {
#ifndef PHASERTNG_USE_FLAT_FILESYSTEM
    fs::path f(fstr());
    return fs::is_directory(f);
#else
    return false;
#endif
  }

  std::string
  FileSystem::stem() const
  {
    fs::path filesystem(fstr());
    return filesystem.stem().string();
  }

  std::string
  FileSystem::full_path(std::string pvalue)  // unquoted
  {
//weakly_canonical(): "Returns p with symlinks resolved and the result normalized. Returns: A path composed of the result of calling the canonical() function on a path composed of the leading elements of p that exist, if any, followed by the elements of p ***that do not exist***, if any."
//cf canonical() "Converts p, ***which must exist***, to an absolute path that has no symbolic link, dot, or dot-dot elements."
//so weakly_canonical does an expansion even if the path does not exist
//    pvalue =  fs::weakly_canonical(pvalue);
    return pvalue;
  }

  std::string
  FileSystem::parent_path()
  {
    fs::path f(fstr());
    fs::path pp = f.parent_path();
    return pp.string();
  }

  void
  FileSystem::prepare_and_check_database() const
  {
#ifndef _WIN32
    signal(SIGSEGV, signal_handler); //subvert normal segmentation faults
#endif
    if (None(database))
      throw Error(err::INPUT,"database: Path not set");
    fs::path db = fs::path(database);
    //if the filesystem is broken then is_directory will segfault!!! not create_directory
    if (!fs::is_directory(db))
      fs::create_directories(db); //which is a string
    fs::path unique_tester("phasertng.test_file.delete_me.tmp");
    fs::path testfile = db / unique_tester; //parses file separators
    std::fstream iofs; //default mode is in and out
      iofs.open(testfile.string().c_str(),std::fstream::out); //must have out flags here, c_str()
    if (!iofs.is_open())
      throw Error(err::INPUT,"database: Path not writeable");
    iofs.close();
    fs::remove(testfile);
#ifndef _WIN32
    signal(SIGSEGV, SIG_DFL); //revert to normal segmentation faults
#endif
  }

  void
  FileSystem::create_directories_and_stream(std::fstream& iofs)
  {
    try
    {
      fs::path f(fstr());
      fs::path pp = f.parent_path();
      if (!fs::is_directory(pp)) //default true for flat directories
        fs::create_directories(pp);
      iofs.open(f.string().c_str(),std::fstream::out); //must have out flags here, must be c_str()
    }
    catch (...)
    { throw Error(err::FILEOPEN,fstr()); }
    if (!iofs.is_open())
      throw Error(err::INPUT,"database: Path not writeable");
  }

  void
  FileSystem::create_directories_for_file()
  {
#ifdef PHASERTNG_USE_FLAT_FILESYSTEM
    return;
#endif
    if (!database.size() and !filepath.size())
      return;
    try
    {
      fs::path f(fstr());
      fs::path pp = f.parent_path();
      if (!fs::is_directory(pp))
        fs::create_directories(pp);
    }
    catch (...)
    { throw Error(err::FILEOPEN,fstr()); }
  }

  void
  FileSystem::write(std::string txt)
  {
    std::fstream ofs;
    create_directories_and_stream(ofs);
    ofs << txt;
    ofs.close();
  }

  void
  FileSystem::write_array(af_string lines)
  {
    std::fstream ofs;
    create_directories_and_stream(ofs);
    for (auto line : lines)
      ofs << line << std::endl;
    ofs.close();
  }

  std::tuple<std::string,std::string,std::string>
  FileSystem::get_tuple()
  { return std::make_tuple(database,filepath,filename); }

  // Function to recursively walk through directories and find files with a specific extension
  void
  FileSystem::findFileWithEnding(const std::string& fileEnding)
  {
#ifndef PHASERTNG_USE_FLAT_FILESYSTEM
    fs::path fullpath = fs::path(database) / fs::path(filepath);
    auto directoryPath = fullpath.string();
    if (!fs::exists(fullpath) or !fs::is_directory(fullpath))
      throw Error(err::FILEOPEN,directoryPath);
    // Lambda to check if a file name ends with the specified ending
    auto hasEnding = [](const std::string& fileName, const std::string& ending) {
      if (fileName.length() >= ending.length())
        return (0 == fileName.compare(fileName.length() - ending.length(), ending.length(), ending));
      return false;
    };
    try
    {
      fs::directory_iterator end_iter;
      for (fs::directory_iterator dir_itr(directoryPath); dir_itr != end_iter; ++dir_itr)
        if (fs::is_regular_file(dir_itr->status()) && hasEnding(dir_itr->path().string(), fileEnding))
           filename = dir_itr->path().filename().string(); //set the last of the three to the filename
    } catch (const fs::filesystem_error& e) {
      throw Error(err::DEVELOPER,e.what());
    }
#else
    filename = findFileWithSuffix(database, fileEnding);
#endif
  }

} //phasertng
