//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
//#include <scitbx/stl/set_wrapper.h>
//#include <scitbx/stl/map_wrapper.h>
//#include <scitbx/array_family/boost_python/c_grid_flex_conversions.h>
#include <boost_adaptbx/std_pair_conversion.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_phasertng_Entries()
{
  using namespace scitbx::stl::boost_python;
  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  using namespace scitbx::boost_python::container_conversions;
}

} // namespace phasertng::<anonymous>

void init_phasertng_Entries()
{
  wrap_phasertng_Entries();
}

} // namespace boost_python
} // namespace phasertng
