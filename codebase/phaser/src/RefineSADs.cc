//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineSAD.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/sim.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::sim& tbl_sim;
}

namespace phaser {

af::shared<cmplxType> RefineSAD::calcFpart(bool selected_only)
{
  // Calculate Fpart in terms of fractional occupancy and B-factor.
  // Scattering factor will be taken account of later.
  af::shared<cmplxType> Fpart;
  af::shared<miller::index<int> > Fmiller;
  if (atoms.size()) for (unsigned r = 0; r < NREFL; r++) FApos[r] = FAneg[r] = 0;

  float1D linearTerm(atoms.size()),debye_waller_u_iso(atoms.size(),0.);
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    linearTerm[a] = symFacPCIF*atoms[a].SCAT.occupancy/M;
    if (!atoms[a].SCAT.flags.use_u_aniso_only()) debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
  }

  if (selected_only)
  {
    for (unsigned r = 0; r < NREFL; r++)
      if (!bad_data[r] && selected[r])
        Fmiller.push_back(miller[r]);
    Fpart.resize(Fmiller.size());
  }
  else
  {
    Fmiller.resize(NREFL);
    Fpart.resize(NREFL);
  }
  for (unsigned r = 0; r < Fpart.size(); r++)
    Fpart[r] = 0;

  if (!atoms.size())
  return Fpart;

  int rr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
  {
    auto this_rotMiller = rotMiller[r];
    auto this_traMiller = traMiller[r];
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
    {
      // int t = atomtype2t[atoms[a].SCAT.scattering_type]; // Could be used for clusters?
      floatType isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
      floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
      if (debye_ScaleB) isoB *= std::exp(-ssqr[r]*debye_ScaleB);
      floatType scat(linearTerm[a]*isoB); // Scattering from unit atom with B and occ
           //   symmOccBfac*=CLUSTER[t].debye(ssqr[r]); // Maybe later?
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        auto this_rotMiller_isym = this_rotMiller[isym];
        floatType anisoB(1);
        floatType anisoScat(scat);
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          anisoB = cctbx::adptbx::debye_waller_factor_u_star(this_rotMiller_isym,atoms[a].SCAT.u_star);
          anisoScat = scat*anisoB;
        }
        floatType theta = this_rotMiller_isym[0]*atoms[a].SCAT.site[0] +
                          this_rotMiller_isym[1]*atoms[a].SCAT.site[1] +
                          this_rotMiller_isym[2]*atoms[a].SCAT.site[2] +
                          this_traMiller[isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
        cmplxType sincostheta = tbl_cos_sin.get(theta); //multiplies by 2*pi internally
        floatType costheta = std::real(sincostheta);
        floatType sintheta = std::imag(sincostheta);
#else
        theta *= scitbx::constants::two_pi;
        floatType costheta = std::cos(theta);
        floatType sintheta = std::sin(theta);
#endif
        floatType anisoScat_costheta(anisoScat*costheta);
        floatType anisoScat_sintheta(anisoScat*sintheta);
        Fpart[rr] += cmplxType(anisoScat_costheta, anisoScat_sintheta);
      }//end sym
    } //end  loop over atoms
    rr++;
  } //end loop r
  else if (!selected_only)
  {
    Fpart[rr++] = 0;
  }
  return Fpart;
}

double RefineSAD::LESSTF1(std::string FINDSITES_TYPE,double dBscat, af::shared<cmplxType> Fpart, af_float mI)
{
  mI.resize(0); //because we will push_back
  double LLconst(0),LLoffset(0);
  //smaller wilsonB will make for sharpening
  dBscat = std::max(0.,WilsonB-dBscat); //possibly minB value AJM TODO
  cmplxType TWOPII(0.,scitbx::constants::two_pi);
  //int rr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
  {
    double rmI(0.);
    double bterm(std::exp(-dBscat*ssqr[r]/4.));
    int t = atomtype2t[FINDSITES_TYPE];
    double fp =  (FINDSITES_TYPE == "AX") ? 0 : fo_plus_fp[r][t]*bterm;
    double fdp = (FINDSITES_TYPE == "AX") ? 1 : AtomFdp[t]*bterm;
    //Linear approximation centered on partial structure factor, if any
    double eU(std::abs(Fpart[r]/bterm));
    if (eU == 0) // No partial substructure
    {
      FHpos[r] = FHneg[r] = 0.;
      std::pair<double,double> LdL = LdL_by_dUsqr_at_0(r,fp,fdp);
      LLconst += LdL.first;
      rmI = LdL.second;
    }
    else
    {
      FHpos[r] = cmplxType(fp,fdp)*eU;
      FHneg[r] = cmplxType(fp,-fdp)*eU;
      if (both[r])
      {
        LLconst -= acentricReflGradSAD2(r,true,false);
        rmI = -(fp*(dL_by_dReFHpos+dL_by_dReFHneg)+fdp*(dL_by_dImFHpos-dL_by_dImFHneg));
      }
      else if (!sad_target_anom_only && cent[r])
      {
        LLconst -= centricReflGradSAD2(r,true,false);
        rmI = -(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos);
      }
      else if (!sad_target_anom_only)
      {
        LLconst -= singletonReflGradSAD2(r,plus[r],true,false);
        rmI = plus[r] ? -(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos): -(fp*dL_by_dReFHneg-fdp*dL_by_dImFHneg);
      }
      double unitary_derivative = 2*eU;
      rmI /= unitary_derivative;
      LLoffset -= fn::pow2(eU)*rmI;
    }
    mI.push_back(rmI);
    //rr++;
  }
  else
  {
    mI.push_back(0); //in PattersonCoefficients, we select later
  }
  LLconst += wilson_llg;
  LLoffset += LLconst; // Correct for null hypothesis, not just mean of search
  return LLoffset;
}

std::pair<double,double> RefineSAD::LdL_by_dUsqr_at_0(unsigned r, double fp, double fdp)
{
  double LL_at_0(0.),dL_by_dUsqr(0.);
  if (both[r])
  {
    double FOneg(NEG.F[r]),FOpos(POS.F[r]);
    double FOnegSqr(fn::pow2(FOneg));
    double FOposSqr(fn::pow2(FOpos));
    double VarFOneg(fn::pow2(NEG.SIGF[r]));
    double VarFOpos(fn::pow2(POS.SIGF[r]));
    double SigmaNeg(sigDsqr[r] + VarFOneg);
    PHASER_ASSERT(SigmaNeg > 0);
    double SigmaPos(sigPlus[r] + VarFOpos + VarFOneg);
    PHASER_ASSERT(SigmaPos > 0);
    double fpSqr(fn::pow2(fp));
    double fdpSqr(fn::pow2(fdp));
    double dFOnegSqr(FOnegSqr-SigmaNeg);
    double X(2*FOneg*FOpos/SigmaPos);
    LL_at_0 = -(FOnegSqr/SigmaNeg+fn::pow2(FOneg-FOpos)/SigmaPos) +
      std::log(4*FOpos*FOneg*phasertng::tbl_ebesseli0.get(X)/(SigmaNeg*SigmaPos));
    dL_by_dUsqr =
      (4*fdpSqr*SigmaNeg*((FOnegSqr+FOposSqr)*SigmaNeg+dFOnegSqr*SigmaPos) +
      (fpSqr+fdpSqr)*dFOnegSqr*fn::pow2(SigmaPos) -
      4*FOneg*FOpos*fdpSqr*SigmaNeg*(2*SigmaNeg+SigmaPos)*phasertng::tbl_sim.get(X)) /
      fn::pow2(SigmaNeg*SigmaPos);
  }
  else if (!sad_target_anom_only && cent[r])
  {
    double FOsqr = fn::pow2(POS.F[r]);
    double V(sigDsqr[r] + 0.5*fn::pow2(POS.SIGF[r]));
    LL_at_0 = -FOsqr/(2.*V) - std::log(scitbx::constants::pi*V/2.)/2.;
    dL_by_dUsqr = (fn::pow2(fp)+fn::pow2(fdp))*(FOsqr-V) /
      (2*fn::pow2(V));
  }
  else if (!sad_target_anom_only)
  {
    double FO = plus[r] ? POS.F[r] : NEG.F[r];
    double FOsqr = fn::pow2(FO);
    double VarFO = fn::pow2(plus[r] ? POS.SIGF[r] : NEG.SIGF[r]);
    double V(sigDsqr[r] + VarFO);
    LL_at_0 = -FOsqr/V + std::log(2.*FO/V);
    dL_by_dUsqr = (fn::pow2(fp)+fn::pow2(fdp))*(FOsqr-V) /
      fn::pow2(V);
  }
  return std::pair<double,double>(LL_at_0,dL_by_dUsqr);
}

}
