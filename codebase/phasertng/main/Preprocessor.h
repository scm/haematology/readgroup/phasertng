//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_preprocessor_class__
#define __phasertng_preprocessor_class__
#include <phasertng/main/Error.h>

namespace phasertng {

class Preprocessor : public Error //store Error
{
  private:
    std::string Cards = "";
    void recursive_processor(std::istream&,int=0);
    std::string getQuotedString(std::istringstream&);

  public:
    Preprocessor() : Error() {}
    ~Preprocessor() {}

    void stream(std::istream&);
    void keywords(std::string);
    void stdio();
    std::string cards(); //blank lines removed
};

}
#endif
