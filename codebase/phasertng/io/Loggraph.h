//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Loggraph_class__
#define __phasertng_Loggraph_class__
#include <string>
#include <vector>

typedef std::vector<std::string> sv_string;

namespace phasertng {

class Loggraph
{
  public:  //members
    std::string title = "";
    sv_string graph = sv_string(0);
    std::string data_labels = "";
    std::string data_text = "";
    bool scatter = false;

  public: //constructors
    Loggraph() {}

  public:
  const std::string print() const
  {
    std::string loggraph;
    loggraph += "$TABLE : " + title + ":\n";
    loggraph += scatter ? "$SCATTER \n" : "$GRAPHS \n";
    for (int c = 0; c < graph.size(); c++)
      loggraph += ":" + graph[c] + ": ";
    loggraph += "\n$$\n";
    loggraph +=  data_labels;
    loggraph += "\n$$ loggraph $$\n";
    loggraph += data_text;
    loggraph += "$$\n";
    return loggraph;
  }

};

} //phasertng

#endif
