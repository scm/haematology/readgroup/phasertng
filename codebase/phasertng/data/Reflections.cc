//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Reflections.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/io/principal_statistics.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/main/hoist.h>
#include <phasertng/math/RiceLLG.h>
#include <phasertng/math/french_wilson.h>
#include <phasertng/math/slope_and_intercept.h>
#include <phasertng/math/subgroups.h>
#include <boost/logic/tribool.hpp>
#include <phasertng/cctbx_project/cctbx/miller/asu.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <cctbx/xray/conversions.h>
#include <phasertng/math/likelihood/pIntensity.h>
#include <cassert>
#include <scitbx/random.h>


//#define PHASERTNG_DEBUG_DOBS
//#define PHASERTNG_DEBUG_EZH
//#define PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
//#define PHASERTNG_DEBUG_SIGMAN

namespace phasertng {

  void
  Reflections::copy_and_reformat_header(const_ReflectionsPtr from)
  {
    if (!from) return;
    //this->Header::operator=( *from ); //explicit call to the operator=() function
   //public:
    MTZCOLTYPE = from->MTZCOLTYPE;
    MTZCOLNAME = from->MTZCOLNAME;
    FRENCH_WILSON = from->FRENCH_WILSON;
    INTENSITIES = from->INTENSITIES;
    RESOLUTION  = from->RESOLUTION ;
    NREFL = from->NREFL;
    iSG = SpaceGroup(from->iSG.HALL); //have to reconstruct this, the base class is not copied
    SG = SpaceGroup(from->SG.HALL); //have to reconstruct this, the base class is not copied
    UC = UnitCell(from->UC.cctbxUC);
    WAVELENGTH = from->WAVELENGTH;
    OVERSAMPLING = from->OVERSAMPLING;
    MAPORI = from->MAPORI;
    REFLID = from->REFLID;
    REFLPREP = from->REFLPREP;
    MAP = from->MAP;
    TNCS_PRESENT = from->TNCS_PRESENT;
    TWINNED = from->TWINNED;
    ANOMALOUS = from->ANOMALOUS;
    SHARPENING = from->SHARPENING;
    WILSON_K_F = from->WILSON_K_F;
    WILSON_B_F = from->WILSON_B_F;
    TNCS_ORDER = from->TNCS_ORDER;
    TNCS_ANGLE = from->TNCS_ANGLE;
    TNCS_VECTOR = from->TNCS_VECTOR;
    ANISO_PRESENT = from->ANISO_PRESENT;
    ANISO_DIRECTION_COSINES = from->ANISO_DIRECTION_COSINES;
    TNCS_RMS = from->TNCS_RMS;
    TNCS_FS = from->TNCS_FS;
    TNCS_GFNRAD = from->TNCS_GFNRAD;
    Z = from->Z;
    TOTAL_SCATTERING = from->TOTAL_SCATTERING;
    //must copy miller indices also
    this->setup_miller(from->NREFL);
    for (int r = 0; r < from->NREFL; r++)
    {
      const millnx& miller = from->get_miller(r);
      this->set_miller(miller,r);
    }
  }

  void
  Reflections::copy_and_reformat_data(const_ReflectionsPtr from)
  {
    if (!from) return;
    copy_and_reformat_header(from);
#ifdef PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
auto history = from->get_history();
for (auto item : history) std::cout << "From Header " << item << std::endl;
#endif
    if (from->LABIN.size() == 0) return;
    this->LABIN.clear();
    PHASER_ASSERT(from->LABIN.size()); //this and from are not pointers to same object
    for (auto col : from->LABIN)
    {
#ifdef PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
std::cout << "Copy " << labin::col2String(col) << std::endl;
#endif
      //ignore the cols that are not present in new data structure
      if (LABIN_ALLOWED.count(col))
      {
        //below throws if column not available
        //for example SPA column labin, not in new data structure
        //does not change column if already present ???
        type::mtzcol createcol(from->MTZCOLTYPE.at(col),from->MTZCOLNAME.at(col),col);
        this->setup_mtzcol(createcol);
        //MTZCOLTYPE and MTZCOLNAME set internally, LABIN added
#ifdef PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
std::cout << "Copy Allowed " << labin::col2String(col) << std::endl;
#endif
        if (this->MTZCOLTYPE.at(col) != 'H')
        {
          bool is_bln(this->MTZCOLTYPE.at(col) == 'B');
          bool is_int(this->MTZCOLTYPE.at(col) == 'I');
          bool is_flt(!is_bln and !is_int);
          for (int r = 0; r < this->NREFL; r++)
          {
                 if (is_flt) { this->set_flt(col,from->get_flt(col,r),r); }
            else if (is_int) { this->set_int(col,from->get_int(col,r),r); }
            else if (is_bln) { this->set_bln(col,from->get_bln(col,r),r); }
          }
        }
      }
    }
    for (auto f : FRIEDEL ) //added with setup_mtzcol
    {
#ifdef PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
std::cout << "Copy Friedel " << friedel::type2String(f) << std::endl;
#endif
      for (int r = 0; r < this->NREFL; r++)
      {
        this->set_present(f,from->get_present(f,r),r);
      }
    }
#ifdef PHASERTNG_DEBUG_COPY_AND_REFORMAT_DATA
history = this->get_history();
for (auto item : history) std::cout << "To Header " << item << std::endl;
#endif
    //if the space group has changed, recalculate these
    //ignored if column not available
    if (LABIN_ALLOWED.count(labin::CENT) and LABIN.count(labin::CENT))
      this->setup_cent(this->get_mtzcol(labin::CENT));
    if (LABIN_ALLOWED.count(labin::EPS) and LABIN.count(labin::EPS))
      this->setup_eps(this->get_mtzcol(labin::EPS));
    if (LABIN_ALLOWED.count(labin::PHSR) and LABIN.count(labin::PHSR))
      this->setup_phsr(this->get_mtzcol(labin::PHSR));
  }

  void
  Reflections::set_anomalous_amplitudes_from_mean()
  {//phaser convert_NAT_to_SAD
    assert(has_col(labin::FNAT));
    assert(has_col(labin::SIGFNAT));
    assert(has_col(labin::FPOS));
    assert(has_col(labin::SIGFPOS));
    assert(has_col(labin::FNEG));
    assert(has_col(labin::SIGFNEG));
    bool singletons(true);
    for (int r = 0; r < this->NREFL; r++)
    {
      double fnat = this->get_flt(labin::FNAT,r);
      double sigfnat = this->get_flt(labin::SIGFNAT,r);
      bool pnat = this->get_present(friedel::NAT,r);
      this->set_flt(labin::FPOS,fnat,r);
      this->set_flt(labin::FNEG,singletons ? 0 : fnat,r);
      this->set_flt(labin::SIGFPOS,sigfnat,r);
      this->set_flt(labin::SIGFNEG,singletons ? 0 : sigfnat,r);
      this->set_present(friedel::POS,pnat,r);
      this->set_present(friedel::NEG,singletons ? false : pnat,r);
    }
  }

  void
  Reflections::set_anomalous_intensities_from_mean()
  {
    assert(has_col(labin::INAT));
    assert(has_col(labin::SIGINAT));
    assert(has_col(labin::IPOS));
    assert(has_col(labin::SIGIPOS));
    assert(has_col(labin::INEG));
    assert(has_col(labin::SIGINEG));
    bool singletons(true);
    for (int r = 0; r < this->NREFL; r++)
    {
      double inat = this->get_flt(labin::INAT,r);
      double siginat = this->get_flt(labin::SIGINAT,r);
      bool pnat = this->get_present(friedel::NAT,r);
      this->set_flt(labin::IPOS,inat,r);
      this->set_flt(labin::INEG,singletons ? 0 : inat,r);
      this->set_flt(labin::SIGIPOS,siginat,r);
      this->set_flt(labin::SIGINEG,singletons ? 0 : siginat,r);
      this->set_present(friedel::POS,pnat,r);
      this->set_present(friedel::NEG,singletons ? false : pnat,r);
    }
  }

  bool
  Reflections::check_native()
  {
    //if anomalous, need to see if there is at least one interesting reflection in the neg
    bool native = true;
    if (!has_col(labin::FPOS) and !has_col(labin::IPOS))
      return native;
    //type might be ReflRowAnom with no F's
    labin::col pos = this->INTENSITIES or !this->has_col(labin::FPOS) ? labin::IPOS : labin::FPOS;
    labin::col neg = this->INTENSITIES or !this->has_col(labin::FPOS) ? labin::INEG : labin::FNEG;
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      if (!centric) //6A85 has centric (+)&(-)
      {
        if (get_present(friedel::NEG,r) and get_flt(neg,r) != 0 and
            get_present(friedel::POS,r) and (get_flt(pos,r) != get_flt(neg,r)))
        {
          native = false;
        }
      }
    }
    return native;
  }

/*
  af_string
  Reflections::VanillaReadMtz(
      std::set<labin::col> COLIN
      )
  {
    af_string output;
    std::string HKLIN = FileSystem::fstr();
    if (!HKLIN.size())
    throw Error(err::INPUT,"No reflection file for reading");

    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    Header::clear();
    //Reflections::set_base_defaults(); //very important, sets the constant parameters of class
    Header::SetHistory(mtzIn.HISTORY);
    SG = mtzIn.SG;
    NREFL = mtzIn.NREFL;
    setup_miller(mtzIn.NREFL);

    LABIN.clear();
    { //LABIN
      //select columns for reading
      //if nothing is specified, read the whole lot
      assert(MTZCOLNAME.size());
      for (auto item : MTZCOLNAME)
      {
        labin::col col = static_cast<labin::col>(item.first);
        std::string name = item.second;
        assert(MTZCOLTYPE.count(col));
        char type = MTZCOLTYPE[col];
        if (LABIN_ALLOWED.count(col) and
           (COLIN.size() == 0 or COLIN.count(col)))
        {
          //MTZCOLNAME&MTZCOLTYPE already setup but doesn't matter, maps
          this->setup_mtzcol(type::mtzcol(type,name,col));
        }
      }
      if (!LABIN.size())
      throw Error(err::FATAL,"No readable data in mtz file");
    } //LABIN

    std::string CELLCOL = "";
    { //CELLCOL
      //here, select the unitcell and wavelength of ANOMALOUS data if present
      //otherwise use the native data
      //order is Ipos, Fpos, I, F, Fmap
      if (LABIN.find(labin::IPOS) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::IPOS];
      else if (LABIN.find(labin::FPOS) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::FPOS];
      if (LABIN.find(labin::INAT) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::INAT];
      else if (LABIN.find(labin::FNAT) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::FNAT];
      else if (LABIN.find(labin::MAPF) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::MAPF];
      if (CELLCOL == "")
        throw Error(err::FATAL,"No reference data for unit cell");
      //not doing anything with isomorphous at the moment
    } //CELLCOL

    mtzIn.load_dataset(CELLCOL);
    UC = mtzIn.UC;
    WAVELENGTH = mtzIn.WAVELENGTH;

    //so that setup_data_present_init sets array sizes
    if (!REFLID.is_set_valid())
    throw Error(err::FATAL,"Input mtz file has not been prepared with phasertng");

    { //DATA
      int ncols = 3 + LABIN.size(); //HKL + data
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      int n(0);
      std::map<labin::col,int> column; //all reflections. not just the set ones
      sv_string hkl = {"H","K","L"};
      for (int i = 0; i < hkl.size(); i++)
      {
        lookup[n] = mtzIn.getCol(hkl[i]);
        column[labin::col2Enum(hkl[i])] = n; //these are special, will need for knowing which to read
        n++;
      }
      //The LABIN has to not only be allowed but also be present in the mtz file
      std::set<labin::col> LABIN_IN_MTZ;
      for (auto labin_col : LABIN)
      {
        assert(MTZCOLNAME.count(labin_col));
        try {
          lookup[n] = mtzIn.getCol(MTZCOLNAME[labin_col]);
          column[labin_col] = n; //these are special, will need for knowing which to read
          LABIN_IN_MTZ.insert(labin_col);
          n++;
        }
        catch(...)
        { output.push_back("Allowed column not in mtz: " + labin::col2String(labin_col)); }
      }
      LABIN = LABIN_IN_MTZ;
      struct labindatstruct {
        labin::col   labin_col;
        bool is_bln;
        bool is_int;
        bool is_flt;
        int  n;
        friedel::type f;
      };
      std::vector<labindatstruct> labindat;
      for (auto labin_col : LABIN)
      {
        labindatstruct dat;
        dat.labin_col = labin_col;
        assert(column.find(labin_col) != column.end());
        dat.is_bln = (MTZCOLTYPE[labin_col] == 'B');
        dat.is_int = (MTZCOLTYPE[labin_col] == 'I');
        dat.is_flt = (!dat.is_bln and !dat.is_int);
        dat.n = column[labin_col];
        dat.f = convert_labin_to_friedel(labin_col);
        labindat.push_back(dat);
      }
      //int respos = mtzIn.resetRefl();
      double all_data_lores = 0;
      double all_data_hires = std::numeric_limits<double>::max();
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        assert(!failure);
        int H(adata[column[labin::H]]);
        int K(adata[column[labin::K]]);
        int L(adata[column[labin::L]]);
        millnx hkl(H,K,L);
        set_miller(hkl,mtzr);
        double resolution = UC.cctbxUC.d(hkl);
        if (resolution > all_data_lores) all_data_lores = resolution;
        if (resolution < all_data_hires) all_data_hires = resolution;
        for (auto dat : labindat)
        {
          bool ispresent(!mtz_isnan(adata[dat.n]));
          set_present(dat.f,ispresent,mtzr);
          if (dat.is_bln)
            set_bln(dat.labin_col,(ispresent?static_cast<bool>(adata[dat.n]):false),mtzr);
          else if (dat.is_int)
            set_int(dat.labin_col,(ispresent?static_cast<int>(adata[dat.n]):0),mtzr);
          else if (dat.is_flt)
            set_flt(dat.labin_col,(ispresent?static_cast<double>(adata[dat.n]):0.),mtzr);
        }
      }
      RESOLUTION[0] = all_data_lores;
      RESOLUTION[1] = all_data_hires;
    } //DATA
    return output;
  }

  void
  Reflections::VanillaWriteMtz(
   // double CELL_SCALE = 1 //AJM cell scale!!
    )
  {
    auto hist = get_history();
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,WAVELENGTH,NREFL,hist);
    assert(LABIN.size());

    for (int r = 0; r < NREFL; r++)
    {
      mtzOut.mtzH->ref[r] = get_miller(r)[0];
      mtzOut.mtzK->ref[r] = get_miller(r)[1];
      mtzOut.mtzL->ref[r] = get_miller(r)[2];
    } //miller

    //DATA
    for (auto item : LABIN)
    {
      // NEW COLUMNS
      CMtz::MTZCOL* mtzCol = mtzOut.addCol(MTZCOLNAME.at(item), MTZCOLTYPE.at(item));
      assert(mtzCol != 0);

      friedel::type f = convert_labin_to_friedel(item);

      //for speed, make type selection first, then loop over reflections and get_present
      auto coltype = MTZCOLTYPE.at(item);
      if (coltype == 'P')
      {
        for (int r = 0; r < NREFL; r++)
        {
          if (get_present(f,r))
          { //put phases in degrees in range -180->180
            double phase = get_flt(item,r);
            while (phase > 180) phase -= 360;
            while (phase <= -180) phase += 360;
            mtzCol->ref[r] = phase;
          } //not set remains as nan
        }
      }
      else if (coltype == 'B')
      {
        for (int r = 0; r < NREFL; r++)
        {
          if (get_present(f,r))
          {
            mtzCol->ref[r] = get_bln(item,r) ? 1.0 : 0.0;
          } //not set remains as nan
        }
      }
      else if (coltype == 'I')
      {
        for (int r = 0; r < NREFL; r++)
        {
          if (get_present(f,r))
          {
            mtzCol->ref[r] = static_cast<double>(get_int(item,r));
          } //not set remains as nan
        }
      }
      else if (item != labin::H and item != labin::K and item != labin::K)
      {
        for (int r = 0; r < NREFL; r++)
        {
          if (get_present(f,r))
          {
            mtzCol->ref[r] = get_flt(item,r);
          } //not set remains as nan
        }
      }
    }

    mtzOut.Put("Phasertng Reflections");
  }

  void
  Reflections::VanillaWriteMtzOriginal()
  {
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,WAVELENGTH,NREFL,get_history());
    assert(LABIN.size());
    for (int r = 0; r < NREFL; r++)
    {
      mtzOut.mtzH->ref[r] = get_miller(r)[0];
      mtzOut.mtzK->ref[r] = get_miller(r)[1];
      mtzOut.mtzL->ref[r] = get_miller(r)[2];
    } //miller

    for (auto item : LABIN)
    {
      bool write_col = false;
      friedel::type f = convert_labin_to_friedel(item);
      if (f == friedel::NAT and ANOMALOUS) continue;
      if (f == friedel::POS and !ANOMALOUS) continue;
      if (f == friedel::NEG and !ANOMALOUS) continue;
      if (this->MAP)
      {
        write_col = (item == labin::MAPF or item == labin::MAPPH);
      }
      else
      {
        write_col = this->INTENSITIES ?
            (item == labin::INAT or item == labin::SIGINAT):
            (item == labin::FNAT or item == labin::SIGFNAT);
        if (!write_col)
          write_col = this->INTENSITIES ?
            (item == labin::IPOS or item == labin::SIGIPOS):
            (item == labin::FPOS or item == labin::SIGFPOS);
        if (!write_col)
          write_col = this->INTENSITIES ?
            (item == labin::INEG or item == labin::SIGINEG):
            (item == labin::FNEG or item == labin::SIGFNEG);
      }
      if (write_col)
      {
        CMtz::MTZCOL* mtzCol = mtzOut.addCol(MTZCOLNAME.at(item), MTZCOLTYPE.at(item));
        assert(mtzCol != 0);
        for (int r = 0; r < NREFL; r++)
          if (get_present(f,r))
            mtzCol->ref[r] = get_flt(item,r);
      }
    }
    mtzOut.Put("Phasertng Reflections");
  }
*/

  double Reflections::data_hires() const
  { return std::min(RESOLUTION[0],RESOLUTION[1]); }

  double Reflections::data_lores() const
  { return std::max(RESOLUTION[0],RESOLUTION[1]); }

  af::double2 Reflections::resolution() const
  { return RESOLUTION; }

  void Reflections::set_data_resolution()
  {
    assert(NREFL);
    af_millnx miller = get_miller_array();
    af::double2 SSQR = UC.cctbxUC.min_max_d_star_sq(miller.const_ref());
    af::double2 RESO = { cctbx::uctbx::d_star_sq_as_d(SSQR[0]),
                         cctbx::uctbx::d_star_sq_as_d(SSQR[1]) };
    RESOLUTION[1] = std::min(RESO[0],RESO[1]);
    RESOLUTION[0] = std::max(RESO[0],RESO[1]);
  }

  af_int
  Reflections::numinbin() const
  {
    af_int numinbin(numbins(),0);
    assert(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      const int  s = get_int(labin::BIN,r);
      numinbin[s]++;
    }
    return numinbin;
  }

  int
  Reflections::numbins() const
  {
    int nbins(0);
    assert(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      nbins = std::max(nbins,get_int(labin::BIN,r)+1);
    }
    return nbins;
  }

  af_double
  Reflections::bin_hires(double hires) const
  {
    af_double bin_hires_array(numbins(),std::numeric_limits<double>::max());
    assert(NREFL);
    int count(0);
    for (int r = 0; r < NREFL; r++)
    {
      const int  ibin = get_int(labin::BIN,r);
      const double  ssqr = this->work_ssqr(r);
      assert(ibin >= 0);
      double reso = 1.0/std::sqrt(ssqr);
      if (reso > hires)
      {
        bin_hires_array[ibin] = std::min(bin_hires_array[ibin],reso);
        count = std::max(count,ibin);
      }
    }
    count++;
    bin_hires_array.erase(bin_hires_array.begin()+count,bin_hires_array.begin()+numbins());
    return bin_hires_array;
  }

  af_double
  Reflections::bin_lores(double hires) const
  {
    af_double bin_lores_array(numbins(),0);
    assert(NREFL);
    int count(0);
    for (int r = 0; r < NREFL; r++)
    {
      const int  ibin = get_int(labin::BIN,r);
      const double  ssqr = this->work_ssqr(r);
      double reso = 1.0/std::sqrt(ssqr);
      if (reso > hires)
      {
        bin_lores_array[ibin] = std::max(bin_lores_array[ibin],reso);
        count = std::max(count,ibin);
      }
    }
    count++;
    bin_lores_array.erase(bin_lores_array.begin()+count,bin_lores_array.begin()+numbins());
    return bin_lores_array;
  }

  af_double
  Reflections::bin_midres(double hires) const
  {
    assert(NREFL);
    af_double bin_hires_array = bin_hires(hires);
    af_double bin_lores_array = bin_lores(hires);
    af_double bin_midres_array;
    for (int ibin = 0; ibin < bin_hires_array.size(); ibin++)
    {
      assert(ibin < bin_lores_array.size());
      assert(ibin < bin_hires_array.size());
      assert(bin_lores_array[ibin] > 0);
      double loS = 1/bin_lores_array[ibin];
      double hiS = 1/bin_hires_array[ibin];
      double mid = 1/std::sqrt((loS*loS+hiS*hiS)/2.0); //just the average
      if (mid > hires)
        bin_midres_array.push_back(mid);
    }
    assert(bin_midres_array.size());
    return bin_midres_array;
  }

  sv_double
  Reflections::bin_midssqr(double hires) const
  {
    assert(NREFL);
    af_double bin_midres_array = bin_midres(hires);
    sv_double midssqr(bin_midres_array.size());
    for (int ibin = 0; ibin < midssqr.size(); ibin++)
    {
      midssqr[ibin] = 1.0/fn::pow2(bin_midres_array[ibin]);
    }
    return midssqr;
  }

  SigmaN
  Reflections::CalculateSigmaN() const
  {
    // Wilson null hypothesis offset
    assert(has_col(labin::RESN));
    // --- Read HKL of reflections ---
    assert(this->has_col(labin::INAT));
    assert(this->has_col(labin::BIN)); //might only be init
    assert(this->has_friedel(friedel::NAT)); //might only be init
    int numbins = this->numbins();
    sv_bool debug_checking_fix(numbins,false);
    af_double debug_checking_aveval(numbins);
    SigmaN SIGMAN(UC.cctbxUC,this->bin_midres(0));//midres for restraints
    for (auto chk : { 1,2 }) //values not important
    {
#ifdef PHASERTNG_DEBUG_SIGMAN
std::cout << "sigman normalization check loop #" << chk << std::endl;
#endif
      af_double  SumWeight(numbins);
      SIGMAN.BINSCALE.resize(numbins);
      for (int s = 0; s < numbins; s++)
        SIGMAN.BINSCALE[s] = SumWeight[s] = 0;
      SIGMAN.MIDRES = this->bin_midres(0); //so can be extracted from the data
      af_double ssqr_bin(numbins);
      for (int s = 0; s < numbins; s++)
      {
        ssqr_bin[s] = 1.0/fn::pow2(SIGMAN.MIDRES[s]); //so can be extracted from the data
      }
      assert(!boost::logic::indeterminate(INTENSITIES));
      for (int r = 0; r < this->NREFL; r++)
      {
        const millnx miller = this->get_miller(r);
        const bool centric = this->work_cent(miller);
        const double ssqr = this->work_ssqr(miller);
        const double eps = this->work_eps(miller);
        const double Idat = this->get_flt(labin::INAT,r);
        const int s = this->get_int(labin::BIN,r);
        if (this->get_present(friedel::NAT,r))
       // if (this->get_present(friedel::NAT,r) and Idat !=0)
        {
          double weight = centric ? 1 : 2;
          double epsnSigmaN = eps*SIGMAN.best_at_ssqr(ssqr);
          assert(epsnSigmaN > 0);
          SIGMAN.BINSCALE[s] += weight*Idat/epsnSigmaN;
          SumWeight[s] += weight;
        }
      }
      for (int s = 0; s < numbins; s++)
      {
        if (SumWeight[s] != 0)
        {
          SIGMAN.BINSCALE[s] /= SumWeight[s];
        }
      }
      //average[s] += I[r]/epsnSigmaN/BINSCALE[s]; = 1

      // Use Wilson plot on BINSCALE (scale correction factors for scaled table_best curve) to determine the
      // overall scale and the part of the isotropic B that is not in ANISO
      // Then remove this variation with resolution from the BINSCALE and put it into the ANISO parameters
      std::pair<double,double> si = wilson_slope_and_intercept(SIGMAN.BINSCALE,ssqr_bin,SumWeight);
      double WilsonK_intensity = std::exp(si.second); // Scale applied to absolute intensities to get observed
      double WilsonB_intensity = -4*si.first;
      af_bool debug_neg_mean_i(numbins,false);
      af_bool debug_constrain_bins(numbins,false);
      af_double debug_raw_bins(numbins,false);
      for (int s = 0; s < numbins; s++)
      {
        double Wilson = std::exp(WilsonB_intensity*ssqr_bin[s]/4.)/WilsonK_intensity;
        SIGMAN.BINSCALE[s] *= Wilson;
        debug_raw_bins[s] = SIGMAN.BINSCALE[s];
        if (debug_checking_fix[s])
        {
#ifdef PHASERTNG_DEBUG_SIGMAN
std::cout << "fix bad normalization of sigman by using average I value only for bin=" << s << std::endl;
#endif
          double ssqr = 1./fn::pow2(SIGMAN.MIDRES[s]);
          double best_at_ssqr = SIGMAN.best_at_ssqr(ssqr);
          SIGMAN.BINSCALE[s] /= best_at_ssqr; //get rid of the best curve from the normalization
        }
        else if (SIGMAN.BINSCALE[s] > 0.)
        {
          // Don't allow initial values to deviate more than maximum factor from table_best
          double maxfac = 2.; double minfac = 1./maxfac;
          if (SIGMAN.BINSCALE[s] > maxfac or SIGMAN.BINSCALE[s] < minfac)
          {
#ifdef PHASERTNG_DEBUG_SIGMAN
std::cout << "constrained binscale for bin=" << s << std::endl;
#endif
            debug_constrain_bins[s] = true;
            SIGMAN.BINSCALE[s] = std::max(std::min(SIGMAN.BINSCALE[s],maxfac),minfac);
          }
        }
        else
        {
#ifdef PHASERTNG_DEBUG_SIGMAN
std::cout << "fix bad normalization of sigman for negative mean for bin=" << s << std::endl;
#endif
          SIGMAN.BINSCALE[s] = 1.; // Negative mean intensity: just use table_best value
          debug_neg_mean_i[s] = true;
        }
      }
      //calculate SIGMAN per reflection, and store
      sv_double sum(numbins,0);
      sv_double weights(numbins,0);
      af_int debug_number(numbins,0);
      SIGMAN.SCALE_I = WilsonK_intensity;
      assert(SIGMAN.SCALE_I > 0);
      SIGMAN.ANISOBETA = SIGMAN.IsoB2AnisoBeta(WilsonB_intensity);
      for (int r = 0; r < this->NREFL; r++)
      {
        const int s = this->get_int(labin::BIN,r);
        const millnx miller = this->get_miller(r);
        const bool centric = this->work_cent(miller);
        const double inat = this->get_flt(labin::INAT,r);
        const double ssqr = this->work_ssqr(miller);
        const double eps = this->work_eps(miller);
        if (this->get_present(friedel::NAT,r))
        {
          double weight = centric ? 1 : 2;
         // double Bterm = std::exp(WilsonB_intensity*ssqr_bin[s]/4.)/WilsonK_intensity;
          double epsnSigmaN = eps*SIGMAN.term(miller,ssqr,s);
          assert(epsnSigmaN > 0);
      //best(r)*BINSCALE[s]*anisoTerm(r,anisoBeta)*WilsonK_I
          sum[s] += weight*inat/epsnSigmaN;
          weights[s] += weight;
          debug_number[s]++;
        }
      }
      for (int s = 0; s < numbins; s++)
      {
        debug_checking_aveval[s] = sum[s]/weights[s];
#ifdef PHASERTNG_DEBUG_SIGMAN
std::cout << "check normalization of sigman for bin " << s << std::endl;
std::cout << "average s=" << s << " n=" << debug_number[s] << " E=" << debug_checking_aveval[s] << std::endl;
#endif
        double maxfac = 2.; double minfac = 1./maxfac;
        debug_checking_fix[s] = (debug_checking_aveval[s] < maxfac or debug_checking_aveval[s] > minfac);
      }
    }
//final assert which should never fail
    for (int s = 0; s < numbins; s++)
    {
      double maxfac = 2.; double minfac = 1./maxfac;
   //   PHASER_ASSERT(debug_checking_aveval[s] < maxfac and debug_checking_aveval[s] > minfac);
    }
    return SIGMAN;
  }

  SigmaN
  Reflections::setup_resn_anisobeta(
      type::mtzcol mtz_resn,
      type::mtzcol mtz_anisobeta)
  {
    SigmaN SIGMAN;
    setup_mtzcol(mtz_resn);
    setup_mtzcol(mtz_anisobeta);
    if (!this->has_col(labin::ANISOBETA)) return SIGMAN;
    if (!this->has_col(labin::RESN)) return SIGMAN;
    if (!has_col(labin::BIN)) throw Error(err::INPUT,"Column BIN not setup");
    SIGMAN = CalculateSigmaN();
    for (int r = 0; r < this->NREFL; r++)
    {
      const millnx miller = this->get_miller(r);
      const double ssqr = this->work_ssqr(miller);
      const int s = this->get_int(labin::BIN,r);
      const double eps = this->work_eps(miller);
      const double sqrt_epsnSigmaN = std::sqrt(eps*SIGMAN.term(miller,ssqr,s));
      this->set_flt(labin::ANISOBETA,1.,r);
      this->set_flt(labin::RESN,sqrt_epsnSigmaN,r);
    }
    return SIGMAN;
  }

  void
  Reflections::setup_wll(type::mtzcol mtz_wll)
  {
    setup_mtzcol(mtz_wll);
    if (!this->has_col(labin::WLL)) return;
    // Wilson null hypothesis offset
    math::pIntensity   pintensity;
    if (!has_col(labin::RESN)) throw Error(err::INPUT,"Column RESN not setup");
    if (!has_col(labin::WLL)) throw Error(err::INPUT,"Column WLL not setup");
    if (!has_col(labin::INAT)) throw Error(err::INPUT,"Column INAT not setup");
    if (!has_col(labin::SIGINAT)) throw Error(err::INPUT,"Column SIGINAT not setup");
    for (int r = 0; r < this->NREFL; r++)
    {
      this->set_flt(labin::WLL,0,r);
      const double sigi = this->get_flt(labin::SIGINAT,r);
      //below is necessary
      if (this->get_present(friedel::NAT,r)) //not "selected", as not selected yet
      {
        const millnx miller = this->get_miller(r);
        const bool   cent = this->work_cent(miller);
        const double inat = this->get_flt(labin::INAT,r);
        const double resn = this->get_flt(labin::RESN,r);
        double pWilson = cent ?
          pintensity.pIobsCen(inat,fn::pow2(resn),sigi) :
          pintensity.pIobsAcen(inat,fn::pow2(resn),sigi);
        pWilson = std::max(std::numeric_limits<double>::min(),pWilson);
        pWilson = std::log(pWilson);
        this->set_flt(labin::WLL,pWilson,r);
      }
    }
  }

  Bin
  Reflections::setup_bin(type::mtzcol mtz_bin,int M,int N, int W)
  {
    // --- Binning ---
    Bin bin;
    setup_mtzcol(mtz_bin);
    if (!this->has_col(labin::BIN)) return bin; //there is no backup calculation
    bool do_recount(true);
    af_double twostol(this->NREFL);
    UnitCell defcell;
    for (int r = 0; r < this->NREFL; r++)
    {
      twostol[r] = this->UC.cctbxUC.two_stol(this->get_miller(r));
    }
    bin.setup(M,N,W,twostol,do_recount,DEF_LORES);
    for (int r = 0; r < this->NREFL; r++)
    {
      this->set_int(labin::BIN,bin.RBIN[r],r);
    }
    return bin;
  }


  double
  Reflections::WilsonLL(cctbx::sgtbx::space_group SGgroup)
  {
    math::pIntensity   pintensity;
    double WilsonLL(0);
    assert(has_col(labin::TEPS));
    for (int r = 0; r < this->NREFL; r++)
    {
      const millnx miller = this->get_miller(r);
      const bool   centric =   this->work_cent(r);
      const double inat =      this->get_flt(labin::INAT,r);
      const double sigi =   this->get_flt(labin::SIGINAT,r);
      const double teps =   this->get_flt(labin::TEPS,r);
      double  resn =   this->get_flt(labin::RESN,r); //mutable below for indicating sysabs
      if (this->get_present(friedel::NAT,r)) //not selected, as not selected yet
      {
        if (SGgroup.is_sys_absent(miller))
          resn = 0; //flag for using pNormal in pintensity
        else
          assert(resn > 0);
        double epsnSigmaN = fn::pow2(resn)*teps;
        double pWilson = centric ?
            pintensity.pIobsCen(inat,epsnSigmaN,sigi) :
            pintensity.pIobsAcen(inat,epsnSigmaN,sigi);
        pWilson = std::max(std::numeric_limits<double>::min(),pWilson);
        pWilson = std::log(pWilson);
        WilsonLL += pWilson;
      }
    }
    return WilsonLL;
  }

  void
  Reflections::check_for_negative_or_zero_sigi(friedel::type f)
  {
    labin::col colsigi = convert_friedel_to_labin("SIGI",f);
    if (!this->has_col(colsigi)) throw Error(err::INPUT,"Column SIGI not setup");
    for (int r = 0; r < this->NREFL; r++)
    {
      if (this->get_present(f,r))
      {
        assert(this->get_present(f,r));
        //labin::INAT can be zero and that is OK
        const double  siginat = this->get_flt(colsigi,r);
        //e.g. because low number significant figures
        //these should have already been excluded in the read, if they have come from runMTZ
        if (siginat<0)
        {
          std::string label = labin::col2String(colsigi);
          throw Error(err::INPUT,label + "<0");
        }
        if (siginat==0)
        {
          std::string label = labin::col2String(colsigi);
          throw Error(err::INPUT,label + "=0");
        }
      }
    }
  }

  void
  Reflections::check_for_negative_f_or_sigf(friedel::type f)
  {
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    assert(this->has_col(colf));
    assert(this->has_col(colsigf));
    for (int r = 0; r < this->NREFL; r++)
    {
      if (this->get_present(f,r))
      {
        //labin::INAT can be zero and that is OK
        const double fobs = this->get_flt(colf,r);
        const double sigf = this->get_flt(colsigf,r);
        //e.g. because low number significant figures
        //these should have already been excluded in the read, if they have come from runMTZ
        if (sigf<0)
        {
          std::string label = labin::col2String(colsigf);
          throw Error(err::INPUT,label + "<0");
        }
        if (fobs<0)
        {
          std::string label = labin::col2String(colf);
          throw Error(err::INPUT,label + "<0");
        }
      }
    }
  }

  std::pair<
    std::map<int,std::map<bool,double> >,
    std::map<int,std::pair<double,double> >
  >
  Reflections::WilsonSysAbs(char HKL)
  {
    //map of index integer for hkl axis (if present) to:
    // a) wll, scored with expected intensity true/false and stored as map
    std::map<int,std::map<bool,double> > sysabs_wll;
    // b) pair of two interesting values, Z (normalized I) and I/SIGI
    std::map<int,std::pair<double,double> > sysabs_isigi;
    math::pIntensity   pintensity;
    int zero_axis1(0),zero_axis2(1),axis(2);
    if (HKL == 'H') { zero_axis1 = 1; zero_axis2 = 2; axis = 0; }
    if (HKL == 'K') { zero_axis1 = 0; zero_axis2 = 2; axis = 1; }
    if (HKL == 'L') { zero_axis1 = 0; zero_axis2 = 1; axis = 2; }
    assert(has_col(labin::TEPS));
    assert(has_col(labin::CENT));
    assert(has_col(labin::RESN));
    for (int r = 0; r < this->NREFL; r++)
    {
      const millnx  miller = this->get_miller(r);
      const bool    cent =   this->work_cent(r);
      const double  inat =   this->get_flt(labin::INAT,r);
      const double  sigi =   this->get_flt(labin::SIGINAT,r);
      const double  teps =   this->get_flt(labin::TEPS,r);
      const double  resn =   this->get_flt(labin::RESN,r); //mutable below for indicating sysabs
      if (this->get_present(friedel::NAT,r) and sigi > 0) //not selected, as not selected yet
      {
        if (miller[zero_axis1] == 0 and miller[zero_axis2] == 0)
        {
          std::map<bool,double> pIntensity;
          for (auto expected_intensity_zero : {true, false})
          {
            double expected_resn = resn;
            if (expected_intensity_zero)
              expected_resn = 0;
            else
              assert(expected_resn > 0);
            double epsnSigmaN = fn::pow2(expected_resn)*teps;
            double pWilson = cent ?
              pintensity.pIobsCen(inat,epsnSigmaN,sigi) :
              pintensity.pIobsAcen(inat,epsnSigmaN,sigi);
            pWilson = std::max(std::numeric_limits<double>::min(),pWilson);
            pWilson = std::log(pWilson);
            pIntensity[expected_intensity_zero] = pWilson;
          }
          sysabs_wll[miller[axis]] = pIntensity;
          double epsnSigmaN = fn::pow2(resn)*teps;
          sysabs_isigi[miller[axis]] = { inat/epsnSigmaN, inat/sigi };
        }
      }
    }
    return { sysabs_wll, sysabs_isigi };
  }

  void
  Reflections::setup_absolute_scale(double KI)
  {
    //manual list of intensity columns that could be scaled
    for (auto f : FRIEDEL)
    {
      labin::col coli = convert_friedel_to_labin("I",f);
      labin::col colsigi = convert_friedel_to_labin("SIGI",f);
      std::set<labin::col> intensity_cols;
      for (auto item : { coli, colsigi } )
      {
        if (this->has_col(item))
          intensity_cols.insert(item);
      }
      //manual list of amplitude columns that could be scaled
      labin::col colf = convert_friedel_to_labin("F",f);
      labin::col colsigf = convert_friedel_to_labin("SIGF",f);
      labin::col colfeff = convert_friedel_to_labin("FEFF",f);
      std::set<labin::col> amplitude_cols;
      for (auto item : { colf, colsigf, colfeff } )
      {
        if (this->has_col(item))
           amplitude_cols.insert(item);
      }
      //add resn once only
      if (this->has_col(labin::RESN) and f == friedel::NAT)
        amplitude_cols.insert(labin::RESN);
      double rescalei = KI;
      double rescalef = std::sqrt(KI);
      for (int r = 0; r < NREFL; r++) //outer loop reflections
      {
        if (get_present(f,r))
        {
          for (auto item : intensity_cols)
          {
            double value = get_flt(item,r)*rescalei;
            set_flt(item,value,r);
          }
          for (auto item : amplitude_cols)
          {
            double value = get_flt(item,r)*rescalef;
            set_flt(item,value,r);
          }
        }
      }
    }
  }

  void
  Reflections::setup_teps_scale()
  {
    //manual list of intensity columns that could be scaled
    for (auto f : FRIEDEL)
    {
      labin::col coli = convert_friedel_to_labin("I",f);
      labin::col colsigi = convert_friedel_to_labin("SIGI",f);
      std::set<labin::col> intensity_cols;
      for (auto item : { coli, colsigi } )
      {
        if (this->has_col(item))
          intensity_cols.insert(item);
      }
      //manual list of amplitude columns that could be scaled
      labin::col colf = convert_friedel_to_labin("F",f);
      labin::col colsigf = convert_friedel_to_labin("SIGF",f);
      labin::col colfeff = convert_friedel_to_labin("FEFF",f);
      std::set<labin::col> amplitude_cols;
      for (auto item : { colf, colsigf, colfeff } )
      {
        if (this->has_col(item))
          amplitude_cols.insert(item);
      }
      //add resn once only
      if (this->has_col(labin::RESN) and f == friedel::NAT)
        amplitude_cols.insert(labin::RESN);
      for (int r = 0; r < NREFL; r++) //outer loop reflections
      {
        double rescalei = 1./get_flt(labin::TEPS,r);
        double rescalef = 1./std::sqrt(rescalei);
        if (get_present(f,r))
        {
          for (auto item : intensity_cols)
          {
            double value = get_flt(item,r)*rescalei;
            set_flt(item,value,r);
          }
          for (auto item : amplitude_cols)
          {
            double value = get_flt(item,r)*rescalef;
            set_flt(item,value,r);
          }
        }
      }
    }
  }

  void
  Reflections::setup_dobs_feff_from_map(type::mtzcol mtz_dobs,type::mtzcol mtz_feff)
  {
    setup_mtzcol(mtz_dobs);
    setup_mtzcol(mtz_feff);
    assert(has_col(labin::RESN));
    assert(!boost::logic::indeterminate(MAP));
    bool has_mapdobs(has_col(labin::MAPDOBS));
    assert(!boost::logic::indeterminate(EVALUES));
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool   present = this->get_present(friedel::NAT,r);
      const double resn = this->get_flt(labin::RESN,r);
      const double fobs = this->get_flt(labin::FNAT,r);
      const double teps = this->get_flt(labin::TEPS,r);
      const double dobs = has_mapdobs ? this->get_flt(labin::MAPDOBS,r) : 1.;
      if (present) //not "selected", as not selected yet
      {
        double epsnSigmaN = fn::pow2(resn)*teps;
        double sqrt_epsnSigmaN = std::sqrt(epsnSigmaN);
        double feff = EVALUES ? fobs : fobs/sqrt_epsnSigmaN;
        set_flt(labin::DOBSNAT,dobs,r);
        set_flt(labin::FEFFNAT,feff,r);
      }
      else
      {
        set_flt(labin::DOBSNAT,1,r);
        set_flt(labin::FEFFNAT,0,r);
      }
    }
  }

  void
  Reflections::setup_dobs_feff(friedel::type f,type::mtzcol mtz_dobs,type::mtzcol mtz_feff,double OUT_PROB)
  {
    math::pIntensity pintensity;
    //this is not guaranteed to be self-consistent, use with care
    setup_mtzcol(mtz_feff);
    setup_mtzcol(mtz_dobs);
    //calculate current estimate of FEFF,DOBS
    //depends on ANISO (through RESN) and TEPS
    assert(has_friedel(f));
#ifdef PHASERTNG_DEBUG_DOBS
int counter(0);
std::cout << std::string(" I=" + std::string(INTENSITIES ? "Y":"N") + " FW=" + std::string(FRENCH_WILSON ? "Y":"N")) << std::endl;
#endif
    labin::col labinI = convert_friedel_to_labin("I",f);
    labin::col labinF = convert_friedel_to_labin("F",f);
    labin::col labinSIGI = convert_friedel_to_labin("SIGI",f);
    labin::col labinSIGF = convert_friedel_to_labin("SIGF",f);
    labin::col labinFEFF = convert_friedel_to_labin("FEFF",f);
    labin::col labinDOBS = convert_friedel_to_labin("DOBS",f);
    if (!this->has_col(labinI)) return;
    if (!this->has_col(labinF)) return;
    if (!this->has_col(labinSIGI)) return;
    if (!this->has_col(labinSIGF)) return;
    if (!this->has_col(labinFEFF)) return;
    if (!this->has_col(labinDOBS)) return;

#ifdef PHASERTNG_DEBUG_DOBS
std::cout << labin::col2String(labinF) << " " << labin::col2String(labinI) <<" " << labin::col2String(labinSIGI) << std::endl;
std::cout << std::string(has_col(labinF)?"Y":"N") << " " <<std::string(has_col(labinI)?"Y":"N") <<" " << std::string(has_col(labinSIGI)?"Y":"N") << std::endl;
#endif

    math::french_wilson french_wilson;
    assert(!boost::logic::indeterminate(INTENSITIES));
    assert(!boost::logic::indeterminate(FRENCH_WILSON));

    if (((INTENSITIES or !FRENCH_WILSON) and has_col(labinI) and has_col(labinSIGI)) or
         (!INTENSITIES and has_col(labinF)))// Deduce expected values from FrenchWilson F and SIGF
    {
      assert(has_col(labinFEFF));
      assert(has_col(labinDOBS));
      assert(has_col(labin::RESN));
      for (int r = 0; r < this->NREFL; r++)
      {
        const bool   present = this->get_present(f,r);
#ifdef PHASERTNG_DEBUG_DOBS
        const millnx miller = this->get_miller(r);
#endif
        const double iobs = this->get_flt(labinI,r);
        const double sigiobs = this->get_flt(labinSIGI,r);
        const double fobs = this->get_flt(labinF,r);
        const double sigfobs = this->get_flt(labinSIGF,r);
        const double teps = this->get_flt(labin::TEPS,r); //but makes no difference, ==dobs/feff
        const double resn = this->get_flt(labin::RESN,r);
        const bool   cent = this->work_cent(r);
        //at the very least we have to do a minimal wilson check here
        double epsnSigmaN = fn::pow2(resn)*teps;
        double sqrt_epsnSigmaN = std::sqrt(epsnSigmaN);
        double thisEsqr = iobs/epsnSigmaN;
        double thisSigesq = sigiobs/epsnSigmaN;
        double prob = pintensity.probability(cent,thisEsqr,thisSigesq);
#ifdef PHASERTNG_DEBUG_DOBS
std::string mi = std::to_string(miller[0]) + " " + std::to_string(miller[1]) + " " + std::to_string(miller[2]);
std::cout << std::string("Data: r=" + std::to_string(r) + " " + friedel::type2String(f)+" ("+ mi +") wilson prob " + std::to_string(prob) + "\nI,SIGI=" + std::to_string(iobs) + ","  + std::to_string(sigiobs) + " \nF,SIGF=" + std::to_string(fobs) + "," + std::to_string(sigfobs)) <<  std::endl;
#endif
        bool selected = prob >= OUT_PROB-(OUT_PROB/1000.); //numerical instability cf outlier check;
        if (present and selected) //not "selected", as not selected yet
        {
          double eEFW(0),eEsqFW(0);
          bool sigma0(false); // Flag for special case of calculated data
          // Either intensities or unmassaged amplitudes (turned back into intensities)
          if (INTENSITIES or !FRENCH_WILSON)
          {
            eEFW   = french_wilson.expectEFW(thisEsqr,thisSigesq,cent);
            eEsqFW = french_wilson.expectEsqFW(thisEsqr,thisSigesq,cent);
            if (thisSigesq == 0)
              sigma0 = true;
#ifdef PHASERTNG_DEBUG_DOBS
            std::cout << std::string("Data as I or !FW: Esqr=" + std::to_string(thisEsqr) + " Sigesq=" + std::to_string(thisSigesq) + " eEFW=" + std::to_string(eEFW) + " eEsqFW=" + std::to_string(eEsqFW)) << std::endl;
#endif
          }
          else if (!INTENSITIES)// Deduce expected values from FrenchWilson F and SIGF
          {
            assert(fobs > 0);
            eEFW = fobs/sqrt_epsnSigmaN;
            double sige = sigfobs/sqrt_epsnSigmaN;
            eEsqFW = fn::pow2(eEFW) + fn::pow2(sige);
#ifdef PHASERTNG_DEBUG_DOBS
std::cout << std::string("Data as F: eEFW=" + std::to_string(eEFW) + " eEsqFW=" + std::to_string(eEsqFW)) << std::endl;
#endif
          }
          // Once DOBS < 0.05, DOBS/eEobs pairs give almost identical distributions
          // Keeping DOBS>0.05 gives more sensible looking eEobs values
          double dobs = sigma0 ? 1.0 : std::max(0.05,getDfactor(eEFW,eEsqFW,cent));
          double eEobs = getEffectiveEobs(eEsqFW,dobs);
          //Secondary condition: restrict to 10 and compute corresponding DOBS from equation (23) of paper
#ifdef PHASERTNG_DEBUG_DOBS
std::cout << std::string("Secondary condition: Dobs=" + std::to_string(dobs) + " eEobs=" + std::to_string(eEobs)) << std::endl;
#endif
          if ((eEobs > 10.) && (eEsqFW > 1.) && !sigma0)
          {
            eEobs = 10.;
            dobs = std::sqrt((eEsqFW-1.)/(fn::pow2(eEobs)-1.));
#ifdef PHASERTNG_DEBUG_DOBS
std::cout << std::string("Tertiary condition: Dobs=" + std::to_string(dobs) + " eEobs=" + std::to_string(eEobs)) << std::endl;
#endif
          }
          double feff = eEobs*sqrt_epsnSigmaN; // Put back on original scale
#ifdef PHASERTNG_DEBUG_DOBS
std::cout << std::string("DOBS=" + std::to_string(dobs) + " FEFF=" + std::to_string(feff)) << std::endl;
#endif
          set_flt(labinDOBS,dobs,r);
          set_flt(labinFEFF,feff,r);
#ifdef PHASERTNG_DEBUG_DOBS
        auto v = miller;
        std::string mv = std::to_string(v[0]) + " " + std::to_string(v[1]) + " " + std::to_string(v[2]);
        if (!(dobs <= 1 and dobs >= 0 and feff > 0))
std::cout << std::string("FAIL: r=" + std::to_string(r) + " " + friedel::type2String(f) + " (" +  mv +  ") \n------DOBS=" + std::to_string(dobs) + " FEFF=" + std::to_string(feff) + "\n------I,SIGI=" + std::to_string(iobs) + "," + std::to_string(sigiobs) + " F,SIGF=" + std::to_string(fobs) + "," + std::to_string(sigfobs)) << std::endl;
       // if (dobs > 1) set_flt(labinDOBS,1,r);
       // if (dobs < 0) set_flt(labinDOBS,0.01,r);
#endif
        }
        else
        {
#ifdef PHASERTNG_DEBUG_DOBS
std::cout << "not selected" << std::endl;
#endif
          set_flt(labinDOBS,0.01,r);
          set_flt(labinFEFF,0,r);
        }
//final total sanity check for data
        //if (present and selected)
        {
          const double dobs = get_flt(labinDOBS,r);
          const double feff = get_flt(labinFEFF,r);
          const double resn = get_flt(labin::RESN,r);
          assert(dobs <= 1); // Otherwise possibly zero or negative variances
          assert(dobs >= 0);
          assert(feff >= 0);
        }
#ifdef PHASERTNG_DEBUG_DOBS
 if (counter++ == 10) std::exit(1);
#endif
      }
    }
  }

  void
  Reflections::generate_free_r_flags(type::mtzcol freer)
  {
    if (this->has_col(labin::FREE)) return;
    scitbx::random::mersenne_twister generator;
    setup_mtzcol(freer);
    for (int r = 0; r < this->NREFL; r++)
    {
      //range of freer flags is 0-19, same as freerflag
      auto val = std::floor(20*generator.random_double()); //[0,1]
      this->set_int(labin::FREE,static_cast<int>(val),r);
    }
  }

  void
  Reflections::setup_phsr(type::mtzcol mtz_phsr)
  {
    setup_mtzcol(mtz_phsr);
    if (!this->has_col(labin::PHSR)) return;
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      if (centric)
      {
        double deg_phase_restriction = this->work_phsr(r);
        this->set_flt(labin::PHSR,deg_phase_restriction,r);
      }
      else
      {
        double deg = -1; //illegal phase restriction, flag, should never be reached with centric test
        this->set_flt(labin::PHSR,deg,r);
      }
    }
  }

  void
  Reflections::setup_cent(type::mtzcol mtz_cent)
  {
    setup_mtzcol(mtz_cent);
    if (!this->has_col(labin::CENT)) return;
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      this->set_bln(labin::CENT,centric,r);
    }
  }

  void
  Reflections::setup_teps(type::mtzcol mtz_teps,type::mtzcol mtz_tbin)
  {
    setup_mtzcol(mtz_teps);
    setup_mtzcol(mtz_tbin);
    if (!this->has_col(labin::TEPS)) return;
    if (!this->has_col(labin::TBIN)) return;
    double one = 1.;
    for (int r = 0; r < this->NREFL; r++)
    {
      this->set_flt(labin::TEPS,one,r);
      this->set_flt(labin::TBIN,one,r);
    }
  }

  void
  Reflections::setup_ssqr(type::mtzcol mtz_ssqr)
  {
    setup_mtzcol(mtz_ssqr);
    if (!this->has_col(labin::SSQR)) return;
    for (int r = 0; r < this->NREFL; r++)
    {
      double d_star_sq = this->work_ssqr(r);
      this->set_flt(labin::SSQR,d_star_sq,r);
    }
  }

  void
  Reflections::setup_eps(type::mtzcol mtz_eps)
  {
    setup_mtzcol(mtz_eps);
    if (!this->has_col(labin::EPS)) return;
    for (int r = 0; r < this->NREFL; r++)
    {
      double epsilon = this->work_eps(r);
      this->set_flt(labin::EPS,epsilon,r);
    }
  }

  bool
  Reflections::check_and_change_to_spacegroup_in_same_pointgroup(SpaceGroup* esg)
  {
    if (esg->HALL == SG.HALL) return true;
    cctbx::sgtbx::change_of_basis_op cb_op_new = esg->cb_op();
    //dvtos(cb_op_dat.c().as_double_array())
    bool match(false);
    std::vector<cctbx::sgtbx::space_group_type> sysabsall = groups_sysabs(this->SG.type());
    for (int s = 0; s < sysabsall.size(); s++)
    {
      SpaceGroup sysabs("Hall: " + sysabsall[s].hall_symbol());
      cctbx::sgtbx::change_of_basis_op cb_op_dat = sysabs.type().cb_op();
      cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
      if (esg->HALL == sysabs.HALL)
        match = true;
      if (match and cb_op_dat2new.is_identity_op()) //and there is no reindexing required
      {
        this->SG = *esg;
        return true;
      }
    }
    //don't need this
    bool friedel(false);
    af_millnx miller = get_miller_array();
    if (!miller::is_unique_set_under_symmetry(esg->type(),friedel,miller.const_ref()))
    throw Error(err::INPUT,"Reflections are not a unique set by new symmetry");
    return false;
  }

  bool
  Reflections::change_to_spacegroup_in_same_pointgroup(SpaceGroup* esg)
  {
    if (esg->HALL != SG.HALL)
    {
      SG = *esg; //no checks
      return true;
    }
    return false;
  }

  std::pair<af_double,af_int>
  Reflections::sigman_with_binning(Bin& bin)
  {
    //binning for sigman need not be the internal binning
    //the range of resolution has to be restricted to the bin resolution limits SMIN/SMAX
    af_double SIGMAN(bin.numbins(),0);
    af_int SumWeight(bin.numbins(),0);
    af_int Count(bin.numbins(),0);
    for (int r = 0; r < this->NREFL; r++)
    {
      const double ssqr = this->work_ssqr(r);
      double reso =  1.0/std::sqrt(ssqr);
      if (reso < bin.lores() and reso > bin.hires())
      {
        const int ibin = bin.get_bin_ssqr(ssqr);
        //binning in ENSEMBLE bins, not data bins
        Count[ibin]++;
        double resn = this->get_flt(labin::RESN,r);
        assert(resn > 0);
        SIGMAN[ibin] += fn::pow2(resn);
      }
    }
    for (int ibin = 0; ibin < bin.numbins(); ibin++)
    {
      if (SumWeight[ibin] != 0) //may be zero
      {
        SIGMAN[ibin] /= SumWeight[ibin];
      }
    }
    return {SIGMAN,Count};
  }

  pod::pdprint
  Reflections::formatting(labin::col col,bool use_generic) const
  {
    pod::pdprint format;
    format.mm["all"] = { std::numeric_limits<double>::max(),
                         std::numeric_limits<double>::min() };
    format.mm["acentric"] = { std::numeric_limits<double>::max(),
                              std::numeric_limits<double>::min() };
    format.mm["centric"] = { std::numeric_limits<double>::max(),
                             std::numeric_limits<double>::min() };
    format.npres["centric"] = 0;
    format.npres["acentric"] = 0;
    format.npres["all"] = 0;
    format.boolean = (MTZCOLTYPE.at(col) == 'B');
    std::set<labin::col> hkl = {labin::H,labin::K,labin::L};
    friedel::type f = convert_labin_to_friedel(col);
    for (int r = 0; r < NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      if (hkl.count(col) or (!hkl.count(col) and this->has_friedel(f) and this->get_present(f,r)))
      {
        double dat;
        if (col == labin::H)
          dat = static_cast<double>(get_miller(r)[0]);
        else if (col == labin::K)
          dat = static_cast<double>(get_miller(r)[1]);
        else if (col == labin::L)
          dat = static_cast<double>(get_miller(r)[2]);
        else if (MTZCOLTYPE.at(col) == 'B')
          dat = static_cast<double>(get_bln(col,r));
        else if (MTZCOLTYPE.at(col) == 'I')
          dat = static_cast<double>(get_int(col,r));
        else
          dat = get_flt(col,r);
        dat = std::fabs(dat);
        format.mm["all"].first = std::min(format.mm["all"].first,dat);
        format.mm["all"].second = std::max(format.mm["all"].second,dat);
        if (MTZCOLTYPE.at(col) != 'H' and get_present(f,r))
        {
          format.npres["all"]++;
        }
        if (centric)
        {
          format.mm["centric"].first = std::min(format.mm["centric"].first,dat);
          format.mm["centric"].second = std::max(format.mm["centric"].second,dat);
          if (MTZCOLTYPE.at(col) != 'H' and get_present(f,r)) format.npres["centric"]++;
        }
        else if (!centric)
        {
          format.mm["acentric"].first = std::min(format.mm["acentric"].first,dat);
          format.mm["acentric"].second = std::max(format.mm["acentric"].second,dat);
          if (MTZCOLTYPE.at(col) != 'H' and get_present(f,r)) format.npres["acentric"]++;
        }
      }
    }
    {
      std::string label = use_generic ? labin::col2String(col) : MTZCOLNAME.at(col);
      int minpd = std::max(label.size(),std::to_string(NREFL).size());
      for (auto refl : {"all","acentric","centric"})
      {
        if (format.mm[refl].first == std::numeric_limits<double>::max())
        {
          format.empty[refl] = true;
        }
      }
      if (format.mm["all"].first == std::numeric_limits<double>::max())
      {
        format.p = minpd;
        format.d = 0;
        format.pd = minpd;
      }
      else if (MTZCOLTYPE.at(col) == 'H')
      {
        minpd = std::max(minpd,5); //strange things can happen if many rejected 3e3k
        format.p = minpd;
        format.d = 0;
        format.pd = minpd;
      }
      else if (MTZCOLTYPE.at(col) == 'I')
      {
        format.p = minpd;
        format.d = 0;
        format.pd = minpd;
      }
      else if (MTZCOLTYPE.at(col) == 'B')
      {
        format.p = 0;
        format.d = 0;
        format.pd = minpd;
      }
      else
      {
        double absmin = std::abs(format.mm["all"].first);
        double absmax = std::abs(format.mm["all"].second);
        int p = (absmin != 0) ? (int) log10 ((double) absmin) : 1;
        int q = (absmax != 0) ? (int) log10 ((double) absmax) : 1;
        format.d = (p < 0) ? -p + 2 : 2; //more than just first significant figure
        format.pd = format.d + ((q < 0) ? 0 : q); //1 for decimal point, one for sign
        format.pd = std::max(format.pd,minpd) + 2; //1 for decimal point, one for sign
        format.p = format.pd - format.d;
      }
    }
    return format;
  }

  Identifier Reflections::reflid() const
  { return REFLID; }

  void
  Reflections::set_mean_amplitudes_from_anomalous()
  {
    //Start mean from anomalous f and/or i"
    //This is a crappy conversion, only used for reflections and tncs
    assert(this->has_friedel(friedel::POS));
    assert(this->has_friedel(friedel::NEG));
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      bool independent(!centric and ANOMALOUS);
      {
      double fobs(0);
      const bool ppos = this->get_present(friedel::POS,r);
      const bool pneg = independent ? this->get_present(friedel::NEG,r) : false;
      bool  present(ppos or pneg);
      if (present)
      {
        const double  fpos = this->get_flt(labin::FPOS,r);
        const double  fneg = this->get_flt(labin::FNEG,r);
        //mean is average of F+ and F-, or singleton value
        fobs = ((ppos ? fpos: 0) + (pneg ? fneg: 0)) / ((ppos ? 1 : 0) + (pneg ? 1 : 0));
      }
      this->set_flt(labin::FNAT,fobs,r);
      }
      {
      double sigfmean(0);
      const bool ppos = this->get_present(friedel::POS,r);
      const bool pneg = independent ? this->get_present(friedel::NEG,r) : false;
      bool  present(ppos or pneg);
      if (present)
      {
        const double  sigfpos = this->get_flt(labin::SIGFPOS,r);
        const double  sigfneg = this->get_flt(labin::SIGFNEG,r);
        //sigma = sqrt(weighted sum of the variances)
        sigfmean = ((ppos ? fn::pow2(sigfpos): 0) + (pneg ? fn::pow2(sigfneg): 0)) /
          fn::pow2((ppos ? 1 : 0) + (pneg ? 1 : 0));
        sigfmean = std::sqrt(sigfmean);
      }
      this->set_flt(labin::SIGFNAT,sigfmean,r);
      }
    }
  }

  void
  Reflections::set_mean_amplitudes_from_map_amplitudes()
  {
    //we don't know the source of the map amplitudes, but whatever they are, they will have to do
    //if you are worried about their validity, run the phased and unphased algorithms with appropriate data
    //there is no sigma for this
    friedel::type f = friedel::NAT;
    assert(this->has_col(labin::MAPF));
    assert(this->has_col(labin::FNAT));
    assert(this->has_col(labin::SIGFNAT));
    assert(this->has_friedel(f));
    for (int r = 0; r < this->NREFL; r++)
    {
      bool present = this->get_present(f,r);
      this->set_flt(labin::FNAT,0,r);
      this->set_flt(labin::SIGFNAT,0,r);
      if (present)
      {
        double fmap = this->get_flt(labin::MAPF,r);
        this->set_flt(labin::FNAT,fmap,r);
      }
    }
  }

  void
  Reflections::set_mean_intensities_from_anomalous()
  {
    assert(this->has_friedel(friedel::POS));
    assert(this->has_friedel(friedel::NEG));
    for (int r = 0; r < this->NREFL; r++)
    {
      const bool centric = this->work_cent(r);
      bool independent(!centric and ANOMALOUS);
      const bool ppos = this->get_present(friedel::POS,r);
      const bool pneg = independent ? this->get_present(friedel::NEG,r) : false;
      bool  present(ppos or pneg);
      double imean(0);
      double sigimean(0);
      if (present)
      {
        const double  ipos = this->get_flt(labin::IPOS,r);
        const double  ineg = this->get_flt(labin::INEG,r);
        //average  of I+ and I-
        imean = ((ppos ? ipos: 0) + (pneg ? ineg: 0)) / ((ppos ? 1 : 0) + (pneg ? 1 : 0));
        const double  sigipos = this->get_flt(labin::SIGIPOS,r);
        const double  sigineg = this->get_flt(labin::SIGINEG,r);
        sigimean = ((ppos ? fn::pow2(sigipos): 0) + (pneg ? fn::pow2(sigineg): 0)) /
          fn::pow2((ppos ? 1 : 0) + (pneg ? 1 : 0));
        sigimean = std::sqrt(sigimean);
      }
      this->set_flt(labin::INAT,imean, r);
      this->set_flt( labin::SIGINAT, sigimean, r);
    }
  }

  void
  Reflections::set_sigf_to_xtriage_value_if_not_present(friedel::type f)
  {
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    //"Setup sigf 0 " + npnstr[f]);
    double sigma(0);
    for (int r = 0; r < this->NREFL; r++)
    {
       //modeled as 0.05*|F|*exp( -Berror/d^2 )
       // with Berror=-3.069
       double Berror=-3.069;
       double d = UC.cctbxUC.d(get_miller(r));
       sigma = 0.05*this->get_flt(colf,r)*std::exp(-Berror/fn::pow2(d));
       this->set_flt(colsigf,sigma,r);
      //present will be set to zero below
    }
  }

  void
  Reflections::set_FfromI(friedel::type f)
  {
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    labin::col coli = convert_friedel_to_labin("I",f);
    labin::col colsigi = convert_friedel_to_labin("SIGI",f);
    assert(this->has_col(colf));
    assert(this->has_col(coli));
    assert(this->has_col(colsigf));
    assert(this->has_col(colsigi));
    assert(this->has_friedel(f));
    for (int r = 0; r < this->NREFL; r++)
    {
      const double  inat = this->get_flt(coli,r);
      const double  sigi = this->get_flt(colsigi,r);
      if (this->get_present(f,r))
      {
        assert(sigi != 0);
        cctbx::xray::f_sq_as_f_xtal_3_7<double> fsigf(inat,sigi);
        double fsigf_f = fsigf.f; // or = 1, should not be used (patterson anomdiff only?)
        double fsigf_sigma_f = fsigf.sigma_f; // or = 1, should not be used
        if (fsigf_f != 0)
        {
          this->set_flt( colf, fsigf_f, r);
          this->set_flt( colsigf, fsigf_sigma_f, r);
        }
        else
        { //use the truncated values for negative sigma
          //we are never using these values except for patterson anomdiff
          this->set_flt( colf, fsigf_f, r);
          this->set_flt( colsigf, fsigf_sigma_f, r);
          //should not be changing the set_present flag, do something else
          //because the present flag is only for nan on input or
          //reversing french wilson f's
          //this->set_present( f, false, r);
        }
      }
    }
  }

  bool
  Reflections::set_french_wilson_flags(friedel::type f)
  {
    assert(this->has_friedel(f));
    math::french_wilson french_wilson;
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    assert(this->has_col(colf));
    af_double fobs_array;
    af_double sigf_array;
    af_bool centric_array;
    for (int r = 0; r < this->NREFL; r++)
    {
      if (this->get_present(f,r))
      {
        fobs_array.push_back(this->get_flt(colf,r));
        sigf_array.push_back(this->get_flt(colsigf,r));
        const bool centric = this->work_cent(r);
        centric_array.push_back(centric);
      }
    }
    bool flag = french_wilson.is_FrenchWilsonF(fobs_array,sigf_array,centric_array);
    if (!boost::logic::indeterminate(this->FRENCH_WILSON) and flag != this->FRENCH_WILSON)
      throw Error(err::INPUT,"French-Wilson test inconsistent between amplitude datasets: reanalyse data");
    this->FRENCH_WILSON = flag;
    return flag; //for python function
  }

  void
  Reflections::reverse_french_wilson_IfromF(friedel::type f)
  {
    assert(!boost::logic::indeterminate(FRENCH_WILSON));
    assert(bool(FRENCH_WILSON));
    assert(this->has_friedel(f));
    labin::col coli = convert_friedel_to_labin("I",f);
    labin::col colsigi = convert_friedel_to_labin("SIGI",f);
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    assert(this->has_col(coli));
    assert(this->has_col(colsigi));
    assert(this->has_col(colf));
    assert(this->has_col(colsigf));
    for (int r = 0; r < this->NREFL; r++)
    {
      const double  fobs = this->get_flt(colf,r);
      const double  sigf = this->get_flt(colsigf,r);
      if (this->get_present(f,r))
      {
        double fwi = (fn::pow2(fobs) + fn::pow2(sigf));
        double fwsigi = 0;
        //here sigi is set to zero. This is not a valid sigi
        //this I/sigi is ONLY used for the exclusion of reflections (ratio 3)
        //in the twinning test, if it has come from not french-wilson else ignored
        this->set_flt( coli, fwi, r);
        this->set_flt( colsigi, fwsigi, r);
        assert(fobs > 0); //should be not present if zero
        if (fwi==0) //should not be called
        {
          this->set_present( f, false, r);
        }
        //FW data will have ALL SIGI=0 so can't use SIGI=0 test to throw out data
        //Be careful how these are used!!!
        //need to make local checks on whether data were french-wilson or not
      }
    }
  }

  void
  Reflections::reverse_not_french_wilson_IfromF(friedel::type f)
  {
    assert(!boost::logic::indeterminate(FRENCH_WILSON));
    assert(!bool(FRENCH_WILSON));
    assert(this->has_friedel(f));
    labin::col coli = convert_friedel_to_labin("I",f);
    labin::col colsigi = convert_friedel_to_labin("SIGI",f);
    labin::col colf = convert_friedel_to_labin("F",f);
    labin::col colsigf = convert_friedel_to_labin("SIGF",f);
    assert(this->has_col(coli));
    assert(this->has_col(colsigi));
    assert(this->has_col(colf));
    assert(this->has_col(colsigf));
    for (int r = 0; r < this->NREFL; r++)
    {
      const double fobs = this->get_flt(colf,r);
      const double sigf = this->get_flt(colsigf,r);
      if (this->get_present(f,r))
      {
        double fsqi = fn::pow2(fobs);
        double fsqsigi = 2.*fobs*sigf + fn::pow2(sigf);
        this->set_flt( coli, fsqi, r);
        this->set_flt( colsigi, fsqsigi, r);
        //If the data have come from !FW and I/F=0 but SIGI/SIGF!=0
        //then reflection is allowed.
        //Be careful how these are used!!!
        //negative test for numerical stability
        if (fsqi < 0 or fsqsigi < 0) //zero or negative intensity and zero sigma
        {
          //this is the one case where the present flag for the data is not as set on input
          //this is why the friedel::NAT must be flagged as present or not also
          this->set_flt(coli,0,r); //not present
          this->set_flt(colsigi,0,r); //not present
          this->set_present(f,false,r);
        }
      }
    }
  }

  void
  Reflections::set_and_check_anomalous_centric_amplitudes()
  {
    assert(this->has_friedel(friedel::POS));
    assert(this->has_friedel(friedel::NEG));
    for (int r = 0; r < this->NREFL; r++)
    {
      const millnx& miller = this->get_miller(r);
      const bool centric = this->work_cent(r);
      if (centric)
      {
        //centric data
        //copy pos-only (neg-nan) to neg and neg-only (pos-nan) to pos
        //if both are recorded throw a warning if they are not the same
        if (this->get_present(friedel::POS,r) and !this->get_present(friedel::NEG,r))
        {
          const double fpos = this->get_flt(labin::FPOS,r);
          this->set_flt(labin::FNEG,fpos,r);
          const double sigfpos = this->get_flt(labin::SIGFPOS,r);
          this->set_flt(labin::SIGFNEG,sigfpos,r);
          this->set_present(friedel::NEG,true,r);
        }
        else if (!this->get_present(friedel::POS,r) and this->get_present(friedel::NEG,r))
        {
          const double fneg = this->get_flt(labin::FNEG,r);
          this->set_flt(labin::FPOS,fneg,r);
          const double sigfneg = this->get_flt(labin::SIGFNEG,r);
          this->set_flt(labin::SIGFPOS,sigfneg,r);
          this->set_present(friedel::POS,true,r);
        }
        else if ((this->get_present(friedel::POS,r) and this->get_present(friedel::NEG,r)) and
                 (this->get_flt(labin::FPOS,r) != this->get_flt(labin::FNEG,r)))
        {
          auto v = miller;
          std::string mv = std::to_string(v[0]) + " " + std::to_string(v[1]) + " " + std::to_string(v[2]);
      //    throw Error(err::INPUT,"Centric Friedel amplitudes inconsistent (" + mv + ")");
        }
      }
    }
  }

  void
  Reflections::set_and_check_anomalous_centric_intensities()
  {
    assert(this->has_friedel(friedel::POS));
    assert(this->has_friedel(friedel::NEG));
    for (int r = 0; r < this->NREFL; r++)
    {
      const millnx& miller = this->get_miller(r);
      const bool centric = this->work_cent(r);
      if (centric)
      {
        //centric data
        //copy pos-only (neg-nan) to neg and neg-only (pos-nan) to pos
        //if both are recorded throw a warning if they are not the same
        if (this->get_present(friedel::POS,r) and !this->get_present(friedel::NEG,r))
        {
          const double ipos = this->get_flt(labin::IPOS,r);
          this->set_flt(labin::INEG,ipos,r);
          const double sigipos = this->get_flt(labin::SIGIPOS,r);
          this->set_flt(labin::SIGINEG,sigipos,r);
          this->set_present(friedel::NEG,true,r);
        }
        else if (!this->get_present(friedel::POS,r) and this->get_present(friedel::NEG,r))
        {
          const double ineg = this->get_flt(labin::INEG,r);
          this->set_flt(labin::IPOS,ineg,r);
          const double sigineg = this->get_flt(labin::SIGINEG,r);
          this->set_flt(labin::SIGIPOS,sigineg,r);
          this->set_present(friedel::POS,true,r);
        }
        else if ((this->get_present(friedel::POS,r) and this->get_present(friedel::NEG,r)) and
                 (this->get_flt(labin::IPOS,r) != this->get_flt(labin::INEG,r)))
        {
          auto v = miller;
          std::string mv = std::to_string(v[0]) + " " + std::to_string(v[1]) + " " + std::to_string(v[2]);
        //  throw Error(err::INPUT,"Centric Friedel intensities inconsistent (" + mv + ")");
        }
      }
    }
  }

  void
  Reflections::move_to_reciprocal_asu()
  {
    cctbx::sgtbx::reciprocal_space::asu asu(this->SG.type());
    cctbx::sgtbx::space_group sg = SG.group();
    for (int r = 0; r < this->NREFL; r++)
    {
      cctbx::miller::asym_index ai(sg, asu, this->get_miller(r));
      cctbx::miller::index_table_layout_adaptor ila = ai.one_column(false);
      this->set_miller(ila.h(),r);
      for (auto item : this->LABIN)
      {
        if (this->MTZCOLTYPE[item] != 'H')
        {
          bool is_BLN(this->MTZCOLTYPE[item] == 'B');
          bool is_INT(this->MTZCOLTYPE[item] == 'I');
          bool is_FLT(!is_BLN and !is_INT);
          if (is_BLN)
          {
            bool deg(false);
            bool value = this->get_bln(item,r);
            cctbx::miller::map_to_asu_policy<double>::eq(ila,value,deg);
            this->set_bln(item,value,r);
          }
          else if (is_INT)
          {
            bool deg(false);
            int value = this->get_int(item,r);
            cctbx::miller::map_to_asu_policy<double>::eq(ila,value,deg);
            this->set_int(item,value,r);
          }
          else if (is_FLT)
          {
            bool deg(this->MTZCOLTYPE[item] == 'P');
            double value =  this->get_flt(item,r);
            cctbx::miller::map_to_asu_policy<double>::eq(ila,value,deg);
            this->set_flt(item,value,r);
          }
        }
      }
      //for (auto f : FRIEDEL) { //no need to change the present flags }
    }
  }

  bool
  Reflections::check_sort_in_resolution_order()
  {
    double ssqr_last = std::numeric_limits<double>::lowest();
    bool has_ssqr(this->has_col(labin::SSQR));
    for (int r = 0; r < NREFL; r++)
    { //use ssqr as this may have already been calculated
      double ssqr = has_ssqr ? work_ssqr(r) : UC.cctbxUC.d_star_sq(get_miller(r));
      if ((ssqr_last) > (ssqr+DEF_PPT)) //low resolution to high resolution
      { //DEF_PPM is not big enough tolerance
        return false;
      }
      ssqr_last = ssqr;
    }
    return true;
  }

  void
  Reflections::sort_in_resolution_order()
  {
    auto cmp = []( const std::pair<double,int> &a, const std::pair<double,int> &b )
    { if (a.first == b.first) return a.second < b.second;
      return a.first >  //> for reverse sort
             b.first; };
    std::vector<std::pair<double,int> > sorted(this->NREFL);
    for (int r = 0; r < this->NREFL; r++)
    {
      double resolution = this->UC.cctbxUC.d(this->get_miller(r));
      sorted[r] = { resolution, r }; //SORT ORDER on first
    }
    std::sort(sorted.begin(),sorted.end(),cmp); //high resolution to low resolution
    // AJM awaiting BOOST 1.70 :: boost::parallel_stable_sort(sorted.begin(),sorted.end());
    { //memory
    af_millnx miller(this->NREFL);
    for (int r = 0; r < this->NREFL; r++)
      miller[r] = this->get_miller(sorted[r].second);
    for (int r = 0; r < this->NREFL; r++)
      this->set_miller(miller[r],r);
    } //memory
    for (auto item : this->LABIN)
    {
      bool is_BLN(this->MTZCOLTYPE[item] == 'B');
      bool is_INT(this->MTZCOLTYPE[item] == 'I');
      bool is_FLT(!is_BLN and !is_INT);
      if (is_FLT) //data
      {
        sv_double newdata(this->NREFL);
        for (int r = 0; r < this->NREFL; r++)
          newdata[r] = this->get_flt(item,sorted[r].second);
        for (int r = 0; r < this->NREFL; r++)
          this->set_flt(item,newdata[r],r);
      }
      else if (is_INT) //FREE is int
      {
        sv_int newdata(this->NREFL);
        for (int r = 0; r < this->NREFL; r++)
          newdata[r] = this->get_int(item,sorted[r].second);
        for (int r = 0; r < this->NREFL; r++)
          this->set_int(item,newdata[r],r);
      }
      else if (is_BLN) //no data, for completeness
      {
        sv_bool newdata(this->NREFL);
        for (int r = 0; r < this->NREFL; r++)
          newdata[r] = this->get_bln(item,sorted[r].second);
        for (int r = 0; r < this->NREFL; r++)
          this->set_bln(item,newdata[r],r);
      }
    }
    for (auto f : FRIEDEL)
    { //assume all always implemented as get_present
      sv_bool  present(this->NREFL);
      for (int r = 0; r < this->NREFL; r++)
        present[r] = this->get_present(f,sorted[r].second);
      for (int r = 0; r < this->NREFL; r++)
        this->set_present(f,present[r],r);
    }
  }

  void
  Reflections::setup_both_plus(type::mtzcol mtz_both,type::mtzcol mtz_plus)
  {
    assert(this->has_friedel(friedel::POS));
    assert(this->has_friedel(friedel::NEG));
    setup_mtzcol(mtz_both);
    setup_mtzcol(mtz_plus);
    if (this->has_col(labin::IPOS) and this->has_col(labin::INEG))
    {
      for (int r = 0; r < this->NREFL; r++)
      {
        const bool centric = this->work_cent(r);
        const bool ppos = this->get_present(friedel::POS,r);
        const bool pneg = this->get_present(friedel::NEG,r);
        const double ipos = this->get_flt(labin::IPOS,r);
        const double ineg = this->get_flt(labin::INEG,r);
        bool effectively_singleton(ipos==ineg); //eg native data as anomalous
        bool both = !centric and ppos and pneg and !effectively_singleton;
        bool plus = !centric and ppos and !pneg;
        this->set_bln(labin::BOTH,both,r);
        this->set_bln(labin::PLUS,plus,r);
      }
    }
    else if (this->has_col(labin::FPOS) and this->has_col(labin::FNEG))
    {
      for (int r = 0; r < this->NREFL; r++)
      {
        const bool centric = this->work_cent(r);
        const bool ppos = this->get_present(friedel::POS,r);
        const bool pneg = this->get_present(friedel::NEG,r);
        const double fpos = this->get_flt(labin::FPOS,r);
        const double fneg = this->get_flt(labin::FNEG,r);
        bool effectively_singleton(fpos==fneg); //eg native data as anomalous
        bool both = !centric and ppos and pneg and !effectively_singleton;
        bool plus = !centric and ppos and !pneg;
        this->set_bln(labin::BOTH,both,r);
        this->set_bln(labin::PLUS,plus,r);
      }
    }
  }

  friedel::type
  Reflections::convert_labin_to_friedel(labin::col col) const
  {
    //find out if this is a POS or NEG labin, else it is a NAT labin
    friedel::type f = friedel::UNDEFINED;
    if (hoist::algorithm::contains(labin::col2String(col),"NAT")) f = friedel::NAT;
    if (hoist::algorithm::contains(labin::col2String(col),"MAP")) f = friedel::NAT; //also map input
    if (hoist::algorithm::contains(labin::col2String(col),"POS")) f = friedel::POS;
    if (hoist::algorithm::contains(labin::col2String(col),"NEG")) f = friedel::NEG;
    //bool isnat = !ispos and !isneg;
    return f; //UNDEFINED
  }

  labin::col
  Reflections::convert_friedel_to_labin(std::string label, friedel::type f) const
  {
    //take the POS and NEG of the friedel type and append to the input label,
    // then convert to labin::col
    std::string ext = friedel::type2String(f);
    labin::col col = labin::col2Enum(label + ext);
    return col;
  }

  bool Reflections::has_col(labin::col col) const
  { return LABIN.count(col); }

  bool Reflections::has_friedel(friedel::type f) const
  { return FRIEDEL.count(f); }

  bool Reflections::has_friedel(std::set<friedel::type> fs) const
  { for (auto f: fs) if (!FRIEDEL.count(f)) return false; return true; }

  int  Reflections::grain(int n) const
  { return (NREFL/n)+1; } //granularity of threading, +1 makes the last thread the shortest

  int  Reflections::head_thread(int t,int n) const
  { return t*grain(n); } //start of thread reflection

  int  Reflections::tail_thread(int t,int n) const
  { return std::min((t+1)*grain(n),NREFL); } //end of thread

  void
  Reflections::set_data(labin::col col,const double v,const int r)
  { //return double, regardless of whether it is an int or a float
    auto coltype = MTZCOLTYPE.at(col);
    if (coltype == 'B')
    {
      set_bln(col, (v == 0. ? false : true), r);
    }
    else if (coltype == 'I')
    {
      set_int(col,static_cast<int>(v),r);
    }
    else if (col != labin::H and col != labin::K and col != labin::K)
    {
      set_flt(col,v,r);
    }
  }

  af_millnx
  Reflections::get_miller_array() const
  {
    af_millnx miller_array(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      miller_array[r] = get_miller(r);
    }
    return miller_array;
  }

  af_bool
  Reflections::get_present_array(friedel::type f) const
  { //return double, regardless of whether it is an int or a float
    af_bool present_array(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      present_array[r] = get_present(f,r);
    }
    return present_array;
  }

  af_double
  Reflections::get_data_array(labin::col item) const
  { //return double, regardless of whether it is an int or a float
    af_double data_array(NREFL);
    auto coltype = MTZCOLTYPE.at(item);
    if (coltype == 'B')
    {
      for (int r = 0; r < NREFL; r++)
        data_array[r] = get_bln(item,r) ? 1.0 : 0.0;
    }
    else if (coltype == 'I')
    {
      for (int r = 0; r < NREFL; r++)
        data_array[r] = static_cast<double>(get_int(item,r));
    }
    else if (item != labin::H and item != labin::K and item != labin::K)
    {
      for (int r = 0; r < NREFL; r++)
        data_array[r] = get_flt(item,r);
    }
    return data_array;
  }

  af_string
  Reflections::unique_under_symmetry_selection()
  {
    af_string output;
    cctbx::sgtbx::space_group_type cctbxSG = SG.type();
    bool anomalous_flag(false);
    af_millnx miller = get_miller_array();
    af::shared<std::size_t> result = cctbx::miller::unique_under_symmetry_selection(cctbxSG,anomalous_flag,miller.const_ref());
    //copy the guts of the above function in order to add logging information
    if (result.size()) return output;
    {{
    output.push_back("");
    output.push_back("Unique under symmetry = " + std::to_string(result.size()) + "/" + std::to_string(NREFL));
    af::shared<std::size_t> rresult((af::reserve(miller.size())));
    std::set<cctbx::miller::index<>, cctbx::miller::fast_less_than<> > unique_set;
    cctbx::sgtbx::reciprocal_space::asu asu(cctbxSG);
    cctbx::sgtbx::space_group const& sg = cctbxSG.group();
    int maxreport = 20; //speed
    for(int i = 0; i < miller.size() and rresult.size() < maxreport; i++)
    {
      cctbx::miller::asym_index ai(sg, asu, miller[i]);
      cctbx::miller::index_table_layout_adaptor ila = ai.one_column(anomalous_flag);
      if (unique_set.find(ila.h()) == unique_set.end())
      {
        unique_set.insert(ila.h());
        rresult.push_back(i);
      }
      else
      {
       // auto v = *unique_set.find(ila.h());
        auto v = miller[i];
        std::string mv = std::to_string(v[0]) + " " + std::to_string(v[1]) + " " + std::to_string(v[2]);
        auto h = ila.h();
        std::string mh = std::to_string(h[0]) + " " + std::to_string(h[1]) + " " + std::to_string(h[2]);
        (v == h) ?
          output.push_back("Duplicate " + mh):
          output.push_back("Duplicate " + mh + " by symmetry of " + mv);
      }
    }
    output.push_back("etc... " + std::to_string(NREFL-result.size()) + " more");
    //output.push_back("Unique = " + std::to_string(result.size()) + "/" + std::to_string(NREFL));
    }}

    int rr(0);
    for (std::size_t r = 0; r < NREFL; r++)
      if (std::find(result.begin(),result.end(),r) != result.end())
        set_miller(get_miller(r),rr++);

    //DATA
    for (auto item : LABIN)
    {
      auto coltype = MTZCOLTYPE.at(item);
      if (coltype == 'B')
      {
        int rr(0);
        for (int r = 0; r < NREFL; r++)
          if (std::find(result.begin(),result.end(),r) != result.end())
            set_bln(item,get_bln(item,r),rr++);
      }
      else if (coltype == 'I')
      {
        int rr(0);
        for (int r = 0; r < NREFL; r++)
          if (std::find(result.begin(),result.end(),r) != result.end())
            set_int(item,get_int(item,r),rr++);
      }
      else if (item != labin::H and item != labin::K and item != labin::K)
      {
        int rr(0);
        for (int r = 0; r < NREFL; r++)
          if (std::find(result.begin(),result.end(),r) != result.end())
            set_flt(item,get_flt(item,r),rr++);
      }
    }

    rr = (0);
    for (std::size_t r = 0; r < NREFL; r++)
      if (std::find(result.begin(),result.end(),r) != result.end())
        rr++;
    erase_from_reflection(rr);
    return output;
  }

  std::tuple<af_string,bool,double>
  Reflections::staraniso()
  {
    auto miller = get_miller_array();
    af_string output; bool warning;
    auto millerp1 = cctbx::miller::expand_to_p1_indices(SG.group(),false,miller.const_ref());
    double fac = 100; //very important, otherwise stats not numerically stable
    af_dvect3 cw;
    double hires = DEF_LORES;
    for (auto r : millerp1)
    {
      dvect3 s = fac*UC.cctbxUC.reciprocal_space_vector(r);
      cw.push_back(s);
      cw.push_back(-s); //p1 not friedel, false correct above
      hires = std::min(hires,fac/(std::sqrt(s*s)));
    }
    principal_statistics statistics;
    statistics.set(cw);
    const dvect3& e = statistics.EXTENT;
    double maxe = std::max(e[0],std::max(e[1],e[2]));
    double mine = std::min(e[0],std::min(e[1],e[2]));
    double ratio = maxe/mine;
    dvect3 reso = { fac/(e[0]/2.), fac/(e[1]/2.),fac/(e[2]/2.) };
    output.push_back("Reciprocal Space Vectors Expanded to P1");
    output.push_back("--Extent Ratios: " + dvtos(statistics.EXTENT/mine,4));
    output.push_back("--Resolutions along Axes: " + dvtos(reso,4));
    output.push_back("--High Resolution : " + dtos(hires,4));
    double limit = 1.1;
    if (ratio > limit)
      output.push_back("Maximum Ratio Over Limit: " + dtos(ratio,3) + " > " +dtos(limit,1));
    double dmin = fac/(mine/2.);
    return std::make_tuple(output, (ratio > limit), dmin);
  }

  std::string Reflections::get_tncsinfo() const
  {
    if (TNCS_ORDER == 1) return  "1:(---None,---None,---None)";
    return itos(TNCS_ORDER)+":("+dtos(TNCS_VECTOR[0],7,4,true)+","+dtos(TNCS_VECTOR[1],7,4,true)+","+dtos(TNCS_VECTOR[2],7,4,true)+")";
  }

  std::string Reflections::get_crystal() const
  { return SG.pgname() + ":" + dvtos(UC.GetBox(),1); } //the point group and the setting for eg PG222

  double Reflections::bfactor_minimum()
 // { return -WILSON_B_F+DEF_PPT; }
  { return std::min(-WILSON_B_F,-WILSON_B_F/2)+DEF_PPT; }

} //phasertng

#include <phasertng/data/include/logReflections.cpp>
#include <phasertng/data/include/ReflColsMap.cpp>
#include <phasertng/data/include/ReflRowsAnom.cpp>
#include <phasertng/data/include/ReflRowsData.cpp>
