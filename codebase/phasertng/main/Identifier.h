//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Identifier_class__
#define __phasertng_Identifier_class__
#include <phasertng/main/uuid.h>
#include <phasertng/enum/entry_data.h>
#include <phasertng/enum/other_data.h>
#include <string>

namespace phasertng {

class Identifier
{
  //input uuid type from phil has value_min=0
  //uuid is size_t, if this changes need to change boost constructor type also
  //size_t is not larger than unsigned long long
  //can set the uuid type different from the read type, and throw error if out of range
  //hence test for number_value > 0 is a test for is_valid
  uuid_t         number_value = 0;
  std::string    tag_value = "None";

  public:
    Identifier() {}
    ~Identifier() {}
    Identifier(const ullint n,const std::string tag)
    { initialize_from_ullint(n); initialize_tag(tag); }
    void set_invalid() { number_value = 0; tag_value = "None"; }

  public:
    bool        is_valid(ullint test) const;
    bool        is_set_valid() const;
    std::string str() const;
    std::string string() const; //returns the string of the identifier
    const uuid_t  identifier() const;
    const std::string tag() const;
    const std::string tag_unparse() const;
    const std::string id_unparse() const;
    std::string stem() const;
    void        unstem(const std::string);
    void        initialize(std::pair<uuid_t,std::string>); //parsing
    bool        initialize_tag(std::string); //returns true if shortened
    void        initialize_from_text(std::string);
    void        initialize_from_random(ullint); //param is the seed
    void        initialize_from_ullint(ullint);
    void        initialize_from_string(const std::string);
    void        initialize_node_tag();
    void        parse_string(std::string num,std::string tag);
    void        increment(const std::string);

  bool
  operator!=( const Identifier& rhs ) const
  {
    return number_value != rhs.number_value or
           tag_value != rhs.tag_value;
  }

  bool
  operator==( const Identifier& rhs ) const
  {
    return number_value == rhs.number_value and
           tag_value == rhs.tag_value;
  }

  bool
  operator<( const Identifier& rhs ) const
  {
    //the test of quality in std::set is not all the same, different from ==
    return number_value < rhs.number_value;
  }

  Identifier&
  operator=(const Identifier& rhs)
  {
    number_value = rhs.number_value;
    tag_value = rhs.tag_value;
    return *this;
  }

  std::pair<std::string,std::string> FileName(std::string);
  std::pair<std::string,std::string> FileEntry(entry::data);
  std::pair<std::string,std::string> FileOther(other::data);
  std::pair<std::string,std::string> FileDataBase(other::data);
  std::string database_subdir();
  std::string extra_file_stem_ext(std::string);
  std::string entry_data_stem_ext(entry::data);
  std::string other_data_stem_ext(other::data);
  std::string reflections_and_models();

};

struct IdentifierHash
{
  template <typename T>
  std::size_t operator()(T t) const
  {
    return static_cast<std::size_t>(t.identifier());
  }
};

}//phasertng
#endif
