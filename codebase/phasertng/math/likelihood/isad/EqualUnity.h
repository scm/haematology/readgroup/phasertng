//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_EqualUnity_class__
#define __phasertng_EqualUnity_class__
#include <vector>
#include <scitbx/constants.h>

namespace phasertng {
namespace likelihood {

//points equally distributed, weights unity  i.e. no quadrature

class EqualUnity
{
  public:
    EqualUnity(int n=15,double start_ang=0)
    {
      nStep = n;
      Ang.clear();
      CosAng.clear();
      SinAng.clear();
      Cos2Ang.clear();
      Sin2Ang.clear();
      Weight = scitbx::constants::two_pi/nStep;
      for (int p = 0; p < n; p++)
      {
        Ang.push_back(start_ang + p*Weight);
        CosAng.push_back(std::cos(Ang[p]));
        SinAng.push_back(std::sin(Ang[p]));
        Cos2Ang.push_back(std::cos(2.0*Ang[p]));
        Sin2Ang.push_back(std::sin(2.0*Ang[p]));
      }
    }

    int nStep = 0;
    double Weight = 0;
    std::vector<double> Ang;
    std::vector<double> CosAng;
    std::vector<double> Cos2Ang;
    std::vector<double> SinAng;
    std::vector<double> Sin2Ang;

  public:
    const int& size() const { return nStep; }
    const double& ang(const int& p) { return Ang[p]; }
    const double& cosAng(const int& p) { return CosAng[p]; }
    const double& sinAng(const int& p) { return SinAng[p]; }
    const double& cos2Ang(const int& p) { return Cos2Ang[p]; }
    const double& sin2Ang(const int& p) { return Sin2Ang[p]; }
};

}}//phasertng

#endif
