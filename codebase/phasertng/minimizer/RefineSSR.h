//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineSSR_class__
#define __phasertng_RefineSSR_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phaser/src/RefineSAD.h>

namespace phasertng {

class RefineSSR : public dtmin::RefineBase, public phaser::RefineSAD
{
  private: //members
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;

  public:
    int    ISTEPS = 0;

  private:
    dag::Node*           NODE;

  public:
    //refined parameters stored here
    const_ReflectionsPtr REFLECTIONS;
   // SelectedPtr          SELECTED;
    ScatterersPtr        SCATTERERS;
   // EpsilonPtr           EPSILON;

  public:  //constructor
    RefineSSR() : dtmin::RefineBase(), phaser::RefineSAD()
    {}
    RefineSSR(
        const_ReflectionsPtr,
    //    SelectedPtr,
     //   EpsilonPtr,
        ScatterersPtr);
    ~RefineSSR() throw() {}

   public:
    std::pair<af_string,std::string> init_node(dag::Node*,sv_double,std::pair<int,int>,bool);

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string setup_parameters();
    af_string reject_outliers();
    af_string cleanup_parameters();

  double maximum_distance_special(
      sv_double&,
      sv_double&,
      sv_bool&,
      sv_double&,
      double&
    );

  private:
    TargetGradientHessianType
    parallel_combined_tgh(
      bool, //gradient
      bool, //hessian
      int, //beginning
      int //end
    );

    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
      );

  void add_restraint(TargetGradientHessianType&,bool,bool);

  public:
    void set_threading(bool,int);
    TargetType likelihood();
    Loggraph fom_loggraph();

};

} //phasertng
#endif
