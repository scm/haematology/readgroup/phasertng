//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/SingleAtoms.h>
#include <phasertng/main/Assert.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

  cmplex
  SingleAtoms::calcAtomicDataSum(
      const millnx& miller,
      const double ssqr,
      const double eps,
      const Scatterers& scatterers,
      bool do_gradient,
      bool do_hessian
    )
  {
    //because atomtypes can be clusters, this cannot be done by fft
    pod::rsymhkl rsym(SG.NSYMP);
    SG.symmetry_hkl_primitive(miller,eps,rsym);

    double ReF(0),ImF(0);
    if (do_hessian) do_gradient = true;

    for (int a = 0; a < this->size(); a++)
    {
      pod::xray::scatterer& A = this->XRAY[a];
      pod::xtra::scatterer& X = this->XTRA[a];
      if (!X.rejected)
      {
        const std::string& t = A.scattering_type;
        double linearTerm = SG.symFacPCIF*A.occupancy/X.multiplicity;
        double isoB = A.flags.use_u_aniso_only() ?
                      1.0 :
                      std::exp(-ssqr*cctbx::adptbx::u_as_b(A.u_iso) * 0.25);
        double symmOccBfac = linearTerm*isoB;
        double fo_plus_fp = 0;
        if (X.Ax)
        {
          fo_plus_fp = 0;
        }
        else if (X.Tx)
        {
          PHASER_ASSERT(false); //not implemented!! AJM TODO
        }
        else
        {
          fo_plus_fp = scatterers.fo(t,ssqr) + A.fp;
        }
        double scat(fo_plus_fp*symmOccBfac);
        for (int isym = 0; isym < rsym.unique; isym++)
        {
          double anisoB(1);
          double anisoScat(scat);
          if (A.flags.use_u_aniso_only())
          {
            anisoB = cctbx::adptbx::debye_waller_factor_u_star(rsym.rotMiller[isym],A.u_star);
            anisoScat = scat*anisoB;
          }
          double theta = rsym.rotMiller[isym]*A.site + rsym.traMiller[isym];
          cmplex sincostheta = tbl_cos_sin.get(theta); //multiplies by 2*pi internally
          double costheta = std::real(sincostheta);
          double sintheta = std::imag(sincostheta);
          double symReF(anisoScat*costheta);
          double symImF(anisoScat*sintheta);
          ReF += symReF;
          ImF += symImF;

          if (do_gradient && REFINE_SITE)
          {
            pod::gradient::mean::scatterer& G = this->GRAD[a];
            for (int x = 0; x < 3; x++)
            {
              double gradfac = scitbx::constants::two_pi*rsym.rotMiller[isym][x];
              G.dReF_by_dX[x] -= gradfac*symImF;
              G.dImF_by_dX[x] += gradfac*symReF;
              if (do_hessian)
              {
                pod::hessian::mean::scatterer& H = this->HESS[a];
                double hessfac = fn::pow2(gradfac);
                H.d2ReF_by_dX2[x] -= hessfac*symReF;
                H.d2ImF_by_dX2[x] -= hessfac*symImF;
              }
            }
          }

          if (do_gradient && A.flags.use_u_aniso_only() && REFINE_BFAC)
          {
            pod::gradient::mean::scatterer& G = this->GRAD[a];
            dmat6 millerSqr = cctbx::adptbx::debye_waller_factor_u_star_gradient_coefficients(rsym.rotMiller[isym],scitbx::type_holder<double>());
            for (int n = 0; n < 6; n++)
            {
              double gradfac = millerSqr[n]*scitbx::constants::two_pi_sq;
              G.dReF_by_dAB[n] -= gradfac*symReF;
              G.dImF_by_dAB[n] -= gradfac*symImF;
              if (do_hessian)
              {
                pod::hessian::mean::scatterer& H = this->HESS[a];
                double hessfac = fn::pow2(-gradfac);
                H.d2ReF_by_dAB2[n] += hessfac*symReF;
                H.d2ImF_by_dAB2[n] += hessfac*symImF;
              }
            }
          }
        }//end sym

        if (do_gradient && REFINE_OCC && A.occupancy)
        {
          pod::gradient::mean::scatterer& G = this->GRAD[a];
          PHASER_ASSERT(A.occupancy > std::numeric_limits<double>::min());
          G.dReF_by_dO = ReF/A.occupancy;
          G.dImF_by_dO = ImF/A.occupancy;
          //hessian = 0
        }

        if (do_gradient && !A.flags.use_u_aniso_only() && REFINE_BFAC)
        {
          pod::gradient::mean::scatterer& G = this->GRAD[a];
          double gradfac = -ssqr*(0.25*scitbx::constants::eight_pi_sq);
          G.dReF_by_dIB = -gradfac*ReF;
          G.dImF_by_dIB = -gradfac*ImF;
          if (do_hessian)
          {
            pod::hessian::mean::scatterer& H = this->HESS[a];
            double hessfac = fn::pow2(-gradfac);
            H.d2ReF_by_dIB2 = hessfac*ReF;
            H.d2ImF_by_dIB2 = hessfac*ImF;
          }
        }
      }
    } //end  loop over atoms
    cmplex FH = {ReF,ImF};
    return FH;
  }

}//phasertng
