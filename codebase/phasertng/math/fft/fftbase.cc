//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/fft/fftbase.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Assert.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <scitbx/vec3.h>
#include <scitbx/array_family/shared.h>

typedef scitbx::vec3<double> dvect3;
typedef std::complex<double> cmplex;
typedef scitbx::af::shared<cmplex>  af_cmplex;

namespace phasertng { namespace fft {

  fftbase::fftbase(
       cctbx::sgtbx::space_group_type SG_,
       cctbx::uctbx::unit_cell UC_
       ) : SG(SG_),UC(UC_)
  {}

  void
  fftbase::translation_search_symmetry_flags(
      bool is_isotropic_search_model,
      bool have_f_part)
  { //copied from cctbx/translation_search/symmetry_flags.h
     sym_flags =  cctbx::sgtbx::search_symmetry_flags(
         is_isotropic_search_model,
         0,
         !have_f_part,
         is_isotropic_search_model && !have_f_part,
         false);
  }

  void
  fftbase::calculate_gridding(
      double savereso, cctbx::uctbx::unit_cell gridding_uc)
  {
    // --- prepare fft grid
    int max_prime(5);
    bool phaser_assert_shannon_sampling(true);
    double resolution_factor = 1.0/Shannon;
    dmin = savereso;
    setup_u_base(); //after dmin set

    gridding = cctbx::maptbx::determine_gridding(
        gridding_uc,
        savereso, //also dmin!!
        resolution_factor,
        sym_flags,
        SG, //PtNcs requires P1
        mandatory_factors,
        max_prime,
        phaser_assert_shannon_sampling
     );

    af::c_grid<3> grid_target(gridding[0],gridding[1],gridding[2]);
    try { //bugfix for 3eke
      //where the memory is huge using deposited intensity data, which are bogus
      //errors are not being thrown from rfft allocation
      //make a std::vector same size as rfft will be, and then kill it
      std::vector<double>(gridding[0]*gridding[1]*gridding[2]);
    }
    catch(std::bad_alloc& ba)
    {
      memory_allocation_error = true;
      throw Error(err::MEMORY,"FFT grid too large");
    }
    if (!memory_allocation_error)
    {
      tags = cctbx::maptbx::grid_tags<long>(grid_target);
    }
    tags.build(SG,sym_flags);
  }

  void
  fftbase::calculate_gridding(
       double dmin_,
       std::pair<double,double> CELL_SCALE,
       bool interpolate
     )
  {
    dmin = dmin_;
    setup_u_base(); //after dmin set
    // --- prepare fft grid
    int max_prime(5);
    bool phaser_assert_shannon_sampling(true);
    double resolution_factor = 1.0/Shannon;
    //when we are building density for structure factor interpolation
    //gridding is more complicated than using the unit cell and resolution for density
    //as we need to allow for cell expansion and contraction in refinement
    //If the cell expands, the number of grid points to a given resolution is larger
    //so need more indices (fewer for contraction, which does not impact worst-case senario)
    //At the very limit, one grid point will be more resolution in the smaller cell,
    //so need to expand the resolution by the unit miller index in the smaller cell
    cctbx::uctbx::unit_cell gridding_uc = UC;
    double savereso = dmin;
    af::double6 TINY_BOX = UC.parameters();
    if (CELL_SCALE != std::pair<double,double>(1,1))
    {
      double CELL_SCALE_MIN = std::min(CELL_SCALE.first,CELL_SCALE.second);
      double CELL_SCALE_MAX = std::max(CELL_SCALE.first,CELL_SCALE.second);
      //test for orthogonal P1 lattice, i.e. lattice for interpolation
      if (SG.number() != 1)
      throw Error(err::DEVELOPER,"Only apply cell scale in P1");
      if (UC.parameters()[3] != 90 or UC.parameters()[4] != 90 or UC.parameters()[5] != 90)
      throw Error(err::DEVELOPER,"Only apply cell scale with orthogonal cell");
      af::double6 HUGE_BOX = UC.parameters();
      HUGE_BOX[0] *= CELL_SCALE_MAX;
      HUGE_BOX[1] *= CELL_SCALE_MAX;
      HUGE_BOX[2] *= CELL_SCALE_MAX;
      //change gridding unit cell
      gridding_uc = cctbx::uctbx::unit_cell(HUGE_BOX);
      TINY_BOX[0] *= CELL_SCALE_MIN;
      TINY_BOX[1] *= CELL_SCALE_MIN;
      TINY_BOX[2] *= CELL_SCALE_MIN;
    } //AJM TODO always extend
    cctbx::uctbx::unit_cell tiny_uc(TINY_BOX);
    //extend the resolution at the edges by one grid unit
    if (interpolate)
    {
      millnx UNIT(1,1,1);
      //add as s=smax+sunit
      savereso = (1./(1./dmin + 1./tiny_uc.d(UNIT)));
    }
    //STORE THE EXTENDED RESOLUTION AS THE DMIN
    dmin = savereso;
    setup_u_base(); //after dmin set

    gridding = cctbx::maptbx::determine_gridding(
        gridding_uc,
        savereso, //also dmin!!
        resolution_factor,
        sym_flags,
        SG, //PtNcs requires P1
        mandatory_factors,
        max_prime,
        phaser_assert_shannon_sampling
     );

    af::c_grid<3> grid_target(gridding[0],gridding[1],gridding[2]);
    try { //bugfix for 3eke
      //where the memory is huge using deposited intensity data, which are bogus
      //errors are not being thrown from rfft allocation
      //make a std::vector same size as rfft will be, and then kill it
      std::vector<double>(gridding[0]*gridding[1]*gridding[2]);
    }
    catch(std::bad_alloc& ba)
    {
      memory_allocation_error = true;
      throw Error(err::MEMORY,"FFT grid too large");
    }
    if (!memory_allocation_error)
    {
      tags = cctbx::maptbx::grid_tags<long>(grid_target);
    }
    tags.build(SG,sym_flags);
  }

  void
  fftbase::add_bulk_solvent(
        af::shared<cctbx::xray::scatterer<double> > xray_scatterers,
        double BULK_FSOL,
        double BULK_BSOL,
        af::shared<millnx> miller_indices,
        af::shared<cmplex> structure_factors) const
  {
    af::shared<dvect3> atoms_sites(xray_scatterers.size());
    af::shared<double> atoms_radii(xray_scatterers.size());
    table::vanderwaals_radii atoms_vdw_table(0);
    for (int a = 0; a < xray_scatterers.size(); a++)
    {
      atoms_sites[a] = xray_scatterers[a].site;
      atoms_radii[a] = atoms_vdw_table.get(xray_scatterers[a].scattering_type);
    }
    cctbx::af::c_interval_grid<3>::index_type n_real = n_grid();
    mmtbx::masks::atom_mask atommask( UC, SG.group(), n_real, 1, 1);
    atommask.compute(atoms_sites,atoms_radii);
    af_cmplex Fmask = atommask.structure_factors(miller_indices.const_ref());
    for (int r = 0; r < miller_indices.size(); r++)
    {
      double ss = UC.d_star_sq(miller_indices[r])/4.;
      double k_mask = BULK_FSOL*std::exp(-BULK_BSOL*ss);
      structure_factors[r] += k_mask*Fmask[r];
    }
  }

  void
  fftbase::setup_u_base()
  {
    double quality_factor(100);
    double resolution_factor = 1.0/Shannon;
    u_base = cctbx::xray::calc_u_base( dmin, resolution_factor, quality_factor);
  }

}}
