//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/data/Cluster.h>
#include <phasertng/minimizer/RefineSSA.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Substructure_completion
  void Phasertng::runSSA()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->ANOMALOUS)
    throw Error(err::INPUT,"Reflections not prepared with anomalous data");
    if (REFLECTIONS->Z > 100)
      throw Error(err::INPUT,"Biological unit represents less than 1% of asymmetric unit: check composition");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag should only have one node");

    if (REFLECTIONS->check_native())
    logAdvisory("Reflections have no anomalous signal");

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Data"));

    bool biological_unit_input =
        !input.value__path.at(".phasertng.biological_unit.model.filename.").is_default() or
      !input.value__path.at(".phasertng.biological_unit.sequence.filename.").is_default() or
      input.size(".phasertng.biological_unit.hetatm.");

    std::vector<sv_string> macro = {
      input.value__choice.at(".phasertng.macssa.macrocycle1.").value_or_default_multi(),
      input.value__choice.at(".phasertng.macssa.macrocycle2.").value_or_default_multi(),
      input.value__choice.at(".phasertng.macssa.macrocycle3.").value_or_default_multi()
      };
    std::vector<int>  ncyc = {
      input.value__int_vector.at(".phasertng.macssa.ncyc.").value_or_default(0),
      input.value__int_vector.at(".phasertng.macssa.ncyc.").value_or_default(1),
      input.value__int_vector.at(".phasertng.macssa.ncyc.").value_or_default(2)
    };
    auto method = input.value__choice.at(".phasertng.macssa.minimizer.").value_or_default();

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    Cluster cluster;
    if (!input.value__path.at(".phasertng.cluster_compound.filename.").is_default())
    {
      cluster.SetFileSystem(input.value__path.at(".phasertng.cluster_compound.filename.").value_or_default());
      cluster.ReadPdb();
    }
    PHASER_ASSERT(DAGDB.size() == 1);

    {{ // First SSA pass with AX type to establish consistency with specified anomalous scatterer
    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }
    for (auto &comp : DAGDB.WORK->COMPOSITION)
    {
      (comp.first == "XX") ? scatterers.insert(cluster) : scatterers.insert(comp.first);
    }
    std::string anomtype = (biological_unit_input) ? scatterers.strongest_anomalous_scatterer().first : "AX";
    if (anomtype == "AX")
      goto ax_sca_is_redundant;
    logUnderLine(out::LOGFILE, "Imaginary Substructure Content Analysis");
    scatterers.insert("AX");

    RefineSSA refine(
        REFLECTIONS.get(),
        &selected,
        &scatterers,
        input.value__flt_number.at(".phasertng.substructure.content_analysis.number.").value_or_default(),
        input.value__flt_number.at(".phasertng.substructure.content_analysis.delta_bfactor.").value_or_default());
     //   &DAGDB);
    refine.SIGMA_ATOMS = input.value__flt_number.at(".phasertng.macssa.restraint_sigma.atoms.").value_or_default();
    refine.SIGMA_RHOPM = input.value__flt_number.at(".phasertng.macssa.restraint_sigma.rhopm.").value_or_default();
    refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
    refine.init_node(DAGDB.WORK);

    dtmin::Minimizer minimizer(this);
    minimizer.run(
      refine,
      macro,
      ncyc,
      input.value__choice.at(".phasertng.macssa.minimizer.").value_or_default(),
      input.value__boolean.at(".phasertng.macssa.study_parameters.").value_or_default()
      );

    logTabArray(out::LOGFILE,refine.logSigmaA(""));
    double fdp = scatterers.fp_fdp(anomtype,0.).as_complex_double().imag();
    double nequiv = refine.nascat / fn::pow2(fdp);
    logTab(out::VERBOSE, "Imaginary scattering content equivalent to that from " +
                        dtos(nequiv, 3) + " fully-occupied " + anomtype + " atoms");
    logBlank(out::VERBOSE);

    logTabArray(out::VERBOSE,DAGDB.WORK->logSigmaa("After refinement"));

    if (refine.NO_SIGNAL)
    {
      logWarning("Very weak signal for imaginary scatterer");
    }

    // Check absrhoFF values for consistency with specified anomalous scatterer
    int nbins = DAGDB.WORK->SADSIGMAA.MIDSSQR.size();
    int nbadbins(0), nbadref(0), ntotref(0);
    af_int numinbin = REFLECTIONS->numinbin();
    logTab(out::VERBOSE, "Check consistency with anomalous scattering from " + anomtype);
    logTab(out::VERBOSE, "Inconsistent bins marked with x");
    logTab(out::VERBOSE, "  Bin  |rhoFF|   minimum for " + anomtype);
    for (int b = 0; b < nbins; b++)
    {
      ntotref += numinbin[b];
      cmplex fA(0,0);
      fA += scatterers.fo(anomtype,DAGDB.WORK->SADSIGMAA.MIDSSQR[b]);
      fA += scatterers.fp_fdp(anomtype,DAGDB.WORK->SADSIGMAA.MIDSSQR[b]).as_complex_double();
      double SigmaP = DAGDB.WORK->SADSIGMAA.SigmaP_bin[b];
      cmplex sigmaPP = DAGDB.WORK->SADSIGMAA.sigmaPP_bin[b];
      double nmax = refine.nAnomMax(SigmaP, sigmaPP, fA);
      double rhoFFmin = refine.absrhoFFfromNanom(SigmaP, sigmaPP, fA, nmax);
      double thisrhoFF = DAGDB.WORK->SADSIGMAA.absrhoFF_bin[b];
      std::string mark = (thisrhoFF <= rhoFFmin) ? "x" : " ";
      if (thisrhoFF <= rhoFFmin)
      {
        nbadbins++;
        nbadref += numinbin[b];
        logTab(out::VERBOSE, snprintftos("  %3d  %8.6f  %8.6f  %1s",
                                        b + 1, thisrhoFF, rhoFFmin, "x"));
      }
      else
      {
        logTab(out::VERBOSE, snprintftos("  %3d  %8.6f  %8.6f",
                                        b + 1, thisrhoFF, rhoFFmin));
      }
    }
    if (nbadbins)
    {
      logBlank(out::VERBOSE);
      logTab(out::VERBOSE, itos(nbadbins) + " of " + itos(nbins) + " are inconsistent");
      logTab(out::VERBOSE, "including " + itos(nbadref) + " of " + itos(ntotref) + " reflections");
    }
    if ((nbadbins > (2. / 3.) * nbins) or (nbadref > (2. / 3.) * ntotref)) // Too much inconsistent data
    {
      // DAGDB.NODES.front().SAD_NOT_CONSISTENT = true;
      refine.NOT_CONSISTENT = true;
      logTab(out::LOGFILE, "|rhoFF| values too low (signal too strong) to be consistent with " + anomtype);
      logWarning( "Data not consistent with specified anomalous scatterer and wavelength");
      logTabArray(out::LOGFILE,scatterers.logScatterers("Substructure Content Analysis"));
      return; // SSA cannot be evaluated with this input
    }
    }} // End first SSA pass
    ax_sca_is_redundant: // Skip to here because done in next block

    bool NO_SIGNAL;
    double nascat,dBscat;
    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    {{ // Second SSA pass evaluating specified anomalous scatterer
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }
    for (auto& comp : DAGDB.WORK->COMPOSITION)
    {
      (comp.first == "XX") ? scatterers.insert(cluster) : scatterers.insert(comp.first);
    }
    std::string anomtype = (biological_unit_input) ? scatterers.strongest_anomalous_scatterer().first : "AX";
    logTab(out::LOGFILE, "Substructure Content Analysis for " + anomtype);
    logTabArray(out::LOGFILE,scatterers.logScatterers("Substructure Content Analysis"));
    if (anomtype == "AX")
      logTab(out::LOGFILE,"Purely imaginary (AX) scatterer: fp and fdp ignored for other atom types");
    logBlank(out::LOGFILE);

    RefineSSA refine(
        REFLECTIONS.get(),
        &selected,
        &scatterers,
        input.value__flt_number.at(".phasertng.substructure.content_analysis.number.").value_or_default(),
        input.value__flt_number.at(".phasertng.substructure.content_analysis.delta_bfactor.").value_or_default());
       // &DAGDB);
    refine.SIGMA_ATOMS = input.value__flt_number.at(".phasertng.macssa.restraint_sigma.atoms.").value_or_default();
    refine.SIGMA_RHOPM = input.value__flt_number.at(".phasertng.macssa.restraint_sigma.rhopm.").value_or_default();
    refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
    refine.init_node(DAGDB.WORK);

    dtmin::Minimizer minimizer(this);
    minimizer.run(
      refine,
      macro,
      ncyc,
      input.value__choice.at(".phasertng.macssa.minimizer.").value_or_default(),
      input.value__boolean.at(".phasertng.macssa.study_parameters.").value_or_default()
      );
    logTabArray(out::VERBOSE,refine.logSigmaA(""));
    NO_SIGNAL = refine.NO_SIGNAL;
    logTabArray(out::VERBOSE,DAGDB.WORK->logSigmaa("After refinement"));
    input.value__boolean.at(".phasertng.substructure.content_analysis.signal.").set_value(!NO_SIGNAL);
    input.value__string.at(".phasertng.substructure.scatterer.").set_value(anomtype);
    (refine.nascat < 1) ? //1 minimum
    input.value__flt_number.at(".phasertng.substructure.content_analysis.number.").set_value(1):
    input.value__flt_number.at(".phasertng.substructure.content_analysis.number.").set_value(refine.nascat);
    input.value__flt_number.at(".phasertng.substructure.content_analysis.delta_bfactor.").set_value(refine.dBscat);

    if (REFLECTIONS->check_native())
    {
      logWarning("No signal for specified anomalous scatterer and wavelength");
    }
    else if (refine.NO_SIGNAL)
    {
      logWarning("Very weak signal for specified anomalous scatterer and wavelength");
    }
    nascat = refine.nascat;
    dBscat = refine.dBscat;
    }} // End second SSA pass

    logTabArray(out::LOGFILE,selected.logSelected("Substructure Content Analysis"));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers("Substructure Content Analysis"));

    {{
    std::string ANOMTYPE = scatterers.strongest_anomalous_scatterer().first;
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Substructure Content Analysis");
    if (REFLECTIONS->check_native())
    {
      logTab(where,"No anomalous signal");
    }
    else if (NO_SIGNAL)
    {
      logTab(where,"Almost no anomalous signal detected");
    }
    else
    {
      logTab(where,"Anomalous signal detected");
      logBlank(where);
      logTab(where,"Strongest anomalous scatterer: " + ANOMTYPE);
      input.value__string.at(".phasertng.substructure.scatterer.").set_value(ANOMTYPE);
      logTab(where,"Equivalent number of fully occupied atoms:  " + dtos(nascat,9,4));
      logTab(where,"Average delta(B) for anomalous substructure:" + dtos(dBscat,9,4));
      logBlank(where);
    }
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
    auto reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(out::LOGFILE,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::SSA);

    dag::Node& node = DAGDB.NODES.front();
    phaser_assert(node.SADSIGMAA.size());

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
