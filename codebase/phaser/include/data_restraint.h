//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_RESTRAINT_CLASS__
#define __PHASER_DATA_RESTRAINT_CLASS__

namespace phaser {

class data_restraint
{

  public:
  data_restraint(bool b=false,double o=1) : RESTRAINT(b),SIGMA(o) { }

  bool   RESTRAINT;
  double SIGMA;
};

} //phaser

#endif
