#ifndef PHASER_NCS_NCSOPERATOR_NEW_H
#define PHASER_NCS_NCSOPERATOR_NEW_H

#include <scitbx/mat3.h>
#include <scitbx/vec3.h>

#include <string>

namespace phaser {  namespace ncs {

class NCSOperator
{
    private:
        scitbx::mat3< double > rotation_;
        scitbx::vec3< double > translation_;

    public:
        NCSOperator(
            scitbx::mat3< double > const& rotation,
            scitbx::vec3< double > const& translation
            );
        ~NCSOperator();

        // Information methods
        double rotation_angle_radian() const;
        double displacement() const;
        const scitbx::mat3< double >& get_rotation() const;
        const scitbx::vec3< double >& get_translation() const;

        // Mathematical operations
        NCSOperator inverse() const;
        NCSOperator operator*(const NCSOperator& other) const;
        bool approx_equals(
            const NCSOperator& other,
            double angular_tolerance,
            double spatial_tolerance
            ) const;
        bool approx_inverse(
            const NCSOperator& other,
            double angular_tolerance,
            double spatial_tolerance
            ) const;
        bool approx_unity(
            double angular_tolerance,
            double spatial_tolerance
            ) const;

        // Miscellaneous
        scitbx::vec3< double > transform(
            const scitbx::vec3< double >& point
            ) const;

        // Display method
        std::string to_string() const;

        // Constants
        static const NCSOperator unity;
};


}  } // namespace phaser::ncs

#endif /*PHASER_NCS_NCSOPERATOR_NEW_H*/
