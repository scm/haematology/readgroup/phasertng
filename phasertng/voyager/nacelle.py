import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys,os
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.mode_generator import *
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng import InputCard,Phasertng,PhaserError,enum_out_stream
from phasertng.run.ModeRunner import *
import argparse

def nacelle(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_nacelle)

def inner_nacelle(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="nacelle")
    initparams = initparams +  "\n" +  build_auto_str("nacelle")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()

    #chk does not contribute to the DAG
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    dbdir = working_params.phasertng.suite.database
    run_silent = (dbdir is None)
   #if false set the default database
    if run_silent:
      #permanently turn off all the file writing, and only output the non-database file
      dryrun_cards = silent_cards()
    else: #don't generate the database directory or clear the xml file
      dryrun_cards = ""
      assert(dbdir is not None)
      if not os.path.isdir(dbdir):
        os.makedirs(dbdir)
      xmlout = working_params.phasertng.graph_report.filestem + ".xml"
      xmlout = os.path.join(dbdir,xmlout)
      xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
      #clear the tracker before we start
      if os.path.exists(xmlout):
        os.remove(xmlout)

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keywords = True
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords)
    basetng.set_user_input_for_suite(basetng.input)

    loop_auto = str("Nacelle")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    ModeRunner(basetng=basetng,mode_list=['chk'])

    #switch the hklout onto the mtzout keyword for phasertng
    mtzout = working_params.nacelle.hklout
    if mtzout is None:
      raise ValueError(enum_err_code.input,"hklout filename not set")
    basetng.input.set_as_str(".phasertng.mtzout.",mtzout)

    ModeRunner(basetng=basetng,mode_list=["walk"])

    #first choice
    hklin = working_params.nacelle.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) > 0:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    ModeRunner(basetng=basetng,mode_list=['imtz','data','sga','sgx','aniso','tncso','tncs','feff','twin','info'])
    if working_params.phasertng.reflections.read_anomalous:
      ModeRunner(basetng=basetng,mode_list=['cca','ccs','ssa'])
    node_number = basetng.DAGDATABASE.PATHWAY.ULTIMATE.identifier()
    if progress_callback is not None:
      progress_callback.emit(node_number)
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
    if not run_silent:
      text = voyager_readme(dbdir)
      basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--mtzin",help="mtz file",type=str,required=True)
  parser.add_argument('-db',"--database",help="database directory",type=str,required=False)
  args = parser.parse_args()
  user_phil = ""
  if args.phil is not None:
    user_phil = args.phil.read()
  user_phil = user_phil + "\nphasertng.hklin.filename=\"" + str(args.mtzin)+"\"\n"
  if len(args.database):
    user_phil = user_phil + "\nphasertng.suite.database=\"" + str(args.database)+"\"\n"
 #user_phil = user_phil + "\nphasertng.suite.database=\"./phasertng_nacelle\"\n"
  basetng = nacelle(user_phil_str=user_phil)
  mtzout =  basetng.input.get_as_str(".phasertng.mtzout.") # quotes
  mtzout =  mtzout.strip("'\"")
  print("mtzout=",mtzout)
  mtzout = mtzout  + ".log"
  f = open(mtzout, "w")
  f.write(basetng.summary())
  f.close()
  print("summary=",mtzout)
