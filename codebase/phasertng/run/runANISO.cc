//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineANISO.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Anisotropy_correction
  void Phasertng::runANISO()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::DATA))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (data)");
    if (REFLECTIONS->NREFL == 0)
    throw Error(err::INPUT,"No Reflections");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.macaniso.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected("Before anisotropy refinement"));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers("Before anisotropy refinement"));
    }}

    bool refine_off =
        input.value__choice.at(".phasertng.macaniso.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macaniso.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macaniso.macrocycle3.").value_or_default_equals("off");
    refine_off = refine_off or
        input.value__choice.at(".phasertng.macaniso.protocol.").value_or_default_equals("off");

    SigmaN sigman;
    double delta_bfactor(0);
    if (REFLECTIONS->EVALUES)
    {
      sigman = SigmaN(REFLECTIONS->UC.cctbxUC,REFLECTIONS->bin_midres(0));//midres for restraints
      logAdvisory("Data input as Evalues so no additional normalization");
      logTab(out::SUMMARY,"No anisotropy refinement");
    }
    else if (refine_off)
    {
      sigman = REFLECTIONS->CalculateSigmaN();
      logTab(out::SUMMARY,"No anisotropy refinement");
    }
    else
    {
      if (REFLECTIONS->MAP)
        logAdvisory("Anisotropy correction using Amplitudes from Map rather than Intensity data");

      sigman = REFLECTIONS->CalculateSigmaN();
      RefineANISO refa(
          REFLECTIONS.get(),
          &selected,
          &sigman);
      refa.BEST_CURVE_RESTRAINT = input.value__boolean.at(".phasertng.macaniso.best_curve_restraint.").value_or_default();
      refa.set_threading(USE_STRICTLY_NTHREADS, NTHREADS);

//for phaser functionality, use macrocycle1 defaults for 2 and 3 also, rather than changing
//and use the newton minimizer
      std::vector<sv_string> macro = {
          input.value__choice.at(".phasertng.macaniso.macrocycle1.").value_or_default_multi(),
          input.value__choice.at(".phasertng.macaniso.macrocycle2.").value_or_default_multi(),
          input.value__choice.at(".phasertng.macaniso.macrocycle3.").value_or_default_multi()
        };
      std::vector<int> ncyc = {
        input.value__int_vector.at(".phasertng.macaniso.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macaniso.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macaniso.ncyc.").value_or_default(2)
      };
      auto method = input.value__choice.at(".phasertng.macaniso.minimizer.").value_or_default();
      logProtocol(out::LOGFILE,macro,ncyc,method);
      logBlank(out::LOGFILE);

      dtmin::Minimizer minimizer(this);
      minimizer.run(refa,
          macro,
          ncyc,
          method,
          input.value__boolean.at(".phasertng.macaniso.study_parameters.").value_or_default()
          );
      //REFLECTIONS = std::move(refa.REFLECTIONS); //take back ownership
      logTabArray(out::VERBOSE,sigman.logAnisotropy(""));
      logTabArray(out::SUMMARY,sigman.logScaling(""));
      logTabArray(out::VERBOSE,sigman.logBinning("",REFLECTIONS->numinbin()));
      logGraph(out::LOGFILE,sigman.logGraph1("",REFLECTIONS->numinbin()));

      delta_bfactor = refa.delta_bfactor();
    }

    {{ //set the values on the reflections
    REFLECTIONS->WILSON_K_F = sigman.wilson_k_f();
    REFLECTIONS->WILSON_B_F = sigman.IsoB_F();
    REFLECTIONS->SHARPENING = sigman.sharpening_bfactor();
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const millnx miller = REFLECTIONS->get_miller(r);
      const double ssqr =   REFLECTIONS->work_ssqr(miller);
      const double eps  =   REFLECTIONS->work_eps(miller);
      const int    s  =   REFLECTIONS->get_int(labin::BIN,r);
      double epsnSigmaN = eps*sigman.term(miller,ssqr,s);
      PHASER_ASSERT(epsnSigmaN > 0);
      bool RemoveIsoB(true);
      const double anisobeta = sigman.AnisoTerm(miller,RemoveIsoB); // store the correction as I/ANISO
      const double resn = std::sqrt(epsnSigmaN);
      REFLECTIONS->set_flt(labin::ANISOBETA,anisobeta,r);
      REFLECTIONS->set_flt(labin::RESN,resn,r);
    }
    }}

    double outer_bin_contrast(0);
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Histogram of anisotropy terms in outer shell");
    af_double anisotropy;
    int last_bin = REFLECTIONS->numbins()-1;
    int min_refl_for_histogram(200);
    do
    {
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        if (selected.get_selected(r) and REFLECTIONS->get_int(labin::BIN,r) == last_bin)
        {
          double anisobeta = REFLECTIONS->get_flt(labin::ANISOBETA,r); //plot the inverse for scale
          anisotropy.push_back(anisobeta); //order of magnitude
        }
      }
      last_bin--;
    }
    while (anisotropy.size() < min_refl_for_histogram and last_bin >= 0);
    double minh = std::numeric_limits<double>::max();
    double maxh = 0;
    double meanh = 0;
    //distribution is around the mean which is not 0
    for (int r = 0; r < anisotropy.size(); r++)
    {
      double anisobeta = anisotropy[r];
      double h = (1/anisobeta); //order of magnitude
      anisotropy[r] = 1/anisobeta; //scale factor
      minh = std::min(minh,h);
      maxh = std::max(maxh,h);
      meanh += h/anisotropy.size();
    }
    logTab(where,"Anisotropy mean: " + dtos(meanh));
    if (anisotropy.size())
    {
      auto nslots = 10;
      if (minh < maxh-DEF_PPH)
        logHistogram(where,anisotropy,nslots,false,"Reflections per anisotropy scale in outer resolution shell",minh,maxh,false,false); //ints
      else logTab(where,"No range for anisotropy histogram");
    }
    else
    {
      logTab(where,"Insufficient data in highest resolution shell");
    }
    }}


    //increment the tracker here so that we can use it for the reflid
    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::ANISO);
    REFLECTIONS->ANISO_PRESENT = true;
    REFLECTIONS->ANISO_DIRECTION_COSINES = sigman.direction_cosines();
    //Phasertng::calculate_dobs_feff();
    }}

    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected("After anisotropy refinement"));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers("After anisotropy refinement"));

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Anisotropy Data"));

    {{
    REFLCOLSMAP.setup_resn_anisobeta( //add to the base class or no copy
      input.value__mtzcol.at(".phasertng.labin.resn.").value_or_default(),
      input.value__mtzcol.at(".phasertng.labin.anisobeta.").value_or_default());
    REFLCOLSMAP.store_new_and_changed_data(REFLECTIONS.get());
    REFLCOLSMAP.setup_mtzcol( //add to the base class
      input.value__mtzcol.at(".phasertng.labin.aniso.").value_or_default());
    for (int r = 0; r < REFLCOLSMAP.NREFL; r++)
    {
      //labin::ANISO is for output only for plotting in hklviewer
      //labin::ANISOBETA is used internally as it does not have the WilsonB included
      const millnx miller = REFLCOLSMAP.get_miller(r);
      bool RemoveIsoB(false);
      const double aniso = sigman.AnisoTerm(miller,RemoveIsoB); // store the correction as I/ANISO
      REFLCOLSMAP.set_flt(labin::ANISO,aniso,r);
    }
    }}

    //---
    input.value__flt_number.at(".phasertng.anisotropy.sharpening_bfactor.").set_value(sigman.sharpening_bfactor());
    input.value__flt_number.at(".phasertng.anisotropy.wilson_scale.").set_value(sigman.wilson_k_f());
    input.value__flt_number.at(".phasertng.anisotropy.wilson_bfactor.").set_value(sigman.IsoB_F());
    input.value__flt_number.at(".phasertng.anisotropy.delta_bfactor.").set_value(delta_bfactor);
    input.value__flt_matrix.at(".phasertng.anisotropy.direction_cosines.").set_value(sigman.direction_cosines());
    input.value__flt_number.at(".phasertng.anisotropy.outer_bin_contrast.").set_value(outer_bin_contrast);
    input.value__flt_numbers.at(".phasertng.anisotropy.beta.").set_value(sigman.AnisoBetaRemoveIsoB());

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
