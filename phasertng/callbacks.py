from libtbx import group_args
import libtbx.callbacks # import dependency

phaser_messages = ["error",
                   "advise",
                   "loopxml",
                   "warn"]
cached_messages = ["error",
                   "advise",
                   "loopxml",
                   "warn"]

class manager (object) :
  def __init__ (self) :
    self._message = ""
    self._data = ""

  def call_back (self, message, data) :
    accumulate = False
    self._message = message
    self._data = data
    libtbx.call_back(message=message, data=data, accumulate=accumulate)
