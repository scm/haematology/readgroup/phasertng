//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineSP_class__
#define __phasertng_RefineSP_class__
#include <phasertng/main/includes.h>
#include <phasertng/main/constants.h>
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phasertng/src/Bin.h>
#include <phasertng/pod/fast_types.h>
#include <phasertng/math/likelihood/llgi/function.h>
#include <phasertng/math/likelihood/llgi/helper.h>
#include <phasertng/enum/refine_segment.h>
#include <phasertng/math/fft/complex_to_complex_3d.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>

namespace phasertng {

//Refine Segmented Pruning

class RefineSP : public dtmin::RefineBase
{
  //scoping of struct
  class sbrdata
  {
    public:
    double current = 0;
    sv_int atom_number;
    sv_double init;
    int size() { return atom_number.size(); } //== init.size()
    double average_init() { double P(0); for (auto p: init) P+=p/size(); return P; }
    double lower() { double P(10000); for (auto p: init) P=std::min(P,p); return P; }
    double upper() { double P(-9999); for (auto p: init) P=std::max(P,p); return P; }
    double bfactor() { return cctbx::adptbx::u_as_b(current); }
  };

  public: //set externally
    double BFAC_MIN = DEF_BFAC_MIN;
    double BFAC_MAX = DEF_BFAC_MAX;
    const double OCC_MIN = 0.1;
    const double OCC_MAX = 1.0;
    double MAX_PERCENT_PRUNED = 100;
    double LLG_BIAS = 10;
    refine::segment REFINE = refine::BFACTOR;
    bool   debugger = false;
    bool   negvar = false;
    FourParameterSigmaA solTerm;

  private:
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;

  private:
    //Parameters derived from Dag node parameters
    dag::Node* NODE;
    std::vector<sbrdata> SEGMENTS;
    phasertng::fft::complex_to_complex_3d reffft;
    af_millnx miller_array;
    af_int rbin;
    Bin bin;
    sv_double SigmaP;

  public: //members
    const_ReflectionsPtr REFLECTIONS;
    sv_bool              SELECTED;
    const_EpsilonPtr     EPSILON;
   // DagDatabasePtr       DAGDB;

  public:  //constructor
    RefineSP() : dtmin::RefineBase() {}
    RefineSP(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr);
       // DagDatabasePtr);
    ~RefineSP() throw() {}

  public:
    void init_node(dag::Node*);
    void init_segments();
    af_string binary_occupancy();

    TargetGradientHessianType
    parallel_combined_tgh(
      bool, //gradient
      bool, //hessian
      int, //beginning
      int //end
    );

  private:
    //pointer to function
    //use this rather than derived classes so that functions can be changed on-the-fly
    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
      );

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string cleanup_parameters();
    af_string finalize_parameters();

  public:
    TargetType likelihood();

  public:
    void set_threading(bool,int);
    af_string logCurrent(std::string);
    void calcSigmaP();
    pod::FastPoseType ecalcs_sigmaa_full();
};

} //phasertng
#endif
