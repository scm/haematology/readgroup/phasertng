//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_rfactor_function_class__
#define __phasertng_likelihood_rfactor_function_class__

namespace phasertng { namespace likelihood { namespace rfactor {

class function
{
  private: //members
    double numerator = 0;
    double denominator = 0;
    double minval = 0; //percent
    double maxval = 100; //percent

  public:  //constructor
    function() {}

  public:  //getter
    void   add(double,double);
    void   clear();
    double get();
};

}}} //phasertng
#endif
