//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_NodeList_class__
#define __phasertng_dag_NodeList_class__
#include <phasertng/dag/Node.h>
#include <boost/logic/tribool.hpp>

namespace phasertng {
namespace dag {

  class NodeList
  {
    public:
      std::vector<dag::Node> node_list; //in dag namespace
      NodeList() {}

    public:
      bool has_all_modlid_the_same() const;
      bool all_space_groups_the_same() const;
      int  number_of_poses();
      int  number_of_holds();
      int  number_of_seeks();
      bool is_all_turn() const;
      bool is_all_gyre() const;
      bool is_all_curl() const;
      bool is_all_pose() const;
      bool is_all_tncs_modelled() const;
      bool has_no_turn_curl_pose() const;
      bool is_all_full_or_part() const;
      af_string add_pose_orientations();
      void apply_clear_last_tncs_group_pose(std::string);
 //     void apply_background_reduction_to_gyre();
 //     void apply_background_reduction_to_curl();
      void apply_unit_cell( cctbx::uctbx::unit_cell );
      void apply_sort_llg();
      void apply_sort_mapllg();
      double apply_partial_sort_and_percent_llg(const double,const double,const double);
      double apply_partial_sort_and_percent_llg_range(const std::pair<double,double>,const double,const double);
      double apply_partial_sort_and_percent_mapllg(const double,const double);
      void apply_partial_sort_and_maximum_stored_llg(const int);
      void apply_partial_sort_and_maximum_stored_mapllg(const int);
      void apply_maximum_stored(const int);
      void flag_tncs_duplicates(dvect3);
      setmodlid_t used_modlid() const;
      setmodlid_t used_modlid_pose(std::string="") const;
      setmodlid_t used_modlid_full_and_part() const;
      af::shared<Identifier> list_of_modlid() const; //python
      void erase_invalid();
      int  llg_precision(boost::logic::tribool) const;
      int  llg_width(boost::logic::tribool) const;
      double top_mapllg() const;
      double top_tfz() const;
      double top_tfz_packs() const;
      std::pair<bool,double> top_llg_packs() const;
      double top_llg() const;
      double low_llg() const;
      std::pair<bool,double> top_novel_turn_llg_packs() const;
      double average_llg();
      int  number_of_parents() const;
      bool multiple_spacegroups() const;
      bool multiple_vrms_values();
      void set_reflid(Identifier);
      void set_hall(std::string);
      void apply_sort_for_search();
      std::tuple<std::string,std::string,Identifier>  duplicate_equivalent(int i) const;
      void set_node(int i,dag::Node n) { node_list[i] =  n; }

  //------------------------------
  //wrap this class so it behaves like a std::vector, with added functions above
  public:
  NodeList& operator=(const dag::NodeList& other)
  { node_list = other.node_list; return *this; }

  typedef std::vector<dag::Node>::value_type value_type;
  typedef std::vector<dag::Node>::size_type size_type;
  typedef std::vector<dag::Node>::difference_type difference_type;
  typedef std::vector<dag::Node>::reference reference;
  typedef std::vector<dag::Node>::const_reference const_reference;
  typedef std::vector<dag::Node>::iterator iterator;
  typedef std::vector<dag::Node>::const_iterator const_iterator;

  const_reference operator[](size_type t) const { return node_list[t]; }
  reference operator[](size_type t) { return node_list[t]; }
  iterator begin() { return node_list.begin(); }
  iterator end() { return node_list.end(); }
  const_iterator begin() const { return node_list.begin(); }
  const_iterator end() const { return node_list.end(); }
  size_type size() const { return node_list.size(); }
  iterator insert(iterator pos, const value_type& v) { return node_list.insert(pos, v); }
  iterator erase(iterator pos) { return node_list.erase(pos); }
  iterator erase(iterator first, iterator last) { return node_list.erase(first, last); }
  void push_back(const value_type& v) { node_list.push_back(v); }
  void pop_back() { node_list.pop_back(); }
  void clear() { node_list.clear(); }
  void resize(int s) { node_list.resize(s);  }
  reference back() { return node_list.back(); }
  const_reference back() const { return node_list.back(); }
  reference front() { return node_list.front(); }
  const_reference front() const { return node_list.front(); }
  };

} //end dag scope
} //phasertng
#endif
