//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SOLPAR_Class__
#define __SOLPAR_Class__
#include <phaser/include/data_solpar.h>

namespace phaser {

    inline double solTerm(double ssqr,data_solpar& solpar)
    {
      double Babinet(1-solpar.SIGA_FSOL*std::exp(-solpar.SIGA_BSOL*ssqr/4)) ;
      return std::max(solpar.SIGA_MIN,Babinet);
    }

} //phaser

#endif
