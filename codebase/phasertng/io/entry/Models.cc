//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Models.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/constants.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <mmtbx/geometry/calculator.hpp>
#include <scitbx/math/superpose.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <mmtbx/geometry/calculator.hpp>
#include <iotbx/pdb/hierarchy.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <fstream>

namespace phasertng {

  void
  Models::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());

    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    infile.close();
    SetPdb(pdblines);
  }

  void
  Models::SetPdb(af_string pdblines)
  {
    PDB.clear();
    //The first ensemble files may not have model serials that are serial,
    //so use a map
    //After the ensemble correlation analysis, the pdb files will be in correct format
    //with serial numbers starting at 1 and the arrays for SERIAL matching the coordinates
    //start by loading the coordinates and the serial information into a map
    // mapping between the serial n and the data
    //rearrange into vectors of data at the end, which will be rationalized on writing
    std::unordered_map<int, std::vector<PdbRecord> > mappdb;
    int current_serial = 0; //current map serial number for indexing mappdb, zero by default
    //maprms will not use current_serial for index, but rms_serial on the remark line
    std::unordered_map<int, double>  maprms;

    //ignore H
    //ignore water
    //ignore anisotropic b-factors
    try {
      for (auto buffer : pdblines)
      {
        PdbRecord next_atom;
        //only allow ATOM cards at start
        //ignore anisotropic b-factors
        if (!buffer.find("CRYST1"))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string cryst1; af::double6 uc;
            sstream >> cryst1;
            sstream >> uc[0];
            sstream >> uc[1];
            sstream >> uc[2];
            sstream >> uc[3];
            sstream >> uc[4];
            sstream >> uc[5];
            std::string sg = buffer.substr(55,10);
            //int z = std::atoi(buffer.substr(66,4).c_str()); //Z is ignored
            INPUT_CELL = UnitCell(uc);
            SpaceGroup INPUT_SG(sg); //may be interesting for comparing input and output solutions
          }
          catch (...) { pdb_errors.insert("Error reading CRYST1"); }
        }
        else if (!buffer.find("ATOM") or !buffer.find("HETATM")) //ATOM and optionally HETATOM at start
        {
          std::unordered_set<std::string> this_pdb_errors;
          next_atom.ReadAtomCard(buffer);
          bool mse = next_atom.ResName == "MSE" or next_atom.ResName == "SME";
          if (next_atom.ResName == "SME") next_atom.ResName = "MSE";
          //there are other modified residues that could be in the main chain, and should not break the chain e.g 2zit
          std::set<std::string> modres = {"MSE","DDE","CSO","CME","TPO","KYN","SEP","MHA","HIE","HID","ACE","NH2","PYL","B3B","NLE","MPT"};
          SELENOMETHIONINE = (mse or SELENOMETHIONINE);
          if (!USE_MSE and mse)
          { //convert HETATM selenomethionine to ATOM methionine
            next_atom.ResName = "MET";
            next_atom.Hetatm = false;
            if (next_atom.Element == "SE")
            {
              next_atom.set_element("S"); //bad geometry!
              next_atom.AtomName = " S  ";
            }
            SELENOMETHIONINE = false;
          }
          if (!buffer.find("HETATM"))
          {
            next_atom.O = 0.0;
          }
          if (!buffer.find("ATOM") or !buffer.find("HETATM") or mse or modres.count(next_atom.ResName) )
          {
            if (next_atom.O < 0.0 and next_atom.AtomName != "UNK ")
              this_pdb_errors.insert("Atom has negative occupancy");
            if (next_atom.at_xplor_infinity() and next_atom.O != 0)
              this_pdb_errors.insert("Atom has coordinates at \"infinity\"");
            if (!next_atom.valid_element)
              this_pdb_errors.insert("Atom element not recognised");
            if (!this_pdb_errors.size() and
                !next_atom.is_water() and      //ignore waters
                next_atom.valid_element and
                next_atom.Element != "H" and   //ignore hydrogens
                next_atom.O >= 0)
            {
              //atom only accepted if it has an element type
              //must increment mappdb before adding atoms
              //If there are no model cards defined, assume MODEL 0
              //if model card is defined (must be, for more that 1) then increment is below
              if (!mappdb.count(current_serial) and current_serial == 0)
                mappdb[current_serial] = std::vector<PdbRecord>(0); //initialize the first map
              mappdb[current_serial].push_back(next_atom); //if above throws error this record is ignored
            }
            pdb_errors.insert(this_pdb_errors.begin(),this_pdb_errors.end());
          }
        }
        else if (!buffer.find("SSBOND"))
        {
          COUNT_SSBOND++;
        }
        else if (!buffer.find("MODEL"))
        {
          //parse the MODEL line to extract SERIAL for comparison with REMARK
          std::stringstream sstream(buffer);
          try
          {
            std::string MODEL;
            sstream >> MODEL >> current_serial; //change the index for the mappdb here
            if (current_serial != 0 and mappdb.count(current_serial))
              pdb_errors.insert("MODEL <n> serial number repeated");
            //clears the coordinates to zero if there is a repeated map index
            //error should be thrown to prevent this from continuing
            mappdb[current_serial] = std::vector<PdbRecord>();
          }
          catch (std::stringstream::failure& e) { } //do nothing if parsing fails
        }
        else if (!buffer.find("REMARK " + constants::pdb_remark_number))
        {
          //buffer = stoupper(buffer); tags can be lower case
          std::stringstream sstream(buffer);
          std::string remark,remarktag,keyword,tag,number;
          sstream >> remark >> remarktag >> keyword;
          try
          {
            if (keyword == "ENSEMBLE") //this is generated by sculptor or the like
            {
              sstream >> tag;
              if (tag == "MODEL")
              { //first pass input by user/voyager may not be correct
                int rms_serial(0);
                sstream >> rms_serial;
                if (rms_serial < 0)
                  pdb_errors.insert("Model serial negative");
                std::string rms;
                sstream >> rms;
                if (rms != "RMS")
                  pdb_errors.insert("Model serial rms parse error");
                double vrms(0);
                sstream >> vrms;
                if (vrms < 0)
                  pdb_errors.insert("Model serial rms negative");
                maprms[rms_serial] = vrms;
              }
            }
          }
          catch (...) { pdb_errors.insert("Model vrms remark parsing read error");}
          try //the ones generated internally, so should always be ok with parsing
          {
            if (keyword == "SERIAL")
            { //first pass input by user/voyager may not be correct
              sstream >> tag;
              //card identified with REMARK remark_tag SERIAL MODEL
              if (tag == "VRMS")
              {
                double value;
                sstream >> number >> value; //number not used, just pushback in order
                SERIAL.vrms_input.push_back(value);
                sstream >> value;
                SERIAL.vrms_start.push_back(value);
                sstream >> value;
                SERIAL.vrms_upper.push_back(value);
                sstream >> value;
                SERIAL.vrms_lower.push_back(value);
              }
              else if (tag == "DRMS")
              {
                sstream >> SERIAL.drms_range.first >> SERIAL.drms_range.second >> SERIAL.drms_first;
              }
              else if (tag == "BFAC")
              {
                double value;
                sstream >> number >> value; //number not used, just pushback in order
                RELATIVE_WILSON_B.push_back(value);
              }
            }
/*
            else if (keyword == "TURN_PT")
            {
              sstream >> TURN_PT[0];
              sstream >> TURN_PT[1];
              sstream >> TURN_PT[2];
            }
            else if (keyword == "REORIENT_1")
            {
              sstream >> REORIENT_PR[0];
              sstream >> REORIENT_PR[1];
              sstream >> REORIENT_PR[2];
            }
            else if (keyword == "REORIENT_2")
            {
              sstream >> REORIENT_PR[3];
              sstream >> REORIENT_PR[4];
              sstream >> REORIENT_PR[5];
            }
            else if (keyword == "REORIENT_3")
            {
              sstream >> REORIENT_PR[6];
              sstream >> REORIENT_PR[7];
              sstream >> REORIENT_PR[8];
            }
*/
            else if (keyword == "REFLID")
            {
              sstream >> number >> tag;
              //REFLID is overwritten here
              REFLID.parse_string(number,tag);
            }
            else if (keyword == "MODLID")
            {
              sstream >> number >> tag;
              MODLID.parse_string(number,tag);
            }
            else if (keyword == "TIMESTAMP")
            {
              sstream >> TIMESTAMP;
            }
          }
          catch (...) { pdb_errors.insert("Model remark parsing read error");}
        }
        else if (!buffer.find("REMARK"))
        { //boring remark
          REMARKS.push_back(buffer);
        }
       // else if (!buffer.find("TER")) //no action required
       // else if (!buffer.find("ENDMDL")) //no action required
       // else if (!buffer.find("END")) break; //don't read any more
       // pymol outputs END then ENDMDL
      }
    }
    catch(...) {}

    //do coordinates first
    std::unordered_map<int,int> indexing;
    int index(0);
    PDB.resize(mappdb.size());
    for (auto item : mappdb)
    {
      PHASER_ASSERT(index < PDB.size());
      PDB[index] = item.second;
      indexing[item.first] = index++;
    }
    phaser_assert(PDB.size());
    phaser_assert(PDB[0].size());
    //now setup the variances, using ECA input first
    if (maprms.size()) //takes precendent over maprms
    {
      SERIAL.resize(PDB.size()); //not resized indicates none present
      if (maprms.size() == 1) //special case for where the coordinates are not wrapped in a MODEL
      {
        //don't check that the model serials match, just set
        SERIAL.add_rms(0,maprms.begin()->second);
      }
      else
      {
        for (auto item : maprms)
        {
          if (!mappdb.count(item.first))
            pdb_errors.insert("RMS serial " + std::to_string(item.first) +" not in MODEL serial");
          int index = indexing[item.first];
          if (!SERIAL.add_rms(index,item.second))
            pdb_errors.insert("RMS serial not in MODEL serial");
        }
      }
    }
  }

  void
  Models::reorient_on_centre(dmat33 pr,dvect3 pt,dvect3 centre)
  {
    //all data from all ensembles, not biased by choice of MTRACE
    for (int m = 0; m < PDB.size(); m++)
    {
      std::vector<PdbRecord>& mPDB = PDB.at(m);
      for (int a = 0; a < mPDB.size(); a++) //m = 0, not the lowest rms (default)
      {
        PdbRecord& A = mPDB.at(a);
        //move to centre
        A.X = pr*A.X + pt + centre;
      }
    }
  }

  void
  Models::recentre(dvect3 centre)
  {
    //all data from all ensembles, not biased by choice of MTRACE
    for (int m = 0; m < PDB.size(); m++)
    {
      std::vector<PdbRecord>& mPDB = PDB.at(m);
      for (int a = 0; a < mPDB.size(); a++) //m = 0, not the lowest rms (default)
      {
        PdbRecord& A = mPDB.at(a);
        //move to centre
        A.X -= centre;
      }
    }
  }

  bool
  Models::is_atom() const
  {
    if (PDB.size() > 1) return false; //input
    //calculate if it is a single atom
    bool this_is_one_atom = false;
    int natom(0);
    for (int a = 0; a < PDB[0].size(); a++)
    {
      if (PDB[0][a].O > 0.0)
      {
        natom++;
        if (natom >= 2) break; //molecule
      }
    }
    if (natom == 1)
      this_is_one_atom = true;
    return this_is_one_atom;
  }

  bool
  Models::is_helix() const
  {
    if (is_atom()) return false;
    //calculate if it is a helix
    af::shared< scitbx::vec3< double > > polyala_segment,reference_segment;
    bool this_is_a_helix = true;
    int ca(0);
    double max_rmsd(2.0); //Glykos test case rmsd=0.52 #Ca=23
                          //4rsj coiled coil tail test case, woggly around residue 666
    double lowrmsd(0),highrmsd(0);
    int min_helix_length(7);
    PHASER_ASSERT(PDB[0].size());
    auto chain = PDB[0][0].Chain;
    auto lastX = PDB[0][0].X;
    int nhelix(0);
    for (int a = 0; a < PDB[0].size(); a++)
    {
      const PdbRecord& A = PDB[0][a];
      dvect3 delta = A.X-lastX;
      auto casep = delta.length(); //check this is not a break in the chain, distance > 3.8+tol
      if (A.is_calpha() and A.Chain == chain and casep < 4.) //must be protein, not a general trace atom (dna)
      {
        reference_segment.push_back(A.X);
        //scitbx::vec3< double > N (-1.488, -0.324,  0.000);
        scitbx::vec3< double > CA(-1.729, -1.484,  0.869);
        //scitbx::vec3< double > C (-0.653, -1.558,  1.954);
        //scitbx::vec3< double > O (-0.955, -1.744,  3.143);
        //scitbx::vec3< double > CB(-1.695, -2.748,  0.062);
        dmat33 helixrot = scitbx::math::euler_angles::zyz_matrix(ca*100.,0.,0.);
        dvect3 helixtra(0,0,1.501*ca);
        CA = helixrot*CA + helixtra;
        polyala_segment.push_back(CA);
        scitbx::math::superpose::least_squares_fit<double> lsf(reference_segment,polyala_segment);
        double rmsd = lsf.other_sites_rmsd();
        ca++;
        //only test 7 residues (two turns) so as to identify bent helices
        if (polyala_segment.size() >= min_helix_length)
        {
          (rmsd < 0.5) ? lowrmsd++ : highrmsd++;
          if (rmsd > max_rmsd)
          {
            this_is_a_helix = false;
            break;
          }
          polyala_segment.erase(polyala_segment.begin());
          reference_segment.erase(reference_segment.begin());
        }
        lastX = A.X;
      }
      else if (A.is_calpha())
      {
        //start again on the next chain, this_is_a_helix is currently true;
        nhelix++;
        chain = A.Chain; //may be no change
        ca = 0; //start 7-mer again
        polyala_segment = af::shared< scitbx::vec3< double > >();
        reference_segment = af::shared< scitbx::vec3< double > >();
        lastX = A.X;
      }
    }
    if (ca <= min_helix_length and nhelix == 0) //only one helix
    {
      this_is_a_helix = false;
    }
    //this is an extra test on the tightness of the helices
    //if the number of fragments with highrmsd is more than 80% of the total number of fragments
    //then this is a very wobbly helix, lets leave it out
    if (highrmsd > 0.8*(highrmsd+lowrmsd))
    {
      this_is_a_helix = false;
    }
    return this_is_a_helix;
  }

  bool
  Models::bfactors_damped_after_relative_wilson_b() const
  {
    phaser_assert(PDB.size() == RELATIVE_WILSON_B.size());
    for (int m = 0; m < PDB.size(); m++)
      for (int a = 0; a < PDB[m].size(); a++)
        if (PDB[m][a].B < RELATIVE_WILSON_B[m])
          return true;
    return false;
  }

  void
  Models::GetPdbHandler(PdbHandler& pdbhandler)
  {
    //PHASER_ASSERT(REFLID.is_set_valid());
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(TIMESTAMP.size());
    //SpaceGroup P1;
    //PdbHandler pdbhandler(P1,INPUT_CELL,1);
    for (auto item : REMARKS)
    {
      pdbhandler.addVanillaRemark(item);
    }
    pdbhandler.addRemarkPhaser("REFLID " + REFLID.string() + " " + REFLID.tag());
    pdbhandler.addRemarkPhaser("MODLID " + MODLID.string() + " " + MODLID.tag());
    pdbhandler.addRemarkPhaser("TIMESTAMP " + TIMESTAMP);
    char buf[80];
   // snprintf(buf,80,"%15.12f %15.12f %15.12f",TURN_PT[0],TURN_PT[1],TURN_PT[2]);
    //pdbhandler.addRemarkPhaser("TURN_PT " + std::string(buf));
    if (RELATIVE_WILSON_B.size())
    {
      int n(SERIAL.vrms_input.size());
      PHASER_ASSERT(RELATIVE_WILSON_B.size() == SERIAL.vrms_input.size());
      snprintf(buf,80,"%8.6f %8.6f %8.6f",
               SERIAL.drms_range.first,SERIAL.drms_range.second,SERIAL.drms_first);
      pdbhandler.addRemarkPhaser("SERIAL DRMS " + std::string(buf));
      for (int s = 0; s < n; s++) //strictly limited to 80 characters wide
      {
        snprintf(buf,80,"%d %8.6f %8.6f %8.6f %8.6f",
                 s+1,SERIAL.vrms_input[s],
                     SERIAL.vrms_start[s],
                     SERIAL.vrms_upper[s],
                     SERIAL.vrms_lower[s]);
        pdbhandler.addRemarkPhaser("SERIAL VRMS " + std::string(buf));
        snprintf(buf,80,"%d %7.3f",
                 s+1,RELATIVE_WILSON_B[s]);
        pdbhandler.addRemarkPhaser("SERIAL BFAC " + std::string(buf));
      }
    }
    for (int m = 0; m < PDB.size(); m++)
    {
      pdbhandler.addModel();
      for (int a = 0; a < PDB[m].size(); a++)
      {
        PdbRecord card = PDB[m][a];
        pdbhandler.addRecord(card);
      }
    }
   // return pdbhandler;
  }

  void
  Models::WritePdb()
  {
    SpaceGroup P1;
    PdbHandler pdbhandler(P1,INPUT_CELL,1);
    GetPdbHandler(pdbhandler);
    pdbhandler.VanillaWritePdb(*this);
  }

  af_string
  Models::get_pdb_cards()
  {
    SpaceGroup P1;
    PdbHandler pdbhandler(P1,INPUT_CELL,1);
    GetPdbHandler(pdbhandler);
    return pdbhandler.PdbString();
  }

  std::unordered_set<std::string>
  Models::set_of_atomtypes() const
  {
    std::unordered_set<std::string> ATOMTYPES;
    for (int s = 0; s < PDB.size(); s++)
    {
      for (int a = 0; a < PDB.at(s).size(); a++)
      {
        ATOMTYPES.insert(PDB.at(s).at(a).get_element());
      }
    }
    return ATOMTYPES;
  }

  std::string
  Models::read_errors() const
  {
    std::string result;
    for (auto item : pdb_errors)
      result += "\n" + item;
    return result;
  }

  void
  Models::move_and_align_to_principal_axes(dmat33 PR,dvect3 PT)
  { //only called once for output, then delete model
    for (int m = 0; m < PDB.size(); m++)
    {
      std::vector<PdbRecord>& mPDB = PDB.at(m);
      for (int a = 0; a < mPDB.size(); a++) //m = 0, not the lowest rms (default)
      {
        PdbRecord& A = mPDB.at(a);
        A.X = PR*A.X + PT;
      }
    }
  }

  int
  Models::mtrace() const
  {
    int bestm = 0;
    for (int m = 1; m < SERIAL.vrms_start.size(); m++)
    {
      if (SERIAL.vrms_start.at(m) < SERIAL.vrms_start.at(bestm))
      {
        bestm = m;
      }
    }
    return bestm;
  }

  composition_t
  Models::composition(int m)
  {
    PHASER_ASSERT(m < PDB.size());
    composition_t AssemblyComp;
    for (int a = 0; a < PDB[m].size(); a++)
    {
      if ( PDB[m][a].O > 0.0 and
          (PDB[m][a].AltLoc == " " or PDB[m][a].AltLoc == "A") and //only count altloc once
           PDB[m][a].AtomName != "UNK ") //already ignored
      {
        std::string element = PDB[m][a].get_element();
        if (AssemblyComp.count(element))
          AssemblyComp[element] += 1;
        else
          AssemblyComp[element] = 1;
      }
    }
    return AssemblyComp;
  }

  sv_int
  Models::scattering()
  {
    sv_int SCATTERING(PDB.size(),0);
    for (int m = 0; m < PDB.size(); m++)
    {
      auto AssemblyComp = composition(m);
      for (auto item : AssemblyComp)
      {
        double atomic_number(0);
        try {
          cctbx::eltbx::tiny_pse::table cctbxAtom(item.first);
          atomic_number = cctbxAtom.atomic_number();
        }
        catch (std::exception const& err)  {}
        SCATTERING[m] += item.second*scitbx::fn::pow2(atomic_number);
      }
    }
    return SCATTERING;
  }

  sv_double
  Models::molecular_weight()
  {
    sv_double MW(PDB.size(),0);
    for (int m = 0; m < PDB.size(); m++)
    {
      auto AssemblyComp = composition(m);
      for (auto item : AssemblyComp)
      {
        double weight(0);
        try {
        cctbx::eltbx::tiny_pse::table cctbxAtom(item.first);
        weight = cctbxAtom.weight();
        }
        catch (std::exception const& err)  {}
        MW[m] += item.second*weight;
      }
    }
    return MW;
  }

  void
  Models::convert_rmsd_to_bfac()
  {
    for (int m = 0; m < PDB.size(); m++)
    {
      double minB(std::numeric_limits<double>::max());
      for (int a = 0; a < PDB[m].size(); a++)
      {
        double thisB = fn::pow2(scitbx::constants::pi*PDB[m][a].B)*8/3;
        PDB[m][a].B = thisB;
        minB = std::min(minB, thisB);
      }
      // Structure factors will be normalised, so only delta-B from min matters
      // Avoid situation where all Fs are zero at high resolution
      if (minB > 200.) // Don't change if unnecessary
      {
        double deltaB = minB - 200.;
        for (int a = 0; a < PDB[m].size(); a++)
        {
          PDB[m][a].B -= deltaB;
        }
      }
    }
  }

  double
  Models::trim(double threshold)
  {
    double trimmed(0),total(0);
    for (int m = 0; m < PDB.size(); m++)
    {
      for (int a = 0; a < PDB[m].size(); a++)
      {
        if (PDB[m][a].B >= threshold)
        {
          PDB[m][a].O = 0;
          trimmed++;
        }
        total++;
      }
    }
    return 1.-(trimmed/total);
  }

  bool
  Models::is_random_coil(double coil_fraction)
  {
    table::vanderwaals_radii vdw(1.4); //oxygen, smaller than carbon 1.7
    std::vector<PdbRecord>& pdb0 = PDB[0];
    sv_double myradii(pdb0.size(),0);
    for (int a = 0; a < pdb0.size(); a++)
      myradii[a] = vdw.get(pdb0[a].get_element());

    int    sampling_point_count = 960; //default
    double probe = 1.4; //water
    mmtbx::geometry::asa::calculator::SimpleCalculator<
      mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
      sv_double // arrays can be used directly without adaptors
     >
     mycalc(
       mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >( pdb0 ),
       myradii,
       probe, // probe radius
       sampling_point_count // sampling point count
     );

    double exposed_asa_fraction_points(0.01); //hardwired value for asa to be considered exposed
    double exposed(0),total(0);
    for (int a = 0; a < pdb0.size(); a++)
   // if (pdb0[a].is_calpha()) //use cbeta not the calpha
    if (pdb0[a].AtomName == " CB ") //https://link.springer.com/article/10.1007/s00894-009-0454-9
    {
      double asa_frac = mycalc.accessible_points(a)/double(sampling_point_count);
      if (asa_frac > exposed_asa_fraction_points) exposed++;
      total++;
    }
    if (!total) return false;
    return exposed/total > coil_fraction;
  }

  void
  Models::renumber()
  { //renumber atoms
    //reallocate chains in order of chain appearance in file so ABCA -> ABCD
    //corrects concatenated pdb files
    for (int m = 0; m < PDB.size(); m++)
    {
      std::string last_chain = "this is not a valid chain";
      std::string last_allocated_chain = "";
      int AtomNum(1);
      pdb_chains_2chainid chains; //so that X is included
      auto& pdb = PDB[m];
      for (int a = 0; a < pdb.size(); a++)
      {
        PdbRecord& card = pdb[a];
        if (card.Chain != last_chain)
        {
          last_chain = card.Chain; //last chain is the last seen in the pdb file, not allocated
          card.Chain = chains.allocate(card.Chain); //give it a new identifier
          last_allocated_chain = card.Chain; //and record the new identifier for rest of the chain
        }
        else
        {
          card.Chain = last_allocated_chain; //rest of the same chain
        }
        card.AtomNum = AtomNum++;
      }
    }
  }

  void
  Models::set_principal_statistics()
  {
    if (is_atom()) //overwrite
      statistics.set_atom();
    else{
    af_dvect3 atom_list;
    for (int m = 0; m < PDB.size(); m++)
      for (int a = 0; a < PDB[m].size(); a++)
        if (PDB[m][a].O > 0)
          atom_list.push_back(PDB[m][a].X);
    statistics.set(atom_list);
    }
  }

  void
  Models::set_principal_components()
  {
    if (!is_atom()) //overwrite
    {
    af_dvect3 atom_list;
    for (int m = 0; m < PDB.size(); m++)
      for (int a = 0; a < PDB[m].size(); a++)
        if (PDB[m][a].O > 0)
          atom_list.push_back(PDB[m][a].X);
    components.set(atom_list);
    }
  }

  void
  Models::conserved_trace(std::vector<PdbRecord>& trace,double& SAMPLING_DISTANCE) const
  {
    phaser_assert(PDB.size());
    //make sure identifiers are set externally
    SAMPLING_DISTANCE = 1.5; //atomic separation
    //select conserved atoms, first altloc, no hetatms or waters
    for (int a = 0; a < PDB[0].size(); a++)
    {
      const PdbRecord& A = PDB[0][a];
      if (A.O != 0.0 and (A.AltLoc == " " or A.AltLoc == "A") and !A.Hetatm and !A.is_water())
      {
        trace.push_back(A); //std::list
      }
    }
    for (auto& item : trace)
    {
      item.valid_element = true;
    }
    for (int m = 1; m < PDB.size(); m++) //start after first which is now trace
    {
      using namespace mmtbx::geometry::asa::calculator;
      using namespace mmtbx::geometry::asa::calculator::utility;
      ConstRadiusCalculator<
          TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
          double >
      other(
             TransformedArray<std::vector<PdbRecord>,PdbRecord_getter >( PDB[m] ),
             SAMPLING_DISTANCE
           );
      for (auto& item : trace)
      { //must be overlapping in all other pdb files
        item.valid_element = item.valid_element and
                             other.is_overlapping_sphere(item.X,SAMPLING_DISTANCE);
      }
    }
    if (trace.size())
    {
      size_t last = 0;
      for (size_t i = 0; i < trace.size(); i++)
      { if (trace[i].valid_element) { trace[last++] = trace[i]; } } //fill start with interesting, in order
      trace.erase(trace.begin() + last, trace.end()); //erase the end, which is not rubbish
      //return trace;
    }
  }

  void
  Models::get_best_pdb_cards(
    Identifier& reflid,
    Identifier& modlid,
    std::string& timestamp,
    std::vector<PdbRecord>& refined) const
  {
    reflid = REFLID;
    modlid = MODLID;
    timestamp = TIMESTAMP;
    auto trace = PDB[mtrace()];
    for (int a = 0; a < trace.size(); a++)
      refined.push_back(trace[a]);
  }
} //phasertng
