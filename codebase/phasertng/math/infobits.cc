#include <phasertng/main/Assert.h>
#include <phasertng/math/infobits.h>
#include <phasertng/math/likelihood/pBijvoet.h>
#include <scitbx/math/erf.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>
#include <phasertng/math/table/sim.h>
#include <phasertng/math/table/ParabolicCylinderD.h>

using namespace scitbx;

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
extern const table::sim& tbl_sim;
extern const table::ParabolicCylinderD& tbl_pcf;
namespace math {

  double infobits::EosqInfoBitsAcen(double eosq, double sigesq)
  {
    phaser_assert(sigesq > 0.); // Only relevant for real imperfect data
    double sigesqsqr(fn::pow2(sigesq)), eosqsqr(fn::pow2(eosq));
    double X((-eosq+sigesqsqr)/(SQRT2*sigesq));
    double term1B((eosqsqr-fn::pow2(sigesqsqr))/(2*sigesqsqr));
    double term1A,term3;
    if (X > 0)
    {
      double erfcxterm(scitbx::math::erfcx(X));
      term1A = (eosq+sigesqsqr)/(SQRT2PI*sigesq*erfcxterm);
      term3 = -std::log(sigesq*erfcxterm);
    }
    else
    {
      double erfcterm(scitbx::math::erfc(X));
      term1A = (X > -9.) ? // Avoid underflow of exponential
               std::exp(-fn::pow2(X))*(eosq+sigesqsqr)/(SQRT2PI*sigesq*erfcterm) :
               0.;
      term3 = -(std::log(sigesq*erfcterm) + fn::pow2(X));
    }
    // Dividing by log2 turns nats into bits
    double infobits = ((term1A+term1B) + term2 + term3)/LOG2;
    return infobits;
  }

  double infobits::EosqInfoBitsCen(double eosq, double sigesq)
  {
    phaser_assert(sigesq > 0.); // Only relevant for real imperfect data
    double sigsqr(fn::pow2(sigesq)),sqrtsig(std::sqrt(sigesq));
    double X((sigsqr-TWO*eosq)/(TWO*sigesq));
    double spcd = scaledPCDmhalf(X);
    double infobits(ZERO);
    int npts(30); // Number of points in numerical integral
    int nsigma(9); // Controls range of numerical integral
    double emin = std::sqrt(std::max(ZERO,eosq-nsigma*sigesq));
    double emax = std::min(std::max(eosq/sigesq+FOUR,7.) ,
                           std::sqrt(std::max(ZERO,eosq)+nsigma*sigesq));
    double de = (emax-emin)/(npts-1);
    double scale1 = ONE/(SQRTPI*sqrtsig*spcd);
    double const1 = std::log(TWO/sigesq) - TWO*std::log(spcd);
    if (X >= ZERO)
      scale1 /= sigsqr;
    else
    {
      const1 += eosq - sigsqr/FOUR;
    }
    for (int i = 0; i < npts; i++)
    {
      double e = emin + i*de;
      double esqr(fn::pow2(e));
      // For numerical stability use Horner form for exparg calculation
      double exparg = (X >= ZERO) ?
        (esqr*(-esqr+TWO*eosq-sigsqr))/(TWO*sigsqr) :
        (-fn::pow2(sigsqr)+esqr*(-FOUR*esqr+EIGHT*eosq-FOUR*sigsqr) +
             eosq*(-FOUR*eosq+FOUR*sigsqr)) /
            (EIGHT*sigsqr);
      phaser_assert(exparg < MAXEXPTERM);
      if (exparg > -50.) // Ignore extremely small terms
      {
        double expterm = std::exp(exparg);
        double intfac = (i==0 || i==npts-1) ? ONE : TWO; // Trapezoidal rule
        double term1;
        term1 = (X >= ZERO) ? -fn::pow2(esqr) + TWO*esqr*eosq + sigsqr*const1 :
                            const1 - fn::pow2(esqr-eosq)/sigsqr;
        infobits += intfac*scale1*term1*expterm;
      }
    }
    infobits *= de/TWO;
    infobits /= LOG2; // Convert nats to bits
    return infobits;
  }

  double infobits::SADextraBits(double zopos, double sigzpos, double zoneg,
    double sigzneg, double rhopm, double absrhoFF)
  {
    math::pBijvoet pbijvoet;
    // Extra information content, in bits, from Friedel pair of normalised observed
    // intensities (i.e. Z-values), compared to weighted mean intensity
    // Requires double numerical integral over values of true Eneg and true Epos,
    // which is restricted to region of significant probability
    phaser_assert((sigzpos > 0.) && (sigzneg > 0.)); // Real imperfect data
    phaser_assert((rhopm > -1.) && (rhopm < 1.)); // error correlation
    phaser_assert((absrhoFF > 0.) && (absrhoFF <= 1.)); // Absolute complex correlation
    if (absrhoFF >= 0.999999) // Effectively no anomalous scattering
      return ZERO;

    // Compute information content of mean intensity
    double vrho = 1. - fn::pow2(rhopm);
    double varzpos = fn::pow2(sigzpos);
    double varzneg = fn::pow2(sigzneg);
    double zdenom = varzpos + varzneg - 2.*rhopm*sigzpos*sigzneg;
    double zmean = (zopos*varzneg + zoneg*varzpos - (zopos + zoneg)*rhopm*sigzpos*sigzneg) / zdenom;
    double sigzmean = sigzpos*sigzneg * std::sqrt(vrho / zdenom);
    double imeaninfobits = EosqInfoBitsAcen(zmean,sigzmean);

    // Outer loop over 30 values of Eneg within 4.5 sigma of centroid given
    // both observations: decent tradeoff between time and accuracy
    int ndiv(30);
    double nsigma(4.5);
    std::pair<double,double> esigeneg =
      pbijvoet.expectedESIGEnegGivenPair(zopos,sigzpos,zoneg,sigzneg,absrhoFF);
    double enegmin = std::max(ZERO,esigeneg.first - nsigma*esigeneg.second);
    double enegmax = esigeneg.first + nsigma*esigeneg.second;
    double deneg = (enegmax - enegmin)/ndiv;
    double enegstart = enegmin + deneg/2.; // Midpoint rule
    double sadinfo(0),pobs(0);
    for (double eneg = enegstart; eneg < enegmax; eneg += deneg)
    {
      // Inner loop over 30 values of Epos within 4.5 sigma of centroid given
      // observation for positive and current value of Eneg
      std::pair<double,double> esigepos =
        pbijvoet.expectedESIGEposGivenZoposEneg(zopos,sigzpos,eneg,absrhoFF);
      double eposmin = std::max(ZERO,esigepos.first - nsigma*esigepos.second);
      double eposmax = esigepos.first + nsigma*esigepos.second;
      double depos = (eposmax - eposmin)/ndiv;
      double eposstart = eposmin + depos/2.;
      for (double epos = eposstart; epos < eposmax; epos += depos)
      {
        double zpos = fn::pow2(epos);
        double zneg = fn::pow2(eneg);
        double exparg =
            -(fn::pow2((zopos-zpos)/sigzpos) +
              fn::pow2((zoneg-zneg)/sigzneg) -
            2.*rhopm*(zopos-zpos)*(zoneg-zneg)/(sigzpos*sigzneg)) /
            (2.*vrho);
        phaser_assert(exparg < MAXEXPTERM);
        double pgauss(0.);
        if (exparg > -50.)
          pgauss = std::exp(exparg) /
            (scitbx::constants::two_pi*std::sqrt(vrho)*sigzpos*sigzneg);
        double pprior = pbijvoet.pZposZneg(zpos,zneg,absrhoFF);
        double pprod = 4.*epos*eneg*depos*deneg*pgauss*pprior;
        if (pprod > 0.)
        {
          pobs += pprod;
          sadinfo += pprod*std::log(pgauss);
        }
      }
    }
    // Factor of log(2) converts information to bits
    sadinfo = (pobs > 0.) ? (sadinfo/pobs - std::log(pobs))/std::log(2.) :
                            imeaninfobits; // Trap result for undetected outlier
    return std::max(0.,sadinfo-imeaninfobits);
  }

  double infobits::expectedFisherWtAcen(double Eeff, double Dobs, double sigmaA)
  {
    double Dosiga(Dobs*sigmaA),term1,term2(ZERO);
    double var(ONE-fn::pow2(Dosiga));
    double EeffSqr(fn::pow2(Eeff));
    double X1(EeffSqr/(TWO*var));
    term1 = std::sqrt(scitbx::constants::pi*var) *
      ((var-EeffSqr)*tbl_ebesseli0.get(X1) - EeffSqr*tbl_ebesseli1.get(X1));
    // Numerical integration needed for term 2. Define mean and sigma of the
    // exponential part of the integrand to determine a sensible range
    double mu(Eeff/Dosiga),sigma(std::sqrt(var/(TWO*fn::pow2(Dosiga))));
    int nstep(31),nsigma(6.);
    double ecmin(std::max(ZERO,mu-nsigma*sigma)),ecmax(mu+nsigma*sigma);
    double delta((ecmax-ecmin)/nstep);
    for (int i = 0; i < nstep; i++)
    {
      double Ec = ecmin + (i+HALF)*delta;
      double expterm = std::exp(-fn::pow2(Dosiga*Ec-Eeff)/var);
      double X(TWO*Dosiga*Ec*Eeff/var);
      term2 += delta*expterm*tbl_sim.get(X)*tbl_ebesseli1.get(X);
    }
    term2 *= FOUR*Dosiga*EeffSqr;
    double wtarget = TWO*Dosiga*Eeff*(term1 + term2)/fn::pow3(var);
    return wtarget;
  }

  double infobits::expectedFisherWtCen(double Eeff, double Dobs, double sigmaA)
  {
    double Dosiga(Dobs*sigmaA),numint(ZERO);
    double var(ONE-fn::pow2(Dosiga));
    double EeffSqr(fn::pow2(Eeff));
    // Numerical integration needed for term 2. Define mean and sigma of the
    // exponential part of the integrand to determine a sensible range
    double mu(Eeff/Dosiga),sigma(std::sqrt(var/fn::pow2(Dosiga)));
    int nstep(31),nsigma(6.);
    double ecmin(std::max(ZERO,mu-nsigma*sigma)),ecmax(mu+nsigma*sigma);
    double delta((ecmax-ecmin)/nstep);
    for (int i = 0; i < nstep; i++)
    {
      double Ec = ecmin + (i+HALF)*delta;
      double expterm = std::exp(-fn::pow2(Dosiga*Ec-Eeff)/(TWO*var));
      double X(Dosiga*Ec*Eeff/var);
      numint += (X < 15.) ?
        delta*expterm*(ONE-std::exp(-TWO*X))*std::tanh(X) :
        delta*expterm;
    }
    numint *= Dosiga*EeffSqr/std::sqrt(var*scitbx::constants::two_pi);
    double wtarget = Dosiga*(var-EeffSqr+numint)/fn::pow2(var);
    return wtarget;
  }

  double infobits::scaledPCDmhalf(double x)
  {
  /* Compute scaled ParabolicCylinderD for va=-1/2, directly for smaller arguments or
     via asymptotic expansion for larger arguments.  This could easily be generalised
     like DVLA, at least for half-integer va.
     Scaled ParabolicCylinderD(-1/2,x) is rescaled by exponential factor to
     avoid overflow for large negative arguments and avoid underflow for large
     positive arguments, using the factor exp(x Sqrt(x^2)/4).
     The asymptotic expansions can be found in Abramowitz & Stegun, with equation
     19.8.1 for U(a,x) handling positive x and equation 19.8.2 for V(a,x) handling
     negative x.  Note that
       U(a,x) = D(-a-1/2,x) (A&S 19.3.7)
     and
       V(a,x) = D(-a-1/2,-x)/Sqrt(2) (A&S 19.3.8)
     so that a = 0 for va=-1/2, yielding the simplified expressions below.
  */

    const double CROSSOVER = (5.8); // Match range of dvsa
    double absx = std::abs(x);
    double spcd;
    if (absx < CROSSOVER)
    {
      spcd = std::exp(x*std::sqrt(fn::pow2(x))/FOUR) *
            tbl_pcf.get(-HALF,x);
    }
    else
    {
      int signx = (x>=ZERO) ? 1 : -1;
      double r(ONE);
      spcd = r;
      int k(1);
      double xsqr(fn::pow2(x));
      while ((k<=18) && (std::abs(r/spcd)>=EPS))
      {
        r *= -signx*(TWO*k-THREEHALF)*(TWO*k-HALF)/(TWO*k*xsqr);
        spcd += r;
        k++;
      }
      spcd /= std::sqrt(absx);
      if (x < ZERO) spcd *= SQRT2;
    }
    return spcd;
  }

}}
