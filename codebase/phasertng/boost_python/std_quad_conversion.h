#ifndef BOOST_ADAPTBX_STD_QUAD_CONVERSION_H
#define BOOST_ADAPTBX_STD_QUAD_CONVERSION_H

#include <boost/python/tuple.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/to_python_converter.hpp>

namespace boost_adaptbx { namespace std_quad_conversions {

  namespace detail {
    template <typename T, typename U, typename V, typename W>
    struct to_tuple
    {
      static PyObject* convert(std::tuple<T,U,V,W> const& p) {
        using namespace boost::python;
        return incref(boost::python::make_tuple(
              std::get<0>(p),
              std::get<1>(p),
              std::get<2>(p),
              std::get<3>(p)
             ).ptr());
      }

      static PyTypeObject const *get_pytype() { return &PyTuple_Type; }
    };
  }

  template <typename T, typename U, typename V, typename W>
  struct to_tuple
  {
    to_tuple() {
      using namespace boost::python;
      to_python_converter<std::tuple<T,U,V,W>, detail::to_tuple<T,U,V,W>
#ifdef BOOST_PYTHON_SUPPORTS_PY_SIGNATURES
                                    , true
#endif
      >();
    }
  };

  template <typename T, typename U, typename V, typename W>
  struct from_tuple
  {
    from_tuple() {
      using namespace boost::python::converter;
      registry::push_back(&convertible,
                          &construct,
                          boost::python::type_id<std::tuple<T,U,V,W> >()
                          #ifdef BOOST_PYTHON_SUPPORTS_PY_SIGNATURES
                          , get_pytype
                          #endif
      );
    }

    static const PyTypeObject *get_pytype() { return &PyTuple_Type; }

    static void *convertible(PyObject *o) {
      using namespace boost::python;
      if (!PyTuple_Check(o) || PyTuple_GET_SIZE(o) != 4) return 0;
      return o;
    }

    static void construct(
      PyObject *o,
      boost::python::converter::rvalue_from_python_stage1_data *data)
    {
      using boost::python::extract;
      using namespace boost::python::converter;
      PyObject *first  = PyTuple_GET_ITEM(o, 0),
               *second = PyTuple_GET_ITEM(o, 1),
               *third = PyTuple_GET_ITEM(o, 2),
               *fourth = PyTuple_GET_ITEM(o, 3);
      void *storage =
        ((rvalue_from_python_storage<std::tuple<T,U,V,W> >*) data)->storage.bytes;
      new (storage) std::tuple<T,U,V,W>(
               extract<T>(first),
               extract<U>(second),
               extract<V>(third),
               extract<W>(fourth)
             );
      data->convertible = storage;
    }
  };

  template <typename T, typename U, typename V, typename W>
  struct to_and_from_tuple
  {
    to_and_from_tuple() {
      to_tuple<T,U,V,W>();
      from_tuple<T,U,V,W>();
    }
  };


}}

#endif
