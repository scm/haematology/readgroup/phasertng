//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/CutoutDensity.h>
#include <cctbx/maptbx/copy.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/main/Error.h>
#include <phasertng/math/rotation/cmplex2polar_deg.h>
#include <phasertng/math/fft/real_to_complex_3d.h>

//#define PHASERTNG_DEBUG_CUTOUT_DENSITY
#ifdef PHASERTNG_DEBUG_CUTOUT_DENSITY
//#include <phasertng/main/jiffy.h>
#endif

namespace phasertng {


  af_string
  CutoutDensity::calculate(
      cctbx::sgtbx::space_group cctbxSG,
      cctbx::uctbx::unit_cell cctbxUC,
      af::shared<millnx> MILLER_RES_LIMITS_,
      af_cmplex FMAP,
      std::pair<dvect3,dvect3> extent
    )
  {
    af_string output;
    MILLER_RES_LIMITS = MILLER_RES_LIMITS_; //store for map output
    //FMAP = FMAP_.deep_copy();

    //need to take out the translational symmetry or the frequency analysis misses reflections
    //due to the systematic absences
    //also required for applying symmetry to patterson vectors!!
    UC = UnitCell(cctbxUC.parameters()); //store for frac->orth conversion
    SG = SpaceGroup("Hall: " + cctbxSG.type().hall_symbol());
    cctbx::sgtbx::space_group      cctbxsg(SG.group());
    cctbx::sgtbx::space_group_type SgInfo(cctbxsg);

    const af::double2 res_limits = cctbxUC.min_max_d_star_sq(MILLER_RES_LIMITS.const_ref());
    //double patt_lores = cctbx::uctbx::d_star_sq_as_d(res_limits[0]);
           cutout_density_hires = cctbx::uctbx::d_star_sq_as_d(res_limits[1]);

    // --- prepare fft grid
    //can't use full rfft class here because intermediates have to be stored
    fft::real_to_complex_3d RFFT(SgInfo,cctbxUC);
    RFFT.calculate_gridding(cutout_density_hires,{1,1},false);
    RFFT.allocate();
    if (RFFT.memory_allocation_error)
      throw Error(err::MEMORY,"CutoutDensity FFT array allocation");

    bool anomalous_flag(false);
    bool conjugate_flag(false);
    af::c_grid_padded<3> map_grid(RFFT.rfft.n_complex());
    cutout_density_gridding = RFFT.gridding;
    bool treat_restricted = true;
    cctbx::maptbx::structure_factors::to_map<double> cutout_density(
        cctbxsg,
        anomalous_flag,
        MILLER_RES_LIMITS.const_ref(),
        FMAP.const_ref(),
        RFFT.rfft.n_real(),
        map_grid,
        conjugate_flag,
        treat_restricted
      );
    af::ref<cmplex, af::c_grid<3> > cutout_density_fft_ref(
        cutout_density.complex_map().begin(),
        af::c_grid<3>(RFFT.rfft.n_complex())
     );

    // --- do the fft
    RFFT.rfft.backward(cutout_density_fft_ref);

    af::ref<double, af::c_grid_padded<3> > real_map_padded(
        reinterpret_cast<double*>( cutout_density.complex_map().begin()),
        af::c_grid_padded<3>( RFFT.rfft.m_real(), RFFT.rfft.n_real())
        );
   // real_map_unpadded is for writing the map but it appears to be rotated through 90 degrees
   // probably something to do with the sort order and the way coot reads the file
   // don't seem to be able to control sort order on write
   // real_map_unpadded = versa_grid_double( af::c_grid<3>(RFFT.rfft.n_real()));
   // cctbx::maptbx::copy(real_map_padded, real_map_unpadded.ref());

    double scalefac = (cutout_density_gridding[0]*
                       cutout_density_gridding[1]*
                       cutout_density_gridding[2]);
    //the 0 0 0 index of the map is at coordinates a,b,c and
    //the top grid corner of the map is at the origin of the coordinates
    //so we have to invert the coordinates to (1-x) to get the position in the map
    //I have no idea why it is this way round
   // for (int i = 0; i < 3; i++)
    //{ //AJM TODO account for when extent is not within the cell
     // phaser_assert(extent.first[i]>=0);
     // phaser_assert(extent.second[i]<1);
    //}
    ivect3 minext,maxext,site;
    dvect3 inv_first,inv_second;
    for (int i = 0; i < 3; i++)
    {
      inv_first[i] = 1-extent.second[i]; //2->1
      inv_second[i] = 1-extent.first[i]; //1->2
    }
    for (int i = 0; i < 3; i++)
    {
      minext[i] = scitbx::math::nearest_integer(inv_first[i]*cutout_density_gridding[i]);
      maxext[i] = scitbx::math::nearest_integer(inv_second[i]*cutout_density_gridding[i]);
    }
    int nflatten(0);
    ivect3 cell(0,0,0);
    for (site[0] = 0; site[0] < cutout_density_gridding[0]; site[0]++) {
    for (site[1] = 0; site[1] < cutout_density_gridding[1]; site[1]++) {
    for (site[2] = 0; site[2] < cutout_density_gridding[2]; site[2]++) {
      bool keep = false;
      for (cell[0] = -1; cell[0] < 2; cell[0]++) {
      for (cell[1] = -1; cell[1] < 2; cell[1]++) {
      for (cell[2] = -1; cell[2] < 2; cell[2]++) {
        //test the grid location and unit cell translations of the grid
        //this accounts for the case where the minext < 0 and maxext > 1
        //any cell translation of the site can be within the box for the
        // site to be kept
        ivect3 test(0,0,0);
        for (int i = 0; i < 3; i++)
          test[i] = site[i] + cell[i]*cutout_density_gridding[i];
        if (test[0] >= minext[0] and test[0] <= maxext[0] and
            test[1] >= minext[1] and test[1] <= maxext[1] and
            test[2] >= minext[2] and test[2] <= maxext[2])
         { keep = true; }
      }}}
      if (!keep)
      {
        nflatten++;
        real_map_padded(site[0],site[1],site[2]) = 0;
      }
    }}}
    bool signal(false);
    for (int i = 0; i < real_map_padded.size(); i++)
      if (real_map_padded[i] != 0)
        signal = true;
    phaser_assert(signal);

    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        real_map_padded.begin(),
        af::c_grid_padded<3>(real_map_padded.accessor()));

    // --- find symmetry equivalents
    RFFT.tags.build(SgInfo, RFFT.sym_flags);
    af::ref<long, af::c_grid<3> > tag_array_ref(
        RFFT.tags.tag_array().begin(),
        af::c_grid<3>(RFFT.tags.tag_array().accessor())
      );
    if (!treat_restricted)
      PHASER_ASSERT(RFFT.tags.verify(real_map_const_ref));
    //don't need to revert to real space, real_map_const_ref unchanged

    RFFT.rfft.forward(real_map_padded);

    af::const_ref<cmplex, af::c_grid_padded<3> > complex_map(
        reinterpret_cast<cmplex*>(&*real_map_padded.begin()),
        af::c_grid_padded<3>(RFFT.rfft.n_complex()));

    bool allow_miller_indices_outside_map = false;
    cctbx::maptbx::structure_factors::from_map<double> from_map(
        anomalous_flag,
        MILLER_RES_LIMITS.const_ref(),
        complex_map,
        conjugate_flag,
        allow_miller_indices_outside_map
      );
    FCUT = from_map.data().deep_copy();
    for (int i = 0; i < FCUT.size(); i++)
       FCUT[i] /= scalefac;

#ifdef PHASERTNG_DEBUG_CUTOUT_DENSITY
    output.push_back("");
    output.push_back("Space group: " + SG.CCP4);
    output.push_back("Unit cell: " + UC.str());
    output.push_back("extent fraction limits: " + dvtos(extent.first) + " " + dvtos(extent.second));
    output.push_back("inverted fraction limits: " + dvtos(inv_first) + " " + dvtos(inv_second));
    output.push_back("(inverted) grid limits: " + ivtos(minext) + " " + ivtos(maxext));
    output.push_back("Number of indices: " + itos(MILLER_RES_LIMITS.size()));
    output.push_back("Number of reflections: " + itos(FCUT.size()));
    output.push_back("Dump F: " + ivtos(MILLER_RES_LIMITS[0]) + " " + ctos(FMAP[0]) + "->" + ctos(FCUT[0]));
    output.push_back("Map cutout percent: " + dtos(100.*double(scalefac-nflatten)/scalefac));
    output.push_back("");
#endif
    return output;
  }

  void
  CutoutDensity::WriteMtz(FileSystem HKLOUT)
  {
    af_string history;
    MtzHandler mtzOut; mtzOut.open(HKLOUT,SG,UC,1.,MILLER_RES_LIMITS.size(),history);
    CMtz::MTZCOL* mtzF = mtzOut.addCol("FMAP",'F');
    CMtz::MTZCOL* mtzP = mtzOut.addCol("PHMAP",'P');
    phaser_assert(FCUT.size() == MILLER_RES_LIMITS.size());
    for (int r = 0; r < FCUT.size(); r++)
    {
      mtzOut.mtzH->ref[r] = MILLER_RES_LIMITS[r][0];
      mtzOut.mtzK->ref[r] = MILLER_RES_LIMITS[r][1];
      mtzOut.mtzL->ref[r] = MILLER_RES_LIMITS[r][2];
      std::pair<double,double> pmap = cmplex2polar_deg(FCUT[r]);
      mtzF->ref[r] = pmap.first;
      mtzP->ref[r] = pmap.second;
    }
    mtzOut.Put("Phasertng CutoutDensity");
  }

} //phasertng
