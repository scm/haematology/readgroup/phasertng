//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_out_stream_enum__
#define __phasertng_out_stream_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace out {
  MakeEnum ( stream,
             (UNDEFINED) //phasertng namespace, use unique name
             (SILENCE)
             (PROCESS)
             (CONCISE)
             (SUMMARY)
             (LOGFILE)
             (VERBOSE)
            // (FURTHER) //too many levels with this also
             (TESTING)
           );

}} //phasertng

#endif
