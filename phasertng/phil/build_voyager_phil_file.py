import types
from phasertng.phil.master_phil_file import *
from phasertng.autogen.keyword_subsets_by_program_name import keyword_subsets_by_program_name
from phasertng.voyager.voyager_generator import generic_documentation_name

def build_voyager_phil_file(program_name=None,keyword_list=None):
  #the keyword_subsets_by_program_name is set by autogen
  if program_name is not None and not program_name in keyword_subsets_by_program_name.keys():
    raise Exception('No dictionary entry for program name in build_voyager_phil_file')
  keyword_subset = []
  if program_name is None and keyword_list is not None:
    for keyword in keyword_list:
      if keyword not in implemented:
        raise Exception('keyword not recognised:  ' + keyword)
    keyword_subset = keyword_list
# construct list from all implemented
  elif program_name is None:
    for key in implemented:
      keyword_subset.append(key)
  elif program_name in [ generic_documentation_name, "InputCard"]:
    for key in implemented:
      keyword_subset.append(key)
  else:
    keyword_subset = keyword_subsets_by_program_name[program_name]['keywords']
  master_phil = phil_scope + " {\n"
  for keyword in sorted(keyword_subset):
    master_phil += implemented[keyword] + "\n"
  master_phil += "}\n"
  return master_phil

if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('-i',"--input",help="script for documentation",required=True)
  args = parser.parse_args()
  master_phil = build_voyager_phil_file(str(args.input))
  print(master_phil)
