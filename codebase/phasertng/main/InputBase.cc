#include <phasertng/main/InputBase.h>
#include <phasertng/main/hoist.h>

namespace phasertng {

  //{ //push_back brackets
  void InputBase::push_back(const std::string p,std::string item)
  { //all the types that can be initiated with std::string
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__string.count(p)) {
      array__string.at(p).push_back(type::string(p));
      array__string.at(p).back().set_value(item);
    }
    else if (array__path.count(p)) {
      array__path.at(p).push_back(type::path(p));
      array__path.at(p).back().set_value(item);
    }
    else if (array__atom_selection.count(p)) {
      array__atom_selection.at(p).push_back(type::atom_selection(p));
      array__atom_selection.at(p).back().set_value(item);
    }
    else if (array__spacegroup.count(p)) {
      array__spacegroup.at(p).push_back(type::spacegroup(p));
      array__spacegroup.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,type::choice item)
  { //choice initialization needs the possibilities as well as the selection, so can't be std::string
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__choice.count(p)) {
      array__choice.at(p).push_back(item);
    }
  }
  void InputBase::push_back(const std::string p,bool item)
  { //all the types that can be initiated with a number
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__boolean.count(p)) {
      array__boolean.at(p).push_back(type::boolean(p));
      array__boolean.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,double item)
  { //all the types that can be initiated with a number
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__percent.count(p)) {
      array__percent.at(p).push_back(type::percent(p,0));
      array__percent.at(p).back().set_value(item);
    }
    else if (array__flt_number.count(p)) {
      array__flt_number.at(p).push_back(type::flt_number(p,false,0,false,0));
      array__flt_number.at(p).back().set_value(item);
    }
    else if (value__flt_numbers.count(p)) {
      value__flt_numbers.at(p).push_back(item);
    }
  }
  void InputBase::push_back(const std::string p,int item)
  { //all the types that can be initiated with a number
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__int_number.count(p)) {
      array__int_number.at(p).push_back(type::int_number(p,false,0,false,0));
      array__int_number.at(p).back().set_value(item);
    }
    else if (value__int_numbers.count(p)) {
      value__int_numbers.at(p).push_back(item);
    }
    push_back(p,static_cast<double>(item)); //try the floats as well
  }
  void InputBase::push_back(const std::string p,uuid_t item)
  { //all the types that can be initiated with a number
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__uuid_number.count(p)) {
      array__uuid_number.at(p).push_back(type::uuid_number(p,true,0,false,0));
      array__uuid_number.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,std::pair<double,double> item)
  { //all the types that can be initiated with integer pair
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__flt_paired.count(p)) {
      array__flt_paired.at(p).push_back(type::flt_paired(p,false,0,false,0));
      array__flt_paired.at(p).back().set_value(item);
    }
    else if (array__int_paired.count(p)) {
      array__int_paired.at(p).push_back(type::int_paired(p,false,0,false,0));
      array__int_paired.at(p).back().set_value(
          { static_cast<int>(item.first),
            static_cast<int>(item.second) } );
    }
  }
  void InputBase::push_back(const std::string p,dvect3 item)
  { //all the types that can be initiated with vector double
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__flt_vector.count(p)) {
      array__flt_vector.at(p).push_back(type::flt_vector(p,false,0,false,0));
      array__flt_vector.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,ivect3 item)
  { //all the types that can be initiated with vector integer
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__int_vector.count(p)) {
      array__int_vector.at(p).push_back(type::int_vector(p,false,0,false,0));
      array__int_vector.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,af::double6 item)
  { //all the types that can be initiated with vector integer
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__flt_cell.count(p)) {
      array__flt_cell.at(p).push_back(type::flt_cell(p));
      array__flt_cell.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,dmat33 item)
  { //all the types that can be initiated with matrix
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__flt_matrix.count(p)) {
      array__flt_matrix.at(p).push_back(type::flt_matrix(p,false,0,false,0));
      array__flt_matrix.at(p).back().set_value(item);
    }
  }
  void InputBase::push_back(const std::string p,sv_double item)
  { //all the types that can be initiated with boolean
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__flt_numbers.count(p)) {
      array__flt_numbers.at(p).push_back(type::flt_numbers(p,false,0,false,0));
      array__flt_numbers.at(p).back().set_value(item);
    }
  }
  //this is the only way of loading an array of an array
  void InputBase::push_back(const std::string p,sv_int item)
  { //all the types that can be initiated with boolean
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
    if (array__int_numbers.count(p)) {
      array__int_numbers.at(p).push_back(type::int_numbers(p,false,0,false,0));
      array__int_numbers.at(p).back().set_value(item);
    }
  }
  //} //push_back brackets

  //{ //pop_back brackets
  void InputBase::pop_back(const std::string p)
  { //all the types that can be initiated with std::string
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
         if (false) { /*pass*/ }
    else if (value__flt_numbers.count(p)) { value__flt_numbers.at(p).pop_back(); }
    else if (value__int_numbers.count(p)) { value__int_numbers.at(p).pop_back(); }
    else if (array__boolean.count(p)) { array__boolean.at(p).pop_back(); }
    else if (array__choice.count(p)) { array__choice.at(p).pop_back(); }
    else if (array__percent.count(p)) { array__percent.at(p).pop_back(); }
    else if (array__int_number.count(p)) { array__int_number.at(p).pop_back(); }
    else if (array__int_paired.count(p)) { array__int_paired.at(p).pop_back(); }
    else if (array__int_vector.count(p)) { array__int_vector.at(p).pop_back(); }
    else if (array__int_numbers.count(p)) { array__int_numbers.at(p).pop_back(); }
    else if (array__uuid_number.count(p)) { array__uuid_number.at(p).pop_back(); }
    else if (array__uuid_numberS.count(p)) { array__uuid_numberS.at(p).pop_back(); }
    else if (array__flt_number.count(p)) { array__flt_number.at(p).pop_back(); }
    else if (array__flt_paired.count(p)) { array__flt_paired.at(p).pop_back(); }
    else if (array__flt_vector.count(p)) { array__flt_vector.at(p).pop_back(); }
    else if (array__flt_cell.count(p)) { array__flt_cell.at(p).pop_back(); }
    else if (array__flt_matrix.count(p)) { array__flt_matrix.at(p).pop_back(); }
    else if (array__flt_numbers.count(p)) { array__flt_numbers.at(p).pop_back(); }
    else if (array__string.count(p)) { array__string.at(p).pop_back(); }
    else if (array__path.count(p)) { array__path.at(p).pop_back(); }
    else if (array__atom_selection.count(p)) { array__atom_selection.at(p).pop_back(); }
    else if (array__spacegroup.count(p)) { array__spacegroup.at(p).pop_back(); }
  }
  //} //pop_back brackets

  //{ //clear brackets
  void InputBase::clear(const std::string p)
  { //all the types that can be initiated with std::string
    result_parameters.insert( std::pair<std::string,std::string>( running_mode,p ) );
         if (false) { /*pass*/ }
    else if (value__flt_numbers.count(p)) { value__flt_numbers.at(p).clear(); }
    else if (value__int_numbers.count(p)) { value__int_numbers.at(p).clear(); }
    else if (array__boolean.count(p)) { array__boolean.at(p).clear(); }
    else if (array__choice.count(p)) { array__choice.at(p).clear(); }
    else if (array__percent.count(p)) { array__percent.at(p).clear(); }
    else if (array__int_number.count(p)) { array__int_number.at(p).clear(); }
    else if (array__int_paired.count(p)) { array__int_paired.at(p).clear(); }
    else if (array__int_vector.count(p)) { array__int_vector.at(p).clear(); }
    else if (array__int_numbers.count(p)) { array__int_numbers.at(p).clear(); }
    else if (array__uuid_number.count(p)) { array__uuid_number.at(p).clear(); }
    else if (array__uuid_numberS.count(p)) { array__uuid_numberS.at(p).clear(); }
    else if (array__flt_number.count(p)) { array__flt_number.at(p).clear(); }
    else if (array__flt_paired.count(p)) { array__flt_paired.at(p).clear(); }
    else if (array__flt_vector.count(p)) { array__flt_vector.at(p).clear(); }
    else if (array__flt_cell.count(p)) { array__flt_cell.at(p).clear(); }
    else if (array__flt_matrix.count(p)) { array__flt_matrix.at(p).clear(); }
    else if (array__flt_numbers.count(p)) { array__flt_numbers.at(p).clear(); }
    else if (array__string.count(p)) { array__string.at(p).clear(); }
    else if (array__path.count(p)) { array__path.at(p).clear(); }
    else if (array__atom_selection.count(p)) { array__atom_selection.at(p).clear(); }
    else if (array__spacegroup.count(p)) { array__spacegroup.at(p).clear(); }
  }
  //} //clear brackets

  std::string InputBase::get_as_str(const std::string p)
  {
    try { return value__mtzcol.at(p).to_python(); } catch(...) {}
    try { return value__boolean.at(p).to_python(); } catch(...) {}
    try { return value__choice.at(p).to_python(); } catch(...) {}
    try { return value__percent.at(p).to_python(); } catch(...) {}
    try { return value__percent_paired.at(p).to_python(); } catch(...) {}
    try { return value__int_number.at(p).to_python(); } catch(...) {}
    try { return value__int_paired.at(p).to_python(); } catch(...) {}
    try { return value__int_vector.at(p).to_python(); } catch(...) {}
    try { return value__int_numbers.at(p).to_python(); } catch(...) {}
    try { return value__uuid_number.at(p).to_python(); } catch(...) {}
    try { return value__uuid_numberS.at(p).to_python(); } catch(...) {}
    try { return value__flt_number.at(p).to_python(); } catch(...) {}
    try { return value__flt_paired.at(p).to_python(); } catch(...) {}
    try { return value__flt_vector.at(p).to_python(); } catch(...) {}
    try { return value__flt_cell.at(p).to_python(); } catch(...) {}
    try { return value__flt_matrix.at(p).to_python(); } catch(...) {}
    try { return value__flt_numbers.at(p).to_python(); } catch(...) {}
    try { return value__string.at(p).to_python(); } catch(...) {}
    try { return value__strings.at(p).to_python(); } catch(...) {}
    try { return value__path.at(p).to_python(); } catch(...) {}
    try { return value__atom_selection.at(p).to_python(); } catch(...) {}
    try { return value__spacegroup.at(p).to_python(); } catch(...) {}
    throw Error(err::DEVELOPER,"Failed to send parameter to python " + p + " Name? Array?");
    return "";
  }

  std::string InputBase::get_as_str_i(const std::string p,const int index)
  {
    try { return array__boolean.at(p).at(index).to_python(); } catch(...) {}
    try { return array__choice.at(p).at(index).to_python(); } catch(...) {}
    try { return array__percent.at(p).at(index).to_python(); } catch(...) {}
    try { return array__int_number.at(p).at(index).to_python(); } catch(...) {}
    try { return array__int_paired.at(p).at(index).to_python(); } catch(...) {}
    try { return array__int_vector.at(p).at(index).to_python(); } catch(...) {}
    try { return array__int_numbers.at(p).at(index).to_python(); } catch(...) {}
    try { return array__uuid_number.at(p).at(index).to_python(); } catch(...) {}
    try { return array__uuid_numberS.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_number.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_paired.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_vector.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_cell.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_matrix.at(p).at(index).to_python(); } catch(...) {}
    try { return array__flt_numbers.at(p).at(index).to_python(); } catch(...) {}
    try { return array__string.at(p).at(index).to_python(); } catch(...) {}
    try { return array__strings.at(p).at(index).to_python(); } catch(...) {}
    try { return array__path.at(p).at(index).to_python(); } catch(...) {}
    try { return array__atom_selection.at(p).at(index).to_python(); } catch(...) {}
    try { return array__spacegroup.at(p).at(index).to_python(); } catch(...) {}
    throw Error(err::DEVELOPER,"Failed to send parameter to python " + p + " Name? Array?");
    return "";
  }

  void InputBase::set_as_str(const std::string p,std::string str)
  {
    try { value__mtzcol.at(p).from_python(str); return; } catch(...) {}
    try { value__boolean.at(p).from_python(str); return; } catch(...) {}
    try { value__choice.at(p).from_python(str); return; } catch(...) {}
    try { value__percent.at(p).from_python(str); return; } catch(...) {}
    try { value__percent_paired.at(p).from_python(str); return; } catch(...) {}
    try { value__int_number.at(p).from_python(str); return; } catch(...) {}
    try { value__int_paired.at(p).from_python(str); return; } catch(...) {}
    try { value__int_vector.at(p).from_python(str); return; } catch(...) {}
    try { value__int_numbers.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_number.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_paired.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_vector.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_cell.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_matrix.at(p).from_python(str); return; } catch(...) {}
    try { value__flt_numbers.at(p).from_python(str); return; } catch(...) {}
    try { value__string.at(p).from_python(str); return; } catch(...) {}
    try { value__strings.at(p).from_python(str); return; } catch(...) {}
    try { value__path.at(p).from_python(str); return; } catch(...) {}
    try { value__atom_selection.at(p).from_python(str); return; } catch(...) {}
    try { value__spacegroup.at(p).from_python(str); return; } catch(...) {}
    try { value__uuid_number.at(p).from_python(str); return; } catch(...) {}
    try { value__uuid_numberS.at(p).from_python(str); return; } catch(...) {}
    throw Error(err::DEVELOPER,"Failed to set parameter from python " + p);
  }

  void InputBase::set_as_str_i(const std::string p,std::string str, const int index)
  {
    try { array__boolean.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__choice.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__percent.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__int_number.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__int_paired.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__int_numbers.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__uuid_number.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__uuid_numberS.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_number.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_paired.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_vector.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_cell.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_matrix.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__flt_numbers.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__string.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__strings.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__path.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__atom_selection.at(p).at(index).from_python(str); return; } catch(...) {}
    try { array__spacegroup.at(p).at(index).from_python(str); return; } catch(...) {}
    throw Error(err::DEVELOPER,"Failed to set parameter from python " + p);
  }

  void InputBase::set_as_def(const std::string p)
  {
    try { value__mtzcol.at(p).set_default(); return; } catch(...) {}
    try { value__boolean.at(p).set_default(); return; } catch(...) {}
    try { value__choice.at(p).set_default(); return; } catch(...) {}
    try { value__percent.at(p).set_default(); return; } catch(...) {}
    try { value__percent_paired.at(p).set_default(); return; } catch(...) {}
    try { value__int_number.at(p).set_default(); return; } catch(...) {}
    try { value__int_paired.at(p).set_default(); return; } catch(...) {}
    try { value__int_vector.at(p).set_default(); return; } catch(...) {}
    try { value__int_numbers.at(p).set_default(); return; } catch(...) {}
    try { value__flt_number.at(p).set_default(); return; } catch(...) {}
    try { value__flt_paired.at(p).set_default(); return; } catch(...) {}
    try { value__flt_vector.at(p).set_default(); return; } catch(...) {}
    try { value__flt_cell.at(p).set_default(); return; } catch(...) {}
    try { value__flt_matrix.at(p).set_default(); return; } catch(...) {}
    try { value__flt_numbers.at(p).set_default(); return; } catch(...) {}
    try { value__string.at(p).set_default(); return; } catch(...) {}
    try { value__strings.at(p).set_default(); return; } catch(...) {}
    try { value__path.at(p).set_default(); return; } catch(...) {}
    try { value__atom_selection.at(p).set_default(); return; } catch(...) {}
    try { value__spacegroup.at(p).set_default(); return; } catch(...) {}
    try { value__uuid_number.at(p).set_default(); return; } catch(...) {}
    try { value__uuid_numberS.at(p).set_default(); return; } catch(...) {}
    throw Error(err::DEVELOPER,"Failed to set parameter from python " + p);
  }

  int InputBase::size(const std::string p) const
  {
         if (false) { /*pass*/ }
    else if (value__flt_numbers.count(p)) return value__flt_numbers.at(p).size();
    else if (value__int_numbers.count(p)) return value__int_numbers.at(p).size();
    else if (array__boolean.count(p)) return array__boolean.at(p).size();
    else if (array__choice.count(p)) return array__choice.at(p).size();
    else if (array__percent.count(p)) return array__percent.at(p).size();
    else if (array__int_number.count(p)) return array__int_number.at(p).size();
    else if (array__int_paired.count(p)) return array__int_paired.at(p).size();
    else if (array__int_vector.count(p)) return array__int_vector.at(p).size();
    else if (array__int_numbers.count(p)) return array__int_numbers.at(p).size();
    else if (array__uuid_number.count(p)) return array__uuid_number.at(p).size();
    else if (array__uuid_numberS.count(p)) return array__uuid_numberS.at(p).size();
    else if (array__flt_number.count(p)) return array__flt_number.at(p).size();
    else if (array__flt_paired.count(p)) return array__flt_paired.at(p).size();
    else if (array__flt_vector.count(p)) return array__flt_vector.at(p).size();
    else if (array__flt_cell.count(p)) return array__flt_cell.at(p).size();
    else if (array__flt_matrix.count(p)) return array__flt_matrix.at(p).size();
    else if (array__flt_numbers.count(p)) return array__flt_numbers.at(p).size();
    else if (array__string.count(p)) return array__string.at(p).size();
    else if (array__strings.count(p)) return array__strings.at(p).size();
    else if (array__path.count(p)) return array__path.at(p).size();
    else if (array__atom_selection.count(p)) return array__atom_selection.at(p).size();
    else if (array__spacegroup.count(p)) return array__spacegroup.at(p).size();
    throw Error(err::DEVELOPER,"size() not found for parameter: " + p);
    return 0;
  }

  void InputBase::resize(const std::string p,int n)
  {
         if (false) { /*pass*/ }
/*
    else if (value__flt_numbers.count(p))    value__flt_numbers.at(p).resize(n);
    else if (value__int_numbers.count(p))    value__int_numbers.at(p).resize(n);
    else if (array__boolean.count(p))        array__boolean.at(p).resize(n);
    else if (array__choice.count(p))         array__choice.at(p).resize(n);
    else if (array__percent.count(p))        array__percent.at(p).resize(n);
    else if (array__int_number.count(p))     array__int_number.at(p).resize(n);
    else if (array__int_paired.count(p))     array__int_paired.at(p).resize(n);
    else if (array__int_vector.count(p))     array__int_vector.at(p).resize(n);
    else if (array__int_numbers.count(p))    array__int_numbers.at(p).resize(n);
    else if (array__uuid_number.count(p))    array__uuid_number.at(p).resize(n);
    else if (array__uuid_numberS.count(p))   array__uuid_numberS.at(p).resize(n);
    else if (array__flt_number.count(p))     array__flt_number.at(p).resize(n);
    else if (array__flt_paired.count(p))     array__flt_paired.at(p).resize(n);
    else if (array__flt_vector.count(p))     array__flt_vector.at(p).resize(n);
    else if (array__flt_cell.count(p))       array__flt_cell.at(p).resize(n);
    else if (array__flt_matrix.count(p))     array__flt_matrix.at(p).resize(n);
    else if (array__flt_numbers.count(p))    array__flt_numbers.at(p).resize(n);
    else if (array__path.count(p))           array__path.at(p).resize(n);
    else if (array__atom_selection.count(p)) array__atom_selection.at(p).resize(n);
    else if (array__spacegroup.count(p))     array__spacegroup.at(p).resize(n);
*/
    else if (array__string.count(p))
    { for (int i = 0; i < n; i++) array__string[p].push_back(type::string(p)); }
    else if (array__strings.count(p))
    { for (int i = 0; i < n; i++) array__strings[p].push_back(type::strings(p)); }
    else if (array__path.count(p))
    { for (int i = 0; i < n; i++) array__path[p].push_back(type::path(p)); }
    else
      throw Error(err::DEVELOPER,"resize(n) not found for parameter: " + p);
  }

  std::string InputBase::unparse(show::type show_type,bool phil) const
  {
    std::string EQUALS = phil ? " = " : "  ";
    std::string BREAK = phil ? "\n" : " ";
    std::string text = "";
    size_t maxvec = 0;
    for (auto item : array__boolean) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__choice) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__percent) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__int_number) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__int_paired) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__int_vector) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__int_numbers) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__uuid_number) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__uuid_numberS) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_number) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_paired) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_vector) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_cell) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_matrix) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__flt_numbers) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__string) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__strings) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__boolean) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__path) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__atom_selection) maxvec = std::max(maxvec,item.second.size());
    for (auto item : array__spacegroup) maxvec = std::max(maxvec,item.second.size());
    //array init will be big enough
    std::string linked_text;
    std::map<std::string,bool> all_default;
    for (auto link : linkages)
    {
      sv_string concat_text(maxvec);
      size_t countvec(0);
      all_default[link.first] = true;
      for (auto k : link.second)
      {
        std::string lookup = link.first + k + std::string(SCOPE_SEPARATOR());
        for (auto item: array__boolean)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__choice)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__percent)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__int_number)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__int_paired)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__int_vector)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__int_numbers)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__uuid_number)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__uuid_numberS)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_number)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_paired)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_vector)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_cell)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_matrix)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__flt_numbers)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__string)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__strings)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__path)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__atom_selection)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
        for (auto item: array__spacegroup)
          if (item.first == lookup)
            for (int v = 0; v < item.second.size(); v++)
            {
              concat_text[v] += (k + EQUALS + item.second[v].unparse_parameter(phil) + BREAK );
              all_default[link.first] = all_default[link.first] && item.second[v].is_default();
              countvec = std::max(countvec,item.second.size());
            }
      }
      if (show_type != show::DEFAULT and !all_default[link.first])
      {
        if (phil && countvec)
        {
          std::string nosep = link.first;
          std::string sep(SCOPE_SEPARATOR());
          hoist::replace_first(nosep,std::string(SCOPE_SEPARATOR()),"");
          hoist::replace_last(nosep,std::string(SCOPE_SEPARATOR()),"");
          hoist::replace_all(nosep,std::string(SCOPE_SEPARATOR()),".");
          for (auto item : concat_text)
          if (item.size())
          {
            linked_text += nosep;
            linked_text += " {\n";
            linked_text += item;
            linked_text += "}\n";
          }
        }
        else if (countvec)
        {
          for (auto item : concat_text)
          if (item.size())
          {
            std::string nosep = link.first;
            hoist::replace_all(nosep,std::string(SCOPE_SEPARATOR())," ");
            hoist::algorithm::trim_left(nosep); //e.g. phasertng node composition element x number n
            linked_text += nosep + " " + item + "\n";
          }
        }
      }
    }

    for (auto item : value__mtzcol) text += item.second.unparse(show_type,phil);
    for (auto item : value__boolean) text += item.second.unparse(show_type,phil);
    for (auto item : value__choice) text += item.second.unparse(show_type,phil);
    for (auto item : value__percent) text += item.second.unparse(show_type,phil);
    for (auto item : value__percent_paired) text += item.second.unparse(show_type,phil);
    for (auto item : value__int_number) text += item.second.unparse(show_type,phil);
    for (auto item : value__int_paired) text += item.second.unparse(show_type,phil);
    for (auto item : value__int_vector) text += item.second.unparse(show_type,phil);
    for (auto item : value__int_numbers) text += item.second.unparse(show_type,phil);
    for (auto item : value__uuid_number) text += item.second.unparse(show_type,phil);
    for (auto item : value__uuid_numberS) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_number) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_paired) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_vector) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_cell) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_matrix) text += item.second.unparse(show_type,phil);
    for (auto item : value__flt_numbers) text += item.second.unparse(show_type,phil);
    for (auto item : value__string) text += item.second.unparse(show_type,phil);
    for (auto item : value__strings) text += item.second.unparse(show_type,phil);
    for (auto item : value__path) text += item.second.unparse(show_type,phil);
    for (auto item : value__atom_selection) text += item.second.unparse(show_type,phil);
    for (auto item : value__spacegroup) text += item.second.unparse(show_type,phil);

    for (auto offset : array__boolean) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__choice) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__percent) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__int_number) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__int_paired) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__int_vector) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__int_numbers) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__uuid_number) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__uuid_numberS) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_number) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_paired) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_vector) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_cell) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_matrix) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__flt_numbers) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__string) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__strings) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__path) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__atom_selection) for (auto item: offset.second) text += item.unparse(show_type,phil);
    for (auto offset : array__spacegroup) for (auto item: offset.second) text += item.unparse(show_type,phil);

    if (text.size())
    {
      sv_string lines;
      hoist::algorithm::split(lines,text, "\n");
      std::sort(lines.begin(),lines.end());
      text = "";
      for (auto line : lines)
      if (line.size())
      {
        hoist::algorithm::trim(line);
        bool found_in_linkage_list(false);
        for (auto link : linkages)
        {
          if (!line.find(link.first))
            found_in_linkage_list = true;
        }
        if (!found_in_linkage_list)
        {
          PHASER_ASSERT(line.find(" ") != std::string::npos);
          std::string kk1 = line.substr(0,line.find(" "));
          std::string kk2 = line.substr(line.find(" "),std::string::npos);
          if (phil)
          {
            hoist::replace_first(line,std::string(SCOPE_SEPARATOR()),"");
            hoist::replace_first(kk1,std::string(SCOPE_SEPARATOR()),"");
            hoist::replace_last(kk1,std::string(SCOPE_SEPARATOR()),"");
            hoist::algorithm::trim_left(kk1);
            text += kk1 + "  = " + kk2 + "\n";
          }
          else
          {
            hoist::replace_all(kk1,std::string(SCOPE_SEPARATOR())," ");
            hoist::algorithm::trim_left(kk1);
            text += kk1 + " " + kk2 + "\n";
          }
        }
      }
    }
    //add back in the linked ones
    text += linked_text;
    if (text.size()) text.pop_back();
    return text;
  }

  sv_string InputBase::scope_keywords(const std::string SCOPE) const
  {
    bool phil(false);
    std::string text = unparse(show::VALUEORDEFAULT,phil);
    sv_string selected;
    sv_string lines;
    hoist::algorithm::split(lines,text, "\n");
    for (auto item : lines)
    {
      hoist::algorithm::trim(item);
      if (!item.find(std::string(SCOPE))) //scope separators replaced with space
      {
        hoist::replace_all(item,std::string(SCOPE_SEPARATOR())," ");
        hoist::algorithm::trim(item);
        selected.push_back(item);
      }
    }
    return selected;
  }

  std::set<std::string> InputBase::input_keywords_for_mode(sv_string query) const
  {
    // This returns the keywords that the phil file says are relevant to this mode
    PHASER_ASSERT(query.size());
    //not all modes have input keywords, e.g. chk
    std::set<std::string> scopekeys;
    for (auto qmode : query)
    {
      if (!phil_input_parameters.count(qmode)) continue;
      for (auto item : phil_input_parameters.at(qmode))
      {
        std::string str(item);
        hoist::algorithm::trim(str);
        scopekeys.insert(str);
      }
    }
    return scopekeys;
  }

  std::set<std::string> InputBase::result_keywords_for_mode(sv_string query) const
  {
    // This returns the keywords that the phil file says are relevant to this mode
    PHASER_ASSERT(query.size());
    //not all modes have result keywords, e.g. chk
    std::set<std::string> scopekeys;
    for (auto& qmode : query)
    {
      if (!phil_result_parameters.count(qmode)) continue;
      for (auto item : phil_result_parameters.at(qmode))
      {
        std::string str(item);
        hoist::algorithm::trim(str);
        scopekeys.insert(str);
      }
    }
    return scopekeys;
  }

  //brutal post-edit of arrays
  //if a linked list is not all the same then delete all references to the array
  //this might be the case for a set with default and non-default values
  void InputBase::clear_incompatible_linkage_arrays()
  {
    //strange storage
    //link.first has separators e.g. = ".phasertng.matthews."
    //link.second does not e.g = "vm"
    for (auto link : linkages)
    {
      sv_double array_sizes;
      for (auto k : link.second)
      {
        std::string lookup = link.first + k + SCOPE_SEPARATOR(); //__matthews__vm__
        for (auto item: array__boolean)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__choice)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__percent)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__int_number)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__int_paired)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__int_vector)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__int_numbers)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__uuid_number)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__uuid_numberS)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_number)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_paired)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_vector)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_cell)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_matrix)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__flt_numbers)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__string)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__strings)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__path)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__atom_selection)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
        for (auto item: array__spacegroup)
          if (item.first == lookup)
            array_sizes.push_back(item.second.size());
      }
      if (!array_sizes.size()) continue; //parnoia
      bool all_the_same = true;
      for (int n = 1; n < array_sizes.size(); n++)
        all_the_same = all_the_same and (array_sizes[n] == array_sizes[0]);
      if (all_the_same) continue;
      for (auto k : link.second)
      {
        std::string lookup = link.first + k + SCOPE_SEPARATOR(); //__matthews__vm__
        for (auto& item: array__boolean) //by reference!!
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__choice) //by reference!!
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__percent) //by reference!!
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__int_number)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__int_paired)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__int_vector)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__int_numbers)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__uuid_number)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__uuid_numberS)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_number)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_paired)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_vector)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_cell)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_matrix)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__flt_numbers)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__string)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__strings)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__path)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__atom_selection)
          if (item.first == lookup)
            item.second.clear();
        for (auto& item: array__spacegroup)
          if (item.first == lookup)
            item.second.clear();
      }
    }
  }

  //this function is used to create the database from the dag nodes
  void InputBase::copy(const std::string p,int j,InputBase* o)
  {
    for (auto i:array__boolean) if (!i.first.find(p) and j < i.second.size()) o->array__boolean[i.first] = std::vector<type::boolean>(1,i.second[j]);
    for (auto i:array__choice) if (!i.first.find(p) and j < i.second.size()) o->array__choice[i.first] = std::vector<type::choice>(1,i.second[j]);
    for (auto i:array__percent) if (!i.first.find(p) and j < i.second.size()) o->array__percent[i.first]=std::vector<type::percent>(1,i.second[j]);
    for (auto i:array__int_number) if (!i.first.find(p) and j < i.second.size()) o->array__int_number[i.first]=std::vector<type::int_number>(1,i.second[j]);
    for (auto i:array__int_paired) if (!i.first.find(p) and j < i.second.size()) o->array__int_paired[i.first]=std::vector<type::int_paired>(1,i.second[j]);
    for (auto i:array__int_vector) if (!i.first.find(p) and j < i.second.size()) o->array__int_vector[i.first]=std::vector<type::int_vector>(1,i.second[j]);
    for (auto i:array__int_numbers) if (!i.first.find(p) and j < i.second.size()) o->array__int_numbers[i.first]=std::vector<type::int_numbers>(1,i.second[j]);
    for (auto i:array__uuid_number) if (!i.first.find(p) and j < i.second.size()) o->array__uuid_number[i.first]=std::vector<type::uuid_number>(1,i.second[j]);
    for (auto i:array__uuid_numberS) if (!i.first.find(p) and j < i.second.size()) o->array__uuid_numberS[i.first]=std::vector<type::uuid_numbers>(1,i.second[j]);
    for (auto i:array__flt_number) if (!i.first.find(p) and j < i.second.size()) o->array__flt_number[i.first]=std::vector<type::flt_number>(1,i.second[j]);
    for (auto i:array__flt_paired) if (!i.first.find(p) and j < i.second.size()) o->array__flt_paired[i.first]=std::vector<type::flt_paired>(1,i.second[j]);
    for (auto i:array__flt_vector) if (!i.first.find(p) and j < i.second.size()) o->array__flt_vector[i.first]=std::vector<type::flt_vector>(1,i.second[j]);
    for (auto i:array__flt_matrix) if (!i.first.find(p) and j < i.second.size()) o->array__flt_matrix[i.first]=std::vector<type::flt_matrix>(1,i.second[j]);
    for (auto i:array__flt_numbers) if (!i.first.find(p) and j < i.second.size()) o->array__flt_numbers[i.first]=std::vector<type::flt_numbers>(1,i.second[j]);
    for (auto i:array__string) if (!i.first.find(p) and j < i.second.size()) o->array__string[i.first]=std::vector<type::string>(1,i.second[j]);
    for (auto i:array__strings) if (!i.first.find(p) and j < i.second.size()) o->array__strings[i.first]=std::vector<type::strings>(1,i.second[j]);
    for (auto i:array__path) if (!i.first.find(p) and j < i.second.size()) o->array__path[i.first] = std::vector<type::path>(1,i.second[j]);
    for (auto i:array__atom_selection) if (!i.first.find(p) and j < i.second.size()) o->array__atom_selection[i.first]=std::vector<type::atom_selection>(1,i.second[j]);
    for (auto i:array__spacegroup) if (!i.first.find(p) and j < i.second.size()) o->array__spacegroup[i.first]=std::vector<type::spacegroup>(1,i.second[j]);
  }

  std::vector<type::mtzcol> InputBase::labin(bool is_default) const
  {
    //use map to sort in labin::col order, could use lambda but this is easier
    std::map<labin::col,type::mtzcol> info;
    for (auto item: value__mtzcol)
    {
      labin::col col = item.second.enumeration();
      //is_default=false, only return the ones that are set
      if (is_default or !item.second.is_not_set())
        info[col] = item.second;
    }
    std::vector<type::mtzcol> result;
    for (auto item: info)
    {
      result.push_back(item.second);
    }
    return result;
  }

  type::mtzcol InputBase::labin_mtzcol(labin::col col) const
  {
    for (auto item: value__mtzcol)
    {
      if (col == item.second.enumeration())
        return item.second;
    }
    return type::mtzcol();
  }

  void InputBase::check_unique(std::string keyword)
  {
    if (present_in_cards.count(keyword))
      storeWarning("Keyword repeated in input");
    present_in_cards.insert(keyword);
  }

  void InputBase::make_unique(std::string p)
  {
    //array__spacegroup is not used but at one point was dealt with in write_input.py
    if (array__spacegroup.count(p)) {
      auto& vec = array__spacegroup.at(p);
      auto cmp1 = [](const type::spacegroup& a, const type::spacegroup& b) { return a.value_or_default() < b.value_or_default(); };
      std::sort(vec.begin(), vec.end(), cmp1);
      auto plast = std::unique(vec.begin(), vec.end());
      vec.erase(plast, vec.end());
      auto cmp2 = [](const type::spacegroup& v) { return (v.value_or_default().size() == 0); };
      vec.erase(std::remove_if(vec.begin(), vec.end(), cmp2));
      size_t last = 0;
      for (size_t i = 0; i < vec.size(); i++)
      { if (vec[i].value_or_default().size() > 0) { vec[last++] = vec[i]; } }
      vec.erase(vec.begin()+last, vec.end());
    }
    if (array__flt_numbers.count(p)) {
      auto& vec = array__flt_numbers.at(p);
      auto cmp1 = [](const type::flt_numbers& a, const type::flt_numbers& b) { return a.value_or_default() < b.value_or_default(); };
      std::sort(vec.begin(), vec.end(), cmp1);
      auto plast = std::unique(vec.begin(), vec.end());
      vec.erase(plast, vec.end());
      size_t last = 0;
      for (size_t i = 0; i < vec.size(); i++)
      { if (vec[i].value_or_default().size() > 0) { vec[last++] = vec[i]; } }
      vec.erase(vec.begin()+last, vec.end());
    }
  }

  std::vector<std::string> InputBase::split_tok(std::string str,std::string delim) const
  {
    //split("abc","a") will return a vector or a single string, "bc", not a vector of elements ["", "bc"]. Using str.split() in Python returns an empty string in case delim was found either at the beginning or in the end
    std::vector<std::string> tokens;
    std::size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
          prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
  }

  void InputBase::process_warnings(std::string warnkey,af_string newwarn)
  {
    //replace spaces with underscores and make the unique set
    std::set<std::string> setwarn; //unique set
    auto oldwarn = array__string.at(warnkey);
    for (auto& w : oldwarn)
    {
      setwarn.insert(w.value_or_default());
    }
    for (auto& w : newwarn)
    {
      hoist::replace_all(w,std::string(" "),std::string("_"));
      setwarn.insert(w);
    }
    clear(warnkey); //easiest to clear and start again
    for (auto w : setwarn)
      push_back(warnkey,w);
  }

  std::string InputBase::card_str(bool without_mode) const
  {
    std::string ccp4_cards;
    bool phil_format(false);
    std::string text = unparse(show::VALUE,phil_format);
    sv_string lines;
    hoist::algorithm::split(lines, text, ("\n"));
    //mode line at the top
    for (auto& item : lines)
      hoist::algorithm::trim(item);
    if (!without_mode)
    {
      for (auto& item : lines)
      {
        bool is_mode = (item.find(PHIL_SCOPE() + " mode") == 0);
        if (is_mode)
          ccp4_cards += item + "\n";
       // ccp4_cards = PHIL_SCOPE() + " mode " + running_mode + "\n";
      }
    }
    for (auto& item : lines)
    {
      hoist::algorithm::trim(item);
      bool is_mode = (item.find(PHIL_SCOPE() + " mode") == 0);
      if (!is_mode)
        ccp4_cards += item + "\n";
    }
    return ccp4_cards;
  }

}
