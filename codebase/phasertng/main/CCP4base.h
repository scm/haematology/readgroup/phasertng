//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_CCP4base_class__
#define __phasertng_CCP4base_class__
#include <phasertng/main/includes.h>
#include <phasertng/enum/token_value.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/main/Error.h>
#include <unordered_set>
#include <map>

namespace phasertng {

class CCP4base
{
  public:
    Token::value  curr_tok;
    bool          error_ullint;
    bool          error_double;
    double        number_value;
    ullint        ullint_value;
    std::string   string_value;

    sv_string  variable; //for parse check
    std::map<std::string,sv_string >  linkages; //for unparse of vectors

    std::map<err::code,sv_string > ERRORS; //order is important
    sv_string WARNINGS; //order is important
    sv_string ADDITIONAL_TOKENS; //order is important

    int max_len_uint32 = (std::floor(std::log10(std::numeric_limits<uint32_t>::max())) + 1);

    bool has_errors() const;
    bool has_warnings() const;

    CCP4base() { reset_ccp4base(); }
    void reset_ccp4base()
    {
      error_ullint = false;
      error_double = false;
      string_value = "";
      number_value = 0;
      ullint_value = 0;
      curr_tok = Token::UNDEFINED;
      ERRORS.clear();
      WARNINGS.clear();
      ADDITIONAL_TOKENS.clear();
    }
    virtual ~CCP4base() { }

    void         storeError(err::code,std::string);
    void         storeWarning(std::string);
    std::string  Boolean(std::string);
//get functions
    Token::value get_token(std::istringstream&);
    std::string  skip_line(std::istringstream&);
    std::string  stoup(const std::string&) const;
//parsing functions
    bool         keyIs(const char*);
    bool         keyIsAdd(std::string,bool);
    bool         keyIsOptional(bool=true);
    bool         keyIsEnd();
    bool         keyIsEol();
    bool         tokenIsInList(std::istringstream&,std::vector<Token::value>);
    bool         tokenIs(std::istringstream&,Token::value);
    bool         compulsoryKey(std::istringstream&,std::unordered_set<const char*>);
    bool         compulsoryKeyStr(std::istringstream&,const std::string&);
    bool         compulsoryKeySet(std::istringstream&,const std::unordered_set<std::string>&);
    void         linkKey(std::string,std::string);
    bool         optionalKey(std::istringstream&,std::unordered_set<const char*>);
    bool         optionalKeySet(std::istringstream&,const std::unordered_set<std::string>&);
    bool         tokenIsAlreadyInList(std::vector<Token::value>);
    bool         tokenIsAlready(Token::value);
    std::string  getAssignString(std::istringstream&);
    std::string  getQuotedString(std::istringstream&);

    bool                     get_boolean(std::istringstream&);
    double                   get_percent(std::istringstream&);
    std::pair<double,double> get_percent_paired(std::istringstream&);
    int                      get_int_number(std::istringstream&,bool=false,int=0,bool=false,int=0);
    std::pair<int,int>       get_int_paired(std::istringstream&,bool=false,int=0,bool=false,int=0);
    scitbx::vec3<int>        get_int_vector(std::istringstream&,bool=false,int=0,bool=false,int=0);
    sv_int                   get_int_numbers(std::istringstream&,bool=false,int=0,bool=false,int=0);
    uuid_t                   get_uuid_number(std::istringstream&,bool=true,uuid_t=0,bool=false,uuid_t=0);
    sv_uuid                  get_uuid_numbers(std::istringstream&,bool=true,uuid_t=0,bool=false,uuid_t=0);
    double                   get_flt_number(std::istringstream&,bool=false,double=0,bool=false,double=0);
    std::pair<double,double> get_flt_paired(std::istringstream&,bool=false,double=0,bool=false,double=0);
    std::complex<double>     get_flt_complx(std::istringstream&,bool=false,double=0,bool=false,double=0);
    scitbx::vec3<double>     get_flt_vector(std::istringstream&,bool=false,double=0,bool=false,double=0);
    sv_dvect3                get_flt_vectors(std::istringstream&,bool=false,double=0,bool=false,double=0);
    scitbx::af::double6      get_flt_cell(std::istringstream&,bool=false,double=0,bool=false,double=0);
    scitbx::mat3<double>     get_flt_matrix(std::istringstream&,bool=false,double=0,bool=false,double=0);
    sv_double                get_flt_numbers(std::istringstream&,bool=false,double=0,bool=false,double=0);
    std::string              get_string(std::istringstream&);
    char                     get_character(std::istringstream&);
    std::string              get_mtzcol(std::istringstream&);
    sv_string                get_strings(std::istringstream&);
    std::string              get_spacegroup(std::istringstream&);
    std::string              get_atom_selection(std::istringstream&);
    std::string              get_path(std::istringstream&);

    std::string              error_message_ullint();
    std::string              error_message_double();

    std::pair<std::pair<uuid_t,std::string>,std::pair<uuid_t,std::string>> get_dogtag(std::istringstream&);
    std::pair<uuid_t,std::string> get_identifier(std::istringstream&);
};

}//phasertng

#endif
