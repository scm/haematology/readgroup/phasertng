
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/math/slope_and_intercept.h>
#include <phasertng/math/rotation/polar2cmplex_deg.h>

namespace phasertng {

typedef af::versa<double, af::c_grid<3> >   versa_grid_double;

  //Map correlation coefficient
  void Phasertng::runMCC()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }

    double io_cc = input.value__flt_number.at(".phasertng.map_correlation_coefficient.similarity.").value_or_default();
    if (DAGDB.NODES.size()== 1)
    {
      out::stream where = out::LOGFILE;
      logTableTop(where,"Map Correlation Matrix");
      logTab(where,snprintftos("* CC=100%c (identity)",'%')); //windows
      logTab(where,"[%]  -- 1 --");
      logTab(where,"-- 1 -- *");
      logTableEnd(where);
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    int io_nprint = input.value__int_number.at(".phasertng.map_correlation_coefficient.maximum_printed.").value_or_default();
    io_nprint = std::min(static_cast<int>(DAGDB.size()),io_nprint);

    int ncols = 5; //hkl
    std::vector<float> adata_(ncols);
    float* adata = &*adata_.begin();
    sv_int logmss_(ncols);
    int* logmss = &*logmss_.begin();
    std::vector<CMtz::MTZCOL*> lookup_(ncols);
    CMtz::MTZCOL** lookup = &*lookup_.begin();
    bool anomalous_flag(false);
    bool conjugate_flag(true);
    bool treat_restricted(true);
    //memory allocation
    //only store the ones in pairs that take up memory
    fft::real_to_complex_3d rffti(REFLECTIONS->SG.type(),REFLECTIONS->UC.cctbxUC);
    rffti.calculate_gridding(REFLECTIONS->data_hires(),{1,1},false);
    rffti.allocate();
    if (rffti.memory_allocation_error)
      throw Error(err::MEMORY,"FFT array allocation");
    af::c_grid_padded<3> map_grid(rffti.rfft.n_complex());

    std::pair<versa_grid_double,versa_grid_double> real_map_unpadded;

    //generic_flag is for unique
    for (int k = 0; k < DAGDB.NODES.size(); k++)
      DAGDB.NODES[k].generic_flag = true;

    math::Matrix<double> ccmatrix(DAGDB.NODES.size(),DAGDB.NODES.size());
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Correlation Coefficients");
    for (int k_ref = 0; k_ref < DAGDB.NODES.size(); k_ref++)
    {
      dag::Node& node_k_ref = DAGDB.NODES[k_ref];
      bool first_chk(true);
      for (int k_chk = k_ref+1; k_chk < DAGDB.NODES.size(); k_chk++)
      {
        dag::Node& node_k_chk = DAGDB.NODES[k_chk];
        if (!DAGDB.NODES[k_ref].is_all_full() or !DAGDB.NODES[k_chk].is_all_full())
        {
          ccmatrix(k_ref,k_chk) = -999;
          ccmatrix(k_chk,k_ref) = -999;
          continue;
        }
        //this one already same as higher in list
        if (!node_k_chk.generic_flag)
        {
          ccmatrix(k_ref,k_chk) = -998;
          ccmatrix(k_chk,k_ref) = -998;
          continue;
        }
        // skip comparison if different space groups
        bool different_space_group(node_k_chk.HALL != node_k_ref.HALL);
        if (different_space_group)
        {
          ccmatrix(k_ref,k_chk) = -997;
          ccmatrix(k_chk,k_ref) = -997;
          continue;
        }
        //if this is the first one then we need to calculate the reference also
        //put code here in lieu of making it a function and calling outside loop
        sv_bool reference = (first_chk) ? sv_bool({true,false}):sv_bool({false});
        for (auto isref : reference)
        {
          DogTag ID  = (isref) ?
              DAGDB.lookup_modlid(node_k_ref.FULL.IDENTIFIER): //optionally, calculate ref
              DAGDB.lookup_modlid(node_k_chk.FULL.IDENTIFIER); //always calculate chk
          FileSystem HKLIN(DataBase(),ID.FileEntry(entry::DENSITY_MTZ));
          bool refine_mtz(false);
          if (!HKLIN.file_exists())
          {
            HKLIN = FileSystem(DataBase(),ID.FileOther(other::REFINE_MTZ));
            refine_mtz = true;
            if (!HKLIN.file_exists())
            {
              ccmatrix(k_ref,k_chk) = -996;
              ccmatrix(k_chk,k_ref) = -996;
              continue;
            }
          }
          int read_reflections(0);
          MtzHandler mtzIn; mtzIn.read(HKLIN,read_reflections);
          lookup[0] = mtzIn.getCol("H");
          lookup[1] = mtzIn.getCol("K");
          lookup[2] = mtzIn.getCol("L");
          if (refine_mtz)
          {
            lookup[3] = mtzIn.getCol("2FOFCWT_no_fill");
            lookup[4] = mtzIn.getCol("PH2FOFCWT_no_fill");
          }
          else
          {
            lookup[3] = mtzIn.getCol("FC");
            lookup[4] = mtzIn.getCol("PHIC");
          }
          af_millnx miller;
          af_cmplex coeffs;
          miller.resize(mtzIn.NREFL);
          coeffs.resize(mtzIn.NREFL);
          for (int mtzr = 0; mtzr < mtzIn.NREFL ; mtzr++)
          {
            int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
            PHASER_ASSERT(!failure);
            miller[mtzr] = millnx(adata[0],adata[1],adata[2]);
            if (!mtz_isnan(adata[3]) and !mtz_isnan(adata[4]))
              coeffs[mtzr] = polar2cmplex_deg({adata[3],adata[4]});
            else
              coeffs[mtzr] = cmplex(0,0);
          }
          cctbx::maptbx::structure_factors::to_map<double> density(
              node_k_chk.SG->group(),
              anomalous_flag,
              miller.const_ref(),
              coeffs.const_ref(),
              rffti.rfft.n_real(),
              map_grid,
              conjugate_flag,
              treat_restricted
            );
          af::ref<cmplex, af::c_grid<3> > density_fft_ref(
              density.complex_map().begin(),
              af::c_grid<3>(rffti.rfft.n_complex())
           );
          rffti.rfft.backward(density_fft_ref);
          af::ref<double, af::c_grid_padded<3> > real_map_padded(
              reinterpret_cast<double*>( density.complex_map().begin()),
              af::c_grid_padded<3>( rffti.rfft.m_real(), rffti.rfft.n_real())
            );
          if (isref)
          {
            real_map_unpadded.second = versa_grid_double( af::c_grid<3>(rffti.rfft.n_real()));
            cctbx::maptbx::copy(real_map_padded, real_map_unpadded.second.ref());
          }
          else
          {
            real_map_unpadded.first = versa_grid_double( af::c_grid<3>(rffti.rfft.n_real()));
            cctbx::maptbx::copy(real_map_padded, real_map_unpadded.first.ref());
          }
        }
        double cc(-999);
        if (real_map_unpadded.first.size() and //empty if the density file does not exist
            real_map_unpadded.second.size() and
            real_map_unpadded.first.size() == real_map_unpadded.second.size())
        {
          cc = correlation_coefficient(real_map_unpadded.first,real_map_unpadded.second);
        }
        logTab(where,"CC " + itos(k_ref+1,6,true,false) + ":" + itos(k_chk+1,6,false,false) + " " + dtos(cc,10,6));
        if (k_ref < io_nprint and k_chk < io_nprint)
        {
          ccmatrix(k_ref,k_chk) =  ccmatrix(k_chk,k_ref) = cc; //symmetric
        }
        if (cc > io_cc)
        {
          node_k_chk.generic_flag = false;
          node_k_chk.generic_float = cc;
          node_k_chk.EQUIV = k_ref;
        }
        first_chk = false;
      }
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Duplicates");
    for (int k = 0; k < DAGDB.NODES.size(); k++)
      DAGDB.NODES[k].NUM = -999;
    int nunique(1);
    for (int k = 0; k < DAGDB.NODES.size(); k++)
      if (DAGDB.NODES[k].generic_flag) //unique
        DAGDB.NODES[k].NUM = nunique++; //output number equivalent
    for (int k = 0; k < DAGDB.NODES.size(); k++)
      if (!DAGDB.NODES[k].generic_flag) //if this is equivalent to something
      {
        int e = DAGDB.NODES[k].EQUIV;
        DAGDB.NODES[k].NUM = DAGDB.NODES[e].NUM;
      }
    for (int k = 0; k < DAGDB.NODES.size(); k++)
      PHASER_ASSERT(DAGDB.NODES[k].NUM != -999);

    int dupl(0);
    for (int k = 0; k < DAGDB.NODES.size(); k++)
    {
      if (!DAGDB.NODES[k].generic_flag) dupl++;
    }
    if (dupl == 1)
      logTab(where,"Duplicates: There is " + itos(dupl) + " out of " + itos(DAGDB.size()));
    else
      logTab(where,"Duplicates: There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
    }}

    {{
    out::stream where = out::LOGFILE;
    int w = std::ceil(std::log10(std::max(io_nprint,1))); //width of indices
    w = std::max(2,w);
    af_string output;
    logTableTop(where,"Map Correlation Matrix");
    logTab(where,snprintftos("* CC=100%c (identity)",'%')); //windows
    logTab(where,snprintftos("= CC>%s%c",dtos(io_cc*100,0).c_str(),'%'));
    logTab(where,snprintftos("^ matched to other (previous)")); //windows
    logTab(where,snprintftos("x different space group")); //windows
    logTab(where,snprintftos(". coordinates not available"));
    logTab(where,snprintftos("- density not available"));
    for (int p = 0; p < io_nprint; p++)
    {
      if (p == 0)
      {
        std::string line = "[%]" + std::string(w-1,' ') + "-- ";
        for (int q = 0; q < io_nprint; q++)
          line += "" + itos(q+1,w,true,false) + " ";
        line += "--";
        output.push_back(line);
      }
      std::string line = "--" + itos(p+1,w,true,false) + "-- ";
      for (int q = 0; q < io_nprint; q++)
      {
        if (p == q)
          line += std::string(w-1,' ') + "* ";
        else if (ccmatrix(p,q) == -999)
          line += std::string(w-1,' ') + ". "; //n/a as one not full
        else if (ccmatrix(p,q) == -998)
          line += std::string(w-1,' ') + "^ "; //matched to higher
        else if (ccmatrix(p,q) == -997)
          line += std::string(w-1,' ') + "x "; //different sg
        else if (ccmatrix(p,q) == -996)
          line += std::string(w-1,' ') + "- "; //no density file
        else if (ccmatrix(p,q)>io_cc)
          line += std::string(w-1,' ') + "= "; //match
        else
          line += dtos(100*ccmatrix(p,q),w,0) + " ";
      }
      output.push_back(line);
    }
    logTabArray(where,output);
    if (DAGDB.size() > io_nprint)
       logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
    logTableEnd(where);
    }}

    {{
    int w = itow(io_nprint);
    int ww = itow(DAGDB.NODES.size());
    out::stream where = out::SUMMARY;
    logTableTop(where,"Map Correlation");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    if (DAGDB.NODES.size())
    {
      logTab(where,"*/&     Unique/Duplicate");
      logTab(where,"#=      Equivalent to peak");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s%c  %*s  %10s %10s %10s",
           w,"#",' ',
           ww,"#=",
           "CC%","LLG","TFZ"));
      logTab(where,"--------");
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      logTab(where,snprintftos(
          "%*d%c  %*s  %10s %-10s %-10s",
          w,i,node->generic_flag ? '*':'&',
          ww,node->generic_flag ? "--" : itos(node->EQUIV+1).c_str(),
          node->generic_flag ? "--" : dtos(100*node->generic_float,3).c_str(),
          dtos(node->LLG,10,1).c_str(),
          dtos(node->ZSCORE,10,2).c_str()
          ));
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge duplicates");
    int before = DAGDB.size();
    DAGDB.NODES.erase_invalid();
    logEllipsisShut(where);
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(DAGDB.size()));
    }}

    int io_maxstored = input.value__int_number.at(".phasertng.map_correlation_coefficient.maximum_stored.").value_or_default();

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge by maximum stored");
      DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored:      " + itos(io_maxstored));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
    }
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
