//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/io/entry/Atoms.h>
#include <phasertng/pod/scatterer.h>
#include <phasertng/minimizer/RefineSSR.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/dag/Node.h>
#include <phasertng/main/jiffy.h>
#include <future>

namespace phasertng {

  RefineSSR::RefineSSR(
      const_ReflectionsPtr  REFLECTIONS_,
     // SelectedPtr SELECTED_,
     // EpsilonPtr EPSILON_,
      ScatterersPtr SCATTERERS_)
    : dtmin::RefineBase(),
      REFLECTIONS(std::move(REFLECTIONS_)),
     // SELECTED(std::move(SELECTED_)),
     // EPSILON(std::move(EPSILON_)),
      SCATTERERS(std::move(SCATTERERS_))
  {
  }

  void
  RefineSSR::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  std::pair<af_string,std::string>
  RefineSSR::init_node(dag::Node* work,sv_double wilson_llg,std::pair<int,int> use_fft,bool verbose)
  {
    af_string output;
    NODE = work;
    phaser_assert(REFLECTIONS->NREFL);
    phaser::af_atom sadset;
    if (NODE->PART.PRESENT)
    {
      auto* substructure = &NODE->PART.ENTRY->SUBSTRUCTURE;
      auto& xray = substructure->XRAY;
      auto& xtra = substructure->XTRA;
      for (unsigned a = 0; a < xray.size(); a++)
      {
        input_atoms = true;
        pod::xray::scatterer& A = xray[a];
        pod::xtra::scatterer& X = xtra[a];
        phaser::data_atom atom;
        atom.SCAT = A; //site in fractional
        atom.XTRA.bswap = false;
        atom.ORIG = true; //always start as frac
        sadset.SET.push_back(atom);
      } //end  loop over atoms
    }
    phaser::data_part partial;
    auto& entry = NODE->FULL.ENTRY;
    if (NODE->FULL.PRESENT and !entry->MAP)
    {
      COORDINATES = &entry->COORDINATES;
      input_partial = true;
      partial.ANOMALOUS = ANOMALOUS;
      partial.RMSID = 1.0;
      partial.map_format = false;
      output.push_back("Partial Model: " + COORDINATES->stem());
    }
    else if (NODE->FULL.PRESENT and entry->MAP)
    {
      FCALCS = &entry->FCALCS;
      input_partial = true;
      partial.ANOMALOUS = false;
      partial.RMSID = 1.0;
      partial.map_format = true;
      output.push_back("Partial Density: " + FCALCS->stem());
    }
    std::string hall = REFLECTIONS->SG.type().hall_symbol(); //without Hall: on the front
    af::double6 unit_cell = REFLECTIONS->UC.cctbxUC.parameters();
    af::shared<miller::index<int> > miller;
    phaser::data_spots pos;
    phaser::data_spots neg;
    pos.INPUT_INTENSITIES = bool(REFLECTIONS->INTENSITIES);
    neg.INPUT_INTENSITIES = bool(REFLECTIONS->INTENSITIES);
    pos.F_ID = "F(+)"; pos.SIGF_ID = "SIGF(+)";
    neg.F_ID = "F(-)"; neg.SIGF_ID = "SIGF(-)";
    phaser::data_tncs ptncs; //default, ptncs is not used
    PHASER_ASSERT(REFLECTIONS->has_col(labin::FPOS));
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
      double aniso = 1.0/std::sqrt(REFLECTIONS->get_flt(labin::ANISOBETA,r));
          // aniso *= std::exp(-REFLECTIONS->SHARPENING*ssqr/4.); //no sharpening
      miller.push_back(REFLECTIONS->get_miller(r));
      pos.F.push_back(REFLECTIONS->get_flt(labin::FPOS,r)*aniso);
      pos.SIGF.push_back(REFLECTIONS->get_flt(labin::SIGFPOS,r)*aniso);
      pos.PRESENT.push_back(REFLECTIONS->get_present(friedel::POS,r));
      neg.F.push_back(REFLECTIONS->get_flt(labin::FNEG,r)*aniso);
      neg.SIGF.push_back(REFLECTIONS->get_flt(labin::SIGFNEG,r)*aniso);
      neg.PRESENT.push_back(REFLECTIONS->get_present(friedel::NEG,r));
      ptncs.EPSFAC.push_back(REFLECTIONS->get_flt(labin::TEPS,r));
    }
    std::string cluster_pdb = "filename";
    phaser::data_bins data_bins; //defaults
    phaser::data_ffts fftVdirect; //defaults
    fftVdirect.MIN = use_fft.first;
    fftVdirect.MAX = use_fft.second;
    std::map<std::string,cctbx::eltbx::fp_fdp> atomtypes = SCATTERERS->FP_FDP;
    phaser::data_outl outlier;
    double wilson_b = REFLECTIONS->WILSON_B_F;
    std::string advisory;
    std::tie(output,advisory) = setRefineSAD(
        hall,
        unit_cell,
        miller,
        pos,neg,
        ptncs,
        sadset,
        cluster_pdb,
        data_bins,
        atomtypes,
        outlier,
        WILSON,wilson_b,
        SPHERICITY,
        FDP,
        partial,
        fftVdirect,
        wilson_llg,
        verbose);
    calcSigmaaData();
    if (false)
    {
      auto txt = logDataStats(20);
      output.insert(output.end(),txt.begin(),txt.end());
    }
    auto txt = rejectOutliers(verbose);
    output.push_back("");
    output.insert(output.end(),txt.begin(),txt.end());
    //calcBinStats();
    //calcPhsStats();
    return {output,advisory};
  }

  void
  RefineSSR::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> set_protocol;
    for (auto& item : const_protocol)
      set_protocol.insert(stolower(item));
    for (auto item : set_protocol)
      if (item != "" and
          item != "siga" and
          item != "sigb" and
          item != "sigp" and
          item != "sigd" and
          item != "site" and
          item != "bfac" and
          item != "occ" and
          item != "fdp" and
          item != "part" and
          item != "data" and
          item != "off"
        )
        throw Error(err::FATAL,"Protocol flag not recognised: " + item);
    if (set_protocol.find("off") != set_protocol.end())
    {
      protocol.off();
    }
    else
    {
      protocol.FIX_K = (set_protocol.find("data") == set_protocol.end());
      protocol.FIX_B = (set_protocol.find("data") == set_protocol.end());
      protocol.FIX_PARTK = (set_protocol.find("part") == set_protocol.end());
      protocol.FIX_PARTU = (set_protocol.find("part") == set_protocol.end());
      protocol.FIX_XYZ = (set_protocol.find("site") == set_protocol.end());
      protocol.FIX_OCC = (set_protocol.find("occ") == set_protocol.end());
      protocol.FIX_BFAC = (set_protocol.find("bfac") == set_protocol.end());
      protocol.FIX_FDP = (set_protocol.find("fdp") == set_protocol.end());
      protocol.FIX_SA = (set_protocol.find("siga") == set_protocol.end());
      protocol.FIX_SB = (set_protocol.find("sigb") == set_protocol.end());
      protocol.FIX_SP = (set_protocol.find("sigp") == set_protocol.end());
      protocol.FIX_SD = (set_protocol.find("sigd") == set_protocol.end());
    }
    setProtocol(&protocol);

    //there isn't a 1-2-1 mapping of refinePar to parameters
    //refinePar has one parameter for n_xyz and n_nadp/1 (u_iso)
    //this is where total_number_of_parameters_ and nmp are DEFINED
    //we do not set
    nmp = npars_ref;
    total_number_of_parameters_ = npars_all;
    //we do not set refine_parameters_

    //make sure parameters are within bounds
    auto x = get_macrocycle_parameters();
    auto bound = bounds();
    if (x.size() == bound.size())
    {
      for (int i = 0; i < x.size(); i++)
      {
        if (bound[i].upper_bounded() and (x[i] > bound[i].upper_limit() ))
        {
          x[i] = bound[i].upper_limit();
        }
        if (bound[i].lower_bounded() and (x[i] < bound[i].lower_limit() ))
        {
          x[i] = bound[i].lower_limit();
        }
      }
    }
    set_macrocycle_parameters(x);
  }

  sv_string
  RefineSSR::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    for (int i = 0; i < nmp; i++)
      names[i] = whatAmI(i);
    return names;
  }

  sv_double
  RefineSSR::get_macrocycle_parameters()
  {
    return getRefinePars();
  }

  void
  RefineSSR::set_macrocycle_parameters(sv_double newx)
  {
    applyShift(newx);
  }

  std::vector<dtmin::Bounds>
  RefineSSR::bounds()
  {
    std::vector<dtmin::Bounds> tmp(nmp);
    auto lower = getLowerBounds();
    auto upper = getUpperBounds();
    for (int i = 0; i < nmp; i++)
    {
      tmp[i].lower_ = lower[i];
      tmp[i].upper_ = upper[i];
    }
    return tmp;
  }

  std::vector<dtmin::Reparams>
  RefineSSR::reparameterize()
  {
    return getRepar();
  }

  sv_double
  RefineSSR::macrocycle_large_shifts()
  {
    return getLargeShifts();
  }

  TargetType
  RefineSSR::likelihood()
  {
    //store the llg without restraints
    TargetType Target;
    Target = getHandEP().getLogLikelihood();
    NODE->LLG = -Target; //-llg for minimization
    return -Target;
  }

  TargetType
  RefineSSR::target()
  {
   // TargetType Target = targetFn(0,NREFL);
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    Tuple.Target -= RefineSAD::wilson_llg;
    atoms.LLG = Tuple.Target; //store value without restraints
    add_restraint(Tuple,false,false);
    return Tuple.Target;
  }

  TargetGradientType
  RefineSSR::target_gradient()
  {
    TargetGradientType Tuple;
    Tuple.Target = gradientFn(Tuple.Gradient);
    return Tuple;
  }

  TargetGradientHessianType
  RefineSSR::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple;
    Tuple.Target = gradientFn(Tuple.Gradient);
    Tuple.Target = hessianFn(Tuple.Hessian,Tuple.Diagonal);
    return Tuple;
  }

  af_string
  RefineSSR::initial_statistics()
  {
    return logInitial();
  }

  af_string
  RefineSSR::current_statistics()
  {
    return logCurrent();
  }

  af_string
  RefineSSR::final_statistics()
  {
    return logFinal();
  }

  af_string
  RefineSSR::cleanup_parameters()
  {
    return cleanUp();
  }

  af_string
  RefineSSR::reject_outliers()
  {
    bool verbose(false);
    return rejectOutliers(verbose);
  }

  af_string
  RefineSSR::setup_parameters()
  {
    bool INTE_FIXED(false); //for debugging
    int  INTE_STEPS(1); //for debugging
    if (INTE_FIXED) setupIntegrationPoints(INTE_STEPS);
    return af_string();
  }

  Loggraph RefineSSR::fom_loggraph()
  {
    Loggraph loggraph;
    loggraph.title = "Figures of Merit";
    loggraph.scatter = false;
    loggraph.data_labels = "1/d^2 Acentrics Centrics Singletons All";
    calcBinStats();
    for (unsigned s = 0; s < bin.numbins(); s++)
    {
      loggraph.data_text += dtos(1.0/fn::pow2(bin.MidRes(s)),5,4) + " " +
          dtos(acentStats.FOM(s),5,3) + " " +
          dtos(centStats.FOM(s),5,3) + " "  +
          dtos(singleStats.FOM(s),5,3) + " "  +
          dtos(allStats.FOM(s)) + "\n";
    }
    loggraph.graph.resize(1);
    loggraph.graph[0] = "FOM vs Resolution:AUTO:1,2,3,4,5";
    return loggraph;
  }

  double
  RefineSSR::maximum_distance_special(
      sv_double& x,
      sv_double& p,
      sv_bool& bounded,
      sv_double& dist,
      double& start_distance)
  {
    return  RefineSAD::getMaxDistSpecial(x,p,bounded,dist,start_distance);
  }

  TargetGradientHessianType
  RefineSSR::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    TargetGradientHessianType tgh(nmp,true);
    //tgh.Target is the target without the -wilson_llg and the restraint terms added
    tgh.Target = RefineSAD::targetFn(beg,end); //always parallel
    return tgh;
  }

  TargetGradientHessianType
  RefineSSR::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    phaser_assert(!do_gradient and !do_hessian); //doesn't work
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,true);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use fewer threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< TargetGradientHessianType > > results;
      std::vector< std::packaged_task< TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineSSR::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineSSR::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
      }
    }
    return tgh;
  }

  void
  RefineSSR::add_restraint(
      TargetGradientHessianType& tgh,
      bool do_gradient,
      bool do_hessian
    )
  {
    if (SPHERICITY.RESTRAINT)
      tgh.Target += RefineSAD::sphericity_restraint_likelihood();
    if (WILSON.RESTRAINT)
      tgh.Target += RefineSAD::wilson_restraint_likelihood();
    if (FDP.RESTRAINT)
      tgh.Target += RefineSAD::fdp_restraint_likelihood();
  }

} //phasertng
