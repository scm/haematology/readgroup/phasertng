//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_spacegroup_class__
#define __phasertng_type_spacegroup_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class spacegroup : public optional
{
  protected:
  std::string dephalt = "";
  std::string uvalue = "";

  public:
  spacegroup() : optional() {}
  spacegroup(const std::string parameter,
             const std::string dephalt_ = ""
        );

  void set_value(std::string other);
  void set_value_string(std::string other);
  void set_value(const sv_string& vcards,const std::string& other);
  bool is_default() const;
  void set_default();
  std::string value_or_default() const;
  std::string value_or_default_upper() const;
  std::string value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::spacegroup& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
