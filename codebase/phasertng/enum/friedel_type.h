#ifndef __phasertng_friedel_type_enum__
#define __phasertng_friedel_type_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace friedel {

MakeEnum ( type,
            (UNDEFINED)
            (NAT)
            (POS)
            (NEG)
         );
//POS and NEG are used as lookups for IPOS and INEG etc
//NAT is used as a blank string under same lookup system
struct EnumHash
{
  template <typename T>
  std::size_t operator()(T t) const
  {
    return static_cast<std::size_t>(t);
  }
};
}}
#endif
