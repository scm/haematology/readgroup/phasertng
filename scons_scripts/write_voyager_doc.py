from __future__ import print_function
from __future__ import division
#file write the voyager doc landing page for the gui using stuff in documentation.py

import sys, os, os.path
import libtbx.load_env
import iotbx.phil
import sys, os, os.path, shutil
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from libtbx.phil import is_plain_none
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'voyager'))
from master_auto_file import *
from converter_registry import *
from OrderedSet import *
from mode_generator import *
from write_input import flat_stanley
from phasertng.voyager.voyager_generator import *
#from voyager_generator import *


splash_header = [
"                __",
" _      _____  / /________  ____ ___  ___",
"| | /| / / _ \\/ / ___/ __ \\/ __ `__ \\/ _ \\",
"| |/ |/ /  __/ / /__/ /_/ / / / / / /  __/",
"|__/|__/\\___/_/\\___/\\____/_/ /_/ /_/\\___/",
"  / /_____",
" / __/ __ \\",
"/ /_/ /_/ /",
"\\__/\\____/_  __  ______ _____ ____  _____",
"| | / / __ \\/ / / / __ `/ __ `/ _ \\/ ___/",
"| |/ / /_/ / /_/ / /_/ / /_/ /  __/ /",
"|___/\\____/\\__, /\\__,_/\\__, /\\___/_/",
"          /____/      /____/",
]

class colorkey():
  def __init__(self):
    self.lookup = [
 #index of array is the expert level
 #expert index is NOT USED
  { "expert" : 0 , "col" : "DarkBlue", "key" : "Input Keywords:", "txt" : "You need to enter some of these, with which ones dependent on the mode or voyager script and how your data are prepared" , "img" : "User1.gif"},
  { "expert" : 1 , "col" : "DarkOrange", "key" : "Basic Keywords:", "txt" : "You can safely change these to modify the results" , "img" : "Output.png"},
  { "expert" : 2 , "col" : "Maroon", "key" : "Expert Keywords:", "txt" : "You should not change these unless you know what you are doing" , "img" : "User2.gif"},
  { "expert" : 3 , "col" : "DarkGreen", "key" : "Logging Keywords:", "txt" : "No change to result of a given node, only to logging and what is passed between nodes" , "img" : "Output.png"},
  { "expert" : 4 , "col" : "Grey", "key" : "Developer Keywords:", "txt" : "You can't change these, they are used for control in voyager scripting" , "img" : "Expert.gif"},
  { "expert" : 5 , "col" : "Silver", "key" : "Debugging Keywords:", "txt" : "Change these to break the software" , "img" : "Developer.gif"},
  ]
#   "<span style=\"color:DarkGreen\"><b>Required Keywords:</b> You need to enter these</span><br>\n"
  def levels(self):
    return len(self.lookup)
  def doc_levels(self):
    return [0,1,2,3] #change the doc levels here
  def color(self,i,kline):
    i = int(i)
    if i in range(int(self.levels())):
      return "<span style=\"color:" + self.lookup[i]["col"] + "\"><b>" + kline + "</b></span><br>"
    return ""
  def htmlkeyheader(self):
    html = "<a id=\"reference\"><h2>Keyword Colour Reference</h2></a>\n"
    for i in range(int(self.levels())):
        line = "<span style=\"color:" + self.lookup[i]["col"] + "\"><b>" + self.lookup[i]["key"] + "</b> " + self.lookup[i]["txt"] + "</span><br>\n"
        html = html + line
    return html + "<br>\n"
  def link(self):
    return ("<a href=\"#reference\">colour key</a><br>")

def copy_files(source,target,env,filelist):
  verbose = False
  rmfiles = False
  docdir = os.path.join(env.dist_path,"doc")
  print("phasertng: Copying voyager documentation files to distribution...")
  if rmfiles:
    for (dirpath, dirnames, filenames) in os.walk(docdir):
      for f in filenames:
        os.remove(f)
    for (dirpath, dirnames, filenames) in os.walk(docdir):
      assert(len(filenames) == 0)
  for f in filelist:
    if (verbose):
      print("phasertng: " + os.path.join(env.autogen,f) + " --> " + os.path.join(docdir,f))
    shutil.copy(os.path.join(env.autogen,f), os.path.join(docdir,f))

def write_wiki_file(class_path,voyager,flat):
    wikifile = voyager+".wiki.txt"
    wiki_file = os.path.join(class_path,wikifile)
 #  sys.stdout.write("Writing wiki file " + wiki_file + "\n")
    wiki = open(wiki_file, "w")
    for expert in colorkey().doc_levels():
      headertxt = ""
      wiki.write("==" + colorkey().lookup[expert]["key"].title() +"==\n")
      headertxt = "===[[Image:" + colorkey().lookup[expert]["img"] + "|link=]]"
      for keyword in sorted(flat.flat_wiki.keys()):
        first = True
        for subkey in sorted(flat.flat_wiki[keyword]):
          if (int(subkey.expert_level) == expert):
            if first:
              wiki.write(headertxt+keyword+"===\n")
              first = False
          # wiki.write(";" + subkey.keywords() + "\n")
            wiki.write(";" + subkey.keywords())
            text = subkey.text()
            splittext = text.split("\n")
            for line in splittext:
              if len(line):
                wiki.write(":"+line + "\n")
            wiki.write("\n")
    wiki.close()
    return wikifile

def write_text_file(class_path,voyager,flat):
    textfile = voyager+".text"
    text_file = os.path.join(class_path,textfile)
  # sys.stdout.write("Writing text file " + text_file + "\n")
    text = open(text_file, "w")
    text.write("<text>\n")
    text.write("<h2>" + voyager.title() + " Documentation</h2>\n")
    text.write(voyager_generator().text(voyager))
    max_expert_level = colorkey().levels()
    if len(flat.modes_keywords[voyager]['modes']):
      text.write("\n\n==Modes==\n")
      text.write("\n")
    descriptors = mode_generator().mode_list
    for key,val,txt,cpp in descriptors:
      if key in flat.modes_keywords[voyager]['modes']:
        text.write(str(key))
        text.write("--" + str(val).replace("_"," ").title()+"\n")
        text.write("" + txt + "\n")
        text.write("Input Keywords:   ")
        for k in sorted(flat.phil_input_keywords[key]):
          text.write(k + " ")
        text.write("\n")
        text.write("Result Keywords:  ")
        for k in sorted(flat.phil_result_keywords[key]):
          text.write(k + " ")
        text.write("\n")
        text.write("\n")
    if len(flat.modes_keywords[voyager]['keywords']):
      text.write("\n")
      text.write("\n\n==Keywords for Modes==\n")
   #for keyword in sorted(flat_wiki.keys()):
    sp=""
    for i in range(4):
      sp = sp + "&nbsp"
    for keyword in sorted(flat.modes_keywords[voyager]['keywords']):
        first = True
        from functools import cmp_to_key
        for subkey in sorted(flat.flat_wiki[keyword],key=cmp_to_key(lambda item1, item2: item1.expert_level < item2.expert_level)):
          if (int(subkey.expert_level) <= max_expert_level):
            if first:
              text.write("\n==="+keyword+"===\n")
              first = False
            kline = subkey.keywords().replace("\"","") # remove quotes from "<string>"
            #put the dots in for phil keywords
            kline = kline.split("<")[0].replace(" ",".")[:-1] + " <" + kline.split("<")[1]
            #convert to text
            kline = kline.replace("\n","")
            text.write("--" + kline + "\n")
            subk = subkey.text()
            splittext = subk.split("\n")
            for i,line in enumerate(splittext):
              if len(line):
                kline = line.replace("\n","")
                text.write(kline + " ")
                if (i == 0): text.write("\n") #end of the help text, rest is defaults etc
            text.write("\n")
            text.write("\n")
    text.write("\n")
    text.write("</text>\n")
    text.close()
    return textfile

def write_html_file(class_path,voyager,flat):
    htmlfile = voyager + ".html"
    html_file = os.path.join(class_path,htmlfile)
 #  sys.stdout.write("Writing html file " + html_file + "\n")
    html = open(html_file, "w")
    html.write("<html>\n")
    html.write("<h2>" + voyager.title() + " Documentation</h2>\n")
    html.write(voyager_generator().html(voyager))
    max_expert_level = colorkey().levels()
    if len(flat.modes_keywords[voyager]['modes']):
      html.write("<h2> <a id=\"modes\">Modes</a></h2>\n")
      html.write("<table>\n")
      descriptors = mode_generator().mode_list
      for key,val,txt,cpp in descriptors:
        if key in flat.modes_keywords[voyager]['modes']:
          html.write("<tr>")
          html.write("<td>")
          html.write("<a href=\"#mode_"+str(key)+"\">"+str(key)+"</a>"+' '.ljust(7-len(key)))
          html.write("</td>\n")
          html.write("<td>")
          html.write("<b>"+str(val).replace("_"," ") +"</b>\n")
          html.write("<br>" + txt + "<br>\n")
          html.write("Input Keywords:   ")
          for k in sorted(flat.phil_input_keywords[key]):
            html.write("<a href=\"#%s\">%-s</a> "%(k,k))
          html.write("<br>\n")
          html.write("Result Keywords:  ")
          for k in sorted(flat.phil_result_keywords[key]):
            html.write("<a href=\"#%s\">%-s</a> "%(k,k))
          html.write("<br>\n")
          html.write("</td>\n")
          html.write("<tr>")
      html.write("</table>")
      html.write("<br>\n")
    if True:
      html.write(colorkey().htmlkeyheader())
    def write_kline(first,html,subkey):
              sp=""
              for i in range(4):
                sp = sp + "&nbsp"
              if first:
                html.write("<h3><a id=\"" + keyword +"\">"+keyword+"</a> </h3>")
                first = False
              kline = subkey.keywords().replace("\"","") # remove quotes from "<string>"
              #put the dots in for phil keywords
              kline = kline.split("<")[0].replace(" ",".")[:-1] + " <" + kline.split("<")[1]
              #convert to html
              kline = kline.replace("<","&lt;").replace(">","&gt;").replace("\n","")
              html.write(colorkey().color(subkey.expert_level,kline))
              text = subkey.text()
              splittext = text.split("\n")
              for line in splittext:
                if len(line):
                  kline = line.replace("<","&lt;").replace(">","&gt;").replace("\n","")
                  html.write(sp+sp+kline + "<br>\n")
              return first
  # html.write("<h2>Sorted Keywords</h2>\n")
    if len(flat.modes_keywords[voyager]['modes']):
      for expert in colorkey().doc_levels():
        headertxt = ""
        html.write("<h2>" + colorkey().lookup[expert]["key"].title() +"</h2>\n")
        html.write("<b>" + colorkey().lookup[expert]["txt"] +"</b><br>\n")
        for keyword in sorted(flat.modes_keywords[voyager]['keywords']):
          first = True
          for subkey in sorted(flat.flat_wiki[keyword]):
            if (int(subkey.expert_level) == expert):
              first = write_kline(first,html,subkey)
          if not first:
            html.write(colorkey().link())
      html.write("<br>\n")
     #for keyword in sorted(flat_wiki.keys()):
    if len(flat.modes_keywords[voyager]['modes']):
      html.write("<h2>All Keywords</h2>\n")
      for keyword in sorted(flat.modes_keywords[voyager]['keywords']):
          first = True
          from functools import cmp_to_key
          for subkey in sorted(flat.flat_wiki[keyword],key=cmp_to_key(lambda item1, item2: item1.expert_level < item2.expert_level)):
            if (int(subkey.expert_level) <= max_expert_level):
              first = write_kline(first,html,subkey)
          if not first:
            html.write(colorkey().link())
      html.write("\n")
    html.write("</html>\n")
    html.close()
    return htmlfile

def write_voyager_html(source,target,env):
    vfile = "voyager.html"
    html_file = os.path.join(env.autogen,vfile)
    html = open(html_file, "w")
 #  sys.stdout.write("Writing voyager html file " + html_file + "\n")
    html.write("<html>\n")
 #  html.write("<pre>\n")
 #  for line in splash_header:
 #     html.write(line + "\n")
 #  html.write("</pre>\n")
    html.write("<h2>Voyager Documentation</h2>\n")
    html.write(voyager_generator().html("voyager"))
    name = generic_documentation_name
    voyager = os.path.join("./doc",name+".html")
    html.write("<a href=\"" + voyager + "\">"+name.title()+" documentation</a><br>\n")
    html.write(voyager_generator().html(name))
    html.write("<p>Voyager is composed of different Voyager protocols.</p>")
    for k in voyager_generator().scripts():
      voyager = os.path.join("./doc",k+".html")
      html.write("<a href=\"" + voyager + "\">"+k.title()+" documentation</a><br>\n")
      html.write(voyager_generator().html(k))
    html.write("<p>Return to this page by clicking on the link to further documentation at the top of the control panel.</p>")
    html.write("</html>\n")
    return vfile

def write_voyager_doc(source,target,env):
    sys.stdout.write("phasertng: Writing documentation files\n")
    doclist = voyager_generator().scripts()
    doclist.append(generic_documentation_name) # extra one for all documentation
    filelist = []
    flat = flat_stanley(env)
    #manually remove the mode keyword from the list of keywords for a mode, since this 'keyword' doesn't make sense
    for voyager in flat.modes_keywords:
      if 'mode' in flat.modes_keywords[voyager]['keywords']:
        flat.modes_keywords[voyager]['keywords'].remove('mode')
    for voyager in doclist:
      f = write_html_file(env.autogen,voyager,flat)
      filelist.append(f)
      f = write_wiki_file(env.autogen,voyager,flat)
      filelist.append(f)
      f = write_text_file(env.autogen,voyager,flat)
      filelist.append(f)
    f = write_voyager_html(source,target,env)
    filelist.append(f)
    copy_files(source,target,env,filelist)

if __name__ == "__main__":
  source = ""
  target = ""
  os.autogen = "/tmp"
  os.phil_params_file  = os.path.join("..","phasertng","phenix_interface","__init__.params")
  os.types_dir  = os.path.join("..","codebase","phasertng","keywords","types")
  os.dist_path  = "."
  write_voyager_doc(source,target,env=os)
