//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/minimizer/RefineSP.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/jiffy.h>
#include <cctbx/xray/fast_gradients.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>

//#define PHASERTNG_REFINESB_AS_OCCUPANCY
//the occupancy parameters work much better in the study params than the u values
//use eg PHASERTNG_DEBUG_REFINESB_STUDYPARAMS_OFF to remove individual conditions
//define flag sets u refinement to occupancy refinement instead, which is better behaved
//and substitutes simple function for LLGI to test derivatives
//and sets each atom to a separate segment for looking per atom
//#define PHASERTNG_DEBUG_REFINESB_STUDYPARAMS
//#define PHASERTNG_DEBUG_REFINESB

namespace phasertng {

  RefineSP::RefineSP(
      const_ReflectionsPtr  REFLECTIONS_,
      sv_bool  SELECTED_,
      const_EpsilonPtr  EPSILON_)
    : RefineBase(),
      REFLECTIONS(std::move(REFLECTIONS_)),
      SELECTED(SELECTED_),
      EPSILON(std::move(EPSILON_))
  {
    const UnitCell& UC = REFLECTIONS->UC;
    af_millnx mref = REFLECTIONS->get_miller_array();
    af_double twostol; //only use selected
    for (int r = 0; r < mref.size(); r++)
      if (SELECTED[r])
        twostol.push_back(UC.cctbxUC.two_stol(mref[r]));
    bool do_recount(true);
    bin.setup(6,200,1000,twostol,do_recount,DEF_LORES);
  }

  void
  RefineSP::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineSP::init_node(dag::Node* work)
  {
    negvar = false;
    NODE = work;
    PHASER_ASSERT(!solTerm.is_default());
    phaser_assert(NODE->FULL.PRESENT);
    phaser_assert(NODE->HALL.size());
    PHASER_ASSERT(NODE->HALL == REFLECTIONS->SG.HALL);
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    PHASER_ASSERT(NODE->fraction_scattering(ztotalscat)<=1);
    //the parameters should already be in range from the check before the refinement
    NODE->reset_parameters(REFLECTIONS->get_ztotalscat());
    const UnitCell& UC = REFLECTIONS->UC;
    const SpaceGroup& SG = REFLECTIONS->SG;
    af_millnx mref = REFLECTIONS->get_miller_array();
    double d_min = cctbx::uctbx::d_star_sq_as_d(UC.cctbxUC.max_d_star_sq(mref.const_ref()));
    reffft = fft::complex_to_complex_3d(SG.type(),UC.cctbxUC);
    reffft.calculate_gridding(d_min,UC.cctbxUC);
    reffft.allocate();
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
      if (SELECTED[r])
      {
        miller_array.push_back(work_row->get_miller());
        miller_array.push_back(-work_row->get_miller());
      }
    }
    calcSigmaP(); // rbin and SigmaP
  }

  void
  RefineSP::init_segments()
  {
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    SEGMENTS.resize(0);
#ifdef PHASERTNG_DEBUG_REFINESB_STUDYPARAMS_OFF
    for (int a = 0; a < coordinates.size(); a++)
    {
      SEGMENTS.push_back(sbrdata()); //segment is a single atom
      auto& segment = SEGMENTS.back();
      segment.atom_number.push_back(a);
      if (REFINE == refine::OCCUPANCY)
        segment.init.push_back(1);
      else if (REFINE == refine::BFACTOR)
        segment.init.push_back(coordinates.XRAY[a].u_iso);
    }
#else
    for (int segid = 1; segid < coordinates.nsegments(); segid++)
    {
      SEGMENTS.push_back(sbrdata());
      for (int a = 0; a < coordinates.size(); a++)
      {
        auto atom = coordinates.PDB[a];
        if (atom.Segment == segid)
        {
          auto& segment = SEGMENTS.back();
          segment.atom_number.push_back(a);
          if (REFINE == refine::OCCUPANCY)
            segment.init.push_back(atom.O);
          else if (REFINE == refine::BFACTOR)
            segment.init.push_back(cctbx::adptbx::b_as_u(atom.B));
        }
      }
      SEGMENTS.back().current = SEGMENTS.back().average_init(); //awkward...
    }
#endif
  }

  void
  RefineSP::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(stolower(item));
    if (protocol.size() != 1)
      throw Error(err::FATAL,"Protocol can only have one choice");
    for (auto item : protocol)
    {
      if (item != "" and
          item != "off" and
          item != "bfac" and
          item != "occ"
        )
        throw Error(err::FATAL,"Protocol flag not recognised: " + item);
      if (item == "occ")
        REFINE = refine::OCCUPANCY;
      else if (item == "bfac")
        REFINE = refine::BFACTOR;
    }
    init_segments();
    bool REFINE_VALUE(true);
    if (protocol.find("off") != protocol.end())
      REFINE_VALUE = false;

    refine_parameter_.clear();
#ifdef PHASERTNG_DEBUG_REFINESB_STUDYPARAMS
    int bb(3); //limit the number for sanity
    if (getenv("PHASER_STUDYPARAMS_NMP") != NULL)
      bb = std::atoi(getenv("PHASER_STUDYPARAMS_NMP"));
    for (int b = 0; b < SEGMENTS.size(); b++)
      (REFINE_VALUE and b<bb) ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
#else
    //everything at the lower limit of the refinement from the previous cycle
    //will be set to zero and fixed
    //don't want to set to zero during refinement as it is hard to get off the lower bound
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      double tol = 0.01;
      bool fix(false);
      if (REFINE == refine::OCCUPANCY)
        fix = (segment.current <= (OCC_MIN + tol));
      (REFINE_VALUE and !fix) ? refine_parameter_.push_back(true):refine_parameter_.push_back(false);
    }
#endif

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();
    return;
  }

  sv_string
  RefineSP::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    int i(0),m(0);
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      std::string value;
      if (REFINE == refine::OCCUPANCY)
        value = " occupancy #";
      else if (REFINE == refine::BFACTOR)
        value = " Uiso #";
      if (refine_parameter_[m++])
        names[i++] = "Segment" + value + itos(b+1);
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineSP::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      if (refine_parameter_[m++])
      {
        pars[i++] = segment.current;
      }
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineSP::set_macrocycle_parameters(sv_double newx)
  { //applyShift
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    int m(0),i(0);
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      if (refine_parameter_[m++]) //BFAC
      {
        segment.current = newx[i++];
        for (int s = 0; s < segment.size(); s++)
        {
          int a = segment.atom_number[s];
          if (REFINE == refine::OCCUPANCY)
            coordinates.SetOccupancy(a,segment.current);
          else if (REFINE == refine::BFACTOR)
            coordinates.SetIsotropicU(a,segment.current+segment.init[s]);
        }
      }
    }
    if (REFINE == refine::OCCUPANCY)
    { //this breaks the gradient calculation as we do not account for delta-fs in the gradients
      //however it is important for the target function
      NODE->FULL.ENTRY->SCATTERING = coordinates.scattering();
      //calcSigmaP();
    }
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineSP::macrocycle_large_shifts()
  {
    sv_double largeShifts(nmp);
    int m(0),i(0);
    double large(0);
    if (REFINE == refine::OCCUPANCY)
      large = 0.1;
    else if (REFINE == refine::BFACTOR)
      large = cctbx::adptbx::b_as_u(10.0);
#ifdef PHASERTNG_DEBUG_REFINESB_STUDYPARAMS
    if (getenv("PHASER_STUDYPARAMS_BFAC") != NULL)
      if (REFINE == refine::OCCUPANCY)
         large = std::atof(getenv("PHASER_STUDYPARAMS_BFAC"));
      else if (REFINE == refine::BFACTOR)
         large = cctbx::adptbx::b_as_u(std::atof(getenv("PHASER_STUDYPARAMS_BFAC")));
#endif
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      if (refine_parameter_[m++])
        largeShifts[i++] = large; //BFAC
    }
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineSP::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    //upper bound for occupancy higher than 1 to allow for reduced scattering elsewhere
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      if (refine_parameter_[m++])
      {
        double lower(0),upper(0);
        if (REFINE == refine::OCCUPANCY)
        {
          lower = OCC_MIN;
          upper = OCC_MAX;
        }
        else if (REFINE == refine::BFACTOR)
        {
          lower = cctbx::adptbx::b_as_u(BFAC_MIN)-segment.lower()+DEF_PPT;
          upper = cctbx::adptbx::b_as_u(BFAC_MAX)-segment.upper()-DEF_PPT;
        }
        bounds[i++].on(lower, upper); //BFAC
      }
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineSP::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    sv_double pars(nmp);
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      if (refine_parameter_[m++]) //BFAC
        repar[i++].off();
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineSP::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    //store the llg without restraints
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don' want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineSP::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineSP::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineSP::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  af_string
  RefineSP::logCurrent(std::string title)
  {
    af_string lines;
    lines.push_back(title);
    lines.push_back("LLG=" + dtos(NODE->LLG,2));
    if (REFINE == refine::OCCUPANCY)
      lines.push_back("Occupancy");
    else if (REFINE == refine::BFACTOR)
      lines.push_back("B-factor");
    int len(10),n(4),l(1);
    len = std::min(len,static_cast<int>(SEGMENTS.size()));
    std::string line("segments " + itos(1,n,true,false) + "->" + itos(len,n,false,false) + " ");
    bool toadd(false),fixed(false);
    bool all_fixed = true; //macrocycle with
    for (int b = 0; b < refine_parameter_.size(); b++)
      if (refine_parameter_[b]) //may not yet be defined
        all_fixed = false;
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      if (REFINE == refine::OCCUPANCY)
      {
        line += dtos(segment.current,4,2);
      }
      else if (REFINE == refine::BFACTOR)
      {
        line += dtos(segment.bfactor(),6,1);
      }
      toadd = true;
      if (b < refine_parameter_.size() and !all_fixed and !refine_parameter_[b]) //may not yet be defined
      {
        line += "*";
        fixed = true;
      }
      else
      {
        line += " ";
      }
      if (!((b+1)%len)) //lists 10 long
      {
        lines.push_back(line);
        l+=len;
        int lmax = l+len-1;
        lmax = std::min(lmax,static_cast<int>(SEGMENTS.size()));
        line = ("segments " + itos(l,n,true,false) + "->" + itos(lmax,n,false,false) + " ");
        toadd = false;
      }
    }
    if (toadd) lines.push_back(line);
    if (fixed) lines.push_back("* fixed value");
    lines.push_back("");
    return lines;
  }

  af_string
  RefineSP::initial_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return logCurrent("Initial");
  }

  af_string
  RefineSP::current_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return logCurrent("Current");
  }

  af_string
  RefineSP::final_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return logCurrent("Final");
  }

  af_string
  RefineSP::cleanup_parameters()
  {
    af_string txt;
    if (REFINE == refine::OCCUPANCY)
    { //do this each macrocycle instead
      auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
      txt = binary_occupancy(); //function
      NODE->FULL.ENTRY->SCATTERING = coordinates.scattering();
      //calcSigmaP();
    }
    return txt;
/*
    if (REFINE == refine::OCCUPANCY)
    {
      auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    //everything at the lower limit of the refinement from the previous cycle
    //will be set to zero and fixed
    //don't want to set to zero during refinement as it is hard to get off the lower bound
      for (int b = 0; b < SEGMENTS.size(); b++)
      {
        auto& segment = SEGMENTS[b];
        double tol = 0.01;
        double occupancy =  (segment.current <= (OCC_MIN + tol)) ? 0 : 1;
        for (int s = 0; s < segment.size(); s++)
          coordinates.SetOccupancy(segment.atom_number[s],occupancy);
      }
      //at the end of the macrocycle re-normalize the data for the partial occupancies
      // ? don't do this, because we want to tie to original scattering
      NODE->FULL.ENTRY->SCATTERING = coordinates.scattering();
      calcSigmaP();
      for (int b = 0; b < SEGMENTS.size(); b++)
      {
        auto& segment = SEGMENTS[b];
        for (int s = 0; s < segment.size(); s++)
          coordinates.SetOccupancy(segment.atom_number[s],segment.current);
      }
    }
    return txt;
*/
  }

  af_string
  RefineSP::finalize_parameters()
  {
    return binary_occupancy(); //function
  }

  TargetGradientHessianType
  RefineSP::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,true);
    tgh.Target = 0;

    sv_double dLL_by_dP(SEGMENTS.size(),0);
    double large(0);
    if (REFINE == refine::OCCUPANCY)
      large = 0.1;
    else if (REFINE == refine::BFACTOR)
      large = cctbx::adptbx::b_as_u(10.0);
    sv_double d2LL_by_dP2(SEGMENTS.size(),1/fn::pow2(large));

    const auto& UC = REFLECTIONS->UC;
    const auto& SG = REFLECTIONS->SG;
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    phaser_assert(coordinates.XRAY.size());

    af_cmplex d_target_d_f_calc;
    {{
    // --- calcuate fft with stored reffft
    af_cmplex Fcalc = reffft.backward(coordinates.XRAY,
                                      coordinates.scattering_type_registry,
                                      miller_array);
    //calcSigmaP(); //false in phaser
    d_target_d_f_calc.reserve(miller_array.size());
    likelihood::llgi::function LLGI;
    likelihood::llgi::tgh_flags flags;
    flags.target(true).gradient(do_gradient).hessian(false);
    const double fs = REFLECTIONS->fracscat(NODE->FULL.ENTRY->SCATTERING);
    const double bfac = 0; //pose B-factor added to atoms
    const double drms = 0; //all in b-factor
    int rr(0);

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        PHASER_ASSERT(rr < rbin.size());
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const auto miller = work_row->get_miller();
        const double feff = work_row->get_feffnat();
        const double resn = work_row->get_resn();
        const double dobs = work_row->get_dobsnat();
        const double eps = work_row->get_eps();
        const bool   cent = work_row->get_cent();
        const double rssqr = work_row->get_ssqr();
        const double ssqr = bin.MidSsqr(rssqr);
        const double teps = work_row->get_teps();
        const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        const double SigaSqr = fn::pow2(solTerm.get(ssqr));
        double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
          rssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
        if (nmol > 1)
        {
          double Vterm = array_G_Vterm[r];
          if (tncs_modelled) Vterm /= nmol;
          thisDobsSigaSqr *= Vterm;
        }
        auto   rDobsSiga = std::sqrt(thisDobsSigaSqr);
        int s = rbin[rr];
        PHASER_ASSERT(s < SigmaP.size());
        double sqrt_epsnSigmaP = std::sqrt(eps*SigmaP[s]);
        double terms_on_sqrt_epsnSigmaP = rDobsSiga/sqrt_epsnSigmaP;
        cmplex DobsSigaEcalc = terms_on_sqrt_epsnSigmaP*Fcalc[rr];
        //====
        //LLGI calculation
        //====
        double V = teps - fn::pow2(rDobsSiga);
        double eobs = feff/resn;
        LLGI.target_gradient_hessian(eobs, DobsSigaEcalc, teps, V, cent, flags);
        tgh.Target -= LLGI.LL;
        static int counter(0);

        if (V <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        if (V <= 0)
        {
          //const std::lock_guard<std::mutex> lock(mutex);
          //threadsafe
          //lock_guard for cleanup of incomplete mutex.lock() with mutex.unlock()
          counter++;
          std::cout << "\n---------- " << counter <<std::endl;
          std::cout << "RefineSP Debugging reflection #" + itos(r) << std::endl;
          std::cout << "V="+ dtos(LLGI.variance()) << std::endl;
          std::cout << "LL="+ dtos(LLGI.LL) << std::endl;
          std::cout << "Miller=" + ivtos(miller) << std::endl;
          std::cout << "Eobs="+ dtos(eobs) << std::endl;
          std::cout << "Ecalc=|"+ ctos(DobsSigaEcalc) + "|=" + dtos(std::abs(DobsSigaEcalc)) << std::endl;
          std::cout << "Centric="+ btos(cent) << std::endl;
          std::cout << "scattering="+ dtos(REFLECTIONS->get_ztotalscat()) << std::endl;
          std::cout << "rssqr="+ dtos(rssqr) << std::endl;
          std::cout << "reso="+ dtos(1/std::sqrt(rssqr)) << std::endl;
          std::cout << "tncs-g_drms="+ dtos(g_drms) << std::endl;
          std::cout << "tncs-epsfac="+ dtos(teps) << std::endl;
          if (array_G_Vterm.size())
          std::cout << "tncs-g_vterm="+ dtos(array_G_Vterm[r]) << std::endl;
          if (array_Gsqr_Vterm.size())
          std::cout << "tncs-gsqr_vterm="+ dtos(array_Gsqr_Vterm[r]) << std::endl;
          std::cout << "Dobs="+ dtos(dobs) << std::endl;
          std::cout << "---------" << std::endl;
          PHASER_ASSERT(V > 0);
        }//debug
#endif

        cmplex dtdec = cmplex(-LLGI.dLL_by_dReEC,-LLGI.dLL_by_dImEC);
        cmplex dtdfc = terms_on_sqrt_epsnSigmaP*dtdec;
        dtdfc *= 0.5; //only added target per r not rr
        d_target_d_f_calc.push_back(dtdfc);
        d_target_d_f_calc.push_back(std::conj(dtdfc));
#if 0
if (r < 10)
{
        const auto miller = work_row->get_miller();
std::cout << "debugging fft: r=" << r << " cent=" << cent << " E=" << eobs << " DobsSiga=" << rDobsSiga << " eps=" << eps << " Ecalc=" << DobsSigaEcalc << " V=" << V << " hkl=" << ivtos(miller) <<  " llg=" << tgh.Target << std::endl;
}
#endif
        rr+=2;
      }
    }
    }}

    if (do_gradient)
    {
      //this builds the density again, so that the gradients are wrf the recalculated density with Fcalc
      cctbx::xray::fast_gradients<double> fast_gradients(
          UC.cctbxUC,
          coordinates.XRAY.const_ref(),
          coordinates.scattering_type_registry,
          reffft.u_base,
          1.e-8 // wing_cutoff 1.e-8
          );

      ivect3 n = reffft.cfft.n();
      double  multiplier = UC.cctbxUC.volume() / n.product() * SG.group().n_ltr();
      cctbx::xray::apply_u_extra( //and normalize
          UC.cctbxUC,
          fast_gradients.u_extra(),
          miller_array.const_ref(),
          d_target_d_f_calc.ref(),
          multiplier);

      // --- do the fft
      bool treat_restricted(true);
      af::c_grid_padded<3> map_grid(reffft.cfft.n());
      cctbx::maptbx::structure_factors::to_map<double> gradmap(
          SG.group(),
          reffft.anomalous_flag,
          miller_array.const_ref(),
          d_target_d_f_calc.const_ref(),
          reffft.cfft.n(),
          map_grid,
          reffft.conjugate_flag,
          treat_restricted);

      // --- do the fft
      af::ref<cmplex, af::c_grid<3> > gradmap_ref(
          gradmap.complex_map().begin(),
          af::c_grid<3>(reffft.cfft.n()));

      reffft.cfft.backward(gradmap_ref);

      af::const_ref<cmplex, scitbx::af::c_grid_padded_periodic<3> > gradmap_const_ref(
          gradmap_ref.begin(),
          scitbx::af::c_grid_padded_periodic<3>(reffft.cfft.n(),reffft.cfft.n()));

      bool site(false),u_iso(false),u_aniso(false),occupancy(false); //rest false
      occupancy = (REFINE == refine::OCCUPANCY);
      u_iso = (REFINE == refine::BFACTOR);
      cctbx::xray::set_scatterer_grad_flags(coordinates.XRAY.ref(),site,u_iso,u_aniso,occupancy);
      af::const_ref<double> u_iso_refinable_params(0,0);
      bool sampled_density_must_be_positive(true);
      fast_gradients.sampling(
          coordinates.XRAY.const_ref(),
          u_iso_refinable_params,//if sqrt_u_iso false in grad_flags, this is ignored
          coordinates.scattering_type_registry,
          coordinates.site_sym_table,
          gradmap_const_ref,
          0, // not packed
          sampled_density_must_be_positive
          );
      af_double dtdparam;
      if (REFINE == refine::OCCUPANCY)
        dtdparam = fast_gradients.d_target_d_occupancy();
      else if (REFINE == refine::BFACTOR)
        dtdparam = fast_gradients.d_target_d_u_iso();
      for (int b = 0; b < SEGMENTS.size(); b++)
      {
        auto& segment = SEGMENTS[b];
        dLL_by_dP[b] = 0;
        for (int s = 0; s < segment.size(); s++)
        {
          int a = segment.atom_number[s];
          dLL_by_dP[b] += dtdparam[a]; //average, don't divide by segments.size()
        }
#if 0
if (b < 10)
std::cout << "debugging rbr fft grad: b=" << b << " " << dLL_by_dP[b] <<   std::endl;
#endif
      }
    }
    //no do_hessian, just set to identity

    if (do_gradient)
    {
      int m(0),i(0);
      for (int b = 0; b < SEGMENTS.size(); b++)
        if (refine_parameter_[m++])
        { tgh.Gradient[i++] = dLL_by_dP[b]; }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    if (do_hessian)
    {
      int m(0),i(0);
      for (int b = 0; b < SEGMENTS.size(); b++)
        if (refine_parameter_[m++])
        { tgh.Hessian(i,i) = d2LL_by_dP2[b]; i++; }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    return tgh;
  }

  void
  RefineSP::calcSigmaP()
  {
    const auto& UC = REFLECTIONS->UC;
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    phaser_assert(coordinates.XRAY.size());
    af_cmplex Fcalc = reffft.backward(coordinates.XRAY,
                                      coordinates.scattering_type_registry,
                                      miller_array);
    phaser_assert(miller_array.size());
    phaser_assert(miller_array.size() <= REFLECTIONS->NREFL*2);
    int NBINS = bin.numbins();
    phaser_assert(NBINS);
    sv_double numInBin(NBINS);
    SigmaP.resize(NBINS,0);
    rbin.resize(miller_array.size());
    for (int rr = 0; rr < miller_array.size(); rr++)
    {
      const double ssqr = UC.cctbxUC.d_star_sq(miller_array[rr]);
      //PHASER_ASSERT(bin.ssqr_in_range(ssqr));
      int s = bin.get_bin_ssqr(ssqr);
      double FCsqr = std::norm(Fcalc[rr]);
      //PHASER_ASSERT(FCsqr); //individual ones can be zero
      SigmaP[s] += FCsqr;
      numInBin[s]++;
      rbin[rr] = s;
    }
    for (int s = 0; s < NBINS; s++)
    {
      PHASER_ASSERT(numInBin[s]);
      PHASER_ASSERT(SigmaP[s]);
      SigmaP[s] /= numInBin[s];
      PHASER_ASSERT(SigmaP[s] > 0);
    }
  }

  pod::FastPoseType
  RefineSP::ecalcs_sigmaa_full()
  {
    calcSigmaP();
    //miller array here is not miller_array since this is not expanded
    af_millnx mref = REFLECTIONS->get_miller_array();
    //Fcalc is not the Fcalc in calcSigmaP
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    phaser_assert(coordinates.XRAY.size());
    af_cmplex Fcalc = reffft.backward(coordinates.XRAY,
                                      coordinates.scattering_type_registry,
                                      mref);
    pod::FastPoseType ecalcs_sigmaa;
    ecalcs_sigmaa.resize(REFLECTIONS->NREFL);
    const double fs = REFLECTIONS->fracscat(NODE->FULL.ENTRY->SCATTERING);
    const double bfac = 0; //pose B-factor added to atoms
    const double drms = 0; //all in b-factor
    phaser_assert(Fcalc.size() == REFLECTIONS->NREFL);

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    likelihood::llgi::function LLGI;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
    if (SELECTED[r])
    {
      const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
      const double rssqr = work_row->get_ssqr();
      const double eps = work_row->get_eps();
      const double ssqr = bin.MidSsqr(rssqr);
      const double dobs = work_row->get_dobsnat();
      const double g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
      int s = bin.get_bin_ssqr(rssqr); //not rbin because this is indexed on r not rr
      const double SigaSqr = fn::pow2(solTerm.get(ssqr));
      PHASER_ASSERT(s < SigmaP.size());
      PHASER_ASSERT(SigmaP[s] > 0);
      double DobsSigaSqr = likelihood::llgi::DobsSigaSqr(
          rssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
      if (nmol > 1)
      {
        double Vterm = array_G_Vterm[r];
        if (tncs_modelled) Vterm /= nmol;
        DobsSigaSqr *= Vterm;
      }
      ecalcs_sigmaa.sigmaa[r] = std::sqrt(DobsSigaSqr); //all the error in the occupancy and b-factor
      double sqrt_epsnSigmaP = std::sqrt(eps*SigmaP[s]);
      ecalcs_sigmaa.ecalcs[r] = (ecalcs_sigmaa.sigmaa[r]/dobs)*Fcalc[r]/sqrt_epsnSigmaP;
    }
    else
    {
      ecalcs_sigmaa.sigmaa[r] = 0;
      ecalcs_sigmaa.ecalcs[r] = {0,0};
    }
    }
    return ecalcs_sigmaa;
  }

  af_string
  RefineSP::binary_occupancy()
  {
    //logs as it goes
    af_string txt;
    if (REFINE != refine::OCCUPANCY)
      return txt;
    std::map<double,double> occperc;
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      //occupancy steps of 1% or 0.01, due to floor below
      //can change the steps here by rounding to nearest 0.02 for example
      double steps(100);
      double occ = std::floor(segment.current*steps)/steps;
      occperc[occ] = 0;
    }
    for (auto& cutoff : occperc)
    {
      for (int b = 0; b < SEGMENTS.size(); b++)
      {
        auto& segment = SEGMENTS[b];
        if (segment.current < cutoff.first)
          cutoff.second += 100./SEGMENTS.size();
      }
    }
    txt.push_back("Binary Occupancy");
    txt.push_back("----------------");
    txt.push_back("There are " + itos(occperc.size()) + " refined occupancies");
    txt.push_back("Maximum percent pruned " + dtos(MAX_PERCENT_PRUNED,0));
    txt.push_back(snprintftos(
        "%-9s %9s %7s","Occupancy","Pruned(%)","LLG"));
    auto& coordinates = NODE->FULL.ENTRY->COORDINATES;
    std::map<double,double> llgocc;
    double minocc = 0.0;
    {
      for (auto cutoff : occperc)
      {
        //if the cutoff is for more than half the structure, then ignore
        //sets a bound on the cutoff that is interesting
        //very low fraction scattering should not score as well in llg anyway
        if (cutoff.second > MAX_PERCENT_PRUNED)
        {
          txt.push_back(snprintftos(
              "<%4.2f %10.1f %10s",cutoff.first,cutoff.second,"---"));
          continue;
        }
        for (int b = 0; b < SEGMENTS.size(); b++)
        {
          auto& segment = SEGMENTS[b];
          double tmp = (segment.current < cutoff.first) ? minocc : OCC_MAX;
          for (int s = 0; s < segment.size(); s++)
          {
            int a = segment.atom_number[s];
            coordinates.SetOccupancy(a,tmp);
          }
        }
        NODE->FULL.ENTRY->SCATTERING = coordinates.scattering();
        //calcSigmaP();
        double l = likelihood();
        llgocc[l] = cutoff.first;
        txt.push_back(snprintftos(
            "<%4.2f %10.1f %10.2f",cutoff.first,cutoff.second,-l));
      }
    }
    double best_occupancy = llgocc.begin()->second;
    double best_llg = llgocc.begin()->first;
    double best_pruned = occperc[best_occupancy];
    double occupancy = best_occupancy;
    double llg = best_llg;
    double pruned = best_pruned;
    txt.push_back("---best---");
    txt.push_back(snprintftos(
        "<%4.2f %10.1f %10.2f",occupancy,pruned,-llg));
    int first(-1);
    for (auto& item : llgocc)
    {
      first++;
      if (first == 0) continue;
      //will take the lowest best_llg with the least amount of pruning
      if (-item.first > (-best_llg)-LLG_BIAS and
         occperc[item.second] < best_pruned)
      {
        if (first == 1)
          txt.push_back("---high---");
        occupancy = item.second;
        llg = item.first;
        pruned = occperc[occupancy];
        txt.push_back(snprintftos(
          "<%4.2f %10.1f %10.2f",occupancy,pruned,-llg));
      }
    }
    for (int b = 0; b < SEGMENTS.size(); b++)
    {
      auto& segment = SEGMENTS[b];
      double occ = (segment.current < occupancy) ? minocc : OCC_MAX;
      for (int s = 0; s < segment.size(); s++)
      {
        int a = segment.atom_number[s];
        coordinates.SetOccupancy(a,occ);
      }
    }
    txt.push_back("----------");
    return txt;
  }

} //phasertng
