//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_mst_class__
#define __phasertng_pod_mst_class__
#include <phasertng/main/uuid.h>
#include <map>
#include <scitbx/array_family/shared.h>
#include <scitbx/vec3.h>
#include <numeric> //Windows accumulate

typedef scitbx::af::shared<double> af_double;
typedef scitbx::vec3<double> dvect3;

namespace phasertng {
namespace pod {

class parent_mean_sigma_top
{
  public:
    std::map<uuid_t,dvect3> mst;

    af_double histdat;
    double histmax;
    double histmin;

  public:

  size_t size() { return mst.size(); }

  double
  zscore(uuid_t id,double v)
  {
    double f_mean = mst.at(id)[0];
    double f_sigma = mst.at(id)[1];
    if (f_sigma == 0) return 0;
    return (v-f_mean)/f_sigma;;
  }

  bool
  has_sigma(uuid_t id)
  {
    double f_sigma = mst.at(id)[1];
    return (f_sigma > 0);
  }

  void set_all(dvect3 v)
  {
    for (auto& smst : mst)
      smst.second = v;
  }

  double
  merged_lowest_mean()
  {
    double min_f_mean = std::numeric_limits<double>::max();
    for (auto item : mst)
    {
      double f_mean = item.second[0];
      min_f_mean = std::min(f_mean,min_f_mean); //histogram, data
    }
    return min_f_mean;
  }

  af_double
  lowest_means()
  {
    af_double dat;
    for (auto item : mst)
    {
      double f_mean = item.second[0];
      dat.push_back(f_mean);
    }
    return dat;
  }

  double
  merged_average_mean()
  {
    af_double mm;
    for (auto item : mst)
      mm.push_back(item.second[0]);
    double sum(0);
    sum = std::accumulate(mm.begin(), mm.end(), sum);
    return sum/mm.size();
  }

  void
  calculate_histogram_mean()
  {
    double min_f_mean = std::numeric_limits<double>::max();
    double max_f_mean = std::numeric_limits<double>::lowest();
    int i(0);
    for (auto item : mst)
    {
      double f_mean = item.second[0];
      histdat.push_back(f_mean+(i++)*DEF_PPM); //so not strictly the same
      //scitbx histogram data_min < data_max fails otherwise
      min_f_mean = std::min(f_mean,min_f_mean); //histogram, data
      max_f_mean = std::max(f_mean,max_f_mean); //histogram
    }
    double buffer = (max_f_mean-min_f_mean)/2.;
    buffer = std::max(1.,buffer);
    histmin = min_f_mean-buffer;
    histmax = max_f_mean+buffer;
  }

  void
  calculate_histogram_sigma()
  {
    double min_f_sigma = std::numeric_limits<double>::max();
    double max_f_sigma = std::numeric_limits<double>::lowest();
    int i(0);
    for (auto item : mst)
    {
      double f_sigma = item.second[1];
      histdat.push_back(f_sigma);
      histdat.push_back(f_sigma+(i++)*DEF_PPM); //so not strictly the same
      //scitbx histogram data_min < data_max fails otherwise
      min_f_sigma = std::min(f_sigma,min_f_sigma); //histogram
      max_f_sigma = std::max(f_sigma,max_f_sigma); //histogram
    }
    double buffer = (max_f_sigma-min_f_sigma)/2.;
    buffer = std::max(1.,buffer);
    histmin = min_f_sigma-buffer;
    histmax = max_f_sigma+buffer;
  }

  double
  max_top()
  {
    double max_f_top = std::numeric_limits<double>::lowest();
    PHASER_ASSERT(mst.size());
    for (auto item : mst)
    {
      double f_top = item.second[2];
      max_f_top = std::max(f_top,max_f_top); //histogram
    }
    return max_f_top;
  }

};

}}
#endif
