#ifndef PHASER_NCS_REFERENCECHAIN_H
#define PHASER_NCS_REFERENCECHAIN_H

#include "MacromoleculeType.h"

#include <iotbx/pdb/hierarchy.h>

#include <string>

namespace phaser {  namespace ncs {

class ChainAlignment
{
    private:
        std::vector<
            std::pair<
                iotbx::pdb::hierarchy::residue_group,
                iotbx::pdb::hierarchy::residue_group
                >
            > alignment_;
        size_t ref_chain_length_;
        size_t ali_chain_length_;

        typedef std::vector<
            std::pair<
                iotbx::pdb::hierarchy::residue_group,
                iotbx::pdb::hierarchy::residue_group
                >
            >::const_iterator aliIter;

    public:
        ChainAlignment(
            const iotbx::pdb::hierarchy::chain& ref,
            const iotbx::pdb::hierarchy::chain& ali,
            long offset
            );
        ~ChainAlignment();

        // Information methods
        float get_sequence_identity() const;
        float get_sequence_coverage() const;
        size_t get_alignment_length() const;
        size_t get_overlap_length() const;
        const std::vector<
            std::pair<
            iotbx::pdb::hierarchy::residue_group,
            iotbx::pdb::hierarchy::residue_group
            > >&
            get_residue_group_alignment() const;
        std::vector<
            std::pair<
            iotbx::pdb::hierarchy::atom,
            iotbx::pdb::hierarchy::atom
            > >
            get_atom_alignment() const;
};


class ReferenceChain
{
    private:
        iotbx::pdb::hierarchy::chain chain_;
        MacromoleculeType mmt_;

    public:
        // Constructor
        ReferenceChain(
            const iotbx::pdb::hierarchy::chain& ch,
            const MacromoleculeType& mmt
            );
        ~ReferenceChain();

        // Accessors
        const iotbx::pdb::hierarchy::chain& get_chain() const;
        const MacromoleculeType& get_macromolecule_type() const;

        // Alignment methods
        std::pair< std::string, long > determine_numbering_shift(
            const iotbx::pdb::hierarchy::chain& other
            ) const;
};

}  } // namespace phaser::ncs

#endif /*PHASER_NCS_REFERENCECHAIN_H*/
