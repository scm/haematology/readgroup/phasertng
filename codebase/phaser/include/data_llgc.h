//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_LLGC_CLASS__
#define __PHASER_DATA_LLGC_CLASS__
#include <set>

namespace phaser {

struct data_llgc
{
  std::set<std::string> ATOMTYPE;
  bool        COMPLETE = true;
  double      CLASH = 0;
  double      SIGMA = 6;
  bool        TOP_ONLY = false;
  int         NCYC = 20;
  std::string METHOD = "ATOMTYPE";
  bool        PEAKS_OVER_HOLES = true;
  bool        ATOM_CHANGE_ORIG = false;
  bool        USE_BSWAP = true;
  bool        USE_PEAK1_BELOW = true;
  double      PEAK1_BELOW_ZSCORE = 6;
};

} //phaser

#endif
