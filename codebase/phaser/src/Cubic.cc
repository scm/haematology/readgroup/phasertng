//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Cubic.h>
#include <phasertng/main/constants.h>

namespace phaser {

    Cubic::Cubic()
    {
      dvect3 DEF_BINS_CUBI(0,1,0); //hardwired default
      CUBIC = DEF_BINS_CUBI;
      IS_DEFAULT = IS_VALID = true;
    }

    Cubic::Cubic(double A, double B, double C)
    {
      bool all_coefficients_positive(A >=0 && B >=0 && C >=0);
      bool is_depressed_cubic( (A == 0 && B == 0 && C != 0) || (A == 0) || (B == 0));
      IS_VALID = (all_coefficients_positive && is_depressed_cubic);
      double tol(0.000001);
      if (B > 1-tol && B < 1+tol) B = 1;
      IS_DEFAULT = (A == 0 && B ==1 && C == 0);
      CUBIC = dvect3(A,B,C);
    }

    double Cubic::FnSShell(double SMIN,double SMAX,int NUMBINS)
    { return (FnS(SMAX) - FnS(SMIN))/NUMBINS; }

    double Cubic::InvFnS(double num)
    {
      double THIRD(1.0/3.0);
      if (IS_DEFAULT) // the default, optimize
        return sqrt(num);
      else if (CUBIC[0] != 0)
      {
        if (CUBIC[1] == 0 && CUBIC[2] == 0)
          return (num >= 0) ? pow(num, THIRD) : -pow(-num, THIRD);
        else if (CUBIC[1] == 0)
        {
    //find the roots of the depressed cubic
          double dB = CUBIC[2]/CUBIC[0];
          double dC = num/CUBIC[0];
          double dPart1 = -dC/2.0 + sqrt((dC*dC/4.0)+(dB*dB*dB/27.0));
          double dPart2 = dC/2.0 + sqrt((dC*dC/4.0)+(dB*dB*dB/27.0));
          dPart1 = (dPart1 >= 0) ? pow(dPart1,THIRD) : -pow(-dPart1,THIRD);
          dPart2 = (dPart2 >= 0) ? pow(dPart2,THIRD) : -pow(-dPart2,THIRD);
          double s = dPart1-dPart2;
          return s>0?s:-s;
        }
    //p.s. Should never get here as it's trapped on input
    //Use int numRoots = gsl_poly_solve_cubic(A,B,C,&x0,&x1,&x2) for general soln
    //if gsl library installed
      }
      else if (CUBIC[1] != 0)
      {
        double A = CUBIC[1];
        double B = CUBIC[2];
        double C = num;
        double x(0);
        double discriminant = B*B-4*A*C;
    //we know the roots have to be real - correct for rounding errors
    //that might make the discriminant slightly negative
        discriminant = (discriminant>0) ? discriminant : 0;
        x = (-B + sqrt(discriminant))/(2*A);
        return x;
      }
      //else if (CUBIC[2] != 0) linear
      return num/CUBIC[2];
    }

    double Cubic::FnS(double s)
    {
    //Function is ax^3 + bx^2 + cx + d  where d is zero
    //The condition applied on input is a>0 b>0 c>0, and its either a
    //linear (a=b=0), quadratic (a=0) or depressed cubic (b=0)
    //This ensures the function is monotonically increasing
    //  linear with  c>0
    //  quadratic with b>0 (upturned parabola)
    //  cubic - bit more complicated.
    //     Take the derivative and you get the quadratic ax^2 + bx + c.
    //     Cubic is monotonic when b^2 - 4ac < 0
    //     So if a,b & c all positive, then function will always be monotonic
    //     when b=0, i.e. it's a "depressed cubic" (no quadratic term)
    //     Furthermore...
    //     A depressed cubic always has a root at
    //     cubert(-c/2 + sqrt(c^2/4+b^3/27)) - cubert(c/2 + sqrt(c^2/4+b^3/27))
    //     (Ferro-Tartaglia formula)
    //     which means we don't have to resort to tricky functions to find
    //     the inverse for resolution calculations.
      if (IS_DEFAULT)
        return s*s;
      return  CUBIC[0]*s*s*s + CUBIC[1]*s*s + CUBIC[2]*s ;
    }

}
