#ifndef __phasertng_spherical_Y_class__
#define __phasertng_spherical_Y_class__
#include <scitbx/constants.h>
#include <vector>
#include <limits>
#include <cmath>

namespace phasertng {
namespace table {

  class sphbessel
  {
    const double DBL_FAC = (1.0e15);
    const double BIGNUM = (std::numeric_limits<double>::max()/DBL_FAC);
    const double BIGLOG = (std::log(BIGNUM));
    const double SMALLNUM = (std::numeric_limits<double>::min()*DBL_FAC);

    public:
      static sphbessel& getInstance() { return inst_; }
      static sphbessel inst_;
    double get(int,double) const;
    std::vector<double> forward_recurrance(int,double) const;

    std::vector<int> m_table;
    std::vector<double> sqrt_table;
    std::vector<double> normalization;

    sphbessel(int=120);//mathematical limit, don't change
  };
}

}//end namespace phaser

#endif
