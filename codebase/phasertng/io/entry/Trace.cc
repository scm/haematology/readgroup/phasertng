//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Trace.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/constants.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <phasertng/table/protein_composition.h>
#include <mmtbx/geometry/calculator.hpp>
#include <fstream>

namespace phasertng {

  void
  Trace::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());
    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    infile.close();
    SetPdb(pdblines);
  }

  void
  Trace::WritePdb()
  {
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    pdbhandler.VanillaWritePdb(*this);
  }

  void
  Trace::GetPdbHandler(PdbHandler& pdbhandler)
  {
    SpaceGroup SG; //default= P1
    UnitCell UC; //default= 1 1 1 90 90 90
    pdbhandler = PdbHandler(SG,UC,1);
    Monostructure::GetPdbHandler(pdbhandler);
    PHASER_ASSERT(SAMPLING_DISTANCE);
    PHASER_ASSERT(MAX_RADIUS);
    pdbhandler.addRemarkPhaser("SAMPLING " + dtos(SAMPLING_DISTANCE,12,6));
    pdbhandler.addRemarkPhaser("CENTRE " + dvtos(CENTRE,12,6));
    pdbhandler.addRemarkPhaser("RADIUS " + dtos(MAX_RADIUS,12,6));
    for (int i = 0; i < POINT_GROUP_EULER.size(); i++)
      pdbhandler.addRemarkPhaser("PG_EULER " + dvtos(POINT_GROUP_EULER[i],12,6));
  }

  af_string
  Trace::GetPdb()
  {
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    return pdbhandler.PdbString();
  }

  bool
  Trace::is_atom() const
  {
    //calculate if it is a single atom
    bool this_is_one_atom = false;
    int natom(0);
    for (auto a : PDB)
    if (a.O > 0.0)
    {
      natom++;
      if (natom >= 2) break; //molecule
    }
    if (natom == 1) this_is_one_atom = true;
    return this_is_one_atom;
  }

  int
  Trace::number_of_calphas() const
  {
    int natom(0);
    for (auto a : PDB)
      if (a.is_calpha())
        natom++;
    return natom;
  }

  void
  Trace::trim_to_calphas()
  {
    for (auto& a : PDB)
      a.valid_element = (a.is_calpha());
    erase_invalid();
  }

  void
  Trace::set_valid_element(bool b)
  {
    for (auto& a : PDB)
      a.valid_element = b;
  }

  void
  Trace::renumber()
  {
    for (int a = 0; a < PDB.size(); a++)
    {
      PdbRecord& card = PDB[a];
      card.AtomNum = a+1;
      card.ResNum = a+1;
    }
  }

  void
  Trace::trim_asa()
  {
    table::vanderwaals_radii vdw(SAMPLING_DISTANCE);
    sv_double myradii(PDB.size(),0);
    for (int a = 0; a < PDB.size(); a++)
      myradii[a] = vdw.get(PDB[a].get_element());

    PHASER_ASSERT(SAMPLING_DISTANCE);
    int    sampling_point_count = 960;
    //double probe = 1.4;
    double probe = SAMPLING_DISTANCE; //if too small, it rolls right around the hexgrid points
    mmtbx::geometry::asa::calculator::SimpleCalculator<
      mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
      sv_double // arrays can be used directly without adaptors
     >
     mycalc(
       mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >( PDB ),
       myradii,
       probe, // probe radius
       sampling_point_count // sampling point count
     );

    //std::cout << "trim_asa" << std::endl;
    //while still a vector
    sv_int counts(101,0); //for debugging histogram
    double structure_asa(0.2);
    for (int a = 0; a < PDB.size(); a++)
    {
      double accessible_surface_area = mycalc.accessible_points(a)/double(sampling_point_count);
      counts[int(accessible_surface_area*100)]++;
      PDB[a].valid_element = (accessible_surface_area < structure_asa);
    //  std::cout << "valid asa=" << " " << PDB[a].valid_element << " " << accessible_surface_area << " " << structure_asa << std::endl;
    }
    int nvalid(0);
    for (int a = 0; a < PDB.size(); a++)
      if (PDB[a].valid_element) nvalid++;

    //don't trim if you trim to nothing!!
    if (nvalid) erase_invalid();
    //for (int a = 0; a < counts.size(); a++) std::cout << "histogram asa=" + dtos(a/100.) + " num=" + itos(counts[a]) + std::string(a/100. > structure_asa ? " erased" :"") << std::endl;
    //std::cout << "calculate_weight" << std::endl;
  }

  //another constructor
  void
  Trace::build_hexgrid_box(double SAMP,dvect3 box_min,dvect3 box_max)
  {
    PDB.clear();
    SAMPLING_DISTANCE = SAMP;
    PHASER_ASSERT(SAMPLING_DISTANCE > 0);
   // double hex_distance = SAMPLING_DISTANCE*DEF_HEXFACTOR;
    //concatenate hexgrid points onto structure
    //see if they are buried
    for (int i = 0; i < 3; i++)
    {
      PHASER_ASSERT(box_min[i] < box_max[i]);
      box_min[i] -= SAMP; //extend past limits
      box_max[i] += SAMP; //extend past limits
    }
    dvect3 orthSite(0,0,0);
    dvect3 orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
    double off1 = 5.0/6.0;

    for (orthSite[2] = box_min[2]; orthSite[2] < box_max[2]; orthSite[2] += orthSamp[2])
    {
      off1 = 1. - off1;
      double off0 = 3.0/4.0;
      for (orthSite[1] = box_min[1] + off1 * orthSamp[1]; orthSite[1] < box_max[1]; orthSite[1] += orthSamp[1])
      {
        off0 = 1. - off0;
        for (orthSite[0] = box_min[0] + off0 * orthSamp[0]; orthSite[0] < box_max[0]; orthSite[0] += orthSamp[0])
        {
          PdbRecord card;
          card.AtomNum = PDB.size()+1;
          card.ResNum  = PDB.size()+1;
          card.AtomName  = " C  "; //default " CA " makes the atom look joined up in coot
          card.X = orthSite;
          PDB.push_back(card);
        }
      }
    }
    PHASER_ASSERT(PDB.size());
  }

  void
  Trace::select_overlapping(const Trace& OTHER,const sv_double& other_radii)
  {
    //this will be used to find out if hexgrid is in sphere
    using namespace mmtbx::geometry::asa::calculator;
    using namespace mmtbx::geometry::asa::calculator::utility;
    SimpleCalculator<
        TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
        sv_double >
    other_pdb(
        TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >( OTHER.PDB ),
        other_radii
        );

    double hexfactor = DEF_HEXFACTOR; //=0.577350269
    //don't include points further than the reference grid spacing away, otherwise can expand
    double sampling = std::min(OTHER.SAMPLING_DISTANCE,SAMPLING_DISTANCE);
    double hex_distance = sampling*hexfactor;
    for (auto& item : PDB)
    {
      item.valid_element = other_pdb.is_overlapping_sphere(item.X,hex_distance);
    }
    //std::cout << "select_overlap" << std::endl;
    erase_invalid();
  }

  int
  Trace::trim_surface(double surface_depth)
  {
    //now select only the ones that are buried
    double cumulative_sampling(0);
    //we need to take off the points to a depth of side chains
    int count(0);
    while (cumulative_sampling <= surface_depth) //at least one
    {
    double hex_distance = SAMPLING_DISTANCE*DEF_HEXFACTOR;
    double probe = hex_distance/20.;
    cumulative_sampling += SAMPLING_DISTANCE;
    count++;
    int    sampling_point_count = 960;
    PHASER_ASSERT(hex_distance);

    using namespace mmtbx::geometry::asa::calculator;
    using namespace mmtbx::geometry::asa::calculator::utility;
    ConstRadiusCalculator<
         TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >,
         double >
    mycalc(
         TransformedArray< std::vector<PdbRecord>, PdbRecord_getter >( PDB ),
         hex_distance,
         probe, // probe radius
         sampling_point_count,
         2*hex_distance+probe //this is required
        );

    sv_int counts(101,0);
    int a(0);
    double hexgrid_asa(0.25);
    for (auto& trace : PDB)
    {
      double accessible_surface_area = mycalc.accessible_points(a)/double(sampling_point_count);
      counts[int(accessible_surface_area*100)]++;
      trace.B = accessible_surface_area*100; //for coot output
      trace.valid_element = (accessible_surface_area < hexgrid_asa);
      a++;
    }

    //std::cout << "trim_surface" << std::endl;
    erase_invalid();
    }
    return count;
    //for (int a = 0; a < counts.size(); a++) std::cout << std::string("histogram asa=" + dtos(a/100.) + " num=" + itos(counts[a]) + std::string(a/100. > hexgrid_asa ? " erased" :"")) << std::endl;
  }

  af_string
  Trace::find_and_move_centre(UnitCell UC,sv_dvect3 possible_centres)
  {
    af_string output;
    PHASER_ASSERT(possible_centres.size());
    dvect3 best_centre(-999,-999,-999); //not really needed, could rely on principal_components
    for (auto second : { false, true }) //set is sorted, so loop is second
    {
      double lowest_max_radius = std::numeric_limits<double>::max();
      for (int c = 0; c < possible_centres.size(); c++)
      {
        dvect3 auto_centre = possible_centres[c];
        for (auto& a : PDB) //in place modification
        {
          dvect3 origaX = a.X; //copy original so starting from same point for comparison each time
          double mindistsqr = (origaX-auto_centre)*(origaX-auto_centre);
          dvect3 cellTrans_range(2,2,2); //guess
          dvect3 cellTrans_min(-cellTrans_range);
          dvect3 cellTrans_max(cellTrans_range);
          dvect3 cellTrans(0,0,0);
          for (cellTrans[0] = cellTrans_min[0]; cellTrans[0] <= cellTrans_max[0]; cellTrans[0]++)
          for (cellTrans[1] = cellTrans_min[1]; cellTrans[1] <= cellTrans_max[1]; cellTrans[1]++)
          for (cellTrans[2] = cellTrans_min[2]; cellTrans[2] <= cellTrans_max[2]; cellTrans[2]++)
          {
            dvect3 orthCellTrans;
            orthCellTrans[0] = cellTrans[0]*UC.A();
            orthCellTrans[1] = cellTrans[1]*UC.B();
            orthCellTrans[2] = cellTrans[2]*UC.C();
            dvect3 orth = origaX+orthCellTrans; //test with original
            double distsqr = (orth-auto_centre)*(orth-auto_centre);
            if (distsqr < mindistsqr-DEF_PPB) //prefer the original
            {
              a.X = orth; //store modification
              mindistsqr = distsqr;
            }
          }
        }
        set_principal_statistics();
        output.push_back("Possible centre " + dvtos(auto_centre));
        output.push_back("  Radius " + dtos(statistics.MAX_RADIUS));
        output.push_back("  Centre " + dvtos(statistics.CENTRE));
        if (statistics.MAX_RADIUS < lowest_max_radius) //or loop size is 1 second time round
        {
          output.push_back("   New Best");
          best_centre = statistics.CENTRE;
          lowest_max_radius = statistics.MAX_RADIUS;
        }
        else
        {
          output.push_back("   Not New Best");
        }
      }
      if (second)
      {
        output.push_back("Final state");
        return output; //because we are in correct configuration, with principal in correct state
      }
      else //first time through
      {
        possible_centres = { best_centre }; //repeat the loop with only the best
      }
    }
    return output;
  }

  void
  Trace::SetPdb(af_string pdblines)
  {
    Monostructure::SetPdb(pdblines);
    try
    {
      for (auto buffer : pdblines)
      {
        if (!buffer.find("REMARK " + constants::pdb_remark_number))
        {
          try
          {
            std::stringstream modelstream(buffer);
            std::string remark,remarktag,keyword;
            modelstream >> remark;
            modelstream >> remarktag;
            modelstream >> keyword;
            if (keyword == "SAMPLING")
            {
              modelstream >> SAMPLING_DISTANCE;
              if (SAMPLING_DISTANCE <= 0)
              pdb_errors.insert("SAMPLING not valid");
            }
            else if (keyword == "RADIUS")
            {
              modelstream >> MAX_RADIUS;
              if (MAX_RADIUS <= 0)
                pdb_errors.insert("MAX_RADIUS not valid");
            }
            else if (keyword == "CENTRE")
            {
              modelstream >> CENTRE[0];
              modelstream >> CENTRE[1];
              modelstream >> CENTRE[2];
            }
            else if (keyword == "PG_EULER")
            {
              dvect3 euler;
              modelstream >> euler[0];
              modelstream >> euler[1];
              modelstream >> euler[2];
              POINT_GROUP_EULER.push_back(euler);
            }
          }
          catch (std::stringstream::failure& e) { } //do nothing if parsing fails
        }
      }
    }
    catch (std::exception const& err)
    { }
    if (SAMPLING_DISTANCE == 0)
      throw Error(err::INPUT,"Sampling distance not set in trace");
  }

  void
  Trace::trim_incomplete_sidechains()
  {
    auto lastA = PDB[0];
    sv_int last_residue;
    table::protein_composition protein;
    for (int a = 0; a < PDB.size(); a++)
    {
      const PdbRecord& A = PDB[a];
      if (!protein.valid_residue(lastA.ResName))
      {
        lastA = A;
        continue;
      }
      if (A.Chain == lastA.Chain and A.ResNum == lastA.ResNum)
      {
        if (A.Element != "H")
          last_residue.push_back(a);
      }
      else
      {
        int nonh = protein.number_of_nonhydrogen(lastA.ResName);
        if (nonh != last_residue.size())
        {
          for (int aa = 0; aa < last_residue.size(); aa++)
          {
            int a_ = last_residue[aa];
            if (!PDB[a_].is_polyala())
            {
              PDB[a_].valid_element = false;
            }
          }
        }
        last_residue.resize(0);
        if (A.Element != "H")
          last_residue.push_back(a);
        lastA = A;
      }
    }
    erase_invalid();
  }

} //phasertng
