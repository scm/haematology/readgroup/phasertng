#include <cassert>
#include <limits>
#include <algorithm>
#include <scitbx/constants.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/alogchI0.h>
#include <phasertng/math/table/alogch.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::alogchI0& tbl_alogchI0;
extern const table::alogch& tbl_alogch;
}

using namespace scitbx;

namespace phasertng {

double pRiceF(double F1, double DF2, double V)
{
/* Rice (Sim/Srinivasan) probability distribution of amplitude F1, given
   the amplitude of its expected value, DF2
*/
  assert(F1 >= 0);
  assert(DF2 >= 0);
  assert(V > 0);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static double maxExpArg(std::log(std::numeric_limits<double>::max()));
  static double minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  double X(2*F1*DF2/V);
  double exparg(-fn::pow2(F1-DF2)/V);
  exparg = std::max(exparg,minExpArgHalf);
  return (2*F1/V)*std::exp(exparg)*tbl_ebesseli0.get(X);
}

double logRelRice(double F1, double DF2, double V)
{
/* Calculate log(Rice), omitting the constant terms omitted from WilsonRice.
   Suitable for LLG calculations, where only relative values needed.
   Assuming F1 is constant observed F, this form works for both
   amplitude and intensity-based likelihoods.
*/
  assert(F1 >= 0. && DF2 >= 0. && V > 0.);
  double X(2*F1*DF2/V);
  return tbl_alogchI0.get(X)-(std::log(V)+(fn::pow2(F1)+fn::pow2(DF2))/V);
}

double pRiceI(double I1, double D2I2, double V)
{
/* Rice (Sim/Srinivasan) probability distribution of intensity I1, given
   the square of the expected value of the corresponding structure factor
*/
  assert(I1 >= 0);
  assert(D2I2 >= 0);
  assert(V > 0);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static double maxExpArg(std::log(std::numeric_limits<double>::max()));
  static double minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  double F1(std::sqrt(I1)),DF2(std::sqrt(D2I2));
  double X(2*F1*DF2/V);
  double exparg(-fn::pow2(F1-DF2)/V);
  return (exparg > minExpArgHalf) ? (1.0/V)*std::exp(exparg)*tbl_ebesseli0.get(X) : 0.;
}

double pWoolfsonF(double F1, double DF2, double V)
{
// Centric (Woolfson) equivalent of pRiceF
  assert(F1 >= 0. && DF2 >= 0. && V > 0.);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static double maxExpArg(std::log(std::numeric_limits<double>::max()));
  static double minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  double X(F1*DF2/V);
  double eCoshX = (X < 15.0) ? std::exp(-X)*cosh(X) : 0.5;
  double prob(eCoshX/std::sqrt(V*scitbx::constants::pi_2));
  double exparg = -fn::pow2(F1-DF2)/(2*V);
  return (exparg > minExpArgHalf) ? prob*std::exp(exparg) : 0.;
}

double logRelWoolfson(double F1, double DF2, double V)
{
// Centric equivalent to logRelRice
  assert(F1 >= 0. && DF2 >= 0. && V > 0.);
  double X(F1*DF2/V),TWO(2);
  return tbl_alogch.get(X)-(std::log(V)+(fn::pow2(F1)+fn::pow2(DF2))/V)/TWO;
}

double pWoolfsonI(double I1, double D2I2, double V)
{
// Centric (Woolfson) equivalent of pRiceI
  assert(I1 > 0. && D2I2 >= 0. && V > 0.);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static double maxExpArg(std::log(std::numeric_limits<double>::max()));
  static double minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  double F1 = std::sqrt(I1);
  double DF2 = std::sqrt(D2I2);
  double X(F1*DF2/V);
  double eCoshX = (X < 15.0) ? std::exp(-X)*cosh(X) : 0.5;
  double prob(eCoshX/(F1*std::sqrt(V*scitbx::constants::two_pi)));
  double exparg = -fn::pow2(F1-DF2)/(2*V);
  return (exparg > minExpArgHalf) ? prob*std::exp(exparg) : 0.;
}

} //phasertng
