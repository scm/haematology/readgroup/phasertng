#include <phasertng/math/likelihood/mapELLG.h>
#include <phasertng/main/Assert.h>
#include <scitbx/array_family/misc_functions.h>
#include <cmath>
#include <algorithm>

namespace phasertng {
namespace likelihood {

  double mapELLG::get(double Emean, double DobsSigA, bool cent)
  {
    phaser_assert(!cent); // Implement later with map symmetry
    return cent ?centric(Emean,DobsSigA) :acentric(Emean,DobsSigA);
  }

  double mapELLG::centric(double Emean, double DobsSigA)
  {
    double eLLG(0.);
    return eLLG;
  }

  double mapELLG::acentric(double Emean, double DobsSigA)
  {
    double eLLG;
    double dosaSqr(scitbx::fn::pow2(DobsSigA));
    dosaSqr = std::min(0.9999,dosaSqr);
    double V(1.-dosaSqr);
    eLLG = -std::log(V);
    // The following term may be unnecessary and should be zero on average
    eLLG += (scitbx::fn::pow2(Emean)-1.)*dosaSqr;
    return eLLG;
  }

}
}
