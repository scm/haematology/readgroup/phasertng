//(c) 2000-2024 Cambridge University Technical Services Ltd
#ifndef __phasertng_mintbx_Optional_class__
#define __phasertng_mintbx_Optional_class__
#include <phasertng/dtmin/includes.h>

namespace phasertng {
namespace dtmin {

class Optional
{
  public:
    Optional() { }
    virtual ~Optional() {}

  public:
    virtual af_string
    reject_outliers()
    { return af_string(); }

    virtual std::vector<dtmin::Bounds>
    bounds()
    { return std::vector<dtmin::Bounds>(0); }

    virtual std::vector<dtmin::Reparams>
    reparameterize()
    { return std::vector<dtmin::Reparams>(0); }

    virtual af_string
    setup_parameters()
    { return af_string(); }

    virtual af_string
    cleanup_parameters()
    { return af_string(); }

    virtual af_string
    finalize_parameters()
    { return af_string(); }

    virtual double
    maximum_distance_special(
      sv_double&,
      sv_double&,
      sv_bool&,
      sv_double&,
      double&)
    { return 0.; }
};

}} //phasertng

#endif
