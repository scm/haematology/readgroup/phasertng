//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Preprocessor.h>
#include <phasertng/main/QuotedString.h>
#include <phasertng/main/hoist.h>
#include <phasertng/enum/end_keys.h>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <cctype>
#include <set>

namespace phasertng {

#if defined(__linux)

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

int input_timeout (int filedes, unsigned int seconds)
{
  fd_set set;
  struct timeval timeout;

  // Initialize the file descriptor set.
  FD_ZERO (&set);
  FD_SET (filedes, &set);

  // Initialize the timeout data structure.
  timeout.tv_sec = seconds;
  timeout.tv_usec = 0;

  // select returns 0 if timeout, 1 if input available, -1 if error.
  return TEMP_FAILURE_RETRY (select (FD_SETSIZE, &set, NULL, NULL, &timeout));
}
#endif

void Preprocessor::stdio()
{
#if defined(__linux)
 // if (isatty(fileno(stdin)))
  //if (isatty(STDIN_FILENO))
  if (input_timeout (STDIN_FILENO, 0.1) == 0)
#endif
  {
    std::cout << "Enter keyword input:" << std::endl;
  }

  recursive_processor(std::cin);
}

void Preprocessor::recursive_processor(std::istream& input_stream,int counter)
{
  counter++;
  while (!input_stream.eof() && counter)
  {
    std::string new_line;
    std::getline(input_stream, new_line,'\n');
    new_line += "\n";
    std::istringstream line_stream(const_cast<char*>(new_line.c_str()));
    char ch;
    do {
         if (!line_stream.get(ch))
         break;
    } while ((std::isspace)(ch) || ch == '\n'); //skip space, tabs, feedforms
    if (ch == '@')
    {
      Cards += new_line;
      std::string filename = getQuotedString(line_stream); //take next white space separated (quoted) string
      std::ifstream file_stream(const_cast<char*>(filename.c_str()));
      if (filename.size() && !file_stream)
      {
        *(static_cast<Error *>(this)) = Error(err::STDIN,"@ \"" + filename+"\"");
        throw(*this);
      }
      else recursive_processor(file_stream,counter);
    }
    else
    {
      line_stream.putback(ch);
      Cards += new_line;
      std::string keyword;
      line_stream >> keyword; //remove leading white spaces
      hoist::to_upper(keyword);
      std::set<std::string> end_keys;
      for (int i = 0; i < End::KeysCount; i++)
        end_keys.insert(End::Keys2String(static_cast<End::Keys>(i))); //defined as upper
      if (end_keys.count(keyword))
      {
        if (counter==1) counter--;
//special case for END at the command line (flagged by counter=1)
//forces while loop condition fail (counter=0) and exit from command line
        break;
      }
    }
  }
  counter--;
}

std::string Preprocessor::getQuotedString(std::istringstream& input_stream)
{
  QuotedString Quoted(input_stream);
  if (Quoted.error)
  {
    *(static_cast<Error *>(this)) = Error(err::STDIN,"@ \"<file>\" file not found (not quoted?)");
    throw(*this);
  }
  return Quoted.string_value;
}

void Preprocessor::stream(std::istream& input_stream)
{
  recursive_processor(input_stream);
}

void Preprocessor::keywords(std::string str)
{
  Cards = str;
}

std::string Preprocessor::cards()
{
//in-place removal of blank lines in Cards
  size_t pos(0);
  while (pos < Cards.size())
  {
    size_t pos_return = Cards.find('\n',pos);
    std::string new_line = Cards.substr(pos,pos_return-pos);
    bool blank_line(true);
    for (int i = 0; i < new_line.size(); i++)
      if (!(std::isspace)(new_line[i])) blank_line = false;
    if (blank_line) Cards.erase(pos,pos_return-pos+1);
    else pos = pos_return+1;
  }
  return Cards;
}

}
