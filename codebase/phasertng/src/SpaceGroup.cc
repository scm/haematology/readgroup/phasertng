//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <cctbx/sgtbx/seminvariant.h>
#include <set>

namespace phasertng {

  SpaceGroup::SpaceGroup(const std::string symbol_)
  {
    std::string symbol = symbol_;
    phaser_assert(symbol.size());
    try {
      hoist::replace_all(symbol,"\\\"","\""); //remove escape from escape"
     // Hermann-Mauguin symbols A1983
     // Schoenflies symbols
     // Hall symbols are entered by prepending 'Hall: '
     // e.g. "Hall: C 2y (x,y,-x+z)"
     // everything gets converted to hall symbol and then to space group type
      cctbx::sgtbx::space_group_symbols Symbol(symbol);
      cctbx::sgtbx::space_group Sg(Symbol.hall());
    //  cctbx::sgtbx::space_group_type cctbxSG_type = Sg.type();
     // cctbx::sgtbx::space_group cctbxSG = cctbxSG.group();
      cctbxSG_type = Sg.type();
      cctbxSG = cctbxSG_type.group();
      //cctbx::sgtbx::space_group_type::operator=(Sg.type());
      //*(static_cast<cctbx::sgtbx::space_group_type*>(this)) = Sg.type();
      HALL = Symbol.hall(); //(x,y,z) added with init function below
      UHMS = Symbol.hermann_mauguin();
      if (!UHMS.size()) //this may not do anything more than above lookup
      {
        UHMS = cctbxSG.match_tabulated_settings().universal_hermann_mauguin();
      }
      if (!UHMS.size()) //build the symbol from scratch
      {
        UHMS = cctbxSG_type.universal_hermann_mauguin_symbol(); //build the symbol
        STANDARD = false;
      }
      CCP4 = ""; //this is the nickname for the space group, for logging : no setting info
      for (int t = 0; t < UHMS.size(); t++)
      { //split at "("
        if (UHMS[t] == '(') break;
        CCP4 += UHMS[t];
      }
      hoist::algorithm::trim(CCP4);
           if (UHMS.find("R 3 :H (") != std::string::npos) CCP4 = "R 3";  //R 3 :H (-y+z,x+z,-x+y+z)
      else if (UHMS.find("R 3 2 :H (") != std::string::npos) CCP4 = "R 3 2";  //change of basis in brackets
      else if (UHMS.find("R 3 :R") != std::string::npos) CCP4 = "R 3";
      else if (UHMS.find("R 3 2 :R") != std::string::npos) CCP4 = "R 3 2";
      else if (UHMS.find("R 3 :H") != std::string::npos) CCP4 = "H 3";
      else if (UHMS.find("R 3 2 :H") != std::string::npos) CCP4 = "H 3 2";
    }
    catch (...)
    { //CCP4 style
      CCP4 = symbol;
      hoist::algorithm::trim(CCP4);
      try {
        if (symbol == "R3") CCP4 = "R 3 :R";
        if (symbol == "R 3") CCP4 = "R 3 :R";
        if (symbol == "R32") CCP4 = "R 3 2 :R";
        if (symbol == "R 32") CCP4 = "R 3 2 :R";
        if (symbol == "R 3 2") CCP4 = "R 3 2 :R";
        if (symbol == "H3") CCP4 = "R 3 :H";
        if (symbol == "H 3") CCP4 = "R 3 :H";
        if (symbol == "H32") CCP4 = "R 3 2 :H";
        if (symbol == "H 32") CCP4 = "R 3 2 :H";
        if (symbol == "H 3 2") CCP4 = "R 3 2 :H";
        cctbx::sgtbx::space_group_symbols Symbols(CCP4,"A1983");
        if (!Symbols.is_valid())
        {
          throw Error(err::FATAL,"Invalid space group (" + symbol + ")");
        }
        HALL = Symbols.hall();
        CCP4 = Symbols.universal_hermann_mauguin();
        hoist::algorithm::trim(CCP4);
        UHMS = CCP4;
      }
      catch (...)
      { throw Error(err::FATAL,"Error setting space group (" + symbol + ")"); }
    }
    init();
  }

  const cctbx::sgtbx::space_group&
  SpaceGroup::group() const
  {
 //   return cctbx::sgtbx::space_group(HALL);
    return cctbxSG;
  }

  const cctbx::sgtbx::space_group_type&
  SpaceGroup::type() const
  {
    return cctbxSG_type;
  }

  void SpaceGroup::init()
  {
    try {
      std::string hall_xyz = HALL;
      if (!hoist::algorithm::contains(HALL,"("))
         hall_xyz += " (x,y,z)"; //add the explicit default setting
      cctbx::sgtbx::space_group_symbols tmp(hall_xyz);
      HALL = hall_xyz; //did not throw
    }
    catch(...) {} //ignore
    hoist::algorithm::trim(HALL);
    if (!hoist::algorithm::contains(HALL,"Hall")) HALL = "Hall: " + HALL;
    const cctbx::sgtbx::space_group& cctbxSG = group();
    symFacPCIF = cctbxSG.order_z()/cctbxSG.order_p();
    phaser_assert(CCP4.size());
    phaser_assert(UHMS.size());
    //make space group C 1 if required
    hoist::algorithm::trim(CCP4);
    if (false and CCP4 == "P 1" && cctbxSG.conventional_centring_type_symbol() != 'R')
    {
      //AJM this may not be right, may break something, possibly only change HMTAB
      CCP4[0] = cctbxSG.conventional_centring_type_symbol();
    }

    int order_z = group().order_z();
    trasym.resize(order_z);
    rotsym.resize(order_z);
    for (int isym = 0; isym < order_z; isym++)
    {
      for (int i = 0; i < 3; i++)
      {
        trasym[isym][i] = static_cast<double>(cctbxSG.operator()(isym).t()[i])/
                                             cctbxSG.operator()(isym).t().den();
        for (int j = 0; j < 3; j++)
          rotsym[isym](i,j) = static_cast<double>(cctbxSG.operator()(isym).r()(i,j))/
                                             cctbxSG.operator()(isym).r().den();
      }
      rotsym[isym] = rotsym[isym].transpose();
    }
    SYMFAC = order_z*order_z/cctbxSG.order_p();
    NSYMP = cctbxSG.order_p();
  }

  dvect3
  SpaceGroup::doSymXYZ(const int& isym, const dvect3& v)  const
  { return rotsym[isym].transpose()*v + trasym[isym]; }

  int SpaceGroup::orderX() const
  {
    int epsn(0);
    dvect3 xaxis(1,0,0);
    for (int isym = 0; isym < NSYMP; isym++) //primitive ops only
      if (xaxis == rotsym[isym]*xaxis)
        epsn++;
    return epsn;
  }

  int
  SpaceGroup::orderY() const
  {
    int epsn(0);
    dvect3 yaxis(0,1,0);
    for (int isym = 0; isym < NSYMP; isym++) //primitive ops only
      if (yaxis == rotsym[isym]*yaxis)
        epsn++ ;
    return epsn;
  }

  int
  SpaceGroup::orderZ() const
  {
    int epsn(0);
    dvect3 zaxis(0,0,1);
    for (int isym = 0; isym < NSYMP; isym++) //primitive ops only
      if (zaxis == rotsym[isym]*zaxis)
        epsn++ ;
    return epsn;
  }

  std::pair<int,char>
  SpaceGroup::highOrderAxis() const
  {
    std::set<std::pair <int,char> > tmp;
    tmp.insert({orderX(),'X'});
    tmp.insert({orderY(),'Y'});
    tmp.insert({orderZ(),'Z'});
    return *tmp.rbegin();
  }

  int
  SpaceGroup::lowOrder() const
  {
    std::vector<int> tmp = {orderX(),orderY(),orderY()};
    std::sort(tmp.begin(),tmp.end()); //sort low to high
    //test could be sophisticated, lowest common symmetry etc, but below is good for 222/422/622
    if (tmp[0] == tmp[1]) //lowest two values
      return tmp[0]; //lowest
    return 1;
  }

  std::string
  SpaceGroup::pgname() const
  {
    cctbx::sgtbx::space_group PG(group().build_derived_point_group());
    std::string Point_Group("");
    cctbx::sgtbx::space_group_type PGtype = PG.type();
    for (int i = 1; i < PGtype.lookup_symbol().size(); i++)
      if (!(std::isspace)(PGtype.lookup_symbol()[i]))
        Point_Group += (std::toupper)(PGtype.lookup_symbol()[i]);
    return Point_Group;
  }

  std::string
  SpaceGroup::pointgroup() const
  {
    std::string Point_Group = pgname();
    if (Point_Group.size() and hoist::algorithm::ends_with(Point_Group,"22"))
      return "D" + std::string(1,Point_Group[0]);
    return "C" + Point_Group;
  }

  char
  SpaceGroup::spcgrpcentring() const
  { //fudge for cctbx
    std::string Extended_Hermann_Mauguin = type().universal_hermann_mauguin_symbol();
    if (Extended_Hermann_Mauguin.find("R 3 :H (") != std::string::npos) return 'R';
    if (Extended_Hermann_Mauguin.find("R 3 2 :H (") != std::string::npos) return 'R';
    if (Extended_Hermann_Mauguin.find("R 3 :H") != std::string::npos) return 'H';
    if (Extended_Hermann_Mauguin.find("R 3 2 :H") != std::string::npos) return 'H';
    return group().conventional_centring_type_symbol();
  }

  bool
  SpaceGroup::is_polar() const
  {
    cctbx::sgtbx::structure_seminvariants seminvariants(group());
    af::small<cctbx::sgtbx::ss_vec_mod, 3> const &vm = seminvariants.vectors_and_moduli();
    for (int i = 0; i < vm.size(); ++i) {
      if (vm[i].m != 0) continue;
      // allowed continuous origin shift:
      return true;
    }
    return false;
  }

  void
  SpaceGroup::symmetry_hkl_primitive(
      const millnx& miller,
      const int eps,
      pod::rsymhkl& r //modify in place
      ) const
  {
    if (eps == 1)
    { //optimization
      r.unique = NSYMP;
      for (int isym = 0; isym < NSYMP; isym++)
      {
        r.rotMiller[isym] = rotsym[isym]*miller;
        r.traMiller[isym] = trasym[isym]*miller;
      }
    }
    else
    {
      r.rotMiller[0] = rotsym[0]*miller;
      r.traMiller[0] = trasym[0]*miller;
      r.unique = 1;
      for (int isym = 1; isym < NSYMP; isym++)
      {
        const millnx rot = rotsym[isym]*miller;
        auto rotMiller_end = r.rotMiller.begin()+r.unique;
        if (std::find(r.rotMiller.begin(),rotMiller_end,rot) == rotMiller_end)
        {
          r.rotMiller[r.unique] = rot;
          r.traMiller[r.unique] = trasym[isym]*miller;
          r.unique++;
        }
      }
    }
  }

  af::shared< std::pair<millnx,double> >
  SpaceGroup::symmetry_hkl_centred(const millnx& miller) const
  {
    af::shared< std::pair<millnx,double> > SYMMETRY;
    for (int isym = 0; isym < rotsym.size(); isym++)
    {
      const millnx rotMiller = rotsym[isym]*miller;
      const double traMiller = trasym[isym]*miller;
      std::pair<millnx,double> paired(rotMiller,traMiller);
      if (std::find(SYMMETRY.begin(),SYMMETRY.end(),paired) == SYMMETRY.end())
      {
        SYMMETRY.push_back(paired); //multiply by 2 pi externally
      }
    }
    //AJM phaser_assert(SYMMETRY.size()/order_n() == epsilon(miller));
    return SYMMETRY;
  }

  af_dvect3
  SpaceGroup::symmetry_xyz_cell(const dvect3& SITE,const int ncells,const bool exclude_no_shift) const
  {
    const ivect3 ncells_vector(ncells,ncells,ncells);
    return symmetry_xyz_cell(SITE,ncells_vector,exclude_no_shift);
  }

  af_dvect3
  SpaceGroup::symmetry_xyz_cell(const dvect3& SITE,const ivect3 ncells,const bool exclude_no_shift) const
  {
    //make an array of the symmetry related sites and their cell translations by +/- ncells
    //include the no_shift position by default,
    //but optionally allow its exclusion, as it may be explicitly handled elsewhere
    af_dvect3 SYMMETRY;
    dvect3 cellTrans(0,0,0);
    for (int isym = 0; isym < rotsym.size(); isym++)
    {
      dvect3 sym_site = rotsym[isym].transpose()*SITE + trasym[isym];
      for (cellTrans[0] = -ncells[0]; cellTrans[0] <= ncells[0]; cellTrans[0]++)
      for (cellTrans[1] = -ncells[1]; cellTrans[1] <= ncells[1]; cellTrans[1]++)
      for (cellTrans[2] = -ncells[2]; cellTrans[2] <= ncells[2]; cellTrans[2]++)
      if (!exclude_no_shift or (exclude_no_shift and !(isym == 0 and cellTrans == dvect3(0,0,0))))
      {
        SYMMETRY.push_back(sym_site + cellTrans);
      }
    }
    //remainder of size divided by nsym is 0 (modulus)
    //checks that no extras are produced accidentally
    if (!exclude_no_shift)
      PHASER_ASSERT(SYMMETRY.size()%rotsym.size() == 0);
    return SYMMETRY;
  }

  std::pair<int,dvect3>
  SpaceGroup::symmetry_xyz_cell(int find_isym,const int ncell,bool exclude_no_shift) const
  {
    //find the symmetry and translation corresponding to an isym from above return function
    //code duplication for simplicity
    int nsymmetry(0);
    dvect3 cellTrans(0,0,0);
    const ivect3 ncells(ncell,ncell,ncell);
    for (int isym = 0; isym < rotsym.size(); isym++)
    {
      for (cellTrans[0] = -ncells[0]; cellTrans[0] <= ncells[0]; cellTrans[0]++)
      for (cellTrans[1] = -ncells[1]; cellTrans[1] <= ncells[1]; cellTrans[1]++)
      for (cellTrans[2] = -ncells[2]; cellTrans[2] <= ncells[2]; cellTrans[2]++)
      if (!exclude_no_shift or (exclude_no_shift and !(isym == 0 and cellTrans == dvect3(0,0,0))))
      { //exclude_no_shift default false
        if (nsymmetry == find_isym) return { isym,cellTrans };
        nsymmetry++;
      }
    }
    return { nsymmetry,cellTrans };
  }

  std::vector< std::pair< dmat33, dvect3 > >
  SpaceGroup::symmetry_xyz_cell_operators(const int ncells, const bool exclude_no_shift) const
  {
    const ivect3 ncells_vector(ncells,ncells,ncells);
    return symmetry_xyz_cell_operators(ncells_vector,exclude_no_shift);
  }

  std::vector< std::pair< dmat33, dvect3 > >
  SpaceGroup::symmetry_xyz_cell_operators(const ivect3 ncells, const bool exclude_no_shift) const
  {
    // return a vector of the symmetry operators as a (rot matrix, tra vector) pair
    // apply to a fractional coordinate like so: sym_point = rot * point + vec
    std::vector< std::pair< dmat33, dvect3 > > SYMMETRY_OPS;
    SYMMETRY_OPS.reserve(rotsym.size() * (2*ncells[0] + 1) * (2*ncells[1] + 1) * (2*ncells[2] + 1));
    dvect3 cellTrans(0,0,0);
    for (int isym = 0; isym < rotsym.size(); isym++)
    {
      for (cellTrans[0] = -ncells[0]; cellTrans[0] <= ncells[0]; cellTrans[0]++)
      for (cellTrans[1] = -ncells[1]; cellTrans[1] <= ncells[1]; cellTrans[1]++)
      for (cellTrans[2] = -ncells[2]; cellTrans[2] <= ncells[2]; cellTrans[2]++)
      if (!exclude_no_shift or (exclude_no_shift and !(isym == 0 and cellTrans == dvect3(0,0,0))))
      {
        SYMMETRY_OPS.push_back( std::make_pair(rotsym[isym].transpose(), trasym[isym] + cellTrans) );
      }
    }
    //remainder of size divided by nsym is 0 (modulus)
    //checks that no extras are produced accidentally
    if (!exclude_no_shift)
      PHASER_ASSERT(SYMMETRY_OPS.size()%rotsym.size() == 0);
    return SYMMETRY_OPS;
  }


  SpaceGroup
  SpaceGroup::enantiomorph() const
  {
    cctbx::sgtbx::change_of_basis_op cb_op = type().change_of_hand_op();
    cctbx::sgtbx::space_group enantiomorph_sg = group().change_basis(cb_op);
    return SpaceGroup("Hall: " + enantiomorph_sg.type().hall_symbol());
  }

  bool        SpaceGroup::is_p1() const { return rotsym.size() == 1; }

  std::string SpaceGroup::sstr() const
  {
    std::string str = CCP4;
    std::string::iterator end_pos = std::remove(str.begin(), str.end(), ' ');
    str.erase(end_pos, str.end());
    return str;
  }

}//phasertng
