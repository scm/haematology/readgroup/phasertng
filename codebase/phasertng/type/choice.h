//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_choice_class__
#define __phasertng_choice_class__
#include <phasertng/type/optional.h>
#include <set>

namespace phasertng {
namespace type {

class choice : public optional
{
  protected:
  sv_string  choices; //lower case choices
  sv_string  CHOICES; //upper case choices
  sv_string  select;
  sv_string  dephalt;

  public:
  choice() : optional() {}
  choice(
      std::string parameter,
      std::string possibilities,
      std::string choice_default = ""
  );
  ~choice() {} //required for link

  void set_value(std::string selection);
  void set_value(const sv_string& vcards,std::string& selection);
  bool is_default() const;
  void set_default();
  std::set<std::string> value_or_default_multi_set() const;
  sv_string value_or_default_multi() const;
  bool check_value(std::string pick) const;
  std::string value_or_default_multi_cat() const;
  std::string value_or_default() const;
  std::string value_or_default_upper() const;
  bool value_or_default_equals(const std::string tester) const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);
  sv_string   all_choices() { return choices; }

  bool operator==(const type::choice& other) const
  { return value_or_default() == other.value_or_default(); }

  private:
   std::string choice2upper(std::string) const;
};

}}
#endif
