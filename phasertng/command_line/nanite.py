# LIBTBX_SET_DISPATCHER_NAME phasertng.nanite

from iotbx.cli_parser import run_program
from phasertng.programs import nanite

if (__name__ == '__main__'):
  try:
    run_program(nanite.Program)
  except Exception as e:
    print(str(e))
