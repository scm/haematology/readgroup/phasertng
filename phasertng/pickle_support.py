from __future__ import division
import phasertng

def Phasertng_getinitargs(self):

    return ( self.suite )

def Reflections_getinitargs(self):

    return ( self.get_history(), self.get_hall(), self.get_cell(), self.WAVELENGTH, self.MILLER, self.get_map_flt(), self.get_map_int(), self.get_map_bln(), self.get_map_fdl() )

def moderunner_getstate(self):

    state = {}
    state[ "pathway_string" ] =  self.get_pathway_string()
    state[ "pathway_tag" ] =  self.get_pathway_tag()
    state[ "pathlog_string" ] =  self.get_pathlog_string()
    state[ "pathlog_tag" ] =  self.get_pathlog_tag()
    state[ "subdir" ] =  self.get_subdir()
    state[ "dag_cards_filename" ] =  self.get_dag_cards_filename()
    state[ "tng_cards_filename" ] =  self.get_tng_cards_filename()
    state[ "size" ] =  self.get_size()
    state[ "top_tfz" ] =  self.get_top_tfz()
    state[ "polarity" ] =  self.get_polarity()
    state[ "number_of_poses" ] =  self.get_number_of_poses()
    state[ "tncs_info" ] =  self.get_tncs_info()
    state[ "sg_info" ] =  self.get_sg_info()
    state[ "ccs" ] =  self.get_ccs()
    state[ "current_seeks" ] =  list(self.get_current_seeks())
    state[ "message" ] =  self.get_message()
    state[ "restart_at_cell_content_scaling" ] =  self.get_restart_at_cell_content_scaling()
    state[ "dagcards" ] =  self.get_dagcards()
    state[ "tngcards" ] =  self.get_tngcards()
    return state


def moderunner_setstate(self, state):

    self.set_pathway_string(state[ "pathway_string" ])
    self.set_pathway_tag(state[ "pathway_tag" ])
    self.set_pathlog_string(state[ "pathlog_string" ])
    self.set_pathlog_tag(state[ "pathlog_tag" ])
    self.set_subdir(state[ "subdir" ])
    self.set_dag_cards_filename(state[ "dag_cards_filename" ])
    self.set_tng_cards_filename(state[ "tng_cards_filename" ])
    self.set_size(state[ "size" ])
    self.set_top_tfz(state[ "top_tfz" ])
    self.set_polarity(state[ "polarity" ])
    self.set_number_of_poses(state[ "number_of_poses" ])
    self.set_tncs_info(state[ "tncs_info" ])
    self.set_sg_info(state[ "sg_info" ])
    self.set_ccs(state[ "ccs" ])
    self.set_current_seeks(state[ "current_seeks" ])
    self.set_message(state[ "message" ])
    self.set_dagcards(state[ "dagcards" ])
    self.set_tngcards(state[ "tngcards" ])

GETINITARGS_FOR = {
    phasertng.Phasertng: Phasertng_getinitargs,
    phasertng.Reflections: Reflections_getinitargs,
    }

#alternative pickle support (alt to init)
GET_SET_STATE_FOR = {
    phasertng.moderunner: ( moderunner_getstate, moderunner_setstate ),
    }

def enable():

    for ( phasertng_object, getinitargs_method ) in GETINITARGS_FOR.items():
        phasertng_object.__getinitargs__ = getinitargs_method

    for ( phasertng_object, ( getstate_method, setstate_method ) ) in GET_SET_STATE_FOR.items():
        phasertng_object.__getstate__ = getstate_method
        phasertng_object.__setstate__ = setstate_method

#code below fixes the problem with
#("Can't pickle <type 'Boost.Python.enum'>: it's not found as Boost.Python.enum",)
def isEnumType(o):
    return isinstance(o, type) and issubclass(o,int) and not (o is int)

def _tuple2enum(enum, value):
    enum = getattr(phasertng, enum)
    e = enum.values.get(value,None)
    if e is None:
        e = enum(value)
    return e

def _registerEnumPicklers():
    import sys
    if sys.version_info[0] == 2:
      from copy_reg import constructor, pickle
    else:
      from copyreg import constructor, pickle
    def reduce_enum(e):
        enum = type(e).__name__.split('.')[-1]
        return ( _tuple2enum, ( enum, int(e) ) )
    constructor( _tuple2enum)
   #for e in [ e for e in vars(phasertng).itervalues() if isEnumType(e) ]:
    for e in [ e for e in vars(phasertng).values() if isEnumType(e) ]:
        pickle(e, reduce_enum)

_registerEnumPicklers()
