//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Elmn.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/constants.h>
#include <phasertng/math/table/lnfactorial.h>
#include <future>

#define PHASERTNG_DEBUG_OLD_SPHBESSEL
#ifdef PHASERTNG_DEBUG_OLD_SPHBESSEL
//old spherical bessel seems to be needed with libtbx.valgrind, boost version generates nan (why?)
#include <phasertng/math/table/sphbessel.h>
#else
#include <boost/math/special_functions/bessel.hpp>
#endif

namespace phasertng {
extern const table::lnfactorial& tbl_lnfac;

  sv_cmplex Elmn::partial_elmn_calculation(
      const HKL_clustered& partial_HKL_list,
      const int TWOPIB,
      const std::pair<int,char> HIGHSYMM
      )
  {
    //calculate the elmn contributions from a subset of the reflections

    const double ZERO(0.),ONE(1.);
    int lmax = LMAX; //local copy on thread
    int PATZ = HIGHSYMM.first; //local copy on thread

    phaser_assert(NELMN>0);
    phaser_assert(NSPHARM>0);
    sv_cmplex elmn(NELMN,cmplex(0,0)); //default constructor for std::complex<T> is 0,0
    sv_double spharmfn(NSPHARM,0);

#ifdef PHASERTNG_DEBUG_OLD_SPHBESSEL
    table::sphbessel sphbessel(lmax);
#endif

    phaser_assert(partial_HKL_list.clustered.size());

    for (int c = 0; c < partial_HKL_list.clustered.size(); c++)
    {
      auto& partial_HKL_list_clustered_c = partial_HKL_list.clustered[c];
      {{
      phaser_assert(partial_HKL_list_clustered_c.size());
      // double x = std::cos(HKL_list.clustered[c][0].theta);
      // double sqRoot = std::sqrt(std::max(0.0,1.0-x*x));
      cmplex cos_sin = std::exp(cmplex(0.,partial_HKL_list_clustered_c[0].theta));
      double x = std::real(cos_sin);
      double sintheta = std::imag(cos_sin); // Previously sqRoot
      //double Pmj(0),Pmm(0),Pmm1(0);
      double Pmj(0),Pmm1(0);
      int i(0);
      for (int m = 0; m <= lmax; m++)
      {
        if (m%PATZ != 0) continue;
        double Pmm_j(0),Pmm1_j(0);
        double Pmm(1);
        if (m>0) {
          int odd_fact=1;
          for (int i=1; i<=m; i++) {
            // Pmm *= -sqRoot*odd_fact;
            Pmm *= -sintheta*odd_fact;
            odd_fact += 2;
          }
        }
        for (int l = m; l <= lmax; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          auto absm = scitbx::fn::absolute(m);
          double expterm(0.5*(tbl_lnfac.get(l-absm)-tbl_lnfac.get(l+absm)));
          double scale = tbl_sphbessel.normalization[l] * std::exp(expterm);
          if (Pmm_j)
          {
            Pmj = (x*(2*l-3)*Pmm1_j-(l+m-2)*Pmm_j)/(l-1-m);
            Pmm = Pmm1_j;
            Pmm1 = Pmj;
            Pmj = (x*(2*l-1)*Pmm1-(l+m-1)*Pmm)/(l-m);
            Pmm_j = Pmm1;
            Pmm1_j = Pmj;
          }
          else if (l==m)
          {
            Pmj = Pmm;
          }
          else if (l==m+1)
          {
            Pmm1=x*(2*m+1)*Pmm;
            Pmj = Pmm1;
          }
          else
          {
            Pmm1=x*(2*m+1)*Pmm;
            for (int j=m+2; j<=l; j++) {
              Pmj = (x*(2*j-1)*Pmm1-(j+m-1)*Pmm)/(j-m);
              Pmm = Pmm1;
              Pmm1 = Pmj;
            }
            Pmm_j = Pmm;
            Pmm1_j = Pmm1;
          }
          spharmfn[i++] = (scale*Pmj);
        } // l loop
      } // m loop
      }}

      for (int r = 0; r < partial_HKL_list_clustered_c.size(); r++)
      {
        int i(0),n_elmn(0);
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
        auto& htp = partial_HKL_list_clustered_c[r];
        double intensity = htp.intensity;
        double sign = (htp.flipped) ?  2 : 1;
        intensity *= sign; // to take into account contribution from -h terms
#else
        auto& htp = partial_HKL_list_clustered_c[r];
        double intensity = htp.intensity;
        intensity *= 2; // to take into account contribution from -h terms
#endif

        double h(TWOPIB*htp.r);
        sv_double besselx(lmax+2);
        //forward recurrence also computes odd but they are cheap
        sv_double sph_bessel = tbl_sphbessel.forward_recurrance(lmax+2,h);
        for (int u = 3; u<sph_bessel.size(); u+=2)
        {
          besselx[u] = tbl_sphbessel.sqrt_table[u]*sph_bessel[u]/h;
        }
        for (int u = sph_bessel.size(); u<lmax+2; u+=2)
        {
#ifdef PHASERTNG_DEBUG_OLD_SPHBESSEL
          besselx[u] = tbl_sphbessel.sqrt_table[u]*sphbessel.get(u,h)/h;
#else
          besselx[u] = tbl_sphbessel.sqrt_table[u]*boost::math::sph_bessel(u,h)/h;
#endif
        }

        cmplex phi_phase_prev = ONE;
        cmplex phi_phase_1 = std::exp(cmplex(ZERO,ONE)*htp.phi);
        for (int m = 0; m <= lmax; m++)
        {
          cmplex phi_phase_m = m ? phi_phase_prev * phi_phase_1 : phi_phase_prev;
          phi_phase_prev = phi_phase_m;
          if (m%PATZ != 0) continue;
          for (int l = m; l <= lmax; l += 2)
          {
            if (l%2 == 1) l++;
            if (l < 2) continue;
            //phaser_assert(i < spharmfn.size());
            cmplex Ylm = spharmfn[i++];
                   Ylm *= phi_phase_m;
                   Ylm *= intensity;
            int nmax = (lmax-l+2)/2;
            //int l_index = l/2-1;
            for (int n = 1; n <= nmax; n++)
            {
              //phaser_assert(n_elmn < elmn.size());
              elmn[n_elmn] += besselx[l+2*n-1]*Ylm; //note way of storing this!
              n_elmn++;
            }
          } // l loop
        } // m loop
      } // r loop
    } // c

    return elmn;
  }

  void
  Elmn::resize_elmn(int PATZ)
  {
    phaser_assert(LMAX > 2);
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    ELMN = elmn_array_t(); //completely clear
    ELMN.resize(LMAX/2); // l=2 => [0],  l=LMAX => [LMAX/2-1]
    for (int l = 2; l <= LMAX; l += 2)
    {
      int l_index = l/2-1;
      int nmax = (LMAX-l+2)/2;
      ELMN[l_index].resize(2*l+1);
      for (int m = 0; m <= l; m++)
      {
        ELMN[l_index][l+m].resize(nmax+1);
        ELMN[l_index][l-m].resize(nmax+1);
      }
    }
#else
    ELMN = elmn_array_t(scitbx::af::c_grid<3>(LMAX/2,2*LMAX+1,LMAX/2+1)); //completely clear
#endif
    NELMN = 0;
    NSPHARM = 0;
    for (int m = 0; m <= LMAX; m++)
    {
      if (m%PATZ != 0) continue;
      for (int l = m; l <= LMAX; l += 2)
      {
        if (l%2 == 1) l++;
        if (l < 2) continue;
        int nmax = (LMAX-l+2)/2;
        NSPHARM++;
        for (int n = 1; n <= nmax; n++)
        {
          NELMN++;
        } // n loop
      } // l loop
    } // m loop
    phaser_assert(NELMN != 0);
  }

  void
  Elmn::clear_elmn()
  {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    for (int l = 0; l < ELMN.size(); l++)
      for (int m = 0; m < ELMN[l].size(); m++)
        for (int n = 0; n < ELMN[l][m].size(); n++)
          ELMN[l][m][n] = cmplex(0,0);
#else
    for (int l = 0; l < ELMN.accessor().size_1d(); l++ )
          ELMN[l] = cmplex(0,0);
#endif
  }

  bool
  Elmn::nonzero()
  {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    for (int l = 0; l < ELMN.size(); l++)
      for (int m = 0; m < ELMN[l].size(); m++)
        for (int n = 0; n < ELMN[l][m].size(); n++)
          if (ELMN[l][m][n] != cmplex(0,0)) return true;
#else
    for (int l = 0; l < ELMN.accessor().size_1d(); l++ )
          if (ELMN[l] != cmplex(0,0)) return true;
#endif
    return false;
  }

  af_string
  Elmn::hkl_to_elmn(
      const HKL_clustered& HKL_list,
      const int TWOPIB,
      const std::pair<int,char> HIGHSYMM)
  {
    af_string output;
/*
    {{ //start with a memory check
    try {
      std::vector< std::future< sv_cmplex > > results(NTHREADS-1);
    }
    catch (...)
    { //if there is not enough memory then resort to using only one thread
      output.push_back("Memory allocation on threads failed, revert to single thread");
      set_nthreads(1,true);
    }
    }}
*/
    // This function assumes that the HKL_list contains Friedel unique entries and
    // no 0,0,0 entry.
    // set the number of threads to be used based on NTHREADS and the number of clusters
    // of reflections
    const int number_of_clusters = HKL_list.clustered.size();
    int nthreads = (number_of_clusters <= NTHREADS) ? number_of_clusters : NTHREADS;
    int PATZ = HIGHSYMM.first; //local copy on thread

    // create the hkl lists for each thread to chew on
    std::vector<HKL_clustered> HKL_lists(nthreads);

    if (number_of_clusters <= NTHREADS) // extremely unlikely in practice
    {
      for (int n = 0; n < nthreads; n++)
      {
        HKL_lists[n].clustered.push_back(HKL_list.clustered[n]);
      }
    }
    else // the expected case
    {
      //integer division, result will always be >= 1 because number_of_clusters > nthreads
      const int grain = HKL_list.size() / nthreads;
      //thread balancing code below, since clusters are different sizes
      std::vector<std::pair<int,int>> count(number_of_clusters);
      sv_bool used(number_of_clusters,false);
      for (int i = 0; i < number_of_clusters; i++)
        count[i] = { HKL_list.clustered[i].size(),i};
      auto cmp = []( const std::pair<int,int> &a, const std::pair<int,int> &b )
      { if (a.first == b.first) return a.second < b.second;
        return a.first >  //reverse sort
               b.first; };
      if (NTHREADS > 1)
        std::sort(count.begin(),count.end(),cmp); //sorted on size
      //this is so that the smallest ones are at the end and can be safely added to complete final
      //thread (because used=false) without overloading the last thread
      sv_int cumulative(nthreads);
      for (int n = 0; n < nthreads; n++)
      {
        for (int j = 0; j < number_of_clusters; j++)
        {
          if (!used[j])
          {
            int i = count[j].second;
            int size = count[j].first;
            if ( (size >= grain) or //these are overflows, fill the entire thread
                ((size  + cumulative[n]) < grain) or //fill up until limit
                 (n == nthreads-1)) //fill up final thread with stragglers, regardless
            {
              cumulative[n] += size;
              HKL_lists[n].clustered.push_back(HKL_list.clustered[i]);
              used[j] = true;
            }
          }
        }
      }
      for (int i = 0; i < number_of_clusters; i++)
      {
        phaser_assert(used[i]);
      // output.push_back("Cluster #" + std::to_string(i) + " count #" + std::to_string(count[i].second) + " " + std::to_string(count[i].first));
      }
      int total(0);
      output.push_back("Load balancing");
      for (int n = 0; n < nthreads; n++)
      {
        output.push_back("Thread #" + std::to_string(n) + " cumulative load " + std::to_string(cumulative[n]));
        total += cumulative[n];
      }
      phaser_assert(total == HKL_list.size());
    }

    //instead of crashing if one of the lists is empty, check in advance
    //if one of the lists is empty then stick them all back on one thread
    bool empty(false);
    for (int n = 0; n < nthreads; n++)
    {
      if (!HKL_lists[n].clustered.size())
       empty = true;
    }
    if (empty)
    {
      HKL_lists.resize(1);
      for (int n = 0; n < nthreads; n++)
      {
        HKL_lists[0].clustered.push_back(HKL_list.clustered[n]);
      }
      nthreads = 1;
    }

    // launch threads for all but the first batch of reflections
    {{
    std::vector< std::future< sv_cmplex > > results;
    std::vector< std::packaged_task< sv_cmplex(
            const HKL_clustered,
            const int,
            const std::pair<int,char>
            )> > packaged_tasks;
    std::vector<std::thread> threads;
    int nthreads_1 = nthreads-1; //not NTHREADS
    results.reserve(nthreads_1);
    if (USE_STRICTLY_NTHREADS)
    {
      packaged_tasks.reserve(nthreads_1);
      threads.reserve(nthreads_1);
    }
    for (int t = 0; t < nthreads_1; t++)
    {
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.emplace_back( std::bind( &Elmn::partial_elmn_calculation, this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3));
        results.push_back(packaged_tasks[t].get_future());
        // create the thread (and set it off running)
        threads.emplace_back(std::move(packaged_tasks[t]),
            HKL_lists[t],
            TWOPIB,
            HIGHSYMM);
      }
      else
      {
        results.push_back( std::async( std::launch::async, &Elmn::partial_elmn_calculation, this,
            HKL_lists[t],
            TWOPIB,
            HIGHSYMM));
      }
    }
    //now the last thread
    sv_cmplex main_thread_elmns = partial_elmn_calculation(
            HKL_lists[nthreads_1],
            TWOPIB,
            HIGHSYMM);

    clear_elmn();
    {{ //main thread
      int n_elmn(0);
      for (int m = 0; m <= LMAX; m++)
      {
        if (m%PATZ != 0) continue;
        for (int l = m; l <= LMAX; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          int nmax = (LMAX-l+2)/2;
          int l_index = l/2-1;
          for (int n = 1; n <= nmax; n++)
          {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
            ELMN[l_index][l+m][n] += main_thread_elmns[n_elmn];
#else
            ELMN(l_index,l+m,n) += main_thread_elmns[n_elmn];
#endif
            n_elmn++;
          } // n loop
        } // l loop
      } // m loop
    }}

    // sum values from different threads
    for (int t = 0; t < nthreads_1; t++)
    {
      auto thread_elmns = results[t].get();
      if (USE_STRICTLY_NTHREADS) threads[t].join(); //terminate the threads
      int n_elmn(0);
      for (int m = 0; m <= LMAX; m++)
      {
        if (m%PATZ != 0) continue;
        for (int l = m; l <= LMAX; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          int nmax = (LMAX-l+2)/2;
          int l_index = l/2-1;
          for (int n = 1; n <= nmax; n++)
          {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
            ELMN[l_index][l+m][n] += thread_elmns[n_elmn];
#else
            ELMN(l_index,l+m,n) += thread_elmns[n_elmn];
#endif
            n_elmn++;
          } // n loop
        } // l loop
      } // m loop
    }
    }}

    // populate the negative m values by using the standard spherical harmonics identity:
    // Y_lm = (-1)^m * conj( Y_(l)(-m) )
    // see e.g. [6.102b] Introduction to quantum mechanics, B. H. Bransden and C. J. Joachain. 1989
    // hence e_(l)(-m)(n) = (-1)^m * conj ( e_lmn )
    const int ONE(1);
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    for (int l_index = 0; l_index < ELMN.size(); l_index++)
#else
    cctbx::af::tiny<std::size_t, 3> dimensions = ELMN.accessor();
    for (int l_index = 0; l_index < dimensions[0]; l_index++)
#endif
    {
      int l = 2*l_index+2;
      for (int m_index = 0; m_index <= l; m_index++)
      {
        double sign = (m_index%2==1) ? -ONE : ONE; // -1 if m_index is odd i.e. (-1)^m_index
        //double sign = (m_index & 1) ? -ONE : ONE; //bitwise AND operation - looks at last bit, very fast
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
        for (int n_index = 1; n_index < ELMN[l_index][m_index].size(); n_index++)
        {
          ELMN[l_index][l-m_index][n_index] = sign*std::conj(ELMN[l_index][l+m_index][n_index]);
        }
#else
        for (int n_index = 1; n_index < dimensions[2]; n_index++)
        {
 //         PHASER_ASSERT(l_index < dimensions[0]);
 //         PHASER_ASSERT(l-m_index < dimensions[1]);
 //         PHASER_ASSERT(n_index < dimensions[2]);
          ELMN(l_index,l-m_index,n_index) = sign*std::conj(ELMN(l_index,l+m_index,n_index));
        }
#endif
      }
    }
    return output;
  }

} //phasertng
