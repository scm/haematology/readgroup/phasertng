//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/rfactorial.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <phasertng/site/FastRotationFunction.h>
#include <phasertng/src/ElmnData.h>
#include <phasertng/src/ElmnEnsemble.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Fast_rotation_function
  void Phasertng::runFRF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!(DAGDB.NODES.is_all_pose() or DAGDB.NODES.has_no_turn_curl_pose()))
    throw Error(err::INPUT,"Dag not prepared with correct mode");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    DAGDB.apply_interpolation(interp::FOUR);

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    int io_maxstored = input.value__int_number.at(".phasertng.fast_rotation_function.maximum_stored.").value_or_default();
    int io_maxmemory = std::max(io_maxstored,1000000);
    int io_nprint = input.value__int_number.at(".phasertng.fast_rotation_function.maximum_printed.").value_or_default();
    double io_percent_max = 100;
    bool io_use_top_not_in_pose = input.value__boolean.at(".phasertng.fast_rotation_function.top_not_in_pose.").value_or_default();
    double io_percent = input.value__percent.at(".phasertng.fast_rotation_function.percent.").value_or_default();
    bool io_use_cluster =  input.value__boolean.at(".phasertng.fast_rotation_function.cluster.").value_or_default();
    double signal_resolution = input.value__flt_number.at(".phasertng.fast_rotation_function.signal_resolution.").value_or_default();
    bool io_any_cluster = input.value__boolean.at(".phasertng.fast_rotation_function.purge_duplicates.").value_or_default();
    bool io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").is_default() ? false : input.value__boolean.at(".phasertng.coiled_coil.").value_or_default(); //default None

    logUnderLine(out::SUMMARY,"Search");
    if (input.value__uuid_number.at(".phasertng.search.id.").is_default() and
        input.value__string.at(".phasertng.search.tag.").is_default())
      throw Error(err::INPUT,"Search identifier not input");
    Identifier identifier(
        input.value__uuid_number.at(".phasertng.search.id.").value_or_default(),
        input.value__string.at(".phasertng.search.tag.").value_or_default());
    auto search = DAGDB.lookup_entry_tag(identifier,DataBase());
    logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
    if (!search->identify().is_set_valid())
      throw Error(err::INPUT,"Search identifier not defined");

    //below clears composition and other things that just inflate the size of the file
    logTab(out::VERBOSE,"Clear the full and partial entry data");
    if (Write()) DAGDB.clear_frf_entries(search->identify());

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    load_entry_from_database(where,entry::DECOMPOSITION_MTZ,search->identify(),false);
    load_entry_from_database(where,entry::MODELS_PDB,search->identify(),false);
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}

    std::set<std::pair<double,double>> vrms_estimates; //sorted set

    if (input.size(".phasertng.fast_rotation_function.maps.id."))
    {
      logTab(out::SUMMARY,"Search will be performed over a list of maps");
      logTab(out::SUMMARY,"Number of maps: " + itos(input.size(".phasertng.fast_rotation_function.maps.id.")));
      if (!REFLECTIONS->MAP)
        logWarning("Input reflections are not a map");
      std::vector<Identifier> reflids;
      std::vector<UnitCell> unitcells;
      for (int i = 0; i < input.size(".phasertng.fast_rotation_function.maps.id."); i++)
      {
        Identifier reflid(
          input.array__uuid_number.at(".phasertng.fast_rotation_function.maps.id.").at(i).value_or_default(),
          input.array__string.at(".phasertng.fast_rotation_function.maps.tag.").at(i).value_or_default());
        reflids.push_back(reflid);
      }
      for (auto reflid : reflids)
      { //open and read the values from the files
        load_reflid_for_node(reflid,out::VERBOSE);
        unitcells.push_back(REFLECTIONS->UC);
        double ztotalscat = REFLECTIONS->get_ztotalscat();
        double fracscat = search->fraction_scattering(ztotalscat);
        fracscat *= nmol;
        if (fracscat > 1)
        {
          logWarning("Fraction scattering will be more than 1 after placement" +
                      std::string((nmol > 1) ? " (*tncs)":""));
          DAGDB.shift_tracker(hashing(),input.running_mode);
          ////DAGDB.clear_cards(); //no result
          return;
        }
      }
      logBlank(out::VERBOSE);
      int before = DAGDB.size();
      DAGDB.apply_reflid_expansion(reflids,unitcells);
      DAGDB.reset_entry_pointers(DataBase());
      logTab(out::SUMMARY,"Number until maps expansion: " + itos(before));
      logTab(out::SUMMARY,"Number after maps expansion: " + itos(DAGDB.size()));
      DAGDB.restart();
      DAGDB.reset_entry_pointers(DataBase());
      logBlank(out::VERBOSE);
    }
    else if (input.size(".phasertng.fast_rotation_function.vrms_estimates.") != 0)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Vrms Estimates");
      logTab(where,"Search requested over a list of vrms estimates");
      auto steps = input.value__flt_numbers.at(".phasertng.fast_rotation_function.vrms_estimates.").value_or_default();
      const auto& modlid = search->identify();
      auto used_modlid = DAGDB.NODES.used_modlid_pose();
      if (used_modlid.count(modlid) > 0)
      {
        logTab(where,"No alternative vrms estimates because poses already present");
      }
      else
      {
        int nstart = DAGDB.size();
        logTab(where,"No poses already present");
        logTab(where,"Vrms estimates from list of alternatives");
        auto vrmses = input.value__flt_numbers.at(".phasertng.fast_rotation_function.vrms_estimates.").value_or_default();
        std::set<double> drms_in,drms_out,vrms_in,vrms_out; //sorted set
        double dmin = search->DRMS_MIN;
        double dmax = search->DRMS_MAX;
        phaser_assert(search->VRMS_START.size()>0);
        bool lowest(false),highest(false);
        double initial = search->VRMS_START[0]; //alphafold esm
        for (auto vrms : vrmses)
        {
          double drms = pod::get_drms_from_initial_for_vrms(vrms,initial);
          vrms_in.insert(vrms);
          drms_in.insert(drms);
          if (drms >= dmin and drms <= dmax)
          {
            drms_out.insert(drms);
            vrms_out.insert(vrms);
            vrms_estimates.insert({drms,vrms});
          }
          else if (drms < dmin) lowest = true;
          else if (drms > dmax) highest = true;
        }
        if (lowest)
        {
          drms_out.insert(dmin);
          double vrms = pod::apply_drms_to_initial(dmin,initial);
          vrms_out.insert(vrms);
          vrms_estimates.insert({dmin,vrms});
        }
        if (highest)
        {
          drms_out.insert(dmax);
          double vrms = pod::apply_drms_to_initial(dmax,initial);
          vrms_out.insert(vrms);
          vrms_estimates.insert({dmax,vrms});
        }
        logTab(where,"Vrms: " + dvtos(sv_double(vrms_in.begin(),vrms_in.end()),3));
        logTab(where,"Drms: " + dvtos(sv_double(drms_in.begin(),drms_in.end()),3));
        logTab(where,"Range of drms: " + dtos(dmin) + "->" + dtos(dmax));
        logTab(where,"Drms in range: " + dvtos(sv_double(drms_out.begin(),drms_out.end()),3));
        logTab(where,"Vrms in range: " + dvtos(sv_double(vrms_out.begin(),vrms_out.end()),3));
        //replace the DAGDB with a list that has been expanded by the vrms alternatives
      }
    }

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Clustering Protocol");
    if (io_use_cluster)
      logTab(where,"Clustering performed and used to purge peaks");
    else if (!io_use_cluster and io_any_cluster)
      logTab(where,"Clustering performed and used to purge very close peaks to purge duplicates");
    else if (!io_any_cluster)
      logTab(where,"No clustering, speed enhancement");
    }}

    //set the star flags for the seek components corrrectly, with pose they become found
    DAGDB.increment_current_seeks_with_identifier(search->identify());
    logUnderLine(out::LOGFILE,"Seek Information");
    logTabArray(out::LOGFILE,DAGDB.seek_info_array());
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    logTab(out::VERBOSE,"Z = " + dtos(REFLECTIONS->Z));
    logTab(out::VERBOSE,"total scattering = " + dtos(REFLECTIONS->TOTAL_SCATTERING,0));
    logTab(out::VERBOSE,"Z total scattering = " + dtos(ztotalscat,0));
    if (DAGDB.WORK->SEEK.size())
    {
      logTab(out::VERBOSE,"tNCS = " + itos(REFLECTIONS->TNCS_ORDER));
      logTab(out::VERBOSE,"nseek = " + itos(DAGDB.WORK->SEEK.size()));
      //scattering_seek includes the nmol
      logTab(out::VERBOSE,"total scattering seek = " + dtos(DAGDB.WORK->scattering_seek()));
      double fs(DAGDB.WORK->fraction_scattering_seek(ztotalscat));
      logTab(out::VERBOSE,"fs = " + dtos(fs));
      if (fs > 1)
      {
        logWarning("Fraction scattering will be more than 1 after placement" +
                    std::string((REFLECTIONS->TNCS_ORDER > 1) ? " (*tncs)":""));
        DAGDB.shift_tracker(hashing(),input.running_mode);
        DAGDB.clear_cards(); //no result
        return;
      }
    }
    DAGDB.WORK->NEXT.IDENTIFIER = search->identify();
    DAGDB.WORK->NEXT.PRESENT = false; //otherwise thinks turn is present
    DAGDB.WORK->NEXT.ENTRY = search;
    //this will fail this search if the fraction scattering is over 1
    //which means that this search will fail from changeling if the mol*nmol doesn't fit in asu
    //where the asu is set at 5% solvent
    double fracscat = search->fraction_scattering(ztotalscat);
    logTab(out::LOGFILE,"Fraction scattering for search = " + dtos(fracscat,2));
    fracscat *= REFLECTIONS->TNCS_ORDER;
    logTab(out::LOGFILE,"Fraction scattering for search (*tncs) = " + dtos(fracscat,2));
    if (fracscat > 1)
    {
      logWarning("Fraction scattering will be more than 1 after placement" +
                  std::string((REFLECTIONS->TNCS_ORDER > 1) ? " (*tncs)":""));
      DAGDB.shift_tracker(hashing(),input.running_mode);
      DAGDB.clear_cards(); //no result
      return;
    }
    //if (!DAGDB.WORK->tncs_obeyed(search->identify()))
    //  throw Error(err::INPUT,"Remaining constituents not sufficient for tNCS order");

    //reset the search so that it is now empty, so that not repeated in gyre mode
    //input.value__uuid_number.at(".phasertng.search.id.").set_default();
    //input.value__string.at(".phasertng.search.tag.").set_default();

    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    bool store_elmn(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR,store_elmn);

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    if (signal_resolution)
       DAGDB.WORK->SIGNAL_RESOLUTION = signal_resolution;
    double resolution = DAGDB.WORK->search_resolution(io_hires);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    logTab(out::LOGFILE,"Seek resolution = " + dtos(DAGDB.WORK->SEEK_RESOLUTION));
    logTab(out::LOGFILE,"Signal resolution = " + dtos(DAGDB.WORK->SIGNAL_RESOLUTION));
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::SUMMARY,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    if (selected.nsel() == 0)
      throw Error(err::FATAL,"No reflections selected");
    }}

    phaser_assert(DAGDB.size());

    //calculate lmax
    int lowsymm(1);
    std::pair<int,char> highsymm = REFLECTIONS->SG.highOrderAxis();
    double hires = selected.STICKY_HIRES;
    double sphereOuter = search->DECOMPOSITION.sphereOuter();
    int lmax = search->calculate_lmax(sphereOuter,selected.STICKY_HIRES);
    double cluster_angle(0),sampling(0);
    std::tie(cluster_angle,sampling) = search->clustang(
        selected.STICKY_HIRES,
        input.value__flt_number.at(".phasertng.fast_rotation_function.sampling.").value_or_default(),
        //helix internal, extent internal, mean_radius internal
        io_coiled_coil,
        (!io_use_cluster and io_any_cluster),
        input.value__flt_number.at(".phasertng.fast_rotation_function.helix_factor.").value_or_default(),
        highsymm.first
      );

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Elmn Calculation");
    logTab(where,"Calculating Elmn with resolution " + dtos(hires,2));
    logTab(where,"Integration radius " + dtos(sphereOuter,2));
    logTab(where,"Maximum degree of spherical harmonics " + itos(lmax));
    logTab(where,"Highest symmetry of multiplicity "+itos(highsymm.first)+" around axis "+chtos(highsymm.second));
    }}

    ElmnEnsemble elmn_ensemble(lmax);
    elmn_ensemble.set_nthreads( NTHREADS,USE_STRICTLY_NTHREADS);

    ElmnData elmn_data(lmax,highsymm);
    elmn_data.set_nthreads( NTHREADS,USE_STRICTLY_NTHREADS);

    //every time a new rotation function peak is added it gets a new rotation function id as parent
    //this means that the background for the search is given an identifier
    //which makes for simple identificiation of solution history
    //even if subsequent refinement moves the background components (slightly)
    Identifier parent;
    parent.initialize_from_text(svtos(DAGDB.WORK->logNode())+hashing());//same for same job
    parent.initialize_tag("frf"+ntos(1,DAGDB.size()));

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Fast Rotation Functions");
    logTab(where,"Background Poses: "+itos(DAGDB.WORK->POSE.size()));
    logTab(where,"Percent cutoff: " + dtos(io_percent,5,1) + "%");
    double io_helix_factor = input.value__flt_number.at(".phasertng.fast_rotation_function.helix_factor.").value_or_default();
    if (io_helix_factor != 1)
    {
      logTab(where,"All-helical (helix factor gridding): " + btos(search->HELIX));
      logTab(where,"Coiled-coil (helix factor gridding): " + btos(io_coiled_coil));
      logTab(where,"Helix factor: " + dtos(io_helix_factor));
    }
    if (!input.value__flt_number.at(".phasertng.fast_rotation_function.sampling.").is_default())
      logTab(where,"Sampling angle given by input");
    logTab(where,"Sampling Angle: " + dtos(sampling,5,2) + " degrees");
    logTab(where,"Cluster Angle:  " + dtos(cluster_angle,5,2) + " degrees");
    if (nmol)
    {
      logTab(where,"Translational NCS: Rotational spacegroup changed to P1");
      logBlank(where);
    }
    if (vrms_estimates.size() == 0)
      vrms_estimates.insert({0,search->VRMS_START[0]});
    else
    {
      //value above is not used but make the loop at least one long
    logTab(where,"Number of vrms estimates = "+itos(vrms_estimates.size()));
    sv_double drms,vrms;
    for (auto est : vrms_estimates)
    {
      drms.push_back(est.first);
      vrms.push_back(est.second);
    }
    logTab(where,"--drms estimates = "+dvtos(drms,6,3));
    logTab(where,"--vrms estimates = "+dvtos(vrms,6,3));
    logTab(where,"keyword: fast_rotation_function vrms_estimates");
    }
    }}

   // DAGDB.shift_tracker(hashing(),input.running_mode);

    dag::NodeList orig_nodelist = DAGDB.NODES;
    std::vector<dag::NodeList> vrms_estimate_output_dag;
    int v(1),V(vrms_estimates.size());
    for (const auto& vrms_estimate : vrms_estimates)
    {
      std::string vV = vrms_estimates.size() == 1 ? "" :nNtos(v,V);
      v++;
      const auto& modlid = search->identify();
      auto used_modlid = DAGDB.NODES.used_modlid_pose();
      if (used_modlid.count(modlid) == 0)
      {
        DAGDB.NODES = orig_nodelist; //before conversion to frf results!
        DAGDB.apply_cell(search->identify(),1);
        DAGDB.apply_drms(search->identify(),vrms_estimate);
      }
      FastRotationFunction rotfun(REFLECTIONS.get(),nmol > 1);
      rotfun.set_nthreads(NTHREADS,USE_STRICTLY_NTHREADS);

      //cannot copy ENTRIES, just use dagdatabase as a container for NODES, which is copied
      pod::parent_mean_sigma_top stats;
      dag::NodeList output_dag;
      int n(1),N(DAGDB.size());
      DAGDB.restart();
      std::string bestnN = nNtos(n,N);
      out::stream progress = out::LOGFILE;
      logProgressBarStart(progress,"Fast Rotation Functions"+vV,DAGDB.size());
      dvect3 bestrfz(0,0,0);
      int maxrf = DAGDB.size();
      while (!DAGDB.at_end())
      {
        std::string nN = nNtos(n++,N);
        double llg;
        out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;

        pod::FastPoseType ecalcs_sigmaa_pose;
        {{
        logUnderLine(where,"Fast Rotation Function" + nN);
        bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (changed or diffsg)
        {
          if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str());
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        }
        ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS);
        std::tie(llg) = DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        logTabArray(where,DAGDB.WORK->logAnnotation(""));
        }}

        if (Suite::Extra())
          logTabArray(out::TESTING,DAGDB.WORK->logPose("Known components"));

        int clmn_lmin = DEF_CLMN_LMIN;
        {{
        logChevron(where,"Spherical Harmonics");
        const auto& modlid = search->identify();
        phaser_assert(DAGDB.WORK->DRMS_SHIFT.count(modlid));
        double DRMS = DAGDB.WORK->DRMS_SHIFT[modlid].VAL;
        logTab(where,"Resolution = " + dtos(hires));
        logTab(where,"Calculate search elmn with drms = " + dtos(DRMS));
        logEllipsisOpen(where,"Calculate search elmn");
        auto txt = elmn_ensemble.ELMNxR2(
            &search->DECOMPOSITION,
            sphereOuter,
            hires,
            DRMS
            );
        logEllipsisShut(where);
        if (Suite::Extra())
          logTabArray(out::TESTING,txt);
        phaser_assert(elmn_ensemble.nonzero());
        }}

        {{
        logEllipsisOpen(where,"Calculate data and fixed component elmn");
        auto txt = elmn_data.ELMNxR2(
            sphereOuter,
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            DAGDB.WORK->TNCS_ORDER,
            DAGDB.WORK->TNCS_MODELLED,
            hires,
            &ecalcs_sigmaa_pose);
        logEllipsisShut(where);
        if (Suite::Extra())
          logTabArray(out::TESTING,txt);
        phaser_assert(elmn_data.nonzero());
        }}

        bool calculate_stats(Suite::Extra());

        {{
        rotfun.rotation_function_data.clear();
        logEllipsisOpen(where,"Scanning the Range of Beta Angles");
        bool store_full_map(false);
        auto txt = rotfun.calculate_fourier_transforms(
            elmn_data.ELMN,
            elmn_ensemble.ELMN,
            lmax,
            sampling,
            lowsymm,
            calculate_stats,
            store_full_map
          );
        logEllipsisShut(where);
        if (Suite::Extra())
          logTabArray(out::TESTING,txt);
        }}

        if (calculate_stats)
        {
          logTab(where,"Mean and Standard Deviation on Beta Sections");
          int i = rotfun.total_number();
          logTab(where,"There " + std::string(i==1?"is ":"are ") + itos(i) + " peak" + std::string(i==1?"":"s"));
          logTab(where,snprintftos(
              "%3s %10s %10s %10s %10s %10s",
              "#","Beta","Mean","Sigma","Top-FSS","Points"));
          for (int b = 0; b < rotfun.beta_section.size(); b++)
          {
            AnglesStored& beta_section = *std::next(rotfun.beta_section.begin(),b);
            logTab(where,snprintftos(
                "%3d %10.3f %10.3f %10.3f %10.3f %10d",
                b+1,
                beta_section.beta_deg,
                beta_section.f_mean,
                beta_section.f_sigma,
                beta_section.f_top,
                beta_section.rotation_function_data.size()));
          }
          logBlank(where);
        }

        {{
        logEllipsisOpen(where,"Merge Beta Sections");
        bool clear_data(true);
        rotfun.merge_beta_sections(clear_data);
        logEllipsisShut(where);
        }}

        if (calculate_stats)
        {
          auto meansig = rotfun.mean_sigma();
          double Rmax = rotfun.top();
          logUnderLine(where,"Mean and Standard Deviation");
          logTab(where,"Mean Score:      " + dtos(meansig.first,3));
          logTab(where,"Sigma of Scores: " + dtos(meansig.second,3));
          logTab(where,"Highest Score:   " + dtos(Rmax,3));
          logTab(where,"Highest Z-score: " + dtos((Rmax-meansig.first)/meansig.second));
          logTab(where,"Number of Peaks: " + itos(rotfun.rotation_function_data.size()));
        }

        {{
        logTab(where,"Mean and Standard Deviation");
        bool newmax = rotfun.statistics();
        logTab(where,snprintftos(
            "%-10s %10s %10s %10s %10s %10s %10s",
            "#","Mean","Sigma","Top-FSS","Top-Z","Max","Max-Z"));
        logTab(where,snprintftos(
            "%10d %10.3f %10.3f %10.3f %10.3f %10.3f %10.3f",
            rotfun.rotation_function_data.size(),
            rotfun.f_mean, rotfun.f_sigma, rotfun.f_top, rotfun.z_top, rotfun.f_max, rotfun.z_max));
        bool newrfz(rotfun.z_max > bestrfz[0]);
        if (n == 1 or (newmax and newrfz))
        {
          logProgressBarAgain(progress,"New Best FSS=" + dtos(rotfun.f_top,2) + " and RFZ=" + dtos(rotfun.z_max,2) + " " + nN,1);
          bestnN = nN;
          bestrfz = dvect3(rotfun.z_max,rotfun.f_max,n-1);
        }
        else if (newmax)
        {
          logProgressBarAgain(progress,"New Best FSS=" + dtos(rotfun.f_top,2) + " (RFZ=" + dtos(rotfun.z_max,2) + ")" + nN,1);
          bestnN = nN;
        }
        else if (newrfz)
        {
          logProgressBarAgain(progress,"New Best RFZ=" + dtos(rotfun.z_max,2) + " (FSS=" + dtos(rotfun.f_max,2) + ")" + nN,1);
          bestrfz = dvect3(rotfun.z_max,rotfun.f_max,n-1);
        }
        logChevron(where,"Current Best FSS = " + dtos(rotfun.f_max,2) + " (RFZ=" + dtos(rotfun.z_max,2) + ")" + bestnN);
        logChevron(where,"Current Best RFZ = " + dtos(bestrfz[1],2) + " (RFZ=" + dtos(bestrfz[0],2) + ")" + nNtos(bestrfz[2],N));
        logBlank(where);
        stats.mst[parent.identifier()] = dvect3(rotfun.f_mean,rotfun.f_sigma,rotfun.f_top);
        //this could be on number of rf not parent, but in the ftf it has to be on id
        //so to use the same object we use the parent as index
        }}

        //after mean and standard deviation
        {{
        logEllipsisOpen(where,"Sort and purge by percent");
        int before = rotfun.count_sites();
        auto node = *DAGDB.WORK;
        //we copy the node and set the identifier so that we can use turn_in_poses as test in rotfun
        //this is the same test as used below
        node.NEXT.IDENTIFIER = search->identify();
        double f_novel_max = io_use_top_not_in_pose ? rotfun.top_novel_llg(node) : rotfun.f_max;
        //doesn't sort unless clustering
        //otherwise just sorts the ones over the cutoff to the front of the list
        double cut = rotfun.apply_partial_sort_and_percent_fss(
                       io_percent,
                       f_novel_max,
                       io_any_cluster); //full sort of the surviving peaks only when clustering
        logEllipsisShut(where);
        logTab(where,"Percent cutoff: " + dtos(io_percent,5,1) + "%");
        logTab(where,"top/top-novel " + dtos(rotfun.f_max,5,2) + " " + dtos(f_novel_max,5,2));
        logTab(where,snprintftos(
            "%s %.2f/%.2f/%.2f/%.2f",
            "FSS Max/Top/Mean/Cutoff:",rotfun.f_max,rotfun.rotation_function_data[0].value,rotfun.f_mean,cut));
        logTab(where,"Number until purge: " + itos(before));
        logTab(where,"Number after purge: " + itos(rotfun.count_sites()));
        logBlank(where);
        }}

        if (rotfun.count_sites() == 0)
        {
          logChevron(where,"Current Best = " + dtos(rotfun.f_max,2) + " (RFZ=" + dtos(rotfun.z_max,2) + ")" + bestnN);
          continue;
        }

        //axis of rotation to reduce the symmetry
        {{
        dmat33 axisrot;
             if (highsymm.second=='Z') axisrot = dmat33(1,0,0, 0,1,0, 0,0,1);
        else if (highsymm.second=='X') axisrot = dmat33(0,0,1, 1,0,0, 0,1,0);
        else if (highsymm.second=='Y') axisrot = dmat33(0,1,0, 0,0,1, 1,0,0);
        logTab(where,"Reverse highest symmetry of multiplicity "+itos(highsymm.first));
        logTab(where,"around axis " + chtos(highsymm.second));
        logTab(where,"matrix: " + dmtos(axisrot));
        //change to pdb coordinate frame
        for (auto& item : rotfun.rotation_function_data)
        {
          //the point to be searched is wrt the pdb orientation
          dmat33 R = zyz_matrix(item.grid);
          const dmat33 ROT = axisrot*R;
          item.grid = scitbx::math::euler_angles::zyz_angles<double>(ROT);
        }
        logEllipsisOpen(where,"Move to Rotational ASU");
        rotfun.move_to_rotational_asu();
        logEllipsisShut(where);
        }}

        rotfun.clustering_off();

        {{
        logTableTop(where,"Fast Rotation Function (before clustering)" + nN,false);
        logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
        if (!io_any_cluster) //in which case not sorted already, so cosmetic sort here
          rotfun.apply_partial_sort_for_print(io_nprint);
        logTabArray(where,rotfun.logFastTable(io_nprint,true,search->PRINCIPAL_ORIENTATION));
        logTableEnd(where);
        }}

        //peaks are sorted by virtue of partial sort in percent cutoff
        if (io_any_cluster)
        {
          logEllipsisOpen(where,"Cluster fast rotation function search peaks");
          //if the sampling is not inflated, then each point has no neighbours!!!
          int cluster_back = input.value__int_number.at(".phasertng.fast_rotation_function.cluster_back.").value_or_default();
          rotfun.cluster(cluster_angle,cluster_back);
          logEllipsisShut(where);

          logEllipsisOpen(where,"Purge by cluster");
          int before = rotfun.count_sites();
          rotfun.select_topsites_and_erase();
          logEllipsisShut(where);
          logTab(where,"Number until purge: " + itos(before));
          logTab(where,"Number after purge: " + itos(rotfun.count_sites()));

          bool AllSites(false);
          logTableTop(where,"Fast Rotation Function (after clustering)"+nN,false);
          logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
          logTabArray(where,rotfun.logFastTable(io_nprint,AllSites,search->PRINCIPAL_ORIENTATION));
          logTableEnd(where);

          Loggraph loggraph;
          loggraph.title = "Top peaks in Rotation Function"+nN;
          loggraph.scatter = true;
          loggraph.graph.resize(1);
          loggraph.graph[0] = "RF Number vs LL-gain:AUTO:1,2";
          int i(1);
          for (auto& item : rotfun.rotation_function_data)
          {
            loggraph.data_text += itos(i++) + " " + dtos(item.value,10,2) + "\n";
          }
          logGraph(where,loggraph);
        }

        //purging by number done after clustering has been done (or not)
        {{
        int before = rotfun.count_sites();
        //there is still a theoretical possibility that this could truncate to
        //fewer than io_maxstored peaks by the end
        if (before > io_maxmemory)
        {
          out::stream where = out::VERBOSE;
          logEllipsisOpen(where,"Purge by maximum number");
          if (io_any_cluster)
          { //already sorted
            rotfun.apply_partial_sort_and_maximum_stored_llg(io_maxmemory);
          }
          else
          { //unsorted above nth element
            rotfun.apply_nth_element_maximum_stored_llg(io_maxmemory);
            //apply cosmetic sort for print
            rotfun.apply_partial_sort_for_print(io_nprint);
          }
          logEllipsisShut(where);
          if (io_maxmemory == 0)
          logTab(where,"Maximum stored: all");
          else
          logTab(where,"Maximum stored:     " + itos(io_maxmemory));
          logTab(where,"Number until purge: " + itos(before));
          logTab(where,"Number after purge: " + itos(rotfun.count_sites()));
        }
        }}

        {{
        logEllipsisOpen(where,"Append to DAG");
        //add the growing list to the DAG, note that this will not be sorted overall
        DAGDB.WORK->initialize_turn({0,0,0},search);
        DAGDB.reset_work_entry_pointers(DataBase());
        for (int i = 0; i < rotfun.rotation_function_data.size(); i++)
        {
          auto item = rotfun.rotation_function_data[i];
          dag::Node next = *DAGDB.WORK; //deep copy
          next.NEXT.IDENTIFIER = search->identify();
          double val = item.value;
          double perc = 100.*(val-rotfun.f_mean)/(rotfun.f_max-rotfun.f_mean);
          next.PERCENT = perc; //fast search score, not the clustered score
          double zsr = (val-rotfun.f_mean)/rotfun.f_sigma;
          //std::string annotation = dtoi(val)+"/"+dtoi(perc)+"%/"+dtos(std::floor(zsr),0)+"z";
          std::string annotation = dtoi(perc)+"%/"+dtos(std::floor(zsr),0)+"z";
          std::string peak = "";
          if (io_use_cluster)
          {
            double pkval = rotfun.rotation_function_data[item.clusterNum].value;
            double pkperc = 100.*(pkval-rotfun.f_mean)/(rotfun.f_max-rotfun.f_mean);
            double pkzsr = (pkval-rotfun.f_mean)/rotfun.f_sigma;
            PHASER_ASSERT(val <= pkval);
            if (val < pkval)
              peak = "^"+dtoi(pkval)+"/"+dtoi(pkperc)+"%/"+dtos(std::floor(pkzsr),0)+"z";
            next.PERCENT = pkperc; //fast search score, not the clustered score
          }
          next.LLG = val; //fast search score, not the clustered score
          if (signal_resolution)
            next.SIGNAL_RESOLUTION = signal_resolution;
          next.FSS = 'Y'; //fast search score FSS
          next.ZSCORE = zsr;
          next.RFACTOR = 0; //very important reset for automation
          next.ANNOTATION += " FIND="+search->identify().tag();
          next.ANNOTATION += " RF="+annotation+peak;
          next.NUM = n-1; //RF number
          if (!next.PARENTS.size())
            next.PARENTS = {0,0}; //resize and init
         // if (v==2) //first vrms estimate
          {
            next.PARENTS[0] = next.PARENTS[1]; //front
            next.PARENTS[1] = parent.identifier(); //back
          }
          dmat33 PR = search->PRINCIPAL_ORIENTATION;
          std::tie(next.NEXT.EULER,next.NEXT.SHIFT_MATRIX) = rotfun.turn_best_sym(i,PR);
          output_dag.push_back(next);
        }
        logEllipsisShut(where);
        }}
        parent.increment("frf"+ntos(n-1,DAGDB.size()));
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);

      if (stats.size() > 1)
      {
        out::stream where = out::VERBOSE;
        logUnderLine(where,"Histograms of Statistics");
        stats.calculate_histogram_mean();
        logHistogram(where,stats.histdat,10,false,"TF means",stats.histmin,stats.histmax);
        stats.calculate_histogram_sigma();
        logHistogram(where,stats.histdat,10,false,"TF sigmas",stats.histmin,stats.histmax);
      }

      logUnderLine(out::LOGFILE,"Merged Results");
      double lowest_mean = stats.merged_lowest_mean();
      bool hasnovel; double top_novel;
      std::tie(hasnovel,top_novel) = io_use_top_not_in_pose ? output_dag.top_novel_turn_llg_packs() : output_dag.top_llg_packs();
      //if not hasnovel then we will not purge by percent
      //however, we need to calculate percentage
      if (!hasnovel)
        std::tie(std::ignore,top_novel) = output_dag.top_llg_packs();
      logTab(out::TESTING,"Has novel="+ btos(hasnovel));
      logTab(out::TESTING,"Lowest mean="+ dtos(lowest_mean));
      if (hasnovel) logTab(out::TESTING,"Top novel="+ dtos(top_novel));
      bool hasvalue; double top;
      std::tie(hasvalue,top) = output_dag.top_llg_packs();
      //not hasvalue has topvalue smallest
      logTab(out::TESTING,"Has value="+ btos(hasvalue));
      if (hasvalue) logTab(out::TESTING,"Top ="+ dtos(top));
      bool lowered_top(false);
      if (input.size(".phasertng.fast_rotation_function.maps.id."))
      {
        out::stream where = out::LOGFILE;
        logTab(where,"Rotation function performed over list of maps");
        logTab(where,"No purge of merged orientations");
      }
      else
      {
        out::stream where = out::LOGFILE;
        (io_use_top_not_in_pose) ?
          logTab(where,"Purge by percent with respect to top novel orientation llg"):
          logTab(where,"Purge by percent with respect to top orientation llg");
        if (hasvalue and hasnovel and top == top_novel)
        {
          logTab(where,"Top orientation is not in pose list");
          logTab(where,"Purge by percent with respect to top orientation llg");
        }
        else if (hasnovel and hasvalue)
        {
          lowered_top = true;
          logTab(where,"Top orientation is in pose list");
          logTab(where,"Purge by percent with respect to top novel orientation llg");
          logTab(where,"top overall = " + dtos(top,5,2) + " ("  + dtos(100*(top-lowest_mean)/(top_novel-lowest_mean),5,2) + "%)");
          logTab(where,"top novel   = " + dtos(top_novel,5,2) + " (100%)");
        }
        logEllipsisOpen(where,"Purge by maximum and minimum percent of FSS");
        int before = output_dag.size();
        if (hasnovel)
        {
          double cut = output_dag.apply_partial_sort_and_percent_llg_range(
                         {io_percent,io_percent_max},
                         top_novel,
                         lowest_mean);
          logEllipsisShut(where);
          logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
          logTab(where,"Top/Mean/Cutoff FSS:  " + dtos(top_novel,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
        }
        logTab(where,"Number until purge:   " + itos(before));
        logTab(where,"Number after purge:   " + itos(output_dag.size()));
      }

      if (lowered_top)
       logAdvisory("Top orientation was in pose list; peak lowered");

      {{
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge by maximum stored");
      int before = output_dag.size();
      output_dag.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored:     " + itos(io_maxstored));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(output_dag.size()));
      double minperc = std::numeric_limits<double>::max();
      if (hasnovel or hasvalue)
      {
        for (auto& node : output_dag)
        {
          double tmp = hasnovel ? top_novel : top;
          double perc = 100*(node.LLG-lowest_mean)/(tmp-lowest_mean);
          minperc = std::min(minperc,perc);
        }
        logTab(where,"Minimum percent after purge:  " + dtos(minperc,2) + "%");
      }
      //There are cases with no hasnovel and no hasvalue
      }}

      {{
      out::stream where = out::LOGFILE;
      Loggraph loggraph;
      loggraph.title = "Top peaks in Fast Rotation Function";
      loggraph.scatter = true;
      loggraph.graph.resize(1);
      loggraph.graph[0] = "RF Number vs LL-gain:AUTO:1,2";
      int i(1);
      for (auto& item : output_dag)
      {
        loggraph.data_text += itos(i++) + " " + dtos(item.LLG,10,2) + "\n";
      }
      logGraph(where,loggraph);
      }}

      DAGDB.NODES = output_dag;
      DAGDB.reset_entry_pointers(DataBase());
      DAGDB.shift_tracker(hashing(),input.running_mode);

      bool unique_search(true);
      for (auto& node : DAGDB.NODES)
      {
        auto& fnode = DAGDB.NODES.front();
        if (fnode.REFLID  != node.REFLID) unique_search = false;
        if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
      }

      {{
      int w = std::max(2,itow(io_nprint)); //-2
      int ww = itow(maxrf,3);
      out::stream where = out::LOGFILE;
      logTableTop(where,"Fast Rotation Function Results");
      logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
      logTab(where,"Rotations with respect to original coordinates");
      std::set<std::string> ensembles;
      for (auto& node : DAGDB.NODES)
        ensembles.insert(node.NEXT.IDENTIFIER.str());
      int j(1);
      for (auto ens : ensembles)
        logTab(where,"Model #" + itos(j++) + " " +  ens);
      int z = DAGDB.size();
      logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
      logTab(where,"--------");
      int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using llg here
      int q = DAGDB.NODES.llg_width(false); //even if it is a map, we are using llg here
      if (DAGDB.size())
      {
        logTab(where,"#       Rank of the peak");
        logTab(where,"RF#     Background number of the search");
        logTab(where,"--------");
        logTab(where,snprintftos(
            "%-*s %-*s %2s %s %*s %5s %7s  %6s=%-5s %-13s %*s %*s",
            w,"#",
            ww,"RF#",
            "",
            dag::header_polar().c_str(),
            q,"FSS",
            "FSS%","RFZ",
            "DRMS","VRMS",
            "Space Group",
            unique_search ? 0:uuid_w,unique_search ? "":"modlid",
            unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
      }
      //dmat33 PR = search->PRINCIPAL_ORIENTATION;
      int i(1);
      for (auto& node : DAGDB.NODES)
      {
        //store the angle with the lowest Euler2, lowest Euler3
        auto& turn = node.NEXT;
        //perc can only be calculated for hasnovel
        //use node.PERCENT
        double perc = hasnovel ?
            100*(node.LLG-lowest_mean)/(top_novel-lowest_mean):
            node.PERCENT;
        phaser_assert(turn.ENTRY->VRMS_START.size()>0);
        const auto& modlid = turn.IDENTIFIER;
        PHASER_ASSERT(node.DRMS_SHIFT.count(modlid));
        double initial = turn.ENTRY->VRMS_START[0]; //alphafold esm
        double drms = node.DRMS_SHIFT[modlid].VAL;
        double vrms = pod::apply_drms_to_initial(drms,initial);
        logTab(where,snprintftos(
            "%*d %*d %2s %s %*.*f %5.1f %7.2f  %+6.2f=%05.3f %-13s %*s %*s",
            w,i,
            ww,node.NUM,
            "",
            dag::print_polar(turn.SHIFT_MATRIX).c_str(),
            q,p,node.LLG,
            perc,
            node.ZSCORE,
            drms,vrms,
            node.SG->sstr().c_str(),
            unique_search ? 0:uuid_w,unique_search ? "":modlid.string().c_str(),
            unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
        logTab(where,snprintftos(
            "%12s EULER=[% 6.4f % 6.4f % 6.4f]",
            "",turn.EULER[0],turn.EULER[1],turn.EULER[2]));
#endif
        for (int isym = 0; isym < REFLECTIONS->SG.NSYMP; isym++)
        {
          dmat33 MROT = turn.SHIFT_MATRIX;
          dmat33 Rsym = REFLECTIONS->UC.orth_Rtr_frac(REFLECTIONS->SG.rotsym[isym])*MROT;
          dvect3 euler = scitbx::math::euler_angles::zyz_angles(Rsym);
          euler = regularize_angle(euler);
          logTab(out::VERBOSE,snprintftos(
              "%-*s%2d %s RzRyRz(% 7.1f % 7.1f % 7.1f)",
              w-2+1+ww+1,"Sym#",isym+1, //-2 is hack for tab, reduce to line up in table
              dag::print_polar(Rsym).c_str(),
              euler[0],euler[1],euler[2]));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
          { //new declarations
          dmat33 MROT = zyz_matrix(turn.EULER);
          dmat33 Rsym = REFLECTIONS->UC.orth_Rtr_frac(REFLECTIONS->SG.rotsym[isym])*MROT;
          dvect3 euler = scitbx::math::euler_angles::zyz_angles(Rsym);
          euler = regularize_angle(euler);
          logTab(where,snprintftos(
              "%12s EULER=[% 6.1f % 6.1f % 6.1f]",
              "",euler[0],euler[1],euler[2]));
          }
#endif
        }
        if (i == io_nprint)
        {
          if (DAGDB.size() > io_nprint)
            logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
          break;
        }
        i++;
      }
      logTableEnd(where);
      }}

      {{
      out::stream where = out::SUMMARY;
      logTableTop(where,"Fast Rotation Function Summary");
      logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
      logTab(where,"Last placed: " + search->identify().str());
      logTab(where,"Percent cutoff: " + dtos(io_percent,1) + "%");
      logTab(where,"(Z-scores from Fast Rotation Function)");
      int z = DAGDB.size();
      logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
      int ww = itow(maxrf,3);
      int p = DAGDB.NODES.llg_precision(false);
      int q = std::max(7,DAGDB.NODES.llg_width(false));
      if (DAGDB.size())
      {
        logTab(where,snprintftos(
            "%-*s %*s %*s %5s %7s %*s %5s %7s %*s %5s %7s  %6s=%-5s %-13s %*s %*s",
            ww,"#",ww,"RF#",
            q,"Top-FSS","(%)","(Z)",q,"Second","(%)","(Z)",q,"Third","(%)","(Z)",
            "DRMS","VRMS","Space Group",
            unique_search ? 0:uuid_w,unique_search ? "":"modlid",
            unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
      }
      auto set_of_parents = DAGDB.set_of_parents();
      int i(0);
      for (auto parent : set_of_parents)
      {
        //group the gyres by background so that the ecalcs_sigmaa_pose is done first time only
        DAGDB.restart(); //very important
        int j(0);
        Identifier modlid;
        double drms,vrms;
        std::string line = itos(i+1,ww,false,false) + " ";
        bool first(true);
        std::string sg;
        while (!DAGDB.at_end())
        {
          auto node = DAGDB.WORK;
          if (node->PARENTS.back() != parent)
            continue;
          if (first)
          { //print the rf number not the index of the hit (i)
            line += itos(node->NUM,ww,false,false) + " ";
            first = false;
            sg = node->SG->sstr();
          }
          modlid = node->NEXT.IDENTIFIER;
          double initial = node->NEXT.ENTRY->VRMS_START[0]; //alphafold esm
          drms = node->DRMS_SHIFT[modlid].VAL;
          vrms = pod::apply_drms_to_initial(drms,initial);
          double perc = hasnovel ?
            100*(node->LLG-lowest_mean)/(top_novel-lowest_mean):
            node->PERCENT;
          line += snprintftos("%*.*f %5.1f % 7.2f ",q,p,node->LLG,perc,node->ZSCORE);
          j++;
          if (j==3) break;
        }                                          //LLG    perc  zscore
        if (j<1) line += snprintftos("%*s %5s %7s ",q,"---","---","---");
        if (j<2) line += snprintftos("%*s %5s %7s ",q,"---","---","---");
        if (j<3) line += snprintftos("%*s %5s %7s ",q,"---","---","---");
        if (j>0) line += snprintftos(" %+6.2f=%05.3f %-13s %*s %*s",
                    drms,vrms,
                    sg.c_str(),
                    unique_search ? 0:uuid_w,unique_search ? "":"modlid",
                    unique_search ? 0:uuid_w,unique_search ? "":"reflid");
        logTab(where,line);
        i++;
      }
      logTableEnd(where);
      }}

      if (false)
      {{
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Original orientation");
      bool found(false);
      for (int i = 0; i < DAGDB.NODES.size(); i++)
      {
        auto line = dag::print_polar(DAGDB.NODES[i].NEXT.SHIFT_MATRIX);
        if (hoist::algorithm::ends_with(line,"[  0.0]"))
        {
          dmat33 MROT = zyz_matrix(DAGDB.NODES[i].NEXT.EULER);
          auto euler = DAGDB.NODES[i].NEXT.EULER;
          logTab(where,snprintftos(
              "%6d %s RzRyRz(% 7.1f % 7.1f % 7.1f)",
              i+1,
              line.c_str(),
              euler[0],euler[1],euler[2]));
          found = true;
        }
      }
      if (!found) logTab(where,"None");
      }}

      if (vrms_estimates.size() > 1)
      {
        vrms_estimate_output_dag.push_back(DAGDB.NODES);
        logTab(out::VERBOSE,"Stored results for this vrms estimate");
      }
    }

    if (vrms_estimates.size() > 1)
    {
      //intercalate the arrays
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Intercalate Rotation Function Results");
      size_t maxs = 0;
      for (int i = 0; i < vrms_estimate_output_dag.size(); i++)
      {
        maxs = std::max(maxs,vrms_estimate_output_dag[i].size());
        logTab(where,"Number vrms estimates #" + itos(i+1) + " " + itos(vrms_estimate_output_dag[i].size()));
      }
      dag::NodeList final_nodelist;
      for (int s = 0; s < maxs; s++)
        for (int i = 0; i < vrms_estimates.size(); i++)
          if (s < vrms_estimate_output_dag[i].size())
            final_nodelist.push_back(vrms_estimate_output_dag[i][s]);
      DAGDB.NODES = final_nodelist;
      logBlank(where);
      logTab(where,"Number of all vrms estimates: " + itos(DAGDB.size()));
    }
  }

} //phasertng
