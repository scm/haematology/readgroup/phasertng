from __future__ import print_function
Import("env_base", "env_etc")
import libtbx.load_env
import time
import sys, os, os.path
import subprocess, re
import stat
import SCons

# Change below to recompile without c++ directory creation
# This is a fallback option only
# Change in response to segmentation fault from phasertng with the error message to stderr
#"Your system does not support c++ coded directory creation"
#"To continue using phasertng please recompile with create_directories = False in SConscript"
create_directories = True


#==========
top_dir = "phasertng"

#this cannot be write_version because it will find phaser path write_version first!
sys.path.append(os.path.join(libtbx.env.dist_path(top_dir), 'scons_scripts'))
from write_tng_version_cc import *

# http://www.boost.org/libs/python/doc/v2/configuration.html
phasertng_boost_python_max_bases = 48

os.system("find . -name \"*.pyc*\" -exec rm -rf {} \;")

env_etc.phasertng_dist = libtbx.env.dist_path(top_dir)
#env_etc.modules = env_etc.norm_join(env_etc.phasertng_dist, "..")
env_etc.phasertng_codebase = env_etc.norm_join(env_etc.phasertng_dist, "codebase")
dict = env_base.Dictionary()

cxxflags = env_etc.cxxflags_base
# Command line options  to libtbx.SCons
cmdlineflags = ['-Wno-unused-function','-Wno-reorder','-Wno-deprecated-declarations']
# Add any commandline flags to the default C++ flags
if env_etc.compiler == "unix_gcc":
  cxxflags.extend(cmdlineflags)

ccp4io_lib = getattr(env_etc, "ccp4io_lib", "cmtz")

phasertng_env = env_base.Clone()

phasertng_env.header_dir         = os.path.join(libtbx.env.dist_path(top_dir),"codebase",top_dir,"main")
phasertng_env.autogen_include    = os.path.join(libtbx.env.dist_path(top_dir),"codebase","autogen","include")
phasertng_env.phasertng_build_code = os.path.join(libtbx.env.under_build(top_dir),"codebase",top_dir,"../phaser*") # phasertng,phaser
phasertng_env.phasertng_build_auto = os.path.join(libtbx.env.under_build(top_dir),"codebase","autogen")

#google formula for checking library present
phasertng_env = phasertng_env.Clone()
conf = phasertng_env.Configure()
if conf.CheckLib('phaserncs') and libtbx.env.dist_path("phaser") is not None:
     use_phasertng_ncs_library = False
     print("Found phaserncs library")
     ncslibname = "phaserncs"
else:
     use_phasertng_ncs_library = True
     print("No phaserncs library")
     ncslibname = "phasertng_ncs"
conf.Finish()

def GlobRecursive(pattern, node='.'):
    results = []
    for f in Glob(str(node) + '/*', source=True):
      if type(f) is SCons.Node.FS.Dir:
        results += GlobRecursive(pattern, f)
    results += Glob(str(node) + '/' + pattern, source=True)
    return sorted(results)
#devv
exe_sources_code = GlobRecursive(node=phasertng_env.phasertng_build_code, pattern='*.cc')
exe_sources_auto = GlobRecursive(node=phasertng_env.phasertng_build_auto, pattern='*.cc')
#devv
phasertng_env.boost_thread_uses_winthreads = False
#devv
if (libtbx.env.build_options.enable_boost_threads and env_etc.boost_thread_support):
  phasertng_env.boost_thread_uses_winthreads = (sys.platform == 'win32')
#devv
if (sys.platform == "win32" and env_etc.compiler == "win32_cl"):
  boost_version = int(libtbx.env.boost_version)
  if boost_version < 107400:
    print("\nError! BOOST version is %i. Phasertng requires 107400 or higher for building on Windows." %boost_version)
    sys.exit(-42)
#devv
#devv
AddOption("--dev",
             action="store_true",
             dest="development",
             default=False,
             help='Compile with on-the-fly file generation')
AddOption("--check",
             action="store_true",
             dest="check",
             default=False,
             help='Compile with on-the-fly file checking')
#devv
if GetOption('development'):
  print("phasertng: Development Version")
  sys.path.append(os.path.join(libtbx.env.dist_path(top_dir), 'scons_scripts'))
  from write_scope import *
  from write_input import *
  from write_voyager_doc import *
  from check_keywords import *
  from subprocess import call
#devv
  env_etc.phasertng_common_libs = [
    "phasertng_main",
    ncslibname,
    ccp4io_lib,
    "cctbx",
    "iotbx_pdb",
    "mmtbx_masks",
    "cctbx_sgtbx_asu",
  ]
#devv
  env_etc.phasertng_common_libs.extend(["boost_system","boost_filesystem"])
  if phasertng_env.boost_thread_uses_winthreads:
    env_etc.phasertng_common_libs.extend(["boost_thread",])
#devv
#devv
  if (use_phasertng_ncs_library):
    env_etc.phasertng_ncs = env_etc.norm_join(env_etc.phasertng_dist)
  else:
    env_etc.phasertng_ncs = libtbx.env.dist_path("phaser")
  env_etc.phasertng_common_includes = [
    env_etc.phasertng_codebase,
    env_etc.phasertng_ncs,
    env_etc.libtbx_include,
    env_etc.cctbx_include,
    env_etc.scitbx_include,
    env_etc.boost_adaptbx_include,
    env_etc.boost_include,
    env_etc.ccp4io_include,
    env_etc.iotbx_include,
  ]
  if (sys.platform == "win32" and env_etc.compiler == "win32_cl"):
    env_etc.phasertng_common_includes.append(os.path.join(env_etc.cctbx_include,"msvc9.0_include"))
#devv
  env_etc.phasertng_common_libpath = [
    "#lib",
  ]
#devv
  # conda library paths (header paths already handled with env_etc.boost_include)
  if libtbx.env.build_options.use_conda:
    env_etc.phasertng_common_libpath = env_etc.conda_libpath + env_etc.phasertng_common_libpath
#devv
  if (hasattr(env_etc, "omptbx_include")): # XXX backward compatibility 2008-06-06
    env_etc.phasertng_common_libs.append("omptbx")
    env_etc.phasertng_common_includes.insert(5, env_etc.omptbx_include)
#devv
  phasertng_env.Replace(
  CXXFLAGS=cxxflags,
  CCFLAGS=env_etc.ccflags_base,
  LIBS=env_etc.phasertng_common_libs + env_etc.libm,
  LIBPATH=env_etc.phasertng_common_libpath,
    )
  #InputCard.cc hangs compiler with debug options with tracking assignments enabled
 #phasertng_env.Append(CCFLAGS='-ftime-report')
 #phasertng_env.Append(CCFLAGS='-ftime-trace')
 #phasertng_env.Append(CCFLAGS='-H')
  if not create_directories:
    phasertng_env.Append(CCFLAGS='-DPHASERTNG_USE_FLAT_FILESYSTEM')
  if env_etc.compiler == "unix_gcc":
    phasertng_env.Append(CCFLAGS='-fno-var-tracking-assignments')
    phasertng_env.Append(CCFLAGS='-DBOOST_BIND_GLOBAL_PLACEHOLDERS')
    phasertng_env.Append(CCFLAGS='-DBOOST_ALLOW_DEPRECATED_HEADERS')
    phasertng_env.Append(CCFLAGS='-DDEBUG_USE_PHASER_ASSERT')
    phasertng_env.Append(CCFLAGS='-DDEBUG_PHASERTNG_STOP_WITH_NEGVAR')

 #if env_etc.compiler != "win32_cl":
 #  phasertng_env.Append(CCFLAGS='-ffp-contract=off')
 #  phasertng_env.Append(CCFLAGS='-fno-fast-math')

#devv
  if env_etc.compiler == "win32_cl":
    phasertng_env.Append(CCFLAGS=env_etc.ccp4io_defines)
    phasertng_env.Append(CCFLAGS="/wd4267") # warning C4267: conversion from 'size_t' to 'int', possible loss of data
    phasertng_env.Append(CXXFLAGS=env_etc.ccp4io_defines)
    phasertng_env.Append(LINKFLAGS=["/DEBUG"])
    phasertng_env.Replace(RCCOM="$RC $_CPPDEFFLAGS $_CPPINCFLAGS $RCFLAGS $SOURCES")
    phasertng_env.Prepend(LIBS=["Advapi32"])
    # Output file of resource compiler defaults to the wrong directory. Include path for versionno.h
    phasertng_env.Append(RCFLAGS=["/I", Dir('#.').abspath + "\phasertng", "/fo", Dir('#.').abspath + "/phasertng/phasertng.res"])
#devv
  env_etc.include_registry.append(
    env=phasertng_env,
    paths=env_etc.phasertng_common_includes)
  Export("phasertng_env")
  env_etc.phasertng_all_libs = []
  env_etc.phasertng_all_libs.append(
     phasertng_env.StaticLibrary(
        target="#lib/phasertng_main",
        source=exe_sources_code
    ))
#devv
  if use_phasertng_ncs_library:
     SConscript("./ncs/SConscript")
 #SConscript("./ncs/SConscript")
  #Note: order of linking can affect python boosting as well as c++
 #SConscript("./codebase/autogen/SConscript")
  #make this phasertng_autogen lib even though we are not going to use it
  #make it here so that non-dev doesn't rebuild all
  #ps but if we do this the build fails if the files are changed, so leave out
#devv
  # Build phasertng executable:
  # Not making a direct dependency on the version file as we don't want version file to be compiled when
  # phasertng sources haven't changed. Instead we cheat by appending the linkerflag of the phasertng executable
  # with the compiled object of the version file.
#devv
  #directories
  #AUTOGENERATED cannot be under phasertng, as this will make phasertng dir already present and give scons error
  phasertng_env.autogen    = os.path.join(libtbx.env.under_build("phasertng_autogen"))
  phasertng_env.dist_path  = libtbx.env.dist_path(top_dir)
  if not os.path.exists(phasertng_env.autogen):
    os.makedirs(phasertng_env.autogen)
  #Autogenerated files
  phasertng_env.version_cc     = os.path.join(phasertng_env.autogen,"Version.cc")
  phasertng_env.scope_cc       = os.path.join(phasertng_env.autogen,"Scope.cc")
  phasertng_env.inputcard_cc   = os.path.join(phasertng_env.autogen,"InputCard.cc")
  phasertng_env.inputcard_name = "InputCard"
  #Autogenerated-associated object files for linking
  phasertng_env.version_obj    = os.path.join(phasertng_env.autogen,"Version" + dict['SHOBJSUFFIX'])
  phasertng_env.scope_obj      = os.path.join(phasertng_env.autogen,"Scope" + dict['SHOBJSUFFIX'])
  phasertng_env.inputcard_obj  = os.path.join(phasertng_env.autogen,"InputCard" + dict['SHOBJSUFFIX'])
  #Parameters for actions
  phasertng_env.phasertng_path = libtbx.env.dist_path(top_dir)
  phasertng_env.objsufx = dict['SHOBJSUFFIX']
  phasertng_env.compiler = env_etc.compiler
#devv
  def modify_targets(target, source, env):
      target.append(phasertng_env.version_cc)
      target.append(phasertng_env.inputcard_cc)
      target.append(phasertng_env.scope_cc)
      return target, source
#devv
  bld = Builder(action = [write_tng_version_cc,
                          write_scope_cc,
                          write_input_cc,
                          write_voyager_doc,
                          ], emitter = modify_targets)
  phasertng_env.Append(BUILDERS = {'GenerateFiles' : bld})
#devv
  phasertng_env.GenerateFiles()
#devv
  print("phasertng: Building development version")
  #files used in analysis
  phasertng_env.py  = os.path.join(libtbx.env.dist_path(top_dir),"phasertng")
  phasertng_env.master_phil_param_file      = os.path.join(phasertng_env.py,"phil","master_phil_file.py")
  assert os.path.isfile(phasertng_env.master_phil_param_file)
#devv
#devv
  # Specify version object file as a linker flag as to trick SCons to include it in the executable
  phasertng_env.Append(LINKFLAGS=phasertng_env.version_obj)
#devv
  phasertng_env.res = os.path.join(phasertng_env.autogen,"phasertng.res")
  if env_etc.compiler == "win32_cl":
    # Specify resource object file as a linker flag as to trick SCons to include it in the executable
    phasertng_env.Append(LINKFLAGS=phasertng_env.res)
#devv
  exe_sources_dev = []
  exe_sources_dev.append(phasertng_env.inputcard_cc)
  exe_sources_dev.append(phasertng_env.scope_cc)
  #ALSO EDIT SCONSCRIPT IN BOOST DIRECTORY FOR LINKING OF NEW OBJECTS
#devv
  code_obj = phasertng_env.Object(exe_sources_code)
  if env_etc.compiler == "win32_cl":
    fastflags = phasertng_env['CCFLAGS'] + ["-Od"]
  else:
    fastflags = phasertng_env['CCFLAGS'] + ["-O0"]
  auto_obj = phasertng_env.Object(exe_sources_dev, CCFLAGS=fastflags)
  exe_sources = code_obj + auto_obj
#devv
  if ( libtbx.env.dist_path(module_name="phenix", default=None) is not None):
    print("phasertng: phenix module")
    phasertng_env.exe = phasertng_env.Program(target="#phasertng/exe/phasertng", source= exe_sources)
    phasertng_exepath = phasertng_env.exe[0].get_abspath()
  else:
    print("phasertng: not phenix module")
    phasertng_env.exe = phasertng_env.Program(target="#exe/phasertng", source= exe_sources)
    phasertng_exepath = phasertng_env.exe[0].get_abspath()
    libtbx.env.write_dispatcher_in_bin(
      source_file=phasertng_exepath,
      target_file=top_dir)
#devv
  # Ensure that actions are called if the dependency file changes
  Depends(phasertng_env.exe,    "SConscript")
  Depends(phasertng_env.exe,    "scons_scripts/write_tng_version_cc.py")
  Depends(phasertng_env.exe,    "CHANGELOG")
  Depends(phasertng_env.exe,    "VERSION")
  Depends(phasertng_env.exe,    "PROGRAM")
  Depends(phasertng_env.exe,    os.path.join(phasertng_env.header_dir,"InputBase.h"))
  Depends(phasertng_env.exe,    os.path.join(phasertng_env.header_dir,"Version.h"))
  Depends(phasertng_env.exe,    os.path.join(phasertng_env.autogen_include,"InputCard.h"))
  Depends(phasertng_env.exe,    os.path.join(phasertng_env.autogen_include,"Scope.h"))
  Depends(phasertng_env.exe,    "scons_scripts/write_input.py")
  Depends(phasertng_env.exe,    "scons_scripts/write_scope.py")
  Depends(phasertng_env.exe,    "scons_scripts/write_voyager_doc.py")
  Depends(phasertng_env.exe,    phasertng_env.master_phil_param_file)
#devv
  Depends(phasertng_env.scope_obj,   "scons_scripts/write_scope.py")
  Depends(phasertng_env.scope_obj,    os.path.join(phasertng_env.autogen_include,"Scope.h"))
#devv
  Depends(phasertng_env.inputcard_obj,   "scons_scripts/write_input.py")
  Depends(phasertng_env.inputcard_obj,    phasertng_env.master_phil_param_file)
  Depends(phasertng_env.inputcard_obj,    os.path.join(phasertng_env.header_dir,"InputBase.h"))
  Depends(phasertng_env.inputcard_obj,    os.path.join(phasertng_env.autogen_include,"InputCard.h"))
#devv
  # Ensure files compiled strictly before phasertng executable is linked
  #otherwise get python import errors
  phasertng_env.AddPreAction(phasertng_env.version_obj,
                                 action=write_tng_version_cc)
  phasertng_env.AddPreAction(phasertng_env.exe, action=write_voyager_doc)
  phasertng_env.AddPreAction([phasertng_env.inputcard_obj],
                                 action=write_input_cc)
#devv
  phasertng_env.Requires(phasertng_exepath, phasertng_env.version_obj)
  phasertng_env.Requires(phasertng_exepath, phasertng_env.inputcard_obj)
  phasertng_env.Requires(phasertng_exepath, phasertng_env.scope_obj)
#devv
  # Add a post-build on windows step to embed the manifest using mt.exe
  # The number at the end of the line indicates the file type (1: EXE; 2:DLL).
  if (env_etc.compiler == "win32_cl" and float(phasertng_env["MSVC_VERSION"]) < 10.0 ):
    phasertng_env.AddPostAction(phasertng_exepath, 'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;1')
  if GetOption('check'):
    print('phasertng: code will be checked for phil input parameter errors')
    phasertng_env.AddPostAction(phasertng_exepath, action=check_keywords)
#devv
  if (not env_etc.no_boost_python):
    #COOK BOOK FOR MAKING BOOST-PYTHON MODULE
    Import("env_no_includes_boost_python_ext") #from libtbx
  # Add any commandline flags to the default C++ flags
    patched_shcxxflags = []
    if env_etc.compiler == "unix_gcc":
      patched_shcxxflags = cmdlineflags
    for item in env_no_includes_boost_python_ext["SHCXXFLAGS"]:
      if (item.startswith("-DBOOST_PYTHON_MAX_BASES=")):
        patched_shcxxflags.append("-DBOOST_PYTHON_MAX_BASES=%d" %
          phasertng_boost_python_max_bases)
      else:
        patched_shcxxflags.append(item)
    env_phasertng_boost_python_ext = env_no_includes_boost_python_ext.Clone(
      SHCXXFLAGS=patched_shcxxflags)
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=["-DBOOST_PYTHON_MAX_ARITY=22"])
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=["-DBOOST_ALLOW_DEPRECATED_HEADERS"])
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=["-DBOOST_BIND_GLOBAL_PLACEHOLDERS"])
    #env_phasertng_boost_python_ext.Prepend(LIBS=["cctbx_sgtbx_asu"])
    env_etc.include_registry.append(
      env=env_phasertng_boost_python_ext,
      paths=env_etc.phasertng_common_includes+[env_etc.python_include]) #from libtbx
    Export("env_phasertng_boost_python_ext")
    SConscript("codebase/phasertng/boost_python/SConscript")
#devv
#xmlfile=open(os.path.join(libtbx.env.under_dist(top_dir,"ccp4i2"),"phasertng_MR_AUTO.xml"),"w")
#subprocess.check_call((os.path.join(libtbx.env.under_build("bin"),"phasertng.phil_as_xml"),"--mode=MR_AUTO"),stdout=xmlfile)
#xmlfile.close()

#sys.exit(1)

else:
  print("phasertng: Distributed version")

  env_etc.phasertng_common_libs = [
    "phasertng_main",
    "phasertng_autogen",
    ncslibname,
    ccp4io_lib,
    "cctbx",
    "iotbx_pdb",
    "mmtbx_masks",
    "cctbx_sgtbx_asu",
  ]

  env_etc.phasertng_common_libs.extend(["boost_system","boost_filesystem"])
  if phasertng_env.boost_thread_uses_winthreads:
    env_etc.phasertng_common_libs.extend(["boost_thread",])

  if (use_phasertng_ncs_library):
    env_etc.phasertng_ncs = env_etc.norm_join(env_etc.phasertng_dist)
  else:
    env_etc.phasertng_ncs = libtbx.env.dist_path("phaser")
  env_etc.phasertng_common_includes = [
    env_etc.phasertng_codebase,
    env_etc.phasertng_ncs,
    env_etc.libtbx_include,
    env_etc.cctbx_include,
    env_etc.scitbx_include,
    env_etc.boost_adaptbx_include,
    env_etc.boost_include,
    env_etc.ccp4io_include,
    env_etc.iotbx_include,
  ]
  if (sys.platform == "win32" and env_etc.compiler == "win32_cl"
        and env_etc.msvc_version < 14):
    env_etc.phasertng_common_includes.append(os.path.join(env_etc.cctbx_include,"msvc9.0_include"))

  env_etc.phasertng_common_libpath = [
    "#lib",
  ]

  # conda library paths (header paths already handled with env_etc.boost_include) DUPLICATE
  if libtbx.env.build_options.use_conda:
    env_etc.phasertng_common_libpath = env_etc.conda_libpath + env_etc.phasertng_common_libpath

  if (hasattr(env_etc, "omptbx_include")): # XXX backward compatibility 2008-06-06
    env_etc.phasertng_common_libs.append("omptbx")
    env_etc.phasertng_common_includes.insert(5, env_etc.omptbx_include)

  env_etc.ccp4io_dist = libtbx.env.dist_path("ccp4io")

  phasertng_env.Replace(
  CXXFLAGS=cxxflags,
  CCFLAGS=env_etc.ccflags_base,
  LIBS=env_etc.phasertng_common_libs + env_etc.libm,
  LIBPATH=env_etc.phasertng_common_libpath,
    )
  #InputCard.cc hangs compiler with debug options with tracking assignments enabled
 #phasertng_env.Append(CCFLAGS='-ftime-report')
 #phasertng_env.Append(CCFLAGS='-ftime-trace')
 #phasertng_env.Append(CCFLAGS='-H')
  if not create_directories:
    phasertng_env.Append(CCFLAGS='-DPHASERTNG_USE_FLAT_FILESYSTEM')
  if env_etc.compiler == "unix_gcc":
    phasertng_env.Append(CCFLAGS='-fno-var-tracking-assignments')
    phasertng_env.Append(CCFLAGS='-DBOOST_BIND_GLOBAL_PLACEHOLDERS')
    phasertng_env.Append(CCFLAGS='-DBOOST_ALLOW_DEPRECATED_HEADERS')

 #if env_etc.compiler != "win32_cl":
 #  phasertng_env.Append(CCFLAGS='-ffp-contract=off')
 #  phasertng_env.Append(CCFLAGS='-fno-fast-math')

  if env_etc.compiler == "win32_cl":
    phasertng_env.Append(CCFLAGS=env_etc.ccp4io_defines)
    phasertng_env.Append(CCFLAGS="/wd4267") # warning C4267: conversion from 'size_t' to 'int', possible loss of data
    phasertng_env.Append(CXXFLAGS=env_etc.ccp4io_defines)
    phasertng_env.Append(LINKFLAGS=["/DEBUG"])
    phasertng_env.Replace(RCCOM="$RC $_CPPDEFFLAGS $_CPPINCFLAGS $RCFLAGS $SOURCES")
    phasertng_env.Prepend(LIBS=["Advapi32"])
    # Output file of resource compiler defaults to the wrong directory. Include path for versionno.h
    phasertng_env.Append(RCFLAGS=["/I", Dir('#.').abspath + "\phasertng", "/fo", Dir('#.').abspath + "/phasertng/phasertng.res"])

  env_etc.include_registry.append(
    env=phasertng_env,
    paths=env_etc.phasertng_common_includes)
  Export("phasertng_env")
  env_etc.phasertng_all_libs = []
  env_etc.phasertng_all_libs.append(
     phasertng_env.StaticLibrary(
        target="#lib/phasertng_main",
        source=exe_sources_code
    ))
  if use_phasertng_ncs_library:
     SConscript("./ncs/SConscript")
  #Add the SConscript for the autogenerated files that are distributed
  SConscript("./codebase/autogen/SConscript")
  #Note: order of linking can affect python boosting as well as c++

  phasertng_env.objsufx = dict['SHOBJSUFFIX']
  phasertng_env.compiler = env_etc.compiler
  phasertng_env.phasertng_path = libtbx.env.dist_path(top_dir)
  phasertng_env.autogen    = os.path.join(libtbx.env.under_build("phasertng_autogen"))
  phasertng_env.version_obj    = os.path.join(phasertng_env.autogen,"Version" + dict['SHOBJSUFFIX'])
  phasertng_env.version_cc     = os.path.join(phasertng_env.autogen,"Version.cc")

  # Build phasertng executable:
  # Not making a direct dependency on the version file as we don't want version file to be compiled when
  # phasertng sources haven't changed.
  # Instead we cheat by appending the linkerflag of the phaser executable
  # with the compiled object of the version file.

  print("phasertng: Building shared version")
  phasertng_env.makeSharedLibrary = True
  # Specify version object file as a linker flag as to trick SCons to include it in the executable  phasertng_env.Append(LINKFLAGS=phasertng_env.version_obj)
  phasertng_env.Append(LINKFLAGS=phasertng_env.version_obj)
  if env_etc.compiler == "win32_cl":
    # Specify resource object file as a linker flag as to trick SCons to include it in the executable
    phasertng_env.Append(LINKFLAGS=libtbx.env.under_build(path="phasertng/phasertng.res"))
  code_obj = phasertng_env.Object(exe_sources_code)
  if env_etc.compiler == "win32_cl":
    fastflags = phasertng_env['CCFLAGS'] + ["-Od"]
  else:
    fastflags = phasertng_env['CCFLAGS'] + ["-O0"]
  auto_obj = phasertng_env.Object(exe_sources_auto, CCFLAGS=fastflags)
  exe_sources = code_obj + auto_obj
  if ( libtbx.env.dist_path(module_name="phenix", default=None) is not None):
    phasertng_env.exe = phasertng_env.Program(target="#phasertng/exe/phasertng", source= exe_sources)
    Depends(phasertng_env.exe,"SConscript")
    Depends(phasertng_env.exe,"CHANGELOG")
    Depends(phasertng_env.exe,os.path.join(phasertng_env.header_dir,"Version.h"))
    phasertng_exepath = phasertng_env.exe[0].get_abspath()
  else:
    phasertng_env.exe = phasertng_env.Program(target="#exe/phasertng", source= exe_sources)
    Depends(phasertng_env.exe,"CHANGELOG")
    Depends(phasertng_env.exe,os.path.join(phasertng_env.header_dir,"Version.h"))
    phasertng_exepath = phasertng_env.exe[0].get_abspath()
    libtbx.env.write_dispatcher_in_bin(
      source_file=phasertng_exepath,
      target_file="phasertng")

  # Ensure version file is compiled strictly before phasertng executable is linked
  phasertng_env.AddPreAction(phasertng_env.exe, action=write_tng_version_cc)
  phasertng_env.Requires(phasertng_exepath, phasertng_env.version_obj)

  # Add a post-build on windows step to embed the manifest using mt.exe
  # The number at the end of the line indicates the file type (1: EXE; 2:DLL).
  if (env_etc.compiler == "win32_cl" and float(phasertng_env["MSVC_VERSION"]) < 10.0 ):
    phasertng_env.AddPostAction(phasertng_exepath, 'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;1')

  if (not env_etc.no_boost_python):
    #COOK BOOK FOR MAKING BOOST-PYTHON MODULE
    Import("env_no_includes_boost_python_ext") #from libtbx
  # Add any commandline flags to the default C++ flags
    patched_shcxxflags = []
    if env_etc.compiler == "unix_gcc":
      patched_shcxxflags = cmdlineflags
    for item in env_no_includes_boost_python_ext["SHCXXFLAGS"]:
      if "DBOOST_PYTHON_MAX_BASES=" in item:
        pitem = "-DBOOST_PYTHON_MAX_BASES=%d" %phasertng_boost_python_max_bases
        if env_etc.compiler == "win32_cl":
          pitem = "/DBOOST_PYTHON_MAX_BASES=%d" %phasertng_boost_python_max_bases
        patched_shcxxflags.append( pitem )
      else:
        patched_shcxxflags.append(item)
    env_phasertng_boost_python_ext = env_no_includes_boost_python_ext.Clone(
      SHCXXFLAGS=patched_shcxxflags)
    bflag = "-DBOOST_PYTHON_MAX_ARITY=22"
    if env_etc.compiler == "win32_cl":
      bflag = "/DBOOST_PYTHON_MAX_ARITY=22"
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=[ bflag ])
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=["-DBOOST_ALLOW_DEPRECATED_HEADERS"])
    env_phasertng_boost_python_ext.Prepend(SHCXXFLAGS=["-DBOOST_BIND_GLOBAL_PLACEHOLDERS"])
    #env_phasertng_boost_python_ext.Prepend(LIBS=["cctbx_sgtbx_asu"])
    env_etc.include_registry.append(
      env=env_phasertng_boost_python_ext,
      paths=env_etc.phasertng_common_includes+[env_etc.python_include]) #from libtbx
    Export("env_phasertng_boost_python_ext")
    SConscript("codebase/phasertng/boost_python/SConscript")

  #xmlfile=open(os.path.join(libtbx.env.under_dist("phasertng","ccp4i2"),"phaser_MR_AUTO.xml"),"w")
  #subprocess.check_call((os.path.join(libtbx.env.under_build("bin"),"phasertng.phil_as_xml"),"--mode=MR_AUTO"),stdout=xmlfile)
  #xmlfile.close()
