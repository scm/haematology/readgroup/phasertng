//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_DagDatabase_class__
#define __phasertng_dag_DagDatabase_class__
#include <phasertng/main/CCP4base.h>
#include <phasertng/main/DogTag.h>
#include <phasertng/pod/fast_types.h>
#include <phasertng/dag/NodeList.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/Error.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/data/LatticeTranslocation.h>
#include <phasertng/io/FileSystem.h>
#include <phasertng/enum/interp_point.h>

namespace phasertng {
namespace dag {

class DuplicateData;

  typedef std::tuple<double> LikelihoodDataType;
  typedef std::map<std::string,dag::DuplicateData> HallDuplicateData;
  typedef std::map<Identifier,dag::Entry> mapentry_t;
  typedef std::map<Identifier,DogTag> maprefls_t;

//filesystem STORES the name of the file on disk, which could be anything, not even a database entry
  class DagDatabase : public CCP4base, public FileSystem
  {
    private: //members
      const cmplex cmplxONE = cmplex(1.,0.);
      const cmplex TWOPII = cmplex(0.,scitbx::constants::two_pi);

    public: //setter
      //entries should never be used as a map lookup except once, when a link is make to Modlid
      // in the Node information
      mapentry_t ENTRIES; //can't copy, problen with Entry destructor
      //***it is important this is as a list***
      //use list not a vector so that current members are not moved in memory after creation
      // - otherwise add_entry causes memory to be reorganised and nasty memory leaks

      maprefls_t REFLIDS; //can't copy, problen with Entry destructor

      std::map<std::string,SpaceGroup> SPACEGROUPS;
      //spacegroup should never be used as a map lookup except once, when a link is make to Modlid
      // in the Node information

      //edge for linking nodes to run-job
      //note that there is a copy of this on each of the nodes, but it has to be the same for all
      //the copy is kept so the nodes themselves can find the files associated with the pathway
      Edge PATHWAY;
      Edge PATHLOG;
      bool debugger = false;
      bool negvar = false;

    public: //members
      //vector for random access
      //use sorting to front algorithm (erase_invalid) for fast random access erase
      dag::NodeList NODES; //deep copy when class copied
      //NodeList class has functions set to behave like std::vector (which it wraps)
      //WORK is a working node that has access to the database entries
      dag::Node* WORK = &NODES.front(); //in place modification of NODES
      std::size_t size() const { return NODES.size(); }

    public: //constructor
      DagDatabase() {}
      void clear();
      void clear_unused_entries();
      void clear_frf_entries(Identifier);
      void clear_packing();
      void parse_card(); //FileSystem internal
      void parse_cards(std::string);
      void parse_entry_data(std::istringstream&,DogTag);
      dag::Edge parse_cards_retaining_seeks(FileSystem);
      void append_parse_cards_raw(std::string);
      void append_parse_card(FileSystem);
      void append_parse_cards(std::string);
      void unparse_card();
      void unparse_card_node(FileSystem,int);
      void unparse_html(FileSystem);
      af_string unparse_card_summary();
      std::string cards();
      std::string node_cards(int i);
      af_string node_cards_list();

    public: //iterators loop through NODES setting WORK to the active node each time
      int  inext = 0;
      bool at_end();
      void restart(unsigned=0);

    public:
      dag::Entry* lookup_entry_tag(Identifier&,std::string);
      dag::Entry* lookup_entry(Identifier&,std::string);
      DogTag lookup_reflections(Identifier&,std::string);
      DogTag lookup_tag(std::string);
      DogTag lookup_reflid(Identifier&);
      DogTag lookup_modlid(Identifier&);

    public: //those that act on the working node
      LikelihoodDataType
      work_node_likelihood(//of working node
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        pod::FastPoseType*
      );

      LikelihoodDataType
      combined_tgh(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        pod::FastPoseType*
      );

      LikelihoodDataType
      parallel_combined_tgh(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int, //beg
        int, //end
        pod::FastPoseType*
      );

    public: //those that act on the working node
      pod::FastPoseType
      work_ecalcs_sigmaa(//of working node
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        int plast=-999 //flag for no exclusion
      );

      void
      combined_calc(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        pod::FastPoseType*,
        int //flag for no exclusion
      );

      void
      parallel_combined_calc(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int, //beg
        int, //end
        pod::FastPoseType*,
        int //flag for no exclusion
      );

    public: //those that act on the working node
      double
      work_node_phased_likelihood(//of working node
        const_ReflectionsPtr,
        sv_bool,
        int,
        bool
      );

      double
      combined_phased_tgh(
        const_ReflectionsPtr,
        sv_bool,
        int,
        bool
      );

      double
      parallel_combined_phased_tgh(
        const_ReflectionsPtr,
        sv_bool,
        int, //beg
        int //end
      );

    public: //those that act on the working node
      LikelihoodDataType
      work_node_likelihood_for_tfz(//of working node
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        pod::FastPoseType*,
        pod::FastTfzType*,
        dvect3
      );

      LikelihoodDataType
      combined_tgh_for_tfz(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int,
        bool,
        pod::FastPoseType*,
        pod::FastTfzType*,
        dvect3
      );

      LikelihoodDataType
      parallel_combined_tgh_for_tfz(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr,
        int, //beg
        int, //end
        pod::FastPoseType*,
        pod::FastTfzType*,
        dvect3
      );

      pod::FastTfzType
      fast_terms_for_tfz(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr
      );

   private:
      void duplicate_poses_parallel(HallDuplicateData,double,int,bool,int,int);
      void duplicate_poses_combine(HallDuplicateData,double,int,bool,bool,int);
      void duplicate_gyres_parallel(HallDuplicateData,double,bool,int,int,int);
      void duplicate_gyres_combine(HallDuplicateData,double,bool,int,bool,int);
      void duplicate_turns_parallel(HallDuplicateData,double,bool,int,int,int);
      void duplicate_turns_combine(HallDuplicateData,double,bool,int,bool,int);
      void duplicate_packs_parallel(HallDuplicateData,double,bool,std::string,int,int);
      void duplicate_packs_combine(HallDuplicateData,double,bool,std::string,bool,int);
      double lowest_hires() const;
      double highest_lores() const;

    public:
      int  calculate_duplicate_poses(double,bool,bool,int);
      int  calculate_duplicate_turns(double,bool,bool,int);
      int  calculate_duplicate_gyres(double,bool,bool,int);
      int  calculate_duplicate_atoms(double,bool,bool,int);
      int  calculate_duplicate_packs(double,bool,std::string,bool,int);
      int  calculate_duplicate_llg();
      int  find_centrosymmetry(double);
      void apply_space_group_expansion( std::vector<cctbx::sgtbx::space_group_type> );
      void apply_cell(Identifier,double);
      void apply_drms(Identifier,std::pair<double,double>);
      void apply_reflid_expansion(std::vector<Identifier>,std::vector<UnitCell>);
      void apply_interpolation(interp::point,LatticeTranslocation disorder=LatticeTranslocation());
      std::string seek_summary() const;
      std::unordered_map<uuid_t,std::string> parent_flags(); //converts id to string(chain)
      void shift_tracker(std::string,std::string); //change all the trackers to the next interation
      void initialize_first_node_from_hash_tag(std::string,std::string);
      DogTag initialize_reflection_identifier(std::string);
      void initialize_for_twilight_zone(std::string,std::string);
      void reset_entry_pointers(std::string);
      void reset_work_pointer();
      void reset_work_entry_pointers(std::string);
      dag::Entry* add_entry(Identifier);
      void add_entry_copy_header(Identifier,Identifier);
      bool solved(double,double);
      void append_seek_to_node(Identifier);
      std::pair<double,double> resolution_range() const;
      bool next_seek_permutation();
      std::set<Identifier> set_of_entries();
      std::vector<uuid_t> set_of_parents();
      std::vector<uuid_t> set_of_grandparents();
      std::vector<Identifier> set_of_reflid();
      void permute_seek(Identifier);
      void apply_partial_solution();
     // std::string unparse_entries();
     // std::string unparse_reflids();
      af_string logEntries();
      af_string logReflids();

    //boosted to python
    void read_file(std::string);
    void clear_cards();
    void clear_seeks();
    void clear_holds();
    void clear_poses_add_seeks(std::vector<Identifier>);
    void clear_seeks_add_seeks(std::vector<Identifier>);
    std::string seek_info_all() const;
    af_string   seek_info_array(int=0) const;
    std::string seek_info_current_number() const;
    std::string seek_info_current_name() const;
    std::string permutation_info_all() const;
    Identifier  seek_next();
    int  number_of_seek_loops_first() const;
    int  number_of_seek_loops_final() const;
    bool constituents_complete() const;
    bool first_constituents_found() const;
    void increment_current_seeks_with_identifier(Identifier);
    void reset_seek_components_from_pose();
    void set_join(int,double,Identifier);
    std::vector<uuid_t> list_of_current_seeks() const;
    std::vector<uuid_t> list_of_all_seeks() const;
    void append_entry_hold(std::string);
    std::string entry_unparse_modlid(Identifier);

  public://from Entry.cc
    std::vector<std::pair<std::string,std::string>> reflid_unparse(const DogTag& ) const;
    std::vector<std::pair<std::string,std::string>> entry_unparse(const dag::Entry& entry) const;
    std::vector<std::pair<std::string,std::string>> edge_unparse(const dag::Edge&, bool) const;
    std::vector<std::pair<std::string,std::string>> node_unparse(const dag::Node&,bool html=false) const;

    std::string reflid_unparse_card(const DogTag&) const;
    std::string entry_unparse_card(const dag::Entry& entry) const;
    std::string entry_unparse_html(const dag::Entry& entry,bool header) const;
    std::string edge_unparse_card(const dag::Edge&, std::string) const;

    void node_unparse_card(const dag::Node&,std::ostream&) const;
    void node_unparse_html(const dag::Node&,std::ostream&,bool) const;
    void set_filesystem(std::string,std::string,std::string);

    public:
      std::string phil_str();
      void unparse_phil(FileSystem);
      std::string node_phil_str(int i);
      std::string work_phil_str() const; //working node
      std::string reflid_unparse_phil(const DogTag&) const;
      std::string entry_unparse_phil(const dag::Entry&) const;
      std::string edge_unparse_phil(const dag::Edge&, std::string) const;
      void node_unparse_phil(const dag::Node&,std::ostream&) const;
      std::vector<DogTag> unparse_dogtags() const;
      void parse_dogtags(std::vector<DogTag>);
      setmodlid_t all_modlid() const;
      std::string polarity() const;
      double solved_tfz(double,double,double);

    public:
    DagDatabase& operator=(const DagDatabase& other) { NODES = other.NODES; return *this; }
    //required for boost python, but should not be called
    //can't copy Entry, there is a problem with the destructor
  };

} //end dag scope

} //phasertng
#endif
