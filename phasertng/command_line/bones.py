# LIBTBX_SET_DISPATCHER_NAME phasertng.bones

from __future__ import print_function
from __future__ import division

from iotbx.cli_parser import run_program
from phasertng.programs import bones

if (__name__ == '__main__'):
  try:
    run_program(bones.Program)
  except Exception as e:
    print(str(e))
