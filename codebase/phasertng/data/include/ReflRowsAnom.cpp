//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/ReflRowsAnom.h>
#include <phasertng/data/ReflColsMap.h>

namespace phasertng {

  void
  ReflRowsAnom::copy_reflections(const ReflColsMap& refl)
  { copy_and_reformat_data(&refl); } //for python

  ReflRowsAnom::ReflRowsAnom(int nrefl) : Reflections()
  { set_base_defaults(); setup_miller(nrefl); }

  void
  ReflRowsAnom::set_base_defaults()
  {
    LABIN_ALLOWED = {
        labin::INAT,
        labin::SIGINAT,
        labin::FEFFNAT,
        labin::DOBSNAT,
        labin::ANISOBETA,
        labin::RESN,
        labin::SSQR,
        labin::TEPS,
        labin::EPS,
        labin::TBIN,
        labin::WLL,
        labin::PHSR,
        labin::INFO,
        labin::EINFO,
        labin::SINFO,
        labin::SELLG,
        labin::SEFOM,
        labin::IPOS,
        labin::SIGIPOS,
        labin::FEFFPOS,
        labin::DOBSPOS,
        labin::INEG,
        labin::SIGINEG,
        labin::FEFFNEG,
        labin::DOBSNEG,

        labin::BIN,

        labin::CENT,
        labin::BOTH,
        labin::PLUS,
      };
  }

  void
  ReflRowsAnom::set_miller(const millnx v,const int r)
  {
    ROW[r].miller = v;
  }

  const millnx
  ReflRowsAnom::get_miller(const int r) const
  {
    return ROW[r].miller;
  }

  void
  ReflRowsAnom::set_present(friedel::type f,const bool v,const int r)
  {
    if (f == friedel::UNDEFINED) return;
    reflection::ReflRowAnom* work_row = &ROW[r];
    if (f == friedel::NAT) work_row->pnat = v;
    if (f == friedel::POS) work_row->ppos = v;
    if (f == friedel::NEG) work_row->pneg = v;
  }

  void
  ReflRowsAnom::setup_mtzcol(type::mtzcol col)
  {
    if (NREFL == 0) throw Error(err::DEVELOPER,"Miller array size has not been set");
    if (!LABIN_ALLOWED.count(col.enumeration()))
      throw Error(err::DEVELOPER,"Tried to setup_mtzcol(" + col.labin_col() + ") to compile-time data");
    CreateLabin(col);
    //hardwired have ppos, pneg and pnat
    friedel::type f = convert_labin_to_friedel(col.enumeration());
    FRIEDEL.insert(f);
  }

  void
  ReflRowsAnom::setup_miller(int nrefl)
  {
    NREFL = nrefl;
    ROW.resize(NREFL);
  }

  void
  ReflRowsAnom::erase_from_reflection(int nrefl)
  {
    NREFL = nrefl;
    ROW.erase(ROW.begin()+nrefl,ROW.end());
  }

  void
  ReflRowsAnom::set_flt(labin::col col,const double v,const int r)
  {
    reflection::ReflRowAnom* work_row = &ROW[r];
         if (col == labin::INAT) work_row->inat = v;
    else if (col == labin::SIGINAT) work_row->siginat = v;
    else if (col == labin::FEFFNAT) work_row->feffnat = v;
    else if (col == labin::DOBSNAT) work_row->dobsnat = v;
    else if (col == labin::ANISOBETA) work_row->anisobeta = v;
    else if (col == labin::RESN) work_row->resn = v;
    else if (col == labin::SSQR) work_row->ssqr = v;
    else if (col == labin::TEPS) work_row->teps = v;
    else if (col == labin::EPS) work_row->eps = v;
    else if (col == labin::TBIN) work_row->tbin = v;
    else if (col == labin::WLL) work_row->wll = v;
    else if (col == labin::PHSR) work_row->phsr = v;
    else if (col == labin::INFO) work_row->info = v;
    else if (col == labin::EINFO) work_row->einfo = v;
    else if (col == labin::SINFO) work_row->sinfo = v;
    else if (col == labin::SELLG) work_row->sellg = v;
    else if (col == labin::SEFOM) work_row->sefom = v;
    else if (col == labin::IPOS) work_row->ipos = v;
    else if (col == labin::SIGIPOS) work_row->sigipos = v;
    else if (col == labin::FEFFPOS) work_row->feffpos = v;
    else if (col == labin::DOBSPOS) work_row->dobspos = v;
    else if (col == labin::INEG) work_row->ineg = v;
    else if (col == labin::SIGINEG) work_row->sigineg = v;
    else if (col == labin::FEFFNEG) work_row->feffneg = v;
    else if (col == labin::DOBSNEG) work_row->dobsneg = v;
    else throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this set_flt(" + labin::col2String(col) +")");
  }

  void
  ReflRowsAnom::set_int(labin::col col,const int v,const int r)
  {
    reflection::ReflRowAnom* work_row = &ROW[r];
         if (col == labin::BIN) work_row->bin = v;
    else throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this set_int(" + labin::col2String(col) +")");
  }

  void
  ReflRowsAnom::set_bln(labin::col col,const bool v,const int r)
  {
    reflection::ReflRowAnom* work_row = &ROW[r];
         if (col == labin::CENT) work_row->cent = v;
    else if (col == labin::BOTH) work_row->both = v;
    else if (col == labin::PLUS) work_row->plus = v;
    else throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this set_bln(" + labin::col2String(col) +")");
  }

  const bool
  ReflRowsAnom::get_present(friedel::type f,const int r) const
  {
    if (f == friedel::UNDEFINED) return true;
    const reflection::ReflRowAnom* work_row = &ROW[r];
    if (f == friedel::NAT) return work_row->pnat;
    if (f == friedel::POS) return work_row->ppos;
    if (f == friedel::NEG) return work_row->pneg;
    return true;
  }

  const double
  ReflRowsAnom::get_flt(labin::col col,const int r) const
  {
    const reflection::ReflRowAnom* work_row = &ROW[r];
    if (col == labin::INAT) return work_row->inat;
    if (col == labin::SIGINAT) return work_row->siginat;
    if (col == labin::FEFFNAT) return work_row->feffnat;
    if (col == labin::DOBSNAT) return work_row->dobsnat;
    if (col == labin::IPOS) return work_row->ipos;
    if (col == labin::ANISOBETA) return work_row->anisobeta;
    if (col == labin::RESN) return work_row->resn;
    if (col == labin::SSQR) return work_row->ssqr;
    if (col == labin::TEPS) return work_row->teps;
    if (col == labin::EPS) return work_row->eps;
    if (col == labin::TBIN) return work_row->tbin;
    if (col == labin::WLL) return work_row->wll;
    if (col == labin::PHSR) return work_row->phsr;
    if (col == labin::INFO) return work_row->info;
    if (col == labin::EINFO) return work_row->einfo;
    if (col == labin::SINFO) return work_row->sinfo;
    if (col == labin::SELLG) return work_row->sellg;
    if (col == labin::SEFOM) return work_row->sefom;
    if (col == labin::IPOS) return work_row->ipos;
    if (col == labin::SIGIPOS) return work_row->sigipos;
    if (col == labin::FEFFPOS) return work_row->feffpos;
    if (col == labin::DOBSPOS) return work_row->dobspos;
    if (col == labin::INEG) return work_row->ineg;
    if (col == labin::SIGINEG) return work_row->sigineg;
    if (col == labin::FEFFNEG) return work_row->feffneg;
    if (col == labin::DOBSNEG) return work_row->dobsneg;
    throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this get_flt(" + labin::col2String(col) +")");
    return DEF_ONE;
  }

  const int ReflRowsAnom::get_int(labin::col col,const int r) const
  {
    const reflection::ReflRowAnom* work_row = &ROW[r];
    if (col == labin::BIN) return work_row->bin;
    throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this get_int(" + labin::col2String(col) +")");
    return default_int;
  }

  const bool
  ReflRowsAnom::get_bln(labin::col col,const int r) const
  {
    const reflection::ReflRowAnom* work_row = &ROW[r];
    if (col == labin::CENT) return work_row->cent;
    if (col == labin::BOTH) return work_row->both;
    if (col == labin::PLUS) return work_row->plus;
    throw Error(err::DEVELOPER,"ReflRowsAnom: Catch this get_bln(" + labin::col2String(col) +")");
    return default_bln;
  }

}
