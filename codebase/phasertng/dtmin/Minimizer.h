//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
//
// Phasertng's minimizer is architected as a minimizer object that refines a refine object.
// One writes a class for their minimization problem (conventionally named Refine'XYZ') that inherits
// the RefineBase class. This class must provide an implementation for RefineBase's pure virtual
// functions, (e.g. get_macrocycle_parameters(), which must return a vector of the values of the parameters currently being refined).
// RefineBase also has virtual functions which may be over-ridden if your problem requires a special feature,
// e.g. maximum_distance_special().

#ifndef __phasertng_mintbx_Minimizer_class__
#define __phasertng_mintbx_Minimizer_class__
#include <phasertng/main/pointers.h>
#include <phasertng/enum/out_stream.h>
#include <phasertng/dtmin/RefineBase.h>
#include <cstdarg>

//#define PHASERTNG_DEBUG_MINIMIZER

namespace phasertng {
namespace dtmin {

class Minimizer
{
private:
  OutputPtr output_;          // needed for logging
  //OutputPtr output_;          // needed for logging
  int       func_count_;      // used by line_search to count the number of function evaluations
  sv_string parameter_names_; // updated at the start of each macrocycle to contain the names of the parameters being refined that macrocycle
  bool write_logfile = true; //where the output goes has to be greater than the level
  bool write_debuggg = true; //but wrapped code not compiled unless define flag set. No user input control possible
  bool write_testing = false; //(output_->Level() >= out::TESTING) in constructor
  out::stream logfile = out::VERBOSE; // write_logfile written by default to output stream even if store not set to verbose
  out::stream testing = out::TESTING;
  out::stream debuggg = out::TESTING;
  bool iteration_limit_reached = false;

public:
  Minimizer(OutputPtr);
  void set_logfile_level(out::stream where);
  bool convergence() { return !iteration_limit_reached; }

  void
  run(
      RefineBase&                  TARGET,
      const std::vector<sv_string> PROTOCOL,
      const std::vector<int>       NCYC,
      const std::string            MINIMIZER,
      const bool                   STUDY_PARAMS = false,
      const double                 SMALL_TARGET = 0);

private:
  void
  check_input(
      std::vector<sv_string> PROTOCOL,
      std::vector<int>       NCYC,
      std::string            MINIMIZER);

  bool
  check_in_bounds(RefineBase& TARGET,sv_string);

  //<f, ncyc_since_hessian_calc, queue_new_hessian, too_small_shift, x, g>
  std::tuple<double, int, bool, bool, bool, sv_double, sv_double>
  bfgs(
      RefineBase&          TARGET,
      bool                 calc_new_hessian,
      bool                 queue_new_hessian,
      int                  ncyc_since_hessian_calc,
      double               required_decrease,
      sv_double&           previous_x,
      sv_double&           previous_g);

  //<f, too_small_shift>
  std::pair<double, bool>
  newton(
      RefineBase& TARGET,
      double      required_gain);

  //<f, too_small_shift>
  std::pair<double, double>
  line_search(
      RefineBase&     TARGET,
      sv_double       x,
      double          f,
      const sv_double g,
      sv_double&      p,
      double          starting_distance,
      double          minimum_distance,
      double          required_decrease
      );

  double
  shift_score(
      RefineBase& TARGET,
      double      a,
      sv_double&  x,
      sv_double&  oldx,
      sv_double&  p,
      sv_double&  dist,
      sv_double&  macrocycle_large_shifts,
      bool        bcount = true);

  void
  shift_parameters(
      RefineBase& TARGET,
      double      a,
      sv_double&  x,
      sv_double&  oldx,
      sv_double&  p,
      sv_double&  dist,
      sv_double&  macrocycle_large_shifts);

  // <termination_reason, calc_new_hessian>
  std::pair<std::string, bool>
  check_termination_criteria(
      bool  too_small_shift,
      bool  too_small_decrease,
      bool  zero_gradient,
      bool  queue_new_hessian,
      int   ncyc_since_hessian_calc,
      int   cycle,
      int   ncyc);

  double
  hessian_diag_rms(versa_flex_double h);

  inline double
  required_shift(
      double              min_dx_over_esd,
      sv_double           p,
      versa_flex_double   h_inv);

  bool
  quadratic_coefficients(
      double  y1,
      double  y2,
      double  y3,
      double  x1,
      double  x2,
      double  x3,
      double& a,
      double& b,
      double& c);

  af_string format_vector(std::string,sv_double&,sv_string);
  af_string format_hessian(af::versa<double,af::flex_grid<> >,sv_string);

std::string snprintftos(const char* format, ...)
{
  static const std::size_t temp_size = 8192;
  char temp[temp_size];
  temp[temp_size-1] = '\0';
  va_list arglist;
  va_start(arglist,format);
  vsnprintf(temp,temp_size,format,arglist);
  va_end(arglist);
  assert(temp_size-1 >= 0 and temp[temp_size-1] == '\0');
  std::string tempstr(temp);
  return tempstr.substr(0,tempstr.find("$"));
}

}; // class Minimizer

}} // namespace phasertng

#endif
