//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/ReflRowsAnom.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/io/entry/Map.h>
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/math/slope_and_intercept.h>
#include <phasertng/minimizer/RefineANISO.h>

namespace phasertng {

  //Ensemble_preparation
  void Phasertng::runMSM()
  {
#include <phasertng/run/runMBA.cpp>
#include <phasertng/run/runMWA.cpp>
#include <phasertng/run/runMAP.cpp>
  }

} //phasertng
