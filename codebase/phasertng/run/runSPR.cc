//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/src/Packing.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/math/likelihood/EELLG.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineSP.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Segmented b-factor refinement
  void Phasertng::runSPR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No ");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
 //   if (!DAGDB.NODES.is_all_full())
 //   throw Error(err::INPUT,"Dag not prepared with full node type");
 // the full ie coordinates would be required to check the re-packing/generation
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with pose");
    //pose may be present but they are not used here
    //mode rbm first to get the coordinates for analysis
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose(); //needed for Packing
    for (auto id : used_modlid)
    {
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false); //for ouput
      load_entry_from_database(where,entry::TRACE_PDB,id,false); //for ouput
    }
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);

    for (auto& item : DAGDB.ENTRIES)
    {
      if (item.second.MAP)
      {
        logTab(out::LOGFILE,"No segmentation pruning for maps");
        return;
      }
    }

    double fsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
    double bsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
    double minb = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();

    FourParameterSigmaA solTerm(
        input.value__flt_number.at(".phasertng.segmented_pruning.segment.rmsd.").value_or_default(),
        fsol,bsol,minb);

    double io_lores = input.value__flt_number.at(".phasertng.segmented_pruning.pruning.low_resolution.").value_or_default();
    int io_maxstored = input.value__int_number.at(".phasertng.segmented_pruning.maximum_stored.").value_or_default();
    double maxres(3);

    {{
    int count(0);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      millnx miller =  REFLECTIONS->get_miller(r);
      double resolution = REFLECTIONS->UC.cctbxUC.d(miller);
      if (resolution < io_lores)
        count++;
    }
    if (count < 1000)
    {
      logAdvisory("Low resolution limit removed due to lack of reflections");
      io_lores = DEF_LORES;
    }
    }}

    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    bool refine_off =
        input.value__choice.at(".phasertng.macspr.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macspr.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macspr.macrocycle3.").value_or_default_equals("off");
    refine_off = refine_off or
        input.value__choice.at(".phasertng.macspr.protocol.").value_or_default_equals("off");

    std::vector<sv_string> macro = {
        input.value__choice.at(".phasertng.macspr.macrocycle1.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macspr.macrocycle2.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macspr.macrocycle3.").value_or_default_multi()
      };
    sv_int ncyc = {
        input.value__int_vector.at(".phasertng.macspr.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macspr.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macspr.ncyc.").value_or_default(2)
      };
    auto method = input.value__choice.at(".phasertng.macspr.minimizer.").value_or_default();
    bool occupancy_refinement(
        !input.value__choice.at(".phasertng.macspr.macrocycle1.").value_or_default_equals("bfac") and
        !input.value__choice.at(".phasertng.macspr.macrocycle2.").value_or_default_equals("bfac") and
        !input.value__choice.at(".phasertng.macspr.macrocycle3.").value_or_default_equals("bfac"));
    logProtocol(out::LOGFILE,macro,ncyc,method);
    logBlank(out::LOGFILE);
    for (int i = 0; i < 3; i++)
      if (macro[i].size() != 1 and (macro[i][0] != "off"))// and macro[i][0] != macro[0][0]))
        throw Error(err::INPUT,"Refinement must be either occupancy or b-factor");

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    // fluorescence scan etc is overkill for only proteins
    scatterers.PARTICLE = formfactor::particle2Enum("XRAY"); //hardwired to avoid scattering keyword
    for (auto comp : { "H","C","N","O","S","SE","P" }) // assume all the same
        scatterers.insert(comp);

    likelihood::ELLG likelihood_ellg;
    likelihood::EELLG likelihood_eellg;
    bool io_use_eellg = false;

    //when the dag nodes are entered
    //only one set of reflections is stored in memory
    //otherwise files must be written out
    int n(1),N(DAGDB.size());
    DAGDB.restart(); //very important to call this to set WORK
    DAGDB.reset_entry_pointers(DataBase()); //very important to call this to set WORK
    af_string output;
    int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using llg here
    int q = std::max(9,DAGDB.NODES.llg_width(false)+1); //one for space to grow, 9"LLG-start"
    output.push_back(snprintftos("%-4s %10s %6s %5s%c %*s %*s %7s",
        "#",
        "Segments",
        "TFZ","FTF",' ',
        q,"LLG-start",
        q,"LLG-final",
        "Pruned%"));
    while (!DAGDB.at_end())
    {
      DAGDB.WORK->RFACTOR = 100;
      if (io_maxstored and n > io_maxstored)
        continue;
      std::string nN = nNtos(n++,N);
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Segmented Pruning Refinement" +nN);
      auto node = DAGDB.WORK; //pointer so in place changes
      logTabArray(where,node->logAnnotation(""));
      //we have to generate the coordinates again
      //so that the tncs_group is stored in the ExtraFlags
      //and can be parsed for the segmentation in the presence of tncs
      //this tNCS relationship information is NOT written out with the coordinates from rbm
      Identifier& modlid = node->TRACKER.ULTIMATE; //modify tag here
      modlid.initialize_tag("spr");
      auto entry = DAGDB.add_entry(modlid);
      node->FULL.PRESENT = true;
      node->FULL.IDENTIFIER = modlid;
      node->FULL.ENTRY = entry;
      node->DRMS_SHIFT[modlid] = dag::node_t(modlid,0);
      node->VRMS_VALUE[modlid] = dag::node_t(modlid,1); //nonsense because we don't have initial vrms or limits
      node->CELL_SCALE[modlid] = dag::node_t(modlid,1);
      {{
        Packing packing;
        packing.init_node(node);
       // packing.move_to_origin();
        packing.structure_for_refinement(entry->COORDINATES);
        entry->COORDINATES.MODLID = entry->identify();
        entry->COORDINATES.REFLID = REFLECTIONS->reflid();
        entry->COORDINATES.TIMESTAMP = TimeStamp();
        Models models; //tmp for getting entry values
        models.SetPdb(entry->COORDINATES.get_pdb_cards());
        models.set_principal_statistics();
        entry->PRINCIPAL_TRANSLATION = -models.statistics.CENTRE;
        entry->MEAN_RADIUS = models.statistics.MEAN_RADIUS;
        entry->CENTRE = models.statistics.CENTRE;
        entry->EXTENT = models.statistics.EXTENT;
        {
        //calculate the volume from the sequence, same as for the map
        Sequence seq(models.SELENOMETHIONINE);
        bool use_unknown(false);
        seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
        seq.initialize(true,true);
        entry->VOLUME = seq.volume();
        }
        entry->SCATTERING = models.scattering()[0];
        entry->MOLECULAR_WEIGHT = models.molecular_weight()[0];
      }}
      node->FULL.FS = entry->fraction_scattering(REFLECTIONS->get_ztotalscat());
      logTabArray(out::VERBOSE,node->logPose("Start"+nN));

      {{
      out::stream where = out::LOGFILE;
      Packing packing(0.5);
      packing.init_node(node); //must be linked with entries
      packing.multiplicity_and_exact_site();
      bool last_only(false); //assume that this has already been through a packing test
      bool no_cell_translation(REFLECTIONS->MAP);
      packing.calculate_packing(last_only,no_cell_translation);
      node->clash_matrix = packing.PACKING_DATA.matrix_as_matrix();
      node->CLASH_WORST = packing.PACKING_DATA.worst_percent();
      logTabArray(where,node->logClash());
      }}

      Coordinates& coordinates = entry->COORDINATES;
      double tLLG = 0;
      double fp = REFLECTIONS->fracscat(entry->SCATTERING);
      {{
      out::stream where = out::VERBOSE;
      logChevron(where,"Expected LLG" +nN);
      //every coordinates is different, from the packing, so start again each time
      PHASER_ASSERT(entry->SCATTERING > 0);
      PHASER_ASSERT(entry->MOLECULAR_WEIGHT > 0);
      phaser_assert(coordinates.size());
      {{
      bool selenomethionine(false);
      Sequence sequence(selenomethionine);
      af_string cards = coordinates.get_pdb_cards();
      sequence.SetPdb(cards);
      phaser_assert(cards.size());
      sequence.initialize(true,true);
      if (sequence.has_nucleic())
        logWarning("Solution includes nucleic acids");
      }}
      int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
      PHASER_ASSERT(fp <= REFLECTIONS->get_ztotalscat());
      double eLLG = 0;
      double eeLLG = 0;
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const bool& p = REFLECTIONS->get_present(friedel::NAT,r);
        if (selected.get_selected(r) and p)
        {
          const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
          const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
          double Dobs_Siga_sqr = fn::pow2(dobs*solTerm.get(ssqr));
          double bratio = 1;
          double SigaSqr = Dobs_Siga_sqr*(fp*bratio);
          eeLLG += likelihood_eellg.get(SigaSqr);
          tLLG = eeLLG;
          if (!io_use_eellg)
          {
            const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
            const double resn = REFLECTIONS->get_flt(labin::RESN,r);
            const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
            const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);
            double DobsSiga = std::sqrt(SigaSqr);
            double Eeff = feff/(resn*sqrt(teps));
            eLLG += likelihood_ellg.get(Eeff,DobsSiga,cent);
            tLLG = eLLG;
          }
        }
      }
      logTab(where,snprintftos(
          "%-8s  %-11s %-12s",
          "fp", "eLLG-(all)-", "eeLLG-(all)-"));
      logTab(where,snprintftos(
          "%-8.6f  %11s %-12.2f",
          fp, eLLG ? dtos(eLLG,11,2).c_str() : "--na--", eeLLG));
      }}

      int nres(0);
      logBlank(where);
      logChevron(where,"Residue Window" +nN);
      bool selenomethionine(false);
      Sequence aminoacids(selenomethionine);
      aminoacids.AddProtein("ACDEFGHIKLMNPQRSTVWY");
      aminoacids.initialize(false,true);
      double average_scattering_per_amino_acid = aminoacids.total_scattering()/20.;
      int max_nres_determined_by_fp = -1+aminoacids.total_scattering()/average_scattering_per_amino_acid;
      double target_eLLG = tLLG -
          input.value__flt_number.at(".phasertng.segmented_pruning.segment.delta_ellg.").value_or_default();
      double tLLG_check(0);
      for (nres = 1; nres < max_nres_determined_by_fp; nres++) //max limit avoids fp < 0
      {
        double aafp = REFLECTIONS->fracscat(nres*average_scattering_per_amino_acid);
        double deltafp = fp-aafp;
        if (deltafp < 0) { nres--; break; } //paranoia, should not be here
        double eLLG_check(0);
        double eeLLG_check(0);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          const bool& p = REFLECTIONS->get_present(friedel::NAT,r);
          if (selected.get_selected(r) and p)
          {
            const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
            const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
            double Dobs_Siga_sqr = fn::pow2(dobs*solTerm.get(ssqr));
            double bratio = 1;
            double SigaSqr = Dobs_Siga_sqr*(deltafp*bratio);
            eeLLG_check += likelihood_eellg.get(SigaSqr);
            tLLG_check = eeLLG_check;
            if (!io_use_eellg)
            {
              const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
              const double resn = REFLECTIONS->get_flt(labin::RESN,r);
              const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
              const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);
              double DobsSiga = std::sqrt(SigaSqr);
              double Eeff = feff/(resn*sqrt(teps));
              eLLG_check += likelihood_ellg.get(Eeff,DobsSiga,cent);
              tLLG_check = eLLG_check;
            }
          }
        }
        if (tLLG_check < target_eLLG) break;
      }
      double dellg = input.value__flt_number.at(".phasertng.segmented_pruning.segment.delta_ellg.").value_or_default();
      logTab(where,"Delta ellg target: " + dtos(dellg));
      logTab(where,"Number of residues to achieve eLLG target: " + itos(nres));
      //reverse the calculate to get the actual ellg
      logTab(where,"Window eLLG: " + dtos(tLLG-tLLG_check,2));

      if (!input.value__int_number.at(".phasertng.segmented_pruning.segment.residues.").is_default())
      { //overwrite
        nres = input.value__int_number.at(".phasertng.segmented_pruning.segment.residues.").value_or_default();
        logTab(where,"User defined number of residues in segment = " + itos(nres));
      }
      if (!nres)
      {
        logTab(where,"LLG of whole fragment very low");
        logWarning("No residues in segment");
        return;
      }
      if (nres > 10) //10 residues for local structure
      {
        logWarning("Segment does not represent local structure, more than 10 residues");
      }

      //all other entry parameters stay the same
      logBlank(where);
      logChevron(where,"Segmentation" +nN);
      bool maxed_out;
      maxed_out = coordinates.calculate_segment_identifiers(nres);
      int nsegments = coordinates.nsegments();
      logTab(where,"Structure divided into  " + itos(nsegments) + " segments");
      if (maxed_out)
        logWarning("At least one chain shorter than ellg-guided number of residues per segment");
      PHASER_ASSERT(nsegments > 0);
      if (Suite::Extra())
      {
        Identifier uid = DAGDB.PATHWAY.ULTIMATE; //just for logging
        coordinates.SetFileSystem(DataBase(),entry->DOGTAG.FileName(ntos(n-1,N)+"_debug_segmented.pdb"));
        logFileWritten(out::LOGFILE,WriteFiles(),"Segmented",coordinates);
        if (WriteFiles()) coordinates.write_to_disk();
      }
      logBlank(where);

      DAGDB.reset_work_entry_pointers(DataBase());

      {{
      out::stream where = out::VERBOSE;
      logTableTop(where,"Segmentation"+nN,false);
      logTabArray(where,coordinates.logSegmentTable());
      logTableEnd(where);
      }}

      {{
      out::stream where = out::VERBOSE;
      logChevron(where,"Segmentation Statistics"+nN);
      logTabArray(where,coordinates.logSegmentStats());
      logBlank(where);
      }}

      coordinates.process(scatterers); //for fft

      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      node->FULL.IDENTIFIER = modlid;
      //entry->identify() = modlid;
      phaser_assert(coordinates.size());

      {{
      out::stream where = out::VERBOSE;
      logTab(where,"Segmented Graphic Before Refinement"+nN);
      logTabArray(where,coordinates.logSegmentGraphic());
      logBlank(where);
      }}

      RefineSP refine(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon);
       //   &DAGDB);
      refine.BFAC_MIN = coordinates.BMIN;
      refine.BFAC_MAX = coordinates.BMAX;
      refine.MAX_PERCENT_PRUNED =
        input.value__percent.at(".phasertng.segmented_pruning.pruning.maximum_percent.").value_or_default();
      refine.LLG_BIAS =
        input.value__flt_number.at(".phasertng.segmented_pruning.pruning.llg_bias_from_maximum_percent.").value_or_default();
      refine.solTerm = solTerm;
      refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
      refine.init_node(node);

      refine.likelihood(); //store
      double initllg = DAGDB.WORK->LLG; //store

      {{
      out::stream where = out::VERBOSE;
      logEllipsisOpen(where,"Calculate electron density");
      auto& entry = node->FULL.ENTRY;
      entry->DENSITY.copy_and_reformat_header(REFLECTIONS.get());
      for (auto ext : {"fwt","phwt","delfwt","delphwt","fcnat","phicnat","fom","hla","hlb","hlc","hld"} )
      {
        std::string key = ".phasertng.labin." + std::string(ext) + ".";
        entry->DENSITY.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
      }
      auto ecalcs_sigmaa = refine.ecalcs_sigmaa_full();
      auto txt = entry->DENSITY.calculate_density(
          REFLECTIONS.get(),
          ecalcs_sigmaa,
          selected.get_selected_array());
      logTabArray(out::LOGFILE,txt);
      bool negative_variance(false);
      if (txt.size()) PHASER_ASSERT(negative_variance);
      logEllipsisShut(where);
     // node->cleanup_drms_shift_cell_scale();

      node->generic_float = entry->DENSITY.calculate_rfactor(
          REFLECTIONS.get(),
          ecalcs_sigmaa,
          selected.get_selected_array(),
          maxres); //resolution
      }}

      if (!refine_off)
      {
        out::stream where = out::LOGFILE;
        logChevron(where,"Refinement"+nN);
        dtmin::Minimizer minimizer(this);
        minimizer.run(
          refine,
          macro,
          ncyc,
          input.value__choice.at(".phasertng.macspr.minimizer.").value_or_default(),
          input.value__boolean.at(".phasertng.macspr.study_parameters.").value_or_default(),
          input.value__flt_number.at(".phasertng.macspr.small_target.").value_or_default()
          );
        if (refine.negvar) logWarning("Negative variance ignored, contact developers");
        logTabArray(out::VERBOSE,refine.logCurrent("Refined"));
      }
      else
      {
        out::stream where = out::LOGFILE;
        logChevron(where,"Refinement"+nN);
        logTab(where,"Refinement off");
        refine.init_segments();
        refine.likelihood();
      }

      {{
      out::stream where = out::VERBOSE;
      logEllipsisOpen(where,"Calculate electron density");
      auto& entry = node->FULL.ENTRY;
      entry->DENSITY.copy_and_reformat_header(REFLECTIONS.get());
      for (auto ext : {"fwt","phwt","delfwt","delphwt","fcnat","phicnat","fom","hla","hlb","hlc","hld"} )
      {
        std::string key = ".phasertng.labin." + std::string(ext) + ".";
        entry->DENSITY.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
      }
      auto ecalcs_sigmaa = refine.ecalcs_sigmaa_full();
      auto txt = entry->DENSITY.calculate_density(
          REFLECTIONS.get(),
          ecalcs_sigmaa,
          selected.get_selected_array());
      logTabArray(out::LOGFILE,txt);
      bool negative_variance(false);
      if (txt.size()) PHASER_ASSERT(negative_variance);
      logEllipsisShut(where);
     // node->cleanup_drms_shift_cell_scale();

      node->RFACTOR = entry->DENSITY.calculate_rfactor(
          REFLECTIONS.get(),
          ecalcs_sigmaa,
          selected.get_selected_array(),
          maxres); //resolution
      }}

      node->ANNOTATION += " SPR=" + dtoi(node->LLG);

      bool replace_poses(false);
      //node->POSE.clear(); must not clear! we need this for returning for solution
      //the poses will NOT have the refined occupancies, but that is a good thing
      if (replace_poses)
      {
        node->POSE.clear();
       //node->HOLD.clear();
       //node->SEEK.clear();
       // node->CELL_SCALE.clear();
       // node->DRMS_SHIFT.clear();
        //put solution with euler and frac is wrt original coordinate positions
        dvect3 self(0,0,0);
        //DAGDB.WORK->initialize_turn(self,modlid);
        DAGDB.WORK->NEXT.PRESENT = true;
        DAGDB.WORK->NEXT.ENTRY = entry;
        DAGDB.WORK->NEXT.IDENTIFIER = modlid;
        DAGDB.WORK->NEXT.EULER = self;
        dvect3 eulerin = self;
        //change rotation to wrt mt
        const dmat33 PR = entry->PRINCIPAL_ORIENTATION;
        auto& turn = DAGDB.WORK->NEXT;
        turn.EULER = dag::euler_wrt_mt_from_in(eulerin,PR);
        std::tie(std::ignore,turn.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(turn.EULER,PR);
        //change translation to wrt mt
        dvect3 fracin = self;
        const dvect3 PT = entry->PRINCIPAL_TRANSLATION;
        dvect3 fract = dag::fract_wrt_mt_from_in(fracin,turn.EULER,PT,DAGDB.WORK->CELL);
        dmat33 orthmat = DAGDB.WORK->CELL.orthogonalization_matrix();
        dvect3 shift_ortht = orthmat*fracin;
        DAGDB.WORK->convert_turn_to_curl(fract,shift_ortht);
        dvect3 tncs_fractional(0,0,0);
        int tmp = DAGDB.WORK->TNCS_ORDER;
        DAGDB.WORK->TNCS_ORDER = 1;
        DAGDB.WORK->convert_curl_to_pose(tncs_fractional);
        DAGDB.WORK->POSE[0].ORTHR = self;
        DAGDB.WORK->POSE[0].ORTHT = self;
        logBlank(where);
        DAGDB.WORK->TNCS_ORDER = tmp;
        DAGDB.WORK->POSE[0].BFAC = 0;
        DAGDB.WORK->set_star_from_pose();
      }

      if (Suite::Extra())
      {
        out::stream where = out::TESTING; //can be as long as the full asu contents
        logTabArray(where,node->logPose("Final"+nN));
        logTableTop(where,"Segmentation"+nN,false);
        logTabArray(where,coordinates.logSegmentTable());
        logTableEnd(where);
      }

      {{
      out::stream where = out::LOGFILE;
      logTab(where,"Segmented Pruning Graphic After Refinement"+nN);
      logTabArray(where,coordinates.logSegmentGraphic(occupancy_refinement));
      logBlank(where);
      }}

      coordinates.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::COORDINATES_PDB));
      logFileWritten(out::LOGFILE,WriteFiles(),"Segmented",coordinates);
      if (WriteFiles()) coordinates.write_to_disk();
      entry->DENSITY.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::DENSITY_MTZ));
      logFileWritten(out::LOGFILE,WriteFiles(),"Density",entry->DENSITY);
      if (WriteFiles()) entry->DENSITY.write_to_disk();

      output.push_back(snprintftos("%-4d %10d %6.2f %5.1f%c %*.*f %*.*f %5.2f",
         n-1,
         nsegments,
         DAGDB.WORK->ZSCORE,DAGDB.WORK->PERCENT,'%',
         q,p,initllg,
         q,p,DAGDB.WORK->LLG,
         coordinates.percent_pruned()));
    }

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge by maximum stored");
      DAGDB.NODES.erase(DAGDB.NODES.begin()+io_maxstored,DAGDB.NODES.end());
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored: " + itos(io_maxstored));
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(DAGDB.NODES.size()));
    }
    }}

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Segmented Pruning Refinement Summary");
    logTabArray(where,output);
    logTableEnd(where);
    logBlank(where);
    }}

  }

} //phasertng
