//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <algorithm>
#include <phasertng/main/Assert.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>
#include <phasertng/math/table/sim.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
extern const table::sim& tbl_sim;
}

using namespace scitbx;

namespace phasertng { namespace likelihood { namespace isad {

  int
  function::calculate_integration_points(
      const double& RESN,
      const double& TEPS,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const cmplex& FHPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const cmplex& FHNEG,
      const double& epsnSigmaPos,
      const double& epsnSigmaNeg
    )
  {
    cmplex wFHneg = DOBSNEG*FHNEG; // Include D-factors here
    cmplex wFHpos = DOBSPOS*FHPOS; // Include D-factors here
    cmplex diff = wFHpos-wFHneg;
    double dFH = std::abs(diff);
    phaser_assert(TEPS*epsnSigmaNeg > 0);
    double X = std::abs(wFHneg)*FEFFNEG/(TEPS*epsnSigmaNeg);
    double ndano = dFH/std::sqrt(TEPS*epsnSigmaPos);
    double nsim = std::sqrt(X);
    double nstep_raw = 5 + 9*std::sqrt(ndano*ndano+nsim*nsim);
    double gaussFac = 1.5;
    int nstep = gaussFac*nstep_raw + 0.5; // scale and round
    phaser_assert(acentPhsIntg.size() > 1);
    nstep = std::max(nstep,acentPhsIntg[0].nStep);
    //search to acentPhsIntg.size() -1 and bail out if value >= nstep is found
    //set ibin to largest nstep value, the default value
    int ibin = acentPhsIntg.size()-1;
    for (int i = 0; i < acentPhsIntg.size()-1; i++) //find the index of with nstep value
    {
      if (acentPhsIntg[i].nStep >= nstep)
      {
        ibin = i;
        break;
      }
    }
    return ibin;
  }

  likelihood::isad::tgh
  function::tgh_acentric(
      const int& s,
      const int& EPS,
      const double& TEPS, // Also need to account for ANISO?
      const double& DOBSPOS,
      const double& FEFFPOS,
      const cmplex& FHPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const cmplex& FHNEG,
      const likelihood::isad::tgh_flags& flags,
      const int& i
    ) const
  {
    likelihood::isad::tgh TGH;
    phaser_assert(s < SADSIGMAA->SigmaN_bin.size());
    const double& SigmaN = SADSIGMAA->SigmaN_bin[s];
    const double& SigmaH = SADSIGMAA->SigmaH_bin[s];
    const double& fDelta = SADSIGMAA->fDelta_bin[s];
    const double& rhopm = SADSIGMAA->rhopm_bin[s];
    const double CEPS = EPS*TEPS;
    const double DOBSNEG2 = fn::pow2(DOBSNEG);
    const double DOBSPOS2 = fn::pow2(DOBSPOS);
    const double DOBSNEGDOBSPOS = DOBSNEG*DOBSPOS;
    const double SigmaDelta = (SigmaN - SigmaH);
    TGH.SigmaNeg = (SigmaN - DOBSNEG2*SigmaH);
    const double rhopmfac = std::sqrt((1.-DOBSPOS2)*(1.-DOBSNEG2))*SigmaN;
    const double SigmaDeltafac = DOBSNEGDOBSPOS*(1.-fDelta);
    const double sigmaPhiObs = SigmaDeltafac*SigmaDelta + rhopm*rhopmfac;
    const double sigmaPhiObsSqr = fn::pow2(sigmaPhiObs);
    phaser_assert(TGH.SigmaNeg > 0); //divide by zero below
    TGH.SigmaPos = (SigmaN - DOBSPOS2*SigmaH) - sigmaPhiObsSqr/TGH.SigmaNeg;
    phaser_assert(TGH.SigmaPos > 0);
    if (flags.target())
    {
      phaser_assert(i < acentPhsIntg.size());
      const EqualUnity& PhsIntg = acentPhsIntg[i]; //reference for fast indexing
      double epsnSigmaNeg = CEPS*TGH.SigmaNeg;
      double epsnSigmaNegInv = 1.0/epsnSigmaNeg;
      double SigmaNegInv = 1.0/TGH.SigmaNeg;
      double SigmaPosInv = 1.0/TGH.SigmaPos;
      double epsnSigmaPos = CEPS*TGH.SigmaPos;
      double epsnSigmaPosInv = 1.0/epsnSigmaPos;
      cmplex wFHneg = DOBSNEG*FHNEG;
      cmplex wFHpos = DOBSPOS*FHPOS;
      //double ReFHneg = std::real(FHNEG);   double ImFHneg = std::imag(FHNEG);
      //double ReFHpos = std::real(FHPOS);   double ImFHpos = std::imag(FHPOS);
      //double RewFHneg = std::real(wFHneg); double ImwFHneg = std::imag(wFHneg);
     // double RewFHpos(std::real(wFHpos));  double ImwFHpos = std::imag(wFHpos);
      double scale = TWO*FEFFPOS*FEFFNEG/(scitbx::constants::pi*epsnSigmaPos*epsnSigmaNeg);
      double BessFac = TWO*FEFFPOS*epsnSigmaPosInv;
      //used later
      double dSigmaPos_by_dSigmaH(0);
      double dSigmaNeg_by_dSigmaH(0);
      double dSigmaPos_by_dfDelta(0);
      double dSigmaPos_by_drhopm(0);
      double dsigmaPhiObs_by_drhopm(0);
      double dsigmaPhiObs_by_dSigmaH(0);
      double dsigmaPhiObs_by_dfDelta(0);
      if (flags.gradient())
      {
        double dSigmaPos_by_dsigmaPhiObs = -TWO*sigmaPhiObs*SigmaNegInv;
        dsigmaPhiObs_by_dfDelta = -DOBSNEGDOBSPOS*SigmaDelta;
        dSigmaPos_by_dfDelta = dSigmaPos_by_dsigmaPhiObs*dsigmaPhiObs_by_dfDelta;
        dsigmaPhiObs_by_drhopm = rhopmfac;
        dSigmaPos_by_drhopm = dSigmaPos_by_dsigmaPhiObs*dsigmaPhiObs_by_drhopm;
        dSigmaNeg_by_dSigmaH = -DOBSNEG2;
        dsigmaPhiObs_by_dSigmaH  = -SigmaDeltafac;
        dSigmaPos_by_dSigmaH =
            -DOBSPOS2 - TWO*sigmaPhiObs*dsigmaPhiObs_by_dSigmaH/TGH.SigmaNeg +
            fn::pow2(sigmaPhiObs/TGH.SigmaNeg)*dSigmaNeg_by_dSigmaH;
        if (flags.hessian())
        {
        }
      }
      //Arguments for exponential will be negative, with the maximum typically close to zero.
      //To attempt to avoid underflow, set initial exponential offset that is small enough
      //to still avoid overflow when summing and squaring for FOM calculation.
      //If necessary, this will be updated and the integral repeated.
      //Exponential offset cancels out in derivative calculation, but correction must be made for Like.
      //Note that Like is positive likelihood, but derivatives taken wrt -Log(likelihood).
      double topExpArg(-std::numeric_limits<double>::max());
      double expArgOffset(maxArgAllow);
      double Like = 0;
      //declare derivatives of likelihood (not loglikelihood)
      double dL_by_dReFHpos(0);
      double dL_by_dImFHpos(0);
      double dL_by_dReFHneg(0);
      double dL_by_dImFHneg(0);
      double dL_by_dfDelta(0);
      double dL_by_dSigmaH(0);
      double dL_by_drhopm(0);
      //end
      for (int oloop = 0; oloop < 2; oloop++)
      {
        Like = 0;
        //derivatives of likelihood (not loglikelihood)
        dL_by_dReFHpos = 0;
        dL_by_dImFHpos = 0;
        dL_by_dReFHneg = 0;
        dL_by_dImFHneg = 0;
        dL_by_dfDelta = 0;
        dL_by_dSigmaH = 0;
        dL_by_drhopm = 0;
        //local
        //these are returned at end of loop, used for storage of TGH varables
        double probCosAng(0),probSinAng(0);
        TGH.HLA = TGH.HLB = TGH.HLC = TGH.HLD = 0;
        for (int p = 0; p < PhsIntg.nStep; p++)
        {
          const double& CosAng =  PhsIntg.CosAng[p]; //for speed, no offset
          const double& SinAng =  PhsIntg.SinAng[p]; //for speed, no offset
          const double& Cos2Ang =  PhsIntg.Cos2Ang[p]; //for speed, no offset
          const double& Sin2Ang =  PhsIntg.Sin2Ang[p]; //for speed, no offset
          cmplex FCneg = { FEFFNEG*CosAng,FEFFNEG*SinAng };
          cmplex DeltaNeg = FCneg-wFHneg;
          double ReDeltaNeg = std::real(DeltaNeg);
          double ImDeltaNeg = std::imag(DeltaNeg);
          cmplex FCpos = (wFHpos + sigmaPhiObs*SigmaNegInv*DeltaNeg);
          double ReFCpos = std::real(FCpos);
          double ImFCpos = std::imag(FCpos);
          double absFCpos = std::abs(FCpos);
          double argFHneg = -std::norm(DeltaNeg)*epsnSigmaNegInv;
          double DeltaPos = FEFFPOS-absFCpos;
          double argFOpos = -fn::pow2(DeltaPos)*epsnSigmaPosInv;
          double expArg = argFHneg+argFOpos;
          //reset topExpArg
          topExpArg = std::max(topExpArg,expArg);
          double expArgTotal = expArg + expArgOffset;
          double expTerm = (expArgTotal > minArgAllow) ? std::exp(expArgTotal) : 0.;
          double X = BessFac*absFCpos;
          double eBessI0 = tbl_ebesseli0.get(X);
          double phsProb = scale*expTerm*eBessI0;
          Like += phsProb;
          if (flags.phsprob() && phsProb > 0)
          {
            probCosAng += phsProb*CosAng;
            probSinAng += phsProb*SinAng;
            double logThisPhsProb = std::log(phsProb);
            TGH.HLA += logThisPhsProb*CosAng;
            TGH.HLB += logThisPhsProb*SinAng;
            TGH.HLC += logThisPhsProb*Cos2Ang;
            TGH.HLD += logThisPhsProb*Sin2Ang;
          }
          if (flags.gradient())
          {
            double eBessI1 = tbl_ebesseli1.get(X);
            double dL_by_dargFHneg = phsProb;
            double dL_by_dargFOpos = phsProb;
            double dL_by_dX = scale*expTerm*(eBessI1-eBessI0);
            double dargFOpos_by_dabsFCpos = TWO*DeltaPos*epsnSigmaPosInv;
            double dX_by_dabsFCpos = BessFac;
            double dL_by_dabsFCpos = dL_by_dargFOpos*dargFOpos_by_dabsFCpos +
                                     dL_by_dX*dX_by_dabsFCpos;
            double normfac = TWO*DOBSNEG*epsnSigmaNegInv;
            double dargFHneg_by_dReFHneg = normfac*ReDeltaNeg;
            double dargFHneg_by_dImFHneg = normfac*ImDeltaNeg;

            //condition below
            double dabsFCpos_by_dReFHpos(0);
            double dabsFCpos_by_dImFHpos(0);
            double dabsFCpos_by_dReFHneg(0);
            double dabsFCpos_by_dImFHneg(0);
            double dabsFCpos_by_dSigmaH(0);
            double dabsFCpos_by_drhopm(0);
            double dabsFCpos_by_dfDelta(0);
            if (absFCpos > 0.)
            {
              normfac = DOBSPOS/absFCpos; //recycle
              dabsFCpos_by_dReFHpos = normfac*ReFCpos;
              dabsFCpos_by_dImFHpos = normfac*ImFCpos;
              normfac *= -DOBSNEG2*SigmaNegInv; //recycle
              dabsFCpos_by_dReFHneg = normfac*sigmaPhiObs*ReFCpos; // assume sigmaPhi real
              dabsFCpos_by_dImFHneg = normfac*sigmaPhiObs*ImFCpos;
              // For next two derivs, assuming sigmaPhiObs is real
              normfac = (dsigmaPhiObs_by_dSigmaH - sigmaPhiObs*SigmaNegInv*dSigmaNeg_by_dSigmaH);
              normfac *= SigmaNegInv; //recycle
              double dReFCpos_by_dSigmaH = normfac*ReDeltaNeg;
              double dImFCpos_by_dSigmaH = normfac*ImDeltaNeg;
              dabsFCpos_by_dSigmaH =
                  (ReFCpos*dReFCpos_by_dSigmaH + ImFCpos*dImFCpos_by_dSigmaH)/absFCpos;
              double dReFCpos_by_drhopm = SigmaNegInv*ReDeltaNeg*dsigmaPhiObs_by_drhopm;
              double dImFCpos_by_drhopm = SigmaNegInv*ImDeltaNeg*dsigmaPhiObs_by_drhopm;
              dabsFCpos_by_drhopm =
                  (ReFCpos*dReFCpos_by_drhopm + ImFCpos*dImFCpos_by_drhopm)/absFCpos;
              double normfac = DOBSNEGDOBSPOS*SigmaNegInv*dsigmaPhiObs_by_dfDelta;
              double dReFCpos_by_dfDelta = normfac*ReDeltaNeg;
              double dImFCpos_by_dfDelta = normfac*ImDeltaNeg;
              dabsFCpos_by_dfDelta =
                  (dReFCpos_by_dfDelta*ReFCpos+dImFCpos_by_dfDelta*ImFCpos)/absFCpos;
            }
            dL_by_dReFHpos +=
                dL_by_dabsFCpos*dabsFCpos_by_dReFHpos;
            dL_by_dImFHpos +=
                dL_by_dabsFCpos*dabsFCpos_by_dImFHpos;
            dL_by_dReFHneg +=
                dL_by_dabsFCpos*dabsFCpos_by_dReFHneg +
                dL_by_dargFHneg*dargFHneg_by_dReFHneg;
            dL_by_dImFHneg +=
                dL_by_dabsFCpos*dabsFCpos_by_dImFHneg +
                dL_by_dargFHneg*dargFHneg_by_dImFHneg;
            {//memory dL_by_dSigmaH
            double dargFHneg_by_dSigmaH =
                -argFHneg*SigmaNegInv*dSigmaNeg_by_dSigmaH;
            double dX_by_dSigmaH =
                dX_by_dabsFCpos*dabsFCpos_by_dSigmaH -
                X*dSigmaPos_by_dSigmaH*SigmaPosInv;
            double dargFOpos_by_dSigmaH =
                -argFOpos*SigmaPosInv*dSigmaPos_by_dSigmaH +
                dargFOpos_by_dabsFCpos*dabsFCpos_by_dSigmaH;
            dL_by_dSigmaH +=
                -phsProb*(dSigmaPos_by_dSigmaH*SigmaPosInv +
                          dSigmaNeg_by_dSigmaH*SigmaNegInv) +
                dL_by_dargFHneg*dargFHneg_by_dSigmaH +
                dL_by_dargFOpos*dargFOpos_by_dSigmaH +
                dL_by_dX * dX_by_dSigmaH;
            }
            { //memory
            double dX_by_drhopm =
                dX_by_dabsFCpos*dabsFCpos_by_drhopm -
                X*dSigmaPos_by_drhopm*SigmaPosInv;
            double dargFOpos_by_drhopm =
                -argFOpos*SigmaPosInv*dSigmaPos_by_drhopm +
                dargFOpos_by_dabsFCpos*dabsFCpos_by_drhopm;
            //normfac = TWO*DeltaPos*epsnSigmaPosInv;
            dL_by_drhopm +=
                -phsProb*dSigmaPos_by_drhopm*SigmaPosInv +
                dL_by_dargFOpos*dargFOpos_by_drhopm +
                dL_by_dX * dX_by_drhopm;
            }
            { //memory
            double dargFOpos_by_dfDelta =
                TWO*DeltaPos*epsnSigmaPosInv*dabsFCpos_by_dfDelta -
                argFOpos*SigmaPosInv*dSigmaPos_by_dfDelta;
            double dX_by_dfDelta =
                dX_by_dabsFCpos*dabsFCpos_by_dfDelta -
                X*SigmaPosInv*dSigmaPos_by_dfDelta;
            dL_by_dfDelta +=
                -phsProb*(dSigmaPos_by_dfDelta*SigmaPosInv) +
                dL_by_dargFOpos*dargFOpos_by_dfDelta +
                dL_by_dX * dX_by_dfDelta;
            }
          }
        } //loop over p
        //calculate phase probability term if required
        if (flags.phsprob())
        {
          TGH.PHIB =  atan2(probSinAng,probCosAng);
          TGH.FOM = (Like > 0) ? std::sqrt(probCosAng*probCosAng + probSinAng*probSinAng)/Like : 0;
          double normfac = 2./static_cast<double>(PhsIntg.nStep);
          TGH.HLA *= normfac;
          TGH.HLB *= normfac;
          TGH.HLC *= normfac;
          TGH.HLD *= normfac;
          //TGH.HL = cctbx::hendrickson_lattman<double>(HLA,HLB,HLC,HLD);
        }
        //if this is ok, break, else reset and repeat
        if (Like > 0.) break;
        expArgOffset = maxArgAllow - topExpArg;
      } //loop to apply new offset if necessary
      phaser_assert(Like > 0); // Could probably eliminate now
      double LikeFactor(-1./Like); // Before weighting, as derivatives are unweighted. Change sign for -log(Like).
      Like *= PhsIntg.Weight;
      TGH.LogLike = -log(Like) + expArgOffset; //Remove offset
      //now convert to LL grads
      TGH.dLL_by_dReFHpos   = dL_by_dReFHpos*LikeFactor;
      TGH.dLL_by_dImFHpos   = dL_by_dImFHpos*LikeFactor;
      TGH.dLL_by_dReFHneg   = dL_by_dReFHneg*LikeFactor;
      TGH.dLL_by_dImFHneg   = dL_by_dImFHneg*LikeFactor;
      TGH.dLL_by_dfDelta    = dL_by_dfDelta*LikeFactor;
      TGH.dLL_by_dSigmaH    = dL_by_dSigmaH*LikeFactor;
      TGH.dLL_by_drhopm     = dL_by_drhopm*LikeFactor;
      //Store measure of relative influence of partial structure for
      //bias-corrected map coefficients
      //double Xano(std::norm(wFHpos-wFHneg)*epsnSigmaPosInv);
      //double Xpart(std::abs(wFHneg)*FEFFNEG*epsnSigmaNegInv);
      //TGH.WtPart = (Xpart > 0.) ? Xpart/(Xano + Xpart) : 0.; //complicated bias correction
    }
    return TGH;
  }

  likelihood::isad::tgh
  function::tgh_centric(
      const int& s,
      const int& EPS,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const double& PHSR,
      const likelihood::isad::tgh_flags& flags
    ) const
  {
    likelihood::isad::tgh TGH;
    const double DOBS2 = fn::pow2(DOBS);
    TGH.SigmaNeg = (SADSIGMAA->SigmaN_bin[s] - DOBS2*SADSIGMAA->SigmaH_bin[s]);
    phaser_assert(TGH.SigmaNeg > 0);
    if (flags.target())
    {
      double epsnSigmaNeg = EPS*TEPS*TGH.SigmaNeg;
      double absFH     = std::abs(FH);
      double phipos    = std::arg(FH);
      double wFH       = DOBS*absFH;
      double argfac    = 1./(TWO*epsnSigmaNeg);
      double normFac   = (1./std::sqrt(scitbx::constants::two_pi*epsnSigmaNeg));
      double diff1     = FEFF-wFH;
      double sum2      = FEFF+wFH;
      double exparg1   = -fn::pow2(diff1)*argfac;
      double exparg2   = -fn::pow2(sum2)*argfac;
      double expArgOffset = maxArgAllow-std::max(exparg1,exparg2);
      if (std::abs(exparg1-exparg2) > logMaxFloat)
      {
        if (exparg1<exparg2) exparg1 = exparg2 - logMaxFloat;
        else exparg2 = exparg1 - logMaxFloat;
      }
      double prob1  = std::exp(exparg1+expArgOffset)*normFac;
      double prob2  = std::exp(exparg2+expArgOffset)*normFac;
      double Like   = prob1+prob2;
      phaser_assert(Like > 0.);
      TGH.LogLike = -std::log(Like)+expArgOffset;
      if (flags.phsprob())
      {
        TGH.PHIB = (prob1 > prob2) ? PHSR : PHSR + scitbx::constants::pi;
        TGH.FOM = (Like > 0) ? std::fabs(prob1 - prob2)/Like : 0;
        double X = std::fabs(std::log(prob1)-std::log(prob2))/2;
        cmplex cos_sin = tbl_cos_sin.get(TGH.PHIB/360.); //multiplies by 2*pi internally
        const double& cosphib = std::real(cos_sin);
        const double& sinphib = std::imag(cos_sin);
        TGH.HLA = X*cosphib;
        TGH.HLB = X*sinphib;
        TGH.HLC = TGH.HLD = 0;
        //TGH.HL = cctbx::hendrickson_lattman<double>(HLA,HLB,0,0);
      }
      if (flags.gradient())
      {
        double dL_by_dReFHpos(0);
        double dL_by_dImFHpos(0);
        if (absFH > 0) // Derivatives stay as zero if absFH is zero
        {
          cmplex cos_sin = tbl_cos_sin.get(phipos/360.); //multiplies by 2*pi internally
          double dL_by_dabsFH = DOBS*(prob1*diff1-prob2*sum2)/epsnSigmaNeg;
          const double& cosphipos = std::real(cos_sin);
          const double& sinphipos = std::imag(cos_sin);
          dL_by_dReFHpos = dL_by_dabsFH*cosphipos;
          dL_by_dImFHpos = dL_by_dabsFH*sinphipos;
        }
        double dSigmaNeg_by_dSigmaH = -DOBS2;
        double dL_by_dSigmaNeg = -(Like/2.+prob1*exparg1+prob2*exparg2)/TGH.SigmaNeg;
        double dL_by_dSigmaH = dL_by_dSigmaNeg*dSigmaNeg_by_dSigmaH;
        double LikeFactor(-1./Like); // Change sign for deriv wrt -Log(Like)
        TGH.dLL_by_dReFHpos = dL_by_dReFHpos*LikeFactor;
        TGH.dLL_by_dImFHpos = dL_by_dImFHpos*LikeFactor;
        TGH.dLL_by_dSigmaH  = dL_by_dSigmaH*LikeFactor;
      }
    }
    return TGH;
  }

  likelihood::isad::tgh
  function::tgh_singleton(
      const bool& PLUS,
      const int& s,
      const int& EPS,
      const double& TEPS,
      const double& DOBS,
      const double& FEFF,
      const cmplex& FH,
      const likelihood::isad::tgh_flags& flags
    ) const
  {
    likelihood::isad::tgh TGH;
    double DOBS2 = fn::pow2(DOBS);
    TGH.SigmaNeg = SADSIGMAA->SigmaN_bin[s] - DOBS2*SADSIGMAA->SigmaH_bin[s];
    if (flags.target())
    {
      phaser_assert(TGH.SigmaNeg > 0);
      double epsnSigmaNeg = EPS*TEPS*TGH.SigmaNeg;
      double absFH = std::abs(FH);
      double wFH = DOBS*absFH;
      double ReF = std::real(FH);
      double ImF = std::imag(FH);
      double X = 2*FEFF*wFH/epsnSigmaNeg;
      double eBessI0 = tbl_ebesseli0.get(X);
      //Arguments for exponential will be negative, with the maximum typically close to zero.
      double expArg = -fn::pow2(FEFF-wFH)/epsnSigmaNeg;
             expArg = std::max(expArg,minExpArgHalf);
      double expterm = std::exp(expArg);
      double scale(2*FEFF/epsnSigmaNeg);
      double Like = scale*expterm*eBessI0;
      phaser_assert(Like >= 0.);
      TGH.LogLike = -log(Like);
      if (flags.phsprob())
      {
        TGH.FOM = tbl_sim.get(X);
        TGH.PHIB = std::arg(FH);
        cmplex cos_sin = tbl_cos_sin.get(TGH.PHIB/360.); //multiplies by 2*pi internally
        const double& cosphib = std::real(cos_sin);
        const double& sinphib = std::imag(cos_sin);
        TGH.HLA = X*cosphib;
        TGH.HLB = X*sinphib;
        TGH.HLC = TGH.HLD = 0;
        //TGH.HL = cctbx::hendrickson_lattman<double>(HLA,HLB,0,0);
      }
      if (flags.gradient())
      {
        double LikeFactor = -1./Like; // Change sign for deriv wrt -Log(Like)
        double eBessI1 = tbl_ebesseli1.get(X);
        double dL_by_dX = scale*expterm*(eBessI1-eBessI0);
        //double dX_by_dSigmaH = DOBS2*X/TGH.SigmaNeg;
        double dL_by_dexpArg = Like;
        if (absFH)
        {
          double dexpArg_by_dabsFH = 2.*DOBS*(FEFF-wFH)/epsnSigmaNeg;
          double dX_by_dabsFH = DOBS*scale;
          double dL_by_dabsFH = dL_by_dexpArg*dexpArg_by_dabsFH + dL_by_dX*dX_by_dabsFH;
          double dL_by_dReF = dL_by_dabsFH*ReF/absFH;
          double dL_by_dImF = dL_by_dabsFH*ImF/absFH;
          double dL_by_dReFHpos = PLUS ? dL_by_dReF : 0;
          double dL_by_dImFHpos = PLUS ? dL_by_dImF : 0;
          double dL_by_dReFHneg = PLUS ? 0 : dL_by_dReF;
          double dL_by_dImFHneg = PLUS ? 0 : dL_by_dImF;
          TGH.dLL_by_dReFHpos = dL_by_dReFHpos*LikeFactor;
          TGH.dLL_by_dImFHpos = dL_by_dImFHpos*LikeFactor;
          TGH.dLL_by_dReFHneg = dL_by_dReFHneg*LikeFactor;
          TGH.dLL_by_dImFHneg = dL_by_dImFHneg*LikeFactor;
        }
        //double dSigmaNeg_by_dSigmaH = -DOBS2;
        //double dscale_by_dSigmaH(-scale*dSigmaNeg_by_dSigmaH/TGH.SigmaNeg);
        //double dexpArg_by_dSigmaH(expArg*DOBS2/TGH.SigmaNeg);
        //double dL_by_dscale(expterm*eBessI0);
        //dL_by_dSigmaH = dL_by_dscale*dscale_by_dSigmaH +
        //                dL_by_dexpArg*dexpArg_by_dSigmaH +
        //                dL_by_dX*dX_by_dSigmaH;
        double dL_by_dSigmaH = (DOBS/TGH.SigmaNeg)*(Like*(1.+expArg)+X*dL_by_dX); //equiv but simplified
        TGH.dLL_by_dSigmaH  = dL_by_dSigmaH*LikeFactor;
      }
    }
    return TGH;
  }

  likelihood::isad::tgh
  function::tgh_null(
      const int& s,
      const double& DOBSPOS,
      const double& FEFFPOS,
      const double& DOBSNEG,
      const double& FEFFNEG,
      const double& RESN,
      const double& TEPS,
      const likelihood::isad::tgh_flags& flags
    ) const
  {
    likelihood::isad::tgh TGH;
    const double ESN = TEPS*fn::pow2(RESN); // Include tNCS epsilon
    phaser_assert(ESN > 0);
    phaser_assert(s < SADSIGMAA->absrhoFF_bin.size());
    const double& absrhoFF = SADSIGMAA->absrhoFF_bin[s];
    phaser_assert(s < SADSIGMAA->rhopm_bin.size());
    const double& rhopm = SADSIGMAA->rhopm_bin[s];
    double DOBSPOS2 = fn::pow2(DOBSPOS);
    double DOBSNEG2 = fn::pow2(DOBSNEG);
    double drhoFFobs_by_drhopm = std::sqrt((1.-DOBSPOS2)*(1.-DOBSNEG2));
    double rhoFFobs = DOBSPOS*DOBSNEG*absrhoFF + rhopm*drhoFFobs_by_drhopm;
    double rhoFFobs2 = fn::pow2(rhoFFobs);
    phaser_assert(rhoFFobs2 < 1.);
    double SigmaNULL = ESN*(1.-rhoFFobs2);
    phaser_assert(SigmaNULL > 0);
    double X = TWO*FEFFPOS*FEFFNEG*rhoFFobs/SigmaNULL;
    double eBessI0 = tbl_ebesseli0.get(X);
    double arg = -(fn::pow2(FEFFPOS)+fn::pow2(FEFFNEG))/SigmaNULL;
    double Like = (4.*FEFFPOS*FEFFNEG/(ESN*SigmaNULL))*std::exp(arg+X)*eBessI0;
    if (Like < minFloat) // Potential untrapped outlier
    {
      TGH.LogLike = -std::log(minFloat);
      if (flags.gradient()) // No variation while Like set to constant
      {
        TGH.dLL_by_dabsrhoFF = 0.;
        TGH.dLL_by_drhopm = 0.;
        if (flags.hessian())
        {
          TGH.d2LL_by_dabsrhoFF2 = 0.;
          TGH.d2LL_by_drhopm2 = 0.;
          TGH.d2LL_by_dabsrhoFF_by_drhopm = 0.;
        }
      }
    }
    else
    {
      TGH.LogLike = -std::log(Like);
      if (flags.gradient())
      {
        double eBessI1 = tbl_ebesseli1.get(X);
        double besrat = eBessI1/eBessI0;
        double dSigmaNULL_by_drhoFFobs = -TWO*ESN*rhoFFobs;
        double sigmaNull_by_rhoFFfactor = - (1. + arg + X*besrat)/SigmaNULL;
        double dL_by_drhoFFobs = Like *
          (dSigmaNULL_by_drhoFFobs*sigmaNull_by_rhoFFfactor + X*besrat/rhoFFobs);
        double drhoFFobs_by_dabsrhoFF = DOBSPOS*DOBSNEG;
        double dL_by_dabsrhoFF = dL_by_drhoFFobs*drhoFFobs_by_dabsrhoFF;
        double dL_by_drhopm    = dL_by_drhoFFobs*drhoFFobs_by_drhopm;
        TGH.dLL_by_dabsrhoFF = -dL_by_dabsrhoFF/Like;
        TGH.dLL_by_drhopm    = -dL_by_drhopm/Like;
        if (flags.hessian())
        {
          double d2SigmaNULL_by_drhoFFobs2 = -TWO*ESN;
          double dX_by_drhoFFobs = -X*dSigmaNULL_by_drhoFFobs/SigmaNULL + X/rhoFFobs;
          double dXbesrat_by_drhoFFobs = X*(1.-fn::pow2(besrat)) *
            dX_by_drhoFFobs;
          double darg_by_drhoFFobs = -arg/SigmaNULL*dSigmaNULL_by_drhoFFobs;
          double drhoFFdfactor_by_drhoFFobs =
            -(darg_by_drhoFFobs + dXbesrat_by_drhoFFobs +
            sigmaNull_by_rhoFFfactor*dSigmaNULL_by_drhoFFobs)/SigmaNULL;
          double d2L_by_drhoFFobs2 = fn::pow2(dL_by_drhoFFobs)/Like +
            Like*(d2SigmaNULL_by_drhoFFobs2*sigmaNull_by_rhoFFfactor +
                  dSigmaNULL_by_drhoFFobs*drhoFFdfactor_by_drhoFFobs +
                  dXbesrat_by_drhoFFobs/rhoFFobs - X*besrat/rhoFFobs2);
          // Potential second terms of next two are zero
          double d2L_by_dabsrhoFF2 = d2L_by_drhoFFobs2*fn::pow2(drhoFFobs_by_dabsrhoFF);
          double d2L_by_drhopm2    = d2L_by_drhoFFobs2*fn::pow2(drhoFFobs_by_drhopm);
          double d2L_by_dabsrhoFF_by_drhopm =
            d2L_by_drhoFFobs2*drhoFFobs_by_dabsrhoFF*drhoFFobs_by_drhopm;
          TGH.d2LL_by_dabsrhoFF2 =
            -d2L_by_dabsrhoFF2/Like + fn::pow2(dL_by_dabsrhoFF/Like);
          TGH.d2LL_by_drhopm2 =
            -d2L_by_drhopm2/Like    + fn::pow2(dL_by_drhopm/Like);
          phaser_assert(TGH.d2LL_by_drhopm2 != 0);
          TGH.d2LL_by_dabsrhoFF_by_drhopm =
            -d2L_by_dabsrhoFF_by_drhopm/Like + dL_by_dabsrhoFF*dL_by_drhopm/fn::pow2(Like);
        }
      }
    }
    return TGH;
  }

}}}//phasertng
