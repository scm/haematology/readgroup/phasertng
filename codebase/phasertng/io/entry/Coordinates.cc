//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Coordinates.h>
#include <phasertng/main/includes.h>
#include <phasertng/main/constants.h>
//includes for fft partial structure calculations
#include <cctbx/xray/scattering_type_registry.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <unordered_set>
#include <fstream>

//#define PHASERTNG_DEBUG_COORDINATES

namespace phasertng {

  void
  Coordinates::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());
    //concatenate instead
    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        //have to parse lines in optional pairs ATOM/ANISOU
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    infile.close();
    SetPdb(pdblines);
  }

  void
  Coordinates::SetPdb(af_string pdblines)
  {
    XRAY.clear();
    PDB.clear();
#ifdef PHASERTNG_DEBUG_COORDINATES
    try
    {
#endif
      //keep the next_atom outside the ATOM parsing scope
      //because we are going to check that if the next record is an ANISOU or REMARK
      //that it corresponds with the revious ATOM record
      PdbRecord next_atom;
      bool previous_atom_record(false);

      for (auto buffer : pdblines)
      {
        //have to parse lines in optional pairs ATOM/ANISOU
        if (!buffer.find("CRYST1"))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string cryst1; af::double6 uc;
            sstream >> cryst1;
            sstream >> uc[0];
            sstream >> uc[1];
            sstream >> uc[2];
            sstream >> uc[3];
            sstream >> uc[4];
            sstream >> uc[5];
            std::string sg = buffer.substr(55,10);
            //int z = std::atoi(buffer.substr(66,4).c_str()); //Z is ignored
            UC = UnitCell(uc);
            SG = SpaceGroup(sg); //will be P1, don't need to store
          }
          catch (...) { pdb_errors.insert("Error reading CRYST1"); }
        }
        if (!buffer.find("REMARK")) //REMARK
        {
          if (!buffer.find("REMARK " + constants::pdb_remark_number))
          {
            std::stringstream modelstream(buffer);
            std::string remark,remarktag,keyword;
            modelstream >> remark;
            modelstream >> remarktag;
            modelstream >> keyword;
            std::string number,tag("");
            if (keyword == "REFLID")
            {
              modelstream >> number;
              modelstream >> tag;
              REFLID.parse_string(number,tag);
            }
            else if (keyword == "MODLID")
            {
              modelstream >> number;
              modelstream >> tag;
              MODLID.parse_string(number,tag);
            }
            else if (keyword == "TIMESTAMP")
            {
              modelstream >> TIMESTAMP;
            }
            else if (keyword == "PARTK")
            {
              modelstream >> PARTK;
              modelstream >> keyword;
              modelstream >> PARTU;
              if (PARTK <= 0 or PARTK > 1)
                pdb_errors.insert("PartK not valid");
            }
          }
          else
          {
            REMARKS.push_back(buffer);
          }
          previous_atom_record = false;
        }
        if (previous_atom_record and !buffer.find("ANISOU"))
        {
          bool match = next_atom.ReadAnisouCard(buffer);
          if (!match)
            pdb_errors.insert("ANISOU card does not match previous ATOM card");
          else
            AddAtom(next_atom);
          previous_atom_record = false;
        }
        //whatever this card is add last atom, except if ANISOU above
        if (previous_atom_record)
        {
          AddAtom(next_atom);
          previous_atom_record = false;
        }
        if (!buffer.find("ATOM") or !buffer.find("HETATM"))
        {
          //previous was atom so add it here and parse for next atom
          previous_atom_record = false;
          next_atom = PdbRecord();
          next_atom.ReadAtomCard(buffer);
          if (next_atom.O < 0.0 and next_atom.AtomName != "UNK ")
            pdb_errors.insert("Atom has negative occupancy");
          else if (next_atom.at_xplor_infinity() and next_atom.O != 0)
            pdb_errors.insert("Atom has coordinates at \"infinity\"");
          else if (!next_atom.valid_element)
            pdb_errors.insert("Atom scatterer not recognised");
          else
            previous_atom_record = true;
        }
      }
      //clean up from last line
      if (previous_atom_record)
      {
        AddAtom(next_atom);
      }
#ifdef PHASERTNG_DEBUG_COORDINATES
    }
    catch (std::exception const& err) {
    }
#endif
  }

  void
  Coordinates::AddAtom(PdbRecord new_atom)
  {
    if (new_atom.AtomName == "UNK ") return; //ignored
    cctbx::xray::scatterer<double> new_xray;
    std::tie(new_atom,new_xray) = ProcessAtom(new_atom);
    PDB.push_back(new_atom);
    XRAY.push_back(new_xray);
  }

  void
  Coordinates::SetAtom(int a,PdbRecord new_atom)
  {
    if (new_atom.AtomName == "UNK ") return; //ignored
    std::tie(PDB[a],XRAY[a]) = ProcessAtom(new_atom);
  }

  void
  Coordinates::SetIsotropicU(int a,double U)
  {
    XRAY[a].u_iso = U;
    PDB[a].B = cctbx::adptbx::u_as_b(U);
    PHASER_ASSERT(PDB[a].B >= BMIN);
    PHASER_ASSERT(PDB[a].B <= BMAX);
  }

  void
  Coordinates::SetOccupancy(int a,double occ)
  {
    XRAY[a].occupancy = occ;
    PDB[a].O = occ;
  }

  std::pair<PdbRecord,cctbx::xray::scatterer<double>>
  Coordinates::ProcessAtom(PdbRecord new_atom)
  {
    PHASER_ASSERT(!UC.is_default()); //needed for frac
    cctbx::xray::scatterer<double> new_xray;
    new_xray.site = UC.fractionalization_matrix()*new_atom.X;
    new_xray.occupancy = new_atom.O;
    new_xray.scattering_type = new_atom.get_element();

    if (!new_atom.FlagAnisoU)
    {
      new_atom.B = std::max(BMIN,new_atom.B);
      new_xray.u_iso = cctbx::adptbx::b_as_u(new_atom.B);
      new_xray.set_use_u(/* iso */ true, /* aniso */ false);
      new_xray.u_star = {0,0,0,0,0,0};
    }
    else //convert cartesian AnisoU records in different types
    {
      new_xray.u_iso = 0;
      new_xray.set_use_u(/* iso */ false, /* aniso */ true);
      double u_iso = cctbx::adptbx::b_as_u(new_atom.B);
      double u_cart_u_iso = cctbx::adptbx::u_cart_as_u_iso(new_atom.AnisoU/DEF_UCART_FACTOR);
      double tol(cctbx::adptbx::b_as_u(1.0e-02));
      //if the isotropic component of AnisoU is zero and not equal to the B, add together
      dmat6 u_cart;
      if (std::fabs(u_cart_u_iso) < tol and std::fabs(u_iso) > tol)  //not standard pdb format
      {
        u_cart = cctbx::adptbx::u_iso_as_u_cart(u_iso)*DEF_UCART_FACTOR;
        u_cart += new_atom.AnisoU;
        new_xray.u_star = cctbx::adptbx::u_cart_as_u_star(UC.cctbxUC,u_cart);
      }
      else //standard pdb format, or something we can't recognise
      {
        u_cart = new_atom.AnisoU;
        new_xray.u_star = cctbx::adptbx::u_cart_as_u_star(UC.cctbxUC,u_cart);
      }
      //new_xtra.USTAR = false; //not the default, cartesian
    }
    return { new_atom, new_xray };
  }

  void
  Coordinates::process(const Scatterers& SCATTERERS)
  {
    PHASER_ASSERT(!UC.is_default());
    phaser_assert(XRAY.size());
    scattering_type_registry.process(XRAY.const_ref());
    scattering_type_registry.assign_from_table(SCATTERERS.formfactor_table_name());
    SCATTERING = 0;
    for (int a = 0; a < XRAY.size(); a++)
    {
      site_sym_table.process(XRAY[a].apply_symmetry(UC.cctbxUC,SG.group()));
      //multiplicity includes symmetry operator count
      //double sym_multiplicity = site_sym_table.get(a).multiplicity();
      const std::string& t = XRAY[a].scattering_type;
      XRAY[a].fp = SCATTERERS.fp(t);
      XRAY[a].fdp = SCATTERERS.fdp(t);
      double atomic_number(0);
      try {
        cctbx::eltbx::tiny_pse::table cctbxAtom(XRAY[a].scattering_type);
        atomic_number = cctbxAtom.atomic_number();
      }
      catch (std::exception const& err)  {}
      SCATTERING += scitbx::fn::pow2(atomic_number)*XRAY[a].occupancy;
    }
  }

  double
  Coordinates::scattering()
  { //useful after change in occupancy
    SCATTERING = 0;
    for (int a = 0; a < XRAY.size(); a++)
    {
      double atomic_number(0);
      try {
        cctbx::eltbx::tiny_pse::table cctbxAtom(XRAY[a].scattering_type);
        atomic_number = cctbxAtom.atomic_number();
      }
      catch (std::exception const& err)  {}
      SCATTERING += scitbx::fn::pow2(atomic_number)*XRAY[a].occupancy;
    }
    return SCATTERING;
  }

  void
  Coordinates::GetPdbHandler(PdbHandler& pdbhandler)
  {
    PHASER_ASSERT(SG.HALL.size());
    PHASER_ASSERT(!UC.is_default());
    PHASER_ASSERT(REFLID.is_set_valid());
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(TIMESTAMP.size());
    pdbhandler = PdbHandler(SG,UC,1);
    for (auto item : REMARKS)
      pdbhandler.addVanillaRemark(item);
    pdbhandler.addRemarkPhaser("REFLID " + REFLID.string() + " " + REFLID.tag());
    pdbhandler.addRemarkPhaser("MODLID " + MODLID.string() + " " + MODLID.tag());
    pdbhandler.addRemarkPhaser("TIMESTAMP " + TIMESTAMP);
    pdbhandler.addRemarkPhaser("PARTK " + dtos(PARTK) + " PARTU " + dtos(PARTU));
    pdbhandler.addModel();
    for (int a = 0; a < PDB.size(); a++)
      pdbhandler.addRecord(PDB[a]);
  }

  void
  Coordinates::WritePdb()
  {
    PHASER_ASSERT(!UC.is_default());
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    pdbhandler.VanillaWritePdb(*this);
  }

  af_string
  Coordinates::get_pdb_cards()
  {
    PHASER_ASSERT(!UC.is_default());
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    return pdbhandler.PdbString();
  }

  std::set<std::string>
  Coordinates::set_of_atomtypes()
  {
    PHASER_ASSERT(!UC.is_default());
    std::set<std::string> ATOMTYPES;
    for (int a = 0; a < XRAY.size(); a++)
      ATOMTYPES.insert(XRAY[a].scattering_type);
    return ATOMTYPES;
  }


  bool
  Coordinates::calculate_segment_identifiers(const int nres)
  {
    PHASER_ASSERT(PDB.size());
    //first divide into residues
    std::vector<sv_int> atoms_in_residue; //atoms (number a) in each residue
    std::vector<std::pair<dvect3,std::string>> alphas; //X and chain per residue
    int  last_resnum = PDB.at(0).ResNum;
    atoms_in_residue.push_back(std::vector<int>(0));
    alphas.push_back({PDB.at(0).X,PDB.at(0).Chain}); //initialize with first atom in residue
    for (int a = 0; a < PDB.size(); a++)
    {
      int this_resnum = PDB.at(a).ResNum;
      if (this_resnum != last_resnum) //create new resudue
      {
        atoms_in_residue.push_back(std::vector<int>(0));
        alphas.push_back({PDB.at(a).X,PDB.at(a).Chain}); //initialize with first atom in residue
        last_resnum = this_resnum;
      }
      if (PDB.at(a).is_calpha() or PDB.at(a).is_palpha())
        alphas.back() = {PDB.at(a).X,PDB.at(a).Chain}; //overwrite with the real alpha atom position
      atoms_in_residue.back().push_back(a); //add this atom to the current residue
    }
    //first divide into connected sets of residues
    std::vector<sv_int> connected; //residues in each connected segment
    connected.push_back(std::vector<int>()); //first connected chain
    connected.back().push_back(0); //first residue in first connected chain
    std::string last_Chain; dvect3 last_alphaX;
    std::tie(last_alphaX,last_Chain) = alphas.at(1);
    for (int n = 1; n < atoms_in_residue.size(); n++) //start with second residue
    {
      std::string this_Chain; dvect3 this_alphaX;
      std::tie(this_alphaX,this_Chain) = alphas.at(n);
      bool new_chain(this_Chain != last_Chain);
      last_Chain = this_Chain;
      double distance_between_alphas = (this_alphaX-last_alphaX)*(this_alphaX-last_alphaX);
      bool separated = distance_between_alphas > fn::pow2(5.0);
      last_alphaX = this_alphaX;
      if (separated or new_chain)
        connected.push_back(std::vector<int>(0));
      connected.back().push_back(n); //add this atom to the current residue
    }

    int segid(1); bool maxed_out(false);//don't start output at zero
    for (int c = 0; c < connected.size(); c++) //start with second residue
    {
      int segment_nres = connected[c].size();
      if (segment_nres <= nres)
      { //do the best upi cam
        for (int n = 0; n < connected[c].size(); n++)
        {
          for (int b = 0; b < atoms_in_residue[connected[c][n]].size(); b++)
          {
            int a = atoms_in_residue[connected[c][n]][b];
            PDB.at(a).Segment = segid;
          }
        }
        maxed_out = true;
        segid++;
      }
      else
      {
        int remainder = segment_nres % nres; //the number hanging over at the end
        int window_nres(0);
        auto list_of_residues = connected[c];
        int nsegments(0);
        for (int n = 0; n < list_of_residues.size(); n++)
        {
          int maximum_nres = nres; //externally calculated, then local modification
          if (nsegments < remainder) //can't be =, if 0 don't inflate
            maximum_nres++; //inflate so that there are fewer left over at the end
          auto list_of_atoms = atoms_in_residue[list_of_residues[n]];
          for (int b = 0; b < list_of_atoms.size(); b++)
          {
            int a = list_of_atoms[b];
            PDB.at(a).Segment = segid;
          }
          window_nres++;
          if (window_nres == maximum_nres)
          {
            window_nres = 0;
            nsegments++;
            segid++;
          }
        }
      }
    }
    bool apply_tncs(true);
    if (apply_tncs)
    {
      std::stringstream s;
      std::map<int,std::set<int>> tncs_info;
      for (int a = 0; a < PDB.size(); a++)
      {
        auto& card = PDB[a];
        auto tgrp = card.tncs_group();
        if (tncs_info.count(tgrp.first))
          tncs_info[tgrp.first].insert(tgrp.second);
        else
          tncs_info[tgrp.first] = { tgrp.second };
      }
      for (auto t : tncs_info)
      {
        sv_int refsegid; //the ones we will copy for this t.first
        for (std::set<int>::iterator iter = t.second.begin(); iter != t.second.end(); iter++)
        {
          if (iter == t.second.begin())
          {
            for (int a = 0; a < PDB.size(); a++)
            {
              auto& card = PDB[a];
              auto tgrp = card.tncs_group();
              if (tgrp.first == t.first)
                refsegid.push_back(card.Segment);
            }
          }
          else
          {
            int aa(0); //counter for the refsegid atoms which we will copy across
            for (int a = 0; a < PDB.size(); a++)
            {
              auto& card = PDB[a];
              auto tgrp = card.tncs_group();
              if (tgrp.first == t.first) //match for tncs_group.first
              if (tgrp.second == *iter) //pick out of the loop by tncs_group.second
              {
                PHASER_ASSERT(aa < refsegid.size());
                card.Segment = refsegid[aa++];
              }
            }
          }
        }
      }
    }
    return maxed_out; //total number of segments
  }

  af_string
  Coordinates::logSegmentTable() const
  {
    af_string output;
    std::stringstream s;
    int nsegid = 0;
    nsegid = nsegments();
    output.push_back("Percent pruned " + dtos(percent_pruned(),6,2) + "%");
    output.push_back(snprintftos(
      "%-5s %8s %6s %5s %4s %9s %9s",
      "#","Residues","Chains","Atoms","Tncs","Average-B","Average-O"));
    for (int segid = 1; segid <= nsegid; segid++)
    {
      std::set<std::string> nchain;
      std::set<int> tncs;
      double avB(0),avO(0);
      int nres(0),natoms(0);
      for (size_t a = 0; a < PDB.size(); a++)
      {
        auto& card = PDB[a];
        if (card.Segment == segid)
        {
          avB += card.B;
          avO += card.O;
          natoms++;
          //residues will be same for tncs, can't count different residues
          if (card.is_calpha() or card.is_palpha())
            nres++;
          //but the chains will be different after tncs
          nchain.insert(card.Chain);
          tncs.insert(card.tncs_group().second);
        }
      }
      output.push_back(snprintftos("%-5d %8d %6d %5d %4d %9.2f %9s",
          segid,
          nres,
          nchain.size(),
          natoms,
          tncs.size(),
          avB/static_cast<double>(natoms),
          avO == 0 ? "0" : dtos((avO/static_cast<double>(natoms)),9,4).c_str()
          ));
    }
    return output;
  }

  af_string
  Coordinates::logSegmentStats() const
  {
    af_string output;
    std::map<int,int> count_nres;
    std::map<int,int> count_nchains;
    std::map<int,int> count_natoms;
    std::map<int,int> count_tncs;
    int nsegid = nsegments();
    int w(10);
    for (int segid = 1; segid <= nsegid; segid++)
    {
      std::set<std::string> nchain;
      std::set<int> tncs;
      int nres(0),natoms(0);
      for (size_t a = 0; a < PDB.size(); a++)
      {
        auto& card = PDB[a];
        if (card.Segment == segid)
        {
          natoms++;
          //residues will be same for tncs, can't count different residues
          if (card.is_calpha() or card.is_palpha())
            nres++;
          //but the chains will be different after tncs
          nchain.insert(card.Chain);
          tncs.insert(card.tncs_group().second);
        }
      }
      int nchains = nchain.size();
      int ntncs = tncs.size();
      if (!count_nres.count(nres)) count_nres[nres] = 1;
      else count_nres[nres]++;
      w = std::max(w,count_nres[nres]);
      if (!count_nchains.count(nchains)) count_nchains[nchains] = 1;
      else count_nchains[nchains]++;
      w = std::max(w,count_nchains[nchains]);
      if (!count_natoms.count(natoms)) count_natoms[natoms] = 1;
      else count_natoms[natoms]++;
      w = std::max(w,count_natoms[natoms]);
      if (!count_tncs.count(ntncs)) count_tncs[ntncs] = 1;
      else count_tncs[ntncs]++;
      w = std::max(w,count_tncs[ntncs]);
    }
    w = std::log10(w)+1;
    output.push_back("Residues per segment");
    std::string line1 = "Residues: ";
    std::string line2 = "Number  : ";
    for (auto& item : count_nres)
    {
      line1 += itos(item.first,w,true,false) + " ";
      line2 += itos(item.second,w,true,false) + " ";
    }
    output.push_back(line1);
    output.push_back(line2);
    output.push_back("Chains per segment");
    line1 = "Chains  : ";
    line2 = "Number  : ";
    for (auto& item : count_nchains)
    {
      line1 += itos(item.first,w,true,false) + " ";
      line2 += itos(item.second,w,true,false) + " ";
    }
    output.push_back(line1);
    output.push_back(line2);
    output.push_back("Tncs related groups (models) per segment");
    line1 = "TncsGrps: ";
    line2 = "Number  : ";
    for (auto& item : count_tncs)
    {
      line1 += itos(item.first,w,true,false) + " ";
      line2 += itos(item.second,w,true,false) + " ";
    }
    output.push_back(line1);
    output.push_back(line2);
    output.push_back("Atoms per segment");
    line1 = "Atoms   : ";
    line2 = "Number  : ";
    for (auto& item : count_natoms)
    {
      line1 += itos(item.first,w,true,false) + " ";
      line2 += itos(item.second,w,true,false) + " ";
    }
    output.push_back(line1);
    output.push_back(line2);
    return output;
  }

  af_string
  Coordinates::logSegmentGraphic(bool occupancy_refinement) const
  {
    af_string output;
    output.push_back("Percent pruned " + dtos(percent_pruned(),6,2) + "%");
    std::string oheader("Occupancy binary  */-  [1/0]");
    std::string bheader("B-factor  log2(B) 0->9 [0<2 1<4 2<8 3<16 4<32 5<64 6<128 7<256 8<512 9<=999.99]");
    int nsegid = nsegments();
    int len(60),n(4),l(1);
    sv_bool loop = {true,false};
    if (occupancy_refinement) loop = {true};
    for (bool use_occupancy : loop)
    {
      output.push_back(use_occupancy ? oheader : bheader);
      std::string line("Segments " + itos(1,n,true,false) + "->" + itos(len,n,false,false) + " ");
      bool added(false);
      for (int segid = 1; segid <= nsegid; segid++)
      {
        std::set<std::string> nchain;
        std::set<int> tncs;
        double avB(0),avO(0),natoms(0);
        for (size_t a = 0; a < PDB.size(); a++)
        {
          auto& card = PDB[a];
          if (card.Segment == segid)
          {
            avB += card.B;
            avO += card.O;
            natoms++;
          }
        }
        avB = std::max(1.,avB/natoms); //std::log2(1) == 0
        std::string charO = avO > 0.5 ? "*" : "-";
        int Btag = std::floor(std::min(10.,std::log2(avB)-1));
        std::string charB = avO > 0.5 ? itos(Btag) : "-"; //0-9
        line += (use_occupancy) ? charO : charB;
        added = true;
        if (!((segid+1)%len)) //lists 10 long
        {
          output.push_back(line);
          l+=len;
          int lmax = l+len-1;
          lmax = std::min(lmax,static_cast<int>(nsegid));
          line = ("Segments " + itos(l,n,true,false) + "->" + itos(lmax,n,false,false) + " ");
          added = false;
        }
      }
      if (added) output.push_back(line);
    }
    return output;
  }

  int
  Coordinates::nresidues()
  {
    int count(0);
    for (int a = 0; a < PDB.size(); a++)
    {
      auto atom = PDB[a];
      if ((atom.O > 0.0) and
          (atom.AltLoc == " " or atom.AltLoc == "A") and //only count altloc once
           atom.AtomName != "UNK ") //already ignored
      {
        if (atom.is_calpha())
          count++;
      }
    }
    return count;
  }

  int
  Coordinates::nsegments() const
  {
    std::unordered_set<int> segid;
    for (int a = 0; a < PDB.size(); a++)
    {
      auto& atom = PDB[a];
      segid.insert(atom.Segment);
    }
    return segid.size();
  }

  void
  Coordinates::clear()
  {
    XRAY.clear();
    PDB.clear();
  }

  std::pair<double,double>
  Coordinates::bfactor_range() const
  {
    phaser_assert(PDB.size());
    std::pair<double,double> result =  { PDB[0].B,PDB[0].B };
    for (int a = 0; a < PDB.size(); a++)
    {
      result.first = std::min(PDB[a].B,result.first);
      result.second = std::max(PDB[a].B,result.second);
    }
    return result;
  }

  double
  Coordinates::percent_pruned() const
  {
    double percent(0);
    for (int a = 0; a < PDB.size(); a++)
      if (PDB[a].O == 0)
        percent += 100./PDB.size();
    return percent;
  }

  void
  Coordinates::add_lattice_translocation(std::vector<std::pair<dvect3,double>> disorder)
  {
    std::vector<PdbRecord>  newpdb;
    for (int a = 0; a < PDB.size(); a++)
    {
      //pick out the first altloc from the input coordinates
      //any second altloc present will be deleted
      if (PDB[a].AltLoc == " " or PDB[a].AltLoc == "A") //only take first altloc
      {
        auto Card = PDB[a];
        char ch = 'A';
        for (int ltd = 0; ltd < disorder.size(); ltd++)
        {
          dvect3 orth = UC.orthogonalization_matrix()*disorder[ltd].first;
          Card.AltLoc = chtos(ch++);
          Card.O = disorder[ltd].second;
          Card.X += orth;
          newpdb.push_back(Card);
        }
      }
    }
    PDB = newpdb;
  }


}//phasertng
