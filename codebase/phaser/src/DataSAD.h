//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_SAD_CLASS__
#define __PHASER_DATA_SAD_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/include/data_spots.h>
#include <phaser/include/data_outl.h>
#include <phaser/include/data_part.h>
#include <phaser/include/data_tncs.h>
#include <phaser/src/BinStats.h>
#include <phaser/src/HandEP.h>
#include <phaser/lib/scattering.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <cctbx/sgtbx/site_symmetry_table.h>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/dag/Entry.h>

namespace phaser {

class DataSAD : public UnitCell, public HandEP
{
  public:
    //Single value
    phasertng::CoordinatesPtr COORDINATES = nullptr;
    unsigned    NREFL;      // number of Reflections to iterate to
    data_spots  POS,NEG;

    // -- Data
    // -- Data dependent on reflection [r]
    af::shared<miller::index<int> >    miller;  // miller::index<int> Indices
    af_bool      bad_data;  // bad input data
    af_bool      cent,both,plus;
    af_cmplx     FPpos,FPneg;
    data_part    PARTIAL;

  public:
    DataSAD();
    af_string setDataSAD(SpaceGroup,
            UnitCell,
            af::shared<miller::index<int> >,
            data_spots, //POS
            data_spots, //NEG
            data_tncs, //tncs epsn
            data_bins,
            data_outl, //outliers
            data_part, //partial
            std::map<std::string,cctbx::eltbx::fp_fdp> //atomtype
    );
    ~DataSAD() {}

  protected:
    data_outl   OUTLIER;
    data_tncs   PTNCS;

    // -- Data dependent on reflection [r]
    float1D     ssqr; // Ssqr value for reflection
    float1D     epsn; // Epsilon value for reflection
    float1D     phsr; // Phase restriction
    unsigned1D  rbin; // bin number for reflection & derivative

    //Data dependent on bin [s]
    Bin         bin;
    float1D     SN_bin,OBSVAR_bin,DANOVAR_bin;

    //Data dependent on reflection and symmetry [r][isym]
    std::vector<std::vector<miller::index<int> > > rotMiller;
    float2D     traMiller;
    void        calcRotMiller();
    void        calcTraMiller();
    floatType   anisoTerm(unsigned&,dmat6&);

  public:
    floatType   maxSsqr();
    floatType   HiRes();
    floatType   LoRes();

    af_string calcAnomPartialStructure(std::map<std::string,cctbx::eltbx::fp_fdp>& ATOMTYPES);
};

} //phaser
#endif
