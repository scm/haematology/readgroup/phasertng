//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Moments_class__
#define __phasertng_Moments_class__
#include <phasertng/main/pointers.h>
#include <phasertng/io/Loggraph.h>
#include <phasertng/data/SigmaN.h>

typedef scitbx::af::shared<double> af_double;
typedef scitbx::af::shared<std::string> af_string;
typedef std::vector<bool> sv_bool;

namespace phasertng {

  struct MomentsData
  {
    double centric_mean;
    double acentric_mean;
    double acentric_sigma;
    double pvalue;
    double ptwin;
  };

  class Moments
  {
    private:
      int    NTHREADS = 1;
      bool   USE_STRICTLY_NTHREADS = false;

    public:
      const_ReflectionsPtr  REFLECTIONS;
      SigmaN                SIGMAN;
      bool NO_I = false;
      bool NO_SIGI = false;
      double hires;

    private: //members
      double meanE2_acent;
      double meanE2_cent;
      double nacentsel,ncentsel;
      bool has_centric,has_acentric;
      int max_s;
      af_double meanE2_acent_bin,meanE4_acent_bin,expectE4_acent_bin;
      af_double meanE2_cent_bin, meanE4_cent_bin, expectE4_cent_bin;
      af_double cumulative_acentric,cumulative_centric;
      af_double HiRes;
      af_double LoRes;
      double meanE4_acent,sigmaE4_acent;
      double meanE4_cent,sigmaE4_cent;
      double expectE4_acent;
      double expectE4_cent;

    private: //getters
      const int    nplot()  const   { return 51; }
      const double deltax() const   { return 0.04; }
      const double twinfrac() const { return 5./100.; }
      bool is_set()     { return meanE2_cent_bin.size(); }

    public:
      const double plimit() const   { return 0.01; }

    public: //constructors
      Moments(const_ReflectionsPtr);
      ~Moments() throw() {}
      void setup_moments(int,const sv_bool&);
      void setup_distributions(int,const sv_bool&);

    private:
     double alpha_from_meanE4(double);
     double expectE4a_from_alpha(double);

    public:
      af_string logMoments(std::string);
      Loggraph  logGraph1(std::string);
      Loggraph  logGraph2(std::string);
      double    twinperc() { return 100.*twinfrac(); }
      double    alpha();
      double    resolution() { return hires; }
      double    pvalue(int sigfig=-999); //negative for double precision
      double    ptwin5percent(int sigfig=-999); //negative for double precision
      double    ptwinnedless(int sigfig=-999); //negative for double precision
      double    second_moments_centric(int sigfig=-999);
      std::pair<double,double> second_moments_acentric(int sigfig=-999);
      std::pair<double,double> second_moments_expected(int sigfig=-999);
      double    acentric(int);
      double    centric(int);
      double    acentric_twinned(int);
      bool      twinned();
      bool      check_data_and_restrict_resolution(bool,const sv_bool&);
      std::pair<double,double> alpha_conf_int();
      double    epsn_sigman(int,int) const;
      MomentsData get_data();

    public:
      void set_threading(bool,int);
  };

} //phasertng
#endif
