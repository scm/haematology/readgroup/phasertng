//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/cctbx_project/cctbx/miller/asu.h>
#include <phasertng/data/Selected.h>
//#include <boost/sort/sort.hpp> //AJM BOOST 1.70

namespace phasertng {

  //Read_mtz_file
  void Phasertng::runDATA()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (boost::logic::indeterminate(REFLECTIONS->MAP))
      throw Error(err::INPUT,"Reflections not prepared correctly (map)");
    if (boost::logic::indeterminate(REFLECTIONS->INTENSITIES))
      throw Error(err::INPUT,"Reflections not prepared correctly (intensities)");
    if (boost::logic::indeterminate(REFLECTIONS->ANOMALOUS))
      throw Error(err::INPUT,"Reflections not prepared correctly (anomalous)");

    if (REFLECTIONS->NREFL == 0)
      throw Error(err::INPUT,"No Reflections");
    if (DAGDB.size() != 1)
      throw Error(err::INPUT,"Dag must contain only one node");

    std::map<friedel::type,std::string> npnstr =
          {{friedel::NAT,""}, {friedel::POS,"(+)"} ,{friedel::NEG,"(-)"}};

    bool read_anomalous = input.value__boolean.at(".phasertng.reflections.read_anomalous.").value_or_default();
    if (read_anomalous)
      logTab(out::LOGFILE,"Anomalous data will be generated");

    if (Suite::Extra())
    {
      Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
      auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
      logTabArray(out::LOGFILE,seltxt);
      seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
      logTabArray(out::LOGFILE,seltxt);
      selected.flag_outliers_and_select(REFLECTIONS.get(),
          { //no rejections yet, just log the data as input
          });
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Initial"));
    }

    {{
    //if not sorted then the ellg calculations will fail
    bool sort_reflections = true;
    logUnderLine(out::LOGFILE,"Sort");
    logTab(out::LOGFILE,"Number of Reflections: " + itos(REFLECTIONS->NREFL));
    if (sort_reflections)
    {
      logTab(out::LOGFILE,"Reflections sorted in resolution order");
      std::vector<std::string> label = { "Unsorted:", "Sorted:" }; //loop parameters
      for (int i = 0; i < label.size(); i++)
      {
        logTab(out::LOGFILE,label[i]);
        int nprint = 3;
        logTab(out::LOGFILE,"First and last " + itos(nprint) + " reflections");
        logTab(out::LOGFILE,snprintftos(
            "%-10s %5s %5s %5s %11s %11s",
             "#","H","K","L","d   ","d*^2  "));
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          if (r < nprint or r > REFLECTIONS->NREFL-1-nprint)
          {
            double resolution = REFLECTIONS->UC.cctbxUC.d(REFLECTIONS->get_miller(r));
            double d_star_sq = REFLECTIONS->UC.cctbxUC.d_star_sq(REFLECTIONS->get_miller(r));
            logTab(out::LOGFILE,snprintftos(
                "%-10d %5i %5i %5i %11.6f %11.6f",
                r+1,
                REFLECTIONS->get_miller(r)[0],
                REFLECTIONS->get_miller(r)[1],
                REFLECTIONS->get_miller(r)[2],
                resolution,
                d_star_sq));
           }
           if (r == nprint) logTab(out::LOGFILE,"---");
        }
        if (i == 0) REFLECTIONS->sort_in_resolution_order();
      }
    }
    else
    {
      logTab(out::LOGFILE,"Reflections NOT sorted in resolution order");
    }
    }}

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Data Preparation");
    //immediately copy the fmap to the f column if it was read from the mtz file
    //this must be done before the selection test below
    logTab(where,"Set and check anomalous centric amplitudes or intensities");
    if (REFLECTIONS->has_col(labin::FPOS) and REFLECTIONS->has_col(labin::FNEG))
    {
      REFLECTIONS->set_and_check_anomalous_centric_amplitudes();
    }
    else if (REFLECTIONS->has_col(labin::IPOS) and REFLECTIONS->has_col(labin::INEG))
    {
      REFLECTIONS->set_and_check_anomalous_centric_intensities();
    }
    //tests throw if they encounter these problems
    //runMTZ does some preliminary processing to remove these up front
    //can rescue data somewhat
    for (auto f : { friedel::NAT,friedel::POS,friedel::NEG } )
    {
      labin::col coli = REFLECTIONS->convert_friedel_to_labin("I",f);
      labin::col colf = REFLECTIONS->convert_friedel_to_labin("F",f);
      labin::col colsigf = REFLECTIONS->convert_friedel_to_labin("SIGF",f);
      if (REFLECTIONS->has_col(coli) and !REFLECTIONS->MAP)
      {
        logTab(where,"Check for negative or zero sigi " + friedel::type2String(f));
        REFLECTIONS->check_for_negative_or_zero_sigi(f);
      }
      else if (REFLECTIONS->has_col(colf) and !REFLECTIONS->has_col(colsigf))
      { //create the col and fill with zeros
        logTab(where,"set sigf to xtriage approximation if not present " + friedel::type2String(f));
        std::string ext = stolower(friedel::type2String(f));
        std::string key_sigf = ".phasertng.labin.sigf" + ext + ".";
        std::string label = "SIGF" + npnstr[f] + "<<0";
        input.value__mtzcol.at(key_sigf).set_value(label);
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigf).value_or_default());
        REFLECTIONS->set_sigf_to_xtriage_value_if_not_present(f);
       // throw Error(err::INPUT,"Data has no sigmas");
        logWarning("SigF not present and set to resolution dependent approximation (see xtriage)");
      }
      else if (REFLECTIONS->has_col(colf) and REFLECTIONS->has_col(colsigf))
      {
        logTab(where,"Check for negative f or sigf " + friedel::type2String(f));
        REFLECTIONS->check_for_negative_f_or_sigf(f);
      }
    }

    logTab(where,"Check anomalous differences");
    if (REFLECTIONS->ANOMALOUS)
    {
      //it is legal to have non-anomalous anomalous data
      //check locally if anomalous signal is required
      REFLECTIONS->check_native() ?
        logWarning("Anomalous data has no anomalous differences"):
        logAdvisory("Anomalous data has anomalous differences");
    }

    if (REFLECTIONS->has_col(labin::MAPF) and !REFLECTIONS->MAP)
    {
      //again legal, but must be properly flagged
      throw Error(err::INPUT,"Data not identified as map data: check flags");
    }

    logTab(where,"Uniqueify");
    cctbx::sgtbx::space_group_type cctbxSG = REFLECTIONS->SG.type();
    bool anomalous_flag(false);
    auto miller = REFLECTIONS->get_miller_array();
    if (!cctbx::miller::is_unique_set_under_symmetry(cctbxSG,anomalous_flag,miller.const_ref()))
    {
      auto txt = REFLECTIONS->unique_under_symmetry_selection();
      logTabArray(where,txt);
      logWarning("Reflections are not a unique set by symmetry");
      REFLCOLSMAP.reduce_to_space_group(REFLECTIONS->SG);
      //REFLECTIONS and REFLCOLMAP point to same object
      phaser_assert(REFLCOLSMAP.NREFL == REFLECTIONS->NREFL);
      miller = REFLECTIONS->get_miller_array();
      if (!cctbx::miller::is_unique_set_under_symmetry(cctbxSG,anomalous_flag,miller.const_ref()))
      {
        throw Error(err::INPUT,"Reflections are not a unique set by symmetry");
      }
    }
    else
      logTab(where,"Reflections are a unique set by symmetry");
    bool map_miller_to_asu(false);
    if (map_miller_to_asu)
    {
      logTab(where,"Reflections moved to reciprocal asymmetric unit");
      REFLECTIONS->move_to_reciprocal_asu();
    }
    }}

    logUnderLine(out::LOGFILE,"StarAniso Test");
    af_string txt; bool warning; double dmin;
    std::tie(txt,warning,dmin) = REFLECTIONS->staraniso();
    logTabArray(out::LOGFILE,txt);
    logTab(out::LOGFILE,"Reflections resolution = " + dtos(REFLECTIONS->data_hires()));
    logTab(out::LOGFILE,"Reflections are isotropic to resolution = " + dtos(dmin));
    if (warning)
      logWarning("Reflection completeness is anisotropic (staraniso?)");

    {{
    //generate IfromF and FfromI for the entered data (anomalous/native)
    REFLECTIONS->FRENCH_WILSON = boost::logic::indeterminate; //required for consistency check
    if (!REFLECTIONS->MAP)
    {
      //Set French-Wilson defaults
      // Reconstruct I and SIGI by reversing transformations
      out::stream where = out::LOGFILE;
      if (!REFLECTIONS->INTENSITIES)
        logUnderLine(where,"French and Wilson Analysis");
      else
      {
        logTab(out::VERBOSE,"Intensities: no French and Wilson analysis");
        REFLECTIONS->FRENCH_WILSON = false;
      }
      for (auto f : { friedel::NAT,friedel::POS,friedel::NEG} )
      {
        labin::col coli = REFLECTIONS->convert_friedel_to_labin("I",f);
        labin::col colf = REFLECTIONS->convert_friedel_to_labin("F",f);
        if (REFLECTIONS->has_col(colf))
        {
          phaser_assert(!REFLECTIONS->has_col(coli));
          REFLECTIONS->set_french_wilson_flags(f);
          //throws if there is an inconsistency, checking against value already set for other friedels
          bool french_wilson = bool(REFLECTIONS->FRENCH_WILSON);
          // Reconstruct I and SIGI by reversing simple transformation from
          // I/SIGI to F/SIGF used in truncate.f and f_sq_as_f_xtal_3_7.
          // The only loss of information is for reflections where the original I
          // was negative.
          // This is not appropriate when F/SIGF have been through French&Wilson
          // procedure, so catch elsewhere when relevant
          // For French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2
          std::string ext = stolower(friedel::type2String(f));
          std::string key_i = ".phasertng.labin.i" + ext + ".";
          std::string key_sigi = ".phasertng.labin.sigi" + ext + ".";
          std::string label = french_wilson ?
                                "I" + npnstr[f] + "<<FW" :
                                "I" + npnstr[f] + "<<FSQ" + npnstr[f];
          input.value__mtzcol.at(key_i).set_value(label);
          input.value__mtzcol.at(key_sigi).set_value("SIG" + label);
          REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_i).value_or_default());
          REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigi).value_or_default());
          if (french_wilson)
          {
            logTab(where,"Reverse French Wilson amplitudes for intensities " + friedel::type2String(f));
            REFLECTIONS->reverse_french_wilson_IfromF(f);
          }
          else
          {
            logTab(where,"Reverse not French Wilson amplitudes for intensities " + friedel::type2String(f));
            REFLECTIONS->reverse_not_french_wilson_IfromF(f);
          }
        }
        else if (REFLECTIONS->has_col(coli))
        {
          REFLECTIONS->FRENCH_WILSON = false;
          phaser_assert(!REFLECTIONS->has_col(colf));
          std::string ext = stolower(friedel::type2String(f));
          std::string key_f = ".phasertng.labin.f" + ext + ".";
          std::string key_sigf = ".phasertng.labin.sigf" + ext + ".";
          std::string labelF = "F" + npnstr[f] + "<<I" + npnstr[f] ;
          std::string labelS = "SIGF" + npnstr[f] + "<<SIGI" + npnstr[f] ;
          input.value__mtzcol.at(key_f).set_value(labelF);
          input.value__mtzcol.at(key_sigf).set_value(labelS);
          REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_f).value_or_default());
          REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigf).value_or_default());
          REFLECTIONS->set_FfromI(f); //should not be used, patterson anomdiff only?
          logAdvisory("Amplitudes generated from intensities for " + ext);
        }
      }
    }
    else
    {
      out::stream where = out::LOGFILE;
      REFLECTIONS->FRENCH_WILSON = false;
      logTab(where,"Set mean amplitudes from map phased amplitudes");
      //Setup mean from anomalous amplitudes
      std::string key_f = ".phasertng.labin.fnat.";
      std::string key_sigf = ".phasertng.labin.sigfnat.";
      std::string key_i = ".phasertng.labin.inat.";
      std::string key_sigi = ".phasertng.labin.siginat.";
      input.value__mtzcol.at(key_f).set_value("F<<MAP");
      input.value__mtzcol.at(key_sigf).set_value("SIGF<<MAP");
      input.value__mtzcol.at(key_i).set_value("I<<MAP");
      input.value__mtzcol.at(key_sigi).set_value("SIGI<<MAP");
      REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_f).value_or_default());
      REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigf).value_or_default());
      REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_i).value_or_default());
      REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigi).value_or_default());
      REFLECTIONS->set_mean_amplitudes_from_map_amplitudes();
      REFLECTIONS->reverse_not_french_wilson_IfromF(friedel::NAT);
      //sigmas are zero anyway
      logBlank(where);
    }
    }}

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Data Analysis Summary");
    input.value__boolean.at(".phasertng.data.intensities.").set_value(bool(REFLECTIONS->INTENSITIES));
    input.value__boolean.at(".phasertng.data.french_wilson.").set_value(bool(REFLECTIONS->FRENCH_WILSON));
    input.value__boolean.at(".phasertng.data.anomalous.").set_value(bool(REFLECTIONS->ANOMALOUS));
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->MAP));
    if (REFLECTIONS->MAP)
    {
      logTab(where,"Data provided as a map");
      logWarning("Non-anomalous amplitudes from map phased amplitudes");
    }
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->ANOMALOUS));
    (REFLECTIONS->ANOMALOUS) ?
      logTab(where,"Anomalous data have been provided") :
      logTab(where,"Non-anomalous data have been provided");
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->INTENSITIES));
    (REFLECTIONS->INTENSITIES) ?
      logTab(where,"Data provided as intensities") :
      logTab(where,"Data provided as amplitudes");
    if (!REFLECTIONS->INTENSITIES)
      logWarning("Data provided as amplitudes; provide intensities for best performance");
    phaser_assert(!boost::logic::indeterminate(REFLECTIONS->FRENCH_WILSON));
    (REFLECTIONS->FRENCH_WILSON) ?
      logTab(where,"Data provided as French-Wilson amplitudes"):
      logTab(where,"Data provided not as French-Wilson amplitudes");
    input.value__boolean.at(".phasertng.data.map.").set_value(bool(REFLECTIONS->MAP));
    }}

    //Set mean from anomalous f and/or i
    //This is a crappy conversion, only used for reflections and tncs
    {{
    if (REFLECTIONS->ANOMALOUS)
    {
      if (REFLECTIONS->has_col(labin::FPOS) and REFLECTIONS->has_col(labin::FNEG))
      {
        phaser_assert(!REFLECTIONS->has_col(labin::FNAT));
        phaser_assert(!REFLECTIONS->has_col(labin::SIGFNAT));
        phaser_assert( REFLECTIONS->has_col(labin::SIGFPOS));
        phaser_assert( REFLECTIONS->has_col(labin::SIGFNEG));
        logWarning("Non-anomalous amplitudes from anomalous amplitudes");
        std::string key_f = ".phasertng.labin.fnat.";
        std::string key_sigf = ".phasertng.labin.sigfnat.";
        input.value__mtzcol.at(key_f).set_value("F<<F(+)(-)");
        input.value__mtzcol.at(key_sigf).set_value("SIGF<<SIGF(+)(-)");
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_f).value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigf).value_or_default());
        REFLECTIONS->set_mean_amplitudes_from_anomalous();
      }
      if (REFLECTIONS->has_col(labin::IPOS) and REFLECTIONS->has_col(labin::INEG))
      {
        phaser_assert(!REFLECTIONS->has_col(labin::INAT));
        phaser_assert(!REFLECTIONS->has_col(labin::SIGINAT));
        phaser_assert( REFLECTIONS->has_col(labin::SIGIPOS));
        phaser_assert( REFLECTIONS->has_col(labin::SIGINEG));
        logAdvisory("Non-anomalous intensities from anomalous intensities");
        std::string key_i = ".phasertng.labin.inat.";
        std::string key_sigi = ".phasertng.labin.siginat.";
        input.value__mtzcol.at(key_i).set_value("I<<I(+)(-)");
        input.value__mtzcol.at(key_sigi).set_value("SIGI<<SIGI(+)(-)");
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_i).value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(key_sigi).value_or_default());
        REFLECTIONS->set_mean_intensities_from_anomalous();
      }
    }
    else
    {
      if (read_anomalous and REFLECTIONS->has_col(labin::INAT))
      {
        REFLECTIONS->ANOMALOUS = true; //even though there is no anomalous signal
        phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
        logAdvisory("Non-anomalous intensity was also assigned to anomalous intensities");
        input.value__mtzcol.at(".phasertng.labin.ipos.").set_value("I(+)<<I");
        input.value__mtzcol.at(".phasertng.labin.sigipos.").set_value("SIGI(+)<<SIGI");
        input.value__mtzcol.at(".phasertng.labin.ineg.").set_value("I(-)<<I");
        input.value__mtzcol.at(".phasertng.labin.sigineg.").set_value("SIGI(-)<<SIGI");
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.ipos.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.ineg.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sigipos.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sigineg.").value_or_default());
        REFLECTIONS->set_anomalous_intensities_from_mean();
      }
      if (read_anomalous and REFLECTIONS->has_col(labin::FNAT))
      {
        REFLECTIONS->ANOMALOUS = true; //even though there is no anomalous signal
        phaser_assert(REFLECTIONS->has_col(labin::SIGFNAT));
        logAdvisory("Non-anomalous amplitude was also assigned to anomalous amplitudes");
        input.value__mtzcol.at(".phasertng.labin.fpos.").set_value("F(+)<<F");
        input.value__mtzcol.at(".phasertng.labin.sigfpos.").set_value("SIGF(+)<<SIGF");
        input.value__mtzcol.at(".phasertng.labin.fneg.").set_value("F(-)<<F");
        input.value__mtzcol.at(".phasertng.labin.sigfneg.").set_value("SIGF(-)<<SIGF");
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.fpos.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.fneg.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sigfpos.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sigfneg.").value_or_default());
        REFLECTIONS->set_anomalous_amplitudes_from_mean();
      }
    }
    }}

    af_string warntxt;
    std::tie(txt,warntxt) = REFLECTIONS->logBadData("Summary");
    logTabArray(out::LOGFILE,txt);
    for (auto item : warntxt) logWarning(item);
    if (Suite::Extra())
    {
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Final"));
    }

    if (read_anomalous)
    {
      out::stream where = out::VERBOSE;
      logTab(where,"Add cols for extra data");
      logTab(where,"Add both/plus");
      REFLECTIONS->setup_both_plus(
          input.value__mtzcol.at(".phasertng.labin.both.").value_or_default(),
          input.value__mtzcol.at(".phasertng.labin.plus.").value_or_default());
    }

    {{
    out::stream where = out::VERBOSE;
    logTab(where,"Add teps and tbin"); //although these could be added with aniso or later as not yet needed
    REFLECTIONS->setup_teps(
        input.value__mtzcol.at(".phasertng.labin.teps.").value_or_default(),
        input.value__mtzcol.at(".phasertng.labin.tbin.").value_or_default());
    logTab(where,"Add cent");
    REFLECTIONS->setup_cent(
        input.value__mtzcol.at(".phasertng.labin.cent.").value_or_default());
    logTab(where,"Add ssqr");
    REFLECTIONS->setup_ssqr(
        input.value__mtzcol.at(".phasertng.labin.ssqr.").value_or_default());
    logTab(where,"Add eps");
    REFLECTIONS->setup_eps(
        input.value__mtzcol.at(".phasertng.labin.eps.").value_or_default());
    logTab(where,"Add phsr");
    REFLECTIONS->setup_phsr(
        input.value__mtzcol.at(".phasertng.labin.phsr.").value_or_default());
    logTab(where,"Add bins");
    Bin bin = REFLECTIONS->setup_bin(
        input.value__mtzcol.at(".phasertng.labin.bin.").value_or_default(),
        input.value__int_paired.at(".phasertng.bins.reflections.range.").value_or_default_min(),
        input.value__int_paired.at(".phasertng.bins.reflections.range.").value_or_default_max(),
        input.value__int_number.at(".phasertng.bins.reflections.width.").value_or_default()
        );
    logTabArray(where,bin.logBin("Data"));
    logTab(where,"Add free r flags");
    REFLECTIONS->generate_free_r_flags(
        input.value__mtzcol.at(".phasertng.labin.free.").value_or_default());
    }}

    SigmaN csn;
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Normalization");
    logTab(where,"Normalization without Anisotropy correction or tNCS Correction");
    csn = REFLECTIONS->setup_resn_anisobeta(
        input.value__mtzcol.at(".phasertng.labin.resn.").value_or_default(),
        input.value__mtzcol.at(".phasertng.labin.anisobeta.").value_or_default());
    logTab(where,"Isotropic B-factor (amplitudes):" + dtos(csn.IsoB_I()/2));
    //does not have total scattering defined yet, absolute scale not known
    }}

    if (Suite::Extra())
    {
      logTabArray(out::TESTING,csn.logAnisotropy("Normalized"));
      logTabArray(out::TESTING,csn.logScaling("Normalized"));
      logTabArray(out::TESTING,csn.logBinning("Normalized",REFLECTIONS->numinbin()));
    }
    logTabArray(out::VERBOSE,REFLECTIONS->logReflections("Normalized"));

    {{
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    //logChevron(out::LOGFILE,"Data Information");
    //logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_outliers_and_select(REFLECTIONS.get(),
        { rejected::RESO,
          rejected::MISSING,
          rejected::SYSABS,
          rejected::WILSONNAT,
          rejected::WILSONPOS,
          rejected::WILSONNEG,
          rejected::NOMEAN,
        });
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Prepared Data"));
    if (!selected.nsel())
      throw Error(err::INPUT,"No reflection data present in file");
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::DATA);
    //Phasertng::calculate_dobs_feff();
    }}

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
