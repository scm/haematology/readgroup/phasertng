//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_includes__
#define __phasertng_includes__
#include <vector>
#include <string>
#include <complex>
#include <cctype>    //toupper etc in Windows
#include <utility>   //std::pair
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/sym_mat3.h>
#include <scitbx/array_family/tiny_types.h>
#include <scitbx/array_family/shared.h>
#include <cctbx/miller.h>
#include <iso646.h>

namespace phasertng { namespace af = scitbx::af; }
namespace phasertng { namespace fn = scitbx::fn; }
namespace phasertng { namespace miller = cctbx::miller; }

namespace phasertng {

typedef cctbx::miller::index<int> millnx;
typedef std::complex<double> cmplex;

typedef scitbx::vec3<double>                dvect3;
typedef scitbx::mat3<double>                dmat33;
typedef scitbx::sym_mat3<double>            dmat6;
typedef scitbx::sym_mat3<int>               imat6;
typedef scitbx::vec3<int>                   ivect3;
typedef scitbx::vec3<cmplex>                cvect3;
typedef scitbx::mat3<cmplex>                cmat33;
typedef scitbx::mat3<int>                   imat33;

typedef std::vector<millnx>                 sv_millnx;
typedef std::vector<std::string>            sv_string;
typedef std::vector<double>                 sv_double;
typedef std::vector<cmplex>                 sv_cmplex;
typedef std::vector<dvect3>                 sv_dvect3;
typedef std::vector<cvect3>                 sv_cvect3;
typedef std::vector<ivect3>                 sv_ivect3;
typedef std::vector<dmat33>                 sv_dmat33;
typedef std::vector<cmat33>                 sv_cmat33;
typedef std::vector<bool>                   sv_bool;
typedef std::vector<int>                    sv_int;

typedef af::shared<millnx>                  af_millnx;
typedef af::shared<std::string>             af_string;
typedef af::shared<double>                  af_double;
typedef af::shared<cmplex>                  af_cmplex;
typedef af::shared<dvect3>                  af_dvect3;
typedef af::shared<dmat33>                  af_dmat33;
typedef af::shared<bool>                    af_bool;
typedef af::shared<int>                     af_int;
typedef af::shared<char>                    af_char;

} //phasertng

#endif
