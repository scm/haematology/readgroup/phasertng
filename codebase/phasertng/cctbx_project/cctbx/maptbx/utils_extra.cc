#include <phasertng/cctbx_project/cctbx/maptbx/utils_extra.h>

namespace cctbx { namespace maptbx {

/* Follows maskdemo.cpp from clipper revision 153 */

void
map_to_wang_mask(
  af::versa<double, af::c_grid<3> >& map_data,
  cctbx::uctbx::unit_cell& unit_cell,
  double solvent_fraction,
  double wang_radius,
  double rho_weight,
  double sig_weight
  )
{
  af::versa<double, af::c_grid<3> > lmean = map_data;
  {//memory for additiona map
  //make squared map
  af::versa<double, af::c_grid<3> > map_data_sqr=map_data.deep_copy();
  for (int i = 0; i < map_data.size(); i++)
    map_data_sqr[i] *= map_data[i];

  af::versa<double, af::c_grid<3> > lmean_sqr = map_data_sqr;

  //now calculate local mean, local mean squared
  int nx = map_data.accessor()[0];
  int ny = map_data.accessor()[1];
  int nz = map_data.accessor()[2];
  double xrad = wang_radius*unit_cell.reciprocal_parameters()[0]*nx;
  double yrad = wang_radius*unit_cell.reciprocal_parameters()[1]*ny;
  double zrad = wang_radius*unit_cell.reciprocal_parameters()[2]*nz;
  for (int lx = 0; lx < nx; lx++) {
    for (int ly = 0; ly < ny; ly++) {
      for (int lz = 0; lz < nz; lz++) {
        double rho = 0.0;
        double rho_sqr = 0.0;
        int counter = 0;
        int x1box=scitbx::math::nearest_integer(static_cast<double>(lx)-xrad);
        int x2box=scitbx::math::nearest_integer(static_cast<double>(lx)+xrad);
        int y1box=scitbx::math::nearest_integer(static_cast<double>(ly)-yrad);
        int y2box=scitbx::math::nearest_integer(static_cast<double>(ly)+yrad);
        int z1box=scitbx::math::nearest_integer(static_cast<double>(lz)-zrad);
        int z2box=scitbx::math::nearest_integer(static_cast<double>(lz)+zrad);
        for (int kx = x1box; kx <= x2box; kx++) {
          for (int ky = y1box; ky <= y2box; ky++) {
            for (int kz = z1box; kz <= z2box; kz++) {
              int mx = scitbx::math::mod_positive(kx, nx);
              int my = scitbx::math::mod_positive(ky, ny);
              int mz = scitbx::math::mod_positive(kz, nz);
              rho += map_data(mx,my,mz);
              rho_sqr += map_data_sqr(mx,my,mz);
              counter += 1;
        }}}
        lmean(lx,ly,lz) = rho / counter;
        lmean_sqr(lx,ly,lz) = rho_sqr / counter;
  }}}

  af::versa<double, af::c_grid<3> > lsigm = lmean_sqr;
  //calculate std deviation
  for (int i = 0; i < map_data.size(); i++)
    lsigm[i] = std::sqrt(lmean_sqr[i] - lmean[i]*lmean[i]);

  //now calculate function to mask (reuse lmean as mask)
  for (int i = 0; i < map_data.size(); i++)
     lmean[i] = rho_weight*lmean[i] + sig_weight * lsigm[i];
  }

  //use a map sort to get the cutoff level
  std::vector<double> sort_lmean(map_data.size());
  for (int i = 0; i < map_data.size(); i++)
    sort_lmean[i] = map_data[i];
  std::sort(sort_lmean.begin(),sort_lmean.end());

  int i_cutoff(solvent_fraction*double(map_data.size()));
  double ed_cutoff = sort_lmean[i_cutoff];

  //make a mask
  for (int i = 0; i < map_data.size(); i++)
     map_data[i] = (lmean[i] > ed_cutoff) ? 1 : 0;
}

void
map_to_wang_cutout(
  af::versa<double, af::c_grid<3> >& map_data,
  cctbx::uctbx::unit_cell& unit_cell,
  double solvent_fraction,
  double wang_radius,
  double rho_weight,
  double sig_weight
  )
{
  af::versa<double, af::c_grid<3> > lmean = map_data;
  {//memory for additiona map
  //make squared map
  af::versa<double, af::c_grid<3> > map_data_sqr=map_data.deep_copy();
  for (int i = 0; i < map_data.size(); i++)
    map_data_sqr[i] *= map_data[i];

  af::versa<double, af::c_grid<3> > lmean_sqr = map_data_sqr;

  //now calculate local mean, local mean squared
  int nx = map_data.accessor()[0];
  int ny = map_data.accessor()[1];
  int nz = map_data.accessor()[2];
  double xrad = wang_radius*unit_cell.reciprocal_parameters()[0]*nx;
  double yrad = wang_radius*unit_cell.reciprocal_parameters()[1]*ny;
  double zrad = wang_radius*unit_cell.reciprocal_parameters()[2]*nz;
  for (int lx = 0; lx < nx; lx++) {
    for (int ly = 0; ly < ny; ly++) {
      for (int lz = 0; lz < nz; lz++) {
        double rho = 0.0;
        double rho_sqr = 0.0;
        int counter = 0;
        int x1box=scitbx::math::nearest_integer(static_cast<double>(lx)-xrad);
        int x2box=scitbx::math::nearest_integer(static_cast<double>(lx)+xrad);
        int y1box=scitbx::math::nearest_integer(static_cast<double>(ly)-yrad);
        int y2box=scitbx::math::nearest_integer(static_cast<double>(ly)+yrad);
        int z1box=scitbx::math::nearest_integer(static_cast<double>(lz)-zrad);
        int z2box=scitbx::math::nearest_integer(static_cast<double>(lz)+zrad);
        for (int kx = x1box; kx <= x2box; kx++) {
          for (int ky = y1box; ky <= y2box; ky++) {
            for (int kz = z1box; kz <= z2box; kz++) {
              int mx = scitbx::math::mod_positive(kx, nx);
              int my = scitbx::math::mod_positive(ky, ny);
              int mz = scitbx::math::mod_positive(kz, nz);
              rho += map_data(mx,my,mz);
              rho_sqr += map_data_sqr(mx,my,mz);
              counter += 1;
        }}}
        lmean(lx,ly,lz) = rho / counter;
        lmean_sqr(lx,ly,lz) = rho_sqr / counter;
  }}}

  af::versa<double, af::c_grid<3> > lsigm = lmean_sqr;
  //calculate std deviation
  for (int i = 0; i < map_data.size(); i++)
    lsigm[i] = std::sqrt(lmean_sqr[i] - lmean[i]*lmean[i]);

  //now calculate function to mask (reuse lmean as mask)
  for (int i = 0; i < map_data.size(); i++)
     lmean[i] = rho_weight*lmean[i] + sig_weight * lsigm[i];
  }

  //use a map sort to get the cutoff level
  std::vector<double> sort_lmean(map_data.size());
  for (int i = 0; i < map_data.size(); i++)
    sort_lmean[i] = map_data[i];
  std::sort(sort_lmean.begin(),sort_lmean.end());

  int i_cutoff(solvent_fraction*double(map_data.size()));
  double ed_cutoff = sort_lmean[i_cutoff];

  //make a mask
  for (int i = 0; i < map_data.size(); i++)
     map_data[i] = (lmean[i] > ed_cutoff) ? lmean[i] : 0;
}

}} // namespace cctbx::maptbx
