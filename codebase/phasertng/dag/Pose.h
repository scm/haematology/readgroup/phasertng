//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Pose_class__
#define __phasertng_dag_Pose_class__
#include <phasertng/main/Identifier.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/dag/xyzRotMatDeg.h>
#include <phasertng/math/rotation/rotdist.h>
#include <phasertng/dag/data_clash.h>
#include <scitbx/math/euler_angles.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Pose : public xyzRotMatDeg
  { //placed components, tncs resolved
    public:
      Pose() {}
      ~Pose() {}

    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      dvect3      EULER = {0,0,0}; //wrt molecular transform
      dvect3      FRACT = {0,0,0}; //wrt molecular transform
      dvect3      ORTHR = {0,0,0}; // xyz rotation perturbations wrt mt
      dvect3      ORTHT = {0,0,0}; // xyz translation perturbations wrt mt
      double      BFAC = 0;
      int         MULT = 1;
      std::pair<int,int>  TNCS_GROUP = {1,1}; //tncs group number and the number of the pose in the group
      dag::data_clash  CLASH;
      dmat33      SHIFT_MATRIX = dmat33(); //wrt original
      dvect3      SHIFT_ORTHT = {0,0,0}; //wrt original
      double      AELLG = 0;
      double      TFZ = 0;

    public: //not parsed/unparsed, just for working memory
      double      FS = 0;
      double      DRMS_SHIFT = 0;
      double      generic_float = 0;

    //return the final rotation and fractional translation, given the unit cell info
    std::pair<dmat33,dvect3>
    finalRT(const dmat33& orthmat,const dmat33& fracmat) const
    {
      dmat33 ROT = scitbx::math::euler_angles::zyz_matrix(EULER[0],EULER[1],EULER[2]);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = perturbed(ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dvect3 iOrthT = orthmat * FRACT;
      iOrthT += ORTHT;
      dvect3 TRA = fracmat*iOrthT;
      return { ROTperturb, TRA };
    }

    //return the size of the translation perturbation
    double
    delta_shift() const
    { return std::sqrt(fn::pow2(ORTHT[0]) + fn::pow2(ORTHT[1]) + fn::pow2(ORTHT[2])); }

    double
    delta_angle() const
    {
      dmat33 Rmat = perturbed(ORTHR);
      double ang;
      std::tie(std::ignore,ang) = axis_and_angle_deg(Rmat,0);
      return std::fabs(ang);
    }

  };

}} //phasertng
#endif
