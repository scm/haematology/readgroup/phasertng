# LIBTBX_SET_DISPATCHER_NAME phasertng.riker

from iotbx.cli_parser import run_program
from phasertng.programs import riker

if (__name__ == '__main__'):
  try:
    run_program(riker.Program)
  except Exception as e:
    print(str(e))
