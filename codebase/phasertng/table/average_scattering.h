#ifndef __phasertng_table_average_scattering_class__
#define __phasertng_table_average_scattering_class__
#include <cctbx/eltbx/tiny_pse.h>
#include <phasertng/enum/composition_type.h>
#include <phasertng/pod/composition.h>
#include <unordered_map>

namespace phasertng {
namespace table {

class average_scattering
{
  public:
    double scattering;
    composition_t composition;
    int nres;

    average_scattering(double totalmw,composition::type ctype)
    {
      std::unordered_map<std::string,double> element_percentages;
      double molecular_weight_per_residue;
      if (ctype == composition::PROTEIN)
      {
    // protein is made up of 54.76% C 15.68% N 21.22% O 1.47% S and 6.97% H by weight
        element_percentages = std::unordered_map<std::string,double>({
          {"C",0.5476},
          {"N",0.1568},
          {"O",0.2211},
          {"S",0.0147},
          {"P",0},
          {"H",0.0697}
          });
        molecular_weight_per_residue = 111;
      }
      else if (ctype == composition::NUCLEIC)
      {
  // dna is made up of 40.50% C 24.31% N 26.00% O 7.19% P and 1.99% H by weight
        element_percentages = std::unordered_map<std::string,double>({
          {"C",0.4050},
          {"N",0.2431},
          {"O",0.2600},
          {"S",0},
          {"P",0.0719},
          {"H",0.0199}
          });
        molecular_weight_per_residue = 330;
      }
      else
      {}

      scattering = 0;
      composition.clear();
      for (auto item : element_percentages)
      {
        cctbx::eltbx::tiny_pse::table atom(item.first);
        double weight = atom.weight();
        double atomic_number = atom.atomic_number();
        double elementmw = item.second*totalmw;
        double natoms = elementmw/weight;
        scattering += natoms*(atomic_number*atomic_number);
        composition[item.first] = std::floor(natoms);
      }
      nres = std::floor(totalmw/molecular_weight_per_residue);
    }
};

}}
#endif
