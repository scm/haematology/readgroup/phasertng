#ifndef __phasertng_HKL_clustered_class__
#define __phasertng_HKL_clustered_class__
#include <phasertng/main/includes.h>

namespace phasertng {

  class p3Dc
  {
    public:
      double x,y,z;
      double r = 0;
      double theta = 0;
      double phi = 0;
      millnx rhkl= {0,0,0};
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
      bool flipped = false;
#endif
      double intensity = 0;
    public:
      const dvect3 rtp() const { return {r,theta,phi}; }
      const dvect3 xyz() const { return {x,y,z}; }
      void toPolar();
      void toCart();

      bool operator<(const p3Dc &right)  const
      { //sort on phi
        return (right.phi < phi);
      };
  };

  class HKL_clustered
  {
    public:
      std::vector<std::vector<p3Dc> > clustered;
      HKL_clustered() { clustered.clear(); }

      void add(p3Dc& this_hkl)
      {
        double COSTHETA_LIMIT(10.0e-04);
        int c = 0;
        for (; c < clustered.size(); c++)
          if (fabs(std::cos(clustered[c][0].theta) - std::cos(this_hkl.theta)) <  COSTHETA_LIMIT)
          {
            clustered[c].push_back(this_hkl);
            return;
          }
        clustered.push_back(std::vector<p3Dc>(0));
        clustered[c].push_back(this_hkl);
      }

      int size() const
      {
        int count = 0;
        for (int c = 0; c < clustered.size(); c++)
          count += clustered[c].size();
        return count;
      }

  };

}//end namespace phaser

#endif
