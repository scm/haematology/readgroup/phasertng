#ifndef __phasertng_sg_prior_class__
#define __phasertng_sg_prior_class__
#include <cctbx/sgtbx/space_group_type.h>
#include <map>

namespace phasertng {

//helper for sort
class prob_sg
{
  public:
  prob_sg() { }
  prob_sg(double r,std::string m):PROB(r),SG(m) { }
  double PROB;
  std::string    SG;
  bool operator<(const prob_sg &right)  const
  {return (PROB < right.PROB);}
};

class sg_prior
{
  std::map<std::string,double> prior;

  sg_prior()
  {
    prior = {
    //Triclinic C1
       {"P1" , 115 },
    //Monoclinic C2
       {"P2" , 3 },
       {"P21" , 128 },
       {"C2" , 75 },
    //Orthorhombic D2
       {"P222" , 1 },
       {"P2221" , 2 },  {"P2122" , 2 },  {"P2212" , 2 },
       {"P21212" , 45 },  {"P21221" , 45  },  {"P22121" , 45 },
       {"P212121" , 212 },
       {"C2221" , 39 },
       {"C222" , 3 },
       {"F222" , 1 },
       {"I222" , 17 },
       {"I212121" , 4 },
    //Tetragonal C4
       {"P4" , 1 },
       {"P41" , 7 },
       {"P42" , 1 },
       {"P43" , 6 },
       {"I4" , 4 },
       {"I41" , 1 },
    //Tetragonal D4
       {"P422" , 1 },
       {"P4212" , 4 },
       {"P4122" , 6 },
       {"P41212" , 31 },
       {"P4222" , 1 },
       {"P42212" , 8 },
       {"P4322" , 5 },
       {"P43212" , 45 },
       {"I422" , 6 },
       {"I4122" , 4 },
    //Trigonal C3 (rhombohedral)
       {"R3" , 11 },
    //Trigonal C3 (hexagonal)
       {"P3" , 1 },
       {"P31" , 3 },
       {"P32" , 5 },
    //Trigonal D3 (rhombohedral)
       {"R32" , 11 },
    //Trigonal D3 (hexagonal)
       {"P312" , 0 },
       {"P321" , 5 },
       {"P3112" , 3 },
       {"P3121" , 36 },
       {"P3212" , 3 },
       {"P3221" , 42 },
    //Hexagonal C6
       {"P6" , 4 },
       {"P61" , 10 },
       {"P65" , 8 },
       {"P62" , 2 },
       {"P64" , 2 },
       {"P63" , 6 },
    //Hexagonal D6
       {"P622" , 2 },
       {"P6122" , 18 },
       {"P6522" , 13 },
       {"P6222" , 5 },
       {"P6422" , 5 },
       {"P6322" , 7 },
    //Cubic T
       {"P23" , 0 },
       {"F23" , 0 },
       {"I23" , 4 },
       {"P213" , 6 },
       {"I213" , 3 },
    //Cubic O
       {"P432" , 0 },
       {"P4232" , 1 },
       {"F432" , 2 },
       {"F4132" , 1 },
       {"I432" , 2 },
       {"P4332" , 1 },
       {"P4132" , 1 },
       {"I4132" , 1 }
     }
   }

    //convert to Hall symbols
    int get(std::string sg_hall)
    {
      cctbx::sgtbx::space_group sg(sg_hall);
      for (std::map<std::string,double>::iterator iter = prior.begin(); iter != prior.end(); iter++)
        if (cctbx::sgtbx::space_group_symbols(iter->first,"A1983").hall() == sg_hall)
          return iter->second;
      return 1;
    }
};

} //phaser

#endif
