//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/data/PattersonFunction.h>
#include <phasertng/data/PattersonCoefficients.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Substructure_Patterson_analysis
  void Phasertng::runSSP()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (REFLECTIONS->check_native())
    throw Error(err::INPUT,"Reflections have no anomalous signal");
    if (DAGDB.size() > 1)
    throw Error(err::INPUT,"More than one node in dag");
    if (DAGDB.size() == 1 and !DAGDB.WORK->SADSIGMAA.in_memory())
    throw Error(err::INPUT,"Sigmaa parameters not present in dag");
    logTab(out::VERBOSE,"Sigmaa from Null hypothesis are input");

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    sv_double epsnSigmaN; // Empty array needed to trigger internal calculation
    selected.flag_outliers_and_select(REFLECTIONS.get(),
        { rejected::RESO,
          rejected::MISSING,
          rejected::SYSABS,
          //rejected::WILSONNAT,
          rejected::WILSONPOS,
          rejected::WILSONNEG,
          //rejected::LOINFONAT,
          rejected::LOINFOPOS,
          rejected::LOINFONEG,
          //rejected::NOMEAN,
          rejected::NOANOM,
          rejected::BADANOM,
        },
        epsnSigmaN,
        DAGDB.WORK->SADSIGMAA
        );
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }

    bool origin_removed(false);
    PattersonFunction patterson;
    int numreported;
    double percent_cutoff =
        input.value__percent.at(".phasertng.substructure.patterson_analysis.percent.").value_or_default();
    int maxnum =
        input.value__int_number.at(".phasertng.substructure.patterson_analysis.maximum_stored.").value_or_default();
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Patterson Calculation");
    PattersonCoefficients spacoef(
        REFLECTIONS->SG,
        REFLECTIONS->UC,
        REFLECTIONS->get_miller_array());
    if (input.value__choice.at(".phasertng.substructure.patterson_analysis.target.").value_or_default_equals("anomdiff"))
    {
      origin_removed = false;
      logTab(where,"Anomalous difference Patterson map");
      spacoef.anomdiff(REFLECTIONS.get());
    }
    else if (input.value__choice.at(".phasertng.substructure.patterson_analysis.target.").value_or_default_equals("likelihood"))
    {
      origin_removed = false;
      logTab(where,"LLGI-gradient Patterson-like map");
      double fp(0),fdp(0);
      std::string atomtype;
      scatterers.insert(input.value__string.at(".phasertng.substructure.scatterer.").value_or_default());
      // AJM  cluster compounds not implemented
      for (auto comp : DAGDB.WORK->COMPOSITION)
        scatterers.insert(comp.first);
      logTabArray(where,scatterers.logScatterers("Phassade"));
      atomtype = scatterers.strongest_anomalous_scatterer().first;
      fp = scatterers.strongest_anomalous_scatterer().second.fp();
      fdp = scatterers.strongest_anomalous_scatterer().second.fdp();
      double dBscat = input.value__flt_number.at(".phasertng.substructure.content_analysis.delta_bfactor.").value_or_default();
      spacoef.LLdLL_by_dUsqr_at_0(REFLECTIONS.get(),
          selected.get_selected_array(),
          atomtype,fp,fdp,dBscat,DAGDB.WORK->SADSIGMAA);
    }
    else if (input.value__choice.at(".phasertng.substructure.patterson_analysis.target.").value_or_default_equals("expected"))
    {
      origin_removed = true;
      logTab(where,"Expected ZH origin-removed Patterson-like map");
      std::string atomtype;
      if (input.value__string.at(".phasertng.substructure.scatterer.").is_default())
        throw Error(err::INPUT,"Content analysis failed, no scatterer");
      scatterers.insert(input.value__string.at(".phasertng.substructure.scatterer.").value_or_default());
      // AJM  cluster compounds not implemented
      for (auto comp : DAGDB.WORK->COMPOSITION)
        scatterers.insert(comp.first);
      logTabArray(where,scatterers.logScatterers("Phassade"));
      // Calculation assumes actual substructure (perfect model)
      likelihood::isad::sigmaa ISAD_perfect = DAGDB.WORK->SADSIGMAA;
      int nbins = ISAD_perfect.SigmaN_bin.size();
      for (int s = 0; s < nbins; s++)
      {
        ISAD_perfect.SigmaH_bin[s]  = ISAD_perfect.SigmaQ_bin[s];
        ISAD_perfect.sigmaHH_bin[s] = ISAD_perfect.sigmaQQ_bin[s];
      }
      spacoef.expectZHsigmaZH(REFLECTIONS.get(),
          selected.get_selected_array(),
          ISAD_perfect);
    }
    logTab(out::VERBOSE,"Number of coefficients = " + itos(spacoef.nrefl()));

    //this is the calculation of the patterson using whichever coefficients have been requested
    //frequency analysis not important
    bool use_multiplicity(true);
    auto text = patterson.calculate(
        REFLECTIONS->SG.group(),
        REFLECTIONS->UC.cctbxUC,
        spacoef.get_miller_array(),
        spacoef.get_cmplex_intensity_array(),
        spacoef.get_sigma_array(),
        origin_removed,
        use_multiplicity,
        input.value__percent.at(".phasertng.substructure.patterson_analysis.minimum_percent.").value_or_default_percent(),
        input.value__int_number.at(".phasertng.substructure.patterson_analysis.minimum_number_of_reflections.").value_or_default(),
        input.value__flt_number.at(".phasertng.substructure.patterson_analysis.minimum_resolution.").value_or_default()
      );
    logTabArray(out::VERBOSE,text);
    numreported = patterson.number_reported(percent_cutoff);
    int io_nprint = 20;
    double origin_distance(10.);
    logTabArray(out::VERBOSE,patterson.logStatsExtra());
    logTabArray(out::VERBOSE,patterson.logStatistics(percent_cutoff,"Substructure Patterson before Origin Removal",io_nprint,origin_distance));
    logBlank(out::VERBOSE);
    logTab(out::LOGFILE,"Peaks:");
    logTab(out::LOGFILE,"Percent cutoff = " + std::to_string(percent_cutoff));
    logTab(out::LOGFILE,"Number until origin purge = " + std::to_string(numreported));
    patterson.erase_origin(origin_distance);
    numreported = patterson.number_reported(percent_cutoff);
    logTab(out::LOGFILE,"Number after origin purge = " + std::to_string(numreported));
    if (maxnum > 0)
      numreported = std::min(numreported,maxnum);
    logTab(out::LOGFILE,"Number after stored purge = " + std::to_string(numreported));
    logTabArray(out::LOGFILE,patterson.logStatistics(percent_cutoff,"Substructure Patterson",io_nprint,origin_distance));

    FileSystem Shelxc(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".shelxc.hkl"));
    logFileWritten(out::LOGFILE,WriteFiles(),"Shelxc",Shelxc);
    if (WriteFiles())
    {
     // DAGDB.shift_tracker(hashing(),input.running_mode);
      spacoef.WriteHkl(Shelxc);
      input.value__path.at(".phasertng.substructure.patterson_analysis.shelxc.").set_value(Shelxc.fstr());
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Phassade Patterson Files");
    input.push_back(".phasertng.substructure.patterson_analysis.insufficient_data.",false);
    input.push_back(".phasertng.substructure.patterson_analysis.insufficient_resolution.",false);
    if (patterson.insufficient_data)
    {
      input.push_back(".phasertng.substructure.patterson_analysis.insufficient_data.",true);
    }
    else if (patterson.insufficient_resolution)
    {
      input.push_back(".phasertng.substructure.patterson_analysis.insufficient_resolution.",true);
    }
    else
    {
      FileSystem MAPOUT(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".map"));
      FileSystem PDBOUT(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".pdb"));
      FileSystem MTZOUT(DataBase(),DAGDB.PATHWAY.ULTIMATE.FileName(".mtz"));
      logFileWritten(out::LOGFILE,WriteFiles(),"Phassade Patterson Map",MAPOUT);
      logFileWritten(out::LOGFILE,WriteFiles(),"Phassade Patterson Data",MTZOUT);
      logFileWritten(out::LOGFILE,WriteFiles(),"Phassade Patterson Vector Coordinates",PDBOUT);
      if (WriteFiles())
      {
        patterson.WriteMap(MAPOUT);
        patterson.WritePdb(PDBOUT,percent_cutoff);
        patterson.WriteMtz(MTZOUT);
      }
      logBlank(where);

      bool constellation = input.value__boolean.at(".phasertng.substructure.patterson_analysis.constellation.").value_or_default();
      if (constellation)
      {
        logUnderLine(where,"Patterson Constellation Function");
        logTab(where,"Space Group: " + patterson.SG.CCP4);
        logTab(where,"Patterson Space Group: " + patterson.PATT_SG.CCP4);
        bool minimum_function = input.value__choice.at(".phasertng.substructure.patterson_analysis.target.").value_or_default_equals("minimum");
        int npeaks = input.value__int_number.at(".phasertng.substructure.constellation.size.").value_or_default();
        double dist = input.value__flt_number.at(".phasertng.substructure.constellation.distance.").value_or_default();
        int number_of_shifts = input.value__int_number.at(".phasertng.substructure.constellation.number_of_shifts.").value_or_default();
        logTab(where,"Function: " + input.value__choice.at(".phasertng.substructure.patterson_analysis.target.").value_or_default());
        logTab(where,"Size: " + itos(npeaks));
        logTab(where,"Distance: " + dtos(dist));
        sv_bool use_peak(numreported);
        use_peak[0] = false;
        for (int i = (origin_removed ? 0 : 1); i < numreported; i++) //not the origin
        {
          logTab(where,"Shift #" + itos(i));
          logTab(where,"Vector " + dvtos(patterson.site(i)));
          cctbx::sgtbx::site_symmetry this_site_sym(
              REFLECTIONS->UC.cctbxUC,
              patterson.PATT_SG.group(),
              patterson.site(i));
          bool harker = patterson.harker(patterson.site(i));
          bool origin = patterson.origin(patterson.site(i),dist).first;
          bool multiple = (patterson.PATT_SG.group().order_z()/this_site_sym.multiplicity() > 1);
          logTab(where,"On a Harker section: " + btos(harker));
          logTab(where,"Close to origin:     " + btos(origin));
          logTab(where,"Multiple :           " + btos(multiple));
          use_peak[i] = (!harker and !origin and !multiple);
        }
        logBlank(where);
        //generate all combinations of vectors
        //vector of vectors
        //outer vector is the list of combinations
        //inner vector is the list of peaks that that combination
        std::vector<sv_int> combos(1); //start with one combination, so that push_back works
        {
          int n(numreported);
          std::vector<bool> v(n);
          std::fill(v.begin(), v.begin() + number_of_shifts, true);
          do {
            for (int i = 0; i < n; ++i) { if (v[i]) {
            //std::cout << " " << i ; //print for debugging
            combos.back().push_back(i); } }
            combos.push_back(sv_int(0));
            //std::cout << std::endl; //print for debugging
          } while (std::prev_permutation(v.begin(), v.end()));
        }
        combos.pop_back(); //remove last (empty) combination (which may be the first, if there are none)
        logTab(where,"There were " + itos(combos.size()) + " combinations of size " + itos(number_of_shifts));
        sv_bool use_combo(combos.size(),true);
        logTab(where,"Combinations");
        for (int j = 0; j < combos.size(); j++)
        {
          for (int k = 0; k < combos[j].size(); k++)
          {
            int i = combos[j][k];
            if (!use_peak[i]) //if the peaks include ones that are harkers or close to origin, ignore
              use_combo[j] = false;
          }
          logTab(where,"Combination #" + itos(j+1,2,false,false) + " " + ivtos(combos[j]) + " " + btos(use_combo[j]));
        }
        logTab(where,"Combinations");
        if (!std::count(use_combo.begin(),use_combo.end(),true))
          logTab(where,"None");
        for (int j = (origin_removed ? 0 : 1); j < combos.size(); j++) //not the origin
        {
          if (use_combo[j]) //only use the interesting ones
          {
            logTab(where,"Combination #" + itos(j+1));
            af_dvect3 shifts; //this is the lift of shift vectors derived from the combo list
            for (int k = 0; k < combos[j].size(); k++)
            {
              int i = combos[j][k];
              shifts.push_back(patterson.site(i)); //collect the sites from the patterson
            }
            for (int i = 0; i < shifts.size(); i++)
            {
              logTab(where,"Shift #" + itos(i+1) + " " + dvtos(shifts[i]));
            }
            af_dvect3 sites; //this is the resulting constellation of sites from the deconvolution
                             //the size of the constellation is already set at this point, npeaks above
            af::versa<double, af::c_grid<3> > minfn; //this is the map
            std::tie(sites,minfn) = patterson.constellation( minimum_function, npeaks, shifts);
            for (int i = 0; i < sites.size(); i++)
            {
              logTab(where,"Constellation #" + itos(i+1) + " " + dvtos(sites[i]));
            }
//AJM TODO make this make a new Entry with the coordinates in SUBSTRUCTURE
//Add a new Node for each set of coordinates
//The maps should probably not be stored in the node in memory but only written to disk for debugging
            if (Suite::Extra())
            {
              Identifier modlid = DAGDB.PATHWAY.ULTIMATE;
              std::string cc = ntos(j,combos.size());
              FileSystem MAPOUT(DataBase(),modlid.FileName(cc+".minimum_function.map"));
              logFileWritten(out::TESTING,WriteFiles(),"Minimum Function Map",MAPOUT);
              if (WriteFiles()) patterson.WriteMap(MAPOUT,minfn);
              FileSystem PDBOUT(DataBase(),modlid.FileName(cc+".minimum_function.pdb"));
              logFileWritten(out::TESTING,WriteFiles(),"Minimum Function Pdb",PDBOUT);
              if (WriteFiles()) patterson.WritePdb(PDBOUT,percent_cutoff,sites);
            }
          }
        }
      }
    }
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Data"));
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
