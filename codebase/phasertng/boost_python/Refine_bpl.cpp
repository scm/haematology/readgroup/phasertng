//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
//#include <scitbx/stl/set_wrapper.h>
//#include <scitbx/stl/map_wrapper.h>
#include <scitbx/array_family/boost_python/c_grid_flex_conversions.h>
//#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <boost_adaptbx/std_pair_conversion.h>
#include <boost_adaptbx/tuple_conversion.h>

#include <phasertng/boost_python/std_triple_conversion.h>
#include <phasertng/boost_python/std_quad_conversion.h>
#include <phasertng/boost_python/std_quint_conversion.h>

#include <phasertng/minimizer/RefineANISO.h>
#include <phasertng/data/ReflRowsAnom.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/SigmaN.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_phasertng_refine()
{
 // using namespace scitbx::stl::boost_python;
  using namespace boost::python;

  class_<RefineANISO>("RefineANISO",init<const ReflRowsAnom&,Selected&,SigmaN&>())
    .def_readwrite("nmp", &RefineANISO::nmp)
    .def_readwrite("total_number_of_parameters_", &RefineANISO::total_number_of_parameters_ )
    .def_readwrite("previous_h_inverse_approximation_", &RefineANISO::previous_h_inverse_approximation_ )
    .def("set_macrocycle_protocol",&RefineANISO::set_macrocycle_protocol)
    .def("macrocycle_parameter_names",&RefineANISO::macrocycle_parameter_names)
    .def("get_macrocycle_parameters",&RefineANISO::get_macrocycle_parameters)
    .def("set_macrocycle_parameters",&RefineANISO::set_macrocycle_parameters)
    .def("macrocycle_large_shifts",&RefineANISO::macrocycle_large_shifts)
    .def("bounds",&RefineANISO::bounds)
    .def("reparameterize",&RefineANISO::reparameterize)
    .def("likelihood",&RefineANISO::likelihood)
    .def("target",&RefineANISO::target)
    .def("target_gradient",&RefineANISO::target_gradient)
    .def("target_gradient_hessian",&RefineANISO::target_gradient_hessian)
    .def("get_reparameterized_parameters",&RefineANISO::get_reparameterized_parameters)
    .def("set_reparameterized_parameters",&RefineANISO::set_reparameterized_parameters)
    .def("reparameterized_large_shifts",&RefineANISO::reparameterized_large_shifts)
    .def("reparameterized_bounds",&RefineANISO::reparameterized_bounds)
    .def("reparameterized_target_gradient",&RefineANISO::reparameterized_target_gradient)
    .def("reparameterized_target_gradient_hessian",&RefineANISO::reparameterized_target_gradient_hessian)
    .def("reparameterized_adjusted_target_gradient_hessian",&RefineANISO::reparameterized_adjusted_target_gradient_hessian)
    .def("maximum_distance",&RefineANISO::maximum_distance)
    .def("damped_shift",&RefineANISO::damped_shift)
    .def("initial_statistics",&RefineANISO::initial_statistics)
    .def("current_statistics",&RefineANISO::current_statistics)
    .def("final_statistics",&RefineANISO::final_statistics)
    .def("reject_outliers",&RefineANISO::reject_outliers)
    .def("setup_parameters",&RefineANISO::setup_parameters)
    .def("cleanup_parameters",&RefineANISO::cleanup_parameters)
    .def("finalize_parameters",&RefineANISO::finalize_parameters)
    .def("set_threading",&RefineANISO::set_threading)
  ;

  class_<dtmin::Bounds>("Bounds",init<>())
    .def("lower_off",&dtmin::Bounds::lower_off)
    .def("upper_off",&dtmin::Bounds::upper_off)
    .def("lower_on",&dtmin::Bounds::lower_on)
    .def("upper_on",&dtmin::Bounds::upper_on)
    .def("on",&dtmin::Bounds::on)
    .def("off",&dtmin::Bounds::off)
    .def("lower_bounded",&dtmin::Bounds::lower_bounded)
    .def("upper_bounded",&dtmin::Bounds::upper_bounded)
    .def("lower_limit",&dtmin::Bounds::lower_limit)
    .def("upper_limit",&dtmin::Bounds::upper_limit)
  ;

  class_<dtmin::Reparams>("Reparams",init<>())
    .def_readwrite("reparamed", &dtmin::Reparams::reparamed)
    .def_readwrite("offset", &dtmin::Reparams::offset)
    .def("off",&dtmin::Reparams::off)
    .def("on",&dtmin::Reparams::on)
    .def("param",&dtmin::Reparams::param)
    .def("unparam",&dtmin::Reparams::unparam)
    .def("gradient",&dtmin::Reparams::gradient)
  ;

  using namespace scitbx::boost_python::container_conversions;
  tuple_mapping<std::vector<dtmin::Bounds> , variable_capacity_policy >();
  tuple_mapping<std::vector<dtmin::Reparams> , variable_capacity_policy >();
  //tuple_mapping<std::vector<dvect3> , variable_capacity_policy >();

  boost_adaptbx::std_pair_conversions::to_and_from_tuple<double, sv_double >();
  boost_adaptbx::std_pair_conversions::to_and_from_tuple<bool, af::versa<double, af::flex_grid<> > >();
  boost_adaptbx::std_triple_conversions::to_and_from_tuple<double, af::versa<double, af::flex_grid<> > , bool >();
  boost_adaptbx::std_quad_conversions::to_and_from_tuple<double, sv_double, af::versa<double, af::flex_grid<> > , bool >();
  boost_adaptbx::std_quint_conversions::to_and_from_tuple<double, sv_double, af::versa<double, af::flex_grid<> > , bool, bool >();
}

} // namespace phasertng::<anonymous>

void init_phasertng_refine()
{
  wrap_phasertng_refine();
}

} // namespace boost_python
} // namespace phasertng
