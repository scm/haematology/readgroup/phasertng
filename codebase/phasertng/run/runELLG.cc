//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/math/likelihood/EELLG.h>
#include <phasertng/math/likelihood/EELLG_analytical.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Expected_llg_for_search
  void Phasertng::runELLG()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->get_ztotalscat())
    throw Error(err::INPUT,"Total scattering not set");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::VARIANCE_MTZ,id,false);
    }}

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    //below for self documenting code, indices into af::double4
    const int imap(3),islow(2),imidl(1),ifast(0);
    int io_ellg(0);
    double io_ellg_target(0), io_mapellg_target(0);
    {{
    logUnderLine(out::LOGFILE,"Target ellg");
    (REFLECTIONS->TWINNED) ?
       logTab(out::LOGFILE,"Twinning indicated: ELLG Target doubled"):
       logTab(out::LOGFILE,"Twinning not indicated: ELLG Target NOT doubled");
    io_ellg_target = input.value__flt_number.at(".phasertng.expected.ellg.target.").value_or_default();
    io_ellg_target *= (REFLECTIONS->TWINNED ? 2.0 : 1.0);
    logTab(out::LOGFILE,"eLLG Target: " + dtos(io_ellg_target,2));
    if (REFLECTIONS->MAP)
    {
      io_ellg = imap;
      logTab(out::LOGFILE,"Map data: eLLG (amplitudes) and mapeLLG (phased) will be calculated");
      io_mapellg_target = input.value__flt_number.at(".phasertng.expected.mapllg.target.").value_or_default();
      logTab(out::LOGFILE,"Map eLLG Target: " + dtos(io_mapellg_target,2));
    }
    else
    {
      if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("ellg"))
      {
        io_ellg = islow;
        logTab(out::LOGFILE,"eLLG, eeLLG and aeLLG values will be calculated");
      }
      else if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("eellg"))
      {
        io_ellg = imidl;
        logTab(out::LOGFILE,"eeLLG and aeLLG values will be calculated");
      }
      else if (input.value__choice.at(".phasertng.expected.ellg.speed_versus_accuracy.").value_or_default_equals("aellg"))
      {
        io_ellg = ifast;
        logTab(out::LOGFILE,"aeLLG values will be calculated");
      }
    }
    }}

    if (!DAGDB.NODES.number_of_seeks())
    {
      if (input.value__string.at(".phasertng.search.tag.").is_default())
        throw Error(err::INPUT,"Search for ellg not set");
      auto tag = input.value__string.at(".phasertng.search.tag.").value_or_default();
      {{
        dag::Seek seek;
        seek.set_missing();
        seek.IDENTIFIER.initialize_tag(tag);
        auto entry = DAGDB.lookup_entry_tag(seek.IDENTIFIER,DataBase()); //will be in HOLD
        seek.IDENTIFIER = entry->identify();
        auto& node = DAGDB.NODES.back();
        node.SEEK.push_back(seek);
        auto modlid = seek.IDENTIFIER;
        PHASER_ASSERT(node.DRMS_SHIFT.count(modlid));//check already set from HOLD
        phaser_assert(node.VRMS_VALUE.count(modlid));//check already set from HOLD
        phaser_assert(node.CELL_SCALE.count(modlid));//check already set from HOLD
      }}
    }

    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      DAGDB.WORK->set_star_from_pose();
      DAGDB.WORK->HALL = REFLECTIONS->SG.HALL;
      DAGDB.WORK->CELL = REFLECTIONS->UC.cctbxUC;
      DAGDB.reset_work_entry_pointers(DataBase());
    }

    DAGDB.restart();
    if (!DAGDB.NODES.number_of_seeks())
    {
      logTab(out::LOGFILE,"No seek entries in Dag");
    }
    else
    {
      //make sure the reflections are sorted low resolution to high resolution
      //this should be done as part of the data preparation step
      if (!REFLECTIONS->check_sort_in_resolution_order())
        throw Error(err::INPUT,"Reflection file not sorted by resolution, aborting");

      likelihood::mapELLG likelihood_emap;
      likelihood::ELLG likelihood_ellg;
      likelihood::EELLG likelihood_eellg;
      likelihood::EELLG_analytical likelihood_aellg;

      //the average vrms is a horrible approximation, but is good for af2 models
      //which don't have ensembles and are all the same rmsd regardless of target
      sv_double average_vrms_final(DAGDB.size(),0.);
      {{
      out::stream where = out::VERBOSE;
      DAGDB.restart();
      int m(0);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK; //pointer
        phaser_assert(node->SEEK.size());
        sv_double average_vrms(node->SEEK.size());
        for (int s = 0; s < node->SEEK.size(); s++)
        {
          auto& seek = node->SEEK[s];
          auto& entry = seek.ENTRY;
          auto  modlid = entry->identify();
          sv_double vrms_final = pod::vrms_final(
            node->DRMS_SHIFT[modlid].VAL,entry->VRMS_START);
          for (int n = 0; n < vrms_final.size(); n++) //over all in the ensemble
            average_vrms[s] += vrms_final[n]/vrms_final.size();
        }
        for (int s = 0; s < node->SEEK.size(); s++)
        {
          logTab(where,"Seek #" + ntos(s+1,node->SEEK.size()) + "  VRMS:[average=" + dtos(average_vrms[s]) + "]");
          average_vrms_final[m] += average_vrms[s]/static_cast<double>(node->SEEK.size());
        }
        logTab(where,"Average VRMS: " + dtos(average_vrms_final[m],7,4));
        m++;
      }
      }}

      double io_sigfac = input.value__flt_number.at(".phasertng.expected.ellg.signal_resolution_factor.").value_or_default();
      for (int m = 0; m < DAGDB.size(); m++)
      { // Resolution for the interesting signal, can't go past data resolution
        // Note that this is overwritten for eLLG and mapeLLG, where Dobs is accounted for
        DAGDB.WORK->SIGNAL_RESOLUTION = io_sigfac*average_vrms_final[m];
        DAGDB.WORK->SIGNAL_RESOLUTION = std::max(REFLECTIONS->data_hires(),DAGDB.WORK->SIGNAL_RESOLUTION);
      }

      {{
      //check that the scattering is all ok first
      DAGDB.restart();
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Check Scattering");
      logTab(where,"Total Scattering = " + dtos(REFLECTIONS->get_ztotalscat(),0));
      int n(1);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK; //pointer
        double FP = 0;
        for (int s = 0; s < node->SEEK.size(); s++)
        {
          auto& seek = node->SEEK[s];
          auto& entry = seek.ENTRY;
          PHASER_ASSERT(entry->SCATTERING);
          FP += entry->fraction_scattering(REFLECTIONS->get_ztotalscat());
          if (n == 1)
          logTab(where,"Seek #" + ntos(s+1,node->SEEK.size()) + " " + entry->identify().tag() + " scattering++ " + dtos(FP,5));
          if (FP > 1.0)
          {
            throw Error(err::INPUT,"Seek scattering > Total scattering");
          }
        }
        n++;
      }
      }}

      if (Suite::Extra())
      {
        for (auto& entry : DAGDB.ENTRIES)
          logTabArray(out::TESTING,entry.second.VARIANCE.BIN.logBin("variance"));
      }

      int nrefl(0);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
        if (selected.get_selected(r) and REFLECTIONS->get_present(friedel::NAT,r))
          nrefl++;
      sv_bool present(REFLECTIONS->NREFL);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
        present[r] = REFLECTIONS->get_present(friedel::NAT,r);

      int n(1),N(DAGDB.size());
      scitbx::af::double4 zero(0.,0.,0.,0.); //fast medium slow map ellg
      std::vector<scitbx::af::double4> seek_cumulative_ellg(DAGDB.size(),zero);
      //where there are no ensembles and the vrms are uniformly low
      //seek increases in indexed on seek number but not all will be filled
      std::vector<sv_double> seek_increases(DAGDB.size());
      DAGDB.restart();
      DAGDB.reset_entry_pointers(DataBase());
      while (!DAGDB.at_end())
      {
        std::string nN = nNtos(n++,N);
        int m(n-2);
        out::stream where = out::LOGFILE;
        logUnderLine(where,"Expected LLG" + nN);
        auto& node = DAGDB.WORK; //pointer
        logTabArray(where,node->logSeek(""));
        node->SEEK_COMPONENTS = nmol; //minimum, even if target is not reached
        node->SEEK_COMPONENTS_ELLG = 0;
        node->SEEK_RESOLUTION = 0;
        node->SEEK_RESOLUTION_PHASED = 0;
        //llg is added to total for each seek component
        //first placement is nmol copies, which is seek position nmol-1
        bool reached_target(false);
        sv_double ellgdata(REFLECTIONS->NREFL,0.);
        std::vector<scitbx::af::double4> seek_ellg(node->SEEK.size(),zero);
        if (io_ellg == imap)
          logTab(where,snprintftos("%4s %14s %16s %16s",
                  "Seek","Resolution","eLLG (unphased)","mapeLLG (phased)"));
        else
          logTab(where,snprintftos("%4s %14s %14s %14s %14s",
                  "Seek","Resolution","Fast (aellg)","Medium (eellg)","Slow (ellg)"));
        for (int s = 0; s < node->SEEK.size(); s+=nmol)
        {
          bool nmol_divisible((s%nmol)==0);
          PHASER_ASSERT(nmol_divisible);
          auto& entry = node->SEEK[s].ENTRY;
          double FP = 0;
          for (int ss = 0; ss <= s; ss++)
          {
            phaser_assert(ss < node->SEEK.size());
            auto& ssentry = node->SEEK[ss].ENTRY;
            phaser_assert(ssentry);
            FP += ssentry->fraction_scattering(REFLECTIONS->get_ztotalscat());
            PHASER_ASSERT(FP <= 1);
          }
          PHASER_ASSERT(FP > 0);

          if (io_ellg == ifast or io_ellg == imidl or io_ellg == islow)
          {
            double smax = 1. / REFLECTIONS->data_hires();
            double expected_LLGI = likelihood_aellg.get(FP, average_vrms_final[m], smax)*nrefl;
                   expected_LLGI *= REFLECTIONS->oversampling_correction();
            seek_cumulative_ellg[m][ifast] = expected_LLGI;
            //average over all vrms in ensemble (not great approx)
            logTab(where,snprintftos("%4d %14s %14.2f %14s %14s",
                             s+1,"",seek_cumulative_ellg[m][ifast],"",""));
          }

          //preparatory arrays
          af_double Siga;
          bool calculate_sigmaa(io_ellg == imidl or
                                io_ellg == islow or
                                io_ellg == imap);
          if (calculate_sigmaa)
          {
            Siga.resize(REFLECTIONS->NREFL);
            auto& variance = node->SEEK[s].ENTRY->VARIANCE;
            for (int r = 0; r < REFLECTIONS->NREFL; r++)
            {
              Siga[r] = std::sqrt(FP*variance.rSigaSqr[r]); //take sqrt binwise
            }
          }

          if (io_ellg == imidl or io_ellg == islow)
          {
            for (int r = 0; r < REFLECTIONS->NREFL; r++)
            {
              if (selected.get_selected(r) and present[r])
              {
                const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
                double SigaSqr = fn::pow2(Siga[r]);
                double expected_LLGI = likelihood_eellg.get(SigaSqr);
                       expected_LLGI *= REFLECTIONS->oversampling_correction();
                ellgdata[r] += expected_LLGI;
                seek_cumulative_ellg[m][imidl] += expected_LLGI;
                if (s == 0 and
                    !reached_target and
                    seek_cumulative_ellg[m][imidl] > io_ellg_target)
                {
                  reached_target = true;
                  node->SEEK_RESOLUTION = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                  node->SEEK_COMPONENTS = s+nmol;
                  node->SEEK_COMPONENTS_ELLG = seek_cumulative_ellg[m][imidl];
                  logTab(where,snprintftos("%4d %14.2f %14s %14s %14s",
                             s+1,node->SEEK_RESOLUTION,"","",""));
                }
              }
            }
            logTab(where,snprintftos("%4d %14s %14s %14.2f %14s",
                             s+1,"","",seek_cumulative_ellg[m][imidl],""));
          }

          if (io_ellg == islow or io_ellg == imap)
          {
            reached_target = false;
            // Keep track of the cumulative increase of eLLG with resolution
            std::vector<std::pair<double,double> > cumulative_ellg_by_reso;
            int nref = REFLECTIONS->NREFL;
            double nref_by_100(nref / 100.);
            int next_percentile = 1;
            for (int r = 0; r < nref; r++)
            {
              if (selected.get_selected(r) and present[r])
              {
                const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
                const double feff = work_row->get_feffnat();
                const double dobs = work_row->get_dobsnat();
                const double resn = work_row->get_resn();
                const double teps = work_row->get_teps();
                const double ssqr = work_row->get_ssqr();
                const bool cent = work_row->get_cent();
                double DobsSiga = dobs*Siga[r];
                double Eeff = feff/(resn*std::sqrt(teps));
                double expected_LLGI = likelihood_ellg.get(Eeff,DobsSiga,cent);
                       expected_LLGI *= REFLECTIONS->oversampling_correction();
                ellgdata[r] += expected_LLGI;
                seek_cumulative_ellg[m][islow] += expected_LLGI;
                if (r/nref_by_100 > next_percentile or r == nref)
                {
                  cumulative_ellg_by_reso.push_back({ssqr, seek_cumulative_ellg[m][islow]});
                  next_percentile = floor(r/nref_by_100) + 1;
                }

                if (s == 0 and
                    !reached_target and
                    seek_cumulative_ellg[m][islow] > io_ellg_target)
                {
                  reached_target = true;
                  node->SEEK_RESOLUTION = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                  node->SEEK_COMPONENTS = s+nmol;
                  node->SEEK_COMPONENTS_ELLG = seek_cumulative_ellg[m][islow];
                  logTab(where,snprintftos("%4d %14.2f %14s %14s %14s",
                             s+1,node->SEEK_RESOLUTION,"","",""));
                }
              }
            }
            // Define signal resolution as resolution that achieves 99% of total eLLG
            double signal_resolution = REFLECTIONS->data_hires();
            double signal_eLLG = 0.99 * seek_cumulative_ellg[m][islow];
            for (int b = 0; b < cumulative_ellg_by_reso.size(); b++)
            {
              if (cumulative_ellg_by_reso[b].second >= signal_eLLG)
              {
                signal_resolution = 1. / std::sqrt(cumulative_ellg_by_reso[b].first);
                break;
              }
            }
            if (io_ellg == imap)
              logTab(where,snprintftos("%4d %14s %16.2f %14s",
                              s+1,"",seek_cumulative_ellg[m][islow],""));
            else
              logTab(where,snprintftos("%4d %14s %14s %14s %14.2f",
                              s+1,"","","",seek_cumulative_ellg[m][islow]));
            DAGDB.WORK->SIGNAL_RESOLUTION = signal_resolution;
          }

          if (io_ellg == imap) // mapeLLG
          {
            reached_target = false;
            // Keep track of the cumulative increase of eLLG with resolution
            std::vector<std::pair<double,double> > cumulative_mapellg_by_reso;
            int nref = REFLECTIONS->NREFL;
            double nref_by_100(nref / 100.);
            int next_percentile = 1;
            for (int r = 0; r < nref; r++)
            {
              if (selected.get_selected(r) and present[r])
              {
                const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
                const double feff = work_row->get_feffnat();
                const double dobs = work_row->get_dobsnat();
                const double resn = work_row->get_resn();
                const double teps = work_row->get_teps();
                const double ssqr = work_row->get_ssqr();
                const bool cent = work_row->get_cent();
                double DobsSiga = dobs*Siga[r];
                double Eeff = feff/(resn*std::sqrt(teps));
                double expected_LLG = likelihood_emap.get(Eeff,DobsSiga,cent);
                       expected_LLG *= REFLECTIONS->oversampling_correction();
                ellgdata[r] += expected_LLG;
                seek_cumulative_ellg[m][imap] += expected_LLG;
                if (r/nref_by_100 > next_percentile or r == nref)
                {
                  cumulative_mapellg_by_reso.push_back({ssqr, seek_cumulative_ellg[m][imap]});
                  next_percentile = floor(r/nref_by_100) + 1;
                }

                if (s == 0 and
                    !reached_target and
                    seek_cumulative_ellg[m][imap] > io_mapellg_target)
                {
                  reached_target = true;
                  node->SEEK_RESOLUTION_PHASED = REFLECTIONS->UC.cctbxUC.d(work_row->get_miller());
                  // node->SEEK_COMPONENTS_PHASED = s+nmol;
                  // node->SEEK_COMPONENTS_ELLG_PHASED = seek_cumulative_ellg[m][imap];
                  logTab(where,snprintftos("%4d %14.2f %14s %14s %14s",
                             s+1,node->SEEK_RESOLUTION_PHASED,"","",""));
                }
              }
            }
            // Define signal resolution as resolution that achieves 99% of total eLLG
            double signal_resolution = REFLECTIONS->data_hires();
            double signal_eLLG = 0.99 * seek_cumulative_ellg[m][imap];
            for (int b = 0; b < cumulative_mapellg_by_reso.size(); b++)
            {
              if (cumulative_mapellg_by_reso[b].second >= signal_eLLG)
              {
                signal_resolution = 1. / std::sqrt(cumulative_mapellg_by_reso[b].first);
                break;
              }
            }
            logTab(where,snprintftos("%4d %14s %16s %16.2f",
                             s+1,"","",seek_cumulative_ellg[m][imap]));
            DAGDB.WORK->SIGNAL_RESOLUTION_PHASED = signal_resolution;
          }

          if (s == 0) //this is the minimum even if it is not target ellg
          {
            if (io_ellg == imap)
            {
              node->SEEK_COMPONENTS_ELLG = seek_cumulative_ellg[m][islow];
            }
            else
            {
              node->SEEK_COMPONENTS_ELLG = seek_cumulative_ellg[m][io_ellg];
            }
          }
          if (io_ellg == imap)
          {
            seek_ellg[s][io_ellg] = seek_cumulative_ellg[m][islow];
          }
          else
          {
            seek_ellg[s][io_ellg] = seek_cumulative_ellg[m][io_ellg];
          }
          double increase = (s == 0)  ?
                     seek_ellg[s][io_ellg] :
                     seek_ellg[s][io_ellg] - seek_ellg[s-nmol][io_ellg];
          seek_increases[m].push_back(increase);
          if (!reached_target and increase > io_ellg_target)
          {
            reached_target = true;
            node->SEEK_RESOLUTION = 0; //na
            node->SEEK_COMPONENTS = s+nmol;
            node->SEEK_COMPONENTS_ELLG = seek_cumulative_ellg[m][io_ellg];
          }
        }

        if (io_ellg == imap or io_ellg == islow or io_ellg == imidl) //medium or slow or map
        {
          af_string table; Loggraph loggraph;
          std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
              ellgdata,present,selected.get_selected_array(),"Expected LLGI","eLLGI",false);
          logTabArray(out::VERBOSE,table);
          logGraph(out::LOGFILE,loggraph);
        }
      }

      {{
      out::stream where = out::LOGFILE;
      DAGDB.restart();
      int m(0);
      while (!DAGDB.at_end())
      {
        DAGDB.WORK->generic_int = m++; //original index for lookup in table
      }
      logEllipsisOpen(where,"Sort into search order");
      DAGDB.NODES.apply_sort_for_search();
      logEllipsisShut(where);
      }}

      {{
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Store the searches that are most likely to generate a solution");
      int nmax = input.value__int_number.at(".phasertng.expected.ellg.maximum_stored.").value_or_default();
      if (nmax == 0) //flag for use all
        nmax = DAGDB.size();
      int before = DAGDB.size();
      DAGDB.restart();
      int m(0);
      while (!DAGDB.at_end())
      {
        DAGDB.WORK->generic_flag = bool(m < nmax);
        m++;
      }
      DAGDB.NODES.erase_invalid();
      logEllipsisShut(where);
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.size()));
      }}

      {{
      out::stream where = out::SUMMARY;
      logTableTop(where,"Ensemble eLLG");
      logTab(where,"Target eLLG = " + dtos(io_ellg_target,10,2));
      logTab(where,"Data Resolution = " + dtos(REFLECTIONS->data_hires(),7,4));
      logTab(where,"tNCS Order = " + itos(nmol));
      logTab(where,snprintftos(
          //   1   2    3   4     5    6   7    8   9     10 11
           "%-5s %-5s %12s %12s %12s %12s %11s %8s %12s  %3s %s",
           "sort#", //1
           "orig#", //2
           " eLLG-(all)-", //3
           "eeLLG-(all)-", //4
           "aeLLG-(all)-", //5
           "target-reso", //6
           "signal-reso", //7
           "seek-num", //8
           "seek-llg", //9
           "#", //10
           "llg-increase" //11
           ));
      DAGDB.restart();
      int mm(1);
      while (!DAGDB.at_end())
      {
        auto& node = DAGDB.WORK;
        int m = node->generic_int; //original sort order
        int s(nmol);
        double seek_total = seek_increases[m][0];
        logTab(where,snprintftos(
          //     1   2    3   4     5    6   7    8   9    10  11
               "%-5d %-5d %12s %12s %12s %12s %11s %8s %12s  %3d %s",
               mm++,
               m+1,
               seek_cumulative_ellg[m][islow] ? dtos(seek_cumulative_ellg[m][islow],12,0).c_str() : "--na--",
               seek_cumulative_ellg[m][imidl] ? dtos(seek_cumulative_ellg[m][imidl],12,0).c_str() : "--na--",
               seek_cumulative_ellg[m][ifast] ? dtos(seek_cumulative_ellg[m][ifast],12,0).c_str() : "--na--",
               node->SEEK_RESOLUTION ? dtos(node->SEEK_RESOLUTION,11,2).c_str() : "(--all--)",
               node->SIGNAL_RESOLUTION ? dtos(node->SIGNAL_RESOLUTION,11,2).c_str() : "(--all--)",
               itos(node->SEEK_COMPONENTS).c_str(),
               node->SEEK_COMPONENTS_ELLG ? dtos(node->SEEK_COMPONENTS_ELLG,12,0).c_str() : "--na--",
               s,dtos(seek_increases[m][0],0).c_str()
               ));
        for (int i = 1; i < seek_increases[m].size(); i++)
        {
          s += nmol;
          seek_total += seek_increases[m][i];
          std::string llgstr = "+" + dtos(seek_increases[m][i],0) + " (=" + dtos(seek_total,0) + ")";
          logTab(where,snprintftos(
               "%-5s %-5s %12s %12s %12s %12s %11s %8s %12s  %3d %s",
               "", "", "", "", "", "", "", "", "",
               s,llgstr.c_str()
               ));
        }
      }
      logTableEnd(where);
      }}
    }
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
