#include <phasertng/math/likelihood/isad/SADeLLG.h>
#include <phasertng/main/Assert.h>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/math/likelihood/isad/EFOM.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <phasertng/math/likelihood/isad/tgh.h>
#include <phasertng/math/likelihood/isad/tgh_flags.h>
#include <phasertng/math/likelihood/isad/EqualUnity.h>
#include <cmath>
#include <algorithm>

using namespace scitbx;

//#define PHASERTNG_SELLG_DEBUG

namespace phasertng {
namespace likelihood {

  std::array<double,3>
  SADeLLG::get(const int& s, const double& RESN, const double& TEPS,
    const double& DOBSPOS, double FEFFPOS, const double& DOBSNEG, double FEFFNEG,
    likelihood::isad::sigmaa ISAD_SIGMAA, const bool& BOTH, const bool& PLUS, const bool& CENT)
  {
    if (BOTH) return acentric(s,RESN,TEPS,DOBSPOS,FEFFPOS,DOBSNEG,FEFFNEG,ISAD_SIGMAA);
    else // Fall back to non-anomalous eLLG
    {
      double DobsSigA,Eeff,sigmaA;
      double epdiff(0); // Expected phase equals substructure phase
      sigmaA = std::sqrt(ISAD_SIGMAA.SigmaH_bin[s]/ISAD_SIGMAA.SigmaN_bin[s]);
      if (!CENT) // singleton
      {
        DobsSigA = (PLUS ? DOBSPOS : DOBSNEG) * sigmaA;
        Eeff = (PLUS ? FEFFPOS : FEFFNEG) / RESN;
      }
      else
      {
        Eeff = FEFFPOS/RESN;
        DobsSigA = DOBSPOS*sigmaA;
      }
      likelihood::ELLG ELLG;
      double eLLG = ELLG.get(Eeff,DobsSigA,CENT);
      likelihood::EFOM EFOM;
      double eFOM = EFOM.get(Eeff,DobsSigA,CENT);
      std::array<double,3> stats = {eLLG,eFOM,epdiff};
      return stats;
    }
  }

  std::array<double,3>
  SADeLLG::acentric(const int& s, const double& RESN, const double& TEPS,
    const double& DOBSPOS, double FEFFPOS, const double& DOBSNEG, double FEFFNEG,
    likelihood::isad::sigmaa ISAD_SIGMAA)
  {
    const double ZERO(0.);
    static double maxExpArg(std::log(std::numeric_limits<double>::max()));
    likelihood::isad::function ISAD(&ISAD_SIGMAA);
    likelihood::isad::tgh_flags flags;
    flags.target(true);
    flags.phsprob(true);
    likelihood::isad::tgh TGH0,TGH;
    TGH0 = ISAD.tgh_null(s, DOBSPOS, FEFFPOS, DOBSNEG, FEFFNEG, RESN, TEPS, flags);
    double L0;
    if (TGH0.LogLike < maxExpArg)
      L0 = std::exp(-TGH0.LogLike);
    else
      return std::array<double,3> {ZERO,ZERO,ZERO}; // Paranoia: shouldn't happen for data screened for BADANOM
    double absrhoFF = ISAD_SIGMAA.absrhoFF_bin[s];
    double rhopm = ISAD_SIGMAA.rhopm_bin[s];
    double ESN = fn::pow2(RESN);
    double SigmaN = ISAD_SIGMAA.SigmaN_bin[s];
    // Sigma*_bin variables are relative to specified composition and inferred
    // relative anomalous scatterer content. Put on scale of actual data for for
    // calcs, including eps-factor for this reflection.
    double rescale = ESN/SigmaN;
    //double sSigmaP = rescale*ISAD_SIGMAA.SigmaP_bin[s];
    cmplex ssigmaPP = rescale*ISAD_SIGMAA.sigmaPP_bin[s];
    double sSigmaQ = rescale*ISAD_SIGMAA.SigmaQ_bin[s];
    cmplex ssigmaQQ = rescale*ISAD_SIGMAA.sigmaQQ_bin[s];
    double RhoH = ISAD_SIGMAA.SigmaH_bin[s]/SigmaN; // ratio
    cmplex rhoHH = ISAD_SIGMAA.sigmaHH_bin[s]/SigmaN;
    double DOBSPOS2 = fn::pow2(DOBSPOS);
    double DOBSNEG2 = fn::pow2(DOBSNEG);
    double SigmaNeg = ESN*(1. - DOBSNEG2*RhoH);
    const cmplex rhoFF = (ssigmaPP + ssigmaQQ)/ESN;
#ifdef PHASERTNG_SELLG_DEBUG
    std::cout << "XXX L0, call: " << L0 << " L0corr[" << ESN << "," << std::real(rhoFF*ESN)
      << " + I * (" << std::imag(rhoFF*ESN) << "),0,0," << DOBSPOS << "," << FEFFPOS << ","
      << DOBSNEG << "," << FEFFNEG << "," << rhopm << ",1]" << std::endl;
#endif
    // Assign phase of theoretical rhoFF to rhoFFobs including the correlation term
    const cmplex rhoFFobs =
      (DOBSNEG*DOBSPOS*absrhoFF + rhopm*std::sqrt((1.-DOBSPOS2)*(1.-DOBSNEG2)))
      * rhoFF/std::abs(rhoFF);
    const cmplex sigmaPhi = ESN*(rhoFFobs - DOBSNEG*DOBSPOS*rhoHH);
    const double sigmaPhiSqr = std::norm(sigmaPhi);
    phaser_assert(SigmaNeg > 0); //divide by zero below
    double SigmaPos = ESN*(1. - DOBSPOS2*RhoH) - sigmaPhiSqr/SigmaNeg;

    // Calculate scattering factor to put E-value on scale of total FH
    // Explore possible E-values to explore possible FHPOS/FHNEG pairs
    double sRefanom = std::sqrt((sSigmaQ + std::real(ssigmaQQ))/2);
    double sImfanom = std::sqrt((sSigmaQ - std::real(ssigmaQQ))/2);
    cmplex sfanom(sRefanom,sImfanom);
    double emax = 3.7; //Less than 1 part per million that FH will be bigger
    int maxpts(0),ndiv(100);
    double de = emax/ndiv;
    double pnorm(0),eLLG(0),eFOM(0),cospdiff(0),sinpdiff(0),epdiff(0);
    double dh = std::sqrt(ISAD_SIGMAA.SigmaH_bin[s]/ISAD_SIGMAA.SigmaQ_bin[s]);
    for (int idiv = 0; idiv < ndiv; idiv++)
    {
      double thisE = (idiv+0.5)*de; //Midpoint rule
      cmplex FHPOS = dh*thisE*sfanom;
      cmplex FHNEG = std::conj(FHPOS);
      int nintpts = ISAD.calculate_integration_points(RESN, TEPS,
        DOBSPOS, FEFFPOS, FHPOS, DOBSNEG, FEFFNEG, FHNEG, SigmaPos, SigmaNeg);
      maxpts = std::max(maxpts,nintpts);
      double pe = 2*thisE*std::exp(-fn::pow2(thisE)); // Wilson probability
      TGH = ISAD.tgh_acentric_canonical(s,RESN,TEPS,DOBSPOS,FEFFPOS,FHPOS,
        DOBSNEG,FEFFNEG,FHNEG,flags,nintpts);
      double LiSAD = std::exp(-TGH.LogLike);
#ifdef PHASERTNG_SELLG_DEBUG
      if (std::abs(thisE-1) <= de/2)
        std::cout << "XXX LiSAD, call: " << LiSAD << " LiSAD[" << ESN << "," << std::real(rhoFF*ESN)
          << " + I * (" << std::imag(rhoFF*ESN) << "),0,0," << RhoH*ESN << "," << std::real(rhoHH)*ESN
          << " + I * (" << std::imag(rhoHH)*ESN << ")," <<  DOBSPOS << "," << FEFFPOS << "," << DOBSNEG
          << "," << FEFFNEG << "," << rhopm << "," << TEPS << "," << std::real(FHPOS) << " + I * ("
          << std::imag(FHPOS) << ")," << std::real(FHNEG) << " + I * (" << std::imag(FHNEG) << "),"
          << nintpts << "]" << std::endl;
#endif
      double Lratio = LiSAD/L0;
      if (Lratio > 1.e-10)
      {
        double normterm = de * pe * Lratio;
        pnorm += normterm;
        eLLG += normterm*std::log(Lratio);//(TGH.LogLike - TGH0.LogLike);
        eFOM += normterm*TGH.FOM;
        cospdiff += normterm*std::cos(TGH.PHIB);
        sinpdiff += normterm*std::sin(TGH.PHIB);
      }
    }
    if (pnorm > 0)
    {
      eLLG /= pnorm;
      eFOM /= pnorm;
      epdiff = atan2(sinpdiff,cospdiff);
    }
    else
    {
      eLLG = eFOM = epdiff = ZERO; // More paranoia: shouldn't happen if data screened for BADANOM
    }
#ifdef PHASERTNG_SELLG_DEBUG
    std::cout << "XXX eLLG, call: " << eLLG << " iSADeLLGerror[" << sSigmaP << ","
      << std::real(ssigmaPP) << " + I * (" << std::imag(ssigmaPP) << "),1," << sRefanom
      << "," << sImfanom << ",1.," << dh << "," <<  DOBSPOS << "," << FEFFPOS << "," << DOBSNEG
      << "," << FEFFNEG << "," << rhopm << "," << TEPS << "," << maxpts << "," << ndiv << "]"
      << std::endl;
#endif
    std::array<double,3> stats = {eLLG,eFOM,epdiff};
    return stats;
  }

}
}
