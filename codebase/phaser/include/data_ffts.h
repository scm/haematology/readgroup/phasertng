//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_FFTS_CLASS__
#define __PHASER_DATA_FFTS_CLASS__

namespace phaser {

struct data_ffts
{
  int MIN = 20;
  int MAX = 80;
};

} //phaser
#endif
