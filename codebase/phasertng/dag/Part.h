//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Part_class__
#define __phasertng_dag_Part_class__
#include <phasertng/main/Identifier.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Part
  { //replace the poses with a part structure after refinement
    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      bool        PRESENT = false;
  };

}} //phasertng
#endif
