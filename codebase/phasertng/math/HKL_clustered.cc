#include <phasertng/math/HKL_clustered.h>

namespace phasertng {

  void
  p3Dc::toPolar()
  {
    r = std::sqrt(x*x+y*y+z*z);
    double ct = z/r;
    theta = acos(ct);
    if (theta == 0) phi=0;
    else phi = atan2(y,x);
  }

  void
  p3Dc::toCart()
  {
    x=r*cos(phi)*sin(theta);
    y=r*sin(phi)*sin(theta);
    z=r*cos(theta);
  }
}
