//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Spanning_class__
#define __phasertng_Spanning_class__
#include <phasertng/main/includes.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <map>

namespace phasertng {

  class Coordinates;
 namespace dag { class Node; }

  class Spanning
  {
    public: //members
      SpaceGroup   SG; //from node
      UnitCell     UC; //from node
      dag::Node*   NODE;

    public:  //constructor
      Spanning() {}
      void init_node(dag::Node*);
      ~Spanning() throw() {}

    public:
      std::pair<ivect3,af_string> spans(
          double,Coordinates&,bool,std::map<std::string,Coordinates>&);
      void structure_for_spanning(Coordinates&);
  };

} //phasertng
#endif
