//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Epsilon_class__
#define __phasertng_Epsilon_class__
#include <phasertng/math/Matrix.h>
#include <phasertng/math/Tensor.h>
#include <phasertng/main/pointers.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/math/table/gfunction_of_ssqr.h>
#include <phasertng/math/table/dgfunction_of_dssqr.h>
#include <phasertng/math/table/d2gfunction_of_dssqr2.h>

namespace phasertng {

//this class handles the tncs epsilon terms
//in the case of no tncs the TEPS terms are 1, otherwise class can be configured for halfR not
// when called from FRF or BRF, or from BTF or FTF before gyre (rotref in phaser),
// false otherwise (BTF or FTF after rotref, RNP)
// however we assume that the rotations are still locked in phasertng gyre

class Epsilon
{
    //scope not obeyed
    static table::gfunction_of_ssqr     GfuncOfRSsqr; //declared
    static table::dgfunction_by_dssqr   dGfuncBydRSsqr; //declared
    static table::d2gfunction_by_dssqr2 d2GfuncBydRSsqr2; //declared

  public:
    int    TNCS_ORDER = 0; //assert value
    dvect3 TNCS_ANGLE = {0,0,0};
    double TNCS_GFNRAD = 0; //assert value
    dvect3 TNCS_VECTOR = {0,0,0};
    double TNCS_RMS = 0;
    double TNCS_FS = 1.0; //default
    sv_double VARBINS = sv_double(0); //deep copy
    SpaceGroup SG;
    UnitCell UC; //only required for check of set

   public: //limits stored
    double MIN_GFNRAD = 0;
    double MAX_GFNRAD = 0;
    double MIN_RMS = 0;
    double MAX_RMS = 0;
    double MIN_VARBIN = 0.01; //default
    double MAX_VARBIN = 100; //default

  public:
    bool REFINE_ROT = false;
    bool REFINE_GFN = false;
    bool REFINE_TRA = false;
    bool REFINE_RMSD = false;
    bool REFINE_FS = false;
    bool REFINE_BINS = false;

  private: //members
    dmat33 identity = dmat33(1,0,0,0,1,0,0,0,1);
    dmat33 zero33 = dmat33(0,0,0,0,0,0,0,0,0);

  private:
    int nsymp;
    double symfac;
    sv_dmat33 Rotsymtr;
    sv_dmat33 GfnradRotsymtrFrac;
 //parameters for reflection-wise calculations
    math::Tensor<3,dvect3> ncsDeltaT_ijsym;
 //parameters for reflection-wise calculations
    math::Tensor<3,af::double6> GfunTensor_ijsym; //std::vector based, deep copy
    math::Tensor<4,af::double6> dGfunTensor_by_dRotn_ijsymdir;
    math::Tensor<5,af::double6> d2GfunTensor_by_dRotn2_ijsymdir12;
 //parameters for reflection-wise calculations
    dmat33 ncsRmat_tr;
    sv_dmat33 dncsRmat_by_dRotn_tr;
    std::vector<sv_dmat33> d2ncsRmat_by_dRotn2_tr;

  public:
    dvect3 dEps_by_dRot = {0,0,0};
    double dEps_by_dGfn = 0;
    dvect3 dEps_by_dTra = {0,0,0};
    double dEps_by_dRms = 0;
    double dEps_by_dFs = 0;
    double dEps_by_dBin = 0;
    dmat33 d2Eps_by_dRot2 = zero33;
    double d2Eps_by_dGfn2 = 0;
    dvect3 d2Eps_by_dTra2 = {0,0,0};
    double d2Eps_by_dRms2 = 0;

  public:
    double    refl_TEPS = 1;
    double    refl_G_DRMS = 1;
    double    refl_Gsqr_Vterm = 0;
    double    refl_G_Vterm = 1;
    sv_double refl_G;
    sv_double refl_Gsqr; //elmn calculation

  private:
    bool do_gradient = false;
    bool do_hessian = false;
    bool is_diagonal = false; // Set false to use off-diagonal rotation terms
    bool halfR = false; //state of current parameters

  public:  //constructor
    Epsilon() {}
    ~Epsilon() throw() {}
    void parse(
         int TNCS_ORDER_,
         dvect3 TNCS_VECTOR_,
         double TNCS_RMS_,
         double MIN_RMS_,
         double MAX_RMS_,
         double TNCS_GFNRAD_,
         double MIN_GFNRAD_,
         double MAX_GFNRAD_);

  public: //arrays for filling
    sv_double array_G_DRMS; //same fullR/halfR
    sv_double array_G_Vterm; //halfR = false or halfR=true but nmol > 2 (so no R)
    sv_double array_Gsqr_Vterm; //halfR = true
    math::Matrix<double> matrix_G; //halfR = true
    math::Matrix<double> matrix_Gsqr; //halfR = true
    //TEPS is stored anyway

  public:
    void initialize(bool); //halfR initialization
    void setup_parameters(const_ReflectionsPtr); //so that constructor can be called explicity
    void store_arrays(const_ReflectionsPtr,bool,bool,bool=false); //so that constructor can be called explicity
    void apply_spacegroup_unitcell(const SpaceGroup&,const UnitCell&);

  private:
    void store_halfR_arrays(const_ReflectionsPtr,bool);
    void store_fullR_arrays(const_ReflectionsPtr);

  public:
    void
    calculate_reflection_parameters(
      const millnx,
      const double,
      const int);

    void
    flags(bool,bool,bool);

  public:
    af_string logEpsilon(std::string,bool) const;
};

} //phasertng
#endif
