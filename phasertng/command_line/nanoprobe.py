# LIBTBX_SET_DISPATCHER_NAME phasertng.nanoprobe

from iotbx.cli_parser import run_program
from phasertng.programs import nanoprobe

if (__name__ == '__main__'):
  try:
    run_program(nanoprobe.Program)
  except Exception as e:
    print(str(e))
