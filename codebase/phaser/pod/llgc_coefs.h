//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_LLGCOEFS_CLASS__
#define __PHASER_LLGCOEFS_CLASS__
#include <phaser/main/Phaser.h>

namespace phaser {

class llgc_coefs
{
  public:
  llgc_coefs() {}
  llgc_coefs(std::string a,af_float b,af_float c) : ATOMTYPE(a),FLLG(b),PHLLG(c) {}
  std::string ATOMTYPE;
  af_float    FLLG,PHLLG;
};

}

#endif
