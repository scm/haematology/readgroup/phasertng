//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Suite.h>
#include <phasertng/main/hoist.h>

namespace phasertng {

  Suite::Suite(const std::string suite_) : suite(suite_)
  {
    set_defaults_for_suite();
  }

  void Suite::set_defaults_for_suite()
  {
    if (suite == "CCP4")
    {
      SUITE.loggraphs = true;
      SUITE.write_phil = false;
      SUITE.write_cards = true;
      SUITE.write_data = true;
      SUITE.write_density = false;
      SUITE.write_files = true;
      SUITE.write_log = true;
      SUITE.write_xml = true;
      SUITE.kill_time = 10; //same as default
      SUITE.database = "";
      SUITE.kill_file = "";
      SUITE.level = out::LOGFILE;
      SUITE.store = out::LOGFILE;
    }
    else if (suite == "PHENIX")
    {
      SUITE.loggraphs = false;
      SUITE.write_phil = false;
      SUITE.write_cards = true;
      SUITE.write_data = true;
      SUITE.write_density = false;
      SUITE.write_files = true;
      SUITE.write_log = true;
      SUITE.write_xml = true;
      SUITE.kill_time = 10; //same as default
      SUITE.database = "";
      SUITE.kill_file = "";
      SUITE.level = out::LOGFILE;
      SUITE.store = out::LOGFILE;
    }
    else if (suite == "VALIDATE")
    {
      SUITE.loggraphs = false;
      SUITE.write_phil = false;
      SUITE.write_cards = false;
      SUITE.write_data = false;
      SUITE.write_density = false;
      SUITE.write_files = false;
      SUITE.write_log = false;
      SUITE.write_xml = false;
      SUITE.kill_time = 10; //same as default
      SUITE.database = "";
      SUITE.kill_file = "";
      SUITE.level = out::LOGFILE;
      SUITE.store = out::LOGFILE;
    }
    else if (suite == "VOYAGER" || suite == "VERBOSE")
    {
      SUITE.loggraphs = false;
      SUITE.write_cards = true;
      SUITE.write_data = true;
      SUITE.write_density = false;
      SUITE.write_files = true;
      SUITE.write_log = true;
      SUITE.write_xml = true;
      SUITE.write_phil = false;
      SUITE.kill_time = 10;
      SUITE.database = "";
      SUITE.kill_file = "";
      SUITE.level = (suite == "VERBOSE") ? out::VERBOSE : out::PROCESS;
      SUITE.store = out::VERBOSE;
    }
    else { throw "unknown suite choice"; }
  }

  std::string Suite::Enum2String(out::stream code)
  { return out::stream2String(code); }

  out::stream Suite::String2Enum(std::string code)
  {
    hoist::to_upper(code);
    return out::stream2Enum(code);
  }

  void Suite::SetLevel(const std::string card)
  { SUITE.level = String2Enum(card); }
  void Suite::SetStore(const std::string card)
  { SUITE.store = String2Enum(card); }
  void Suite::SetWritePhil(const bool card)
  { SUITE.write_phil = card; }
  void Suite::SetWriteCards(const bool card)
  { SUITE.write_cards = card; }
  void Suite::SetWriteFiles(const bool card)
  { SUITE.write_files = card; }
  void Suite::SetWriteData(const bool card)
  { SUITE.write_data = card; }
  void Suite::SetWriteDensity(const bool card)
  { SUITE.write_density = card; }
  void Suite::SetWriteLog(const bool card)
  { SUITE.write_log = card; }
  void Suite::SetWriteXml(const bool card)
  { SUITE.write_xml = card; }
  void Suite::SetKillFile(const std::string card)
  { SUITE.kill_file = card; }
  void Suite::SetKillTime(const double card)
  { SUITE.kill_time = card; }
  void Suite::SetLoggraphs(const bool card)
  { SUITE.loggraphs = card; }
  void Suite::SetDataBase(const std::string card)
  { SUITE.database = card; }

  void Suite::check_store_level()
  {
    if (SUITE.store < SUITE.level)
      SUITE.store = SUITE.level;
  }

  bool Suite::Write() const
  {
    return
      SUITE.write_phil or
      SUITE.write_cards or
      SUITE.write_data or
      SUITE.write_density or
      SUITE.write_files or
      SUITE.write_log or
      SUITE.write_xml;
  }
  //setting mute has to be done with input cards, since everything is set on input anyway

  void Suite::SetSilent()
  {
      SUITE.write_phil =
      SUITE.write_cards =
      SUITE.write_data =
      SUITE.write_density =
      SUITE.write_files =
      SUITE.write_log =
      SUITE.write_xml = false;
  }
}
