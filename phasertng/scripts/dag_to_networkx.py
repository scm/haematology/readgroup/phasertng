from io import open #this covers the python 2 and python 3 encoding

import sys, os, os.path
import libtbx.phil
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
from phasertng.phil.master_node_file import *
from phasertng.phil.converter_registry import *
import networkx as nx
import matplotlib.pyplot as plt
import argparse

def dag_to_working_params(file_path):
  master_phil = parse(build_node_str(),converter_registry=converter_registry)
  all_working_params = []
  with open(file_path, 'r',encoding="utf8", errors='ignore') as f:
    f_content = f.read()
    #the splitter is the single line brace at the end of each phaserdag block
    #the end of the file is "\n}"
    splitter = "\n}\n"
    f_nodes = f_content.split(splitter)
    for f_node in f_nodes:
      if len(f_node) > 10: # hack removes the splitter and final splitter elements
        f_node = f_node + splitter #need to put the splitter back for parsing
      # for line in f_node.split("\n"):
      #   print(line)
      # print(node_separator)
        user_phil = parse(f_node,converter_registry=converter_registry)
        working_phil = master_phil.fetch(source=user_phil).extract() #NOT diff
        all_working_params.append(working_phil)
  return all_working_params

def tracker_to_networkx(walk_dir,io_pyvis):
  master_phil = parse(build_node_str(),converter_registry=converter_registry)
  TG = nx.Graph()
  TG.clear()
  labeldict = {}
  print('Checking Path: ' + os.path.abspath(walk_dir))
  for (dirpath, dirnames, filenames) in os.walk(walk_dir):
    for name in filenames:
      file_path = os.path.join(dirpath, name)
      if (file_path.endswith(".dag.phil")):
        print('Checking File: ' + os.path.abspath(file_path))
        all_working_params = dag_to_working_params(file_path)
      # print(len(all_working_params))
        for working_phil in all_working_params:
          uid = str(working_phil.phaserdag.tracker.ultimate.identifier)
          pid = str(working_phil.phaserdag.tracker.penultimate.identifier)
          TG.add_node(uid)
          labeldict[uid] = str(working_phil.phaserdag.tracker.ultimate.tag)
          #print(uid,pid)
          if pid != "0":
            labeldict[pid] = str(working_phil.phaserdag.tracker.penultimate.tag)
            TG.add_node(pid)
            TG.add_edge(uid,pid)
          else:
            labeldict[pid] = "root" # this is for uid=0
            TG.add_node(pid)
            TG.add_edge(uid,pid)
        #   print("Root of Dag",uid,labeldict[uid])
  filename = None
  if True:
  # nx.draw_spectral(TG,labels=labeldict,with_labels=True)
    plt.clf()
    nx.draw(TG,labels=labeldict,with_labels=True)
  # ax.margins(0.1)
  # plt.show() # interactive
    filename = os.path.join(walk_dir, "dag.png")
    print(filename)
    plt.savefig(filename)
  jname = None
  if io_pyvis:
    jname = os.path.join(walk_dir, "dag.html")
    from pyvis.network import Network
    nt = Network('500px', '500px')
    nt.from_nx(TG)
    nt.show(jname)
  return filename,jname

def pathway_tracker_to_networkx(walk_dir,io_pyvis):
  master_phil = parse(build_node_str(),converter_registry=converter_registry)
  PTG = nx.Graph()
  PTG.clear()
  labeldict = {}
  colordict = {}
  print('Checking Path: ' + os.path.abspath(walk_dir))
  for (dirpath, dirnames, filenames) in os.walk(walk_dir):
    for name in filenames:
      file_path = os.path.join(dirpath, name)
      if (file_path.endswith(".dag.phil")):
        print('Checking File: ' + os.path.abspath(file_path))
        all_working_params = dag_to_working_params(file_path)
      # print(len(all_working_params))
        for working_phil in all_working_params:
          pid = str(working_phil.phaserdag.pathway.ultimate.identifier)
          qid = str(working_phil.phaserdag.pathway.penultimate.identifier)
          uid = str(working_phil.phaserdag.tracker.ultimate.identifier)
          PTG.add_node(uid)
          PTG.add_node(qid)
          PTG.add_node(pid)
          labeldict[pid] = str(working_phil.phaserdag.pathway.ultimate.tag)
          labeldict[qid] = str(working_phil.phaserdag.pathway.penultimate.tag)
          labeldict[uid] = str(working_phil.phaserdag.tracker.ultimate.tag)
          colordict[pid] = "blue"
          colordict[qid] = "blue"
          colordict[uid] = "green"
          #print(uid,pid)
          PTG.add_edge(uid,pid)
          PTG.add_edge(qid,pid)
        #   print("Root of Dag",uid,labeldict[uid])
  if True:
  # nx.draw_spectral(TG,labels=labeldict,with_labels=True)
    plt.clf()
    color_map = []
    for node in PTG:
      color_map.append(colordict[node])
    nx.draw(PTG,labels=labeldict,with_labels=True,node_color=color_map)
  # ax.margins(0.1)
  # plt.show() # interactive
    filename = os.path.join(walk_dir, "tracker_dag.png")
    print(filename)
    plt.savefig(filename)
  jname = None
  if io_pyvis:
    jname = os.path.join(walk_dir, "tracker_dag.html")
    from pyvis.network import Network
    nt = Network('500px', '500px')
    nt.from_nx(PTG)
    nt.show(jname)
  return filename,jname


def pathway_to_networkx(walk_dir,io_pyvis):
  master_phil = parse(build_node_str(),converter_registry=converter_registry)
  PG = nx.Graph()
  PG.clear()
  labeldict = {}
  print('Checking Path: ' + os.path.abspath(walk_dir))
  for (dirpath, dirnames, filenames) in os.walk(walk_dir):
    for name in filenames:
      file_path = os.path.join(dirpath, name)
      if (file_path.endswith(".dag.phil")):
        print('Checking File: ' + os.path.abspath(file_path))
        with open(file_path, 'r',encoding="utf8", errors='ignore') as f:
          f_content = f.read()
          #only need to process the first one per dag.phil because all the same
          user_phil = parse(f_content,converter_registry=converter_registry)
          working_phil = master_phil.fetch(source=user_phil).extract() #NOT diff
          uid = str(working_phil.phaserdag.pathway.ultimate.identifier)
          pid = str(working_phil.phaserdag.pathway.penultimate.identifier)
          PG.add_node(uid)
          labeldict[uid] = str(working_phil.phaserdag.pathway.ultimate.tag)
          #print(uid,pid)
          if pid != "0":
            labeldict[pid] = str(working_phil.phaserdag.pathway.penultimate.tag)
            PG.add_node(pid)
            PG.add_edge(uid,pid)
          else:
            labeldict[pid] = "root" # this is for uid=0
            PG.add_node(pid)
            PG.add_edge(uid,pid)
        #   print("Root of Dag",uid,labeldict[uid])
  filename = None
  if True:
    plt.clf()
    nx.draw_spectral(PG,labels=labeldict,with_labels=True)
    filename = os.path.join(walk_dir, "pathway.png")
    print(filename)
    plt.savefig(filename)
  jname = None
  if io_pyvis:
    jname = os.path.join(walk_dir, "pathway.html")
    from pyvis.network import Network
    nt = Network('500px', '500px')
    nt.from_nx(PG)
    nt.show(jname)
  return filename,jname

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="directory path walked for dag files",required=True)
  parser.add_argument('-cs',"--pyvis",help="write pyvis files",type=bool,required=False,default=True)
  args = parser.parse_args()
  f1,j1 = tracker_to_networkx(walk_dir=args.directory,io_pyvis=args.pyvis)
  f2,j2 = pathway_to_networkx(walk_dir=args.directory,io_pyvis=args.pyvis)
  f3,j3 = pathway_tracker_to_networkx(walk_dir=args.directory,io_pyvis=args.pyvis)
  print("Results files")
  print(f1,j1)
  print(f2,j2)
  print(f3,j3)
