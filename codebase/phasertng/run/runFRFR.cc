//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/AnglesStored.h>
#include <phasertng/site/AnglesRandom.h>
#include <phasertng/site/FastRotationFunction.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/data/Selected.h>
#include <phasertng/math/mean_sigma_top.h>
#include <phasertng/pod/parent_mean_sigma_top.h>

namespace phasertng {

  //Rotation_function_zscore_equivalent
  void Phasertng::runFRFR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_turn())
    throw Error(err::INPUT,"Dag not prepared with correct mode (frf failed?)");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete, skipping");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    if (Suite::Extra())
    {
    out::stream where = out::TESTING;
    for (int i = 0; i < DAGDB.size(); i++)
    {
      bool changed = load_reflid_for_node(DAGDB.NODES[i].REFLID,where);
      if (i == 0 or changed)
      {
        logUnderLine(where,"Seek Information" + nNtos(i+1,DAGDB.size()));
        logTabArray(where,DAGDB.seek_info_array(i));
        logTab(where,"reflid = " + REFLECTIONS->REFLID.str() + " changed = " + btos(changed));
        double ztotalscat = REFLECTIONS->get_ztotalscat();
        logTab(where,"Z = " + dtos(REFLECTIONS->Z));
        logTab(where,"total scattering = " + dtos(REFLECTIONS->TOTAL_SCATTERING));
        logTab(where,"Z total scattering = " + dtos(ztotalscat));
        logTab(where,"tNCS = " + itos(REFLECTIONS->TNCS_ORDER));
        logTab(where,"nseek = " + itos(DAGDB.WORK->SEEK.size()));
        //scattering seek includes nmol
        logTab(where,"total scattering seek = " + dtos(DAGDB.WORK->scattering_seek()));
        double fs(DAGDB.WORK->fraction_scattering_seek(ztotalscat));
        logTab(where,"fs = " + dtos(fs));
        if (fs > 1) //check repeated below if not in testing mode
          throw Error(err::INPUT,"Fraction scattering for total seek is greater than 1");
      }
    }
    }

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    bool use_average_random(true);
    if (DAGDB.NODES[0].TNCS_INDICATED == 'Y' and
        DAGDB.NODES[0].TWINNED and
        DAGDB.NODES.multiple_spacegroups()) //very tricky cases, space group problens
    { //5bug
      use_average_random = false;
    }
    if (DAGDB.NODES.multiple_vrms_values()) //because the llg of the next will be very different because of siga
    { //7rpq
      use_average_random = false;
    }
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    { //if the frf come from different maps,
      // then we cannot share the backgrounds (eg no background) and have one random set overall
      if (DAGDB.NODES[0].REFLID != DAGDB.NODES[i].REFLID)
      {  use_average_random = false; break; }
    }
    //3it5 where adding the next component causes V<0 because bfactors are too high from previous refinement
    bool adjusted(false);
    for (auto& node : DAGDB.NODES)
    {
      node.reset_parameters(REFLECTIONS->get_ztotalscat());
      bool thisadjusted = node.initKnownMR(REFLECTIONS->data_hires(),DEF_BFAC_MAX,DEF_BFAC_MIN);
      adjusted = adjusted or thisadjusted;
    }
    if (adjusted)
      logAdvisory("Bfactors of poses adjusted for next component");

    double io_percent = input.value__percent.at(".phasertng.fast_rotation_function.percent.").value_or_default();
    double io_buffer = input.value__percent.at(".phasertng.rescore_rotation_function.frf_percent_increase.").value_or_default();
    bool io_top_not_in_pose = input.value__boolean.at(".phasertng.fast_rotation_function.top_not_in_pose.").value_or_default();
    if (io_percent+io_buffer < 100)
      io_percent += io_buffer;
    if (io_buffer/io_percent > 0.1)
      logAdvisory("Buffer percentage is a large fraction of percent cutoff, more peaks may be retained than required");
    int io_maxstored = input.value__int_number.at(".phasertng.rescore_rotation_function.maximum_stored.").value_or_default();
    double io_nprint = input.value__int_number.at(".phasertng.rescore_rotation_function.maximum_printed.").value_or_default();
    unsigned random_seed = 0;

    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    double cluster_angle(0);
    bool io_use_cluster = input.value__boolean.at(".phasertng.rescore_rotation_function.cluster.").value_or_default();
    bool io_coiled_coil = input.value__boolean.at(".phasertng.coiled_coil.").is_default() ? false : input.value__boolean.at(".phasertng.coiled_coil.").value_or_default(); //default None
    {{
    std::pair<int,char> highsymm = REFLECTIONS->SG.highOrderAxis();
    //cluster angle will be smaller with coiled coil but we are going to turn off anyway
    std::tie(cluster_angle,std::ignore) = DAGDB.WORK->NEXT.ENTRY->clustang(
        selected.STICKY_HIRES,
        input.value__flt_number.at(".phasertng.fast_rotation_function.sampling.").value_or_default(),
        io_coiled_coil,
        false, //duplicates only already removed in frf
        input.value__flt_number.at(".phasertng.fast_rotation_function.helix_factor.").value_or_default(),
        highsymm.first
      );
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Clustering");
    double io_helix_factor = input.value__flt_number.at(".phasertng.fast_rotation_function.helix_factor.").value_or_default();
    if (io_helix_factor != 1)
    {
      logTab(where,"All-helical (helix factor gridding): " + btos(DAGDB.WORK->NEXT.ENTRY->HELIX));
      logTab(where,"Coiled-coil (helix factor gridding): " + btos(io_coiled_coil));
      logTab(where,"Helix factor: " + dtos(io_helix_factor));
    }
    logTab(where,"Cluster Angle:  " + dtos(cluster_angle,5,2) + " degrees");
    }}

    double best_llg = std::numeric_limits<double>::lowest();
    dvect3 best_site;
    pod::parent_mean_sigma_top stats; //id/mean/sd/top
    auto set_of_parents = DAGDB.set_of_parents(); //a vector, in order of sort frf
    int nparents = DAGDB.NODES.number_of_parents();
    int nrand  = input.value__int_number.at(".phasertng.rescore_rotation_function.nrand.").value_or_default();
    bool calculate_rfz = input.value__boolean.at(".phasertng.rescore_rotation_function.calculate_zscore.").value_or_default();
    {{
    logUnderLine(out::LOGFILE,"Statistics");
    if (use_average_random)
    {
      if (nparents > nrand)
        nparents = nrand;
      nrand = std::ceil(nrand/nparents)*nparents;
    }
    AnglesRandom rotList(nrand,random_seed++);
    if (calculate_rfz)
      logTab(out::LOGFILE,"Random orientations (" + itos(nrand) + ")");
    else
      logTab(out::LOGFILE,"No zscore statistics calculated");
    int nrot = rotList.count_sites(); //batch the rotations
   //if (calculate_rfz and use_average_random) //stats used for pruning by percent not jus rfz
    if (use_average_random)
    {
      PHASER_ASSERT(nrot%nparents == 0);
      nrot = nrot/nparents;
      logTab(out::LOGFILE,"Random orientations in batches of " + itos(nrot));
    }
    sv_double allv;

    int nnp1(0),nnp2(0);
    int n(1),N(nparents);
    DAGDB.restart();
    out::stream progress = out::LOGFILE;
    logProgressBarStart(progress,"Calculating statistics for parents",nparents,1,false);
    while (!DAGDB.at_end())
    {
      DAGDB.WORK->generic_flag = false;
      bool new_parent = !stats.mst.count(DAGDB.WORK->PARENTS.back());
      bool over_nrand = use_average_random and allv.size() == nrand;
      if (new_parent and over_nrand)
      {
        out::stream where = (nnp1++ < io_nprint) ? out::VERBOSE : out::TESTING;
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Rotation Function Statistics for Parent" + nN);
        logTab(where,"No calculation for this parent");
        stats.mst[DAGDB.WORK->PARENTS.back()] = dvect3(0,0,0); //just create map index
      }
      else if (new_parent)
      {
        std::string nN = nNtos(n++,N);
        out::stream where = (nnp2++ < io_nprint) ? out::VERBOSE : out::TESTING;
        logUnderLine(where,"Rotation Function Statistics for Parent" + nN);
        bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (changed or diffsg)
        {
          if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str());
          double ztotalscat = REFLECTIONS->get_ztotalscat();
          if (DAGDB.WORK->fraction_scattering_seek(ztotalscat) > 1)
            throw Error(err::INPUT,"Fraction scattering for total seek is greater than 1");
          double resolution = DAGDB.WORK->search_resolution(io_hires);
          auto seltxt = selected.set_resolution(
              REFLECTIONS->data_hires(),
              DAGDB.resolution_range(),
              resolution);
          if (changed) logTabArray(where,seltxt);
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        }
        DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
        auto& turn = DAGDB.WORK->NEXT.IDENTIFIER;
        logTab(where,"Drms shift = " + dtos(DAGDB.WORK->DRMS_SHIFT[turn].VAL));
        logTab(where,"Vrms value = " + dtos(DAGDB.WORK->VRMS_VALUE[turn].VAL));
        //prevents repeat rescoring of this one at the original turn position
        DAGDB.WORK->generic_flag = true; //has been rescored
        auto original_turn = DAGDB.WORK->NEXT;
        //calculate background for speed
        pod::FastPoseType ecalcs_sigmaa_pose;
        ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS);
        double original_llg;
        std::tie(original_llg) = DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        logProgressBarStart(where,"Scoring random orientations",nrot,2,false);
        sv_double v;
        if (!use_average_random) rotList.restart();
        int count(0); //counts zero to nrot, since rotList may not be starting at 0
        while (!rotList.at_end())
        {
          DAGDB.WORK->NEXT.EULER = rotList.next_site();
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
          //all the same space group
          Site newsite(DAGDB.WORK->NEXT.EULER,DAGDB.WORK->LLG);
          //could store in the dag here, but we select first, then append at the end
          if (use_average_random)
            allv.push_back(DAGDB.WORK->LLG);
          //add the values to v anyway, for additional stats
          v.push_back(DAGDB.WORK->LLG);
          if (DAGDB.WORK->LLG > best_llg)
          {
            best_llg = DAGDB.WORK->LLG;
            best_site = DAGDB.WORK->NEXT.EULER;
          }
          logProgressBarNext(where,2);
          count++;
          if (use_average_random)
            if (count == nrot) //end this batch of rotations
              break;
        }
        logProgressBarEnd(where,2);
        DAGDB.WORK->NEXT = original_turn;
        DAGDB.WORK->LLG = original_llg;
        dvect3 mst = math::mean_sigma_top(v);
        stats.mst[DAGDB.WORK->PARENTS.back()] = mst;
        logTab(where,"Random Mean (Sigma):  " + dtos(mst[0],10,2) + "   (" + dtos(mst[1],5,2) + ")");
        logTab(where,"Random Highest (RFZ): " + dtos(mst[2],10,2) + "   (" + dtos((mst[2]-mst[0])/mst[1],5,2) + ")");
        logBlank(where);
        if (Suite::Extra() and v.size() > 2)
        {
          logHistogram(out::TESTING,v,10,false,"RF random parent");
        }
        logProgressBarNext(progress,1);
      }
    } //dagdb
    logProgressBarEnd(progress,1);

    if (stats.size() > 1)
    {
      out::stream where = out::VERBOSE;
      stats.calculate_histogram_mean();
      logHistogram(where,stats.histdat,10,false,"RF means",stats.histmin,stats.histmax);
      stats.calculate_histogram_sigma();
      logHistogram(where,stats.histdat,10,false,"RF sigmas",stats.histmin,stats.histmax);
    }

    if (use_average_random)
    {
      dvect3 mst = math::mean_sigma_top(allv);
      stats.set_all(mst);
      out::stream where = out::LOGFILE;
      PHASER_ASSERT(mst[1] > 0);
      logTab(where,"Average Random Mean (Sigma):  " + dtos(mst[0],10,2) + "   (" + dtos(mst[1],5,2) + ")");
      logTab(where,"Average Random Highest (RFZ): " + dtos(mst[2],10,2) + "   (" + dtos((mst[2]-mst[0])/mst[1],5,2) + ")");
      logBlank(where);
      logHistogram(out::VERBOSE,allv,10,false,"Average RF random");
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Rescoring Orientations");
    logProgressBarStart(where,"Rescoring orientations",DAGDB.size());
    int c(1),C(DAGDB.size()); //count of the solutions overall
    int n(1),N(set_of_parents.size());
    bool breakit(false);
    double best_score = std::numeric_limits<double>::lowest();
    for (auto parent : set_of_parents)
    {
      if (breakit) break;
      //group the turns by background so that the ecalcs_sigmaa_pose is done first time only
      pod::FastPoseType ecalcs_sigmaa_pose;
      std::string nN = nNtos(n,N);
      out::stream where2 = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
      if (!breakit)
        logChevron(where2,"Rotation Function Rescoring for Parent" + nN);
      DAGDB.restart(); //very important
      while (!DAGDB.at_end() and !breakit)
      {
        std::string cC = nNtos(c,C);
        if (DAGDB.WORK->PARENTS.back() != parent)
        {
          continue; //we sort in order of the parents
        }
        if (!DAGDB.WORK->generic_flag) //has not yet been rescored
        {
          bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where2);
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
          if (changed or diffsg)
          {
            if (changed) logTab(where2,"Reflections changed to " + DAGDB.WORK->REFLID.str());
            double resolution = DAGDB.WORK->search_resolution(io_hires);
            auto seltxt = selected.set_resolution(
                REFLECTIONS->data_hires(),
                DAGDB.resolution_range(),
                resolution);
            if (changed) logTabArray(where2,seltxt);
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
            ecalcs_sigmaa_pose.precalculated = false;
          }
          if (!ecalcs_sigmaa_pose.precalculated)
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS);
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose); //if not the first parent node, then this will be precalculated
          DAGDB.WORK->FSS = false;
        }
        if (calculate_rfz)
          DAGDB.WORK->ZSCORE = stats.zscore(parent,DAGDB.WORK->LLG);
        if (DAGDB.WORK->LLG > best_score)
        {
          best_score = DAGDB.WORK->LLG;
          logProgressBarAgain(where,"New Best LLG=" + dtos(best_score,2)+cC);
        }
        c++;
        if (!logProgressBarNext(where)) break;
      }
      n++;
    }
    logProgressBarEnd(where);
    }}

    // bool no_signal(false);
    //don't make decision on purging here
    //if (random top > top) but (top > lowest_mean) we can still purge
    //if (top < lowest_mean) then we have to get desperate
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Merged Results");
    if (stats.max_top() > DAGDB.NODES.top_llg())
    {
      logTab(where,"Maximum Random  LLG = " + dtos(stats.max_top(),2));
      logTab(where,"Maximum Rescore LLG = " + dtos(DAGDB.NODES.top_llg(),2));
      logTab(where,"Random Best Angle = (" + dvtos(best_site,6,1) + ")");
      logWarning("Best llg from random orientation is higher than search llg");
     // no_signal = true;
    }
    }}

    //if there is clustering we have to treat each background separately
    //group on parent, cluster, purge cluster, and make a new NODES
    //if no clustering, then the purge percent can be applied independent of background
    if (io_use_cluster)
    {
      int before = DAGDB.size();
      logEllipsisOpen(out::LOGFILE,"Clustering");
      dag::NodeList output_dag;
      int n(1),N(stats.size());
      for (auto parent : set_of_parents)
      {
        //this will allow the tncs vectors to be tested for all symmetry orientations; no symmetry clustering
        bool cluster_in_p1(nmol > 1);
        FastRotationFunction rotfun(REFLECTIONS.get(),cluster_in_p1);
        rotfun.set_nthreads(NTHREADS,USE_STRICTLY_NTHREADS);
        {{
        std::string nN = nNtos(n++,N);
        out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
        logUnderLine(where,"Clustering for Parent" + nN);
        for (int i = 0; i < DAGDB.NODES.size(); i++)
        {
          auto& node = DAGDB.NODES[i]; //because not using the iterator
          if (parent == node.PARENTS.back())
          {
            auto& node = DAGDB.NODES[i]; //because not using the iterator
            Site newsite(node.NEXT.EULER,node.LLG,i);
            rotfun.rotation_function_data.push_back(newsite);
          }
        }
        }}

        {{
        out::stream where = out::VERBOSE;
        bool AllSites(true); //not just the clustered top sites
        logTableTop(where,"Fast Rotation Function Rescored (FSS sort order before clustering)",false);
        logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
        logTab(where,"Percent cutoff: " + dtos(io_percent,0) + "%");
        dmat33 PR = DAGDB.NODES[0].NEXT.ENTRY->PRINCIPAL_ORIENTATION;
        logTabArray(where,rotfun.logTable(io_nprint,AllSites,PR));
        logTableEnd(where);
        }}

        {{
        out::stream where = out::VERBOSE;
        for (int i = 0; i < DAGDB.NODES.size(); i++)
          DAGDB.NODES[i].generic_int = i+1;
        logEllipsisOpen(where,"Sorting on LLG");
        rotfun.full_sort(); //full sort on new value, no erase
        logEllipsisShut(where);
        }}

        if (Suite::Extra())
        {
          dmat33 PR = DAGDB.NODES[0].NEXT.ENTRY->PRINCIPAL_ORIENTATION;
          logTabArray(out::TESTING,rotfun.logTable(io_nprint,true,PR));
        }

        {{
        out::stream where = out::VERBOSE;
        logEllipsisOpen(where,"Cluster");
        int cluster_back = input.value__int_number.at(".phasertng.rescore_rotation_function.cluster_back.").value_or_default();
        rotfun.cluster(cluster_angle,cluster_back);
        logEllipsisShut(where);
        }}

        {{
        out::stream where = out::VERBOSE;
        logEllipsisOpen(where,"Purge by cluster");
        int before = rotfun.count_sites();
        rotfun.select_topsites_and_erase();
        logEllipsisShut(where);
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(rotfun.count_sites()));
        }}

        {{
        out::stream where = out::VERBOSE;
        bool AllSites(false);
        logTableTop(where,"Fast Rotation Function Rescored (LLG sort order after clustering)",false);
        logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
        dmat33 PR = DAGDB.NODES[0].NEXT.ENTRY->PRINCIPAL_ORIENTATION;
        logTabArray(where,rotfun.logTable(io_nprint,AllSites,PR));
        logTableEnd(where);
        }}

        {{
        out::stream where = out::VERBOSE;
        logEllipsisOpen(where,"Append to DAG");
        //add the growing list to the DAG, note that this will not be sorted overall
        for (auto& item : rotfun.rotation_function_data)
        {
          //no selection for top peaks, recore them all
          output_dag.push_back(DAGDB.NODES[item.index]); //pointer to last in list
        }
        logEllipsisShut(where);
        }}
      }
      DAGDB.NODES = output_dag;
      DAGDB.reset_entry_pointers(DataBase());
      logEllipsisShut(out::LOGFILE);
      logTab(out::LOGFILE,"Number before purge: " + itos(before));
      logTab(out::LOGFILE,"Number after purge:  " + itos(DAGDB.size()));
    }
    else
    {
      out::stream where = out::LOGFILE;
      logBlank(where);
      logTab(where,"No clustering");
      logBlank(where);
    }

    std::sort(set_of_parents.begin(),set_of_parents.end());
    //at this point all the parents are present, pruning is of clustered peaks only, iff it is done
    //may have broken out of rescoring loop, but the first one will be present regardless
    bool paranoia_first_present(false);
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      DAGDB.NODES[i].NUM = DAGDB.NODES[i].PARENTS.back()-set_of_parents.front()+1;
      if (DAGDB.NODES[i].NUM == 1) paranoia_first_present = true;
      //distance from the first one
    }
    PHASER_ASSERT(paranoia_first_present);

    //top rf llg is top llg with turn not in poses, ie novel
    double top_novel = io_top_not_in_pose ? DAGDB.NODES.top_novel_turn_llg_packs().second : DAGDB.NODES.top_llg_packs().second;
    double lowest_mean = stats.merged_lowest_mean();
    bool no_signal = (top_novel <= lowest_mean);
    int min_nodes_for_average = 10;
    //if (top < lowest_mean) then we have to get desperate
    //take the average llg provided there are enough points for the average
    //we can cope with just keeping a few without purging, it is the 1000's we are worried about
    std::string toppacksstr = DAGDB.NODES.top_llg_packs().first ? dtos(DAGDB.NODES.top_llg_packs().second,2) : "---";
    std::string topnovelstr = DAGDB.NODES.top_novel_turn_llg_packs().first ? dtos(DAGDB.NODES.top_novel_turn_llg_packs().second,2) : "---";
    logTab(out::LOGFILE,"Top/Top-Packs/Top-Packs-Novel: " + dtos(DAGDB.NODES.top_llg(),2) + "/" +  toppacksstr + "/" + topnovelstr);
    if (no_signal)
      logWarning("Best llg from search is lower than average llg from random orientations");
    if (no_signal and DAGDB.NODES.top_llg() > lowest_mean)
    {
      top_novel = DAGDB.NODES.top_llg();
      logAdvisory("Top packing llg for purge is lower than top random llg values");
      no_signal = false;
    }
    else if (no_signal and DAGDB.size() > min_nodes_for_average)
    {
      //this is a hack
      //there is no signal so we don't want to leave all the peaks as they are all rubbish
      //just pick a value
      lowest_mean = DAGDB.NODES.average_llg();
      logAdvisory("Mean llg for purge is average or scored llg values");
      no_signal = false;
    }
    if (!no_signal)
    {
      out::stream where = out::LOGFILE;
      int before = DAGDB.NODES.size();
      double top = DAGDB.NODES.top_llg_packs().second;
      if (top == top_novel and io_top_not_in_pose)
      {
        logTab(where,"Top orientation is not in pose list");
        logTab(where,"Purge by percent with respect to top orientation llg");
      }
      else if (io_top_not_in_pose)
      {
        logTab(where,"Top orientation is in pose list");
        logTab(where,"Purge by percent with respect to top novel orientation llg");
        logTab(where,"top overall = " + dtos(top,5,2) + " ("  + dtos(100*(top-lowest_mean)/(top_novel-lowest_mean),5,2) + "%)");
        logTab(where,"top novel   = " + dtos(top_novel,5,2) + " (100%)");
        PHASER_ASSERT(top_novel < top);
        PHASER_ASSERT(top > lowest_mean);
        PHASER_ASSERT(top_novel > lowest_mean);
      }
      logTab(where,"Top/Low-Mean: " + dtos(top_novel,2) + "/" + dtos(lowest_mean,2));

      //collect the nodes by space group and treat each one differently as we have not found sg yet 1dps
      std::map<std::string,dag::NodeList> sgnodes;
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        sgnodes[DAGDB.NODES[i].SG->sstr()] = dag::NodeList();
      double purge_by_percent_by_space_group = true; //DAGDB.NODES[0].TNCS_INDICATED == 'Y';
      if (purge_by_percent_by_space_group and
          sgnodes.size() > 1)
      {
        logEllipsisOpen(where,"Purge by percent by space group");
        logTab(where,"Percent: "  + dtos(io_percent,4,1) + "%");
        for (int i = 0; i < DAGDB.NODES.size(); i++)
        {
          auto sg = DAGDB.NODES[i].SG->sstr();
          sgnodes[sg].push_back(DAGDB.NODES[i]);
        }
        dag::NodeList output_dag;
        for (auto& sgnode : sgnodes)
        {
          logChevron(where,"Space Group: "  + sgnode.first);
          int beforesg = sgnode.second.size();
          double sg_top_novel = top_novel;
          sg_top_novel =  io_top_not_in_pose ? sgnode.second.top_novel_turn_llg_packs().second : sgnode.second.top_llg_packs().second;
          if (sg_top_novel <= lowest_mean)
          {
            sg_top_novel =  sgnode.second.top_llg();
            logTab(where,"Use top not top-packing");
          }
          if (sg_top_novel <= lowest_mean)
          {
            logTab(where,"Mean above highest");
            continue;//because some space groups may be randomly lower
          }
          double cut = sgnode.second.apply_partial_sort_and_percent_llg(io_percent,sg_top_novel,lowest_mean);
          logTab(where,"Top/Cutoff LLG:      " + dtos(sg_top_novel,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
          logTab(where,"Number before purge: " + itos(beforesg));
          logTab(where,"Number after purge:  " + itos(sgnode.second.size()));
          for (int i = 0; i < sgnode.second.size(); i++)
            output_dag.push_back(sgnode.second[i]);
        }
        logEllipsisShut(where);
        DAGDB.NODES = output_dag;
      }
      else
      {
        logEllipsisOpen(where,"Purge by percent");
        double cut = DAGDB.NODES.apply_partial_sort_and_percent_llg(io_percent,top_novel,lowest_mean);
        logEllipsisShut(where);
        logTab(where,"Percent:             "  + dtos(io_percent,4,1) + "%");
        logTab(where,"Top/Lo-Mean/Cutoff LLG: " + dtos(top_novel,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
      }
    }
   // else
   // {
   //   logTab(out::LOGFILE,"No signal in search, all orientations retained");
   // }

    {{
    out::stream where = out::LOGFILE;
    int before = DAGDB.NODES.size();
    if (before > io_maxstored)
    {
      logEllipsisOpen(where,"Purge merged by maximum number on LLG");
      DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      (io_maxstored == 0) ?
         logTab(where,"Maximum stored: all"):
         logTab(where,"Maximum stored:      " + itos(io_maxstored));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
    }
    else if (false)
    { //we do not want to sort overall because that will happen in gyre, this is the order in the ffs
      logEllipsisOpen(where,"Sort merged on LLG");
      DAGDB.NODES.apply_sort_llg();
      logEllipsisShut(where);
      logTab(where,"Number of nodes:  " + itos(DAGDB.NODES.size()));
    }
    }}

    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    double lowest_mean = stats.merged_lowest_mean();
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      double percent = 100.*(node->LLG-lowest_mean)/(top_novel-lowest_mean);
      double unstable100=101;
      std::string percentstr = (percent < 0) ? "<0" : std::string((percent > unstable100) ? "random" : dtoi(percent));
      node->ANNOTATION += " RFR=" + dtoi(node->LLG)+"/"+percentstr+"%/"+dtos(std::floor(node->ZSCORE),0)+"z";
    }
    }}

    if (Suite::Store() >= out::VERBOSE) //save on the sort unless needed
    {
    out::stream where = out::VERBOSE;
    int io_nprint2(0);
    dag::NodeList output_dag = DAGDB.NODES; //store for replacement
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int,io_nprint2);
    DAGDB.NODES.apply_partial_sort_and_maximum_stored_llg(io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = itow(io_nprint2);
    logTableTop(where,"Fast Rotation Function Rescored Results");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"Rotations with respect to original coordinates");
    std::set<std::string> ensembles;
    for (auto& node : DAGDB.NODES)
      ensembles.insert(node.NEXT.IDENTIFIER.str());
    int j(1);
    for (auto ens : ensembles)
      logTab(where,"Model #" + itos(j++) + " " +  ens);
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using llg here
    int q = DAGDB.NODES.llg_width(false); //even if it is a map, we are using llg here
    if (DAGDB.size())
    {
      logTab(where,"#       Final rank of the peak");
      logTab(where,"##      Input rank of the peak");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s%c %-*s %2s %s %*s %7s %6s  %*s %*s",
          w,"#",' ',ww,"##","",
          dag::header_polar().c_str(),
          q,"LLG",
          "RFZ","FSS%",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      auto& turn = node.NEXT;
      logTab(where,snprintftos(
          "%-*i%c %-*i %2s %s % *.*f %7.2f %6.1f  %*s %*s",
          w,i,' ',ww,node.generic_int,
          "",
          dag::print_polar(turn.SHIFT_MATRIX).c_str(),
          q,p,node.LLG,
          node.ZSCORE,node.PERCENT,
          unique_search ? 0:uuid_w,unique_search ? "":turn.IDENTIFIER.string().c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()
          ));
      for (int isym = 0; isym < REFLECTIONS->SG.NSYMP; isym++)
      {
        dmat33 MROT = turn.SHIFT_MATRIX;
        dmat33 Rsym = REFLECTIONS->UC.orth_Rtr_frac(REFLECTIONS->SG.rotsym[isym])*MROT;
        dvect3 euler = scitbx::math::euler_angles::zyz_angles(Rsym);
        euler = regularize_angle(euler);
        logTab(out::VERBOSE,snprintftos(
            "%-*s%2d %s  RzRyRz(% 7.1f % 7.1f % 7.1f)",
            w-2+2+ww+1,"Sym#",isym+1, //-2 is hack for tab, reduce to line up in table
            dag::print_polar(Rsym).c_str(),
            euler[0],euler[1],euler[2]));
#ifdef PHASERTNG_DEBUG_EULER_FRACT
{ //new declarations
      dmat33 MROT = zyz_matrix(turn.EULER);
      dmat33 Rsym = REFLECTIONS->UC.orth_Rtr_frac(REFLECTIONS->SG.rotsym[isym])*MROT;
      dvect3 euler = scitbx::math::euler_angles::zyz_angles(Rsym);
      euler = regularize_angle(euler);
      logTab(where,snprintftos(
          "%12s EULER=[% 6.4f % 6.4f % 6.4f]",
          "",euler[0],euler[1],euler[2]));
}
#endif
      }
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    DAGDB.NODES = output_dag;
    }

    //all below is the faff to get the sg names sorted on the top llg
    std::set<std::string> sgs; //for unique test
    typedef std::pair<std::string,double> sgllg;
    std::vector<sgllg> sgnodes; //so that sort on top llg
    int maxgint(2); //for width only
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      auto sg = DAGDB.NODES[i].SG->sstr();
      maxgint = std::max(maxgint,DAGDB.NODES[i].generic_int);
      if (sgs.count(sg) == 0) //take the first
      {
        sgnodes.push_back({sg,DAGDB.NODES[i].LLG});
        sgs.insert(sg);
      }
    }
    std::sort(sgnodes.begin(), sgnodes.end(), [](const sgllg& a, const sgllg& b)
       { return a.second > b.second; }); //reverse sort
    for (auto sgnode : sgnodes)
    {
      std::string sgstr = sgnode.first;
      double lowest_mean = stats.merged_lowest_mean();
      int ws = itow(stats.size())+2;
      int w = itow(maxgint);
      out::stream where = out::SUMMARY;
      logTableTop(where,"Fast Rotation Function Rescored Summary: " + sgstr);
      logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
      DAGDB.restart(); //very important to call this to set WORK
      logTab(where,"Last placed: " + DAGDB.WORK->NEXT.IDENTIFIER.str());
      int z = DAGDB.size();
      logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
      logTab(where,"--------");
      int p = DAGDB.NODES.llg_precision(false); //even if it is a map, we are using llg here
      int q = DAGDB.NODES.llg_width(false); //even if it is a map, we are using llg here
      logTab(where,"#i      Rank of the input peak");
      logTab(where,"RF#     Background number of the search");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %-*s %*s %6s %7s %6s  %s %-13s %*s %*s",
          ws,"RF#",
          w,"#i",
          q,"LLG",
          "LLG%","RFZ","FSS%",
          dag::header_polar().c_str(),
          "Space Group",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
      int i(1);
      DAGDB.restart(); //very important to call this to set WORK
      while (!DAGDB.at_end())
      {
        dag::Node* node = DAGDB.WORK;
        if (node->SG->sstr() != sgstr) continue; //pick out this space group
        auto& turn = node->NEXT;
  /*
        dmat33 PR = turn.ENTRY->PRINCIPAL_ORIENTATION;
        dvect3 eulerin;
        std::tie(eulerin,std::ignore,std::ignore) =
            dag::print_euler_wrt_in_from_mt(turn.EULER,PR);
  */
        double percent = 100.*(node->LLG-lowest_mean)/(top_novel-lowest_mean);
        double unstable100=101;
        std::string percentstr = (percent < 0) ? "< 0.0" : std::string((percent > unstable100) ? "random" : dtos(percent,6,1));
        logTab(where,snprintftos(
            "%-*i %-*i % *.*f %6s %7.2f %6.1f  %s %-13s %*s %*s",
            ws,node->NUM,
            w,node->generic_int,
            q,p,node->LLG,
            percentstr.c_str(),
            node->ZSCORE,
            node->PERCENT,
            dag::print_polar(turn.SHIFT_MATRIX).c_str(),
            node->SG->sstr().c_str(),
            unique_search ? 0:uuid_w,unique_search ? "":turn.IDENTIFIER.string().c_str(),
            unique_search ? 0:uuid_w,unique_search ? "":node->REFLID.string().c_str()
            ));
        if (i == io_nprint)
        {
          int more = DAGDB.size()-io_nprint;
          if (DAGDB.size() > io_nprint)
            logTab(where,"--- etc " + itos(more) + " more" );
          break;
        }
        i++;
      }
      logTableEnd(where);
    }

    if (false)
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Original orientation");
    bool found(false);
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      auto line = dag::print_polar(DAGDB.NODES[i].NEXT.SHIFT_MATRIX);
      if (hoist::algorithm::ends_with(line,"[  0.0]"))
      {
        dmat33 MROT = zyz_matrix(DAGDB.NODES[i].NEXT.EULER);
        auto euler = DAGDB.NODES[i].NEXT.EULER;
        logTab(where,snprintftos(
            "%6d %s RzRyRz(% 7.1f % 7.1f % 7.1f)",
            i+1,
            line.c_str(),
            euler[0],euler[1],euler[2]));
        found = true;
      }
    }
    if (!found) logTab(where,"None");
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
