//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef phaser_pdb_chains_2chainid_class__
#define phaser_pdb_chains_2chainid_class__
#include <string>
#include <vector>
#include <iostream>

namespace phasertng {

class pdb_chains_2chainid
{
  private: //members
    std::vector<std::string> allowed_chains;

  public: //getters
  pdb_chains_2chainid() {}
  void pdb_chains_2chainid::generate()
  {
    std::vector<std::pair<char,char> > generate_chains;
    std::string char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    std::pair<char,char> chain;
    for (int i = 0; i < char_set.size(); i++)
    {
      chain = {' ',char_set.at(i)};
      generate_chains.push_back(chain);
    }
    //next intuitive set
    char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    //next intuitive set
    char_set="abcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    std::string char_set2="0123456789";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set2.size(); j++)
    {
      chain = { char_set.at(i) , char_set2.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    char_set2="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    char_set="0123456789";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set2.size(); j++)
    {
      chain = { char_set.at(i) , char_set2.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    char_set="0123456789";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    //desperation, nicer extra values first
    char_set="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&*+-<=>?@.substr(i,1)^~";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    //desperation
    char_set="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&'()*+,-./:;<=>?@.substr(i,1)^_`{|}~";
    for (int i = 0; i < char_set.size(); i++)
    for (int j = 0; j < char_set.size(); j++)
    {
      chain = { char_set.at(i) , char_set.at(j) };
      if (std::find(generate_chains.begin(),generate_chains.end(),chain) == generate_chains.end())
        generate_chains.push_back(chain);
    }
    for (auto item : generate_chains)
    {
      std::string chain = "  ";
      chain[0] = item.first;
      chain[1] = item.second;
      allowed_chains.push_back(chain);
    }
    for (auto item : allowed_chains)
      std::cout << "\"" << item << "\", " <<std::endl;
  }
};

} //phasertng
#endif
