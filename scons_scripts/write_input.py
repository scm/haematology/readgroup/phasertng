from __future__ import print_function
from __future__ import division

import libtbx.load_env
import iotbx.phil
import sys, os, os.path, shutil
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from libtbx.phil import is_plain_none
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'voyager'))
from master_phil_file import *
from converter_registry import *
from OrderedSet import *
from mode_generator import *
from phasertng.voyager.voyager_generator import *
#from voyager_generator import *

#print os.path.dirname(__file__)

class keyword_parse_stuff:
  def __init__(self, valuetype_):
    self.valuetype = valuetype_
    self.getter = []
  def replace_getter(self,getter_):
    self.getter = []
    self.getter.append(str(getter_))
  def append_getter(self,getter_):
    if len(getter_):
      self.getter.append(str(getter_))
  def __len__(self):
    return 0
  def __str__(self):
    return ".type=" + str(self.valuetype)

class keyword_wiki_stuff:
  def __init__(self, param, helptype, helpval, valmin,valmax,short_caption, caption, helpattr, expert_level,multiple):
    self.param = param
    self.helptype = helptype
    self.valmin = valmin
    self.valmax = valmax
    self.helpval = helpval
    self.helpattr = helpattr
    self.caption = caption
    self.short_caption = short_caption
    self.expert_level = expert_level
    self.multiple = multiple
  def __len__(self): # required for recursive depth of dictionary
    return 0
  def __str__(self):
    return str(self.param + " " + self.helptype +
              "\nhelp=" + self.helpattr +
              "\nshort_caption=" + self.short_caption +
              "\nexpert-level=" + self.expert_level + "\n")
  def __lt__(self, other):
    return self.param < other.param
  def keywords(self):
    cards = self.param
    if (self.helptype is not None):
      cards +=  " " + self.helptype
    return cards + "\n"
  def text(self):
    cards = self.short_caption if self.helpattr == "None" else self.helpattr
    if not len(cards):
      raise ValueError(self.param + " has no help")
    if (cards[-1] is not '.'): cards = cards + '.' #add full stop to the end
    cards = cards + '\n' #add full stop to the end
    cards += "" if self.helpval is None else "Default = " + self.helpval + "\n"
    cards += "" if self.valmin is None else "Minimum = " + self.valmin + "\n"
    cards += "" if self.valmax is None else "Maximum = " + self.valmax + "\n"
    cards += "" if self.multiple is None else "Multiple = " + str(self.multiple) + "\n"
    return cards

class recursion_strings:
  def __init__(self,ident = '',catstr=''):
    self.catstr = catstr
    self.ident = ident

  def clear(self):
    self.catstr = ""
    self.ident = ""

  def make_parse_string(self,dictionary,ident = ""):
    tab = '  '
    # find recursive depth of dictionary
    c = sum([len(i) for i in dictionary.values() ])
    if c > 0:
      #list required for python3
      sortedkeys = sorted(list(dictionary.keys()))
     #keylist = str(list(dictionary.keys())).replace(" ","").replace("\'","\"").replace("]","").replace("[","")
      keylist = str(sortedkeys).replace(" ","").replace("\'","\"").replace("]","").replace("[","")
      self.catstr += ident + "compulsoryKey(input_stream,{" + keylist+ "});\n"
      self.catstr += ident + "if (keyIsEol()) return false;\n" #so that all other if keyIs calls can be else if
    #for key, value in dictionary.iteritems(): #removed for python3
    for key, value in sorted(dictionary.items()):
      if c > 0:
        elseif = True
        if elseif:
          self.catstr += ident+'else if (keyIs(\"%s\")) {\n' %(key)
        else:
          self.catstr += ident+'if (keyIs(\"%s\")) {\n' %(key)
      if isinstance(value, dict):
        self.make_parse_string(value, ident+tab)
      else:
        for getter in value.getter:
          if len(getter):
            splitgetter = getter.split("\n")
            for g in splitgetter:
              self.catstr += (ident+'%s\n' %(g))
      if c > 0:
        self.catstr += ident+'}\n'

def mapvar(n,qparameter):
  return "value__" + n.lower().split(":")[-1] + "[" + qparameter + "]"

def mapvarat(n,qparameter):
  return "value__" + n.lower().split(":")[-1] + ".at(" + qparameter + ")"

def svec(param):
  return "if (keyIs(\"" + param.split(" ")[-1] + "\")) { "

def vecmapvar(n,qparameter):
  return "array__" + n.lower().split(":")[-1] + "[" + qparameter + "]"

def vecmapvarat(n,qparameter):
  return "array__" + n.lower().split(":")[-1] + ".at(" + qparameter + ")"

def rreplace(s, old, new, occurrence):
  li = s.rsplit(old, occurrence)
  return new.join(li)

def print_dict(dictionary, ident = ''):
    tab = '  '
    c = sum([len(i) for i in dictionary.values() ])
    for key, value in dictionary.iteritems():
       if c > 0:
         print(ident+'if (keyIs(\"%s\")) {' %(key))
       if isinstance(value, dict):
         print_dict(value, ident+tab)
       else:
         print(ident+'%s' %("value.getter"))
       if c > 0:
         print(ident+'}')

# Return the longest prefix of all list elements.
def commonprefix(m):
    "Given a list of pathnames, returns the longest common leading component"
    if not m: return ''
    s1 = min(m)
    s2 = max(m)
    for i, c in enumerate(s1):
        if c != s2[i]:
            return s1[:i]
    return s1

def constexpr(qconstexpr,qstr):
   return "constexpr char " + qconstexpr + "[] = " + qstr + ";\n"

def make_getter(otype,qparameter,multiple,uservalue,qconstexpr,qstr,getfn=None):
    if getfn == None:
      getfn = otype
    if multiple:
      getter = constexpr(qconstexpr,qstr)
      getter += (vecmapvar(otype,qparameter) + ".push_back(type::" + otype + "(" + qparameter + uservalue + "));\n")
      paramback = vecmapvar(otype,qparameter) + ".back()"
      getter += (paramback+".set_value(variable,get_" + getfn + "(input_stream));\n")
    else:
      paramback = mapvar(otype,qparameter)
      getter = constexpr(qconstexpr,qstr)
      getter += ("check_unique(" + qparameter + ");\n")
      getter += (paramback+".set_value(variable,get_" + getfn + "(input_stream));")
    return getter

def make_unique_getter(otype,qparameter,multiple,uservalue,qconstexpr,qstr,getfn=None):
    if getfn == None:
      getfn = otype
    if multiple:
      #see make_unique function for details for which otypes are handled
      getter = constexpr(qconstexpr,qstr)
      vec = vecmapvar(otype,qparameter)
      getter += (vecmapvar(otype,qparameter) + ".push_back(type::" + otype + "(" + qparameter + uservalue + "));\n")
      paramback = vecmapvar(otype,qparameter) + ".back()"
      getter += (paramback+".set_value(variable,get_" + otype + "(input_stream));")
      getter += ("make_unique(" + qparameter + ");\n")
    else:
      paramback = mapvar(otype,qparameter)
      getter = constexpr(qconstexpr,qstr)
      getter += ("check_unique(" + qparameter + ");\n")
      getter += (paramback+".set_value(variable,get_" + getfn + "(input_stream));")
    return getter

def make_getter_min_max(otype,qparameter,multiple,uservalue,qconstexpr,qstr):
    if multiple:
      getter = constexpr(qconstexpr,qstr)
      getter += (vecmapvar(otype,qparameter) + ".push_back(type::" + otype + "(" + qparameter + uservalue + "));\n")
      paramback = vecmapvar(otype,qparameter) + ".back()"
      getter += (paramback+".set_value(variable,get_" + otype + "(input_stream,"+paramback+".minset(),"+paramback+".minval(),"+paramback+".maxset(),"+paramback+".maxval()));\n")
    else:
      paramback = mapvar(otype,qparameter)
      getter = constexpr(qconstexpr,qstr)
      getter += ("check_unique(" + qparameter + ");\n")
      getter += (paramback+".set_value(variable,get_" + otype + "(input_stream,"+paramback+".minset(),"+paramback+".minval(),"+paramback+".maxset(),"+paramback+".maxval()));")
    return getter

#{{{
class flat_stanley():
#{
  def __init__(self,env):
#{{{
      self.tree_parse = {}
      self.flat_constrt = defaultdict(list)
      self.flat_parse = defaultdict(list)
      self.flat_mult = defaultdict(list)
      self.flat_analyse = defaultdict(list)
      self.flat_wiki = defaultdict(list)
      # self.flat_linkages a dict only to allow a sort on first element, not required for function
      self.flat_linkages = defaultdict(list)
      self.flat_mtzcollist = defaultdict(list)
      indent = "  "
      initparams = build_phil_str()
      assert(len(initparams))
      self.master_phil = parse(initparams,converter_registry=converter_registry)

    # { scope for dealing with special case of multiples at higher level of definition
    # store multiples as separate arrays, not as objects, so use code below
    # however in parse function make all the keywords appear on one command line
    # and make all subkeys compulsory

    #below doesn't work
    #can't look infinitely up the tree to find a multiple parent
    #can only look for e.parent.multiple, which is workable - less complicated data structure
      def multiple_parent(e):
        extract_attribute_error = False
        if (getattr(e.object, "path", extract_attribute_error) is extract_attribute_error):
          return False
        if (getattr(e.object, "multiple", extract_attribute_error) is not extract_attribute_error):
          multtype = str(getattr(e.object, "multiple"))
          return (multtype == "True") or e.parent.multiple or multiple_parent(e.parent)
        return False

      multiple_params = defaultdict(list)
      for e in self.master_phil.all_definitions():
          if e.parent.multiple == True:
            last = e.path.split(".")[-1]
            multiple_params[e.parent.full_path()].append(last)
          if multiple_parent(e):
            print(e.path,multiple_parent(e))

    # }

      for e in self.master_phil.all_definitions():
        object = e.object
        if (not self.master_phil.deprecated) :
          parameter = e.path
        # parameter = parameter.lower()
          k = parameter.split(".")[1] # base keyword for class
       #  parameter = parameter.replace(phil_scope.lower()+tail,"")
          tree_recursion = self.tree_parse
          keyword_list = parameter.split(".")
          parameter += "." # must be after split, to avoid extra loop in recursion,
                           # and to make sure class name cannot be parameter name
                           # which would lead to compilation error
    #     { scope for dealing with all cases
          qparameter = "\"." + parameter + "\""
          qstr = qparameter
          qconstexpr = qparameter.replace(".","_").replace("\"","")[1:-1]
          qparameter = qconstexpr #implicit cast to string
         #print(qconstexpr,qparameter,qstr)
          extract_attribute_error = False
          optionalstr = str(getattr(object, "optional")) # don't use the if error access
          optionalattr = not optionalstr.startswith("False") # must be not false rather than true
          #getting optionalattr is tricky, above works
          #sys.stdout.write( "start optional " + parameter+ " "+ str(optionalattr)+ "\n")
          extract_attribute_error = False
          multiple = False
          if e.parent.multiple: #or the parent
            multiple = True
            linkroot = "\"."+ '.'.join(keyword_list[:-1]) + ".\""
            linkstem = "\""+ (keyword_list[-1]) + "\""
            if linkroot not in self.flat_linkages:
              self.flat_linkages[linkroot] = []
            clinkroot = linkroot.replace(".","_").replace("\"","")[1:-1]
            clinkstem = linkstem.replace(".","_").replace("\"","")
            self.flat_linkages[linkroot].append("{\n")
            self.flat_linkages[linkroot].append(constexpr(clinkroot,linkroot))
            self.flat_linkages[linkroot].append(constexpr(clinkstem,linkstem))
            slinkroot = clinkroot #implicit cast to string
            slinkstem = clinkstem #implicit cast to string
            self.flat_linkages[linkroot].append("linkKey(" + slinkroot + "," + slinkstem + ");\n")
            self.flat_linkages[linkroot].append("}\n")
          if (getattr(object, "multiple", extract_attribute_error) is not extract_attribute_error):
            multtype = str(getattr(object, "multiple"))
            multiple = multiple or (multtype == "True")
          if e.parent.multiple: #don't enter the last key as an index, subsequent keywords we be compulsory
            for part in keyword_list[:-1]:
              tree_recursion = tree_recursion.setdefault(part, { })
          else:
            for part in keyword_list:
              tree_recursion = tree_recursion.setdefault(part, { })
          extract_attribute_error = False
          if (getattr(object, "help", extract_attribute_error) is not extract_attribute_error):
          # param =  e.path.lower().replace("."," ").replace(phil_scope.lower(),"")
            param =  e.path.replace("."," ")
            while len(param) > 1 and param[0] == ' ':
              param = param[1:]
            helpval = "<"
            for x in object.words:
              helpval +=  str(x) + " "
            helpval =  helpval[:-1] + ">"
            if helpval == "<None>":
              helpval = None
          # if helpval == None or helpval == "<None>":
          #   helpval = str(getattr(object, "type"))
          #   helpval = "<" + helpval.split("(")[0] + ">"
            expert_level = str(getattr(object, "expert_level"))
            if (expert_level == "None"):
              expert_level = "0" # because 0 is converted to None in the phil parser

          extract_attribute_error = False
    #pre-analyse all the attributes here
          if (getattr(object, "type", extract_attribute_error) is not extract_attribute_error):
            valuetype = str(getattr(object, "type"))

            #set attribute flags
            valuetype = valuetype.replace('(',',',1)
            valuetype = rreplace(valuetype,')',',',1)
            attr_list = valuetype.replace(' ','').split(',')
            attr_minval = ",false,0" #for c++ string
            attr_maxval = ",false,0" #for c++ string
            valmin = None
            valmax = None
            uservalue = "," + str(object.words[0])
            perc1 = False
            perc2 = False
            size2 = False
            size3 = False
            size6 = False #anisotropy beta values; cell is unit_cell
            size9 = False
            directory = False # type defaults
            ext = None # ext defaults
            for a in attr_list: #first loop selects size=2
              kv = a.split("=")
              if len(kv) == 2:
                if kv[0] == "value_min": #find value_min if present
                  attr_minval = ",true,"+str(kv[1])
                  valmin = kv[1]
                  if kv[1] == "0":
                    perc1 = True
                if kv[0] == "value_max": #find value_max if present
                  attr_maxval = ",true,"+str(kv[1])
                  valmax = kv[1]
                  if kv[1] == "100":
                    perc2 = True
                if kv[0] == "size" and kv[1] == "2":
                  size2 = True
                if kv[0] == "size" and kv[1] == "3":
                  size3 = True
                if kv[0] == "size" and kv[1] == "6":
                  size6 = True
                if kv[0] == "size" and kv[1] == "9":
                  size9 = True
              if len(kv) >= 2 and "filesystem" in attr_list:
                for i in (0,len(kv)-2):
                  if kv[i] == "directory" and kv[i+1] == "True":
                    directory = True
                for i in (0,len(kv)-2):
                  if kv[i] == "ext":
                    ext = kv[i+1]
            percent = perc1 and perc2
            attr_minmax = attr_minval + attr_maxval
            getter = ""

            helptype = None

            if valuetype.startswith("bools"):
              helptype = "<list of boolean>"
            elif valuetype.startswith("bool"):
              helptype = "<boolean>"
            elif percent and size2:
              helptype = "<percent percent>"
            elif percent:
              helptype = "<percent>"
            elif valuetype.startswith("uuids"):
              helptype = "<list of uuids>"
            elif valuetype.startswith("uuid"):
              helptype = "<uuid>"
            elif valuetype.startswith("int"):
              if "size" not in valuetype and valuetype.startswith("ints"):
                helptype = "<list of int>"
              elif size2:
                helptype = "<int int>"
              elif size3:
                helptype = "<int int int>"
              elif size6:
                helptype = "<int int int int int int>"
              elif size9:
                helptype = "<int int int int int int int int int>"
              else:
                helptype = "<int>"
            elif valuetype.startswith("float"):
              if "size" not in valuetype and valuetype.startswith("floats"):
                helptype = "<list of float>"
              elif size2:
                helptype = "<float float>"
              elif size3:
                helptype = "<float float float>"
              elif size6:
                helptype = "<float float float float float float>"
              elif size9:
                helptype = "<float float float float float float float float float>"
              else:
                helptype = "<float>"
            elif valuetype.startswith("space_group"):
              helptype = "\"<space group>\""
            elif valuetype.startswith("unit_cell"):
              helptype = "\"<unit cell>\""
            elif valuetype.startswith("strings"):
              helptype = "<list of string>"
            elif valuetype.startswith("str"):
              helptype = "\"<string>\""
            elif valuetype.startswith("atom_selection"):
              helptype = "\"<atom selection>\""
            elif valuetype.startswith("scatterers"):
              helptype = "\"<scatterers>\""
            elif valuetype.startswith("scatterer"):
              helptype = "\"<scatterer>\""
            elif valuetype.startswith("filesystem(directory=True"):
              helptype = "\"<directory>\""
            elif valuetype.startswith("filesystem"):
              helptype = "\"<directory/file/ext>\""
            elif valuetype.startswith("path"):
              helptype = "\"<directory/file>\""
            elif directory:
              helptype = "\"<directory>\""
            elif valuetype.startswith("mtzcol"):
              helptype = "<mtz column>"
            elif valuetype.startswith("choices"):
              helptype = "<" + mode_generator().unquoted_string()[:-1]+ ">"
              helpval = "<" + mode_generator().the_default + ">"
            elif valuetype.startswith("choice"):
              helptype = ""
              helpval = ""
              for word in object.words:
                if word.value.startswith('*'):
                  helpval += word.value.replace('*','') + " "
                helptype += word.value.replace('*','') + " "
              helptype = "<" + helptype[:-1]+ ">"
              helpval = "<" + helpval[:-1] + ">"
              if len(helpval) == 2:
                helpval = None
            assert(helptype is not None)
            self.flat_wiki[k].append(keyword_wiki_stuff(
                                param=param,
                                helpval=helpval,
                                helptype=helptype,
                                valmin=valmin,
                                valmax=valmax,
                                helpattr = str(getattr(object, "help")),
                                short_caption = str(getattr(object, "short_caption")),
                                caption = str(getattr(object, "caption")),
                                expert_level = expert_level,
                                multiple = multiple
                                ))

            otype = ""

           #getter += ("constexpr auto " + qconstexpr + "[] = " + qstr + ";\n")
            if valuetype == "bool":
              otype = "boolean"
              uservalue = "" if is_plain_none(object.words) else uservalue.lower()
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif percent and size2:
              otype = "percent_paired"
              uservalue = ",{"
              for x in range(0, len(object.words)):
                uservalue += str(object.words[x]) + ","
              uservalue = uservalue[:-1]
              uservalue += "}"
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif percent:
              otype = "percent"
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif valuetype.startswith("int") or \
                 valuetype.startswith("uuid") or \
                 valuetype.startswith("float"):
              # set number type to float or int
              if valuetype.startswith("int"):
                ntype = "int"
              elif valuetype.startswith("float"):
                ntype = "flt"
              elif valuetype.startswith("uuid"):
                ntype = "uuid"
              uservalue = ",{"
              for x in range(0, len(object.words)):
                uservalue += str(object.words[x]) + ","
              uservalue = uservalue[:-1]
              uservalue += "}"
              if size2:
                otype = ntype +"_paired"
              elif size3:
                otype = ntype + "_vector"
              elif size6:
                otype = ntype + "_numbers" # don't need to handle differently really; beta values
              elif size9:
                otype = ntype + "_matrix"
              else:
                otype = ntype +"_numbers"
              #overwrite for single and uservalue with non-brace number
              if not valuetype.startswith("ints") and \
                 not valuetype.startswith("floats") and \
                 not valuetype.startswith("uuids"):
                otype = ntype + "_number"
                uservalue = "," + str(object.words[0])
              uservalue = attr_minmax if is_plain_none(object.words) else (attr_minmax + uservalue)
              if multiple and otype == "flt_numbers":
                getter += make_unique_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
              else:
                getter += make_getter_min_max(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype == "strings" or valuetype == "scatterers"):
              otype = "strings"
              uservalue = ",{"
              for word in object.words:
                uservalue += "\"" +word.value + "\","
              uservalue = uservalue[:-1]
              uservalue += "}"
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype == "str" or valuetype == "scatterer"):
              otype = "string"
              uservalue = ""
              for word in object.words:
                uservalue += word.value + " "
              uservalue = ",\"" + uservalue[:-1] + "\""
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif valuetype.startswith("atom_selection"):
              otype = "atom_selection"
              uservalue = ""
              for word in object.words:
                uservalue += word.value + " "
              uservalue = ",\"" + uservalue[:-1] + "\""
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype == "space_group"):
              otype = "spacegroup"
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_unique_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype == "unit_cell"):
              otype = "flt_cell"
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype.startswith("stream")):
              otype = "out::stream"
              uservalue = attr_minmax
              getter += make_unique_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr,getfn="string")
            if (valuetype.startswith("path")) or (valuetype.startswith("filesystem")):
              otype = "path"
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
            elif (valuetype.startswith("choices")): # choices(
            #{ hack because we know this is the mode
              otype = "choice"
              getter += "std::string choices = \"\";\n"
              getter += ("while (optionalKey(input_stream,{" + mode_generator().quoted_list() + "}))\n")
              getter += "{\n"
              getter += "  if (keyIsEol()) return false;\n"
              getter += "  else if (keyIsOptional()) { choices += string_value + \" \"; }\n"
              getter += "}\n"
              getter += constexpr(qconstexpr,qstr)
              getter += (mapvar(otype,qparameter) + ".set_value(choices);")
              uservalue = ",\""  +mode_generator().unquoted_string() + "\",\"" + mode_generator().the_default + "\""
            #  CHOICE[".phasertng.mode."].set_value(choices);
            # paramback = mapvar(otype,qparameter)
            #}
            elif (valuetype.startswith("choice")): # choice(multi=...
            #{
            # multichoice = valuetype.startswith("choice,multi=True")
              multichoice = "multi=True" in valuetype
              dephalt = ""
              choices = ""
              for word in object.words:
                choices += word.value.replace('*','') + " "
                #choices += word.value.replace('*','').lower()+ " "
             # don't change the case here or below
             # so that there is a direct relationship between enumerations and choice : case sensitive
                if word.value.startswith("*"):
                  dephalt += word.value[1:] + " "
              if len(dephalt)!=0:
                dephalt =  dephalt[:-1] + ","
              undefined = len(dephalt)==0
              otype = "choice"
              uservalue = "" if undefined else dephalt
              string1D=""
              uservalue = "," + "\""+ choices + "\"," + "\"" + uservalue.replace(",","").replace("*","") +"\""
             # uservalue = uservalue.lower();
              for enum in object.words:
               # string1D += "\"" + str(enum).replace("*","").lower() + "\","
                string1D += "\"" + str(enum).replace("*","") + "\","
              if multiple and not multichoice:
                getter  = ("compulsoryKey(input_stream,{" + string1D[:-1] + "});\n")
                getter += ("if (keyIsEol()) return false;\n")
                for s in string1D.split(",")[:-1]:
                  getter += ("else if (keyIsAdd(" + s + ",false))\n")
                  getter += ("{\n")
                  getter += indent + constexpr(qconstexpr,qstr)
                  getter += (indent + vecmapvar(otype,qparameter) + ".push_back(type::choice(" + qparameter + uservalue + "));\n")
                  paramback = vecmapvar(otype,qparameter) + ".back()"
                  getter += (indent + paramback +".set_value(variable,string_value);\n")
                  getter += ("}\n")
              elif multiple and multichoice:
                getter = ""
                getter += ("std::string choices = \"\";\n")
                # allocate memory and set to default, although this will be overwritten with set_value below
                # this is not where default is set - it is set in self.flat_analyse below, if array is empty at the end
                getter += constexpr(qconstexpr,qstr)
                getter += (vecmapvar(otype,qparameter) + ".push_back(type::choice(" + qparameter + uservalue + "));\n")
                getter += ("while (optionalKey(input_stream,{" + string1D[:-1] + "}))\n")
                getter += ("{\n")
                getter += ("  if (keyIsEol()) return false;\n")
           #windows complains about nexted loops with else if so just add the choices value here
                getter += (indent + "else if (keyIsOptional()) { choices += string_value + \" \"; }\n")
           #    for s in string1D.split(",")[:-1]:
           #      getter += (indent + "else if (keyIs(" + s + ")) { choices += string_value + \" \"; }\n")
                getter += ("}\n")
                getter += constexpr(qconstexpr,qstr)
                getter += (vecmapvar(otype,qparameter) + ".back().set_value(choices);")
              elif not multiple and multichoice:
                getter = ""
                getter += ("std::string choices = \"\";\n")
                getter += ("while (optionalKey(input_stream,{" + string1D[:-1] + "}))\n")
                getter += ("{\n")
                getter += ("  if (keyIsEol()) return false;\n")
           #windows complains about nexted loops with else if so just add the choices value here
                getter += (indent + "else if (keyIsOptional()) { choices += string_value + \" \"; }\n")
           #    for s in string1D.split(",")[:-1]:
           #      getter += (indent + "else if (keyIs(" + s + ")) { choices += string_value + \" \"; }\n")
                getter += ("}\n")
                getter += constexpr(qconstexpr,qstr)
                getter += (mapvar(otype,qparameter) + ".set_value(choices);")
              elif not multiple and not multichoice:
                getter = ("compulsoryKey(input_stream,{" + string1D[:-1] + "});\n")
                paramback = mapvar(otype,qparameter)
                getter += constexpr(qconstexpr,qstr)
                getter += ("check_unique(" + qparameter + ");\n")
                getter += (paramback +".set_value(variable,string_value);")
              else:
                sys.stdout.write("parameter=" +  parameter + " or .type=" + valuetype + " has not been dealt with in write_input.py (developer - fix it!)\n")
                sys.exit()

            #}
            elif (valuetype.startswith("mtzcol")):
            #{
              otype = "mtzcol"
              uservalue = ""
              for word in object.words:
                uservalue += word.value + " "
              uservalue = ",\"" + uservalue[:-1] + "\""
              uservalue = "" if is_plain_none(object.words) else uservalue
              getter += make_getter(otype=otype,qparameter=qparameter,multiple=multiple,uservalue=uservalue,qconstexpr=qconstexpr,qstr=qstr)
              otype = "type::mtzcol"
              attr_mtzcol = ""
              for a in attr_list: #first loop selects size=2
                kv = a.split("=")
                if len(kv) == 2:
                  if kv[0] == "type":
                    attr_mtzcol += "," + str(kv[1])
                  if kv[0] == "name":
                    attr_mtzcol += "," + str(kv[1])
              splitkey =  qstr.split(".")
              labin_col = splitkey[0] + splitkey[-2] + splitkey[-1] #quote-lastkeyword-quote
              attr_mtzcol += "," + labin_col
              attr_mtzcol = attr_mtzcol.upper() # to match the enumerations, which are UPPER
              uservalue = attr_mtzcol if is_plain_none(object.words) else (attr_mtzcol + uservalue)
            #}

    #     } scope for dealing with all cases

    #     {
          if (len(otype) == 0):
            sys.stdout.write("parameter=" +  parameter + " or .type=" + valuetype + " has not been dealt with in SConscript (developer - fix it!)\n")
            sys.exit()
          if e.parent.multiple:
            self.flat_mult[e.parent.full_path()].append("compulsoryKey(input_stream,{\"" + keyword_list[-1] + "\"});")
            self.flat_mult[e.parent.full_path()].append("variable.push_back(\"" + keyword_list[-1] + "\");")
            while getter.endswith("\n"):
              getter = getter[:-1] # to avoid line with nothing on it
            if len(getter):
              self.flat_mult[e.parent.full_path()].append(getter)
            self.flat_mult[e.parent.full_path()].append("variable.pop_back();")
          elif multiple and not e.parent.multiple:
            while getter.endswith("\n"):
              getter = getter[:-1] # to avoid line with nothing on it
            if len(getter):
              self.flat_mult[e.parent.full_path()].append(getter)


          if (otype != "type::mtzcol"):
            self.flat_constrt[k].append(constexpr(qconstexpr,qstr))
            if not multiple:
              self.flat_constrt[k].append(mapvar(otype,qparameter) + " = type::" + otype + "(" + qparameter + uservalue + ");\n")
            elif getattr(e.object, "multiple") and len(uservalue):
              # this is (currently) for the special case of a vector of vectors
              # where the individual parameter is multiple
              # i.e. it is not a member of a scope of multiple
              # and the std::vector array can be initialized with one entry of e.g. {x,y,z}
              # This would need much work to get it to work for scopes
              defvalue = "type::" + otype + "(" + qparameter + uservalue + ")"
              self.flat_constrt[k].append(vecmapvar(otype,qparameter) + " = std::vector<type::" + otype + ">(1," + defvalue + ");\n") #initialize vector with one value, the default
            else:
              self.flat_constrt[k].append(vecmapvar(otype,qparameter) + " = std::vector<type::" + otype + ">();\n")
            if e.parent.multiple:
              tree_recursion[keyword_list[-2]] = keyword_parse_stuff("")
              for i in self.flat_mult[e.parent.full_path()]:
                tree_recursion[keyword_list[-2]].append_getter(i)
            else:
              tree_recursion[keyword_list[-1]] = keyword_parse_stuff(valuetype)
              tree_recursion[keyword_list[-1]].append_getter(getter)
          elif otype.startswith("type::mtzcol"):
            qkq = "\"" + parameter.split(".")[-2] + "\""
            self.flat_mtzcollist[k].append(qkq)
            self.flat_constrt[k].append(constexpr(qconstexpr,qstr))
            self.flat_constrt[k].append("value__mtzcol[" + qparameter + "] = " + otype + "(" + qparameter + attr_mtzcol + ");\n")
            self.flat_parse[k] = [] #rather than tree recursion
            keylist = ""
            for q in self.flat_mtzcollist[k]:
              keylist += "," + q
            self.flat_parse[k].append("while (optionalKey(input_stream,{" + keylist[1:] + "}))")
            self.flat_parse[k].append("{")
            for q in self.flat_mtzcollist[k]:
              splitkey =  qstr.split(".")
              labin_col = splitkey[1]  #keyword labin, map_preparation
              lookup = "\"." + labin_col + "." + q.replace('"',"") + ".\""
              self.flat_parse[k].append("  if (keyIs(" + q + ")) value__mtzcol[" + lookup + "].set_value(variable,getAssignString(input_stream));")
            self.flat_parse[k].append("}\n")
            tree_recursion[keyword_list[-1]] = keyword_parse_stuff(valuetype)
            tree_recursion[keyword_list[-1]].append_getter(getter)
    #     }
#}}} init
      #call below and store in the constructor, since file parsing is slow
      self.phil_input_keywords,self.phil_result_keywords,self.phil_all_keywords = self.get_phil_io_keywords()

      self.modes_keywords = dict()
      allowed_modes = set()
      descriptors = mode_generator().mode_list
      for k,v,d,c in descriptors:
         allowed_modes.add(k)

      get_phasertng_mode = set()
      get_phasertng_keywords = set()
      get_phasertng_input = set()
      get_phasertng_result = set()
      for voyager in voyager_generator().scripts():
        actionfile = voyager
        #this is a hack to make the bones documentation match the picard documentation
        if voyager == 'bones': actionfile = 'picard'
        filename = os.path.join(env.dist_path,"phasertng","voyager", actionfile+".py")
       #print(filename)
        with open(filename, 'r') as f1:
          get_auto_mode = set()
          get_auto_keywords = set()
          get_auto_input = set()
          get_auto_result = set()
          lines = f1.readlines()
          for line in lines:
            if ("ModeRunner(basetng" in line):
              dat = line.replace("\"","\'").split("\'")
              for word in dat:
                 if (word in allowed_modes): # lots of other junk on the line
                   get_auto_mode.add(word)
                   get_phasertng_mode.add(word)
          for m in get_auto_mode:
            for k in sorted(self.phil_input_keywords[m]):
              get_auto_input.add(k)
              get_phasertng_input.add(k)
            for k in sorted(self.phil_result_keywords[m]):
              get_auto_result.add(k)
              get_phasertng_result.add(k)
            for k in sorted(self.phil_all_keywords[m]):
              get_auto_keywords.add(k)
              get_phasertng_keywords.add(k)
        self.modes_keywords[voyager] = { "modes" : get_auto_mode,
                                         "keywords" : get_auto_keywords,
                                         "input" : get_auto_input,
                                         "result" : get_auto_result, }
      #below is a hack
      self.modes_keywords[generic_documentation_name] = { "modes" : get_phasertng_mode,
                                           "keywords" : get_phasertng_keywords,
                                           "input" : get_phasertng_input,
                                           "result" : get_phasertng_result, }

  def get_phil_io_keywords(self):
        master_phil = self.master_phil
        phil_input_keywords = defaultdict(OrderedSet)
        phil_result_keywords = defaultdict(OrderedSet)
        phil_all_keywords = defaultdict(OrderedSet)
        for e in master_phil.all_definitions():
          parameter = e.path
        # parameter = parameter.lower()
          extract_attribute_error = False
          k = parameter.split(".")[1] # base keyword for class
          if (getattr(e.object, "style", extract_attribute_error) is not extract_attribute_error):
            child_style = getattr(e.object, "style")
            #no need to nest as we only need one child keyword and the base keyword
            # will be added to the set, doesn't need to catch all definition keywords
            for style in {child_style,e.parent.style}: #also check the parent
              if style != None:
                style_list = str(style).split(" ")
                for s in style_list:
                  if s.startswith("tng:input:"):
                    s = s.replace("tng:input:","")
                    modes = s.split("+")
                    for m in modes:
                      if len(m):
                        phil_input_keywords[m].add(k)
                        phil_all_keywords[m].add(k)
                  if s.startswith("tng:result:"):
                    s = s.replace("tng:result:","")
                    modes = s.split("+")
                    for m in modes:
                      if len(m):
                        phil_result_keywords[m].add(k)
                        phil_all_keywords[m].add(k)
        return phil_input_keywords,phil_result_keywords,phil_all_keywords
#}
#}}}


def write_input_generic(class_name,class_path,env):
  if not class_name:
    pass
  #read phil file
  print ("Building %s"%class_name)
  flat = flat_stanley(env)

  write_input_file = True
  if write_input_file:
    sys.stdout.write("phasertng: Writing input file " + env.inputcard_cc + "\n")
    f3 = open(env.inputcard_cc, "w")
    f3.write("// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
    f3.write("// EDITS WILL BE OVERWRITTEN!\n")
    f3.write("#include <autogen/include/" + class_name + ".h>\n")
    f3.write("\n")
    f3.write("namespace phasertng {\n")
    f3.write("\n")

    #constructor with defaults
    indent = "  "
    f3.write(indent + class_name + "::" + class_name + "() : InputBase()\n")
    f3.write(indent + "{\n")
    f3.write(indent*2 + "add_keywords();\n")
    f3.write(indent*2 + "add_linkKeys();\n")
    f3.write(indent*2 + "add_phil_input_parameters();\n")
    f3.write(indent*2 + "add_phil_result_parameters();\n")
    f3.write(indent + "}\n")
    f3.write("\n")

    f3.write(indent + "void\n")
    f3.write(indent + class_name + "::add_keywords()\n")
    f3.write(indent + "{\n")
    for keyword in sorted(flat.flat_constrt.keys()):
      for constrt in flat.flat_constrt[keyword]:
        f3.write(indent*2 + constrt)
    f3.write(indent + "}\n")

    f3.write(indent + "void\n")
    f3.write(indent + class_name + "::add_linkKeys()\n")
    f3.write(indent + "{\n")
    for keyword in sorted(flat.flat_linkages.keys()):
      for links in flat.flat_linkages[keyword]: #these must not be sorted, order is important
        f3.write(indent*2 + links)
    f3.write(indent + "}\n")
    f3.write("\n")

    #do the analysis of the phil_keywords parameters here, locally
    #input object can be queried for keywords relevant to each mode
    #use input_keywords(mode) and output_keywords(mode) to query each
    #mode about which keywords have ACTUALLY been used in each mode
    #because the input object keeps a trace of them
    #there have to be manually added to the phil of course, for gui etc
    phil_input_keywords,phil_result_keywords,phil_all_keywords = flat.get_phil_io_keywords()
    possible_modes = mode_generator().possible_modes()

    f3.write(indent + "void\n")
    f3.write(indent + class_name + "::add_phil_input_parameters()\n")
    f3.write(indent + "{\n")
    for m,values in sorted(phil_input_keywords.items()):
      if m not in possible_modes:
        print(str(m) + " " + str(values))
        raise ValueError("tng:input not in possible modes")
      parameters = "phil_input_parameters[\"" + m + "\"] = {"
      for k in sorted(values):
        parameters += "\""+ k + "\","
      parameters = parameters[:-1] + "};"
      f3.write(indent*2 + parameters + "\n")
    f3.write(indent + "}\n")
    f3.write("\n")

    f3.write(indent + "void\n")
    f3.write(indent + class_name + "::add_phil_result_parameters()\n")
    f3.write(indent + "{\n")
    for m,values in sorted(phil_result_keywords.items()):
      if m not in possible_modes:
        raise ValueError("tng:result not in possible modes: " + m)
      parameters = "phil_result_parameters[\"" + m + "\"] = {"
      for k in sorted(values):
        parameters += "\""+ k + "\","
      parameters = parameters[:-1] + "};"
      f3.write(indent*2 + parameters + "\n")
    f3.write(indent + "}\n")
    f3.write("\n")

    #parse function
    f3.write(indent + "bool\n")
    f3.write(indent + class_name + "::parse(std::string cards, bool ignore_unknown_keys)\n")
    f3.write(indent + "{\n")
 # below already in constructor, slows down parse of each node in InputNode
 #  for keyword in sorted(flat.flat_linkages.keys()):
 #    for links in flat.flat_linkages[keyword]: #these must not be sorted, order is important
 #      f3.write(indent*2 + links)
    f3.write(indent*2 + "cards.erase(std::unique(cards.begin(),cards.end(),[](char a,char b){return a=='\\n' && b=='\\n';}),cards.end());\n") # removes blank lines that will not parse
    f3.write(indent*2 + "cards.erase(cards.begin(), std::find_if(cards.begin(), cards.end(), [](unsigned char ch) { return !std::isspace(ch);})); // Lambda function checks for non-whitespace\n"); #removes starting blank lines that will return eof
    f3.write(indent*2 + "reset_ccp4base();\n")
    f3.write(indent*2 + "std::istringstream input_stream(cards);\n")
    f3.write(indent*2 + "input_stream.seekg(0,input_stream.beg);\n")
    f3.write(indent*2 + "while (input_stream)\n")
    f3.write(indent*2 + "{\n")
    f3.write(indent*3 + "variable.clear();\n")
    f3.write(indent*3 + "get_token(input_stream);\n")
    f3.write(indent*3 + "if (keyIsEnd()) return false;\n")
    f3.write(indent*3 + "else if (keyIsEol()) return false;\n")
    f3.write(indent*3 + "else if (tokenIsAlready(Token::ASSIGN)) return true; //!!and stop parsing file\n")
#   f3.write(indent*3 + "else if (keyIs(NODE_SEPARATOR())) return true; //!!and stop parsing file\n")
    f3.write(indent*3 + "else if (tokenIsAlready(Token::COMMENT)) { skip_line(input_stream); }\n")
#include a special case that returns a boolean from parse for flagging call of external functions
#this special case must be a string token in CCP4 base
#special handing that ==== is not recognised as an assign token
#if first is =, if second is also '=', then all to whitespace captured as spacer
#   f3.write(indent*3 + "else if (keyIs(\"====\")) return true;\n")
    for keyword in sorted(flat.tree_parse.keys()):
      elseif = True
      if elseif:
        f3.write(indent*3 + "else if (keyIs(\"" + keyword + "\"))\n")
      else:
        f3.write(indent*3 + "if (keyIs(\"" + keyword + "\"))\n")
      f3.write(indent*3 + "{\n")
      #--recursion part of parse
      recursion = recursion_strings()
      recursion.make_parse_string(flat.tree_parse[keyword],indent*4)
      f3.write(recursion.catstr)
      recursion.clear()
      #--flat.flat part of parse
 #    for parze in flat.flat_parse[keyword]:
 #      f3.write(indent*4 + parze + "\n")
      f3.write(indent*4 + "skip_line(input_stream);\n")
      f3.write(indent*3 + "}\n")
    f3.write(indent*3 + "else {\n")
    f3.write(indent*4 + "if (!ignore_unknown_keys and string_value.size())\n")
    f3.write(indent*5 + "storeError(err::SYNTAX,\"Unknown keyword: \" + string_value);\n")
    f3.write(indent*4 + "skip_line(input_stream);\n")
    f3.write(indent*3 + "}\n")
    f3.write(indent*2 + "}\n")
    f3.write(indent*2 + "return false;\n")
    f3.write(indent + "}\n")
    f3.write("\n")
    #analyse function
    f3.write(indent + "void\n")
    f3.write(indent + class_name + "::analyse()\n")
    f3.write(indent + "{\n")
    for keyword in sorted(flat.flat_analyse.keys()):
      for analyse in flat.flat_analyse[keyword]:
        f3.write(indent*2 + analyse) # path can be written to, file checking if file exists
    f3.write(indent*2 + "clear_incompatible_linkage_arrays();\n")
    f3.write(indent + "}\n")
    if (class_name == "InputCard"):
      mode_descriptors = mode_generator().mode_descriptors()
      f3.write("\n")
      f3.write(indent + "std::string " + class_name + "::header() const\n")
      f3.write(indent + "{\n")
      f3.write(indent*2 + "if (false) return \"\";\n" )
     #for k,v in mode_descriptors.iteritems(): #removed for python3
      for k,v in sorted(mode_descriptors.items()):
        f3.write(indent*2 + "else if (running_mode == \"" + k + "\") return \"" + v.upper() + "\";\n" )
      f3.write(indent*2 + "return \"THE TWILIGHT ZONE\";\n" )
      f3.write(indent + "}\n")
      f3.write("\n")

    if (class_name == "InputCard"):
      f3.write(indent + "std::set<std::string> " + class_name + "::python_modes() const\n")
      descriptors = mode_generator().mode_list
      f3.write(indent + "{\n")
      f3.write(indent*2 + "std::set<std::string> pymodes;\n")
      for k,val,txt,cpp in descriptors:
        if not bool(cpp):
          f3.write(indent*2 + "pymodes.insert(\"" + k + "\");\n" )
      f3.write(indent*2 + "return pymodes;\n" )
      f3.write(indent + "}\n")

    f3.write("\n")
    f3.write("}\n")
    f3.close()

  #(class_path) #env.autogen
  sys.stdout.write("Writing python keywords file " + env.pyfile + "\n")
  pyfile = open(env.pyfile, "w")
  pyfile.write("keyword_subsets_by_program_name = {\n")
  pyfile.write("\"InputCard\" :   [],\n")
  #flat_stanley includes the generation of generic_documentation_name dictionaries
  #default generic_documentation_name != [] as for InputCard
  #pyfile.write("\"" + generic_documentation_name + "\" :   [],\n")
  mdict = "\"modes\" : " + str(sorted(flat.modes_keywords[generic_documentation_name]["modes"]))
  kdict = "\"keywords\" : " + str(sorted(flat.modes_keywords[generic_documentation_name]["keywords"]))
  pyfile.write("\"" + generic_documentation_name + "\" : { " + mdict + "," + kdict + "},\n")
  #scripts excludes generic_documentation_name
  for voyager in voyager_generator().scripts():
      mdict = "\"modes\" : " + str(sorted(flat.modes_keywords[voyager]["modes"]))
      kdict = "\"keywords\" : " + str(sorted(flat.modes_keywords[voyager]["keywords"]))
   #  kdict = "\"input\" : " + str(flat.modes_keywords[voyager]["input"])
   #  kdict = "\"output\" : " + str(flat.modes_keywords[voyager]["ouput"])
      pyfile.write("\"" + voyager + "\" : { " + mdict + "," + kdict + "},\n")
  pyfile.write("}\n")
  pyfile.close()

def write_input_cc(source,target,env):
  pyfile = "keyword_subsets_by_program_name.py"
  env.pyfile = os.path.join(env.autogen,pyfile)
  write_input_generic(env.inputcard_name,env.autogen,env)
  verbose = False
  f = "InputCard.cc"
  assert(env.inputcard_cc.endswith(f))
  print("phasertng: Copying " + f)
  if (verbose):
    print("phasertng: " + os.path.join(env.autogen,f) + " --> " +
                os.path.join(env.dist_path,"codebase","autogen",f))
  shutil.copy(os.path.join(env.autogen,f),
              os.path.join(env.dist_path,"codebase","autogen",f))
  print("phasertng: Copying " + os.path.basename(pyfile))
  if (verbose):
    print("phasertng: " + env.pyfile + " --> " +
                os.path.join(env.dist_path,"phasertng","autogen",pyfile))
  shutil.copy(env.pyfile,
              os.path.join(env.dist_path,"phasertng","autogen",pyfile))

if __name__ == "__main__":
  source = ""
  target = ""
  os.autogen = "./"
  os.phil_params_file  = os.path.join("..","phasertng","phenix_interface","__init__.params")
  os.types_dir  = os.path.join("..","codebase","phasertng","keywords","types")
  os.inputcard_name = "InputCard"
  os.inputcard_cc   = "./InputCard.cc"
  os.dist_path   = "./"
  write_input_cc(source,target,env=os)
