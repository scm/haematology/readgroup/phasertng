#ifndef __phasertng_End_Keys_enum__
#define __phasertng_End_Keys_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace End {
  MakeEnum ( Keys,
               (END)
               (QUIT)
               (STOP)
               (EXIT)
               (GO)
               (RUN)
               (START)
               (KILL)
           );
// below were conditions in phaser:
// END  //not included, to avoid ignoring END in pdb file
// KILL //MUST not be included, to avoid ignoring of KILL keyword
}}
#endif
