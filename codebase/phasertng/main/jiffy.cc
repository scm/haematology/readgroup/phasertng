#include <phasertng/main/jiffy.h>
#include <cstdarg>
#include <cctype> //Windows toupper

namespace phasertng {
constexpr int DEF_MAXSTR = 120;
constexpr int DEF_SSO = 15; //small string optimization, size 15-23 in string object

//-----------------------------------------------------------------------
// stoq - std::string to quoted string
//-----------------------------------------------------------------------
std::string stoq(const std::string & str)
{
  // Check if the string is quoted at both ends
  bool quoted = (str.length() >= 2 && str.front() == '"' && str.back() == '"');
  if (quoted) return str;
  std::string result(str.length()+2, '\0');
  result[0] = '"';
  int s = 0;
  for (s = 0; s < str.length(); s++)
    result[s+1] = str[s];
  result[s+1] = '"';
  return result;
}

//-----------------------------------------------------------------------
// ntos - int to std::string with leading zeros
//-----------------------------------------------------------------------
std::string ntos(const int i,const int maxi)
{
  std::string result(DEF_SSO, '\0');
  int w = (maxi > 0) ? static_cast<int>(std::log10(maxi) + 1) : 1;
  int len = snprintf(&result[0],DEF_SSO,"%0*d",w,i);
  result.resize(len);
  return result;
}

//-----------------------------------------------------------------------
// chtos - char to std::string
//-----------------------------------------------------------------------
std::string chtos(char i)
{
  return std::string(1,i); //probably intrinsic sso
}

//-----------------------------------------------------------------------
// itos - int to std::string
//-----------------------------------------------------------------------
std::string itos(const int i)
{
  return std::to_string(i);
}

//-----------------------------------------------------------------------
// itos - int to std::string
//-----------------------------------------------------------------------
std::string itos(const size_t i)
{
  return std::to_string(i);
}

//-----------------------------------------------------------------------
// itos - int to std::string with justification and width
//-----------------------------------------------------------------------
std::string itos(const int i,const int w,bool rj,bool sign)
{
  std::string result(DEF_SSO, '\0');
  int len;
  if (sign)
    len = rj ?
      snprintf(&result[0],DEF_SSO,"%+*d",w,i):
      snprintf(&result[0],DEF_SSO,"%-+*d",w,i);
  else
    len = rj ?
      snprintf(&result[0],DEF_SSO,"%*d",w,i):
      snprintf(&result[0],DEF_SSO,"%-*d",w,i);
  result.resize(len);
  return result;
}

//-----------------------------------------------------------------------
// dtos - double to std::string
//-----------------------------------------------------------------------

std::string dtos(const double f)
{
  return std::to_string(f);
}

std::string dtos(const double f,int w,int p)
{
  std::string result(DEF_SSO, '\0');
  int len = snprintf(&result[0],DEF_SSO,"%*.*f",w,p,f);
  if (len >= DEF_SSO)
  {
    result.resize(DEF_MAXSTR);
    len = snprintf(&result[0],DEF_MAXSTR,"%.*f",p,f);
  }
  result.resize(len);
  return result;
}

std::string dtos(const double f,int p)
{
  std::string result(DEF_SSO, '\0');
  int len = snprintf(&result[0],DEF_SSO,"%.*f",p,f);
  if (len >= DEF_SSO)
  {
    result.resize(DEF_MAXSTR);
    len = snprintf(&result[0],DEF_MAXSTR,"%.*f",p,f);
  }
  result.resize(len);
  return result;
}

std::string dtos(const double f,int w,int p,bool sign,bool remove_trailing_zero,bool rj)
{
  int strsize = DEF_MAXSTR;
  std::string result(strsize, '\0');
  int f_cols(p);
  double cp_f(f);
  while (cp_f >= 1) { f_cols++; cp_f/=10.; }
  if (sign) w -= 1; //take off char for sign
  int len;
  if (sign and f_cols <= w)
  {
    len = snprintf(&result[0],strsize,"%+*.*f",w,p,f);
  }
  else if (!sign and f_cols <= w and rj)
  {
    len = (rj) ? snprintf(&result[0],strsize,"%-*.*f",w,p,f):
                 snprintf(&result[0],strsize,"%*.*f",w,p,f);
  }
  else if (sign)
  {
    len = snprintf(&result[0],strsize,"%*.*e",w,w-7,f);
  }
  else
  {
     len = snprintf(&result[0],strsize,"%*.*e",w,w-6,f);
  }
  result.resize(len);
  //if there are floating zeros at the end of the decimal places, remove them in situ
  if (remove_trailing_zero and (result.find('.') != std::string::npos))
  {
    int i(0);
    for (i = 0; i < result.size(); i++)
      if (result[i] == '.') break;
    for (int j = result.size()-1; j > i;  j--)
    {
      if (result[j] == '0' or result[j] == ' ') result[j] = ' ';
      else break;
    }
    if (result.back() == '.') result.back() = ' ';
  }
  return result;
}

std::string gtos(const double f,int p)
{
  std::string result(DEF_SSO, '\0');
  int len = snprintf(&result[0],DEF_SSO,"%.*g",p,f);
  if (len >= DEF_SSO)
  {
    result.resize(DEF_MAXSTR);
    len = snprintf(&result[0],DEF_MAXSTR,"%.*f",p,f);
  }
  result.resize(len);
  return result;
}

std::string dtoi(const double f)
{
  std::string result(DEF_SSO, '\0');
  int len;
  if (std::fabs(f) >= 1) len = snprintf(&result[0],DEF_SSO,"%d",static_cast<int>(f));
  else if (std::fabs(f) >= 0.01) len = snprintf(&result[0],DEF_SSO,"%2.2f",f);
  else return "0";
  result.resize(len);
  return result;
}

std::string etos(const double f,int w,int p,bool sign)
{
  std::string result(DEF_SSO, '\0');
  int len;
  if (sign) w -= 1; //take off char for sign
  len = sign ?
    snprintf(&result[0],DEF_SSO,"%+*.*e",w,p,f):
    snprintf(&result[0],DEF_SSO,"%*.*e",w,p,f);
  result.resize(len);
  return result;
}

//-----------------------------------------------------------------------
// ctos - complex to std::string
//-----------------------------------------------------------------------
std::string ctos(const cmplex c)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"(%f,%f)",std::real(c),std::imag(c));
  result.resize(len);
  return result;
}

std::string ctos(const cmplex c,int p,bool formatted)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = formatted ?
      snprintf(&result[0],DEF_MAXSTR,"(%.*f,%.*f)",p,std::real(c),p,std::imag(c)):
      snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f",p,std::real(c),p,std::imag(c));
  result.resize(len);
  return result;
}

std::string ctos(const cmplex c,int w,int p, bool a,bool b)
{
  return "(" + dtos(std::real(c),w,p,a,b) + "," +  dtos(std::imag(c),w,p,a,b) + ")";
}

std::string cmtos(const cmat33 & f, int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f %.*f || %.*f %.*f %.*f || %.*f %.*f %.*f",
      p,std::abs(f(0,0)),p,std::abs(f(0,1)),p,std::abs(f(0,2)),
      p,std::abs(f(1,0)),p,std::abs(f(1,1)),p,std::abs(f(1,2)),
      p,std::abs(f(2,0)),p,std::abs(f(2,1)),p,std::abs(f(2,2)));
  result.resize(len);
  return result;
}

//-----------------------------------------------------------------------
// btos - bool to std::string
//-----------------------------------------------------------------------

std::string bvtos(const sv_bool& f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += btos(f[i]) + " ";
  str.pop_back();
  return str;
}

//-----------------------------------------------------------------------
// btos - bool to "bool"
//-----------------------------------------------------------------------
std::string btos(const bool i)
{
  return i ? "true":"false";
}

std::string otos(const char i) //optional to string
{
  if (i == 'Y') return "true";
  if (i == 'N') return "false";
  if (i == '?') return "?";
  return "";
}

//-----------------------------------------------------------------------
// ivtos - vector to std::string
//-----------------------------------------------------------------------
std::string ivtos(const std::pair<int,int> & f)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%d %d",f.first,f.second);
  result.resize(len);
  return result;
}

std::string ivtos(const ivect3 & f)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%d %d %d",f[0],f[1],f[2]);
  result.resize(len);
  return result;
}

std::string ivtos(const sv_int & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size(); i++) str += itos(f[i]) + " ";
  str.pop_back();
  return str;
}

std::string ivtos(const sv_int & f,int w,bool rj,bool sign)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size(); i++) str += itos(f[i],w,rj,sign) + " ";
  str.pop_back();
  return str;
}

std::string ivtos(const sv_int & f, std::string sep)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size(); i++) str += itos(f[i]) + sep;
  str.pop_back();
  return str;
}

std::string ivtos(const af_int f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size(); i++) str += itos(f[i]) + " ";
  str.pop_back();
  return str;
}

//-----------------------------------------------------------------------
// dvtos, vector to string
//-----------------------------------------------------------------------
std::string dvtos(const std::pair<double,double> & f)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%f %f",f.first,f.second);
  result.resize(len);
  return result;
}

std::string dvtos(const dvect3 & f)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%f %f %f",f[0],f[1],f[2]);
  result.resize(len);
  return result;
}

std::string dvtos(const dvect3 & f,int w, int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%*.*f %*.*f %*.*f",w,p,f[0],w,p,f[1],w,p,f[2]);
  result.resize(len);
  return result;
}

std::string dvtos(const dvect3 & f,int w, int p,bool sign,char ch)
{ return dtos(f[0],w,p,sign) + std::string(1,ch) + dtos(f[1],w,p,sign) + std::string(1,ch) + dtos(f[2],w,p,sign) ; }

std::string dvtos(const dvect3 & f,int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f %.*f",p,f[0],p,f[1],p,f[2]);
  result.resize(len);
  return result;
}

std::string dvtos(const scitbx::af::double6 & f,int w,int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%*.*f %*.*f %*.*f %*.*f %*.*f %*.*f",
       w,p,f[0],w,p,f[1],w,p,f[2],w,p,f[3],w,p,f[4],w,p,f[5]);
  result.resize(len);
  return result;
}

std::string dvtos(const af_double & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i]) + " ";
  str += dtos(f[f.size()-1]);
  return str;
}

std::string dvtos(const af_double & f,int w, int p)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],w,p) + " ";
  str += dtos(f[f.size()-1],w,p);
  return str;
}

std::string dvtos(const sv_double & f,int p, std::string sep)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],p) + sep;
  str += dtos(f[f.size()-1],p);
  return str;
}

std::string dvtos(const sv_double & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i]) + " ";
  str += dtos(f[f.size()-1]);
  return str;
}

std::string dvtos(const sv_double & f,int w, int p)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],w,p) + " ";
  str += dtos(f[f.size()-1],w,p);
  return str;
}

std::string dvtos(const sv_double & f,int p)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],p) + " ";
  str += dtos(f[f.size()-1],p);
  return str;
}

//-----------------------------------------------------------------------
// mtos - matrix to std::string
//-----------------------------------------------------------------------

std::string dmtos(const dmat33 & f, int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f %.*f || %.*f %.*f %.*f || %.*f %.*f %.*f",
      p,f(0,0),p,f(0,1),p,f(0,2),
      p,f(1,0),p,f(1,1),p,f(1,2),
      p,f(2,0),p,f(2,1),p,f(2,2));
  result.resize(len);
  return result;
}

std::string dmtos(const dmat33 & f, int w, int p)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%*.*f %*.*f %*.*f  %*.*f %*.*f %*.*f  %*.*f %*.*f %*.*f",
      w,p,f(0,0),w,p,f(0,1),w,p,f(0,2),
      w,p,f(1,0),w,p,f(1,1),w,p,f(1,2),
      w,p,f(2,0),w,p,f(2,1),w,p,f(2,2));
  result.resize(len);
  return result;
}

std::string dmtos(const dmat33 & f)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%f %f %f || %f %f %f || %f %f %f",
      f(0,0),f(0,1),f(0,2),
      f(1,0),f(1,1),f(1,2),
      f(2,0),f(2,1),f(2,2));
  result.resize(len);
  return result;
}

//-----------------------------------------------------------------------
// printf - printf to string
//-----------------------------------------------------------------------
std::string snprintftos(const char* format, ...)
{
    // Create a string filled with null characters, to be used as a buffer.
    std::string result(DEF_MAXSTR, '\0');
    va_list arglist;
    va_start(arglist, format);
    // Use vsnprintf to format the string into the buffer.
    int len = vsnprintf(&result[0], result.size() + 1, format, arglist);
    va_end(arglist);
    // Check if the formatted length exceeds the buffer size.
    if (len < 0) {
        throw std::runtime_error("Error formatting string");
    } else if (static_cast<size_t>(len) >= result.size()) {
        // If the buffer size was not sufficient, resize the result to fit.
        result.resize(len);
        va_start(arglist, format);
        vsnprintf(&result[0], result.size() + 1, format, arglist);
        va_end(arglist);
    }
    // Resize to the actual length of the formatted string.
    result.resize(len);
#if 0
    // If you are looking for a specific character ('$' in this case), truncate at its position.
    size_t pos = result.find('$');
    if (pos != std::string::npos) {
        return result.substr(0, pos);
    }
#endif
    return result;
}

//-----------------------------------------------------------------------
// nN - nN to string
//-----------------------------------------------------------------------
std::string nNtos(int n, int N)
{
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR," [#%d/#%d]",n,N);
  result.resize(len);
  return result;
}

std::string svtos(const sv_string & f)
{
  std::string str;
  for (int i = 0; i < f.size(); i++) str += f[i] + " ";
  return str;
}

std::string svtos(const af_string f)
{
  std::string str;
  for (int i = 0; i < f.size(); i++) str += f[i] + " ";
  return str;
}

//-----------------------------------------------------------------------
// to stream, with rounding
//-----------------------------------------------------------------------

std::string dtoss(const double f, int p)
{
  // Check if f is an integer
  if (std::fmod(f, 1) == 0) {
      return std::to_string(static_cast<int>(f));
  }
  // Get the formatted string
  std::string result(DEF_SSO, '\0');
  int len = snprintf(&result[0],DEF_SSO,"%.*f",p,f);
  if (len >= DEF_SSO)
  {
    result.resize(DEF_MAXSTR);
    len = snprintf(&result[0],DEF_MAXSTR,"%.*f",p,f);
  }
  result.resize(len);
  return result;
}

std::string dvtoss(const dvect3& f,int p)
{
  if (f[0] == 0 and f[1] == 0)
  {
    if (f[2] == 0) return "0 0 0";
    if (f[2] == 1) return "0 0 1";
  }
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f %.*f",p,f[0],p,f[1],p,f[2]);
  result.resize(len);
  return result;
}

std::string dmtoss(const dmat33& f,int p)
{
  if (f[0] == 1 and f[1] == 0 and f[2] == 0 and
      f[3] == 0 and f[4] == 1 and f[5] == 0 and
      f[6] == 0 and f[7] == 0 and f[8] == 1)  return "1 0 0  0 1 0  0 0 1";
  std::string result(DEF_MAXSTR, '\0');
  int len = snprintf(&result[0],DEF_MAXSTR,"%.*f %.*f %.*f  %.*f %.*f %.*f  %.*f %.*f %.*f",
      p,f(0,0),p,f(0,1),p,f(0,2),
      p,f(1,0),p,f(1,1),p,f(1,2),
      p,f(2,0),p,f(2,1),p,f(2,2));
  result.resize(len);
  return result;
}

std::string afstringtos(af_string lines)
{
  std::string txt;
  for (auto& line : lines)
    txt += line + "\n";
  return txt;
}


//-----------------------------------------------------------------------
// btos - bool to std::string
//-----------------------------------------------------------------------
std::string on_off(const bool i)
{
  return i ? "ON ":"OFF";
}

std::string btoch(const bool i)
{
  return i ? "Y":"N";
}

std::string btos5(const bool i)
{
  return i ? "true ":"false";
}

//-----------------------------------------------------------------------
// stolower - std::string to lowercase
//-----------------------------------------------------------------------
std::string stolower(std::string s)
{
  std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return tolower(c); });
  return s;
}

//-----------------------------------------------------------------------
// stoupper - std::string to uppercase
//-----------------------------------------------------------------------
std::string stoupper(std::string s)
{
  std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
  return s;
}
//-----------------------------------------------------------------------
// width for output of numbers
//-----------------------------------------------------------------------
int itow(int f,int m)
{
  if (f <= 1) return m;
  return static_cast<int>(std::max(std::log10(f)+1,static_cast<double>(m)));
}

}
