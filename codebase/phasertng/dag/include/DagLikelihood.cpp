//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/math/likelihood/llgi/function.h>
#include <phasertng/math/likelihood/llgi/helper.h>
#include <future>
#include <phasertng/src/SpaceGroup.h>

#define PHASERTNG_FAST_INTERPV

namespace phasertng {
namespace dag {

  LikelihoodDataType
  DagDatabase::work_node_likelihood(//of working node
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      pod::FastPoseType* ecalcs_sigmaa_pose
    )
  {
    PHASER_ASSERT(ecalcs_sigmaa_pose->sigmaa.size() == ecalcs_sigmaa_pose->ecalcs.size());
    if (ecalcs_sigmaa_pose->precalculated)
      PHASER_ASSERT(ecalcs_sigmaa_pose->sigmaa.size() == REFLECTIONS->NREFL);
    PHASER_ASSERT(WORK->HALL.size());
    PHASER_ASSERT(WORK->HALL == REFLECTIONS->SG.HALL);
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    PHASER_ASSERT(WORK->fraction_scattering(ztotalscat)<=1);
    WORK->FSS = 'N';
    std::tie(WORK->LLG) = combined_tgh(
        REFLECTIONS,
        SELECTED,
        EPSILON,
        NTHREADS,
        USE_STRICTLY_NTHREADS,
        ecalcs_sigmaa_pose
      );
    WORK->LLG *= -1; //we don' want the -llg, which is only for refinement
    WORK->LLG *= REFLECTIONS->oversampling_correction();
    return std::make_tuple(WORK->LLG);
  }

  LikelihoodDataType
  DagDatabase::combined_tgh(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      pod::FastPoseType* ecalcs_sigmaa_pose
    )
  {
    double Target = 0;
    phaser_assert(NTHREADS> 0);
    phaser_assert(REFLECTIONS->NREFL > 0);
    phaser_assert(REFLECTIONS->NREFL > NTHREADS);
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      std::tie(Target) = parallel_combined_tgh(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose
              );
    }
    else
    {
      std::vector<std::future< LikelihoodDataType > > results;
      std::vector<std::packaged_task< LikelihoodDataType(
              const_ReflectionsPtr,
              sv_bool,
              const_EpsilonPtr,
              int,
              int,
              pod::FastPoseType*
              )>> packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&DagDatabase::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose
              );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &DagDatabase::parallel_combined_tgh, this,
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose
              ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      std::tie(Target) = parallel_combined_tgh(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose
              );
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        double thread_llg = std::get<0>(result);
        Target += thread_llg;
        //ecalcs_sigmaa_pose filled internally
      }
    }
    return std::make_tuple(Target);
  }

  LikelihoodDataType
  DagDatabase::parallel_combined_tgh(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int beg,
      int end,
      pod::FastPoseType* ecalcs_sigmaa_pose
    )
  {
    double Target = 0;

    // tNCS case: curvatures (particularly vrms) should take account of correlations between
    // tNCS-related copies.  for now, rely on BFGS algorithm to compensate, but it would be better
    // to deal with this explicitly.
    const SpaceGroup* SG = &REFLECTIONS->SG;

    for (auto& entry : ENTRIES)
    {
      if (WORK->CELL_SCALE.count(entry.first) and entry.second.INTERPOLATION.in_memory())
      {
        auto val = WORK->CELL_SCALE[entry.first].VAL;
        entry.second.INTERPOLATION.UC.reset_orthogonal_cell();
        entry.second.INTERPOLATION.UC.scale_orthogonal_cell(val);
      }
    }

    auto pose_terms = WORK->pose_terms_interpolate();
    auto curl_terms = WORK->curl_terms_interpolate();
    auto gyre_terms = WORK->gyre_terms_interpolate();
    auto turn_terms = WORK->turn_terms_interpolate();
    auto gyre_curl_terms = WORK->gyre_curl_terms_interpolate();

    int nmol = WORK->TNCS_ORDER;
    bool tncs_modelled = WORK->TNCS_MODELLED;
    bool tncs_not_modelled = !WORK->TNCS_MODELLED;

    double nsymp = SG->group().order_p();
    likelihood::llgi::tgh_flags flags;
    flags.target(true).gradient(false).hessian(false);
    likelihood::llgi::function LLGI;

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;
    millnx miller(0,0,0);
    double dobs(0),feff(0),resn(0),teps(0),ssqr(0),g_drms(0),eps(0);
    bool   cent(0);
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        if (!REFLECTIONS->has_rows())
        {
          miller = REFLECTIONS->get_miller(r);
          dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
          feff =   REFLECTIONS->get_flt(labin::FEFFNAT,r);
          teps =   REFLECTIONS->get_flt(labin::TEPS,r);
          resn =   REFLECTIONS->get_flt(labin::RESN,r);
          cent =   REFLECTIONS->get_bln(labin::CENT,r);
          ssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
          eps  =   REFLECTIONS->get_flt(labin::EPS,r);
          g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1.;
        }
        else
        {
          const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
          miller = work_row->get_miller();
          dobs = work_row->get_dobsnat();
          feff = work_row->get_feffnat();
          resn = work_row->get_resn();
          teps = work_row->get_teps();
          cent = work_row->get_cent();
          ssqr = work_row->get_ssqr();
          eps =  work_row->get_eps();
          g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1.;
        }
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);

        //these are the terms that must be populated for the likelihood calculation
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);
        double sum_Esqr_search(0);

// halfR=true (look for average orientation of tNCS pair)
// when called from FRF or BRF, or from BTF or FTF before rotref,
// false otherwise (BTF or FTF after rotref, RNP)

        if (ecalcs_sigmaa_pose->precalculated)
        {
          DobsSigaEcalc = ecalcs_sigmaa_pose->ecalcs[r]*dobs;
          DobsSigaSqr = fn::pow2(ecalcs_sigmaa_pose->sigmaa[r]);
          //sum_Esqr_search not set
        }
        else
        {
          if (WORK->POSE.size())
          { //poses have tncs resolved (halfR = false)
            //if the poses come with grouped identifiers, this is a speedup
            double last_thisDobsSigaSqr = 0;
            Identifier last_id;
            int last_tncsgrp = -999;
            for (int p = 0; p < WORK->POSE.size(); p++)
            {
              const auto& pose = WORK->POSE[p];
              const auto& entry = pose.ENTRY;
              double thisDobsSigaSqr = 0;
              if (pose.IDENTIFIER == last_id and last_tncsgrp == pose.TNCS_GROUP.first)
              { //bfactors all the same in the same tncs group
                thisDobsSigaSqr = last_thisDobsSigaSqr;
              }
              else
              {
                double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
                double bfac = pose.BFAC;
                double fs = REFLECTIONS->fracscat(entry->SCATTERING);
                double drms = WORK->DRMS_SHIFT[pose.IDENTIFIER].VAL;
                thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                    ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
                last_thisDobsSigaSqr = thisDobsSigaSqr;
                last_id = pose.IDENTIFIER;
                last_tncsgrp = pose.TNCS_GROUP.first;
              }
              for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
              {
                const millnx& SymHKL = rsym.rotMiller[isym];
                dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
                cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
                cmplex scalefac = likelihood::llgi::EcalcTerm(
                    thisDobsSigaSqr,eps,nsymp, //use thisDobsSigaSqr before tncs var correction
                    SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
                cmplex thisE = scalefac * iEcalc;
                DobsSigaEcalc += thisE;
              }
              if (nmol > 1)
              { //halfR = false
                double Vterm = array_G_Vterm[r];
                if (tncs_modelled) Vterm /= nmol;
                thisDobsSigaSqr *= Vterm;
              }
              DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes Dobs
            }
          } //poses
          else if (WORK->FULL.PRESENT)
          { //poses have tncs resolved (halfR = false)
            const auto& entry = WORK->FULL.ENTRY;
            double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
            double bfac = 0; //bfactor in monostructure
            double fs = REFLECTIONS->fracscat(entry->SCATTERING);
            double drms = WORK->DRMS_SHIFT[WORK->FULL.IDENTIFIER].VAL;
            double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
              ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = dmat33(1,0,0, 0,1,0, 0,0,1)*SymHKL;
              cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                  thisDobsSigaSqr,eps,nsymp, //use thisDobsSigaSqr before tncs var correction
                  SymHKL,{0,0,0},rsym.traMiller[isym]);
              cmplex thisE = scalefac * iEcalc;
              DobsSigaEcalc += thisE;
            }
            if (nmol > 1)
            { //halfR = false
              double Vterm = array_G_Vterm[r];
              if (tncs_modelled) Vterm /= nmol;
              thisDobsSigaSqr *= Vterm;
            }
            DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes Dobs
          } //full
          //store values for map calculation etc
          //phaser_assert(dobs > 0);
         // ecalcs_pose[r] = DobsSigaEcalc/Dobs;
         // sigmaa_pose[r] = fn::sqrt(DobsSigaSqr);
        }

        //if the ecalcs_sigmaa precalculated is a subset of the poses
        //then we need to select the pose that has changed
        if (WORK->last_pose)
        {
          int p = WORK->POSE.size()-1;
          const auto& pose = WORK->POSE[p];
          const auto& entry = pose.ENTRY;
          double thisDobsSigaSqr = 0;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac = pose.BFAC;
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = WORK->DRMS_SHIFT[pose.IDENTIFIER].VAL;
          thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
                thisDobsSigaSqr,eps,nsymp, //use thisDobsSigaSqr before tncs var correction
                SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac * iEcalc;
            DobsSigaEcalc += thisE;
          }
          if (nmol > 1)
          { //halfR = false
            double Vterm = array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes Dobs
        } //poses

        if (WORK->NEXT.curl_present()) //curl only
        { //before tncs expanded
          const auto& curl = WORK->NEXT;
          const auto& entry = curl.ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = WORK->DRMS_SHIFT[curl.IDENTIFIER].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = curl_terms.Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
            cmplex scalefac = likelihood::llgi::EcalcTerm(
               thisDobsSigaSqr,eps,nsymp,
               SymHKL,curl_terms.TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac * iEcalc;
            if (nmol > 1 and tncs_not_modelled)
            { //halfR = true
              const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1;
            }
            DobsSigaEcalc += thisE;
          }
          if (nmol > 1)
          { //halfR = true
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes Dobs
        } //curls
        else if (WORK->NEXT.gyre_curl_present()) //gyre with curl
        {
#ifdef PHASERTNG_FAST_INTERPV
          double last_thisDobsSigaSqr = 0;
          Identifier last_id;
#endif
          for (int c = 0; c < WORK->NEXT.ngyre(); c++) //fixed phase relative to each other
          {
            const auto& curl = WORK->NEXT.GYRE[c];
            const auto& entry = curl.ENTRY;
            double thisDobsSigaSqr = 0;
#ifdef PHASERTNG_FAST_INTERPV
            if (curl.IDENTIFIER == last_id)
            { //can store the whole thing because bfacs are all the same (0)
              thisDobsSigaSqr = last_thisDobsSigaSqr;
            }
            else
#endif
            {
              double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
              double bfac(0);
              double fs = REFLECTIONS->fracscat(entry->SCATTERING);
              double drms = WORK->DRMS_SHIFT[curl.IDENTIFIER].VAL;
              thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
#ifdef PHASERTNG_FAST_INTERPV
              last_thisDobsSigaSqr = thisDobsSigaSqr;
              last_id = curl.IDENTIFIER;
#endif
            }
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = gyre_curl_terms[c].Q1tr*SymHKL;
              cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                 thisDobsSigaSqr,eps,nsymp,
                 SymHKL,gyre_curl_terms[c].TRA,rsym.traMiller[isym]);
              cmplex thisE = scalefac * iEcalc;
              if (nmol > 1 and tncs_not_modelled)
              { //halfR = true
                const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
                cmplex ptncs_scat = cmplxONE; //imol=0
                for (double imol = 1; imol < nmol; imol++)
                  ptncs_scat += std::exp(TWOPII*imol*theta);
                thisE *= ptncs_scat;
                thisE *= (nmol == 2) ? matrix_G(r,isym) : 1;
              }
              DobsSigaEcalc += thisE;
            }
            if (nmol > 1)
            { //halfR = true
              double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
              if (tncs_modelled) Vterm /= nmol;
              thisDobsSigaSqr *= Vterm;
            }
            DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes Dobs
          }
        } //curls
        else if (WORK->NEXT.gyre_present()) //gyre without curl
        { //orientation only
          af_cmplex ecalcs_sym(rsym.unique,{0,0});
#ifdef PHASERTNG_FAST_INTERPV
          double last_thisDobsSigaSqr = 0;
          Identifier last_id;
#endif
          for (int g = 0; g < WORK->NEXT.ngyre(); g++) //fixed phase relative to each other
          {
            const auto& gyre = WORK->NEXT.GYRE[g];
            const auto& entry = gyre.ENTRY;
            double thisDobsSigaSqr = 0;
#ifdef PHASERTNG_FAST_INTERPV
            if (entry->identify() == last_id)
            { //can store the whole thing because bfacs are all the same (0)
              thisDobsSigaSqr = last_thisDobsSigaSqr;
            }
            else
#endif
            {
              double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
              double bfac(0);
              double fs = REFLECTIONS->fracscat(entry->SCATTERING);
              double drms = WORK->DRMS_SHIFT[gyre.IDENTIFIER].VAL;
              thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
                  ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
#ifdef PHASERTNG_FAST_INTERPV
              last_thisDobsSigaSqr = thisDobsSigaSqr;
              last_id = entry->identify();
#endif
            }
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              cmplex& iecalcs_sym = ecalcs_sym[isym]; //reference for modification
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = gyre_terms[g].Q1tr*SymHKL;
              cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                  thisDobsSigaSqr,eps,nsymp,
                  RotSymHKL,gyre_terms[g].TRA,0); //rsym.traMiller[isym],
              cmplex thisE = scalefac*iEcalc;
              if (nmol > 1 and tncs_not_modelled)
              { //halfR = true
                const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
                cmplex ptncs_scat = cmplxONE; //imol=0
                for (double imol = 1; imol < nmol; imol++)
                  ptncs_scat += std::exp(TWOPII*imol*theta);
                //iecalcs_sym *= ptncs_scat;
                //iecalcs_sym *= (nmol == 2) ? matrix_G(r,isym) : 1;
                thisE *= ptncs_scat;
                thisE *= (nmol == 2) ? matrix_G(r,isym) : 1;
              }
              iecalcs_sym += thisE;
            } //gyres
            if (nmol > 1)
            { //halfR = true
              double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
              if (tncs_modelled) Vterm /= nmol;
              thisDobsSigaSqr *= Vterm;
            }
            DobsSigaSqr += thisDobsSigaSqr;
            //gyres have fixed relative phase for each symmetry component
            //ecalcs_sym is summed over all gyres, for one symmetry operator
          }
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double Esqr_search = std::norm(ecalcs_sym[isym]);
            sum_Esqr_search += Esqr_search;
          }
//below does the Rice function, taking out the largest structure factor
          double max_Esqr_search(0);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double Esqr_search = std::norm(ecalcs_sym[isym]);
            max_Esqr_search = std::max(max_Esqr_search,Esqr_search);
          }
          //if this is a pure rotation (gyre), then make it a Rice function
          //by fixing the phase of the largest structure factor
          if (std::norm(DobsSigaEcalc) == 0 and sum_Esqr_search > 0)
          {
            sum_Esqr_search -= max_Esqr_search;
            phaser_assert(max_Esqr_search >= 0); //numerical
            DobsSigaEcalc = { std::sqrt(max_Esqr_search), 0 }; //makes phasedEsqr
          }
//done
        }
        else if (WORK->NEXT.turn_present())
        { //orientation only
          af_cmplex ecalcs_sym(rsym.unique,{0,0});
          {{
          const auto&   turn = WORK->NEXT;
          const auto&   entry = turn.ENTRY;
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = WORK->DRMS_SHIFT[turn.IDENTIFIER].VAL;
          double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
              ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            cmplex& iecalcs_sym = ecalcs_sym[isym]; //reference for modification
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = turn_terms.Q1tr*SymHKL;
            cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
            double scalefac = likelihood::llgi::EcalcSqrTerm(
                thisDobsSigaSqr,eps,nsymp); //without variance correction
            phaser_assert(scalefac>=0);
            cmplex thisE = std::sqrt(scalefac)*iEcalc;
            if (nmol > 1 and tncs_not_modelled)
            { //halfR=true
              const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
              cmplex ptncs_scat = cmplxONE; //imol=0
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              thisE *= ptncs_scat;
              thisE *= (nmol == 2) ? matrix_G(r,isym) : 1;
            }
            iecalcs_sym += thisE;
          }
          if (nmol > 1)
          { //halfR = true
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //with variance corrrection
          }}
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double Esqr_search = std::norm(ecalcs_sym[isym]);
            sum_Esqr_search += Esqr_search;
          }
//below does the Rice function
          double max_Esqr_search(0);
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double Esqr_search = std::norm(ecalcs_sym[isym]);
            max_Esqr_search = std::max(max_Esqr_search,Esqr_search);
          }
          //if this is a pure rotation (gyre), then make it a Rice function
          //by fixing the phase of the largest structure factor
          if (std::norm(DobsSigaEcalc) == 0 and sum_Esqr_search > 0)
          {
            sum_Esqr_search -= max_Esqr_search;
            phaser_assert(max_Esqr_search >= 0); //numerical
            DobsSigaEcalc = { std::sqrt(max_Esqr_search), 0 }; //makes phasedEsqr
          }
//done
        }

        //====
        //LLGI calculation
        //====
        double V = teps - DobsSigaSqr + sum_Esqr_search;
        double eobs = feff/resn;
        LLGI.target_gradient_hessian(eobs, DobsSigaEcalc, teps, V, cent, flags);
        Target -= LLGI.LL; // This is already log-likelihood-gain so no wll term required

        if (V <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        static std::mutex mutex;
        static int counter(0);
        if (V <= 0)
        {
          const std::lock_guard<std::mutex> lock(mutex);
          //threadsafe
          //lock_guard for cleanup of incomplete mutex.lock() with mutex.unlock()
          counter++;
          std::cout << "\n-------------------- " << counter <<std::endl;
          std::cout << "DagLikelihood Debugging reflection #" << r << std::endl;
          std::cout << "V=" << LLGI.variance() << std::endl;
          std::cout << "LL=" << LLGI.LL << std::endl;
          std::cout << "Miller=(" << miller[0] << "," << miller[1] << "," << miller[2] << ")"<< std::endl;
          std::cout << "Eobs=" << eobs << std::endl;
          std::cout << "Dobs=" << dobs << std::endl;
          std::cout << "|Ecalc|=" << std::abs(DobsSigaEcalc) << std::endl;
          std::cout << "DobsSigaSqr="  << DobsSigaSqr << std::endl;
          std::cout << "Centric=" << cent << std::endl;
          std::cout << "scattering=" << REFLECTIONS->get_ztotalscat() << std::endl;
          std::cout << "ssqr=" << ssqr << std::endl;
          std::cout << "reso=" << 1/std::sqrt(ssqr) << std::endl;
          std::cout << "tncs-g_drms=" << g_drms << std::endl;
          std::cout << "tncs-epsfac=" << teps << std::endl;
          if (array_G_Vterm.size())
          std::cout << "tncs-g_vterm=" << array_G_Vterm[r] << std::endl;
          if (array_Gsqr_Vterm.size())
          std::cout << "tncs-gsqr_vterm=" << array_Gsqr_Vterm[r] << std::endl;
          std::cout << "Dobs=" << dobs << std::endl;
          std::cout << "--------------------" << std::endl;
        }//debug
        PHASER_ASSERT(V > 0);
#endif
      } //selected
    } //reflections
    return std::make_tuple(Target);
  }

}} //phasertng
