//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/ReflColsMap.h>
#include <phasertng/math/subgroups.h>

//#define PHASERTNG_DEBUG_SGX

namespace phasertng {

  //Space_group_expansion
  void Phasertng::runSGX()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!(REFLECTIONS->prepared(reflprep::DATA) or REFLECTIONS->prepared(reflprep::ANISO)))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (data,aniso)");

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Initial Data"));
    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Initial"));

    std::unique_ptr<ReflColsMap> SGXREFL(new ReflColsMap());

    {{
    out::stream where = out::SUMMARY;
    if (input.value__string.at(".phasertng.space_group_expansion.hall.").is_default() and
        input.value__string.at(".phasertng.space_group_expansion.spacegroup.").is_default())
       // not hermann_mauguin_symbol because this duplicates the full setting in the hall symbol
    {
      logTab(where,"Input Space Group: " + REFLECTIONS->SG.str());
      logTab(where,"Input Unit Cell:   " + REFLECTIONS->UC.str());
      logBlank(where);
      logTab(where,"No change to unit cell or space group");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      int order_z_expansion_ratio(1);
      input.value__int_number.at(".phasertng.space_group_expansion.order_z_expansion_ratio.").set_value(order_z_expansion_ratio);
      return;
    }
    }}

    //return above excludes below if not require
    auto expansion_hall = input.value__string.at(".phasertng.space_group_expansion.hall.").value_or_default();
    //spacegroup in hall representation has precedence
    //should be the same
    if (!input.value__string.at(".phasertng.space_group_expansion.spacegroup.").is_default())
    {
      auto expansion_hm = input.value__string.at(".phasertng.space_group_expansion.hermann_mauguin_symbol.").value_or_default();
      auto hall = SpaceGroup(expansion_hm).HALL;
      if (!input.value__string.at(".phasertng.space_group_expansion.hall.").is_default() and
          expansion_hall != hall)
      {
        logWarning("Expansion space group by hall and hermann maugin symbol are not the same");
      }
      if (input.value__string.at(".phasertng.space_group_expansion.hall.").is_default())
        expansion_hall = hall;
    }
    SpaceGroup  expansion_spacegroup(expansion_hall);
    double ihires = REFLECTIONS->data_hires();
    cctbx::sgtbx::change_of_basis_op cb_op_new = expansion_spacegroup.cb_op();
    auto newgrp = expansion_spacegroup.group().change_basis(cb_op_new);
    std::string newhall("Hall: " + newgrp.match_tabulated_settings().hall());
    auto newsg = SpaceGroup(newhall);
    UnitCell newuc(REFLECTIONS->UC.cctbxUC.change_basis(cb_op_new).parameters());
    int iZ = REFLECTIONS->SG.group().order_z();
    int oZ = expansion_spacegroup.group().order_z();
    int order_z_expansion_ratio = iZ/oZ;
    input.value__int_number.at(".phasertng.space_group_expansion.order_z_expansion_ratio.").set_value(order_z_expansion_ratio);

    {{
    out::stream where = out::SUMMARY;
    logTab(where,"Input Space Group: " + REFLECTIONS->SG.str());
    logTab(where,"Input Unit Cell:   " + REFLECTIONS->UC.str());
    logTab(where,"Reindex Space Group: " + expansion_spacegroup.str());
    logTab(where,"Final Space Group: " + newsg.str());
    logTab(where,"Final Unit Cell:   " + newuc.str());
    logTab(where,snprintftos(
        "Order of Data Expansion : %d:%d (%d)",iZ,oZ,order_z_expansion_ratio));
    logBlank(where);
    }}

    bool found_subgroup(false);
    std::vector<cctbx::sgtbx::space_group_type> sysabsall = groups_sysabs(REFLECTIONS->SG.type());
//get desperate for the found_subgroup, do expansion for both
    std::vector<cctbx::sgtbx::space_group_type> expabsall = groups_sysabs(expansion_spacegroup.type());
    for (int s = 0; s < sysabsall.size() and !found_subgroup; s++)
    {
      for (int t = 0; t < expabsall.size() and !found_subgroup; t++)
      {
        if (expabsall[t].hall_symbol() == sysabsall[s].hall_symbol())
        {
          found_subgroup = true;
        }
      }
    }
    found_subgroup = found_subgroup and cb_op_new.is_identity_op(); //and there is no reindexing required
    //if there is a match in the pointgroup, change the spacegroup to the new spacegroup

    if (found_subgroup)
    {
      out::stream where = out::LOGFILE;
      REFLECTIONS->SG = expansion_spacegroup;
      logTab(where,"New Space Group is in same Point-Group");
      logTab(where,"No data expansion required");
      logBlank(where);
      //else just use the expansion cell as determined locally here
      {
        logTab(where,"Add bins");
        Bin bin = REFLECTIONS->setup_bin(
            input.value__mtzcol.at(".phasertng.labin.bin.").value_or_default(),
            input.value__int_paired.at(".phasertng.bins.reflections.range.").value_or_default_min(),
            input.value__int_paired.at(".phasertng.bins.reflections.range.").value_or_default_max(),
            input.value__int_number.at(".phasertng.bins.reflections.width.").value_or_default()
            );
        logTabArray(out::VERBOSE,bin.logBin("Reset binning"));
      }
      logBlank(where);
    }
    else
    {
      out::stream where = out::VERBOSE;
      auto all_spacegroups = subgroups_sysabs(REFLECTIONS->SG.type(),false);
      std::set<std::string> unique;
      std::vector<cctbx::sgtbx::space_group_type> sglist = subgroups(REFLECTIONS->SG.type(),false,unique);
      if (sglist.size())
      {
        logUnderLine(where,"Subgroups of space group");
        logTab(where,snprintftos(
            "%-3s %-13s %c %-35s",
            "Z","Space Group",' ',"Hall Symbol"));
        logTab(where,"----");
        int last_nsymz(0);
        int count_settings(0);
        //UnitCell newuc(REFLECTIONS->UC.cctbxUC.change_basis(cb_op_dat2new).parameters());
        for (auto item : all_spacegroups)
        {
          SpaceGroup  SGLIST("Hall: " + item.hall_symbol());
          if (SGLIST == expansion_spacegroup)
         // if (SGLIST.HALL.rfind(expansion_refhall) == 0)
            count_settings++;
          int this_nsymz = SGLIST.group().order_z();
          if (last_nsymz && last_nsymz != this_nsymz)
            logTab(where,"---");
          logTab(where,snprintftos(
              "%-3s %-13s %-35s",
              (last_nsymz != SGLIST.group().order_z()) ? itos(SGLIST.group().order_z()).c_str() : "",
              SGLIST.CCP4.c_str(),
              SGLIST.HALL.c_str()));
          last_nsymz = this_nsymz;
        }
        logTab(where,"----");
        logBlank(where);
        where = out::LOGFILE;
        logUnderLine(where,"Subgroup Check");
        if (count_settings == 0)

          throw Error(err::FATAL,"Space group is not a subgroup of data space group: Check setting");
        else if (count_settings > 1)
          throw Error(err::FATAL,"Space group has more than one setting and setting not defined");
        else if (count_settings == 1)
        {
          out::stream where = out::VERBOSE;
          logTab(where,"Space group is a subgroup of data space group");
          logTab(where,"Expand data to lower symmetry");
          //extension has to be done in ReflColsMap format - Column-data
          logTab(where,"Change reflection class");
          //SGXREFL = REFLECTIONS; //base classes
          SGXREFL->copy_and_reformat_data(REFLECTIONS.get());
          logTab(where,"Expand data to p1");
          bool change_spacegroup_to_p1(false);
          SGXREFL->expand_to_p1(change_spacegroup_to_p1);
          logTab(where,"Reindex");
          SGXREFL->reindex(expansion_spacegroup); //not reference setting
          logTab(where,"Reduce data to new spacegroup");
          SGXREFL->reduce_to_space_group(newsg);
          SGXREFL->SG = newsg;
          SGXREFL->UC = newuc;
          if (Suite::Extra())
            logTabArray(out::TESTING,SGXREFL->logReflections("Expanded"));
          logTab(where,"Copy data back to reflection class");
          REFLECTIONS->copy_and_reformat_data(SGXREFL.get()); //replace REFLECTIONS data structure
        }
      }
    }//in pointgroup or not

//cent in copy_and_reformat_data
//eps in copy_and_reformat_data
//phsr in copy_and_reformat_data
//ssqr  in change_unitcell //because of unit cell changes
//bin  in change_unitcell //because of unit cell changes

    {{
    out::stream where = out::VERBOSE;
    logTab(where,"Check unique under symmetry");
    auto output = REFLECTIONS->unique_under_symmetry_selection();
    logTabArray(where,output);
    bool map_miller_to_asu(false);
    if (map_miller_to_asu)
    {
      logTab(where,"Reflections moved to reciprocal asymmetric unit");
      REFLECTIONS->move_to_reciprocal_asu();
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logTab(where,"Reindexed Space Group: " + REFLECTIONS->SG.str());
    logTab(where,"Reindexed Unit Cell:   " + REFLECTIONS->UC.str());
    logTab(where,"Reindexed Reflections: " + itos(REFLECTIONS->NREFL));
    REFLECTIONS->set_data_resolution();
    double ohires = REFLECTIONS->data_hires();
    logBlank(where);
    logTab(where,"Resolution Check");
    logTab(where,"Input: " + dtos(ihires,12));
    logTab(where,"Final: " + dtos(ohires,12));
    PHASER_ASSERT(ohires > ihires-DEF_PPM and ohires < ihires+DEF_PPM);
    if (!REFLECTIONS->check_sort_in_resolution_order())
    { //only do if necessary, for speed
      logEllipsisOpen(out::LOGFILE,"Sort on resolution");
      REFLECTIONS->sort_in_resolution_order();
      logEllipsisShut(out::LOGFILE);
    }
    logBlank(where);
    }}

    //must change the identifier because we have changed the reflection list
    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::LOGFILE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::SGX);
    //clang complains PHASER_ASSERT(typeid(*REFLECTIONS).name() == typeid(REFLCOLSMAP).name());
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Reindexed"));
    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Reindexed"));

    //change the space group on the nodes
    DAGDB.NODES.set_hall(REFLECTIONS->SG.HALL);
    DAGDB.NODES.apply_unit_cell(REFLECTIONS->UC.cctbxUC);
    DAGDB.reset_entry_pointers(DataBase()); //for spacegroup
    logTabArray(out::VERBOSE,REFLECTIONS->logMtzHeader(""));

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
