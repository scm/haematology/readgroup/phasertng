//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PattersonCoefficients_class__
#define __phasertng_PattersonCoefficients_class__
#include <phasertng/main/includes.h>
#include <phasertng/main/pointers.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>

namespace phaser {
class RefineSAD;
}

namespace phasertng {

namespace likelihood { namespace isad { class sigmaa; }}
class FileSystem;

  namespace pod {
    class shelxc
    {
      public:
        int    H = 0;
        int    K = 0;
        int    L = 0;
        double F = 1;
        double SIGMA = 1;
        int    PHI = 0;
      public:
        shelxc () {}
        shelxc(millnx MILLER,double F_,double SIGMA_,int PHI_) :
            H(MILLER[0]),K(MILLER[1]),L(MILLER[2]),F(F_),SIGMA(SIGMA_),PHI(PHI_) {}
    };
  } //pod

  class PattersonCoefficients
  {
    private:
      af_millnx MILLER; //these will be the miller indices for the Patterson map
      af_double IOBS_EQUIVALENT; //these will be the coefficents for the Patterson map
      af_double SIGIOBS_EQUIVALENT; //these could be sigmas for the Patterson map coefficients
      af_bool   PRESENT;
      af::shared<pod::shelxc>   SHELXC; //not always filled
      std::pair<std::string,std::string> LABOUT;
      double LL = 0;
      SpaceGroup SG;
      UnitCell UC;

    public:
    PattersonCoefficients(SpaceGroup,UnitCell,af_millnx);
    void init();

    double LLconst() { return LL; }
    int nrefl();
    af_millnx get_miller_array();
    af_double get_intensity_array();
    af_cmplex get_cmplex_intensity_array();
    af_double get_sigma_array();

    void
    LLdLL_by_dUsqr_at_0(
      const_ReflectionsPtr,
      const sv_bool&,
      std::string atomtype,
      double fp,
      double fdp,
      double dBscat,
      likelihood::isad::sigmaa ISAD_SIGMAA
    );

    void
    expectZHsigmaZH(
      const_ReflectionsPtr,
      const sv_bool&,
      likelihood::isad::sigmaa
    );

    void
    anomdiff(
      const_ReflectionsPtr
    );

   void
   LESSTF1(
     phaser::RefineSAD&,
     std::string,
     double
   );

    void WriteHkl(FileSystem);
  };
}
#endif
