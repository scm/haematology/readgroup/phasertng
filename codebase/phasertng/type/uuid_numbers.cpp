//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/uuid_numbers.h> //definition of uuid
#include <sstream>
#include <iomanip>
#include <set>

namespace phasertng {
namespace type {

  uuid_numbers::uuid_numbers(
      const std::string parameter,
      const bool pminset_, const uuid_t pminval_,
      const bool pmaxset_, const uuid_t pmaxval_,
      const sv_uuid dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  uuid_numbers::set_value(sv_uuid other)
  {
    user = true;
    uvalue = other;
  }

  void
  uuid_numbers::set_value(const sv_string& vcards,const sv_uuid& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  void
  uuid_numbers::push_back(const uuid_t& other)
  {
    user = true;
    uvalue.push_back(other);
  }

  //virtual
  bool
  uuid_numbers::is_default() const
  { return uvalue == dephalt; }

  void
  uuid_numbers::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  uuid_numbers::to_python()
  { return unparse_parameter(false); }

  void
  uuid_numbers::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    uvalue.resize(txt.size());
    for (int i = 0; i < txt.size(); i++)
      uvalue[i] = std::stoull(txt[i]);
  }

  int
  uuid_numbers::size() const
  { return value_or_default().size(); }

  //treat the long form of the integer as the special case
  //as mostly it will not be used
  //primarily for identifiers modlid,reflid
  sv_uuid
  uuid_numbers::value_or_default() const
  { return user ? uvalue : dephalt; }

  uuid_t
  uuid_numbers::value_or_default(int i) const
  {
   // phaser_assert(i < value_or_default().size());
    return user ? uvalue[i] : dephalt[i];
  }

  sv_uuid
  uuid_numbers::value_or_default_unique() const
  {
    sv_uuid tmp = value_or_default();
    std::set<uuid_t> tmp_set;
    for (auto item : tmp)
      tmp_set.insert(item);
    tmp.clear();
    for (auto item : tmp_set)
      tmp.push_back(item);
    return tmp; //sorted and unique
  }

  std::string
  uuid_numbers::unparse_parameter(bool phil) const
  {
    if (!value_or_default().size())
      return None();
    std::stringstream ss;
    for (int i = 0; i < value_or_default().size(); i++)
      ss << value_or_default()[i] << " ";
    return value_or_default().size() ? ss.str() : None();
  }

  std::string
  uuid_numbers::show_default(bool phil) const
  {
    if (!dephalt.size())
      return None();
    std::stringstream ss;
    for (int i = 0; i < dephalt.size(); i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
