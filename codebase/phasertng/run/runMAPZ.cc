//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/rotation/rotdist.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/site/AnglesRandom.h>
#include <phasertng/math/mean_sigma_top.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Map_docking_zscore
  void Phasertng::runMAPZ()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::LOGFILE,"No poses");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.set_of_entries();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    DAGDB.reset_entry_pointers(DataBase());
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    unsigned random_seed = 0;

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    double io_nprint = input.value__int_number.at(".phasertng.zscore_equivalent_map.maximum_printed.").value_or_default();

    pod::parent_mean_sigma_top stats;
    {{
    int nrand  = input.value__int_number.at(".phasertng.zscore_equivalent_map.nrand.").value_or_default();
    logEllipsisOpen(out::LOGFILE,"Generating random angles (" + itos(nrand) + ")");
    AnglesRandom rotList(nrand,random_seed++);
    logEllipsisShut(out::LOGFILE);

    double best_llg = std::numeric_limits<double>::lowest();
    dvect3 best_site;
    int n(1),N(DAGDB.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      out::stream where = out::LOGFILE;
      dag::Node* node = DAGDB.WORK;
      std::string nN = nNtos(n++,N);
      logUnderLine(where,"Map LLG Z-score" + nN);
      logTabArray(where,node->logPose("Known components"));
      double original_llg = node->MAPLLG; //need to calculate as it is the value for the TFZ==
      DAGDB.work_node_phased_likelihood(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          NTHREADS,
          USE_STRICTLY_NTHREADS);
      node->MAPLLG = original_llg; //numerical instablity means this may be different
      node->generic_flag = true; //generic flag
      node->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
      auto original_pose = node->POSE;
      int l(1); int L(rotList.count_sites());
      logProgressBarStart(where,"Scoring random angles",L,1,false);
      rotList.restart();
      sv_double v;
      while (!rotList.at_end())
      {
        std::string lL = nNtos(l++,L);
        dvect3 next_site = rotList.next_site();
        dmat33 ROT = zyz_matrix(next_site);
        for (int p = 0; p < node->POSE.size(); p++)
        {
          //cumulative rotation, it is random so doesn't matter
          //rotate all the poses by the same rotation
          //this will be about the centre of mass of each separately, not the com of the complex
          //so there will be packing problems in the case of multiple components
          //AJM TODO rotate around centre of mass of the pose complex
          dmat33 rot = ROT * zyz_matrix(node->POSE[p].EULER);
          node->POSE[p].EULER = scitbx::math::euler_angles::zyz_angles(rot);
        }
        DAGDB.work_node_phased_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            NTHREADS,
            USE_STRICTLY_NTHREADS);
        //all the same space group
        v.push_back(node->MAPLLG);
        if (node->MAPLLG > best_llg)
        {
          best_llg = node->MAPLLG;
          best_site = next_site;
          if (Suite::Extra())
            logProgressBarAgain(where,"Random Best llg = "+dtos(node->MAPLLG)+" ("+dvtos(best_site,6,3)+")"+lL);
        }
        logProgressBarNext(where);
      }
      logProgressBarEnd(where);
      dvect3 mst = math::mean_sigma_top(v);
      logTab(where,"Mean and Standard Deviation (brute search):");
      logTab(where,"Random Mean LLG (Sigma):  " + dtos(mst[0],10,2) + "   (" + dtos(mst[1],5,2) + ")");
      logTab(where,"Random Highest LLG (TFZ): " + dtos(mst[2],10,2) + "   (" + dtos((mst[2]-mst[0])/mst[1],5,2) + ")");
      logTab(where,"Random Best Site = (" + dvtos(best_site,6,4) + ")");
      //add the growing list to the DAG, note that this will not be sorted overall
      //no selection for top peaks, recore them all
      node->generic_float = node->ZSCORE; //store the old zscore for printing (but not printed)
      node->MAPLLG = original_llg;
      node->POSE = original_pose; //revert
      node->FSS = false;
      double zscore = (original_llg-mst[0])/mst[1];
      node->ZSCORE = zscore;
      node->ANNOTATION += " MZ=" + dtos(zscore,1);
      logTab(where,"PTF Z-score : " + dtos(node->generic_float,7,2));
      logTab(where,"Map Z-score : " + dtos(zscore,7,2));
      DAGDB.reset_work_entry_pointers(DataBase());
      stats.mst[n-2] = mst; //map
    } //dagdb
    }}

    //DAGDB.reset_entry_pointers(DataBase()); //unnecessary, done each time

    {{
    out::stream where = out::TESTING;
    if (Suite::Store() >= where)
    {
      int n(1),N(DAGDB.size());
      phaser_assert(N);
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Map LLG Z-score #" +nN);
        logTabArray(where,DAGDB.WORK->logNode(nN+""));
      }
    }
    }}

    double lowest_mean = stats.merged_lowest_mean();
    double top = DAGDB.NODES.top_mapllg();
    {{
    int w = itow(io_nprint);
    out::stream where = out::SUMMARY;
    logTableTop(where,"Map Zscore");
    DAGDB.restart(); //very important to call this to set WORK
    logTab(where,"Top/mean for percent: " + dtos(top,2) + "/" + dtos(lowest_mean));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#       Rank of the peak");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %10s %7s %7s %5s %*s %*s",
          w,"#","LLG","PTFZ","MAPZ","%",uuid_w,"modlid",uuid_w,"reflid"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      double pscore = 100.*(node->MAPLLG-lowest_mean)/(top-lowest_mean);
      logTab(where,snprintftos(
          "%-*i % 10.*f %7.2f %7.2f %5s %*s %*s",
          w,i,
          q,node->MAPLLG,
          node->generic_float,
          node->ZSCORE,
          dtos(pscore,5,1).c_str(),
          uuid_w,node->POSE.back().IDENTIFIER.string().c_str(),
          uuid_w,node->REFLID.string().c_str()
          ));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    if (DAGDB.size() > 1)
    {
      out::stream where = out::LOGFILE;
      double io_percent = input.value__percent.at(".phasertng.zscore_equivalent_map.percent.").value_or_default();
      dag::NodeList& output_dag = DAGDB.NODES;
      logEllipsisOpen(where,"Purge by percent");
      int before = output_dag.size();
      output_dag.apply_partial_sort_and_percent_mapllg(io_percent,lowest_mean);
      logEllipsisShut(where);
      logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
      double cut = (io_percent/100.)*(top-lowest_mean);
      logTab(where,"Top/Mean/Cutoff LLG: " + dtos(top,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(output_dag.size()));
      DAGDB.reset_entry_pointers(DataBase());
    }

    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
