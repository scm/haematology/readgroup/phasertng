#ifndef __phasertng_estimator_class__
#define __phasertng_estimator_class__
#include <cmath>
#include <utility>
#include <cassert>

namespace phasertng {

class Estimator
{
  private:
    double vrms_perturb = 0;

  public:
  Estimator(double vrms_perturb_ = 0) : vrms_perturb(vrms_perturb_)
  {
    //range of sensible values
    //for perturbation of original value in sigma steps around the mean
    assert(vrms_perturb >= -3.);
    assert(vrms_perturb <= 3.);
  }

  double
  chothia_and_lesk(double identity)
  {
    //if it comes in as a percent, change to fraction
    if (identity > 100.) return 0;
    if (identity < 0.) return 0;
    if (identity > 1) identity /= 100.;
    double estimate_floor = 0.8;
    double estimate = 0.40*exp(1.87*(1.0-identity));
    //no sigma for perturbation
    return std::max(estimate_floor,estimate); //floor the function
  }

  double
  oeffner(double identity, int num_residues)
  {
    //if it comes in as a percent, change to fraction
    if (identity > 1) identity /= 100.;
    assert(identity <= 1.);
    assert(identity >= 0.);
    // The use of min/max on the number of residues avoids extrapolating beyond range of well
    // populated data
    // Acta Cryst. (2013). D69, 2209-2215 page:2213
    // eVRMS = A(B + num_residues)^(1/3) * exp(C*H), H = 1-identity/100
    // A = 0.0569, B = 173, C = 1.52.
    double A = 0.0568808;
    double B =  172.53;
    double C = 1.52198;
    double estimate_floor = 0.362426;
    double estimate_sigma = 0.1965;
    double estimate =  (A * std::pow(B + std::min(std::max(num_residues, 125), 1500), 1. / 3.)) * exp(C * (1. - identity));
    //do the perturbation if set in constructor
           estimate *= (1 + vrms_perturb*estimate_sigma);
    return std::max(estimate_floor,estimate); //floor the function
  }

  double
  hatti(double identity, int num_residues, double molprobity, double resolution)
  {
    return 0.1; // :-)
  }

};

} //phasertng
#endif
