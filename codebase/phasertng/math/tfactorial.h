#ifndef __phasertng_tfactorial_function__
#define __phasertng_tfactorial_function__
#include <boost/math/special_functions/factorials.hpp>

namespace phasertng {

  inline double tfactorial(int n)
  { return  boost::math::factorial<double>(n); }
  inline int max_tfactorial()
  { return boost::math::max_factorial<double>::value; } //value is for <=

}//end namespace phasertng
#endif

