//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Assert.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>

//#define PHASERTNG_DEBUG_U_EXTRA

namespace phasertng { namespace fft {

  real_to_complex_3d::real_to_complex_3d(
       cctbx::sgtbx::space_group_type SG_,
       cctbx::uctbx::unit_cell UC_
       ) : fftbase(SG_,UC_)
  {}

  void
  real_to_complex_3d::allocate()
  {
    if (!memory_allocation_error)
    {
      rfft = scitbx::fftpack::real_to_complex_3d<double>(gridding);
    }
  }

  af::versa<double, af::c_grid<3> >
  real_to_complex_3d::backward(af::shared<millnx> MILLER, af::shared<cmplex> COEFFICIENTS)
  {
    if (memory_allocation_error)
      throw Error(err::MEMORY,"FFT array allocation");
    cctbx::sgtbx::space_group      SgOps(SG.group());
    phaser_assert(MILLER.size() == COEFFICIENTS.size());

    bool treat_restricted(false);
    af::c_grid_padded<3> map_grid(rfft.n_complex());
    cctbx::maptbx::structure_factors::to_map<double> real_to_map(
        SgOps, //PtNcs requires P1
        anomalous_flag,
        MILLER.const_ref(),
        COEFFICIENTS.const_ref(),
        rfft.n_real(),
        map_grid,
        conjugate_flag,
        treat_restricted);
    af::ref<cmplex, af::c_grid<3> > real_map_fft_ref(
        real_to_map.complex_map().begin(),
        af::c_grid<3>(rfft.n_complex()));

    // --- do the fft
    rfft.backward(real_map_fft_ref);
    { double scale = 1.0 /sqrt(rfft.n_real()[0]*rfft.n_real()[1]*rfft.n_real()[2]);
    for (int i  = 0; i < real_map_fft_ref.size(); i++) real_map_fft_ref[i]*=scale; }

    af::ref<double, af::c_grid_padded<3> > real_map_padded(
        reinterpret_cast<double*>( real_to_map.complex_map().begin()),
        af::c_grid_padded<3>( rfft.m_real(), rfft.n_real())
     );

    // --- have to store this map as well as const ref below
    real_map_unpadded = af::versa<double, af::c_grid<3> >( af::c_grid<3>(rfft.n_real()));
    cctbx::maptbx::copy(real_map_padded, real_map_unpadded.ref());

    // --- store the map
    real_map_const_ref = af::const_ref<double, af::c_grid_padded<3> >(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor())
      );

    return real_map_unpadded;
  }

  af::const_ref<cmplex, af::c_grid_padded<3> >
  real_to_complex_3d::forward(
          af::shared<cctbx::xray::scatterer<double> > xray_scatterers,
          std::string FORMFACTOR_TABLE_NAME
        )
  {
    if (memory_allocation_error)
      throw Error(err::MEMORY,"FFT array allocation");
    setup_u_base();

    cctbx::xray::scattering_type_registry scattering_type_registry;
    scattering_type_registry.process(xray_scatterers.const_ref());
    scattering_type_registry.assign_from_table(FORMFACTOR_TABLE_NAME);

    cctbx::xray::sampled_model_density<> sampled_density(
         UC,
         xray_scatterers.const_ref(),
         scattering_type_registry,
         rfft.n_real(),
         rfft.m_real(),
         u_base
         );

    u_extra = sampled_density.u_extra();

    // --- check that the density is not anomalous
    phaser_assert(!sampled_density.anomalous_flag());

    // --- fiddle to get correct form to pass to the fft calculation
    //have to store real_map or else return of complex_map below fails in extract structure factors
    // af::versa<double, af::c_grid_padded_periodic<3> > real_map;
    //aka cctbx::xray::sampled_model_density<>::real_map_type real_map;
    real_map = sampled_density.real_map();
    //focus_size_1d = sampled_density.focus_size_1d();
    //not available from sampled_density, manufacture from n_real array values
    focus_size_1d = rfft.n_real()[0]*rfft.n_real()[1]*rfft.n_real()[2];

    af::ref<double, af::c_grid<3> > real_map_fft_ref(
        &*real_map.begin(), af::c_grid<3>(rfft.m_real()));

    // --- do the fft
    rfft.forward(real_map_fft_ref);

    // --- extract the complex structure factors
    af::const_ref<cmplex, af::c_grid_padded<3> > complex_map_cref(
        reinterpret_cast<cmplex*>(&*real_map.begin()),
        af::c_grid_padded<3>(rfft.n_complex()));

    anomalous_flag = sampled_density.anomalous_flag();

#ifdef PHASERTNG_DEBUG_U_EXTRA
    //check sampled_density.focus_size_1d is the same
    //hack cctbx/xray/sampled_model_density.h to print intermediate values
    //std::cout << "this->unit_cell_.volume() " << this->unit_cell_.volume() << std::endl;
    //std::cout << "this->map_accessor_.focus_size_1d() " << this->map_accessor_.focus_size_1d() << std::endl;
 //first pdb, calculate millers and structure factors to required resolution limit
    cctbx::maptbx::structure_factors::from_map<double> from_map(
       UC,
       SG,
       anomalous_flag,
       dmin,
       complex_map_cref,
       conjugate_flag
      );
    af_millnx tmp_miller = from_map.miller_indices();
    af_cmplex tmp_Fcalc = from_map.data();
   // af::const_ref<miller::index<> > mmm;
   // af::ref<std::complex<double> > ddd;
    sampled_density.eliminate_u_extra_and_normalize(tmp_miller.const_ref(),tmp_Fcalc.ref());
    for (int r = 0; r < 3; r++)
      std::cout << "after eliminate r=" << r << " miller=" << tmp_miller[r][0] << " " << tmp_miller[r][1] << " " << tmp_miller[r][2]  << " fc=" << tmp_Fcalc[r] << std::endl;
#endif

    //unless real_map is stored, complex map is not kept in memory
    return complex_map_cref;
  }

  void
  real_to_complex_3d::WriteMap(std::string MAPOUT)
  {
    //stored as af::const_ref<double, af::c_grid_padded_periodic<3> >
    //which is correct for writing
    af::shared<std::string> labels(1);
    labels[0] = "phaser-tng map";
    af::int3 gridding_first(0,0,0);
    af::int3 gridding_last = gridding;
    //this works but is a nightmare
    if (!real_map.size())
    {
      af::versa<double, af::c_grid<3> > real_map_padded(
         af::c_grid<3>(real_map_const_ref.accessor().focus())
       );
      cctbx::maptbx::copy(real_map_const_ref, real_map_padded.ref());
      real_map = af::versa<double, af::c_grid_padded_periodic<3> >(
        real_map_padded, //af::versa<double, af::c_grid<3> > tsmap;
        af::c_grid_padded_periodic<3>(real_map_padded.accessor())
      );
    }
    iotbx::ccp4_map::write_ccp4_map_p1_cell(
        MAPOUT,
        UC,
        SG.group(),
        gridding_first,
        gridding_last,
        real_map.const_ref(),
        labels.const_ref()
      );
  }

}}
