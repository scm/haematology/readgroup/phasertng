//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_EELLG_class__
#define __phasertng_EELLG_class__

namespace phasertng {
namespace likelihood {

class EELLG
{
  public:
    EELLG() {}

  public:
    double get(double SigaSqr);

};

}}
#endif
