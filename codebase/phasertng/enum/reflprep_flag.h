#ifndef __phasertng_reflprep_flag_enum__
#define __phasertng_reflprep_flag_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace reflprep { //unique name

MakeEnum ( flag,
            (UNDEFINED)
            (INIT)
            (SSA)
            (SGX)
            (DATA)
            (ANISO)
            (INFO)
            (TNCS)
            (FEFF)
            (CCA)
            (CCS)
         );

}}
#endif
