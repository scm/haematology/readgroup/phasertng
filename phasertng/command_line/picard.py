# LIBTBX_SET_DISPATCHER_NAME phasertng.picard

from __future__ import print_function
from __future__ import division

from iotbx.cli_parser import run_program
from phasertng.programs import picard

if (__name__ == '__main__'):
  try:
    run_program(picard.Program)
  except Exception as e:
    print(str(e))
