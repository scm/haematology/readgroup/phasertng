#ifndef __phasertng_rejected_code_enum__
#define __phasertng_rejected_code_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace rejected {
  MakeEnum ( code,
             (OK)        //0
             (MISSING)   //1
             (RESO)      //2
             (SYSABS)    //3
             (WILSONNAT) //4
             (WILSONPOS) //5
             (WILSONNEG) //6
             (LOINFONAT) //7
             (LOINFOPOS) //8
             (LOINFONEG) //9
             (NOMEAN)    //10
             (NOANOM)    //11
             (BADANOM)   //12
           );

}}
#endif
