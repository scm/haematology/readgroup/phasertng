//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Assert.h>
#include <phasertng/site/AnglesAround.h>
#include <scitbx/math/euler_angles.h>
#include <iso646.h>

namespace phasertng {

  AnglesAround::AnglesAround(double SAMP_,dvect3 ANGLE_,double RANGE_) :
      SiteList(),
      ANGLE(ANGLE_),
      SAMP(SAMP_),
      RANGE(RANGE_)
  {
    phaser_assert(SAMP > 0);
    restart();
    Size = 0;
    while (!at_end()) { Size++; }
    restart(); //reset
  }

  void
  AnglesAround::restart(unsigned seed_)
  {
    tpstart = ANGLE[0] + ANGLE[2];
    tmstart = ANGLE[0] - ANGLE[2];
    betmin = ANGLE[1] - RANGE;
    betmax = ANGLE[1] + RANGE;
    dbeta = SAMP*std::sqrt(2./3.);
    nbeta = std::floor(((betmax-betmin)/dbeta)+0.5);
    if ((nbeta*dbeta) && (betmax-betmin)/(nbeta*dbeta) >  1.05 )
      nbeta++;
    dbeta = (nbeta > 0) ? (betmax-betmin)/nbeta : 0 ;
    betmax = betmax + 0.001*dbeta;
    for (int i = 0; i < 3; i++)
      starteuler[i] = scitbx::deg_as_rad(ANGLE[i]);
    offp = 5.0/6.0;
    beta = betmin;
    {
      offp = 1.0 - offp;
      cosb2 = std::fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
      sinb2 = std::fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
      if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
      {
        tpmin = 0.0;
        tpmax = 1.0;
        dtp = 1.0;
        dtm = SAMP;
        tmmin = tmstart - RANGE - dtm;
        tmmax = tmstart + RANGE;
      }
      else if (sinb2 < SAMP/360.0)
      {
        dtp = sqrt(3./4.)*SAMP;
        tpmin = tpstart - std::sqrt(3./4.)*RANGE - dtp;
        tpmax = tpstart + std::sqrt(3./4.)*RANGE;
        tmmin = 0.0;
        tmmax = 1.0;
        dtm = 1.0;
      }
      else
      {
        dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
        dtm = SAMP/sinb2;
        tpmin = tpstart - std::sqrt(3./4.)*RANGE/cosb2 - dtp;
        tpmax = tpstart + std::sqrt(3./4.)*RANGE/cosb2;
        if (tpmax-tpmin > 720.0)
        {
          tpmin = 0.0;
          tpmax = 720.0;
        }
        tmmin = tmstart - RANGE/sinb2 - dtm;
        tmmax = tmstart + RANGE/sinb2;
        if (tmmax-tmmin > 360.0)
        {
          tmmin = 0.0;
          tmmax = 360.0;
        }
      }

      offm = 3.0/4.0;
      thetp = tpmin+(offp*dtp);
      {
        offm = 1.0 - offm;
        thetm = tmmin+(offm*dtm);
      }
    }
    inext = 0;
  }

  bool
  AnglesAround::at_end()
  {
    if (inext == 0)
    {
      inext++;
      Site = ANGLE;
      return false;
    }
    phaser_assert(inext > 0);
    while (beta < betmax and
           SAMP < RANGE) //else infinite loop
    {
      alpha = std::fmod(720.0+(thetp+thetm)/2.0,360.0);
      gamma = std::fmod(720.0+(thetp-thetm)/2.0,360.0);
      dmat33 rmat1 = scitbx::math::euler_angles::zyz_matrix<double>(alpha,beta,gamma);
      dmat33 rmat2 = scitbx::math::euler_angles::zyz_matrix<double>(ANGLE[0],ANGLE[1],ANGLE[2]);
      dmat33 dmat = rmat2*rmat1.transpose();
      double cosdel = (dmat(0,0)+dmat(1,1)+dmat(2,2)-1.)/2.;
             cosdel = std::max(cosdel,-1.);
             cosdel = std::min(cosdel,1.);
      double rotdistDEG =  scitbx::rad_as_deg(std::acos(cosdel));
      Site = dvect3(alpha,beta,gamma);
      thetm += dtm;
      if (!(beta < betmax && thetp < tpmax && thetm < tmmax))
      {
        thetp += dtp;
        offm = 1.0 - offm;
        thetm = tmmin+(offm*dtm);
        if (!(beta < betmax && thetp < tpmax && thetm < tmmax))
        {
          beta += dbeta;
          offp = 1.0 - offp;
          cosb2 = std::fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
          sinb2 = std::fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
          if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
          {
            tpmin = 0.0;
            tpmax = 1.0;
            dtp = 1.0;
            dtm = SAMP;
            tmmin = tmstart - RANGE - dtm;
            tmmax = tmstart + RANGE;
          }
          else if (sinb2 < SAMP/360.0)
          {
            dtp = std::sqrt(3./4.)*SAMP;
            tpmin = tpstart - std::sqrt(3./4.)*RANGE - dtp;
            tpmax = tpstart + std::sqrt(3./4.)*RANGE;
            tmmin = 0.0;
            tmmax = 1.0;
            dtm = 1.0;
          }
          else
          {
            dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
            dtm = SAMP/sinb2;
            tpmin = tpstart - std::sqrt(3./4.)*RANGE/cosb2 - dtp;
            tpmax = tpstart + std::sqrt(3./4.)*RANGE/cosb2;
            if (tpmax-tpmin > 720.0)
            {
              tpmin = 0.0;
              tpmax = 720.0;
            }
            tmmin = tmstart - RANGE/sinb2 - dtm;
            tmmax = tmstart + RANGE/sinb2;
            if (tmmax-tmmin > 360.0)
            {
              tmmin = 0.0;
              tmmax = 360.0;
            }
          }

          offm = 3.0/4.0;
          thetp = tpmin+(offp*dtp);
          offm = 1.0 - offm;
          thetm = tmmin+(offm*dtm);
        }
      }
      if (rotdistDEG < RANGE && rotdistDEG > SAMP/2.0)
      {
        inext++;
        return false;
        //leaving in state ready for next iteration
      }
    }
    restart(); //ready for next
    return true;
  }

  af_dvect3
  AnglesAround::all_sites()
  {
    af_dvect3 siteList;
    siteList.push_back(ANGLE);
    double alpha(0),gamma(0),dbeta(0),betmin(0),betmax(0);
    double tpmin(0),tmmin(0),tpmax(0),dtp(0),tmmax(0),dtm(0),nbeta(0);
    double cosb2(0),sinb2(0),offm(0),offp(0);

    double tpstart = ANGLE[0] + ANGLE[2];
    double tmstart = ANGLE[0] - ANGLE[2];
    betmin = ANGLE[1] - RANGE;
    betmax = ANGLE[1] + RANGE;
    dbeta = SAMP*std::sqrt(2./3.);
    nbeta = std::floor(((betmax-betmin)/dbeta) + 0.5);
    if ((nbeta*dbeta) && (betmax-betmin)/(nbeta*dbeta) >  1.05 )
      nbeta++;
    dbeta = (nbeta > 0) ? (betmax-betmin)/nbeta : 0 ;
    betmax = betmax + 0.001*dbeta;
    for (int i = 0; i < 3; i++)
      starteuler[i] = scitbx::deg_as_rad(ANGLE[i]);

    offp = 5.0/6.0;
    for (double beta = betmin; beta < betmax; beta += dbeta)
    {
      offp = 1.0 - offp;
      cosb2 = std::fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
      sinb2 = std::fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
      if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
      {
        tpmin = 0.0;
        tpmax = 1.0;
        dtp = 1.0;
        dtm = SAMP;
        tmmin = tmstart - RANGE - dtm;
        tmmax = tmstart + RANGE;
      }
      else if (sinb2 < SAMP/360.0)
      {
        dtp = sqrt(3./4.)*SAMP;
        tpmin = tpstart - std::sqrt(3./4.)*RANGE - dtp;
        tpmax = tpstart + std::sqrt(3./4.)*RANGE;
        tmmin = 0.0;
        tmmax = 1.0;
        dtm = 1.0;
      }
      else
      {
        dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
        dtm = SAMP/sinb2;
        tpmin = tpstart - std::sqrt(3./4.)*RANGE/cosb2 - dtp;
        tpmax = tpstart + std::sqrt(3./4.)*RANGE/cosb2;
        if (tpmax-tpmin > 720.0)
        {
          tpmin = 0.0;
          tpmax = 720.0;
        }
        tmmin = tmstart - RANGE/sinb2 - dtm;
        tmmax = tmstart + RANGE/sinb2;
        if (tmmax-tmmin > 360.0)
        {
          tmmin = 0.0;
          tmmax = 360.0;
        }
      }

      offm = 3.0/4.0;
      for (double thetp = tpmin+(offp*dtp); thetp < tpmax; thetp += dtp)
      {
        offm = 1.0 - offm;
        for (double thetm = tmmin+(offm*dtm); thetm < tmmax; thetm += dtm)
        {
          alpha = std::fmod(720.0+(thetp+thetm)/2.0,360.0);
          gamma = std::fmod(720.0+(thetp-thetm)/2.0,360.0);
          dmat33 rmat1 = scitbx::math::euler_angles::zyz_matrix<double>(alpha,beta,gamma);
          dmat33 rmat2 = scitbx::math::euler_angles::zyz_matrix<double>(ANGLE[0],ANGLE[1],ANGLE[2]);
          dmat33 dmat = rmat2*rmat1.transpose();
          double cosdel = (dmat(0,0)+dmat(1,1)+dmat(2,2)-1.)/2.;
                 cosdel = std::max(cosdel,-1.);
                 cosdel = std::min(cosdel,1.);
          double rotdistDEG =  scitbx::rad_as_deg(std::acos(cosdel));
          dvect3 Sitelist(alpha,beta,gamma);
          if (rotdistDEG < RANGE && rotdistDEG > SAMP/2.0)
          {
            siteList.push_back(Sitelist);
          }
        }
      }
    }
    //restart(); no need to restart, doesn't use class member variables
    return siteList;
  }

}// phasertng
