//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Sequence.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/io/PdbRecord.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <fstream>
#include <cctype>
#include <phasertng/table/protein_composition.h>
#include <phasertng/table/nucleic_composition.h>
#include <phasertng/io/tostr2.h>

namespace phasertng {

  void
  Sequence::ReadSeq(int m)
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());
    af_string seqlines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        seqlines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory
    }
    infile.close();
    //if there is a multiplicity, literally duplicate the lines as though they were ^C ^V in the file
    af_string mseqlines;
    for (int i = 0; i < m; i++)
      for (auto line : seqlines)
        mseqlines.push_back(line);
    SetSeq(mseqlines);
  }

  void
  Sequence::SetSeq(af_string seqlines)
  {
    //open each chain in a separate string, so it can be identified as protein or nucleic
    seq = sv_string(0);
    try {
      for (auto buffer : seqlines)
      {
        for (auto& c : buffer)
          if (std::isalpha(c))
            c = toupper(c); //convert all to upper case
        for (auto c : buffer)
        {
          if ((isspace)(c)) continue; //eat whitespace at start
//comment line can come anywhere in the file (as it does in the pdb download fasta file)
          if (c == '>')
          {
            bool nucleic = buffer.find(constants::remark_tag + " RNA") != std::string::npos or
                           buffer.find(constants::remark_tag + " DNA") != std::string::npos;
            bool protein = buffer.find(constants::remark_tag + " PROTEIN") != std::string::npos;
            if (protein and nucleic)//DNA BINDING PROTEIN
              user_defined_nucleic.push_back(boost::logic::indeterminate);
            else if (protein)
              user_defined_nucleic.push_back(false);
            else if (nucleic)
              user_defined_nucleic.push_back(true);
            else
              user_defined_nucleic.push_back(boost::logic::indeterminate);
            seq.push_back(""); //new sequence
            break;
          }
          else if (std::isalpha(c)) //not end of line character ^M or something odd
          {
            if (!seq.size()) //first sequence, without > beforehand
            {
              seq.push_back("");
              user_defined_nucleic.push_back(boost::logic::indeterminate);
              //default is to assume protein for ATGC
              //(e.g. polyala, polyala+glycine) AAAAAAAGAAAAAA = protein
            }
            seq.back() += buffer;
            //grab the whole line of sequence here
            //this line may have odd characters on it, like ^M before the eol
            break;
          }
        }
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory
    }
   // initialize();
  }

  void
  Sequence::AddProtein(std::string proteinseq)
  {
    user_defined_nucleic.push_back(false);
    seq.push_back(proteinseq); //new sequence
  }

  void
  Sequence::AddNucleic(std::string nucleicseq)
  {
    user_defined_nucleic.push_back(true);
    seq.push_back(nucleicseq); //new sequence
  }

  void
  Sequence::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());
    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory
    }
    infile.close();
    SetPdb(pdblines);
  }

  void
  Sequence::SetPdb(af_string pdblines)
  {
    table::protein_composition protein;
    table::nucleic_composition nucleic;
    //open each chain in a separate string, so it can be identified as protein or nucleic
    //crudely reads the sequence from the pdb file, looks for CA and P atoms
    //and uses their entries
    //could look at residue numbers instead!!!!
    seq = sv_string(0);
    std::unordered_map<std::string,std::vector<char> > mapseq;
    bool first_model_card(true);
    try
    {
      for (auto buffer : pdblines)
      {
        PdbRecord next_atom;
        //only allow ATOM cards at start
        if (!buffer.find("ATOM"))
        {
          next_atom.ReadAtomCard(buffer);
          if (next_atom.O > 0) //only include those with some occupancy
          { //if the atom is CA for protein or P for nucleic acid, and the residue is recognised, add
            if (protein.valid_residue(next_atom.ResName) and next_atom.is_calpha())
            { mapseq[next_atom.Chain].push_back(protein.single_letter(next_atom.ResName)); }
            if (nucleic.valid_residue(next_atom.ResName) and next_atom.is_palpha())
            { mapseq[next_atom.Chain].push_back(nucleic.single_letter(next_atom.ResName)); }
          }
        }
        if (!buffer.find("SSBOND"))
        {
          next_atom.ReadAtomCard(buffer);
          count_ssbond+=2;
        }
        if (!buffer.find("MODEL"))
        { //only read the first model, otherwise the concatenate
          //can't trust looking for ENDMDL cards, so count MODEL cards and exit on second
          //if it occurs in the file
          if (first_model_card) first_model_card = false;
          else break;
        }
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }

    for (auto item : mapseq)
    {
      std::string str(item.second.begin(), item.second.end());
      seq.push_back(str);
      user_defined_nucleic.push_back(boost::logic::indeterminate);
    }
    //initialize();
  }

  void
  Sequence::ParseModel(const std::vector<PdbRecord>& PDB,const int COUNT_SSBOND,bool use_unknown)
  {
    table::protein_composition protein;
    table::nucleic_composition nucleic;
    //same parsing as pdb file
    //disulphides not recorded
    if (!PDB.size()) return;
    seq = sv_string(0);
    //std::string is the chainid
    //std::vector<char> is the single-letter sequence
    std::unordered_map<std::string,std::vector<char>> mapseq;
    count_ssbond = COUNT_SSBOND; //previously read from pdb file
    for (auto next_atom : PDB)
    {
      if (next_atom.AtomName != "UNK " or use_unknown)
      if (next_atom.O > 0) //only include those with some occupancy
      { //if the atom is CA for protein or P for nucleic acid, and the residue is recognised, add
        if (protein.valid_residue(next_atom.ResName) and next_atom.AtomName == " CA ")
        {
          mapseq[next_atom.Chain].push_back(protein.single_letter(next_atom.ResName));
        }
        else if (nucleic.valid_residue(next_atom.ResName) and next_atom.AtomName == " P  ")
        {
          mapseq[next_atom.Chain].push_back(nucleic.single_letter(next_atom.ResName));
        }
        else if (!protein.valid_residue(next_atom.ResName) and
                 !nucleic.valid_residue(next_atom.ResName) and
                 next_atom.Hetatm) //should be a hetatm
        {
          AddAtom(next_atom.get_element(),1);
        }
      }
    }
    for (auto item : mapseq)
    {
      std::string str(item.second.begin(), item.second.end());
      seq.push_back(str);
      //user doesn't need to change, since entered via pdb with 3 letter code, which can't
      //be misinterpreted, like a one-letter sequence
      user_defined_nucleic.push_back(boost::logic::indeterminate);
    }
    //initialize();
    //at this point all the residues with CA or P are added to the composition with the complete
    //sidechain even if atoms are missing in the original pdb file
  }

  void
  Sequence::initialize(bool add_terminal_atoms, bool add_hydrogens)
  {
    table::protein_composition protein;
    table::nucleic_composition nucleic;
    //loop over the array of sequences and find out if they are protein or nucleic
    //based on whether the residues/bases are uniquely one or the other
    //Gly-Ala-Cys-Thr / G-A-C-T sequences are not unique
    //Ala-Ala-Ala-Ala / A-A-A-A sequences are not unique
    uniquely_protein.resize(seq.size(),true);
    uniquely_nucleic.resize(seq.size(),true);
    uniquely_alanine.resize(seq.size(),true);
    PHASER_ASSERT(user_defined_nucleic.size() == seq.size());
    CompType = composition::UNDEFINED;
    std::string tmp;
    for (int s = 0; s < seq.size(); s++)
    {
      for (auto r : seq[s])
      {
        if (r == ' ') continue; //just a gap
        if (r == 'X') continue; //just a gap
        tmp += std::string(1,r);
        if (!std::isalpha(r)) continue; //something odd like ^M, which can be ignored
        if (!protein.valid_residue(r) and !nucleic.valid_residue(r))
        throw Error(err::INPUT,"Residue \"" + std::string(1,r) + "\" not recognised in sequence\n" + tmp);
        if (!nucleic.valid_residue(r))
        {
          uniquely_nucleic[s] = false;
        }
        if (!protein.valid_residue(r))
        {
          uniquely_protein[s] = false;
        }
        if (!(r == 'A' or r == 'G')) //polyalanine can be polyglycine (mixed A/G)
        {
          uniquely_alanine[s] = false;
        }
      }
      if (uniquely_protein[s] and uniquely_nucleic[s])
      {
        if (!boost::logic::indeterminate(user_defined_nucleic[s]))
        {
          uniquely_nucleic[s] = bool(user_defined_nucleic[s]);
          uniquely_protein[s] =! bool(user_defined_nucleic[s]);
        }
        else if (uniquely_alanine[s])
        {
          uniquely_protein[s] = true;
          uniquely_nucleic[s] = false;
        }
        else
        {
          uniquely_protein[s] = false;
          uniquely_nucleic[s] = true;
        }
      }
      //protein only composition of seq
      bool first(CompType == composition::UNDEFINED);
      if (first and uniquely_protein[s])
        CompType = composition::PROTEIN;
      else if (first and uniquely_nucleic[s])
        CompType = composition::NUCLEIC;
      else if (first)
        CompType = composition::MIXED;
      else if (CompType == composition::PROTEIN and uniquely_nucleic[s])
        CompType = composition::MIXED;
      else if (CompType == composition::NUCLEIC and uniquely_protein[s])
        CompType = composition::MIXED;
    }

    //AssemblyComp will become the composition, with empty elements removed
    //these are the residues that can come from the sequence only
    std::unordered_map<std::string,double> AssemblyComp =
          { {"H", 0}, {"D", 0 }, {"C", 0}, {"N",0}, {"O",0}, {"S",0}, {"SE",0}, {"P",0}, {"SS",0} };

    for (int s = 0; s < seq.size(); s++)
    {
      //add the scattering for the N and C terminii
      if (add_terminal_atoms)
      {
        AssemblyComp["H"] += 2; //N terminus
        AssemblyComp["O"] += 1; //C terminus
      }
      if (!(uniquely_protein[s] or uniquely_nucleic[s]))
        throw Error(err::INPUT,"Sequence is neither uniquely protein nor uniquely nucleic");
      if (uniquely_protein[s] and uniquely_nucleic[s])
        throw Error(err::INPUT,"Sequence is uniquely protein and uniquely nucleic");
      if (uniquely_protein[s])
      {
        for (auto residue : seq[s])
        {
          if (selenomethionine and residue == 'M') residue = protein.SeMet();
          if (disulphide and residue == 'C') residue = protein.SSbond();
          if (!disulphide and residue == 'C' and count_ssbond)
          {
            residue = protein.SSbond();
            count_ssbond--; //count_ssbond is 2xnumber of Cys, so only apply until count depletes
          }
          for (auto item : protein.composition(residue))
          {
            std::string element = (deuterium and item.first == "H") ? "D" : item.first;
              AssemblyComp[element] += item.second; //will be 0.5 for SSbond
          }
        }
      }
      else if (uniquely_nucleic[s])
      {
        for (auto residue : seq[s])
        {
          for (auto item : nucleic.composition(residue))
          {
             AssemblyComp[item.first] += item.second;
          }
        }
      }
      else PHASER_ASSERT(false);
    }

    if (!add_hydrogens)
      AssemblyComp.erase("H");

    //convert AssemblyComp to the composition we will use
    //convert double to integer and don't copy the empty elements
    for (auto item : AssemblyComp)
    {
      if (item.second > 0) //only if non-zero
      {
        std::string element = tostr2(item.first).first;
        COMPOSITION[element] = static_cast<int>(std::ceil(item.second)); //round up SSbond
      }
    }
    //others can be added to directly with AddAtom if there are additional HETATM to be included
  }

  void
  Sequence::AddAtom(std::string element, int number)
  {
    element = tostr2(element).first;
    COMPOSITION.count(element) ?
      COMPOSITION[element] += number :
      COMPOSITION[element] = number; //initialize with new element
    if (element == "XX" and !xx.composition().size())
      throw Error(err::FATAL,"Cluster compound not entered although specified in composition");
  }

  int
  Sequence::total_scattering() const
  {
    //note that this scattering will be different from the scattering obtained
    //directly from the pdb file because this scattering is based on the residue names
    //whether they are complete or not in the pdb file
    //and ignores hetatms
    double scattering = 0;
    for (auto item : COMPOSITION)
    {
      //allow the cluster types as special cases
      if (item.first == "SS")
      {
        Cluster ss("SS");
        for (auto item : ss.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          scattering += item.second * fn::pow2(atm.atomic_number());
        }
      }
      else if (item.first == "TX")
      {
        Cluster tx("TX");
        for (auto item : tx.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          scattering += item.second * fn::pow2(atm.atomic_number());
        }
      }
      else if (item.first == "RX")
      {
      }
      else if (item.first == "AX")
      {
      }
      else if (item.first == "XX")
      {
        for (auto item : xx.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          scattering += item.second * fn::pow2(atm.atomic_number());
        }
      }
      else if (item.first == "X")
      {
        //do nothing, missing residue in sequence (iotbx.pdb_as_fasta)
      }
      else
      {
        try
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          scattering += item.second * fn::pow2(atm.atomic_number());
        }
        catch (...)
        { throw Error(err::FATAL,"Atom type (" + item.first + ") not recognised"); }
      }
    }
    return std::ceil(scattering*water_inflation);
  }

  double
  Sequence::total_molecular_weight() const
  {
    double molecular_weight = 0;
    for (auto item : COMPOSITION)
    {
      if (item.first == "SS")
      {
        Cluster ss("SS");
        for (auto item : ss.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          molecular_weight += item.second * atm.weight();
        }
      }
      else if (item.first == "TX")
      {
        Cluster tx("TX");
        for (auto item : tx.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          molecular_weight += item.second * atm.weight();
        }
      }
      else if (item.first == "AX")
      {
      }
      else if (item.first == "XX")
      {
        if (!xx.composition().size())
        { throw Error(err::FATAL,"Cluster compound not entered although specified in composition"); }
        for (auto item : xx.composition())
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          molecular_weight += item.second * atm.weight();
        }
      }
      else if (item.first == "X")
      {
        //do nothing, missing residue in sequence (iotbx.pdb_as_fasta)
      }
      else
      {
        try
        {
          cctbx::eltbx::tiny_pse::table atm(item.first);
          molecular_weight += item.second * atm.weight();
        }
        catch (...)
        { throw Error(err::FATAL,"Atom type (" + item.first + ") not recognised"); }
      }
    }
    return molecular_weight; //in daltons
  }

  double
  Sequence::minimum_radius_from_molecular_weight() const
  {
    double nm = 0.066*std::cbrt(total_molecular_weight());
    return 10*nm;
    //Erickson, Biological Procedure Online vol. 11, no. 1.
    //http://www.biologicalproceduresonline.com/content/pdf/1480-9222-11-1-9008.pdf
  }

  //psv Rupp 2003 + RNA doi:10.1016/j.jmb.2004.11.072,
  //note latter has different number for protein psv than Rupp
  double
  Sequence::volume() const
  {
    double MW = total_molecular_weight();
    double PROTEIN(0),NUCLEIC(0);
    if (CompType == composition::PROTEIN)
      PROTEIN = MW; //all protein
    else if (CompType == composition::NUCLEIC)
      NUCLEIC = MW; //all nucleic
    else if (CompType == composition::PROTEIN)
      NUCLEIC = PROTEIN = MW/2.;
     //half and half is crude approximation, but it is lots more work to store correctly
    return 1.22879889*PROTEIN + 0.944846714*NUCLEIC;
  }

  af_string
  Sequence::logComposition(std::string description)
  {
    af_string output;
    output.push_back("Composition: " + description);
    std::string tab1(DEF_TAB*1,' ');
    std::string tab2(DEF_TAB*2,' ');
    if (filesystem_is_set())
      output.push_back(tab1 + "Sequence File: " + fstr());
    output.push_back(tab2 + "Protein/Nucleic/Mixed: " + composition::type2String(CompType));
    output.push_back(tab2 + "Disulphides: " + std::to_string(count_ssbond));
    output.push_back(tab2 + "Water percent: " + std::to_string(100*(water_inflation-1)));
    for (int s = 0; s < seq.size(); s++)
    {
      output.push_back(tab2 + "Sequence #" + std::to_string(s+1) );
      output.push_back(tab2 + "  Sequence Uniquely Protein: " + std::string(uniquely_protein[s] ? "Y":"N") );
      output.push_back(tab2 + "  Sequence Uniquely Nucleic: " + std::string(uniquely_nucleic[s] ? "Y":"N") );
      output.push_back(tab2 + "  Sequence Uniquely Alanine: " + std::string(uniquely_alanine[s] ? "Y":"N") );
      auto YN = user_defined_nucleic[s] ? "Y":"N"; //tribool
      auto YNU = boost::logic::indeterminate(user_defined_nucleic[s]) ? "U" : YN;
      output.push_back(tab2 + "  Sequence Defined  Nucleic: " + YNU);
    }
    char buf[80];
    snprintf(buf,80,"%9s %10s", "Atom type","Number");
    output.push_back(std::string(buf));
    for (auto item : COMPOSITION)
    {
      snprintf(buf,80,"%9s %10i",item.first.c_str(),item.second);
      output.push_back(std::string(buf));
    }
    return output;
  }

  af_string
  Sequence::logSequence(std::string description)
  {
    af_string output;
    output.push_back("Sequence: " + description);
    for (int s = 0; s < seq.size(); s++)
    {
      std::string SeqType = "";
      SeqType +=  uniquely_protein[s] ? "Protein" : "";
      SeqType +=  uniquely_nucleic[s] ? "Nucleic Acid" : "";
      SeqType +=  uniquely_alanine[s] ? ":Polyalanine" : "";
      output.push_back("#" + std::to_string(s+1) + " (" + SeqType + ")");
      std::string tmp;
      for (int i = 0; i < seq[s].size(); i++)
      {
        if (seq[s][i] == ' ') continue;
        int count(0);
        for (int j = 0; j < tmp.size(); j++) if (tmp[j] != ' ') count++;
        if (count == 60)
        { output.push_back(tmp); tmp = ""; count = 0; }
        else if (count and count%10 == 0)
        { tmp += " "; }
        tmp += std::string(1,seq[s][i]);
      }
      if (tmp.size())
      { output.push_back(tmp); }
    }
    return output;
  }

  const bool Sequence::has_protein() const
  { return CompType == composition::PROTEIN or CompType == composition::MIXED; }
  const bool Sequence::has_nucleic() const
  { return CompType == composition::NUCLEIC or CompType == composition::MIXED; }
  const bool Sequence::all_protein() const
  { return CompType == composition::PROTEIN; }
  const bool Sequence::all_nucleic() const
  { return CompType == composition::NUCLEIC; }

  const int
  Sequence::size() const
  {
    int n(0);
    for (auto item : seq)
    {
      n += item.size();
    }
    return n;
  }

} //phasertng
