//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_SpaceGroup_class__
#define __phasertng_SpaceGroup_class__
#include <phasertng/main/includes.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>

namespace phasertng {
  namespace pod {
    class rsymhkl
    {
      public:
        std::vector<millnx> rotMiller;
        std::vector<double> traMiller;
        int unique = 0;
      public:
        rsymhkl(int n=0) { resize(n); }
        void resize(int n) { rotMiller.resize(n); traMiller.resize(n); unique = n; }
    };
  } //pod

  class SpaceGroup
  {
    private:
      cctbx::sgtbx::space_group_type cctbxSG_type;
      cctbx::sgtbx::space_group      cctbxSG; //store this for rapid access to e.g is_centric
    public:
      const cctbx::sgtbx::space_group_type& type() const;
      const cctbx::sgtbx::space_group&      group() const; //store this for rapid access to e.g is_centric

    public: //members
      sv_dmat33   rotsym;
      sv_dvect3   trasym;
      unsigned    SYMFAC = 1;
      std::string HALL = "";
      std::string CCP4 = "";
      std::string UHMS = "";
      double symFacPCIF = 0;
      bool STANDARD = true;
      int NSYMP;

    public: //constructors
      SpaceGroup() {}
      SpaceGroup(const std::string);
      virtual ~SpaceGroup(){}
      SpaceGroup enantiomorph() const;
      //copy these from class, as though they were base class functions
      cctbx::sgtbx::change_of_basis_op cb_op() const { return type().cb_op(); }

    private:
      void init();

    public:
      std::string str() const { return CCP4 + " [" + HALL + "]"; };
      std::string hall() const { return HALL.substr(5,HALL.length()-1); };
      std::string pgname() const;
      std::string pointgroup() const;
      char        spcgrpcentring() const;
      int         orderX() const;        //order of symmetry about x-axis
      int         orderY() const;        //order of symmetry about y-axis
      int         orderZ() const;        //order of symmetry about z-axis
      std::pair<int,char>  highOrderAxis() const; //highest symmetry axis
      int         lowOrder() const;      //lowest order of second and third order symmetry axis
      bool        is_polar() const;
      bool        is_p1() const;
      std::string sstr() const;

      dvect3 doSymXYZ(const int&, const dvect3&) const; //Apply ith Symmetry to vector
      double getSpaceGroupR(int s,int i,int j)
      { return (s < group().order_z() && i < 3 && j < 3) ? rotsym[s](i,j) : 0; }
      double getSpaceGroupT(int s,int i)
      { return (s < group().order_z() && i < 3) ? trasym[s][i] : 0; }

      void symmetry_hkl_primitive(const millnx&,const int, pod::rsymhkl&) const;

      af::shared< std::pair<millnx,double> >
      symmetry_hkl_centred(const millnx&) const;

      af_dvect3
      symmetry_xyz_cell(const dvect3&,const ivect3,const bool=false) const;

      af_dvect3
      symmetry_xyz_cell(const dvect3&,const int,const bool=false) const;

      std::pair<int,dvect3>
      symmetry_xyz_cell(int, const int,const bool=false) const;

      std::vector< std::pair< dmat33, dvect3 > >
      symmetry_xyz_cell_operators(const int, const bool=false) const;

      std::vector< std::pair< dmat33, dvect3 > >
      symmetry_xyz_cell_operators(const ivect3, const bool=false) const;

    public://operator
    bool
    operator<( const SpaceGroup& rhs ) const
    { //anything, doesn't matter for std::set
      return this->str() < rhs.str();
    }

    bool
    operator==( const SpaceGroup& rhs ) const
    {
      return cctbxSG == rhs.group();
    }
  };

} //phasertng

#endif
