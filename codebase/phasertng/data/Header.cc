//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Header.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/jiffy.h>
#include <cassert>

namespace phasertng {

  boost::logic::tribool Header::token_to_tribool(std::string token) const
  {
    if (!token.find("T")) return true;
    if (!token.find("F")) return false;
    phaser_assert(!token.find("U"));
    return boost::logic::indeterminate;
  }

  std::string Header::tribool_to_token(boost::logic::tribool i) const
  {
    if (boost::logic::indeterminate(i)) return "U";
    return i ? "T ":"F";
  }

  std::string Header::bool_to_token(bool i) const
  { return i ? "T ":"F"; }

  bool Header::token_to_bool(std::string token) const
  { return (!token.find("T")) ? true : false; }

  void
  Header::clear()
  {
    MTZCOLTYPE.clear();
    MTZCOLNAME.clear();
    init_miller(); //vital, constructor!
    LABIN.clear();
    REFLID.set_invalid();
    REFLPREP.clear(); //data preparation history
    MAP = boost::logic::indeterminate;
    EVALUES = boost::logic::indeterminate;
    TNCS_PRESENT = boost::logic::indeterminate;
    TWINNED = boost::logic::indeterminate;
    ANOMALOUS = boost::logic::indeterminate;
    SHARPENING = 0;
    WILSON_K_F = 0; //flag for set, start with impossible value
    WILSON_B_F = 0;
    TNCS_ORDER = 1;
    TNCS_ANGLE = dvect3(0,0,0);
    ANISO_PRESENT = false; //cosines, not magnitude
    ANISO_DIRECTION_COSINES = dmat33(0,0,0,0,0,0,0,0,0);
    TNCS_VECTOR = dvect3(0,0,0);
    TNCS_RMS = 0;
    TNCS_FS = 1;
    TNCS_GFNRAD = 0;
    TOTAL_SCATTERING = 0;
    Z = 0;
    FRENCH_WILSON = false;
    INTENSITIES = false;
    WILSON_SCALED = false;
  }

  void
  Header::SetHistory(af_string histlines)
  {
    for (auto line : histlines)
    {
      sv_string token;
      hoist::algorithm::split(token,line," ");
      int i = 2; //after remarktag and single word specifier
      if (!line.find(remark_identifier))
      {
        phaser_assert(token.size() >= 4);
        std::string idnum = token[i++];
        std::string idtag = token[i++];
        Identifier Id;
        Id.parse_string(idnum,idtag);
        REFLID = Id;
      }
      else if (!line.find(remark_preparation))
      {
        for (int j = i; j < token.size(); j++)
        REFLPREP.insert(reflprep::flag2Enum(token[j]));
      }
      else if (!line.find(remark_data))
      {
        phaser_assert(token.size() >= 12);
        phaser_assert(token[i] == "INTENSITIES"); i++;
        INTENSITIES = token_to_tribool(token[i++]);
        phaser_assert(token[i] == "FRENCH-WILSON"); i++;
        FRENCH_WILSON = token_to_tribool(token[i++]);
        phaser_assert(token[i] == "ANO"); i++;
        ANOMALOUS = token_to_tribool(token[i++]);
        phaser_assert(token[i] == "MAP"); i++;
        MAP = token_to_tribool(token[i++]);
        phaser_assert(token[i] == "EVALUES"); i++;
        EVALUES = token_to_tribool(token[i++]);
      }
      else if (!line.find(remark_wilson))
      {
        phaser_assert(token.size() >= 10);
        phaser_assert(token[i] == "K"); i++;
        WILSON_K_F = std::atof(token[i++].c_str());
        phaser_assert(token[i] == "B"); i++;
        WILSON_B_F = std::atof(token[i++].c_str());
        phaser_assert(token[i] == "SHARP"); i++;
        SHARPENING = std::atof(token[i++].c_str());
        phaser_assert(token[i] == "SCALED"); i++;
        WILSON_SCALED = token_to_bool(token[i++]);
      }
      else if (!line.find(remark_labin))
      {
        for (int i = 2; i < token.size(); i++)
        {
          sv_string subtoken;
//Column labels should be alphanumeric.
//Avoid special characters
//(the "/" in the standard label "M/ISYM" is a special case -
//slashes in other places will break the code).
//Therefore, the forward slash is an excellent character to use for the separation here
          hoist::algorithm::split(subtoken,token[i],("/"));
          phaser_assert(subtoken.size() == 3);
          labin::col labin_col = labin::col2Enum(subtoken[0]);
          std::string name = subtoken[1];
          char type = subtoken[2][0];
          MTZCOLNAME[labin_col] = name;
          MTZCOLTYPE[labin_col] = type;
        }
      }
      else if (!line.find(remark_anisotropy))
      {
        phaser_assert(i < token.size());
        ANISO_PRESENT = true;
        for (int j = 0; j < 9; j++)
        {
          phaser_assert(i < token.size());
          ANISO_DIRECTION_COSINES[j] = std::atof(token[i++].c_str());
        }
      }
      else if (!line.find(remark_tncs))
      {
        phaser_assert(i < token.size());
        std::string parse = token[i++];
        if (parse == "PRESENT")
        {
          phaser_assert(i < token.size());
          TNCS_PRESENT = token_to_tribool(token[i++]);
        }
        else if (parse == "ORDER")
        {
          phaser_assert(token.size() >= 10);
          TNCS_ORDER = std::atoi(token[i++].c_str());
          phaser_assert(token[i] == "RMS"); i++;
          TNCS_RMS = std::atof(token[i++].c_str());
          phaser_assert(token[i] == "FS"); i++;
          TNCS_FS = std::atof(token[i++].c_str());
          phaser_assert(token[i] == "RAD"); i++;
          TNCS_GFNRAD = std::atof(token[i++].c_str());
        }
        else if (parse == "VECTOR")
        {
          for (int j = 0; j < 3; j++)
          {
            phaser_assert(i < token.size());
            TNCS_VECTOR[j] = std::atof(token[i++].c_str());
          }
        }
        else if (parse == "ANGLE")
        {
          for (int j = 0; j < 3; j++)
          {
            phaser_assert(i < token.size());
            TNCS_ANGLE[j] = std::atof(token[i++].c_str());
          }
        }
      }
      else if (!line.find(remark_twinned))
      {
        phaser_assert(i < token.size());
        TWINNED = token_to_tribool(token[i++]);
      }
      else if (!line.find(remark_asu))
      {
        phaser_assert(token.size() >= 6);
        phaser_assert(token[i] == "TOTAL-SCATTERING"); i++;
        TOTAL_SCATTERING = std::atoi(token[i++].c_str());
        phaser_assert(token[i] == "Z"); i++;
        Z = std::atof(token[i++].c_str());
      }
      else if (!line.find(remark_oversampling))
      {
        OVERSAMPLING = std::atof(token[i++].c_str());
        phaser_assert(token[i] == map_origin); i++;
        for (int j = 0; j < 3; j++)
        {
          phaser_assert(i < token.size());
          MAPORI[j] = std::atof(token[i++].c_str());
        }
      }
      else if (!line.find(remark_isg))
      {
        std::string isg;
        for (; i < token.size(); i++)
          isg += token[i] + " ";
        isg.pop_back();
        iSG = SpaceGroup(isg);
      }
    }
  }

  af_string
  Header::get_history() const
  {
    //Up to 30 Character*80 lines containing history information
    af_string histlines;
    {
      //PHASER_ASSERT(REFLID.is_set_valid()); //must have REFLID!
      std::string strhistory(remark_identifier + REFLID.string() + " " + REFLID.tag());
      histlines.push_back(strhistory);
    }
    { //PREP
      std::string nospace = remark_preparation;
      nospace.pop_back();
      std::string strhistory(nospace);
      for (auto item : REFLPREP)
      {
        std::string prep = " " + reflprep::flag2String(item);
        std::string look_ahead = strhistory + prep;
        int MTZRECORDLENGTH = 80; //from mtz header
        if (look_ahead.size() >= int(MTZRECORDLENGTH)) // need new line
        {
          histlines.push_back(strhistory);
          strhistory = nospace;
        }
        //assumes that the new strhistory will not exceed MTZRECORDLENGTH
        strhistory += prep;
        phaser_assert(strhistory.size() < int(MTZRECORDLENGTH));
      }
      if (strhistory.size() > remark_labin.size())
      {
        histlines.push_back(strhistory);
      }
    }//PREP
    //AJM instead of putting these LABIN in the output, could fix the names of the columns
    //for local phasertng use only, and then the mtzcol would come from the name only
    //which would be the default name, not the user labin name
    //user labin name stored in phil if ever needed
    { //LABIN
      std::string nospace = remark_labin;
      nospace.pop_back();
      std::string strhistory(nospace);
      for (auto item : LABIN)
      {
        //white space separated for istringstream parsing
        //values in pairs
        //PHASER_ASSERT(MTZCOLNAME[item].size()); //paranoia
        std::string assign_labin =
          " " + labin::col2String(item) +
          "/" + std::string(MTZCOLNAME.at(item)) +
          "/" + std::string(1,MTZCOLTYPE.at(item));
        std::string look_ahead = strhistory + assign_labin;
        int MTZRECORDLENGTH = 80; //from mtz header
        if (look_ahead.size() >= int(MTZRECORDLENGTH)) // need new line
        {
          histlines.push_back(strhistory);
          strhistory = nospace;
        }
        //assumes that the new strhistory will not exceed MTZRECORDLENGTH
        strhistory += assign_labin;
        phaser_assert(strhistory.size() < int(MTZRECORDLENGTH));
      }
      if (strhistory.size() > remark_labin.size())
      {
        histlines.push_back(strhistory);
      }
    }//LABIN
    {
      //assume pos and neg are the same
      try {
      std::string strhistory(remark_data +
             "INTENSITIES " + tribool_to_token(INTENSITIES) +
             " FRENCH-WILSON " + tribool_to_token(FRENCH_WILSON) +
             " ANO " + tribool_to_token(ANOMALOUS) +
             " MAP " + tribool_to_token(MAP) +
             " EVALUES " + tribool_to_token(EVALUES));
      histlines.push_back(strhistory);
      } catch(...) {}
    }
    if (WILSON_K_F != 0) //impossible default value
    {
      std::string strhistory(remark_wilson +
             "K " + gtos(WILSON_K_F,10) +
             " B " + gtos(WILSON_B_F,10) +
             " SHARP " + gtos(SHARPENING,10) +
             " SCALED " + bool_to_token(WILSON_SCALED));
      histlines.push_back(strhistory);
    }
    if (!boost::logic::indeterminate(TNCS_PRESENT))
    { //split history because is too long for line
      if (TNCS_PRESENT)
      {
        //reverse order construction
        std::string strhistory1(remark_tncs +
             "ORDER " + itos(TNCS_ORDER) +
             " RMS " + dtos(TNCS_RMS,8,5) +
             " FS " + dtos(TNCS_FS,8,5) +
             " RAD " + dtos(TNCS_GFNRAD,8,5));
        histlines.push_back(strhistory1);
        std::string strhistory2(remark_tncs +
             "VECTOR " + dvtos(TNCS_VECTOR,12,10));
        histlines.push_back(strhistory2);
        std::string strhistory3(remark_tncs +
             "ANGLE " + dvtos(TNCS_ANGLE,12,10));
        histlines.push_back(strhistory3);
      }
      //true or false
      std::string strhistory(remark_tncs + "PRESENT " + tribool_to_token(TNCS_PRESENT));
      histlines.push_back(strhistory);
    }
    if (ANISO_PRESENT)
    {
      //if all the values in the matrix are negative then this will exceed the line length
      //but that is not possible
      std::string strhistory(remark_anisotropy + dmtos(ANISO_DIRECTION_COSINES,6,4));
      histlines.push_back(strhistory);
    }
    if (!boost::logic::indeterminate(TWINNED))
    {
      std::string strhistory(remark_twinned + tribool_to_token(TWINNED));
      histlines.push_back(strhistory);
    }
    if (TOTAL_SCATTERING != 0)
    {
      std::string strhistory(remark_asu +
             "TOTAL-SCATTERING " + itos(TOTAL_SCATTERING) +
             " Z " + dtos(Z,(Z == std::round(Z)) ? 0 : 6));
      histlines.push_back(strhistory);
    }
    if (OVERSAMPLING != 1 or MAPORI != dvect3(0,0,0))
    {
      std::string strhistory(remark_oversampling + dtoss(OVERSAMPLING,10) +
            " " + map_origin + " " +  dvtoss(MAPORI,2));
      histlines.push_back(strhistory);
    }
    if (iSG.CCP4.size())
    {
      std::string strhistory(remark_isg + iSG.CCP4);
      histlines.push_back(strhistory);
    }
    //the maximum number of history lines appears to be 30
    //This is from the documentation of mtzformat, but there doesn't appear to be a parameter
    //in the library that defines this number. The maximum length of the line however
    //is defined in the library. Confusing.
    phaser_assert(histlines.size() <= 30);
    int MTZRECORDLENGTH = 80; //from mtz header
    for (auto& line : histlines)
      phaser_assert(line.size() < int(MTZRECORDLENGTH));
    return histlines;
  }

  void
  Header::CreateLabin(type::mtzcol label)
  {
    labin::col col = label.enumeration();
    MTZCOLNAME[col] = label.name();
    MTZCOLTYPE[col] = label.type();
    LABIN.insert(col);
  }

  type::mtzcol
  Header::get_mtzcol(labin::col col) const
  { //checks done externally
    PHASER_ASSERT(MTZCOLTYPE.count(col));
    PHASER_ASSERT(MTZCOLNAME.count(col));
    return type::mtzcol(MTZCOLTYPE.at(col),MTZCOLNAME.at(col),col);
  }

  const bool
  Header::prepared(reflprep::flag name) const
  { //checks done externally
    for (auto item : REFLPREP)
      if (item == name) return true;
    return false;
  }

} //phasertng
