//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_AnglesStored_class__
#define __phasertng_AnglesStored_class__
#include <phasertng/site/SiteList.h>
#include <phasertng/site/Site.h>
#include <phasertng/io/Loggraph.h>

namespace phasertng {

class AnglesStored : public SiteList
{
  //use vector as clustering will be faster with elements in cache
  protected:
    double DEF_PPT = 1.0e-03;
  public:
    std::vector<Site> rotation_function_data;
    std::vector<Site>::iterator inext = rotation_function_data.begin();
    double beta_deg = 0;
    double f_mean = 0;
    double f_sigma = 0;
    double f_top = 0;
    double z_top = 0;
    double SAMP = 0;

  public:
    AnglesStored() : SiteList() { rotation_function_data.resize(0); }
    double  peak() { return inext->value; }

  public:
    bool         at_end();
    void         restart(unsigned=0);
    dvect3       next_site();
    std::size_t  count_sites();
    af_dvect3    all_sites();
    double       point_distance() { return SAMP; }

  public:
    void partial_sort_and_erase(const int&);
    void full_sort();
    Loggraph logGraph(std::string);
    void select_topsites_and_erase();
    std::pair<double,double> mean_sigma();
    double top();
    void allocate_memory(int,double);
    void fill_memory(int,double);
};

}
#endif
