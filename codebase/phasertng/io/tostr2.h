//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_tostr_class__
#define __phasertng_tostr_class__
#include <string>

namespace phasertng {

 std::pair<std::string,std::string> tostr2(std::string);

} //phasertng
#endif
