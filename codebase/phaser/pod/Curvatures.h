//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_CURVATURES__
#define __PHASER_CURVATURES__
#include <phaser/main/Phaser.h>

namespace phaser {

typedef struct {

  af::shared<miller::index<int> > miller;
  af_dvect3 d2L_by_dFC2;

} Curvatures;

} //phaser

#endif
