//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_fft_fftbase_class__
#define __phasertng_fft_fftbase_class__
#include <phasertng/cctbx_project/cctbx/maptbx/structure_factors.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <mmtbx/masks/atom_mask.h>
#include <cctbx/miller.h>
#include <complex>

using namespace scitbx;
typedef cctbx::miller::index<int> millnx;
typedef std::complex<double> cmplex;

namespace phasertng { namespace fft {

  class fftbase
  {
    public:
      fftbase() {}
      fftbase(cctbx::sgtbx::space_group_type, cctbx::uctbx::unit_cell);

    public:
     //may want to set up other factors first
      void calculate_gridding(double,std::pair<double,double>,bool);
      void calculate_gridding(double,cctbx::uctbx::unit_cell);

      //setters
      void shannon(double s) { Shannon = s; }
      void mandatory_factor(int i) { mandatory_factors = {i,i,i}; }
      void translation_search_symmetry_flags(bool,bool);

    protected:
      cctbx::sgtbx::space_group_type SG;
      cctbx::uctbx::unit_cell UC;
      double Shannon = 3;
      af::int3 mandatory_factors = af::int3(2,2,2);
     void setup_u_base();

    public:
      //bool is_isotropic_search_model = true;
      double u_extra = 0; //eliminate_u_extra_and_normalize;
      double u_base = 0; //eliminate_u_extra_and_normalize;
      double focus_size_1d = 0;
      cctbx::sgtbx::search_symmetry_flags sym_flags = cctbx::sgtbx::search_symmetry_flags(true);
      bool conjugate_flag  = true;
      double dmin = 0;
      af::int3 gridding = {0,0,0};
      bool memory_allocation_error = false;
      cctbx::maptbx::grid_tags<long> tags;

    public:
      double multiplier() { return UC.volume()/focus_size_1d; } //eliminate_u_extra_and_normalize

      void
      add_bulk_solvent(
          af::shared<cctbx::xray::scatterer<double> >,
          double,double,
          af::shared<millnx> miller_indices,
          af::shared<cmplex> structure_factors) const;

    //pure virtual function, class cannot be instantiated
    virtual cctbx::af::c_interval_grid<3>::index_type n_grid() const = 0;
  };

}}
#endif
