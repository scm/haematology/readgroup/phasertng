//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_FileSystem_class____
#define __phasertng_FileSystem_class____
#include <string>
#include <fstream>
#include <phasertng/main/includes.h>

namespace phasertng {

class FileSystem
{
  //filesystem is either database/filepath/filename or
  //                     database/filename (script)
  //                     database/filepath (creation)
  public:
    std::string database = "None";
    std::string filepath = "None";
    std::string filename = "None";

  public: //constructor
    FileSystem() {}
    FileSystem(std::string);
    FileSystem(std::string,std::pair<std::string,std::string>);
    FileSystem(std::tuple<std::string,std::string,std::string>); //for python, strictly database,subdir,filename
    FileSystem(std::string,std::string,std::string); //for python, strictly database,subdir,filename
    void SetFileSystem(std::string,std::pair<std::string,std::string>);
    void SetFileSystem(std::string="");

  public:
    std::string fstr() const;
    std::string stem() const;
    std::string qfstrq() const;
    bool None(std::string) const;
    bool filesystem_is_set() const;
    bool file_exists();
    bool filepath_exists();
    bool is_a_directory();
    void set_filename(std::string);
    std::string full_path(std::string);
    std::string parent_path();
    void prepare_and_check_database() const;
    void create_directories_and_stream(std::fstream&);
    void create_directories_for_file();
    void write(std::string);
    void write_array(af_string);
    std::tuple<std::string,std::string,std::string> get_tuple();
    void findFileWithEnding(const std::string&);
    bool flat_filesystem();
};

}
#endif
