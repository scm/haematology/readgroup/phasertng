//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Selected_class__
#define __phasertng_Selected_class__
#include <phasertng/main/includes.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/pointers.h>
#include <phasertng/math/table/sigesq_acentric.h>
#include <phasertng/math/table/sigesq_centric.h>
#include <phasertng/enum/rejected_code.h>
#include <phasertng/math/likelihood/isad/sigmaa.h>
#include <phasertng/enum/rejected_code.h>
#include <phasertng/enum/friedel_type.h>
#include <map>
#include <set>
#include <limits>

namespace phasertng {

  namespace pod { class probtype; } //forward declaration of class below
  typedef std::map<int,pod::probtype> ProbType;
  //reflection and probtype info

  //these functions used to flip a map sorted on key to a multimap, which is map sorted by value
  template<typename A, typename B>
  std::pair<B,A> flip_pair(const std::pair<A,B> &p)
  {
    return std::pair<B,A>(p.second, p.first);
  }

  template<typename A, typename B>
  std::multimap<B,A> flip_map(const std::map<A,B> &src)
  {
    std::multimap<B,A> dst;
    std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()), flip_pair<A,B>);
    return dst;
  }

//this class handles the outlier rejection information and the selected array
//note that boolean selected array is always stored in column format regardless of the
//row-wise or col-wise storage of the rest of the reflection data
//Selected is a base class of Reflections
//along with ReflBase, which handles the conserved header information and functions
//Selected also contains conserved information, but is ephemeral, not being output to mtz file
//hence the separate class for this information

class Selected
{
  double def_sigesqr_cent = table::sigesq_centric().get(0.01);
  double def_sigesqr_acent = table::sigesq_acentric().get(0.01);
  int  NTHREADS;
  bool USE_STRICTLY_NTHREADS;
  bool STICKY_TNCS_MODELLED;
  public:
    Selected(int n,bool s,bool t) : NTHREADS(n),USE_STRICTLY_NTHREADS(s),STICKY_TNCS_MODELLED(t) {}
    ~Selected() throw() {}
    af_string parse(
      bool OUT_REJ_,
      double OUT_INFO_,
      double OUT_PROB_,
      int LOGGED_);
    af_string set_resolution(
      double data_hires = 0,
      std::pair<double,double> database_resolution = {0,0},
      double io_hires = 0);

  public:
    //the map index int (r) is the important index for flagging reflections in SELECTED
    //and is required even though the miller and resolution information are duplicitously stored in probtype
    //After the selection has been done, the probabilities and criteria could be deleted
    //as they are only used again for logging
    //probabilities map length should be considerably shorter than reflection list unless the data are appalling
    ProbType PROBABILITIES; //concise storage of outlier information
    std::set<rejected::code>  CRITERIA; //current selection criteria rejection codes for flagging SELECTED array below

    sv_bool SELECTED; //important array for get selected //must be deep_copy by default
    bool    OUT_REJ =  true; //default same as master_phil_file
    double  OUT_INFO = 0.01; //default same as master_phil_file
    double  OUT_PROB = 1e-06; //default same as master_phil_file
    int     LOGGED = 20;
    //the selection hires and lores are sticky, in that after they are set the
    //resolution cutoffs continue to be appled every time there is a selection taking place
    double  STICKY_HIRES = 0;
    double  STICKY_LORES = DEF_LORES;

  public: //getter
    bool get_selected(int& r);
    sv_bool get_selected_array();
    af_bool get_selected_shared();
    void flag_outliers_and_select(
           const_ReflectionsPtr,
           const std::set<rejected::code>,
           sv_double=sv_double(), //epsnSigmaN, can be created in situ
           likelihood::isad::sigmaa=likelihood::isad::sigmaa()  //carries absrhoFF and rhopm by bin
         );

  public:
    int    nsel() const;
    double percent_selected() const;
    int    nrej() const;
    std::map<rejected::code,int> count_selected() const;
    std::pair<double,double> sigesqr_values() const;

  public:
    af_string logOutliers(std::string) const;
    af_string logSelected(std::string) const;

  private:
    double resolution_selected = 0;
    ProbType flag_fixed_criteria(
        const_ReflectionsPtr,bool,bool,bool,bool,bool,int,int) const;
    ProbType flag_epsnsigman_criteria(
        const_ReflectionsPtr,const sv_double,bool,bool,bool,bool,int,int) const;
    ProbType flag_badanom(
        const_ReflectionsPtr,const sv_double,likelihood::isad::sigmaa,int,int) const;
    void append_probabilities(
        ProbType&);

  public:
    std::set<rejected::code> standard_criteria();
    std::set<rejected::code> minimal_criteria();
    std::set<rejected::code> hires_criteria();
    void flag_standard_outliers_and_select(
           const_ReflectionsPtr
         );
    ProbType flag_standard(
           const_ReflectionsPtr,
           int,
           int
         ) const;

};

namespace pod {
class probtype
{
  private:
    double maxprob = std::numeric_limits<double>::max();
    double PROBNAT = std::numeric_limits<double>::max();
    double PROBPOS = std::numeric_limits<double>::max();
    double PROBNEG = std::numeric_limits<double>::max();
    double SIGESQR = 0;

  public: //members
    std::set<rejected::code> CODES;
    double RESO = 0;
    millnx MILLER = {0,0,0};

  public: //constructor
    probtype() {}

    probtype(const millnx& miller,const double& resolution)
    {
      MILLER = miller;
      RESO = resolution;
    }

    probtype(const millnx& miller,const double& resolution,const rejected::code rej)
    { //creates outlier and inserts first rejection code into the list
      MILLER = miller;
      RESO = resolution;
      CODES.insert(rej);
    }

    double get_sigessqr() const//return worst of the lot
    { return SIGESQR; }

    void set_sigessqr(const double v) //return worst of the lot
    { SIGESQR = v; }

    //maxprobs will never be smaller than any set value
    void set_probability(friedel::type f,const double v) //return worst of the lot
    {
      if (f == friedel::NAT) PROBNAT = v;
      if (f == friedel::POS) PROBPOS = v;
      if (f == friedel::NEG) PROBNEG = v;
     // phaser_assert(f != friedel::UNDEFINED);
    }

    double lowest_wilson_probability() const //return worst of the lot
    { return std::min(PROBNAT,std::min(PROBPOS,PROBNEG)); }

    double probability_str(friedel::type f) const
    {
      if (f == friedel::NAT) return PROBNAT == maxprob ? 0 : (PROBNAT);
      if (f == friedel::POS) return PROBPOS == maxprob ? 0 : (PROBPOS);
      if (f == friedel::NEG) return PROBNEG == maxprob ? 0 : (PROBNEG);
      return 0;
    }

  public: //function
    bool operator<(const probtype &right) const
    {
      return lowest_wilson_probability() < right.lowest_wilson_probability();
    } //least probable first in the list
};
} //pod

} //phasertng
#endif
