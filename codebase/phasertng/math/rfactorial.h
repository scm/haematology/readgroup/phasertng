#ifndef __phasertng_rfactorial_function__
#define __phasertng_rfactorial_function__

namespace phasertng {

  inline double rfactorial(int n)
  { return (n>1) ?  n*rfactorial(n-1): 1.0; }
  inline int max_rfactorial()
  { return 49; } //<= i.e. <50

}//end namespace phasertng

#endif
