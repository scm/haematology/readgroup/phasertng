//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_random_unsigned__
#define __phasertng_random_unsigned__
#include <random>

namespace phasertng {

  inline unsigned random_unsigned()
  {
    std::random_device rd;     //Get a random seed from the OS entropy device, or whatever
    std::mt19937_64 eng(rd()); //Use the 64-bit Mersenne Twister 19937 generator
                               //and seed it with entropy.
    //Define the distribution, by default it goes from 0 to MAX
    std::uniform_int_distribution<unsigned> distr;
    //Generate random numbers
    return distr(eng);
  }

} //phasertng
#endif
