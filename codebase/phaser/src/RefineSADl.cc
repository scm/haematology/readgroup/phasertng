//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/lib/maths.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/BinStats.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/dag/Helper.h>
#include <cctbx/adptbx.h>

namespace phaser {

af_string RefineSAD::logScale()
{
  af_string output;
  if (input_atoms and !FIX_SCALES)
  {
    output.push_back("Scales for Atomic Structure:");
    output.push_back("Overall Scale: " + std::to_string(ScaleK));
    output.push_back("Overall B-factor: " + std::to_string(cctbx::adptbx::u_as_b(ScaleU)));
    output.push_back("");
  }
  if (input_partial and !FIX_SCALES)
  {
    output.push_back("Scales for Partial Structure:");
    output.push_back("Overall Partial Scale: " + std::to_string(PartK));
    output.push_back("Overall Partial B-factor: " + std::to_string(cctbx::adptbx::u_as_b(PartU)));
    output.push_back("");
  }
  return output;
}

af_string RefineSAD::logAtoms()
{
  af_string output;
  bool verbose_anisob(false);
  assert(atoms.size() == atoms.size());
  int natoms(0);
  for (int a = 0; a < atoms.size(); a++) if (!atoms[a].XTRA.rejected) natoms++;
  output.push_back("Atom Substructure: "+std::to_string(natoms)+" atom"+std::string((natoms==1)?"":"s")+" in list");
  if (input_partial)
  {
    natoms = COORDINATES->PDB.size();
    if (natoms)
    output.push_back("Partial Structure: "+std::to_string(natoms)+" atom"+std::string((natoms==1)?"":"s")+" in list");
  }
  int naniso(0);
  for (unsigned a = 0; a < atoms.size(); a++)
  {
    if (atoms[a].SCAT.flags.use_u_aniso_only())
    {
      if (verbose_anisob and output.size() == 1)
      {
        output.push_back("ANISU, BETA and USTAR are the anisotropic B-factor including the isotropic B-factor");
        output.push_back("ANISU is printed as for pdb output");
        output.push_back("EIGEN are the eigenvalues of the anisotropy tensor including the isotropic B-factor");
      }
      naniso++;
    }
  }
  output.push_back("Number of anisotropic atoms = " + std::to_string(naniso));
  int dottenth=2;
  int wA = std::max(std::log10(A())+1,2.)+dottenth;
  int wB = std::max(std::log10(B())+1,2.)+dottenth;
  int wC = std::max(std::log10(C())+1,2.)+dottenth;
  int cellw = std::max(wC,std::max(wA,wB));
  if (atoms.size())
    output.push_back(phasertng::snprintftos("%-5s %s=%s %7s %7s (%-6s) %2s %4s %8s %8s",
            "#",
            phasertng::dag::header_fract().c_str(),
            phasertng::dag::header_ortht(cellw).c_str(),
            "O","B","AnisoB","M","Z","Atomtype","Rejected"));
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  for (unsigned a = 0; a < atoms.size(); a++)
  {
    auto SCAT = atoms[a].SCAT;
    auto XTRA = atoms[a].XTRA;
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    auto orth = cctbxUC.orthogonalization_matrix()*SCAT.site;
    int p = SCAT.occupancy < 10 ? 3 : 2;
    if (!SCAT.flags.use_u_aniso_only())
    {
      output.push_back(phasertng::snprintftos("%-5d %s=%s %7.*f %7.2f (%6s) %2d %4.1f %8s %8s",
          a+1,
          phasertng::dag::print_fract(SCAT.site).c_str(),
          phasertng::dag::print_ortht(orth,cellw).c_str(),
          p,SCAT.occupancy,
          cctbx::adptbx::u_as_b(SCAT.u_iso)," ---- ",
          M,
          XTRA.zscore,
          SCAT.scattering_type.c_str(),XTRA.rejected ? "Y":"N"));
    }
    else
    {
      cctbx::adptbx::factor_u_star_u_iso<double> factor(UnitCell::getCctbxUC(),SCAT.u_star);
      dvect3 eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),factor.u_star_minus_u_iso));
      double minU(std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2])));
      double maxU(std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2])));
      output.push_back(phasertng::snprintftos("%-5d %s=%s %7.*f %7.1f (%+6.1f) %2d %4.1f %8s %8s",
          a+1,
          phasertng::dag::print_fract(SCAT.site).c_str(),
          phasertng::dag::print_ortht(orth,cellw).c_str(),
          p,SCAT.occupancy,
          cctbx::adptbx::u_as_b(factor.u_iso),cctbx::adptbx::u_as_b(maxU-minU),
          M,
          XTRA.zscore,
          SCAT.scattering_type.c_str(),XTRA.rejected ? "Y":"N"));
      if (verbose_anisob)
      {
      eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),SCAT.u_star));
      output.push_back(phasertng::snprintftos("EIGEN  %6.3f %6.3f %6.3f",
        cctbx::adptbx::u_as_b(eigenUvals[0]),cctbx::adptbx::u_as_b(eigenUvals[1]),cctbx::adptbx::u_as_b(eigenUvals[2])));
      dmat6 u_cart = cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),SCAT.u_star);
      u_cart *= u_cart_pdb_factor;
      output.push_back(phasertng::snprintftos("ANISU  %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f",
        u_cart[0],u_cart[1],u_cart[2],u_cart[3],u_cart[4],u_cart[5]));
      dmat6 beta = cctbx::adptbx::u_star_as_beta(SCAT.u_star);
      output.push_back(phasertng::snprintftos("BETA   %9.8f %9.8f %9.8f %9.8f %9.8f %9.8f",
        beta[0],beta[1],beta[2],beta[3],beta[4],beta[5]));
      output.push_back(phasertng::snprintftos("USTAR  %9.8f %9.8f %9.8f %9.8f %9.8f %9.8f",
        SCAT.u_star[0],SCAT.u_star[1],SCAT.u_star[2],SCAT.u_star[3],SCAT.u_star[4],SCAT.u_star[5]));
      }
    }
  }
  if (!atoms.size())
    output.push_back("There were no substructure atoms");
  output.push_back("");
  return output;
}

af_string RefineSAD::logSigmaA(bool verbose)
{
  af_string output;
  output.push_back("SigmaA Parameters:");
  int smin(0),smax(bin.numbins()-1);
  output.push_back("SDsqr: (lores-hires) " + phasertng::dtos(SDsqr_bin[smin],12,0) + " to " + phasertng::dtos(SDsqr_bin[smax],12,0));
  output.push_back("Dphi : (lores-hires) " + phasertng::dtos(DphiA_bin[smin],10,8)+","+phasertng::dtos(DphiB_bin[smin],10,8) + ") to (" + phasertng::dtos(DphiA_bin[smax],10,8)+","+phasertng::dtos(DphiB_bin[smax],10,8) + ")");
  output.push_back("Splus: (lores-hires) " + phasertng::dtos(SP_bin[smin],12,8) + " to " + phasertng::dtos(SP_bin[smax],12,8));
  if (verbose)
  {
    output.push_back(phasertng::snprintftos("%3s  %12s    %-24s  %-8s","Bin","SDsqr","s12 (real,imag)","Sigma+"));
    for (int s = 0; s < bin.numbins(); s++)
      output.push_back(phasertng::snprintftos("%3d: %12.0f    (% 10.8f,% 10.8f) % 8f", s+1,SDsqr_bin[s],DphiA_bin[s],DphiB_bin[s],SP_bin[s]));
  }
  output.push_back("");
  return output;
}

af_string RefineSAD::logScat()
{
  if (!input_atoms and !input_partial) return af_string();
  af_string output;
  output.push_back("Scattering Parameters:");
  output.push_back(phasertng::snprintftos("%5s %14s %14s", "Atom","f\"","(f\')"));
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    assert(t < t2atomtype.size());
    output.push_back(phasertng::snprintftos("%5s %14.4f %14.4f", t2atomtype[t].c_str(),AtomFdp[t],AtomFp[t]));
  }
  output.push_back("");
  return output;
}

af_string RefineSAD::logStatInfo()
{
  af_string output;
  output.push_back("Unweighted:");
  output.push_back("Statistics are calculated with \'bestphase\' PhiB");
  output.push_back("Phase difference =  < |PhiB - (Phi of FH)|> ");
  output.push_back("Weighted");
  output.push_back("Statistics are calculated as probablity weighted sum over Phi (0-360)");
  output.push_back("Wt Phase difference =  < SumPhi Prob(Phi)*|Phi - (Phi of FH)|> ");
  output.push_back("where <f(x)> is the mean value of f(x) for the reflections in the bin");
  output.push_back("Arithmetic averages are used instead of root mean square (rms) averages,");
  output.push_back("because although the latter are theoretically correct, they are very");
  output.push_back("sensitive to outliers");
  output.push_back("The mean absolute phase difference should be 90deg (within about 10deg)");
  output.push_back("The standard deviation should be 52deg for acentrics and 90deg for centrics,");
  output.push_back("although these values will probably not be attained in SIR or when there");
  output.push_back("is significant correlation between the phased and derivative structures");
  output.push_back("i.e. the heavy atoms replace atoms in the phased structure");
  output.push_back("");
  return output;
}

af_string RefineSAD::logFOM(bool verbose)
{
  if (!input_atoms and !input_partial) return af_string();
  af_string output;
  calcBinStats();
  output.push_back("Figures of Merit:");
  int w = phasertng::itow(allStats.Num(),6);
  int ww = w+6;
  output.push_back(phasertng::snprintftos("%5s %3s %-12s %-*s %-*s %-*s %-*s",
          "","Bin","Resolution",ww,"Acent(+&-)",ww,"Centric",ww,"Acent(+|-)",ww,"Total"));
  output.push_back(phasertng::snprintftos("%5s %3s %-12s %-*s %5s %-*s %5s %-*s %5s %-*s %5s",
          "","","",w,"Number","--FOM",w,"Number","--FOM",w,"Number","--FOM",w,"Number","--FOM"));
  if (verbose)
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    output.push_back(phasertng::snprintftos("%5s %3d %6.2f-%5.2f %*d %5.3f %*d %5.3f %*d %5.3f %*d %5.3f",
       "",s+1, bin.LoRes(s), bin.HiRes(s),
       w,acentStats.Num(s), acentStats.FOM(s),
       w,centStats.Num(s), centStats.FOM(s),
       w,singleStats.Num(s), singleStats.FOM(s),
       w,allStats.Num(s), allStats.FOM(s)));
  }
  output.push_back(phasertng::snprintftos("%5s FOM %6.2f-%5.2f %*d %5.3f %*d %5.3f %*d %5.3f %*d %5.3f",
       "Total",bin.LoRes(0),bin.HiRes(bin.numbins()-1),
       w,acentStats.Num(), acentStats.FOM(),
       w,centStats.Num(), centStats.FOM(),
       w,singleStats.Num(), singleStats.FOM(),
       w,allStats.Num(), allStats.FOM()));
  output.push_back("");
  return output;
}

af_string RefineSAD::logDataStats(unsigned nrefl)
{
  af_string output;
  output.push_back("Reflection statistics:");
  int nboth(0),npos(0),nneg(0),ncent(0);
  int1D NumInBin(bin.numbins(),0),NumInBinC(bin.numbins(),0),NumInBinA(bin.numbins(),0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    if (cent[r]) ncent++;
    else if (both[r]) nboth++;
    else if (plus[r]) npos++;
    else nneg++;
    cent[r] ? NumInBinC[rbin[r]]++ : NumInBinA[rbin[r]]++;
    NumInBin[rbin[r]]++;
  }
  output.push_back("There were " +std::to_string(ncent) + " centric reflections");
  output.push_back("There were " +std::to_string(nboth) + " acentric reflections with F+ and F-");
  output.push_back("There were " +std::to_string(npos) + " acentric reflections with F+ only");
  output.push_back("There were " +std::to_string(nneg) + " acentric reflections with F- only");
  output.push_back("");
  output.push_back(phasertng::snprintftos("%3s    %10s    %5s %9s %9s","Bin","Resolution","#All","#Centric","#Acentric"));
  for (unsigned s = 0; s < bin.numbins(); s++)
    output.push_back(phasertng::snprintftos("%3d   %5.2f - %5.2f  %5d %9d %9d",
        s+1, bin.LoRes(s), bin.HiRes(s), NumInBin[s],NumInBinC[s],NumInBinA[s]));

  output.push_back("");

  if (ncent)
  {
    output.push_back("CENTRIC reflections:");
    output.push_back(phasertng::snprintftos("%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso"));
    output.push_back(phasertng::snprintftos("%12s %10s","F","SIGF"));
    unsigned rcent(0);
    for (int r = 0; r < NREFL && rcent < nrefl; r++)
    if (!bad_data[r] && cent[r])
    {
      output.push_back(phasertng::snprintftos("%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r]));
      rcent++;
    }
    output.push_back("");
  }
  if (nboth)
  {
    output.push_back("ACENTRIC reflections:");
    output.push_back(phasertng::snprintftos("%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso"));
    output.push_back(phasertng::snprintftos("%12s %10s %12s %10s","F(+)","SIGF(+)","F(-)","SIGF(-)"));
    unsigned rboth(0);
    for (int r = 0; r < NREFL && rboth < nrefl; r++)
    if (!bad_data[r] && both[r])
    {
      output.push_back(phasertng::snprintftos("%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f %12.3f %10.3f",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r],NEG.F[r],NEG.SIGF[r]));
      rboth++;
    }
    output.push_back("");
  }
  if (npos)
  {
    output.push_back("SINGLETON(+) reflections:");
    output.push_back(phasertng::snprintftos("%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso"));
    output.push_back(phasertng::snprintftos("%12s %10s %12s %10s","F(+)","SIGF(+)","F(-)","SIGF(-)"));
    unsigned rpos(0);
    for (int r = 0; r < NREFL && rpos < nrefl; r++)
    if (!bad_data[r] && !cent[r] && !both[r] && plus[r])
    {
      output.push_back(phasertng::snprintftos("%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f %12s %10s",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r],"absent","absent"));
      rpos++;
    }
    output.push_back("");
  }
  if (nneg)
  {
    output.push_back("SINGLETON(-) reflections:");
    output.push_back(phasertng::snprintftos("%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso"));
    output.push_back(phasertng::snprintftos("%12s %10s %12s %10s","F(+)","SIGF(+)","F(-)","SIGF(-)"));
    unsigned rneg(0);
    for (int r = 0; r < NREFL && rneg < nrefl; r++)
    if (!bad_data[r] && !cent[r] && !both[r] && !plus[r])
    {
        output.push_back(phasertng::snprintftos("%-3d %-3d %-3d (%5d) %4.3f %5.2f %12s %10s %12.3f %10.3f",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),"absent","absent",NEG.F[r],NEG.SIGF[r]));
      rneg++;
    }
    output.push_back("");
  }
  return output;
}

af_string RefineSAD::logCurrent()
{
  af_string output;
  output.push_back(">>Current parameters<<");
  auto txt = logScale();
  output.insert(output.end(),txt.begin(),txt.end());
  if (!FIX_ATOMIC)
  {
    txt = logAtoms();
    output.insert(output.end(),txt.begin(),txt.end());
    txt = logScat();
    output.insert(output.end(),txt.begin(),txt.end());
  }
  if (!FIX_SIGMAA and FIX_ATOMIC)
  {
    txt = logSigmaA();
    output.insert(output.end(),txt.begin(),txt.end());
  }
  if (overall_fom() != 0)
    txt = logFOM();
  output.insert(output.end(),txt.begin(),txt.end());
  output.push_back("Log-likelihood = " + std::to_string(-atoms.LLG));
  output.push_back("");
  return output;
}

double RefineSAD::overall_fom()
{
  if (!input_atoms and !input_partial) return 0;
  calcBinStats();
  return allStats.FOM();
}

af_string RefineSAD::logInitial()
{
  af_string output;
  output.push_back(">>Initial parameters<<");
  auto txt = logAtoms(); //FIX_ATOMIC not yet set
  output.insert(output.end(),txt.begin(),txt.end());
  output.push_back("");
  return output;
}

af_string RefineSAD::logFinal()
{
  af_string output;
  output.push_back(">>Final parameters<<");
  auto txt = logOutliers(false);
  output.insert(output.end(),txt.begin(),txt.end());
  txt = logScale();
  output.insert(output.end(),txt.begin(),txt.end());
  if (!FIX_ATOMIC)
  {
    txt = logAtoms();
    output.insert(output.end(),txt.begin(),txt.end());
    txt = logScat();
    output.insert(output.end(),txt.begin(),txt.end());
  }
  if (!FIX_SIGMAA and FIX_ATOMIC)
  {
    txt = logSigmaA();
    output.insert(output.end(),txt.begin(),txt.end());
  }
  if (overall_fom() != 0)
    txt = logFOM();
  output.insert(output.end(),txt.begin(),txt.end());
  output.push_back("");
 // double llg = -getHandEP().getLogLikelihood();
  output.push_back("Log-likelihood = " + std::to_string(-atoms.LLG));
  double Rfac = Rfactor();
  output.push_back("Final R-factor = "+ phasertng::dtos(Rfac,5,1));
  return output;
}

af_string RefineSAD::logProtocolPars(bool verbose)
{
  af_string output;
  if (verbose)
  {
    output.push_back("Parameters to refine:");
    char YES('+'),NO('-');
  //--sigmaa--
    char SAYN = protocol.FIX_SA ? YES : NO;
    char SBYN = protocol.FIX_SB ? YES : NO;
    char SPYN = protocol.FIX_SP ? YES : NO;
    char SDYN = protocol.FIX_SD ? YES : NO;
    output.push_back(phasertng::snprintftos("Variances SA SB SP SD: %c %c %c %c (%d Bins)",SAYN,SBYN,SPYN,SDYN,bin.numbins()));

  //-- scales --
    if (input_atoms)
    {
      output.push_back(phasertng::snprintftos("Overall Scale:         %c",protocol.FIX_K ? YES : NO)); //ScaleK
      output.push_back(phasertng::snprintftos("Overall B-factor:      %c",protocol.FIX_B ? YES : NO)); //ScaleU
    }
    if (input_partial)
    {
      output.push_back(phasertng::snprintftos("Partial Scale:         %c",protocol.FIX_PARTK ? YES : NO)); //PartK
      output.push_back(phasertng::snprintftos("Partial B-factor:      %c",protocol.FIX_PARTU ? YES : NO)); //PartU
    }

  //-- atomic --
    int last_diff_a(0),last_a(0);
    char lastXYN(' '),lastOYN(' '),lastBYN(' '),lastAYN(' ');
    int natoms(0);
    for (unsigned a = 0; a < atoms.size(); a++) if (!atoms[a].XTRA.rejected) natoms++;
    bool first_time(true);
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
    {
      char XYN = protocol.FIX_XYZ ? YES : NO;
      char OYN = protocol.FIX_OCC ? YES : NO;
      char BYN = !atoms[a].SCAT.flags.use_u_aniso_only() ? (protocol.FIX_BFAC ? YES : NO) : NO;
      char AYN = atoms[a].SCAT.flags.use_u_aniso_only() ? (protocol.FIX_BFAC ? YES : NO) : NO;
      if (first_time)
      {
        output.push_back(phasertng::snprintftos("Atomic X Y Z O B AB:"));
        lastXYN = XYN; lastOYN = OYN; lastBYN = BYN; lastAYN = AYN;
        first_time = false;
      }
      if (lastXYN != XYN || lastOYN != OYN || lastBYN != BYN || lastAYN != AYN)
      {
        if (last_diff_a < last_a)
          output.push_back(phasertng::snprintftos("Atoms #%-3i to #%-3i   %c %c %c %c %c %c",
             last_diff_a+1,last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN));
        else
          output.push_back(phasertng::snprintftos("Atom  #%-3i           %c %c %c %c %c %c",
             last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN));
        last_diff_a = a;
        { lastXYN = XYN; lastOYN = OYN; lastBYN = BYN; lastAYN = AYN; }
      }
      last_a = a;
    }
    if (last_diff_a < last_a && natoms)
      output.push_back(phasertng::snprintftos("Atoms #%-3i to #%-3i   %c %c %c %c %c %c",
         last_diff_a+1,last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN));
    else if (natoms)
      output.push_back(phasertng::snprintftos("Atom  #%-3i           %c %c %c %c %c %c",
         last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN));
    else if (!input_atoms) //phenix, setFC
      output.push_back("Atomic X Y Z O B AB:   None (partial structure only)");

  //Fdp
    for (int t = 0; t < AtomFdp.size(); t++)
      output.push_back(phasertng::snprintftos("Atom Type %2s f\":       %c",t2atomtype[t].c_str(),protocol.FIX_FDP ? YES : NO));
    output.push_back("");
  }
  atoms.size() == 1 ?
    output.push_back("There is 1 atom"):
    output.push_back("There are " + std::to_string(atoms.size()) + " atoms");
  use_fft ?
    output.push_back("FFT method will be used for structure factor and gradient calculations"):
    output.push_back("Direct summation will be used for structure factor and gradient calculations");
  output.push_back("");
  return output;
}

af_string RefineSAD::logOutliers(bool verbose)
{
  af_string output;
  output.push_back("Outlier Rejection");
  if (!OUTLIER.REJECT)
  {
    output.push_back("Outliers will not be rejected");
    output.push_back("");
    return output;
  }
  if (!OUTLIER.ANO.size() && !OUTLIER.SAD.size())
  {
    output.push_back("There were 0 reflections rejected as outliers");
    output.push_back("");
    return output;
  }
  if (OUTLIER.ANO.size())
  {
    double percent = 100.*double(OUTLIER.ANO.size())/NREFL;
    OUTLIER.ANO.size() == 1 ?
    output.push_back("There was 1 reflection of " + std::to_string(NREFL) + " (" + phasertng::dtos(percent,4) + "%) rejected as Wilson outlier"):
    output.push_back("There were " + std::to_string(OUTLIER.ANO.size()) + " reflections of " + std::to_string(NREFL) + " (" + phasertng::dtos(percent,4) + "%) rejected as Wilson outliers");
    output.push_back("Measurements with a probability less than " + std::to_string(OUTLIER.PROB) + " are rejected");
    output.push_back("Measurements with fewer bits of expected information than " + std::to_string(OUTLIER.INFO) + " are rejected:");
    output.push_back("threshold sigma(Eobs^2) for centrics/acentrics is " + std::to_string(OUTLIER.SIGESQ_CENT) + " / " + std::to_string(OUTLIER.SIGESQ_ACENT));
    if (verbose)
    {
    output.push_back("");
    int maxprint(20);
    output.push_back(phasertng::snprintftos("%4s %4s %4s %6s  %10s  %10s %11s %7s %8s","H","K","L","reso","Eo^2","sigma","probability","wilson","low-info"));
    for (int o = 0; o < OUTLIER.ANO.size(); o++)
    {
      int& r = OUTLIER.ANO[o].refl;
      int H(std::round(miller[r][0]));
      int K(std::round(miller[r][1]));
      int L(std::round(miller[r][2]));
      double reso = UnitCell::reso(miller[r]);
      if (o == maxprint) output.push_back("More than " + std::to_string(maxprint) + " outliers (see VERBOSE output)");
      if (std::fabs(OUTLIER.ANO[o].eosqr) < 1.0e+7 && std::fabs(OUTLIER.ANO[o].sigesqr) < 1.0e+7)
        output.push_back(phasertng::snprintftos("%4i %4i %4i %6.2f % 11.3f % 11.3f %11.3e %-7s %-8s", H,K,L,reso,OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,phasertng::btos(OUTLIER.ANO[o].wilson).c_str(),phasertng::btos(OUTLIER.ANO[o].lowInfo).c_str()));
      else
        output.push_back(phasertng::snprintftos("%4i %4i %4i %6.2f % 11.6e % 11.6e %11.3e %-7s %-8s", H,K,L,reso,OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,phasertng::btos(OUTLIER.ANO[o].wilson).c_str(),phasertng::btos(OUTLIER.ANO[o].lowInfo).c_str()));
    }
    output.push_back("");
    }
  }
  if (OUTLIER.SAD.size())
  {
    double percent = 100.*double(OUTLIER.SAD.size())/NREFL;
    OUTLIER.SAD.size() == 1 ?
    output.push_back("There was 1 reflection of " + std::to_string(NREFL) + " (" + phasertng::dtos(percent,4) + "%) rejected as SAD outlier"):
    output.push_back("There were " + std::to_string(OUTLIER.SAD.size()) + " reflections of " + std::to_string(NREFL) + " (" + std::to_string((OUTLIER.SAD.size()*100)/double(NREFL)) + "%) rejected as SAD outliers");
    if (verbose)
    {
    std::sort(OUTLIER.SAD.begin(),OUTLIER.SAD.end());
    output.push_back("Reflections listed with most improbable first");
    output.push_back("Criterion on which reflection rejected indicated with *");
    output.push_back("");
    int num_output = 10;
    bool first_print(true);
    int counto(0);
    for (int o = 0; o < OUTLIER.SAD.size(); o++)
    {
      if (first_print)
      output.push_back(phasertng::snprintftos("%3s %3s %3s %8s %6s %9s %-9s %8s %8s %8s",
        "H","K","L","","reso","F+","F-","P(F+)","P(F-)","P(F+;F-)"));
      first_print = false;
      int r = OUTLIER.SAD[o].refl;
      std::string ac = cent[r] ? "centric " : "acentric";
      if ((cent[r]) || (!both[r] &&  plus[r]))
      output.push_back(phasertng::snprintftos("%3i %3i %3i %s %6.1f %9.1f %9s %3.1e*",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),
        UnitCell::reso(miller[r]),
        POS.F[r],"",OUTLIER.SAD[o].probPos));
      else if (!both[r] && !plus[r])
      output.push_back(phasertng::snprintftos("%3i %3i %3i %s %6.1f %9s %-9.1f %8s %3.1e*",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),
        UnitCell::reso(miller[r]),
        "",NEG.F[r],"",OUTLIER.SAD[o].probNeg));
      else
      output.push_back(phasertng::snprintftos("%3i %3i %3i %s %6.2f %9.1f %-9.1f %3.1e%s %3.1e%s %3.1e%s",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),UnitCell::reso(miller[r]),
        POS.F[r],NEG.F[r],
        OUTLIER.SAD[o].probPos, OUTLIER.SAD[o].probPos < OUTLIER.SAD[o].probLow() ? "*" : " ",
        OUTLIER.SAD[o].probNeg, OUTLIER.SAD[o].probNeg == OUTLIER.SAD[o].probLow() ? "*" : " ",
        OUTLIER.SAD[o].probCon, OUTLIER.SAD[o].probCon == OUTLIER.SAD[o].probLow() ? "*" : " "));
      counto++;
      if (counto == num_output) output.push_back(" <-------  Top " + std::to_string(num_output) + " (use VERBOSE to print all) ------->");
    }
    if (!first_print) output.push_back("");
    }
  }
  return output;
}

} //namespace phaser
