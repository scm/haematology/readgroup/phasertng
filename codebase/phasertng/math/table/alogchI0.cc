#include <phasertng/math/table/alogchI0.h>
#include <cmath>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace table {

constexpr const double table::alogchI0::lnI0Table1[4097][2];
constexpr const double table::alogchI0::lnI0Table2[4097][2];
constexpr const double table::alogchI0::lnI0Table3[4097][2];
constexpr const double table::alogchI0::lnI0InvTable[4097][2];
  alogchI0::alogchI0()
  {
    argfac1 = tbllen/argcut1;
    argfac2 = tbllen/argcut2;
    argfac3 = tbllen/argcut3;
    invArgFac = tbllen*argcut3;
  }

  double
  alogchI0::get(double arg) const
  {
      if (arg < argcut1)
      {
        double fracTableArg = argfac1*arg;
        int index = static_cast<int>(fracTableArg);
        phaser_assert(index < tbllen1);
        return double(lnI0Table1[index][0] + (fracTableArg-index)*lnI0Table1[index][1]);
      }
      else if (arg < argcut2)
      {
        double fracTableArg = argfac2*arg;
        int index = static_cast<int>(fracTableArg);
        phaser_assert(index < tbllen1);
        return double(lnI0Table2[index][0] + (fracTableArg-index)*lnI0Table2[index][1]);
      }
      else if (arg < argcut3)
      {
        double fracTableArg = argfac3*arg;
        int index = static_cast<int>(fracTableArg);
        return double(lnI0Table3[index][0] + (fracTableArg-index)*lnI0Table3[index][1]);
      }
      else
      {
        double HALF(0.5);
        double ONE = 1;
        double invArg(ONE/arg);
        double fracTableArg = invArgFac*invArg;
        int index = static_cast<int>(fracTableArg);
        phaser_assert(index < tbllen1);
        return double((lnI0InvTable[index][0] + (fracTableArg-index)*lnI0InvTable[index][1]) + arg - HALF*std::log(arg));
      }
      return 0;
  }

  alogchI0 alogchI0::inst_;

}}
