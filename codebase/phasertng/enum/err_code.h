#ifndef __phasertng_err_code_enum__
#define __phasertng_err_code_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace err {

// these error codes are offset with 64 as in
// /usr/include/sysexists.h
MakeEnum ( code,
            (SUCCESS)    //0 { EXIT_CODE_OFFSET = 63 }
            (PARSE)      //64
            (SYNTAX)     //65
            (INPUT)      //66
            (FILEOPEN)   //67
            (FILESET)    //68
            (MEMORY)     //69
            (KILLFLAG)   //70
            (KILLFILE)   //71
            (KILLTIME)   //72
            (FATAL)      //73
            (RESULT)     //74
            (UNHANDLED)  //75
            (UNKNOWN)    //76
            (STDIN)      //77
            (DEVELOPER)  //78
            (PYTHON)     //79
            (ASSERT)     //80
         );

}}
#endif
