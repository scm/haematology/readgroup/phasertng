from iotbx.file_reader import any_file
from phasertng.scripts.jiffy import *
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.phil.mode_generator import isNone
import argparse
import iotbx,mmtbx
import iotbx.pdb
import mmtbx.validation.sequence
import os, os.path, sys
import time
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))


#these are internal parameters not interesting for the user
class runWALK:

  def walk_directory_for_files(self,
          level, # change this to walk down subdirectories to find files
          basetng,
          filetype,
          filesuffix, # list
          io_directory,
          ):
      where = enum_out_stream.logfile
      basetng.logEllipsisOpen(where,"Walk directory for files")
      basetng.logTab(where,str("Find " + str(filetype).upper() + " Files"))
      if io_directory is None:
        raise ValueError(enum_err_code.fileset,"directory")
      if not os.path.exists(io_directory):
        raise ValueError(enum_err_code.fileopen,"directory " + str(io_directory))
      basetng.logTab(where,"Query Directory: " + str(io_directory))
      found_files = []
      some_dir = io_directory.rstrip(os.path.sep)
      num_sep = some_dir.count(os.path.sep) #base number of separators
      for root, subdir,files in os.walk(some_dir):
          num_sep_this = root.count(os.path.sep)
          if (level is not None) and (num_sep_this > (num_sep + level)):
             continue
          for ifile in files:
            for suffix in list(filesuffix):
              if ifile.endswith(str(suffix)): # necessary to stop the opening of massive irrelevant files
                  testf = os.path.join(root, ifile)
                  testf = os.path.abspath(testf) #new
                  found_files.append(testf)
      if len(found_files) == 1:
        basetng.logTab(where,"Directory has 1 file")
      else:
        basetng.logTab(where,"Directory has " + str(len(found_files)) + " files")
      basetng.logEllipsisShut(where)
      basetng.logBlank(where)
      basetng.logChevron(where,"Accepted files in directory: " + io_directory)
      for testf in found_files:
        basetng.logTab(where,"Found File: "+testf)
      basetng.logBlank(where)
   #  if len(found_files) == 0:
   #    message = "No Files"
   #    raise ValueError(enum_err_code.input,str(message))
      return found_files

  def run_from_basetng_and_input(self,basetng):
    self.inner_walk(basetng = basetng)

  def inner_walk(self, basetng):
 #{{{
    DAGDB = basetng.DAGDATABASE
    DAGDB.initialize_for_twilight_zone(str(basetng.hashing()),str("walk"))
    io_directory = isNone(basetng.input.get_as_str(".phasertng.file_discovery.directory."))
    io_filenames = []
    n = basetng.input.size(".phasertng.file_discovery.filename_in_directory.")
    for i in range(n):
      ifile  = (isNone(basetng.input.get_as_str_i(".phasertng.file_discovery.filename_in_directory.",i)))
    # ifile = os.path.join(io_directory,ifile)
      io_filenames.append(ifile)
    if io_directory is None:
        basetng.logTab(enum_out_stream.summary,"No directory")
        return
    discover_reflections = True
    discover_coordinates = True
    discover_sequences = True

    def find_found(filetype,filesuffix,basetng,io_directory,io_filenames):
        fullpath = self.walk_directory_for_files(
                 level = 0,
                 basetng=basetng,
                 filetype = filetype,
                 filesuffix = filesuffix,
                 io_directory=io_directory)
        if len(io_filenames) == 0:
          return fullpath
        found = []
        for ifile in io_filenames:
          if ifile.endswith(tuple(filesuffix)): #endswith accepts a tuple!
            for jfile in fullpath:
              if ifile == str(os.path.basename(jfile)):
                found.append(jfile)
        return found #fullpath

    if discover_reflections:
   #{
      basetng.logUnderLine(enum_out_stream.summary,"Reflection File Discovery")
      validated = []
      found = find_found("hkl",['.mtz'],basetng,io_directory,io_filenames)
      for ifile in found:
     #{
        f = os.path.join(io_directory,ifile) #new
        f = os.path.abspath(f) #new
        try:
          mtzobj = iotbx.mtz.object(file_name=f)
        except Exception as e:
          basetng.logTab(enum_out_stream.logfile,"Fails: " + str(ifile))
          basetng.logTab(enum_out_stream.logfile,"Error: " + str(e))
          continue
        basetng.logTab(enum_out_stream.logfile,"Validated: " + str(ifile))
        validated.append(ifile)
     #}
      basetng.logBlank(enum_out_stream.logfile)
      if len(validated) > 0:
        basetng.input.resize(".phasertng.file_discovery.validated.mtz_files.",len(validated))
        for i,mtzin in enumerate(validated):
          basetng.logTab(enum_out_stream.summary,"Reflection File: " + str(mtzin))
          basetng.input.set_as_str_i(".phasertng.file_discovery.validated.mtz_files.",str(mtzin),i)
      if len(validated) > 1:
        basetng.logAdvisory("There is more than one valid mtz file in the directory")
      if len(validated) == 0:
        basetng.logAdvisory("No validated reflection files")
      basetng.logBlank(enum_out_stream.summary)
   #}

    if discover_sequences:
   #{
      basetng.logUnderLine(enum_out_stream.summary,"Sequence File Discovery")
      validated = []
      found = find_found("Seq",['.seq','.fa','.fasta'],basetng,io_directory,io_filenames)
      for ifile in found:
     #{
        f = os.path.join(io_directory,ifile) #new
        f = os.path.abspath(f) #new
        try:
          newlines = []
          with open(f, 'r') as ff:
              lines = ff.readlines()
              for line in lines:
                line = line.strip()
                if len(line) == 1 and line[0] == ">":
                  newlines.append(line + "sequence") #must have something after the >
                if len(line) > 1 and line[0] == ">":
                  newlines.append(line)
                if len(line) > 0 and line[0] != ">":
                  while len(line) > 60:
                    newlines.append(line[:60])  # Append the first 60 characters to newlines
                    line = line[60:]  # Remove the processed part
                  newlines.append(line)  # Append the remaining part of the line
          lines = newlines
          if not len(lines) > 0:
            raise ValueError("The file is empty, invalid FASTA format.")
          from iotbx.bioinformatics import sequence
          first_line = lines[0].strip()
          if first_line.startswith('>'):
            real_fasta = str('\n'.join(lines))
            fasta_sequences = iotbx.bioinformatics.fasta_sequence_parse.parse(real_fasta)
          else:
            fake_fasta = str('>sequence\n' + ''.join(lines))
            fasta_sequences = iotbx.bioinformatics.fasta_sequence_parse.parse(fake_fasta)
          if not fasta_sequences:
            raise ValueError("The file does not contain any valid sequences.")
          for j,seqlist in enumerate(fasta_sequences):
            if len(seqlist): #first is bioinfomatics.sequence second is []
              for i,seq in enumerate(seqlist): # ie seqlist[0]
                if not seq.sequence:
                 raise ValueError(f"Invalid sequence entry found: {seq}")
        except Exception as e:
          basetng.logTab(enum_out_stream.logfile,"Fails: " + str(ifile))
          basetng.logTab(enum_out_stream.logfile,"Error: " + str(e))
          continue
        basetng.logTab(enum_out_stream.logfile,"Validated: " + str(ifile))
        validated.append(ifile)
     #}
      basetng.logBlank(enum_out_stream.logfile)
      sorted(validated, key=lambda x: not x.endswith(".seq"))
      if len(validated) > 0:
        basetng.input.resize(".phasertng.file_discovery.validated.seq_files.",len(validated))
        for i,seqin in enumerate(validated):
          basetng.logTab(enum_out_stream.summary,"Sequence File: " + str(seqin))
          basetng.input.set_as_str_i(".phasertng.file_discovery.validated.seq_files.",str(seqin),i)
      if len(validated) > 1:
        basetng.logAdvisory("There is more than one valid sequence file in the directory")
      if len(validated) == 0:
        basetng.logAdvisory("No validated sequence files")
      basetng.logBlank(enum_out_stream.summary)
   #}

    if discover_coordinates:
   #{
      basetng.logUnderLine(enum_out_stream.summary,"Coordinate File Discovery")
      #choice of multiple pdb files is through file_discovery.filenames_in_directory
      validated = []
      found = find_found("pdb",['.pdb','.ent'],basetng,io_directory,io_filenames)
      for ifile in found:
     #{
        f = os.path.join(io_directory,ifile) #new
        f = os.path.abspath(f) #new
        try:
          pdbobj = iotbx.pdb.input(f)
          pdb_hierarchy = pdbobj.construct_hierarchy()
        except Exception as e:
          basetng.logTab(enum_out_stream.logfile,"Fails: " + str(ifile))
          basetng.logTab(enum_out_stream.logfile,"Error: " + str(e))
          continue
        if len(pdb_hierarchy.models()) == 0:
          basetng.logTab(enum_out_stream.logfile,"Fails: " + str(ifile))
          basetng.logTab(enum_out_stream.logfile,"Error:  No models in file")
        else:
          validated.append(ifile)
          basetng.logTab(enum_out_stream.logfile,"Validated: " + str(ifile))
     #}
      basetng.logBlank(enum_out_stream.logfile)
      #sort so that pdb is prefered ending
      sorted(validated, key=lambda x: not x.endswith(".pdb"))
      if len(validated) > 0:
        basetng.input.resize(".phasertng.file_discovery.validated.pdb_files.",len(validated))
        for i,pdbin in enumerate(validated):
          basetng.logTab(enum_out_stream.summary,"Coordinate File: " + str(pdbin))
          basetng.input.set_as_str_i(".phasertng.file_discovery.validated.pdb_files.",str(pdbin),i)
      if len(validated) == 0:
        basetng.logAdvisory("No validated coordinate files")
      basetng.logBlank(enum_out_stream.summary)
   #}
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  cards = ""
  if args.cards is not None:
    cards = args.cards.read()
  if cards is None:
    cards = cards + "\nphasertng suite directory = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng = runWALK().inner_walk(basetng=basetng)
  print(basetng.summary())
