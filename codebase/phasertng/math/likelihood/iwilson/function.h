//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_iwilson_function_class__
#define __phasertng_likelihood_iwilson_function_class__
#include <phasertng/math/likelihood/iwilson/tgh_flags.h>

namespace phasertng { namespace likelihood { namespace iwilson {

class function
{
  private: //members
    double V = 0;

  public:
    double LL = 0;
    double dLL_by_dV = 0;
    double d2LL_by_dV2 = 0;

  public:  //constructor
    function() {}
    bool negative_variance() { return V <= 0; };
    double variance() { return V; };

  public:
    void
    target_gradient_hessian(
      const double& EO,
      const double& teps,
      const double& V,
      const bool&   CENT,
      const likelihood::iwilson::tgh_flags&
    );
};

}}} //phasertng
#endif
