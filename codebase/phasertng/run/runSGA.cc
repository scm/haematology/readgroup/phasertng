//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <phasertng/math/subgroups.h>

namespace phasertng {

  //Space_group_analysis
  void Phasertng::runSGA()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::DATA))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (data)");
    logTabArray(out::VERBOSE,REFLECTIONS->logWilson(""));

    SpaceGroup iSG = REFLECTIONS->SG;
    UnitCell iUC = REFLECTIONS->UC;
    UnitCell sUC = REFLECTIONS->UC;
    char yes = '*';
    char nay = '=';

    input.value__string.at(".phasertng.subgroup.original.hall.").set_value(iSG.HALL);
    input.value__string.at(".phasertng.subgroup.original.spacegroup.").set_value(iSG.CCP4);
    input.value__string.at(".phasertng.subgroup.original.hermann_mauguin_symbol.").set_value(iSG.type().universal_hermann_mauguin_symbol());

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Reference setting");
    cctbx::sgtbx::change_of_basis_op cb_op_dat = iSG.cb_op();
    UnitCell datuc(iUC.cctbxUC.change_basis(cb_op_dat).parameters());
    auto newsg = iSG.group().change_basis(cb_op_dat);
    SpaceGroup datsg("Hall: " + newsg.match_tabulated_settings().hall());
    logTab(where,"Reference setting for input space group: " + cb_op_dat.as_xyz());
    logTab(where,snprintftos(
         "Reflections Setting: %-13s %s  %-26s",
          iSG.CCP4.c_str(),
          iUC.str().c_str(),
          iSG.HALL.c_str(),""
        ));
    logTab(where,snprintftos(
         "Reference Setting:   %-13s %s  %-26s",
          datsg.CCP4.c_str(),
          datuc.str().c_str(),
          datsg.HALL.c_str()
        ));
 //   iSG = datsg;
    sUC = datuc;
    }}

    sv_double WilsonLL;
    std::vector<cctbx::sgtbx::space_group_type> sysabs;
    std::string hm_symbols("");//for no HM output
    //std::string hall_symbols("");//for no HM output
    std::string hall_symbols("Hall Symbols with Reindexing");//for no hall output
    std::string uch = UnitCell().str("Unit Cell for Standard Setting");
    //std::string hm_symbols("Hermann-Mauguin Symbol"); //for HM output
    {{
    //the Wilson sysabs analysis is critical to the space group expansion
    //because the Wilson sysabs analysis is the part that really decides the sysabs
    //not what the user input as the space group/mtz file
    out::stream where = out::LOGFILE;
    logUnderLine(where,"(a) Space groups derived by translation (screw) symmetry");
    logTab(where,snprintftos(
        " #) %-3s %-13s %7s %8s  %-26s %s",
        "Z","Space Group","#sysabs","WilsonLL",hall_symbols.c_str(),hm_symbols.c_str()));
    logTab(where,"----");
    int tt(1);
    sysabs = groups_sysabs(iSG.type());
  //  sysabs.erase(std::remove(sysabs.begin(),sysabs.end(),iSG),sysabs.end());
    double baseline_WilsonLL(0);
    WilsonLL.resize(sysabs.size());
    for (int t = 0; t < sysabs.size(); t++)
    {
      SpaceGroup sg("Hall: " + sysabs[t].hall_symbol());
      int nsysabs(0);
      cctbx::sgtbx::space_group sg_group = sg.group();
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
        if (sg_group.is_sys_absent(REFLECTIONS->get_miller(r)) and
            REFLECTIONS->get_present(friedel::NAT,r)) //must also be present
          nsysabs++;
      WilsonLL[t] = REFLECTIONS->WilsonLL(sg_group);
      if (t == 0) baseline_WilsonLL = WilsonLL[t];
      WilsonLL[t] -= baseline_WilsonLL;
      logTab(where,snprintftos(
          "%2d) %-3s %-13s %-5d %8.1f  %-26s %s",
          tt,
          "",
          sg.CCP4.c_str(),
          nsysabs,
          WilsonLL[t],
          hall_symbols.size() ? sg.HALL.c_str() : "",
          hm_symbols.size() ? sg.UHMS.c_str() : ""
          ));
      tt++;
    }
    logTab(where,"----");
    logBlank(where);

    std::map<char,int> best_sysabs;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Axial Reflections: ");
    logTab(where,"All crystallographic screw operations are analysed regardless of space group");
    logTab(where,"Score is gain(+)/loss(-) in WLL for expecting systematic absence of intensity");
    for (auto axis : { 'H','K','L' })
    {
      best_sysabs[axis] = 0;
      std::map<int,std::map<bool,double> > sysabs_wll;
      std::map<int,std::pair<double,double> > sysabs_isigi;
      std::tie(sysabs_wll,sysabs_isigi) = REFLECTIONS->WilsonSysAbs(axis);
      logTab(where,"Index: " + std::string(1,axis));
      if (sysabs_wll.size())
      {
        std::string line = snprintftos(
            "    %c %8s %8s",
            axis,"Z","Z/SIGZ");
        line += ("    (WLL)");
        line += ("     ");
        for (auto t : {2,3,4,6} )
        {
          line += snprintftos("  %4d(1)",t);
        }
        logTab(where,line);
        int maxaxis = sysabs_wll.rbegin()->first;
        std::map<int,double> totals;
        for (auto t : {1,2,3,4,6} )
          totals[t] = 0;
        for (int i = 1; i <= maxaxis; i++)
        {
          //millnx miller = { axis == 'H' ? i:0, axis == 'K' ? i:0, axis == 'L' ? i:0};
          std::string line = snprintftos(" %4d",i);
          if (sysabs_wll.count(i))
          {
            line += snprintftos(" %8.5f",sysabs_isigi[i].first);
            line += snprintftos(" %8.2f",sysabs_isigi[i].second);
          }
          else
          {
            line += snprintftos(" %8s"," --?--");
            line += snprintftos(" %8s"," --?--");
          }
          for (auto t : {1,2,3,4,6} )
          {
            if (sysabs_wll.count(i) and t == 1)
            {
              //programming trick, after the print of the first, subtract from the others
              line += snprintftos(" %+8.1f",sysabs_wll[i][false]);
              totals[t] += sysabs_wll[i][false];
              line += snprintftos(" (+) ");
            }
            else if (sysabs_wll.count(i) and (i%t))
            {
              double diff = sysabs_wll[i][true]-sysabs_wll[i][false];
              line += snprintftos(" %+8.1f",diff);
              totals[t] += diff;
            }
            else if (sysabs_wll.count(i))
            {
              //snprintftos(" %+8.1f ",sysabs_wll[i][false]-sysabs_wll[i][false]);
              line += snprintftos(" %8s"," --=--");
            }
            else if (t == 1)
            {
              line += snprintftos(" %8s"," -----");
              line += snprintftos("     ");
            }
            else
            {
              line += snprintftos(" %8s"," -----");
            }
          }
          logTab(where,line);
        }
        logTab(where,snprintftos(
            " %22s","====="));
        line = snprintftos(" %22s","total:");
        for (auto t : {1,2,3,4,6} )
        {
          line += snprintftos(" %+8.1f",totals[t]);
          if (t == 1)
            line += snprintftos(" (+) ");
        }
        logTab(where,line);
        //lambda for max element
        auto cmp = [](const std::pair<int,double>& a, const std::pair<int,double>& b)
        { return a.second < b.second; };
        auto best = std::max_element(totals.begin(),totals.end(),cmp);
        best_sysabs[axis] = best->first;
      }
      else
        logTab(where,"None");
      logTab(where,"----");
      logBlank(where);
    }
    }}

    logChevron(out::SUMMARY,"Axial reflections");
    for (auto item : best_sysabs)
    {
      if (item.second == 0)
        logTab(out::SUMMARY,std::string(1,item.first) + ": no data");
      else if (item.second == 1)
        logTab(out::SUMMARY,std::string(1,item.first) + ": best is no screw");
      else
        logTab(out::SUMMARY,std::string(1,item.first) + ": best is screw sub(" + itos(item.second) + ")");
    }
    logBlank(out::SUMMARY);
    }}

    bool use_all_sysabs = input.value__boolean.at(".phasertng.subgroup.use_all_sysabs.").value_or_default();
    bool use_no_sysabs = input.value__boolean.at(".phasertng.subgroup.use_no_sysabs.").value_or_default();
    double maxWilsonLL = std::numeric_limits<double>::lowest();
    for (int t = 0; t < WilsonLL.size(); t++)
       maxWilsonLL = std::max(maxWilsonLL,WilsonLL[t]);
    std::vector<cctbx::sgtbx::space_group_type> selected_groups_sysabs;
    for (int t = 0; t < WilsonLL.size(); t++)
       if (WilsonLL[t] > (maxWilsonLL - DEF_PPT))
         selected_groups_sysabs.insert(selected_groups_sysabs.begin(),sysabs[t]);
    //if use_all_sysabs selected, then all the sysabs are used not just the best
    //overwrite selected with the full vector
    if (use_all_sysabs)
      selected_groups_sysabs = sysabs;
    if (use_no_sysabs) //only use original
      selected_groups_sysabs = { cctbx::sgtbx::space_group_type(iSG.type()) } ;
    logTab(out::LOGFILE,"Number of selected space groups for expansion: " + itos(selected_groups_sysabs.size()));

   //print the subgroups by systematic absence group
   //many will be duplicated
   //all_sg is the unique list
    {{
    bool header(true);
    out::stream where = out::LOGFILE;
    for (int t = 0; t < selected_groups_sysabs.size(); t++)
    {
      SpaceGroup sg("Hall: " + selected_groups_sysabs[t].hall_symbol());
      std::set<std::string> unique;
      std::vector<cctbx::sgtbx::space_group_type> sglist = subgroups(sg.type(),false,unique);
      if (sglist.size())
      {
        if (header)
        {
          logUnderLine(where,"(b) Subgroups of space group for perfect twinning expansions");
          logTab(where,"Systematic absences may really represent tncs modulations");
          logTab(where,"Analysis includes dropping translational symmetry along each axis");
          logTab(where,"With respect to Reflection Setting");
          logTab(where,"ir: Unit Cell for standard setting (input/reference)");
          logBlank(where);
          header = false;
        }
        logUnderLine(where,itos(t+1) + ") Space Group: " + sg.str());
        logTab(where,snprintftos(
            "%-3s %-13s %c%c %s  %-26s %s",
            "Z","Space Group",'i','r',uch.c_str(),hall_symbols.c_str(),hm_symbols.c_str()));
        logTab(where,"----");
        int last_order_z(0);
        for (int s = 0; s < sglist.size(); s++)
        {
          SpaceGroup ssglist("Hall: " + sglist[s].hall_symbol());
          if (s && last_order_z != ssglist.group().order_z())
            logTab(where,"---");
          cctbx::sgtbx::change_of_basis_op cb_op_new = sglist[s].cb_op();
          //cctbx::sgtbx::change_of_basis_op cb_op_dat = iSG.cb_op();
          //cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
          UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_new).parameters());
          bool is_input = newuc.cctbxUC.is_similar_to(iUC.cctbxUC,0.001,0.001);
          bool is_refer = newuc.cctbxUC.is_similar_to(sUC.cctbxUC,0.001,0.001);
          auto newgrp = sglist[s].group().change_basis(cb_op_new);
          /// this is correct!!
          SpaceGroup newsg("Hall: " + newgrp.match_tabulated_settings().hall());
          logTab(where,snprintftos(
             "%-3s %-13s %c%c %s  %-26s %s",
             (last_order_z != sglist[s].group().order_z()) ? itos(sglist[s].group().order_z()).c_str() : "",
              newsg.CCP4.c_str(),
              char(is_input?nay:yes),
              char(is_refer?nay:yes),
              newuc.str().c_str(),
              hall_symbols.size() ? ssglist.HALL.c_str() : "",
              hm_symbols.size() ? ssglist.UHMS.c_str() : ""
             ));
          last_order_z = sglist[s].group().order_z();
          if (s == sglist.size()-1)
            logTab(where,"----");
        }
      }
      logBlank(where);
    }
    }}

    std::set<phasertng::sorted_space_group_type> all_sg;
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Subgroups");
    std::set<std::string> unique;
    for (int t = 0; t < selected_groups_sysabs.size(); t++)
    {
      auto v = subgroups(selected_groups_sysabs[t],false,unique);
      for (auto vv : v)
      {
        all_sg.insert(vv);
      }
    }
    logTab(where,"Number of unique subgroups = " + itos(all_sg.size()));
    logTab(where,"ir: Unit Cell for standard setting (input/reference)");
    logTab(where,"----");
    logTab(where,snprintftos(
        "%-3s %-13s %c%c %s  %-26s %s",
        "Z","Space Group",'i','r',uch.c_str(),hall_symbols.c_str(),hm_symbols.c_str()));
    logTab(where,"----");
    int last_order_z(0);
    //change the hardwired number below to make a different default selection
    for (auto item : all_sg)
    {
      SpaceGroup ssglist("Hall: " + item.hall_symbol());
      int this_order_z = item.group().order_z();
      if (last_order_z && last_order_z != this_order_z)
        logTab(where,"---");
      cctbx::sgtbx::change_of_basis_op cb_op_new = ssglist.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat = iSG.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
      //UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_dat2new).parameters());
      UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_new).parameters());
      bool is_input = newuc.cctbxUC.is_similar_to(iUC.cctbxUC,0.001,0.001);
      bool is_refer = newuc.cctbxUC.is_similar_to(sUC.cctbxUC,0.001,0.001);
      auto newgrp = ssglist.group().change_basis(cb_op_new);
      /// this is correct!!
      SpaceGroup newsg("Hall: " + newgrp.match_tabulated_settings().hall());
      logTab(where,snprintftos(
         "%-3s %-13s %c%c %s  %-26s %s",
         (last_order_z != this_order_z) ? itos(this_order_z).c_str() : "",
          newsg.CCP4.c_str(),
          char(is_input?nay:yes),
          char(is_refer?nay:yes),
          newuc.str().c_str(),
          hall_symbols.size() ? ssglist.HALL.c_str() : "",
          hm_symbols.size() ? ssglist.UHMS.c_str() : ""
        ));
      last_order_z = this_order_z;
    }
    logTab(where,"---");
    }}

    std::set<phasertng::reverse_sorted_space_group_type> reverse_sort_all_sg;
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Search Subgroups");
    logTab(where,"Remove groups the same except for translational symmetry");
    //the last space group is the original
    //keep the sysabs for the original
    //std::vector<cctbx::sgtbx::space_group_type> selected_groups_sysabs;
    for (int t = 0; t < selected_groups_sysabs.size(); t++)
    {
      std::set<std::string> unique;
      auto v = subgroups(selected_groups_sysabs[t],false,unique);
      for (auto vv : v)
      {
        auto sgtype = cctbx::sgtbx::space_group(vv.hall_symbol());
        auto new_space_group = cctbx::sgtbx::space_group();
        for (auto rtmx : sgtype.all_ops())
        {
          auto newt = cctbx::sgtbx::tr_vec(0,0,0, 12);
          new_space_group.expand_smx(cctbx::sgtbx::rt_mx(rtmx.r(), newt));
        }
        new_space_group.expand_conventional_centring_type(sgtype.conventional_centring_type_symbol());
        reverse_sort_all_sg.insert(new_space_group.type());
      }
    }
    logTab(where,"Number of expansion subgroups = " + itos(reverse_sort_all_sg.size()));
    logTab(where,"----");
    logTab(where,snprintftos(
        "%-3s %-13s %c%c %s  %-26s %s",
        "Z","Space Group",'i','r',uch.c_str(),hall_symbols.c_str(),hm_symbols.c_str()));
    logTab(where,"----");
    int last_order_z(0);
    //change the hardwired number below to make a different default selection
    for (auto item : reverse_sort_all_sg)
    {
      SpaceGroup ssglist("Hall: " + item.hall_symbol());
      int this_order_z = item.group().order_z();
      if (last_order_z && last_order_z != this_order_z)
        logTab(where,"---");
      cctbx::sgtbx::change_of_basis_op cb_op_new = ssglist.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat = iSG.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
      //UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_dat2new).parameters());
      UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_new).parameters());
      bool is_input = newuc.cctbxUC.is_similar_to(iUC.cctbxUC,0.001,0.001);
      bool is_refer = newuc.cctbxUC.is_similar_to(sUC.cctbxUC,0.001,0.001);
      auto newgrp = ssglist.group().change_basis(cb_op_new);
      /// this is correct!!
      SpaceGroup newsg("Hall: " + newgrp.match_tabulated_settings().hall());
      logTab(where,snprintftos(
         "%-3s %-13s %c%c %s  %-26s %s",
         (last_order_z != this_order_z) ? itos(this_order_z).c_str() : "",
          newsg.CCP4.c_str(),
          char(is_input?nay:yes),
          char(is_refer?nay:yes),
          newuc.str().c_str(),
          hall_symbols.size() ? ssglist.HALL.c_str() : "",
          hm_symbols.size() ? ssglist.UHMS.c_str() : ""
        ));
      last_order_z = this_order_z;
    }
    logTab(where,"---");
    }}

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Default Expansion Space Group");
    SpaceGroup newsg = iSG; //default don't change
    UnitCell newuc = iUC;
    //output in reverse order so that the original space group is at the top of the list
    //and then the number of operators is slowly dropped to P1
    //this is the order we want for auto_search
    bool first(true);
    for (auto item : reverse_sort_all_sg)
    {
      if (first)
      { //special case, make the first subgroup the same sysabs as the original, not the sysabs removed one eg p64 2 2
        input.push_back(".phasertng.subgroup.expansion.hall.",newsg.HALL);
        input.push_back(".phasertng.subgroup.expansion.spacegroup.",newsg.CCP4);
        input.push_back(".phasertng.subgroup.expansion.hermann_mauguin_symbol.",newsg.type().universal_hermann_mauguin_symbol());
        first = false;
        continue;
      }
      SpaceGroup ssglist("Hall: " + item.hall_symbol());  //eg P 3 2"  not recognised without Hall: in SpaceGroup
      cctbx::sgtbx::change_of_basis_op cb_op_new = ssglist.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat = iSG.cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
      UnitCell newuc(iUC.cctbxUC.change_basis(cb_op_new).parameters());
      bool is_identity_op = newuc.cctbxUC.is_similar_to(iUC.cctbxUC,0.001,0.001);
      input.push_back(".phasertng.subgroup.expansion.hall.",ssglist.HALL);
      input.push_back(".phasertng.subgroup.expansion.spacegroup.",ssglist.CCP4);
      input.push_back(".phasertng.subgroup.expansion.hermann_mauguin_symbol.",ssglist.type().universal_hermann_mauguin_symbol());
    }
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
    phaser_assert(DAGDB.size());
  }

} //phasertng
