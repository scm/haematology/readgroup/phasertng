//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Gyre_class__
#define __phasertng_dag_Gyre_class__
#include <phasertng/main/Identifier.h>
#include <phasertng/dag/xyzRotMatDeg.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Gyre : public xyzRotMatDeg
  { //rotation only, with relative translation
    //this is for use in conjunction with turn
    //when the translation is determined it is put in curl, because all the same pt,pr for entries
    //AJM TODO write code to split
    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      dvect3      ORTHR = {0,0,0}; //wrt mt
      dvect3      ORTHT = {0,0,0}; //wrt mt
      dmat33      SHIFT_MATRIX = dmat33(); //wrt original

    public: //internal variable only, not parsed/unparsed
      double      FS = 0;
      double      DRMS_SHIFT = 0;
      dvect3      SHIFT = {0,0,0}; //shift to apply to put at origin for tfz

    //return the final rotation and fractional translation, given the unit cell info
    dmat33
    finalR(dvect3 EULER)
    {
      dmat33 ROT = scitbx::math::euler_angles::zyz_matrix(EULER[0],EULER[1],EULER[2]);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = perturbed(ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      return ROTperturb;
    }

    //return the size of the translation perturbation
    double
    delta_shift() const
    { return std::sqrt(fn::pow2(ORTHT[0]) + fn::pow2(ORTHT[1]) + fn::pow2(ORTHT[2])); }

    double
    delta_angle() const
    {
      dmat33 Rmat = perturbed(ORTHR);
      double ang;
      std::tie(std::ignore,ang) = axis_and_angle_deg(Rmat,0);
      return std::fabs(ang);
    }

  };

}} //phasertng
#endif
