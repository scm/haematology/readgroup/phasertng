//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_ATOM_CLASS__
#define __PHASER_DATA_ATOM_CLASS__
#include <phasertng/pod/scatterer.h>
//#include <phasertng/main/jiffy.h>
//#include <cctbx/uctbx.h>

namespace phaser {

class data_atom
{
  public:
    data_atom() { set_input_defaults(); set_extra_defaults(); }

  phasertng::pod::xray::scatterer SCAT;
  phasertng::pod::xtra::scatterer XTRA;
  bool   ORIG; //orig is flagged for an input atom
  bool   generic_bool;

  void copy_input(data_atom& init)
  {
    SCAT = init.SCAT;
    XTRA = init.XTRA;
    ORIG = init.ORIG;
  }

  void set_input_defaults()
  { XTRA.fix_site = XTRA.fix_occ = XTRA.fix_bfac = ORIG = false; }

  void set_extra_defaults()
  { XTRA.rejected = XTRA.restored = false; XTRA.n_adp = XTRA.n_xyz = 0; }

};

} //phaser
#endif
