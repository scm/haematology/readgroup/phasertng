//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/rotation/rotdist.h>
#include <phasertng/math/likelihood/EELLG_analytical.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Relative_expected_llg_for_model
  void Phasertng::runRELLG()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with pose mode");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");

    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    //dag nodes are already loaded in Phasertng.cc
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(), store_halfR, store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    double hires_all_data(0);
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    double io_rmsd = input.value__flt_number.at(".phasertng.expected.llg.rmsd.").value_or_default();

    sv_double reLLG(DAGDB.size());
    //assume all POSE sizes are the same
    math::Matrix<double> analytical(DAGDB.size(),used_modlid.size());
    {{
    likelihood::EELLG_analytical EELLG_analytical;
    DAGDB.restart(); //very important to call this to set WORK
    int n(0),N(DAGDB.size());
    while (!DAGDB.at_end())
    {
      out::stream where = out::VERBOSE;
      std::string nN = nNtos(n+1,N);
      logUnderLine(where,"Calculation" +nN);
      logTabArray(where,DAGDB.WORK->logPose(""));
      auto ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS);
      int NBINS = REFLECTIONS->numbins();
      auto bin_reso = REFLECTIONS->bin_midres(0);
      auto bin_nref = REFLECTIONS->numinbin();
      sv_int ninbin(NBINS,0);
      sv_double sume2(NBINS,0),sumec2(NBINS,0),sumcosterm(NBINS,0),sumssqr(NBINS,0);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const int    bin  = REFLECTIONS->get_int(labin::BIN,r);
        const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
        const double phif = scitbx::deg_as_rad(REFLECTIONS->get_flt(labin::MAPPH,r));
        if (selected.get_selected(r))
        {
          ninbin[bin]++;
          sumssqr[bin] += ssqr;
          double eobs = feff / resn;
          double e2 = fn::pow2(eobs);
          sume2[bin] += e2;
          double ec2 = std::norm(ecalcs_sigmaa_pose.ecalcs[r]);
          sumec2[bin] += ec2;
          cmplex E = eobs * std::exp(cmplex(0., 1.) * phif);
          double costerm = std::real(E * std::conj(ecalcs_sigmaa_pose.ecalcs[r]));
          sumcosterm[bin] += costerm;
        } //selected
      }
      double refeLLG(0),eLLG(0);

      // Factor for reference sigmaA: complete model with 0.4A rms error
      double argfac = -DEF_TWOPISQ_ON_THREE * fn::pow2(io_rmsd);
      af_string output;
      output.push_back(snprintftos(
        "%3s %6s %8s %10s %10s %10s %9s",
        "Bin","MidRes","#refl","eLLG","refeLLG","reLLG","Map-CC"));
      for (int bin = 0; bin < NBINS; bin++)
      {
        double denom = std::sqrt(sume2[bin] * sumec2[bin]);
        if (denom > 0)
        {
          double mapCC = sumcosterm[bin] / denom;
          double ssqr = sumssqr[bin] / ninbin[bin];
          double sigaref = std::exp(argfac * ssqr);
          //std::cout << "Ninbin, ssqr, mapCC, sigaref: " << ninbin[bin] << " " << ssqr << " " << mapCC << " " << sigaref << std::endl;
          double bin_eLLG = ninbin[bin] * fn::pow4(mapCC) / 2;
          double bin_refeLLG = ninbin[bin] * fn::pow4(sigaref) / 2;
          eLLG += bin_eLLG;
          refeLLG += bin_refeLLG;
          double bin_rellg = bin_eLLG/bin_refeLLG;
          output.push_back(snprintftos(
           "%3d %6.2f %8d %10.1f %10.1f %10.8f %+9.6f",
          bin+1,bin_reso[bin],bin_nref[bin],bin_eLLG,bin_refeLLG,bin_rellg,mapCC));
        }
      }
      reLLG[n] = eLLG / refeLLG;
      logTabArray(where,output);
      logTab(where,"eLLG = " + dtos(eLLG));
      logTab(where,"refeLLG = " + dtos(refeLLG));
      logTab(where,"reLLG = " + dtos(reLLG[n]) + " (fraction)");
      if (reLLG[n] > 1)
        logWarning("Model more accurate than ideally inaccurate model");
      DAGDB.WORK->RELLG = reLLG[n]; //indexed n on node number

      int e(0);
      for (auto modlid : used_modlid)
      {
        //vrms_final is an array indexed on number of models in the ensemble
        auto entry = DAGDB.lookup_entry_tag(modlid,DataBase());
        sv_double vrms_final = pod::vrms_final(
            DAGDB.WORK->DRMS_SHIFT[modlid].VAL,entry->VRMS_START);
        //scattering for the ensemble is not indexed per model
        double scattering = entry->SCATTERING/REFLECTIONS->get_totalscat(); //cast to double
        double smax = 1. / REFLECTIONS->data_hires();
        //analytical indexed on the dag node (because the drms changes per node)
        //and on the modlid index (e) of the entry which has different scattering
        //mostly one node and one modlid, so matrix is normally single value
        analytical(n, e) = EELLG_analytical.get(scattering, vrms_final[e], smax) /
                             EELLG_analytical.get(1., io_rmsd, smax);
        logTab(where,"aeLLG = " + dtos(analytical(n, e)) + " for " + modlid.str());
        for (int p = 0; p < DAGDB.WORK->POSE.size(); p++)
        {
          if (modlid == DAGDB.WORK->POSE[p].IDENTIFIER)
          {
            DAGDB.WORK->POSE[p].AELLG = analytical(n,e);
          }
        }
        e++;
      }
      n++;
    }
    }}

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where, "Relative eLLG");
    logTab(where,"Relative eLLG for this model compared to complete model with RMS errors");
    logTab(where,"Ideally inaccurate RMSD = " + dtos(io_rmsd));
    logTab(where,snprintftos(
        "%3s  %11s %11s %*s",
        "#","reLLG","analytical",uuid_w,"modlid"));
    int n(0);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      int e(0);
      for (auto& modlid : used_modlid)
      {
        if (e == 0)
        {
          logTab(where,snprintftos(
              "%3d  %11.6f %11.6f %*s",
              n+1, DAGDB.WORK->RELLG, analytical(n,e),uuid_w,modlid.string().c_str()));
        }
        else
        {
          logTab(where,snprintftos(
              "%3s  %11s %11.6f %*s",
              "","", analytical(n,e), uuid_w,modlid.string().c_str()));
        }
        e++;
      }
      n++;
    }
    logTableEnd(where);
    }}

    DAGDB.restart();
    auto& pose = DAGDB.WORK->POSE[0];
    input.value__uuid_number.at(".phasertng.expected.llg.id.").set_value(pose.IDENTIFIER.identifier());
    input.value__string.at(".phasertng.expected.llg.tag.").set_value(pose.IDENTIFIER.tag());
    if (DAGDB.WORK->RELLG)
      input.value__flt_number.at(".phasertng.expected.llg.rellg.").set_value(DAGDB.WORK->RELLG);
    //this is a hack, and assumes that there is only one pose/one entry
    if (pose.AELLG)
      input.value__flt_number.at(".phasertng.expected.llg.ellg.").set_value(pose.AELLG);
  }

} //phasertng
