//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_reflection_ReflRowAnom_class__
#define __phasertng_reflection_ReflRowAnom_class__
#include <cctbx/miller.h>
#include <phasertng/data/reflection/ReflRow.h>

namespace phasertng {
namespace reflection {

class ReflRowAnom : public ReflRow
{
  public:
    cctbx::miller::index<int> miller = {0,0,0};
//23
    double inat = 0;
    double siginat = 0;
    double feffnat = 0;
    double dobsnat = 0;
    double anisobeta = 0;
    double resn = 0;
    double ssqr = 0;
    double teps = 0;
    double eps = 0;
    double tbin = 0;
    double wll = 0;
    double phsr = 0;
    double info = 0;
    double einfo = 0;
    double sinfo = 0;
    double sellg = 0;
    double sefom = 0;
    double ipos = 0;
    double sigipos = 0;
    double feffpos = 0;
    double dobspos = 0;
    double ineg = 0;
    double sigineg = 0;
    double feffneg = 0;
    double dobsneg = 0;
//2
    int    bin = 0;
//3
    bool   cent = 0;
    bool   both = 0;
    bool   plus = 0;
//3
    bool   pnat = 0;
    bool   ppos = 0;
    bool   pneg = 0;

  public:
    const cctbx::miller::index<int> get_miller() const { return miller; }
//23
    const double get_inat() const { return inat; }
    const double get_siginat() const { return siginat; }
    const double get_feffnat() const { return feffnat; }
    const double get_dobsnat() const { return dobsnat; }
    const double get_anisobeta() const { return anisobeta; }
    const double get_resn() const { return resn; }
    const double get_ssqr() const { return ssqr; }
    const double get_teps() const { return teps; }
    const double get_eps() const { return eps; }
    const double get_tbin() const { return tbin; }
    const double get_wll() const { return wll; }
    const double get_phsr() const { return phsr; }
    const double get_info() const { return info; }
    const double get_einfo() const { return einfo; }
    const double get_sinfo() const { return sinfo; }
    const double get_sellg() const { return sellg; }
    const double get_sefom() const { return sefom; }
    const double get_ipos() const { return ipos; }
    const double get_sigipos() const { return sigipos; }
    const double get_feffpos() const { return feffpos; }
    const double get_dobspos() const { return dobspos; }
    const double get_ineg() const { return ineg; }
    const double get_sigineg() const { return sigineg; }
    const double get_feffneg() const { return feffneg; }
    const double get_dobsneg() const { return dobsneg; }
//2
    const int    get_bin() const { return bin; }
//3
    const bool   get_cent() const { return cent; }
    const bool   get_both() const { return both; }
    const bool   get_plus() const { return plus; }
//3
    const bool   get_pnat() const { return pnat; }
    const bool   get_ppos() const { return ppos; }
    const bool   get_pneg() const { return pneg; }
};

} //end reflection scope
} //phasertng
#endif
