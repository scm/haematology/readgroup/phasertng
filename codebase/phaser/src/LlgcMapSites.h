//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_LLGC_MAP_SITES_CLASS__
#define __PHASER_LLGC_MAP_SITES_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/af_atom.h>
#include <phaser/include/data_llgc.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <set>

namespace phaser {

class restored_data
{
  public:
    restored_data() { atomtype = ""; zscore = occupancy = 0; }
    restored_data(std::string t,floatType z,floatType o) { atomtype = t; zscore = z; occupancy = o; }
    std::string atomtype;
    floatType zscore,occupancy;
};

class LlgcMapSites
{
  public:
    LlgcMapSites()
    { atoms.clear(); restored.clear(); anisotropic.clear(); }
    af_atom atoms;
    std::set<int> anisotropic;
    std::multimap<int,restored_data> restored;
    void merge(LlgcMapSites& right)
    {
      atoms.insert(atoms.end(),right.atoms.begin(),right.atoms.end());
      restored.insert(right.restored.begin(),right.restored.end());
      anisotropic.insert(right.anisotropic.begin(),right.anisotropic.end());
    }
    int nanisotropic()
    { return anisotropic.size(); }
    int nrestored()
    {
      std::set<int> unique_restored;
      typedef std::multimap<int,restored_data>::iterator I;
      for (I iter = restored.begin(); iter != restored.end(); iter++)
        unique_restored.insert(iter->first);
      return unique_restored.size();
    }
    int natoms()
    { return atoms.size(); }
    bool nchanged()
    { return (natoms() + nrestored() + nanisotropic()) > 0; }
    af_string selectTopSites(SpaceGroup,UnitCell,data_llgc,int);
};

}//phaser

#endif
