//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/io/entry/Map.h>
#include <phasertng/math/slope_and_intercept.h>

namespace phasertng {

  //Read_mtz_file
  void Phasertng::runIMAP()
  {
    if (DAGDB.size() >= 2)
    throw Error(err::INPUT,"Dag must contain none or one nodes");
    if (input.value__flt_number.at(".phasertng.reflections.wavelength.").is_default())
      throw Error(err::INPUT,"Wavelength must be set on input");

    if (input.value__path.at(".phasertng.mapin.filename.").is_default())
      throw Error(err::FILESET,"mapin filename");
    Map readmap;
    readmap.SetFileSystem(input.value__path.at(".phasertng.mapin.filename.").value_or_default());
    if (!readmap.file_exists())
      throw Error(err::FILEOPEN,readmap.fstr());
    if (readmap.is_a_directory())
      throw Error(err::FILEOPEN,readmap.fstr());

    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    if (DAGDB.size() == 0)
    {
      DAGDB.initialize_first_node_from_hash_tag(hashing(),input.running_mode);
      std::string tag = readmap.stem(); //default
      if (!input.value__string.at(".phasertng.mapin.tag.").is_default())
        tag = input.value__string.at(".phasertng.mapin.tag.").value_or_default();
      reflid = DAGDB.initialize_reflection_identifier(tag);
      if (reflid.shortened)
        logTab(where,"Filename too long for tag, tag modified");
    }
    else
    {
      DAGDB.shift_tracker(hashing(),input.running_mode);
      std::string tag = DAGDB.WORK->REFLID.tag();
      reflid = DAGDB.initialize_reflection_identifier(tag);
      if (reflid.shortened)
        logTab(where,"Filename too long for tag, tag modified");
    }
    DAGDB.restart();
    readmap.MODLID = reflid.identify();
    logTab(where,"Reflections identifier: " + reflid.str());
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Map");
    logTab(where,"Map read from file: " + readmap.qfstrq());
    logTab(where,"Reflections identifier: " + reflid.str());
    if (!readmap.is_mtz() and !readmap.is_map())
      throw Error(err::INPUT,"Map format not recognised");
    if (readmap.is_mtz()) logTab(where,"Map format mtz");
    if (readmap.is_map()) logTab(where,"Map format mrc");
    }}

    //if the map extends past the data resolution limit, then load the data slightly
    //past that for interpolation
    //if map resolution is less than data resolution, then will have to limit resolution
    //of map to allow for interpolation
    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Map Configuration");
    logTab(where,"Read the input map/mtz");
    if (readmap.is_mtz())
    {
      std::string mapf  = input.value__mtzcol.at(".phasertng.labin.mapf.").is_default() ?
                          "" : input.value__mtzcol.at(".phasertng.labin.mapf.").name();
      std::string mapph = input.value__mtzcol.at(".phasertng.labin.mapph.").is_default() ?
                          "" : input.value__mtzcol.at(".phasertng.labin.mapph.").name();
      bool no_phases(false),require_box(false);
      auto txt = readmap.ReadMtz(mapf,mapph,no_phases,require_box);
      logTabArray(out::LOGFILE,txt);
    }
    else if (readmap.is_map())
    {
      double oversample_hires = input.value__flt_number.at(".phasertng.mapin.oversample_resolution_limit.").value_or_default();
      logTab(where,"Read the map, but if it is vastly oversampled, resolution will be truncated to " + dtos(oversample_hires));
      bool write_files(Suite::Extra() and WriteFiles());
      auto txt = readmap.ReadMap(oversample_hires,write_files,DataBase(),DAGDB.PATHWAY.ULTIMATE);
      logTabArray(out::LOGFILE,txt);
    }
    else
    {
      throw Error(err::INPUT,"Map format error");
    }
    //the map may be very highly oversampled
    logTab(where,"Map resolution = " + dtos(readmap.hires(),5,2));
    if (!input.value__flt_number.at(".phasertng.reflections.resolution.").is_default())
    {
      logTab(where,"User request to truncate the resolution of the map = true");
      double io_map_resolution = input.value__flt_number.at(".phasertng.reflections.resolution.").value_or_default();
      bool truncate(io_map_resolution > readmap.hires());
      logTab(where,"Map truncation resolution = " + dtos(io_map_resolution,5,2));
      logTab(where,"User resolution > map resolution" + btos(truncate));
      if (truncate)
      {
        logEllipsisOpen(where,"Truncate reflections");
        readmap.truncate_to_resolution(io_map_resolution);
        logEllipsisShut(where);
      }
    }
    logTab(where,"Sort reflections on cctbx default for miller indices: k first (0 1 0)");
    {
      logEllipsisOpen(where,"Sorting");
      readmap.sort_reflections();
      logEllipsisShut(where);
    }
    }}

    //this is a map so no option to read values from input map
    {
      double oversampling = input.value__flt_number.at(".phasertng.reflections.oversampling.").value_or_default();
      if ( !input.value__flt_number.at(".phasertng.reflections.oversampling.").is_default())
      {
        if (oversampling != readmap.HEADER.OVERSAMPLING)
        {
          out::stream where = out::LOGFILE;
          logTab(where,"Mtz Oversampling: " + dtos(readmap.HEADER.OVERSAMPLING));
          logTab(where,"Input Oversampling: " + dtos(oversampling));
          logAdvisory("Oversampling in mtz file and user input are different " + dtos(readmap.HEADER.OVERSAMPLING) + " cf " + dtos(oversampling));
        }
        REFLCOLSMAP.OVERSAMPLING = oversampling;
      }
      //if the oversampling has come from the mtz file, make sure this difference is recorded in phil
      input.value__flt_number.at(".phasertng.reflections.oversampling.").set_value(readmap.HEADER.OVERSAMPLING);
    }
    {
      dvect3 mapori = input.value__flt_vector.at(".phasertng.reflections.map_origin.").value_or_default();
      if (!input.value__flt_vector.at(".phasertng.reflections.map_origin.").is_default())
      {
        if (readmap.HEADER.MAPORI != mapori)
        {
          out::stream where = out::LOGFILE;
          logTab(where,"Mtz origin relative to original map: " + dvtoss(readmap.HEADER.MAPORI,3));
          logTab(where,"Input origin                       : " + dvtoss(mapori,3));
          logWarning("Map origin in mtz file and user input are different");
        }
        readmap.HEADER.MAPORI = mapori;
      }
      //if the mapori has come from the mtz file, make sure this difference is recorded in phil
      input.value__flt_vector.at(".phasertng.reflections.map_origin.").set_value(readmap.HEADER.MAPORI);
    }
    if (input.value__flt_number.at(".phasertng.reflections.oversampling.").is_default())
      logWarning("Oversampling is set to default value of 1");
    if (input.value__flt_vector.at(".phasertng.reflections.map_origin.").is_default())
      logWarning("Map origin is set to default");

    ModelsFcalc fcalcs;
    {{
    //fcalcs.REFLID = REFLCOLSMAP.reflid();
    fcalcs.MODLID = reflid.identify();
    fcalcs.TIMESTAMP = TimeStamp();
    fcalcs.MILLER = readmap.MILLER;
    fcalcs.UC = readmap.UC;
    fcalcs.NMODELS = 1;
    fcalcs.FCALC_HIRES = readmap.hires();
    fcalcs.FCALC.resize(1,readmap.MILLER.size());
    for (int r = 0; r < readmap.MILLER.size(); r++)
    {
      fcalcs.FCALC(0,r) = readmap.FCALC[r];
    }
    fcalcs.setup_bins_with_statistically_weighted_bin_width(
        readmap.UC.GetBox(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_min(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_max(),
        input.value__int_number.at(".phasertng.bins.maps.width.").value_or_default()
        );
    logTabArray(out::VERBOSE,fcalcs.BIN.logBin("fcalc"));
    }}

    logHistogram(out::LOGFILE,readmap.density({1,1},false),20,true,"Map Density");

    REFLCOLSMAP.setup_miller(fcalcs.FCALC.size());
    {
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::FNAT));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::SIGFNAT));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::INAT));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::SIGINAT));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::MAPF));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::MAPPH));
      REFLCOLSMAP.setup_mtzcol(input.labin_mtzcol(labin::BIN));
    }
    REFLCOLSMAP.UC = fcalcs.UC;
    REFLCOLSMAP.SG = SpaceGroup("P 1"); //default spacegroup
    //all other header records default, including totalscat 0
    for (int r = 0; r < fcalcs.FCALC.size(); r++)
    {
      //labin::INAT can be zero and that is OK
      //delete this reflection from the dataset, permanently
      REFLCOLSMAP.set_miller(fcalcs.MILLER[r],r);
      //convert from complex to real here
      const int nmodel(0);
      REFLCOLSMAP.set_flt(labin::FNAT,std::abs(fcalcs.FCALC(nmodel,r)),r);
      REFLCOLSMAP.set_flt(labin::SIGFNAT,0.,r);
      REFLCOLSMAP.set_present(friedel::NAT,true,r);
      double fmap,phase;
      std::tie(fmap,phase) = fcalcs.polar_deg(nmodel,r);
      REFLCOLSMAP.set_flt(labin::MAPF,fmap,r);
      REFLCOLSMAP.set_flt(labin::MAPPH,phase,r);
    }
    REFLCOLSMAP.reverse_not_french_wilson_IfromF(friedel::NAT);
    REFLCOLSMAP.INTENSITIES = false;
    REFLCOLSMAP.MAP = true;
    REFLCOLSMAP.EVALUES = false;
    REFLCOLSMAP.ANOMALOUS = false;
    REFLCOLSMAP.SHARPENING = 0;
    REFLCOLSMAP.setup_resn_anisobeta(
       input.labin_mtzcol(labin::RESN),
       input.labin_mtzcol(labin::ANISOBETA));
    REFLCOLSMAP.REFLID = reflid.identify();
    REFLCOLSMAP.REFLPREP.insert(reflprep::INIT);
    REFLCOLSMAP.WAVELENGTH = input.value__flt_number.at(".phasertng.reflections.wavelength.").value_or_default();
    if (REFLCOLSMAP.WAVELENGTH == 0)
      REFLCOLSMAP.WAVELENGTH = DEF_WAVELENGTH;
    REFLCOLSMAP.OVERSAMPLING = input.value__flt_number.at(".phasertng.reflections.oversampling.").value_or_default();
    REFLCOLSMAP.MAPORI = input.value__flt_vector.at(".phasertng.reflections.map_origin.").value_or_default();
    REFLCOLSMAP.set_data_resolution();
    input.value__flt_paired.at(".phasertng.data.resolution_available.").set_value(
      { REFLCOLSMAP.RESOLUTION[1],REFLCOLSMAP.RESOLUTION[0]});
    input.value__flt_cell.at(".phasertng.data.unitcell.").set_value(
      REFLCOLSMAP.UC.cctbxUC.parameters());

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"History");
    logBlank(where);
    logTabArray(where,REFLCOLSMAP.get_history());
    }}

    dag::Node& node = DAGDB.NODES.front();
    node.CELL = REFLCOLSMAP.UC.cctbxUC;
    node.HALL = REFLCOLSMAP.SG.HALL;
    PHASER_ASSERT(!boost::logic::indeterminate(REFLCOLSMAP.MAP));
    node.SG = nullptr;
    node.REFLID = reflid.identify();

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
