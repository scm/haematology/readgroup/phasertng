//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/percent_paired.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  percent_paired::percent_paired(
      const std::string parameter,
      const std::pair<double,double> dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  { }

  void
  percent_paired::set_value(std::pair<double,double> other) //allow in place contructor {x,y}
  {
   // phaser_assert(other.first >= 0. or other.first <= 100.);
   // phaser_assert(other.second >= 0. or other.second <= 100.);
    user = true;
    uvalue = other;
  }

  void
  percent_paired::set_value(const sv_string& vcards,std::pair<double,double> other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  percent_paired::is_default() const
  { return uvalue == dephalt; }

  void
  percent_paired::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  percent_paired::to_python()
  { return unparse_parameter(false); }

  void
  percent_paired::from_python(std::string str)
  {
    sv_string txt = to_words(str);
    set_value({std::stod(txt[0]),std::stod(txt[1])});
  }

  std::pair<double,double>
  percent_paired::value_or_default() const
  { return user ? uvalue : dephalt; }

  double
  percent_paired::value_or_default_min() const
  {
    std::pair<double,double> tmp = value_or_default();
    return tmp.first < tmp.second ? tmp.first : tmp.second;
  }

  double
  percent_paired::value_or_default_max() const
  {
    std::pair<double,double> tmp = value_or_default();
    return tmp.first < tmp.second ? tmp.second : tmp.first;
  }

  std::string
  percent_paired::unparse_parameter(bool phil) const
  {
    if (value_or_default() == std::pair<double,double>( DEF_FLT_FLAG, DEF_FLT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << uvalue.first << " " << uvalue.second;
    return ss.str();
  }

  std::string
  percent_paired::show_default(bool phil) const
  {
    if (dephalt == std::pair<double,double>( DEF_FLT_FLAG, DEF_FLT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << dephalt.first << " " << dephalt.second;
    return is_default() ? None() : ss.str();
  }

}}
