//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_SCATTERING_CLASS__
#define __PHASER_SCATTERING_CLASS__
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <phaser/main/Phaser.h>

namespace phaser {

bool   isElement(std::string);

class scattering
{
  public:
    scattering()
    {
      fetch["H"] = cctbx::eltbx::xray_scattering::wk1995("H").fetch();
      fetch["C"] = cctbx::eltbx::xray_scattering::wk1995("C").fetch();
      fetch["N"] = cctbx::eltbx::xray_scattering::wk1995("N").fetch();
      fetch["O"] = cctbx::eltbx::xray_scattering::wk1995("O").fetch();
      fetch["S"] = cctbx::eltbx::xray_scattering::wk1995("S").fetch();
      fetch["P"] = cctbx::eltbx::xray_scattering::wk1995("P").fetch();
    }

    std::map<std::string,cctbx::eltbx::xray_scattering::gaussian> fetch;

};

}//end namespace phaser

#endif
