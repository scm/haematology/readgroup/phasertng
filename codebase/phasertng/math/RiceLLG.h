//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RiceLLG_functions__
#define __phasertng_RiceLLG_functions__

// Functions needed to compute Rice approximation to the LLG including
// measurement error on intensities

namespace phasertng {

double effSigaRootAcen(double ee, double eesq, double sigaeff);

double DobsSigaRootAcen_by_dsa(double eesq, double sigaeff);

double d2effSigaRootAcen_by_dsa2(double eesq, double sigaeff);

double effSigaRootCen(double ee, double eesq, double sigaeff);

double DobsSigaRootCen_by_dsa(double eesq, double sigaeff);

double d2effSigaRootCen_by_dsa2(double eesq, double sigaeff);

double effSigaRoot(double ee, double eesq, double sigaeff, bool centric);

double DobsSigaRoot_by_dsa(double eesq, double sigaeff, bool centric);

double d2effSigaRoot_by_dsa2(double eesq, double sigaeff, bool centric);

double getDfactor(double ee, double eesq, bool centric);

double getEffectiveEobs(double eesq, double Dobstor);

}
#endif
