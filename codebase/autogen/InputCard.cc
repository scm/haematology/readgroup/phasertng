// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS
// EDITS WILL BE OVERWRITTEN!
#include <autogen/include/InputCard.h>

namespace phasertng {

  InputCard::InputCard() : InputBase()
  {
    add_keywords();
    add_linkKeys();
    add_phil_input_parameters();
    add_phil_result_parameters();
  }

  void
  InputCard::add_keywords()
  {
    constexpr char phasertng_anisotropy_sharpening_bfactor[] = ".phasertng.anisotropy.sharpening_bfactor.";
    value__flt_number[phasertng_anisotropy_sharpening_bfactor] = type::flt_number(phasertng_anisotropy_sharpening_bfactor,false,0,false,0,0);
    constexpr char phasertng_anisotropy_wilson_scale[] = ".phasertng.anisotropy.wilson_scale.";
    value__flt_number[phasertng_anisotropy_wilson_scale] = type::flt_number(phasertng_anisotropy_wilson_scale,false,0,false,0,0);
    constexpr char phasertng_anisotropy_wilson_bfactor[] = ".phasertng.anisotropy.wilson_bfactor.";
    value__flt_number[phasertng_anisotropy_wilson_bfactor] = type::flt_number(phasertng_anisotropy_wilson_bfactor,false,0,false,0,0);
    constexpr char phasertng_anisotropy_delta_bfactor[] = ".phasertng.anisotropy.delta_bfactor.";
    value__flt_number[phasertng_anisotropy_delta_bfactor] = type::flt_number(phasertng_anisotropy_delta_bfactor,false,0,false,0,0);
    constexpr char phasertng_anisotropy_direction_cosines[] = ".phasertng.anisotropy.direction_cosines.";
    value__flt_matrix[phasertng_anisotropy_direction_cosines] = type::flt_matrix(phasertng_anisotropy_direction_cosines,false,0,false,0,{0,0,0,0,0,0,0,0,0});
    constexpr char phasertng_anisotropy_beta[] = ".phasertng.anisotropy.beta.";
    value__flt_numbers[phasertng_anisotropy_beta] = type::flt_numbers(phasertng_anisotropy_beta,false,0,false,0,{0,0,0,0,0,0});
    constexpr char phasertng_anisotropy_outer_bin_contrast[] = ".phasertng.anisotropy.outer_bin_contrast.";
    value__flt_number[phasertng_anisotropy_outer_bin_contrast] = type::flt_number(phasertng_anisotropy_outer_bin_contrast,false,0,false,0,0);
    constexpr char phasertng_atoms_filename[] = ".phasertng.atoms.filename.";
    value__path[phasertng_atoms_filename] = type::path(phasertng_atoms_filename);
    constexpr char phasertng_atoms_fractional_scatterer[] = ".phasertng.atoms.fractional.scatterer.";
    array__string[phasertng_atoms_fractional_scatterer] = std::vector<type::string>();
    constexpr char phasertng_atoms_fractional_site[] = ".phasertng.atoms.fractional.site.";
    array__flt_vector[phasertng_atoms_fractional_site] = std::vector<type::flt_vector>();
    constexpr char phasertng_atoms_fractional_occupancy[] = ".phasertng.atoms.fractional.occupancy.";
    array__flt_number[phasertng_atoms_fractional_occupancy] = std::vector<type::flt_number>();
    constexpr char phasertng_bfactor_minimum[] = ".phasertng.bfactor.minimum.";
    value__flt_number[phasertng_bfactor_minimum] = type::flt_number(phasertng_bfactor_minimum,true,-1000,true,1000);
    constexpr char phasertng_bfactor_maximum[] = ".phasertng.bfactor.maximum.";
    value__flt_number[phasertng_bfactor_maximum] = type::flt_number(phasertng_bfactor_maximum,true,-1000,true,1000,500);
    constexpr char phasertng_bins_reflections_range[] = ".phasertng.bins.reflections.range.";
    value__int_paired[phasertng_bins_reflections_range] = type::int_paired(phasertng_bins_reflections_range,true,1,false,0,{6,50});
    constexpr char phasertng_bins_reflections_width[] = ".phasertng.bins.reflections.width.";
    value__int_number[phasertng_bins_reflections_width] = type::int_number(phasertng_bins_reflections_width,true,100,false,0,500);
    constexpr char phasertng_bins_molecular_transforms_range[] = ".phasertng.bins.molecular_transforms.range.";
    value__int_paired[phasertng_bins_molecular_transforms_range] = type::int_paired(phasertng_bins_molecular_transforms_range,true,1,false,0,{6,1000});
    constexpr char phasertng_bins_molecular_transforms_width[] = ".phasertng.bins.molecular_transforms.width.";
    value__int_number[phasertng_bins_molecular_transforms_width] = type::int_number(phasertng_bins_molecular_transforms_width,true,100,false,0,1000);
    constexpr char phasertng_bins_maps_range[] = ".phasertng.bins.maps.range.";
    value__int_paired[phasertng_bins_maps_range] = type::int_paired(phasertng_bins_maps_range,true,1,false,0,{6,1000});
    constexpr char phasertng_bins_maps_width[] = ".phasertng.bins.maps.width.";
    value__int_number[phasertng_bins_maps_width] = type::int_number(phasertng_bins_maps_width,true,100,false,0,1000);
    constexpr char phasertng_biological_unit_sequence_filename[] = ".phasertng.biological_unit.sequence.filename.";
    value__path[phasertng_biological_unit_sequence_filename] = type::path(phasertng_biological_unit_sequence_filename);
    constexpr char phasertng_biological_unit_sequence_multiplicity[] = ".phasertng.biological_unit.sequence.multiplicity.";
    value__int_number[phasertng_biological_unit_sequence_multiplicity] = type::int_number(phasertng_biological_unit_sequence_multiplicity,true,1,false,0,1);
    constexpr char phasertng_biological_unit_model_filename[] = ".phasertng.biological_unit.model.filename.";
    value__path[phasertng_biological_unit_model_filename] = type::path(phasertng_biological_unit_model_filename);
    constexpr char phasertng_biological_unit_model_via_sequence[] = ".phasertng.biological_unit.model.via_sequence.";
    value__boolean[phasertng_biological_unit_model_via_sequence] = type::boolean(phasertng_biological_unit_model_via_sequence,true);
    constexpr char phasertng_biological_unit_solvent[] = ".phasertng.biological_unit.solvent.";
    value__percent[phasertng_biological_unit_solvent] = type::percent(phasertng_biological_unit_solvent);
    constexpr char phasertng_biological_unit_selenomethionine[] = ".phasertng.biological_unit.selenomethionine.";
    value__boolean[phasertng_biological_unit_selenomethionine] = type::boolean(phasertng_biological_unit_selenomethionine,false);
    constexpr char phasertng_biological_unit_disulphide[] = ".phasertng.biological_unit.disulphide.";
    value__boolean[phasertng_biological_unit_disulphide] = type::boolean(phasertng_biological_unit_disulphide,false);
    constexpr char phasertng_biological_unit_deuterium[] = ".phasertng.biological_unit.deuterium.";
    value__boolean[phasertng_biological_unit_deuterium] = type::boolean(phasertng_biological_unit_deuterium,false);
    constexpr char phasertng_biological_unit_water_percent[] = ".phasertng.biological_unit.water_percent.";
    value__percent[phasertng_biological_unit_water_percent] = type::percent(phasertng_biological_unit_water_percent,0);
    constexpr char phasertng_biological_unit_hetatm[] = ".phasertng.biological_unit.hetatm.";
    array__strings[phasertng_biological_unit_hetatm] = std::vector<type::strings>();
    constexpr char phasertng_biological_unit_builder_build_from_sequence[] = ".phasertng.biological_unit_builder.build_from_sequence.";
    value__boolean[phasertng_biological_unit_builder_build_from_sequence] = type::boolean(phasertng_biological_unit_builder_build_from_sequence,false);
    constexpr char phasertng_biological_unit_builder_coverage[] = ".phasertng.biological_unit_builder.coverage.";
    value__percent[phasertng_biological_unit_builder_coverage] = type::percent(phasertng_biological_unit_builder_coverage,85);
    constexpr char phasertng_biological_unit_builder_identity[] = ".phasertng.biological_unit_builder.identity.";
    value__percent[phasertng_biological_unit_builder_identity] = type::percent(phasertng_biological_unit_builder_identity,90);
    constexpr char phasertng_biological_unit_builder_overlap[] = ".phasertng.biological_unit_builder.overlap.";
    value__percent[phasertng_biological_unit_builder_overlap] = type::percent(phasertng_biological_unit_builder_overlap,1);
    constexpr char phasertng_biological_unit_builder_model[] = ".phasertng.biological_unit_builder.model.";
    array__path[phasertng_biological_unit_builder_model] = std::vector<type::path>();
    constexpr char phasertng_brute_rotation_function_volume[] = ".phasertng.brute_rotation_function.volume.";
    value__choice[phasertng_brute_rotation_function_volume] = type::choice(phasertng_brute_rotation_function_volume,"unique around random ","around");
    constexpr char phasertng_brute_rotation_function_range[] = ".phasertng.brute_rotation_function.range.";
    value__flt_number[phasertng_brute_rotation_function_range] = type::flt_number(phasertng_brute_rotation_function_range,false,0,false,0,10);
    constexpr char phasertng_brute_rotation_function_shift_matrix[] = ".phasertng.brute_rotation_function.shift_matrix.";
    value__flt_matrix[phasertng_brute_rotation_function_shift_matrix] = type::flt_matrix(phasertng_brute_rotation_function_shift_matrix,false,0,false,0,{1,0,0,0,1,0,0,0,1});
    constexpr char phasertng_brute_rotation_function_nrand[] = ".phasertng.brute_rotation_function.nrand.";
    value__int_number[phasertng_brute_rotation_function_nrand] = type::int_number(phasertng_brute_rotation_function_nrand,false,0,false,0,1);
    constexpr char phasertng_brute_rotation_function_sampling[] = ".phasertng.brute_rotation_function.sampling.";
    value__flt_number[phasertng_brute_rotation_function_sampling] = type::flt_number(phasertng_brute_rotation_function_sampling,false,0,false,0);
    constexpr char phasertng_brute_rotation_function_helix_factor[] = ".phasertng.brute_rotation_function.helix_factor.";
    value__flt_number[phasertng_brute_rotation_function_helix_factor] = type::flt_number(phasertng_brute_rotation_function_helix_factor,true,1,false,0,1);
    constexpr char phasertng_brute_rotation_function_maximum_stored[] = ".phasertng.brute_rotation_function.maximum_stored.";
    value__int_number[phasertng_brute_rotation_function_maximum_stored] = type::int_number(phasertng_brute_rotation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_brute_rotation_function_maximum_printed[] = ".phasertng.brute_rotation_function.maximum_printed.";
    value__int_number[phasertng_brute_rotation_function_maximum_printed] = type::int_number(phasertng_brute_rotation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_brute_rotation_function_generate_only[] = ".phasertng.brute_rotation_function.generate_only.";
    value__boolean[phasertng_brute_rotation_function_generate_only] = type::boolean(phasertng_brute_rotation_function_generate_only,false);
    constexpr char phasertng_brute_rotation_function_signal_resolution[] = ".phasertng.brute_rotation_function.signal_resolution.";
    value__flt_number[phasertng_brute_rotation_function_signal_resolution] = type::flt_number(phasertng_brute_rotation_function_signal_resolution,true,0,false,0,0);
    constexpr char phasertng_brute_rotation_function_cluster_back[] = ".phasertng.brute_rotation_function.cluster_back.";
    value__int_number[phasertng_brute_rotation_function_cluster_back] = type::int_number(phasertng_brute_rotation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_brute_translation_function_volume[] = ".phasertng.brute_translation_function.volume.";
    value__choice[phasertng_brute_translation_function_volume] = type::choice(phasertng_brute_translation_function_volume,"around random unique box ","around");
    constexpr char phasertng_brute_translation_function_range[] = ".phasertng.brute_translation_function.range.";
    value__flt_number[phasertng_brute_translation_function_range] = type::flt_number(phasertng_brute_translation_function_range,false,0,false,0,10);
    constexpr char phasertng_brute_translation_function_point[] = ".phasertng.brute_translation_function.point.";
    value__flt_vector[phasertng_brute_translation_function_point] = type::flt_vector(phasertng_brute_translation_function_point,false,0,false,0,{0,0,0});
    constexpr char phasertng_brute_translation_function_fractional[] = ".phasertng.brute_translation_function.fractional.";
    value__boolean[phasertng_brute_translation_function_fractional] = type::boolean(phasertng_brute_translation_function_fractional,true);
    constexpr char phasertng_brute_translation_function_nrand[] = ".phasertng.brute_translation_function.nrand.";
    value__int_number[phasertng_brute_translation_function_nrand] = type::int_number(phasertng_brute_translation_function_nrand,false,0,false,0,1);
    constexpr char phasertng_brute_translation_function_sampling[] = ".phasertng.brute_translation_function.sampling.";
    value__flt_number[phasertng_brute_translation_function_sampling] = type::flt_number(phasertng_brute_translation_function_sampling,false,0,false,0);
    constexpr char phasertng_brute_translation_function_sampling_factor[] = ".phasertng.brute_translation_function.sampling_factor.";
    value__flt_number[phasertng_brute_translation_function_sampling_factor] = type::flt_number(phasertng_brute_translation_function_sampling_factor,true,0.1,false,0,4.0);
    constexpr char phasertng_brute_translation_function_maximum_stored[] = ".phasertng.brute_translation_function.maximum_stored.";
    value__int_number[phasertng_brute_translation_function_maximum_stored] = type::int_number(phasertng_brute_translation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_brute_translation_function_maximum_printed[] = ".phasertng.brute_translation_function.maximum_printed.";
    value__int_number[phasertng_brute_translation_function_maximum_printed] = type::int_number(phasertng_brute_translation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_brute_translation_function_cluster_back[] = ".phasertng.brute_translation_function.cluster_back.";
    value__int_number[phasertng_brute_translation_function_cluster_back] = type::int_number(phasertng_brute_translation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_cell_content_scaling_z[] = ".phasertng.cell_content_scaling.z.";
    value__flt_number[phasertng_cell_content_scaling_z] = type::flt_number(phasertng_cell_content_scaling_z,true,1e-06,false,0);
    constexpr char phasertng_cell_scale[] = ".phasertng.cell_scale.";
    value__flt_paired[phasertng_cell_scale] = type::flt_paired(phasertng_cell_scale,true,0.5,true,2,{0.9,1.1});
    constexpr char phasertng_cluster_compound_filename[] = ".phasertng.cluster_compound.filename.";
    value__path[phasertng_cluster_compound_filename] = type::path(phasertng_cluster_compound_filename);
    constexpr char phasertng_coiled_coil[] = ".phasertng.coiled_coil.";
    value__boolean[phasertng_coiled_coil] = type::boolean(phasertng_coiled_coil);
    constexpr char phasertng_composition_number_in_asymmetric_unit[] = ".phasertng.composition.number_in_asymmetric_unit.";
    value__flt_number[phasertng_composition_number_in_asymmetric_unit] = type::flt_number(phasertng_composition_number_in_asymmetric_unit,true,1e-06,false,0);
    constexpr char phasertng_composition_solvent_range[] = ".phasertng.composition.solvent_range.";
    value__percent_paired[phasertng_composition_solvent_range] = type::percent_paired(phasertng_composition_solvent_range,{25,75});
    constexpr char phasertng_composition_map[] = ".phasertng.composition.map.";
    value__boolean[phasertng_composition_map] = type::boolean(phasertng_composition_map,false);
    constexpr char phasertng_composition_maximum_z[] = ".phasertng.composition.maximum_z.";
    value__int_number[phasertng_composition_maximum_z] = type::int_number(phasertng_composition_maximum_z,true,0,false,0);
    constexpr char phasertng_composition_multiplicity_restriction[] = ".phasertng.composition.multiplicity_restriction.";
    value__int_number[phasertng_composition_multiplicity_restriction] = type::int_number(phasertng_composition_multiplicity_restriction,true,1,false,0,1);
    constexpr char phasertng_composition_order_z_expansion_ratio[] = ".phasertng.composition.order_z_expansion_ratio.";
    value__int_number[phasertng_composition_order_z_expansion_ratio] = type::int_number(phasertng_composition_order_z_expansion_ratio,true,1,false,0,1);
    constexpr char phasertng_composition_use_order_z_expansion_ratio[] = ".phasertng.composition.use_order_z_expansion_ratio.";
    value__boolean[phasertng_composition_use_order_z_expansion_ratio] = type::boolean(phasertng_composition_use_order_z_expansion_ratio,true);
    constexpr char phasertng_composition_sort_by_probability[] = ".phasertng.composition.sort_by_probability.";
    value__boolean[phasertng_composition_sort_by_probability] = type::boolean(phasertng_composition_sort_by_probability,false);
    constexpr char phasertng_cross_rotation_function_filename[] = ".phasertng.cross_rotation_function.filename.";
    value__path[phasertng_cross_rotation_function_filename] = type::path(phasertng_cross_rotation_function_filename);
    constexpr char phasertng_dag_cards_subdir[] = ".phasertng.dag.cards.subdir.";
    value__path[phasertng_dag_cards_subdir] = type::path(phasertng_dag_cards_subdir);
    constexpr char phasertng_dag_cards_filename_in_subdir[] = ".phasertng.dag.cards.filename_in_subdir.";
    value__path[phasertng_dag_cards_filename_in_subdir] = type::path(phasertng_dag_cards_filename_in_subdir);
    constexpr char phasertng_dag_phil_filename[] = ".phasertng.dag.phil.filename.";
    value__path[phasertng_dag_phil_filename] = type::path(phasertng_dag_phil_filename);
    constexpr char phasertng_dag_counters[] = ".phasertng.dag.counters.";
    value__int_vector[phasertng_dag_counters] = type::int_vector(phasertng_dag_counters,true,0,false,0,{0,0,0});
    constexpr char phasertng_dag_consecutive[] = ".phasertng.dag.consecutive.";
    value__boolean[phasertng_dag_consecutive] = type::boolean(phasertng_dag_consecutive,true);
    constexpr char phasertng_dag_skip_if_complete[] = ".phasertng.dag.skip_if_complete.";
    value__boolean[phasertng_dag_skip_if_complete] = type::boolean(phasertng_dag_skip_if_complete,false);
    constexpr char phasertng_dag_tncs_present_in_model[] = ".phasertng.dag.tncs_present_in_model.";
    value__boolean[phasertng_dag_tncs_present_in_model] = type::boolean(phasertng_dag_tncs_present_in_model);
    constexpr char phasertng_dag_read_file[] = ".phasertng.dag.read_file.";
    value__boolean[phasertng_dag_read_file] = type::boolean(phasertng_dag_read_file,false);
    constexpr char phasertng_dag_maximum_stored[] = ".phasertng.dag.maximum_stored.";
    value__int_number[phasertng_dag_maximum_stored] = type::int_number(phasertng_dag_maximum_stored,true,1,false,0);
    constexpr char phasertng_data_resolution_available[] = ".phasertng.data.resolution_available.";
    value__flt_paired[phasertng_data_resolution_available] = type::flt_paired(phasertng_data_resolution_available,true,0,false,0);
    constexpr char phasertng_data_unitcell[] = ".phasertng.data.unitcell.";
    value__flt_cell[phasertng_data_unitcell] = type::flt_cell(phasertng_data_unitcell);
    constexpr char phasertng_data_number_of_reflections[] = ".phasertng.data.number_of_reflections.";
    value__int_number[phasertng_data_number_of_reflections] = type::int_number(phasertng_data_number_of_reflections,true,0,false,0);
    constexpr char phasertng_data_anomalous[] = ".phasertng.data.anomalous.";
    value__boolean[phasertng_data_anomalous] = type::boolean(phasertng_data_anomalous);
    constexpr char phasertng_data_intensities[] = ".phasertng.data.intensities.";
    value__boolean[phasertng_data_intensities] = type::boolean(phasertng_data_intensities);
    constexpr char phasertng_data_french_wilson[] = ".phasertng.data.french_wilson.";
    value__boolean[phasertng_data_french_wilson] = type::boolean(phasertng_data_french_wilson);
    constexpr char phasertng_data_map[] = ".phasertng.data.map.";
    value__boolean[phasertng_data_map] = type::boolean(phasertng_data_map);
    constexpr char phasertng_ensemble_filename[] = ".phasertng.ensemble.filename.";
    value__path[phasertng_ensemble_filename] = type::path(phasertng_ensemble_filename);
    constexpr char phasertng_ensemble_tag[] = ".phasertng.ensemble.tag.";
    value__string[phasertng_ensemble_tag] = type::string(phasertng_ensemble_tag);
    constexpr char phasertng_ensemble_disable_percent_deviation_check[] = ".phasertng.ensemble.disable_percent_deviation_check.";
    value__boolean[phasertng_ensemble_disable_percent_deviation_check] = type::boolean(phasertng_ensemble_disable_percent_deviation_check,false);
    constexpr char phasertng_ensemble_disable_structure_factors_correlated_check[] = ".phasertng.ensemble.disable_structure_factors_correlated_check.";
    value__boolean[phasertng_ensemble_disable_structure_factors_correlated_check] = type::boolean(phasertng_ensemble_disable_structure_factors_correlated_check,false);
    constexpr char phasertng_ensemble_percent_deviation[] = ".phasertng.ensemble.percent_deviation.";
    value__percent[phasertng_ensemble_percent_deviation] = type::percent(phasertng_ensemble_percent_deviation,20);
    constexpr char phasertng_ensemble_vrms_estimate[] = ".phasertng.ensemble.vrms_estimate.";
    value__flt_numbers[phasertng_ensemble_vrms_estimate] = type::flt_numbers(phasertng_ensemble_vrms_estimate,true,0,false,0,{1.0});
    constexpr char phasertng_ensemble_selenomethionine[] = ".phasertng.ensemble.selenomethionine.";
    value__boolean[phasertng_ensemble_selenomethionine] = type::boolean(phasertng_ensemble_selenomethionine,false);
    constexpr char phasertng_ensemble_convert_rmsd_to_bfac[] = ".phasertng.ensemble.convert_rmsd_to_bfac.";
    value__boolean[phasertng_ensemble_convert_rmsd_to_bfac] = type::boolean(phasertng_ensemble_convert_rmsd_to_bfac,false);
    constexpr char phasertng_expected_mapllg_target[] = ".phasertng.expected.mapllg.target.";
    value__flt_number[phasertng_expected_mapllg_target] = type::flt_number(phasertng_expected_mapllg_target,false,0,false,0,225);
    constexpr char phasertng_expected_llg_rmsd[] = ".phasertng.expected.llg.rmsd.";
    value__flt_number[phasertng_expected_llg_rmsd] = type::flt_number(phasertng_expected_llg_rmsd,true,0.1,false,0,0.4);
    constexpr char phasertng_expected_llg_id[] = ".phasertng.expected.llg.id.";
    value__uuid_number[phasertng_expected_llg_id] = type::uuid_number(phasertng_expected_llg_id,true,0,false,0);
    constexpr char phasertng_expected_llg_tag[] = ".phasertng.expected.llg.tag.";
    value__string[phasertng_expected_llg_tag] = type::string(phasertng_expected_llg_tag);
    constexpr char phasertng_expected_llg_rellg[] = ".phasertng.expected.llg.rellg.";
    value__flt_number[phasertng_expected_llg_rellg] = type::flt_number(phasertng_expected_llg_rellg,false,0,false,0,0);
    constexpr char phasertng_expected_llg_ellg[] = ".phasertng.expected.llg.ellg.";
    value__flt_number[phasertng_expected_llg_ellg] = type::flt_number(phasertng_expected_llg_ellg,false,0,false,0,0);
    constexpr char phasertng_expected_ellg_speed_versus_accuracy[] = ".phasertng.expected.ellg.speed_versus_accuracy.";
    value__choice[phasertng_expected_ellg_speed_versus_accuracy] = type::choice(phasertng_expected_ellg_speed_versus_accuracy,"aellg eellg ellg ","eellg");
    constexpr char phasertng_expected_ellg_target[] = ".phasertng.expected.ellg.target.";
    value__flt_number[phasertng_expected_ellg_target] = type::flt_number(phasertng_expected_ellg_target,true,0,false,0,225);
    constexpr char phasertng_expected_ellg_maximum_stored[] = ".phasertng.expected.ellg.maximum_stored.";
    value__int_number[phasertng_expected_ellg_maximum_stored] = type::int_number(phasertng_expected_ellg_maximum_stored,true,0,false,0,20);
    constexpr char phasertng_expected_ellg_maximum_first_search[] = ".phasertng.expected.ellg.maximum_first_search.";
    value__int_paired[phasertng_expected_ellg_maximum_first_search] = type::int_paired(phasertng_expected_ellg_maximum_first_search,true,1,true,12,{1,3});
    constexpr char phasertng_expected_ellg_signal_resolution_factor[] = ".phasertng.expected.ellg.signal_resolution_factor.";
    value__flt_number[phasertng_expected_ellg_signal_resolution_factor] = type::flt_number(phasertng_expected_ellg_signal_resolution_factor,true,1,false,0,1.5);
    constexpr char phasertng_expected_atom_scatterer[] = ".phasertng.expected.atom.scatterer.";
    value__strings[phasertng_expected_atom_scatterer] = type::strings(phasertng_expected_atom_scatterer,{"S"});
    constexpr char phasertng_expected_atom_bfactor[] = ".phasertng.expected.atom.bfactor.";
    value__flt_numbers[phasertng_expected_atom_bfactor] = type::flt_numbers(phasertng_expected_atom_bfactor,false,0,false,0,{0,-1.0});
    constexpr char phasertng_expected_atom_rmsd[] = ".phasertng.expected.atom.rmsd.";
    value__flt_number[phasertng_expected_atom_rmsd] = type::flt_number(phasertng_expected_atom_rmsd,false,0,false,0,0.1);
    constexpr char phasertng_expected_atom_target[] = ".phasertng.expected.atom.target.";
    value__flt_number[phasertng_expected_atom_target] = type::flt_number(phasertng_expected_atom_target,false,0,false,0,20);
    constexpr char phasertng_expected_atom_minimum_search_number[] = ".phasertng.expected.atom.minimum_search_number.";
    value__int_number[phasertng_expected_atom_minimum_search_number] = type::int_number(phasertng_expected_atom_minimum_search_number,false,0,false,0,1);
    constexpr char phasertng_expected_subdir[] = ".phasertng.expected.subdir.";
    array__string[phasertng_expected_subdir] = std::vector<type::string>();
    constexpr char phasertng_expected_filenames_in_subdir[] = ".phasertng.expected.filenames_in_subdir.";
    array__string[phasertng_expected_filenames_in_subdir] = std::vector<type::string>();
    constexpr char phasertng_fast_rotation_function_percent[] = ".phasertng.fast_rotation_function.percent.";
    value__percent[phasertng_fast_rotation_function_percent] = type::percent(phasertng_fast_rotation_function_percent,50.);
    constexpr char phasertng_fast_rotation_function_cluster[] = ".phasertng.fast_rotation_function.cluster.";
    value__boolean[phasertng_fast_rotation_function_cluster] = type::boolean(phasertng_fast_rotation_function_cluster,false);
    constexpr char phasertng_fast_rotation_function_cluster_back[] = ".phasertng.fast_rotation_function.cluster_back.";
    value__int_number[phasertng_fast_rotation_function_cluster_back] = type::int_number(phasertng_fast_rotation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_fast_rotation_function_sampling[] = ".phasertng.fast_rotation_function.sampling.";
    value__flt_number[phasertng_fast_rotation_function_sampling] = type::flt_number(phasertng_fast_rotation_function_sampling,false,0,false,0);
    constexpr char phasertng_fast_rotation_function_maximum_stored[] = ".phasertng.fast_rotation_function.maximum_stored.";
    value__int_number[phasertng_fast_rotation_function_maximum_stored] = type::int_number(phasertng_fast_rotation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_fast_rotation_function_maximum_printed[] = ".phasertng.fast_rotation_function.maximum_printed.";
    value__int_number[phasertng_fast_rotation_function_maximum_printed] = type::int_number(phasertng_fast_rotation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_fast_rotation_function_helix_factor[] = ".phasertng.fast_rotation_function.helix_factor.";
    value__flt_number[phasertng_fast_rotation_function_helix_factor] = type::flt_number(phasertng_fast_rotation_function_helix_factor,true,1,true,3,1);
    constexpr char phasertng_fast_rotation_function_top_not_in_pose[] = ".phasertng.fast_rotation_function.top_not_in_pose.";
    value__boolean[phasertng_fast_rotation_function_top_not_in_pose] = type::boolean(phasertng_fast_rotation_function_top_not_in_pose,false);
    constexpr char phasertng_fast_rotation_function_vrms_estimates[] = ".phasertng.fast_rotation_function.vrms_estimates.";
    value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates] = type::flt_numbers(phasertng_fast_rotation_function_vrms_estimates,true,0,false,0);
    constexpr char phasertng_fast_rotation_function_maps_id[] = ".phasertng.fast_rotation_function.maps.id.";
    array__uuid_number[phasertng_fast_rotation_function_maps_id] = std::vector<type::uuid_number>();
    constexpr char phasertng_fast_rotation_function_maps_tag[] = ".phasertng.fast_rotation_function.maps.tag.";
    array__string[phasertng_fast_rotation_function_maps_tag] = std::vector<type::string>();
    constexpr char phasertng_fast_rotation_function_signal_resolution[] = ".phasertng.fast_rotation_function.signal_resolution.";
    value__flt_number[phasertng_fast_rotation_function_signal_resolution] = type::flt_number(phasertng_fast_rotation_function_signal_resolution,true,0,false,0,0);
    constexpr char phasertng_fast_rotation_function_purge_duplicates[] = ".phasertng.fast_rotation_function.purge_duplicates.";
    value__boolean[phasertng_fast_rotation_function_purge_duplicates] = type::boolean(phasertng_fast_rotation_function_purge_duplicates,true);
    constexpr char phasertng_fast_translation_function_percent[] = ".phasertng.fast_translation_function.percent.";
    value__percent[phasertng_fast_translation_function_percent] = type::percent(phasertng_fast_translation_function_percent,75);
    constexpr char phasertng_fast_translation_function_single_atom_use[] = ".phasertng.fast_translation_function.single_atom.use.";
    value__boolean[phasertng_fast_translation_function_single_atom_use] = type::boolean(phasertng_fast_translation_function_single_atom_use,false);
    constexpr char phasertng_fast_translation_function_single_atom_scatterer[] = ".phasertng.fast_translation_function.single_atom.scatterer.";
    value__string[phasertng_fast_translation_function_single_atom_scatterer] = type::string(phasertng_fast_translation_function_single_atom_scatterer,"S");
    constexpr char phasertng_fast_translation_function_cluster[] = ".phasertng.fast_translation_function.cluster.";
    value__boolean[phasertng_fast_translation_function_cluster] = type::boolean(phasertng_fast_translation_function_cluster,true);
    constexpr char phasertng_fast_translation_function_cluster_back[] = ".phasertng.fast_translation_function.cluster_back.";
    value__int_number[phasertng_fast_translation_function_cluster_back] = type::int_number(phasertng_fast_translation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_fast_translation_function_report_cluster[] = ".phasertng.fast_translation_function.report_cluster.";
    value__boolean[phasertng_fast_translation_function_report_cluster] = type::boolean(phasertng_fast_translation_function_report_cluster,true);
    constexpr char phasertng_fast_translation_function_sampling_factor[] = ".phasertng.fast_translation_function.sampling_factor.";
    value__flt_number[phasertng_fast_translation_function_sampling_factor] = type::flt_number(phasertng_fast_translation_function_sampling_factor,true,0.1,false,0,4.0);
    constexpr char phasertng_fast_translation_function_sampling[] = ".phasertng.fast_translation_function.sampling.";
    value__flt_number[phasertng_fast_translation_function_sampling] = type::flt_number(phasertng_fast_translation_function_sampling,true,0.1,false,0);
    constexpr char phasertng_fast_translation_function_add_pose_orientations[] = ".phasertng.fast_translation_function.add_pose_orientations.";
    value__boolean[phasertng_fast_translation_function_add_pose_orientations] = type::boolean(phasertng_fast_translation_function_add_pose_orientations,true);
    constexpr char phasertng_fast_translation_function_stop_best_packing_tfz_over[] = ".phasertng.fast_translation_function.stop.best_packing_tfz_over.";
    value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over] = type::flt_number(phasertng_fast_translation_function_stop_best_packing_tfz_over,true,0,false,0,99);
    constexpr char phasertng_fast_translation_function_stop_rf_percent_under[] = ".phasertng.fast_translation_function.stop.rf_percent_under.";
    value__percent[phasertng_fast_translation_function_stop_rf_percent_under] = type::percent(phasertng_fast_translation_function_stop_rf_percent_under,0);
    constexpr char phasertng_fast_translation_function_stop_rf_zscore_under[] = ".phasertng.fast_translation_function.stop.rf_zscore_under.";
    value__percent[phasertng_fast_translation_function_stop_rf_zscore_under] = type::percent(phasertng_fast_translation_function_stop_rf_zscore_under,0);
    constexpr char phasertng_fast_translation_function_maximum_stored[] = ".phasertng.fast_translation_function.maximum_stored.";
    value__int_number[phasertng_fast_translation_function_maximum_stored] = type::int_number(phasertng_fast_translation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_fast_translation_function_maximum_printed[] = ".phasertng.fast_translation_function.maximum_printed.";
    value__int_number[phasertng_fast_translation_function_maximum_printed] = type::int_number(phasertng_fast_translation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_fast_translation_function_write_maps[] = ".phasertng.fast_translation_function.write_maps.";
    value__boolean[phasertng_fast_translation_function_write_maps] = type::boolean(phasertng_fast_translation_function_write_maps,false);
    constexpr char phasertng_fast_translation_function_packing_use[] = ".phasertng.fast_translation_function.packing.use.";
    value__boolean[phasertng_fast_translation_function_packing_use] = type::boolean(phasertng_fast_translation_function_packing_use,true);
    constexpr char phasertng_fast_translation_function_packing_low_tfz[] = ".phasertng.fast_translation_function.packing.low_tfz.";
    value__flt_number[phasertng_fast_translation_function_packing_low_tfz] = type::flt_number(phasertng_fast_translation_function_packing_low_tfz,true,0,false,0,5);
    constexpr char phasertng_fast_translation_function_helix_factor[] = ".phasertng.fast_translation_function.helix_factor.";
    value__flt_number[phasertng_fast_translation_function_helix_factor] = type::flt_number(phasertng_fast_translation_function_helix_factor,true,1,false,0,1);
    constexpr char phasertng_file_discovery_directory[] = ".phasertng.file_discovery.directory.";
    value__path[phasertng_file_discovery_directory] = type::path(phasertng_file_discovery_directory);
    constexpr char phasertng_file_discovery_filename_in_directory[] = ".phasertng.file_discovery.filename_in_directory.";
    array__path[phasertng_file_discovery_filename_in_directory] = std::vector<type::path>();
    constexpr char phasertng_file_discovery_validated_mtz_files[] = ".phasertng.file_discovery.validated.mtz_files.";
    array__string[phasertng_file_discovery_validated_mtz_files] = std::vector<type::string>();
    constexpr char phasertng_file_discovery_validated_seq_files[] = ".phasertng.file_discovery.validated.seq_files.";
    array__string[phasertng_file_discovery_validated_seq_files] = std::vector<type::string>();
    constexpr char phasertng_file_discovery_validated_pdb_files[] = ".phasertng.file_discovery.validated.pdb_files.";
    array__string[phasertng_file_discovery_validated_pdb_files] = std::vector<type::string>();
    constexpr char phasertng_fuse_translation_function_models_tags[] = ".phasertng.fuse_translation_function.models.tags.";
    value__strings[phasertng_fuse_translation_function_models_tags] = type::strings(phasertng_fuse_translation_function_models_tags);
    constexpr char phasertng_fuse_translation_function_tfz[] = ".phasertng.fuse_translation_function.tfz.";
    value__flt_number[phasertng_fuse_translation_function_tfz] = type::flt_number(phasertng_fuse_translation_function_tfz,true,0,false,0,10);
    constexpr char phasertng_fuse_translation_function_packing_percent[] = ".phasertng.fuse_translation_function.packing_percent.";
    value__percent[phasertng_fuse_translation_function_packing_percent] = type::percent(phasertng_fuse_translation_function_packing_percent,5);
    constexpr char phasertng_fuse_translation_function_maximum_remaining[] = ".phasertng.fuse_translation_function.maximum_remaining.";
    value__int_number[phasertng_fuse_translation_function_maximum_remaining] = type::int_number(phasertng_fuse_translation_function_maximum_remaining,true,0,true,15,10);
    constexpr char phasertng_fuse_translation_function_fused[] = ".phasertng.fuse_translation_function.fused.";
    value__boolean[phasertng_fuse_translation_function_fused] = type::boolean(phasertng_fuse_translation_function_fused);
    constexpr char phasertng_fuse_translation_function_complete[] = ".phasertng.fuse_translation_function.complete.";
    value__boolean[phasertng_fuse_translation_function_complete] = type::boolean(phasertng_fuse_translation_function_complete);
    constexpr char phasertng_graph_report_filestem[] = ".phasertng.graph_report.filestem.";
    value__string[phasertng_graph_report_filestem] = type::string(phasertng_graph_report_filestem,"graph_report");
    constexpr char phasertng_graph_report_best_solution[] = ".phasertng.graph_report.best_solution.";
    value__int_number[phasertng_graph_report_best_solution] = type::int_number(phasertng_graph_report_best_solution,true,1,false,0,1);
    constexpr char phasertng_grid_search_selection[] = ".phasertng.grid_search.selection.";
    value__choice[phasertng_grid_search_selection] = type::choice(phasertng_grid_search_selection,"tra rot both trarot rottra ","tra");
    constexpr char phasertng_grid_search_range[] = ".phasertng.grid_search.range.";
    value__flt_paired[phasertng_grid_search_range] = type::flt_paired(phasertng_grid_search_range,false,0,false,0,{15,3});
    constexpr char phasertng_grid_search_sampling[] = ".phasertng.grid_search.sampling.";
    value__flt_paired[phasertng_grid_search_sampling] = type::flt_paired(phasertng_grid_search_sampling,false,0,false,0,{5,0.5});
    constexpr char phasertng_grid_search_maximum_stored[] = ".phasertng.grid_search.maximum_stored.";
    value__int_number[phasertng_grid_search_maximum_stored] = type::int_number(phasertng_grid_search_maximum_stored,true,0,false,0,1);
    constexpr char phasertng_grid_search_resolution[] = ".phasertng.grid_search.resolution.";
    value__flt_number[phasertng_grid_search_resolution] = type::flt_number(phasertng_grid_search_resolution,true,0,false,0);
    constexpr char phasertng_grid_search_ncyc[] = ".phasertng.grid_search.ncyc.";
    value__int_number[phasertng_grid_search_ncyc] = type::int_number(phasertng_grid_search_ncyc,true,0,false,0,1);
    constexpr char phasertng_grid_search_poses[] = ".phasertng.grid_search.poses.";
    value__int_numbers[phasertng_grid_search_poses] = type::int_numbers(phasertng_grid_search_poses,true,1,false,0);
    constexpr char phasertng_gyration_id[] = ".phasertng.gyration.id.";
    array__uuid_number[phasertng_gyration_id] = std::vector<type::uuid_number>();
    constexpr char phasertng_gyration_tag[] = ".phasertng.gyration.tag.";
    array__string[phasertng_gyration_tag] = std::vector<type::string>();
    constexpr char phasertng_hklin_filename[] = ".phasertng.hklin.filename.";
    value__path[phasertng_hklin_filename] = type::path(phasertng_hklin_filename);
    constexpr char phasertng_hklin_tag[] = ".phasertng.hklin.tag.";
    value__string[phasertng_hklin_tag] = type::string(phasertng_hklin_tag);
    constexpr char phasertng_hklout_filename[] = ".phasertng.hklout.filename.";
    value__path[phasertng_hklout_filename] = type::path(phasertng_hklout_filename);
    constexpr char phasertng_information_ellg_fraction_anomalous[] = ".phasertng.information.ellg_fraction_anomalous.";
    value__flt_number[phasertng_information_ellg_fraction_anomalous] = type::flt_number(phasertng_information_ellg_fraction_anomalous,true,1e-05,true,1,1.0);
    constexpr char phasertng_information_ellg_rms_factor[] = ".phasertng.information.ellg_rms_factor.";
    value__flt_number[phasertng_information_ellg_rms_factor] = type::flt_number(phasertng_information_ellg_rms_factor,true,0,false,0,0.125);
    constexpr char phasertng_information_information_content[] = ".phasertng.information.information_content.";
    value__flt_number[phasertng_information_information_content] = type::flt_number(phasertng_information_information_content,false,0,false,0);
    constexpr char phasertng_information_expected_information_content[] = ".phasertng.information.expected_information_content.";
    value__flt_number[phasertng_information_expected_information_content] = type::flt_number(phasertng_information_expected_information_content,false,0,false,0);
    constexpr char phasertng_information_sad_information_content[] = ".phasertng.information.sad_information_content.";
    value__flt_number[phasertng_information_sad_information_content] = type::flt_number(phasertng_information_sad_information_content,false,0,false,0);
    constexpr char phasertng_information_sad_expected_llg[] = ".phasertng.information.sad_expected_llg.";
    value__flt_number[phasertng_information_sad_expected_llg] = type::flt_number(phasertng_information_sad_expected_llg,false,0,false,0);
    constexpr char phasertng_isostructure_delta_maximum_angstroms[] = ".phasertng.isostructure.delta.maximum_angstroms.";
    value__int_number[phasertng_isostructure_delta_maximum_angstroms] = type::int_number(phasertng_isostructure_delta_maximum_angstroms,true,0,false,0,10);
    constexpr char phasertng_isostructure_delta_maximum_degrees[] = ".phasertng.isostructure.delta.maximum_degrees.";
    value__int_number[phasertng_isostructure_delta_maximum_degrees] = type::int_number(phasertng_isostructure_delta_maximum_degrees,true,0,false,0,5);
    constexpr char phasertng_isostructure_delta_maximum_ncdist[] = ".phasertng.isostructure.delta.maximum_ncdist.";
    value__flt_number[phasertng_isostructure_delta_maximum_ncdist] = type::flt_number(phasertng_isostructure_delta_maximum_ncdist,true,0,false,0,4.0);
    constexpr char phasertng_isostructure_delta_minimum_correlation_coefficient[] = ".phasertng.isostructure.delta.minimum_correlation_coefficient.";
    value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient] = type::flt_number(phasertng_isostructure_delta_minimum_correlation_coefficient,true,0,true,1,0.40);
    constexpr char phasertng_isostructure_delta_maximum_explored[] = ".phasertng.isostructure.delta.maximum_explored.";
    value__int_number[phasertng_isostructure_delta_maximum_explored] = type::int_number(phasertng_isostructure_delta_maximum_explored,true,0,false,0,3);
    constexpr char phasertng_isostructure_delta_resolution[] = ".phasertng.isostructure.delta.resolution.";
    value__flt_number[phasertng_isostructure_delta_resolution] = type::flt_number(phasertng_isostructure_delta_resolution,true,1,false,0,3);
    constexpr char phasertng_isostructure_delta_matches_cut_off[] = ".phasertng.isostructure.delta.matches_cut_off.";
    value__percent[phasertng_isostructure_delta_matches_cut_off] = type::percent(phasertng_isostructure_delta_matches_cut_off,75);
    constexpr char phasertng_isostructure_tutorial_ignore_best[] = ".phasertng.isostructure.tutorial.ignore_best.";
    value__boolean[phasertng_isostructure_tutorial_ignore_best] = type::boolean(phasertng_isostructure_tutorial_ignore_best,false);
    constexpr char phasertng_isostructure_tutorial_ignore_match[] = ".phasertng.isostructure.tutorial.ignore_match.";
    value__strings[phasertng_isostructure_tutorial_ignore_match] = type::strings(phasertng_isostructure_tutorial_ignore_match);
    constexpr char phasertng_isostructure_tutorial_force_match[] = ".phasertng.isostructure.tutorial.force_match.";
    value__string[phasertng_isostructure_tutorial_force_match] = type::string(phasertng_isostructure_tutorial_force_match);
    constexpr char phasertng_isostructure_perfect_match_cc[] = ".phasertng.isostructure.perfect_match_cc.";
    value__flt_number[phasertng_isostructure_perfect_match_cc] = type::flt_number(phasertng_isostructure_perfect_match_cc,true,0,true,1,0.95);
    constexpr char phasertng_isostructure_found_match[] = ".phasertng.isostructure.found.match.";
    value__boolean[phasertng_isostructure_found_match] = type::boolean(phasertng_isostructure_found_match,false);
    constexpr char phasertng_isostructure_found_perfect_match[] = ".phasertng.isostructure.found.perfect_match.";
    value__boolean[phasertng_isostructure_found_perfect_match] = type::boolean(phasertng_isostructure_found_perfect_match,false);
    constexpr char phasertng_isostructure_found_pointgroup[] = ".phasertng.isostructure.found.pointgroup.";
    value__choice[phasertng_isostructure_found_pointgroup] = type::choice(phasertng_isostructure_found_pointgroup,"same expansion reduction ","same");
    constexpr char phasertng_isostructure_found_spacegroup[] = ".phasertng.isostructure.found.spacegroup.";
    value__string[phasertng_isostructure_found_spacegroup] = type::string(phasertng_isostructure_found_spacegroup);
    constexpr char phasertng_isostructure_found_pdbid[] = ".phasertng.isostructure.found.pdbid.";
    value__string[phasertng_isostructure_found_pdbid] = type::string(phasertng_isostructure_found_pdbid);
    constexpr char phasertng_jog_refinement_sampling[] = ".phasertng.jog_refinement.sampling.";
    value__flt_number[phasertng_jog_refinement_sampling] = type::flt_number(phasertng_jog_refinement_sampling,false,0,false,0);
    constexpr char phasertng_jog_refinement_sampling_factor[] = ".phasertng.jog_refinement.sampling_factor.";
    value__flt_number[phasertng_jog_refinement_sampling_factor] = type::flt_number(phasertng_jog_refinement_sampling_factor,true,0.1,false,0,4.0);
    constexpr char phasertng_jog_refinement_maximum_stored[] = ".phasertng.jog_refinement.maximum_stored.";
    value__int_number[phasertng_jog_refinement_maximum_stored] = type::int_number(phasertng_jog_refinement_maximum_stored,true,0,false,0,10);
    constexpr char phasertng_jog_refinement_maximum_printed[] = ".phasertng.jog_refinement.maximum_printed.";
    value__int_number[phasertng_jog_refinement_maximum_printed] = type::int_number(phasertng_jog_refinement_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_jog_refinement_percent[] = ".phasertng.jog_refinement.percent.";
    value__percent[phasertng_jog_refinement_percent] = type::percent(phasertng_jog_refinement_percent,10);
    constexpr char phasertng_jog_refinement_distance[] = ".phasertng.jog_refinement.distance.";
    value__flt_paired[phasertng_jog_refinement_distance] = type::flt_paired(phasertng_jog_refinement_distance,true,0,false,0,{1,10});
    constexpr char phasertng_jog_refinement_jogged[] = ".phasertng.jog_refinement.jogged.";
    value__boolean[phasertng_jog_refinement_jogged] = type::boolean(phasertng_jog_refinement_jogged);
    constexpr char phasertng_labin_inat[] = ".phasertng.labin.inat.";
    value__mtzcol[phasertng_labin_inat] = type::mtzcol(phasertng_labin_inat,"J","I","INAT");
    constexpr char phasertng_labin_siginat[] = ".phasertng.labin.siginat.";
    value__mtzcol[phasertng_labin_siginat] = type::mtzcol(phasertng_labin_siginat,"Q","SIGI","SIGINAT");
    constexpr char phasertng_labin_ipos[] = ".phasertng.labin.ipos.";
    value__mtzcol[phasertng_labin_ipos] = type::mtzcol(phasertng_labin_ipos,"K","I(+)","IPOS");
    constexpr char phasertng_labin_sigipos[] = ".phasertng.labin.sigipos.";
    value__mtzcol[phasertng_labin_sigipos] = type::mtzcol(phasertng_labin_sigipos,"M","SIGI(+)","SIGIPOS");
    constexpr char phasertng_labin_ineg[] = ".phasertng.labin.ineg.";
    value__mtzcol[phasertng_labin_ineg] = type::mtzcol(phasertng_labin_ineg,"K","I(-)","INEG");
    constexpr char phasertng_labin_sigineg[] = ".phasertng.labin.sigineg.";
    value__mtzcol[phasertng_labin_sigineg] = type::mtzcol(phasertng_labin_sigineg,"M","SIGI(-)","SIGINEG");
    constexpr char phasertng_labin_fnat[] = ".phasertng.labin.fnat.";
    value__mtzcol[phasertng_labin_fnat] = type::mtzcol(phasertng_labin_fnat,"F","F","FNAT");
    constexpr char phasertng_labin_sigfnat[] = ".phasertng.labin.sigfnat.";
    value__mtzcol[phasertng_labin_sigfnat] = type::mtzcol(phasertng_labin_sigfnat,"Q","SIGF","SIGFNAT");
    constexpr char phasertng_labin_fpos[] = ".phasertng.labin.fpos.";
    value__mtzcol[phasertng_labin_fpos] = type::mtzcol(phasertng_labin_fpos,"G","F(+)","FPOS");
    constexpr char phasertng_labin_sigfpos[] = ".phasertng.labin.sigfpos.";
    value__mtzcol[phasertng_labin_sigfpos] = type::mtzcol(phasertng_labin_sigfpos,"L","SIGF(+)","SIGFPOS");
    constexpr char phasertng_labin_fneg[] = ".phasertng.labin.fneg.";
    value__mtzcol[phasertng_labin_fneg] = type::mtzcol(phasertng_labin_fneg,"G","F(-)","FNEG");
    constexpr char phasertng_labin_sigfneg[] = ".phasertng.labin.sigfneg.";
    value__mtzcol[phasertng_labin_sigfneg] = type::mtzcol(phasertng_labin_sigfneg,"L","SIGF(-)","SIGFNEG");
    constexpr char phasertng_labin_fcpos[] = ".phasertng.labin.fcpos.";
    value__mtzcol[phasertng_labin_fcpos] = type::mtzcol(phasertng_labin_fcpos,"G","FC(+)","FCPOS");
    constexpr char phasertng_labin_phicpos[] = ".phasertng.labin.phicpos.";
    value__mtzcol[phasertng_labin_phicpos] = type::mtzcol(phasertng_labin_phicpos,"G","PHIC(+)","PHICPOS");
    constexpr char phasertng_labin_fcneg[] = ".phasertng.labin.fcneg.";
    value__mtzcol[phasertng_labin_fcneg] = type::mtzcol(phasertng_labin_fcneg,"G","FC(-)","FCNEG");
    constexpr char phasertng_labin_phicneg[] = ".phasertng.labin.phicneg.";
    value__mtzcol[phasertng_labin_phicneg] = type::mtzcol(phasertng_labin_phicneg,"G","PHIC(-)","PHICNEG");
    constexpr char phasertng_labin_mapf[] = ".phasertng.labin.mapf.";
    value__mtzcol[phasertng_labin_mapf] = type::mtzcol(phasertng_labin_mapf,"F","FMAP","MAPF");
    constexpr char phasertng_labin_mapph[] = ".phasertng.labin.mapph.";
    value__mtzcol[phasertng_labin_mapph] = type::mtzcol(phasertng_labin_mapph,"P","PHMAP","MAPPH");
    constexpr char phasertng_labin_mapdobs[] = ".phasertng.labin.mapdobs.";
    value__mtzcol[phasertng_labin_mapdobs] = type::mtzcol(phasertng_labin_mapdobs,"W","DOBSMAP","MAPDOBS");
    constexpr char phasertng_labin_dobsnat[] = ".phasertng.labin.dobsnat.";
    value__mtzcol[phasertng_labin_dobsnat] = type::mtzcol(phasertng_labin_dobsnat,"R","DOBS","DOBSNAT");
    constexpr char phasertng_labin_feffnat[] = ".phasertng.labin.feffnat.";
    value__mtzcol[phasertng_labin_feffnat] = type::mtzcol(phasertng_labin_feffnat,"F","FEFF","FEFFNAT");
    constexpr char phasertng_labin_dobspos[] = ".phasertng.labin.dobspos.";
    value__mtzcol[phasertng_labin_dobspos] = type::mtzcol(phasertng_labin_dobspos,"R","DOBS(+)","DOBSPOS");
    constexpr char phasertng_labin_feffpos[] = ".phasertng.labin.feffpos.";
    value__mtzcol[phasertng_labin_feffpos] = type::mtzcol(phasertng_labin_feffpos,"F","FEFF(+)","FEFFPOS");
    constexpr char phasertng_labin_dobsneg[] = ".phasertng.labin.dobsneg.";
    value__mtzcol[phasertng_labin_dobsneg] = type::mtzcol(phasertng_labin_dobsneg,"R","DOBS(-)","DOBSNEG");
    constexpr char phasertng_labin_feffneg[] = ".phasertng.labin.feffneg.";
    value__mtzcol[phasertng_labin_feffneg] = type::mtzcol(phasertng_labin_feffneg,"F","FEFF(-)","FEFFNEG");
    constexpr char phasertng_labin_resn[] = ".phasertng.labin.resn.";
    value__mtzcol[phasertng_labin_resn] = type::mtzcol(phasertng_labin_resn,"R","RESN","RESN");
    constexpr char phasertng_labin_einfo[] = ".phasertng.labin.einfo.";
    value__mtzcol[phasertng_labin_einfo] = type::mtzcol(phasertng_labin_einfo,"R","EINFO","EINFO");
    constexpr char phasertng_labin_info[] = ".phasertng.labin.info.";
    value__mtzcol[phasertng_labin_info] = type::mtzcol(phasertng_labin_info,"R","INFO","INFO");
    constexpr char phasertng_labin_sinfo[] = ".phasertng.labin.sinfo.";
    value__mtzcol[phasertng_labin_sinfo] = type::mtzcol(phasertng_labin_sinfo,"R","SINFO","SINFO");
    constexpr char phasertng_labin_sellg[] = ".phasertng.labin.sellg.";
    value__mtzcol[phasertng_labin_sellg] = type::mtzcol(phasertng_labin_sellg,"R","SELLG","SELLG");
    constexpr char phasertng_labin_sefom[] = ".phasertng.labin.sefom.";
    value__mtzcol[phasertng_labin_sefom] = type::mtzcol(phasertng_labin_sefom,"R","SEFOM","SEFOM");
    constexpr char phasertng_labin_aniso[] = ".phasertng.labin.aniso.";
    value__mtzcol[phasertng_labin_aniso] = type::mtzcol(phasertng_labin_aniso,"R","ANISO","ANISO");
    constexpr char phasertng_labin_anisobeta[] = ".phasertng.labin.anisobeta.";
    value__mtzcol[phasertng_labin_anisobeta] = type::mtzcol(phasertng_labin_anisobeta,"R","ANISOBETA","ANISOBETA");
    constexpr char phasertng_labin_teps[] = ".phasertng.labin.teps.";
    value__mtzcol[phasertng_labin_teps] = type::mtzcol(phasertng_labin_teps,"R","TEPS","TEPS");
    constexpr char phasertng_labin_tbin[] = ".phasertng.labin.tbin.";
    value__mtzcol[phasertng_labin_tbin] = type::mtzcol(phasertng_labin_tbin,"R","TBIN","TBIN");
    constexpr char phasertng_labin_fwt[] = ".phasertng.labin.fwt.";
    value__mtzcol[phasertng_labin_fwt] = type::mtzcol(phasertng_labin_fwt,"F","FWT","FWT");
    constexpr char phasertng_labin_phwt[] = ".phasertng.labin.phwt.";
    value__mtzcol[phasertng_labin_phwt] = type::mtzcol(phasertng_labin_phwt,"P","PHWT","PHWT");
    constexpr char phasertng_labin_delfwt[] = ".phasertng.labin.delfwt.";
    value__mtzcol[phasertng_labin_delfwt] = type::mtzcol(phasertng_labin_delfwt,"F","DELFWT","DELFWT");
    constexpr char phasertng_labin_delphwt[] = ".phasertng.labin.delphwt.";
    value__mtzcol[phasertng_labin_delphwt] = type::mtzcol(phasertng_labin_delphwt,"P","DELPHWT","DELPHWT");
    constexpr char phasertng_labin_fcnat[] = ".phasertng.labin.fcnat.";
    value__mtzcol[phasertng_labin_fcnat] = type::mtzcol(phasertng_labin_fcnat,"F","FC","FCNAT");
    constexpr char phasertng_labin_phicnat[] = ".phasertng.labin.phicnat.";
    value__mtzcol[phasertng_labin_phicnat] = type::mtzcol(phasertng_labin_phicnat,"P","PHIC","PHICNAT");
    constexpr char phasertng_labin_llgf[] = ".phasertng.labin.llgf.";
    value__mtzcol[phasertng_labin_llgf] = type::mtzcol(phasertng_labin_llgf,"F","LLGF","LLGF");
    constexpr char phasertng_labin_llgph[] = ".phasertng.labin.llgph.";
    value__mtzcol[phasertng_labin_llgph] = type::mtzcol(phasertng_labin_llgph,"P","LLGPHI","LLGPH");
    constexpr char phasertng_labin_fom[] = ".phasertng.labin.fom.";
    value__mtzcol[phasertng_labin_fom] = type::mtzcol(phasertng_labin_fom,"W","FOM","FOM");
    constexpr char phasertng_labin_hla[] = ".phasertng.labin.hla.";
    value__mtzcol[phasertng_labin_hla] = type::mtzcol(phasertng_labin_hla,"A","HLA","HLA");
    constexpr char phasertng_labin_hlb[] = ".phasertng.labin.hlb.";
    value__mtzcol[phasertng_labin_hlb] = type::mtzcol(phasertng_labin_hlb,"A","HLB","HLB");
    constexpr char phasertng_labin_hlc[] = ".phasertng.labin.hlc.";
    value__mtzcol[phasertng_labin_hlc] = type::mtzcol(phasertng_labin_hlc,"A","HLC","HLC");
    constexpr char phasertng_labin_hld[] = ".phasertng.labin.hld.";
    value__mtzcol[phasertng_labin_hld] = type::mtzcol(phasertng_labin_hld,"A","HLD","HLD");
    constexpr char phasertng_labin_siga[] = ".phasertng.labin.siga.";
    value__mtzcol[phasertng_labin_siga] = type::mtzcol(phasertng_labin_siga,"R","SIGA","SIGA");
    constexpr char phasertng_labin_ssqr[] = ".phasertng.labin.ssqr.";
    value__mtzcol[phasertng_labin_ssqr] = type::mtzcol(phasertng_labin_ssqr,"R","SSQR","SSQR");
    constexpr char phasertng_labin_eps[] = ".phasertng.labin.eps.";
    value__mtzcol[phasertng_labin_eps] = type::mtzcol(phasertng_labin_eps,"R","EPS","EPS");
    constexpr char phasertng_labin_phsr[] = ".phasertng.labin.phsr.";
    value__mtzcol[phasertng_labin_phsr] = type::mtzcol(phasertng_labin_phsr,"R","PHSR","PHSR");
    constexpr char phasertng_labin_bin[] = ".phasertng.labin.bin.";
    value__mtzcol[phasertng_labin_bin] = type::mtzcol(phasertng_labin_bin,"I","BIN","BIN");
    constexpr char phasertng_labin_free[] = ".phasertng.labin.free.";
    value__mtzcol[phasertng_labin_free] = type::mtzcol(phasertng_labin_free,"I","FREE","FREE");
    constexpr char phasertng_labin_cent[] = ".phasertng.labin.cent.";
    value__mtzcol[phasertng_labin_cent] = type::mtzcol(phasertng_labin_cent,"B","CENT","CENT");
    constexpr char phasertng_labin_both[] = ".phasertng.labin.both.";
    value__mtzcol[phasertng_labin_both] = type::mtzcol(phasertng_labin_both,"B","BOTH","BOTH");
    constexpr char phasertng_labin_plus[] = ".phasertng.labin.plus.";
    value__mtzcol[phasertng_labin_plus] = type::mtzcol(phasertng_labin_plus,"B","PLUS","PLUS");
    constexpr char phasertng_macaniso_protocol[] = ".phasertng.macaniso.protocol.";
    value__choice[phasertng_macaniso_protocol] = type::choice(phasertng_macaniso_protocol,"off on ","on");
    constexpr char phasertng_macaniso_macrocycle1[] = ".phasertng.macaniso.macrocycle1.";
    value__choice[phasertng_macaniso_macrocycle1] = type::choice(phasertng_macaniso_macrocycle1,"off scale aniso bins ","scale aniso bins");
    constexpr char phasertng_macaniso_macrocycle2[] = ".phasertng.macaniso.macrocycle2.";
    value__choice[phasertng_macaniso_macrocycle2] = type::choice(phasertng_macaniso_macrocycle2,"off scale aniso bins ","off");
    constexpr char phasertng_macaniso_macrocycle3[] = ".phasertng.macaniso.macrocycle3.";
    value__choice[phasertng_macaniso_macrocycle3] = type::choice(phasertng_macaniso_macrocycle3,"off scale aniso bins ","off");
    constexpr char phasertng_macaniso_ncyc[] = ".phasertng.macaniso.ncyc.";
    value__int_vector[phasertng_macaniso_ncyc] = type::int_vector(phasertng_macaniso_ncyc,true,0,false,0,{100,100,100});
    constexpr char phasertng_macaniso_minimizer[] = ".phasertng.macaniso.minimizer.";
    value__choice[phasertng_macaniso_minimizer] = type::choice(phasertng_macaniso_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macaniso_best_curve_restraint[] = ".phasertng.macaniso.best_curve_restraint.";
    value__boolean[phasertng_macaniso_best_curve_restraint] = type::boolean(phasertng_macaniso_best_curve_restraint,true);
    constexpr char phasertng_macaniso_resolution[] = ".phasertng.macaniso.resolution.";
    value__flt_number[phasertng_macaniso_resolution] = type::flt_number(phasertng_macaniso_resolution,true,0,false,0,0);
    constexpr char phasertng_macaniso_study_parameters[] = ".phasertng.macaniso.study_parameters.";
    value__boolean[phasertng_macaniso_study_parameters] = type::boolean(phasertng_macaniso_study_parameters,false);
    constexpr char phasertng_macbox_protocol[] = ".phasertng.macbox.protocol.";
    value__choice[phasertng_macbox_protocol] = type::choice(phasertng_macbox_protocol,"off on ","on");
    constexpr char phasertng_macbox_macrocycle1[] = ".phasertng.macbox.macrocycle1.";
    value__choice[phasertng_macbox_macrocycle1] = type::choice(phasertng_macbox_macrocycle1,"off scale aniso bins ","scale aniso bins");
    constexpr char phasertng_macbox_macrocycle2[] = ".phasertng.macbox.macrocycle2.";
    value__choice[phasertng_macbox_macrocycle2] = type::choice(phasertng_macbox_macrocycle2,"off scale aniso bins ","scale aniso bins");
    constexpr char phasertng_macbox_macrocycle3[] = ".phasertng.macbox.macrocycle3.";
    value__choice[phasertng_macbox_macrocycle3] = type::choice(phasertng_macbox_macrocycle3,"off scale aniso bins ","scale aniso bins");
    constexpr char phasertng_macbox_ncyc[] = ".phasertng.macbox.ncyc.";
    value__int_vector[phasertng_macbox_ncyc] = type::int_vector(phasertng_macbox_ncyc,true,0,false,0,{100,100,100});
    constexpr char phasertng_macbox_minimizer[] = ".phasertng.macbox.minimizer.";
    value__choice[phasertng_macbox_minimizer] = type::choice(phasertng_macbox_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macbox_best_curve_restraint[] = ".phasertng.macbox.best_curve_restraint.";
    value__boolean[phasertng_macbox_best_curve_restraint] = type::boolean(phasertng_macbox_best_curve_restraint,true);
    constexpr char phasertng_macbox_study_parameters[] = ".phasertng.macbox.study_parameters.";
    value__boolean[phasertng_macbox_study_parameters] = type::boolean(phasertng_macbox_study_parameters,false);
    constexpr char phasertng_macgyre_protocol[] = ".phasertng.macgyre.protocol.";
    value__choice[phasertng_macgyre_protocol] = type::choice(phasertng_macgyre_protocol,"off on ","on");
    constexpr char phasertng_macgyre_macrocycle1[] = ".phasertng.macgyre.macrocycle1.";
    value__choice[phasertng_macgyre_macrocycle1] = type::choice(phasertng_macgyre_macrocycle1,"off rotn tran vrms ","rotn tran vrms");
    constexpr char phasertng_macgyre_macrocycle2[] = ".phasertng.macgyre.macrocycle2.";
    value__choice[phasertng_macgyre_macrocycle2] = type::choice(phasertng_macgyre_macrocycle2,"off rotn tran vrms ","off");
    constexpr char phasertng_macgyre_macrocycle3[] = ".phasertng.macgyre.macrocycle3.";
    value__choice[phasertng_macgyre_macrocycle3] = type::choice(phasertng_macgyre_macrocycle3,"off rotn tran vrms ","off");
    constexpr char phasertng_macgyre_ncyc[] = ".phasertng.macgyre.ncyc.";
    value__int_vector[phasertng_macgyre_ncyc] = type::int_vector(phasertng_macgyre_ncyc,true,0,false,0,{50,50,50});
    constexpr char phasertng_macgyre_minimizer[] = ".phasertng.macgyre.minimizer.";
    value__choice[phasertng_macgyre_minimizer] = type::choice(phasertng_macgyre_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macgyre_small_target[] = ".phasertng.macgyre.small_target.";
    value__flt_number[phasertng_macgyre_small_target] = type::flt_number(phasertng_macgyre_small_target,true,0,false,0,0.01);
    constexpr char phasertng_macgyre_restraint_sigma_rotation[] = ".phasertng.macgyre.restraint_sigma.rotation.";
    value__flt_number[phasertng_macgyre_restraint_sigma_rotation] = type::flt_number(phasertng_macgyre_restraint_sigma_rotation,true,0,false,0,0);
    constexpr char phasertng_macgyre_restraint_sigma_position[] = ".phasertng.macgyre.restraint_sigma.position.";
    value__flt_number[phasertng_macgyre_restraint_sigma_position] = type::flt_number(phasertng_macgyre_restraint_sigma_position,true,0,false,0,0);
    constexpr char phasertng_macgyre_maximum_printed[] = ".phasertng.macgyre.maximum_printed.";
    value__int_number[phasertng_macgyre_maximum_printed] = type::int_number(phasertng_macgyre_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_macgyre_purge_duplicates[] = ".phasertng.macgyre.purge_duplicates.";
    value__boolean[phasertng_macgyre_purge_duplicates] = type::boolean(phasertng_macgyre_purge_duplicates,true);
    constexpr char phasertng_macgyre_refine_helices[] = ".phasertng.macgyre.refine_helices.";
    value__boolean[phasertng_macgyre_refine_helices] = type::boolean(phasertng_macgyre_refine_helices,false);
    constexpr char phasertng_macgyre_study_parameters[] = ".phasertng.macgyre.study_parameters.";
    value__boolean[phasertng_macgyre_study_parameters] = type::boolean(phasertng_macgyre_study_parameters,false);
    constexpr char phasertng_macjog_protocol[] = ".phasertng.macjog.protocol.";
    value__choice[phasertng_macjog_protocol] = type::choice(phasertng_macjog_protocol,"off on ","on");
    constexpr char phasertng_macjog_macrocycle1[] = ".phasertng.macjog.macrocycle1.";
    value__choice[phasertng_macjog_macrocycle1] = type::choice(phasertng_macjog_macrocycle1,"off rotn tran bfac vrms cell ","rotn tran bfac vrms");
    constexpr char phasertng_macjog_macrocycle2[] = ".phasertng.macjog.macrocycle2.";
    value__choice[phasertng_macjog_macrocycle2] = type::choice(phasertng_macjog_macrocycle2,"off rotn tran bfac vrms cell last ","off");
    constexpr char phasertng_macjog_macrocycle3[] = ".phasertng.macjog.macrocycle3.";
    value__choice[phasertng_macjog_macrocycle3] = type::choice(phasertng_macjog_macrocycle3,"off rotn tran bfac vrms cell last ","off");
    constexpr char phasertng_macjog_ncyc[] = ".phasertng.macjog.ncyc.";
    value__int_vector[phasertng_macjog_ncyc] = type::int_vector(phasertng_macjog_ncyc,true,0,false,0,{50,0,0});
    constexpr char phasertng_macjog_minimizer[] = ".phasertng.macjog.minimizer.";
    value__choice[phasertng_macjog_minimizer] = type::choice(phasertng_macjog_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macjog_small_target[] = ".phasertng.macjog.small_target.";
    value__flt_number[phasertng_macjog_small_target] = type::flt_number(phasertng_macjog_small_target,true,0,false,0,0.01);
    constexpr char phasertng_macjog_restraint_sigma_rotation[] = ".phasertng.macjog.restraint_sigma.rotation.";
    value__flt_number[phasertng_macjog_restraint_sigma_rotation] = type::flt_number(phasertng_macjog_restraint_sigma_rotation,true,0,false,0,0);
    constexpr char phasertng_macjog_restraint_sigma_translation[] = ".phasertng.macjog.restraint_sigma.translation.";
    value__flt_number[phasertng_macjog_restraint_sigma_translation] = type::flt_number(phasertng_macjog_restraint_sigma_translation,true,0,false,0,0);
    constexpr char phasertng_macjog_restraint_sigma_bfactor[] = ".phasertng.macjog.restraint_sigma.bfactor.";
    value__flt_number[phasertng_macjog_restraint_sigma_bfactor] = type::flt_number(phasertng_macjog_restraint_sigma_bfactor,true,0,false,0,6);
    constexpr char phasertng_macjog_restraint_sigma_drms[] = ".phasertng.macjog.restraint_sigma.drms.";
    value__flt_number[phasertng_macjog_restraint_sigma_drms] = type::flt_number(phasertng_macjog_restraint_sigma_drms,true,0,false,0,1);
    constexpr char phasertng_macjog_restraint_sigma_cell[] = ".phasertng.macjog.restraint_sigma.cell.";
    value__flt_number[phasertng_macjog_restraint_sigma_cell] = type::flt_number(phasertng_macjog_restraint_sigma_cell,true,0,false,0,0);
    constexpr char phasertng_macrbr_protocol[] = ".phasertng.macrbr.protocol.";
    value__choice[phasertng_macrbr_protocol] = type::choice(phasertng_macrbr_protocol,"off on ","on");
    constexpr char phasertng_macrbr_macrocycle1[] = ".phasertng.macrbr.macrocycle1.";
    value__choice[phasertng_macrbr_macrocycle1] = type::choice(phasertng_macrbr_macrocycle1,"off rotn tran bfac vrms cell ","rotn tran bfac vrms");
    constexpr char phasertng_macrbr_macrocycle2[] = ".phasertng.macrbr.macrocycle2.";
    value__choice[phasertng_macrbr_macrocycle2] = type::choice(phasertng_macrbr_macrocycle2,"off rotn tran bfac vrms cell ","off");
    constexpr char phasertng_macrbr_macrocycle3[] = ".phasertng.macrbr.macrocycle3.";
    value__choice[phasertng_macrbr_macrocycle3] = type::choice(phasertng_macrbr_macrocycle3,"off rotn tran bfac vrms cell ","off");
    constexpr char phasertng_macrbr_ncyc[] = ".phasertng.macrbr.ncyc.";
    value__int_vector[phasertng_macrbr_ncyc] = type::int_vector(phasertng_macrbr_ncyc,true,0,false,0,{50,50,50});
    constexpr char phasertng_macrbr_last_only[] = ".phasertng.macrbr.last_only.";
    value__boolean[phasertng_macrbr_last_only] = type::boolean(phasertng_macrbr_last_only,false);
    constexpr char phasertng_macrbr_minimizer[] = ".phasertng.macrbr.minimizer.";
    value__choice[phasertng_macrbr_minimizer] = type::choice(phasertng_macrbr_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macrbr_small_target[] = ".phasertng.macrbr.small_target.";
    value__flt_number[phasertng_macrbr_small_target] = type::flt_number(phasertng_macrbr_small_target,true,0,false,0,0.01);
    constexpr char phasertng_macrbr_restraint_sigma_rotation[] = ".phasertng.macrbr.restraint_sigma.rotation.";
    value__flt_number[phasertng_macrbr_restraint_sigma_rotation] = type::flt_number(phasertng_macrbr_restraint_sigma_rotation,true,0,false,0,0);
    constexpr char phasertng_macrbr_restraint_sigma_translation[] = ".phasertng.macrbr.restraint_sigma.translation.";
    value__flt_number[phasertng_macrbr_restraint_sigma_translation] = type::flt_number(phasertng_macrbr_restraint_sigma_translation,true,0,false,0,0);
    constexpr char phasertng_macrbr_restraint_sigma_bfactor[] = ".phasertng.macrbr.restraint_sigma.bfactor.";
    value__flt_number[phasertng_macrbr_restraint_sigma_bfactor] = type::flt_number(phasertng_macrbr_restraint_sigma_bfactor,true,0,false,0,6);
    constexpr char phasertng_macrbr_restraint_sigma_drms[] = ".phasertng.macrbr.restraint_sigma.drms.";
    value__flt_number[phasertng_macrbr_restraint_sigma_drms] = type::flt_number(phasertng_macrbr_restraint_sigma_drms,true,0,false,0,1);
    constexpr char phasertng_macrbr_restraint_sigma_cell[] = ".phasertng.macrbr.restraint_sigma.cell.";
    value__flt_number[phasertng_macrbr_restraint_sigma_cell] = type::flt_number(phasertng_macrbr_restraint_sigma_cell,true,0,false,0,0);
    constexpr char phasertng_macrbr_maximum_stored[] = ".phasertng.macrbr.maximum_stored.";
    value__int_number[phasertng_macrbr_maximum_stored] = type::int_number(phasertng_macrbr_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_macrbr_maximum_printed[] = ".phasertng.macrbr.maximum_printed.";
    value__int_number[phasertng_macrbr_maximum_printed] = type::int_number(phasertng_macrbr_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_macrbr_purge_duplicates[] = ".phasertng.macrbr.purge_duplicates.";
    value__boolean[phasertng_macrbr_purge_duplicates] = type::boolean(phasertng_macrbr_purge_duplicates,true);
    constexpr char phasertng_macrbr_cleanup[] = ".phasertng.macrbr.cleanup.";
    value__boolean[phasertng_macrbr_cleanup] = type::boolean(phasertng_macrbr_cleanup,false);
    constexpr char phasertng_macrbr_study_parameters[] = ".phasertng.macrbr.study_parameters.";
    value__boolean[phasertng_macrbr_study_parameters] = type::boolean(phasertng_macrbr_study_parameters,false);
    constexpr char phasertng_macrm_protocol[] = ".phasertng.macrm.protocol.";
    value__choice[phasertng_macrm_protocol] = type::choice(phasertng_macrm_protocol,"off on ","on");
    constexpr char phasertng_macrm_macrocycle1[] = ".phasertng.macrm.macrocycle1.";
    value__choice[phasertng_macrm_macrocycle1] = type::choice(phasertng_macrm_macrocycle1,"off rotn tran bfac vrms cell ","rotn tran");
    constexpr char phasertng_macrm_macrocycle2[] = ".phasertng.macrm.macrocycle2.";
    value__choice[phasertng_macrm_macrocycle2] = type::choice(phasertng_macrm_macrocycle2,"off rotn tran bfac vrms cell ","rotn tran vrms");
    constexpr char phasertng_macrm_macrocycle3[] = ".phasertng.macrm.macrocycle3.";
    value__choice[phasertng_macrm_macrocycle3] = type::choice(phasertng_macrm_macrocycle3,"off rotn tran bfac vrms cell ","off");
    constexpr char phasertng_macrm_ncyc[] = ".phasertng.macrm.ncyc.";
    value__int_vector[phasertng_macrm_ncyc] = type::int_vector(phasertng_macrm_ncyc,true,0,false,0,{50,50,50});
    constexpr char phasertng_macrm_minimizer[] = ".phasertng.macrm.minimizer.";
    value__choice[phasertng_macrm_minimizer] = type::choice(phasertng_macrm_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macrm_small_target[] = ".phasertng.macrm.small_target.";
    value__flt_number[phasertng_macrm_small_target] = type::flt_number(phasertng_macrm_small_target,true,0,false,0,0.1);
    constexpr char phasertng_macrm_restraint_sigma_rotation[] = ".phasertng.macrm.restraint_sigma.rotation.";
    value__flt_number[phasertng_macrm_restraint_sigma_rotation] = type::flt_number(phasertng_macrm_restraint_sigma_rotation,true,0,false,0,0);
    constexpr char phasertng_macrm_restraint_sigma_translation[] = ".phasertng.macrm.restraint_sigma.translation.";
    value__flt_number[phasertng_macrm_restraint_sigma_translation] = type::flt_number(phasertng_macrm_restraint_sigma_translation,true,0,false,0,0);
    constexpr char phasertng_macrm_restraint_sigma_bfactor[] = ".phasertng.macrm.restraint_sigma.bfactor.";
    value__flt_number[phasertng_macrm_restraint_sigma_bfactor] = type::flt_number(phasertng_macrm_restraint_sigma_bfactor,true,0,false,0,0);
    constexpr char phasertng_macrm_restraint_sigma_drms[] = ".phasertng.macrm.restraint_sigma.drms.";
    value__flt_number[phasertng_macrm_restraint_sigma_drms] = type::flt_number(phasertng_macrm_restraint_sigma_drms,true,0,false,0,1);
    constexpr char phasertng_macrm_restraint_sigma_cell[] = ".phasertng.macrm.restraint_sigma.cell.";
    value__flt_number[phasertng_macrm_restraint_sigma_cell] = type::flt_number(phasertng_macrm_restraint_sigma_cell,true,0,false,0,0);
    constexpr char phasertng_macrm_maximum_stored[] = ".phasertng.macrm.maximum_stored.";
    value__int_number[phasertng_macrm_maximum_stored] = type::int_number(phasertng_macrm_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_macrm_maximum_printed[] = ".phasertng.macrm.maximum_printed.";
    value__int_number[phasertng_macrm_maximum_printed] = type::int_number(phasertng_macrm_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_macrm_percent[] = ".phasertng.macrm.percent.";
    value__percent[phasertng_macrm_percent] = type::percent(phasertng_macrm_percent,75.);
    constexpr char phasertng_macrm_study_parameters[] = ".phasertng.macrm.study_parameters.";
    value__boolean[phasertng_macrm_study_parameters] = type::boolean(phasertng_macrm_study_parameters,false);
    constexpr char phasertng_macsad_macrocycle1[] = ".phasertng.macsad.macrocycle1.";
    value__choice[phasertng_macsad_macrocycle1] = type::choice(phasertng_macsad_macrocycle1,"off data siga sigb sigp sigd site bfac occ fdp part ","occ part");
    constexpr char phasertng_macsad_macrocycle2[] = ".phasertng.macsad.macrocycle2.";
    value__choice[phasertng_macsad_macrocycle2] = type::choice(phasertng_macsad_macrocycle2,"off data siga sigb sigp sigd site bfac occ fdp part ","sigd site bfac occ part");
    constexpr char phasertng_macsad_macrocycle3[] = ".phasertng.macsad.macrocycle3.";
    value__choice[phasertng_macsad_macrocycle3] = type::choice(phasertng_macsad_macrocycle3,"off data siga sigb sigp sigd site bfac occ fdp part ","off");
    constexpr char phasertng_macsad_ncyc[] = ".phasertng.macsad.ncyc.";
    value__int_vector[phasertng_macsad_ncyc] = type::int_vector(phasertng_macsad_ncyc,true,0,false,0,{50,50,50});
    constexpr char phasertng_macsad_minimizer[] = ".phasertng.macsad.minimizer.";
    value__choice[phasertng_macsad_minimizer] = type::choice(phasertng_macsad_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macsad_hand[] = ".phasertng.macsad.hand.";
    value__boolean[phasertng_macsad_hand] = type::boolean(phasertng_macsad_hand,false);
    constexpr char phasertng_macsad_enantiomorphs[] = ".phasertng.macsad.enantiomorphs.";
    value__boolean[phasertng_macsad_enantiomorphs] = type::boolean(phasertng_macsad_enantiomorphs,true);
    constexpr char phasertng_macsad_use_partial_anomalous[] = ".phasertng.macsad.use_partial_anomalous.";
    value__boolean[phasertng_macsad_use_partial_anomalous] = type::boolean(phasertng_macsad_use_partial_anomalous,false);
    constexpr char phasertng_macsad_restraint_sigma_sphericity[] = ".phasertng.macsad.restraint_sigma.sphericity.";
    value__flt_number[phasertng_macsad_restraint_sigma_sphericity] = type::flt_number(phasertng_macsad_restraint_sigma_sphericity,true,0,false,0,10);
    constexpr char phasertng_macsad_restraint_sigma_wilson_bfactor[] = ".phasertng.macsad.restraint_sigma.wilson_bfactor.";
    value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor] = type::flt_number(phasertng_macsad_restraint_sigma_wilson_bfactor,true,0,false,0,2);
    constexpr char phasertng_macsad_restraint_sigma_fdp[] = ".phasertng.macsad.restraint_sigma.fdp.";
    value__flt_number[phasertng_macsad_restraint_sigma_fdp] = type::flt_number(phasertng_macsad_restraint_sigma_fdp,true,0,false,0,5);
    constexpr char phasertng_macsad_integration_steps[] = ".phasertng.macsad.integration_steps.";
    value__int_number[phasertng_macsad_integration_steps] = type::int_number(phasertng_macsad_integration_steps,true,0,false,0);
    constexpr char phasertng_macsad_study_parameters[] = ".phasertng.macsad.study_parameters.";
    value__boolean[phasertng_macsad_study_parameters] = type::boolean(phasertng_macsad_study_parameters,false);
    constexpr char phasertng_macspr_protocol[] = ".phasertng.macspr.protocol.";
    value__choice[phasertng_macspr_protocol] = type::choice(phasertng_macspr_protocol,"off on ","on");
    constexpr char phasertng_macspr_macrocycle1[] = ".phasertng.macspr.macrocycle1.";
    value__choice[phasertng_macspr_macrocycle1] = type::choice(phasertng_macspr_macrocycle1,"off bfac occ ","occ");
    constexpr char phasertng_macspr_macrocycle2[] = ".phasertng.macspr.macrocycle2.";
    value__choice[phasertng_macspr_macrocycle2] = type::choice(phasertng_macspr_macrocycle2,"off bfac occ ","off");
    constexpr char phasertng_macspr_macrocycle3[] = ".phasertng.macspr.macrocycle3.";
    value__choice[phasertng_macspr_macrocycle3] = type::choice(phasertng_macspr_macrocycle3,"off bfac occ ","off");
    constexpr char phasertng_macspr_ncyc[] = ".phasertng.macspr.ncyc.";
    value__int_vector[phasertng_macspr_ncyc] = type::int_vector(phasertng_macspr_ncyc,true,0,false,0,{50,50,50});
    constexpr char phasertng_macspr_minimizer[] = ".phasertng.macspr.minimizer.";
    value__choice[phasertng_macspr_minimizer] = type::choice(phasertng_macspr_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macspr_small_target[] = ".phasertng.macspr.small_target.";
    value__flt_number[phasertng_macspr_small_target] = type::flt_number(phasertng_macspr_small_target,true,0,false,0,0.1);
    constexpr char phasertng_macspr_study_parameters[] = ".phasertng.macspr.study_parameters.";
    value__boolean[phasertng_macspr_study_parameters] = type::boolean(phasertng_macspr_study_parameters,false);
    constexpr char phasertng_macssa_macrocycle1[] = ".phasertng.macssa.macrocycle1.";
    value__choice[phasertng_macssa_macrocycle1] = type::choice(phasertng_macssa_macrocycle1,"off rhoff rhopm ","rhoff rhopm");
    constexpr char phasertng_macssa_macrocycle2[] = ".phasertng.macssa.macrocycle2.";
    value__choice[phasertng_macssa_macrocycle2] = type::choice(phasertng_macssa_macrocycle2,"off rhoff rhopm ","rhoff rhopm");
    constexpr char phasertng_macssa_macrocycle3[] = ".phasertng.macssa.macrocycle3.";
    value__choice[phasertng_macssa_macrocycle3] = type::choice(phasertng_macssa_macrocycle3,"off rhoff rhopm ","off");
    constexpr char phasertng_macssa_ncyc[] = ".phasertng.macssa.ncyc.";
    value__int_vector[phasertng_macssa_ncyc] = type::int_vector(phasertng_macssa_ncyc,true,0,false,0,{100,100,100});
    constexpr char phasertng_macssa_minimizer[] = ".phasertng.macssa.minimizer.";
    value__choice[phasertng_macssa_minimizer] = type::choice(phasertng_macssa_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_macssa_restraint_sigma_rhopm[] = ".phasertng.macssa.restraint_sigma.rhopm.";
    value__flt_number[phasertng_macssa_restraint_sigma_rhopm] = type::flt_number(phasertng_macssa_restraint_sigma_rhopm,true,0,false,0,0.2);
    constexpr char phasertng_macssa_restraint_sigma_atoms[] = ".phasertng.macssa.restraint_sigma.atoms.";
    value__flt_number[phasertng_macssa_restraint_sigma_atoms] = type::flt_number(phasertng_macssa_restraint_sigma_atoms,true,0,false,0,1.5);
    constexpr char phasertng_macssa_study_parameters[] = ".phasertng.macssa.study_parameters.";
    value__boolean[phasertng_macssa_study_parameters] = type::boolean(phasertng_macssa_study_parameters,false);
    constexpr char phasertng_mactncs_protocol[] = ".phasertng.mactncs.protocol.";
    value__choice[phasertng_mactncs_protocol] = type::choice(phasertng_mactncs_protocol,"off on ","on");
    constexpr char phasertng_mactncs_macrocycle1[] = ".phasertng.mactncs.macrocycle1.";
    value__choice[phasertng_mactncs_macrocycle1] = type::choice(phasertng_mactncs_macrocycle1,"off rot gfn tra rmsd fs bins ","rot tra rmsd fs");
    constexpr char phasertng_mactncs_macrocycle2[] = ".phasertng.mactncs.macrocycle2.";
    value__choice[phasertng_mactncs_macrocycle2] = type::choice(phasertng_mactncs_macrocycle2,"off rot gfn tra rmsd fs bins ","rot tra bins");
    constexpr char phasertng_mactncs_macrocycle3[] = ".phasertng.mactncs.macrocycle3.";
    value__choice[phasertng_mactncs_macrocycle3] = type::choice(phasertng_mactncs_macrocycle3,"off rot gfn tra rmsd fs bins ","off");
    constexpr char phasertng_mactncs_ncyc[] = ".phasertng.mactncs.ncyc.";
    value__int_vector[phasertng_mactncs_ncyc] = type::int_vector(phasertng_mactncs_ncyc,true,0,false,0,{100,100,100});
    constexpr char phasertng_mactncs_minimizer[] = ".phasertng.mactncs.minimizer.";
    value__choice[phasertng_mactncs_minimizer] = type::choice(phasertng_mactncs_minimizer,"bfgs newton ","bfgs");
    constexpr char phasertng_mactncs_perturbation[] = ".phasertng.mactncs.perturbation.";
    value__int_numbers[phasertng_mactncs_perturbation] = type::int_numbers(phasertng_mactncs_perturbation,true,0,false,0);
    constexpr char phasertng_mactncs_restraint_sigma_rot[] = ".phasertng.mactncs.restraint_sigma.rot.";
    value__flt_number[phasertng_mactncs_restraint_sigma_rot] = type::flt_number(phasertng_mactncs_restraint_sigma_rot,true,0,false,0,0);
    constexpr char phasertng_mactncs_restraint_sigma_tra[] = ".phasertng.mactncs.restraint_sigma.tra.";
    value__flt_number[phasertng_mactncs_restraint_sigma_tra] = type::flt_number(phasertng_mactncs_restraint_sigma_tra,true,0,false,0,0);
    constexpr char phasertng_mactncs_restraint_sigma_gfn[] = ".phasertng.mactncs.restraint_sigma.gfn.";
    value__flt_number[phasertng_mactncs_restraint_sigma_gfn] = type::flt_number(phasertng_mactncs_restraint_sigma_gfn,true,0,false,0,0);
    constexpr char phasertng_mactncs_restraint_sigma_rmsd[] = ".phasertng.mactncs.restraint_sigma.rmsd.";
    value__flt_number[phasertng_mactncs_restraint_sigma_rmsd] = type::flt_number(phasertng_mactncs_restraint_sigma_rmsd,true,0,false,0,0);
    constexpr char phasertng_mactncs_restraint_sigma_fs[] = ".phasertng.mactncs.restraint_sigma.fs.";
    value__flt_number[phasertng_mactncs_restraint_sigma_fs] = type::flt_number(phasertng_mactncs_restraint_sigma_fs,true,0,false,0,0);
    constexpr char phasertng_mactncs_restraint_sigma_binwise_unity[] = ".phasertng.mactncs.restraint_sigma.binwise_unity.";
    value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity] = type::flt_number(phasertng_mactncs_restraint_sigma_binwise_unity,true,0,false,0,0.01);
    constexpr char phasertng_mactncs_restraint_sigma_smooth[] = ".phasertng.mactncs.restraint_sigma.smooth.";
    value__flt_number[phasertng_mactncs_restraint_sigma_smooth] = type::flt_number(phasertng_mactncs_restraint_sigma_smooth,true,0,false,0,0);
    constexpr char phasertng_mactncs_resolution[] = ".phasertng.mactncs.resolution.";
    value__flt_number[phasertng_mactncs_resolution] = type::flt_number(phasertng_mactncs_resolution,true,0,false,0,0);
    constexpr char phasertng_mactncs_study_parameters[] = ".phasertng.mactncs.study_parameters.";
    value__boolean[phasertng_mactncs_study_parameters] = type::boolean(phasertng_mactncs_study_parameters,false);
    constexpr char phasertng_map_correlation_coefficient_similarity[] = ".phasertng.map_correlation_coefficient.similarity.";
    value__flt_number[phasertng_map_correlation_coefficient_similarity] = type::flt_number(phasertng_map_correlation_coefficient_similarity,true,0.4,true,1,0.95);
    constexpr char phasertng_map_correlation_coefficient_maximum_stored[] = ".phasertng.map_correlation_coefficient.maximum_stored.";
    value__int_number[phasertng_map_correlation_coefficient_maximum_stored] = type::int_number(phasertng_map_correlation_coefficient_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_map_correlation_coefficient_maximum_printed[] = ".phasertng.map_correlation_coefficient.maximum_printed.";
    value__int_number[phasertng_map_correlation_coefficient_maximum_printed] = type::int_number(phasertng_map_correlation_coefficient_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_map_ellg_fs[] = ".phasertng.map_ellg.fs.";
    value__flt_numbers[phasertng_map_ellg_fs] = type::flt_numbers(phasertng_map_ellg_fs,true,0,true,1,{0.5,1.0});
    constexpr char phasertng_map_ellg_rmsd[] = ".phasertng.map_ellg.rmsd.";
    value__flt_numbers[phasertng_map_ellg_rmsd] = type::flt_numbers(phasertng_map_ellg_rmsd,true,0,false,0,{0.4,0.8,1.2,1.6});
    constexpr char phasertng_map_ellg_result_fs[] = ".phasertng.map_ellg.result.fs.";
    array__flt_number[phasertng_map_ellg_result_fs] = std::vector<type::flt_number>();
    constexpr char phasertng_map_ellg_result_rmsd[] = ".phasertng.map_ellg.result.rmsd.";
    array__flt_number[phasertng_map_ellg_result_rmsd] = std::vector<type::flt_number>();
    constexpr char phasertng_map_ellg_result_ellg[] = ".phasertng.map_ellg.result.ellg.";
    array__flt_number[phasertng_map_ellg_result_ellg] = std::vector<type::flt_number>();
    constexpr char phasertng_mapin_filename[] = ".phasertng.mapin.filename.";
    value__path[phasertng_mapin_filename] = type::path(phasertng_mapin_filename);
    constexpr char phasertng_mapin_tag[] = ".phasertng.mapin.tag.";
    value__string[phasertng_mapin_tag] = type::string(phasertng_mapin_tag);
    constexpr char phasertng_mapin_oversample_resolution_limit[] = ".phasertng.mapin.oversample_resolution_limit.";
    value__flt_number[phasertng_mapin_oversample_resolution_limit] = type::flt_number(phasertng_mapin_oversample_resolution_limit,true,0.1,false,0,0.5);
    constexpr char phasertng_matthews_vm[] = ".phasertng.matthews.vm.";
    array__flt_number[phasertng_matthews_vm] = std::vector<type::flt_number>();
    constexpr char phasertng_matthews_probability[] = ".phasertng.matthews.probability.";
    array__flt_number[phasertng_matthews_probability] = std::vector<type::flt_number>();
    constexpr char phasertng_matthews_z[] = ".phasertng.matthews.z.";
    array__flt_number[phasertng_matthews_z] = std::vector<type::flt_number>();
    constexpr char phasertng_mirrors[] = ".phasertng.mirrors.";
    value__strings[phasertng_mirrors] = type::strings(phasertng_mirrors,{"rcsb","pdbe","pdbj"});
    constexpr char phasertng_mode[] = ".phasertng.mode.";
    value__choice[phasertng_mode] = type::choice(phasertng_mode,"chk bub walk tree ipdb imtz imap data sga sgx aniso tncso tncs feff twin srf srfp deep join pool hyss sscc find cca ccs esm emm msm rellg perm ellg etfz eatm emap frf frfr brf gyre ftf ftfr btf ptf pose put pak move span exp1 rbr rbgs tfz mapz fuse spr rmr rbm mcc jog info ssa ssd ssm ssp ssr rfac beam fetch xref test ","chk");
    constexpr char phasertng_model_filename[] = ".phasertng.model.filename.";
    value__path[phasertng_model_filename] = type::path(phasertng_model_filename);
    constexpr char phasertng_model_tag[] = ".phasertng.model.tag.";
    value__string[phasertng_model_tag] = type::string(phasertng_model_tag);
    constexpr char phasertng_model_single_atom_use[] = ".phasertng.model.single_atom.use.";
    value__boolean[phasertng_model_single_atom_use] = type::boolean(phasertng_model_single_atom_use,false);
    constexpr char phasertng_model_single_atom_scatterer[] = ".phasertng.model.single_atom.scatterer.";
    value__string[phasertng_model_single_atom_scatterer] = type::string(phasertng_model_single_atom_scatterer);
    constexpr char phasertng_model_single_atom_rmsd[] = ".phasertng.model.single_atom.rmsd.";
    value__flt_number[phasertng_model_single_atom_rmsd] = type::flt_number(phasertng_model_single_atom_rmsd,true,0,false,0,0.1);
    constexpr char phasertng_model_vrms_estimate[] = ".phasertng.model.vrms_estimate.";
    value__flt_number[phasertng_model_vrms_estimate] = type::flt_number(phasertng_model_vrms_estimate,true,0,false,0,1.0);
    constexpr char phasertng_model_selenomethionine[] = ".phasertng.model.selenomethionine.";
    value__boolean[phasertng_model_selenomethionine] = type::boolean(phasertng_model_selenomethionine,false);
    constexpr char phasertng_model_convert_rmsd_to_bfac[] = ".phasertng.model.convert_rmsd_to_bfac.";
    value__boolean[phasertng_model_convert_rmsd_to_bfac] = type::boolean(phasertng_model_convert_rmsd_to_bfac,false);
    constexpr char phasertng_model_as_map_filename[] = ".phasertng.model_as_map.filename.";
    value__path[phasertng_model_as_map_filename] = type::path(phasertng_model_as_map_filename);
    constexpr char phasertng_model_as_map_resolution[] = ".phasertng.model_as_map.resolution.";
    value__flt_number[phasertng_model_as_map_resolution] = type::flt_number(phasertng_model_as_map_resolution,true,0,false,0,0);
    constexpr char phasertng_model_as_map_fmap[] = ".phasertng.model_as_map.fmap.";
    value__string[phasertng_model_as_map_fmap] = type::string(phasertng_model_as_map_fmap);
    constexpr char phasertng_model_as_map_phimap[] = ".phasertng.model_as_map.phimap.";
    value__string[phasertng_model_as_map_phimap] = type::string(phasertng_model_as_map_phimap);
    constexpr char phasertng_model_as_map_tag[] = ".phasertng.model_as_map.tag.";
    value__string[phasertng_model_as_map_tag] = type::string(phasertng_model_as_map_tag);
    constexpr char phasertng_model_as_map_sequence_filename[] = ".phasertng.model_as_map.sequence.filename.";
    value__path[phasertng_model_as_map_sequence_filename] = type::path(phasertng_model_as_map_sequence_filename);
    constexpr char phasertng_model_as_map_vrms_estimate[] = ".phasertng.model_as_map.vrms_estimate.";
    value__flt_number[phasertng_model_as_map_vrms_estimate] = type::flt_number(phasertng_model_as_map_vrms_estimate,true,0,false,0,1.0);
    constexpr char phasertng_model_as_map_point_group_symbol[] = ".phasertng.model_as_map.point_group_symbol.";
    value__string[phasertng_model_as_map_point_group_symbol] = type::string(phasertng_model_as_map_point_group_symbol);
    constexpr char phasertng_model_as_map_point_group_euler[] = ".phasertng.model_as_map.point_group_euler.";
    value__flt_numbers[phasertng_model_as_map_point_group_euler] = type::flt_numbers(phasertng_model_as_map_point_group_euler,false,0,false,0);
    constexpr char phasertng_model_as_map_centre[] = ".phasertng.model_as_map.centre.";
    value__flt_vector[phasertng_model_as_map_centre] = type::flt_vector(phasertng_model_as_map_centre,false,0,false,0);
    constexpr char phasertng_model_as_map_solvent_constant[] = ".phasertng.model_as_map.solvent_constant.";
    value__flt_number[phasertng_model_as_map_solvent_constant] = type::flt_number(phasertng_model_as_map_solvent_constant,false,0,false,0,0);
    constexpr char phasertng_model_as_map_extent_minimum[] = ".phasertng.model_as_map.extent_minimum.";
    value__flt_vector[phasertng_model_as_map_extent_minimum] = type::flt_vector(phasertng_model_as_map_extent_minimum,false,0,false,0);
    constexpr char phasertng_model_as_map_extent_maximum[] = ".phasertng.model_as_map.extent_maximum.";
    value__flt_vector[phasertng_model_as_map_extent_maximum] = type::flt_vector(phasertng_model_as_map_extent_maximum,false,0,false,0);
    constexpr char phasertng_model_as_map_wang_volume_factor[] = ".phasertng.model_as_map.wang_volume_factor.";
    value__flt_number[phasertng_model_as_map_wang_volume_factor] = type::flt_number(phasertng_model_as_map_wang_volume_factor,true,0.5,true,2,0.5);
    constexpr char phasertng_model_as_map_wang_unsharpen_bfactor[] = ".phasertng.model_as_map.wang_unsharpen_bfactor.";
    value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor] = type::flt_number(phasertng_model_as_map_wang_unsharpen_bfactor,true,0,true,150,100);
    constexpr char phasertng_model_as_map_oversample_resolution_limit[] = ".phasertng.model_as_map.oversample_resolution_limit.";
    value__flt_number[phasertng_model_as_map_oversample_resolution_limit] = type::flt_number(phasertng_model_as_map_oversample_resolution_limit,true,0.1,false,0,0.5);
    constexpr char phasertng_model_as_map_no_phases[] = ".phasertng.model_as_map.no_phases.";
    value__boolean[phasertng_model_as_map_no_phases] = type::boolean(phasertng_model_as_map_no_phases,false);
    constexpr char phasertng_model_as_map_require_box[] = ".phasertng.model_as_map.require_box.";
    value__boolean[phasertng_model_as_map_require_box] = type::boolean(phasertng_model_as_map_require_box,true);
    constexpr char phasertng_molecular_transform_bulk_solvent_use[] = ".phasertng.molecular_transform.bulk_solvent.use.";
    value__boolean[phasertng_molecular_transform_bulk_solvent_use] = type::boolean(phasertng_molecular_transform_bulk_solvent_use,false);
    constexpr char phasertng_molecular_transform_bulk_solvent_fsol[] = ".phasertng.molecular_transform.bulk_solvent.fsol.";
    value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol] = type::flt_number(phasertng_molecular_transform_bulk_solvent_fsol,true,0.01,true,1,0.35);
    constexpr char phasertng_molecular_transform_bulk_solvent_bsol[] = ".phasertng.molecular_transform.bulk_solvent.bsol.";
    value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol] = type::flt_number(phasertng_molecular_transform_bulk_solvent_bsol,false,0,false,0,45);
    constexpr char phasertng_molecular_transform_write_coordinates[] = ".phasertng.molecular_transform.write_coordinates.";
    value__boolean[phasertng_molecular_transform_write_coordinates] = type::boolean(phasertng_molecular_transform_write_coordinates,false);
    constexpr char phasertng_molecular_transform_boxscale[] = ".phasertng.molecular_transform.boxscale.";
    value__flt_number[phasertng_molecular_transform_boxscale] = type::flt_number(phasertng_molecular_transform_boxscale,true,2,false,0,6);
    constexpr char phasertng_molecular_transform_resolution[] = ".phasertng.molecular_transform.resolution.";
    value__flt_number[phasertng_molecular_transform_resolution] = type::flt_number(phasertng_molecular_transform_resolution,true,0,false,0,0);
    constexpr char phasertng_molecular_transform_skip_bfactor_analysis[] = ".phasertng.molecular_transform.skip_bfactor_analysis.";
    value__boolean[phasertng_molecular_transform_skip_bfactor_analysis] = type::boolean(phasertng_molecular_transform_skip_bfactor_analysis,false);
    constexpr char phasertng_molecular_transform_reorient[] = ".phasertng.molecular_transform.reorient.";
    value__boolean[phasertng_molecular_transform_reorient] = type::boolean(phasertng_molecular_transform_reorient,true);
    constexpr char phasertng_molecular_transform_fix[] = ".phasertng.molecular_transform.fix.";
    value__boolean[phasertng_molecular_transform_fix] = type::boolean(phasertng_molecular_transform_fix,false);
    constexpr char phasertng_molecular_transform_bfactor_trim[] = ".phasertng.molecular_transform.bfactor_trim.";
    value__flt_number[phasertng_molecular_transform_bfactor_trim] = type::flt_number(phasertng_molecular_transform_bfactor_trim,true,0,false,0,0);
    constexpr char phasertng_molecular_transform_barbwire_test[] = ".phasertng.molecular_transform.barbwire_test.";
    value__boolean[phasertng_molecular_transform_barbwire_test] = type::boolean(phasertng_molecular_transform_barbwire_test,true);
    constexpr char phasertng_molecular_transform_helix_test[] = ".phasertng.molecular_transform.helix_test.";
    value__boolean[phasertng_molecular_transform_helix_test] = type::boolean(phasertng_molecular_transform_helix_test,true);
    constexpr char phasertng_molecular_transform_decomposition_radius_factor[] = ".phasertng.molecular_transform.decomposition_radius_factor.";
    value__flt_number[phasertng_molecular_transform_decomposition_radius_factor] = type::flt_number(phasertng_molecular_transform_decomposition_radius_factor,true,0.1,false,0,2);
    constexpr char phasertng_molecular_transform_preparation[] = ".phasertng.molecular_transform.preparation.";
    value__choice[phasertng_molecular_transform_preparation] = type::choice(phasertng_molecular_transform_preparation,"off decomposition interpolation trace monostructure variance ","decomposition interpolation trace monostructure variance");
    constexpr char phasertng_mtzout[] = ".phasertng.mtzout.";
    value__string[phasertng_mtzout] = type::string(phasertng_mtzout);
    constexpr char phasertng_notifications_hires[] = ".phasertng.notifications.hires.";
    value__flt_number[phasertng_notifications_hires] = type::flt_number(phasertng_notifications_hires,true,0,false,0);
    constexpr char phasertng_notifications_advisory[] = ".phasertng.notifications.advisory.";
    array__string[phasertng_notifications_advisory] = std::vector<type::string>();
    constexpr char phasertng_notifications_warning[] = ".phasertng.notifications.warning.";
    array__string[phasertng_notifications_warning] = std::vector<type::string>();
    constexpr char phasertng_notifications_git_revision[] = ".phasertng.notifications.git_revision.";
    value__string[phasertng_notifications_git_revision] = type::string(phasertng_notifications_git_revision);
    constexpr char phasertng_notifications_success[] = ".phasertng.notifications.success.";
    value__boolean[phasertng_notifications_success] = type::boolean(phasertng_notifications_success,true);
    constexpr char phasertng_notifications_error_type[] = ".phasertng.notifications.error.type.";
    value__choice[phasertng_notifications_error_type] = type::choice(phasertng_notifications_error_type,"success parse syntax input fileset fileopen memory killfile killtime fatal result unhandled unknown stdin developer python assert ","success");
    constexpr char phasertng_notifications_error_message[] = ".phasertng.notifications.error.message.";
    value__string[phasertng_notifications_error_message] = type::string(phasertng_notifications_error_message);
    constexpr char phasertng_notifications_error_code[] = ".phasertng.notifications.error.code.";
    value__int_number[phasertng_notifications_error_code] = type::int_number(phasertng_notifications_error_code,false,0,false,0);
    constexpr char phasertng_notifications_result_subdir[] = ".phasertng.notifications.result.subdir.";
    value__path[phasertng_notifications_result_subdir] = type::path(phasertng_notifications_result_subdir);
    constexpr char phasertng_notifications_result_filename_in_subdir[] = ".phasertng.notifications.result.filename_in_subdir.";
    value__path[phasertng_notifications_result_filename_in_subdir] = type::path(phasertng_notifications_result_filename_in_subdir);
    constexpr char phasertng_outlier_reject[] = ".phasertng.outlier.reject.";
    value__boolean[phasertng_outlier_reject] = type::boolean(phasertng_outlier_reject,true);
    constexpr char phasertng_outlier_probability[] = ".phasertng.outlier.probability.";
    value__flt_number[phasertng_outlier_probability] = type::flt_number(phasertng_outlier_probability,true,0,false,0,1e-06);
    constexpr char phasertng_outlier_information[] = ".phasertng.outlier.information.";
    value__flt_number[phasertng_outlier_information] = type::flt_number(phasertng_outlier_information,true,1e-08,false,0,0.01);
    constexpr char phasertng_outlier_maximum_printed[] = ".phasertng.outlier.maximum_printed.";
    value__int_number[phasertng_outlier_maximum_printed] = type::int_number(phasertng_outlier_maximum_printed,true,0,false,0,20);
    constexpr char phasertng_overwrite[] = ".phasertng.overwrite.";
    value__boolean[phasertng_overwrite] = type::boolean(phasertng_overwrite,true);
    constexpr char phasertng_packing_percent[] = ".phasertng.packing.percent.";
    value__percent[phasertng_packing_percent] = type::percent(phasertng_packing_percent,5);
    constexpr char phasertng_packing_keep_high_tfz[] = ".phasertng.packing.keep_high_tfz.";
    value__boolean[phasertng_packing_keep_high_tfz] = type::boolean(phasertng_packing_keep_high_tfz,true);
    constexpr char phasertng_packing_high_tfz[] = ".phasertng.packing.high_tfz.";
    value__flt_number[phasertng_packing_high_tfz] = type::flt_number(phasertng_packing_high_tfz,true,0,false,0,11);
    constexpr char phasertng_packing_overlap[] = ".phasertng.packing.overlap.";
    value__percent[phasertng_packing_overlap] = type::percent(phasertng_packing_overlap,20);
    constexpr char phasertng_packing_maximum_stored[] = ".phasertng.packing.maximum_stored.";
    value__int_number[phasertng_packing_maximum_stored] = type::int_number(phasertng_packing_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_packing_maximum_printed[] = ".phasertng.packing.maximum_printed.";
    value__int_number[phasertng_packing_maximum_printed] = type::int_number(phasertng_packing_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_packing_move_to_origin[] = ".phasertng.packing.move_to_origin.";
    value__boolean[phasertng_packing_move_to_origin] = type::boolean(phasertng_packing_move_to_origin,true);
    constexpr char phasertng_packing_move_to_compact[] = ".phasertng.packing.move_to_compact.";
    value__boolean[phasertng_packing_move_to_compact] = type::boolean(phasertng_packing_move_to_compact,true);
    constexpr char phasertng_packing_move_to_input[] = ".phasertng.packing.move_to_input.";
    value__boolean[phasertng_packing_move_to_input] = type::boolean(phasertng_packing_move_to_input,true);
    constexpr char phasertng_packing_move_when_tncs_present[] = ".phasertng.packing.move_when_tncs_present.";
    value__boolean[phasertng_packing_move_when_tncs_present] = type::boolean(phasertng_packing_move_when_tncs_present,true);
    constexpr char phasertng_packing_distance_factor[] = ".phasertng.packing.distance_factor.";
    value__flt_number[phasertng_packing_distance_factor] = type::flt_number(phasertng_packing_distance_factor,true,0.1,false,0,0.5);
    constexpr char phasertng_packing_clash_coordinates[] = ".phasertng.packing.clash_coordinates.";
    value__boolean[phasertng_packing_clash_coordinates] = type::boolean(phasertng_packing_clash_coordinates,false);
    constexpr char phasertng_packing_revert_on_failure[] = ".phasertng.packing.revert_on_failure.";
    value__boolean[phasertng_packing_revert_on_failure] = type::boolean(phasertng_packing_revert_on_failure,false);
    constexpr char phasertng_packing_reverted[] = ".phasertng.packing.reverted.";
    value__boolean[phasertng_packing_reverted] = type::boolean(phasertng_packing_reverted);
    constexpr char phasertng_pdbin_filename[] = ".phasertng.pdbin.filename.";
    value__path[phasertng_pdbin_filename] = type::path(phasertng_pdbin_filename);
    constexpr char phasertng_pdbin_tag[] = ".phasertng.pdbin.tag.";
    value__string[phasertng_pdbin_tag] = type::string(phasertng_pdbin_tag);
    constexpr char phasertng_pdbin_convert_rmsd_to_bfac[] = ".phasertng.pdbin.convert_rmsd_to_bfac.";
    value__boolean[phasertng_pdbin_convert_rmsd_to_bfac] = type::boolean(phasertng_pdbin_convert_rmsd_to_bfac,false);
    constexpr char phasertng_pdbin_bulk_solvent_use[] = ".phasertng.pdbin.bulk_solvent.use.";
    value__boolean[phasertng_pdbin_bulk_solvent_use] = type::boolean(phasertng_pdbin_bulk_solvent_use,false);
    constexpr char phasertng_pdbin_bulk_solvent_fsol[] = ".phasertng.pdbin.bulk_solvent.fsol.";
    value__flt_number[phasertng_pdbin_bulk_solvent_fsol] = type::flt_number(phasertng_pdbin_bulk_solvent_fsol,true,0.01,true,1,0.35);
    constexpr char phasertng_pdbin_bulk_solvent_bsol[] = ".phasertng.pdbin.bulk_solvent.bsol.";
    value__flt_number[phasertng_pdbin_bulk_solvent_bsol] = type::flt_number(phasertng_pdbin_bulk_solvent_bsol,false,0,false,0,45);
    constexpr char phasertng_pdbout_filename[] = ".phasertng.pdbout.filename.";
    value__path[phasertng_pdbout_filename] = type::path(phasertng_pdbout_filename);
    constexpr char phasertng_pdbout_pdbid[] = ".phasertng.pdbout.pdbid.";
    value__string[phasertng_pdbout_pdbid] = type::string(phasertng_pdbout_pdbid);
    constexpr char phasertng_pdbout_tag[] = ".phasertng.pdbout.tag.";
    value__string[phasertng_pdbout_tag] = type::string(phasertng_pdbout_tag);
    constexpr char phasertng_pdbout_id[] = ".phasertng.pdbout.id.";
    value__uuid_number[phasertng_pdbout_id] = type::uuid_number(phasertng_pdbout_id,true,0,false,0);
    constexpr char phasertng_pdbout_deposited_title[] = ".phasertng.pdbout.deposited_title.";
    value__string[phasertng_pdbout_deposited_title] = type::string(phasertng_pdbout_deposited_title);
    constexpr char phasertng_permutations_tags[] = ".phasertng.permutations.tags.";
    array__strings[phasertng_permutations_tags] = std::vector<type::strings>();
    constexpr char phasertng_permutations_matthews_z[] = ".phasertng.permutations.matthews_z.";
    value__int_number[phasertng_permutations_matthews_z] = type::int_number(phasertng_permutations_matthews_z,false,0,false,0,1);
    constexpr char phasertng_permutations_tags_in_search_order[] = ".phasertng.permutations.tags_in_search_order.";
    value__strings[phasertng_permutations_tags_in_search_order] = type::strings(phasertng_permutations_tags_in_search_order);
    constexpr char phasertng_perturbations_selection[] = ".phasertng.perturbations.selection.";
    value__choice[phasertng_perturbations_selection] = type::choice(phasertng_perturbations_selection,"tra rot both ","tra");
    constexpr char phasertng_perturbations_range[] = ".phasertng.perturbations.range.";
    value__flt_paired[phasertng_perturbations_range] = type::flt_paired(phasertng_perturbations_range,false,0,false,0,{1,1});
    constexpr char phasertng_perturbations_number[] = ".phasertng.perturbations.number.";
    value__int_number[phasertng_perturbations_number] = type::int_number(phasertng_perturbations_number,true,0,false,0,5);
    constexpr char phasertng_phased_translation_function_cluster[] = ".phasertng.phased_translation_function.cluster.";
    value__boolean[phasertng_phased_translation_function_cluster] = type::boolean(phasertng_phased_translation_function_cluster,true);
    constexpr char phasertng_phased_translation_function_cluster_back[] = ".phasertng.phased_translation_function.cluster_back.";
    value__int_number[phasertng_phased_translation_function_cluster_back] = type::int_number(phasertng_phased_translation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_phased_translation_function_sampling[] = ".phasertng.phased_translation_function.sampling.";
    value__flt_number[phasertng_phased_translation_function_sampling] = type::flt_number(phasertng_phased_translation_function_sampling,false,0,false,0);
    constexpr char phasertng_phased_translation_function_sampling_factor[] = ".phasertng.phased_translation_function.sampling_factor.";
    value__flt_number[phasertng_phased_translation_function_sampling_factor] = type::flt_number(phasertng_phased_translation_function_sampling_factor,true,0.1,false,0,4.0);
    constexpr char phasertng_phased_translation_function_add_pose_orientations[] = ".phasertng.phased_translation_function.add_pose_orientations.";
    value__boolean[phasertng_phased_translation_function_add_pose_orientations] = type::boolean(phasertng_phased_translation_function_add_pose_orientations,false);
    constexpr char phasertng_phased_translation_function_maximum_stored[] = ".phasertng.phased_translation_function.maximum_stored.";
    value__int_number[phasertng_phased_translation_function_maximum_stored] = type::int_number(phasertng_phased_translation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_phased_translation_function_maximum_printed[] = ".phasertng.phased_translation_function.maximum_printed.";
    value__int_number[phasertng_phased_translation_function_maximum_printed] = type::int_number(phasertng_phased_translation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_phased_translation_function_percent[] = ".phasertng.phased_translation_function.percent.";
    value__percent[phasertng_phased_translation_function_percent] = type::percent(phasertng_phased_translation_function_percent,75);
    constexpr char phasertng_phased_translation_function_write_maps[] = ".phasertng.phased_translation_function.write_maps.";
    value__boolean[phasertng_phased_translation_function_write_maps] = type::boolean(phasertng_phased_translation_function_write_maps,false);
    constexpr char phasertng_point_group_calculate_from_coordinates[] = ".phasertng.point_group.calculate_from_coordinates.";
    value__boolean[phasertng_point_group_calculate_from_coordinates] = type::boolean(phasertng_point_group_calculate_from_coordinates,true);
    constexpr char phasertng_point_group_coverage[] = ".phasertng.point_group.coverage.";
    value__percent[phasertng_point_group_coverage] = type::percent(phasertng_point_group_coverage,90);
    constexpr char phasertng_point_group_identity[] = ".phasertng.point_group.identity.";
    value__percent[phasertng_point_group_identity] = type::percent(phasertng_point_group_identity,90);
    constexpr char phasertng_point_group_rmsd[] = ".phasertng.point_group.rmsd.";
    value__flt_number[phasertng_point_group_rmsd] = type::flt_number(phasertng_point_group_rmsd,true,0.1,false,0,3.0);
    constexpr char phasertng_point_group_angular_tolerance[] = ".phasertng.point_group.angular_tolerance.";
    value__flt_number[phasertng_point_group_angular_tolerance] = type::flt_number(phasertng_point_group_angular_tolerance,true,0.001,false,0,0.035);
    constexpr char phasertng_point_group_spatial_tolerance[] = ".phasertng.point_group.spatial_tolerance.";
    value__flt_number[phasertng_point_group_spatial_tolerance] = type::flt_number(phasertng_point_group_spatial_tolerance,true,0.01,false,0,1.0);
    constexpr char phasertng_pose_scoring_rescore[] = ".phasertng.pose_scoring.rescore.";
    value__boolean[phasertng_pose_scoring_rescore] = type::boolean(phasertng_pose_scoring_rescore,true);
    constexpr char phasertng_pose_scoring_maximum_printed[] = ".phasertng.pose_scoring.maximum_printed.";
    value__int_number[phasertng_pose_scoring_maximum_printed] = type::int_number(phasertng_pose_scoring_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_put_solution_use_tncs[] = ".phasertng.put_solution.use_tncs.";
    value__boolean[phasertng_put_solution_use_tncs] = type::boolean(phasertng_put_solution_use_tncs,false);
    constexpr char phasertng_put_solution_clear[] = ".phasertng.put_solution.clear.";
    value__boolean[phasertng_put_solution_clear] = type::boolean(phasertng_put_solution_clear,true);
    constexpr char phasertng_put_solution_dag_filename[] = ".phasertng.put_solution.dag.filename.";
    value__path[phasertng_put_solution_dag_filename] = type::path(phasertng_put_solution_dag_filename);
    constexpr char phasertng_put_solution_pose_euler[] = ".phasertng.put_solution.pose.euler.";
    value__flt_vector[phasertng_put_solution_pose_euler] = type::flt_vector(phasertng_put_solution_pose_euler,false,0,false,0,{0,0,0});
    constexpr char phasertng_put_solution_pose_fractional[] = ".phasertng.put_solution.pose.fractional.";
    value__flt_vector[phasertng_put_solution_pose_fractional] = type::flt_vector(phasertng_put_solution_pose_fractional,false,0,false,0,{0,0,0});
    constexpr char phasertng_put_solution_pose_orthogonal_rotation[] = ".phasertng.put_solution.pose.orthogonal_rotation.";
    value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation] = type::flt_vector(phasertng_put_solution_pose_orthogonal_rotation,false,0,false,0,{0,0,0});
    constexpr char phasertng_put_solution_pose_orthogonal_position[] = ".phasertng.put_solution.pose.orthogonal_position.";
    value__flt_vector[phasertng_put_solution_pose_orthogonal_position] = type::flt_vector(phasertng_put_solution_pose_orthogonal_position,false,0,false,0,{0,0,0});
    constexpr char phasertng_put_solution_pose_bfactor[] = ".phasertng.put_solution.pose.bfactor.";
    value__flt_number[phasertng_put_solution_pose_bfactor] = type::flt_number(phasertng_put_solution_pose_bfactor,true,-999,true,999,0);
    constexpr char phasertng_refinement_method[] = ".phasertng.refinement.method.";
    value__choice[phasertng_refinement_method] = type::choice(phasertng_refinement_method,"phenix refmac ","phenix");
    constexpr char phasertng_refinement_resolution[] = ".phasertng.refinement.resolution.";
    value__flt_number[phasertng_refinement_resolution] = type::flt_number(phasertng_refinement_resolution,true,0,false,0,2.8);
    constexpr char phasertng_refinement_top_files[] = ".phasertng.refinement.top_files.";
    value__int_number[phasertng_refinement_top_files] = type::int_number(phasertng_refinement_top_files,true,0,false,0,1);
    constexpr char phasertng_refinement_ncyc_refmac[] = ".phasertng.refinement.ncyc.refmac.";
    value__int_number[phasertng_refinement_ncyc_refmac] = type::int_number(phasertng_refinement_ncyc_refmac,true,1,false,0,10);
    constexpr char phasertng_refinement_ncyc_phenix[] = ".phasertng.refinement.ncyc.phenix.";
    value__int_number[phasertng_refinement_ncyc_phenix] = type::int_number(phasertng_refinement_ncyc_phenix,true,1,false,0,5);
    constexpr char phasertng_refinement_significant_twin_fraction[] = ".phasertng.refinement.significant_twin_fraction.";
    value__flt_number[phasertng_refinement_significant_twin_fraction] = type::flt_number(phasertng_refinement_significant_twin_fraction,true,0,true,0.5,0.4);
    constexpr char phasertng_refinement_treat_as_twinned[] = ".phasertng.refinement.treat_as_twinned.";
    value__boolean[phasertng_refinement_treat_as_twinned] = type::boolean(phasertng_refinement_treat_as_twinned);
    constexpr char phasertng_refinement_rigid_body[] = ".phasertng.refinement.rigid_body.";
    value__boolean[phasertng_refinement_rigid_body] = type::boolean(phasertng_refinement_rigid_body,false);
    constexpr char phasertng_refinement_tncs_correction[] = ".phasertng.refinement.tncs_correction.";
    value__boolean[phasertng_refinement_tncs_correction] = type::boolean(phasertng_refinement_tncs_correction,false);
    constexpr char phasertng_reflections_read_anomalous[] = ".phasertng.reflections.read_anomalous.";
    value__boolean[phasertng_reflections_read_anomalous] = type::boolean(phasertng_reflections_read_anomalous,false);
    constexpr char phasertng_reflections_resolution[] = ".phasertng.reflections.resolution.";
    value__flt_number[phasertng_reflections_resolution] = type::flt_number(phasertng_reflections_resolution,true,0,true,3,0);
    constexpr char phasertng_reflections_wavelength[] = ".phasertng.reflections.wavelength.";
    value__flt_number[phasertng_reflections_wavelength] = type::flt_number(phasertng_reflections_wavelength,true,0,false,0);
    constexpr char phasertng_reflections_evalues[] = ".phasertng.reflections.evalues.";
    value__boolean[phasertng_reflections_evalues] = type::boolean(phasertng_reflections_evalues,false);
    constexpr char phasertng_reflections_read_map[] = ".phasertng.reflections.read_map.";
    value__boolean[phasertng_reflections_read_map] = type::boolean(phasertng_reflections_read_map,false);
    constexpr char phasertng_reflections_oversampling[] = ".phasertng.reflections.oversampling.";
    value__flt_number[phasertng_reflections_oversampling] = type::flt_number(phasertng_reflections_oversampling,true,0,false,0,1);
    constexpr char phasertng_reflections_map_origin[] = ".phasertng.reflections.map_origin.";
    value__flt_vector[phasertng_reflections_map_origin] = type::flt_vector(phasertng_reflections_map_origin,false,0,false,0,{0,0,0});
    constexpr char phasertng_reflid_tag[] = ".phasertng.reflid.tag.";
    value__string[phasertng_reflid_tag] = type::string(phasertng_reflid_tag);
    constexpr char phasertng_reflid_id[] = ".phasertng.reflid.id.";
    value__uuid_number[phasertng_reflid_id] = type::uuid_number(phasertng_reflid_id,true,0,false,0);
    constexpr char phasertng_reflid_force_read[] = ".phasertng.reflid.force_read.";
    value__boolean[phasertng_reflid_force_read] = type::boolean(phasertng_reflid_force_read,false);
    constexpr char phasertng_rescore_rotation_function_frf_percent_increase[] = ".phasertng.rescore_rotation_function.frf_percent_increase.";
    value__percent[phasertng_rescore_rotation_function_frf_percent_increase] = type::percent(phasertng_rescore_rotation_function_frf_percent_increase,5.);
    constexpr char phasertng_rescore_rotation_function_maximum_stored[] = ".phasertng.rescore_rotation_function.maximum_stored.";
    value__int_number[phasertng_rescore_rotation_function_maximum_stored] = type::int_number(phasertng_rescore_rotation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_rescore_rotation_function_maximum_printed[] = ".phasertng.rescore_rotation_function.maximum_printed.";
    value__int_number[phasertng_rescore_rotation_function_maximum_printed] = type::int_number(phasertng_rescore_rotation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_rescore_rotation_function_cluster[] = ".phasertng.rescore_rotation_function.cluster.";
    value__boolean[phasertng_rescore_rotation_function_cluster] = type::boolean(phasertng_rescore_rotation_function_cluster,true);
    constexpr char phasertng_rescore_rotation_function_cluster_back[] = ".phasertng.rescore_rotation_function.cluster_back.";
    value__int_number[phasertng_rescore_rotation_function_cluster_back] = type::int_number(phasertng_rescore_rotation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_rescore_rotation_function_nrand[] = ".phasertng.rescore_rotation_function.nrand.";
    value__int_number[phasertng_rescore_rotation_function_nrand] = type::int_number(phasertng_rescore_rotation_function_nrand,true,500,true,4000,500);
    constexpr char phasertng_rescore_rotation_function_calculate_zscore[] = ".phasertng.rescore_rotation_function.calculate_zscore.";
    value__boolean[phasertng_rescore_rotation_function_calculate_zscore] = type::boolean(phasertng_rescore_rotation_function_calculate_zscore,false);
    constexpr char phasertng_rescore_translation_function_cluster[] = ".phasertng.rescore_translation_function.cluster.";
    value__boolean[phasertng_rescore_translation_function_cluster] = type::boolean(phasertng_rescore_translation_function_cluster,true);
    constexpr char phasertng_rescore_translation_function_cluster_back[] = ".phasertng.rescore_translation_function.cluster_back.";
    value__int_number[phasertng_rescore_translation_function_cluster_back] = type::int_number(phasertng_rescore_translation_function_cluster_back,true,1,false,0,2000);
    constexpr char phasertng_rescore_translation_function_percent[] = ".phasertng.rescore_translation_function.percent.";
    value__percent[phasertng_rescore_translation_function_percent] = type::percent(phasertng_rescore_translation_function_percent,75);
    constexpr char phasertng_rescore_translation_function_calculate_zscore[] = ".phasertng.rescore_translation_function.calculate_zscore.";
    value__boolean[phasertng_rescore_translation_function_calculate_zscore] = type::boolean(phasertng_rescore_translation_function_calculate_zscore,false);
    constexpr char phasertng_rescore_translation_function_nrand[] = ".phasertng.rescore_translation_function.nrand.";
    value__int_number[phasertng_rescore_translation_function_nrand] = type::int_number(phasertng_rescore_translation_function_nrand,true,0,true,4000,500);
    constexpr char phasertng_rescore_translation_function_purge_tncs_duplicates[] = ".phasertng.rescore_translation_function.purge_tncs_duplicates.";
    value__boolean[phasertng_rescore_translation_function_purge_tncs_duplicates] = type::boolean(phasertng_rescore_translation_function_purge_tncs_duplicates,false);
    constexpr char phasertng_rescore_translation_function_maximum_stored[] = ".phasertng.rescore_translation_function.maximum_stored.";
    value__int_number[phasertng_rescore_translation_function_maximum_stored] = type::int_number(phasertng_rescore_translation_function_maximum_stored,true,0,false,0,10000);
    constexpr char phasertng_rescore_translation_function_maximum_printed[] = ".phasertng.rescore_translation_function.maximum_printed.";
    value__int_number[phasertng_rescore_translation_function_maximum_printed] = type::int_number(phasertng_rescore_translation_function_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_resolution[] = ".phasertng.resolution.";
    value__flt_number[phasertng_resolution] = type::flt_number(phasertng_resolution,true,0,false,0,0);
    constexpr char phasertng_rigid_body_maps_keep_chains[] = ".phasertng.rigid_body_maps.keep_chains.";
    value__boolean[phasertng_rigid_body_maps_keep_chains] = type::boolean(phasertng_rigid_body_maps_keep_chains,false);
    constexpr char phasertng_rigid_body_maps_maximum_stored[] = ".phasertng.rigid_body_maps.maximum_stored.";
    value__int_number[phasertng_rigid_body_maps_maximum_stored] = type::int_number(phasertng_rigid_body_maps_maximum_stored,true,0,false,0,5);
    constexpr char phasertng_sampling_generator_change_of_basis_op[] = ".phasertng.sampling_generator.change_of_basis_op.";
    value__string[phasertng_sampling_generator_change_of_basis_op] = type::string(phasertng_sampling_generator_change_of_basis_op);
    constexpr char phasertng_sampling_generator_continuous_shifts[] = ".phasertng.sampling_generator.continuous_shifts.";
    value__int_vector[phasertng_sampling_generator_continuous_shifts] = type::int_vector(phasertng_sampling_generator_continuous_shifts,false,0,false,0);
    constexpr char phasertng_sampling_generator_cuts_normals[] = ".phasertng.sampling_generator.cuts_normals.";
    value__flt_numbers[phasertng_sampling_generator_cuts_normals] = type::flt_numbers(phasertng_sampling_generator_cuts_normals,false,0,false,0);
    constexpr char phasertng_sampling_generator_cuts_constants[] = ".phasertng.sampling_generator.cuts_constants.";
    value__flt_numbers[phasertng_sampling_generator_cuts_constants] = type::flt_numbers(phasertng_sampling_generator_cuts_constants,false,0,false,0);
    constexpr char phasertng_scattering_fluorescence_scan_scatterer[] = ".phasertng.scattering.fluorescence_scan.scatterer.";
    array__string[phasertng_scattering_fluorescence_scan_scatterer] = std::vector<type::string>();
    constexpr char phasertng_scattering_fluorescence_scan_fp[] = ".phasertng.scattering.fluorescence_scan.fp.";
    array__flt_number[phasertng_scattering_fluorescence_scan_fp] = std::vector<type::flt_number>();
    constexpr char phasertng_scattering_fluorescence_scan_fdp[] = ".phasertng.scattering.fluorescence_scan.fdp.";
    array__flt_number[phasertng_scattering_fluorescence_scan_fdp] = std::vector<type::flt_number>();
    constexpr char phasertng_scattering_fluorescence_scan_fix_fdp[] = ".phasertng.scattering.fluorescence_scan.fix_fdp.";
    array__choice[phasertng_scattering_fluorescence_scan_fix_fdp] = std::vector<type::choice>();
    constexpr char phasertng_scattering_sasaki_table_scatterer[] = ".phasertng.scattering.sasaki_table.scatterer.";
    array__string[phasertng_scattering_sasaki_table_scatterer] = std::vector<type::string>();
    constexpr char phasertng_scattering_sasaki_table_move_to_edge[] = ".phasertng.scattering.sasaki_table.move_to_edge.";
    array__boolean[phasertng_scattering_sasaki_table_move_to_edge] = std::vector<type::boolean>();
    constexpr char phasertng_scattering_form_factors[] = ".phasertng.scattering.form_factors.";
    value__choice[phasertng_scattering_form_factors] = type::choice(phasertng_scattering_form_factors,"xray electron neutron ","xray");
    constexpr char phasertng_search_id[] = ".phasertng.search.id.";
    value__uuid_number[phasertng_search_id] = type::uuid_number(phasertng_search_id,true,0,false,0);
    constexpr char phasertng_search_tag[] = ".phasertng.search.tag.";
    value__string[phasertng_search_tag] = type::string(phasertng_search_tag);
    constexpr char phasertng_search_combination[] = ".phasertng.search_combination.";
    array__strings[phasertng_search_combination] = std::vector<type::strings>();
    constexpr char phasertng_segmented_pruning_segment_delta_ellg[] = ".phasertng.segmented_pruning.segment.delta_ellg.";
    value__flt_number[phasertng_segmented_pruning_segment_delta_ellg] = type::flt_number(phasertng_segmented_pruning_segment_delta_ellg,true,5,false,0,5);
    constexpr char phasertng_segmented_pruning_segment_rmsd[] = ".phasertng.segmented_pruning.segment.rmsd.";
    value__flt_number[phasertng_segmented_pruning_segment_rmsd] = type::flt_number(phasertng_segmented_pruning_segment_rmsd,false,0,false,0,1);
    constexpr char phasertng_segmented_pruning_segment_residues[] = ".phasertng.segmented_pruning.segment.residues.";
    value__int_number[phasertng_segmented_pruning_segment_residues] = type::int_number(phasertng_segmented_pruning_segment_residues,true,1,false,0);
    constexpr char phasertng_segmented_pruning_pruning_low_resolution[] = ".phasertng.segmented_pruning.pruning.low_resolution.";
    value__flt_number[phasertng_segmented_pruning_pruning_low_resolution] = type::flt_number(phasertng_segmented_pruning_pruning_low_resolution,true,0,false,0,10000);
    constexpr char phasertng_segmented_pruning_pruning_maximum_percent[] = ".phasertng.segmented_pruning.pruning.maximum_percent.";
    value__percent[phasertng_segmented_pruning_pruning_maximum_percent] = type::percent(phasertng_segmented_pruning_pruning_maximum_percent,20);
    constexpr char phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent[] = ".phasertng.segmented_pruning.pruning.llg_bias_from_maximum_percent.";
    value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent] = type::flt_number(phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent,true,0,false,0,10);
    constexpr char phasertng_segmented_pruning_maximum_stored[] = ".phasertng.segmented_pruning.maximum_stored.";
    value__int_number[phasertng_segmented_pruning_maximum_stored] = type::int_number(phasertng_segmented_pruning_maximum_stored,true,0,false,0,5);
    constexpr char phasertng_self_rotation_function_integration_radius[] = ".phasertng.self_rotation_function.integration_radius.";
    value__flt_number[phasertng_self_rotation_function_integration_radius] = type::flt_number(phasertng_self_rotation_function_integration_radius,true,3,false,0,20);
    constexpr char phasertng_self_rotation_function_sequence_filename[] = ".phasertng.self_rotation_function.sequence.filename.";
    value__path[phasertng_self_rotation_function_sequence_filename] = type::path(phasertng_self_rotation_function_sequence_filename);
    constexpr char phasertng_self_rotation_function_sequence_integration_radius_factor[] = ".phasertng.self_rotation_function.sequence.integration_radius_factor.";
    value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor] = type::flt_number(phasertng_self_rotation_function_sequence_integration_radius_factor,true,0.1,true,2,2.);
    constexpr char phasertng_self_rotation_function_model_filename[] = ".phasertng.self_rotation_function.model.filename.";
    value__path[phasertng_self_rotation_function_model_filename] = type::path(phasertng_self_rotation_function_model_filename);
    constexpr char phasertng_self_rotation_function_model_integration_radius_factor[] = ".phasertng.self_rotation_function.model.integration_radius_factor.";
    value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor] = type::flt_number(phasertng_self_rotation_function_model_integration_radius_factor,true,0.1,true,2,2.);
    constexpr char phasertng_self_rotation_function_percent[] = ".phasertng.self_rotation_function.percent.";
    value__percent[phasertng_self_rotation_function_percent] = type::percent(phasertng_self_rotation_function_percent,20.);
    constexpr char phasertng_self_rotation_function_resolution[] = ".phasertng.self_rotation_function.resolution.";
    value__flt_number[phasertng_self_rotation_function_resolution] = type::flt_number(phasertng_self_rotation_function_resolution,true,0,false,0,3.0);
    constexpr char phasertng_self_rotation_function_plot_chi[] = ".phasertng.self_rotation_function.plot_chi.";
    value__int_number[phasertng_self_rotation_function_plot_chi] = type::int_number(phasertng_self_rotation_function_plot_chi,true,1,true,180,15);
    constexpr char phasertng_self_rotation_function_maximum_stored[] = ".phasertng.self_rotation_function.maximum_stored.";
    value__int_number[phasertng_self_rotation_function_maximum_stored] = type::int_number(phasertng_self_rotation_function_maximum_stored,true,0,false,0,1000);
    constexpr char phasertng_self_rotation_function_maximum_printed[] = ".phasertng.self_rotation_function.maximum_printed.";
    value__int_number[phasertng_self_rotation_function_maximum_printed] = type::int_number(phasertng_self_rotation_function_maximum_printed,true,1,false,0,50);
    constexpr char phasertng_self_rotation_function_maximum_order[] = ".phasertng.self_rotation_function.maximum_order.";
    value__flt_number[phasertng_self_rotation_function_maximum_order] = type::flt_number(phasertng_self_rotation_function_maximum_order,true,1,false,0,12);
    constexpr char phasertng_seqout_filename[] = ".phasertng.seqout.filename.";
    value__path[phasertng_seqout_filename] = type::path(phasertng_seqout_filename);
    constexpr char phasertng_sgalt_selection[] = ".phasertng.sgalt.selection.";
    value__choice[phasertng_sgalt_selection] = type::choice(phasertng_sgalt_selection,"all hand list original ","all");
    constexpr char phasertng_sgalt_spacegroups[] = ".phasertng.sgalt.spacegroups.";
    value__strings[phasertng_sgalt_spacegroups] = type::strings(phasertng_sgalt_spacegroups);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_fsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model.fsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_fsol,false,0,false,0,1.05);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_bsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model.bsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_bsol,false,0,false,0,501.605);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum,true,0.01,false,0,0.1);
    constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol,false,0,false,0,0.9);
    constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol,false,0,false,0,501.605);
    constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum] = type::flt_number(phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum,true,0.01,false,0,0.1);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.fsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol,false,0,false,0,0.30);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.bsol.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol,false,0,false,0,501.605);
    constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.babinet_minimum.";
    value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum] = type::flt_number(phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum,true,0.01,false,0,0.1);
    constexpr char phasertng_space_group_expansion_hall[] = ".phasertng.space_group_expansion.hall.";
    value__string[phasertng_space_group_expansion_hall] = type::string(phasertng_space_group_expansion_hall);
    constexpr char phasertng_space_group_expansion_spacegroup[] = ".phasertng.space_group_expansion.spacegroup.";
    value__string[phasertng_space_group_expansion_spacegroup] = type::string(phasertng_space_group_expansion_spacegroup);
    constexpr char phasertng_space_group_expansion_hermann_mauguin_symbol[] = ".phasertng.space_group_expansion.hermann_mauguin_symbol.";
    value__string[phasertng_space_group_expansion_hermann_mauguin_symbol] = type::string(phasertng_space_group_expansion_hermann_mauguin_symbol);
    constexpr char phasertng_space_group_expansion_order_z_expansion_ratio[] = ".phasertng.space_group_expansion.order_z_expansion_ratio.";
    value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio] = type::int_number(phasertng_space_group_expansion_order_z_expansion_ratio,true,1,false,0);
    constexpr char phasertng_spanning_maximum_stored[] = ".phasertng.spanning.maximum_stored.";
    value__int_number[phasertng_spanning_maximum_stored] = type::int_number(phasertng_spanning_maximum_stored,true,0,false,0,1);
    constexpr char phasertng_spanning_maximum_printed[] = ".phasertng.spanning.maximum_printed.";
    value__int_number[phasertng_spanning_maximum_printed] = type::int_number(phasertng_spanning_maximum_printed,true,1,false,0,1);
    constexpr char phasertng_spanning_distance[] = ".phasertng.spanning.distance.";
    value__flt_number[phasertng_spanning_distance] = type::flt_number(phasertng_spanning_distance,true,1,false,0,10);
    constexpr char phasertng_spanning_find_gaps[] = ".phasertng.spanning.find_gaps.";
    value__boolean[phasertng_spanning_find_gaps] = type::boolean(phasertng_spanning_find_gaps,false);
    constexpr char phasertng_subgroup_expansion_hall[] = ".phasertng.subgroup.expansion.hall.";
    array__string[phasertng_subgroup_expansion_hall] = std::vector<type::string>();
    constexpr char phasertng_subgroup_expansion_spacegroup[] = ".phasertng.subgroup.expansion.spacegroup.";
    array__string[phasertng_subgroup_expansion_spacegroup] = std::vector<type::string>();
    constexpr char phasertng_subgroup_expansion_hermann_mauguin_symbol[] = ".phasertng.subgroup.expansion.hermann_mauguin_symbol.";
    array__string[phasertng_subgroup_expansion_hermann_mauguin_symbol] = std::vector<type::string>();
    constexpr char phasertng_subgroup_original_hall[] = ".phasertng.subgroup.original.hall.";
    value__string[phasertng_subgroup_original_hall] = type::string(phasertng_subgroup_original_hall);
    constexpr char phasertng_subgroup_original_spacegroup[] = ".phasertng.subgroup.original.spacegroup.";
    value__string[phasertng_subgroup_original_spacegroup] = type::string(phasertng_subgroup_original_spacegroup);
    constexpr char phasertng_subgroup_original_hermann_mauguin_symbol[] = ".phasertng.subgroup.original.hermann_mauguin_symbol.";
    value__string[phasertng_subgroup_original_hermann_mauguin_symbol] = type::string(phasertng_subgroup_original_hermann_mauguin_symbol);
    constexpr char phasertng_subgroup_use_all_sysabs[] = ".phasertng.subgroup.use_all_sysabs.";
    value__boolean[phasertng_subgroup_use_all_sysabs] = type::boolean(phasertng_subgroup_use_all_sysabs,false);
    constexpr char phasertng_subgroup_use_no_sysabs[] = ".phasertng.subgroup.use_no_sysabs.";
    value__boolean[phasertng_subgroup_use_no_sysabs] = type::boolean(phasertng_subgroup_use_no_sysabs,false);
    constexpr char phasertng_substructure_scatterer[] = ".phasertng.substructure.scatterer.";
    value__string[phasertng_substructure_scatterer] = type::string(phasertng_substructure_scatterer);
    constexpr char phasertng_substructure_content_analysis_number[] = ".phasertng.substructure.content_analysis.number.";
    value__flt_number[phasertng_substructure_content_analysis_number] = type::flt_number(phasertng_substructure_content_analysis_number,true,0,false,0,1);
    constexpr char phasertng_substructure_content_analysis_delta_bfactor[] = ".phasertng.substructure.content_analysis.delta_bfactor.";
    value__flt_number[phasertng_substructure_content_analysis_delta_bfactor] = type::flt_number(phasertng_substructure_content_analysis_delta_bfactor,false,0,false,0,0);
    constexpr char phasertng_substructure_content_analysis_signal[] = ".phasertng.substructure.content_analysis.signal.";
    value__boolean[phasertng_substructure_content_analysis_signal] = type::boolean(phasertng_substructure_content_analysis_signal,false);
    constexpr char phasertng_substructure_patterson_analysis_target[] = ".phasertng.substructure.patterson_analysis.target.";
    value__choice[phasertng_substructure_patterson_analysis_target] = type::choice(phasertng_substructure_patterson_analysis_target,"anomdiff likelihood expected ","expected");
    constexpr char phasertng_substructure_patterson_analysis_percent[] = ".phasertng.substructure.patterson_analysis.percent.";
    value__percent[phasertng_substructure_patterson_analysis_percent] = type::percent(phasertng_substructure_patterson_analysis_percent,20);
    constexpr char phasertng_substructure_patterson_analysis_maximum_stored[] = ".phasertng.substructure.patterson_analysis.maximum_stored.";
    value__int_number[phasertng_substructure_patterson_analysis_maximum_stored] = type::int_number(phasertng_substructure_patterson_analysis_maximum_stored,true,0,false,0,3);
    constexpr char phasertng_substructure_patterson_analysis_shelxc[] = ".phasertng.substructure.patterson_analysis.shelxc.";
    value__path[phasertng_substructure_patterson_analysis_shelxc] = type::path(phasertng_substructure_patterson_analysis_shelxc);
    constexpr char phasertng_substructure_patterson_analysis_constellation[] = ".phasertng.substructure.patterson_analysis.constellation.";
    value__boolean[phasertng_substructure_patterson_analysis_constellation] = type::boolean(phasertng_substructure_patterson_analysis_constellation,false);
    constexpr char phasertng_substructure_patterson_analysis_minimum_number_of_reflections[] = ".phasertng.substructure.patterson_analysis.minimum_number_of_reflections.";
    value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections] = type::int_number(phasertng_substructure_patterson_analysis_minimum_number_of_reflections,true,10,false,0,50);
    constexpr char phasertng_substructure_patterson_analysis_minimum_percent[] = ".phasertng.substructure.patterson_analysis.minimum_percent.";
    value__percent[phasertng_substructure_patterson_analysis_minimum_percent] = type::percent(phasertng_substructure_patterson_analysis_minimum_percent,10);
    constexpr char phasertng_substructure_patterson_analysis_insufficient_data[] = ".phasertng.substructure.patterson_analysis.insufficient_data.";
    value__boolean[phasertng_substructure_patterson_analysis_insufficient_data] = type::boolean(phasertng_substructure_patterson_analysis_insufficient_data,false);
    constexpr char phasertng_substructure_patterson_analysis_minimum_resolution[] = ".phasertng.substructure.patterson_analysis.minimum_resolution.";
    value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution] = type::flt_number(phasertng_substructure_patterson_analysis_minimum_resolution,true,0,false,0,10);
    constexpr char phasertng_substructure_patterson_analysis_insufficient_resolution[] = ".phasertng.substructure.patterson_analysis.insufficient_resolution.";
    value__boolean[phasertng_substructure_patterson_analysis_insufficient_resolution] = type::boolean(phasertng_substructure_patterson_analysis_insufficient_resolution,false);
    constexpr char phasertng_substructure_constellation_size[] = ".phasertng.substructure.constellation.size.";
    value__int_number[phasertng_substructure_constellation_size] = type::int_number(phasertng_substructure_constellation_size,false,0,false,0,3);
    constexpr char phasertng_substructure_constellation_number_of_shifts[] = ".phasertng.substructure.constellation.number_of_shifts.";
    value__int_number[phasertng_substructure_constellation_number_of_shifts] = type::int_number(phasertng_substructure_constellation_number_of_shifts,false,0,false,0,1);
    constexpr char phasertng_substructure_constellation_distance[] = ".phasertng.substructure.constellation.distance.";
    value__flt_number[phasertng_substructure_constellation_distance] = type::flt_number(phasertng_substructure_constellation_distance,false,0,false,0,8.0);
    constexpr char phasertng_substructure_preparation_scatterer[] = ".phasertng.substructure.preparation.scatterer.";
    value__string[phasertng_substructure_preparation_scatterer] = type::string(phasertng_substructure_preparation_scatterer);
    constexpr char phasertng_substructure_preparation_wilson_bfactor[] = ".phasertng.substructure.preparation.wilson_bfactor.";
    value__boolean[phasertng_substructure_preparation_wilson_bfactor] = type::boolean(phasertng_substructure_preparation_wilson_bfactor,true);
    constexpr char phasertng_substructure_determination_scatterer[] = ".phasertng.substructure.determination.scatterer.";
    value__string[phasertng_substructure_determination_scatterer] = type::string(phasertng_substructure_determination_scatterer);
    constexpr char phasertng_substructure_determination_occupancy[] = ".phasertng.substructure.determination.occupancy.";
    value__flt_number[phasertng_substructure_determination_occupancy] = type::flt_number(phasertng_substructure_determination_occupancy,true,0.1,true,10,0.9);
    constexpr char phasertng_substructure_determination_bfactor[] = ".phasertng.substructure.determination.bfactor.";
    value__flt_number[phasertng_substructure_determination_bfactor] = type::flt_number(phasertng_substructure_determination_bfactor,false,0,false,0,0.0);
    constexpr char phasertng_substructure_determination_minimum_number[] = ".phasertng.substructure.determination.minimum_number.";
    value__int_number[phasertng_substructure_determination_minimum_number] = type::int_number(phasertng_substructure_determination_minimum_number,true,1,true,10000,1);
    constexpr char phasertng_substructure_determination_find[] = ".phasertng.substructure.determination.find.";
    value__int_number[phasertng_substructure_determination_find] = type::int_number(phasertng_substructure_determination_find,true,1,false,0,1);
    constexpr char phasertng_substructure_determination_maximum_find[] = ".phasertng.substructure.determination.maximum_find.";
    value__int_number[phasertng_substructure_determination_maximum_find] = type::int_number(phasertng_substructure_determination_maximum_find,true,1,false,0,4);
    constexpr char phasertng_substructure_determination_percent[] = ".phasertng.substructure.determination.percent.";
    value__percent[phasertng_substructure_determination_percent] = type::percent(phasertng_substructure_determination_percent,75);
    constexpr char phasertng_substructure_determination_zscore[] = ".phasertng.substructure.determination.zscore.";
    value__flt_number[phasertng_substructure_determination_zscore] = type::flt_number(phasertng_substructure_determination_zscore,true,2,false,0,3);
    constexpr char phasertng_substructure_determination_target_fom[] = ".phasertng.substructure.determination.target_fom.";
    value__flt_number[phasertng_substructure_determination_target_fom] = type::flt_number(phasertng_substructure_determination_target_fom,true,0,true,1,1.0);
    constexpr char phasertng_substructure_determination_peaks_over_deepest_hole[] = ".phasertng.substructure.determination.peaks_over_deepest_hole.";
    value__boolean[phasertng_substructure_determination_peaks_over_deepest_hole] = type::boolean(phasertng_substructure_determination_peaks_over_deepest_hole,true);
    constexpr char phasertng_substructure_determination_maximum_stored[] = ".phasertng.substructure.determination.maximum_stored.";
    value__int_number[phasertng_substructure_determination_maximum_stored] = type::int_number(phasertng_substructure_determination_maximum_stored,true,0,false,0,10);
    constexpr char phasertng_substructure_completion_complete[] = ".phasertng.substructure.completion.complete.";
    value__boolean[phasertng_substructure_completion_complete] = type::boolean(phasertng_substructure_completion_complete,true);
    constexpr char phasertng_substructure_completion_ncyc[] = ".phasertng.substructure.completion.ncyc.";
    value__int_number[phasertng_substructure_completion_ncyc] = type::int_number(phasertng_substructure_completion_ncyc,false,0,false,0,50);
    constexpr char phasertng_substructure_completion_scatterer[] = ".phasertng.substructure.completion.scatterer.";
    value__strings[phasertng_substructure_completion_scatterer] = type::strings(phasertng_substructure_completion_scatterer);
    constexpr char phasertng_substructure_completion_separation_distance[] = ".phasertng.substructure.completion.separation_distance.";
    value__flt_number[phasertng_substructure_completion_separation_distance] = type::flt_number(phasertng_substructure_completion_separation_distance,true,0.1,true,10);
    constexpr char phasertng_substructure_completion_zscore[] = ".phasertng.substructure.completion.zscore.";
    value__flt_number[phasertng_substructure_completion_zscore] = type::flt_number(phasertng_substructure_completion_zscore,true,1,false,0,5.5);
    constexpr char phasertng_substructure_completion_peaks_above_deepest_hole[] = ".phasertng.substructure.completion.peaks_above_deepest_hole.";
    value__boolean[phasertng_substructure_completion_peaks_above_deepest_hole] = type::boolean(phasertng_substructure_completion_peaks_above_deepest_hole,true);
    constexpr char phasertng_substructure_completion_top_peak_below_deepest_hole[] = ".phasertng.substructure.completion.top_peak_below_deepest_hole.";
    value__boolean[phasertng_substructure_completion_top_peak_below_deepest_hole] = type::boolean(phasertng_substructure_completion_top_peak_below_deepest_hole,true);
    constexpr char phasertng_substructure_completion_top_peak_below_deepest_hole_zscore[] = ".phasertng.substructure.completion.top_peak_below_deepest_hole_zscore.";
    value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore] = type::flt_number(phasertng_substructure_completion_top_peak_below_deepest_hole_zscore,true,1,false,0,6.0);
    constexpr char phasertng_substructure_completion_change_scatterers[] = ".phasertng.substructure.completion.change_scatterers.";
    value__boolean[phasertng_substructure_completion_change_scatterers] = type::boolean(phasertng_substructure_completion_change_scatterers,true);
    constexpr char phasertng_substructure_completion_swap_to_anisotropic_scatterers[] = ".phasertng.substructure.completion.swap_to_anisotropic_scatterers.";
    value__boolean[phasertng_substructure_completion_swap_to_anisotropic_scatterers] = type::boolean(phasertng_substructure_completion_swap_to_anisotropic_scatterers,true);
    constexpr char phasertng_substructure_completion_rfactor_termination[] = ".phasertng.substructure.completion.rfactor_termination.";
    value__percent[phasertng_substructure_completion_rfactor_termination] = type::percent(phasertng_substructure_completion_rfactor_termination,30);
    constexpr char phasertng_substructure_completion_write_files[] = ".phasertng.substructure.completion.write_files.";
    value__boolean[phasertng_substructure_completion_write_files] = type::boolean(phasertng_substructure_completion_write_files,false);
    constexpr char phasertng_substructure_test_fft_versus_summation[] = ".phasertng.substructure.test_fft_versus_summation.";
    value__int_paired[phasertng_substructure_test_fft_versus_summation] = type::int_paired(phasertng_substructure_test_fft_versus_summation,false,0,false,0,{20,80});
    constexpr char phasertng_substructure_hyss_phil[] = ".phasertng.substructure.hyss.phil.";
    value__path[phasertng_substructure_hyss_phil] = type::path(phasertng_substructure_hyss_phil);
    constexpr char phasertng_substructure_hyss_command_line[] = ".phasertng.substructure.hyss.command_line.";
    value__strings[phasertng_substructure_hyss_command_line] = type::strings(phasertng_substructure_hyss_command_line);
    constexpr char phasertng_suite_level[] = ".phasertng.suite.level.";
    value__choice[phasertng_suite_level] = type::choice(phasertng_suite_level,"default silence process concise summary logfile verbose testing ","default");
    constexpr char phasertng_suite_store[] = ".phasertng.suite.store.";
    value__choice[phasertng_suite_store] = type::choice(phasertng_suite_store,"default silence process concise summary logfile verbose testing ","default");
    constexpr char phasertng_suite_write_phil[] = ".phasertng.suite.write_phil.";
    value__boolean[phasertng_suite_write_phil] = type::boolean(phasertng_suite_write_phil);
    constexpr char phasertng_suite_write_cards[] = ".phasertng.suite.write_cards.";
    value__boolean[phasertng_suite_write_cards] = type::boolean(phasertng_suite_write_cards);
    constexpr char phasertng_suite_write_files[] = ".phasertng.suite.write_files.";
    value__boolean[phasertng_suite_write_files] = type::boolean(phasertng_suite_write_files);
    constexpr char phasertng_suite_write_data[] = ".phasertng.suite.write_data.";
    value__boolean[phasertng_suite_write_data] = type::boolean(phasertng_suite_write_data);
    constexpr char phasertng_suite_write_density[] = ".phasertng.suite.write_density.";
    value__boolean[phasertng_suite_write_density] = type::boolean(phasertng_suite_write_density);
    constexpr char phasertng_suite_write_log[] = ".phasertng.suite.write_log.";
    value__boolean[phasertng_suite_write_log] = type::boolean(phasertng_suite_write_log);
    constexpr char phasertng_suite_write_xml[] = ".phasertng.suite.write_xml.";
    value__boolean[phasertng_suite_write_xml] = type::boolean(phasertng_suite_write_xml);
    constexpr char phasertng_suite_kill_file[] = ".phasertng.suite.kill_file.";
    value__path[phasertng_suite_kill_file] = type::path(phasertng_suite_kill_file);
    constexpr char phasertng_suite_kill_time[] = ".phasertng.suite.kill_time.";
    value__flt_number[phasertng_suite_kill_time] = type::flt_number(phasertng_suite_kill_time,true,0,false,0,10);
    constexpr char phasertng_suite_loggraphs[] = ".phasertng.suite.loggraphs.";
    value__boolean[phasertng_suite_loggraphs] = type::boolean(phasertng_suite_loggraphs);
    constexpr char phasertng_suite_database[] = ".phasertng.suite.database.";
    value__path[phasertng_suite_database] = type::path(phasertng_suite_database);
    constexpr char phasertng_threads_number[] = ".phasertng.threads.number.";
    value__int_number[phasertng_threads_number] = type::int_number(phasertng_threads_number,true,1,false,0,3);
    constexpr char phasertng_threads_strictly[] = ".phasertng.threads.strictly.";
    value__boolean[phasertng_threads_strictly] = type::boolean(phasertng_threads_strictly,false);
    constexpr char phasertng_time_mode_wall[] = ".phasertng.time.mode.wall.";
    value__flt_number[phasertng_time_mode_wall] = type::flt_number(phasertng_time_mode_wall,true,0,false,0,0);
    constexpr char phasertng_time_mode_cpu[] = ".phasertng.time.mode.cpu.";
    value__flt_number[phasertng_time_mode_cpu] = type::flt_number(phasertng_time_mode_cpu,true,0,false,0,0);
    constexpr char phasertng_time_cumulative_wall[] = ".phasertng.time.cumulative.wall.";
    value__flt_number[phasertng_time_cumulative_wall] = type::flt_number(phasertng_time_cumulative_wall,true,0,false,0,0);
    constexpr char phasertng_time_cumulative_cpu[] = ".phasertng.time.cumulative.cpu.";
    value__flt_number[phasertng_time_cumulative_cpu] = type::flt_number(phasertng_time_cumulative_cpu,true,0,false,0,0);
    constexpr char phasertng_title[] = ".phasertng.title.";
    value__string[phasertng_title] = type::string(phasertng_title);
    constexpr char phasertng_tncs_order[] = ".phasertng.tncs.order.";
    value__int_number[phasertng_tncs_order] = type::int_number(phasertng_tncs_order,false,0,false,0);
    constexpr char phasertng_tncs_vector[] = ".phasertng.tncs.vector.";
    value__flt_vector[phasertng_tncs_vector] = type::flt_vector(phasertng_tncs_vector,false,0,false,0);
    constexpr char phasertng_tncs_parity[] = ".phasertng.tncs.parity.";
    value__int_vector[phasertng_tncs_parity] = type::int_vector(phasertng_tncs_parity,false,0,false,0);
    constexpr char phasertng_tncs_unique[] = ".phasertng.tncs.unique.";
    value__boolean[phasertng_tncs_unique] = type::boolean(phasertng_tncs_unique);
    constexpr char phasertng_tncs_gfunction_radius[] = ".phasertng.tncs.gfunction_radius.";
    value__flt_number[phasertng_tncs_gfunction_radius] = type::flt_number(phasertng_tncs_gfunction_radius,true,10,false,0);
    constexpr char phasertng_tncs_rmsd[] = ".phasertng.tncs.rmsd.";
    value__flt_number[phasertng_tncs_rmsd] = type::flt_number(phasertng_tncs_rmsd,true,0.1,true,3,0.2);
    constexpr char phasertng_tncs_present_in_model[] = ".phasertng.tncs.present_in_model.";
    value__boolean[phasertng_tncs_present_in_model] = type::boolean(phasertng_tncs_present_in_model,false);
    constexpr char phasertng_tncs_refine_perturb_special_position[] = ".phasertng.tncs.refine.perturb_special_position.";
    value__boolean[phasertng_tncs_refine_perturb_special_position] = type::boolean(phasertng_tncs_refine_perturb_special_position,true);
    constexpr char phasertng_tncs_refine_rotation_sampling[] = ".phasertng.tncs.refine.rotation_sampling.";
    value__flt_number[phasertng_tncs_refine_rotation_sampling] = type::flt_number(phasertng_tncs_refine_rotation_sampling,false,0,false,0);
    constexpr char phasertng_tncs_refine_rotation_range[] = ".phasertng.tncs.refine.rotation_range.";
    value__flt_number[phasertng_tncs_refine_rotation_range] = type::flt_number(phasertng_tncs_refine_rotation_range,false,0,false,0);
    constexpr char phasertng_tncs_analysis_result_order[] = ".phasertng.tncs_analysis.result.order.";
    array__int_number[phasertng_tncs_analysis_result_order] = std::vector<type::int_number>();
    constexpr char phasertng_tncs_analysis_result_vector[] = ".phasertng.tncs_analysis.result.vector.";
    array__flt_vector[phasertng_tncs_analysis_result_vector] = std::vector<type::flt_vector>();
    constexpr char phasertng_tncs_analysis_result_parity[] = ".phasertng.tncs_analysis.result.parity.";
    array__int_vector[phasertng_tncs_analysis_result_parity] = std::vector<type::int_vector>();
    constexpr char phasertng_tncs_analysis_result_unique[] = ".phasertng.tncs_analysis.result.unique.";
    array__boolean[phasertng_tncs_analysis_result_unique] = std::vector<type::boolean>();
    constexpr char phasertng_tncs_analysis_number[] = ".phasertng.tncs_analysis.number.";
    value__int_number[phasertng_tncs_analysis_number] = type::int_number(phasertng_tncs_analysis_number,false,0,false,0);
    constexpr char phasertng_tncs_analysis_insufficient_data[] = ".phasertng.tncs_analysis.insufficient_data.";
    value__boolean[phasertng_tncs_analysis_insufficient_data] = type::boolean(phasertng_tncs_analysis_insufficient_data,false);
    constexpr char phasertng_tncs_analysis_insufficient_resolution[] = ".phasertng.tncs_analysis.insufficient_resolution.";
    value__boolean[phasertng_tncs_analysis_insufficient_resolution] = type::boolean(phasertng_tncs_analysis_insufficient_resolution,false);
    constexpr char phasertng_tncs_analysis_coiled_coil[] = ".phasertng.tncs_analysis.coiled_coil.";
    value__boolean[phasertng_tncs_analysis_coiled_coil] = type::boolean(phasertng_tncs_analysis_coiled_coil,false);
    constexpr char phasertng_tncs_analysis_indicated[] = ".phasertng.tncs_analysis.indicated.";
    value__boolean[phasertng_tncs_analysis_indicated] = type::boolean(phasertng_tncs_analysis_indicated);
    constexpr char phasertng_tncs_order_selection_by_rank[] = ".phasertng.tncs_order.selection_by_rank.";
    value__int_number[phasertng_tncs_order_selection_by_rank] = type::int_number(phasertng_tncs_order_selection_by_rank,true,1,false,0);
    constexpr char phasertng_tncs_order_include_no_tncs[] = ".phasertng.tncs_order.include_no_tncs.";
    value__boolean[phasertng_tncs_order_include_no_tncs] = type::boolean(phasertng_tncs_order_include_no_tncs,true);
    constexpr char phasertng_tncs_order_minimum_number_of_reflections[] = ".phasertng.tncs_order.minimum_number_of_reflections.";
    value__int_number[phasertng_tncs_order_minimum_number_of_reflections] = type::int_number(phasertng_tncs_order_minimum_number_of_reflections,true,3,false,0,50);
    constexpr char phasertng_tncs_order_minimum_resolution[] = ".phasertng.tncs_order.minimum_resolution.";
    value__flt_number[phasertng_tncs_order_minimum_resolution] = type::flt_number(phasertng_tncs_order_minimum_resolution,true,0,false,0,10);
    constexpr char phasertng_tncs_order_patterson_resolution[] = ".phasertng.tncs_order.patterson.resolution.";
    value__flt_paired[phasertng_tncs_order_patterson_resolution] = type::flt_paired(phasertng_tncs_order_patterson_resolution,true,0,false,0,{5.0,10.0});
    constexpr char phasertng_tncs_order_patterson_percent[] = ".phasertng.tncs_order.patterson.percent.";
    value__percent[phasertng_tncs_order_patterson_percent] = type::percent(phasertng_tncs_order_patterson_percent,20);
    constexpr char phasertng_tncs_order_patterson_origin_distance[] = ".phasertng.tncs_order.patterson.origin_distance.";
    value__flt_number[phasertng_tncs_order_patterson_origin_distance] = type::flt_number(phasertng_tncs_order_patterson_origin_distance,true,0,false,0,15);
    constexpr char phasertng_tncs_order_frequency_cutoff[] = ".phasertng.tncs_order.frequency_cutoff.";
    value__percent[phasertng_tncs_order_frequency_cutoff] = type::percent(phasertng_tncs_order_frequency_cutoff,50);
    constexpr char phasertng_trace_filename[] = ".phasertng.trace.filename.";
    value__path[phasertng_trace_filename] = type::path(phasertng_trace_filename);
    constexpr char phasertng_trace_volume_sampling[] = ".phasertng.trace.volume_sampling.";
    value__choice[phasertng_trace_volume_sampling] = type::choice(phasertng_trace_volume_sampling,"all calpha hexgrid optimal ","optimal");
    constexpr char phasertng_trace_trim_asa[] = ".phasertng.trace.trim_asa.";
    value__boolean[phasertng_trace_trim_asa] = type::boolean(phasertng_trace_trim_asa,true);
    constexpr char phasertng_trace_hexgrid_number_of_points[] = ".phasertng.trace.hexgrid.number_of_points.";
    value__int_paired[phasertng_trace_hexgrid_number_of_points] = type::int_paired(phasertng_trace_hexgrid_number_of_points,false,0,false,0,{900,1100});
    constexpr char phasertng_trace_hexgrid_ncyc[] = ".phasertng.trace.hexgrid.ncyc.";
    value__int_number[phasertng_trace_hexgrid_ncyc] = type::int_number(phasertng_trace_hexgrid_ncyc,true,1,false,0,10);
    constexpr char phasertng_trace_hexgrid_minimum_distance[] = ".phasertng.trace.hexgrid.minimum_distance.";
    value__flt_number[phasertng_trace_hexgrid_minimum_distance] = type::flt_number(phasertng_trace_hexgrid_minimum_distance,false,0,false,0,1.0);
    constexpr char phasertng_trace_hexgrid_distance[] = ".phasertng.trace.hexgrid.distance.";
    value__flt_number[phasertng_trace_hexgrid_distance] = type::flt_number(phasertng_trace_hexgrid_distance,false,0,false,0);
    constexpr char phasertng_twinning_test[] = ".phasertng.twinning.test.";
    value__choice[phasertng_twinning_test] = type::choice(phasertng_twinning_test,"moments ltest moments_or_ltest moments_and_ltest ","moments");
    constexpr char phasertng_twinning_padilla_yeates_test[] = ".phasertng.twinning.padilla_yeates_test.";
    value__flt_number[phasertng_twinning_padilla_yeates_test] = type::flt_number(phasertng_twinning_padilla_yeates_test,false,0,true,0.5,0.5);
    constexpr char phasertng_twinning_parity[] = ".phasertng.twinning.parity.";
    value__int_vector[phasertng_twinning_parity] = type::int_vector(phasertng_twinning_parity,true,2,false,0,{2,2,2});
    constexpr char phasertng_twinning_indicated[] = ".phasertng.twinning.indicated.";
    value__boolean[phasertng_twinning_indicated] = type::boolean(phasertng_twinning_indicated);
    constexpr char phasertng_zscore_equivalent_percent[] = ".phasertng.zscore_equivalent.percent.";
    value__percent[phasertng_zscore_equivalent_percent] = type::percent(phasertng_zscore_equivalent_percent,75);
    constexpr char phasertng_zscore_equivalent_nrand[] = ".phasertng.zscore_equivalent.nrand.";
    value__int_number[phasertng_zscore_equivalent_nrand] = type::int_number(phasertng_zscore_equivalent_nrand,true,100,true,4000,500);
    constexpr char phasertng_zscore_equivalent_range_zscore[] = ".phasertng.zscore_equivalent.range_zscore.";
    value__flt_paired[phasertng_zscore_equivalent_range_zscore] = type::flt_paired(phasertng_zscore_equivalent_range_zscore,true,0,true,99,{5,11});
    constexpr char phasertng_zscore_equivalent_range_percent[] = ".phasertng.zscore_equivalent.range_percent.";
    value__percent[phasertng_zscore_equivalent_range_percent] = type::percent(phasertng_zscore_equivalent_range_percent,90);
    constexpr char phasertng_zscore_equivalent_maximum_printed[] = ".phasertng.zscore_equivalent.maximum_printed.";
    value__int_number[phasertng_zscore_equivalent_maximum_printed] = type::int_number(phasertng_zscore_equivalent_maximum_printed,true,1,false,0,20);
    constexpr char phasertng_zscore_equivalent_map_percent[] = ".phasertng.zscore_equivalent_map.percent.";
    value__percent[phasertng_zscore_equivalent_map_percent] = type::percent(phasertng_zscore_equivalent_map_percent,75);
    constexpr char phasertng_zscore_equivalent_map_nrand[] = ".phasertng.zscore_equivalent_map.nrand.";
    value__int_number[phasertng_zscore_equivalent_map_nrand] = type::int_number(phasertng_zscore_equivalent_map_nrand,true,100,true,4000,500);
    constexpr char phasertng_zscore_equivalent_map_maximum_printed[] = ".phasertng.zscore_equivalent_map.maximum_printed.";
    value__int_number[phasertng_zscore_equivalent_map_maximum_printed] = type::int_number(phasertng_zscore_equivalent_map_maximum_printed,true,1,false,0,20);
  }
  void
  InputCard::add_linkKeys()
  {
    {
    constexpr char phasertng_atoms_fractional[] = ".phasertng.atoms.fractional.";
    constexpr char scatterer[] = "scatterer";
    linkKey(phasertng_atoms_fractional,scatterer);
    }
    {
    constexpr char phasertng_atoms_fractional[] = ".phasertng.atoms.fractional.";
    constexpr char site[] = "site";
    linkKey(phasertng_atoms_fractional,site);
    }
    {
    constexpr char phasertng_atoms_fractional[] = ".phasertng.atoms.fractional.";
    constexpr char occupancy[] = "occupancy";
    linkKey(phasertng_atoms_fractional,occupancy);
    }
    {
    constexpr char phasertng_fast_rotation_function_maps[] = ".phasertng.fast_rotation_function.maps.";
    constexpr char id[] = "id";
    linkKey(phasertng_fast_rotation_function_maps,id);
    }
    {
    constexpr char phasertng_fast_rotation_function_maps[] = ".phasertng.fast_rotation_function.maps.";
    constexpr char tag[] = "tag";
    linkKey(phasertng_fast_rotation_function_maps,tag);
    }
    {
    constexpr char phasertng_gyration[] = ".phasertng.gyration.";
    constexpr char id[] = "id";
    linkKey(phasertng_gyration,id);
    }
    {
    constexpr char phasertng_gyration[] = ".phasertng.gyration.";
    constexpr char tag[] = "tag";
    linkKey(phasertng_gyration,tag);
    }
    {
    constexpr char phasertng_map_ellg_result[] = ".phasertng.map_ellg.result.";
    constexpr char fs[] = "fs";
    linkKey(phasertng_map_ellg_result,fs);
    }
    {
    constexpr char phasertng_map_ellg_result[] = ".phasertng.map_ellg.result.";
    constexpr char rmsd[] = "rmsd";
    linkKey(phasertng_map_ellg_result,rmsd);
    }
    {
    constexpr char phasertng_map_ellg_result[] = ".phasertng.map_ellg.result.";
    constexpr char ellg[] = "ellg";
    linkKey(phasertng_map_ellg_result,ellg);
    }
    {
    constexpr char phasertng_matthews[] = ".phasertng.matthews.";
    constexpr char vm[] = "vm";
    linkKey(phasertng_matthews,vm);
    }
    {
    constexpr char phasertng_matthews[] = ".phasertng.matthews.";
    constexpr char probability[] = "probability";
    linkKey(phasertng_matthews,probability);
    }
    {
    constexpr char phasertng_matthews[] = ".phasertng.matthews.";
    constexpr char z[] = "z";
    linkKey(phasertng_matthews,z);
    }
    {
    constexpr char phasertng_scattering_fluorescence_scan[] = ".phasertng.scattering.fluorescence_scan.";
    constexpr char scatterer[] = "scatterer";
    linkKey(phasertng_scattering_fluorescence_scan,scatterer);
    }
    {
    constexpr char phasertng_scattering_fluorescence_scan[] = ".phasertng.scattering.fluorescence_scan.";
    constexpr char fp[] = "fp";
    linkKey(phasertng_scattering_fluorescence_scan,fp);
    }
    {
    constexpr char phasertng_scattering_fluorescence_scan[] = ".phasertng.scattering.fluorescence_scan.";
    constexpr char fdp[] = "fdp";
    linkKey(phasertng_scattering_fluorescence_scan,fdp);
    }
    {
    constexpr char phasertng_scattering_fluorescence_scan[] = ".phasertng.scattering.fluorescence_scan.";
    constexpr char fix_fdp[] = "fix_fdp";
    linkKey(phasertng_scattering_fluorescence_scan,fix_fdp);
    }
    {
    constexpr char phasertng_scattering_sasaki_table[] = ".phasertng.scattering.sasaki_table.";
    constexpr char scatterer[] = "scatterer";
    linkKey(phasertng_scattering_sasaki_table,scatterer);
    }
    {
    constexpr char phasertng_scattering_sasaki_table[] = ".phasertng.scattering.sasaki_table.";
    constexpr char move_to_edge[] = "move_to_edge";
    linkKey(phasertng_scattering_sasaki_table,move_to_edge);
    }
    {
    constexpr char phasertng_subgroup_expansion[] = ".phasertng.subgroup.expansion.";
    constexpr char hall[] = "hall";
    linkKey(phasertng_subgroup_expansion,hall);
    }
    {
    constexpr char phasertng_subgroup_expansion[] = ".phasertng.subgroup.expansion.";
    constexpr char spacegroup[] = "spacegroup";
    linkKey(phasertng_subgroup_expansion,spacegroup);
    }
    {
    constexpr char phasertng_subgroup_expansion[] = ".phasertng.subgroup.expansion.";
    constexpr char hermann_mauguin_symbol[] = "hermann_mauguin_symbol";
    linkKey(phasertng_subgroup_expansion,hermann_mauguin_symbol);
    }
    {
    constexpr char phasertng_tncs_analysis_result[] = ".phasertng.tncs_analysis.result.";
    constexpr char order[] = "order";
    linkKey(phasertng_tncs_analysis_result,order);
    }
    {
    constexpr char phasertng_tncs_analysis_result[] = ".phasertng.tncs_analysis.result.";
    constexpr char vector[] = "vector";
    linkKey(phasertng_tncs_analysis_result,vector);
    }
    {
    constexpr char phasertng_tncs_analysis_result[] = ".phasertng.tncs_analysis.result.";
    constexpr char parity[] = "parity";
    linkKey(phasertng_tncs_analysis_result,parity);
    }
    {
    constexpr char phasertng_tncs_analysis_result[] = ".phasertng.tncs_analysis.result.";
    constexpr char unique[] = "unique";
    linkKey(phasertng_tncs_analysis_result,unique);
    }
  }

  void
  InputCard::add_phil_input_parameters()
  {
    phil_input_parameters["aniso"] = {"dag","graph_report","labin","macaniso","mode","outlier","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["beam"] = {"dag","graph_report","hklin","isostructure","mirrors","mode","overwrite","pdbout","suite","threads","title"};
    phil_input_parameters["brf"] = {"brute_rotation_function","coiled_coil","dag","graph_report","mode","outlier","overwrite","reflid","resolution","search","suite","threads","title"};
    phil_input_parameters["btf"] = {"brute_translation_function","dag","graph_report","mode","outlier","overwrite","reflid","resolution","sampling_generator","suite","threads","title"};
    phil_input_parameters["bub"] = {"biological_unit","biological_unit_builder","dag","graph_report","hklin","mode","overwrite","suite","threads","title"};
    phil_input_parameters["cca"] = {"biological_unit","cluster_compound","composition","dag","graph_report","mode","overwrite","reflid","space_group_expansion","suite","threads","title"};
    phil_input_parameters["ccs"] = {"cell_content_scaling","dag","graph_report","mode","outlier","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["data"] = {"dag","graph_report","labin","mode","outlier","overwrite","reflections","reflid","resolution","suite","threads","title"};
    phil_input_parameters["deep"] = {"dag","graph_report","mode","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["eatm"] = {"dag","expected","graph_report","mode","outlier","overwrite","reflid","scattering","suite","threads","title"};
    phil_input_parameters["ellg"] = {"dag","expected","graph_report","mode","outlier","overwrite","reflid","resolution","search","suite","threads","title"};
    phil_input_parameters["emap"] = {"dag","graph_report","map_ellg","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["emm"] = {"cell_scale","dag","data","ensemble","graph_report","mode","model","molecular_transform","overwrite","point_group","reflid","suite","threads","title","trace"};
    phil_input_parameters["esm"] = {"cell_scale","dag","data","graph_report","mode","model","molecular_transform","overwrite","pdbout","point_group","reflid","suite","threads","title","trace"};
    phil_input_parameters["etfz"] = {"dag","graph_report","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["exp1"] = {"dag","graph_report","mode","overwrite","perturbations","reflid","suite","threads","title"};
    phil_input_parameters["feff"] = {"dag","graph_report","labin","mode","outlier","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["fetch"] = {"dag","graph_report","mirrors","mode","overwrite","pdbout","reflid","suite","threads","title"};
    phil_input_parameters["find"] = {"dag","graph_report","mode","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["frf"] = {"coiled_coil","dag","fast_rotation_function","graph_report","mode","outlier","overwrite","reflid","resolution","search","suite","threads","title"};
    phil_input_parameters["frfr"] = {"coiled_coil","dag","fast_rotation_function","graph_report","mode","outlier","overwrite","reflid","rescore_rotation_function","resolution","suite","threads","title"};
    phil_input_parameters["ftf"] = {"coiled_coil","dag","fast_translation_function","graph_report","mode","outlier","overwrite","packing","reflid","resolution","sgalt","suite","threads","title"};
    phil_input_parameters["ftfr"] = {"coiled_coil","dag","fast_translation_function","graph_report","mode","outlier","overwrite","packing","reflid","rescore_translation_function","resolution","suite","threads","title"};
    phil_input_parameters["fuse"] = {"bfactor","dag","fuse_translation_function","graph_report","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["gyre"] = {"coiled_coil","dag","graph_report","gyration","macgyre","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["hyss"] = {"dag","graph_report","hklin","mode","overwrite","reflections","reflid","substructure","suite","threads","title"};
    phil_input_parameters["imap"] = {"dag","graph_report","labin","mapin","mode","overwrite","reflections","reflid","suite","threads","title"};
    phil_input_parameters["imtz"] = {"dag","graph_report","hklin","labin","mode","overwrite","reflections","reflid","suite","threads","title"};
    phil_input_parameters["info"] = {"dag","graph_report","information","labin","mode","mtzout","outlier","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["ipdb"] = {"dag","graph_report","mode","overwrite","pdbin","reflections","reflid","resolution","suite","threads","title"};
    phil_input_parameters["jog"] = {"bfactor","cell_scale","dag","graph_report","jog_refinement","macjog","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["join"] = {"dag","graph_report","mode","model","overwrite","pdbout","reflid","suite","threads","title"};
    phil_input_parameters["mapz"] = {"dag","graph_report","mode","outlier","overwrite","reflid","resolution","suite","threads","title","zscore_equivalent_map"};
    phil_input_parameters["mcc"] = {"dag","graph_report","map_correlation_coefficient","mode","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["move"] = {"bfactor","dag","graph_report","mode","outlier","overwrite","packing","reflid","resolution","suite","threads","title"};
    phil_input_parameters["msm"] = {"cell_scale","dag","graph_report","macbox","mode","model_as_map","molecular_transform","outlier","overwrite","reflid","resolution","suite","threads","title","trace"};
    phil_input_parameters["pak"] = {"dag","graph_report","mode","overwrite","packing","reflid","suite","threads","title"};
    phil_input_parameters["perm"] = {"dag","expected","graph_report","mode","outlier","overwrite","permutations","reflid","resolution","search","suite","threads","title"};
    phil_input_parameters["pool"] = {"dag","graph_report","mode","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["pose"] = {"bfactor","dag","graph_report","mode","outlier","overwrite","pose_scoring","reflid","resolution","suite","threads","title"};
    phil_input_parameters["ptf"] = {"dag","graph_report","mode","outlier","overwrite","phased_translation_function","reflid","resolution","suite","threads","title"};
    phil_input_parameters["put"] = {"dag","graph_report","mode","overwrite","put_solution","reflid","search","suite","threads","title"};
    phil_input_parameters["rbgs"] = {"dag","graph_report","grid_search","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["rbm"] = {"dag","graph_report","labin","mode","outlier","overwrite","reflid","resolution","rigid_body_maps","suite","threads","title"};
    phil_input_parameters["rbr"] = {"bfactor","cell_scale","dag","graph_report","macrbr","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["rellg"] = {"dag","expected","graph_report","mode","outlier","overwrite","reflid","suite","threads","title"};
    phil_input_parameters["rfac"] = {"dag","graph_report","labin","mode","overwrite","refinement","suite","threads","title"};
    phil_input_parameters["rmr"] = {"bfactor","cell_scale","dag","graph_report","macrm","mode","outlier","overwrite","reflid","resolution","suite","threads","title"};
    phil_input_parameters["sga"] = {"dag","graph_report","mode","overwrite","reflid","subgroup","suite","threads","title"};
    phil_input_parameters["sgx"] = {"dag","graph_report","labin","mode","overwrite","reflid","space_group_expansion","suite","threads","title"};
    phil_input_parameters["span"] = {"dag","graph_report","mode","overwrite","reflid","spanning","suite","threads","title"};
    phil_input_parameters["spr"] = {"dag","graph_report","labin","macspr","mode","outlier","overwrite","reflid","resolution","segmented_pruning","suite","threads","title"};
    phil_input_parameters["srf"] = {"cross_rotation_function","dag","graph_report","mode","outlier","overwrite","reflid","resolution","self_rotation_function","suite","threads","title"};
    phil_input_parameters["srfp"] = {"dag","graph_report","mode","overwrite","suite","threads","title"};
    phil_input_parameters["ssa"] = {"biological_unit","cluster_compound","dag","graph_report","macssa","mode","outlier","overwrite","reflid","scattering","substructure","suite","threads","title"};
    phil_input_parameters["sscc"] = {"dag","graph_report","mode","overwrite","pdbin","reflid","suite","threads","title"};
    phil_input_parameters["ssd"] = {"dag","graph_report","macsad","mode","overwrite","reflid","resolution","scattering","sgalt","substructure","suite","threads","title"};
    phil_input_parameters["ssm"] = {"atoms","bfactor","dag","graph_report","mode","overwrite","reflid","scattering","substructure","suite","threads","title"};
    phil_input_parameters["ssp"] = {"dag","graph_report","mode","outlier","overwrite","reflid","resolution","scattering","substructure","suite","threads","title"};
    phil_input_parameters["ssr"] = {"cluster_compound","dag","graph_report","labin","macsad","mode","overwrite","reflid","resolution","scattering","substructure","suite","threads","title"};
    phil_input_parameters["tfz"] = {"dag","graph_report","mode","outlier","overwrite","reflid","resolution","suite","threads","title","zscore_equivalent"};
    phil_input_parameters["tncs"] = {"dag","graph_report","mactncs","mode","outlier","overwrite","reflid","resolution","suite","threads","title","tncs"};
    phil_input_parameters["tncso"] = {"coiled_coil","dag","graph_report","mode","outlier","overwrite","reflid","suite","threads","title","tncs","tncs_order"};
    phil_input_parameters["tree"] = {"graph_report","mode","overwrite","suite","title"};
    phil_input_parameters["twin"] = {"dag","graph_report","mode","outlier","overwrite","reflid","suite","threads","title","tncs","twinning"};
    phil_input_parameters["walk"] = {"dag","file_discovery","graph_report","mode","overwrite","suite","threads","title"};
    phil_input_parameters["xref"] = {"dag","graph_report","labin","mode","overwrite","refinement","suite","threads","title"};
  }

  void
  InputCard::add_phil_result_parameters()
  {
    phil_result_parameters["aniso"] = {"anisotropy","dag","hklout","notifications","overwrite","title"};
    phil_result_parameters["beam"] = {"dag","hklout","isostructure","notifications","overwrite","pdbout","search_combination","seqout","title"};
    phil_result_parameters["brf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["btf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["bub"] = {"dag","notifications","overwrite","search_combination","title"};
    phil_result_parameters["cca"] = {"cell_content_scaling","composition","dag","hklout","matthews","notifications","overwrite","title"};
    phil_result_parameters["ccs"] = {"dag","hklout","notifications","overwrite","title"};
    phil_result_parameters["data"] = {"dag","data","hklout","labin","notifications","overwrite","title"};
    phil_result_parameters["deep"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["eatm"] = {"dag","expected","notifications","overwrite","title"};
    phil_result_parameters["ellg"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["emap"] = {"dag","map_ellg","notifications","overwrite","title"};
    phil_result_parameters["emm"] = {"dag","notifications","overwrite","pdbout","search","title"};
    phil_result_parameters["esm"] = {"dag","notifications","overwrite","search","title"};
    phil_result_parameters["etfz"] = {"dag","expected","notifications","overwrite","title"};
    phil_result_parameters["exp1"] = {"dag","hklout","notifications","overwrite","title"};
    phil_result_parameters["feff"] = {"dag","hklout","notifications","overwrite","title"};
    phil_result_parameters["fetch"] = {"dag","notifications","overwrite","pdbout","title"};
    phil_result_parameters["find"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["frf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["frfr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ftf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ftfr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["fuse"] = {"dag","fuse_translation_function","notifications","overwrite","title"};
    phil_result_parameters["gyre"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["hyss"] = {"atoms","dag","notifications","overwrite","title"};
    phil_result_parameters["imap"] = {"dag","data","hklout","notifications","overwrite","reflections","title"};
    phil_result_parameters["imtz"] = {"dag","data","hklout","labin","notifications","overwrite","reflections","title"};
    phil_result_parameters["info"] = {"dag","hklout","information","notifications","overwrite","title"};
    phil_result_parameters["ipdb"] = {"dag","data","hklout","labin","notifications","overwrite","title"};
    phil_result_parameters["jog"] = {"dag","jog_refinement","notifications","overwrite","title"};
    phil_result_parameters["join"] = {"dag","notifications","overwrite","pdbout","title"};
    phil_result_parameters["mapz"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["mcc"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["move"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["msm"] = {"dag","model_as_map","notifications","overwrite","search","title"};
    phil_result_parameters["pak"] = {"dag","notifications","overwrite","packing","title"};
    phil_result_parameters["perm"] = {"dag","expected","notifications","overwrite","title"};
    phil_result_parameters["pool"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["pose"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ptf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["put"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["rbgs"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["rbm"] = {"dag","notifications","overwrite","pdbout","title"};
    phil_result_parameters["rbr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["rellg"] = {"dag","expected","notifications","overwrite","title"};
    phil_result_parameters["rfac"] = {"dag","notifications","overwrite","pdbout","title"};
    phil_result_parameters["rmr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["sga"] = {"dag","notifications","overwrite","subgroup","title"};
    phil_result_parameters["sgx"] = {"dag","hklout","notifications","overwrite","space_group_expansion","title"};
    phil_result_parameters["span"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["spr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["srf"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["srfp"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ssa"] = {"dag","hklout","notifications","overwrite","substructure","title"};
    phil_result_parameters["sscc"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ssd"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ssm"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["ssp"] = {"dag","notifications","overwrite","substructure","title"};
    phil_result_parameters["ssr"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["tfz"] = {"dag","notifications","overwrite","title"};
    phil_result_parameters["tncs"] = {"dag","hklout","notifications","overwrite","title","tncs"};
    phil_result_parameters["tncso"] = {"dag","notifications","overwrite","title","tncs","tncs_analysis"};
    phil_result_parameters["tree"] = {"notifications","overwrite","title"};
    phil_result_parameters["twin"] = {"dag","hklout","notifications","overwrite","title","twinning"};
    phil_result_parameters["walk"] = {"dag","file_discovery","notifications","overwrite","title"};
    phil_result_parameters["xref"] = {"dag","notifications","overwrite","pdbout","title"};
  }

  bool
  InputCard::parse(std::string cards, bool ignore_unknown_keys)
  {
    cards.erase(std::unique(cards.begin(),cards.end(),[](char a,char b){return a=='\n' && b=='\n';}),cards.end());
    cards.erase(cards.begin(), std::find_if(cards.begin(), cards.end(), [](unsigned char ch) { return !std::isspace(ch);})); // Lambda function checks for non-whitespace
    reset_ccp4base();
    std::istringstream input_stream(cards);
    input_stream.seekg(0,input_stream.beg);
    while (input_stream)
    {
      variable.clear();
      get_token(input_stream);
      if (keyIsEnd()) return false;
      else if (keyIsEol()) return false;
      else if (tokenIsAlready(Token::ASSIGN)) return true; //!!and stop parsing file
      else if (tokenIsAlready(Token::COMMENT)) { skip_line(input_stream); }
      else if (keyIs("phasertng"))
      {
        compulsoryKey(input_stream,{"anisotropy","atoms","bfactor","bins","biological_unit","biological_unit_builder","brute_rotation_function","brute_translation_function","cell_content_scaling","cell_scale","cluster_compound","coiled_coil","composition","cross_rotation_function","dag","data","ensemble","expected","fast_rotation_function","fast_translation_function","file_discovery","fuse_translation_function","graph_report","grid_search","gyration","hklin","hklout","information","isostructure","jog_refinement","labin","macaniso","macbox","macgyre","macjog","macrbr","macrm","macsad","macspr","macssa","mactncs","map_correlation_coefficient","map_ellg","mapin","matthews","mirrors","mode","model","model_as_map","molecular_transform","mtzout","notifications","outlier","overwrite","packing","pdbin","pdbout","permutations","perturbations","phased_translation_function","point_group","pose_scoring","put_solution","refinement","reflections","reflid","rescore_rotation_function","rescore_translation_function","resolution","rigid_body_maps","sampling_generator","scattering","search","search_combination","segmented_pruning","self_rotation_function","seqout","sgalt","solvent_sigmaa","space_group_expansion","spanning","subgroup","substructure","suite","threads","time","title","tncs","tncs_analysis","tncs_order","trace","twinning","zscore_equivalent","zscore_equivalent_map"});
        if (keyIsEol()) return false;
        else if (keyIs("anisotropy")) {
          compulsoryKey(input_stream,{"beta","delta_bfactor","direction_cosines","outer_bin_contrast","sharpening_bfactor","wilson_bfactor","wilson_scale"});
          if (keyIsEol()) return false;
          else if (keyIs("beta")) {
            constexpr char phasertng_anisotropy_beta[] = ".phasertng.anisotropy.beta.";
            check_unique(phasertng_anisotropy_beta);
            value__flt_numbers[phasertng_anisotropy_beta].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_anisotropy_beta].minset(),value__flt_numbers[phasertng_anisotropy_beta].minval(),value__flt_numbers[phasertng_anisotropy_beta].maxset(),value__flt_numbers[phasertng_anisotropy_beta].maxval()));
          }
          else if (keyIs("delta_bfactor")) {
            constexpr char phasertng_anisotropy_delta_bfactor[] = ".phasertng.anisotropy.delta_bfactor.";
            check_unique(phasertng_anisotropy_delta_bfactor);
            value__flt_number[phasertng_anisotropy_delta_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_anisotropy_delta_bfactor].minset(),value__flt_number[phasertng_anisotropy_delta_bfactor].minval(),value__flt_number[phasertng_anisotropy_delta_bfactor].maxset(),value__flt_number[phasertng_anisotropy_delta_bfactor].maxval()));
          }
          else if (keyIs("direction_cosines")) {
            constexpr char phasertng_anisotropy_direction_cosines[] = ".phasertng.anisotropy.direction_cosines.";
            check_unique(phasertng_anisotropy_direction_cosines);
            value__flt_matrix[phasertng_anisotropy_direction_cosines].set_value(variable,get_flt_matrix(input_stream,value__flt_matrix[phasertng_anisotropy_direction_cosines].minset(),value__flt_matrix[phasertng_anisotropy_direction_cosines].minval(),value__flt_matrix[phasertng_anisotropy_direction_cosines].maxset(),value__flt_matrix[phasertng_anisotropy_direction_cosines].maxval()));
          }
          else if (keyIs("outer_bin_contrast")) {
            constexpr char phasertng_anisotropy_outer_bin_contrast[] = ".phasertng.anisotropy.outer_bin_contrast.";
            check_unique(phasertng_anisotropy_outer_bin_contrast);
            value__flt_number[phasertng_anisotropy_outer_bin_contrast].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_anisotropy_outer_bin_contrast].minset(),value__flt_number[phasertng_anisotropy_outer_bin_contrast].minval(),value__flt_number[phasertng_anisotropy_outer_bin_contrast].maxset(),value__flt_number[phasertng_anisotropy_outer_bin_contrast].maxval()));
          }
          else if (keyIs("sharpening_bfactor")) {
            constexpr char phasertng_anisotropy_sharpening_bfactor[] = ".phasertng.anisotropy.sharpening_bfactor.";
            check_unique(phasertng_anisotropy_sharpening_bfactor);
            value__flt_number[phasertng_anisotropy_sharpening_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_anisotropy_sharpening_bfactor].minset(),value__flt_number[phasertng_anisotropy_sharpening_bfactor].minval(),value__flt_number[phasertng_anisotropy_sharpening_bfactor].maxset(),value__flt_number[phasertng_anisotropy_sharpening_bfactor].maxval()));
          }
          else if (keyIs("wilson_bfactor")) {
            constexpr char phasertng_anisotropy_wilson_bfactor[] = ".phasertng.anisotropy.wilson_bfactor.";
            check_unique(phasertng_anisotropy_wilson_bfactor);
            value__flt_number[phasertng_anisotropy_wilson_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_anisotropy_wilson_bfactor].minset(),value__flt_number[phasertng_anisotropy_wilson_bfactor].minval(),value__flt_number[phasertng_anisotropy_wilson_bfactor].maxset(),value__flt_number[phasertng_anisotropy_wilson_bfactor].maxval()));
          }
          else if (keyIs("wilson_scale")) {
            constexpr char phasertng_anisotropy_wilson_scale[] = ".phasertng.anisotropy.wilson_scale.";
            check_unique(phasertng_anisotropy_wilson_scale);
            value__flt_number[phasertng_anisotropy_wilson_scale].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_anisotropy_wilson_scale].minset(),value__flt_number[phasertng_anisotropy_wilson_scale].minval(),value__flt_number[phasertng_anisotropy_wilson_scale].maxset(),value__flt_number[phasertng_anisotropy_wilson_scale].maxval()));
          }
        }
        else if (keyIs("atoms")) {
          compulsoryKey(input_stream,{"filename","fractional"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_atoms_filename[] = ".phasertng.atoms.filename.";
            check_unique(phasertng_atoms_filename);
            value__path[phasertng_atoms_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("fractional")) {
            compulsoryKey(input_stream,{"scatterer"});
            variable.push_back("scatterer");
            constexpr char phasertng_atoms_fractional_scatterer[] = ".phasertng.atoms.fractional.scatterer.";
            array__string[phasertng_atoms_fractional_scatterer].push_back(type::string(phasertng_atoms_fractional_scatterer));
            array__string[phasertng_atoms_fractional_scatterer].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
            compulsoryKey(input_stream,{"site"});
            variable.push_back("site");
            constexpr char phasertng_atoms_fractional_site[] = ".phasertng.atoms.fractional.site.";
            array__flt_vector[phasertng_atoms_fractional_site].push_back(type::flt_vector(phasertng_atoms_fractional_site,false,0,false,0));
            array__flt_vector[phasertng_atoms_fractional_site].back().set_value(variable,get_flt_vector(input_stream,array__flt_vector[phasertng_atoms_fractional_site].back().minset(),array__flt_vector[phasertng_atoms_fractional_site].back().minval(),array__flt_vector[phasertng_atoms_fractional_site].back().maxset(),array__flt_vector[phasertng_atoms_fractional_site].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"occupancy"});
            variable.push_back("occupancy");
            constexpr char phasertng_atoms_fractional_occupancy[] = ".phasertng.atoms.fractional.occupancy.";
            array__flt_number[phasertng_atoms_fractional_occupancy].push_back(type::flt_number(phasertng_atoms_fractional_occupancy,true,0,false,0));
            array__flt_number[phasertng_atoms_fractional_occupancy].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_atoms_fractional_occupancy].back().minset(),array__flt_number[phasertng_atoms_fractional_occupancy].back().minval(),array__flt_number[phasertng_atoms_fractional_occupancy].back().maxset(),array__flt_number[phasertng_atoms_fractional_occupancy].back().maxval()));
            variable.pop_back();
          }
        }
        else if (keyIs("bfactor")) {
          compulsoryKey(input_stream,{"maximum","minimum"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum")) {
            constexpr char phasertng_bfactor_maximum[] = ".phasertng.bfactor.maximum.";
            check_unique(phasertng_bfactor_maximum);
            value__flt_number[phasertng_bfactor_maximum].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_bfactor_maximum].minset(),value__flt_number[phasertng_bfactor_maximum].minval(),value__flt_number[phasertng_bfactor_maximum].maxset(),value__flt_number[phasertng_bfactor_maximum].maxval()));
          }
          else if (keyIs("minimum")) {
            constexpr char phasertng_bfactor_minimum[] = ".phasertng.bfactor.minimum.";
            check_unique(phasertng_bfactor_minimum);
            value__flt_number[phasertng_bfactor_minimum].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_bfactor_minimum].minset(),value__flt_number[phasertng_bfactor_minimum].minval(),value__flt_number[phasertng_bfactor_minimum].maxset(),value__flt_number[phasertng_bfactor_minimum].maxval()));
          }
        }
        else if (keyIs("bins")) {
          compulsoryKey(input_stream,{"maps","molecular_transforms","reflections"});
          if (keyIsEol()) return false;
          else if (keyIs("maps")) {
            compulsoryKey(input_stream,{"range","width"});
            if (keyIsEol()) return false;
            else if (keyIs("range")) {
              constexpr char phasertng_bins_maps_range[] = ".phasertng.bins.maps.range.";
              check_unique(phasertng_bins_maps_range);
              value__int_paired[phasertng_bins_maps_range].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_bins_maps_range].minset(),value__int_paired[phasertng_bins_maps_range].minval(),value__int_paired[phasertng_bins_maps_range].maxset(),value__int_paired[phasertng_bins_maps_range].maxval()));
            }
            else if (keyIs("width")) {
              constexpr char phasertng_bins_maps_width[] = ".phasertng.bins.maps.width.";
              check_unique(phasertng_bins_maps_width);
              value__int_number[phasertng_bins_maps_width].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_bins_maps_width].minset(),value__int_number[phasertng_bins_maps_width].minval(),value__int_number[phasertng_bins_maps_width].maxset(),value__int_number[phasertng_bins_maps_width].maxval()));
            }
          }
          else if (keyIs("molecular_transforms")) {
            compulsoryKey(input_stream,{"range","width"});
            if (keyIsEol()) return false;
            else if (keyIs("range")) {
              constexpr char phasertng_bins_molecular_transforms_range[] = ".phasertng.bins.molecular_transforms.range.";
              check_unique(phasertng_bins_molecular_transforms_range);
              value__int_paired[phasertng_bins_molecular_transforms_range].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_bins_molecular_transforms_range].minset(),value__int_paired[phasertng_bins_molecular_transforms_range].minval(),value__int_paired[phasertng_bins_molecular_transforms_range].maxset(),value__int_paired[phasertng_bins_molecular_transforms_range].maxval()));
            }
            else if (keyIs("width")) {
              constexpr char phasertng_bins_molecular_transforms_width[] = ".phasertng.bins.molecular_transforms.width.";
              check_unique(phasertng_bins_molecular_transforms_width);
              value__int_number[phasertng_bins_molecular_transforms_width].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_bins_molecular_transforms_width].minset(),value__int_number[phasertng_bins_molecular_transforms_width].minval(),value__int_number[phasertng_bins_molecular_transforms_width].maxset(),value__int_number[phasertng_bins_molecular_transforms_width].maxval()));
            }
          }
          else if (keyIs("reflections")) {
            compulsoryKey(input_stream,{"range","width"});
            if (keyIsEol()) return false;
            else if (keyIs("range")) {
              constexpr char phasertng_bins_reflections_range[] = ".phasertng.bins.reflections.range.";
              check_unique(phasertng_bins_reflections_range);
              value__int_paired[phasertng_bins_reflections_range].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_bins_reflections_range].minset(),value__int_paired[phasertng_bins_reflections_range].minval(),value__int_paired[phasertng_bins_reflections_range].maxset(),value__int_paired[phasertng_bins_reflections_range].maxval()));
            }
            else if (keyIs("width")) {
              constexpr char phasertng_bins_reflections_width[] = ".phasertng.bins.reflections.width.";
              check_unique(phasertng_bins_reflections_width);
              value__int_number[phasertng_bins_reflections_width].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_bins_reflections_width].minset(),value__int_number[phasertng_bins_reflections_width].minval(),value__int_number[phasertng_bins_reflections_width].maxset(),value__int_number[phasertng_bins_reflections_width].maxval()));
            }
          }
        }
        else if (keyIs("biological_unit")) {
          compulsoryKey(input_stream,{"deuterium","disulphide","hetatm","model","selenomethionine","sequence","solvent","water_percent"});
          if (keyIsEol()) return false;
          else if (keyIs("deuterium")) {
            constexpr char phasertng_biological_unit_deuterium[] = ".phasertng.biological_unit.deuterium.";
            check_unique(phasertng_biological_unit_deuterium);
            value__boolean[phasertng_biological_unit_deuterium].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("disulphide")) {
            constexpr char phasertng_biological_unit_disulphide[] = ".phasertng.biological_unit.disulphide.";
            check_unique(phasertng_biological_unit_disulphide);
            value__boolean[phasertng_biological_unit_disulphide].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("hetatm")) {
            constexpr char phasertng_biological_unit_hetatm[] = ".phasertng.biological_unit.hetatm.";
            array__strings[phasertng_biological_unit_hetatm].push_back(type::strings(phasertng_biological_unit_hetatm));
            array__strings[phasertng_biological_unit_hetatm].back().set_value(variable,get_strings(input_stream));
          }
          else if (keyIs("model")) {
            compulsoryKey(input_stream,{"filename","via_sequence"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_biological_unit_model_filename[] = ".phasertng.biological_unit.model.filename.";
              check_unique(phasertng_biological_unit_model_filename);
              value__path[phasertng_biological_unit_model_filename].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("via_sequence")) {
              constexpr char phasertng_biological_unit_model_via_sequence[] = ".phasertng.biological_unit.model.via_sequence.";
              check_unique(phasertng_biological_unit_model_via_sequence);
              value__boolean[phasertng_biological_unit_model_via_sequence].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("selenomethionine")) {
            constexpr char phasertng_biological_unit_selenomethionine[] = ".phasertng.biological_unit.selenomethionine.";
            check_unique(phasertng_biological_unit_selenomethionine);
            value__boolean[phasertng_biological_unit_selenomethionine].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("sequence")) {
            compulsoryKey(input_stream,{"filename","multiplicity"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_biological_unit_sequence_filename[] = ".phasertng.biological_unit.sequence.filename.";
              check_unique(phasertng_biological_unit_sequence_filename);
              value__path[phasertng_biological_unit_sequence_filename].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("multiplicity")) {
              constexpr char phasertng_biological_unit_sequence_multiplicity[] = ".phasertng.biological_unit.sequence.multiplicity.";
              check_unique(phasertng_biological_unit_sequence_multiplicity);
              value__int_number[phasertng_biological_unit_sequence_multiplicity].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_biological_unit_sequence_multiplicity].minset(),value__int_number[phasertng_biological_unit_sequence_multiplicity].minval(),value__int_number[phasertng_biological_unit_sequence_multiplicity].maxset(),value__int_number[phasertng_biological_unit_sequence_multiplicity].maxval()));
            }
          }
          else if (keyIs("solvent")) {
            constexpr char phasertng_biological_unit_solvent[] = ".phasertng.biological_unit.solvent.";
            check_unique(phasertng_biological_unit_solvent);
            value__percent[phasertng_biological_unit_solvent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("water_percent")) {
            constexpr char phasertng_biological_unit_water_percent[] = ".phasertng.biological_unit.water_percent.";
            check_unique(phasertng_biological_unit_water_percent);
            value__percent[phasertng_biological_unit_water_percent].set_value(variable,get_percent(input_stream));
          }
        }
        else if (keyIs("biological_unit_builder")) {
          compulsoryKey(input_stream,{"build_from_sequence","coverage","identity","model","overlap"});
          if (keyIsEol()) return false;
          else if (keyIs("build_from_sequence")) {
            constexpr char phasertng_biological_unit_builder_build_from_sequence[] = ".phasertng.biological_unit_builder.build_from_sequence.";
            check_unique(phasertng_biological_unit_builder_build_from_sequence);
            value__boolean[phasertng_biological_unit_builder_build_from_sequence].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("coverage")) {
            constexpr char phasertng_biological_unit_builder_coverage[] = ".phasertng.biological_unit_builder.coverage.";
            check_unique(phasertng_biological_unit_builder_coverage);
            value__percent[phasertng_biological_unit_builder_coverage].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("identity")) {
            constexpr char phasertng_biological_unit_builder_identity[] = ".phasertng.biological_unit_builder.identity.";
            check_unique(phasertng_biological_unit_builder_identity);
            value__percent[phasertng_biological_unit_builder_identity].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("model")) {
            constexpr char phasertng_biological_unit_builder_model[] = ".phasertng.biological_unit_builder.model.";
            array__path[phasertng_biological_unit_builder_model].push_back(type::path(phasertng_biological_unit_builder_model));
            array__path[phasertng_biological_unit_builder_model].back().set_value(variable,get_path(input_stream));
          }
          else if (keyIs("overlap")) {
            constexpr char phasertng_biological_unit_builder_overlap[] = ".phasertng.biological_unit_builder.overlap.";
            check_unique(phasertng_biological_unit_builder_overlap);
            value__percent[phasertng_biological_unit_builder_overlap].set_value(variable,get_percent(input_stream));
          }
        }
        else if (keyIs("brute_rotation_function")) {
          compulsoryKey(input_stream,{"cluster_back","generate_only","helix_factor","maximum_printed","maximum_stored","nrand","range","sampling","shift_matrix","signal_resolution","volume"});
          if (keyIsEol()) return false;
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_brute_rotation_function_cluster_back[] = ".phasertng.brute_rotation_function.cluster_back.";
            check_unique(phasertng_brute_rotation_function_cluster_back);
            value__int_number[phasertng_brute_rotation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_rotation_function_cluster_back].minset(),value__int_number[phasertng_brute_rotation_function_cluster_back].minval(),value__int_number[phasertng_brute_rotation_function_cluster_back].maxset(),value__int_number[phasertng_brute_rotation_function_cluster_back].maxval()));
          }
          else if (keyIs("generate_only")) {
            constexpr char phasertng_brute_rotation_function_generate_only[] = ".phasertng.brute_rotation_function.generate_only.";
            check_unique(phasertng_brute_rotation_function_generate_only);
            value__boolean[phasertng_brute_rotation_function_generate_only].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("helix_factor")) {
            constexpr char phasertng_brute_rotation_function_helix_factor[] = ".phasertng.brute_rotation_function.helix_factor.";
            check_unique(phasertng_brute_rotation_function_helix_factor);
            value__flt_number[phasertng_brute_rotation_function_helix_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_rotation_function_helix_factor].minset(),value__flt_number[phasertng_brute_rotation_function_helix_factor].minval(),value__flt_number[phasertng_brute_rotation_function_helix_factor].maxset(),value__flt_number[phasertng_brute_rotation_function_helix_factor].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_brute_rotation_function_maximum_printed[] = ".phasertng.brute_rotation_function.maximum_printed.";
            check_unique(phasertng_brute_rotation_function_maximum_printed);
            value__int_number[phasertng_brute_rotation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_rotation_function_maximum_printed].minset(),value__int_number[phasertng_brute_rotation_function_maximum_printed].minval(),value__int_number[phasertng_brute_rotation_function_maximum_printed].maxset(),value__int_number[phasertng_brute_rotation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_brute_rotation_function_maximum_stored[] = ".phasertng.brute_rotation_function.maximum_stored.";
            check_unique(phasertng_brute_rotation_function_maximum_stored);
            value__int_number[phasertng_brute_rotation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_rotation_function_maximum_stored].minset(),value__int_number[phasertng_brute_rotation_function_maximum_stored].minval(),value__int_number[phasertng_brute_rotation_function_maximum_stored].maxset(),value__int_number[phasertng_brute_rotation_function_maximum_stored].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_brute_rotation_function_nrand[] = ".phasertng.brute_rotation_function.nrand.";
            check_unique(phasertng_brute_rotation_function_nrand);
            value__int_number[phasertng_brute_rotation_function_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_rotation_function_nrand].minset(),value__int_number[phasertng_brute_rotation_function_nrand].minval(),value__int_number[phasertng_brute_rotation_function_nrand].maxset(),value__int_number[phasertng_brute_rotation_function_nrand].maxval()));
          }
          else if (keyIs("range")) {
            constexpr char phasertng_brute_rotation_function_range[] = ".phasertng.brute_rotation_function.range.";
            check_unique(phasertng_brute_rotation_function_range);
            value__flt_number[phasertng_brute_rotation_function_range].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_rotation_function_range].minset(),value__flt_number[phasertng_brute_rotation_function_range].minval(),value__flt_number[phasertng_brute_rotation_function_range].maxset(),value__flt_number[phasertng_brute_rotation_function_range].maxval()));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_brute_rotation_function_sampling[] = ".phasertng.brute_rotation_function.sampling.";
            check_unique(phasertng_brute_rotation_function_sampling);
            value__flt_number[phasertng_brute_rotation_function_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_rotation_function_sampling].minset(),value__flt_number[phasertng_brute_rotation_function_sampling].minval(),value__flt_number[phasertng_brute_rotation_function_sampling].maxset(),value__flt_number[phasertng_brute_rotation_function_sampling].maxval()));
          }
          else if (keyIs("shift_matrix")) {
            constexpr char phasertng_brute_rotation_function_shift_matrix[] = ".phasertng.brute_rotation_function.shift_matrix.";
            check_unique(phasertng_brute_rotation_function_shift_matrix);
            value__flt_matrix[phasertng_brute_rotation_function_shift_matrix].set_value(variable,get_flt_matrix(input_stream,value__flt_matrix[phasertng_brute_rotation_function_shift_matrix].minset(),value__flt_matrix[phasertng_brute_rotation_function_shift_matrix].minval(),value__flt_matrix[phasertng_brute_rotation_function_shift_matrix].maxset(),value__flt_matrix[phasertng_brute_rotation_function_shift_matrix].maxval()));
          }
          else if (keyIs("signal_resolution")) {
            constexpr char phasertng_brute_rotation_function_signal_resolution[] = ".phasertng.brute_rotation_function.signal_resolution.";
            check_unique(phasertng_brute_rotation_function_signal_resolution);
            value__flt_number[phasertng_brute_rotation_function_signal_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_rotation_function_signal_resolution].minset(),value__flt_number[phasertng_brute_rotation_function_signal_resolution].minval(),value__flt_number[phasertng_brute_rotation_function_signal_resolution].maxset(),value__flt_number[phasertng_brute_rotation_function_signal_resolution].maxval()));
          }
          else if (keyIs("volume")) {
            compulsoryKey(input_stream,{"unique","around","random"});
            constexpr char phasertng_brute_rotation_function_volume[] = ".phasertng.brute_rotation_function.volume.";
            check_unique(phasertng_brute_rotation_function_volume);
            value__choice[phasertng_brute_rotation_function_volume].set_value(variable,string_value);
          }
        }
        else if (keyIs("brute_translation_function")) {
          compulsoryKey(input_stream,{"cluster_back","fractional","maximum_printed","maximum_stored","nrand","point","range","sampling","sampling_factor","volume"});
          if (keyIsEol()) return false;
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_brute_translation_function_cluster_back[] = ".phasertng.brute_translation_function.cluster_back.";
            check_unique(phasertng_brute_translation_function_cluster_back);
            value__int_number[phasertng_brute_translation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_translation_function_cluster_back].minset(),value__int_number[phasertng_brute_translation_function_cluster_back].minval(),value__int_number[phasertng_brute_translation_function_cluster_back].maxset(),value__int_number[phasertng_brute_translation_function_cluster_back].maxval()));
          }
          else if (keyIs("fractional")) {
            constexpr char phasertng_brute_translation_function_fractional[] = ".phasertng.brute_translation_function.fractional.";
            check_unique(phasertng_brute_translation_function_fractional);
            value__boolean[phasertng_brute_translation_function_fractional].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_brute_translation_function_maximum_printed[] = ".phasertng.brute_translation_function.maximum_printed.";
            check_unique(phasertng_brute_translation_function_maximum_printed);
            value__int_number[phasertng_brute_translation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_translation_function_maximum_printed].minset(),value__int_number[phasertng_brute_translation_function_maximum_printed].minval(),value__int_number[phasertng_brute_translation_function_maximum_printed].maxset(),value__int_number[phasertng_brute_translation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_brute_translation_function_maximum_stored[] = ".phasertng.brute_translation_function.maximum_stored.";
            check_unique(phasertng_brute_translation_function_maximum_stored);
            value__int_number[phasertng_brute_translation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_translation_function_maximum_stored].minset(),value__int_number[phasertng_brute_translation_function_maximum_stored].minval(),value__int_number[phasertng_brute_translation_function_maximum_stored].maxset(),value__int_number[phasertng_brute_translation_function_maximum_stored].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_brute_translation_function_nrand[] = ".phasertng.brute_translation_function.nrand.";
            check_unique(phasertng_brute_translation_function_nrand);
            value__int_number[phasertng_brute_translation_function_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_brute_translation_function_nrand].minset(),value__int_number[phasertng_brute_translation_function_nrand].minval(),value__int_number[phasertng_brute_translation_function_nrand].maxset(),value__int_number[phasertng_brute_translation_function_nrand].maxval()));
          }
          else if (keyIs("point")) {
            constexpr char phasertng_brute_translation_function_point[] = ".phasertng.brute_translation_function.point.";
            check_unique(phasertng_brute_translation_function_point);
            value__flt_vector[phasertng_brute_translation_function_point].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_brute_translation_function_point].minset(),value__flt_vector[phasertng_brute_translation_function_point].minval(),value__flt_vector[phasertng_brute_translation_function_point].maxset(),value__flt_vector[phasertng_brute_translation_function_point].maxval()));
          }
          else if (keyIs("range")) {
            constexpr char phasertng_brute_translation_function_range[] = ".phasertng.brute_translation_function.range.";
            check_unique(phasertng_brute_translation_function_range);
            value__flt_number[phasertng_brute_translation_function_range].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_translation_function_range].minset(),value__flt_number[phasertng_brute_translation_function_range].minval(),value__flt_number[phasertng_brute_translation_function_range].maxset(),value__flt_number[phasertng_brute_translation_function_range].maxval()));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_brute_translation_function_sampling[] = ".phasertng.brute_translation_function.sampling.";
            check_unique(phasertng_brute_translation_function_sampling);
            value__flt_number[phasertng_brute_translation_function_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_translation_function_sampling].minset(),value__flt_number[phasertng_brute_translation_function_sampling].minval(),value__flt_number[phasertng_brute_translation_function_sampling].maxset(),value__flt_number[phasertng_brute_translation_function_sampling].maxval()));
          }
          else if (keyIs("sampling_factor")) {
            constexpr char phasertng_brute_translation_function_sampling_factor[] = ".phasertng.brute_translation_function.sampling_factor.";
            check_unique(phasertng_brute_translation_function_sampling_factor);
            value__flt_number[phasertng_brute_translation_function_sampling_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_brute_translation_function_sampling_factor].minset(),value__flt_number[phasertng_brute_translation_function_sampling_factor].minval(),value__flt_number[phasertng_brute_translation_function_sampling_factor].maxset(),value__flt_number[phasertng_brute_translation_function_sampling_factor].maxval()));
          }
          else if (keyIs("volume")) {
            compulsoryKey(input_stream,{"around","random","unique","box"});
            constexpr char phasertng_brute_translation_function_volume[] = ".phasertng.brute_translation_function.volume.";
            check_unique(phasertng_brute_translation_function_volume);
            value__choice[phasertng_brute_translation_function_volume].set_value(variable,string_value);
          }
        }
        else if (keyIs("cell_content_scaling")) {
          compulsoryKey(input_stream,{"z"});
          if (keyIsEol()) return false;
          else if (keyIs("z")) {
            constexpr char phasertng_cell_content_scaling_z[] = ".phasertng.cell_content_scaling.z.";
            check_unique(phasertng_cell_content_scaling_z);
            value__flt_number[phasertng_cell_content_scaling_z].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_cell_content_scaling_z].minset(),value__flt_number[phasertng_cell_content_scaling_z].minval(),value__flt_number[phasertng_cell_content_scaling_z].maxset(),value__flt_number[phasertng_cell_content_scaling_z].maxval()));
          }
        }
        else if (keyIs("cell_scale")) {
          constexpr char phasertng_cell_scale[] = ".phasertng.cell_scale.";
          check_unique(phasertng_cell_scale);
          value__flt_paired[phasertng_cell_scale].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_cell_scale].minset(),value__flt_paired[phasertng_cell_scale].minval(),value__flt_paired[phasertng_cell_scale].maxset(),value__flt_paired[phasertng_cell_scale].maxval()));
        }
        else if (keyIs("cluster_compound")) {
          compulsoryKey(input_stream,{"filename"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_cluster_compound_filename[] = ".phasertng.cluster_compound.filename.";
            check_unique(phasertng_cluster_compound_filename);
            value__path[phasertng_cluster_compound_filename].set_value(variable,get_path(input_stream));
          }
        }
        else if (keyIs("coiled_coil")) {
          constexpr char phasertng_coiled_coil[] = ".phasertng.coiled_coil.";
          check_unique(phasertng_coiled_coil);
          value__boolean[phasertng_coiled_coil].set_value(variable,get_boolean(input_stream));
        }
        else if (keyIs("composition")) {
          compulsoryKey(input_stream,{"map","maximum_z","multiplicity_restriction","number_in_asymmetric_unit","order_z_expansion_ratio","solvent_range","sort_by_probability","use_order_z_expansion_ratio"});
          if (keyIsEol()) return false;
          else if (keyIs("map")) {
            constexpr char phasertng_composition_map[] = ".phasertng.composition.map.";
            check_unique(phasertng_composition_map);
            value__boolean[phasertng_composition_map].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_z")) {
            constexpr char phasertng_composition_maximum_z[] = ".phasertng.composition.maximum_z.";
            check_unique(phasertng_composition_maximum_z);
            value__int_number[phasertng_composition_maximum_z].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_composition_maximum_z].minset(),value__int_number[phasertng_composition_maximum_z].minval(),value__int_number[phasertng_composition_maximum_z].maxset(),value__int_number[phasertng_composition_maximum_z].maxval()));
          }
          else if (keyIs("multiplicity_restriction")) {
            constexpr char phasertng_composition_multiplicity_restriction[] = ".phasertng.composition.multiplicity_restriction.";
            check_unique(phasertng_composition_multiplicity_restriction);
            value__int_number[phasertng_composition_multiplicity_restriction].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_composition_multiplicity_restriction].minset(),value__int_number[phasertng_composition_multiplicity_restriction].minval(),value__int_number[phasertng_composition_multiplicity_restriction].maxset(),value__int_number[phasertng_composition_multiplicity_restriction].maxval()));
          }
          else if (keyIs("number_in_asymmetric_unit")) {
            constexpr char phasertng_composition_number_in_asymmetric_unit[] = ".phasertng.composition.number_in_asymmetric_unit.";
            check_unique(phasertng_composition_number_in_asymmetric_unit);
            value__flt_number[phasertng_composition_number_in_asymmetric_unit].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_composition_number_in_asymmetric_unit].minset(),value__flt_number[phasertng_composition_number_in_asymmetric_unit].minval(),value__flt_number[phasertng_composition_number_in_asymmetric_unit].maxset(),value__flt_number[phasertng_composition_number_in_asymmetric_unit].maxval()));
          }
          else if (keyIs("order_z_expansion_ratio")) {
            constexpr char phasertng_composition_order_z_expansion_ratio[] = ".phasertng.composition.order_z_expansion_ratio.";
            check_unique(phasertng_composition_order_z_expansion_ratio);
            value__int_number[phasertng_composition_order_z_expansion_ratio].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_composition_order_z_expansion_ratio].minset(),value__int_number[phasertng_composition_order_z_expansion_ratio].minval(),value__int_number[phasertng_composition_order_z_expansion_ratio].maxset(),value__int_number[phasertng_composition_order_z_expansion_ratio].maxval()));
          }
          else if (keyIs("solvent_range")) {
            constexpr char phasertng_composition_solvent_range[] = ".phasertng.composition.solvent_range.";
            check_unique(phasertng_composition_solvent_range);
            value__percent_paired[phasertng_composition_solvent_range].set_value(variable,get_percent_paired(input_stream));
          }
          else if (keyIs("sort_by_probability")) {
            constexpr char phasertng_composition_sort_by_probability[] = ".phasertng.composition.sort_by_probability.";
            check_unique(phasertng_composition_sort_by_probability);
            value__boolean[phasertng_composition_sort_by_probability].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("use_order_z_expansion_ratio")) {
            constexpr char phasertng_composition_use_order_z_expansion_ratio[] = ".phasertng.composition.use_order_z_expansion_ratio.";
            check_unique(phasertng_composition_use_order_z_expansion_ratio);
            value__boolean[phasertng_composition_use_order_z_expansion_ratio].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("cross_rotation_function")) {
          compulsoryKey(input_stream,{"filename"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_cross_rotation_function_filename[] = ".phasertng.cross_rotation_function.filename.";
            check_unique(phasertng_cross_rotation_function_filename);
            value__path[phasertng_cross_rotation_function_filename].set_value(variable,get_path(input_stream));
          }
        }
        else if (keyIs("dag")) {
          compulsoryKey(input_stream,{"cards","consecutive","counters","maximum_stored","phil","read_file","skip_if_complete","tncs_present_in_model"});
          if (keyIsEol()) return false;
          else if (keyIs("cards")) {
            compulsoryKey(input_stream,{"filename_in_subdir","subdir"});
            if (keyIsEol()) return false;
            else if (keyIs("filename_in_subdir")) {
              constexpr char phasertng_dag_cards_filename_in_subdir[] = ".phasertng.dag.cards.filename_in_subdir.";
              check_unique(phasertng_dag_cards_filename_in_subdir);
              value__path[phasertng_dag_cards_filename_in_subdir].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("subdir")) {
              constexpr char phasertng_dag_cards_subdir[] = ".phasertng.dag.cards.subdir.";
              check_unique(phasertng_dag_cards_subdir);
              value__path[phasertng_dag_cards_subdir].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("consecutive")) {
            constexpr char phasertng_dag_consecutive[] = ".phasertng.dag.consecutive.";
            check_unique(phasertng_dag_consecutive);
            value__boolean[phasertng_dag_consecutive].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("counters")) {
            constexpr char phasertng_dag_counters[] = ".phasertng.dag.counters.";
            check_unique(phasertng_dag_counters);
            value__int_vector[phasertng_dag_counters].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_dag_counters].minset(),value__int_vector[phasertng_dag_counters].minval(),value__int_vector[phasertng_dag_counters].maxset(),value__int_vector[phasertng_dag_counters].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_dag_maximum_stored[] = ".phasertng.dag.maximum_stored.";
            check_unique(phasertng_dag_maximum_stored);
            value__int_number[phasertng_dag_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_dag_maximum_stored].minset(),value__int_number[phasertng_dag_maximum_stored].minval(),value__int_number[phasertng_dag_maximum_stored].maxset(),value__int_number[phasertng_dag_maximum_stored].maxval()));
          }
          else if (keyIs("phil")) {
            compulsoryKey(input_stream,{"filename"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_dag_phil_filename[] = ".phasertng.dag.phil.filename.";
              check_unique(phasertng_dag_phil_filename);
              value__path[phasertng_dag_phil_filename].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("read_file")) {
            constexpr char phasertng_dag_read_file[] = ".phasertng.dag.read_file.";
            check_unique(phasertng_dag_read_file);
            value__boolean[phasertng_dag_read_file].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("skip_if_complete")) {
            constexpr char phasertng_dag_skip_if_complete[] = ".phasertng.dag.skip_if_complete.";
            check_unique(phasertng_dag_skip_if_complete);
            value__boolean[phasertng_dag_skip_if_complete].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("tncs_present_in_model")) {
            constexpr char phasertng_dag_tncs_present_in_model[] = ".phasertng.dag.tncs_present_in_model.";
            check_unique(phasertng_dag_tncs_present_in_model);
            value__boolean[phasertng_dag_tncs_present_in_model].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("data")) {
          compulsoryKey(input_stream,{"anomalous","french_wilson","intensities","map","number_of_reflections","resolution_available","unitcell"});
          if (keyIsEol()) return false;
          else if (keyIs("anomalous")) {
            constexpr char phasertng_data_anomalous[] = ".phasertng.data.anomalous.";
            check_unique(phasertng_data_anomalous);
            value__boolean[phasertng_data_anomalous].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("french_wilson")) {
            constexpr char phasertng_data_french_wilson[] = ".phasertng.data.french_wilson.";
            check_unique(phasertng_data_french_wilson);
            value__boolean[phasertng_data_french_wilson].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("intensities")) {
            constexpr char phasertng_data_intensities[] = ".phasertng.data.intensities.";
            check_unique(phasertng_data_intensities);
            value__boolean[phasertng_data_intensities].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("map")) {
            constexpr char phasertng_data_map[] = ".phasertng.data.map.";
            check_unique(phasertng_data_map);
            value__boolean[phasertng_data_map].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("number_of_reflections")) {
            constexpr char phasertng_data_number_of_reflections[] = ".phasertng.data.number_of_reflections.";
            check_unique(phasertng_data_number_of_reflections);
            value__int_number[phasertng_data_number_of_reflections].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_data_number_of_reflections].minset(),value__int_number[phasertng_data_number_of_reflections].minval(),value__int_number[phasertng_data_number_of_reflections].maxset(),value__int_number[phasertng_data_number_of_reflections].maxval()));
          }
          else if (keyIs("resolution_available")) {
            constexpr char phasertng_data_resolution_available[] = ".phasertng.data.resolution_available.";
            check_unique(phasertng_data_resolution_available);
            value__flt_paired[phasertng_data_resolution_available].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_data_resolution_available].minset(),value__flt_paired[phasertng_data_resolution_available].minval(),value__flt_paired[phasertng_data_resolution_available].maxset(),value__flt_paired[phasertng_data_resolution_available].maxval()));
          }
          else if (keyIs("unitcell")) {
            constexpr char phasertng_data_unitcell[] = ".phasertng.data.unitcell.";
            check_unique(phasertng_data_unitcell);
            value__flt_cell[phasertng_data_unitcell].set_value(variable,get_flt_cell(input_stream));
          }
        }
        else if (keyIs("ensemble")) {
          compulsoryKey(input_stream,{"convert_rmsd_to_bfac","disable_percent_deviation_check","disable_structure_factors_correlated_check","filename","percent_deviation","selenomethionine","tag","vrms_estimate"});
          if (keyIsEol()) return false;
          else if (keyIs("convert_rmsd_to_bfac")) {
            constexpr char phasertng_ensemble_convert_rmsd_to_bfac[] = ".phasertng.ensemble.convert_rmsd_to_bfac.";
            check_unique(phasertng_ensemble_convert_rmsd_to_bfac);
            value__boolean[phasertng_ensemble_convert_rmsd_to_bfac].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("disable_percent_deviation_check")) {
            constexpr char phasertng_ensemble_disable_percent_deviation_check[] = ".phasertng.ensemble.disable_percent_deviation_check.";
            check_unique(phasertng_ensemble_disable_percent_deviation_check);
            value__boolean[phasertng_ensemble_disable_percent_deviation_check].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("disable_structure_factors_correlated_check")) {
            constexpr char phasertng_ensemble_disable_structure_factors_correlated_check[] = ".phasertng.ensemble.disable_structure_factors_correlated_check.";
            check_unique(phasertng_ensemble_disable_structure_factors_correlated_check);
            value__boolean[phasertng_ensemble_disable_structure_factors_correlated_check].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("filename")) {
            constexpr char phasertng_ensemble_filename[] = ".phasertng.ensemble.filename.";
            check_unique(phasertng_ensemble_filename);
            value__path[phasertng_ensemble_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("percent_deviation")) {
            constexpr char phasertng_ensemble_percent_deviation[] = ".phasertng.ensemble.percent_deviation.";
            check_unique(phasertng_ensemble_percent_deviation);
            value__percent[phasertng_ensemble_percent_deviation].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("selenomethionine")) {
            constexpr char phasertng_ensemble_selenomethionine[] = ".phasertng.ensemble.selenomethionine.";
            check_unique(phasertng_ensemble_selenomethionine);
            value__boolean[phasertng_ensemble_selenomethionine].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_ensemble_tag[] = ".phasertng.ensemble.tag.";
            check_unique(phasertng_ensemble_tag);
            value__string[phasertng_ensemble_tag].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("vrms_estimate")) {
            constexpr char phasertng_ensemble_vrms_estimate[] = ".phasertng.ensemble.vrms_estimate.";
            check_unique(phasertng_ensemble_vrms_estimate);
            value__flt_numbers[phasertng_ensemble_vrms_estimate].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_ensemble_vrms_estimate].minset(),value__flt_numbers[phasertng_ensemble_vrms_estimate].minval(),value__flt_numbers[phasertng_ensemble_vrms_estimate].maxset(),value__flt_numbers[phasertng_ensemble_vrms_estimate].maxval()));
          }
        }
        else if (keyIs("expected")) {
          compulsoryKey(input_stream,{"atom","ellg","filenames_in_subdir","llg","mapllg","subdir"});
          if (keyIsEol()) return false;
          else if (keyIs("atom")) {
            compulsoryKey(input_stream,{"bfactor","minimum_search_number","rmsd","scatterer","target"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_expected_atom_bfactor[] = ".phasertng.expected.atom.bfactor.";
              check_unique(phasertng_expected_atom_bfactor);
              value__flt_numbers[phasertng_expected_atom_bfactor].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_expected_atom_bfactor].minset(),value__flt_numbers[phasertng_expected_atom_bfactor].minval(),value__flt_numbers[phasertng_expected_atom_bfactor].maxset(),value__flt_numbers[phasertng_expected_atom_bfactor].maxval()));
            }
            else if (keyIs("minimum_search_number")) {
              constexpr char phasertng_expected_atom_minimum_search_number[] = ".phasertng.expected.atom.minimum_search_number.";
              check_unique(phasertng_expected_atom_minimum_search_number);
              value__int_number[phasertng_expected_atom_minimum_search_number].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_expected_atom_minimum_search_number].minset(),value__int_number[phasertng_expected_atom_minimum_search_number].minval(),value__int_number[phasertng_expected_atom_minimum_search_number].maxset(),value__int_number[phasertng_expected_atom_minimum_search_number].maxval()));
            }
            else if (keyIs("rmsd")) {
              constexpr char phasertng_expected_atom_rmsd[] = ".phasertng.expected.atom.rmsd.";
              check_unique(phasertng_expected_atom_rmsd);
              value__flt_number[phasertng_expected_atom_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_atom_rmsd].minset(),value__flt_number[phasertng_expected_atom_rmsd].minval(),value__flt_number[phasertng_expected_atom_rmsd].maxset(),value__flt_number[phasertng_expected_atom_rmsd].maxval()));
            }
            else if (keyIs("scatterer")) {
              constexpr char phasertng_expected_atom_scatterer[] = ".phasertng.expected.atom.scatterer.";
              check_unique(phasertng_expected_atom_scatterer);
              value__strings[phasertng_expected_atom_scatterer].set_value(variable,get_strings(input_stream));
            }
            else if (keyIs("target")) {
              constexpr char phasertng_expected_atom_target[] = ".phasertng.expected.atom.target.";
              check_unique(phasertng_expected_atom_target);
              value__flt_number[phasertng_expected_atom_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_atom_target].minset(),value__flt_number[phasertng_expected_atom_target].minval(),value__flt_number[phasertng_expected_atom_target].maxset(),value__flt_number[phasertng_expected_atom_target].maxval()));
            }
          }
          else if (keyIs("ellg")) {
            compulsoryKey(input_stream,{"maximum_first_search","maximum_stored","signal_resolution_factor","speed_versus_accuracy","target"});
            if (keyIsEol()) return false;
            else if (keyIs("maximum_first_search")) {
              constexpr char phasertng_expected_ellg_maximum_first_search[] = ".phasertng.expected.ellg.maximum_first_search.";
              check_unique(phasertng_expected_ellg_maximum_first_search);
              value__int_paired[phasertng_expected_ellg_maximum_first_search].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_expected_ellg_maximum_first_search].minset(),value__int_paired[phasertng_expected_ellg_maximum_first_search].minval(),value__int_paired[phasertng_expected_ellg_maximum_first_search].maxset(),value__int_paired[phasertng_expected_ellg_maximum_first_search].maxval()));
            }
            else if (keyIs("maximum_stored")) {
              constexpr char phasertng_expected_ellg_maximum_stored[] = ".phasertng.expected.ellg.maximum_stored.";
              check_unique(phasertng_expected_ellg_maximum_stored);
              value__int_number[phasertng_expected_ellg_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_expected_ellg_maximum_stored].minset(),value__int_number[phasertng_expected_ellg_maximum_stored].minval(),value__int_number[phasertng_expected_ellg_maximum_stored].maxset(),value__int_number[phasertng_expected_ellg_maximum_stored].maxval()));
            }
            else if (keyIs("signal_resolution_factor")) {
              constexpr char phasertng_expected_ellg_signal_resolution_factor[] = ".phasertng.expected.ellg.signal_resolution_factor.";
              check_unique(phasertng_expected_ellg_signal_resolution_factor);
              value__flt_number[phasertng_expected_ellg_signal_resolution_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_ellg_signal_resolution_factor].minset(),value__flt_number[phasertng_expected_ellg_signal_resolution_factor].minval(),value__flt_number[phasertng_expected_ellg_signal_resolution_factor].maxset(),value__flt_number[phasertng_expected_ellg_signal_resolution_factor].maxval()));
            }
            else if (keyIs("speed_versus_accuracy")) {
              compulsoryKey(input_stream,{"aellg","eellg","ellg"});
              constexpr char phasertng_expected_ellg_speed_versus_accuracy[] = ".phasertng.expected.ellg.speed_versus_accuracy.";
              check_unique(phasertng_expected_ellg_speed_versus_accuracy);
              value__choice[phasertng_expected_ellg_speed_versus_accuracy].set_value(variable,string_value);
            }
            else if (keyIs("target")) {
              constexpr char phasertng_expected_ellg_target[] = ".phasertng.expected.ellg.target.";
              check_unique(phasertng_expected_ellg_target);
              value__flt_number[phasertng_expected_ellg_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_ellg_target].minset(),value__flt_number[phasertng_expected_ellg_target].minval(),value__flt_number[phasertng_expected_ellg_target].maxset(),value__flt_number[phasertng_expected_ellg_target].maxval()));
            }
          }
          else if (keyIs("filenames_in_subdir")) {
            constexpr char phasertng_expected_filenames_in_subdir[] = ".phasertng.expected.filenames_in_subdir.";
            array__string[phasertng_expected_filenames_in_subdir].push_back(type::string(phasertng_expected_filenames_in_subdir));
            array__string[phasertng_expected_filenames_in_subdir].back().set_value(variable,get_string(input_stream));
          }
          else if (keyIs("llg")) {
            compulsoryKey(input_stream,{"ellg","id","rellg","rmsd","tag"});
            if (keyIsEol()) return false;
            else if (keyIs("ellg")) {
              constexpr char phasertng_expected_llg_ellg[] = ".phasertng.expected.llg.ellg.";
              check_unique(phasertng_expected_llg_ellg);
              value__flt_number[phasertng_expected_llg_ellg].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_llg_ellg].minset(),value__flt_number[phasertng_expected_llg_ellg].minval(),value__flt_number[phasertng_expected_llg_ellg].maxset(),value__flt_number[phasertng_expected_llg_ellg].maxval()));
            }
            else if (keyIs("id")) {
              constexpr char phasertng_expected_llg_id[] = ".phasertng.expected.llg.id.";
              check_unique(phasertng_expected_llg_id);
              value__uuid_number[phasertng_expected_llg_id].set_value(variable,get_uuid_number(input_stream,value__uuid_number[phasertng_expected_llg_id].minset(),value__uuid_number[phasertng_expected_llg_id].minval(),value__uuid_number[phasertng_expected_llg_id].maxset(),value__uuid_number[phasertng_expected_llg_id].maxval()));
            }
            else if (keyIs("rellg")) {
              constexpr char phasertng_expected_llg_rellg[] = ".phasertng.expected.llg.rellg.";
              check_unique(phasertng_expected_llg_rellg);
              value__flt_number[phasertng_expected_llg_rellg].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_llg_rellg].minset(),value__flt_number[phasertng_expected_llg_rellg].minval(),value__flt_number[phasertng_expected_llg_rellg].maxset(),value__flt_number[phasertng_expected_llg_rellg].maxval()));
            }
            else if (keyIs("rmsd")) {
              constexpr char phasertng_expected_llg_rmsd[] = ".phasertng.expected.llg.rmsd.";
              check_unique(phasertng_expected_llg_rmsd);
              value__flt_number[phasertng_expected_llg_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_llg_rmsd].minset(),value__flt_number[phasertng_expected_llg_rmsd].minval(),value__flt_number[phasertng_expected_llg_rmsd].maxset(),value__flt_number[phasertng_expected_llg_rmsd].maxval()));
            }
            else if (keyIs("tag")) {
              constexpr char phasertng_expected_llg_tag[] = ".phasertng.expected.llg.tag.";
              check_unique(phasertng_expected_llg_tag);
              value__string[phasertng_expected_llg_tag].set_value(variable,get_string(input_stream));
            }
          }
          else if (keyIs("mapllg")) {
            compulsoryKey(input_stream,{"target"});
            if (keyIsEol()) return false;
            else if (keyIs("target")) {
              constexpr char phasertng_expected_mapllg_target[] = ".phasertng.expected.mapllg.target.";
              check_unique(phasertng_expected_mapllg_target);
              value__flt_number[phasertng_expected_mapllg_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_expected_mapllg_target].minset(),value__flt_number[phasertng_expected_mapllg_target].minval(),value__flt_number[phasertng_expected_mapllg_target].maxset(),value__flt_number[phasertng_expected_mapllg_target].maxval()));
            }
          }
          else if (keyIs("subdir")) {
            constexpr char phasertng_expected_subdir[] = ".phasertng.expected.subdir.";
            array__string[phasertng_expected_subdir].push_back(type::string(phasertng_expected_subdir));
            array__string[phasertng_expected_subdir].back().set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("fast_rotation_function")) {
          compulsoryKey(input_stream,{"cluster","cluster_back","helix_factor","maps","maximum_printed","maximum_stored","percent","purge_duplicates","sampling","signal_resolution","top_not_in_pose","vrms_estimates"});
          if (keyIsEol()) return false;
          else if (keyIs("cluster")) {
            constexpr char phasertng_fast_rotation_function_cluster[] = ".phasertng.fast_rotation_function.cluster.";
            check_unique(phasertng_fast_rotation_function_cluster);
            value__boolean[phasertng_fast_rotation_function_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_fast_rotation_function_cluster_back[] = ".phasertng.fast_rotation_function.cluster_back.";
            check_unique(phasertng_fast_rotation_function_cluster_back);
            value__int_number[phasertng_fast_rotation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_rotation_function_cluster_back].minset(),value__int_number[phasertng_fast_rotation_function_cluster_back].minval(),value__int_number[phasertng_fast_rotation_function_cluster_back].maxset(),value__int_number[phasertng_fast_rotation_function_cluster_back].maxval()));
          }
          else if (keyIs("helix_factor")) {
            constexpr char phasertng_fast_rotation_function_helix_factor[] = ".phasertng.fast_rotation_function.helix_factor.";
            check_unique(phasertng_fast_rotation_function_helix_factor);
            value__flt_number[phasertng_fast_rotation_function_helix_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_rotation_function_helix_factor].minset(),value__flt_number[phasertng_fast_rotation_function_helix_factor].minval(),value__flt_number[phasertng_fast_rotation_function_helix_factor].maxset(),value__flt_number[phasertng_fast_rotation_function_helix_factor].maxval()));
          }
          else if (keyIs("maps")) {
            compulsoryKey(input_stream,{"id"});
            variable.push_back("id");
            constexpr char phasertng_fast_rotation_function_maps_id[] = ".phasertng.fast_rotation_function.maps.id.";
            array__uuid_number[phasertng_fast_rotation_function_maps_id].push_back(type::uuid_number(phasertng_fast_rotation_function_maps_id,true,0,false,0));
            array__uuid_number[phasertng_fast_rotation_function_maps_id].back().set_value(variable,get_uuid_number(input_stream,array__uuid_number[phasertng_fast_rotation_function_maps_id].back().minset(),array__uuid_number[phasertng_fast_rotation_function_maps_id].back().minval(),array__uuid_number[phasertng_fast_rotation_function_maps_id].back().maxset(),array__uuid_number[phasertng_fast_rotation_function_maps_id].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"tag"});
            variable.push_back("tag");
            constexpr char phasertng_fast_rotation_function_maps_tag[] = ".phasertng.fast_rotation_function.maps.tag.";
            array__string[phasertng_fast_rotation_function_maps_tag].push_back(type::string(phasertng_fast_rotation_function_maps_tag));
            array__string[phasertng_fast_rotation_function_maps_tag].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_fast_rotation_function_maximum_printed[] = ".phasertng.fast_rotation_function.maximum_printed.";
            check_unique(phasertng_fast_rotation_function_maximum_printed);
            value__int_number[phasertng_fast_rotation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_rotation_function_maximum_printed].minset(),value__int_number[phasertng_fast_rotation_function_maximum_printed].minval(),value__int_number[phasertng_fast_rotation_function_maximum_printed].maxset(),value__int_number[phasertng_fast_rotation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_fast_rotation_function_maximum_stored[] = ".phasertng.fast_rotation_function.maximum_stored.";
            check_unique(phasertng_fast_rotation_function_maximum_stored);
            value__int_number[phasertng_fast_rotation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_rotation_function_maximum_stored].minset(),value__int_number[phasertng_fast_rotation_function_maximum_stored].minval(),value__int_number[phasertng_fast_rotation_function_maximum_stored].maxset(),value__int_number[phasertng_fast_rotation_function_maximum_stored].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_fast_rotation_function_percent[] = ".phasertng.fast_rotation_function.percent.";
            check_unique(phasertng_fast_rotation_function_percent);
            value__percent[phasertng_fast_rotation_function_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("purge_duplicates")) {
            constexpr char phasertng_fast_rotation_function_purge_duplicates[] = ".phasertng.fast_rotation_function.purge_duplicates.";
            check_unique(phasertng_fast_rotation_function_purge_duplicates);
            value__boolean[phasertng_fast_rotation_function_purge_duplicates].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_fast_rotation_function_sampling[] = ".phasertng.fast_rotation_function.sampling.";
            check_unique(phasertng_fast_rotation_function_sampling);
            value__flt_number[phasertng_fast_rotation_function_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_rotation_function_sampling].minset(),value__flt_number[phasertng_fast_rotation_function_sampling].minval(),value__flt_number[phasertng_fast_rotation_function_sampling].maxset(),value__flt_number[phasertng_fast_rotation_function_sampling].maxval()));
          }
          else if (keyIs("signal_resolution")) {
            constexpr char phasertng_fast_rotation_function_signal_resolution[] = ".phasertng.fast_rotation_function.signal_resolution.";
            check_unique(phasertng_fast_rotation_function_signal_resolution);
            value__flt_number[phasertng_fast_rotation_function_signal_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_rotation_function_signal_resolution].minset(),value__flt_number[phasertng_fast_rotation_function_signal_resolution].minval(),value__flt_number[phasertng_fast_rotation_function_signal_resolution].maxset(),value__flt_number[phasertng_fast_rotation_function_signal_resolution].maxval()));
          }
          else if (keyIs("top_not_in_pose")) {
            constexpr char phasertng_fast_rotation_function_top_not_in_pose[] = ".phasertng.fast_rotation_function.top_not_in_pose.";
            check_unique(phasertng_fast_rotation_function_top_not_in_pose);
            value__boolean[phasertng_fast_rotation_function_top_not_in_pose].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("vrms_estimates")) {
            constexpr char phasertng_fast_rotation_function_vrms_estimates[] = ".phasertng.fast_rotation_function.vrms_estimates.";
            check_unique(phasertng_fast_rotation_function_vrms_estimates);
            value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates].minset(),value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates].minval(),value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates].maxset(),value__flt_numbers[phasertng_fast_rotation_function_vrms_estimates].maxval()));
          }
        }
        else if (keyIs("fast_translation_function")) {
          compulsoryKey(input_stream,{"add_pose_orientations","cluster","cluster_back","helix_factor","maximum_printed","maximum_stored","packing","percent","report_cluster","sampling","sampling_factor","single_atom","stop","write_maps"});
          if (keyIsEol()) return false;
          else if (keyIs("add_pose_orientations")) {
            constexpr char phasertng_fast_translation_function_add_pose_orientations[] = ".phasertng.fast_translation_function.add_pose_orientations.";
            check_unique(phasertng_fast_translation_function_add_pose_orientations);
            value__boolean[phasertng_fast_translation_function_add_pose_orientations].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster")) {
            constexpr char phasertng_fast_translation_function_cluster[] = ".phasertng.fast_translation_function.cluster.";
            check_unique(phasertng_fast_translation_function_cluster);
            value__boolean[phasertng_fast_translation_function_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_fast_translation_function_cluster_back[] = ".phasertng.fast_translation_function.cluster_back.";
            check_unique(phasertng_fast_translation_function_cluster_back);
            value__int_number[phasertng_fast_translation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_translation_function_cluster_back].minset(),value__int_number[phasertng_fast_translation_function_cluster_back].minval(),value__int_number[phasertng_fast_translation_function_cluster_back].maxset(),value__int_number[phasertng_fast_translation_function_cluster_back].maxval()));
          }
          else if (keyIs("helix_factor")) {
            constexpr char phasertng_fast_translation_function_helix_factor[] = ".phasertng.fast_translation_function.helix_factor.";
            check_unique(phasertng_fast_translation_function_helix_factor);
            value__flt_number[phasertng_fast_translation_function_helix_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_translation_function_helix_factor].minset(),value__flt_number[phasertng_fast_translation_function_helix_factor].minval(),value__flt_number[phasertng_fast_translation_function_helix_factor].maxset(),value__flt_number[phasertng_fast_translation_function_helix_factor].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_fast_translation_function_maximum_printed[] = ".phasertng.fast_translation_function.maximum_printed.";
            check_unique(phasertng_fast_translation_function_maximum_printed);
            value__int_number[phasertng_fast_translation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_translation_function_maximum_printed].minset(),value__int_number[phasertng_fast_translation_function_maximum_printed].minval(),value__int_number[phasertng_fast_translation_function_maximum_printed].maxset(),value__int_number[phasertng_fast_translation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_fast_translation_function_maximum_stored[] = ".phasertng.fast_translation_function.maximum_stored.";
            check_unique(phasertng_fast_translation_function_maximum_stored);
            value__int_number[phasertng_fast_translation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fast_translation_function_maximum_stored].minset(),value__int_number[phasertng_fast_translation_function_maximum_stored].minval(),value__int_number[phasertng_fast_translation_function_maximum_stored].maxset(),value__int_number[phasertng_fast_translation_function_maximum_stored].maxval()));
          }
          else if (keyIs("packing")) {
            compulsoryKey(input_stream,{"low_tfz","use"});
            if (keyIsEol()) return false;
            else if (keyIs("low_tfz")) {
              constexpr char phasertng_fast_translation_function_packing_low_tfz[] = ".phasertng.fast_translation_function.packing.low_tfz.";
              check_unique(phasertng_fast_translation_function_packing_low_tfz);
              value__flt_number[phasertng_fast_translation_function_packing_low_tfz].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_translation_function_packing_low_tfz].minset(),value__flt_number[phasertng_fast_translation_function_packing_low_tfz].minval(),value__flt_number[phasertng_fast_translation_function_packing_low_tfz].maxset(),value__flt_number[phasertng_fast_translation_function_packing_low_tfz].maxval()));
            }
            else if (keyIs("use")) {
              constexpr char phasertng_fast_translation_function_packing_use[] = ".phasertng.fast_translation_function.packing.use.";
              check_unique(phasertng_fast_translation_function_packing_use);
              value__boolean[phasertng_fast_translation_function_packing_use].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_fast_translation_function_percent[] = ".phasertng.fast_translation_function.percent.";
            check_unique(phasertng_fast_translation_function_percent);
            value__percent[phasertng_fast_translation_function_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("report_cluster")) {
            constexpr char phasertng_fast_translation_function_report_cluster[] = ".phasertng.fast_translation_function.report_cluster.";
            check_unique(phasertng_fast_translation_function_report_cluster);
            value__boolean[phasertng_fast_translation_function_report_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_fast_translation_function_sampling[] = ".phasertng.fast_translation_function.sampling.";
            check_unique(phasertng_fast_translation_function_sampling);
            value__flt_number[phasertng_fast_translation_function_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_translation_function_sampling].minset(),value__flt_number[phasertng_fast_translation_function_sampling].minval(),value__flt_number[phasertng_fast_translation_function_sampling].maxset(),value__flt_number[phasertng_fast_translation_function_sampling].maxval()));
          }
          else if (keyIs("sampling_factor")) {
            constexpr char phasertng_fast_translation_function_sampling_factor[] = ".phasertng.fast_translation_function.sampling_factor.";
            check_unique(phasertng_fast_translation_function_sampling_factor);
            value__flt_number[phasertng_fast_translation_function_sampling_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_translation_function_sampling_factor].minset(),value__flt_number[phasertng_fast_translation_function_sampling_factor].minval(),value__flt_number[phasertng_fast_translation_function_sampling_factor].maxset(),value__flt_number[phasertng_fast_translation_function_sampling_factor].maxval()));
          }
          else if (keyIs("single_atom")) {
            compulsoryKey(input_stream,{"scatterer","use"});
            if (keyIsEol()) return false;
            else if (keyIs("scatterer")) {
              constexpr char phasertng_fast_translation_function_single_atom_scatterer[] = ".phasertng.fast_translation_function.single_atom.scatterer.";
              check_unique(phasertng_fast_translation_function_single_atom_scatterer);
              value__string[phasertng_fast_translation_function_single_atom_scatterer].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("use")) {
              constexpr char phasertng_fast_translation_function_single_atom_use[] = ".phasertng.fast_translation_function.single_atom.use.";
              check_unique(phasertng_fast_translation_function_single_atom_use);
              value__boolean[phasertng_fast_translation_function_single_atom_use].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("stop")) {
            compulsoryKey(input_stream,{"best_packing_tfz_over","rf_percent_under","rf_zscore_under"});
            if (keyIsEol()) return false;
            else if (keyIs("best_packing_tfz_over")) {
              constexpr char phasertng_fast_translation_function_stop_best_packing_tfz_over[] = ".phasertng.fast_translation_function.stop.best_packing_tfz_over.";
              check_unique(phasertng_fast_translation_function_stop_best_packing_tfz_over);
              value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over].minset(),value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over].minval(),value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over].maxset(),value__flt_number[phasertng_fast_translation_function_stop_best_packing_tfz_over].maxval()));
            }
            else if (keyIs("rf_percent_under")) {
              constexpr char phasertng_fast_translation_function_stop_rf_percent_under[] = ".phasertng.fast_translation_function.stop.rf_percent_under.";
              check_unique(phasertng_fast_translation_function_stop_rf_percent_under);
              value__percent[phasertng_fast_translation_function_stop_rf_percent_under].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("rf_zscore_under")) {
              constexpr char phasertng_fast_translation_function_stop_rf_zscore_under[] = ".phasertng.fast_translation_function.stop.rf_zscore_under.";
              check_unique(phasertng_fast_translation_function_stop_rf_zscore_under);
              value__percent[phasertng_fast_translation_function_stop_rf_zscore_under].set_value(variable,get_percent(input_stream));
            }
          }
          else if (keyIs("write_maps")) {
            constexpr char phasertng_fast_translation_function_write_maps[] = ".phasertng.fast_translation_function.write_maps.";
            check_unique(phasertng_fast_translation_function_write_maps);
            value__boolean[phasertng_fast_translation_function_write_maps].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("file_discovery")) {
          compulsoryKey(input_stream,{"directory","filename_in_directory","validated"});
          if (keyIsEol()) return false;
          else if (keyIs("directory")) {
            constexpr char phasertng_file_discovery_directory[] = ".phasertng.file_discovery.directory.";
            check_unique(phasertng_file_discovery_directory);
            value__path[phasertng_file_discovery_directory].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("filename_in_directory")) {
            constexpr char phasertng_file_discovery_filename_in_directory[] = ".phasertng.file_discovery.filename_in_directory.";
            array__path[phasertng_file_discovery_filename_in_directory].push_back(type::path(phasertng_file_discovery_filename_in_directory));
            array__path[phasertng_file_discovery_filename_in_directory].back().set_value(variable,get_path(input_stream));
          }
          else if (keyIs("validated")) {
            compulsoryKey(input_stream,{"mtz_files","pdb_files","seq_files"});
            if (keyIsEol()) return false;
            else if (keyIs("mtz_files")) {
              constexpr char phasertng_file_discovery_validated_mtz_files[] = ".phasertng.file_discovery.validated.mtz_files.";
              array__string[phasertng_file_discovery_validated_mtz_files].push_back(type::string(phasertng_file_discovery_validated_mtz_files));
              array__string[phasertng_file_discovery_validated_mtz_files].back().set_value(variable,get_string(input_stream));
            }
            else if (keyIs("pdb_files")) {
              constexpr char phasertng_file_discovery_validated_pdb_files[] = ".phasertng.file_discovery.validated.pdb_files.";
              array__string[phasertng_file_discovery_validated_pdb_files].push_back(type::string(phasertng_file_discovery_validated_pdb_files));
              array__string[phasertng_file_discovery_validated_pdb_files].back().set_value(variable,get_string(input_stream));
            }
            else if (keyIs("seq_files")) {
              constexpr char phasertng_file_discovery_validated_seq_files[] = ".phasertng.file_discovery.validated.seq_files.";
              array__string[phasertng_file_discovery_validated_seq_files].push_back(type::string(phasertng_file_discovery_validated_seq_files));
              array__string[phasertng_file_discovery_validated_seq_files].back().set_value(variable,get_string(input_stream));
            }
          }
        }
        else if (keyIs("fuse_translation_function")) {
          compulsoryKey(input_stream,{"complete","fused","maximum_remaining","models","packing_percent","tfz"});
          if (keyIsEol()) return false;
          else if (keyIs("complete")) {
            constexpr char phasertng_fuse_translation_function_complete[] = ".phasertng.fuse_translation_function.complete.";
            check_unique(phasertng_fuse_translation_function_complete);
            value__boolean[phasertng_fuse_translation_function_complete].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("fused")) {
            constexpr char phasertng_fuse_translation_function_fused[] = ".phasertng.fuse_translation_function.fused.";
            check_unique(phasertng_fuse_translation_function_fused);
            value__boolean[phasertng_fuse_translation_function_fused].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_remaining")) {
            constexpr char phasertng_fuse_translation_function_maximum_remaining[] = ".phasertng.fuse_translation_function.maximum_remaining.";
            check_unique(phasertng_fuse_translation_function_maximum_remaining);
            value__int_number[phasertng_fuse_translation_function_maximum_remaining].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_fuse_translation_function_maximum_remaining].minset(),value__int_number[phasertng_fuse_translation_function_maximum_remaining].minval(),value__int_number[phasertng_fuse_translation_function_maximum_remaining].maxset(),value__int_number[phasertng_fuse_translation_function_maximum_remaining].maxval()));
          }
          else if (keyIs("models")) {
            compulsoryKey(input_stream,{"tags"});
            if (keyIsEol()) return false;
            else if (keyIs("tags")) {
              constexpr char phasertng_fuse_translation_function_models_tags[] = ".phasertng.fuse_translation_function.models.tags.";
              check_unique(phasertng_fuse_translation_function_models_tags);
              value__strings[phasertng_fuse_translation_function_models_tags].set_value(variable,get_strings(input_stream));
            }
          }
          else if (keyIs("packing_percent")) {
            constexpr char phasertng_fuse_translation_function_packing_percent[] = ".phasertng.fuse_translation_function.packing_percent.";
            check_unique(phasertng_fuse_translation_function_packing_percent);
            value__percent[phasertng_fuse_translation_function_packing_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("tfz")) {
            constexpr char phasertng_fuse_translation_function_tfz[] = ".phasertng.fuse_translation_function.tfz.";
            check_unique(phasertng_fuse_translation_function_tfz);
            value__flt_number[phasertng_fuse_translation_function_tfz].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_fuse_translation_function_tfz].minset(),value__flt_number[phasertng_fuse_translation_function_tfz].minval(),value__flt_number[phasertng_fuse_translation_function_tfz].maxset(),value__flt_number[phasertng_fuse_translation_function_tfz].maxval()));
          }
        }
        else if (keyIs("graph_report")) {
          compulsoryKey(input_stream,{"best_solution","filestem"});
          if (keyIsEol()) return false;
          else if (keyIs("best_solution")) {
            constexpr char phasertng_graph_report_best_solution[] = ".phasertng.graph_report.best_solution.";
            check_unique(phasertng_graph_report_best_solution);
            value__int_number[phasertng_graph_report_best_solution].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_graph_report_best_solution].minset(),value__int_number[phasertng_graph_report_best_solution].minval(),value__int_number[phasertng_graph_report_best_solution].maxset(),value__int_number[phasertng_graph_report_best_solution].maxval()));
          }
          else if (keyIs("filestem")) {
            constexpr char phasertng_graph_report_filestem[] = ".phasertng.graph_report.filestem.";
            check_unique(phasertng_graph_report_filestem);
            value__string[phasertng_graph_report_filestem].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("grid_search")) {
          compulsoryKey(input_stream,{"maximum_stored","ncyc","poses","range","resolution","sampling","selection"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_grid_search_maximum_stored[] = ".phasertng.grid_search.maximum_stored.";
            check_unique(phasertng_grid_search_maximum_stored);
            value__int_number[phasertng_grid_search_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_grid_search_maximum_stored].minset(),value__int_number[phasertng_grid_search_maximum_stored].minval(),value__int_number[phasertng_grid_search_maximum_stored].maxset(),value__int_number[phasertng_grid_search_maximum_stored].maxval()));
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_grid_search_ncyc[] = ".phasertng.grid_search.ncyc.";
            check_unique(phasertng_grid_search_ncyc);
            value__int_number[phasertng_grid_search_ncyc].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_grid_search_ncyc].minset(),value__int_number[phasertng_grid_search_ncyc].minval(),value__int_number[phasertng_grid_search_ncyc].maxset(),value__int_number[phasertng_grid_search_ncyc].maxval()));
          }
          else if (keyIs("poses")) {
            constexpr char phasertng_grid_search_poses[] = ".phasertng.grid_search.poses.";
            check_unique(phasertng_grid_search_poses);
            value__int_numbers[phasertng_grid_search_poses].set_value(variable,get_int_numbers(input_stream,value__int_numbers[phasertng_grid_search_poses].minset(),value__int_numbers[phasertng_grid_search_poses].minval(),value__int_numbers[phasertng_grid_search_poses].maxset(),value__int_numbers[phasertng_grid_search_poses].maxval()));
          }
          else if (keyIs("range")) {
            constexpr char phasertng_grid_search_range[] = ".phasertng.grid_search.range.";
            check_unique(phasertng_grid_search_range);
            value__flt_paired[phasertng_grid_search_range].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_grid_search_range].minset(),value__flt_paired[phasertng_grid_search_range].minval(),value__flt_paired[phasertng_grid_search_range].maxset(),value__flt_paired[phasertng_grid_search_range].maxval()));
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_grid_search_resolution[] = ".phasertng.grid_search.resolution.";
            check_unique(phasertng_grid_search_resolution);
            value__flt_number[phasertng_grid_search_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_grid_search_resolution].minset(),value__flt_number[phasertng_grid_search_resolution].minval(),value__flt_number[phasertng_grid_search_resolution].maxset(),value__flt_number[phasertng_grid_search_resolution].maxval()));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_grid_search_sampling[] = ".phasertng.grid_search.sampling.";
            check_unique(phasertng_grid_search_sampling);
            value__flt_paired[phasertng_grid_search_sampling].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_grid_search_sampling].minset(),value__flt_paired[phasertng_grid_search_sampling].minval(),value__flt_paired[phasertng_grid_search_sampling].maxset(),value__flt_paired[phasertng_grid_search_sampling].maxval()));
          }
          else if (keyIs("selection")) {
            compulsoryKey(input_stream,{"tra","rot","both","trarot","rottra"});
            constexpr char phasertng_grid_search_selection[] = ".phasertng.grid_search.selection.";
            check_unique(phasertng_grid_search_selection);
            value__choice[phasertng_grid_search_selection].set_value(variable,string_value);
          }
        }
        else if (keyIs("gyration")) {
          compulsoryKey(input_stream,{"id"});
          variable.push_back("id");
          constexpr char phasertng_gyration_id[] = ".phasertng.gyration.id.";
          array__uuid_number[phasertng_gyration_id].push_back(type::uuid_number(phasertng_gyration_id,true,0,false,0));
          array__uuid_number[phasertng_gyration_id].back().set_value(variable,get_uuid_number(input_stream,array__uuid_number[phasertng_gyration_id].back().minset(),array__uuid_number[phasertng_gyration_id].back().minval(),array__uuid_number[phasertng_gyration_id].back().maxset(),array__uuid_number[phasertng_gyration_id].back().maxval()));
          variable.pop_back();
          compulsoryKey(input_stream,{"tag"});
          variable.push_back("tag");
          constexpr char phasertng_gyration_tag[] = ".phasertng.gyration.tag.";
          array__string[phasertng_gyration_tag].push_back(type::string(phasertng_gyration_tag));
          array__string[phasertng_gyration_tag].back().set_value(variable,get_string(input_stream));
          variable.pop_back();
        }
        else if (keyIs("hklin")) {
          compulsoryKey(input_stream,{"filename","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_hklin_filename[] = ".phasertng.hklin.filename.";
            check_unique(phasertng_hklin_filename);
            value__path[phasertng_hklin_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_hklin_tag[] = ".phasertng.hklin.tag.";
            check_unique(phasertng_hklin_tag);
            value__string[phasertng_hklin_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("hklout")) {
          compulsoryKey(input_stream,{"filename"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_hklout_filename[] = ".phasertng.hklout.filename.";
            check_unique(phasertng_hklout_filename);
            value__path[phasertng_hklout_filename].set_value(variable,get_path(input_stream));
          }
        }
        else if (keyIs("information")) {
          compulsoryKey(input_stream,{"ellg_fraction_anomalous","ellg_rms_factor","expected_information_content","information_content","sad_expected_llg","sad_information_content"});
          if (keyIsEol()) return false;
          else if (keyIs("ellg_fraction_anomalous")) {
            constexpr char phasertng_information_ellg_fraction_anomalous[] = ".phasertng.information.ellg_fraction_anomalous.";
            check_unique(phasertng_information_ellg_fraction_anomalous);
            value__flt_number[phasertng_information_ellg_fraction_anomalous].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_ellg_fraction_anomalous].minset(),value__flt_number[phasertng_information_ellg_fraction_anomalous].minval(),value__flt_number[phasertng_information_ellg_fraction_anomalous].maxset(),value__flt_number[phasertng_information_ellg_fraction_anomalous].maxval()));
          }
          else if (keyIs("ellg_rms_factor")) {
            constexpr char phasertng_information_ellg_rms_factor[] = ".phasertng.information.ellg_rms_factor.";
            check_unique(phasertng_information_ellg_rms_factor);
            value__flt_number[phasertng_information_ellg_rms_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_ellg_rms_factor].minset(),value__flt_number[phasertng_information_ellg_rms_factor].minval(),value__flt_number[phasertng_information_ellg_rms_factor].maxset(),value__flt_number[phasertng_information_ellg_rms_factor].maxval()));
          }
          else if (keyIs("expected_information_content")) {
            constexpr char phasertng_information_expected_information_content[] = ".phasertng.information.expected_information_content.";
            check_unique(phasertng_information_expected_information_content);
            value__flt_number[phasertng_information_expected_information_content].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_expected_information_content].minset(),value__flt_number[phasertng_information_expected_information_content].minval(),value__flt_number[phasertng_information_expected_information_content].maxset(),value__flt_number[phasertng_information_expected_information_content].maxval()));
          }
          else if (keyIs("information_content")) {
            constexpr char phasertng_information_information_content[] = ".phasertng.information.information_content.";
            check_unique(phasertng_information_information_content);
            value__flt_number[phasertng_information_information_content].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_information_content].minset(),value__flt_number[phasertng_information_information_content].minval(),value__flt_number[phasertng_information_information_content].maxset(),value__flt_number[phasertng_information_information_content].maxval()));
          }
          else if (keyIs("sad_expected_llg")) {
            constexpr char phasertng_information_sad_expected_llg[] = ".phasertng.information.sad_expected_llg.";
            check_unique(phasertng_information_sad_expected_llg);
            value__flt_number[phasertng_information_sad_expected_llg].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_sad_expected_llg].minset(),value__flt_number[phasertng_information_sad_expected_llg].minval(),value__flt_number[phasertng_information_sad_expected_llg].maxset(),value__flt_number[phasertng_information_sad_expected_llg].maxval()));
          }
          else if (keyIs("sad_information_content")) {
            constexpr char phasertng_information_sad_information_content[] = ".phasertng.information.sad_information_content.";
            check_unique(phasertng_information_sad_information_content);
            value__flt_number[phasertng_information_sad_information_content].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_information_sad_information_content].minset(),value__flt_number[phasertng_information_sad_information_content].minval(),value__flt_number[phasertng_information_sad_information_content].maxset(),value__flt_number[phasertng_information_sad_information_content].maxval()));
          }
        }
        else if (keyIs("isostructure")) {
          compulsoryKey(input_stream,{"delta","found","perfect_match_cc","tutorial"});
          if (keyIsEol()) return false;
          else if (keyIs("delta")) {
            compulsoryKey(input_stream,{"matches_cut_off","maximum_angstroms","maximum_degrees","maximum_explored","maximum_ncdist","minimum_correlation_coefficient","resolution"});
            if (keyIsEol()) return false;
            else if (keyIs("matches_cut_off")) {
              constexpr char phasertng_isostructure_delta_matches_cut_off[] = ".phasertng.isostructure.delta.matches_cut_off.";
              check_unique(phasertng_isostructure_delta_matches_cut_off);
              value__percent[phasertng_isostructure_delta_matches_cut_off].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("maximum_angstroms")) {
              constexpr char phasertng_isostructure_delta_maximum_angstroms[] = ".phasertng.isostructure.delta.maximum_angstroms.";
              check_unique(phasertng_isostructure_delta_maximum_angstroms);
              value__int_number[phasertng_isostructure_delta_maximum_angstroms].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_isostructure_delta_maximum_angstroms].minset(),value__int_number[phasertng_isostructure_delta_maximum_angstroms].minval(),value__int_number[phasertng_isostructure_delta_maximum_angstroms].maxset(),value__int_number[phasertng_isostructure_delta_maximum_angstroms].maxval()));
            }
            else if (keyIs("maximum_degrees")) {
              constexpr char phasertng_isostructure_delta_maximum_degrees[] = ".phasertng.isostructure.delta.maximum_degrees.";
              check_unique(phasertng_isostructure_delta_maximum_degrees);
              value__int_number[phasertng_isostructure_delta_maximum_degrees].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_isostructure_delta_maximum_degrees].minset(),value__int_number[phasertng_isostructure_delta_maximum_degrees].minval(),value__int_number[phasertng_isostructure_delta_maximum_degrees].maxset(),value__int_number[phasertng_isostructure_delta_maximum_degrees].maxval()));
            }
            else if (keyIs("maximum_explored")) {
              constexpr char phasertng_isostructure_delta_maximum_explored[] = ".phasertng.isostructure.delta.maximum_explored.";
              check_unique(phasertng_isostructure_delta_maximum_explored);
              value__int_number[phasertng_isostructure_delta_maximum_explored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_isostructure_delta_maximum_explored].minset(),value__int_number[phasertng_isostructure_delta_maximum_explored].minval(),value__int_number[phasertng_isostructure_delta_maximum_explored].maxset(),value__int_number[phasertng_isostructure_delta_maximum_explored].maxval()));
            }
            else if (keyIs("maximum_ncdist")) {
              constexpr char phasertng_isostructure_delta_maximum_ncdist[] = ".phasertng.isostructure.delta.maximum_ncdist.";
              check_unique(phasertng_isostructure_delta_maximum_ncdist);
              value__flt_number[phasertng_isostructure_delta_maximum_ncdist].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_isostructure_delta_maximum_ncdist].minset(),value__flt_number[phasertng_isostructure_delta_maximum_ncdist].minval(),value__flt_number[phasertng_isostructure_delta_maximum_ncdist].maxset(),value__flt_number[phasertng_isostructure_delta_maximum_ncdist].maxval()));
            }
            else if (keyIs("minimum_correlation_coefficient")) {
              constexpr char phasertng_isostructure_delta_minimum_correlation_coefficient[] = ".phasertng.isostructure.delta.minimum_correlation_coefficient.";
              check_unique(phasertng_isostructure_delta_minimum_correlation_coefficient);
              value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient].minset(),value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient].minval(),value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient].maxset(),value__flt_number[phasertng_isostructure_delta_minimum_correlation_coefficient].maxval()));
            }
            else if (keyIs("resolution")) {
              constexpr char phasertng_isostructure_delta_resolution[] = ".phasertng.isostructure.delta.resolution.";
              check_unique(phasertng_isostructure_delta_resolution);
              value__flt_number[phasertng_isostructure_delta_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_isostructure_delta_resolution].minset(),value__flt_number[phasertng_isostructure_delta_resolution].minval(),value__flt_number[phasertng_isostructure_delta_resolution].maxset(),value__flt_number[phasertng_isostructure_delta_resolution].maxval()));
            }
          }
          else if (keyIs("found")) {
            compulsoryKey(input_stream,{"match","pdbid","perfect_match","pointgroup","spacegroup"});
            if (keyIsEol()) return false;
            else if (keyIs("match")) {
              constexpr char phasertng_isostructure_found_match[] = ".phasertng.isostructure.found.match.";
              check_unique(phasertng_isostructure_found_match);
              value__boolean[phasertng_isostructure_found_match].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("pdbid")) {
              constexpr char phasertng_isostructure_found_pdbid[] = ".phasertng.isostructure.found.pdbid.";
              check_unique(phasertng_isostructure_found_pdbid);
              value__string[phasertng_isostructure_found_pdbid].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("perfect_match")) {
              constexpr char phasertng_isostructure_found_perfect_match[] = ".phasertng.isostructure.found.perfect_match.";
              check_unique(phasertng_isostructure_found_perfect_match);
              value__boolean[phasertng_isostructure_found_perfect_match].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("pointgroup")) {
              compulsoryKey(input_stream,{"same","expansion","reduction"});
              constexpr char phasertng_isostructure_found_pointgroup[] = ".phasertng.isostructure.found.pointgroup.";
              check_unique(phasertng_isostructure_found_pointgroup);
              value__choice[phasertng_isostructure_found_pointgroup].set_value(variable,string_value);
            }
            else if (keyIs("spacegroup")) {
              constexpr char phasertng_isostructure_found_spacegroup[] = ".phasertng.isostructure.found.spacegroup.";
              check_unique(phasertng_isostructure_found_spacegroup);
              value__string[phasertng_isostructure_found_spacegroup].set_value(variable,get_string(input_stream));
            }
          }
          else if (keyIs("perfect_match_cc")) {
            constexpr char phasertng_isostructure_perfect_match_cc[] = ".phasertng.isostructure.perfect_match_cc.";
            check_unique(phasertng_isostructure_perfect_match_cc);
            value__flt_number[phasertng_isostructure_perfect_match_cc].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_isostructure_perfect_match_cc].minset(),value__flt_number[phasertng_isostructure_perfect_match_cc].minval(),value__flt_number[phasertng_isostructure_perfect_match_cc].maxset(),value__flt_number[phasertng_isostructure_perfect_match_cc].maxval()));
          }
          else if (keyIs("tutorial")) {
            compulsoryKey(input_stream,{"force_match","ignore_best","ignore_match"});
            if (keyIsEol()) return false;
            else if (keyIs("force_match")) {
              constexpr char phasertng_isostructure_tutorial_force_match[] = ".phasertng.isostructure.tutorial.force_match.";
              check_unique(phasertng_isostructure_tutorial_force_match);
              value__string[phasertng_isostructure_tutorial_force_match].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("ignore_best")) {
              constexpr char phasertng_isostructure_tutorial_ignore_best[] = ".phasertng.isostructure.tutorial.ignore_best.";
              check_unique(phasertng_isostructure_tutorial_ignore_best);
              value__boolean[phasertng_isostructure_tutorial_ignore_best].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("ignore_match")) {
              constexpr char phasertng_isostructure_tutorial_ignore_match[] = ".phasertng.isostructure.tutorial.ignore_match.";
              check_unique(phasertng_isostructure_tutorial_ignore_match);
              value__strings[phasertng_isostructure_tutorial_ignore_match].set_value(variable,get_strings(input_stream));
            }
          }
        }
        else if (keyIs("jog_refinement")) {
          compulsoryKey(input_stream,{"distance","jogged","maximum_printed","maximum_stored","percent","sampling","sampling_factor"});
          if (keyIsEol()) return false;
          else if (keyIs("distance")) {
            constexpr char phasertng_jog_refinement_distance[] = ".phasertng.jog_refinement.distance.";
            check_unique(phasertng_jog_refinement_distance);
            value__flt_paired[phasertng_jog_refinement_distance].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_jog_refinement_distance].minset(),value__flt_paired[phasertng_jog_refinement_distance].minval(),value__flt_paired[phasertng_jog_refinement_distance].maxset(),value__flt_paired[phasertng_jog_refinement_distance].maxval()));
          }
          else if (keyIs("jogged")) {
            constexpr char phasertng_jog_refinement_jogged[] = ".phasertng.jog_refinement.jogged.";
            check_unique(phasertng_jog_refinement_jogged);
            value__boolean[phasertng_jog_refinement_jogged].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_jog_refinement_maximum_printed[] = ".phasertng.jog_refinement.maximum_printed.";
            check_unique(phasertng_jog_refinement_maximum_printed);
            value__int_number[phasertng_jog_refinement_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_jog_refinement_maximum_printed].minset(),value__int_number[phasertng_jog_refinement_maximum_printed].minval(),value__int_number[phasertng_jog_refinement_maximum_printed].maxset(),value__int_number[phasertng_jog_refinement_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_jog_refinement_maximum_stored[] = ".phasertng.jog_refinement.maximum_stored.";
            check_unique(phasertng_jog_refinement_maximum_stored);
            value__int_number[phasertng_jog_refinement_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_jog_refinement_maximum_stored].minset(),value__int_number[phasertng_jog_refinement_maximum_stored].minval(),value__int_number[phasertng_jog_refinement_maximum_stored].maxset(),value__int_number[phasertng_jog_refinement_maximum_stored].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_jog_refinement_percent[] = ".phasertng.jog_refinement.percent.";
            check_unique(phasertng_jog_refinement_percent);
            value__percent[phasertng_jog_refinement_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_jog_refinement_sampling[] = ".phasertng.jog_refinement.sampling.";
            check_unique(phasertng_jog_refinement_sampling);
            value__flt_number[phasertng_jog_refinement_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_jog_refinement_sampling].minset(),value__flt_number[phasertng_jog_refinement_sampling].minval(),value__flt_number[phasertng_jog_refinement_sampling].maxset(),value__flt_number[phasertng_jog_refinement_sampling].maxval()));
          }
          else if (keyIs("sampling_factor")) {
            constexpr char phasertng_jog_refinement_sampling_factor[] = ".phasertng.jog_refinement.sampling_factor.";
            check_unique(phasertng_jog_refinement_sampling_factor);
            value__flt_number[phasertng_jog_refinement_sampling_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_jog_refinement_sampling_factor].minset(),value__flt_number[phasertng_jog_refinement_sampling_factor].minval(),value__flt_number[phasertng_jog_refinement_sampling_factor].maxset(),value__flt_number[phasertng_jog_refinement_sampling_factor].maxval()));
          }
        }
        else if (keyIs("labin")) {
          compulsoryKey(input_stream,{"aniso","anisobeta","bin","both","cent","delfwt","delphwt","dobsnat","dobsneg","dobspos","einfo","eps","fcnat","fcneg","fcpos","feffnat","feffneg","feffpos","fnat","fneg","fom","fpos","free","fwt","hla","hlb","hlc","hld","inat","ineg","info","ipos","llgf","llgph","mapdobs","mapf","mapph","phicnat","phicneg","phicpos","phsr","phwt","plus","resn","sefom","sellg","siga","sigfnat","sigfneg","sigfpos","siginat","sigineg","sigipos","sinfo","ssqr","tbin","teps"});
          if (keyIsEol()) return false;
          else if (keyIs("aniso")) {
            constexpr char phasertng_labin_aniso[] = ".phasertng.labin.aniso.";
            check_unique(phasertng_labin_aniso);
            value__mtzcol[phasertng_labin_aniso].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("anisobeta")) {
            constexpr char phasertng_labin_anisobeta[] = ".phasertng.labin.anisobeta.";
            check_unique(phasertng_labin_anisobeta);
            value__mtzcol[phasertng_labin_anisobeta].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("bin")) {
            constexpr char phasertng_labin_bin[] = ".phasertng.labin.bin.";
            check_unique(phasertng_labin_bin);
            value__mtzcol[phasertng_labin_bin].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("both")) {
            constexpr char phasertng_labin_both[] = ".phasertng.labin.both.";
            check_unique(phasertng_labin_both);
            value__mtzcol[phasertng_labin_both].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("cent")) {
            constexpr char phasertng_labin_cent[] = ".phasertng.labin.cent.";
            check_unique(phasertng_labin_cent);
            value__mtzcol[phasertng_labin_cent].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("delfwt")) {
            constexpr char phasertng_labin_delfwt[] = ".phasertng.labin.delfwt.";
            check_unique(phasertng_labin_delfwt);
            value__mtzcol[phasertng_labin_delfwt].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("delphwt")) {
            constexpr char phasertng_labin_delphwt[] = ".phasertng.labin.delphwt.";
            check_unique(phasertng_labin_delphwt);
            value__mtzcol[phasertng_labin_delphwt].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("dobsnat")) {
            constexpr char phasertng_labin_dobsnat[] = ".phasertng.labin.dobsnat.";
            check_unique(phasertng_labin_dobsnat);
            value__mtzcol[phasertng_labin_dobsnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("dobsneg")) {
            constexpr char phasertng_labin_dobsneg[] = ".phasertng.labin.dobsneg.";
            check_unique(phasertng_labin_dobsneg);
            value__mtzcol[phasertng_labin_dobsneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("dobspos")) {
            constexpr char phasertng_labin_dobspos[] = ".phasertng.labin.dobspos.";
            check_unique(phasertng_labin_dobspos);
            value__mtzcol[phasertng_labin_dobspos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("einfo")) {
            constexpr char phasertng_labin_einfo[] = ".phasertng.labin.einfo.";
            check_unique(phasertng_labin_einfo);
            value__mtzcol[phasertng_labin_einfo].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("eps")) {
            constexpr char phasertng_labin_eps[] = ".phasertng.labin.eps.";
            check_unique(phasertng_labin_eps);
            value__mtzcol[phasertng_labin_eps].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fcnat")) {
            constexpr char phasertng_labin_fcnat[] = ".phasertng.labin.fcnat.";
            check_unique(phasertng_labin_fcnat);
            value__mtzcol[phasertng_labin_fcnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fcneg")) {
            constexpr char phasertng_labin_fcneg[] = ".phasertng.labin.fcneg.";
            check_unique(phasertng_labin_fcneg);
            value__mtzcol[phasertng_labin_fcneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fcpos")) {
            constexpr char phasertng_labin_fcpos[] = ".phasertng.labin.fcpos.";
            check_unique(phasertng_labin_fcpos);
            value__mtzcol[phasertng_labin_fcpos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("feffnat")) {
            constexpr char phasertng_labin_feffnat[] = ".phasertng.labin.feffnat.";
            check_unique(phasertng_labin_feffnat);
            value__mtzcol[phasertng_labin_feffnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("feffneg")) {
            constexpr char phasertng_labin_feffneg[] = ".phasertng.labin.feffneg.";
            check_unique(phasertng_labin_feffneg);
            value__mtzcol[phasertng_labin_feffneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("feffpos")) {
            constexpr char phasertng_labin_feffpos[] = ".phasertng.labin.feffpos.";
            check_unique(phasertng_labin_feffpos);
            value__mtzcol[phasertng_labin_feffpos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fnat")) {
            constexpr char phasertng_labin_fnat[] = ".phasertng.labin.fnat.";
            check_unique(phasertng_labin_fnat);
            value__mtzcol[phasertng_labin_fnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fneg")) {
            constexpr char phasertng_labin_fneg[] = ".phasertng.labin.fneg.";
            check_unique(phasertng_labin_fneg);
            value__mtzcol[phasertng_labin_fneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fom")) {
            constexpr char phasertng_labin_fom[] = ".phasertng.labin.fom.";
            check_unique(phasertng_labin_fom);
            value__mtzcol[phasertng_labin_fom].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fpos")) {
            constexpr char phasertng_labin_fpos[] = ".phasertng.labin.fpos.";
            check_unique(phasertng_labin_fpos);
            value__mtzcol[phasertng_labin_fpos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("free")) {
            constexpr char phasertng_labin_free[] = ".phasertng.labin.free.";
            check_unique(phasertng_labin_free);
            value__mtzcol[phasertng_labin_free].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("fwt")) {
            constexpr char phasertng_labin_fwt[] = ".phasertng.labin.fwt.";
            check_unique(phasertng_labin_fwt);
            value__mtzcol[phasertng_labin_fwt].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("hla")) {
            constexpr char phasertng_labin_hla[] = ".phasertng.labin.hla.";
            check_unique(phasertng_labin_hla);
            value__mtzcol[phasertng_labin_hla].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("hlb")) {
            constexpr char phasertng_labin_hlb[] = ".phasertng.labin.hlb.";
            check_unique(phasertng_labin_hlb);
            value__mtzcol[phasertng_labin_hlb].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("hlc")) {
            constexpr char phasertng_labin_hlc[] = ".phasertng.labin.hlc.";
            check_unique(phasertng_labin_hlc);
            value__mtzcol[phasertng_labin_hlc].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("hld")) {
            constexpr char phasertng_labin_hld[] = ".phasertng.labin.hld.";
            check_unique(phasertng_labin_hld);
            value__mtzcol[phasertng_labin_hld].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("inat")) {
            constexpr char phasertng_labin_inat[] = ".phasertng.labin.inat.";
            check_unique(phasertng_labin_inat);
            value__mtzcol[phasertng_labin_inat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("ineg")) {
            constexpr char phasertng_labin_ineg[] = ".phasertng.labin.ineg.";
            check_unique(phasertng_labin_ineg);
            value__mtzcol[phasertng_labin_ineg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("info")) {
            constexpr char phasertng_labin_info[] = ".phasertng.labin.info.";
            check_unique(phasertng_labin_info);
            value__mtzcol[phasertng_labin_info].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("ipos")) {
            constexpr char phasertng_labin_ipos[] = ".phasertng.labin.ipos.";
            check_unique(phasertng_labin_ipos);
            value__mtzcol[phasertng_labin_ipos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("llgf")) {
            constexpr char phasertng_labin_llgf[] = ".phasertng.labin.llgf.";
            check_unique(phasertng_labin_llgf);
            value__mtzcol[phasertng_labin_llgf].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("llgph")) {
            constexpr char phasertng_labin_llgph[] = ".phasertng.labin.llgph.";
            check_unique(phasertng_labin_llgph);
            value__mtzcol[phasertng_labin_llgph].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("mapdobs")) {
            constexpr char phasertng_labin_mapdobs[] = ".phasertng.labin.mapdobs.";
            check_unique(phasertng_labin_mapdobs);
            value__mtzcol[phasertng_labin_mapdobs].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("mapf")) {
            constexpr char phasertng_labin_mapf[] = ".phasertng.labin.mapf.";
            check_unique(phasertng_labin_mapf);
            value__mtzcol[phasertng_labin_mapf].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("mapph")) {
            constexpr char phasertng_labin_mapph[] = ".phasertng.labin.mapph.";
            check_unique(phasertng_labin_mapph);
            value__mtzcol[phasertng_labin_mapph].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("phicnat")) {
            constexpr char phasertng_labin_phicnat[] = ".phasertng.labin.phicnat.";
            check_unique(phasertng_labin_phicnat);
            value__mtzcol[phasertng_labin_phicnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("phicneg")) {
            constexpr char phasertng_labin_phicneg[] = ".phasertng.labin.phicneg.";
            check_unique(phasertng_labin_phicneg);
            value__mtzcol[phasertng_labin_phicneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("phicpos")) {
            constexpr char phasertng_labin_phicpos[] = ".phasertng.labin.phicpos.";
            check_unique(phasertng_labin_phicpos);
            value__mtzcol[phasertng_labin_phicpos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("phsr")) {
            constexpr char phasertng_labin_phsr[] = ".phasertng.labin.phsr.";
            check_unique(phasertng_labin_phsr);
            value__mtzcol[phasertng_labin_phsr].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("phwt")) {
            constexpr char phasertng_labin_phwt[] = ".phasertng.labin.phwt.";
            check_unique(phasertng_labin_phwt);
            value__mtzcol[phasertng_labin_phwt].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("plus")) {
            constexpr char phasertng_labin_plus[] = ".phasertng.labin.plus.";
            check_unique(phasertng_labin_plus);
            value__mtzcol[phasertng_labin_plus].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("resn")) {
            constexpr char phasertng_labin_resn[] = ".phasertng.labin.resn.";
            check_unique(phasertng_labin_resn);
            value__mtzcol[phasertng_labin_resn].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sefom")) {
            constexpr char phasertng_labin_sefom[] = ".phasertng.labin.sefom.";
            check_unique(phasertng_labin_sefom);
            value__mtzcol[phasertng_labin_sefom].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sellg")) {
            constexpr char phasertng_labin_sellg[] = ".phasertng.labin.sellg.";
            check_unique(phasertng_labin_sellg);
            value__mtzcol[phasertng_labin_sellg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("siga")) {
            constexpr char phasertng_labin_siga[] = ".phasertng.labin.siga.";
            check_unique(phasertng_labin_siga);
            value__mtzcol[phasertng_labin_siga].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sigfnat")) {
            constexpr char phasertng_labin_sigfnat[] = ".phasertng.labin.sigfnat.";
            check_unique(phasertng_labin_sigfnat);
            value__mtzcol[phasertng_labin_sigfnat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sigfneg")) {
            constexpr char phasertng_labin_sigfneg[] = ".phasertng.labin.sigfneg.";
            check_unique(phasertng_labin_sigfneg);
            value__mtzcol[phasertng_labin_sigfneg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sigfpos")) {
            constexpr char phasertng_labin_sigfpos[] = ".phasertng.labin.sigfpos.";
            check_unique(phasertng_labin_sigfpos);
            value__mtzcol[phasertng_labin_sigfpos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("siginat")) {
            constexpr char phasertng_labin_siginat[] = ".phasertng.labin.siginat.";
            check_unique(phasertng_labin_siginat);
            value__mtzcol[phasertng_labin_siginat].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sigineg")) {
            constexpr char phasertng_labin_sigineg[] = ".phasertng.labin.sigineg.";
            check_unique(phasertng_labin_sigineg);
            value__mtzcol[phasertng_labin_sigineg].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sigipos")) {
            constexpr char phasertng_labin_sigipos[] = ".phasertng.labin.sigipos.";
            check_unique(phasertng_labin_sigipos);
            value__mtzcol[phasertng_labin_sigipos].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("sinfo")) {
            constexpr char phasertng_labin_sinfo[] = ".phasertng.labin.sinfo.";
            check_unique(phasertng_labin_sinfo);
            value__mtzcol[phasertng_labin_sinfo].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("ssqr")) {
            constexpr char phasertng_labin_ssqr[] = ".phasertng.labin.ssqr.";
            check_unique(phasertng_labin_ssqr);
            value__mtzcol[phasertng_labin_ssqr].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("tbin")) {
            constexpr char phasertng_labin_tbin[] = ".phasertng.labin.tbin.";
            check_unique(phasertng_labin_tbin);
            value__mtzcol[phasertng_labin_tbin].set_value(variable,get_mtzcol(input_stream));
          }
          else if (keyIs("teps")) {
            constexpr char phasertng_labin_teps[] = ".phasertng.labin.teps.";
            check_unique(phasertng_labin_teps);
            value__mtzcol[phasertng_labin_teps].set_value(variable,get_mtzcol(input_stream));
          }
        }
        else if (keyIs("macaniso")) {
          compulsoryKey(input_stream,{"best_curve_restraint","macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","protocol","resolution","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("best_curve_restraint")) {
            constexpr char phasertng_macaniso_best_curve_restraint[] = ".phasertng.macaniso.best_curve_restraint.";
            check_unique(phasertng_macaniso_best_curve_restraint);
            value__boolean[phasertng_macaniso_best_curve_restraint].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macaniso_macrocycle1[] = ".phasertng.macaniso.macrocycle1.";
            value__choice[phasertng_macaniso_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macaniso_macrocycle2[] = ".phasertng.macaniso.macrocycle2.";
            value__choice[phasertng_macaniso_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macaniso_macrocycle3[] = ".phasertng.macaniso.macrocycle3.";
            value__choice[phasertng_macaniso_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macaniso_minimizer[] = ".phasertng.macaniso.minimizer.";
            check_unique(phasertng_macaniso_minimizer);
            value__choice[phasertng_macaniso_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macaniso_ncyc[] = ".phasertng.macaniso.ncyc.";
            check_unique(phasertng_macaniso_ncyc);
            value__int_vector[phasertng_macaniso_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macaniso_ncyc].minset(),value__int_vector[phasertng_macaniso_ncyc].minval(),value__int_vector[phasertng_macaniso_ncyc].maxset(),value__int_vector[phasertng_macaniso_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macaniso_protocol[] = ".phasertng.macaniso.protocol.";
            check_unique(phasertng_macaniso_protocol);
            value__choice[phasertng_macaniso_protocol].set_value(variable,string_value);
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_macaniso_resolution[] = ".phasertng.macaniso.resolution.";
            check_unique(phasertng_macaniso_resolution);
            value__flt_number[phasertng_macaniso_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macaniso_resolution].minset(),value__flt_number[phasertng_macaniso_resolution].minval(),value__flt_number[phasertng_macaniso_resolution].maxset(),value__flt_number[phasertng_macaniso_resolution].maxval()));
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macaniso_study_parameters[] = ".phasertng.macaniso.study_parameters.";
            check_unique(phasertng_macaniso_study_parameters);
            value__boolean[phasertng_macaniso_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macbox")) {
          compulsoryKey(input_stream,{"best_curve_restraint","macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","protocol","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("best_curve_restraint")) {
            constexpr char phasertng_macbox_best_curve_restraint[] = ".phasertng.macbox.best_curve_restraint.";
            check_unique(phasertng_macbox_best_curve_restraint);
            value__boolean[phasertng_macbox_best_curve_restraint].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macbox_macrocycle1[] = ".phasertng.macbox.macrocycle1.";
            value__choice[phasertng_macbox_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macbox_macrocycle2[] = ".phasertng.macbox.macrocycle2.";
            value__choice[phasertng_macbox_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","scale","aniso","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macbox_macrocycle3[] = ".phasertng.macbox.macrocycle3.";
            value__choice[phasertng_macbox_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macbox_minimizer[] = ".phasertng.macbox.minimizer.";
            check_unique(phasertng_macbox_minimizer);
            value__choice[phasertng_macbox_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macbox_ncyc[] = ".phasertng.macbox.ncyc.";
            check_unique(phasertng_macbox_ncyc);
            value__int_vector[phasertng_macbox_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macbox_ncyc].minset(),value__int_vector[phasertng_macbox_ncyc].minval(),value__int_vector[phasertng_macbox_ncyc].maxset(),value__int_vector[phasertng_macbox_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macbox_protocol[] = ".phasertng.macbox.protocol.";
            check_unique(phasertng_macbox_protocol);
            value__choice[phasertng_macbox_protocol].set_value(variable,string_value);
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macbox_study_parameters[] = ".phasertng.macbox.study_parameters.";
            check_unique(phasertng_macbox_study_parameters);
            value__boolean[phasertng_macbox_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macgyre")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","maximum_printed","minimizer","ncyc","protocol","purge_duplicates","refine_helices","restraint_sigma","small_target","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","vrms"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macgyre_macrocycle1[] = ".phasertng.macgyre.macrocycle1.";
            value__choice[phasertng_macgyre_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","vrms"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macgyre_macrocycle2[] = ".phasertng.macgyre.macrocycle2.";
            value__choice[phasertng_macgyre_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","vrms"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macgyre_macrocycle3[] = ".phasertng.macgyre.macrocycle3.";
            value__choice[phasertng_macgyre_macrocycle3].set_value(choices);
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_macgyre_maximum_printed[] = ".phasertng.macgyre.maximum_printed.";
            check_unique(phasertng_macgyre_maximum_printed);
            value__int_number[phasertng_macgyre_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macgyre_maximum_printed].minset(),value__int_number[phasertng_macgyre_maximum_printed].minval(),value__int_number[phasertng_macgyre_maximum_printed].maxset(),value__int_number[phasertng_macgyre_maximum_printed].maxval()));
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macgyre_minimizer[] = ".phasertng.macgyre.minimizer.";
            check_unique(phasertng_macgyre_minimizer);
            value__choice[phasertng_macgyre_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macgyre_ncyc[] = ".phasertng.macgyre.ncyc.";
            check_unique(phasertng_macgyre_ncyc);
            value__int_vector[phasertng_macgyre_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macgyre_ncyc].minset(),value__int_vector[phasertng_macgyre_ncyc].minval(),value__int_vector[phasertng_macgyre_ncyc].maxset(),value__int_vector[phasertng_macgyre_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macgyre_protocol[] = ".phasertng.macgyre.protocol.";
            check_unique(phasertng_macgyre_protocol);
            value__choice[phasertng_macgyre_protocol].set_value(variable,string_value);
          }
          else if (keyIs("purge_duplicates")) {
            constexpr char phasertng_macgyre_purge_duplicates[] = ".phasertng.macgyre.purge_duplicates.";
            check_unique(phasertng_macgyre_purge_duplicates);
            value__boolean[phasertng_macgyre_purge_duplicates].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("refine_helices")) {
            constexpr char phasertng_macgyre_refine_helices[] = ".phasertng.macgyre.refine_helices.";
            check_unique(phasertng_macgyre_refine_helices);
            value__boolean[phasertng_macgyre_refine_helices].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"position","rotation"});
            if (keyIsEol()) return false;
            else if (keyIs("position")) {
              constexpr char phasertng_macgyre_restraint_sigma_position[] = ".phasertng.macgyre.restraint_sigma.position.";
              check_unique(phasertng_macgyre_restraint_sigma_position);
              value__flt_number[phasertng_macgyre_restraint_sigma_position].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macgyre_restraint_sigma_position].minset(),value__flt_number[phasertng_macgyre_restraint_sigma_position].minval(),value__flt_number[phasertng_macgyre_restraint_sigma_position].maxset(),value__flt_number[phasertng_macgyre_restraint_sigma_position].maxval()));
            }
            else if (keyIs("rotation")) {
              constexpr char phasertng_macgyre_restraint_sigma_rotation[] = ".phasertng.macgyre.restraint_sigma.rotation.";
              check_unique(phasertng_macgyre_restraint_sigma_rotation);
              value__flt_number[phasertng_macgyre_restraint_sigma_rotation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macgyre_restraint_sigma_rotation].minset(),value__flt_number[phasertng_macgyre_restraint_sigma_rotation].minval(),value__flt_number[phasertng_macgyre_restraint_sigma_rotation].maxset(),value__flt_number[phasertng_macgyre_restraint_sigma_rotation].maxval()));
            }
          }
          else if (keyIs("small_target")) {
            constexpr char phasertng_macgyre_small_target[] = ".phasertng.macgyre.small_target.";
            check_unique(phasertng_macgyre_small_target);
            value__flt_number[phasertng_macgyre_small_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macgyre_small_target].minset(),value__flt_number[phasertng_macgyre_small_target].minval(),value__flt_number[phasertng_macgyre_small_target].maxset(),value__flt_number[phasertng_macgyre_small_target].maxval()));
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macgyre_study_parameters[] = ".phasertng.macgyre.study_parameters.";
            check_unique(phasertng_macgyre_study_parameters);
            value__boolean[phasertng_macgyre_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macjog")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","protocol","restraint_sigma","small_target"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macjog_macrocycle1[] = ".phasertng.macjog.macrocycle1.";
            value__choice[phasertng_macjog_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell","last"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macjog_macrocycle2[] = ".phasertng.macjog.macrocycle2.";
            value__choice[phasertng_macjog_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell","last"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macjog_macrocycle3[] = ".phasertng.macjog.macrocycle3.";
            value__choice[phasertng_macjog_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macjog_minimizer[] = ".phasertng.macjog.minimizer.";
            check_unique(phasertng_macjog_minimizer);
            value__choice[phasertng_macjog_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macjog_ncyc[] = ".phasertng.macjog.ncyc.";
            check_unique(phasertng_macjog_ncyc);
            value__int_vector[phasertng_macjog_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macjog_ncyc].minset(),value__int_vector[phasertng_macjog_ncyc].minval(),value__int_vector[phasertng_macjog_ncyc].maxset(),value__int_vector[phasertng_macjog_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macjog_protocol[] = ".phasertng.macjog.protocol.";
            check_unique(phasertng_macjog_protocol);
            value__choice[phasertng_macjog_protocol].set_value(variable,string_value);
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"bfactor","cell","drms","rotation","translation"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_macjog_restraint_sigma_bfactor[] = ".phasertng.macjog.restraint_sigma.bfactor.";
              check_unique(phasertng_macjog_restraint_sigma_bfactor);
              value__flt_number[phasertng_macjog_restraint_sigma_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_restraint_sigma_bfactor].minset(),value__flt_number[phasertng_macjog_restraint_sigma_bfactor].minval(),value__flt_number[phasertng_macjog_restraint_sigma_bfactor].maxset(),value__flt_number[phasertng_macjog_restraint_sigma_bfactor].maxval()));
            }
            else if (keyIs("cell")) {
              constexpr char phasertng_macjog_restraint_sigma_cell[] = ".phasertng.macjog.restraint_sigma.cell.";
              check_unique(phasertng_macjog_restraint_sigma_cell);
              value__flt_number[phasertng_macjog_restraint_sigma_cell].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_restraint_sigma_cell].minset(),value__flt_number[phasertng_macjog_restraint_sigma_cell].minval(),value__flt_number[phasertng_macjog_restraint_sigma_cell].maxset(),value__flt_number[phasertng_macjog_restraint_sigma_cell].maxval()));
            }
            else if (keyIs("drms")) {
              constexpr char phasertng_macjog_restraint_sigma_drms[] = ".phasertng.macjog.restraint_sigma.drms.";
              check_unique(phasertng_macjog_restraint_sigma_drms);
              value__flt_number[phasertng_macjog_restraint_sigma_drms].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_restraint_sigma_drms].minset(),value__flt_number[phasertng_macjog_restraint_sigma_drms].minval(),value__flt_number[phasertng_macjog_restraint_sigma_drms].maxset(),value__flt_number[phasertng_macjog_restraint_sigma_drms].maxval()));
            }
            else if (keyIs("rotation")) {
              constexpr char phasertng_macjog_restraint_sigma_rotation[] = ".phasertng.macjog.restraint_sigma.rotation.";
              check_unique(phasertng_macjog_restraint_sigma_rotation);
              value__flt_number[phasertng_macjog_restraint_sigma_rotation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_restraint_sigma_rotation].minset(),value__flt_number[phasertng_macjog_restraint_sigma_rotation].minval(),value__flt_number[phasertng_macjog_restraint_sigma_rotation].maxset(),value__flt_number[phasertng_macjog_restraint_sigma_rotation].maxval()));
            }
            else if (keyIs("translation")) {
              constexpr char phasertng_macjog_restraint_sigma_translation[] = ".phasertng.macjog.restraint_sigma.translation.";
              check_unique(phasertng_macjog_restraint_sigma_translation);
              value__flt_number[phasertng_macjog_restraint_sigma_translation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_restraint_sigma_translation].minset(),value__flt_number[phasertng_macjog_restraint_sigma_translation].minval(),value__flt_number[phasertng_macjog_restraint_sigma_translation].maxset(),value__flt_number[phasertng_macjog_restraint_sigma_translation].maxval()));
            }
          }
          else if (keyIs("small_target")) {
            constexpr char phasertng_macjog_small_target[] = ".phasertng.macjog.small_target.";
            check_unique(phasertng_macjog_small_target);
            value__flt_number[phasertng_macjog_small_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macjog_small_target].minset(),value__flt_number[phasertng_macjog_small_target].minval(),value__flt_number[phasertng_macjog_small_target].maxset(),value__flt_number[phasertng_macjog_small_target].maxval()));
          }
        }
        else if (keyIs("macrbr")) {
          compulsoryKey(input_stream,{"cleanup","last_only","macrocycle1","macrocycle2","macrocycle3","maximum_printed","maximum_stored","minimizer","ncyc","protocol","purge_duplicates","restraint_sigma","small_target","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("cleanup")) {
            constexpr char phasertng_macrbr_cleanup[] = ".phasertng.macrbr.cleanup.";
            check_unique(phasertng_macrbr_cleanup);
            value__boolean[phasertng_macrbr_cleanup].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("last_only")) {
            constexpr char phasertng_macrbr_last_only[] = ".phasertng.macrbr.last_only.";
            check_unique(phasertng_macrbr_last_only);
            value__boolean[phasertng_macrbr_last_only].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrbr_macrocycle1[] = ".phasertng.macrbr.macrocycle1.";
            value__choice[phasertng_macrbr_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrbr_macrocycle2[] = ".phasertng.macrbr.macrocycle2.";
            value__choice[phasertng_macrbr_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrbr_macrocycle3[] = ".phasertng.macrbr.macrocycle3.";
            value__choice[phasertng_macrbr_macrocycle3].set_value(choices);
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_macrbr_maximum_printed[] = ".phasertng.macrbr.maximum_printed.";
            check_unique(phasertng_macrbr_maximum_printed);
            value__int_number[phasertng_macrbr_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macrbr_maximum_printed].minset(),value__int_number[phasertng_macrbr_maximum_printed].minval(),value__int_number[phasertng_macrbr_maximum_printed].maxset(),value__int_number[phasertng_macrbr_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_macrbr_maximum_stored[] = ".phasertng.macrbr.maximum_stored.";
            check_unique(phasertng_macrbr_maximum_stored);
            value__int_number[phasertng_macrbr_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macrbr_maximum_stored].minset(),value__int_number[phasertng_macrbr_maximum_stored].minval(),value__int_number[phasertng_macrbr_maximum_stored].maxset(),value__int_number[phasertng_macrbr_maximum_stored].maxval()));
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macrbr_minimizer[] = ".phasertng.macrbr.minimizer.";
            check_unique(phasertng_macrbr_minimizer);
            value__choice[phasertng_macrbr_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macrbr_ncyc[] = ".phasertng.macrbr.ncyc.";
            check_unique(phasertng_macrbr_ncyc);
            value__int_vector[phasertng_macrbr_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macrbr_ncyc].minset(),value__int_vector[phasertng_macrbr_ncyc].minval(),value__int_vector[phasertng_macrbr_ncyc].maxset(),value__int_vector[phasertng_macrbr_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macrbr_protocol[] = ".phasertng.macrbr.protocol.";
            check_unique(phasertng_macrbr_protocol);
            value__choice[phasertng_macrbr_protocol].set_value(variable,string_value);
          }
          else if (keyIs("purge_duplicates")) {
            constexpr char phasertng_macrbr_purge_duplicates[] = ".phasertng.macrbr.purge_duplicates.";
            check_unique(phasertng_macrbr_purge_duplicates);
            value__boolean[phasertng_macrbr_purge_duplicates].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"bfactor","cell","drms","rotation","translation"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_macrbr_restraint_sigma_bfactor[] = ".phasertng.macrbr.restraint_sigma.bfactor.";
              check_unique(phasertng_macrbr_restraint_sigma_bfactor);
              value__flt_number[phasertng_macrbr_restraint_sigma_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_restraint_sigma_bfactor].minset(),value__flt_number[phasertng_macrbr_restraint_sigma_bfactor].minval(),value__flt_number[phasertng_macrbr_restraint_sigma_bfactor].maxset(),value__flt_number[phasertng_macrbr_restraint_sigma_bfactor].maxval()));
            }
            else if (keyIs("cell")) {
              constexpr char phasertng_macrbr_restraint_sigma_cell[] = ".phasertng.macrbr.restraint_sigma.cell.";
              check_unique(phasertng_macrbr_restraint_sigma_cell);
              value__flt_number[phasertng_macrbr_restraint_sigma_cell].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_restraint_sigma_cell].minset(),value__flt_number[phasertng_macrbr_restraint_sigma_cell].minval(),value__flt_number[phasertng_macrbr_restraint_sigma_cell].maxset(),value__flt_number[phasertng_macrbr_restraint_sigma_cell].maxval()));
            }
            else if (keyIs("drms")) {
              constexpr char phasertng_macrbr_restraint_sigma_drms[] = ".phasertng.macrbr.restraint_sigma.drms.";
              check_unique(phasertng_macrbr_restraint_sigma_drms);
              value__flt_number[phasertng_macrbr_restraint_sigma_drms].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_restraint_sigma_drms].minset(),value__flt_number[phasertng_macrbr_restraint_sigma_drms].minval(),value__flt_number[phasertng_macrbr_restraint_sigma_drms].maxset(),value__flt_number[phasertng_macrbr_restraint_sigma_drms].maxval()));
            }
            else if (keyIs("rotation")) {
              constexpr char phasertng_macrbr_restraint_sigma_rotation[] = ".phasertng.macrbr.restraint_sigma.rotation.";
              check_unique(phasertng_macrbr_restraint_sigma_rotation);
              value__flt_number[phasertng_macrbr_restraint_sigma_rotation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_restraint_sigma_rotation].minset(),value__flt_number[phasertng_macrbr_restraint_sigma_rotation].minval(),value__flt_number[phasertng_macrbr_restraint_sigma_rotation].maxset(),value__flt_number[phasertng_macrbr_restraint_sigma_rotation].maxval()));
            }
            else if (keyIs("translation")) {
              constexpr char phasertng_macrbr_restraint_sigma_translation[] = ".phasertng.macrbr.restraint_sigma.translation.";
              check_unique(phasertng_macrbr_restraint_sigma_translation);
              value__flt_number[phasertng_macrbr_restraint_sigma_translation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_restraint_sigma_translation].minset(),value__flt_number[phasertng_macrbr_restraint_sigma_translation].minval(),value__flt_number[phasertng_macrbr_restraint_sigma_translation].maxset(),value__flt_number[phasertng_macrbr_restraint_sigma_translation].maxval()));
            }
          }
          else if (keyIs("small_target")) {
            constexpr char phasertng_macrbr_small_target[] = ".phasertng.macrbr.small_target.";
            check_unique(phasertng_macrbr_small_target);
            value__flt_number[phasertng_macrbr_small_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrbr_small_target].minset(),value__flt_number[phasertng_macrbr_small_target].minval(),value__flt_number[phasertng_macrbr_small_target].maxset(),value__flt_number[phasertng_macrbr_small_target].maxval()));
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macrbr_study_parameters[] = ".phasertng.macrbr.study_parameters.";
            check_unique(phasertng_macrbr_study_parameters);
            value__boolean[phasertng_macrbr_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macrm")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","maximum_printed","maximum_stored","minimizer","ncyc","percent","protocol","restraint_sigma","small_target","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrm_macrocycle1[] = ".phasertng.macrm.macrocycle1.";
            value__choice[phasertng_macrm_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrm_macrocycle2[] = ".phasertng.macrm.macrocycle2.";
            value__choice[phasertng_macrm_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rotn","tran","bfac","vrms","cell"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macrm_macrocycle3[] = ".phasertng.macrm.macrocycle3.";
            value__choice[phasertng_macrm_macrocycle3].set_value(choices);
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_macrm_maximum_printed[] = ".phasertng.macrm.maximum_printed.";
            check_unique(phasertng_macrm_maximum_printed);
            value__int_number[phasertng_macrm_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macrm_maximum_printed].minset(),value__int_number[phasertng_macrm_maximum_printed].minval(),value__int_number[phasertng_macrm_maximum_printed].maxset(),value__int_number[phasertng_macrm_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_macrm_maximum_stored[] = ".phasertng.macrm.maximum_stored.";
            check_unique(phasertng_macrm_maximum_stored);
            value__int_number[phasertng_macrm_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macrm_maximum_stored].minset(),value__int_number[phasertng_macrm_maximum_stored].minval(),value__int_number[phasertng_macrm_maximum_stored].maxset(),value__int_number[phasertng_macrm_maximum_stored].maxval()));
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macrm_minimizer[] = ".phasertng.macrm.minimizer.";
            check_unique(phasertng_macrm_minimizer);
            value__choice[phasertng_macrm_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macrm_ncyc[] = ".phasertng.macrm.ncyc.";
            check_unique(phasertng_macrm_ncyc);
            value__int_vector[phasertng_macrm_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macrm_ncyc].minset(),value__int_vector[phasertng_macrm_ncyc].minval(),value__int_vector[phasertng_macrm_ncyc].maxset(),value__int_vector[phasertng_macrm_ncyc].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_macrm_percent[] = ".phasertng.macrm.percent.";
            check_unique(phasertng_macrm_percent);
            value__percent[phasertng_macrm_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macrm_protocol[] = ".phasertng.macrm.protocol.";
            check_unique(phasertng_macrm_protocol);
            value__choice[phasertng_macrm_protocol].set_value(variable,string_value);
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"bfactor","cell","drms","rotation","translation"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_macrm_restraint_sigma_bfactor[] = ".phasertng.macrm.restraint_sigma.bfactor.";
              check_unique(phasertng_macrm_restraint_sigma_bfactor);
              value__flt_number[phasertng_macrm_restraint_sigma_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_restraint_sigma_bfactor].minset(),value__flt_number[phasertng_macrm_restraint_sigma_bfactor].minval(),value__flt_number[phasertng_macrm_restraint_sigma_bfactor].maxset(),value__flt_number[phasertng_macrm_restraint_sigma_bfactor].maxval()));
            }
            else if (keyIs("cell")) {
              constexpr char phasertng_macrm_restraint_sigma_cell[] = ".phasertng.macrm.restraint_sigma.cell.";
              check_unique(phasertng_macrm_restraint_sigma_cell);
              value__flt_number[phasertng_macrm_restraint_sigma_cell].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_restraint_sigma_cell].minset(),value__flt_number[phasertng_macrm_restraint_sigma_cell].minval(),value__flt_number[phasertng_macrm_restraint_sigma_cell].maxset(),value__flt_number[phasertng_macrm_restraint_sigma_cell].maxval()));
            }
            else if (keyIs("drms")) {
              constexpr char phasertng_macrm_restraint_sigma_drms[] = ".phasertng.macrm.restraint_sigma.drms.";
              check_unique(phasertng_macrm_restraint_sigma_drms);
              value__flt_number[phasertng_macrm_restraint_sigma_drms].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_restraint_sigma_drms].minset(),value__flt_number[phasertng_macrm_restraint_sigma_drms].minval(),value__flt_number[phasertng_macrm_restraint_sigma_drms].maxset(),value__flt_number[phasertng_macrm_restraint_sigma_drms].maxval()));
            }
            else if (keyIs("rotation")) {
              constexpr char phasertng_macrm_restraint_sigma_rotation[] = ".phasertng.macrm.restraint_sigma.rotation.";
              check_unique(phasertng_macrm_restraint_sigma_rotation);
              value__flt_number[phasertng_macrm_restraint_sigma_rotation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_restraint_sigma_rotation].minset(),value__flt_number[phasertng_macrm_restraint_sigma_rotation].minval(),value__flt_number[phasertng_macrm_restraint_sigma_rotation].maxset(),value__flt_number[phasertng_macrm_restraint_sigma_rotation].maxval()));
            }
            else if (keyIs("translation")) {
              constexpr char phasertng_macrm_restraint_sigma_translation[] = ".phasertng.macrm.restraint_sigma.translation.";
              check_unique(phasertng_macrm_restraint_sigma_translation);
              value__flt_number[phasertng_macrm_restraint_sigma_translation].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_restraint_sigma_translation].minset(),value__flt_number[phasertng_macrm_restraint_sigma_translation].minval(),value__flt_number[phasertng_macrm_restraint_sigma_translation].maxset(),value__flt_number[phasertng_macrm_restraint_sigma_translation].maxval()));
            }
          }
          else if (keyIs("small_target")) {
            constexpr char phasertng_macrm_small_target[] = ".phasertng.macrm.small_target.";
            check_unique(phasertng_macrm_small_target);
            value__flt_number[phasertng_macrm_small_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macrm_small_target].minset(),value__flt_number[phasertng_macrm_small_target].minval(),value__flt_number[phasertng_macrm_small_target].maxset(),value__flt_number[phasertng_macrm_small_target].maxval()));
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macrm_study_parameters[] = ".phasertng.macrm.study_parameters.";
            check_unique(phasertng_macrm_study_parameters);
            value__boolean[phasertng_macrm_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macsad")) {
          compulsoryKey(input_stream,{"enantiomorphs","hand","integration_steps","macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","restraint_sigma","study_parameters","use_partial_anomalous"});
          if (keyIsEol()) return false;
          else if (keyIs("enantiomorphs")) {
            constexpr char phasertng_macsad_enantiomorphs[] = ".phasertng.macsad.enantiomorphs.";
            check_unique(phasertng_macsad_enantiomorphs);
            value__boolean[phasertng_macsad_enantiomorphs].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("hand")) {
            constexpr char phasertng_macsad_hand[] = ".phasertng.macsad.hand.";
            check_unique(phasertng_macsad_hand);
            value__boolean[phasertng_macsad_hand].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("integration_steps")) {
            constexpr char phasertng_macsad_integration_steps[] = ".phasertng.macsad.integration_steps.";
            check_unique(phasertng_macsad_integration_steps);
            value__int_number[phasertng_macsad_integration_steps].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_macsad_integration_steps].minset(),value__int_number[phasertng_macsad_integration_steps].minval(),value__int_number[phasertng_macsad_integration_steps].maxset(),value__int_number[phasertng_macsad_integration_steps].maxval()));
          }
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","data","siga","sigb","sigp","sigd","site","bfac","occ","fdp","part"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macsad_macrocycle1[] = ".phasertng.macsad.macrocycle1.";
            value__choice[phasertng_macsad_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","data","siga","sigb","sigp","sigd","site","bfac","occ","fdp","part"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macsad_macrocycle2[] = ".phasertng.macsad.macrocycle2.";
            value__choice[phasertng_macsad_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","data","siga","sigb","sigp","sigd","site","bfac","occ","fdp","part"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macsad_macrocycle3[] = ".phasertng.macsad.macrocycle3.";
            value__choice[phasertng_macsad_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macsad_minimizer[] = ".phasertng.macsad.minimizer.";
            check_unique(phasertng_macsad_minimizer);
            value__choice[phasertng_macsad_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macsad_ncyc[] = ".phasertng.macsad.ncyc.";
            check_unique(phasertng_macsad_ncyc);
            value__int_vector[phasertng_macsad_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macsad_ncyc].minset(),value__int_vector[phasertng_macsad_ncyc].minval(),value__int_vector[phasertng_macsad_ncyc].maxset(),value__int_vector[phasertng_macsad_ncyc].maxval()));
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"fdp","sphericity","wilson_bfactor"});
            if (keyIsEol()) return false;
            else if (keyIs("fdp")) {
              constexpr char phasertng_macsad_restraint_sigma_fdp[] = ".phasertng.macsad.restraint_sigma.fdp.";
              check_unique(phasertng_macsad_restraint_sigma_fdp);
              value__flt_number[phasertng_macsad_restraint_sigma_fdp].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macsad_restraint_sigma_fdp].minset(),value__flt_number[phasertng_macsad_restraint_sigma_fdp].minval(),value__flt_number[phasertng_macsad_restraint_sigma_fdp].maxset(),value__flt_number[phasertng_macsad_restraint_sigma_fdp].maxval()));
            }
            else if (keyIs("sphericity")) {
              constexpr char phasertng_macsad_restraint_sigma_sphericity[] = ".phasertng.macsad.restraint_sigma.sphericity.";
              check_unique(phasertng_macsad_restraint_sigma_sphericity);
              value__flt_number[phasertng_macsad_restraint_sigma_sphericity].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macsad_restraint_sigma_sphericity].minset(),value__flt_number[phasertng_macsad_restraint_sigma_sphericity].minval(),value__flt_number[phasertng_macsad_restraint_sigma_sphericity].maxset(),value__flt_number[phasertng_macsad_restraint_sigma_sphericity].maxval()));
            }
            else if (keyIs("wilson_bfactor")) {
              constexpr char phasertng_macsad_restraint_sigma_wilson_bfactor[] = ".phasertng.macsad.restraint_sigma.wilson_bfactor.";
              check_unique(phasertng_macsad_restraint_sigma_wilson_bfactor);
              value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor].minset(),value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor].minval(),value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor].maxset(),value__flt_number[phasertng_macsad_restraint_sigma_wilson_bfactor].maxval()));
            }
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macsad_study_parameters[] = ".phasertng.macsad.study_parameters.";
            check_unique(phasertng_macsad_study_parameters);
            value__boolean[phasertng_macsad_study_parameters].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("use_partial_anomalous")) {
            constexpr char phasertng_macsad_use_partial_anomalous[] = ".phasertng.macsad.use_partial_anomalous.";
            check_unique(phasertng_macsad_use_partial_anomalous);
            value__boolean[phasertng_macsad_use_partial_anomalous].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macspr")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","protocol","small_target","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","bfac","occ"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macspr_macrocycle1[] = ".phasertng.macspr.macrocycle1.";
            value__choice[phasertng_macspr_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","bfac","occ"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macspr_macrocycle2[] = ".phasertng.macspr.macrocycle2.";
            value__choice[phasertng_macspr_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","bfac","occ"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macspr_macrocycle3[] = ".phasertng.macspr.macrocycle3.";
            value__choice[phasertng_macspr_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macspr_minimizer[] = ".phasertng.macspr.minimizer.";
            check_unique(phasertng_macspr_minimizer);
            value__choice[phasertng_macspr_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macspr_ncyc[] = ".phasertng.macspr.ncyc.";
            check_unique(phasertng_macspr_ncyc);
            value__int_vector[phasertng_macspr_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macspr_ncyc].minset(),value__int_vector[phasertng_macspr_ncyc].minval(),value__int_vector[phasertng_macspr_ncyc].maxset(),value__int_vector[phasertng_macspr_ncyc].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_macspr_protocol[] = ".phasertng.macspr.protocol.";
            check_unique(phasertng_macspr_protocol);
            value__choice[phasertng_macspr_protocol].set_value(variable,string_value);
          }
          else if (keyIs("small_target")) {
            constexpr char phasertng_macspr_small_target[] = ".phasertng.macspr.small_target.";
            check_unique(phasertng_macspr_small_target);
            value__flt_number[phasertng_macspr_small_target].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macspr_small_target].minset(),value__flt_number[phasertng_macspr_small_target].minval(),value__flt_number[phasertng_macspr_small_target].maxset(),value__flt_number[phasertng_macspr_small_target].maxval()));
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macspr_study_parameters[] = ".phasertng.macspr.study_parameters.";
            check_unique(phasertng_macspr_study_parameters);
            value__boolean[phasertng_macspr_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("macssa")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","restraint_sigma","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rhoff","rhopm"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macssa_macrocycle1[] = ".phasertng.macssa.macrocycle1.";
            value__choice[phasertng_macssa_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rhoff","rhopm"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macssa_macrocycle2[] = ".phasertng.macssa.macrocycle2.";
            value__choice[phasertng_macssa_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rhoff","rhopm"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_macssa_macrocycle3[] = ".phasertng.macssa.macrocycle3.";
            value__choice[phasertng_macssa_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_macssa_minimizer[] = ".phasertng.macssa.minimizer.";
            check_unique(phasertng_macssa_minimizer);
            value__choice[phasertng_macssa_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_macssa_ncyc[] = ".phasertng.macssa.ncyc.";
            check_unique(phasertng_macssa_ncyc);
            value__int_vector[phasertng_macssa_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_macssa_ncyc].minset(),value__int_vector[phasertng_macssa_ncyc].minval(),value__int_vector[phasertng_macssa_ncyc].maxset(),value__int_vector[phasertng_macssa_ncyc].maxval()));
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"atoms","rhopm"});
            if (keyIsEol()) return false;
            else if (keyIs("atoms")) {
              constexpr char phasertng_macssa_restraint_sigma_atoms[] = ".phasertng.macssa.restraint_sigma.atoms.";
              check_unique(phasertng_macssa_restraint_sigma_atoms);
              value__flt_number[phasertng_macssa_restraint_sigma_atoms].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macssa_restraint_sigma_atoms].minset(),value__flt_number[phasertng_macssa_restraint_sigma_atoms].minval(),value__flt_number[phasertng_macssa_restraint_sigma_atoms].maxset(),value__flt_number[phasertng_macssa_restraint_sigma_atoms].maxval()));
            }
            else if (keyIs("rhopm")) {
              constexpr char phasertng_macssa_restraint_sigma_rhopm[] = ".phasertng.macssa.restraint_sigma.rhopm.";
              check_unique(phasertng_macssa_restraint_sigma_rhopm);
              value__flt_number[phasertng_macssa_restraint_sigma_rhopm].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_macssa_restraint_sigma_rhopm].minset(),value__flt_number[phasertng_macssa_restraint_sigma_rhopm].minval(),value__flt_number[phasertng_macssa_restraint_sigma_rhopm].maxset(),value__flt_number[phasertng_macssa_restraint_sigma_rhopm].maxval()));
            }
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_macssa_study_parameters[] = ".phasertng.macssa.study_parameters.";
            check_unique(phasertng_macssa_study_parameters);
            value__boolean[phasertng_macssa_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("mactncs")) {
          compulsoryKey(input_stream,{"macrocycle1","macrocycle2","macrocycle3","minimizer","ncyc","perturbation","protocol","resolution","restraint_sigma","study_parameters"});
          if (keyIsEol()) return false;
          else if (keyIs("macrocycle1")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rot","gfn","tra","rmsd","fs","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_mactncs_macrocycle1[] = ".phasertng.mactncs.macrocycle1.";
            value__choice[phasertng_mactncs_macrocycle1].set_value(choices);
          }
          else if (keyIs("macrocycle2")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rot","gfn","tra","rmsd","fs","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_mactncs_macrocycle2[] = ".phasertng.mactncs.macrocycle2.";
            value__choice[phasertng_mactncs_macrocycle2].set_value(choices);
          }
          else if (keyIs("macrocycle3")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","rot","gfn","tra","rmsd","fs","bins"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_mactncs_macrocycle3[] = ".phasertng.mactncs.macrocycle3.";
            value__choice[phasertng_mactncs_macrocycle3].set_value(choices);
          }
          else if (keyIs("minimizer")) {
            compulsoryKey(input_stream,{"bfgs","newton"});
            constexpr char phasertng_mactncs_minimizer[] = ".phasertng.mactncs.minimizer.";
            check_unique(phasertng_mactncs_minimizer);
            value__choice[phasertng_mactncs_minimizer].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            constexpr char phasertng_mactncs_ncyc[] = ".phasertng.mactncs.ncyc.";
            check_unique(phasertng_mactncs_ncyc);
            value__int_vector[phasertng_mactncs_ncyc].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_mactncs_ncyc].minset(),value__int_vector[phasertng_mactncs_ncyc].minval(),value__int_vector[phasertng_mactncs_ncyc].maxset(),value__int_vector[phasertng_mactncs_ncyc].maxval()));
          }
          else if (keyIs("perturbation")) {
            constexpr char phasertng_mactncs_perturbation[] = ".phasertng.mactncs.perturbation.";
            check_unique(phasertng_mactncs_perturbation);
            value__int_numbers[phasertng_mactncs_perturbation].set_value(variable,get_int_numbers(input_stream,value__int_numbers[phasertng_mactncs_perturbation].minset(),value__int_numbers[phasertng_mactncs_perturbation].minval(),value__int_numbers[phasertng_mactncs_perturbation].maxset(),value__int_numbers[phasertng_mactncs_perturbation].maxval()));
          }
          else if (keyIs("protocol")) {
            compulsoryKey(input_stream,{"off","on"});
            constexpr char phasertng_mactncs_protocol[] = ".phasertng.mactncs.protocol.";
            check_unique(phasertng_mactncs_protocol);
            value__choice[phasertng_mactncs_protocol].set_value(variable,string_value);
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_mactncs_resolution[] = ".phasertng.mactncs.resolution.";
            check_unique(phasertng_mactncs_resolution);
            value__flt_number[phasertng_mactncs_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_resolution].minset(),value__flt_number[phasertng_mactncs_resolution].minval(),value__flt_number[phasertng_mactncs_resolution].maxset(),value__flt_number[phasertng_mactncs_resolution].maxval()));
          }
          else if (keyIs("restraint_sigma")) {
            compulsoryKey(input_stream,{"binwise_unity","fs","gfn","rmsd","rot","smooth","tra"});
            if (keyIsEol()) return false;
            else if (keyIs("binwise_unity")) {
              constexpr char phasertng_mactncs_restraint_sigma_binwise_unity[] = ".phasertng.mactncs.restraint_sigma.binwise_unity.";
              check_unique(phasertng_mactncs_restraint_sigma_binwise_unity);
              value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_binwise_unity].maxval()));
            }
            else if (keyIs("fs")) {
              constexpr char phasertng_mactncs_restraint_sigma_fs[] = ".phasertng.mactncs.restraint_sigma.fs.";
              check_unique(phasertng_mactncs_restraint_sigma_fs);
              value__flt_number[phasertng_mactncs_restraint_sigma_fs].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_fs].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_fs].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_fs].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_fs].maxval()));
            }
            else if (keyIs("gfn")) {
              constexpr char phasertng_mactncs_restraint_sigma_gfn[] = ".phasertng.mactncs.restraint_sigma.gfn.";
              check_unique(phasertng_mactncs_restraint_sigma_gfn);
              value__flt_number[phasertng_mactncs_restraint_sigma_gfn].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_gfn].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_gfn].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_gfn].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_gfn].maxval()));
            }
            else if (keyIs("rmsd")) {
              constexpr char phasertng_mactncs_restraint_sigma_rmsd[] = ".phasertng.mactncs.restraint_sigma.rmsd.";
              check_unique(phasertng_mactncs_restraint_sigma_rmsd);
              value__flt_number[phasertng_mactncs_restraint_sigma_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_rmsd].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_rmsd].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_rmsd].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_rmsd].maxval()));
            }
            else if (keyIs("rot")) {
              constexpr char phasertng_mactncs_restraint_sigma_rot[] = ".phasertng.mactncs.restraint_sigma.rot.";
              check_unique(phasertng_mactncs_restraint_sigma_rot);
              value__flt_number[phasertng_mactncs_restraint_sigma_rot].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_rot].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_rot].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_rot].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_rot].maxval()));
            }
            else if (keyIs("smooth")) {
              constexpr char phasertng_mactncs_restraint_sigma_smooth[] = ".phasertng.mactncs.restraint_sigma.smooth.";
              check_unique(phasertng_mactncs_restraint_sigma_smooth);
              value__flt_number[phasertng_mactncs_restraint_sigma_smooth].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_smooth].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_smooth].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_smooth].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_smooth].maxval()));
            }
            else if (keyIs("tra")) {
              constexpr char phasertng_mactncs_restraint_sigma_tra[] = ".phasertng.mactncs.restraint_sigma.tra.";
              check_unique(phasertng_mactncs_restraint_sigma_tra);
              value__flt_number[phasertng_mactncs_restraint_sigma_tra].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mactncs_restraint_sigma_tra].minset(),value__flt_number[phasertng_mactncs_restraint_sigma_tra].minval(),value__flt_number[phasertng_mactncs_restraint_sigma_tra].maxset(),value__flt_number[phasertng_mactncs_restraint_sigma_tra].maxval()));
            }
          }
          else if (keyIs("study_parameters")) {
            constexpr char phasertng_mactncs_study_parameters[] = ".phasertng.mactncs.study_parameters.";
            check_unique(phasertng_mactncs_study_parameters);
            value__boolean[phasertng_mactncs_study_parameters].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("map_correlation_coefficient")) {
          compulsoryKey(input_stream,{"maximum_printed","maximum_stored","similarity"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_map_correlation_coefficient_maximum_printed[] = ".phasertng.map_correlation_coefficient.maximum_printed.";
            check_unique(phasertng_map_correlation_coefficient_maximum_printed);
            value__int_number[phasertng_map_correlation_coefficient_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_map_correlation_coefficient_maximum_printed].minset(),value__int_number[phasertng_map_correlation_coefficient_maximum_printed].minval(),value__int_number[phasertng_map_correlation_coefficient_maximum_printed].maxset(),value__int_number[phasertng_map_correlation_coefficient_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_map_correlation_coefficient_maximum_stored[] = ".phasertng.map_correlation_coefficient.maximum_stored.";
            check_unique(phasertng_map_correlation_coefficient_maximum_stored);
            value__int_number[phasertng_map_correlation_coefficient_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_map_correlation_coefficient_maximum_stored].minset(),value__int_number[phasertng_map_correlation_coefficient_maximum_stored].minval(),value__int_number[phasertng_map_correlation_coefficient_maximum_stored].maxset(),value__int_number[phasertng_map_correlation_coefficient_maximum_stored].maxval()));
          }
          else if (keyIs("similarity")) {
            constexpr char phasertng_map_correlation_coefficient_similarity[] = ".phasertng.map_correlation_coefficient.similarity.";
            check_unique(phasertng_map_correlation_coefficient_similarity);
            value__flt_number[phasertng_map_correlation_coefficient_similarity].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_map_correlation_coefficient_similarity].minset(),value__flt_number[phasertng_map_correlation_coefficient_similarity].minval(),value__flt_number[phasertng_map_correlation_coefficient_similarity].maxset(),value__flt_number[phasertng_map_correlation_coefficient_similarity].maxval()));
          }
        }
        else if (keyIs("map_ellg")) {
          compulsoryKey(input_stream,{"fs","result","rmsd"});
          if (keyIsEol()) return false;
          else if (keyIs("fs")) {
            constexpr char phasertng_map_ellg_fs[] = ".phasertng.map_ellg.fs.";
            check_unique(phasertng_map_ellg_fs);
            value__flt_numbers[phasertng_map_ellg_fs].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_map_ellg_fs].minset(),value__flt_numbers[phasertng_map_ellg_fs].minval(),value__flt_numbers[phasertng_map_ellg_fs].maxset(),value__flt_numbers[phasertng_map_ellg_fs].maxval()));
          }
          else if (keyIs("result")) {
            compulsoryKey(input_stream,{"fs"});
            variable.push_back("fs");
            constexpr char phasertng_map_ellg_result_fs[] = ".phasertng.map_ellg.result.fs.";
            array__flt_number[phasertng_map_ellg_result_fs].push_back(type::flt_number(phasertng_map_ellg_result_fs,false,0,false,0));
            array__flt_number[phasertng_map_ellg_result_fs].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_map_ellg_result_fs].back().minset(),array__flt_number[phasertng_map_ellg_result_fs].back().minval(),array__flt_number[phasertng_map_ellg_result_fs].back().maxset(),array__flt_number[phasertng_map_ellg_result_fs].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"rmsd"});
            variable.push_back("rmsd");
            constexpr char phasertng_map_ellg_result_rmsd[] = ".phasertng.map_ellg.result.rmsd.";
            array__flt_number[phasertng_map_ellg_result_rmsd].push_back(type::flt_number(phasertng_map_ellg_result_rmsd,true,0,false,0));
            array__flt_number[phasertng_map_ellg_result_rmsd].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_map_ellg_result_rmsd].back().minset(),array__flt_number[phasertng_map_ellg_result_rmsd].back().minval(),array__flt_number[phasertng_map_ellg_result_rmsd].back().maxset(),array__flt_number[phasertng_map_ellg_result_rmsd].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"ellg"});
            variable.push_back("ellg");
            constexpr char phasertng_map_ellg_result_ellg[] = ".phasertng.map_ellg.result.ellg.";
            array__flt_number[phasertng_map_ellg_result_ellg].push_back(type::flt_number(phasertng_map_ellg_result_ellg,false,0,false,0));
            array__flt_number[phasertng_map_ellg_result_ellg].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_map_ellg_result_ellg].back().minset(),array__flt_number[phasertng_map_ellg_result_ellg].back().minval(),array__flt_number[phasertng_map_ellg_result_ellg].back().maxset(),array__flt_number[phasertng_map_ellg_result_ellg].back().maxval()));
            variable.pop_back();
          }
          else if (keyIs("rmsd")) {
            constexpr char phasertng_map_ellg_rmsd[] = ".phasertng.map_ellg.rmsd.";
            check_unique(phasertng_map_ellg_rmsd);
            value__flt_numbers[phasertng_map_ellg_rmsd].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_map_ellg_rmsd].minset(),value__flt_numbers[phasertng_map_ellg_rmsd].minval(),value__flt_numbers[phasertng_map_ellg_rmsd].maxset(),value__flt_numbers[phasertng_map_ellg_rmsd].maxval()));
          }
        }
        else if (keyIs("mapin")) {
          compulsoryKey(input_stream,{"filename","oversample_resolution_limit","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_mapin_filename[] = ".phasertng.mapin.filename.";
            check_unique(phasertng_mapin_filename);
            value__path[phasertng_mapin_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("oversample_resolution_limit")) {
            constexpr char phasertng_mapin_oversample_resolution_limit[] = ".phasertng.mapin.oversample_resolution_limit.";
            check_unique(phasertng_mapin_oversample_resolution_limit);
            value__flt_number[phasertng_mapin_oversample_resolution_limit].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_mapin_oversample_resolution_limit].minset(),value__flt_number[phasertng_mapin_oversample_resolution_limit].minval(),value__flt_number[phasertng_mapin_oversample_resolution_limit].maxset(),value__flt_number[phasertng_mapin_oversample_resolution_limit].maxval()));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_mapin_tag[] = ".phasertng.mapin.tag.";
            check_unique(phasertng_mapin_tag);
            value__string[phasertng_mapin_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("matthews")) {
          compulsoryKey(input_stream,{"vm"});
          variable.push_back("vm");
          constexpr char phasertng_matthews_vm[] = ".phasertng.matthews.vm.";
          array__flt_number[phasertng_matthews_vm].push_back(type::flt_number(phasertng_matthews_vm,true,0,false,0));
          array__flt_number[phasertng_matthews_vm].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_matthews_vm].back().minset(),array__flt_number[phasertng_matthews_vm].back().minval(),array__flt_number[phasertng_matthews_vm].back().maxset(),array__flt_number[phasertng_matthews_vm].back().maxval()));
          variable.pop_back();
          compulsoryKey(input_stream,{"probability"});
          variable.push_back("probability");
          constexpr char phasertng_matthews_probability[] = ".phasertng.matthews.probability.";
          array__flt_number[phasertng_matthews_probability].push_back(type::flt_number(phasertng_matthews_probability,true,0,true,1));
          array__flt_number[phasertng_matthews_probability].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_matthews_probability].back().minset(),array__flt_number[phasertng_matthews_probability].back().minval(),array__flt_number[phasertng_matthews_probability].back().maxset(),array__flt_number[phasertng_matthews_probability].back().maxval()));
          variable.pop_back();
          compulsoryKey(input_stream,{"z"});
          variable.push_back("z");
          constexpr char phasertng_matthews_z[] = ".phasertng.matthews.z.";
          array__flt_number[phasertng_matthews_z].push_back(type::flt_number(phasertng_matthews_z,true,0,false,0));
          array__flt_number[phasertng_matthews_z].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_matthews_z].back().minset(),array__flt_number[phasertng_matthews_z].back().minval(),array__flt_number[phasertng_matthews_z].back().maxset(),array__flt_number[phasertng_matthews_z].back().maxval()));
          variable.pop_back();
        }
        else if (keyIs("mirrors")) {
          constexpr char phasertng_mirrors[] = ".phasertng.mirrors.";
          check_unique(phasertng_mirrors);
          value__strings[phasertng_mirrors].set_value(variable,get_strings(input_stream));
        }
        else if (keyIs("mode")) {
          std::string choices = "";
          while (optionalKey(input_stream,{"chk","bub","walk","tree","ipdb","imtz","imap","data","sga","sgx","aniso","tncso","tncs","feff","twin","srf","srfp","deep","join","pool","hyss","sscc","find","cca","ccs","esm","emm","msm","rellg","perm","ellg","etfz","eatm","emap","frf","frfr","brf","gyre","ftf","ftfr","btf","ptf","pose","put","pak","move","span","exp1","rbr","rbgs","tfz","mapz","fuse","spr","rmr","rbm","mcc","jog","info","ssa","ssd","ssm","ssp","ssr","rfac","beam","fetch","xref","test"}))
          {
            if (keyIsEol()) return false;
            else if (keyIsOptional()) { choices += string_value + " "; }
          }
          constexpr char phasertng_mode[] = ".phasertng.mode.";
          value__choice[phasertng_mode].set_value(choices);
        }
        else if (keyIs("model")) {
          compulsoryKey(input_stream,{"convert_rmsd_to_bfac","filename","selenomethionine","single_atom","tag","vrms_estimate"});
          if (keyIsEol()) return false;
          else if (keyIs("convert_rmsd_to_bfac")) {
            constexpr char phasertng_model_convert_rmsd_to_bfac[] = ".phasertng.model.convert_rmsd_to_bfac.";
            check_unique(phasertng_model_convert_rmsd_to_bfac);
            value__boolean[phasertng_model_convert_rmsd_to_bfac].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("filename")) {
            constexpr char phasertng_model_filename[] = ".phasertng.model.filename.";
            check_unique(phasertng_model_filename);
            value__path[phasertng_model_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("selenomethionine")) {
            constexpr char phasertng_model_selenomethionine[] = ".phasertng.model.selenomethionine.";
            check_unique(phasertng_model_selenomethionine);
            value__boolean[phasertng_model_selenomethionine].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("single_atom")) {
            compulsoryKey(input_stream,{"rmsd","scatterer","use"});
            if (keyIsEol()) return false;
            else if (keyIs("rmsd")) {
              constexpr char phasertng_model_single_atom_rmsd[] = ".phasertng.model.single_atom.rmsd.";
              check_unique(phasertng_model_single_atom_rmsd);
              value__flt_number[phasertng_model_single_atom_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_single_atom_rmsd].minset(),value__flt_number[phasertng_model_single_atom_rmsd].minval(),value__flt_number[phasertng_model_single_atom_rmsd].maxset(),value__flt_number[phasertng_model_single_atom_rmsd].maxval()));
            }
            else if (keyIs("scatterer")) {
              constexpr char phasertng_model_single_atom_scatterer[] = ".phasertng.model.single_atom.scatterer.";
              check_unique(phasertng_model_single_atom_scatterer);
              value__string[phasertng_model_single_atom_scatterer].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("use")) {
              constexpr char phasertng_model_single_atom_use[] = ".phasertng.model.single_atom.use.";
              check_unique(phasertng_model_single_atom_use);
              value__boolean[phasertng_model_single_atom_use].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_model_tag[] = ".phasertng.model.tag.";
            check_unique(phasertng_model_tag);
            value__string[phasertng_model_tag].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("vrms_estimate")) {
            constexpr char phasertng_model_vrms_estimate[] = ".phasertng.model.vrms_estimate.";
            check_unique(phasertng_model_vrms_estimate);
            value__flt_number[phasertng_model_vrms_estimate].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_vrms_estimate].minset(),value__flt_number[phasertng_model_vrms_estimate].minval(),value__flt_number[phasertng_model_vrms_estimate].maxset(),value__flt_number[phasertng_model_vrms_estimate].maxval()));
          }
        }
        else if (keyIs("model_as_map")) {
          compulsoryKey(input_stream,{"centre","extent_maximum","extent_minimum","filename","fmap","no_phases","oversample_resolution_limit","phimap","point_group_euler","point_group_symbol","require_box","resolution","sequence","solvent_constant","tag","vrms_estimate","wang_unsharpen_bfactor","wang_volume_factor"});
          if (keyIsEol()) return false;
          else if (keyIs("centre")) {
            constexpr char phasertng_model_as_map_centre[] = ".phasertng.model_as_map.centre.";
            check_unique(phasertng_model_as_map_centre);
            value__flt_vector[phasertng_model_as_map_centre].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_model_as_map_centre].minset(),value__flt_vector[phasertng_model_as_map_centre].minval(),value__flt_vector[phasertng_model_as_map_centre].maxset(),value__flt_vector[phasertng_model_as_map_centre].maxval()));
          }
          else if (keyIs("extent_maximum")) {
            constexpr char phasertng_model_as_map_extent_maximum[] = ".phasertng.model_as_map.extent_maximum.";
            check_unique(phasertng_model_as_map_extent_maximum);
            value__flt_vector[phasertng_model_as_map_extent_maximum].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_model_as_map_extent_maximum].minset(),value__flt_vector[phasertng_model_as_map_extent_maximum].minval(),value__flt_vector[phasertng_model_as_map_extent_maximum].maxset(),value__flt_vector[phasertng_model_as_map_extent_maximum].maxval()));
          }
          else if (keyIs("extent_minimum")) {
            constexpr char phasertng_model_as_map_extent_minimum[] = ".phasertng.model_as_map.extent_minimum.";
            check_unique(phasertng_model_as_map_extent_minimum);
            value__flt_vector[phasertng_model_as_map_extent_minimum].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_model_as_map_extent_minimum].minset(),value__flt_vector[phasertng_model_as_map_extent_minimum].minval(),value__flt_vector[phasertng_model_as_map_extent_minimum].maxset(),value__flt_vector[phasertng_model_as_map_extent_minimum].maxval()));
          }
          else if (keyIs("filename")) {
            constexpr char phasertng_model_as_map_filename[] = ".phasertng.model_as_map.filename.";
            check_unique(phasertng_model_as_map_filename);
            value__path[phasertng_model_as_map_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("fmap")) {
            constexpr char phasertng_model_as_map_fmap[] = ".phasertng.model_as_map.fmap.";
            check_unique(phasertng_model_as_map_fmap);
            value__string[phasertng_model_as_map_fmap].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("no_phases")) {
            constexpr char phasertng_model_as_map_no_phases[] = ".phasertng.model_as_map.no_phases.";
            check_unique(phasertng_model_as_map_no_phases);
            value__boolean[phasertng_model_as_map_no_phases].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("oversample_resolution_limit")) {
            constexpr char phasertng_model_as_map_oversample_resolution_limit[] = ".phasertng.model_as_map.oversample_resolution_limit.";
            check_unique(phasertng_model_as_map_oversample_resolution_limit);
            value__flt_number[phasertng_model_as_map_oversample_resolution_limit].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_oversample_resolution_limit].minset(),value__flt_number[phasertng_model_as_map_oversample_resolution_limit].minval(),value__flt_number[phasertng_model_as_map_oversample_resolution_limit].maxset(),value__flt_number[phasertng_model_as_map_oversample_resolution_limit].maxval()));
          }
          else if (keyIs("phimap")) {
            constexpr char phasertng_model_as_map_phimap[] = ".phasertng.model_as_map.phimap.";
            check_unique(phasertng_model_as_map_phimap);
            value__string[phasertng_model_as_map_phimap].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("point_group_euler")) {
            constexpr char phasertng_model_as_map_point_group_euler[] = ".phasertng.model_as_map.point_group_euler.";
            check_unique(phasertng_model_as_map_point_group_euler);
            value__flt_numbers[phasertng_model_as_map_point_group_euler].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_model_as_map_point_group_euler].minset(),value__flt_numbers[phasertng_model_as_map_point_group_euler].minval(),value__flt_numbers[phasertng_model_as_map_point_group_euler].maxset(),value__flt_numbers[phasertng_model_as_map_point_group_euler].maxval()));
          }
          else if (keyIs("point_group_symbol")) {
            constexpr char phasertng_model_as_map_point_group_symbol[] = ".phasertng.model_as_map.point_group_symbol.";
            check_unique(phasertng_model_as_map_point_group_symbol);
            value__string[phasertng_model_as_map_point_group_symbol].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("require_box")) {
            constexpr char phasertng_model_as_map_require_box[] = ".phasertng.model_as_map.require_box.";
            check_unique(phasertng_model_as_map_require_box);
            value__boolean[phasertng_model_as_map_require_box].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_model_as_map_resolution[] = ".phasertng.model_as_map.resolution.";
            check_unique(phasertng_model_as_map_resolution);
            value__flt_number[phasertng_model_as_map_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_resolution].minset(),value__flt_number[phasertng_model_as_map_resolution].minval(),value__flt_number[phasertng_model_as_map_resolution].maxset(),value__flt_number[phasertng_model_as_map_resolution].maxval()));
          }
          else if (keyIs("sequence")) {
            compulsoryKey(input_stream,{"filename"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_model_as_map_sequence_filename[] = ".phasertng.model_as_map.sequence.filename.";
              check_unique(phasertng_model_as_map_sequence_filename);
              value__path[phasertng_model_as_map_sequence_filename].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("solvent_constant")) {
            constexpr char phasertng_model_as_map_solvent_constant[] = ".phasertng.model_as_map.solvent_constant.";
            check_unique(phasertng_model_as_map_solvent_constant);
            value__flt_number[phasertng_model_as_map_solvent_constant].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_solvent_constant].minset(),value__flt_number[phasertng_model_as_map_solvent_constant].minval(),value__flt_number[phasertng_model_as_map_solvent_constant].maxset(),value__flt_number[phasertng_model_as_map_solvent_constant].maxval()));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_model_as_map_tag[] = ".phasertng.model_as_map.tag.";
            check_unique(phasertng_model_as_map_tag);
            value__string[phasertng_model_as_map_tag].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("vrms_estimate")) {
            constexpr char phasertng_model_as_map_vrms_estimate[] = ".phasertng.model_as_map.vrms_estimate.";
            check_unique(phasertng_model_as_map_vrms_estimate);
            value__flt_number[phasertng_model_as_map_vrms_estimate].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_vrms_estimate].minset(),value__flt_number[phasertng_model_as_map_vrms_estimate].minval(),value__flt_number[phasertng_model_as_map_vrms_estimate].maxset(),value__flt_number[phasertng_model_as_map_vrms_estimate].maxval()));
          }
          else if (keyIs("wang_unsharpen_bfactor")) {
            constexpr char phasertng_model_as_map_wang_unsharpen_bfactor[] = ".phasertng.model_as_map.wang_unsharpen_bfactor.";
            check_unique(phasertng_model_as_map_wang_unsharpen_bfactor);
            value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor].minset(),value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor].minval(),value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor].maxset(),value__flt_number[phasertng_model_as_map_wang_unsharpen_bfactor].maxval()));
          }
          else if (keyIs("wang_volume_factor")) {
            constexpr char phasertng_model_as_map_wang_volume_factor[] = ".phasertng.model_as_map.wang_volume_factor.";
            check_unique(phasertng_model_as_map_wang_volume_factor);
            value__flt_number[phasertng_model_as_map_wang_volume_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_model_as_map_wang_volume_factor].minset(),value__flt_number[phasertng_model_as_map_wang_volume_factor].minval(),value__flt_number[phasertng_model_as_map_wang_volume_factor].maxset(),value__flt_number[phasertng_model_as_map_wang_volume_factor].maxval()));
          }
        }
        else if (keyIs("molecular_transform")) {
          compulsoryKey(input_stream,{"barbwire_test","bfactor_trim","boxscale","bulk_solvent","decomposition_radius_factor","fix","helix_test","preparation","reorient","resolution","skip_bfactor_analysis","write_coordinates"});
          if (keyIsEol()) return false;
          else if (keyIs("barbwire_test")) {
            constexpr char phasertng_molecular_transform_barbwire_test[] = ".phasertng.molecular_transform.barbwire_test.";
            check_unique(phasertng_molecular_transform_barbwire_test);
            value__boolean[phasertng_molecular_transform_barbwire_test].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("bfactor_trim")) {
            constexpr char phasertng_molecular_transform_bfactor_trim[] = ".phasertng.molecular_transform.bfactor_trim.";
            check_unique(phasertng_molecular_transform_bfactor_trim);
            value__flt_number[phasertng_molecular_transform_bfactor_trim].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_bfactor_trim].minset(),value__flt_number[phasertng_molecular_transform_bfactor_trim].minval(),value__flt_number[phasertng_molecular_transform_bfactor_trim].maxset(),value__flt_number[phasertng_molecular_transform_bfactor_trim].maxval()));
          }
          else if (keyIs("boxscale")) {
            constexpr char phasertng_molecular_transform_boxscale[] = ".phasertng.molecular_transform.boxscale.";
            check_unique(phasertng_molecular_transform_boxscale);
            value__flt_number[phasertng_molecular_transform_boxscale].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_boxscale].minset(),value__flt_number[phasertng_molecular_transform_boxscale].minval(),value__flt_number[phasertng_molecular_transform_boxscale].maxset(),value__flt_number[phasertng_molecular_transform_boxscale].maxval()));
          }
          else if (keyIs("bulk_solvent")) {
            compulsoryKey(input_stream,{"bsol","fsol","use"});
            if (keyIsEol()) return false;
            else if (keyIs("bsol")) {
              constexpr char phasertng_molecular_transform_bulk_solvent_bsol[] = ".phasertng.molecular_transform.bulk_solvent.bsol.";
              check_unique(phasertng_molecular_transform_bulk_solvent_bsol);
              value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol].minset(),value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol].minval(),value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol].maxset(),value__flt_number[phasertng_molecular_transform_bulk_solvent_bsol].maxval()));
            }
            else if (keyIs("fsol")) {
              constexpr char phasertng_molecular_transform_bulk_solvent_fsol[] = ".phasertng.molecular_transform.bulk_solvent.fsol.";
              check_unique(phasertng_molecular_transform_bulk_solvent_fsol);
              value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol].minset(),value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol].minval(),value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol].maxset(),value__flt_number[phasertng_molecular_transform_bulk_solvent_fsol].maxval()));
            }
            else if (keyIs("use")) {
              constexpr char phasertng_molecular_transform_bulk_solvent_use[] = ".phasertng.molecular_transform.bulk_solvent.use.";
              check_unique(phasertng_molecular_transform_bulk_solvent_use);
              value__boolean[phasertng_molecular_transform_bulk_solvent_use].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("decomposition_radius_factor")) {
            constexpr char phasertng_molecular_transform_decomposition_radius_factor[] = ".phasertng.molecular_transform.decomposition_radius_factor.";
            check_unique(phasertng_molecular_transform_decomposition_radius_factor);
            value__flt_number[phasertng_molecular_transform_decomposition_radius_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_decomposition_radius_factor].minset(),value__flt_number[phasertng_molecular_transform_decomposition_radius_factor].minval(),value__flt_number[phasertng_molecular_transform_decomposition_radius_factor].maxset(),value__flt_number[phasertng_molecular_transform_decomposition_radius_factor].maxval()));
          }
          else if (keyIs("fix")) {
            constexpr char phasertng_molecular_transform_fix[] = ".phasertng.molecular_transform.fix.";
            check_unique(phasertng_molecular_transform_fix);
            value__boolean[phasertng_molecular_transform_fix].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("helix_test")) {
            constexpr char phasertng_molecular_transform_helix_test[] = ".phasertng.molecular_transform.helix_test.";
            check_unique(phasertng_molecular_transform_helix_test);
            value__boolean[phasertng_molecular_transform_helix_test].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("preparation")) {
            std::string choices = "";
            while (optionalKey(input_stream,{"off","decomposition","interpolation","trace","monostructure","variance"}))
            {
              if (keyIsEol()) return false;
              else if (keyIsOptional()) { choices += string_value + " "; }
            }
            constexpr char phasertng_molecular_transform_preparation[] = ".phasertng.molecular_transform.preparation.";
            value__choice[phasertng_molecular_transform_preparation].set_value(choices);
          }
          else if (keyIs("reorient")) {
            constexpr char phasertng_molecular_transform_reorient[] = ".phasertng.molecular_transform.reorient.";
            check_unique(phasertng_molecular_transform_reorient);
            value__boolean[phasertng_molecular_transform_reorient].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_molecular_transform_resolution[] = ".phasertng.molecular_transform.resolution.";
            check_unique(phasertng_molecular_transform_resolution);
            value__flt_number[phasertng_molecular_transform_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_molecular_transform_resolution].minset(),value__flt_number[phasertng_molecular_transform_resolution].minval(),value__flt_number[phasertng_molecular_transform_resolution].maxset(),value__flt_number[phasertng_molecular_transform_resolution].maxval()));
          }
          else if (keyIs("skip_bfactor_analysis")) {
            constexpr char phasertng_molecular_transform_skip_bfactor_analysis[] = ".phasertng.molecular_transform.skip_bfactor_analysis.";
            check_unique(phasertng_molecular_transform_skip_bfactor_analysis);
            value__boolean[phasertng_molecular_transform_skip_bfactor_analysis].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_coordinates")) {
            constexpr char phasertng_molecular_transform_write_coordinates[] = ".phasertng.molecular_transform.write_coordinates.";
            check_unique(phasertng_molecular_transform_write_coordinates);
            value__boolean[phasertng_molecular_transform_write_coordinates].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("mtzout")) {
          constexpr char phasertng_mtzout[] = ".phasertng.mtzout.";
          check_unique(phasertng_mtzout);
          value__string[phasertng_mtzout].set_value(variable,get_string(input_stream));
        }
        else if (keyIs("notifications")) {
          compulsoryKey(input_stream,{"advisory","error","git_revision","hires","result","success","warning"});
          if (keyIsEol()) return false;
          else if (keyIs("advisory")) {
            constexpr char phasertng_notifications_advisory[] = ".phasertng.notifications.advisory.";
            array__string[phasertng_notifications_advisory].push_back(type::string(phasertng_notifications_advisory));
            array__string[phasertng_notifications_advisory].back().set_value(variable,get_string(input_stream));
          }
          else if (keyIs("error")) {
            compulsoryKey(input_stream,{"code","message","type"});
            if (keyIsEol()) return false;
            else if (keyIs("code")) {
              constexpr char phasertng_notifications_error_code[] = ".phasertng.notifications.error.code.";
              check_unique(phasertng_notifications_error_code);
              value__int_number[phasertng_notifications_error_code].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_notifications_error_code].minset(),value__int_number[phasertng_notifications_error_code].minval(),value__int_number[phasertng_notifications_error_code].maxset(),value__int_number[phasertng_notifications_error_code].maxval()));
            }
            else if (keyIs("message")) {
              constexpr char phasertng_notifications_error_message[] = ".phasertng.notifications.error.message.";
              check_unique(phasertng_notifications_error_message);
              value__string[phasertng_notifications_error_message].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("type")) {
              compulsoryKey(input_stream,{"success","parse","syntax","input","fileset","fileopen","memory","killfile","killtime","fatal","result","unhandled","unknown","stdin","developer","python","assert"});
              constexpr char phasertng_notifications_error_type[] = ".phasertng.notifications.error.type.";
              check_unique(phasertng_notifications_error_type);
              value__choice[phasertng_notifications_error_type].set_value(variable,string_value);
            }
          }
          else if (keyIs("git_revision")) {
            constexpr char phasertng_notifications_git_revision[] = ".phasertng.notifications.git_revision.";
            check_unique(phasertng_notifications_git_revision);
            value__string[phasertng_notifications_git_revision].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("hires")) {
            constexpr char phasertng_notifications_hires[] = ".phasertng.notifications.hires.";
            check_unique(phasertng_notifications_hires);
            value__flt_number[phasertng_notifications_hires].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_notifications_hires].minset(),value__flt_number[phasertng_notifications_hires].minval(),value__flt_number[phasertng_notifications_hires].maxset(),value__flt_number[phasertng_notifications_hires].maxval()));
          }
          else if (keyIs("result")) {
            compulsoryKey(input_stream,{"filename_in_subdir","subdir"});
            if (keyIsEol()) return false;
            else if (keyIs("filename_in_subdir")) {
              constexpr char phasertng_notifications_result_filename_in_subdir[] = ".phasertng.notifications.result.filename_in_subdir.";
              check_unique(phasertng_notifications_result_filename_in_subdir);
              value__path[phasertng_notifications_result_filename_in_subdir].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("subdir")) {
              constexpr char phasertng_notifications_result_subdir[] = ".phasertng.notifications.result.subdir.";
              check_unique(phasertng_notifications_result_subdir);
              value__path[phasertng_notifications_result_subdir].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("success")) {
            constexpr char phasertng_notifications_success[] = ".phasertng.notifications.success.";
            check_unique(phasertng_notifications_success);
            value__boolean[phasertng_notifications_success].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("warning")) {
            constexpr char phasertng_notifications_warning[] = ".phasertng.notifications.warning.";
            array__string[phasertng_notifications_warning].push_back(type::string(phasertng_notifications_warning));
            array__string[phasertng_notifications_warning].back().set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("outlier")) {
          compulsoryKey(input_stream,{"information","maximum_printed","probability","reject"});
          if (keyIsEol()) return false;
          else if (keyIs("information")) {
            constexpr char phasertng_outlier_information[] = ".phasertng.outlier.information.";
            check_unique(phasertng_outlier_information);
            value__flt_number[phasertng_outlier_information].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_outlier_information].minset(),value__flt_number[phasertng_outlier_information].minval(),value__flt_number[phasertng_outlier_information].maxset(),value__flt_number[phasertng_outlier_information].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_outlier_maximum_printed[] = ".phasertng.outlier.maximum_printed.";
            check_unique(phasertng_outlier_maximum_printed);
            value__int_number[phasertng_outlier_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_outlier_maximum_printed].minset(),value__int_number[phasertng_outlier_maximum_printed].minval(),value__int_number[phasertng_outlier_maximum_printed].maxset(),value__int_number[phasertng_outlier_maximum_printed].maxval()));
          }
          else if (keyIs("probability")) {
            constexpr char phasertng_outlier_probability[] = ".phasertng.outlier.probability.";
            check_unique(phasertng_outlier_probability);
            value__flt_number[phasertng_outlier_probability].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_outlier_probability].minset(),value__flt_number[phasertng_outlier_probability].minval(),value__flt_number[phasertng_outlier_probability].maxset(),value__flt_number[phasertng_outlier_probability].maxval()));
          }
          else if (keyIs("reject")) {
            constexpr char phasertng_outlier_reject[] = ".phasertng.outlier.reject.";
            check_unique(phasertng_outlier_reject);
            value__boolean[phasertng_outlier_reject].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("overwrite")) {
          constexpr char phasertng_overwrite[] = ".phasertng.overwrite.";
          check_unique(phasertng_overwrite);
          value__boolean[phasertng_overwrite].set_value(variable,get_boolean(input_stream));
        }
        else if (keyIs("packing")) {
          compulsoryKey(input_stream,{"clash_coordinates","distance_factor","high_tfz","keep_high_tfz","maximum_printed","maximum_stored","move_to_compact","move_to_input","move_to_origin","move_when_tncs_present","overlap","percent","revert_on_failure","reverted"});
          if (keyIsEol()) return false;
          else if (keyIs("clash_coordinates")) {
            constexpr char phasertng_packing_clash_coordinates[] = ".phasertng.packing.clash_coordinates.";
            check_unique(phasertng_packing_clash_coordinates);
            value__boolean[phasertng_packing_clash_coordinates].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("distance_factor")) {
            constexpr char phasertng_packing_distance_factor[] = ".phasertng.packing.distance_factor.";
            check_unique(phasertng_packing_distance_factor);
            value__flt_number[phasertng_packing_distance_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_packing_distance_factor].minset(),value__flt_number[phasertng_packing_distance_factor].minval(),value__flt_number[phasertng_packing_distance_factor].maxset(),value__flt_number[phasertng_packing_distance_factor].maxval()));
          }
          else if (keyIs("high_tfz")) {
            constexpr char phasertng_packing_high_tfz[] = ".phasertng.packing.high_tfz.";
            check_unique(phasertng_packing_high_tfz);
            value__flt_number[phasertng_packing_high_tfz].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_packing_high_tfz].minset(),value__flt_number[phasertng_packing_high_tfz].minval(),value__flt_number[phasertng_packing_high_tfz].maxset(),value__flt_number[phasertng_packing_high_tfz].maxval()));
          }
          else if (keyIs("keep_high_tfz")) {
            constexpr char phasertng_packing_keep_high_tfz[] = ".phasertng.packing.keep_high_tfz.";
            check_unique(phasertng_packing_keep_high_tfz);
            value__boolean[phasertng_packing_keep_high_tfz].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_packing_maximum_printed[] = ".phasertng.packing.maximum_printed.";
            check_unique(phasertng_packing_maximum_printed);
            value__int_number[phasertng_packing_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_packing_maximum_printed].minset(),value__int_number[phasertng_packing_maximum_printed].minval(),value__int_number[phasertng_packing_maximum_printed].maxset(),value__int_number[phasertng_packing_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_packing_maximum_stored[] = ".phasertng.packing.maximum_stored.";
            check_unique(phasertng_packing_maximum_stored);
            value__int_number[phasertng_packing_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_packing_maximum_stored].minset(),value__int_number[phasertng_packing_maximum_stored].minval(),value__int_number[phasertng_packing_maximum_stored].maxset(),value__int_number[phasertng_packing_maximum_stored].maxval()));
          }
          else if (keyIs("move_to_compact")) {
            constexpr char phasertng_packing_move_to_compact[] = ".phasertng.packing.move_to_compact.";
            check_unique(phasertng_packing_move_to_compact);
            value__boolean[phasertng_packing_move_to_compact].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("move_to_input")) {
            constexpr char phasertng_packing_move_to_input[] = ".phasertng.packing.move_to_input.";
            check_unique(phasertng_packing_move_to_input);
            value__boolean[phasertng_packing_move_to_input].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("move_to_origin")) {
            constexpr char phasertng_packing_move_to_origin[] = ".phasertng.packing.move_to_origin.";
            check_unique(phasertng_packing_move_to_origin);
            value__boolean[phasertng_packing_move_to_origin].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("move_when_tncs_present")) {
            constexpr char phasertng_packing_move_when_tncs_present[] = ".phasertng.packing.move_when_tncs_present.";
            check_unique(phasertng_packing_move_when_tncs_present);
            value__boolean[phasertng_packing_move_when_tncs_present].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("overlap")) {
            constexpr char phasertng_packing_overlap[] = ".phasertng.packing.overlap.";
            check_unique(phasertng_packing_overlap);
            value__percent[phasertng_packing_overlap].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_packing_percent[] = ".phasertng.packing.percent.";
            check_unique(phasertng_packing_percent);
            value__percent[phasertng_packing_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("revert_on_failure")) {
            constexpr char phasertng_packing_revert_on_failure[] = ".phasertng.packing.revert_on_failure.";
            check_unique(phasertng_packing_revert_on_failure);
            value__boolean[phasertng_packing_revert_on_failure].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("reverted")) {
            constexpr char phasertng_packing_reverted[] = ".phasertng.packing.reverted.";
            check_unique(phasertng_packing_reverted);
            value__boolean[phasertng_packing_reverted].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("pdbin")) {
          compulsoryKey(input_stream,{"bulk_solvent","convert_rmsd_to_bfac","filename","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("bulk_solvent")) {
            compulsoryKey(input_stream,{"bsol","fsol","use"});
            if (keyIsEol()) return false;
            else if (keyIs("bsol")) {
              constexpr char phasertng_pdbin_bulk_solvent_bsol[] = ".phasertng.pdbin.bulk_solvent.bsol.";
              check_unique(phasertng_pdbin_bulk_solvent_bsol);
              value__flt_number[phasertng_pdbin_bulk_solvent_bsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_pdbin_bulk_solvent_bsol].minset(),value__flt_number[phasertng_pdbin_bulk_solvent_bsol].minval(),value__flt_number[phasertng_pdbin_bulk_solvent_bsol].maxset(),value__flt_number[phasertng_pdbin_bulk_solvent_bsol].maxval()));
            }
            else if (keyIs("fsol")) {
              constexpr char phasertng_pdbin_bulk_solvent_fsol[] = ".phasertng.pdbin.bulk_solvent.fsol.";
              check_unique(phasertng_pdbin_bulk_solvent_fsol);
              value__flt_number[phasertng_pdbin_bulk_solvent_fsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_pdbin_bulk_solvent_fsol].minset(),value__flt_number[phasertng_pdbin_bulk_solvent_fsol].minval(),value__flt_number[phasertng_pdbin_bulk_solvent_fsol].maxset(),value__flt_number[phasertng_pdbin_bulk_solvent_fsol].maxval()));
            }
            else if (keyIs("use")) {
              constexpr char phasertng_pdbin_bulk_solvent_use[] = ".phasertng.pdbin.bulk_solvent.use.";
              check_unique(phasertng_pdbin_bulk_solvent_use);
              value__boolean[phasertng_pdbin_bulk_solvent_use].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("convert_rmsd_to_bfac")) {
            constexpr char phasertng_pdbin_convert_rmsd_to_bfac[] = ".phasertng.pdbin.convert_rmsd_to_bfac.";
            check_unique(phasertng_pdbin_convert_rmsd_to_bfac);
            value__boolean[phasertng_pdbin_convert_rmsd_to_bfac].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("filename")) {
            constexpr char phasertng_pdbin_filename[] = ".phasertng.pdbin.filename.";
            check_unique(phasertng_pdbin_filename);
            value__path[phasertng_pdbin_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_pdbin_tag[] = ".phasertng.pdbin.tag.";
            check_unique(phasertng_pdbin_tag);
            value__string[phasertng_pdbin_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("pdbout")) {
          compulsoryKey(input_stream,{"deposited_title","filename","id","pdbid","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("deposited_title")) {
            constexpr char phasertng_pdbout_deposited_title[] = ".phasertng.pdbout.deposited_title.";
            check_unique(phasertng_pdbout_deposited_title);
            value__string[phasertng_pdbout_deposited_title].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("filename")) {
            constexpr char phasertng_pdbout_filename[] = ".phasertng.pdbout.filename.";
            check_unique(phasertng_pdbout_filename);
            value__path[phasertng_pdbout_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("id")) {
            constexpr char phasertng_pdbout_id[] = ".phasertng.pdbout.id.";
            check_unique(phasertng_pdbout_id);
            value__uuid_number[phasertng_pdbout_id].set_value(variable,get_uuid_number(input_stream,value__uuid_number[phasertng_pdbout_id].minset(),value__uuid_number[phasertng_pdbout_id].minval(),value__uuid_number[phasertng_pdbout_id].maxset(),value__uuid_number[phasertng_pdbout_id].maxval()));
          }
          else if (keyIs("pdbid")) {
            constexpr char phasertng_pdbout_pdbid[] = ".phasertng.pdbout.pdbid.";
            check_unique(phasertng_pdbout_pdbid);
            value__string[phasertng_pdbout_pdbid].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_pdbout_tag[] = ".phasertng.pdbout.tag.";
            check_unique(phasertng_pdbout_tag);
            value__string[phasertng_pdbout_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("permutations")) {
          compulsoryKey(input_stream,{"matthews_z","tags","tags_in_search_order"});
          if (keyIsEol()) return false;
          else if (keyIs("matthews_z")) {
            constexpr char phasertng_permutations_matthews_z[] = ".phasertng.permutations.matthews_z.";
            check_unique(phasertng_permutations_matthews_z);
            value__int_number[phasertng_permutations_matthews_z].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_permutations_matthews_z].minset(),value__int_number[phasertng_permutations_matthews_z].minval(),value__int_number[phasertng_permutations_matthews_z].maxset(),value__int_number[phasertng_permutations_matthews_z].maxval()));
          }
          else if (keyIs("tags")) {
            constexpr char phasertng_permutations_tags[] = ".phasertng.permutations.tags.";
            array__strings[phasertng_permutations_tags].push_back(type::strings(phasertng_permutations_tags));
            array__strings[phasertng_permutations_tags].back().set_value(variable,get_strings(input_stream));
          }
          else if (keyIs("tags_in_search_order")) {
            constexpr char phasertng_permutations_tags_in_search_order[] = ".phasertng.permutations.tags_in_search_order.";
            check_unique(phasertng_permutations_tags_in_search_order);
            value__strings[phasertng_permutations_tags_in_search_order].set_value(variable,get_strings(input_stream));
          }
        }
        else if (keyIs("perturbations")) {
          compulsoryKey(input_stream,{"number","range","selection"});
          if (keyIsEol()) return false;
          else if (keyIs("number")) {
            constexpr char phasertng_perturbations_number[] = ".phasertng.perturbations.number.";
            check_unique(phasertng_perturbations_number);
            value__int_number[phasertng_perturbations_number].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_perturbations_number].minset(),value__int_number[phasertng_perturbations_number].minval(),value__int_number[phasertng_perturbations_number].maxset(),value__int_number[phasertng_perturbations_number].maxval()));
          }
          else if (keyIs("range")) {
            constexpr char phasertng_perturbations_range[] = ".phasertng.perturbations.range.";
            check_unique(phasertng_perturbations_range);
            value__flt_paired[phasertng_perturbations_range].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_perturbations_range].minset(),value__flt_paired[phasertng_perturbations_range].minval(),value__flt_paired[phasertng_perturbations_range].maxset(),value__flt_paired[phasertng_perturbations_range].maxval()));
          }
          else if (keyIs("selection")) {
            compulsoryKey(input_stream,{"tra","rot","both"});
            constexpr char phasertng_perturbations_selection[] = ".phasertng.perturbations.selection.";
            check_unique(phasertng_perturbations_selection);
            value__choice[phasertng_perturbations_selection].set_value(variable,string_value);
          }
        }
        else if (keyIs("phased_translation_function")) {
          compulsoryKey(input_stream,{"add_pose_orientations","cluster","cluster_back","maximum_printed","maximum_stored","percent","sampling","sampling_factor","write_maps"});
          if (keyIsEol()) return false;
          else if (keyIs("add_pose_orientations")) {
            constexpr char phasertng_phased_translation_function_add_pose_orientations[] = ".phasertng.phased_translation_function.add_pose_orientations.";
            check_unique(phasertng_phased_translation_function_add_pose_orientations);
            value__boolean[phasertng_phased_translation_function_add_pose_orientations].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster")) {
            constexpr char phasertng_phased_translation_function_cluster[] = ".phasertng.phased_translation_function.cluster.";
            check_unique(phasertng_phased_translation_function_cluster);
            value__boolean[phasertng_phased_translation_function_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_phased_translation_function_cluster_back[] = ".phasertng.phased_translation_function.cluster_back.";
            check_unique(phasertng_phased_translation_function_cluster_back);
            value__int_number[phasertng_phased_translation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_phased_translation_function_cluster_back].minset(),value__int_number[phasertng_phased_translation_function_cluster_back].minval(),value__int_number[phasertng_phased_translation_function_cluster_back].maxset(),value__int_number[phasertng_phased_translation_function_cluster_back].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_phased_translation_function_maximum_printed[] = ".phasertng.phased_translation_function.maximum_printed.";
            check_unique(phasertng_phased_translation_function_maximum_printed);
            value__int_number[phasertng_phased_translation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_phased_translation_function_maximum_printed].minset(),value__int_number[phasertng_phased_translation_function_maximum_printed].minval(),value__int_number[phasertng_phased_translation_function_maximum_printed].maxset(),value__int_number[phasertng_phased_translation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_phased_translation_function_maximum_stored[] = ".phasertng.phased_translation_function.maximum_stored.";
            check_unique(phasertng_phased_translation_function_maximum_stored);
            value__int_number[phasertng_phased_translation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_phased_translation_function_maximum_stored].minset(),value__int_number[phasertng_phased_translation_function_maximum_stored].minval(),value__int_number[phasertng_phased_translation_function_maximum_stored].maxset(),value__int_number[phasertng_phased_translation_function_maximum_stored].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_phased_translation_function_percent[] = ".phasertng.phased_translation_function.percent.";
            check_unique(phasertng_phased_translation_function_percent);
            value__percent[phasertng_phased_translation_function_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("sampling")) {
            constexpr char phasertng_phased_translation_function_sampling[] = ".phasertng.phased_translation_function.sampling.";
            check_unique(phasertng_phased_translation_function_sampling);
            value__flt_number[phasertng_phased_translation_function_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_phased_translation_function_sampling].minset(),value__flt_number[phasertng_phased_translation_function_sampling].minval(),value__flt_number[phasertng_phased_translation_function_sampling].maxset(),value__flt_number[phasertng_phased_translation_function_sampling].maxval()));
          }
          else if (keyIs("sampling_factor")) {
            constexpr char phasertng_phased_translation_function_sampling_factor[] = ".phasertng.phased_translation_function.sampling_factor.";
            check_unique(phasertng_phased_translation_function_sampling_factor);
            value__flt_number[phasertng_phased_translation_function_sampling_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_phased_translation_function_sampling_factor].minset(),value__flt_number[phasertng_phased_translation_function_sampling_factor].minval(),value__flt_number[phasertng_phased_translation_function_sampling_factor].maxset(),value__flt_number[phasertng_phased_translation_function_sampling_factor].maxval()));
          }
          else if (keyIs("write_maps")) {
            constexpr char phasertng_phased_translation_function_write_maps[] = ".phasertng.phased_translation_function.write_maps.";
            check_unique(phasertng_phased_translation_function_write_maps);
            value__boolean[phasertng_phased_translation_function_write_maps].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("point_group")) {
          compulsoryKey(input_stream,{"angular_tolerance","calculate_from_coordinates","coverage","identity","rmsd","spatial_tolerance"});
          if (keyIsEol()) return false;
          else if (keyIs("angular_tolerance")) {
            constexpr char phasertng_point_group_angular_tolerance[] = ".phasertng.point_group.angular_tolerance.";
            check_unique(phasertng_point_group_angular_tolerance);
            value__flt_number[phasertng_point_group_angular_tolerance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_point_group_angular_tolerance].minset(),value__flt_number[phasertng_point_group_angular_tolerance].minval(),value__flt_number[phasertng_point_group_angular_tolerance].maxset(),value__flt_number[phasertng_point_group_angular_tolerance].maxval()));
          }
          else if (keyIs("calculate_from_coordinates")) {
            constexpr char phasertng_point_group_calculate_from_coordinates[] = ".phasertng.point_group.calculate_from_coordinates.";
            check_unique(phasertng_point_group_calculate_from_coordinates);
            value__boolean[phasertng_point_group_calculate_from_coordinates].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("coverage")) {
            constexpr char phasertng_point_group_coverage[] = ".phasertng.point_group.coverage.";
            check_unique(phasertng_point_group_coverage);
            value__percent[phasertng_point_group_coverage].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("identity")) {
            constexpr char phasertng_point_group_identity[] = ".phasertng.point_group.identity.";
            check_unique(phasertng_point_group_identity);
            value__percent[phasertng_point_group_identity].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("rmsd")) {
            constexpr char phasertng_point_group_rmsd[] = ".phasertng.point_group.rmsd.";
            check_unique(phasertng_point_group_rmsd);
            value__flt_number[phasertng_point_group_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_point_group_rmsd].minset(),value__flt_number[phasertng_point_group_rmsd].minval(),value__flt_number[phasertng_point_group_rmsd].maxset(),value__flt_number[phasertng_point_group_rmsd].maxval()));
          }
          else if (keyIs("spatial_tolerance")) {
            constexpr char phasertng_point_group_spatial_tolerance[] = ".phasertng.point_group.spatial_tolerance.";
            check_unique(phasertng_point_group_spatial_tolerance);
            value__flt_number[phasertng_point_group_spatial_tolerance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_point_group_spatial_tolerance].minset(),value__flt_number[phasertng_point_group_spatial_tolerance].minval(),value__flt_number[phasertng_point_group_spatial_tolerance].maxset(),value__flt_number[phasertng_point_group_spatial_tolerance].maxval()));
          }
        }
        else if (keyIs("pose_scoring")) {
          compulsoryKey(input_stream,{"maximum_printed","rescore"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_pose_scoring_maximum_printed[] = ".phasertng.pose_scoring.maximum_printed.";
            check_unique(phasertng_pose_scoring_maximum_printed);
            value__int_number[phasertng_pose_scoring_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_pose_scoring_maximum_printed].minset(),value__int_number[phasertng_pose_scoring_maximum_printed].minval(),value__int_number[phasertng_pose_scoring_maximum_printed].maxset(),value__int_number[phasertng_pose_scoring_maximum_printed].maxval()));
          }
          else if (keyIs("rescore")) {
            constexpr char phasertng_pose_scoring_rescore[] = ".phasertng.pose_scoring.rescore.";
            check_unique(phasertng_pose_scoring_rescore);
            value__boolean[phasertng_pose_scoring_rescore].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("put_solution")) {
          compulsoryKey(input_stream,{"clear","dag","pose","use_tncs"});
          if (keyIsEol()) return false;
          else if (keyIs("clear")) {
            constexpr char phasertng_put_solution_clear[] = ".phasertng.put_solution.clear.";
            check_unique(phasertng_put_solution_clear);
            value__boolean[phasertng_put_solution_clear].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("dag")) {
            compulsoryKey(input_stream,{"filename"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_put_solution_dag_filename[] = ".phasertng.put_solution.dag.filename.";
              check_unique(phasertng_put_solution_dag_filename);
              value__path[phasertng_put_solution_dag_filename].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("pose")) {
            compulsoryKey(input_stream,{"bfactor","euler","fractional","orthogonal_position","orthogonal_rotation"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_put_solution_pose_bfactor[] = ".phasertng.put_solution.pose.bfactor.";
              check_unique(phasertng_put_solution_pose_bfactor);
              value__flt_number[phasertng_put_solution_pose_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_put_solution_pose_bfactor].minset(),value__flt_number[phasertng_put_solution_pose_bfactor].minval(),value__flt_number[phasertng_put_solution_pose_bfactor].maxset(),value__flt_number[phasertng_put_solution_pose_bfactor].maxval()));
            }
            else if (keyIs("euler")) {
              constexpr char phasertng_put_solution_pose_euler[] = ".phasertng.put_solution.pose.euler.";
              check_unique(phasertng_put_solution_pose_euler);
              value__flt_vector[phasertng_put_solution_pose_euler].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_put_solution_pose_euler].minset(),value__flt_vector[phasertng_put_solution_pose_euler].minval(),value__flt_vector[phasertng_put_solution_pose_euler].maxset(),value__flt_vector[phasertng_put_solution_pose_euler].maxval()));
            }
            else if (keyIs("fractional")) {
              constexpr char phasertng_put_solution_pose_fractional[] = ".phasertng.put_solution.pose.fractional.";
              check_unique(phasertng_put_solution_pose_fractional);
              value__flt_vector[phasertng_put_solution_pose_fractional].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_put_solution_pose_fractional].minset(),value__flt_vector[phasertng_put_solution_pose_fractional].minval(),value__flt_vector[phasertng_put_solution_pose_fractional].maxset(),value__flt_vector[phasertng_put_solution_pose_fractional].maxval()));
            }
            else if (keyIs("orthogonal_position")) {
              constexpr char phasertng_put_solution_pose_orthogonal_position[] = ".phasertng.put_solution.pose.orthogonal_position.";
              check_unique(phasertng_put_solution_pose_orthogonal_position);
              value__flt_vector[phasertng_put_solution_pose_orthogonal_position].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_put_solution_pose_orthogonal_position].minset(),value__flt_vector[phasertng_put_solution_pose_orthogonal_position].minval(),value__flt_vector[phasertng_put_solution_pose_orthogonal_position].maxset(),value__flt_vector[phasertng_put_solution_pose_orthogonal_position].maxval()));
            }
            else if (keyIs("orthogonal_rotation")) {
              constexpr char phasertng_put_solution_pose_orthogonal_rotation[] = ".phasertng.put_solution.pose.orthogonal_rotation.";
              check_unique(phasertng_put_solution_pose_orthogonal_rotation);
              value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation].minset(),value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation].minval(),value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation].maxset(),value__flt_vector[phasertng_put_solution_pose_orthogonal_rotation].maxval()));
            }
          }
          else if (keyIs("use_tncs")) {
            constexpr char phasertng_put_solution_use_tncs[] = ".phasertng.put_solution.use_tncs.";
            check_unique(phasertng_put_solution_use_tncs);
            value__boolean[phasertng_put_solution_use_tncs].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("refinement")) {
          compulsoryKey(input_stream,{"method","ncyc","resolution","rigid_body","significant_twin_fraction","tncs_correction","top_files","treat_as_twinned"});
          if (keyIsEol()) return false;
          else if (keyIs("method")) {
            compulsoryKey(input_stream,{"phenix","refmac"});
            constexpr char phasertng_refinement_method[] = ".phasertng.refinement.method.";
            check_unique(phasertng_refinement_method);
            value__choice[phasertng_refinement_method].set_value(variable,string_value);
          }
          else if (keyIs("ncyc")) {
            compulsoryKey(input_stream,{"phenix","refmac"});
            if (keyIsEol()) return false;
            else if (keyIs("phenix")) {
              constexpr char phasertng_refinement_ncyc_phenix[] = ".phasertng.refinement.ncyc.phenix.";
              check_unique(phasertng_refinement_ncyc_phenix);
              value__int_number[phasertng_refinement_ncyc_phenix].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_refinement_ncyc_phenix].minset(),value__int_number[phasertng_refinement_ncyc_phenix].minval(),value__int_number[phasertng_refinement_ncyc_phenix].maxset(),value__int_number[phasertng_refinement_ncyc_phenix].maxval()));
            }
            else if (keyIs("refmac")) {
              constexpr char phasertng_refinement_ncyc_refmac[] = ".phasertng.refinement.ncyc.refmac.";
              check_unique(phasertng_refinement_ncyc_refmac);
              value__int_number[phasertng_refinement_ncyc_refmac].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_refinement_ncyc_refmac].minset(),value__int_number[phasertng_refinement_ncyc_refmac].minval(),value__int_number[phasertng_refinement_ncyc_refmac].maxset(),value__int_number[phasertng_refinement_ncyc_refmac].maxval()));
            }
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_refinement_resolution[] = ".phasertng.refinement.resolution.";
            check_unique(phasertng_refinement_resolution);
            value__flt_number[phasertng_refinement_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_refinement_resolution].minset(),value__flt_number[phasertng_refinement_resolution].minval(),value__flt_number[phasertng_refinement_resolution].maxset(),value__flt_number[phasertng_refinement_resolution].maxval()));
          }
          else if (keyIs("rigid_body")) {
            constexpr char phasertng_refinement_rigid_body[] = ".phasertng.refinement.rigid_body.";
            check_unique(phasertng_refinement_rigid_body);
            value__boolean[phasertng_refinement_rigid_body].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("significant_twin_fraction")) {
            constexpr char phasertng_refinement_significant_twin_fraction[] = ".phasertng.refinement.significant_twin_fraction.";
            check_unique(phasertng_refinement_significant_twin_fraction);
            value__flt_number[phasertng_refinement_significant_twin_fraction].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_refinement_significant_twin_fraction].minset(),value__flt_number[phasertng_refinement_significant_twin_fraction].minval(),value__flt_number[phasertng_refinement_significant_twin_fraction].maxset(),value__flt_number[phasertng_refinement_significant_twin_fraction].maxval()));
          }
          else if (keyIs("tncs_correction")) {
            constexpr char phasertng_refinement_tncs_correction[] = ".phasertng.refinement.tncs_correction.";
            check_unique(phasertng_refinement_tncs_correction);
            value__boolean[phasertng_refinement_tncs_correction].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("top_files")) {
            constexpr char phasertng_refinement_top_files[] = ".phasertng.refinement.top_files.";
            check_unique(phasertng_refinement_top_files);
            value__int_number[phasertng_refinement_top_files].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_refinement_top_files].minset(),value__int_number[phasertng_refinement_top_files].minval(),value__int_number[phasertng_refinement_top_files].maxset(),value__int_number[phasertng_refinement_top_files].maxval()));
          }
          else if (keyIs("treat_as_twinned")) {
            constexpr char phasertng_refinement_treat_as_twinned[] = ".phasertng.refinement.treat_as_twinned.";
            check_unique(phasertng_refinement_treat_as_twinned);
            value__boolean[phasertng_refinement_treat_as_twinned].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("reflections")) {
          compulsoryKey(input_stream,{"evalues","map_origin","oversampling","read_anomalous","read_map","resolution","wavelength"});
          if (keyIsEol()) return false;
          else if (keyIs("evalues")) {
            constexpr char phasertng_reflections_evalues[] = ".phasertng.reflections.evalues.";
            check_unique(phasertng_reflections_evalues);
            value__boolean[phasertng_reflections_evalues].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("map_origin")) {
            constexpr char phasertng_reflections_map_origin[] = ".phasertng.reflections.map_origin.";
            check_unique(phasertng_reflections_map_origin);
            value__flt_vector[phasertng_reflections_map_origin].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_reflections_map_origin].minset(),value__flt_vector[phasertng_reflections_map_origin].minval(),value__flt_vector[phasertng_reflections_map_origin].maxset(),value__flt_vector[phasertng_reflections_map_origin].maxval()));
          }
          else if (keyIs("oversampling")) {
            constexpr char phasertng_reflections_oversampling[] = ".phasertng.reflections.oversampling.";
            check_unique(phasertng_reflections_oversampling);
            value__flt_number[phasertng_reflections_oversampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_reflections_oversampling].minset(),value__flt_number[phasertng_reflections_oversampling].minval(),value__flt_number[phasertng_reflections_oversampling].maxset(),value__flt_number[phasertng_reflections_oversampling].maxval()));
          }
          else if (keyIs("read_anomalous")) {
            constexpr char phasertng_reflections_read_anomalous[] = ".phasertng.reflections.read_anomalous.";
            check_unique(phasertng_reflections_read_anomalous);
            value__boolean[phasertng_reflections_read_anomalous].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("read_map")) {
            constexpr char phasertng_reflections_read_map[] = ".phasertng.reflections.read_map.";
            check_unique(phasertng_reflections_read_map);
            value__boolean[phasertng_reflections_read_map].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_reflections_resolution[] = ".phasertng.reflections.resolution.";
            check_unique(phasertng_reflections_resolution);
            value__flt_number[phasertng_reflections_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_reflections_resolution].minset(),value__flt_number[phasertng_reflections_resolution].minval(),value__flt_number[phasertng_reflections_resolution].maxset(),value__flt_number[phasertng_reflections_resolution].maxval()));
          }
          else if (keyIs("wavelength")) {
            constexpr char phasertng_reflections_wavelength[] = ".phasertng.reflections.wavelength.";
            check_unique(phasertng_reflections_wavelength);
            value__flt_number[phasertng_reflections_wavelength].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_reflections_wavelength].minset(),value__flt_number[phasertng_reflections_wavelength].minval(),value__flt_number[phasertng_reflections_wavelength].maxset(),value__flt_number[phasertng_reflections_wavelength].maxval()));
          }
        }
        else if (keyIs("reflid")) {
          compulsoryKey(input_stream,{"force_read","id","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("force_read")) {
            constexpr char phasertng_reflid_force_read[] = ".phasertng.reflid.force_read.";
            check_unique(phasertng_reflid_force_read);
            value__boolean[phasertng_reflid_force_read].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("id")) {
            constexpr char phasertng_reflid_id[] = ".phasertng.reflid.id.";
            check_unique(phasertng_reflid_id);
            value__uuid_number[phasertng_reflid_id].set_value(variable,get_uuid_number(input_stream,value__uuid_number[phasertng_reflid_id].minset(),value__uuid_number[phasertng_reflid_id].minval(),value__uuid_number[phasertng_reflid_id].maxset(),value__uuid_number[phasertng_reflid_id].maxval()));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_reflid_tag[] = ".phasertng.reflid.tag.";
            check_unique(phasertng_reflid_tag);
            value__string[phasertng_reflid_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("rescore_rotation_function")) {
          compulsoryKey(input_stream,{"calculate_zscore","cluster","cluster_back","frf_percent_increase","maximum_printed","maximum_stored","nrand"});
          if (keyIsEol()) return false;
          else if (keyIs("calculate_zscore")) {
            constexpr char phasertng_rescore_rotation_function_calculate_zscore[] = ".phasertng.rescore_rotation_function.calculate_zscore.";
            check_unique(phasertng_rescore_rotation_function_calculate_zscore);
            value__boolean[phasertng_rescore_rotation_function_calculate_zscore].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster")) {
            constexpr char phasertng_rescore_rotation_function_cluster[] = ".phasertng.rescore_rotation_function.cluster.";
            check_unique(phasertng_rescore_rotation_function_cluster);
            value__boolean[phasertng_rescore_rotation_function_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_rescore_rotation_function_cluster_back[] = ".phasertng.rescore_rotation_function.cluster_back.";
            check_unique(phasertng_rescore_rotation_function_cluster_back);
            value__int_number[phasertng_rescore_rotation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_rotation_function_cluster_back].minset(),value__int_number[phasertng_rescore_rotation_function_cluster_back].minval(),value__int_number[phasertng_rescore_rotation_function_cluster_back].maxset(),value__int_number[phasertng_rescore_rotation_function_cluster_back].maxval()));
          }
          else if (keyIs("frf_percent_increase")) {
            constexpr char phasertng_rescore_rotation_function_frf_percent_increase[] = ".phasertng.rescore_rotation_function.frf_percent_increase.";
            check_unique(phasertng_rescore_rotation_function_frf_percent_increase);
            value__percent[phasertng_rescore_rotation_function_frf_percent_increase].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_rescore_rotation_function_maximum_printed[] = ".phasertng.rescore_rotation_function.maximum_printed.";
            check_unique(phasertng_rescore_rotation_function_maximum_printed);
            value__int_number[phasertng_rescore_rotation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_rotation_function_maximum_printed].minset(),value__int_number[phasertng_rescore_rotation_function_maximum_printed].minval(),value__int_number[phasertng_rescore_rotation_function_maximum_printed].maxset(),value__int_number[phasertng_rescore_rotation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_rescore_rotation_function_maximum_stored[] = ".phasertng.rescore_rotation_function.maximum_stored.";
            check_unique(phasertng_rescore_rotation_function_maximum_stored);
            value__int_number[phasertng_rescore_rotation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_rotation_function_maximum_stored].minset(),value__int_number[phasertng_rescore_rotation_function_maximum_stored].minval(),value__int_number[phasertng_rescore_rotation_function_maximum_stored].maxset(),value__int_number[phasertng_rescore_rotation_function_maximum_stored].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_rescore_rotation_function_nrand[] = ".phasertng.rescore_rotation_function.nrand.";
            check_unique(phasertng_rescore_rotation_function_nrand);
            value__int_number[phasertng_rescore_rotation_function_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_rotation_function_nrand].minset(),value__int_number[phasertng_rescore_rotation_function_nrand].minval(),value__int_number[phasertng_rescore_rotation_function_nrand].maxset(),value__int_number[phasertng_rescore_rotation_function_nrand].maxval()));
          }
        }
        else if (keyIs("rescore_translation_function")) {
          compulsoryKey(input_stream,{"calculate_zscore","cluster","cluster_back","maximum_printed","maximum_stored","nrand","percent","purge_tncs_duplicates"});
          if (keyIsEol()) return false;
          else if (keyIs("calculate_zscore")) {
            constexpr char phasertng_rescore_translation_function_calculate_zscore[] = ".phasertng.rescore_translation_function.calculate_zscore.";
            check_unique(phasertng_rescore_translation_function_calculate_zscore);
            value__boolean[phasertng_rescore_translation_function_calculate_zscore].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster")) {
            constexpr char phasertng_rescore_translation_function_cluster[] = ".phasertng.rescore_translation_function.cluster.";
            check_unique(phasertng_rescore_translation_function_cluster);
            value__boolean[phasertng_rescore_translation_function_cluster].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("cluster_back")) {
            constexpr char phasertng_rescore_translation_function_cluster_back[] = ".phasertng.rescore_translation_function.cluster_back.";
            check_unique(phasertng_rescore_translation_function_cluster_back);
            value__int_number[phasertng_rescore_translation_function_cluster_back].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_translation_function_cluster_back].minset(),value__int_number[phasertng_rescore_translation_function_cluster_back].minval(),value__int_number[phasertng_rescore_translation_function_cluster_back].maxset(),value__int_number[phasertng_rescore_translation_function_cluster_back].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_rescore_translation_function_maximum_printed[] = ".phasertng.rescore_translation_function.maximum_printed.";
            check_unique(phasertng_rescore_translation_function_maximum_printed);
            value__int_number[phasertng_rescore_translation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_translation_function_maximum_printed].minset(),value__int_number[phasertng_rescore_translation_function_maximum_printed].minval(),value__int_number[phasertng_rescore_translation_function_maximum_printed].maxset(),value__int_number[phasertng_rescore_translation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_rescore_translation_function_maximum_stored[] = ".phasertng.rescore_translation_function.maximum_stored.";
            check_unique(phasertng_rescore_translation_function_maximum_stored);
            value__int_number[phasertng_rescore_translation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_translation_function_maximum_stored].minset(),value__int_number[phasertng_rescore_translation_function_maximum_stored].minval(),value__int_number[phasertng_rescore_translation_function_maximum_stored].maxset(),value__int_number[phasertng_rescore_translation_function_maximum_stored].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_rescore_translation_function_nrand[] = ".phasertng.rescore_translation_function.nrand.";
            check_unique(phasertng_rescore_translation_function_nrand);
            value__int_number[phasertng_rescore_translation_function_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rescore_translation_function_nrand].minset(),value__int_number[phasertng_rescore_translation_function_nrand].minval(),value__int_number[phasertng_rescore_translation_function_nrand].maxset(),value__int_number[phasertng_rescore_translation_function_nrand].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_rescore_translation_function_percent[] = ".phasertng.rescore_translation_function.percent.";
            check_unique(phasertng_rescore_translation_function_percent);
            value__percent[phasertng_rescore_translation_function_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("purge_tncs_duplicates")) {
            constexpr char phasertng_rescore_translation_function_purge_tncs_duplicates[] = ".phasertng.rescore_translation_function.purge_tncs_duplicates.";
            check_unique(phasertng_rescore_translation_function_purge_tncs_duplicates);
            value__boolean[phasertng_rescore_translation_function_purge_tncs_duplicates].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("resolution")) {
          constexpr char phasertng_resolution[] = ".phasertng.resolution.";
          check_unique(phasertng_resolution);
          value__flt_number[phasertng_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_resolution].minset(),value__flt_number[phasertng_resolution].minval(),value__flt_number[phasertng_resolution].maxset(),value__flt_number[phasertng_resolution].maxval()));
        }
        else if (keyIs("rigid_body_maps")) {
          compulsoryKey(input_stream,{"keep_chains","maximum_stored"});
          if (keyIsEol()) return false;
          else if (keyIs("keep_chains")) {
            constexpr char phasertng_rigid_body_maps_keep_chains[] = ".phasertng.rigid_body_maps.keep_chains.";
            check_unique(phasertng_rigid_body_maps_keep_chains);
            value__boolean[phasertng_rigid_body_maps_keep_chains].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_rigid_body_maps_maximum_stored[] = ".phasertng.rigid_body_maps.maximum_stored.";
            check_unique(phasertng_rigid_body_maps_maximum_stored);
            value__int_number[phasertng_rigid_body_maps_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_rigid_body_maps_maximum_stored].minset(),value__int_number[phasertng_rigid_body_maps_maximum_stored].minval(),value__int_number[phasertng_rigid_body_maps_maximum_stored].maxset(),value__int_number[phasertng_rigid_body_maps_maximum_stored].maxval()));
          }
        }
        else if (keyIs("sampling_generator")) {
          compulsoryKey(input_stream,{"change_of_basis_op","continuous_shifts","cuts_constants","cuts_normals"});
          if (keyIsEol()) return false;
          else if (keyIs("change_of_basis_op")) {
            constexpr char phasertng_sampling_generator_change_of_basis_op[] = ".phasertng.sampling_generator.change_of_basis_op.";
            check_unique(phasertng_sampling_generator_change_of_basis_op);
            value__string[phasertng_sampling_generator_change_of_basis_op].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("continuous_shifts")) {
            constexpr char phasertng_sampling_generator_continuous_shifts[] = ".phasertng.sampling_generator.continuous_shifts.";
            check_unique(phasertng_sampling_generator_continuous_shifts);
            value__int_vector[phasertng_sampling_generator_continuous_shifts].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_sampling_generator_continuous_shifts].minset(),value__int_vector[phasertng_sampling_generator_continuous_shifts].minval(),value__int_vector[phasertng_sampling_generator_continuous_shifts].maxset(),value__int_vector[phasertng_sampling_generator_continuous_shifts].maxval()));
          }
          else if (keyIs("cuts_constants")) {
            constexpr char phasertng_sampling_generator_cuts_constants[] = ".phasertng.sampling_generator.cuts_constants.";
            check_unique(phasertng_sampling_generator_cuts_constants);
            value__flt_numbers[phasertng_sampling_generator_cuts_constants].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_sampling_generator_cuts_constants].minset(),value__flt_numbers[phasertng_sampling_generator_cuts_constants].minval(),value__flt_numbers[phasertng_sampling_generator_cuts_constants].maxset(),value__flt_numbers[phasertng_sampling_generator_cuts_constants].maxval()));
          }
          else if (keyIs("cuts_normals")) {
            constexpr char phasertng_sampling_generator_cuts_normals[] = ".phasertng.sampling_generator.cuts_normals.";
            check_unique(phasertng_sampling_generator_cuts_normals);
            value__flt_numbers[phasertng_sampling_generator_cuts_normals].set_value(variable,get_flt_numbers(input_stream,value__flt_numbers[phasertng_sampling_generator_cuts_normals].minset(),value__flt_numbers[phasertng_sampling_generator_cuts_normals].minval(),value__flt_numbers[phasertng_sampling_generator_cuts_normals].maxset(),value__flt_numbers[phasertng_sampling_generator_cuts_normals].maxval()));
          }
        }
        else if (keyIs("scattering")) {
          compulsoryKey(input_stream,{"fluorescence_scan","form_factors","sasaki_table"});
          if (keyIsEol()) return false;
          else if (keyIs("fluorescence_scan")) {
            compulsoryKey(input_stream,{"scatterer"});
            variable.push_back("scatterer");
            constexpr char phasertng_scattering_fluorescence_scan_scatterer[] = ".phasertng.scattering.fluorescence_scan.scatterer.";
            array__string[phasertng_scattering_fluorescence_scan_scatterer].push_back(type::string(phasertng_scattering_fluorescence_scan_scatterer));
            array__string[phasertng_scattering_fluorescence_scan_scatterer].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
            compulsoryKey(input_stream,{"fp"});
            variable.push_back("fp");
            constexpr char phasertng_scattering_fluorescence_scan_fp[] = ".phasertng.scattering.fluorescence_scan.fp.";
            array__flt_number[phasertng_scattering_fluorescence_scan_fp].push_back(type::flt_number(phasertng_scattering_fluorescence_scan_fp,false,0,false,0));
            array__flt_number[phasertng_scattering_fluorescence_scan_fp].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_scattering_fluorescence_scan_fp].back().minset(),array__flt_number[phasertng_scattering_fluorescence_scan_fp].back().minval(),array__flt_number[phasertng_scattering_fluorescence_scan_fp].back().maxset(),array__flt_number[phasertng_scattering_fluorescence_scan_fp].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"fdp"});
            variable.push_back("fdp");
            constexpr char phasertng_scattering_fluorescence_scan_fdp[] = ".phasertng.scattering.fluorescence_scan.fdp.";
            array__flt_number[phasertng_scattering_fluorescence_scan_fdp].push_back(type::flt_number(phasertng_scattering_fluorescence_scan_fdp,false,0,false,0));
            array__flt_number[phasertng_scattering_fluorescence_scan_fdp].back().set_value(variable,get_flt_number(input_stream,array__flt_number[phasertng_scattering_fluorescence_scan_fdp].back().minset(),array__flt_number[phasertng_scattering_fluorescence_scan_fdp].back().minval(),array__flt_number[phasertng_scattering_fluorescence_scan_fdp].back().maxset(),array__flt_number[phasertng_scattering_fluorescence_scan_fdp].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"fix_fdp"});
            variable.push_back("fix_fdp");
            compulsoryKey(input_stream,{"on","off","edge"});
            if (keyIsEol()) return false;
            else if (keyIsAdd("on",false))
            {
              constexpr char phasertng_scattering_fluorescence_scan_fix_fdp[] = ".phasertng.scattering.fluorescence_scan.fix_fdp.";
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].push_back(type::choice(phasertng_scattering_fluorescence_scan_fix_fdp,"on off edge ","edge"));
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].back().set_value(variable,string_value);
            }
            else if (keyIsAdd("off",false))
            {
              constexpr char phasertng_scattering_fluorescence_scan_fix_fdp[] = ".phasertng.scattering.fluorescence_scan.fix_fdp.";
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].push_back(type::choice(phasertng_scattering_fluorescence_scan_fix_fdp,"on off edge ","edge"));
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].back().set_value(variable,string_value);
            }
            else if (keyIsAdd("edge",false))
            {
              constexpr char phasertng_scattering_fluorescence_scan_fix_fdp[] = ".phasertng.scattering.fluorescence_scan.fix_fdp.";
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].push_back(type::choice(phasertng_scattering_fluorescence_scan_fix_fdp,"on off edge ","edge"));
              array__choice[phasertng_scattering_fluorescence_scan_fix_fdp].back().set_value(variable,string_value);
            }
            variable.pop_back();
          }
          else if (keyIs("form_factors")) {
            compulsoryKey(input_stream,{"xray","electron","neutron"});
            constexpr char phasertng_scattering_form_factors[] = ".phasertng.scattering.form_factors.";
            check_unique(phasertng_scattering_form_factors);
            value__choice[phasertng_scattering_form_factors].set_value(variable,string_value);
          }
          else if (keyIs("sasaki_table")) {
            compulsoryKey(input_stream,{"scatterer"});
            variable.push_back("scatterer");
            constexpr char phasertng_scattering_sasaki_table_scatterer[] = ".phasertng.scattering.sasaki_table.scatterer.";
            array__string[phasertng_scattering_sasaki_table_scatterer].push_back(type::string(phasertng_scattering_sasaki_table_scatterer));
            array__string[phasertng_scattering_sasaki_table_scatterer].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
            compulsoryKey(input_stream,{"move_to_edge"});
            variable.push_back("move_to_edge");
            constexpr char phasertng_scattering_sasaki_table_move_to_edge[] = ".phasertng.scattering.sasaki_table.move_to_edge.";
            array__boolean[phasertng_scattering_sasaki_table_move_to_edge].push_back(type::boolean(phasertng_scattering_sasaki_table_move_to_edge,true));
            array__boolean[phasertng_scattering_sasaki_table_move_to_edge].back().set_value(variable,get_boolean(input_stream));
            variable.pop_back();
          }
        }
        else if (keyIs("search")) {
          compulsoryKey(input_stream,{"id","tag"});
          if (keyIsEol()) return false;
          else if (keyIs("id")) {
            constexpr char phasertng_search_id[] = ".phasertng.search.id.";
            check_unique(phasertng_search_id);
            value__uuid_number[phasertng_search_id].set_value(variable,get_uuid_number(input_stream,value__uuid_number[phasertng_search_id].minset(),value__uuid_number[phasertng_search_id].minval(),value__uuid_number[phasertng_search_id].maxset(),value__uuid_number[phasertng_search_id].maxval()));
          }
          else if (keyIs("tag")) {
            constexpr char phasertng_search_tag[] = ".phasertng.search.tag.";
            check_unique(phasertng_search_tag);
            value__string[phasertng_search_tag].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("search_combination")) {
          constexpr char phasertng_search_combination[] = ".phasertng.search_combination.";
          array__strings[phasertng_search_combination].push_back(type::strings(phasertng_search_combination));
          array__strings[phasertng_search_combination].back().set_value(variable,get_strings(input_stream));
        }
        else if (keyIs("segmented_pruning")) {
          compulsoryKey(input_stream,{"maximum_stored","pruning","segment"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_segmented_pruning_maximum_stored[] = ".phasertng.segmented_pruning.maximum_stored.";
            check_unique(phasertng_segmented_pruning_maximum_stored);
            value__int_number[phasertng_segmented_pruning_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_segmented_pruning_maximum_stored].minset(),value__int_number[phasertng_segmented_pruning_maximum_stored].minval(),value__int_number[phasertng_segmented_pruning_maximum_stored].maxset(),value__int_number[phasertng_segmented_pruning_maximum_stored].maxval()));
          }
          else if (keyIs("pruning")) {
            compulsoryKey(input_stream,{"llg_bias_from_maximum_percent","low_resolution","maximum_percent"});
            if (keyIsEol()) return false;
            else if (keyIs("llg_bias_from_maximum_percent")) {
              constexpr char phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent[] = ".phasertng.segmented_pruning.pruning.llg_bias_from_maximum_percent.";
              check_unique(phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent);
              value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent].minset(),value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent].minval(),value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent].maxset(),value__flt_number[phasertng_segmented_pruning_pruning_llg_bias_from_maximum_percent].maxval()));
            }
            else if (keyIs("low_resolution")) {
              constexpr char phasertng_segmented_pruning_pruning_low_resolution[] = ".phasertng.segmented_pruning.pruning.low_resolution.";
              check_unique(phasertng_segmented_pruning_pruning_low_resolution);
              value__flt_number[phasertng_segmented_pruning_pruning_low_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_segmented_pruning_pruning_low_resolution].minset(),value__flt_number[phasertng_segmented_pruning_pruning_low_resolution].minval(),value__flt_number[phasertng_segmented_pruning_pruning_low_resolution].maxset(),value__flt_number[phasertng_segmented_pruning_pruning_low_resolution].maxval()));
            }
            else if (keyIs("maximum_percent")) {
              constexpr char phasertng_segmented_pruning_pruning_maximum_percent[] = ".phasertng.segmented_pruning.pruning.maximum_percent.";
              check_unique(phasertng_segmented_pruning_pruning_maximum_percent);
              value__percent[phasertng_segmented_pruning_pruning_maximum_percent].set_value(variable,get_percent(input_stream));
            }
          }
          else if (keyIs("segment")) {
            compulsoryKey(input_stream,{"delta_ellg","residues","rmsd"});
            if (keyIsEol()) return false;
            else if (keyIs("delta_ellg")) {
              constexpr char phasertng_segmented_pruning_segment_delta_ellg[] = ".phasertng.segmented_pruning.segment.delta_ellg.";
              check_unique(phasertng_segmented_pruning_segment_delta_ellg);
              value__flt_number[phasertng_segmented_pruning_segment_delta_ellg].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_segmented_pruning_segment_delta_ellg].minset(),value__flt_number[phasertng_segmented_pruning_segment_delta_ellg].minval(),value__flt_number[phasertng_segmented_pruning_segment_delta_ellg].maxset(),value__flt_number[phasertng_segmented_pruning_segment_delta_ellg].maxval()));
            }
            else if (keyIs("residues")) {
              constexpr char phasertng_segmented_pruning_segment_residues[] = ".phasertng.segmented_pruning.segment.residues.";
              check_unique(phasertng_segmented_pruning_segment_residues);
              value__int_number[phasertng_segmented_pruning_segment_residues].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_segmented_pruning_segment_residues].minset(),value__int_number[phasertng_segmented_pruning_segment_residues].minval(),value__int_number[phasertng_segmented_pruning_segment_residues].maxset(),value__int_number[phasertng_segmented_pruning_segment_residues].maxval()));
            }
            else if (keyIs("rmsd")) {
              constexpr char phasertng_segmented_pruning_segment_rmsd[] = ".phasertng.segmented_pruning.segment.rmsd.";
              check_unique(phasertng_segmented_pruning_segment_rmsd);
              value__flt_number[phasertng_segmented_pruning_segment_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_segmented_pruning_segment_rmsd].minset(),value__flt_number[phasertng_segmented_pruning_segment_rmsd].minval(),value__flt_number[phasertng_segmented_pruning_segment_rmsd].maxset(),value__flt_number[phasertng_segmented_pruning_segment_rmsd].maxval()));
            }
          }
        }
        else if (keyIs("self_rotation_function")) {
          compulsoryKey(input_stream,{"integration_radius","maximum_order","maximum_printed","maximum_stored","model","percent","plot_chi","resolution","sequence"});
          if (keyIsEol()) return false;
          else if (keyIs("integration_radius")) {
            constexpr char phasertng_self_rotation_function_integration_radius[] = ".phasertng.self_rotation_function.integration_radius.";
            check_unique(phasertng_self_rotation_function_integration_radius);
            value__flt_number[phasertng_self_rotation_function_integration_radius].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_self_rotation_function_integration_radius].minset(),value__flt_number[phasertng_self_rotation_function_integration_radius].minval(),value__flt_number[phasertng_self_rotation_function_integration_radius].maxset(),value__flt_number[phasertng_self_rotation_function_integration_radius].maxval()));
          }
          else if (keyIs("maximum_order")) {
            constexpr char phasertng_self_rotation_function_maximum_order[] = ".phasertng.self_rotation_function.maximum_order.";
            check_unique(phasertng_self_rotation_function_maximum_order);
            value__flt_number[phasertng_self_rotation_function_maximum_order].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_self_rotation_function_maximum_order].minset(),value__flt_number[phasertng_self_rotation_function_maximum_order].minval(),value__flt_number[phasertng_self_rotation_function_maximum_order].maxset(),value__flt_number[phasertng_self_rotation_function_maximum_order].maxval()));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_self_rotation_function_maximum_printed[] = ".phasertng.self_rotation_function.maximum_printed.";
            check_unique(phasertng_self_rotation_function_maximum_printed);
            value__int_number[phasertng_self_rotation_function_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_self_rotation_function_maximum_printed].minset(),value__int_number[phasertng_self_rotation_function_maximum_printed].minval(),value__int_number[phasertng_self_rotation_function_maximum_printed].maxset(),value__int_number[phasertng_self_rotation_function_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_self_rotation_function_maximum_stored[] = ".phasertng.self_rotation_function.maximum_stored.";
            check_unique(phasertng_self_rotation_function_maximum_stored);
            value__int_number[phasertng_self_rotation_function_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_self_rotation_function_maximum_stored].minset(),value__int_number[phasertng_self_rotation_function_maximum_stored].minval(),value__int_number[phasertng_self_rotation_function_maximum_stored].maxset(),value__int_number[phasertng_self_rotation_function_maximum_stored].maxval()));
          }
          else if (keyIs("model")) {
            compulsoryKey(input_stream,{"filename","integration_radius_factor"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_self_rotation_function_model_filename[] = ".phasertng.self_rotation_function.model.filename.";
              check_unique(phasertng_self_rotation_function_model_filename);
              value__path[phasertng_self_rotation_function_model_filename].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("integration_radius_factor")) {
              constexpr char phasertng_self_rotation_function_model_integration_radius_factor[] = ".phasertng.self_rotation_function.model.integration_radius_factor.";
              check_unique(phasertng_self_rotation_function_model_integration_radius_factor);
              value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor].minset(),value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor].minval(),value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor].maxset(),value__flt_number[phasertng_self_rotation_function_model_integration_radius_factor].maxval()));
            }
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_self_rotation_function_percent[] = ".phasertng.self_rotation_function.percent.";
            check_unique(phasertng_self_rotation_function_percent);
            value__percent[phasertng_self_rotation_function_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("plot_chi")) {
            constexpr char phasertng_self_rotation_function_plot_chi[] = ".phasertng.self_rotation_function.plot_chi.";
            check_unique(phasertng_self_rotation_function_plot_chi);
            value__int_number[phasertng_self_rotation_function_plot_chi].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_self_rotation_function_plot_chi].minset(),value__int_number[phasertng_self_rotation_function_plot_chi].minval(),value__int_number[phasertng_self_rotation_function_plot_chi].maxset(),value__int_number[phasertng_self_rotation_function_plot_chi].maxval()));
          }
          else if (keyIs("resolution")) {
            constexpr char phasertng_self_rotation_function_resolution[] = ".phasertng.self_rotation_function.resolution.";
            check_unique(phasertng_self_rotation_function_resolution);
            value__flt_number[phasertng_self_rotation_function_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_self_rotation_function_resolution].minset(),value__flt_number[phasertng_self_rotation_function_resolution].minval(),value__flt_number[phasertng_self_rotation_function_resolution].maxset(),value__flt_number[phasertng_self_rotation_function_resolution].maxval()));
          }
          else if (keyIs("sequence")) {
            compulsoryKey(input_stream,{"filename","integration_radius_factor"});
            if (keyIsEol()) return false;
            else if (keyIs("filename")) {
              constexpr char phasertng_self_rotation_function_sequence_filename[] = ".phasertng.self_rotation_function.sequence.filename.";
              check_unique(phasertng_self_rotation_function_sequence_filename);
              value__path[phasertng_self_rotation_function_sequence_filename].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("integration_radius_factor")) {
              constexpr char phasertng_self_rotation_function_sequence_integration_radius_factor[] = ".phasertng.self_rotation_function.sequence.integration_radius_factor.";
              check_unique(phasertng_self_rotation_function_sequence_integration_radius_factor);
              value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor].minset(),value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor].minval(),value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor].maxset(),value__flt_number[phasertng_self_rotation_function_sequence_integration_radius_factor].maxval()));
            }
          }
        }
        else if (keyIs("seqout")) {
          compulsoryKey(input_stream,{"filename"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_seqout_filename[] = ".phasertng.seqout.filename.";
            check_unique(phasertng_seqout_filename);
            value__path[phasertng_seqout_filename].set_value(variable,get_path(input_stream));
          }
        }
        else if (keyIs("sgalt")) {
          compulsoryKey(input_stream,{"selection","spacegroups"});
          if (keyIsEol()) return false;
          else if (keyIs("selection")) {
            compulsoryKey(input_stream,{"all","hand","list","original"});
            constexpr char phasertng_sgalt_selection[] = ".phasertng.sgalt.selection.";
            check_unique(phasertng_sgalt_selection);
            value__choice[phasertng_sgalt_selection].set_value(variable,string_value);
          }
          else if (keyIs("spacegroups")) {
            constexpr char phasertng_sgalt_spacegroups[] = ".phasertng.sgalt.spacegroups.";
            check_unique(phasertng_sgalt_spacegroups);
            value__strings[phasertng_sgalt_spacegroups].set_value(variable,get_strings(input_stream));
          }
        }
        else if (keyIs("solvent_sigmaa")) {
          compulsoryKey(input_stream,{"reflections_as_map_versus_model","reflections_versus_model","reflections_versus_model_as_map"});
          if (keyIsEol()) return false;
          else if (keyIs("reflections_as_map_versus_model")) {
            compulsoryKey(input_stream,{"babinet_minimum","bsol","fsol"});
            if (keyIsEol()) return false;
            else if (keyIs("babinet_minimum")) {
              constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.";
              check_unique(phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum);
              value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_babinet_minimum].maxval()));
            }
            else if (keyIs("bsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_bsol].maxval()));
            }
            else if (keyIs("fsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol[] = ".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_as_map_versus_model_fsol].maxval()));
            }
          }
          else if (keyIs("reflections_versus_model")) {
            compulsoryKey(input_stream,{"babinet_minimum","bsol","fsol"});
            if (keyIsEol()) return false;
            else if (keyIs("babinet_minimum")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_babinet_minimum].maxval()));
            }
            else if (keyIs("bsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_bsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model.bsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_bsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_bsol].maxval()));
            }
            else if (keyIs("fsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_fsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model.fsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_fsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_fsol].maxval()));
            }
          }
          else if (keyIs("reflections_versus_model_as_map")) {
            compulsoryKey(input_stream,{"babinet_minimum","bsol","fsol"});
            if (keyIsEol()) return false;
            else if (keyIs("babinet_minimum")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.babinet_minimum.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_babinet_minimum].maxval()));
            }
            else if (keyIs("bsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.bsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_bsol].maxval()));
            }
            else if (keyIs("fsol")) {
              constexpr char phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol[] = ".phasertng.solvent_sigmaa.reflections_versus_model_as_map.fsol.";
              check_unique(phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol);
              value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol].minset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol].minval(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol].maxset(),value__flt_number[phasertng_solvent_sigmaa_reflections_versus_model_as_map_fsol].maxval()));
            }
          }
        }
        else if (keyIs("space_group_expansion")) {
          compulsoryKey(input_stream,{"hall","hermann_mauguin_symbol","order_z_expansion_ratio","spacegroup"});
          if (keyIsEol()) return false;
          else if (keyIs("hall")) {
            constexpr char phasertng_space_group_expansion_hall[] = ".phasertng.space_group_expansion.hall.";
            check_unique(phasertng_space_group_expansion_hall);
            value__string[phasertng_space_group_expansion_hall].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("hermann_mauguin_symbol")) {
            constexpr char phasertng_space_group_expansion_hermann_mauguin_symbol[] = ".phasertng.space_group_expansion.hermann_mauguin_symbol.";
            check_unique(phasertng_space_group_expansion_hermann_mauguin_symbol);
            value__string[phasertng_space_group_expansion_hermann_mauguin_symbol].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("order_z_expansion_ratio")) {
            constexpr char phasertng_space_group_expansion_order_z_expansion_ratio[] = ".phasertng.space_group_expansion.order_z_expansion_ratio.";
            check_unique(phasertng_space_group_expansion_order_z_expansion_ratio);
            value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio].minset(),value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio].minval(),value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio].maxset(),value__int_number[phasertng_space_group_expansion_order_z_expansion_ratio].maxval()));
          }
          else if (keyIs("spacegroup")) {
            constexpr char phasertng_space_group_expansion_spacegroup[] = ".phasertng.space_group_expansion.spacegroup.";
            check_unique(phasertng_space_group_expansion_spacegroup);
            value__string[phasertng_space_group_expansion_spacegroup].set_value(variable,get_string(input_stream));
          }
        }
        else if (keyIs("spanning")) {
          compulsoryKey(input_stream,{"distance","find_gaps","maximum_printed","maximum_stored"});
          if (keyIsEol()) return false;
          else if (keyIs("distance")) {
            constexpr char phasertng_spanning_distance[] = ".phasertng.spanning.distance.";
            check_unique(phasertng_spanning_distance);
            value__flt_number[phasertng_spanning_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_spanning_distance].minset(),value__flt_number[phasertng_spanning_distance].minval(),value__flt_number[phasertng_spanning_distance].maxset(),value__flt_number[phasertng_spanning_distance].maxval()));
          }
          else if (keyIs("find_gaps")) {
            constexpr char phasertng_spanning_find_gaps[] = ".phasertng.spanning.find_gaps.";
            check_unique(phasertng_spanning_find_gaps);
            value__boolean[phasertng_spanning_find_gaps].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_spanning_maximum_printed[] = ".phasertng.spanning.maximum_printed.";
            check_unique(phasertng_spanning_maximum_printed);
            value__int_number[phasertng_spanning_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_spanning_maximum_printed].minset(),value__int_number[phasertng_spanning_maximum_printed].minval(),value__int_number[phasertng_spanning_maximum_printed].maxset(),value__int_number[phasertng_spanning_maximum_printed].maxval()));
          }
          else if (keyIs("maximum_stored")) {
            constexpr char phasertng_spanning_maximum_stored[] = ".phasertng.spanning.maximum_stored.";
            check_unique(phasertng_spanning_maximum_stored);
            value__int_number[phasertng_spanning_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_spanning_maximum_stored].minset(),value__int_number[phasertng_spanning_maximum_stored].minval(),value__int_number[phasertng_spanning_maximum_stored].maxset(),value__int_number[phasertng_spanning_maximum_stored].maxval()));
          }
        }
        else if (keyIs("subgroup")) {
          compulsoryKey(input_stream,{"expansion","original","use_all_sysabs","use_no_sysabs"});
          if (keyIsEol()) return false;
          else if (keyIs("expansion")) {
            compulsoryKey(input_stream,{"hall"});
            variable.push_back("hall");
            constexpr char phasertng_subgroup_expansion_hall[] = ".phasertng.subgroup.expansion.hall.";
            array__string[phasertng_subgroup_expansion_hall].push_back(type::string(phasertng_subgroup_expansion_hall));
            array__string[phasertng_subgroup_expansion_hall].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
            compulsoryKey(input_stream,{"spacegroup"});
            variable.push_back("spacegroup");
            constexpr char phasertng_subgroup_expansion_spacegroup[] = ".phasertng.subgroup.expansion.spacegroup.";
            array__string[phasertng_subgroup_expansion_spacegroup].push_back(type::string(phasertng_subgroup_expansion_spacegroup));
            array__string[phasertng_subgroup_expansion_spacegroup].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
            compulsoryKey(input_stream,{"hermann_mauguin_symbol"});
            variable.push_back("hermann_mauguin_symbol");
            constexpr char phasertng_subgroup_expansion_hermann_mauguin_symbol[] = ".phasertng.subgroup.expansion.hermann_mauguin_symbol.";
            array__string[phasertng_subgroup_expansion_hermann_mauguin_symbol].push_back(type::string(phasertng_subgroup_expansion_hermann_mauguin_symbol));
            array__string[phasertng_subgroup_expansion_hermann_mauguin_symbol].back().set_value(variable,get_string(input_stream));
            variable.pop_back();
          }
          else if (keyIs("original")) {
            compulsoryKey(input_stream,{"hall","hermann_mauguin_symbol","spacegroup"});
            if (keyIsEol()) return false;
            else if (keyIs("hall")) {
              constexpr char phasertng_subgroup_original_hall[] = ".phasertng.subgroup.original.hall.";
              check_unique(phasertng_subgroup_original_hall);
              value__string[phasertng_subgroup_original_hall].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("hermann_mauguin_symbol")) {
              constexpr char phasertng_subgroup_original_hermann_mauguin_symbol[] = ".phasertng.subgroup.original.hermann_mauguin_symbol.";
              check_unique(phasertng_subgroup_original_hermann_mauguin_symbol);
              value__string[phasertng_subgroup_original_hermann_mauguin_symbol].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("spacegroup")) {
              constexpr char phasertng_subgroup_original_spacegroup[] = ".phasertng.subgroup.original.spacegroup.";
              check_unique(phasertng_subgroup_original_spacegroup);
              value__string[phasertng_subgroup_original_spacegroup].set_value(variable,get_string(input_stream));
            }
          }
          else if (keyIs("use_all_sysabs")) {
            constexpr char phasertng_subgroup_use_all_sysabs[] = ".phasertng.subgroup.use_all_sysabs.";
            check_unique(phasertng_subgroup_use_all_sysabs);
            value__boolean[phasertng_subgroup_use_all_sysabs].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("use_no_sysabs")) {
            constexpr char phasertng_subgroup_use_no_sysabs[] = ".phasertng.subgroup.use_no_sysabs.";
            check_unique(phasertng_subgroup_use_no_sysabs);
            value__boolean[phasertng_subgroup_use_no_sysabs].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("substructure")) {
          compulsoryKey(input_stream,{"completion","constellation","content_analysis","determination","hyss","patterson_analysis","preparation","scatterer","test_fft_versus_summation"});
          if (keyIsEol()) return false;
          else if (keyIs("completion")) {
            compulsoryKey(input_stream,{"change_scatterers","complete","ncyc","peaks_above_deepest_hole","rfactor_termination","scatterer","separation_distance","swap_to_anisotropic_scatterers","top_peak_below_deepest_hole","top_peak_below_deepest_hole_zscore","write_files","zscore"});
            if (keyIsEol()) return false;
            else if (keyIs("change_scatterers")) {
              constexpr char phasertng_substructure_completion_change_scatterers[] = ".phasertng.substructure.completion.change_scatterers.";
              check_unique(phasertng_substructure_completion_change_scatterers);
              value__boolean[phasertng_substructure_completion_change_scatterers].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("complete")) {
              constexpr char phasertng_substructure_completion_complete[] = ".phasertng.substructure.completion.complete.";
              check_unique(phasertng_substructure_completion_complete);
              value__boolean[phasertng_substructure_completion_complete].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("ncyc")) {
              constexpr char phasertng_substructure_completion_ncyc[] = ".phasertng.substructure.completion.ncyc.";
              check_unique(phasertng_substructure_completion_ncyc);
              value__int_number[phasertng_substructure_completion_ncyc].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_completion_ncyc].minset(),value__int_number[phasertng_substructure_completion_ncyc].minval(),value__int_number[phasertng_substructure_completion_ncyc].maxset(),value__int_number[phasertng_substructure_completion_ncyc].maxval()));
            }
            else if (keyIs("peaks_above_deepest_hole")) {
              constexpr char phasertng_substructure_completion_peaks_above_deepest_hole[] = ".phasertng.substructure.completion.peaks_above_deepest_hole.";
              check_unique(phasertng_substructure_completion_peaks_above_deepest_hole);
              value__boolean[phasertng_substructure_completion_peaks_above_deepest_hole].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("rfactor_termination")) {
              constexpr char phasertng_substructure_completion_rfactor_termination[] = ".phasertng.substructure.completion.rfactor_termination.";
              check_unique(phasertng_substructure_completion_rfactor_termination);
              value__percent[phasertng_substructure_completion_rfactor_termination].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("scatterer")) {
              constexpr char phasertng_substructure_completion_scatterer[] = ".phasertng.substructure.completion.scatterer.";
              check_unique(phasertng_substructure_completion_scatterer);
              value__strings[phasertng_substructure_completion_scatterer].set_value(variable,get_strings(input_stream));
            }
            else if (keyIs("separation_distance")) {
              constexpr char phasertng_substructure_completion_separation_distance[] = ".phasertng.substructure.completion.separation_distance.";
              check_unique(phasertng_substructure_completion_separation_distance);
              value__flt_number[phasertng_substructure_completion_separation_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_completion_separation_distance].minset(),value__flt_number[phasertng_substructure_completion_separation_distance].minval(),value__flt_number[phasertng_substructure_completion_separation_distance].maxset(),value__flt_number[phasertng_substructure_completion_separation_distance].maxval()));
            }
            else if (keyIs("swap_to_anisotropic_scatterers")) {
              constexpr char phasertng_substructure_completion_swap_to_anisotropic_scatterers[] = ".phasertng.substructure.completion.swap_to_anisotropic_scatterers.";
              check_unique(phasertng_substructure_completion_swap_to_anisotropic_scatterers);
              value__boolean[phasertng_substructure_completion_swap_to_anisotropic_scatterers].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("top_peak_below_deepest_hole")) {
              constexpr char phasertng_substructure_completion_top_peak_below_deepest_hole[] = ".phasertng.substructure.completion.top_peak_below_deepest_hole.";
              check_unique(phasertng_substructure_completion_top_peak_below_deepest_hole);
              value__boolean[phasertng_substructure_completion_top_peak_below_deepest_hole].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("top_peak_below_deepest_hole_zscore")) {
              constexpr char phasertng_substructure_completion_top_peak_below_deepest_hole_zscore[] = ".phasertng.substructure.completion.top_peak_below_deepest_hole_zscore.";
              check_unique(phasertng_substructure_completion_top_peak_below_deepest_hole_zscore);
              value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore].minset(),value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore].minval(),value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore].maxset(),value__flt_number[phasertng_substructure_completion_top_peak_below_deepest_hole_zscore].maxval()));
            }
            else if (keyIs("write_files")) {
              constexpr char phasertng_substructure_completion_write_files[] = ".phasertng.substructure.completion.write_files.";
              check_unique(phasertng_substructure_completion_write_files);
              value__boolean[phasertng_substructure_completion_write_files].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("zscore")) {
              constexpr char phasertng_substructure_completion_zscore[] = ".phasertng.substructure.completion.zscore.";
              check_unique(phasertng_substructure_completion_zscore);
              value__flt_number[phasertng_substructure_completion_zscore].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_completion_zscore].minset(),value__flt_number[phasertng_substructure_completion_zscore].minval(),value__flt_number[phasertng_substructure_completion_zscore].maxset(),value__flt_number[phasertng_substructure_completion_zscore].maxval()));
            }
          }
          else if (keyIs("constellation")) {
            compulsoryKey(input_stream,{"distance","number_of_shifts","size"});
            if (keyIsEol()) return false;
            else if (keyIs("distance")) {
              constexpr char phasertng_substructure_constellation_distance[] = ".phasertng.substructure.constellation.distance.";
              check_unique(phasertng_substructure_constellation_distance);
              value__flt_number[phasertng_substructure_constellation_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_constellation_distance].minset(),value__flt_number[phasertng_substructure_constellation_distance].minval(),value__flt_number[phasertng_substructure_constellation_distance].maxset(),value__flt_number[phasertng_substructure_constellation_distance].maxval()));
            }
            else if (keyIs("number_of_shifts")) {
              constexpr char phasertng_substructure_constellation_number_of_shifts[] = ".phasertng.substructure.constellation.number_of_shifts.";
              check_unique(phasertng_substructure_constellation_number_of_shifts);
              value__int_number[phasertng_substructure_constellation_number_of_shifts].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_constellation_number_of_shifts].minset(),value__int_number[phasertng_substructure_constellation_number_of_shifts].minval(),value__int_number[phasertng_substructure_constellation_number_of_shifts].maxset(),value__int_number[phasertng_substructure_constellation_number_of_shifts].maxval()));
            }
            else if (keyIs("size")) {
              constexpr char phasertng_substructure_constellation_size[] = ".phasertng.substructure.constellation.size.";
              check_unique(phasertng_substructure_constellation_size);
              value__int_number[phasertng_substructure_constellation_size].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_constellation_size].minset(),value__int_number[phasertng_substructure_constellation_size].minval(),value__int_number[phasertng_substructure_constellation_size].maxset(),value__int_number[phasertng_substructure_constellation_size].maxval()));
            }
          }
          else if (keyIs("content_analysis")) {
            compulsoryKey(input_stream,{"delta_bfactor","number","signal"});
            if (keyIsEol()) return false;
            else if (keyIs("delta_bfactor")) {
              constexpr char phasertng_substructure_content_analysis_delta_bfactor[] = ".phasertng.substructure.content_analysis.delta_bfactor.";
              check_unique(phasertng_substructure_content_analysis_delta_bfactor);
              value__flt_number[phasertng_substructure_content_analysis_delta_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_content_analysis_delta_bfactor].minset(),value__flt_number[phasertng_substructure_content_analysis_delta_bfactor].minval(),value__flt_number[phasertng_substructure_content_analysis_delta_bfactor].maxset(),value__flt_number[phasertng_substructure_content_analysis_delta_bfactor].maxval()));
            }
            else if (keyIs("number")) {
              constexpr char phasertng_substructure_content_analysis_number[] = ".phasertng.substructure.content_analysis.number.";
              check_unique(phasertng_substructure_content_analysis_number);
              value__flt_number[phasertng_substructure_content_analysis_number].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_content_analysis_number].minset(),value__flt_number[phasertng_substructure_content_analysis_number].minval(),value__flt_number[phasertng_substructure_content_analysis_number].maxset(),value__flt_number[phasertng_substructure_content_analysis_number].maxval()));
            }
            else if (keyIs("signal")) {
              constexpr char phasertng_substructure_content_analysis_signal[] = ".phasertng.substructure.content_analysis.signal.";
              check_unique(phasertng_substructure_content_analysis_signal);
              value__boolean[phasertng_substructure_content_analysis_signal].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("determination")) {
            compulsoryKey(input_stream,{"bfactor","find","maximum_find","maximum_stored","minimum_number","occupancy","peaks_over_deepest_hole","percent","scatterer","target_fom","zscore"});
            if (keyIsEol()) return false;
            else if (keyIs("bfactor")) {
              constexpr char phasertng_substructure_determination_bfactor[] = ".phasertng.substructure.determination.bfactor.";
              check_unique(phasertng_substructure_determination_bfactor);
              value__flt_number[phasertng_substructure_determination_bfactor].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_determination_bfactor].minset(),value__flt_number[phasertng_substructure_determination_bfactor].minval(),value__flt_number[phasertng_substructure_determination_bfactor].maxset(),value__flt_number[phasertng_substructure_determination_bfactor].maxval()));
            }
            else if (keyIs("find")) {
              constexpr char phasertng_substructure_determination_find[] = ".phasertng.substructure.determination.find.";
              check_unique(phasertng_substructure_determination_find);
              value__int_number[phasertng_substructure_determination_find].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_determination_find].minset(),value__int_number[phasertng_substructure_determination_find].minval(),value__int_number[phasertng_substructure_determination_find].maxset(),value__int_number[phasertng_substructure_determination_find].maxval()));
            }
            else if (keyIs("maximum_find")) {
              constexpr char phasertng_substructure_determination_maximum_find[] = ".phasertng.substructure.determination.maximum_find.";
              check_unique(phasertng_substructure_determination_maximum_find);
              value__int_number[phasertng_substructure_determination_maximum_find].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_determination_maximum_find].minset(),value__int_number[phasertng_substructure_determination_maximum_find].minval(),value__int_number[phasertng_substructure_determination_maximum_find].maxset(),value__int_number[phasertng_substructure_determination_maximum_find].maxval()));
            }
            else if (keyIs("maximum_stored")) {
              constexpr char phasertng_substructure_determination_maximum_stored[] = ".phasertng.substructure.determination.maximum_stored.";
              check_unique(phasertng_substructure_determination_maximum_stored);
              value__int_number[phasertng_substructure_determination_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_determination_maximum_stored].minset(),value__int_number[phasertng_substructure_determination_maximum_stored].minval(),value__int_number[phasertng_substructure_determination_maximum_stored].maxset(),value__int_number[phasertng_substructure_determination_maximum_stored].maxval()));
            }
            else if (keyIs("minimum_number")) {
              constexpr char phasertng_substructure_determination_minimum_number[] = ".phasertng.substructure.determination.minimum_number.";
              check_unique(phasertng_substructure_determination_minimum_number);
              value__int_number[phasertng_substructure_determination_minimum_number].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_determination_minimum_number].minset(),value__int_number[phasertng_substructure_determination_minimum_number].minval(),value__int_number[phasertng_substructure_determination_minimum_number].maxset(),value__int_number[phasertng_substructure_determination_minimum_number].maxval()));
            }
            else if (keyIs("occupancy")) {
              constexpr char phasertng_substructure_determination_occupancy[] = ".phasertng.substructure.determination.occupancy.";
              check_unique(phasertng_substructure_determination_occupancy);
              value__flt_number[phasertng_substructure_determination_occupancy].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_determination_occupancy].minset(),value__flt_number[phasertng_substructure_determination_occupancy].minval(),value__flt_number[phasertng_substructure_determination_occupancy].maxset(),value__flt_number[phasertng_substructure_determination_occupancy].maxval()));
            }
            else if (keyIs("peaks_over_deepest_hole")) {
              constexpr char phasertng_substructure_determination_peaks_over_deepest_hole[] = ".phasertng.substructure.determination.peaks_over_deepest_hole.";
              check_unique(phasertng_substructure_determination_peaks_over_deepest_hole);
              value__boolean[phasertng_substructure_determination_peaks_over_deepest_hole].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("percent")) {
              constexpr char phasertng_substructure_determination_percent[] = ".phasertng.substructure.determination.percent.";
              check_unique(phasertng_substructure_determination_percent);
              value__percent[phasertng_substructure_determination_percent].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("scatterer")) {
              constexpr char phasertng_substructure_determination_scatterer[] = ".phasertng.substructure.determination.scatterer.";
              check_unique(phasertng_substructure_determination_scatterer);
              value__string[phasertng_substructure_determination_scatterer].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("target_fom")) {
              constexpr char phasertng_substructure_determination_target_fom[] = ".phasertng.substructure.determination.target_fom.";
              check_unique(phasertng_substructure_determination_target_fom);
              value__flt_number[phasertng_substructure_determination_target_fom].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_determination_target_fom].minset(),value__flt_number[phasertng_substructure_determination_target_fom].minval(),value__flt_number[phasertng_substructure_determination_target_fom].maxset(),value__flt_number[phasertng_substructure_determination_target_fom].maxval()));
            }
            else if (keyIs("zscore")) {
              constexpr char phasertng_substructure_determination_zscore[] = ".phasertng.substructure.determination.zscore.";
              check_unique(phasertng_substructure_determination_zscore);
              value__flt_number[phasertng_substructure_determination_zscore].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_determination_zscore].minset(),value__flt_number[phasertng_substructure_determination_zscore].minval(),value__flt_number[phasertng_substructure_determination_zscore].maxset(),value__flt_number[phasertng_substructure_determination_zscore].maxval()));
            }
          }
          else if (keyIs("hyss")) {
            compulsoryKey(input_stream,{"command_line","phil"});
            if (keyIsEol()) return false;
            else if (keyIs("command_line")) {
              constexpr char phasertng_substructure_hyss_command_line[] = ".phasertng.substructure.hyss.command_line.";
              check_unique(phasertng_substructure_hyss_command_line);
              value__strings[phasertng_substructure_hyss_command_line].set_value(variable,get_strings(input_stream));
            }
            else if (keyIs("phil")) {
              constexpr char phasertng_substructure_hyss_phil[] = ".phasertng.substructure.hyss.phil.";
              check_unique(phasertng_substructure_hyss_phil);
              value__path[phasertng_substructure_hyss_phil].set_value(variable,get_path(input_stream));
            }
          }
          else if (keyIs("patterson_analysis")) {
            compulsoryKey(input_stream,{"constellation","insufficient_data","insufficient_resolution","maximum_stored","minimum_number_of_reflections","minimum_percent","minimum_resolution","percent","shelxc","target"});
            if (keyIsEol()) return false;
            else if (keyIs("constellation")) {
              constexpr char phasertng_substructure_patterson_analysis_constellation[] = ".phasertng.substructure.patterson_analysis.constellation.";
              check_unique(phasertng_substructure_patterson_analysis_constellation);
              value__boolean[phasertng_substructure_patterson_analysis_constellation].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("insufficient_data")) {
              constexpr char phasertng_substructure_patterson_analysis_insufficient_data[] = ".phasertng.substructure.patterson_analysis.insufficient_data.";
              check_unique(phasertng_substructure_patterson_analysis_insufficient_data);
              value__boolean[phasertng_substructure_patterson_analysis_insufficient_data].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("insufficient_resolution")) {
              constexpr char phasertng_substructure_patterson_analysis_insufficient_resolution[] = ".phasertng.substructure.patterson_analysis.insufficient_resolution.";
              check_unique(phasertng_substructure_patterson_analysis_insufficient_resolution);
              value__boolean[phasertng_substructure_patterson_analysis_insufficient_resolution].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("maximum_stored")) {
              constexpr char phasertng_substructure_patterson_analysis_maximum_stored[] = ".phasertng.substructure.patterson_analysis.maximum_stored.";
              check_unique(phasertng_substructure_patterson_analysis_maximum_stored);
              value__int_number[phasertng_substructure_patterson_analysis_maximum_stored].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_patterson_analysis_maximum_stored].minset(),value__int_number[phasertng_substructure_patterson_analysis_maximum_stored].minval(),value__int_number[phasertng_substructure_patterson_analysis_maximum_stored].maxset(),value__int_number[phasertng_substructure_patterson_analysis_maximum_stored].maxval()));
            }
            else if (keyIs("minimum_number_of_reflections")) {
              constexpr char phasertng_substructure_patterson_analysis_minimum_number_of_reflections[] = ".phasertng.substructure.patterson_analysis.minimum_number_of_reflections.";
              check_unique(phasertng_substructure_patterson_analysis_minimum_number_of_reflections);
              value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections].minset(),value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections].minval(),value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections].maxset(),value__int_number[phasertng_substructure_patterson_analysis_minimum_number_of_reflections].maxval()));
            }
            else if (keyIs("minimum_percent")) {
              constexpr char phasertng_substructure_patterson_analysis_minimum_percent[] = ".phasertng.substructure.patterson_analysis.minimum_percent.";
              check_unique(phasertng_substructure_patterson_analysis_minimum_percent);
              value__percent[phasertng_substructure_patterson_analysis_minimum_percent].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("minimum_resolution")) {
              constexpr char phasertng_substructure_patterson_analysis_minimum_resolution[] = ".phasertng.substructure.patterson_analysis.minimum_resolution.";
              check_unique(phasertng_substructure_patterson_analysis_minimum_resolution);
              value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution].minset(),value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution].minval(),value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution].maxset(),value__flt_number[phasertng_substructure_patterson_analysis_minimum_resolution].maxval()));
            }
            else if (keyIs("percent")) {
              constexpr char phasertng_substructure_patterson_analysis_percent[] = ".phasertng.substructure.patterson_analysis.percent.";
              check_unique(phasertng_substructure_patterson_analysis_percent);
              value__percent[phasertng_substructure_patterson_analysis_percent].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("shelxc")) {
              constexpr char phasertng_substructure_patterson_analysis_shelxc[] = ".phasertng.substructure.patterson_analysis.shelxc.";
              check_unique(phasertng_substructure_patterson_analysis_shelxc);
              value__path[phasertng_substructure_patterson_analysis_shelxc].set_value(variable,get_path(input_stream));
            }
            else if (keyIs("target")) {
              compulsoryKey(input_stream,{"anomdiff","likelihood","expected"});
              constexpr char phasertng_substructure_patterson_analysis_target[] = ".phasertng.substructure.patterson_analysis.target.";
              check_unique(phasertng_substructure_patterson_analysis_target);
              value__choice[phasertng_substructure_patterson_analysis_target].set_value(variable,string_value);
            }
          }
          else if (keyIs("preparation")) {
            compulsoryKey(input_stream,{"scatterer","wilson_bfactor"});
            if (keyIsEol()) return false;
            else if (keyIs("scatterer")) {
              constexpr char phasertng_substructure_preparation_scatterer[] = ".phasertng.substructure.preparation.scatterer.";
              check_unique(phasertng_substructure_preparation_scatterer);
              value__string[phasertng_substructure_preparation_scatterer].set_value(variable,get_string(input_stream));
            }
            else if (keyIs("wilson_bfactor")) {
              constexpr char phasertng_substructure_preparation_wilson_bfactor[] = ".phasertng.substructure.preparation.wilson_bfactor.";
              check_unique(phasertng_substructure_preparation_wilson_bfactor);
              value__boolean[phasertng_substructure_preparation_wilson_bfactor].set_value(variable,get_boolean(input_stream));
            }
          }
          else if (keyIs("scatterer")) {
            constexpr char phasertng_substructure_scatterer[] = ".phasertng.substructure.scatterer.";
            check_unique(phasertng_substructure_scatterer);
            value__string[phasertng_substructure_scatterer].set_value(variable,get_string(input_stream));
          }
          else if (keyIs("test_fft_versus_summation")) {
            constexpr char phasertng_substructure_test_fft_versus_summation[] = ".phasertng.substructure.test_fft_versus_summation.";
            check_unique(phasertng_substructure_test_fft_versus_summation);
            value__int_paired[phasertng_substructure_test_fft_versus_summation].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_substructure_test_fft_versus_summation].minset(),value__int_paired[phasertng_substructure_test_fft_versus_summation].minval(),value__int_paired[phasertng_substructure_test_fft_versus_summation].maxset(),value__int_paired[phasertng_substructure_test_fft_versus_summation].maxval()));
          }
        }
        else if (keyIs("suite")) {
          compulsoryKey(input_stream,{"database","kill_file","kill_time","level","loggraphs","store","write_cards","write_data","write_density","write_files","write_log","write_phil","write_xml"});
          if (keyIsEol()) return false;
          else if (keyIs("database")) {
            constexpr char phasertng_suite_database[] = ".phasertng.suite.database.";
            check_unique(phasertng_suite_database);
            value__path[phasertng_suite_database].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("kill_file")) {
            constexpr char phasertng_suite_kill_file[] = ".phasertng.suite.kill_file.";
            check_unique(phasertng_suite_kill_file);
            value__path[phasertng_suite_kill_file].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("kill_time")) {
            constexpr char phasertng_suite_kill_time[] = ".phasertng.suite.kill_time.";
            check_unique(phasertng_suite_kill_time);
            value__flt_number[phasertng_suite_kill_time].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_suite_kill_time].minset(),value__flt_number[phasertng_suite_kill_time].minval(),value__flt_number[phasertng_suite_kill_time].maxset(),value__flt_number[phasertng_suite_kill_time].maxval()));
          }
          else if (keyIs("level")) {
            compulsoryKey(input_stream,{"default","silence","process","concise","summary","logfile","verbose","testing"});
            constexpr char phasertng_suite_level[] = ".phasertng.suite.level.";
            check_unique(phasertng_suite_level);
            value__choice[phasertng_suite_level].set_value(variable,string_value);
          }
          else if (keyIs("loggraphs")) {
            constexpr char phasertng_suite_loggraphs[] = ".phasertng.suite.loggraphs.";
            check_unique(phasertng_suite_loggraphs);
            value__boolean[phasertng_suite_loggraphs].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("store")) {
            compulsoryKey(input_stream,{"default","silence","process","concise","summary","logfile","verbose","testing"});
            constexpr char phasertng_suite_store[] = ".phasertng.suite.store.";
            check_unique(phasertng_suite_store);
            value__choice[phasertng_suite_store].set_value(variable,string_value);
          }
          else if (keyIs("write_cards")) {
            constexpr char phasertng_suite_write_cards[] = ".phasertng.suite.write_cards.";
            check_unique(phasertng_suite_write_cards);
            value__boolean[phasertng_suite_write_cards].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_data")) {
            constexpr char phasertng_suite_write_data[] = ".phasertng.suite.write_data.";
            check_unique(phasertng_suite_write_data);
            value__boolean[phasertng_suite_write_data].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_density")) {
            constexpr char phasertng_suite_write_density[] = ".phasertng.suite.write_density.";
            check_unique(phasertng_suite_write_density);
            value__boolean[phasertng_suite_write_density].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_files")) {
            constexpr char phasertng_suite_write_files[] = ".phasertng.suite.write_files.";
            check_unique(phasertng_suite_write_files);
            value__boolean[phasertng_suite_write_files].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_log")) {
            constexpr char phasertng_suite_write_log[] = ".phasertng.suite.write_log.";
            check_unique(phasertng_suite_write_log);
            value__boolean[phasertng_suite_write_log].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_phil")) {
            constexpr char phasertng_suite_write_phil[] = ".phasertng.suite.write_phil.";
            check_unique(phasertng_suite_write_phil);
            value__boolean[phasertng_suite_write_phil].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("write_xml")) {
            constexpr char phasertng_suite_write_xml[] = ".phasertng.suite.write_xml.";
            check_unique(phasertng_suite_write_xml);
            value__boolean[phasertng_suite_write_xml].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("threads")) {
          compulsoryKey(input_stream,{"number","strictly"});
          if (keyIsEol()) return false;
          else if (keyIs("number")) {
            constexpr char phasertng_threads_number[] = ".phasertng.threads.number.";
            check_unique(phasertng_threads_number);
            value__int_number[phasertng_threads_number].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_threads_number].minset(),value__int_number[phasertng_threads_number].minval(),value__int_number[phasertng_threads_number].maxset(),value__int_number[phasertng_threads_number].maxval()));
          }
          else if (keyIs("strictly")) {
            constexpr char phasertng_threads_strictly[] = ".phasertng.threads.strictly.";
            check_unique(phasertng_threads_strictly);
            value__boolean[phasertng_threads_strictly].set_value(variable,get_boolean(input_stream));
          }
        }
        else if (keyIs("time")) {
          compulsoryKey(input_stream,{"cumulative","mode"});
          if (keyIsEol()) return false;
          else if (keyIs("cumulative")) {
            compulsoryKey(input_stream,{"cpu","wall"});
            if (keyIsEol()) return false;
            else if (keyIs("cpu")) {
              constexpr char phasertng_time_cumulative_cpu[] = ".phasertng.time.cumulative.cpu.";
              check_unique(phasertng_time_cumulative_cpu);
              value__flt_number[phasertng_time_cumulative_cpu].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_time_cumulative_cpu].minset(),value__flt_number[phasertng_time_cumulative_cpu].minval(),value__flt_number[phasertng_time_cumulative_cpu].maxset(),value__flt_number[phasertng_time_cumulative_cpu].maxval()));
            }
            else if (keyIs("wall")) {
              constexpr char phasertng_time_cumulative_wall[] = ".phasertng.time.cumulative.wall.";
              check_unique(phasertng_time_cumulative_wall);
              value__flt_number[phasertng_time_cumulative_wall].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_time_cumulative_wall].minset(),value__flt_number[phasertng_time_cumulative_wall].minval(),value__flt_number[phasertng_time_cumulative_wall].maxset(),value__flt_number[phasertng_time_cumulative_wall].maxval()));
            }
          }
          else if (keyIs("mode")) {
            compulsoryKey(input_stream,{"cpu","wall"});
            if (keyIsEol()) return false;
            else if (keyIs("cpu")) {
              constexpr char phasertng_time_mode_cpu[] = ".phasertng.time.mode.cpu.";
              check_unique(phasertng_time_mode_cpu);
              value__flt_number[phasertng_time_mode_cpu].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_time_mode_cpu].minset(),value__flt_number[phasertng_time_mode_cpu].minval(),value__flt_number[phasertng_time_mode_cpu].maxset(),value__flt_number[phasertng_time_mode_cpu].maxval()));
            }
            else if (keyIs("wall")) {
              constexpr char phasertng_time_mode_wall[] = ".phasertng.time.mode.wall.";
              check_unique(phasertng_time_mode_wall);
              value__flt_number[phasertng_time_mode_wall].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_time_mode_wall].minset(),value__flt_number[phasertng_time_mode_wall].minval(),value__flt_number[phasertng_time_mode_wall].maxset(),value__flt_number[phasertng_time_mode_wall].maxval()));
            }
          }
        }
        else if (keyIs("title")) {
          constexpr char phasertng_title[] = ".phasertng.title.";
          check_unique(phasertng_title);
          value__string[phasertng_title].set_value(variable,get_string(input_stream));
        }
        else if (keyIs("tncs")) {
          compulsoryKey(input_stream,{"gfunction_radius","order","parity","present_in_model","refine","rmsd","unique","vector"});
          if (keyIsEol()) return false;
          else if (keyIs("gfunction_radius")) {
            constexpr char phasertng_tncs_gfunction_radius[] = ".phasertng.tncs.gfunction_radius.";
            check_unique(phasertng_tncs_gfunction_radius);
            value__flt_number[phasertng_tncs_gfunction_radius].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_gfunction_radius].minset(),value__flt_number[phasertng_tncs_gfunction_radius].minval(),value__flt_number[phasertng_tncs_gfunction_radius].maxset(),value__flt_number[phasertng_tncs_gfunction_radius].maxval()));
          }
          else if (keyIs("order")) {
            constexpr char phasertng_tncs_order[] = ".phasertng.tncs.order.";
            check_unique(phasertng_tncs_order);
            value__int_number[phasertng_tncs_order].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_tncs_order].minset(),value__int_number[phasertng_tncs_order].minval(),value__int_number[phasertng_tncs_order].maxset(),value__int_number[phasertng_tncs_order].maxval()));
          }
          else if (keyIs("parity")) {
            constexpr char phasertng_tncs_parity[] = ".phasertng.tncs.parity.";
            check_unique(phasertng_tncs_parity);
            value__int_vector[phasertng_tncs_parity].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_tncs_parity].minset(),value__int_vector[phasertng_tncs_parity].minval(),value__int_vector[phasertng_tncs_parity].maxset(),value__int_vector[phasertng_tncs_parity].maxval()));
          }
          else if (keyIs("present_in_model")) {
            constexpr char phasertng_tncs_present_in_model[] = ".phasertng.tncs.present_in_model.";
            check_unique(phasertng_tncs_present_in_model);
            value__boolean[phasertng_tncs_present_in_model].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("refine")) {
            compulsoryKey(input_stream,{"perturb_special_position","rotation_range","rotation_sampling"});
            if (keyIsEol()) return false;
            else if (keyIs("perturb_special_position")) {
              constexpr char phasertng_tncs_refine_perturb_special_position[] = ".phasertng.tncs.refine.perturb_special_position.";
              check_unique(phasertng_tncs_refine_perturb_special_position);
              value__boolean[phasertng_tncs_refine_perturb_special_position].set_value(variable,get_boolean(input_stream));
            }
            else if (keyIs("rotation_range")) {
              constexpr char phasertng_tncs_refine_rotation_range[] = ".phasertng.tncs.refine.rotation_range.";
              check_unique(phasertng_tncs_refine_rotation_range);
              value__flt_number[phasertng_tncs_refine_rotation_range].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_refine_rotation_range].minset(),value__flt_number[phasertng_tncs_refine_rotation_range].minval(),value__flt_number[phasertng_tncs_refine_rotation_range].maxset(),value__flt_number[phasertng_tncs_refine_rotation_range].maxval()));
            }
            else if (keyIs("rotation_sampling")) {
              constexpr char phasertng_tncs_refine_rotation_sampling[] = ".phasertng.tncs.refine.rotation_sampling.";
              check_unique(phasertng_tncs_refine_rotation_sampling);
              value__flt_number[phasertng_tncs_refine_rotation_sampling].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_refine_rotation_sampling].minset(),value__flt_number[phasertng_tncs_refine_rotation_sampling].minval(),value__flt_number[phasertng_tncs_refine_rotation_sampling].maxset(),value__flt_number[phasertng_tncs_refine_rotation_sampling].maxval()));
            }
          }
          else if (keyIs("rmsd")) {
            constexpr char phasertng_tncs_rmsd[] = ".phasertng.tncs.rmsd.";
            check_unique(phasertng_tncs_rmsd);
            value__flt_number[phasertng_tncs_rmsd].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_rmsd].minset(),value__flt_number[phasertng_tncs_rmsd].minval(),value__flt_number[phasertng_tncs_rmsd].maxset(),value__flt_number[phasertng_tncs_rmsd].maxval()));
          }
          else if (keyIs("unique")) {
            constexpr char phasertng_tncs_unique[] = ".phasertng.tncs.unique.";
            check_unique(phasertng_tncs_unique);
            value__boolean[phasertng_tncs_unique].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("vector")) {
            constexpr char phasertng_tncs_vector[] = ".phasertng.tncs.vector.";
            check_unique(phasertng_tncs_vector);
            value__flt_vector[phasertng_tncs_vector].set_value(variable,get_flt_vector(input_stream,value__flt_vector[phasertng_tncs_vector].minset(),value__flt_vector[phasertng_tncs_vector].minval(),value__flt_vector[phasertng_tncs_vector].maxset(),value__flt_vector[phasertng_tncs_vector].maxval()));
          }
        }
        else if (keyIs("tncs_analysis")) {
          compulsoryKey(input_stream,{"coiled_coil","indicated","insufficient_data","insufficient_resolution","number","result"});
          if (keyIsEol()) return false;
          else if (keyIs("coiled_coil")) {
            constexpr char phasertng_tncs_analysis_coiled_coil[] = ".phasertng.tncs_analysis.coiled_coil.";
            check_unique(phasertng_tncs_analysis_coiled_coil);
            value__boolean[phasertng_tncs_analysis_coiled_coil].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("indicated")) {
            constexpr char phasertng_tncs_analysis_indicated[] = ".phasertng.tncs_analysis.indicated.";
            check_unique(phasertng_tncs_analysis_indicated);
            value__boolean[phasertng_tncs_analysis_indicated].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("insufficient_data")) {
            constexpr char phasertng_tncs_analysis_insufficient_data[] = ".phasertng.tncs_analysis.insufficient_data.";
            check_unique(phasertng_tncs_analysis_insufficient_data);
            value__boolean[phasertng_tncs_analysis_insufficient_data].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("insufficient_resolution")) {
            constexpr char phasertng_tncs_analysis_insufficient_resolution[] = ".phasertng.tncs_analysis.insufficient_resolution.";
            check_unique(phasertng_tncs_analysis_insufficient_resolution);
            value__boolean[phasertng_tncs_analysis_insufficient_resolution].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("number")) {
            constexpr char phasertng_tncs_analysis_number[] = ".phasertng.tncs_analysis.number.";
            check_unique(phasertng_tncs_analysis_number);
            value__int_number[phasertng_tncs_analysis_number].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_tncs_analysis_number].minset(),value__int_number[phasertng_tncs_analysis_number].minval(),value__int_number[phasertng_tncs_analysis_number].maxset(),value__int_number[phasertng_tncs_analysis_number].maxval()));
          }
          else if (keyIs("result")) {
            compulsoryKey(input_stream,{"order"});
            variable.push_back("order");
            constexpr char phasertng_tncs_analysis_result_order[] = ".phasertng.tncs_analysis.result.order.";
            array__int_number[phasertng_tncs_analysis_result_order].push_back(type::int_number(phasertng_tncs_analysis_result_order,false,0,false,0));
            array__int_number[phasertng_tncs_analysis_result_order].back().set_value(variable,get_int_number(input_stream,array__int_number[phasertng_tncs_analysis_result_order].back().minset(),array__int_number[phasertng_tncs_analysis_result_order].back().minval(),array__int_number[phasertng_tncs_analysis_result_order].back().maxset(),array__int_number[phasertng_tncs_analysis_result_order].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"vector"});
            variable.push_back("vector");
            constexpr char phasertng_tncs_analysis_result_vector[] = ".phasertng.tncs_analysis.result.vector.";
            array__flt_vector[phasertng_tncs_analysis_result_vector].push_back(type::flt_vector(phasertng_tncs_analysis_result_vector,false,0,false,0));
            array__flt_vector[phasertng_tncs_analysis_result_vector].back().set_value(variable,get_flt_vector(input_stream,array__flt_vector[phasertng_tncs_analysis_result_vector].back().minset(),array__flt_vector[phasertng_tncs_analysis_result_vector].back().minval(),array__flt_vector[phasertng_tncs_analysis_result_vector].back().maxset(),array__flt_vector[phasertng_tncs_analysis_result_vector].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"parity"});
            variable.push_back("parity");
            constexpr char phasertng_tncs_analysis_result_parity[] = ".phasertng.tncs_analysis.result.parity.";
            array__int_vector[phasertng_tncs_analysis_result_parity].push_back(type::int_vector(phasertng_tncs_analysis_result_parity,false,0,false,0));
            array__int_vector[phasertng_tncs_analysis_result_parity].back().set_value(variable,get_int_vector(input_stream,array__int_vector[phasertng_tncs_analysis_result_parity].back().minset(),array__int_vector[phasertng_tncs_analysis_result_parity].back().minval(),array__int_vector[phasertng_tncs_analysis_result_parity].back().maxset(),array__int_vector[phasertng_tncs_analysis_result_parity].back().maxval()));
            variable.pop_back();
            compulsoryKey(input_stream,{"unique"});
            variable.push_back("unique");
            constexpr char phasertng_tncs_analysis_result_unique[] = ".phasertng.tncs_analysis.result.unique.";
            array__boolean[phasertng_tncs_analysis_result_unique].push_back(type::boolean(phasertng_tncs_analysis_result_unique));
            array__boolean[phasertng_tncs_analysis_result_unique].back().set_value(variable,get_boolean(input_stream));
            variable.pop_back();
          }
        }
        else if (keyIs("tncs_order")) {
          compulsoryKey(input_stream,{"frequency_cutoff","include_no_tncs","minimum_number_of_reflections","minimum_resolution","patterson","selection_by_rank"});
          if (keyIsEol()) return false;
          else if (keyIs("frequency_cutoff")) {
            constexpr char phasertng_tncs_order_frequency_cutoff[] = ".phasertng.tncs_order.frequency_cutoff.";
            check_unique(phasertng_tncs_order_frequency_cutoff);
            value__percent[phasertng_tncs_order_frequency_cutoff].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("include_no_tncs")) {
            constexpr char phasertng_tncs_order_include_no_tncs[] = ".phasertng.tncs_order.include_no_tncs.";
            check_unique(phasertng_tncs_order_include_no_tncs);
            value__boolean[phasertng_tncs_order_include_no_tncs].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("minimum_number_of_reflections")) {
            constexpr char phasertng_tncs_order_minimum_number_of_reflections[] = ".phasertng.tncs_order.minimum_number_of_reflections.";
            check_unique(phasertng_tncs_order_minimum_number_of_reflections);
            value__int_number[phasertng_tncs_order_minimum_number_of_reflections].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_tncs_order_minimum_number_of_reflections].minset(),value__int_number[phasertng_tncs_order_minimum_number_of_reflections].minval(),value__int_number[phasertng_tncs_order_minimum_number_of_reflections].maxset(),value__int_number[phasertng_tncs_order_minimum_number_of_reflections].maxval()));
          }
          else if (keyIs("minimum_resolution")) {
            constexpr char phasertng_tncs_order_minimum_resolution[] = ".phasertng.tncs_order.minimum_resolution.";
            check_unique(phasertng_tncs_order_minimum_resolution);
            value__flt_number[phasertng_tncs_order_minimum_resolution].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_order_minimum_resolution].minset(),value__flt_number[phasertng_tncs_order_minimum_resolution].minval(),value__flt_number[phasertng_tncs_order_minimum_resolution].maxset(),value__flt_number[phasertng_tncs_order_minimum_resolution].maxval()));
          }
          else if (keyIs("patterson")) {
            compulsoryKey(input_stream,{"origin_distance","percent","resolution"});
            if (keyIsEol()) return false;
            else if (keyIs("origin_distance")) {
              constexpr char phasertng_tncs_order_patterson_origin_distance[] = ".phasertng.tncs_order.patterson.origin_distance.";
              check_unique(phasertng_tncs_order_patterson_origin_distance);
              value__flt_number[phasertng_tncs_order_patterson_origin_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_tncs_order_patterson_origin_distance].minset(),value__flt_number[phasertng_tncs_order_patterson_origin_distance].minval(),value__flt_number[phasertng_tncs_order_patterson_origin_distance].maxset(),value__flt_number[phasertng_tncs_order_patterson_origin_distance].maxval()));
            }
            else if (keyIs("percent")) {
              constexpr char phasertng_tncs_order_patterson_percent[] = ".phasertng.tncs_order.patterson.percent.";
              check_unique(phasertng_tncs_order_patterson_percent);
              value__percent[phasertng_tncs_order_patterson_percent].set_value(variable,get_percent(input_stream));
            }
            else if (keyIs("resolution")) {
              constexpr char phasertng_tncs_order_patterson_resolution[] = ".phasertng.tncs_order.patterson.resolution.";
              check_unique(phasertng_tncs_order_patterson_resolution);
              value__flt_paired[phasertng_tncs_order_patterson_resolution].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_tncs_order_patterson_resolution].minset(),value__flt_paired[phasertng_tncs_order_patterson_resolution].minval(),value__flt_paired[phasertng_tncs_order_patterson_resolution].maxset(),value__flt_paired[phasertng_tncs_order_patterson_resolution].maxval()));
            }
          }
          else if (keyIs("selection_by_rank")) {
            constexpr char phasertng_tncs_order_selection_by_rank[] = ".phasertng.tncs_order.selection_by_rank.";
            check_unique(phasertng_tncs_order_selection_by_rank);
            value__int_number[phasertng_tncs_order_selection_by_rank].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_tncs_order_selection_by_rank].minset(),value__int_number[phasertng_tncs_order_selection_by_rank].minval(),value__int_number[phasertng_tncs_order_selection_by_rank].maxset(),value__int_number[phasertng_tncs_order_selection_by_rank].maxval()));
          }
        }
        else if (keyIs("trace")) {
          compulsoryKey(input_stream,{"filename","hexgrid","trim_asa","volume_sampling"});
          if (keyIsEol()) return false;
          else if (keyIs("filename")) {
            constexpr char phasertng_trace_filename[] = ".phasertng.trace.filename.";
            check_unique(phasertng_trace_filename);
            value__path[phasertng_trace_filename].set_value(variable,get_path(input_stream));
          }
          else if (keyIs("hexgrid")) {
            compulsoryKey(input_stream,{"distance","minimum_distance","ncyc","number_of_points"});
            if (keyIsEol()) return false;
            else if (keyIs("distance")) {
              constexpr char phasertng_trace_hexgrid_distance[] = ".phasertng.trace.hexgrid.distance.";
              check_unique(phasertng_trace_hexgrid_distance);
              value__flt_number[phasertng_trace_hexgrid_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_trace_hexgrid_distance].minset(),value__flt_number[phasertng_trace_hexgrid_distance].minval(),value__flt_number[phasertng_trace_hexgrid_distance].maxset(),value__flt_number[phasertng_trace_hexgrid_distance].maxval()));
            }
            else if (keyIs("minimum_distance")) {
              constexpr char phasertng_trace_hexgrid_minimum_distance[] = ".phasertng.trace.hexgrid.minimum_distance.";
              check_unique(phasertng_trace_hexgrid_minimum_distance);
              value__flt_number[phasertng_trace_hexgrid_minimum_distance].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_trace_hexgrid_minimum_distance].minset(),value__flt_number[phasertng_trace_hexgrid_minimum_distance].minval(),value__flt_number[phasertng_trace_hexgrid_minimum_distance].maxset(),value__flt_number[phasertng_trace_hexgrid_minimum_distance].maxval()));
            }
            else if (keyIs("ncyc")) {
              constexpr char phasertng_trace_hexgrid_ncyc[] = ".phasertng.trace.hexgrid.ncyc.";
              check_unique(phasertng_trace_hexgrid_ncyc);
              value__int_number[phasertng_trace_hexgrid_ncyc].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_trace_hexgrid_ncyc].minset(),value__int_number[phasertng_trace_hexgrid_ncyc].minval(),value__int_number[phasertng_trace_hexgrid_ncyc].maxset(),value__int_number[phasertng_trace_hexgrid_ncyc].maxval()));
            }
            else if (keyIs("number_of_points")) {
              constexpr char phasertng_trace_hexgrid_number_of_points[] = ".phasertng.trace.hexgrid.number_of_points.";
              check_unique(phasertng_trace_hexgrid_number_of_points);
              value__int_paired[phasertng_trace_hexgrid_number_of_points].set_value(variable,get_int_paired(input_stream,value__int_paired[phasertng_trace_hexgrid_number_of_points].minset(),value__int_paired[phasertng_trace_hexgrid_number_of_points].minval(),value__int_paired[phasertng_trace_hexgrid_number_of_points].maxset(),value__int_paired[phasertng_trace_hexgrid_number_of_points].maxval()));
            }
          }
          else if (keyIs("trim_asa")) {
            constexpr char phasertng_trace_trim_asa[] = ".phasertng.trace.trim_asa.";
            check_unique(phasertng_trace_trim_asa);
            value__boolean[phasertng_trace_trim_asa].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("volume_sampling")) {
            compulsoryKey(input_stream,{"all","calpha","hexgrid","optimal"});
            constexpr char phasertng_trace_volume_sampling[] = ".phasertng.trace.volume_sampling.";
            check_unique(phasertng_trace_volume_sampling);
            value__choice[phasertng_trace_volume_sampling].set_value(variable,string_value);
          }
        }
        else if (keyIs("twinning")) {
          compulsoryKey(input_stream,{"indicated","padilla_yeates_test","parity","test"});
          if (keyIsEol()) return false;
          else if (keyIs("indicated")) {
            constexpr char phasertng_twinning_indicated[] = ".phasertng.twinning.indicated.";
            check_unique(phasertng_twinning_indicated);
            value__boolean[phasertng_twinning_indicated].set_value(variable,get_boolean(input_stream));
          }
          else if (keyIs("padilla_yeates_test")) {
            constexpr char phasertng_twinning_padilla_yeates_test[] = ".phasertng.twinning.padilla_yeates_test.";
            check_unique(phasertng_twinning_padilla_yeates_test);
            value__flt_number[phasertng_twinning_padilla_yeates_test].set_value(variable,get_flt_number(input_stream,value__flt_number[phasertng_twinning_padilla_yeates_test].minset(),value__flt_number[phasertng_twinning_padilla_yeates_test].minval(),value__flt_number[phasertng_twinning_padilla_yeates_test].maxset(),value__flt_number[phasertng_twinning_padilla_yeates_test].maxval()));
          }
          else if (keyIs("parity")) {
            constexpr char phasertng_twinning_parity[] = ".phasertng.twinning.parity.";
            check_unique(phasertng_twinning_parity);
            value__int_vector[phasertng_twinning_parity].set_value(variable,get_int_vector(input_stream,value__int_vector[phasertng_twinning_parity].minset(),value__int_vector[phasertng_twinning_parity].minval(),value__int_vector[phasertng_twinning_parity].maxset(),value__int_vector[phasertng_twinning_parity].maxval()));
          }
          else if (keyIs("test")) {
            compulsoryKey(input_stream,{"moments","ltest","moments_or_ltest","moments_and_ltest"});
            constexpr char phasertng_twinning_test[] = ".phasertng.twinning.test.";
            check_unique(phasertng_twinning_test);
            value__choice[phasertng_twinning_test].set_value(variable,string_value);
          }
        }
        else if (keyIs("zscore_equivalent")) {
          compulsoryKey(input_stream,{"maximum_printed","nrand","percent","range_percent","range_zscore"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_zscore_equivalent_maximum_printed[] = ".phasertng.zscore_equivalent.maximum_printed.";
            check_unique(phasertng_zscore_equivalent_maximum_printed);
            value__int_number[phasertng_zscore_equivalent_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_zscore_equivalent_maximum_printed].minset(),value__int_number[phasertng_zscore_equivalent_maximum_printed].minval(),value__int_number[phasertng_zscore_equivalent_maximum_printed].maxset(),value__int_number[phasertng_zscore_equivalent_maximum_printed].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_zscore_equivalent_nrand[] = ".phasertng.zscore_equivalent.nrand.";
            check_unique(phasertng_zscore_equivalent_nrand);
            value__int_number[phasertng_zscore_equivalent_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_zscore_equivalent_nrand].minset(),value__int_number[phasertng_zscore_equivalent_nrand].minval(),value__int_number[phasertng_zscore_equivalent_nrand].maxset(),value__int_number[phasertng_zscore_equivalent_nrand].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_zscore_equivalent_percent[] = ".phasertng.zscore_equivalent.percent.";
            check_unique(phasertng_zscore_equivalent_percent);
            value__percent[phasertng_zscore_equivalent_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("range_percent")) {
            constexpr char phasertng_zscore_equivalent_range_percent[] = ".phasertng.zscore_equivalent.range_percent.";
            check_unique(phasertng_zscore_equivalent_range_percent);
            value__percent[phasertng_zscore_equivalent_range_percent].set_value(variable,get_percent(input_stream));
          }
          else if (keyIs("range_zscore")) {
            constexpr char phasertng_zscore_equivalent_range_zscore[] = ".phasertng.zscore_equivalent.range_zscore.";
            check_unique(phasertng_zscore_equivalent_range_zscore);
            value__flt_paired[phasertng_zscore_equivalent_range_zscore].set_value(variable,get_flt_paired(input_stream,value__flt_paired[phasertng_zscore_equivalent_range_zscore].minset(),value__flt_paired[phasertng_zscore_equivalent_range_zscore].minval(),value__flt_paired[phasertng_zscore_equivalent_range_zscore].maxset(),value__flt_paired[phasertng_zscore_equivalent_range_zscore].maxval()));
          }
        }
        else if (keyIs("zscore_equivalent_map")) {
          compulsoryKey(input_stream,{"maximum_printed","nrand","percent"});
          if (keyIsEol()) return false;
          else if (keyIs("maximum_printed")) {
            constexpr char phasertng_zscore_equivalent_map_maximum_printed[] = ".phasertng.zscore_equivalent_map.maximum_printed.";
            check_unique(phasertng_zscore_equivalent_map_maximum_printed);
            value__int_number[phasertng_zscore_equivalent_map_maximum_printed].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_zscore_equivalent_map_maximum_printed].minset(),value__int_number[phasertng_zscore_equivalent_map_maximum_printed].minval(),value__int_number[phasertng_zscore_equivalent_map_maximum_printed].maxset(),value__int_number[phasertng_zscore_equivalent_map_maximum_printed].maxval()));
          }
          else if (keyIs("nrand")) {
            constexpr char phasertng_zscore_equivalent_map_nrand[] = ".phasertng.zscore_equivalent_map.nrand.";
            check_unique(phasertng_zscore_equivalent_map_nrand);
            value__int_number[phasertng_zscore_equivalent_map_nrand].set_value(variable,get_int_number(input_stream,value__int_number[phasertng_zscore_equivalent_map_nrand].minset(),value__int_number[phasertng_zscore_equivalent_map_nrand].minval(),value__int_number[phasertng_zscore_equivalent_map_nrand].maxset(),value__int_number[phasertng_zscore_equivalent_map_nrand].maxval()));
          }
          else if (keyIs("percent")) {
            constexpr char phasertng_zscore_equivalent_map_percent[] = ".phasertng.zscore_equivalent_map.percent.";
            check_unique(phasertng_zscore_equivalent_map_percent);
            value__percent[phasertng_zscore_equivalent_map_percent].set_value(variable,get_percent(input_stream));
          }
        }
        skip_line(input_stream);
      }
      else {
        if (!ignore_unknown_keys and string_value.size())
          storeError(err::SYNTAX,"Unknown keyword: " + string_value);
        skip_line(input_stream);
      }
    }
    return false;
  }

  void
  InputCard::analyse()
  {
    clear_incompatible_linkage_arrays();
  }

  std::string InputCard::header() const
  {
    if (false) return "";
    else if (running_mode == "aniso") return "ANISOTROPY CORRECTION";
    else if (running_mode == "beam") return "ISOSTRUCTURE SEARCH";
    else if (running_mode == "brf") return "BRUTE ROTATION FUNCTION";
    else if (running_mode == "btf") return "BRUTE TRANSLATION FUNCTION";
    else if (running_mode == "bub") return "BIOLOGICAL UNIT BUILDER";
    else if (running_mode == "cca") return "CELL CONTENT ANALYSIS";
    else if (running_mode == "ccs") return "CELL CONTENT SCALING";
    else if (running_mode == "chk") return "INITIALIZE";
    else if (running_mode == "data") return "DATA PREPARATION";
    else if (running_mode == "deep") return "DEEP SEARCH";
    else if (running_mode == "eatm") return "EXPECTED LLG FOR ATOM";
    else if (running_mode == "ellg") return "EXPECTED LLG FOR SEARCH";
    else if (running_mode == "emap") return "EXPECTED LLG FOR MAP";
    else if (running_mode == "emm") return "ENSEMBLE MULTI MODEL";
    else if (running_mode == "esm") return "ENSEMBLE SINGLE MODEL";
    else if (running_mode == "etfz") return "EXPECTED TFZ FOR SEARCH";
    else if (running_mode == "exp1") return "EXPAND TO P1";
    else if (running_mode == "feff") return "EFFECTIVE AMPLITUDES";
    else if (running_mode == "fetch") return "FETCH PDB";
    else if (running_mode == "find") return "FIND COMPONENTS";
    else if (running_mode == "frf") return "FAST ROTATION FUNCTION";
    else if (running_mode == "frfr") return "FAST ROTATION FUNCTION RESCORE";
    else if (running_mode == "ftf") return "FAST TRANSLATION FUNCTION";
    else if (running_mode == "ftfr") return "FAST TRANSLATION FUNCTION RESCORE";
    else if (running_mode == "fuse") return "FUSE SOLUTIONS";
    else if (running_mode == "gyre") return "GYRE REFINEMENT";
    else if (running_mode == "hyss") return "HYSS SUBSTRUCTURE";
    else if (running_mode == "imap") return "ELECTRON MICROSCOPY DATA FILE READER";
    else if (running_mode == "imtz") return "MTZ FILE READER";
    else if (running_mode == "info") return "INFORMATION CONTENT";
    else if (running_mode == "ipdb") return "PDB FILE READER";
    else if (running_mode == "jog") return "JOG BY MAP DOCKING";
    else if (running_mode == "join") return "JOIN NODES";
    else if (running_mode == "mapz") return "MAP DOCKING ZSCORE";
    else if (running_mode == "mcc") return "MAP CORRELATION COEFFICIENT";
    else if (running_mode == "move") return "MOVING";
    else if (running_mode == "msm") return "MAP SINGLE MODEL";
    else if (running_mode == "pak") return "PACKING";
    else if (running_mode == "perm") return "PERMUTATION FOR SEARCH";
    else if (running_mode == "pool") return "POOL NODES";
    else if (running_mode == "pose") return "POSE SCORING";
    else if (running_mode == "ptf") return "PHASED TRANSLATION FUNCTION";
    else if (running_mode == "put") return "PUT WITHOUT SEARCH";
    else if (running_mode == "rbgs") return "RIGID BODY GRID SEARCH";
    else if (running_mode == "rbm") return "RIGID BODY MAPS FOR INTENSITIES";
    else if (running_mode == "rbr") return "RIGID BODY REFINEMENT";
    else if (running_mode == "rellg") return "RELATIVE EXPECTED LLG FOR MODEL";
    else if (running_mode == "rfac") return "R-FACTOR CALCULATION";
    else if (running_mode == "rmr") return "RIGID MAP REFINEMENT";
    else if (running_mode == "sga") return "SPACE GROUP ANALYSIS";
    else if (running_mode == "sgx") return "SPACE GROUP EXPANSION";
    else if (running_mode == "span") return "SPAN SPACE";
    else if (running_mode == "spr") return "SEGMENTED PRUNING REFINEMENT";
    else if (running_mode == "srf") return "SELF ROTATION FUNCTION";
    else if (running_mode == "srfp") return "SELF ROTATION FUNCTION PLOT";
    else if (running_mode == "ssa") return "SUBSTRUCTURE CONTENT ANALYSIS";
    else if (running_mode == "sscc") return "GET CC MTZ PDB";
    else if (running_mode == "ssd") return "SUBSTRUCTURE DETERMINATION";
    else if (running_mode == "ssm") return "SUBSTRUCTURE MODEL PREPARATION";
    else if (running_mode == "ssp") return "SUBSTRUCTURE PATTERSON ANALYSIS";
    else if (running_mode == "ssr") return "SUBSTRUCTURE REFINEMENT";
    else if (running_mode == "test") return "TEST";
    else if (running_mode == "tfz") return "TRANSLATION FUNCTION ZSCORE EQUIVALENT";
    else if (running_mode == "tncs") return "TNCS CORRECTION REFINEMENT";
    else if (running_mode == "tncso") return "TNCS COMMENSURATE MODULATION ANALYSIS";
    else if (running_mode == "tree") return "TREE ANALYSIS OF DIRECTORIES";
    else if (running_mode == "twin") return "TWIN TEST";
    else if (running_mode == "walk") return "WALK DIRECTORY";
    else if (running_mode == "xref") return "XRAY COORDINATE REFINEMENT";
    return "THE TWILIGHT ZONE";
  }

  std::set<std::string> InputCard::python_modes() const
  {
    std::set<std::string> pymodes;
    pymodes.insert("bub");
    pymodes.insert("walk");
    pymodes.insert("tree");
    pymodes.insert("srfp");
    pymodes.insert("deep");
    pymodes.insert("join");
    pymodes.insert("pool");
    pymodes.insert("hyss");
    pymodes.insert("sscc");
    pymodes.insert("find");
    pymodes.insert("rfac");
    pymodes.insert("beam");
    pymodes.insert("fetch");
    pymodes.insert("xref");
    return pymodes;
  }

}
