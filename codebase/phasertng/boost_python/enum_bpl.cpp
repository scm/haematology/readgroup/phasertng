//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <phasertng/enum/labin_col.h>
#include <phasertng/enum/friedel_type.h>
#include <phasertng/enum/out_stream.h>
#include <phasertng/enum/err_code.h>
#include <phasertng/enum/entry_data.h>
#include <phasertng/enum/other_data.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_phasertng_enum()
{
  using namespace boost::python;

  enum_<labin::col>("enum_labin_col")
       .value("undefined",labin::UNDEFINED)
       .value("h",labin::H)
       .value("k",labin::K)
       .value("l",labin::L)
       .value("inat",labin::INAT)      //data
       .value("siginat",labin::SIGINAT)//data
       .value("ipos",labin::IPOS)      //data
       .value("sigipos",labin::SIGIPOS)//data
       .value("ineg",labin::INEG)      //data
       .value("sigineg",labin::SIGINEG)//data
       .value("fnat",labin::FNAT)      //data
       .value("sigfnat",labin::SIGFNAT)//data
       .value("fpos",labin::FPOS)      //data
       .value("sigfpos",labin::SIGFPOS)//data
       .value("fneg",labin::FNEG)      //data
       .value("sigfneg",labin::SIGFNEG)//data
       .value("mapf",labin::MAPF)      //data
       .value("mapph",labin::MAPPH)    //data
       .value("mapdobs",labin::MAPDOBS)//data
       .value("bin",labin::BIN)    //constant for data
       .value("eps",labin::EPS)    //constant sg/uc
       .value("phsr",labin::PHSR)   //constant sg/uc
       .value("ssqr",labin::SSQR)   //constant sg/uc
       .value("cent",labin::CENT)   //constant sg/uc
       .value("both",labin::BOTH)   //constant sg/uc
       .value("plus",labin::PLUS)   //constant sg/uc
       .value("feffnat",labin::FEFFNAT)//data likelihood
       .value("dobsnat",labin::DOBSNAT)//data likelihood
       .value("feffpos",labin::FEFFPOS)//data likelihood
       .value("dobspos",labin::DOBSPOS)//data likelihood
       .value("feffneg",labin::FEFFNEG)//data likelihood
       .value("dobsneg",labin::DOBSNEG)//data likelihood
       .value("resn",labin::RESN)   //likelihood (includes ano)
       .value("aniso",labin::ANISO) //likelihood (ano only)
       .value("anisobeta",labin::ANISOBETA) //likelihood (ano only)
       .value("teps",labin::TEPS)   //likelihood
       .value("tbin",labin::TBIN)   //likelihood
       .value("wll",labin::WLL)     //likelihood
       .value("siga",labin::SIGA)   //likelihood
       .value("fpart",labin::FPART)   //model
       .value("phpart",labin::PHPART) //model
       .value("fcnat",labin::FCNAT)    //model
       .value("phicnat",labin::PHICNAT)//model
       .value("fcpos",labin::FCPOS)    //model
       .value("phicpos",labin::PHICPOS)//model
       .value("fcneg",labin::FCNEG)    //model
       .value("phicneg",labin::PHICNEG)//model
       .value("fom",labin::FOM)        //hklout
       .value("fwt",labin::FWT)        //hklout
       .value("phwt",labin::PHWT)      //hklout
       .value("delfwt",labin::DELFWT)  //hklout
       .value("delphwt",labin::DELPHWT)//hklout
       .value("llgf",labin::LLGF)      //hklout
       .value("llgph",labin::LLGPH)    //hklout
       .value("hla",labin::HLA)        //hklout
       .value("hlb",labin::HLB)        //hklout
       .value("hlc",labin::HLC)        //hklout
       .value("hld",labin::HLD)        //hklout
       .value("einfo",labin::EINFO)    //hklout
       .value("info",labin::INFO)      //hklout
       .value("sinfo",labin::SINFO)    //hklout
       .value("sellg",labin::SELLG)    //hklout
       .value("sefom",labin::SEFOM)    //hklout
       .value("free",labin::FREE)      //carried
       .export_values()
       ;

  enum_<friedel::type>("enum_friedel_type")
       .value("undefined",friedel::UNDEFINED)
       .value("nat",friedel::NAT)
       .value("pos",friedel::POS)
       .value("neg",friedel::NEG)
       .export_values()
       ;

  enum_<out::stream>("enum_out_stream")
       .value("undefined",out::UNDEFINED)
       .value("silence",out::SILENCE)
       .value("process",out::PROCESS)
       .value("concise",out::CONCISE)
       .value("summary",out::SUMMARY)
       .value("logfile",out::LOGFILE)
       .value("verbose",out::VERBOSE)
       .value("testing",out::TESTING)
       .export_values()
       ;

   enum_<err::code>("enum_err_code")
       .value("success",err::SUCCESS)    //0 { EXIT_CODE_OFFSET = 63 }
       .value("parse",err::PARSE)        //64
       .value("syntax",err::SYNTAX)      //65
       .value("input",err::INPUT)        //66
       .value("fileopen",err::FILEOPEN)  //67
       .value("fileset",err::FILESET)    //68
       .value("memory",err::MEMORY)      //69
       .value("killflag",err::KILLFLAG)  //70
       .value("killfile",err::KILLFILE)  //71
       .value("killtime",err::KILLTIME)  //72
       .value("fatal",err::FATAL)        //73
       .value("result",err::RESULT)      //74
       .value("unhandled",err::UNHANDLED)//75
       .value("unknown",err::UNKNOWN)    //76
       .value("stdin",err::STDIN)        //77
       .value("developer",err::DEVELOPER)//78
       .value("python",err::PYTHON)      //79
       .value("assert",err::ASSERT)      //80
       .export_values()
       ;

  enum_<entry::data>("enum_entry_data")
       .value("undefined",entry::UNDEFINED)
       .value("models_pdb",entry::MODELS_PDB)
       .value("trace_pdb",entry::TRACE_PDB)
       .value("monostructure_pdb",entry::MONOSTRUCTURE_PDB)
       .value("variance_mtz",entry::VARIANCE_MTZ)
       .value("decomposition_mtz",entry::DECOMPOSITION_MTZ)
       .value("coordinates_pdb",entry::COORDINATES_PDB)
       .value("density_mtz",entry::DENSITY_MTZ)
       .value("interpolation_mtz",entry::INTERPOLATION_MTZ)
       .value("substructure.pdb",entry::SUBSTRUCTURE_PDB)
       .value("gradent_mtz",entry::GRADIENT_MTZ)
       .value("fcalcs_mtz",entry::FCALCS_MTZ)
       .value("imodels_pdb",entry::IMODELS_PDB)
       .value("ifcalcs_mtz",entry::IFCALCS_MTZ)
       .export_values()
       ;

  enum_<other::data>("enum_other_data")
       .value("undefined",other::UNDEFINED)
       .value("data_mtz",other::DATA_MTZ)
       .value("tiny_mtz",other::TINY_MTZ)
       .value("refine_mtz",other::REFINE_MTZ)
       .value("concise_log",other::CONCISE_LOG)
       .value("summary_log",other::SUMMARY_LOG)
       .value("logfile_log",other::LOGFILE_LOG)
       .value("verbose_log",other::VERBOSE_LOG)
       .value("pathway_cards",other::RESULT_CARDS)
       .value("result_cards",other::PATHWAY_CARDS)
       .value("io_eff",other::IO_EFF)
       .value("dag_cards",other::DAG_CARDS)
       .value("dag_html",other::DAG_HTML)
       .value("dag_phil",other::DAG_PHIL)
       .value("entry_cards",other::ENTRY_CARDS)
       .value("solutions_log",other::SOLUTIONS_LOG)
       .export_values()
       ;

}
} // namespace phasertng::<anonymous>

void init_phasertng_enum()
{
  wrap_phasertng_enum();
}

} // namespace boost_python
} // namespace phasertng
