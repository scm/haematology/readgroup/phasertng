//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_SOLPAR_CLASS__
#define __PHASER_DATA_SOLPAR_CLASS__

namespace phaser {

struct data_solpar
{
  bool is_set()
  { return SIGA_FSOL; }

  double  SIGA_FSOL = 1.05;
  double  SIGA_BSOL = 501.605;
  double  SIGA_MIN = 0.1;
  double  BULK_FSOL = 0.35;
  double  BULK_BSOL = 45;
  double  BULK_USE = false;
};

} //phaser

#endif
