//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Information.h>
#include <phasertng/math/infobits.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/math/table/expect_infobits.h>
#include <phasertng/math/likelihood/isad/SADeLLG.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <future>

namespace phasertng {

  void
  Information::partial_info_calculation(int beg, int end)
  {
    math::infobits infobits;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double iobs = REFLECTIONS->get_flt(labin::INAT,r);
        const double sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
        const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);

        double epsnSigmaN = fn::pow2(resn)*teps;
        PHASER_ASSERT(epsnSigmaN > 0);
        double eosqr   = iobs/epsnSigmaN;
        double sigesqr = sigi/epsnSigmaN;
        double thisInfo = cent ?
          infobits.EosqInfoBitsCen(eosqr,sigesqr) :
          infobits.EosqInfoBitsAcen(eosqr,sigesqr);
        INFO[r] = thisInfo;
        INFO_PRESENT[r] = true;
      }
    }
  }

  void
  Information::calculate_info()
  {
    INFO.resize(REFLECTIONS->NREFL);
    INFO_PRESENT.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      INFO[r] = 0.;
      INFO_PRESENT[r] = false;
    }
    if (REFLECTIONS->FRENCH_WILSON) return;
    if (!REFLECTIONS->NREFL) return;

    double maxSIGI(0.0);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const double sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
        maxSIGI = std::max(maxSIGI,sigi);
      }
    }
    if (maxSIGI == 0) return;
    phaser_assert(REFLECTIONS->has_col(labin::INAT));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    bool data_ok(false);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    { if (SELECTED[r]) { data_ok = true; } }
    PHASER_ASSERT(data_ok);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = REFLECTIONS->NREFL;
      partial_info_calculation(
            beg,
            end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&Information::partial_info_calculation, this,
              std::placeholders::_1,
              std::placeholders::_2
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              beg,
              end );
        }
        else
        {
          results.push_back(std::async(std::launch::async, &Information::partial_info_calculation, this,
              beg,
              end ));
        }
      }
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      {{
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      partial_info_calculation(
            beg,
            end);
      }}
      // collate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        if (USE_STRICTLY_NTHREADS) threads[t].join();
      }
    }
  }

  void
  Information::calculate_mapinfo()
  {
    INFO.resize(REFLECTIONS->NREFL);
    INFO_PRESENT.resize(REFLECTIONS->NREFL);
    phaser_assert(REFLECTIONS->has_col(labin::FEFFNAT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    if (!REFLECTIONS->MAP) return;
    if (!REFLECTIONS->NREFL) return;

    bool data_ok(false);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    { if (SELECTED[r]) { data_ok = true; } }
    phaser_assert(data_ok);

    likelihood::mapELLG mapellg;
    double log2 = std::log(2.);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const double feff = REFLECTIONS->get_flt(labin::FEFFNAT,r);
      const double resn = REFLECTIONS->get_flt(labin::RESN,r);
      phaser_assert(resn);
      const double dobs = REFLECTIONS->get_flt(labin::DOBSNAT,r);
      INFO[r] = mapellg.get(feff/resn,dobs,false)/log2;
      INFO_PRESENT[r] = true;
    }
  }

  void
  Information::partial_einfo_calculation(
      int beg,
      int end)
  {
    table::expectedInfoBitsCen expected_info_bits_cen;
    table::expectedInfoBitsAcen expected_info_bits_acen;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double sigi = REFLECTIONS->get_flt(labin::SIGINAT,r);
        const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);

        double epsnSigmaN = fn::pow2(resn)*teps;
        double sigesq = sigi/epsnSigmaN;
        double thisInfo = cent ?
          expected_info_bits_cen.get(sigesq) :
          expected_info_bits_acen.get(sigesq);
        EINFO[r] = thisInfo;
        EINFO_PRESENT[r] = true;
      }
    }
  }

  void
  Information::calculate_einfo()
  {
    EINFO.resize(REFLECTIONS->NREFL);
    EINFO_PRESENT.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      EINFO[r] = 0.;
      EINFO_PRESENT[r] = false;
    }
    bool indeterminate(boost::logic::indeterminate(REFLECTIONS->INTENSITIES));
    phaser_assert(!indeterminate and bool(REFLECTIONS->INTENSITIES));
    phaser_assert(REFLECTIONS->NREFL);
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    bool data_ok(false);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    { if (SELECTED[r]) { data_ok = true; } }
    PHASER_ASSERT(data_ok);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = REFLECTIONS->NREFL;
       partial_einfo_calculation(
            beg,
            end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&Information::partial_einfo_calculation, this,
              std::placeholders::_1,
              std::placeholders::_2
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              beg,
              end );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &Information::partial_einfo_calculation, this,
              beg,
              end ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      partial_einfo_calculation(
              beg,
              end);
      }}
      // collate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        if (USE_STRICTLY_NTHREADS) threads[t].join();
      }
    }
  }

  void
  Information::partial_sinfo_calculation(
      likelihood::isad::sigmaa isad_sigmaa,
      int beg,
      int end)
  {
    math::infobits infobits;
    for (int r = beg; r < end; r++)
    {
      const bool   cent = REFLECTIONS->get_bln(labin::CENT,r);
      const bool   both = REFLECTIONS->get_bln(labin::BOTH,r);
      if (!cent and both and SELECTED[r])
      {
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double ipos   = REFLECTIONS->get_flt(labin::IPOS,r);
        const double sigipos = REFLECTIONS->get_flt(labin::SIGIPOS,r);
        const double ineg   = REFLECTIONS->get_flt(labin::INEG,r);
        const double sigineg = REFLECTIONS->get_flt(labin::SIGINEG,r);
        const int s = REFLECTIONS->get_int(labin::BIN,r);

        const double epsnSigmaN = fn::pow2(resn)*teps;
        const double absrhoFF = isad_sigmaa.absrhoFF_bin[s];
        const double rhopm = isad_sigmaa.rhopm_bin[s];
        PHASER_ASSERT(epsnSigmaN > 0);
        double zopos   = ipos /  epsnSigmaN;
        double sigzpos = sigipos/epsnSigmaN;
        double zoneg   = ineg /  epsnSigmaN;
        double sigzneg = sigineg/epsnSigmaN;
        double thisInfo = infobits.SADextraBits(zopos,sigzpos,zoneg,sigzneg,rhopm,absrhoFF);
        SINFO[r] = thisInfo;
        SINFO_PRESENT[r] = true;
      }
    }
  }

  void
  Information::calculate_sinfo(likelihood::isad::sigmaa isad_sigmaa)
  {
    SINFO.resize(REFLECTIONS->NREFL);
    SINFO_PRESENT.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      SINFO[r] = 0.;
      SINFO_PRESENT[r] = false;
    }

    bool indeterminate(boost::logic::indeterminate(REFLECTIONS->ANOMALOUS));
    phaser_assert(!indeterminate and bool(REFLECTIONS->ANOMALOUS));
    indeterminate = bool(boost::logic::indeterminate(REFLECTIONS->INTENSITIES));
    phaser_assert(!indeterminate and bool(REFLECTIONS->INTENSITIES));
    phaser_assert(REFLECTIONS->NREFL);
    double maxSIGI(0.0);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        const double sigipos = REFLECTIONS->get_flt(labin::SIGIPOS,r);
        const double sigineg = REFLECTIONS->get_flt(labin::SIGINEG,r);
        maxSIGI = std::max(maxSIGI,sigipos);
        maxSIGI = std::max(maxSIGI,sigineg);
      }
    }
    PHASER_ASSERT(maxSIGI);
    phaser_assert(REFLECTIONS->has_col(labin::IPOS));
    phaser_assert(REFLECTIONS->has_col(labin::SIGIPOS));
    phaser_assert(REFLECTIONS->has_col(labin::INEG));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINEG));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    bool data_ok(false);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    { if (SELECTED[r]) { data_ok = true; } }
    PHASER_ASSERT(data_ok);

    //set off threads;
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = REFLECTIONS->NREFL;
      partial_sinfo_calculation(
              isad_sigmaa,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(
              likelihood::isad::sigmaa,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&Information::partial_sinfo_calculation, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              isad_sigmaa,
              beg,
              end );
        }
        else
        {
          results.push_back(std::async(std::launch::async, &Information::partial_sinfo_calculation, this,
              isad_sigmaa,
              beg,
              end ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      partial_sinfo_calculation(
              isad_sigmaa,
              beg,
              end );
      }}
      // wait for the other threads to finish (they will have populated SINFO and SINFO_PRESENT)
      for (int t = 0; t < nthreads_1; t++)
      {
        if (USE_STRICTLY_NTHREADS) threads[t].join();
      }
    }
  }

  void
  Information::partial_sellg_calculation(
      likelihood::isad::sigmaa isad_complete,
      int beg,
      int end)
  {
    likelihood::SADeLLG SADeLLG;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const double feffpos = REFLECTIONS->get_flt(labin::FEFFPOS,r);
        const double feffneg = REFLECTIONS->get_flt(labin::FEFFNEG,r);
        const double dobspos = REFLECTIONS->get_flt(labin::DOBSPOS,r);
        const double dobsneg = REFLECTIONS->get_flt(labin::DOBSNEG,r);
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const int s = REFLECTIONS->get_int(labin::BIN,r);
        const bool cent = REFLECTIONS->get_bln(labin::CENT,r);
        const bool plus = REFLECTIONS->get_bln(labin::PLUS,r);
        const bool both = REFLECTIONS->get_bln(labin::BOTH,r);
        PHASER_ASSERT(resn > 0);
        std::array<double,3> stats = SADeLLG.get(s,resn,teps,dobspos,feffpos,
          dobsneg,feffneg,isad_complete,both,plus,cent);
        double thiseLLG = stats[0];
        double thiseFOM = stats[1];
        // double thisepdiff = stats[2]; // Probably more useful with <ZH> for SHELXD?
        SELLG[r] = thiseLLG;
        SELLG_PRESENT[r] = true;
        SEFOM[r] = thiseFOM;
        SEFOM_PRESENT[r] = true;
      }
    }
  }

  void
  Information::calculate_sellg(
      likelihood::isad::sigmaa ISAD_SIGMAA,
      double fracH,
      double HRMS)
  {
    SELLG.resize(REFLECTIONS->NREFL);
    SELLG_PRESENT.resize(REFLECTIONS->NREFL);
    SEFOM.resize(REFLECTIONS->NREFL);
    SEFOM_PRESENT.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      SELLG[r] = 0.;
      SELLG_PRESENT[r] = false;
      SEFOM[r] = 0.;
      SEFOM_PRESENT[r] = false;
    }

    bool indeterminate(boost::logic::indeterminate(REFLECTIONS->ANOMALOUS));
    phaser_assert(!indeterminate and bool(REFLECTIONS->ANOMALOUS));
    phaser_assert(REFLECTIONS->NREFL);
    phaser_assert(REFLECTIONS->has_col(labin::FEFFPOS));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSPOS));
    phaser_assert(REFLECTIONS->has_col(labin::FEFFNEG));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNEG));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::CENT));
    phaser_assert(REFLECTIONS->has_col(labin::BOTH));

    likelihood::isad::sigmaa isad_complete = ISAD_SIGMAA;
    int nbins = isad_complete.SigmaN_bin.size();
    double HRMSsqr = fn::pow2(HRMS);
    for (int s = 0; s < nbins; s++)
    {
      // Assume FT of limiting coordinate error (DH) would be absorbed into FH refinement
      double DHsqr = std::exp(-2*DEF_TWOPISQ_ON_THREE*HRMSsqr*isad_complete.MIDSSQR[s]);
      isad_complete.SigmaH_bin[s] = fracH*DHsqr*isad_complete.SigmaQ_bin[s];
      isad_complete.sigmaHH_bin[s] = fracH*DHsqr*isad_complete.sigmaQQ_bin[s];
    }

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = REFLECTIONS->NREFL;
      partial_sellg_calculation(
              isad_complete,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(
              likelihood::isad::sigmaa,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&Information::partial_sellg_calculation, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              isad_complete,
              beg,
              end );
        }
        else
        {
          results.push_back(std::async(std::launch::async, &Information::partial_sellg_calculation, this,
              isad_complete,
              beg,
              end ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      partial_sellg_calculation(
              isad_complete,
              beg,
              end );
      }}
      // wait for the other threads to finish (they will have populated SINFO and SINFO_PRESENT)
      for (int t = 0; t < nthreads_1; t++)
      {
        if (USE_STRICTLY_NTHREADS) threads[t].join();
      }
    }
  }

} //phasertng
