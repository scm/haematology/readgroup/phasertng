//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_TNCS_CLASS__
#define __PHASER_DATA_TNCS_CLASS__
#include <phaser/main/Phaser.h>

namespace phaser {

struct data_tncs_rot
{
  dvect3 ANGLE = dvect3(0,0,0); //"Orthogonal" angles, small
};

struct data_tncs_tra
{
  bool VECTOR_SET = false;
  dvect3 VECTOR = dvect3(0,0,0);
};

class data_tncs
{
  public:
    bool          USE = false;
    data_tncs_rot ROT = data_tncs_rot();
    data_tncs_tra TRA = data_tncs_tra();
    double        GFUNCTION_RADIUS = 0;
    int           NMOL = 1;
    float1D       EPSFAC;

    bool use_and_present() { return (USE && TRA.VECTOR_SET); }
};

} //phaser

#endif
