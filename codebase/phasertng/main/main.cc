//test
//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Preprocessor.h>
#include <phasertng/main/Phasertng.h>
#include <phasertng/main/hoist.h>

#ifdef _WIN32

//#ifdef _MSC_VER
//#include <crtdbg.h> // for the sake of memory leak checks
//#endif

// BYTE, INT16 and INT32 are defined in libccp4/ccp4/ccp4_sysdep.h
// and they conflict with Windows typedefs.
#ifdef BYTE
#undef BYTE
#endif
#ifdef INT16
#undef INT16
#endif
#ifdef INT32
#undef INT32
#endif

#include <windows.h>
// define function pointers for Windows exceptionhandler library, XCPTLib.dll,
// available from https://sourceforge.net/projects/xcptlib/
typedef void (*INITXCPT)(LPCTSTR szcompanyname, LPCTSTR szemailaddress, HWND hwnd, bool bshowgui);
typedef void (*USEXCPT)(bool bfulldump, bool binvokedebugger);
typedef void (*STOPXCPT)();
// declare pointers for the exception handler functions
INITXCPT pinitxcpt = NULL;
STOPXCPT pstopxcpt = NULL;
USEXCPT pusexcpt = NULL;
HINSTANCE hXCPTLib = NULL;


#endif


  void
  enable_Windows_XCPTLibHandler_if_possible()
  {
#ifdef _WIN32
#ifdef _MSC_VER
// for the sake of memory leak checks
   _CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
#endif

// Use exception handler in XCPTLib.dll if present
// Catches almost all exceptions that escapes phasertngs try-catch blocks
// Dynamic loading so phasertng won't fail if XCPTLib.dll isn't present
   hXCPTLib = LoadLibrary(TEXT("XCPTLib.dll"));

   if (hXCPTLib != NULL)
   {
// assign function pointers
      pinitxcpt = (INITXCPT) GetProcAddress(hXCPTLib, TEXT("InitialiseXCPTHandler"));
      pstopxcpt = (STOPXCPT) GetProcAddress(hXCPTLib, TEXT("StopUsingXCPTHandler"));
      pusexcpt = (USEXCPT) GetProcAddress(hXCPTLib, TEXT("UseMyXCPTHandler"));
// Initialise exception handler library with no GUI and no email invocation
      if (pinitxcpt)
         pinitxcpt("The Phaser Team", "", NULL, false);


      HKEY hKey(0);
      int nregval(0);
      DWORD dwBufLen=4;
      LONG lRet(0);

      bool binvokedebugger = false;
      bool fulldump = true;

#pragma comment( lib, "Advapi32" )// contains registry functions
// if HKEY_LOCAL_MACHINE\SOFTWARE\Phaser\InvokeDebugger is non-zero then invoke debugger at crash time
      lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,"SOFTWARE\\Phaser", 0, KEY_QUERY_VALUE, &hKey );
      if( lRet == ERROR_SUCCESS )
      {
         lRet = RegQueryValueEx( hKey, "InvokeDebugger", NULL, NULL, (LPBYTE) &nregval, &dwBufLen);
         if( (lRet == ERROR_SUCCESS) )
            nregval == 0 ? binvokedebugger = false : binvokedebugger = true;

         RegCloseKey( hKey );
      }

// if HKEY_LOCAL_MACHINE\SOFTWARE\Phaser\ProduceFullCrashDump is non-zero then write full dump at crash time
      lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,"SOFTWARE\\Phaser", 0, KEY_QUERY_VALUE, &hKey );
      if( lRet == ERROR_SUCCESS )
      {
         lRet = RegQueryValueEx( hKey, "ProduceFullCrashDump", NULL, NULL, (LPBYTE) &nregval, &dwBufLen);
         if( (lRet == ERROR_SUCCESS) )
            nregval == 0 ? fulldump = false : fulldump = true;

         RegCloseKey( hKey );
      }

      if (pusexcpt)
         pusexcpt(fulldump, binvokedebugger);

   }
#endif
  }

  void
  disable_Windows_XCPTLibHandler_if_possible()
  {
#ifdef _WIN32
    if (pstopxcpt)
    {// if exception handling library XCPTLib.dll was present then uninitialise and release it
      pstopxcpt();
      FreeLibrary(hXCPTLib);
    }
#endif
  }

//Do not make main.cc part of the phasertng namespace
//If you do, the fortran libraries will not link to it

#if defined(__linux)
#include <fenv.h>
#endif

  void
  enable_floating_point_exceptions_if_possible()
  {
#if defined(__linux)
    int flags = 0;
    flags |= FE_DIVBYZERO;
    flags |= FE_INVALID;
    flags |= FE_OVERFLOW;
    if (flags != 0) {
      feenableexcept(flags);
    }
    fedisableexcept(FE_INVALID);
#endif
  }

  bool
  boost_adptbx_libc_backtrace(int n_frames_skip=0)
  {
    bool result = false;
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_EXECINFO_H)
    static const int max_frames = 1024;
    void *array[max_frames];
    int size = backtrace(array, max_frames);
    fprintf(stderr, "libc backtrace (%d frames, most recent call last):\n",
      size - n_frames_skip);
    fflush(stderr);
    char **strings = backtrace_symbols(array, size);
    for(int i=size-1;i>=n_frames_skip;i--) {
      char* s = strings[i];
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_CXXABI_H)
      const char* m_bgn = 0;
#if defined(__APPLE_CC__)
      if (strlen(s) >= 52 && strncmp(s+40, "0x", 2) == 0) {
        m_bgn = strchr(s+40, ' ');
      }
#else // __linux
      m_bgn = strchr(s, '(');
#endif
      if (m_bgn != 0) {
        m_bgn++;
        const char* m_end = strchr(m_bgn,
#if defined(__APPLE_CC__)
        ' '
#else // __linux
        '+'
#endif
        );
        long n = m_end - m_bgn;
        if (n > 0) {
          char* mangled = static_cast<char*>(malloc(n+1));
          if (mangled != 0) {
            strncpy(mangled, m_bgn, n);
            mangled[n] = '\0';
            char* demangled = abi::__cxa_demangle(mangled, 0, 0, 0);
            free(mangled);
            if (demangled != 0) {
              long n1 = m_bgn - s;
              long n2 = strlen(demangled);
              long n3 = strlen(m_end);
              char* b = static_cast<char*>(
                malloc(static_cast<size_t>(n1+n2+n3+1)));
              if (b != 0) {
                strncpy(b, s, n1);
                strncpy(b+n1, demangled, n2);
                strncpy(b+n1+n2, m_end, n3);
                b[n1+n2+n3] = '\0';
                s = b;
              }
              free(demangled);
            }
          }
        }
      }
#endif // BOOST_ADAPTBX_META_EXT_HAVE_CXXABI_H
      fprintf(stderr, "  %s\n", s);
      fflush(stderr);
      if (s != strings[i]) free(s);
      result = true;
    }
    free(strings);
#endif // defined(BOOST_ADAPTBX_META_EXT_HAVE_EXECINFO_H)
    return result;
  }

  void
  show_call_stacks_and_exit(const char* what)
  {
    bool have_libc_trace = boost_adptbx_libc_backtrace(2);
    const char* hint = "sorry, call stacks not available";
    if (have_libc_trace) {
      hint = "libc call stack above";
    }
    fprintf(stderr, "%s (%s)\n", what, hint);
    fflush(stderr);
    exit(1);
  }

  void
  boost_adaptbx_segmentation_fault_backtrace(int)
  {
    show_call_stacks_and_exit("Segmentation fault");
  }

  void
  boost_adaptbx_bus_error_backtrace(int)
  {
    show_call_stacks_and_exit("Bus error");
  }

  void
  boost_adaptbx_floating_point_error_backtrace(int)
  {
    show_call_stacks_and_exit("Floating-point error");
  }

  void
  enable_signals_backtrace_if_possible()
  {
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_SIGNAL_H)
#if defined(SIGSEGV)
    signal(SIGSEGV, boost_adaptbx_segmentation_fault_backtrace);
#endif
#if defined(SIGBUS)
    signal(SIGBUS, boost_adaptbx_bus_error_backtrace);
#endif
#if defined(SIGFPE)
    signal(SIGFPE, boost_adaptbx_floating_point_error_backtrace);
#endif
#endif
  }

int main(int argc, char* argv[])
{
//PHASERTNG_DEBUG_THROW_STACKTRACE is defined in Phasertng.h
#ifndef PHASERTNG_DEBUG_THROW_STACKTRACE
  try {
#endif
    enable_Windows_XCPTLibHandler_if_possible();
    enable_floating_point_exceptions_if_possible();
    enable_signals_backtrace_if_possible();

  //catch the failures and print some info if it fails
  //from phenix.python wrapper, have to enter --version as special case
  //in order for this --version code to be called
    bool show_version(false);
    bool show_change_log(false);
    bool help_codes(false);
    std::string suite("PHENIX"); //default
    std::set<std::string> possible_suites = { "PHENIX","CCP4","VOYAGER","VERBOSE" };
    //phasertng --suite=phenix --suite=ccp4 --suite=voyager

    int i(1); //only consider first command line argument
    if (argc > i)
    {
      std::string argument = argv[i];
      if (argument == "--version") //special
        show_version = true;
      else if (argument == "--change_log") //special
        show_change_log = true;
      else if (argument == "--codes") //special
        help_codes = true;
      else if (argument.substr(0,7) == "--suite")
      {
        std::vector<std::string> keyval;
        phasertng::hoist::algorithm::split(keyval, argument, ("="));
        if (keyval.size() == 1)
        {
          std::cout << "Suite type not defined" << std::endl;
          std::exit(EXIT_SUCCESS);
        }
        else if (keyval.size() == 2)
        {
          suite = phasertng::stoupper(keyval[1]);
          if (!possible_suites.count(suite))
          {
            std::cout << "Suite type \"" + suite + "\" not recognised" << std::endl;
            std::exit(EXIT_SUCCESS);
          }
        }
      }
      else if (argument == "--help" or
               argument == "--options" or
               argument == "--usage") //special
      {
        std::cout << "Command line flags" << std::endl;
        std::cout << "  --help             Print command line help and exit" << std::endl;
        std::cout << "  --codes            Print help for error codes and exit" << std::endl;
        std::cout << "  --version          Print version number and exit" << std::endl;
        std::cout << "  --change_log       Print change_log and exit" << std::endl;
        std::cout << "  --suite=phenix     Call executable with phenix suite output" << std::endl;
        std::cout << "  --suite=ccp4       Call executable with ccp4 suite output" << std::endl;
        std::cout << "  --suite=voyager    Call executable with voyager suite output" << std::endl;
        std::exit(EXIT_SUCCESS);
      }
      else
      {
        std::cout << "Command line flag \"" + argument + "\" not recognised" << std::endl;
        std::exit(EXIT_SUCCESS);
      }
    }

    //construct Phasertng class as in python
    phasertng::Phasertng basetng(suite);
    basetng.called_from_executable = true;

    if (show_version) //special
    {
      //phasertng --version and exit
      std::cout << basetng.command_line(false) << std::endl;
      std::exit(EXIT_SUCCESS);
    }
    else if (show_change_log) //special
    {
      //phasertng --change_log and exit
      std::cout << basetng.command_line(true) << std::endl;
      std::exit(EXIT_SUCCESS);
    }
    else if (help_codes) //special
    {
      //phasertng --change_log and exit
      std::cout << basetng.error_help() << std::endl;
      std::exit(EXIT_SUCCESS);
    }
    phasertng::Preprocessor preprocessor; //output only
    //parse input stream ( phasertng < input)
    preprocessor.stdio(); //eats std::cin, outputs to std::cout
    //BELOW IS THE CALL OF phasertng.run() AS IN PYTHON
    basetng.run(preprocessor.cards()); //all errors caught
    disable_Windows_XCPTLibHandler_if_possible();
    //return an integer which encodes the error status / success=0
    return basetng.exit_code();
#ifndef PHASERTNG_DEBUG_THROW_STACKTRACE
  }
  //other errors from main caught and thrown
  catch (std::exception const& err) { //e.g. preprocessor include @file not found
    std::cerr << "Phasertng Parse Error: " << err.what() << std::endl;
    return static_cast<int>(phasertng::err::UNHANDLED);
  }
  catch (...) {
    std::cerr << "Phasertng Parse Error" << std::endl;
    return static_cast<int>(phasertng::err::DEVELOPER);
  }
#endif
  return 0;
}
