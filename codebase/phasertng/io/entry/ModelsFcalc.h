//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ModelsFcalc_class____
#define __phasertng_ModelsFcalc_class____
#include <phasertng/main/includes.h>
#include <phasertng/src/Bin.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/io/EntryBase.h>
#include <phasertng/math/Matrix.h>

#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <scitbx/array_family/accessors/c_grid.h>
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;
typedef scitbx::af::versa<double, scitbx::af::c_grid<3> >   versa_grid_double;

namespace phasertng {

  class Models;//foward declaration

  class ModelsFcalc : public EntryBase
  {
    public: //members
      Identifier             MODLID;
      std::string            TIMESTAMP = "";
      double                 FCALC_HIRES = 0; //the resolution stored in the structure factors
      int                    NMODELS = 0;
      Bin                    BIN;
      UnitCell               UC; //cell used for structure factor calculation
      SpaceGroup             SG = SpaceGroup("P 1");
      af::shared<millnx>     MILLER;
      math::Matrix<cmplex>   FCALC;

    public: //constructors
      ModelsFcalc() : EntryBase() {}
      ModelsFcalc(af_string,af::double6,af_millnx,af_cmplex);
      void SetModelsFcalc(ModelsFcalc&);
      bool in_memory() { return MILLER.size(); }
      void read_from_disk() { VanillaReadMtz(); }
      void write_to_disk() { VanillaWriteMtz(); }

    public:
      void p1_structure_factor_calculation(
        Models*,
        UnitCell,
        const double,
        const int,
        const bool,
        const double,
        const double
      );
      void p1_structure_factor_calculation(
        Models*,
        UnitCell,
        const double,
        const cctbx::uctbx::unit_cell,
        const double,
        const int,
        const bool,
        const double,
        const double
      );

      std::pair<double,versa_flex_double>  //midSsqr of correlated hires bin, CorMat
      calculate_correlation_matrix(
        double,
        double, // Target CC/sigma(CC) for worst off-diagonal element
        double
      );
      double setup_bins_with_statistically_weighted_bin_width(dvect3,int,int,int);
      void VanillaWriteMtz();
      void VanillaReadMtz();
      af_string   get_history();
      af::double6 get_cell() { return UC.get_cell(); }
      af_millnx   get_miller() { return MILLER.deep_copy(); }
      af_cmplex   get_data()   { return FCALC.ref(); }
      const cmplex& ecalc(int m, int r) const { return FCALC(m,r); }
      cmplex&     ecalc(int m, int r) { return FCALC(m,r); }
      std::pair<double,double>     polar_deg(int m, int r);
      math::Matrix<double> sigmap_with_binning();
      void        apply_sigmap(const math::Matrix<double>&);
      void        SetHistory(af_string);
      void        SetData(af_cmplex);
      af_cmplex   fcalc(int);
      af_double   density(std::pair<double,double>,bool);
      double      available_hires() { return FCALC_HIRES; }
      double      available_lores();
      void        initialize_bin(Bin);
      void        initialize_ecalc(int,af_cmplex);
      int         nrefl() { return MILLER.size(); }

      std::pair<af_millnx,af_cmplex>
      interpolate(int,UnitCell,double,dvect3,dvect3,std::pair<double,double>,bool);
  };

}
#endif
