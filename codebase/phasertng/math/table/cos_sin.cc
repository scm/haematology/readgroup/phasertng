#include <phasertng/main/Assert.h>
#include <phasertng/math/table/cos_sin.h>
#include <iostream>

namespace phasertng { namespace table {


      cos_sin_lin_interp_table::cos_sin_lin_interp_table(int n_points)
      :
        n_points_(n_points),
        n_points_4_(n_points/4)
      {
        values_.resize(n_points_+n_points_4_+1);
        diffvalues_.resize(n_points_+n_points_4_+1);
        PHASER_ASSERT(n_points % 4 == 0);
        using scitbx::constants::two_pi;
        for(int i=0;i<=n_points_+n_points_4_;i++)
        {
          values_[i] = std::sin(i*scitbx::constants::two_pi/n_points_);
          diffvalues_[i] = std::sin((i+1)*two_pi/n_points_)
                         - std::sin(i*two_pi/n_points_);
        }
      }

      std::complex<double>
      cos_sin_lin_interp_table::get(double unary_angle) const
      {
        double dec = unary_angle*n_points_;
        if (dec<0.0)
        {
          dec = -dec;
          int i = static_cast<int>(dec); // floor it to an integer
          // get fraction between the ceiling and the floor of fval
          double frac = dec - i;
          // return interpolated value between the i-th and the (i+1)th entry
          i = i % n_points_;

          return std::complex<double>(
              values_[i+n_points_4_] + frac*diffvalues_[i+n_points_4_],
              - values_[i] -frac*diffvalues_[i]);
        }
        else
        {
          int i = static_cast<int>(dec); // floor it to an integer
          double frac = dec - i;
          // return interpolated value between the i-th and the (i+1)th entry
          i = i % n_points_;

          return std::complex<double>(
              values_[i+n_points_4_] + frac*diffvalues_[i+n_points_4_],
              values_[i] + frac*diffvalues_[i]);
        }
      }
      // initialise the one and only static instance of this class
      cos_sin_lin_interp_table cos_sin_lin_interp_table::inst_;

}} // namespace cctbx::math
