//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/minimizer/RefineTest.h>
#include <scitbx/constants.h>
#include <set>
#include <string>

double gauss2d0(sv_double xy, double s11, double s12, double s22)
{
  double x = xy[0];
  double y = xy[1];
//  return -math.log(1/math.sqrt(4*math.pi**2*(s11*s22-s12*s12))
//           *math.exp(-(s22*x*x-2*s12*x*y+s11*y*y)/(2*(s11*s22-s12*s12))))

  return -std::log(1/std::sqrt(4*std::pow(scitbx::constants::pi,2)*(s11*s22-s12*s12))) + \
           (s22*x*x-2*s12*x*y+s11*y*y)/(2*(s11*s22-s12*s12));
}

double twisted_gauss2d0(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  double arg = twist*std::sqrt(x*x+y*y);
  double c = std::cos(arg);
  double s = std::sin(arg);
  double xt = x*c - y*s;
  double yt = y*c + x*s;
  sv_double xtyt = {xt,yt};
  return gauss2d0(xtyt, s11, s12, s22);
}

double analytic_grad_x(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  if (x == 0 && y == 0) return 0.;
  return (
         (-2*s12*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (std::cos(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (std::cos(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)))/
         (2.*(-s12*s12 + s11*s22)));
}

double analytic_grad_y(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  if (x == 0 && y == 0) return 0.;
  return (
         (-2*s12*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (std::cos(twist*std::sqrt(x*x + y*y)) +
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (std::cos(twist*std::sqrt(x*x + y*y)) +
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)))/
         (2.*(-s12*s12 + s11*s22)));
}

double analytic_curv_xx(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  if (x == 0 && y == 0) return 0.;
  return (
         (-2*s12*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) +
              (twist*twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) +
              (twist*twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*std::pow((std::cos(twist*std::sqrt(x*x + y*y)) -
               (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
               (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)),2)
             + 2*s11*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (3*twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (3*twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           4*s12*(std::cos(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            ((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*std::pow(((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/
                std::sqrt(x*x + y*y) + std::sin(twist*std::sqrt(x*x + y*y)) -
               (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)),2))
          /(2.*(-s12*s12 + s11*s22)));
}

double analytic_curv_yy(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  if (x == 0 and y == 0) return 0.;
  return (
         (-2*s12*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*y*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) +
              (twist*twist*y*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*y*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) +
              (twist*twist*y*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*y*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*y*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*y*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*y*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (3*twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*std::pow((-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                  std::sqrt(x*x + y*y)) - std::sin(twist*std::sqrt(x*x + y*y)) -
               (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)),2)\
            - 4*s12*(-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::sqrt(x*x + y*y)) - std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            (std::cos(twist*std::sqrt(x*x + y*y)) +
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*std::pow((std::cos(twist*std::sqrt(x*x + y*y)) +
               (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
               (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)),2)
           )/(2.*(-s12*s12 + s11*s22)));
}

double analytic_curv_xy(sv_double xy, double s11, double s12, double s22, double twist)
{
  double x = xy[0];
  double y = xy[1];
  if (x == 0 && y == 0) return 0.;
  return (
         (2*s11*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            (-((twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::pow((x*x + y*y),1.5)) -
              (twist*twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) +
              (twist*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(y*std::cos(twist*std::sqrt(x*x + y*y)) +
              x*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) +
              (twist*twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(x*std::cos(twist*std::sqrt(x*x + y*y)) -
              y*std::sin(twist*std::sqrt(x*x + y*y)))*
            ((twist*x*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::pow((x*x + y*y),1.5) -
              (twist*twist*x*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              (twist*x*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/
               std::pow((x*x + y*y),1.5) +
              (twist*twist*x*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/(x*x + y*y) -
              (twist*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s22*(std::cos(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            (-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(-((twist*y*y*std::cos(twist*std::sqrt(x*x + y*y)))/
                 std::sqrt(x*x + y*y)) - std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            ((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) +
              std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) -
           2*s12*(std::cos(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*x*x*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            (std::cos(twist*std::sqrt(x*x + y*y)) +
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)) +
           2*s11*((twist*x*x*std::cos(twist*std::sqrt(x*x + y*y)))/
               std::sqrt(x*x + y*y) + std::sin(twist*std::sqrt(x*x + y*y)) -
              (twist*x*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y))*
            (std::cos(twist*std::sqrt(x*x + y*y)) +
              (twist*x*y*std::cos(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y) -
              (twist*y*y*std::sin(twist*std::sqrt(x*x + y*y)))/std::sqrt(x*x + y*y)))/
         (2.*(-s12*s12 + s11*s22)));
}

namespace phasertng {

  RefineTest::RefineTest(sv_double x_start) : RefineBase()
  {
    x = x_start;
    s11 = 1.0;
    s12 = 1.2;
    s22 = 2.0;
    twist = 0.5;
  }

  void
  RefineTest::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(item);
    //sets refine_parameter_ vector, nmp and total_number_of_parameters_
    refine_parameter_.clear();
    if (protocol.find("first") != protocol.end())
      refine_parameter_.push_back(true);
    else
      refine_parameter_.push_back(false);
    if (protocol.find("second") != protocol.end())
      refine_parameter_.push_back(true);
    else
      refine_parameter_.push_back(false);

    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i])
         nmp++;
    total_number_of_parameters_ = refine_parameter_.size();
    assert(nmp != 0);
  }

  sv_string
  RefineTest::macrocycle_parameter_names()
  {
    sv_string names = {"First", "Second"};
    return names;
  }

  sv_double
  RefineTest::get_macrocycle_parameters()
  {
    sv_double pars;
    for (int i=0; i<total_number_of_parameters_; i++)
      if (refine_parameter_[i])
        pars.push_back(x[i]);
    PHASER_ASSERT (pars.size() == nmp);
    return pars;
  }

  void
  RefineTest::set_macrocycle_parameters(sv_double newx)
  {
    int l = 0;
    for (int i=0; i<total_number_of_parameters_; i++)
      if (refine_parameter_[i])
        x[i] = newx[l++];
    PHASER_ASSERT(l == nmp);
  }

  sv_double
  RefineTest::macrocycle_large_shifts()
  {
    sv_double largeShifts_all = {10., 10.};
    sv_double largeShifts;
    for (int i=0; i<total_number_of_parameters_; i++)
      if (refine_parameter_[i])
        largeShifts.push_back(largeShifts_all[i]);
    return largeShifts;
  }

/*
  std::vector<dtmin::Bounds>
  RefineTest::bounds()
  {
    dtmin::Bounds a, b;
    a.on(1., 10.);
    b.on(2., 20.);
    std::vector<dtmin::Bounds> bounds_all = {a,b};

    std::vector<dtmin::Bounds> bounds;
    for (int i=0; i<total_number_of_parameters_; i++)
      if (refine_parameter_[i])
        bounds.push_back(bounds_all[i]);

    PHASER_ASSERT(bounds.size() == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineTest::reparameterize()
  {
    dtmin::Reparams a(false, 1.0);
    dtmin::Reparams b(false, 2.0);
    std::vector<dtmin::Reparams> repar_all = {a,b};

    std::vector<dtmin::Reparams> repar;
    for (int i=0; i<total_number_of_parameters_; i++)
      if (refine_parameter_[i])
        repar.push_back(repar_all[i]);

    PHASER_ASSERT(repar.size() == nmp);
    return repar;
  }
*/

  TargetType
  RefineTest::target()
  {
    PHASER_ASSERT(x.size() == nmp);
    return twisted_gauss2d0(x, s11, s12, s22, twist);
  }

  TargetGradientType
  RefineTest::target_gradient()
  {
    TargetGradientType tg(nmp);
    for (auto& item : tg.Gradient) item = 0;
    //in thise test class we calulate the full gradient for all parameters (not just those being refined),
    //actual code may take care to only calculate the terms for the parameters being refined.
    double g1 = analytic_grad_x(x, s11, s12, s22, twist);
    double g2 = analytic_grad_y(x, s11, s12, s22, twist);
    sv_double g = {g1, g2};

    // modify the gradient
    int i(0);
    for (int m = 0; m < total_number_of_parameters_; m++)
      if (refine_parameter_[m])
        tg.Gradient[i++] = g[m];
    PHASER_ASSERT(i == nmp);
    tg.Target = target();
    return tg;
  }

  TargetGradientHessianType
  RefineTest::target_gradient_hessian()
  {
    //in this test class we calulate the full Hessian for all parameters (not just those being refined),
    //actual code may take care to only calculate the terms for the parameters being refined.
    TargetGradientHessianType tgh(nmp,false);
    af::versa<double, af::flex_grid<> > h(af::flex_grid<>(2,2));
    h(0,0) = analytic_curv_xx(x, s11, s12, s22, twist);
    h(1,1) = analytic_curv_yy(x, s11, s12, s22, twist);
    h(0,1) = analytic_curv_xy(x, s11, s12, s22, twist);
    h(1,0) = h(0,1);
    //select the terms corresponding to parameters that are being refined
    double k(0), l(0);
    for (int i=0; i<total_number_of_parameters_; i++) {
      if (refine_parameter_[i]) {
        for (int j=0; j<total_number_of_parameters_; j++) {
          if (refine_parameter_[j])
            tgh.Hessian(k,l++) = h(i,j);
        }
        l = 0;
        k++;
      }
    }

    //set the is_diagonal variable
    TargetGradientType tg = RefineTest::target_gradient();
    tgh.Target = tg.Target;
    tgh.Gradient = tg.Gradient;
    return tgh;
  }

  af_string RefineTest::current_statistics()
  {
    af_string txt;
    txt.push_back("x,f: (" + std::to_string(x[0]) + ", " +
                  std::to_string(x[1]) + ") " +
                  std::to_string(RefineTest::target()) + '\n');
    return txt;
  }



} //phasertng
