//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/int_number.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  int_number::int_number(
      const std::string parameter,
      const bool pminset_, const int  pminval_,
      const bool pmaxset_, const int  pmaxval_,
      const int  dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  int_number::set_value(int  other)
  {
    user = true;
    uvalue = other;
  }

  void
  int_number::set_value(const sv_string& vcards,const int & other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  int_number::is_default() const
  { return uvalue == dephalt; }

  void
  int_number::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  int
  int_number::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  int_number::to_python()
  { return unparse_parameter(false); }

  void
  int_number::from_python(std::string str)
  { set_value(std::stoi(str)); }

  int
  int_number::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  int_number::unparse_parameter(bool phil) const
  {
    if (value_or_default() == DEF_INT_FLAG)
      return None();
    std::stringstream ss;
    ss << uvalue;
    return ss.str();
  }

  std::string
  int_number::show_default(bool phil) const
  {
    if (dephalt == DEF_INT_FLAG)
      return None();
    std::stringstream ss;
    ss << dephalt;
    return ss.str();
  }

}}
