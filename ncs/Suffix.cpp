#include "Suffix.h"

using namespace std;

namespace phaser {  namespace ncs {

//
// Suffix methods
//

// Constructor and destructor
Suffix::Suffix( const string& word, const Node& origin)
        : _word( word ), _origin( origin ), _considered_length( 0 ),
          _empty_node( Node() ),
          _entry_point( _empty_node )
{
}


Suffix::~Suffix()
{
}


// Accessors
const Node&
Suffix::get_origin() const
{
    return _origin;
}


const Node&
Suffix::get_entry_point() const
{
    return _entry_point;
}


const string&
Suffix::get_word() const
{
    return _word;
}


const string
Suffix::get_residual_suffix() const
{
    return _word.substr( _considered_length );
}


// Logical functions
bool
Suffix::is_explicit() const
{
    return ( _considered_length == 0 );
}


bool
Suffix::has_next_char() const
{
    return ( _considered_length < _word.length() );
}


bool
Suffix::entry_point_empty() const
{
    return ( _entry_point == _empty_node );
}


// Suffix behaviour
void
Suffix::extend( size_t char_count )
{
    _considered_length += char_count;
}


void
Suffix::decrement( size_t char_count )
{
    _considered_length -= char_count;
    _word = _word.substr( char_count );
}


void
Suffix::reduce()
{
    while ( ! is_explicit() )
    {
        Edge active_edge( _word[0], _origin );

        if ( active_edge.length() <= _considered_length )
        {
            decrement( active_edge.length() );
            _origin = active_edge.get_end_node();
        } else
        {
            break;
        }
    }
}


void
Suffix::prepare_new_entry()
{
    char next_char( _word[ _considered_length ] );

    if ( ! is_explicit() )
    {
        Edge active_edge( _word[0], _origin );
        char trial_char( active_edge.get_arc()[ _considered_length ] );

        if ( trial_char == next_char )
        {
            _entry_point = _empty_node;
        } else
        {
            active_edge.split_edge_at( _considered_length );
            _entry_point = active_edge.get_end_node();
        }
    } else if ( _origin.has_child( next_char ) )
    {
        _entry_point = _empty_node;
    } else
    {
        _entry_point = _origin;
    }
}


void
Suffix::shift_origin_to_suffix()
{
    _origin = _origin.get_suffix_node();
}

}  } // namespace phaser::ncs
