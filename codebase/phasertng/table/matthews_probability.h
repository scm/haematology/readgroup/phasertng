#ifndef __phasertng_matthews_probability_table__
#define __phasertng_matthews_probability_table__
#include <phasertng/enum/composition_type.h>

namespace phasertng {
namespace table {

double matthews_probability(double,double,composition::type);
double matthews_vm(double,composition::type);

}} //namespace phaser
#endif
