//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <phasertng/main/Suite.h>
#include <autogen/include/InputCard.h>
#include <phasertng/main/Version.h>
#include <phasertng/main/Output.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Preprocessor.h>
#include <phasertng/main/CCP4base.h>
#include <phasertng/main/Phasertng.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

class ScopedGILRelease
{
public:
    inline ScopedGILRelease()
    {
        m_thread_state = PyEval_SaveThread();
    }

    inline ~ScopedGILRelease()
    {
        PyEval_RestoreThread(m_thread_state);
        m_thread_state = NULL;
    }

private:
    PyThreadState * m_thread_state;
};

void GIL_run( Phasertng& self, std::string Cards_)
{
  ScopedGILRelease scoped;
  self.run( Cards_ );
}

bool GIL_setup_before_call_to_mode( Phasertng& self, af_string pymulti, int imode)
{
  ScopedGILRelease scoped;
  return self.setup_before_call_to_mode( pymulti , imode);
}

void GIL_cleanup_after_call_to_mode( Phasertng& self, bool job_has_been_run_previously)
{
  ScopedGILRelease scoped;
  self.cleanup_after_call_to_mode(job_has_been_run_previously);
}

struct python_out : phenix_out
{
  virtual ~python_out();
  virtual void write(const std::string& text);
  virtual void flush();

  python_out(boost::python::object const& file_object)
  :
    have_flush(false),
    write_method(file_object.attr("write"))
  {
    if (PyObject_HasAttrString(file_object.ptr(), "flush")) {
      flush_method = file_object.attr("flush");
      have_flush = true;
    }
  }

  bool have_flush;
  boost::python::object write_method;
  boost::python::object flush_method;
};

void
python_out::write(const std::string& text)
{
  write_method(text);
}

void
python_out::flush()
{
  if (have_flush) flush_method();
}

python_out::~python_out() {}

void
set_sys_stdout(Output& self)
{
  boost::python::object sys_stdout = boost::python::object(
    boost::python::handle<>(PyImport_ImportModule("sys"))).attr("stdout");
  self.setOutStream(std::shared_ptr<phenix_out>(
    new python_out(sys_stdout)));
}

void
set_file_object(
  Output& self,
  boost::python::object const& file_object)
{
  self.setOutStream(std::shared_ptr<phenix_out>(
    new python_out(file_object)));
}

// Start of callback interface
struct python_callback : phenix_callback
{
  virtual ~python_callback();
  virtual void call_back(const std::string& message, const std::string& data);

  python_callback(boost::python::object const& callback_object) :
    call_back_method(callback_object.attr("call_back"))
  {}

  boost::python::object call_back_method;
};

void python_callback::call_back(const std::string& message,
                                const std::string& data)
{
  call_back_method(message, data);
}

python_callback::~python_callback() {}

void set_callback_object(
  Output& self,
  boost::python::object const& callback_object)
{
  self.setCallBack(std::shared_ptr<phenix_callback>(
    new python_callback(callback_object)));
}

// End of callback interface

void
wrap_phasertng_output()
{
  using namespace boost::python;

  class_<Version>("Version", init<>())
    .def("git_revision", &Version::git_revision)
    .def("git_info", &Version::git_info)
    .def("version_number", &Version::version_number)
  ;

  //change name of phasertng::Error to PhaserError
  class_<Error>("PhaserError")
    .def(init<>())
    .def(init<err::code,std::string>())
    .def("Set", &Error::Set)
    .def("failure", &Error::failure)
    .def("success", &Error::success)
    .def("error_type", &Error::error_type)
    .def("exit_code", &Error::exit_code)
    .def("error_message", &Error::error_message)
    .def("error_help", &Error::error_help)
    .def("error_code", &Error::error_code)
  ;

  class_<Suite>("Suite", init<std::string>())
    //getters
    .def_readwrite("suite",&Suite::suite)
    .def("isLevel",&Suite::isLevel)
    .def("Store",&Suite::Store)
    .def("Write",&Suite::Write)
    .def("WritePhil",&Suite::WritePhil)
    .def("WriteCards",&Suite::WriteCards)
    .def("WriteFiles",&Suite::WriteFiles)
    .def("WriteData",&Suite::WriteData)
    .def("WriteDensity",&Suite::WriteDensity)
    .def("WriteLog",&Suite::WriteLog)
    .def("WriteXml",&Suite::WriteXml)
    .def("Loggraphs",&Suite::Loggraphs)
    .def("KillFile",&Suite::KillFile)
    .def("KillTime",&Suite::KillTime)
    .def("DataBase",&Suite::DataBase)
    //setters
    //don't use SetWriteData etc
    // use cards parse_multi including "phasertng suite write_data on"
    // set_as_str phasertng.suite.write_data does not work
    .def("SetLevel",&Suite::SetLevel)
    .def("SetStore",&Suite::SetStore)
    .def("set_defaults_for_suite",&Suite::set_defaults_for_suite)
  ;

  class_<Output, bases <Suite,Version,Error> >("Output", init<std::string>())
    .def("set_sys_stdout", set_sys_stdout)
    .def("set_file_object", set_file_object, ( arg("file_object")))
    .def("set_callback_object", set_callback_object, ( arg("callback")))
    .def("get_mode_cpus_seconds", &Output::get_mode_cpus_seconds)
    .def("get_mode_wall_seconds", &Output::get_mode_wall_seconds)
    .def("get_cumulative_cpus_seconds", &Output::get_cumulative_cpus_seconds)
    .def("get_cumulative_wall_seconds", &Output::get_cumulative_wall_seconds)
    .def("clear_storage", &Output::clear_storage)
    .def("warnings", &Output::warnings)
    .def("loggraphs", &Output::loggraphs)
    .def("advisories", &Output::advisories)
    .def("tables", &Output::tables)
    .def("logfile", &Output::logfile)
    .def("summary", &Output::summary)
    .def("concise", &Output::concise)
    .def("process", &Output::process)
    .def("verbose", &Output::verbose)
    //.def("further", &Output::further)
    .def("testing", &Output::testing)
    .def("logHeader", &Output::logHeader)
    .def("logSectionHeader", &Output::logSectionHeader)
    .def("logProgramStart", &Output::logProgramStart)
    .def("logProgramEnd", &Output::logProgramEnd)
    .def("logHeaderTitle", &Output::logHeaderTitle)
    .def("logTrailer", &Output::logTrailer)
    .def("logTab", &Output::logTab)
    .def("logTabArray", &Output::logTabArray)
    .def("logFileWritten", &Output::logFileWritten)
    .def("logBlank", &Output::logBlank)
    .def("logUnderLine", &Output::logUnderLine)
    .def("logLine", &Output::logLine)
    .def("logTableTop", &Output::logTableTop)
    .def("logTableEnd", &Output::logTableEnd)
    .def("logLoopOpen", &Output::logLoopOpen)
    .def("logLoopNext", &Output::logLoopNext)
    .def("logLoopShut", &Output::logLoopShut)
    .def("logLoopHere", &Output::logLoopHere)
    .def("logLoopExit", &Output::logLoopExit)
    .def("logLoopInfo", &Output::logLoopInfo)
    .def("logEllipsisOpen", &Output::logEllipsisOpen)
    .def("logEllipsisShut", &Output::logEllipsisShut)
    .def("logChevron", &Output::logChevron)
    .def("logWarning", &Output::logWarning)
    .def("logAdvisory", &Output::logAdvisory)
    .def("logError", &Output::logError)
    .def("logAssert", &Output::logAssert)
    .def("logFlush", &Output::logFlush)
    .def("Print", &Output::Print)
    .def("Banner", &Output::Banner)
    .def("Footer", &Output::Footer)
    .def("add_trailer", &Output::add_trailer)
    .def("subtract_trailer", &Output::subtract_trailer)
    .def_readwrite("USE_CALLBACKS", &Output::USE_CALLBACKS)
    .def_readwrite("LOOPXML", &Output::LOOPXML)
    .def("loopxml", &Output::loopxml)
  ;

  class_<Preprocessor, bases<Error> >("Preprocessor",init<>())
    .def("stdio",&Preprocessor::stdio)
    .def("stream",&Preprocessor::stream)
    .def("keywords",&Preprocessor::keywords)
    .def("cards",&Preprocessor::cards)
  ;

  class_<CCP4base>("CCP4base",init<>())
    .def("has_errors",&CCP4base::has_errors)
  ;

  class_<InputCard, bases<CCP4base> >("InputCard",init<>())
    .def("parse",&InputCard::parse)
    .def("parse_multi",&InputCard::parse_multi)
    .def("card_str",&InputCard::card_str)
    .def("analyse",&InputCard::analyse)
    .def("labin", &InputCard::labin)
    .def("has_errors",&InputCard::has_errors)
    .def("set_as_str",&InputCard::set_as_str)
    .def("set_as_str_i",&InputCard::set_as_str_i)
    .def("set_as_def",&InputCard::set_as_def)
    .def("get_as_str",&InputCard::get_as_str)
    .def("get_as_str_i",&InputCard::get_as_str_i)
    .def("size",&InputCard::size)
    .def("resize",&InputCard::resize)
    .def_readwrite("generic_string",&InputCard::generic_string) //for passing data in memory
    .def_readwrite("generic_int",&InputCard::generic_int) //for passing data in memory
    .def_readwrite("running_mode",&InputCard::running_mode)
  ;

  class_<Phasertng, bases<Output,Error> >("Phasertng",init<>())
    .enable_pickling()
    .def(init<std::string>())
  //.def("run", GIL_run, (args ("Cards_")))
    .def("run", &Phasertng::run, (args ("Cards_")))
  //  .def("setup_before_call_to_mode",GIL_setup_before_call_to_mode,(arg("multi"),arg("imode")))
  //  .def("append_parse_cards", &Phasertng::append_parse_cards)
    .def("setup_before_call_to_mode",&Phasertng::setup_before_call_to_mode,(arg("multi"),arg("imode")))
   // .def("cleanup_after_call_to_mode",GIL_cleanup_after_call_to_mode,(arg("job_has_been_run_previously")))
    .def("cleanup_after_call_to_mode",&Phasertng::cleanup_after_call_to_mode,(arg("job_has_been_run_previously")))
    .def("set_user_input_for_suite",&Phasertng::set_user_input_for_suite)
    .def("skipping", &Phasertng::skipping)
    .def("phil_str",&Phasertng::phil_str)
    .def("card_str",&Phasertng::card_str)
    .def("get_filenames",&Phasertng::get_filenames)
    .def("clear_filenames",&Phasertng::clear_filenames)
    .def("get_error_object",&Phasertng::get_error_object)
    .def("input_keywords_for_mode",&Phasertng::input_keywords_for_mode)
    .def("result_keywords_for_mode",&Phasertng::result_keywords_for_mode)
    .def("load_dag",&Phasertng::load_dag)
    .def("load_dag_file",&Phasertng::load_dag_file)
    .def("set_nthreads",&Phasertng::set_nthreads)
    .def("set_use_strictly_nthreads",&Phasertng::set_use_strictly_nthreads)
    .def("load_entry_from_database",&Phasertng::load_entry_from_database)
    .def("reset_reflections",&Phasertng::reset_reflections)
    .def("hashing",&Phasertng::hashing)
    .def("hashing_extra",&Phasertng::hashing_extra)
    .def("write_logfiles",&Phasertng::write_logfiles)
    .def("get_moderunner",&Phasertng::get_moderunner)
    .def("load_moderunner",&Phasertng::load_moderunner)
    .def("load_cards",&Phasertng::load_cards)
    .def_readwrite("input",&Phasertng::input)
    .def_readwrite("REFLECTIONS", &Phasertng::REFLCOLSMAP) //change the name here
    .def_readwrite("DAGDATABASE", &Phasertng::DAGDB) //change the name here
  ;

  class_<pod::moderunner>("moderunner",init<>())
    .enable_pickling()
    .def( init<>() )
    .def("deepcopy", &pod::moderunner::copy)
//these readwrite parameters must be defined for the __dict__ method to have a default
//and therefore for deepcopy to work and pickle to work
    .def_readwrite("pathway_string", &pod::moderunner::pathway_string)
    .def_readwrite("pathway_tag",&pod::moderunner::pathway_tag)
    .def_readwrite("pathlog_string",&pod::moderunner::pathlog_string)
    .def_readwrite("pathlog_tag",&pod::moderunner::pathlog_tag)
    .def_readwrite("subdir",&pod::moderunner::subdir)
    .def_readwrite("dag_cards_filename",&pod::moderunner::dag_cards_filename)
    .def_readwrite("tng_cards_filename",&pod::moderunner::dag_cards_filename)
    .def_readwrite("size",&pod::moderunner::size)
    .def_readwrite("top_tfz",&pod::moderunner::top_tfz)
    .def_readwrite("polarity",&pod::moderunner::polarity)
    .def_readwrite("number_of_poses",&pod::moderunner::number_of_poses)
    .def_readwrite("tncs_info",&pod::moderunner::tncs_info)
    .def_readwrite("sg_info",&pod::moderunner::sg_info)
    .def_readwrite("ccs",&pod::moderunner::ccs)
    .def_readwrite("current_seeks", &pod::moderunner::current_seeks)
    .def_readwrite("message", &pod::moderunner::message)
    .def_readwrite("restart_at_cell_content_scaling", &pod::moderunner::restart_at_cell_content_scaling)
    .def_readwrite("dagcards",&pod::moderunner::dagcards)
    .def_readwrite("tngcards",&pod::moderunner::tngcards)
    .def("get_pathway_string", &pod::moderunner::get_pathway_string)
    .def("get_pathway_tag",&pod::moderunner::get_pathway_tag)
    .def("get_pathlog_string",&pod::moderunner::get_pathlog_string)
    .def("get_pathlog_tag",&pod::moderunner::get_pathlog_tag)
    .def("get_subdir",&pod::moderunner::get_subdir)
    .def("get_dag_cards_filename",&pod::moderunner::get_dag_cards_filename)
    .def("get_tng_cards_filename",&pod::moderunner::get_tng_cards_filename)
    .def("get_size",&pod::moderunner::get_size)
    .def("get_top_tfz",&pod::moderunner::get_top_tfz)
    .def("get_polarity",&pod::moderunner::get_polarity)
    .def("get_number_of_poses",&pod::moderunner::get_number_of_poses)
    .def("get_tncs_info",&pod::moderunner::get_tncs_info)
    .def("get_sg_info",&pod::moderunner::get_sg_info)
    .def("get_ccs",&pod::moderunner::get_ccs)
    .def("get_current_seeks", &pod::moderunner::get_current_seeks)
    .def("get_message", &pod::moderunner::get_message)
    .def("get_restart_at_cell_content_scaling", &pod::moderunner::get_restart_at_cell_content_scaling)
    .def("get_dagcards", &pod::moderunner::get_dagcards)
    .def("get_tngcards", &pod::moderunner::get_tngcards)
    .def("set_pathway_string", &pod::moderunner::set_pathway_string)
    .def("set_pathway_tag",&pod::moderunner::set_pathway_tag)
    .def("set_pathlog_string",&pod::moderunner::set_pathlog_string)
    .def("set_pathlog_tag",&pod::moderunner::set_pathlog_tag)
    .def("set_subdir",&pod::moderunner::set_subdir)
    .def("set_dag_cards_filename",&pod::moderunner::set_dag_cards_filename)
    .def("set_tng_cards_filename",&pod::moderunner::set_tng_cards_filename)
    .def("set_size",&pod::moderunner::set_size)
    .def("set_top_tfz",&pod::moderunner::set_top_tfz)
    .def("set_polarity",&pod::moderunner::set_polarity)
    .def("set_number_of_poses",&pod::moderunner::set_number_of_poses)
    .def("set_tncs_info",&pod::moderunner::set_tncs_info)
    .def("set_sg_info",&pod::moderunner::set_sg_info)
    .def("set_ccs",&pod::moderunner::set_ccs)
    .def("set_current_seeks", &pod::moderunner::set_current_seeks)
    .def("set_message", &pod::moderunner::set_message)
    .def("set_restart_at_cell_content_scaling", &pod::moderunner::set_restart_at_cell_content_scaling)
    .def("set_dagcards", &pod::moderunner::set_dagcards)
    .def("set_tngcards", &pod::moderunner::set_tngcards)
  ;

}
} // namespace phasertng::<anonymous>

void init_phasertng_output()
{
  wrap_phasertng_output();
}

} // namespace boost_python
} // namespace phasertng
