//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/math/slope_and_intercept.h>
#include <phasertng/src/ConsistentDLuz.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <phasertng/io/tostr2.h>
#include <iotbx/pdb/hierarchy.h>
#include <iotbx/pdb/input.h>
#include <scitbx/misc/split_lines.h>
#include <ncs/EnsembleSymmetry.h>

namespace phasertng {

  //Ensemble_single_model
  void Phasertng::runESM()
  {
    //for checking for extended molecule
    //not compulsory, used if present
    UnitCell UC;
    double config_hires(3.0); //fallback default for nomad, no data
    bool reflections_input = REFLECTIONS->reflid().is_set_valid();
    if (reflections_input)
    {
      UC = UnitCell(REFLECTIONS->UC); //reflections not compulsory, used if present
      config_hires = REFLECTIONS->data_hires();
    }
    else if (!input.value__flt_cell.at(".phasertng.data.unitcell.").is_default() and
             !input.value__flt_paired.at(".phasertng.data.resolution_available.").is_default())
    {
      if (input.value__flt_cell.at(".phasertng.data.unitcell.").is_default())
        throw Error(err::INPUT,"data unitcell not set");
      UC = UnitCell(input.value__flt_cell.at(".phasertng.data.unitcell.").value_or_default());
      if (input.value__flt_paired.at(".phasertng.data.resolution_available.").is_default())
        throw Error(err::INPUT,"data resolution_available not set");
      config_hires =
        input.value__flt_paired.at(".phasertng.data.resolution_available.").value_or_default_min();
    }
    else
    {
      out::stream where = out::LOGFILE;
      logTab(where,"No reflections: working resolution set to " + dtos(config_hires,2) + "A");
      logBlank(where);
    }

    DAGDB.shift_tracker(hashing(),input.running_mode);
    bool io_helix_test = input.value__boolean.at(".phasertng.molecular_transform.helix_test.").value_or_default();

    Identifier modlid;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Model Identifier");
    //modlid = DAGDB.NODES.front().TRACKER.ULTIMATE;
    //if tag is not input then the default tag is the stem of the pdb file
    //(stem = the filename without the final extension)
    FileSystem filesystem;
    if (input.value__path.at(".phasertng.model.filename.").is_default() and
        input.value__boolean.at(".phasertng.model.single_atom.use.").value_or_default())
    {
      logTab(where,"Model single atom");
      //single atom is used if the model filename is not set
      if (!input.value__boolean.at(".phasertng.model.single_atom.use.").value_or_default())
        throw Error(err::FILESET,"model filename");
      if (input.value__string.at(".phasertng.model.single_atom.scatterer.").is_default())
        throw Error(err::INPUT,"model single_atom scatterer");
      auto atm = input.value__string.at(".phasertng.model.single_atom.scatterer.").value_or_default();
      atm = tostr2(atm).second; //always upper, trimmed
      modlid.initialize_from_text(atm);
      modlid.initialize_tag(atm);
    }
    else if (!input.value__path.at(".phasertng.model.filename.").is_default())
    {
      filesystem.SetFileSystem(input.value__path.at(".phasertng.model.filename.").value_or_default());
      std::string tag = filesystem.stem();
      if (!input.value__string.at(".phasertng.model.tag.").is_default())
      {
        tag = input.value__string.at(".phasertng.model.tag.").value_or_default();
        logTab(where,"Model tag from input");
      }
      else
      {
        logTab(where,"Model tag from filename");
      }
      auto reorient = btos(input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default());
      auto boxscale = dtos(input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").value_or_default());
      modlid.initialize_from_text(tag+reorient+boxscale); //number
      modlid.initialize_tag(tag); //overwrites tag only
    }
    else if (input.generic_string.size()) //flag for generic_string as pdb file
    {
      if (input.value__string.at(".phasertng.pdbout.tag.").is_default())
        throw Error(err::INPUT,"pdb string in memory without tag");
      auto tag = input.value__string.at(".phasertng.pdbout.tag.").value_or_default();
      auto id =  input.value__uuid_number.at(".phasertng.pdbout.id.").value_or_default();
      //we will make reorient false for ease of comparison
      modlid.initialize_from_text(tag);
      modlid.initialize_tag(tag); //overwrites tag only
    }
    else
    {
      throw Error(err::INPUT,"no model defined");
    }
    logTab(where,"Identifier: " + modlid.str());
    phaser_assert(modlid.is_set_valid());
    }}

    auto search = DAGDB.add_entry(modlid);

    Models& models = search->MODELS;
    models.MODLID = search->identify();
    models.INPUT_CELL = UnitCell();
    models.TIMESTAMP = TimeStamp();
    auto reflid = REFLECTIONS->reflid();
    if (!reflid.is_set_valid())
      reflid.parse_string("1",models.MODLID.tag());
    models.REFLID = reflid;
    models.USE_MSE = input.value__boolean.at(".phasertng.model.selenomethionine.").value_or_default();

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Model");
    if (input.value__path.at(".phasertng.model.filename.").is_default() and
        input.value__boolean.at(".phasertng.model.single_atom.use.").value_or_default())
    {
      if (!input.value__boolean.at(".phasertng.model.single_atom.use.").value_or_default())
        throw Error(err::FILESET,"single filename");
      models.PDB.resize(1);
      models.PDB[0].resize(1);
      auto atm = input.value__string.at(".phasertng.model.single_atom.scatterer.").value_or_default();
      atm = stoupper(atm); //always upper
      models.PDB[0][0].set_element(atm);
    }
    else if (!input.value__path.at(".phasertng.model.filename.").is_default())
    {
      models.SetFileSystem(input.value__path.at(".phasertng.model.filename.").value_or_default());
      logTab(where,"Model read from file: " + models.qfstrq());
      models.read_from_disk(); ///ReadPdb !!!
      if (models.has_read_errors())
      throw Error(err::FATAL,models.read_errors());
      PHASER_ASSERT(models.PDB.size());
      logTab(where,"Number of models in single: " + itos(models.PDB.size()));
      if (input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default())
        logTab(out::LOGFILE,"Reorient to principal axes");
      if (models.PDB.size() > 1)
      {
        logWarning("Only first model used");
        models.PDB.erase(models.PDB.begin()+1,models.PDB.end());
      }
    }
    else if (input.generic_string.size()) //flag for generic_string as pdb file
    {
      logTab(where,"Model in memory");
      models.SetFileSystem(modlid.tag());
      auto ent =  input.generic_string; //pass the pdb file as an in memory string on the input object
      PHASER_ASSERT(ent.size() > 0);
      sv_string sv_pdblines;
      if (hoist::algorithm::contains(ent,NODE_SEPARATOR()))
        hoist::algorithm::split(sv_pdblines,ent,NODE_SEPARATOR());
      else
        hoist::algorithm::split(sv_pdblines,ent,"\n");
      af_string af_pdblines;
      for (auto& line : sv_pdblines)
        af_pdblines.push_back(line);
      models.SetPdb(af_pdblines); //in memory
      if (models.has_read_errors())
      throw Error(err::FATAL,models.read_errors());
      PHASER_ASSERT(models.PDB.size());
      logTab(where,"Number of models in single: " + itos(models.PDB.size()));
      if (input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default())
        logTab(out::LOGFILE,"Reorient to principal axes");
      if (models.PDB.size() > 1)
      {
        logWarning("Only first model used");
        models.PDB.erase(models.PDB.begin()+1,models.PDB.end());
      }
    }
    logTab(where,"Number of atoms: " + itos(models.PDB[0].size()));
    }}

    if (models.is_atom() or
        input.value__boolean.at(".phasertng.molecular_transform.fix.").value_or_default())
    {
      search->PRINCIPAL_ORIENTATION = {1,0,0, 0,1,0, 0,0,1};
      search->PRINCIPAL_TRANSLATION = {0,0,0};
      search->CENTRE = {0,0,0};
      logTab(out::VERBOSE,"Reorient rotation: [1,0,0, 0,1,0, 0,0,1] (fixed)");
      logTab(out::VERBOSE,"Reorient translation: [0,0,0] (fixed)");
    }
    else if (input.value__boolean.at(".phasertng.molecular_transform.reorient.").value_or_default())
    {
      models.set_principal_components();
      models.reorient_on_centre(models.components.PR,models.components.PT,dvect3(0,0,0));
      search->PRINCIPAL_ORIENTATION = models.components.PR;
      search->PRINCIPAL_TRANSLATION = models.components.PT;
      search->CENTRE = -models.components.PT;
      logTab(out::VERBOSE,"Reorient rotation: " + dmtos(models.components.PR,3));
      logTab(out::VERBOSE,"Reorient translation: " + dvtos(models.components.PT,3));
    }
    else
    {
      models.set_principal_statistics();
      models.recentre(models.statistics.CENTRE);
      search->PRINCIPAL_ORIENTATION =  {1,0,0, 0,1,0, 0,0,1};
      search->PRINCIPAL_TRANSLATION = -models.statistics.CENTRE;
      search->CENTRE = models.statistics.CENTRE;
      logTab(out::VERBOSE,"No reorientation");
      logTab(out::VERBOSE,"Centre: " + dvtos(models.statistics.CENTRE,3));
    }

    if (input.value__boolean.at(".phasertng.model.convert_rmsd_to_bfac.").value_or_default())
    {
      models.convert_rmsd_to_bfac();
      logTab(out::LOGFILE,"Rmsd converted to B-factors");
    }

    models.set_principal_statistics();
    {{
    //recalculate extent in new orientation
    out::stream where = reflections_input ? out::VERBOSE : out::LOGFILE;
    logUnderLine(where,"Model Properties");
    search->HIRES = config_hires;
    search->ATOM = models.is_atom();
    search->HELIX = io_helix_test ? models.is_helix() : false;
    search->MAP = false;
    search->SCATTERING = std::ceil(models.scattering()[0]);
    auto comp = models.composition(0);
    for (auto elem : comp)
      logTab(where,"Composition Element: " + elem.first + " " + itos(elem.second));
    search->MOLECULAR_WEIGHT = models.molecular_weight()[0];
    search->MEAN_RADIUS = models.statistics.MEAN_RADIUS;
    search->EXTENT = models.statistics.EXTENT;
    search->VOLUME = 1; //single atom
    if (!models.is_atom())
    {
      //calculate the volume from the sequence, same as for the map
      Sequence seq(models.SELENOMETHIONINE);
      bool use_unknown(false);
      seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
      seq.initialize(true,true);
      search->VOLUME = seq.volume();
    }
    logTab(where,"Mean Radius: " + dtos(models.statistics.MEAN_RADIUS,2));
    logTab(where,"Extent: " + dvtos(models.statistics.EXTENT,2));
    logTab(where,"Mass Extent Ratio: " + dtos(models.statistics.EXTENT_RATIO,2));
    logTab(where,"Volume: " + dtos(search->VOLUME,2));
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Model Atom/Helix/Barbwire Check");
    io_helix_test ?
      logTab(where,"Helix test will be performed") :
      logTab(where,"Helix test will NOT be performed");
    logTab(where,"Atom:  " + btos(search->ATOM));
    logTab(where,"Helix: " + btos(search->HELIX));
    double coilx = 2.0;
    bool barbwire_test = input.value__boolean.at(".phasertng.molecular_transform.barbwire_test.").value_or_default();
    dvect3 extent = search->EXTENT;
    dvect3 cell = UC.GetBox();
    std::sort(extent.begin(),extent.end());
    std::sort(cell.begin(),cell.end());
    if (!UC.is_default() and ( //if the data.unitcell is not set and no reflections
        extent[0] > coilx*cell[0] or
        extent[1] > coilx*cell[1] or
        extent[2] > coilx*cell[2]))
    {
      out::stream where = out::SUMMARY;
      logBlank(where);
      logTab(where,"Model Extent: " + dvtos(extent,1));
      logTab(where,"Cell Extent:  " + dtos(coilx,2) + "*(" + dvtos(cell,1) + ")=" + dvtos(coilx*cell,1));
      logWarning("Model is extremely extended");
      if (models.is_random_coil(0.90))
      {
        logWarning("Model appears to be random coil with high per-residue asa");
      }
      if (barbwire_test)
      {
        throw Error(err::INPUT,"Model is extremely extended, larger than target unit cell, and likely alphafold barbwire. To remove this error turn off barbwire test.");
      }
    }
    logTab(where,"Barbwire:  " + btos(false));
    }}

    {{
    out::stream where = out::LOGFILE;
    auto& serial = models.SERIAL;
    logUnderLine(where,"Model Vrms Estimation");
    //vrms always comes from the input
    {
      double vrms = models.is_atom() ?
        input.value__flt_number.at(".phasertng.model.single_atom.rmsd.").value_or_default():
        input.value__flt_number.at(".phasertng.model.vrms_estimate.").value_or_default();
      sv_double VRMS(1,vrms);
      PHASER_ASSERT(VRMS.size() > 0);
      serial.vrms_input.resize(1,VRMS[0]);
      serial.vrms_lower.resize(1,-999); //flag for not set
      serial.vrms_upper.resize(1,-999); //flag for not set
      serial.vrms_start.resize(1,-999); //flag for not set
    }
    logTab(where,"VRMS input value: " + dvtos(serial.vrms_input,8,6));
    double best_rms;
    double worst_rms;
    {{
      double best_factor = 10;
      PHASER_ASSERT(best_factor > 0);
      best_rms = config_hires/best_factor;// Lowest rms that would make a difference given resolution
      double worst_factor = 6.;
      PHASER_ASSERT(worst_factor > 0);
      worst_rms = search->MEAN_RADIUS/worst_factor; // Highest rms that might be sensible for size of model
      logTab(where,"Default best/worst rms: " + dtos(best_rms) +"/" +dtos(worst_rms));
    }}
    double hires_factor(0.8);
    if (reflections_input and //otherwise the whole thing is bogus anyway
        worst_rms > config_hires*hires_factor)
    {
      logAdvisory("Worst rms adjusted by radius of the model, small models will have lower limits");
      worst_rms = std::min(worst_rms,config_hires*hires_factor);
    }
    //these values can be fudged because they are fudged anyway
    //the input value must be acceptable, the default ranges should not mess with them
    PHASER_ASSERT(serial.size() == 1);
    worst_rms = std::max(serial.vrms_input[0], worst_rms);
    best_rms = std::min(best_rms,serial.vrms_input[0]);
    serial.vrms_lower[0] = best_rms;
    serial.vrms_upper[0] = worst_rms;
    logTab(where,"Final best/worst rms: " + dtos(best_rms) +"/" +dtos(worst_rms));
    serial.vrms_start = serial.vrms_input;
    //setup all the internal parameters
    serial.setup_drms_first_and_limits();
    logTab(where,"VRMS input:     " + dvtos(serial.vrms_input,8,6));
    logTab(where,"VRMS lower/upper: " + dvtos(serial.vrms_lower,8,6) + " / " + dvtos(serial.vrms_upper,8,6));
    logTab(where,"DRMS lower/upper: " + dtos(serial.drms_lower(),8,6) + " / " + dtos(serial.drms_upper(),8,6));
    logBlank(where);
    search->DRMS_MIN = models.SERIAL.drms_lower();
    search->DRMS_MAX = models.SERIAL.drms_upper();
    search->VRMS_START = models.SERIAL.vrms_start;
    }}

    auto cell_scale = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default();
    double cellmin = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_min();
    double boxmin = input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").minval();

    double savereso(0);
    {{
    out::stream where = out::VERBOSE;
    UnitCell tiny_uc(search->EXTENT);
    //use the min to make sure the extrapolation is OK
    //smallest box has biggest resolution shift between indices
    //build includes minimum cell checks including min based on data resolution
    double minscale = boxmin*cellmin;
    tiny_uc.build_interpolation_unit_cell(minscale,config_hires);
    savereso = tiny_uc.resolution(config_hires,cell_scale); //uuu
    logTab(where,"Cell scale minimum: " + dtos(cellmin));
    logTab(where,"Box scale minimum: " + dtos(boxmin));
    logTab(where,"Configure resolution: " + dtos(config_hires));
    logTab(where,"Interpolation Minimum Unit Cell: " + dvtos(tiny_uc.GetBox(),2));
    logTab(where,"Interpolation Save resolution: " + dtos(savereso));
    }}

    //this is not written out
    ModelsFcalc& fcalcs = search->FCALCS;
    fcalcs.MODLID = search->identify();
    fcalcs.TIMESTAMP = TimeStamp();
    fcalcs.NMODELS = 1;

    models.RELATIVE_WILSON_B.resize(models.PDB.size());
    std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
    //if (reflections_input)
    {
    int    min_bins_for_wilson(6); //3 in the middle (linear) + 2 unreliable end points
    double lores_for_wilson(5); // //Wilson plot is nonsense at lores
    double min_hires_for_wilson(4);  //data must extend to at least this resolution
    sv_int wilson_bin; //the list of the bins used for wilson scaling
    out::stream where = out::VERBOSE;
    dvect3 extent = search->EXTENT; //this gets larger
    int w(1);
    do
    {
      logUnderLine(where,"Structure Factor Calculation for Wilson Scaling (loop #"+itos(w++)+")");
      logTab(where,"Extent: " + dvtos(extent));
      logTab(where,"Use Bulk: " + btos(input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default()));
      logTab(where,"Fsol: " + dtos(input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default()));
      logTab(where,"Bsol: " + dtos(input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()));
      double vboxscale = 2;
      UnitCell box(extent);
      auto str = box.build_variance_unit_cell(vboxscale,config_hires);
      logTabArray(where,str);
      //add padding to resolution for cell scaling
      //The structure factors below are calculated with the B-factors of the model
      // without the Data WilsonB subtracted from them,
      //they will be normalized to Ecalc, but the density calculation is affected by model b's
      double shannon = 3;
      //even for single atom, use fft
      fcalcs.p1_structure_factor_calculation(
          &models,
          box, //this is the extent that changes
          savereso,
          box.cctbxUC,
          shannon,
          NTHREADS,
          input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()
          );
      PHASER_ASSERT(fcalcs.FCALC.num_rows() == models.PDB.size());
      double f = fcalcs.setup_bins_with_statistically_weighted_bin_width(
          extent,
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_min(),
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_max(),
          input.value__int_number.at(".phasertng.bins.molecular_transforms.width.").value_or_default()
         );
      if (Suite::Extra())
      {
        out::stream where = out::TESTING;
        logTab(where,"Number of reflections " + itos(fcalcs.MILLER.size()));
        logTab(where,"Number of bins " + itos(fcalcs.BIN.numbins()));
        logTab(where,"Stored high resolution (accounting for cell scale) " + dtos(fcalcs.FCALC_HIRES));
        logTab(where,"Model Bin hires " + dtos(fcalcs.BIN.hires()));
        logTab(where,"Statistical weighting factor = " + dtos(f));
        logTabArray(where,fcalcs.BIN.logBin("Statistically weighted binning"));
      }
      fcalcs.initialize_bin(fcalcs.BIN); //paranoia check
      wilson_bin = fcalcs.BIN.wilson_bins(lores_for_wilson,config_hires);
      //for lookup into the bin array, for plotting
      if (wilson_bin.size() < min_bins_for_wilson) //should be defined by bins range limits also
      {
        logTab(where,"Not enough data for Wilson scaling");
        extent *= 1.1; //inflate the cell by 10% each time, will be small to start with
      }
      else
      {
        logTab(where,"Enough data for Wilson scaling");
      }
    }
    while (wilson_bin.size() < min_bins_for_wilson and
           (!reflections_input or REFLECTIONS->data_hires() <= min_hires_for_wilson));
    //if the resolution is low then just stick to the first init of wilson_bin

    if (reflections_input)
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Wilson Scaling");
    logTab(where,"Data Wilson B-factor: " + dtos(REFLECTIONS->WILSON_B_F)); //local copy for output
    if (REFLECTIONS->MAP)
    {
      logTab(where,"Map: Wilson Scaling not performed");
      logTab(where,"Wilson B-factors set to 0");
    }
    else if (models.is_atom() or
             input.value__boolean.at(".phasertng.molecular_transform.skip_bfactor_analysis.").value_or_default())
    {
      logTab(where,"Wilson Scaling not performed (by user request)");
      logTab(where,"Wilson B-factors set to 0");
    }
    else if (REFLECTIONS->data_hires() > min_hires_for_wilson)
    {
      logTab(where,"Data does not extend beyond " + dtos(min_hires_for_wilson,3) + " Angstroms");
      logTab(where,"Wilson Scaling not performed");
      logTab(where,"Wilson B-factors set to 0");
    }
    else if (REFLECTIONS->WILSON_K_F == 0)
    {
      logTab(where,"Data not scaled");
      logTab(where,"Wilson Scaling not performed");
      logTab(where,"Wilson B-factors set to 0");
    }
    else
    {
      math::Matrix<double> SigmaP = fcalcs.sigmap_with_binning(); //statistically weighted bins
      //Must be oversampled or scaling is totally unreliable!!! u_base screws it up
      //these should be similar to the reflection bins values, not ensemble bins
      logTab(where,"Number of calculated structure factors = " + itos(fcalcs.MILLER.size()));
      af_double SigmaN; af_int numinbin_data;
      std::tie(SigmaN,numinbin_data) = REFLECTIONS->sigman_with_binning(fcalcs.BIN);
      //scale the SigmaP and SigmaN so they are on an absolute scale
      double KI = REFLECTIONS->SG.SYMFAC*fn::pow2(REFLECTIONS->WILSON_K_F);
   // KI *= REFLECTIONS->get_ztotalscat();
      sv_int models_scattering = models.scattering();
      for (int s = 0; s < fcalcs.BIN.numbins(); s++)
      {
        SigmaN[s] /= KI;
        for (int m = 0; m < models.PDB.size(); m++)
        { //scale this up as though it was fp=1
          double JI = models_scattering[m];
          SigmaP(m,s) = SigmaP(m,s)/JI;
        }
      }
      //arrays below will only contain the values for the linear regression
      af_double ssqr_bin;
      for (auto s : wilson_bin)
      {
        ssqr_bin.push_back(1.0/fn::pow2(fcalcs.BIN.MidRes(s)));
      }
      for (int m = 0; m < models.PDB.size(); m++)
      { //the B-factor is refined wrt the B-factor of the search
        af_double ratio; //between 5 and config_hires
        for (auto s : wilson_bin)
        {
          ratio.push_back(SigmaN[s]/SigmaP(m,s));
        }
        double WilsonB_intensity = -4*wilson_slope_and_intercept(ratio,ssqr_bin).first;
        models.RELATIVE_WILSON_B[m] = WilsonB_intensity/2.0; //local copy for output
        logTab(where,"Relative Wilson B-factor " + dtos(models.RELATIVE_WILSON_B[m]));

        Loggraph loggraph;
        loggraph.title = "Wilson Plot for Model " + search->identify().str() + " #" + itos(m+1) + " B=" + dtos(models.RELATIVE_WILSON_B[m],7,2);
        loggraph.scatter = false;
        loggraph.data_labels = "1/d^2 d #refl-data #refl-model Mean-Icalc Mean-Iobs Ratio";
        for (int ibin = 0; ibin < wilson_bin.size(); ibin++) //ratio length less than bin limit
        {
          int s = wilson_bin[ibin]; //lookup into bin array
          double midres = 1.0/std::sqrt(ssqr_bin[ibin]);
          loggraph.data_text +=
              dtos(1.0/fn::pow2(midres),5,4) + " " +
              dtos(midres,5,4) + " " +
              itos(numinbin_data[s],15,true,false) + " " +
              itos(fcalcs.BIN.NUMINBIN[s],15,true,false) + " " +
              etos(SigmaP(m,s),15,8) + " " +
              etos(SigmaN[s],15,8)+ " " +
              etos(ratio[ibin],15,8) + " " +
              "\n";
        }
        loggraph.graph.resize(4);
        loggraph.graph[0] = "SigmaN vs Resolution:AUTO:1,6";
        loggraph.graph[1] = "SigmaP vs Resolution:AUTO:1,5";
        loggraph.graph[2] = "Ratio SigmaP/SigmaN vs Resolution:AUTO:1,7";
        loggraph.graph[3] = "Number Reflections vs Resolution:AUTO:1,3,4";
        logGraph(where,loggraph);
      }
      bool damped = (models.bfactors_damped_after_relative_wilson_b());
      if (damped)
      {
        logWarning("After Wilson scaling model had sharpened Bfactors which will be damped on output");
      }
    }
    }}
    }

    phaser::ncs::PointGroup pg;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Model Point Group");
    if (!models.is_atom() and
        input.value__boolean.at(".phasertng.point_group.calculate_from_coordinates.").value_or_default())
    {
      SpaceGroup SG("P 1");
      UnitCell UC; //default
      Monostructure refined;
      models.get_best_pdb_cards(refined.REFLID,refined.MODLID,refined.TIMESTAMP,refined.PDB);
      auto pdb_file_content = refined.get_pdb_cards();
      const char* const delim = "\n";
      std::ostringstream imploded;
      std::copy(pdb_file_content.begin(), pdb_file_content.end(),
           std::ostream_iterator<std::string>(imploded, delim));
      std::string source_info = models.fstr();
      iotbx::pdb::input ensemble_input(
        source_info.c_str(),
        scitbx::misc::split_lines(
          imploded.str().c_str(),
          imploded.str().size(),
          false,
          true
          ).const_ref()
        ); //throws UNHANDLED ERROR
      phaser::ncs::EnsembleSymmetry ensemble_symmetry;
      ensemble_symmetry.set_min_sequence_coverage(
        input.value__percent.at(".phasertng.point_group.coverage.").value_or_default_fraction());
      ensemble_symmetry.set_min_sequence_identity(
        input.value__percent.at(".phasertng.point_group.identity.").value_or_default_fraction());
      ensemble_symmetry.set_max_rmsd(
        input.value__flt_number.at(".phasertng.point_group.rmsd.").value_or_default());
      ensemble_symmetry.set_angular_tolerance(
        input.value__flt_number.at(".phasertng.point_group.angular_tolerance.").value_or_default());
      ensemble_symmetry.set_spatial_tolerance(
        input.value__flt_number.at(".phasertng.point_group.spatial_tolerance.").value_or_default());
      ensemble_symmetry.set_root(ensemble_input.construct_hierarchy());
      pg = ensemble_symmetry.get_point_group();
    }
    else
    {
      dmat33 rot(1,0,0,0,1,0,0,0,1);
      dvect3 tra(0,0,0);
      double angular(0),spatial(0);
      pg = phaser::ncs::PointGroup({phaser::ncs::NCSOperator(rot,tra)},angular,spatial);
    }
    // SYMM_EULER = pg.get_euler_list();
    //int MULT = pg.get_orth_list().size(); //multiplicity
    search->POINT_GROUP_SYMBOL = pg.hermann_mauguin_symbol();
    logTab(where,"Point Group: " + pg.hermann_mauguin_symbol());
    // exclude default case because default is 1 and eulers=000 already loaded in Entry
    for (int i = 1; i < pg.multiplicity(); i++)
    {
      PHASER_ASSERT(pg.get_euler(0) == dvect3(0,0,0)); //paranoia
      logTab(where,"Point Group Euler Angles: " + dvtos(pg.get_euler(i),7,1));
      search->POINT_GROUP_EULER.push_back(pg.get_euler(i));
    }
    }}

    Trace& trace = search->TRACE;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("trace"))
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Model Trace");
      //trace filename not set is caught below
      trace.SetFileSystem(input.value__path.at(".phasertng.trace.filename.").value_or_default()); //this is the read pdb, different from write below
      if (models.is_atom())
      {
        logTab(where,"Trace is single atom");
        //trace.SERIAL = models.SERIAL;
        trace.SAMPLING_DISTANCE = 1.5;
        trace.PDB.push_back(models.PDB[0][0]);
        trace.MODLID = search->identify();
        trace.REFLID = reflid;
        trace.TIMESTAMP = TimeStamp();
      }
      else if (models.PDB.size() > 0 and !trace.filesystem_is_set())
      {
        logTab(where,"Trace from coordinates in conserved core of ensemble");
        logTab(where,"Number of atoms per model in ensemble: " + itos(models.PDB[models.mtrace()].size()));
        models.conserved_trace(trace.PDB,trace.SAMPLING_DISTANCE);
        logTab(where,"Trace number of conserved atoms: " + itos(trace.size()));
      }
      else if (!trace.filesystem_is_set())
      {
        logTab(where,"Number of model atoms: " + itos(models.PDB[models.mtrace()].size()));
        models.conserved_trace(trace.PDB,trace.SAMPLING_DISTANCE);
      }
      else
      {
        logTab(where,"Trace from coordinates: " + trace.fstr());
        trace.read_from_disk();
        if (trace.has_read_errors())
          throw Error(err::INPUT,trace.read_errors());
        //AJM TODO fix below so that not hardwired, somehow from trace
        trace.SAMPLING_DISTANCE = 1.5; //or other value if not from coordinates!!
        logTab(where,"Trace number of read atoms: " + itos(trace.size()));
      }
      trace.MODLID = search->identify();
      trace.REFLID = reflid;
      trace.TIMESTAMP = TimeStamp();
      trace.CENTRE = search->CENTRE;
      trace.MAX_RADIUS = models.statistics.MAX_RADIUS;
      PHASER_ASSERT(trace.size());

      sv_string tmp = input.scope_keywords("trace hexgrid");
      for (auto item : tmp)
        logTab(where,item);

      if (Suite::Extra()) //must wrap in WriteFiles so that dir created
      {
        trace.SetFileSystem( DataBase(),search->DOGTAG.FileName(".debug.conserved.trace.pdb"));
        logFileWritten(where,WriteFiles(),"Debug Trace",trace);
        if (WriteFiles()) trace.write_to_disk();
      }

      //now remove surface atoms regardless
      if (input.value__boolean.at(".phasertng.trace.trim_asa.").value_or_default())
      {
        Trace backup = trace;
        out::stream where = out::VERBOSE;
        logTab(where,"Remove surface atoms");
        trace.trim_asa();
        logTab(where,"Trace size: " + itos(trace.size()));
        logTab(where,"Remove incomplete sidechains");
        trace.trim_incomplete_sidechains(); //only if protein
        logTab(where,"Trace size: " + itos(trace.size()));
        if (!trace.size())
        {
          logTab(where,"Revert trace to unedited");
          trace = backup;
        }
        bool turn_pt(false); dvect3 pt(0,0,0);
        PHASER_ASSERT(trace.size());
        if (Suite::Extra())
        {
          trace.SetFileSystem(DataBase(),search->DOGTAG.FileName(".debug.asa.trace.pdb"));
          logFileWritten(where,WriteFiles(),"Debug Trace",trace);
          if (WriteFiles()) trace.write_to_disk();
        }
      }

      //store these for rapid access
      int io_hexgrid_ncyc = input.value__int_number.at(".phasertng.trace.hexgrid.ncyc.").value_or_default();
      int io_hexgrid_min = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_min();
      int io_hexgrid_max = input.value__int_paired.at(".phasertng.trace.hexgrid.number_of_points.").value_or_default_max();
      double io_hexgrid_min_distance = input.value__flt_number.at(".phasertng.trace.hexgrid.minimum_distance.").value_or_default();
      if (trace.is_atom())
      {
        trace.SAMPLING_DISTANCE = 1.5; //average atom radius
        logTab(where,"Trace size: " + itos(trace.size()));
      }
      else if (trace.size() <= io_hexgrid_max and
               trace.number_of_calphas() == 0)
      {
        logTab(where,"Trace is all atoms");
        trace.SAMPLING_DISTANCE = 1.5; //atomic radii sampling distance
        logTab(where,"Trace size: " + itos(trace.size()));
      }
      else if ((input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("optimal") and
                trace.size() <= io_hexgrid_max)
                or input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("all"))
      {
        logTab(where,"Trace is all atoms");
        trace.SAMPLING_DISTANCE = 1.5; //atomic radii sampling distance
        logTab(where,"Trace size: " + itos(trace.size()));
      }
      else if ((input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("optimal") and
                trace.number_of_calphas() <= io_hexgrid_max)
                or input.value__choice.at(".phasertng.trace.volume_sampling.").value_or_default_equals("calpha"))
      {
        logTab(where,"Trace is calpha atoms");
        trace.trim_to_calphas();
        trace.SAMPLING_DISTANCE = 3.8; //atomic radii sampling distance
        logTab(where,"Trace size: " + itos(trace.size()));
      }
      else
      {
        logTab(where,"Trace is hexagonal grid");
        Trace hexgrid;
        {{
        out::stream where = out::VERBOSE;
        double sampling = 0;
        double hexgrid_ntarget = (io_hexgrid_max + io_hexgrid_min)/2.0;
        if (input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").is_default())
        {
          //best estimate up front
          double volume;
          {
          //calculate the volume from the sequence, same as for the map
          Sequence seq(models.SELENOMETHIONINE);
          bool use_unknown(false);
          seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
          seq.initialize(true,true);
          volume = seq.volume();
          }
          double mapvolume = volume*0.74; //Kepler conjecture packing spheres
          double voxel = mapvolume/hexgrid_ntarget;
          sampling = std::pow(voxel/(4./3.*scitbx::constants::pi),1./3.);
          sampling *= 2; //diameter, between hexagonal points
          sampling = std::max(io_hexgrid_min_distance,sampling);
        }
        else
        {
          sampling = input.value__flt_number.at(".phasertng.trace.hexgrid.distance.").value_or_default();
          io_hexgrid_ncyc = 0; //terminate at end of first loop
        }

        int ncyc(0);
        std::map<double,int> samp_results; //sampling/number
        samp_results[search->MEAN_RADIUS] = 1; //a fake limit for upper bound
        hexgrid.MODLID = search->identify();
        hexgrid.REFLID = reflid;
        hexgrid.TIMESTAMP = TimeStamp();
        hexgrid.CENTRE = search->CENTRE;
        hexgrid.MAX_RADIUS = models.statistics.MAX_RADIUS;
        logTab(where,"Target =" + dtos(hexgrid_ntarget));
        do
        {
          logTab(where,"Sampling #" + itos(ncyc+1) + ": " + dtos(sampling) + " Angstroms");
          hexgrid.build_hexgrid_box(sampling,models.statistics.MINBOX,models.statistics.MAXBOX);
          std::string cc = ntos(ncyc+1,99);
          PHASER_ASSERT(hexgrid.size());
          if (Suite::Extra()) //write here so that asa is in coordinates
          {
            std::string cc = ntos(ncyc+1,99);
            hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.raw.pdb"));
            logFileWritten(where,WriteFiles(),"Debug Hexgrid",hexgrid);
            if (WriteFiles()) hexgrid.write_to_disk();
          }
          //trace consists of (conserved) atoms, so use vanderwaals distances
          table::vanderwaals_radii vdw(0);
          sv_double other_radii(trace.PDB.size(),0);
          for (int a = 0; a < trace.PDB.size(); a++)
            other_radii[a] = vdw.get(trace.PDB[a].get_element());
          hexgrid.select_overlapping(trace,other_radii);
          if (Suite::Extra()) //write here so that asa is in coordinates
          {
            std::string cc = ntos(ncyc+1,99);
            hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.clash.pdb"));
            logFileWritten(where,WriteFiles(),"Debug Hexgrid",hexgrid);
            if (WriteFiles()) hexgrid.write_to_disk();
          }
          int cycles = hexgrid.trim_surface();
          logTab(out::TESTING,"Number of surface trim cycles = "+ itos(cycles));
          if (Suite::Extra()) //write here so that asa is in coordinates
          {
            hexgrid.SetFileSystem(DataBase(),search->DOGTAG.FileName(cc+".debug.hexgrid.buried.pdb"));
            logFileWritten(where,WriteFiles(),"Debug Hexgrid",hexgrid);
            if (WriteFiles()) hexgrid.write_to_disk();
          }
          double number_of_hexgrid_points = hexgrid.size();
          logTab(where,"Trace length=" + itos(hexgrid.size()));
          samp_results[sampling] = number_of_hexgrid_points;
         // double estimated_volume = fn::pow3(sampling)*N*scitbx::constants::pi*(4./(3.*8.));
          if (hexgrid.size() > io_hexgrid_max || hexgrid.size() < io_hexgrid_min)
          {
            //adjustment best guess
            //if number_of_hexgrid_points > hexgrid_ntarget (too finely sampled) then increase sampling by this factor
            logTab(where,"Ratio current/target length=" + dtos(number_of_hexgrid_points/hexgrid_ntarget));
            double factor = std::pow(number_of_hexgrid_points/hexgrid_ntarget,1./3.);
            if (number_of_hexgrid_points > io_hexgrid_max)
            {
              logTab(where,"Too large!");
              sampling *= factor; //widen the spacing
              logTab(where,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
              for (auto item : samp_results) //sorted on sampling
              {
                //test in order of lowest to highest sampling
                //first one that is larger than samp and has given too many points
                logTab(where,"Already tried " + dtos(item.first) + "->" + itos(item.second));
                if (sampling < item.first and item.second > io_hexgrid_max) //already know it is too far
                {
                  logTab(where,"But " + dtos(sampling) + "<" + dtos(item.first));
                  logTab(where,"And " + itos(item.second) + ">" + itos(io_hexgrid_max));
                  sampling = (item.first+sampling)/2.0;
                  logTab(where,"Sampling reset to half way between two =" + dtos(sampling));
                  break; //regardless, we only need to look at the first one with bigger sampling
                }
              }
            }
            else if (number_of_hexgrid_points < io_hexgrid_min)
            {
              logTab(where,"Too small!");
              if (sampling < io_hexgrid_min_distance + DEF_PPT) //tolerance for safety
              {
                ncyc = io_hexgrid_ncyc; //to force while to fail
                logTab(where,"Sampling aborted, at minimum");
                //possibly sent here from condition below
              }
              else if (sampling*factor > io_hexgrid_min_distance)
              {
                sampling *= factor;
                logTab(where,"Sampling multiplied by " + dtos(factor) + "=" + dtos(sampling));
                for (auto iter = samp_results.rbegin(); iter != samp_results.rend(); ++iter)
                {
                  //test in order of highest to lowest sampling
                  //first one that that has given too many points
                  logTab(where,"Already tried " + dtos(iter->first) + "->" + itos(iter->second));
                  if (sampling > iter->first and iter->second < io_hexgrid_min)
                  {
                    logTab(where,"But " + dtos(sampling) + ">" + dtos(iter->first));
                    logTab(where,"And " + itos(iter->second) + "<" + itos(io_hexgrid_max));
                    sampling = (iter->first+sampling)/2.0;
                    logTab(where,"Sampling reset to half way between two =" + dtos(sampling));
                    PHASER_ASSERT(sampling > io_hexgrid_min_distance);
                    break; //regardless
                  }
                }
              }
              else
              {
                sampling = io_hexgrid_min_distance;
                logTab(where,"Sampling set to minimum, just generate trace");
              }
            }
          }
          if (samp_results.count(sampling))
            ncyc = io_hexgrid_ncyc; //abort if we have tested this already
          logBlank(where);
          ncyc++;
        }
        while ((hexgrid.size() > io_hexgrid_max || hexgrid.size() < io_hexgrid_min) and
                ncyc < io_hexgrid_ncyc); //infinite loop trap

        }}
        logTab(where,"Number of points in hexagonal grid: " + itos(hexgrid.size()));
        if (hexgrid.size() < trace.size())
        {
          trace = hexgrid;
          trace.renumber();//sort out the numbering at the end
        }
        else
        {
          logTab(where,"Hexagonal grid has more points than atoms");
          logTab(where,"Use trace without hexagonal grid");
        }
        logTab(where,"Number of points in trace: " + itos(trace.size()));
      }
      trace.POINT_GROUP_EULER.clear();
      for (int i = 0; i < pg.multiplicity(); i++)
        trace.POINT_GROUP_EULER.push_back(pg.get_euler(i));
    }
    trace.CENTRE = models.statistics.CENTRE;
    trace.MAX_RADIUS = models.statistics.MAX_RADIUS;

    Monostructure& monostructure = search->MONOSTRUCTURE;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("monostructure"))
    {
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Model Monostructure");
      logTab(where,"Copy model to single representative structure");
      monostructure.MODLID = search->identify();
      monostructure.REFLID = reflid;
      monostructure.TIMESTAMP = TimeStamp();
      int ntrace; double start;
      std::tie(start,ntrace) = models.SERIAL.get_lowest();
/*
      monostructure.SERIAL.resize(1);
      monostructure.SERIAL.vrms_input[0] = models.SERIAL.vrms_input[ntrace];
      monostructure.SERIAL.vrms_start[0] = start;
      monostructure.SERIAL.vrms_lower[0] = models.SERIAL.vrms_lower[ntrace];
      monostructure.SERIAL.vrms_upper[0] = models.SERIAL.vrms_upper[ntrace];
*/
      //make sure identifiers are set externally
      for (int a = 0; a < models.PDB.at(ntrace).size(); a++)
      {
        if (models.PDB.at(ntrace).at(a).O != 0.0)
        {
          monostructure.PDB.push_back(models.PDB.at(ntrace).at(a));
        }
      }
      monostructure.apply_wilson_bfactor(REFLECTIONS->WILSON_B_F);
      PHASER_ASSERT(monostructure.PDB.size() > 0);
    }

    //Ensemble_weight_analysis
    //build name required for sequence
    std::set<entry::data> preparation;
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("decomposition"))
      preparation.insert(entry::DECOMPOSITION_MTZ);
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("interpolation"))
      preparation.insert(entry::INTERPOLATION_MTZ);
    Variance& variance = search->VARIANCE;
    //if (reflections_input)
    {
      if (preparation.size() or
        input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("variance"))
      {
    {{
    logTab(out::VERBOSE,"Copy fcalcs to variance");
    variance.SetModelsFcalc(search->FCALCS); //deep copy constructor
    logTab(out::VERBOSE,"Calculate SigmaP");
    math::Matrix<double> SigmaP = fcalcs.sigmap_with_binning(); //statistically weighted bins
    phaser_assert(fcalcs.BIN.numbins());
    variance.SIGMAA.resize(fcalcs.BIN.numbins());
    variance.MODELWT.resize(fcalcs.BIN.numbins(),1);
    variance.REFLID = reflid;
    logTab(out::VERBOSE,"Apply SigmaP");
    variance.apply_sigmap(SigmaP); //convert to evalues
    search->FCALCS = ModelsFcalc(); //clear the memory, in_memory=false because MILLER is empty
    }}

    {{
    logTab(out::VERBOSE,"Setup SigmaA terms");
    double fsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
    double bsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
    double minb = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();
    FourParameterSigmaA solTerm(models.SERIAL.vrms_input[0],fsol,bsol,minb);
    for (int ibin = 0; ibin < variance.BIN.numbins(); ibin++)
    {
      double midSsqr = variance.BIN.MidSsqr(ibin);
      variance.MODELWT(ibin,0) = 1;
      variance.SIGMAA[ibin] = solTerm.get(midSsqr);
    }
    }}

    search->LORES = variance.available_lores();
      }
    }

    //if (reflections_input)
    {
    bool boxwarn(false),memwarn(false);
    logEllipsisOpen(out::LOGFILE,"Calculating structure factors");
    for (auto loop : preparation)
    {
      out::stream where = out::VERBOSE;
      loop == entry::DECOMPOSITION_MTZ ?
        logUnderLine(where,"Model Structure Factors for Decomposition"):
        logUnderLine(where,"Model Structure Factors for Interpolation");
      Ensemble ensemble;
      ensemble.REFLID = reflid;
      ensemble.MODLID = search->identify();
      ensemble.TIMESTAMP = TimeStamp();
      ensemble.UC = UnitCell(search->EXTENT);

      std::pair<double,double> cell_scale = {1.,1.};
      if (loop == entry::INTERPOLATION_MTZ)
        cell_scale = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default();

      if (loop == entry::DECOMPOSITION_MTZ)
      {
        double rf = input.value__flt_number.at(".phasertng.molecular_transform.decomposition_radius_factor.").value_or_default();
        ensemble.SPHERE = search->sphereOuter(rf,config_hires);
        auto text = ensemble.UC.build_decomposition_unit_cell(ensemble.SPHERE,config_hires);
        logTabArray(where,text);
        logTab(where,"Decomposition Unit Cell: " + dvtos(ensemble.UC.GetBox(),2));
      }
      if (loop == entry::INTERPOLATION_MTZ)
      {
        double boxscale = input.value__flt_number.at(".phasertng.molecular_transform.boxscale.").value_or_default();
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
        boxscale = 4;
#endif
        //do not include the cell scale here, buffer is in the resolution
        auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
        logTabArray(where,text);
        //if the boxscale makes the cell really huge then reduce the boxscale
        double excessively_large_cell(600.); //6*100
        auto initboxscale = boxscale;
        while (ensemble.UC.maximum_cell_dimension() > excessively_large_cell and boxscale > 4)
        {
          out::stream extra = out::VERBOSE;
          logTab(extra,"Excessively large cell = " + dtos(excessively_large_cell,0) + "A");
          logTab(extra,"Maximum cell dimension = " + dtos(ensemble.UC.maximum_cell_dimension(),0) + "A");
          boxscale--;
          logTab(where,"Boxscale reduced due to excessively large cell: " + dtos(boxscale,0));
          ensemble.UC = UnitCell(search->EXTENT); //reset
          auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
          logTabArray(where,text);
        }
        boxwarn = (initboxscale != boxscale);
        while (boxscale >= boxmin)
        {
          try {
            cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
            cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
            fft::real_to_complex_3d rfft(SgInfoP1,ensemble.UC.cctbxUC);
            rfft.shannon(3);
            //we could reduce the savereso here if boxscale > minboxscale
            //but we will keep the original worst-case savereso for == minboxscale
            rfft.calculate_gridding(savereso,ensemble.UC.cctbxUC);
            logTab(where,"Interpolation Boxscale: " + dtos(boxscale));
            logTab(where,"Interpolation Boxscale FFT Unit Cell: " + dvtos(ensemble.UC.GetBox(),2));
            break; //success
          }
          catch(...)
          {
            ensemble.UC = UnitCell(search->EXTENT);
            boxscale -= 1;
            memwarn = true;
            logTab(where,"Boxscale reduced due to memory exhaustion: " + dtos(boxscale,0));
            auto text = ensemble.UC.build_interpolation_unit_cell(boxscale,config_hires);
            logTabArray(where,text);
          }
        }
      }
     // dvect3 max_box = double(CELL_SCALE_MAX)*loop.second;
     // if (getenv("PHASER_STUDY_PARAMS_CELL") != NULL) //because studyparams can go out of range!!
     //    max_box = 4.0*max_box; //for CELL REFINEMENT

      Variance sfcalc; //decomposition or interpolation
      //The structure factors below are calculated with the WilsonB subtracted
      //they will be normalized anyway, but the density calculation is affected
      double shannon = 3;
      double thisreso = (loop == entry::INTERPOLATION_MTZ) ? savereso : config_hires;
      sfcalc.p1_structure_factor_calculation(
          &models,
          ensemble.UC,
          thisreso,
          ensemble.UC.cctbxUC,
          shannon,
          NTHREADS,
          input.value__boolean.at(".phasertng.molecular_transform.bulk_solvent.use.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.molecular_transform.bulk_solvent.bsol.").value_or_default()
          );
      logTab(where,"Setup Bins");
      sfcalc.setup_bins_with_statistically_weighted_bin_width(
          ensemble.UC.GetBox(),
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_min(),
          input.value__int_paired.at(".phasertng.bins.molecular_transforms.range.").value_or_default_max(),
          input.value__int_number.at(".phasertng.bins.molecular_transforms.width.").value_or_default());
      if (Suite::Extra())
      {
        out::stream where = out::TESTING;
        logTab(where,"Number of reflections " + itos(sfcalc.MILLER.size()));
        logTab(where,"Number of bins " + itos(sfcalc.BIN.numbins()));
        logTab(where,"Stored high resolution (accounting for cell scale) " + dtos(sfcalc.FCALC_HIRES));
        logTab(where,"Model Bin hires " + dtos(sfcalc.BIN.hires()));
        logTabArray(where,sfcalc.BIN.logBin("sfcalc"));
        PHASER_ASSERT(variance.in_memory());
        logTab(where,"Variance Bin hires " + dtos(variance.BIN.hires()));
        logTabArray(where,variance.BIN.logBin("variance"));
        double posdiff(sfcalc.BIN.hires()-variance.BIN.hires());
        logTab(where,"Hires Binning Difference (calc-var)=" + std::to_string(posdiff) + std::string((posdiff>=0) ? " OK":" BAD"));
      }
      PHASER_ASSERT(variance.BIN.hires() <= sfcalc.BIN.hires()+DEF_PPM);

      math::Matrix<double> SigmaP = sfcalc.sigmap_with_binning();
      sfcalc.apply_sigmap(SigmaP);

      //-------------------------------------------------------------------------------------
      // --- OK, finally happy with the weights
      //-------------------------------------------------------------------------------------
      ensemble.MILLER = sfcalc.MILLER; //the binning is for the siga
      ensemble.ECALC.resize(sfcalc.MILLER.size());
      ensemble.initialize_bin(variance.BIN); //the binning is for the siga
      for (int r = 0; r < sfcalc.MILLER.size(); r++)
      {
        int ibin = ensemble.BIN.RBIN[r]; //initialized above
        ensemble.addData(r,sfcalc.FCALC(0,r),variance.SIGMAA[ibin],ibin);
      }
      //now write the data as mtz file
      if (loop == entry::DECOMPOSITION_MTZ)
        search->DECOMPOSITION = ensemble;
      else if (loop == entry::INTERPOLATION_MTZ)
        search->INTERPOLATION.ParseEnsemble(ensemble); //Interpolation()
    }
    logEllipsisShut(out::LOGFILE);
    if (boxwarn)
      logAdvisory("Interpolation boxscale reduced because of excessively large cell");
    if (memwarn)
      logAdvisory("Interpolation boxscale reduced because of memory exhaustion");

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"Structure-factor Generation");
    logTab(where,"Decomposition unit cell = " + dvtos(search->DECOMPOSITION.UC.GetBox(),6,1));
    logTab(where,"Decomposition number of reflections = " + itos(search->DECOMPOSITION.nrefl()));
    logTab(where,"Interpolation unit cell = " + dvtos(search->INTERPOLATION.UC.GetBox(),6,1));
    logTab(where,"Interpolation number of reflections = " + itos(search->INTERPOLATION.nrefl()));
    logTab(where,"Variance unit cell = " + dvtos(search->VARIANCE.UC.GetBox(),6,1));
    logTab(where,"Variance number of reflections = " + itos(search->VARIANCE.nrefl()));
    }}
    }

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Model Generation");
    logTab(where,"Resolution: " + dtos(search->HIRES));
    logTab(where,snprintftos(
        "%6s %6s:%-15s  %6s %6s %6s %9s%-16s",
        "Radius","DRMS","[Range]","Model#","Rel-B","iVRMS","","VRMS:[Range]"));
    for (int m = 0; m < models.PDB.size(); m++)
    {
      pod::serial& serial = models.SERIAL;
      logTab(where,snprintftos(
         "%6s %6.4f:[% 6.4f/%6.4f] %6d  % 5.1f %6.3f %9.6f:[%6.3f/%6.3f]",
         m ? "" : dtos(search->MEAN_RADIUS,6,1).c_str(),
         serial.drms_first,serial.drms_range.first,serial.drms_range.second,
         m+1,
         models.RELATIVE_WILSON_B[m],
         serial.vrms_input[m],
         serial.vrms_start[m],serial.vrms_lower[m],serial.vrms_upper[m]));
    }
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("trace"))
      logTab(where,"Trace number of atoms = " + itos(search->TRACE.size()));
    if (input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("monostructure"))
      logTab(where,"Model number of atoms = " + itos(search->MONOSTRUCTURE.size()));
    logTabArray(out::SUMMARY,search->logEntry());
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Database Entries");

    //no write of FCALCS_MTZ
    if (fcalcs.in_memory())
    {
      fcalcs.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::FCALCS_MTZ));
      logFileWritten(out::LOGFILE,WriteData(),"Structure Factor",fcalcs);
      if (WriteData()) //dryrun
        fcalcs.write_to_disk();
    }
    else logTab(where,"Structure factors not in memory");

    if (models.in_memory())
    {
      PHASER_ASSERT(models.has_relative_wilson_bfactor());
      models.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MODELS_PDB));
      logFileWritten(out::LOGFILE,WriteData(),"Models",models);
      if (WriteData()) //dryrun
        models.write_to_disk();
    }
    else logTab(where,"Models not in memory");

    Monostructure& monostructure = search->MONOSTRUCTURE;
    if (monostructure.in_memory())
    {
      monostructure.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::MONOSTRUCTURE_PDB));
      logFileWritten(out::LOGFILE,WriteData(),"Monostructure",monostructure);
      if (WriteData()) monostructure.write_to_disk();
    }
    else logTab(where,"Monostructure not in memory");

    Trace& trace = search->TRACE;
    if (trace.in_memory())
    {
      //this is the place where the final filename is set
      trace.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::TRACE_PDB));
      logFileWritten(out::LOGFILE,WriteData(),"Trace",trace);
      if (WriteData()) trace.write_to_disk();
    }
    else logTab(where,"Trace not in memory");

    Interpolation& interpolation = search->INTERPOLATION;
    if (interpolation.in_memory())
    {
      interpolation.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::INTERPOLATION_MTZ));
      logFileWritten(out::LOGFILE,WriteData(),"Interpolation",interpolation);
      if (WriteData()) interpolation.write_to_disk();
    }
    else logTab(where,"Interpolation not in memory");

    Ensemble& decomposition = search->DECOMPOSITION;
    if (decomposition.in_memory())
    {
      decomposition.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::DECOMPOSITION_MTZ));
      logFileWritten(out::LOGFILE,WriteData(),"Decomposition",decomposition);
      if (WriteData()) decomposition.write_to_disk();
    }
    else logTab(where,"Decomposition not in memory");

    if (variance.in_memory())
    { //modify database entry from ECA, with weights
      variance.SetFileSystem(DataBase(),search->DOGTAG.FileEntry(entry::VARIANCE_MTZ));
      //this is a hack for the dryrun,
      //if variance is specified then write anyway
   // bool write_files = input.value__choice.at(".phasertng.molecular_transform.preparation.").value_or_default_equals("variance") or WriteData();
      bool wf = WriteData();
      logFileWritten(out::LOGFILE,wf,"Variance",variance);
      if (wf) variance.write_to_disk();
    }
    else logTab(where,"Variance not in memory");

    std::string molecular_transform_coordinates("");
    if (input.value__boolean.at(".phasertng.molecular_transform.write_coordinates.").value_or_default())
    {
      Models PRPT = models; //deep copy so no inplace modification
//activate this part of the code to write out a model at origin via PR and PT
//so that it can be input into phaser for the results of phasertng and phaser to be compared
      logUnderLine(out::LOGFILE,"Models for molecular transform");
      PRPT.SetFileSystem(DataBase(),search->DOGTAG.FileName(".mt.pdb"));
      PRPT.move_and_align_to_principal_axes(
          search->PRINCIPAL_ORIENTATION,
          search->PRINCIPAL_TRANSLATION);
      PRPT.INPUT_CELL = UnitCell();
      logFileWritten(out::LOGFILE,WriteFiles(),"Molecular Transform Model",PRPT);
      if (WriteFiles()) PRPT.write_to_disk();
    }
    }}

    {{
    logTab(out::LOGFILE,"Append Dag with model");
    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      //add the models info to each dag entry
      auto& node = DAGDB.NODES[i];
      dag::Hold hold;
      hold.ENTRY = search; //copy pointer
      hold.IDENTIFIER = search->identify();
      node.HOLD.push_back(hold);
      auto modlid = search->identify();
      node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0.); //create
      node.VRMS_VALUE[modlid] = dag::node_t(modlid,search->VRMS_START[0]); //create
      node.CELL_SCALE[modlid] = dag::node_t(modlid,1.); //create
    }
    DAGDB.reset_entry_pointers(DataBase());
    }}

    //set all the output parameters here
    input.value__uuid_number.at(".phasertng.search.id.").set_value(modlid.identifier());
    input.value__string.at(".phasertng.search.tag.").set_value(modlid.tag());

    {{
    FileSystem filesystem;
    filesystem.SetFileSystem(DataBase(),search->identify().FileDataBase(other::ENTRY_CARDS));
    //this puts the file in the database directory where it can be read from the modlid alone
    //write_cards true required for bones
    //WriteFiles to sync with other filew
    bool wf = WriteCards();
    logFileWritten(out::LOGFILE,wf,"Entry cards",filesystem);
    auto dagcards = DAGDB.entry_unparse_card(DAGDB.ENTRIES[search->identify()]);
    if (wf) filesystem.write(dagcards);
    //the files are read if the entry is not in memory, with the filename reconstructed from the modlid
    input.generic_string = dagcards; //overwriting anything on the input
    }}
  }

} //phasertng
