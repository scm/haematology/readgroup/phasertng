//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/choice.h>
#include <phasertng/main/Error.h>
#include <algorithm>
#include <cctype> //Windows toupper

namespace phasertng {
namespace type {

  choice::choice(
      std::string parameter,
      std::string possibilities,
      std::string choice_default
  ) :
    optional(parameter)
  {
    if (!parameter.size()) return; //default constructor
    //phaser_assert(possibilities.size());
    choices = to_words(possibilities);
    for (auto item:choices) //comparison all done in terms of uppercase, but not stored
      CHOICES.push_back(choice2upper(item));
    if (!choice_default.size()) return;
    dephalt = to_words(choice_default);
    select = to_words(choice_default);
/* compiler warning message
    bool found_choice_default(true);
    for (int i = 0; i < dephalt.size(); i++)
      if (std::find(CHOICES.begin(),CHOICES.end(),choice2upper(dephalt[i])) == CHOICES.end())
        found_choice_default = false;
    phaser_assert(dephalt.size());
    phaser_assert(found_choice_default); //developer error!
*/
  }

  void
  choice::set_value(std::string selection)
  {
    user = true;
    if (!selection.size())
      return; //throw Error(err::DEVELOPER,"no selection " + parameter());
    select = to_words(selection);
    std::vector<bool> found_select(select.size(),false);
    for (int i = 0; i < select.size(); i++)
    { //comparison all done in terms of uppercase, but not stored
      if (std::find(CHOICES.begin(),CHOICES.end(),choice2upper(select[i])) != CHOICES.end())
        found_select[i] = true;
    }
    //phaser_assert(select.size());
    for (int i = 0; i < select.size(); i++)
    {
      if (!found_select[i])
      {
        std::string message = unscoped() + " <choice unknown> ";
        for (auto istr : select) message += istr + " ";
        message += "(";
        for (auto item : choices) message += item + " ";
        message.pop_back();
        throw Error(err::INPUT,message + ")");
      }
    }
  }

  void
  choice::set_value(const sv_string& vcards,std::string& selection)
  {
    paranoia_check(vcards);
    set_value(selection);
  }

  bool
  choice::is_default() const
  {
    std::set<std::string> dephalt_set(dephalt.begin(),dephalt.end());
    std::set<std::string> select_set(select.begin(),select.end());
    return dephalt_set == select_set;
  }

  void
  choice::set_default()
  {
    user = false;
    select = dephalt;
  }

  std::string
  choice::to_python()
  { return unparse_parameter(false); }

  void
  choice::from_python(std::string str)
  { set_value(str); }

  //use set so that order is not important
  std::set<std::string>
  choice::value_or_default_multi_set() const
  {
    std::set<std::string> dephalt_set(dephalt.begin(),dephalt.end());
    std::set<std::string> select_set(select.begin(),select.end());
    return is_default() ? dephalt_set : select_set;
  }

  sv_string
  choice::value_or_default_multi() const
  {
    return is_default() ? dephalt:select;
  }

  std::string
  choice::value_or_default_upper() const
  { return choice2upper(value_or_default()); }

  bool
  choice::check_value(std::string pick) const
  {
    for (auto value : choices)
      if (value == pick) return true;
    return false;
  }

  std::string
  choice::value_or_default_multi_cat() const
  {
    std::string result;
    sv_string rvec = is_default() ? dephalt : select;
    for (auto const& s : rvec) { result += s; } //concatenation without spaces; check not None
    return result;
  }

  //assumes single value, not a set
  std::string
  choice::value_or_default() const
  {
    std::string result;
    sv_string rvec = is_default() ? dephalt : select;
    for (auto const& s : rvec) { result += s; }
    //phaser_assert(rvec.size() == 1 or !rvec.size()); //None or single value
    return result;
  }

  bool
  choice::value_or_default_equals(const std::string tester) const
  {
    sv_string testing = to_words(tester);
    std::set<std::string> multi = value_or_default_multi_set();
    std::set<std::string> testset = std::set<std::string>(testing.begin(),testing.end());
    if (testset.size() == 1) //for single value in multi, check if the single value is present
      return (multi.count(*testset.begin()) == 1);
    return multi == testset;
  }

  std::string
  choice::unparse_parameter(bool phil) const
  {
    std::string item = "";
    char link = phil ? '+' : ' ';
    for (int c = 0; c < select.size(); c++)
      if (std::find(choices.begin(),choices.end(),select[c]) != choices.end())
        item += select[c] + link; //format for multi-choice is x+y+z in phil, x y z for cards
    if (phil and item.size()) //remove last +
      item.pop_back();
    if (!item.size())
      item = None();
    return item;
  }

  std::string
  choice::show_default(bool phil) const
  {
    std::string item = "";
    char link = phil ? '+' : ' ';
    for (int c = 0; c < dephalt.size(); c++)
      if (std::find(choices.begin(),choices.end(),dephalt[c]) != choices.end())
        item += dephalt[c] + link; //format for multi-choice is x+y+z in phil, x y z for cards
    if (phil and item.size()) //remove last +
      item.pop_back();
    if (!item.size())
      item = None();
    return item;
/* this gives the starred list
    std::string item = "";
    std::set<std::string> def(dephalt.begin(),dephalt.end());
    for (int c = 0; c < choices.size(); c++)
    {
      if (def.find(choices[c]) != def.end())
        item += "*";
      item += choices[c];
      if (c < choices.size()-1) item += ' ';
    }
    return item;
*/
  }

  std::string
  choice::choice2upper(std::string s) const
  {
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
    return s;
  }

}}
