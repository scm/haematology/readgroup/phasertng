/*
The use of boost::preprocessor makes possible an elegant solution like the following:

Step 1: include the header file:

#include <phasertng/enum/EnumUtilities.h>

Step 2: declare the enumeration object with the following syntax:

MakeEnum( TestData,
         (x)
         (y)
         (z)
         );

Step 3: use your data:

Getting the number of elements:

std::cout << "Number of Elements: " << TestDataCount << std::endl;

Getting the associated string:

std::cout << "Value of " << TestData2String(x) << " is " << x << std::endl;
std::cout << "Value of " << TestData2String(y) << " is " << y << std::endl;
std::cout << "Value of " << TestData2String(z) << " is " << z << std::endl;

Getting the enum value from the associated string:

std::cout << "Value of x is " << TestData2Enum("x") << std::endl;
std::cout << "Value of y is " << TestData2Enum("y") << std::endl;
std::cout << "Value of z is " << TestData2Enum("z") << std::endl;

This looks clean and compact, with no extra files to include. The code I wrote within EnumUtilities.h is the following:
*/

#include <boost/preprocessor/seq/for_each.hpp>
#include <string>

#define FLTLY_MAKE_STRING(x) #x
#define MAKE_STRING(x) FLTLY_MAKE_STRING(x)
#define MACRO1(r, data, elem) elem,
#define MACRO1_STRING(r, data, elem)    case elem: return FLTLY_MAKE_STRING(elem);
#define MACRO1_ENUM(r, data, elem)      if (FLTLY_MAKE_STRING(elem) == eStrEl) return elem;

#ifndef Enum2StringError
#define Enum2StringError "Unknown enumerator value."
#endif

#define MakeEnum(eName, SEQ) \
    enum eName { BOOST_PP_SEQ_FOR_EACH(MACRO1, , SEQ) \
    last_##eName##_enum}; \
    const int eName##Count = BOOST_PP_SEQ_SIZE(SEQ); \
    static std::string eName##2String(const enum eName eel) \
    { \
        switch (eel) \
        { \
        BOOST_PP_SEQ_FOR_EACH(MACRO1_STRING, , SEQ) \
        default: return Enum2StringError; \
        }; \
    }; \
    static enum eName eName##2Enum(const std::string eStrEl) \
    { \
        BOOST_PP_SEQ_FOR_EACH(MACRO1_ENUM, , SEQ) \
        return (enum eName)0; \
    };
