import sys
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
import os, os.path

#these are internal parameters not interesting for the user
class runDEEP:

  def run_from_basetng_and_input(self,basetng):
    self.inner_deep(basetng=basetng)

  def inner_deep(self, basetng):
 #{{{
    DAGDB = basetng.DAGDATABASE
    assert(DAGDB.size())  #or else the hashing is not specific
    DAGDB.shift_tracker(str(basetng.hashing()),"deep")
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    root = os.path.join(basetng.DataBase(),subdir)
    wf = basetng.Write()
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(root):
      os.makedirs(root)
    wf = basetng.WriteData()
    if not wf: #stupid option for this mode, so just exit
      basetng.logTab(enum_out_stream.logfile,str("No files written: exit"))
      return basetng
    basetng.logTab(enum_out_stream.summary,"Copy database for deep search")
    basetng.logUnderLine(enum_out_stream.summary,"Seek Information")
    lines = DAGDB.seek_info_all().split('\n')
    for line in lines:
      basetng.logTab(enum_out_stream.summary,line)
    basetng.logBlank(enum_out_stream.logfile)
    return basetng
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  cards = None
  if args.cards is not None:
    cards = args.cards.read()
  if directory is not None:
    cards = cards + "\nphasertng suite database = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng.DAGDATABASE.parse_cards(cards)
  basetng = runDEEP().inner_deep(basetng=basetng)
  print(basetng.summary())
