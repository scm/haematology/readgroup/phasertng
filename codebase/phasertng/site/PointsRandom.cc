//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/PointsRandom.h>

namespace phasertng {

  PointsRandom::PointsRandom(unsigned seed_) :
      SiteList()
  {
    Size = 0;
    restart(seed_);
  }

  PointsRandom::PointsRandom(int nrand_,unsigned seed_) :
      SiteList(),
      Size(nrand_)
  {
    restart(seed_);
  }

  void
  PointsRandom::restart(unsigned seed_)
  {
    //reset generator
    generator = scitbx::random::mersenne_twister(seed_);
    inext = 0;
  }

  bool
  PointsRandom::at_end()
  {
    return inext >= Size;
  }

   dvect3
   PointsRandom::next_site()
   {
    double x = generator.random_double(); //[0,1]
    double y = generator.random_double(); //[0,1]
    double z = generator.random_double(); //[0,1]
    dvect3 Site(x,y,z);
    inext++;
    return Site;
  }

  dvect3
  PointsRandom::next_site(double range)
  {
    double x = range*generator.random_double(); //[0,1]
    double y = range*generator.random_double(); //[0,1]
    double z = range*generator.random_double(); //[0,1]
    x = generator.random_bool(1,0.5)[0] ? x : -x;
    y = generator.random_bool(1,0.5)[0] ? y : -y;
    z = generator.random_bool(1,0.5)[0] ? z : -z;
    dvect3 Site(x,y,z);
    inext++;
    return Site;
  }

  af_dvect3
  PointsRandom::all_sites()
  {
    af_dvect3 siteList;
    for (int i = 0; i < Size; i++)
    {
      double x = generator.random_double(); //[0,1]
      double y = generator.random_double(); //[0,1]
      double z = generator.random_double(); //[0,1]
      dvect3 Site(x,y,z);
      inext++;
      siteList.push_back(Site);
    }
    return siteList;
  }

}// phasertng
