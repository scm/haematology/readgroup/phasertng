//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/Edge.h>

namespace phasertng {
namespace dag {

  void
  Edge::shift_edge(const std::string text,const std::string tag)
  {
    PENULTIMATE = ULTIMATE;
    ULTIMATE.initialize_from_text(text);
    ULTIMATE.initialize_tag(tag);
    PROPREANTEPENULTIMATE.insert(PENULTIMATE);
  }

  void
  Edge::increment(const std::string tag)
  {
    PENULTIMATE = ULTIMATE;
    do {
      ULTIMATE.increment(tag);
    }
    while (PROPREANTEPENULTIMATE.count(ULTIMATE));
    PROPREANTEPENULTIMATE.insert(ULTIMATE);
  }

}} //phasertng
