//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_int_vector_class__
#define __phasertng_type_int_vector_class__
#include <phasertng/type/optional.h>
#include <scitbx/vec3.h>

namespace phasertng {
namespace type {
typedef scitbx::vec3<int>                ivect3;

class int_vector : public optional
{
  ivect3  dephalt;
  ivect3  uvalue;
  bool    pminset = false;
  bool    pmaxset = false;
  int     pminval = 0.;
  int     pmaxval = 0.;

  public:
  const bool& minset() const { return pminset; }
  const int&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const int&  maxval() const { return pmaxval; }

  int_vector() : optional() {}
  int_vector(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const ivect3 dephalt_ = { DEF_INT_FLAG,DEF_INT_FLAG,DEF_INT_FLAG }
  );

  void set_value(ivect3 other);
  void set_value(int a, int b, int c);
  void set_value(int i, int a);
  void set_value(const sv_string& vcards,const ivect3& other);
  bool is_default() const;
  void set_default();
  ivect3 value_or_default() const;
  int value_or_default(int i) const;
  ivect3 value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::int_vector& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
