//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Interpolation_class__
#define __phasertng_Interpolation_class__
#include <phasertng/io/EntryBase.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/src/Bin.h>

namespace phasertng {

  class Ensemble;

  class Interpolation : public EntryBase
  {
    public:
      af_double  SIGMAA; //indexed on ibin
      af_cmplex  EMAP;
      Identifier MODLID;
      Identifier REFLID;
      Bin        BIN;
      UnitCell   UC;
      SpaceGroup SG;
      std::string Warnings;
      af_double  rSigaSqr; //depends on reflid
      std::vector<std::pair<dvect3,double>>  DISORDER; //lattice translocation disorder vector and occupancy

    private:
      static const int H0K0L0 = 0;
      static const int H1K0L0 = 1;
      static const int H0K1L0 = 2;
      static const int H1K1L0 = 3;
      static const int H0K0L1 = 4;
      static const int H1K0L1 = 5;
      static const int H0K1L1 = 6;
      static const int H1K1L1 = 7;
      int NREFL = 0;
      int sizeh = 0;
      int sizek = 0;
      int sizel = 0;

    public:
      Interpolation() { set_four_point(); }
      Interpolation(af_string,std::string,af::double6,af_double,af_cmplex);

    public:
      cmplex (Interpolation::*Pt)(const dvect3&); //pointer to function
      cmplex callPt(const dvect3& offGrid) //call to pointer to function
      { return (this->*Pt)(offGrid); }

      std::tuple<cmplex, cvect3, cmat33> (Interpolation::*TGH)(const dvect3&,const bool,const bool);
      std::tuple<cmplex, cvect3, cmat33> callTGH(const dvect3& offGrid,const bool dograd,const bool dohess)
      { return (this->*TGH)(offGrid,dograd,dohess); }

      //note that the fast rotation function does not use callPt but the ECALC terms directly

    public:
      cmplex FourPt(const dvect3&);
      cmplex EightPt(const dvect3&);
      cmplex TricubicPt(const dvect3&);
      cmplex FourPtLTD(const dvect3&);
      cmplex EightPtLTD(const dvect3&);
      cmplex TricubicPtLTD(const dvect3&);
      std::tuple<cmplex, cvect3, cmat33> errorTGH(const dvect3&,const bool,const bool);
      std::tuple<cmplex, cvect3, cmat33> linearTGH(const dvect3&,const bool,const bool);
      std::tuple<cmplex, cvect3, cmat33> tricubicTGH(const dvect3&,const bool,const bool);
      std::tuple<cmplex, cvect3, cmat33> linearTGHLTD(const dvect3&,const bool,const bool);
      std::tuple<cmplex, cvect3, cmat33> tricubicTGHLTD(const dvect3&,const bool,const bool);
      void   set_four_point()
      {
        Pt = &Interpolation::FourPt; //for llg rescoring
        TGH = &Interpolation::errorTGH; //for refinement but just placeholder, should never be called
      }
      void   set_eight_point()
      {
        Pt = &Interpolation::EightPt; //for llg rescoring, consistent with the refinement target
        TGH = &Interpolation::linearTGH;  //for refinement
      }
      void   set_tricubic_point()
      { //no ltd for this one
        Pt = &Interpolation::TricubicPt; //experimental code, not used
        TGH = &Interpolation::tricubicTGH; //experimental code, not used
      }
      void   set_four_point_ltd()
      {
        Pt = &Interpolation::FourPtLTD; //for llg rescoring
        TGH = &Interpolation::errorTGH; //for refinement but just placeholder, should never be called
      }
      void   set_eight_point_ltd()
      {
        Pt = &Interpolation::EightPtLTD; //for llg rescoring, consistent with the refinement target
        TGH = &Interpolation::linearTGHLTD;  //for refinement
      }
      void   set_tricubic_point_ltd()
      { //no ltd for this one
        Pt = &Interpolation::TricubicPtLTD; //experimental code, not used
        TGH = &Interpolation::tricubicTGHLTD; //experimental code, not used
      }

    private:
      cmplex ConstLTD(const dvect3& offGrid) const;

    public: //concrete members of virtual base in EntryBase
      bool in_memory() { return (SIGMAA.size() > 0 and EMAP.size() > 0); }
      void read_from_disk() { ReadMtz(); }
      void write_to_disk() { WriteMtz(); }

    protected:
      cmplex  getE(int,int,int);
      ivect3  getHKL(int,int,int);
      af::shared< std::tuple<millnx,cmplex,double,int> > VanillaReadMtz();

    public:
      void   ReadMtz();
      void   WriteMtz();
      //double SigmaA(const double&);
      void   ParseEnsemble(Ensemble&);
      af_string  get_history();
      void       SetHistory(af_string);
      const int  nrefl() const { return NREFL; }
      std::string get_hall() { return SG.HALL; }
      af::double6 get_cell() { return UC.cctbxUC.parameters(); }

      inline int floor_double2int(const double& d)
      {
        return ( (d >= 0) ? int( d ) : int( d ) - 1 );
      }

      af_string logInterp(int);
      void setup_sigmaa_sqr(af_double);

      private:
        cmplex monocubic(cmplex[4],double);
        cmplex bicubic(cmplex[4][4],double,double);
        cmplex tricubic(cmplex[4][4][4],double,double,double);
        cmplex TricubicXYZ(double,double,double);
  };

}//phasertng
#endif
