//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Variance_class____
#define __phasertng_Variance_class____
#include <phasertng/main/includes.h>
#include <phasertng/io/entry/ModelsFcalc.h>

namespace phasertng {

  class FourParameterSigmaA; //forward declaration

  class Variance : public ModelsFcalc
  {
    //class derived with E's not F's although shared FCALC name from ModelsFcalc
    //added the SIGMAA and the MODELWT
    public: //members
      Identifier           REFLID;
      af_double            SIGMAA;
      math::Matrix<double> MODELWT;
      int                  NTHREADS = 1;
      af_double  rSigaSqr; //depends on reflid

    public: //constructors
      bool in_memory() { return MILLER.size(); }
      void read_from_disk() { ReadMtz(); }
      void write_to_disk() { WriteMtz(); }

    public: //constructors
      Variance(): ModelsFcalc() {}
      Variance(af_string,af::double6,af_millnx,af_cmplex,af_double,af_double);

    public:
      void      WriteMtz();
      void      ReadMtz();
      af_double get_sigmaa() { return SIGMAA.deep_copy(); }
      af_double get_weights() { return MODELWT.ref(); }
      void      SetWts(af_double);
      sv_double modelwt(int);
      af_string get_history();
      void      SetHistory(af_string);

      af_string logCalculationOfStatisticalWeighting();
      af_string logVariancesAndModelWeightsByResolutionBin();
      af_string logVariancesAndModelWeights();
      af_string calculate_modelweights(const double,const sv_double,const FourParameterSigmaA);
      void setup_sigmaa_sqr(af_double);

    private:
      sv_string calculate_partial_modelweights(int,int,const double,const sv_double,const FourParameterSigmaA);
  };

}
#endif
