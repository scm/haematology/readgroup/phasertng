//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_percent_class__
#define __phasertng_type_percent_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class percent : public optional
{
  private:
    double dephalt = 0;
    double uvalue = 0;
    bool   pminset = true;
    bool   pmaxset = true;
    double pminval = 0;
    double pmaxval = 100;

  public:
    const bool&   minset() const { return pminset; }
    const double& minval() const { return pminval; }
    const bool&   maxset() const { return pmaxset; }
    const double& maxval() const { return pmaxval; }

  percent() : optional() {}
  percent(
      const std::string parameter,
      const double dephalt_=DEF_FLT_FLAG
  );

  void set_value(double other);
  void set_value(const sv_string& vcards,const double other);
  double value_or_default_percent() const;
  double value_or_default_fraction() const;
  bool is_default() const;
  void set_default();
  double value_or_default() const;
  double value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::percent& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
