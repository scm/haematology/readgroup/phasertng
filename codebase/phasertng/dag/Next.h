//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Next_class__
#define __phasertng_dag_Next_class__
#include <phasertng/main/Identifier.h>
#include <phasertng/dag/xyzRotMatDeg.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Next : public xyzRotMatDeg
  { //rotation only, with relative translation
    public:
      bool        PRESENT = false;
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      dvect3      EULER = {0,0,0}; //wrt mt
      dvect3      ORTHR = {0,0,0}; //wrt mt
      dmat33      SHIFT_MATRIX = dmat33(); //wrt original
      bool        TRANSLATED = false;
      dvect3      FRACT = {0,0,0}; //difference for different curls internally, as wrt PT
      dvect3      SHIFT_ORTHT = {0,0,0}; //wrt original
      double      TFZ = 0;
      std::vector<dag::Gyre>  GYRE;
      //fract externally wrt input coordinates
      //fract internally wrt molecular transform coordinates
      //fract use dag database functions to switch between the two forms in i/o

    public: //internal variable only, not parsed/unparsed
      double      FS = 0;
      double      DRMS_SHIFT = 0;
      double      generic_float = 0;

    //return the final rotation and fractional translation, given the unit cell info
    dmat33
    finalR() const
    {
      dmat33 ROT = scitbx::math::euler_angles::zyz_matrix(EULER[0],EULER[1],EULER[2]);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = perturbed(ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      return ROTperturb;
    }

    //return the final rotation and fractional translation, given the unit cell info
    std::pair<dmat33,dvect3>
    finalRT(const dmat33& orthmat,const dmat33& fracmat) const
    {
      dmat33 ROT = scitbx::math::euler_angles::zyz_matrix(EULER[0],EULER[1],EULER[2]);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = perturbed(ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      dvect3 iOrthT = orthmat * FRACT;
      dvect3 TRA = fracmat*iOrthT;
      return { ROTperturb, TRA };
    }

   //return the final rotation and fractional translation, given the unit cell info
    dmat33
    gyre_finalR(int g) const
    {
      dmat33 ROT = scitbx::math::euler_angles::zyz_matrix(EULER[0],EULER[1],EULER[2]);
      //apply xyz rotation perturbation to molecular transform
      //orientation (for refinement)
      dmat33 Rperturb = perturbed(GYRE[g].ORTHR);
      dmat33 ROTperturb = ROT * Rperturb;
      return ROTperturb;
    }

    double
    delta_angle() const
    {
      dmat33 Rmat = perturbed(ORTHR);
      double ang;
      std::tie(std::ignore,ang) = axis_and_angle_deg(Rmat,0);
      return std::fabs(ang);
    }

    bool turn_present() const { return PRESENT and !TRANSLATED and !GYRE.size(); }
    bool curl_present() const { return PRESENT and TRANSLATED and !GYRE.size(); }
    bool gyre_present() const { return PRESENT and !TRANSLATED and GYRE.size(); }
    bool gyre_curl_present() const { return PRESENT and TRANSLATED and GYRE.size(); }
    int  ngyre() const { return GYRE.size(); }
  };

}} //phasertng
#endif
