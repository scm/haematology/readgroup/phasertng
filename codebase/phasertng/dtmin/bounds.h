//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dtmin_bounds_class__
#define __phasertng_dtmin_bounds_class__

namespace phasertng {
namespace dtmin {

struct bounds
{
  bool bounded_;
  double limit_;
  bounds() {bounded_ = false; limit_ = 0;}
  bounds(bool b,double f) {bounded_ = b; limit_ = f;}
  void off() {bounded_ = false; limit_ = 0;}
  void on(double f) {bounded_ = true; limit_ = f;}
};

struct Bounds
{
  bounds lower_ = bounds(false,0);
  bounds upper_ = bounds(false,0);

  Bounds() {}

  void lower_off()            { lower_.bounded_ = false; }
  void upper_off()            { upper_.bounded_ = false; }
  void lower_on(double l)     { lower_ = bounds(true,l); }
  void upper_on(double u)     { upper_ = bounds(true,u); }
  void on(double l, double u) { lower_ = bounds(true,l); upper_ = bounds(true,u); }
  void off()                  { lower_.bounded_ = upper_.bounded_ = false; }
  bool lower_bounded() { return lower_.bounded_; }
  bool upper_bounded() { return upper_.bounded_; }
  double lower_limit() { return lower_.limit_; }
  double upper_limit() { return upper_.limit_; }
};

} //pod
} //phasertng
#endif
