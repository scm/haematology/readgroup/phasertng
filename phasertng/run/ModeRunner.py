from phasertng import InputCard,Phasertng,PhaserError,SpaceGroup
from phasertng import enum_out_stream,enum_entry_data,enum_err_code
from phasertng.phil.mode_generator import *
from phasertng.scripts.voyager_jiffies import *
from phasertng.run.runRFAC import *
from phasertng.run.runBEAM import *
from phasertng.run.runFETCH import *
from phasertng.run.runBUB import *
from phasertng.run.runSRFP import *
from phasertng.run.runXREF import *
from phasertng.run.runDEEP import *
from phasertng.run.runFIND import *
from phasertng.run.runWALK import *
from phasertng.run.runJOIN import *
from phasertng.run.runPOOL import *
from phasertng.run.runHYSS import *
from phasertng.run.runSSCC import *
from phasertng.run.runTREE import *
import copy,shutil

def get_chk_cards():
   return '''
phasertng suite write_files off
phasertng suite write_cards off
phasertng suite write_phil off
phasertng suite write_log off
phasertng suite write_density off
phasertng suite write_data off
phasertng suite write_xml on
phasertng suite level  concise
phasertng suite store  silence
'''

def call_inner_function(user_phil_str,outsel,inner_function):
  #{{{
  basetng = Phasertng("PHENIX") #VOYAGER, PHENIX

  if outsel == 1:
    from libtbx.utils import multi_out
    out_wrapper = multi_out()
    log_file = open("phaser.log", "w")
    out_wrapper.register("Primary output stream", sys.stdout)
    out_wrapper.register("Phaser logfile", log_file)
    basetng.set_file_object(out_wrapper)
  elif outsel == 2:
    log_file = open("phaser.log", "w")
    basetng.set_file_object(log_file)
  elif outsel == 3:
    from libtbx.utils import null_out
    basetng.set_file_object(null_out())
  elif outsel == 4:
    basetng.set_sys_stdout()
  elif outsel == 5: #same as 4
    basetng.set_file_object(sys.stdout)
  else: #same as 4 (default)
    basetng = basetng
  try:
    basetng = inner_function(basetng,user_phil_str)
  except ValueError as e:
    raise e
  except Exception as e:
    raise e
  return basetng
  #}}}


class ModeRunner:
  def __init__(self,basetng,mode_list):
    self.modedata = None
    if len(mode_list) > 1:
      for m in  mode_list:
        if [m] in mode_generator().python_modes():
          raise ValueError(enum_err_code.input,"Multi-mode cannot include python mode")
    if len(mode_list) == 1 and mode_list in mode_generator().python_modes():
      assert(len(mode_list)==1)
      self.run(basetng=basetng,mode=str(mode_list[0]))
    else:
      #this is where we can enter c++ modes as either a sentence or [words]
      #words are converted to sentence
      multistr = ' '.join(str(n) for n in mode_list)
      basetng.input.set_as_str(".phasertng.mode.",multistr)
      without_mode = False
      Cards = basetng.input.card_str(without_mode)
      basetng.run(Cards)
    if not basetng.success():
      raise ValueError(basetng.error_code(),basetng.error_message())
    self.modedata = basetng.get_moderunner()
    if hasattr(basetng, "use_pickle") and basetng.use_pickle:
      import pickle
      if not hasattr(basetng, "mrsolution"):
        basetng.mrsolution = dict()
      DAGDB = basetng.DAGDATABASE
      subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
      dumpfile = FileSystem(basetng.DataBase(),subdir,'picard.pickle')
      if os.path.exists(dumpfile.parent_path()):
        #to reconstruct the pathway we want to restore the mrsolution so the decision making is the same
        #even if skipping, there are new jobs being created at the end that need pickle file written (eg after crash)
        if not os.path.exists(dumpfile.fstr()):
          basetng.logLoopInfo(enum_out_stream.process,"Dump File " + dumpfile.fstr())
          pickle.dump(basetng.mrsolution, open(dumpfile.fstr(), 'wb'))
        skipping = bool(basetng.input.get_as_str(".phasertng.overwrite.").strip() == "False")
        if skipping: #we are skipping runs that exist
          #below will waste time reading the pickle files from new subdirs after a previous job failed
          #but this is unavoidable because we don't know which are new
          if os.path.exists(dumpfile.fstr()):
            basetng.logLoopInfo(enum_out_stream.process,"Load File:\nReading File " + dumpfile.fstr())
            basetng.mrsolution = pickle.load(open(dumpfile.fstr(), "rb"))
          else:
            basetng.logLoopInfo(enum_out_stream.process,"Load File:\nNo File " + dumpfile.fstr())
   #put changes on basetng.input

  def solution(self):
     return self.modedata

  #runs a single mode only
  def run(self,basetng,mode):
    #{{{
    try:
      #this replicates the code in Phasertng.cc run
      basetng.clear_storage()
     #ignore_unknown_keys = True
     #without_mode = True
     #Cards = basetng.input.card_str(without_mode)
     #basetng.input = InputCard()
     #basetng.input.parse(Cards,ignore_unknown_keys)
     #basetng.set_user_input_for_suite(basetng.input)
      basetng.logProgramStart(enum_out_stream.concise)
      job_has_been_run_previously = basetng.setup_before_call_to_mode([mode],0)
      basetng.logSectionHeader(enum_out_stream.summary,"RUN MODE " + mode.upper())
      if job_has_been_run_previously:
        basetng.skipping()
      else:
        basetng.logTab(enum_out_stream.process,"Running...");
        if (mode == "rfac"):
          runRFAC().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "beam"):
          runBEAM().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "fetch"):
          runFETCH().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "bub"):
          runBUB().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "xref"):
          runXREF().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "srfp"):
          runSRFP().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "deep"):
          runDEEP().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "find"):
          runFIND().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "walk"):
          runWALK().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "join"):
          runJOIN().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "hyss"):
          runHYSS().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "sscc"):
          runSSCC().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "tree"):
          runTREE().run_from_basetng_and_input(basetng=basetng)
        elif (mode == "pool"):
          runPOOL().run_from_basetng_and_input(basetng=basetng)
        else:
          raise ValueError(enum_err_code.fatal, "The ModeRunner Twilight Zone")
      basetng.cleanup_after_call_to_mode(job_has_been_run_previously)
      basetng.logProgramEnd(enum_out_stream.concise)
    except ValueError as e:
        error = PhaserError()
        error.Set(str(e.args[0]).upper(),e.args[1])
        basetng.logError(enum_out_stream.concise,error)
        job_previous = False
        basetng.write_logfiles(job_previous)
        basetng.logTrailer(enum_out_stream.summary);
    except AssertionError:
        _, _, tb = sys.exc_info()
     #  Traceback.print_tb(tb) # Fixed format
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]
        fstem = os.path.basename(filename) # include the ext
        rawmsg = ('Assert error line {} of {}: {}'.format(line, fstem, text))
        error = PhaserError()
        error.Set("FATAL",rawmsg) #we will use PYTHON for a scripting error, this catches cctbx etc
        basetng.logError(enum_out_stream.concise,error)
        job_previous = False
        basetng.write_logfiles(job_previous)
        basetng.logTrailer(enum_out_stream.summary);
    except Exception as e:
        rawmsg = "" if str(e) is None else str(e)
        error = PhaserError()
       #print(type(e))
       #print(dir(e))
       #print(e.args)
        error.Set("FATAL",rawmsg) #we will use PYTHON for a scripting error, this catches cctbx etc
        basetng.logError(enum_out_stream.concise,error)
        job_previous = False
        basetng.write_logfiles(job_previous)
        basetng.logTrailer(enum_out_stream.summary);
    basetng.logTab(enum_out_stream.process,"Exit Code: " + str(basetng.exit_code()))
    basetng.input.set_as_str(".phasertng.notifications.success.",str(basetng.success()))
    basetng.input.set_as_str(".phasertng.notifications.error.type.",basetng.error_type().lower())
    basetng.input.set_as_str(".phasertng.notifications.error.message.",basetng.error_message())
    basetng.input.set_as_str(".phasertng.notifications.error.code.",str(basetng.exit_code()))
    #}}}


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-m',"--mode",help="Mode for runner",required=True)
  parser.add_argument('-c',"--cards",help="Cards for runner",required=True)
  parser.add_argument('-v',"--verbose",dest='verbose',action='store_true',help="output to stdout",required=False)
  parser.add_argument('-t',"--testing",dest='testing',action='store_true',help="output to stdout",required=False)
  parser.add_argument('-j',"--join",dest='jointype',type=str,default='',required=False)
  args = parser.parse_args()
  with open(args.cards, 'r',encoding="utf8", errors='ignore') as f:
    f_content = f.read()
  if args.testing:
    f_content += "\nphasertng suite level testing"
  elif args.verbose:
    f_content += "\nphasertng suite level verbose"
  else:
    f_content += "\nphasertng suite level logfile"
  basetng = Phasertng("VOYAGER")
  basetng.input = InputCard()
  basetng.input.parse(f_content,False)
  basetng.set_user_input_for_suite(basetng.input)
  if len(args.jointype) > 0:
    basetng.input.generic_int  = ord(args.jointype[0])-65
  cards = ModeRunner(basetng=basetng,mode_list=[str(args.mode)]).solution() #python code underneath
# print(cards)
