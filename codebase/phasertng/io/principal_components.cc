//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/principal_components.h>
#include <scitbx/math/principal_axes_of_inertia.h>
#include <scitbx/math/eigensystem.h>
#include <cctbx/adptbx.h>

namespace phasertng {

  void principal_components::set(af_dvect3 cw)
  {
    dmat33 Inertia(1,0,0,0,1,0,0,0,1);
    dvect3 principal_T(0,0,0);
    if (!cw.size()) return;
    dvect3 xyz_centre = dvect3(0,0,0);
    double total_weight(0);
    for (int a = 0; a < cw.size(); a++)
    {
      for (int i = 0; i < 3; i++)
      {
        xyz_centre[i] += cw[a][i];
      }
      total_weight++;
    }
    for (int i = 0; i < 3; i++)
      xyz_centre[i] /= total_weight;

    double xx(0),xy(0),xz(0),yy(0),yz(0),zz(0);
    double xterm(0),yterm(0),zterm(0),dot(0);
    int i(0),j(0),k(0),a(0);

    for (a = 0; a < cw.size(); a++)
    {
      xterm = cw[a][0] - xyz_centre[0];
      yterm = cw[a][1] - xyz_centre[1];
      zterm = cw[a][2] - xyz_centre[2];

      xx += xterm*xterm;
      xy += xterm*yterm;
      xz += xterm*zterm;
      yy += yterm*yterm;
      yz += yterm*zterm;
      zz += zterm*zterm;
    }

    dmat33 tensor(0,0,0,0,0,0,0,0,0);
    tensor(0,0) = yy + zz;
    tensor(0,1) = -xy;
    tensor(0,2) = -xz;
    tensor(1,0) = -xy;
    tensor(1,1) = xx + zz;
    tensor(1,2) = -yz;
    tensor(2,0) = -xz;
    tensor(2,1) = -yz;
    tensor(2,2) = xx + yy;

    dmat6 sym_tensor(tensor);
    cctbx::adptbx::eigensystem<double> eigens(sym_tensor);
    //select the direction for each principal moment axis

    for (int j_ = 0; j_ < 3; j_++)
    {
      Inertia(0,j_) = eigens.vectors(2)[j_];
      Inertia(1,j_) = eigens.vectors(1)[j_];
      Inertia(2,j_) = eigens.vectors(0)[j_];
    }

    for (i = 0; i < 2; i++)
    {
      for (a = 0; a < cw.size(); a++)
      {
        xterm = Inertia(i,0) * (cw[a][0]-xyz_centre[0]);
        yterm = Inertia(i,1) * (cw[a][1]-xyz_centre[1]);
        zterm = Inertia(i,2) * (cw[a][2]-xyz_centre[2]);
        dot = xterm + yterm + zterm;
        if (dot < 0)
        {
          for (k = 0; k < 3; k++)
            Inertia(i,k) = -Inertia(i,k);
        }
        if (dot != 0) goto checkRHS;
      }
    }

    checkRHS:

    //moment axes must give a right-handed coordinate system
    xterm = Inertia(0,0)*(Inertia(1,1)*Inertia(2,2)-Inertia(2,1)*Inertia(1,2));
    yterm = Inertia(0,1)*(Inertia(2,0)*Inertia(1,2)-Inertia(1,0)*Inertia(2,2));
    zterm = Inertia(0,2)*(Inertia(1,0)*Inertia(2,1)-Inertia(2,0)*Inertia(1,1));
    dot = xterm + yterm + zterm;
    if (dot < 0)
      for (j = 0; j < 3; j++)
        Inertia(2,j) = -Inertia(2,j);
    //theEnd:

    //print the moments of PR and the principal axes
   // dvect3 moment(xterm,yterm,zterm);

    //Translation after rotation will be given by matrix*(-xyz_centre).
    principal_T = Inertia*xyz_centre;
  //should be -centre but the operators aren't defined properly
    principal_T[0] *= -1;
    principal_T[1] *= -1;
    principal_T[2] *= -1;
    PR = Inertia;
    PT = principal_T;
  }

} //phasertng
