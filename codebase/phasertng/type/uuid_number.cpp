//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/uuid_number.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  uuid_number::uuid_number(
      const std::string parameter,
      const bool pminset_, const uuid_t pminval_,
      const bool pmaxset_, const uuid_t pmaxval_,
      const uuid_t  dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  uuid_number::set_value(uuid_t  other)
  {
    user = true;
    uvalue = other;
  }

  void
  uuid_number::set_value(const sv_string& vcards,const uuid_t & other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  uuid_number::is_default() const
  { return uvalue == dephalt; }

  void
  uuid_number::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  uuid_number::to_python()
  { return unparse_parameter(false); }

  void
  uuid_number::from_python(std::string str)
  { set_value(std::stoull(str)); }

  uuid_t
  uuid_number::value_or_default() const
  { return user ? uvalue : dephalt; }

  uuid_t
  uuid_number::value() const
  {
   // if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  uuid_number::unparse_parameter(bool phil) const
  {
    if (value_or_default() == DEF_UUID_FLAG)
      return None();
    std::string strid = std::to_string(uvalue);
    std::reverse(strid.begin(),strid.end());
    for (int i = strid.size(); i < uuid_w-1; i++)
      strid += '0';  //last uuid_w digits padded with zero
    std::reverse(strid.begin(),strid.end());
    return strid;
  }

  std::string
  uuid_number::show_default(bool phil) const
  {
    if (dephalt == DEF_UUID_FLAG)
      return None();
    std::stringstream ss;
    ss << dephalt;
    return ss.str();
  }

}}
