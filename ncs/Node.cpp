#include "Node.h"

using namespace boost;
using namespace std;

namespace phaser { namespace ncs {

//
// NodeData methods
//

// Constructor
NodeData::NodeData()
{
}

// Destructor
NodeData::~NodeData()
{
}

//
// Node methods
//

// Constructors
Node::Node() : _node_data( new NodeData() )
{
}


Node::Node( const boost::shared_ptr< NodeData > node_data )
    : _node_data( node_data )
{
}


Node::Node( const Node& rhs ) : _node_data( rhs._node_data )
{
}


// Destructor
Node::~Node()
{
}


// Overloaded operators
Node&
Node::operator=( const Node& rhs )
{
    // Self assignment
    if ( this == &rhs )
    {
        return *this;
    }

    // Normal assignment
    _node_data = rhs._node_data;
    return *this;
}


bool
Node::operator==( const Node& rhs ) const
{
    return _node_data == rhs._node_data;
}


bool
Node::operator!=( const Node& rhs ) const
{
    return !( *this == rhs );
}


// Child management
void
Node::insert_child(
        const char child_key,
        const shared_ptr< EdgeData >& child_to_add ) const
{
    _node_data->_edges[ child_key ] = child_to_add;
}


const shared_ptr< EdgeData >&
Node::get_child( const char child_key ) const
{
    std::map< char, boost::shared_ptr< EdgeData > >::const_iterator iter =
        _node_data->_edges.find( child_key );

    // This should only happen if the tree construction algorithm is wrong
    assert ( iter != _node_data->_edges.end() );

    return iter->second;
}


bool
Node::has_child( const char child_key ) const
{
    std::map< char, boost::shared_ptr< EdgeData > >::const_iterator iter =
        _node_data->_edges.find( child_key );

    return iter != _node_data->_edges.end();
}


// Suffix node
void
Node::link_suffix_node( const Node& suffix_node ) const
{
    _node_data->_suffix_node = suffix_node._node_data;
}


const Node
Node::get_suffix_node() const
{
    return Node( _node_data->_suffix_node.lock() );
}


// Set management
void
Node::append_to_set( const set< char >& values ) const
{
    _node_data->_terminators.insert( values.begin(), values.end() );
}


const set< char >&
Node::inspect_set() const
{
    return _node_data->_terminators;
}


// Iterator interface
Node::const_iterator
Node::begin() const
{
    return _node_data->_edges.begin();
}


Node::const_iterator
Node::end() const
{
    return _node_data->_edges.end();
}

}  } // namespace phaser::ncs
