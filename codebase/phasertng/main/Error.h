//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_error_class__
#define __phasertng_error_class__
#include <phasertng/enum/err_code.h>
#include <phasertng/main/Assert.h>

// The main "result" of phasertng is an error code!
namespace phasertng { //global namespace

constexpr int EXIT_CODE_OFFSET = 63;

class Error : public std::exception
{
  protected:
    err::code   CODE = err::SUCCESS;
    std::string MESSAGE = "";

  public:
    Error(err::code code=err::SUCCESS,std::string message=""):
      CODE(code),
      MESSAGE(message)
    { }

  public: //setters
    void Set(const std::string code,const std::string message)
    {
      //check that the input code is one of the allowed string values in enum
      //have to loop over possibilities as there is no error checking in macros
      CODE = err::code2Enum(code);
      MESSAGE = message;
    }

  public:
    err::code   error_code() const     { return CODE; }
    std::string error_type() const     { return err::code2String(CODE); }
    bool        success() const  { return (CODE == err::SUCCESS); }
    bool        failure() const  { return (CODE != err::SUCCESS); }
    int         exit_code() const     { return success() ? EXIT_SUCCESS : (static_cast<int>(CODE) + EXIT_CODE_OFFSET); }
    std::string error_message() const  { return MESSAGE; }
//virtual
    virtual const char* what() const throw()
    { return MESSAGE.c_str(); }
    virtual ~Error() throw() {}

    std::string
    error_help() const
    {
      std::string text = "  " + std::to_string(EXIT_SUCCESS) + ":SUCCESS\n";
      for (int i = 1; i < err::codeCount; i++)
        text += "  " + std::to_string(i+EXIT_CODE_OFFSET) + ":" +
                       err::code2String(static_cast<err::code>(i)) + "\n";
      text.pop_back();
      return text;
    }

    std::string
    error_code_as_error_type(int num) const
    {
      if (num == 0) return err::code2String(static_cast<err::code>(num));
      num -= EXIT_CODE_OFFSET;
      if (num >= err::codeCount or num <= 1)
        return "Error code out of range";
      return err::code2String(static_cast<err::code>(num));
    }

};

} //phasertng

#endif
