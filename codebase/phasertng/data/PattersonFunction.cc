//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/PattersonFunction.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/main/Error.h>
#include <phasertng/math/fft/real_to_complex_3d.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <cctbx/translation_search/fast_nv1995.h>
#include <cctbx/translation_search/symmetry_flags.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/sgtbx/rot_mx_info.h>
#include <cassert>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

typedef versa_grid_double   versa_grid_double;

  double
  PattersonFunction::eight_point_interpolation_percent(dvect3 frac)
  {
    assert(Patterson_max > 0);
/*
    bool docheck(true);
    if (docheck)
    {
    //don't need to revert to real space, real_map_const_ref unchanged
    assert(real_map_const_ref.size());
    assert(r1 < real_map_const_ref.size());
    assert(r2 < real_map_const_ref.size());
    bool patterson_map_memory_uncorrupted(true);
    patterson_map_memory_uncorrupted = (random_check1 == real_map_const_ref[r1]);
    assert(patterson_map_memory_uncorrupted);
    patterson_map_memory_uncorrupted = (random_check2 == real_map_const_ref[r2]);
    assert(patterson_map_memory_uncorrupted);
    }
*/
    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));
    double value = cctbx::maptbx::eight_point_interpolation(real_map_const_ref,frac);
    return 100*value/Patterson_max;
  }

  int
  PattersonFunction::number_reported(double reporting_percent)
  {
    assert(reporting_percent > 0);
    for (int i = 0; i < Patterson_heights_percent.size(); i++)
      if (Patterson_heights_percent[i] < reporting_percent)
        return i;
    return 0;
  }

  af_string
  PattersonFunction::calculate(
      cctbx::sgtbx::space_group cctbxSG,
      cctbx::uctbx::unit_cell cctbxUC,
      af::shared<millnx> MILLER_RES_LIMITS_,
      af_cmplex IOBS_EQUIVALENT_,
      af_double SIGIOBS_EQUIVALENT_,
      bool ORIGIN_REMOVED_,
      bool use_multiplicity,
      double MAPCUT_PERCENT,
      int MIN_REFL,
      double MIN_RESOLUTION,
      ivect3 max_frequency
    )
  {
    af_string output;
    output.push_back("");
    ORIGIN_REMOVED = ORIGIN_REMOVED_;
    MILLER_RES_LIMITS = MILLER_RES_LIMITS_;
    IOBS_EQUIVALENT = IOBS_EQUIVALENT_;
    SIGIOBS_EQUIVALENT = SIGIOBS_EQUIVALENT_;
    insufficient_data = false;
    if (MILLER_RES_LIMITS.size() < MIN_REFL)
    {
      insufficient_data = true;
      return output;
    }

    //need to take out the translational symmetry or the frequency analysis misses reflections
    //due to the systematic absences
    //also required for applying symmetry to patterson vectors!!
    UC = UnitCell(cctbxUC.parameters()); //store for frac->orth conversion
    SG = SpaceGroup("Hall: " + cctbxSG.type().hall_symbol());
    cctbxSG = cctbxSG.build_derived_group(false,false);
    DERIVED_SG = SpaceGroup("Hall: " + cctbxSG.type().hall_symbol());
    output.push_back("Derived Space Group: " + DERIVED_SG.CCP4);

    cctbx::sgtbx::space_group      derivedSG(DERIVED_SG.group());
    cctbx::sgtbx::space_group_type SgInfo(derivedSG);
    cctbx::sgtbx::space_group      cctbxPattSG(derivedSG.build_derived_patterson_group());
    cctbx::sgtbx::space_group_type PattInfo(cctbxPattSG);
    PATT_SG = SpaceGroup("Hall: " + PattInfo.hall_symbol());
    output.push_back("Patterson Symmetry: " + PATT_SG.CCP4);

    const af::double2 res_limits = cctbxUC.min_max_d_star_sq(MILLER_RES_LIMITS.const_ref());
    double patt_lores = cctbx::uctbx::d_star_sq_as_d(res_limits[0]);
           Patterson_hires = cctbx::uctbx::d_star_sq_as_d(res_limits[1]);
    char buf[80];
    snprintf(buf,80,
        "Resolution of Patterson (Number):     %6.2f %6.2f (%d)",
        Patterson_hires,
        patt_lores,
        static_cast<int>(MILLER_RES_LIMITS.size()));
    output.push_back(buf);
    output.push_back("");

    insufficient_resolution = false;
    if (Patterson_hires > MIN_RESOLUTION)
    {
      insufficient_resolution = true;
      return output;
    }
    // --- prepare fft grid
    //can't use full rfft class here because intermediates have to be stored
    fft::real_to_complex_3d RFFT(SgInfo,cctbxUC);
    RFFT.calculate_gridding(Patterson_hires,{1,1},false);
    RFFT.allocate();
    if (RFFT.memory_allocation_error)
      throw Error(err::MEMORY,"Patterson FFT array allocation");

    bool anomalous_flag(false);
    bool conjugate_flag(true);
    af::c_grid_padded<3> map_grid(RFFT.rfft.n_complex());
    Patterson_gridding = RFFT.gridding;
    bool treat_restricted = true;
    cctbx::maptbx::structure_factors::to_map<double> Patterson(
        cctbxPattSG,
        anomalous_flag,
        MILLER_RES_LIMITS.const_ref(),
        IOBS_EQUIVALENT.const_ref(),
        RFFT.rfft.n_real(),
        map_grid,
        conjugate_flag,
        treat_restricted
      );
    af::ref<cmplex, af::c_grid<3> > Patterson_fft_ref(
        Patterson.complex_map().begin(),
        af::c_grid<3>(RFFT.rfft.n_complex())
     );

    // --- do the fft
    RFFT.rfft.backward(Patterson_fft_ref);

    af::ref<double, af::c_grid_padded<3> > real_map_padded(
        reinterpret_cast<double*>( Patterson.complex_map().begin()),
        af::c_grid_padded<3>( RFFT.rfft.m_real(), RFFT.rfft.n_real())
     );

    // --- store the Patterson map
    // --- have to store this map as well as const ref below
    // --- or else call to 8pt interpolation gives valgrind error in line below
    real_map_unpadded = versa_grid_double( af::c_grid<3>(RFFT.rfft.n_real()));
    //copy source to targets
    cctbx::maptbx::copy(real_map_padded, real_map_unpadded.ref());

    // --- work out mean and sigma for total search
    cctbx::maptbx::statistics<double> stats(
        af::const_ref<double, af::flex_grid<> >(
             real_map_unpadded.begin(),
             real_map_unpadded.accessor().as_flex_grid()
          )
      );
    Patterson_mean = stats.mean();
    Patterson_sigma = stats.sigma();
    output.push_back("Mean   = "+std::to_string(Patterson_mean));
    output.push_back("Sigma  = "+std::to_string(Patterson_sigma));
    assert(Patterson_sigma > 0);

    // --- store the Patterson map
    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));

    // --- find symmetry equivalents
    RFFT.tags.build(PattInfo, RFFT.sym_flags);
    af::ref<long, af::c_grid<3> > tag_array_ref(
        RFFT.tags.tag_array().begin(),
        af::c_grid<3>(RFFT.tags.tag_array().accessor())
      );
    if (!treat_restricted)
    assert(RFFT.tags.verify(real_map_const_ref));
    //don't need to revert to real space, real_map_const_ref unchanged
    r1 = std::floor(real_map_const_ref.size()/2.);
    random_check1 = real_map_const_ref[r1];
    r2 = std::floor(real_map_const_ref.size()/3.);
    random_check2 = real_map_const_ref[r2];
    assert(r1 < real_map_const_ref.size());
    assert(r2 < real_map_const_ref.size());
    assert(random_check1 == real_map_const_ref[r1]);
    assert(random_check2 == real_map_const_ref[r2]);

    // --- peak search
    int peak_search_level(3);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
        real_map_const_ref,
        tag_array_ref,
        peak_search_level,
        max_peaks,
        interpolate
     );

    output.push_back("Number of peaks = " + std::to_string(peak_list.size()));
    Patterson_heights_percent = peak_list.heights().deep_copy();
    Patterson_zscore = peak_list.heights().deep_copy();
    Patterson_sites = peak_list.sites().deep_copy();

    if (peak_list.size())
    {
      Patterson_max = Patterson_heights_percent[0];
      for (std::size_t i = 0; i < Patterson_heights_percent.size(); i++)
        Patterson_max = std::max(Patterson_max,Patterson_heights_percent[i]);
      output.push_back("Maximum = " + std::to_string(Patterson_max));
    }

    Patterson_multiplicity.resize(peak_list.size());
    for (int i = 0; i < peak_list.size(); i++)
    {
      //apply multiplicity even if there is no move
      double min_distance_sym_equiv = 1;//more generous than default of 0.5
      cctbx::sgtbx::site_symmetry this_site_sym(
         cctbxUC,PATT_SG.group(),Patterson_sites[i],min_distance_sym_equiv);
      Patterson_sites[i] = this_site_sym.exact_site();
      double multiplicity = PATT_SG.group().order_z()/this_site_sym.multiplicity();
      assert(multiplicity);
      Patterson_multiplicity[i] = multiplicity;
    }

    if (use_multiplicity)
    {
      output.push_back("Apply Patterson Multiplicity");
      for (int i = 0; i < peak_list.size(); i++)
      {
        //apply multiplicity even if there is no move
        int multiplicity = Patterson_multiplicity[i];
        Patterson_zscore[i] /= multiplicity;
        double perc = 100*Patterson_heights_percent[i]/Patterson_max;
        snprintf(buf,80,
            "Site #%2d M=%2d at %7.3f %7.3f %7.3f %6.2f%c /M=%6.2f%c",
            i+1,multiplicity,
            Patterson_sites[i][0],Patterson_sites[i][1],Patterson_sites[i][2],
            perc,'%',perc/multiplicity,'%');
        output.push_back(buf);
        Patterson_heights_percent[i] /= multiplicity;
      }
      output.push_back("");
      std::vector<std::pair<double,int> > sorted(peak_list.size());
      output.push_back("Sort after Patterson Multiplicity");
      {{
        for (int i = 0; i < peak_list.size(); i++)
          sorted[i] = { Patterson_heights_percent[i], i }; //sort on first
        auto cmp = []( const std::pair<double,int> &a, const std::pair<double,int> &b )
        { if (a.first == b.first) return a.second < b.second;
          return a.first >  //reverse sort
                 b.first; };
        std::sort(sorted.begin(),sorted.end(),cmp);
        {{
        af_double newdata(peak_list.size());
        for (int i = 0; i < peak_list.size(); i++)
          newdata[i] = Patterson_heights_percent[sorted[i].second]; //SORT ORDER on first
        Patterson_heights_percent = newdata.deep_copy();
        for (int i = 0; i < peak_list.size(); i++)
          newdata[i] = Patterson_zscore[sorted[i].second]; //SORT ORDER on first
        Patterson_zscore = newdata.deep_copy();
        }}
        {{
        af_int newdata(peak_list.size());
        for (int i = 0; i < peak_list.size(); i++)
          newdata[i] = Patterson_multiplicity[sorted[i].second]; //SORT ORDER on first
        Patterson_multiplicity = newdata.deep_copy();
        }}
        {{
        af_dvect3 newdata(peak_list.size());
        for (int i = 0; i < peak_list.size(); i++)
          newdata[i] = Patterson_sites[sorted[i].second]; //SORT ORDER on first
        Patterson_sites = newdata.deep_copy();
        }}
      }}
      for (int i = 0; i < peak_list.size(); i++)
      {
        //apply multiplicity even if there is no move
        output.push_back("Site # "+ itos(i+1) + " M=" + itos(Patterson_multiplicity[i]) + " at " + dvtos(Patterson_sites[i],7,3) + " " + dtos(100*Patterson_heights_percent[i]/Patterson_max,2) + " ##" + itos(sorted[i].second+1));
      }
    }

    if (peak_list.size())
    {
      Patterson_max = Patterson_heights_percent[0];
      for (std::size_t i = 0; i < Patterson_heights_percent.size(); i++)
        Patterson_max = std::max(Patterson_max,Patterson_heights_percent[i]);
      output.push_back("Maximum = " + dtos(Patterson_max));
      output.push_back("");
    }

    int top = ORIGIN_REMOVED ? 0 : 1;
    if (peak_list.size() > top)
    {
      for (int i = peak_list.size()-1; i >= 0; i--)
      {
        //Patterson_zscore = peak_list.heights deep copy at this point
        Patterson_zscore[i] = (Patterson_zscore[i]-Patterson_mean)/Patterson_sigma;
        Patterson_heights_percent[i] *= 100/Patterson_max;
      }
      Patterson_top = Patterson_heights_percent[top];
      Patterson_topsite = Patterson_sites[top];
    }

   //this is the percent below which the noise is removed for the back transform
   //for the frequency analysis
    assert(MAPCUT_PERCENT>0);
    double map_cutoff_height = (MAPCUT_PERCENT/100)*Patterson_max;

    for (int i = 0; i < real_map_padded.size(); i++)
    {
      //set to constant value - even out peak heights to all the same
      real_map_padded[i] = real_map_padded[i] < map_cutoff_height ? 0 : 1;
      //set to original value
      //real_map_padded[i] = real_map_padded[i] < map_cutoff_height ? 0 : real_map_padded[i];
    }

    RFFT.rfft.forward(real_map_padded);

    assert(r1 < real_map_const_ref.size());
    assert(r2 < real_map_const_ref.size());
    assert(random_check1 == real_map_const_ref[r1]);
    assert(random_check2 == real_map_const_ref[r2]);

    af::const_ref<cmplex, af::c_grid_padded<3> > complex_map(
        reinterpret_cast<cmplex*>(&*real_map_padded.begin()),
        af::c_grid_padded<3>(RFFT.rfft.n_complex()));

    bool discard_indices_affected_by_aliasing = true;
    cctbx::maptbx::structure_factors::from_map<double> from_map(
        cctbxUC,
        SgInfo,
        anomalous_flag,
        Patterson_hires,
        complex_map,
        conjugate_flag,
        discard_indices_affected_by_aliasing
      );

    //don't need to revert to real space, real_map_const_ref unchanged
    assert(random_check1 == real_map_const_ref[r1]);
    assert(random_check2 == real_map_const_ref[r2]);

    frequency_peaks.reserve(from_map.miller_indices().size());
    for (unsigned i = 0; i < from_map.miller_indices().size(); i++)
    {
      double data = std::abs(from_map.data()[i]);
      if (data != 0) // can get a flat map above when map_cutoff_height = minimum-patterson (0)
      {
        const millnx& miller_i = from_map.miller_indices()[i];
        const int& h = miller_i[0];
        const int& k = miller_i[1];
        const int& l = miller_i[2];
        pod::frequencies next_peak;
        next_peak.strength =  data;
        next_peak.index[0] = h;
        next_peak.index[1] = k;
        next_peak.index[2] = l;
        double ssqr(cctbxUC.d_star_sq(miller_i));
        next_peak.distance = cctbx::uctbx::d_star_sq_as_d(ssqr);
        if (
            !(l < 0 or (l == 0 && k < 0) or (l == 0 && k == 0 && h < 0)) and //friedel positive
          //  (std::fabs(h) != 0 and std::fabs(k) != 0 and std::fabs(l) != 0) and //boring
          //  (std::fabs(h) != 1 and std::fabs(k) != 1 and std::fabs(l) != 1) and //boring
          //  (std::fabs(h) >= 2 or std::fabs(k) >= 2 or std::fabs(l) >= 2) and //boring
            std::fabs(h) <= max_frequency[0] and
            std::fabs(k) <= max_frequency[1] and
            std::fabs(l) <= max_frequency[2]) //AJM added condition that limits distance separation
          frequency_peaks.push_back(next_peak);
      }
    }
    //assert(frequency_peaks.size()); //definitely don't assert

    //sort before return
    std::sort(frequency_peaks.begin(),frequency_peaks.end(),sort_freq_strength);
    std::reverse(frequency_peaks.begin(),frequency_peaks.end());
    output.push_back("");
    return output;
  }

  af_string
  PattersonFunction::logStatistics(double reporting_percent,std::string description,int io_nprint,double origindist,double tncs_percent)
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Patterson Statistics: " + description + "<<");
    output.push_back("Reporting Percent = " + dtoss(reporting_percent,2) + "%");
    if (insufficient_data or insufficient_resolution)
    {
      insufficient_data ?
        output.push_back("Insufficient data"):
        output.push_back("Insufficient resolution");
      output.push_back("");
    }
    size_t numreported = number_reported(reporting_percent);
    if (numreported == 0)
    {
      output.push_back("No sites");
      return output;
    }
    output.push_back("There " + std::string(numreported == 1 ? "was " : "were ") + itos(numreported) + " peak" + std::string(numreported == 1 ? "" : "s"));
    output.push_back("");
    output.push_back("Origin Height Zscore  M: Vector                                         Distance-to-Origin");
    bool first(true);
    int n = std::min(Patterson_sites.size(),numreported);
    for (int i = 0; i < numreported; i++)
    {
      dvect3 frac_i(Patterson_sites[i]);
      dvect3 orth_i = UC.orthogonalization_matrix()*(frac_i);
      auto ori = origin(frac_i,origindist);
      if (first and Patterson_heights_percent[i] < tncs_percent)
      {
        output.push_back(snprintftos("------ %5.1f%c ------",tncs_percent,'%'));
        first=false;
      }
      output.push_back(snprintftos(
          "%c      %5.1f%c  %5.1f %2i: FRAC %+6.4f %+6.4f %+6.4f (ORTH %6.1f %6.1f %6.1f) %7.1fA",
          ori.first ? '*':' ',
          Patterson_heights_percent[i],'%',
          Patterson_zscore[i],
          Patterson_multiplicity[i],
          frac_i[0],frac_i[1],frac_i[2],
          orth_i[0],orth_i[1],orth_i[2],
          ori.second));
      if (i+1 == io_nprint)
      {
        if (n > io_nprint)
          output.push_back("--- etc " + itos(n-io_nprint) + " more" );
        break;
      }
    }
    output.push_back("");
    return output;
  }

  af_string
  PattersonFunction::logStatsExtra()
  {
    af_string output;
    output.push_back("");
    output.push_back("Origin removed: " + btos(ORIGIN_REMOVED));
    output.push_back("Maximum = " + dtos(Patterson_max));
    output.push_back("Mean    = " + dtos(Patterson_mean));
    output.push_back("Sigma   = " + dtos(Patterson_sigma));
    output.push_back("Top(%)  = " + dtos(Patterson_top,5,2));
    output.push_back("  at site = "+ dvtos(Patterson_topsite));
    output.push_back("");
    return output;
  }

  af_string
  PattersonFunction::logFrequencies(std::string description)
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Patterson Frequency Statistics: " + description + "<<");
    output.push_back("There " + std::string(frequency_peaks.size() == 1 ? "was " : "were ") + itos(frequency_peaks.size()) + " peak" + std::string(frequency_peaks.size() == 1 ? "" : "s"));
    output.push_back("");
    output.push_back("#  Strength   ---Index---   (d)");
    for (int i = 0; i < frequency_peaks.size(); i++)
    {
      output.push_back(snprintftos(
          "%3d %8.2f %+3d %+3d %+3d   %7.2f",
          i+1,
          frequency_peaks[i].strength,
          frequency_peaks[i].index[0],frequency_peaks[i].index[1],frequency_peaks[i].index[2],
          frequency_peaks[i].distance
          ));
    }
    output.push_back("");
    return output;
  }

  void
  PattersonFunction::WritePdb(FileSystem PDBOUT,double reporting_percent)
  {
    PdbHandler pdbhandler(PATT_SG,UC.cctbxUC,1);
    pdbhandler.addRemarkPhaser("PATTERSON");
    pdb_chains_2chainid chains; //so that X is included
    af::shared<PdbRecord> model;
    std::string Chain = chains.allocate(" A");
    int top = ORIGIN_REMOVED ? 0 : 1;
    size_t numreported = number_reported(reporting_percent) + top;
    int n = std::min(Patterson_sites.size(),numreported);
    int count(1);
    for (int j = top; j < n; j++,count++)
    {
      PdbRecord next;
      next.AtomNum = count;
      next.AtomName = " CA ";
      next.ResName = "PPK";
      next.Chain = Chain;
      Chain = chains.allocate(Chain); //increment Chain
      next.ResNum = count;
      next.X = UC.orthogonalization_matrix()*Patterson_sites[j];
      next.O = Patterson_heights_percent[j]/100.;
      next.B = Patterson_zscore[j];
      model.push_back(next);
    }
    pdbhandler.addModel(model);
    pdbhandler.VanillaWritePdb(PDBOUT);
  }

  void
  PattersonFunction::WritePdb(FileSystem PDBOUT,double reporting_percent,af_dvect3 sites)
  {
    PdbHandler pdbhandler(PATT_SG,UC.cctbxUC,1);
    pdbhandler.addRemarkPhaser("PATTERSON MINIMUM FUNCTION");
    af::shared<PdbRecord> model;
    int top = ORIGIN_REMOVED ? 0 : 1;
    size_t numreported = number_reported(reporting_percent) + top;
    int n = std::min(sites.size(),numreported);
    int count(1);
    for (unsigned j = top; j < n; j++,count++)
    {
      PdbRecord next;
      next.AtomNum = count;
      next.AtomName = " CA "; //so that coot draws atoms on read
      next.ResName = "PPK";
      next.ResNum = count;
      next.X = UC.orthogonalization_matrix()*sites[j];
      model.push_back(next);
    }
    pdbhandler.addModel(model);
    pdbhandler.VanillaWritePdb(PDBOUT);
  }

  void
  PattersonFunction::WriteMap(FileSystem MAPOUT)
  {
    MAPOUT.create_directories_for_file();
    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));

    bool docheck(true);
    if (docheck)
    {
    assert(real_map_const_ref.size());
    assert(r1 < real_map_const_ref.size());
    assert(r2 < real_map_const_ref.size());
    //don't need to revert to real space, real_map_const_ref unchanged
    bool patterson_map_memory_uncorrupted(true);
    patterson_map_memory_uncorrupted = (random_check1 == real_map_const_ref[r1]);
    assert(patterson_map_memory_uncorrupted);
    patterson_map_memory_uncorrupted = (random_check2 == real_map_const_ref[r2]);
    assert(patterson_map_memory_uncorrupted);
    }

    af_string labels(1);
    labels[0] = "Patterson map";
    af::int3 gridding_first(0,0,0);
    af::int3 gridding_last = Patterson_gridding;

    versa_grid_double real_map_padded(
         af::c_grid<3>(real_map_const_ref.accessor().focus())
       );
    cctbx::maptbx::copy(real_map_const_ref, real_map_padded.ref());
    af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
        real_map_padded.begin(), //versa_grid_double tsmap;
        af::c_grid_padded_periodic<3>(real_map_padded.accessor())
      );
    cctbx::sgtbx::space_group SgOpsP1("P1");
    iotbx::ccp4_map::write_ccp4_map_p1_cell(
        MAPOUT.fstr(),
        UC.cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels.const_ref()
      );
  }

  void
  PattersonFunction::WriteMap(FileSystem MAPOUT, versa_grid_double map)
  {
    MAPOUT.create_directories_for_file();
    af_string labels(1);
    labels[0] = "Patterson Map";
    af::int3 gridding_first(0,0,0);
    af::int3 gridding_last = Patterson_gridding;

    // --- store the Patterson map
    af::const_ref<double, af::c_grid_padded<3> > map_const_ref(
        map.begin(),
        af::c_grid_padded<3>(map.accessor()));

    versa_grid_double real_map_padded(
         af::c_grid<3>(map_const_ref.accessor().focus())
       );
    cctbx::maptbx::copy(map_const_ref, real_map_padded.ref());
    af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
        real_map_padded.begin(), //versa_grid_double tsmap;
        af::c_grid_padded_periodic<3>(real_map_padded.accessor())
      );
    cctbx::sgtbx::space_group SgOpsP1("P1");
    iotbx::ccp4_map::write_ccp4_map_p1_cell(
        MAPOUT.fstr(),
        UC.cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels.const_ref()
      );
  }

  bool
  PattersonFunction::harker(dvect3 frac)
  {
    //copy from harker.py in cctbx
    //returns true if frac is a harker vector
    sv_dvect3 list;
    std::vector<std::pair<scitbx::vec3<int>,int> > nc_dict;
    cctbx::sgtbx::space_group group = SG.group();
    for (int i_smx = 0; i_smx <  group.order_p(); i_smx++)
    {
      auto s = group(i_smx);
      auto r_info = s.r().info();
      if (r_info.type() < 2)
        continue;
      if (r_info.sense() < 0)
        continue;
      auto n = r_info.ev();
      auto p = s.t().mod_positive();
      assert(p.den() == group.t_den());
      auto c = -(n[0]*p.num()[0] + n[1]*p.num()[1] + n[2]*p.num()[2]);
      std::pair<scitbx::vec3<int>,int> nc = {n,c};
      if (std::find(nc_dict.begin(),nc_dict.end(),nc) == nc_dict.end())
      {
        nc_dict.push_back(nc);
        double den = static_cast<double>(p.den());
        dvect3 point(p[0]/den,p[1]/den,p[2]/den);
        std::string explore("normal:" + ivtos(n) + " point: " + dvtos(point));
        //std::cout << explore << std::endl;
        //list.push_back(plane_fractional(s=s, n=n, p=p, c=rational.int(c,p.den())));
        dvect3 vec_in_plane(frac[0]-point[0],frac[1]-point[1],frac[2]-point[2]);
        double dotprod = vec_in_plane[0]*n[0]+vec_in_plane[1]*n[1]+vec_in_plane[2]*n[2];
        if (std::fabs(dotprod) < DEF_PPM)
          return true;
      }
    }
    return false;
  }

  std::pair<bool,double>
  PattersonFunction::origin(dvect3 frac,double DIST)
  {
    af_dvect3 symmetry = PATT_SG.symmetry_xyz_cell(frac,1);
    double mindist = std::numeric_limits<double>::max();
    for (int isym = 0; isym < symmetry.size(); isym++)
    {
      const dvect3& fracCell_i = symmetry[isym];
      dvect3 orth_i = UC.orthogonalization_matrix()*(fracCell_i);
      double dist_to_origin(std::sqrt(std::fabs((orth_i)*(orth_i))));
      mindist = std::min(mindist,dist_to_origin);
    }
    return { (mindist < DIST), mindist };
  }

  std::pair<af_dvect3,versa_grid_double >
  PattersonFunction::constellation(
      bool minimum_function,
      std::size_t npeaks,
      af_dvect3 shift)
  {
    versa_grid_double minfn = real_map_unpadded.deep_copy();
    for (int i = 0; i < shift.size(); i++)
    {
      double value;
      double nx = minfn.accessor()[0];
      double ny = minfn.accessor()[1];
      double nz = minfn.accessor()[2];
      for (int lx = 0; lx < nx; lx++) {
        for (int ly = 0; ly < ny; ly++) {
          for (int lz = 0; lz < nz; lz++) {
            dvect3 frac = shift[i] + dvect3(lx/nx,ly/ny,lz/nz);
            for (int j = 0; j < 3; j++) while (frac[j] <  0) frac[j] += 1.0;
            for (int j = 0; j < 3; j++) while (frac[j] >= 1) frac[j] -= 1.0;
            //interpolate into original map, unmodified
            value = cctbx::maptbx::eight_point_interpolation(real_map_unpadded.const_ref(),frac);
            if (minimum_function)
              minfn(lx,ly,lz) = std::min(value,minfn(lx,ly,lz));
            else //sum
              minfn(lx,ly,lz) += value;
      }}}
    }

    // --- store the Patterson map
    af::const_ref<double, af::c_grid_padded<3> > minfn_const_ref(
        minfn.begin(),
        af::c_grid_padded<3>(minfn.accessor()));

    // --- find symmetry equivalents
    //false means don't use space group symmetry since after the shift
    //the map no longer has the space group symmetry
    //cctbx::sgtbx::search_symmetry_flags sym_flags(true,1);
    cctbx::sgtbx::search_symmetry_flags sym_flags(false,1);
    af::c_grid<3> grid_target(Patterson_gridding);
    cctbx::maptbx::grid_tags<long> tags(grid_target);
    tags.build(PATT_SG.type(),sym_flags);
    af::ref<long, af::c_grid<3> > tag_array_ref(
        tags.tag_array().begin(),
        af::c_grid<3>(tags.tag_array().accessor())
      );

    // --- peak search
    int peak_search_level(3);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
        minfn_const_ref,
        tag_array_ref,
        peak_search_level,
        max_peaks,
        interpolate
     );

    af_dvect3 top_npeaks;
    for (int i = 0; i < std::min(peak_list.size(),npeaks); i++)
    {
      dvect3 pk = peak_list.sites()[i];
      for (int j = 0; j < 3; j++)
        if (std::fabs(pk[j]) < DEF_PPM) pk[j] = 0;
      top_npeaks.push_back(pk);
    }
    return { top_npeaks,minfn};
  }

  void
  PattersonFunction::WriteMtz(FileSystem HKLOUT)
  {
    af_string history;
    history.push_back(constants::remark_tag + " PATTERSON");
    MtzHandler mtzOut; mtzOut.open(HKLOUT,SG,UC,1.,MILLER_RES_LIMITS.size(),history);
    CMtz::MTZCOL* mtzA = mtzOut.addCol("I",'J');
    CMtz::MTZCOL* mtzB = mtzOut.addCol("SIGI",'Q');
    for (int r = 0; r < IOBS_EQUIVALENT.size(); r++)
    {
      mtzOut.mtzH->ref[r] = MILLER_RES_LIMITS[r][0];
      mtzOut.mtzK->ref[r] = MILLER_RES_LIMITS[r][1];
      mtzOut.mtzL->ref[r] = MILLER_RES_LIMITS[r][2];
      mtzA->ref[r] = std::real(IOBS_EQUIVALENT[r]);
      mtzB->ref[r] = SIGIOBS_EQUIVALENT[r];
    }
    mtzOut.Put("Phasertng Patterson");
  }

  void
  PattersonFunction::erase_origin(double maxdist)
  {
    size_t last = 0;
    //fill start with interesting, in order
    for (size_t i = 0; i < Patterson_sites.size(); i++)
    {
      if (!origin(Patterson_sites[i],maxdist).first)
      {
        Patterson_heights_percent[last] = Patterson_heights_percent[i];
        Patterson_zscore[last] = Patterson_zscore[i];
        Patterson_sites[last] = Patterson_sites[i];
        Patterson_multiplicity[last] = Patterson_multiplicity[i];
        last++;
      }
    }
    //erase the end, which is rubbish
    Patterson_heights_percent.erase(Patterson_heights_percent.begin() + last, Patterson_heights_percent.end());
    Patterson_zscore.erase(Patterson_zscore.begin() + last, Patterson_zscore.end());
    Patterson_sites.erase(Patterson_sites.begin() + last, Patterson_sites.end());
    Patterson_multiplicity.erase(Patterson_multiplicity.begin() + last, Patterson_multiplicity.end());
  }

  std::pair<versa_grid_double,af::int3>
  PattersonFunction::calculate_substructure(
      UnitCell UC,
      SpaceGroup SG,
      bool haveFpart,
      af_cmplex Fpart,
      af_millnx Fmiller,
      af_double mI,
      double LLconst
    )
  {
    versa_grid_double ssdmap;
    af::int3 gridding;
    //fft thing
    cctbx::uctbx::unit_cell UCell = UC.cctbxUC;
    cctbx::sgtbx::space_group SgOps = SG.group();
    cctbx::sgtbx::space_group_type SgInfo = SG.type();
    af::int3 one_one_one(1,1,1);
    int max_prime(5);
    bool assert_shannon_sampling(true);
    double resolution_factor = 0.25;
    //cctbx assert resolution_factor <= 0.5
    resolution_factor = std::min(0.5,resolution_factor);
    const af::double2 res_limits = UCell.min_max_d_star_sq(Fmiller.const_ref());
    double hires = cctbx::uctbx::d_star_sq_as_d(res_limits[1]);

    bool isIsotropicSearchModel(false); //true doesn't run
    cctbx::translation_search::symmetry_flags sym_flags(isIsotropicSearchModel,haveFpart);

    gridding = cctbx::maptbx::determine_gridding(
      UCell, hires, resolution_factor,sym_flags,SgInfo,
      one_one_one,max_prime,assert_shannon_sampling);
    af::c_grid<3> grid_target(gridding);
    cctbx::maptbx::grid_tags<long> tags(grid_target);
    tags.build(SgInfo,sym_flags);

   // calcFpart(Fmiller,Fpart); // Structure factors for fixed atoms with unit scattering factor
   // PHASER_ASSERT(Fmiller.size() == Fpart.size());
    bool FriedelFlag(true);
    af_millnx FmillerP1 = miller::expand_to_p1_indices(SgOps,FriedelFlag,Fmiller.const_ref());
  //  af_float mI;
   // double LLconst = LESSTF1(FINDSITES,Fpart,mI);
    af_cmplex FcalcP1;
    // Search object can be one point atom or a constellation of point atoms
    af_dvect3 constellation;
    constellation.push_back(dvect3(0,0,0));
    for (unsigned idat = 0; idat < FmillerP1.size(); idat++)
    {
      cmplex Fsearch(0.,0.);
      for (unsigned natom = 0; natom < constellation.size(); natom++)
      {
        double dphi = FmillerP1[idat]*constellation[natom];
        Fsearch += tbl_cos_sin.get(dphi); //multiplies by 2*pi internally
      }
      Fsearch *= 1;//FINDSITES.OCCUPANCY; // Default occupancy for new sites
      FcalcP1.push_back(Fsearch);
    }
    cctbx::translation_search::fast_terms<double> fast_terms(
            gridding,
            FriedelFlag,
            FmillerP1.const_ref(),
            FcalcP1.const_ref());

    bool squared_flag(false); //true for second order mIsqr
    ssdmap = fast_terms.summation(
      SgOps,
      Fmiller.const_ref(),
      mI.const_ref(),
      Fpart.const_ref(),
      squared_flag).fft().accu_real_copy();

    for (int i = 0; i < ssdmap.size(); i++) ssdmap[i] = ssdmap[i] + LLconst; // multiplicity correction before constant correction

     return { ssdmap, gridding };
  }
} //phasertng
