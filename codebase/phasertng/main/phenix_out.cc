//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/phenix_out.h>

namespace phasertng {

// placeholders for phenix_out object
void phenix_out::write(const std::string& text) { std::cout << text; }

void phenix_out::flush() { std::cout << std::flush; }

phenix_out::~phenix_out() {}

}//namespace phaser
