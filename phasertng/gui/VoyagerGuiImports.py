from __future__ import absolute_import, division, print_function

from crys3d.hklviewer.qt import QWebEngineView
try: # if invoked by cctbx.python or some such
  from crys3d.hklviewer.helpers import HeaderDataTableWidget, MyQDoubleSpinBox # implicit import
except Exception as e: # if invoked by a generic python that doesn't know cctbx modules
  from .helpers import HeaderDataTableWidget, MyQDoubleSpinBox # implicit import

from crys3d.hklviewer.qt import ( QCoreApplication, QMetaObject, QRect, QSize, Qt,  # implicit import
 QFont, QAbstractItemView, QAction, QCheckBox, QComboBox, QLineEdit,
 QDoubleSpinBox, QFrame, QGridLayout, QGroupBox, QLabel, QPlainTextEdit,
 QPushButton, QRadioButton, QScrollArea, QSlider, QSplitter, QSizePolicy, QSpinBox,
 QTableWidget, QtCore, QtWidgets, QTabWidget, QTextEdit, QWidget, QIcon )

import sys
if 'PyQt5' in sys.modules:
  # PyQt5
  from PyQt5 import ( QtCore, QtWidgets, QtGui )   # special import
else:
  # PySide2
  from PySide2 import ( QtCore, QtWidgets, QtGui)   # special import
