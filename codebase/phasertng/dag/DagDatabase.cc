//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <autogen/include/Scope.h>
#include <phasertng/main/constants.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <phasertng/src/SpaceGroup.h>
#include <fstream>

//#define PHASERTNG_DEBUG_WORKING_NODE
//#define PHASERTNG_FAST_INTERPV

namespace phasertng {
namespace dag {

  void
  DagDatabase::initialize_first_node_from_hash_tag(
       std::string hashing,
       std::string tag)
  {
   //have to initialize ultimate here if the init is done BEFORE run-job
   //but currently this is called afterwards
    //PATHWAY.shift_edge(hashing,tag); //init
    //PATHLOG.shift_edge(hashing,tag); //init
    NODES.clear();
    //create the first node
    //tag is for the reflections, different from the ultimate tag
    //node.REFLID = Identifier(DAGDB.PATHWAY.ULTIMATE.identifier(),tag);
    NODES.push_back(dag::Node());
    WORK = &NODES.front();
    //now there is a node, can init the tracker for [0]
    shift_tracker(hashing+"tracker",tag); //init
    //now there is a tracker, we can init the reflid with the tracker
    //return DAGDB.NODES.front().TRACKER.ULTIMATE;
  }

  DogTag
  DagDatabase::initialize_reflection_identifier(std::string tag)
  {
    auto& node = NODES.front();
    //now there is a tracker, we can init the reflid with the tracker
    DogTag reflid;
    reflid.PATHWAY = PATHWAY.ULTIMATE;
    reflid.IDENTIFIER = node.TRACKER.ULTIMATE;
    reflid.shortened = reflid.IDENTIFIER.initialize_tag(tag); //and change the tag
    if (REFLIDS.count(reflid.IDENTIFIER))
      throw Error(err::DEVELOPER,"Reflid already in database "+reflid.IDENTIFIER.str());
    REFLIDS[reflid.IDENTIFIER] = reflid;
    return reflid;
  }

  void
  DagDatabase::initialize_for_twilight_zone(std::string hashing,std::string tag)
  {
    //PATHWAY.shift_edge(hashing,tag); //init, just depends on tag
    //PATHLOG.shift_edge(hashing,tag); //init, just depends on tag
    phaser_assert(!NODES.size());
    NODES.push_back(dag::Node());
    shift_tracker(hashing,tag); //init
    dag::Node& node = NODES.front();
    node.REFLID = Identifier(); // node.TRACKER.ULTIMATE;
    WORK = &NODES.front();
  }

  void
  DagDatabase::clear_unused_entries()
  {
    auto used = NODES.used_modlid(); //uuid
    //below is counterintuitive, but if there are no used entries then this mode is a linker mode, don't mess with the database
    if (used.size() == 0) return;
    typedef mapentry_t::const_iterator citer; //can't copy, problen with Entry destructor
    for (citer iter = ENTRIES.cbegin(); iter != ENTRIES.cend() ; )
    {
      auto modlid = iter->first; //uuid
      iter = used.count(modlid) ? std::next(iter) : ENTRIES.erase(iter);
    }
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
      typedef mapnode_t::const_iterator citer;
      for (citer iter = node->DRMS_SHIFT.cbegin() ; iter !=  node->DRMS_SHIFT.cend() ; )
        iter = !used.count(iter->first) ? node->DRMS_SHIFT.erase(iter) : std::next(iter);
      for (citer iter = node->VRMS_VALUE.cbegin() ; iter !=  node->VRMS_VALUE.cend() ; )
        iter = !used.count(iter->first) ? node->VRMS_VALUE.erase(iter) : std::next(iter);
      for (citer iter = node->CELL_SCALE.cbegin() ; iter !=  node->CELL_SCALE.cend() ; )
        iter = !used.count(iter->first) ? node->CELL_SCALE.erase(iter) : std::next(iter);
    }
  }

  void
  DagDatabase::clear_frf_entries(Identifier search)
  {
    setmodlid_t used;
    used.insert(search);
    for (const auto& node: NODES)
    {
      for (int p = 0; p < node.POSE.size(); p++)
        used.insert(node.POSE[p].IDENTIFIER);
      for (int h = 0; h < node.HOLD.size(); h++)
        used.insert(node.HOLD[h].IDENTIFIER);
      for (int s = 0; s < node.SEEK.size(); s++)
        used.insert(node.SEEK[s].IDENTIFIER);
    }
    for (auto& node: NODES)
    {
      node.FULL.PRESENT = false;
      node.FULL.IDENTIFIER = Identifier();
      node.PART.PRESENT = false;
      node.PART.IDENTIFIER = Identifier();
    }
    typedef mapentry_t::const_iterator citer; //can't copy, problen with Entry destructor
    for (citer iter = ENTRIES.cbegin(); iter != ENTRIES.cend() ; )
    {
      auto modlid = iter->first; //uuid
      iter = used.count(modlid) ? std::next(iter) : ENTRIES.erase(iter);
    }
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
      typedef mapnode_t::const_iterator citer;
      for (citer iter = node->DRMS_SHIFT.cbegin() ; iter !=  node->DRMS_SHIFT.cend() ; )
        iter = !used.count(iter->first) ? node->DRMS_SHIFT.erase(iter) : std::next(iter);
      for (citer iter = node->VRMS_VALUE.cbegin() ; iter !=  node->VRMS_VALUE.cend() ; )
        iter = !used.count(iter->first) ? node->VRMS_VALUE.erase(iter) : std::next(iter);
      for (citer iter = node->CELL_SCALE.cbegin() ; iter !=  node->CELL_SCALE.cend() ; )
        iter = !used.count(iter->first) ? node->CELL_SCALE.erase(iter) : std::next(iter);
    }
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
     // don't clear the packing at the start of the frf
     // because we still need to use the top packing solution in purge of frfr
     // or else we lose the low tfz but packing solutions from the ftfr in the next cycle
     // node->PACKS = '?';
     // node->SPECIAL_POSITION = '?';
     // node->CLASH_WORST = 0;
      node->LLG = 0;
      node->COMPOSITION.clear(); //controversial, we only use this for sad phasing
      node->SEEK_COMPONENTS_ELLG = 0;
      node->RFACTOR = 0;
    }
  }

  void
  DagDatabase::clear_packing()
  {
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
      node->PACKS = pod::pakflag().unset(); // '?'
      node->SPECIAL_POSITION = '?';
      node->CLASH_WORST = 0;
    }
  }

  void
  DagDatabase::clear_cards()
  {
    negvar = false;
    NODES = dag::NodeList();
  }

  void
  DagDatabase::parse_cards(std::string cards)
  {
    clear_cards();
    append_parse_cards(cards);
  }

  void
  DagDatabase::reset_entry_pointers(std::string database)
  {
    //now set all the lookups, which are ptr back to the ENTRY in ENTRIES
    auto used = NODES.used_modlid(); //uuid
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
      for (int p = 0; p < node->POSE.size(); p++)
        node->POSE[p].ENTRY = lookup_entry(node->POSE[p].IDENTIFIER,database);
      if (node->NEXT.IDENTIFIER.is_set_valid())//no! and node->NEXT.PRESENT)
        node->NEXT.ENTRY = lookup_entry(node->NEXT.IDENTIFIER,database);
      for (int g = 0; g < node->NEXT.GYRE.size(); g++)
        node->NEXT.GYRE[g].ENTRY = lookup_entry(node->NEXT.GYRE[g].IDENTIFIER,database);
      for (int s = 0; s < node->SEEK.size(); s++)
        node->SEEK[s].ENTRY = lookup_entry(node->SEEK[s].IDENTIFIER,database);
      for (int h = 0; h < node->HOLD.size(); h++)
        node->HOLD[h].ENTRY = lookup_entry(node->HOLD[h].IDENTIFIER,database);
      if (node->FULL.IDENTIFIER.is_set_valid())//no! and node->FULL.PRESENT)
        node->FULL.ENTRY = lookup_entry(node->FULL.IDENTIFIER,database);
      if (node->PART.IDENTIFIER.is_set_valid())//no! and node->PART.PRESENT)
        node->PART.ENTRY = lookup_entry(node->PART.IDENTIFIER,database);
      if (!SPACEGROUPS.count(node->HALL))
        SPACEGROUPS[node->HALL] = SpaceGroup(node->HALL);
      node->SG = &SPACEGROUPS[node->HALL];
      typedef mapnode_t::const_iterator citer;
      for (citer iter = node->DRMS_SHIFT.cbegin() ; iter !=  node->DRMS_SHIFT.cend() ; )
        iter = !used.count(iter->first) ? node->DRMS_SHIFT.erase(iter) : std::next(iter);
      for (citer iter = node->VRMS_VALUE.cbegin() ; iter !=  node->VRMS_VALUE.cend() ; )
        iter = !used.count(iter->first) ? node->VRMS_VALUE.erase(iter) : std::next(iter);
      for (citer iter = node->CELL_SCALE.cbegin() ; iter !=  node->CELL_SCALE.cend() ; )
        iter = !used.count(iter->first) ? node->CELL_SCALE.erase(iter) : std::next(iter);
      for (auto modlid : used)
      {
        auto entry = lookup_entry(modlid,database);
        PHASER_ASSERT(entry->VRMS_START.size());
        if (!node->DRMS_SHIFT.count(modlid)) node->DRMS_SHIFT[modlid] = dag::node_t(modlid,0);
        if (!node->VRMS_VALUE.count(modlid)) node->VRMS_VALUE[modlid] = dag::node_t(modlid,entry->VRMS_START[0]);
        if (!node->CELL_SCALE.count(modlid)) node->CELL_SCALE[modlid] = dag::node_t(modlid,1);
      }
    }
  }

  void
  DagDatabase::reset_work_entry_pointers(std::string database)
  {
    //now set all the lookups, which are ptr back to the ENTRY in ENTRIES
    //use the lookup on identifier as it is faster
    for (int p = 0; p < WORK->POSE.size(); p++)
      WORK->POSE[p].ENTRY = lookup_entry(WORK->POSE[p].IDENTIFIER,database);
    if (WORK->NEXT.IDENTIFIER.is_set_valid())
      WORK->NEXT.ENTRY = lookup_entry(WORK->NEXT.IDENTIFIER,database);
    for (int g = 0; g < WORK->NEXT.GYRE.size(); g++)
      WORK->NEXT.GYRE[g].ENTRY = lookup_entry(WORK->NEXT.GYRE[g].IDENTIFIER,database);
    for (int s = 0; s < WORK->SEEK.size(); s++)
      WORK->SEEK[s].ENTRY = lookup_entry(WORK->SEEK[s].IDENTIFIER,database);
    for (int h = 0; h < WORK->HOLD.size(); h++)
      WORK->HOLD[h].ENTRY = lookup_entry(WORK->HOLD[h].IDENTIFIER,database);
    if (WORK->FULL.IDENTIFIER.is_set_valid())
      WORK->FULL.ENTRY = lookup_entry(WORK->FULL.IDENTIFIER,database);
    if (WORK->PART.IDENTIFIER.is_set_valid())
      WORK->PART.ENTRY = lookup_entry(WORK->PART.IDENTIFIER,database);
    if (!SPACEGROUPS.count(WORK->HALL))
      SPACEGROUPS[WORK->HALL] = SpaceGroup(WORK->HALL);
    WORK->SG = &SPACEGROUPS[WORK->HALL];
  }

  void
  DagDatabase::apply_space_group_expansion(std::vector<cctbx::sgtbx::space_group_type> SGALT)
  {
    dag::NodeList new_node_list;
    //we need to expand the parent list too because the sg is different
    //only called for first tf, so no poses, each node is separate parent
    Identifier parent;
    parent.initialize_from_text(svtos(WORK->logPose()));//same for same background
    parent.initialize_tag("frf"+ntos(1,NODES.size()));
    int n(2); //first already set
    for (const auto& node: NODES)
    {
      phaser_assert(node.POSE.size() == 0);
      for (const auto& sg : SGALT)
      {
        dag::Node next = node;
        next.HALL = SpaceGroup("Hall: " + sg.hall_symbol()).HALL;
        next.SG = nullptr; //explicit set here
        if (!next.PARENTS.size())
          next.PARENTS = {0,parent.identifier()}; //resize and init
        else
        {
          next.PARENTS[0] = next.PARENTS[1];
          next.PARENTS[1] = parent.identifier();
        }
        new_node_list.push_back(next); //copy
        parent.increment("frf"+ntos(n++,NODES.size()));
      }
    }
    NODES = new_node_list;
  }

  void
  DagDatabase::apply_drms(Identifier search,std::pair<double,double> shifts)
  {
    for (auto& node: NODES)
    {
      //phaser_assert(node.POSE.size() == 0);
      node.DRMS_SHIFT[search].VAL = shifts.first;
      node.VRMS_VALUE[search].VAL = shifts.second;
      phaser_assert(node.CELL_SCALE.count(search));
    }
  }

  void
  DagDatabase::apply_cell(Identifier search,double scale)
  {
    for (auto& node: NODES)
    {
      //will create if not present
      node.CELL_SCALE[search].VAL = scale;
    }
  }

  void
  DagDatabase::apply_reflid_expansion(
      std::vector<Identifier> reflids,
      std::vector<UnitCell> unitcells)
  {
    int n = reflids.size();
    phaser_assert(unitcells.size() == n);
    dag::NodeList new_node_list;
    //replaces the original reflid with only the ones in the list
    std::string p1 = SpaceGroup("P 1").HALL;
    for (const auto& node: NODES)
    {
      for (int i = 0; i < reflids.size(); i++)
      {
        //check that all space groups are p1 so that there is no change in space group
        if (node.HALL != p1)
          throw Error(err::DEVELOPER,"Space group for reflection expansion must be P1");
        dag::Node next = node;
        next.REFLID = reflids[i]; //explicit set here
        next.CELL = unitcells[i].cctbxUC; //explicit set here
        new_node_list.push_back(next); //copy
      }
    }
    NODES = new_node_list;
  }

  std::set<Identifier>
  DagDatabase::set_of_entries()
  {
    std::set<Identifier> set_entries;
    for (const auto& item : ENTRIES)
    {
      set_entries.insert(item.first);
    }
    return set_entries;
  }

  void
  DagDatabase::apply_interpolation(interp::point interpolation,LatticeTranslocation LT)
  { //disorder has been prefiltered by LatticeTranslocation
    for (auto& item : ENTRIES)
    {
      item.second.INTERPOLATION.DISORDER = LT.DISORDER;
      if (interpolation == interp::FOUR)
        //four pt Ecalc, error for TGH
        (LT.HAS_LTD) ?
          item.second.INTERPOLATION.set_four_point_ltd():
          item.second.INTERPOLATION.set_four_point();
      else if (interpolation == interp::EIGHT)
        //eight pt Ecalc, linear for TGH
        (LT.HAS_LTD) ?
          item.second.INTERPOLATION.set_eight_point_ltd():
          item.second.INTERPOLATION.set_eight_point();
      else if (interpolation == interp::TRICUBIC)
        //experimental, slow
        (LT.HAS_LTD) ?
          item.second.INTERPOLATION.set_tricubic_point_ltd():
          item.second.INTERPOLATION.set_tricubic_point();
    }
  }

  std::vector<uuid_t>
  DagDatabase::set_of_parents()
  {
    std::set<uuid_t> tmp; //for uniqueness
    std::vector<uuid_t> result; //ordered list
    for (const auto& item : NODES)
    { //only called if there are parents expected
      if (item.PARENTS.size())
      {
        auto parent = item.PARENTS.back();
        if (tmp.count(parent) == 0)
          result.push_back(parent);
        tmp.insert(parent);
      }
    }
    return result;
  }

  std::vector<Identifier>
  DagDatabase::set_of_reflid()
  {
    std::set<Identifier> tmp; //for uniqueness
    std::vector<Identifier> result; //ordered list
    for (const auto& item : NODES)
    {
      auto reflid = item.REFLID;
      if (tmp.count(reflid) == 0)
        result.push_back(reflid);
      tmp.insert(reflid);
    }
    return result;
  }

  std::vector<uuid_t>
  DagDatabase::set_of_grandparents()
  {
    std::set<uuid_t> tmp; //for uniqueness
    std::vector<uuid_t> result; //ordered list
    for (const auto& item : NODES)
    { //only called if there are parents expected
      if (item.PARENTS.size())
      {
        auto parent = item.PARENTS.front();
        if (tmp.count(parent) == 0)
          result.push_back(parent);
        tmp.insert(parent);
      }
    }
    return result;
  }

  dag::Entry*
  DagDatabase::add_entry(Identifier IDENTIFIER)
  {
    //if present, don't create again
    phaser_assert(IDENTIFIER.is_set_valid());
    if (!ENTRIES.count(IDENTIFIER))
    {
      DogTag dogtag(PATHWAY.ULTIMATE,IDENTIFIER);
      ENTRIES[IDENTIFIER] = dag::Entry(dogtag);
    }
    //std::unordered_map No iterators or references are invalidated.
    return &ENTRIES[IDENTIFIER];
  }

  void
  DagDatabase::add_entry_copy_header(Identifier IDENTIFIER,Identifier other)
  {
    //if present, don't create again
    phaser_assert(IDENTIFIER.is_set_valid());
    if (!ENTRIES.count(IDENTIFIER))
      ENTRIES[IDENTIFIER] = DogTag(PATHWAY.ULTIMATE,IDENTIFIER);
    //std::unordered_map No iterators or references are invalidated.
    phaser_assert(ENTRIES.count(other));
    ENTRIES[IDENTIFIER].copy_header(ENTRIES[other]);
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node* node = &NODES[i];
      node->DRMS_SHIFT[IDENTIFIER] = node->DRMS_SHIFT[other];
      node->VRMS_VALUE[IDENTIFIER] = node->VRMS_VALUE[other];
      node->CELL_SCALE[IDENTIFIER] = node->CELL_SCALE[other];
    }
  }

  bool
  DagDatabase::at_end()
  {
    if (inext == NODES.size())
    { //reset interator before return
      inext = 0;
      WORK = &NODES.front(); //shallow copy
      return true;
    }
    WORK = &NODES[inext]; //shallow copy
    inext++;
    return false;
  }

  void
  DagDatabase::reset_work_pointer()
  {
    WORK = &NODES[inext-1]; //shallow copy
  }

  void
  DagDatabase::restart(unsigned seed)
  {
    inext = 0;
    WORK = nullptr;
    if (!NODES.size()) return;
    WORK = &NODES.front();
  }

  double
  DagDatabase::lowest_hires() const
  {
    //returns the lowest resolution of the hires configurations in node
    //worst case senario for using data resolution
    auto used_modlid = WORK->used_modlid(true);
    double lowest_high_resolution = 0;
    for (const auto& modlid : used_modlid)
    {
      PHASER_ASSERT(ENTRIES.count(modlid));
      const auto& entry = ENTRIES.at(modlid);
      lowest_high_resolution = std::max(lowest_high_resolution,entry.HIRES);
    }
    return lowest_high_resolution;
  }

  double
  DagDatabase::highest_lores() const
  {
    //returns the highest resolution of the lores configurations in node
    //worst case senario for interpolation
    auto used_modlid = WORK->used_modlid(true);
    double highest_low_resolution = DEF_LORES;
    for (const auto& modlid : used_modlid)
    {
      PHASER_ASSERT(ENTRIES.count(modlid));
      const auto& entry = ENTRIES.at(modlid);
      if (!entry.ATOM) //which is never interpolated
        highest_low_resolution = std::min(highest_low_resolution,entry.LORES);
    }
    return highest_low_resolution;
  }

  std::pair<double,double>
  DagDatabase::resolution_range() const
  { return { highest_lores(),lowest_hires()}; }

  dag::Entry*
  DagDatabase::lookup_entry_tag(Identifier& identifier,std::string database)
  {
    //if (!ENTRIES.size())
    //  throw Error(err::DEVELOPER,"No entries in search for " + identifier.str());
    const uuid_t UUID  = identifier.identifier();
    const std::string TAG = identifier.tag();
    for (auto& entry : ENTRIES)
    {
      auto& item = entry.first;
      if (UUID == item.identifier())
      {
        if (TAG != item.tag())
          throw Error(err::DEVELOPER,"Tags inconsistent for Identifier: lookup=" + identifier.str() + " != " + item.str());
        //return std::make_shared<dag::Entry>(std::move(item));
        return &entry.second;
      }
    }
    int count(0); //need to check there are not multiple entries first
    for (auto& entry : ENTRIES)
    {
      auto& item = entry.first;
      if (TAG == item.tag())
      {
        count++;
      }
    }
    FileSystem filesystem;
    if (count == 0 and UUID == 0)
    { //just use the tag to find the file and therefore the identifier
      std::string ending = Identifier(0,TAG).other_data_stem_ext(other::ENTRY_CARDS);
      filesystem.SetFileSystem(database,{Identifier().reflections_and_models(),""}); //directory only
      filesystem.findFileWithEnding(ending);
    }
    else
    {
      filesystem.SetFileSystem(database,identifier.FileDataBase(other::ENTRY_CARDS));
    }
    if (count == 0)// and database.size())
    {
      PHASER_ASSERT(database.size());

    std::ifstream infile(filesystem.fstr().c_str(), std::ios::in | std::ios::binary);
    if (!infile.is_open()) throw Error(err::FILEOPEN,"Loading entry: " + filesystem.fstr());
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    infile.close();
    std::istringstream input_stream(buffer.str());

      input_stream.seekg(0);
      if (input_stream)
      {
        compulsoryKeyStr(input_stream,NODE_SCOPE());
        compulsoryKeyStr(input_stream,"entry");
        DogTag dogtag;
        dogtag.initialize(get_dogtag(input_stream));
        if (!dogtag.is_set_valid())
        throw Error(err::FATAL,"Identifier does not exist: " + identifier.str());
        parse_entry_data(input_stream,dogtag);
        skip_line(input_stream);
      }
      for (auto& entry : ENTRIES)
      {
        auto& item = entry.first;
        if (TAG == item.tag())
        {
          count++;
        }
      }
    }
    if (count == 0 and UUID)
      throw Error(err::DEVELOPER,"Identifier number not found: " + identifier.str());
    if (count == 0)
      throw Error(err::DEVELOPER,"Identifier tag not found: " + identifier.str());
    if (count >= 2)
      throw Error(err::DEVELOPER,"Identifier not unique: " + identifier.str());
    //ok return pointer
    PHASER_ASSERT(count == 1);
    for (auto& entry : ENTRIES)
    {
      auto& item = entry.first;
      if (TAG == item.tag())
      {
        return &entry.second;
      }
    }
    throw Error(err::DEVELOPER,"Compilation Return Triggered");
    return nullptr;
  }

  dag::Entry*
  DagDatabase::lookup_entry(Identifier& modlid,std::string database)
  {
    phaser_assert(modlid.is_set_valid());
    if (!ENTRIES.count(modlid) and database.size()) //database is set
    {
      FileSystem filesystem;
      filesystem.SetFileSystem(database,modlid.FileDataBase(other::ENTRY_CARDS));

    std::ifstream infile(filesystem.fstr().c_str(), std::ios::in | std::ios::binary);
    if (!infile.is_open()) throw Error(err::FILEOPEN,"Loading entry: " + filesystem.fstr());
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    infile.close();
    std::istringstream input_stream(buffer.str());

      input_stream.seekg(0);
      if (input_stream)
      {
        compulsoryKeyStr(input_stream,NODE_SCOPE());
        compulsoryKeyStr(input_stream,"entry");
        DogTag dogtag;
        dogtag.initialize(get_dogtag(input_stream));
        PHASER_ASSERT(dogtag.is_set_valid());
        parse_entry_data(input_stream,dogtag);
        skip_line(input_stream);
      }
    }
    phaser_assert(ENTRIES.count(modlid)); //because database could be not set
    return &ENTRIES[modlid];
  }

  void
  DagDatabase::shift_tracker(std::string hashing,std::string tag)
  {
    int N = NODES.size();
    if (!N) return;
    for (int i = 0; i < NODES.size(); i++)
    {
      //adding the tag to the node guarantees a change in the string each time
      //since we cannot check that the values of the node have changed (yet, or ever)
      //in addition! sometimes the nodes are the same! add index counter
      auto& node = NODES[i];
      std::string tagnN = tag + "-" + ntos(i+1,N);
      //use the current tracker as the hash value!! infinitessimal difference avoided
      node.TRACKER.shift_edge(hashing+tagnN,tagnN);
      PHASER_ASSERT(node.TRACKER.ULTIMATE != node.TRACKER.PENULTIMATE);
    }
  }

  bool
  DagDatabase::solved(double llg,double tfz)
  {
    if (!NODES.size()) return false;
    return (NODES[0].LLG > llg and NODES[0].ZSCORE > tfz);
  }

  std::unordered_map<uuid_t,std::string>
  DagDatabase::parent_flags()
  {
    auto parents = set_of_parents();
    pdb_chains_2chainid chains;
    std::unordered_map<uuid_t,std::string> result;
    for (const auto& item : parents)
      result[item] = chains.allocate(" A");
    return result;
  }

  void
  DagDatabase::permute_seek(Identifier id)
  {
    //id is the identifier if the next seek
    int pswap(-999),pseek(-999); //flag not set
    for (int p = 0; p < WORK->SEEK.size(); p++)
    {
      //find first matching the seek
      if (!WORK->SEEK[p].is_found() and pseek < 0 and WORK->SEEK[p].IDENTIFIER == id)
         pseek = p;
    }
    for (int p = 0; p < WORK->SEEK.size(); p++)
    {
      //find first for the swap which is any as long it is not pseek
      if (!WORK->SEEK[p].is_found() and pswap < 0 and pswap != pseek)
         pswap = p;
    }
    if (pswap>=0 and pseek>=0) //ie not only one left
      for (auto& node : NODES)
        std::swap(node.SEEK[pswap],node.SEEK[pseek]);
  }

  void DagDatabase::clear()
  {
    clear_cards();
    PATHWAY = Edge();
    PATHLOG = Edge();
    ENTRIES = mapentry_t();
    SPACEGROUPS = std::map<std::string,SpaceGroup>();
  }

  DogTag
  DagDatabase::lookup_reflections(Identifier& reflid,std::string database)
  {
    if (!REFLIDS.count(reflid) and database.size()) //database is set
    {
      FileSystem filesystem;
      filesystem.SetFileSystem(database,reflid.FileDataBase(other::ENTRY_CARDS));

    std::ifstream infile(filesystem.fstr().c_str(), std::ios::in | std::ios::binary);
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    infile.close();
    std::istringstream input_stream(buffer.str());

      if (infile.is_open())
      {
        input_stream.seekg(0);
        if (input_stream)
        {
          compulsoryKeyStr(input_stream,NODE_SCOPE());
          compulsoryKeyStr(input_stream,"reflections");
          DogTag dogtag;
          dogtag.initialize(get_dogtag(input_stream));
          PHASER_ASSERT(dogtag.is_set_valid());
          REFLIDS[dogtag.identify()] = dogtag;
          skip_line(input_stream);
        }
      }
    }
    if (!REFLIDS.count(reflid))
      throw Error(err::INPUT,"Reflections identifier not in database " + reflid.str());
    return REFLIDS[reflid];
  }

  DogTag
  DagDatabase::lookup_reflid(Identifier& reflid)
  {
    if (!REFLIDS.count(reflid))
      throw Error(err::INPUT,"Reflection identifier not in database " + reflid.str());
    return REFLIDS[reflid];
  }

  DogTag
  DagDatabase::lookup_modlid(Identifier& modlid)
  {
    if (!ENTRIES.count(modlid))
      throw Error(err::INPUT,"Model (entry) identifier not in database");
    return ENTRIES[modlid].DOGTAG;
  }

  DogTag
  DagDatabase::lookup_tag(std::string tag)
  {
    for (auto entry : ENTRIES)
      if (entry.first.tag() == tag)
        return entry.second.DOGTAG;
    for (auto entry : REFLIDS)
      if (entry.first.tag() == tag)
        return entry.second;
    throw Error(err::INPUT,"Tag not in database");
    return DogTag(); //compilation
  }

  std::vector<DogTag>
  DagDatabase::unparse_dogtags() const
  {
    std::vector<DogTag> cards;
    for (const auto& reflid : REFLIDS)
      cards.push_back(reflid.second);
    return cards;
  }

  void
  DagDatabase::parse_dogtags(std::vector<DogTag> cards)
  {
    for (const auto& reflid : cards)
      REFLIDS[reflid.IDENTIFIER] = reflid;
  }

  setmodlid_t
  DagDatabase::all_modlid() const
  {
    setmodlid_t modlid;
    for (const auto& item: ENTRIES)
    { //just pick out the original entries, not the rbm ones
      std::string modestr = item.second.DOGTAG.PATHWAY.tag();
      if (modestr == "esm" or modestr == "ens" or modestr == "map")
        modlid.insert(item.first);
    }
    return modlid;
  }

  std::string
  DagDatabase::polarity() const
  {
    if (size())
    {
      SpaceGroup hall(NODES.front().HALL);
      if (hall.is_p1()) return "is_p1";
      if (hall.is_polar()) return "is_polar";
    }
    return "is_nonpolar";
  }

  double
  DagDatabase::solved_tfz(double nonpolar,double polar,double p1)
  {
    if (NODES.number_of_poses() > 0)
      return nonpolar;
    if (polarity() == "is_nonpolar")
      return nonpolar;
    if (polarity() == "is_polar")
      return polar;
    return p1;
  }


}} //phasertng

#include <phasertng/dag/include/DagLikelihoodTfz.cpp>
#include <phasertng/dag/include/DagPhasedLikelihood.cpp>
#include <phasertng/dag/include/DagEcalcsSigmaa.cpp>
#include <phasertng/dag/include/DagLikelihood.cpp>
#include <phasertng/dag/include/ListEditingPose.cpp>
#include <phasertng/dag/include/ListEditingGyre.cpp>
#include <phasertng/dag/include/ListEditingTurn.cpp>
#include <phasertng/dag/include/ListEditingAtom.cpp>
#include <phasertng/dag/include/ListEditingPack.cpp>
#include <phasertng/dag/include/CentroSymmetry.cpp>
#include <phasertng/dag/include/DagReadWrite.cpp>
#include <phasertng/dag/include/DagBoostPython.cpp>
#include <phasertng/dag/include/DagPhilWrite.cpp>
