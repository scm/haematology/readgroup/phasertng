from cctbx import crystal
import cctbx.crystal.close_packing
from cctbx import sgtbx
import argparse
import sys
from libtbx.phil import parse
from phasertng.phil.master_phil_file import build_phil_str
from phasertng.phil.converter_registry import converter_registry

def sampling_generator_info(
        crystal_symmetry,
        symmetry_flags,
        point_distance,
        buffer_thickness=None,
        all_twelve_neighbors=None):

  initparams = build_phil_str(program_name="InputCard")
  assert(len(initparams))
  master_phil = parse(initparams,converter_registry=converter_registry)
  #user_phil = parse(user_phil_str,converter_registry=converter_registry)
  #working_params = master_phil.fetch(source=user_phil).extract()
  working_params = master_phil.fetch(source=master_phil).extract()

  if (buffer_thickness is None): buffer_thickness = -1
  if (all_twelve_neighbors is None): all_twelve_neighbors = False
  s = crystal.close_packing.setup_hexagonal_sampling(
    crystal_symmetry=crystal_symmetry,
    symmetry_flags=symmetry_flags)
  symbol = "\"" + crystal_symmetry.space_group().type().universal_hermann_mauguin_symbol() + "\""
  #working_params.phasertng.sampling_generator.symbol = crystal_symmetry.space_group().type()
  #symbol = "phasertng.sampling_generator.symbol = " + symbol
 #print(symbol)
  chop = "\"" + str(s.cb_op_original_to_sampling) + "\""
  working_params.phasertng.sampling_generator.change_of_basis_op = chop
  chop = "phasertng.sampling_generator.change_of_basis_op  = " + chop
 #print chop
  x = "1" if s.continuous_shift_flags[0] else "0"
  y = "1" if s.continuous_shift_flags[1] else "0"
  z = "1" if s.continuous_shift_flags[2] else "0"
  continuous_shifts = x + " " + y + " " + z
  working_params.phasertng.sampling_generator.continuous_shifts = s.continuous_shift_flags
  continuous_shifts = "phasertng.sampling_generator.continuous_shifts = " + continuous_shifts
 #print continuous_shifts
  normals = ""
  for i in s.float_asu.cuts():
    n = str(i.n).replace("(","").replace(",","").replace(")","")
    normals = normals + "  " + n
  working_params.phasertng.sampling_generator.cuts_normals = normals
  normals = "phasertng.sampling_generator.cuts_normals =" + normals
 #print(normals)
  constants = ""
  for i in s.float_asu.cuts():
    c = str(i.c)
    constants = constants + " " + c
  working_params.phasertng.sampling_generator.cuts_constants = constants
  constants = "phasertng.sampling_generator.cuts_constants = " + constants
 #print(constants)
  phil_str = ""
  phil_str = phil_str+ symbol + "\n"
  phil_str = phil_str+ chop + "\n"
  phil_str = phil_str+ continuous_shifts + "\n"
  phil_str = phil_str+ normals + "\n"
  phil_str = phil_str+ constants + "\n"
  return phil_str

def sampling_generator(
        unit_cell=None,
        space_group_symbol=None,
        use_semivariants=None,
        use_space_group_symmetry=None):
  crystal_symmetry = crystal.symmetry(
    unit_cell,
    space_group_symbol)
  phil_str = sampling_generator_info(
    crystal_symmetry=crystal_symmetry,
    symmetry_flags=sgtbx.search_symmetry_flags(
      use_space_group_symmetry=use_space_group_symmetry,
      use_seminvariants=True,
      use_normalizer_k2l=False,
      use_normalizer_l2n=False),
    point_distance=2)
  sampling_generator = crystal.close_packing.hexagonal_sampling(
    crystal_symmetry=crystal_symmetry,
    symmetry_flags=sgtbx.search_symmetry_flags(
      use_space_group_symmetry=use_space_group_symmetry,
      use_seminvariants=use_semivariants,
      use_normalizer_k2l=False,
      use_normalizer_l2n=False),
    point_distance=2)
  phil_str = phil_str + "phasertng.sampling_generator.change_of_basis_op  = " + str(use_semivariants) + "\n"
  return phil_str, sampling_generator.count_sites()

def sampling_generator_test(unit_cell=None,space_group_symbol=None):
  for use_semivariants in [True, False]:
    for use_space_group_symmetry in [True, False]:
      phil_str,count = sampling_generator(
          unit_cell,
          space_group_symbol,
          use_semivariants,
          use_space_group_symmetry)
      print("Sampling Generator Test:")
      print("  number of sampling points:", count)
      print("  use semivariants :", str(use_semivariants))
      print("  use space group symmetry :", str(use_space_group_symmetry))
      print(phil_str)
      print("")

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-uc',"--unitcell",help="unitcell",required=False,default="255.260  265.250  184.400  90.00  90.00 90.00")
  parser.add_argument('-sg',"--spacegroup",help="spacegroup",required=False,default="P 21 21 2")
  parser.add_argument('-semi',"--use_semivariants",help="T/F",type=bool,required=False,default=True)
  parser.add_argument('-symm',"--use_space_group_symmetry",help="T/F",type=bool,required=False,default=False)
  args = parser.parse_args()
  print("Sampling generator options:")
  sampling_generator_test(unit_cell=args.unitcell,space_group_symbol=args.spacegroup)
  print("Phil:")
  phil_str,count = sampling_generator(unit_cell=args.unitcell,space_group_symbol=args.spacegroup)
  print(phil_str)
  sys.exit(0)
