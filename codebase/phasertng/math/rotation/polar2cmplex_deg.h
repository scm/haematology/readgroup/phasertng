#ifndef __phasertng_polar2cmplex_function__
#define __phasertng_polar2cmplex_function__
#include <scitbx/constants.h>

namespace phasertng {

inline cmplex polar2cmplex_deg(std::pair<double,double> FCALC)
{
  //not pass by reference, as needs to return new value anyway
  double F = std::abs(FCALC.first);
  double PHI = scitbx::deg_as_rad(FCALC.second);
  return std::polar(F, PHI);
}

}
#endif
