import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys,os
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.mode_generator import *
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng import InputCard,Phasertng,PhaserError,enum_out_stream
from phasertng.run.ModeRunner import *
import argparse

def nomad(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_nomad)

def inner_nomad(basetng,user_phil_str,progress_callback=None):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="nomad")
    initparams = initparams +  "\n" +  build_auto_str("nomad")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    assert(dbdir is not None)
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    loop_auto = str("Nomad")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout):
      os.remove(xmlout)
    #chk does not contribute to the DAG
    ModeRunner(basetng=basetng,mode_list=['chk'])

    #first check that there are some files, or else we don't fail until auto_search called
    #nb auto search calls this function again internally
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse(user_cards,ignore_unknown_keys)
    ModeRunner(basetng=basetng,mode_list=['walk'])

    #first choice
    seqin = working_params.nomad.seqin
    if seqin is None:
      #second choice is phasertng
      seqin = isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
    if seqin is None and int(basetng.input.size(".phasertng.file_discovery.validated.seq_files.")) > 0:
      #third choice is discovery
      seqin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.seq_files.",0)
    if seqin is not None: #set to the bub requirement
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(seqin))

    pdbin = []
    nbu = basetng.input.size(".phasertng.biological_unit_builder.model.") #alt direct use input case
    nfd = basetng.input.size(".phasertng.file_discovery.validated.pdb_files.") #file discovery
    #first xyzins
    if len(working_params.nomad.xyzin) > 0:
      basetng.logLoopInfo(enum_out_stream.process,"Use nomad xyzin")
      n = len(working_params.nomad.xyzin)
      basetng.input.resize(".phasertng.biological_unit_builder.model.",n)
      for i,ifile in enumerate(working_params.nomad.xyzin):
        ifile = os.path.normpath(ifile)
        basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(ifile),int(i))
        pdbin.append(ifile)
    #second bu models
    elif int(nbu) > 0:
      basetng.logLoopInfo(enum_out_stream.process,"Use biological unit builder model")
      for i in range(nbu):
        ifile = basetng.input.get_as_str_i(".phasertng.biological_unit_builder.model.",int(i))
        ifile = os.path.normpath(ifile)
        pdbin.append(ifile)
    #third file discovery
    elif int(nfd) > 0:
      basetng.logLoopInfo(enum_out_stream.process,"Use validated pdb files")
      basetng.input.resize(".phasertng.biological_unit_builder.model.",nfd)
      for i in range(nfd):
        ifile = isNone(basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",int(i)))
        ifile = os.path.normpath(ifile)
        basetng.input.set_as_str_i(".phasertng.biological_unit_builder.model.",str(ifile),int(i))
        pdbin.append(ifile)
    if len(pdbin) == 0:
      raise ValueError(enum_err_code.input,"No model file(s)")

    aniso1 = ModeRunner(basetng=basetng,mode_list=['bub']).solution()

    set_of_esm_filenames = sorted(pdbin)
   #{{{
    #generate ensembles
    #ensembles has to be done after the aniso, for the scaling
    loop_ens = "Generate models"
    #we can just change the pathway tracker because pathlog carries the logfile info
    basetng.logLoopOpen(enum_out_stream.process,loop_ens,len(set_of_esm_filenames),
          '\n'.join(n for n in set_of_esm_filenames))
    entries_dagcards = []
    for ifile in set_of_esm_filenames:
      basetng.logLoopNext(enum_out_stream.process,loop_ens,ifile)
      pdbf = os.path.join(ifile)
      basetng.input.set_as_str(".phasertng.model.filename.",pdbf)
    # basetng.input.set_as_str(".phasertng.molecular_transform.preparation.","decomposition")
    # basetng.input.set_as_str(".phasertng.suite.write_data.",str(True))
    # Mode-Runner(basetng=basetng,mode_list=['emm'])
      ModeRunner(basetng=basetng,mode_list=['esm'])
      entries_dagcards.append(basetng.input.generic_string)
      basetng.load_moderunner(aniso1,user_cards,"")
    basetng.logLoopShut(enum_out_stream.process,loop_ens)
   #}}}

    basetng.logLoopShut(enum_out_stream.process,loop_auto)

    ModeRunner(basetng=basetng,mode_list=['tree'])
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  user_phil = args.phil.read()
  (error) = nomad(user_phil_str=user_phil)
  sys.exit(error.exit_code())
