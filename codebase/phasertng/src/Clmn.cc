//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Clmn.h>
#include <phasertng/main/Assert.h>
#include <phasertng/math/rotation/rotationgroup.h>
#include <algorithm>

//#define PHASERTNG_DEBUG_CLMN
#ifdef PHASERTNG_DEBUG_CLMN
#include <iostream>
#endif

namespace phasertng {

  void
  Clmn::clmnx(
      const elmn_array_t& ELMN1,
      const elmn_array_t& ELMN2,
      const int LMIN,
      const int LMAX
    )
  {
    // Calculate the Clmm' from two Elmn lists by summing over the n values
    phaser_assert(LMIN < LMAX);
    int LMINtake2on2 = (LMIN-2)/2;
    int LMAXtake2on2 = (LMAX-2)/2;
    int max_index = LMAX/2;

#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
    clm1m2.resize(max_index);
    for (int l_index = 0; l_index < max_index; l_index++)
    {
      int l=2*l_index+2;
      int twolplus1 = 2*l+1;
      clm1m2[l_index].resize(twolplus1);
      for (int m1 = 0; m1 < twolplus1; m1++)
      {
        clm1m2[l_index][m1].resize(twolplus1);
      }
    }
#else
    cctbx::af::tiny<std::size_t, 3> dimensions = ELMN1.accessor();
    int maxi2(0);
    for (int l_index = 0; l_index < max_index; l_index++)
    {
      int l=2*l_index+2;
      int twolplus1 = 2*l+1;
      maxi2 = std::max(maxi2,twolplus1);
    }
    clm1m2 = elmn_array_t(scitbx::af::c_grid<3>(max_index,maxi2,maxi2));
    //get dim for index array
    int dim1(0),dim2(0);
    for (int l = 2; l <= LMAX; l += 2)
    { //resize elmn matrix
      int l_index = l/2-1;
      for (int m = 0; m <= l; m++)
      {
        dim1 = std::max(dim1,l_index);
        dim2 = std::max(dim2,l+m);
      }
    }
    //fill index array
    scitbx::af::versa<size_t,scitbx::af::c_grid<2>> index(scitbx::af::c_grid<2>(dim1+1,dim2+1));
    for (int l = 2; l <= LMAX; l += 2)
    { //resize elmn matrix
      int l_index = l/2-1;
      int nmax = (LMAX-l+2)/2;
      for (int m = 0; m <= l; m++)
      {
        index(l_index,l+m) = (nmax+1);
        index(l_index,l-m) = (nmax+1);
      }
    }
#endif

    phaser_assert(ELMN1.size());
    phaser_assert(ELMN1.size() == ELMN2.size());

    for (int l_index = 0; l_index < max_index; l_index++)
    {
      int l=2*l_index+2;
      size_t twolplus1 = 2*l+1;
     // size_t indexmax = (LMAX-l+2)/2;
      for (int m1 = 0; m1 < twolplus1; m1++)
      {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
        size_t ELMN1_l_index__m1__size = ELMN1[l_index][m1].size();
#else
        //size_t ELMN1_l_index__m1__size = indexmax; //index(l_index,m1);
        size_t ELMN1_l_index__m1__size = index(l_index,m1);
#endif
        for (int m2 = 0; m2 < twolplus1; m2++)
        {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
          clm1m2[l_index][m1][m2] = cmplex(0,0);
          int nmax = std::min(ELMN1_l_index__m1__size,ELMN2[l_index][m2].size());
#else
          clm1m2(l_index,m1,m2) = cmplex(0,0);
          //int nmax = std::min(ELMN1_l_index__m1__size,indexmax); //index(l_index,m2));
          int nmax = std::min(ELMN1_l_index__m1__size,index(l_index,m2));
#endif
          for (unsigned int n = 1; n < nmax; n++)
          {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
            clm1m2[l_index][m1][m2] += std::conj(ELMN1[l_index][m1][n])*ELMN2[l_index][m2][n];
#else
            clm1m2(l_index,m1,m2) += std::conj(ELMN1(l_index,m1,n))*ELMN2(l_index,m2,n);
#endif
          }
          if (l_index < LMINtake2on2 or l_index > LMAXtake2on2)
          {
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
            clm1m2[l_index][m1][m2] = cmplex(0.0);
#else
            clm1m2(l_index,m1,m2) = cmplex(0.0);
#endif
          }
        }
      }
    }
    phaser_assert(clm1m2.size());
  }

} //phasertng
