from __future__ import print_function
from __future__ import division

from libtbx import test_utils
import libtbx.load_env

def run():
  tst_list = (
  "$D/regression/test_example.py"
  )

  build_dir = libtbx.env.under_build("phasertng")
  dist_dir = libtbx.env.dist_path("phasertng")

  test_utils.run_tests(build_dir, dist_dir, tst_list)

if (__name__ == "__main__"):
  run()
