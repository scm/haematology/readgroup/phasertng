//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_SigmaN_class__
#define __phasertng_SigmaN_class__
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/Loggraph.h>
#include <phasertng/data/best_curve_at_ssqr.h>
#include <cctbx/adptbx.h>

namespace phasertng {

class SigmaN
{
  private:
    table::best_curve_at_ssqr table_best;

  public: //members
    double  SCALE_I = 1;
    dmat6   ANISOBETA; //anisoBeta parameter
    af_double  BINSCALE; //bin correction term
    af_double  MIDRES; //for restraints (midssqr) and information
    UnitCell   UC;

  public: //constructors
    SigmaN() {}
    SigmaN(const cctbx::uctbx::unit_cell& uc,af_double v) : MIDRES(v)
    { UC = UnitCell(uc); ANISOBETA.fill(0); BINSCALE.resize(v.size(),1); }

  public:
    double  AnisoTerm(const millnx&,bool) const; //RemoveAnisoB
    dvect3  EigenBs() const;
    double  AnisoDeltaB() const;
    double  IsoB_F() const;
    double  IsoB_I() const;
    cctbx::adptbx::eigensystem<double> EigenSystem() const;

  /* Isotropic <-> Anisotropic  & Beta <-> U or B conversions */
    double  AnisoBeta2IsoB() const;
    dmat6   AnisoBeta2IsoBCoeffs() const;
    dmat6   AnisoBeta2AnisoB() const;
    dmat6   AnisoBeta2AnisoU() const;
    dmat6   IsoB2AnisoBeta(double=1.0) const;
    void    AnisoU2AnisoBeta(const dmat6&);
    void    AnisoB2AnisoBeta(const dmat6&);
    dmat6   AnisoBetaRemoveIsoB() const;
    dmat6   LargeShiftCoeffs() const;

    const char* c_str(int i)
    {
      if (i == 0) return " HH ";
      if (i == 1) return " KK ";
      if (i == 2) return " LL ";
      if (i == 3) return " HK ";
      if (i == 4) return " HL ";
      if (i == 5) return " KL ";
      return " -- ";
    }

    dmat33 direction_cosines() const;
    double sharpening_bfactor() const;
    double wilson_k_f() const;

  public:
    af_string logAnisotropy(std::string) const;
    af_string logScaling(std::string) const;
    af_string logBinning(std::string,af_int) const;
    const double term(const millnx,const double,const int) const;
    const double best_at_ssqr(const double) const;
    Loggraph  logGraph1(std::string,af_int);
};

}//phasertng
#endif
