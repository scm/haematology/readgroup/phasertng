//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_OUTLIER_CLASS__
#define __PHASER_DATA_OUTLIER_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/pod/probType.h>
#include <phaser/lib/maths.h>

namespace phaser {

struct data_outl
{
    data_outl()
    {
      SIGESQ_CENT = lookup_sigesq_cent(INFO);
      SIGESQ_ACENT = lookup_sigesq_acent(INFO);
      ANO.clear();
      SAD.clear();
    }

  bool   REJECT = true;
  double PROB = 1.e-06;
  double MAXPROB = 0.001;
  double INFO = 0.01;
  double SIGESQ_CENT = lookup_sigesq_cent(INFO);
  double SIGESQ_ACENT;
  af::shared<probTypeANO>  ANO;
  af::shared<probTypeSAD>  SAD;

};

} //phaser

#endif
