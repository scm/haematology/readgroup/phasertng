//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_fasttypes_class__
#define __phasertng_pod_fasttypes_class__
#include <phasertng/math/Matrix.h>
#include <complex>

typedef std::complex<double> cmplex;
typedef std::vector<cmplex> sv_cmplex;
typedef std::vector<double> sv_double;

namespace phasertng {
namespace pod {

  struct FastPoseType
  {
    sv_cmplex ecalcs = sv_cmplex(0);
    sv_double sigmaa = sv_double(0);
    bool      precalculated = false;
    void resize(int n) { ecalcs = sv_cmplex(n,{0,0}); sigmaa = sv_double(n,0); }
    int size() { return ecalcs.size(); }
  };

  struct FastTfzType
  {
    sv_double DobsSigaSqr = sv_double(0);
    math::Matrix<cmplex> Ecalc_isym = math::Matrix<cmplex>();
    void resize(int r,int s) { DobsSigaSqr = sv_double(r,0); Ecalc_isym.resize(r,s,{0,0}); }
  };

} //pod
} //phasertng
#endif
