//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/LlgcMapSites.h>
#include <phasertng/main/jiffy.h>

namespace phaser {

af_string LlgcMapSites::selectTopSites(SpaceGroup sg,UnitCell uc,data_llgc llgc,int numAtoms)
{
  af_string output;
  double DIST = llgc.CLASH;
  output.push_back("");
  output.push_back("--Add Sites Identified from Log-Likelihood Gradient Map(s)--");
  std::vector<std::pair<floatType,int> > sorted_list;
  for (int i = 0; i < atoms.size(); i++)
    sorted_list.push_back(std::pair<floatType,int>(atoms[i].XTRA.zscore,i));
  std::sort(sorted_list.begin(),sorted_list.end());
  std::reverse(sorted_list.begin(),sorted_list.end());
  //now square it so don't have to keep taking sqrt(distance) for comparison
  floatType DISTsqr = fn::pow2(DIST);
  bool1D top_site(sorted_list.size(),false);
  std::multimap<int,int> cluster;
  int natoms(0);
  for (unsigned i = 0; i < sorted_list.size(); i++) //all i
  {
    int j = sorted_list[i].second;
    dvect3 orthRef = uc.doFrac2Orth(atoms[j].SCAT.site);
    for (int i_prev = 0; i_prev < i; i_prev++)
    {
      int j_prev = sorted_list[i_prev].second;
      for (unsigned isym = 0; isym < sg.NSYMM; isym++)
      {
        dvect3 fracPrev = sg.doSymXYZ(isym,atoms[j_prev].SCAT.site);
        dvect3 cellTrans;
        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
            for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
            {
              dvect3 fracPrevCell = fracPrev+cellTrans;
              dvect3 orthPrev = uc.doFrac2Orth(fracPrevCell);
              floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
              if (delta < DISTsqr)
              {
                cluster.insert(std::pair<int,int>(j_prev,j));
                goto next_i;
              }
            } //cells
      } //symm
    }
    // no hits found,this site must be unique
    top_site[j] = true;
    natoms++;
    next_i: continue; //performs the next iteration of the loop.
  }
  if (llgc.TOP_ONLY)//make sure only one is selected
  {
    bool first_found(false);
    for (unsigned j = 0; j < top_site.size(); j++) //all i
      if (first_found) top_site[j] = false;
      else first_found = top_site[j];
    //paranoia recount
    natoms = 0;
    for (unsigned j = 0; j < top_site.size(); j++)
      if (top_site[j]) natoms++;
  }
  output.push_back(std::to_string(natoms) + " atom" + std::string(natoms==1?"":"s") + " after pruning");
  struct resultinfo {
    int a;
    double zscore;
    std::vector<int> j;
    bool operator<(const resultinfo &right) const
    { return zscore > right.zscore; } //reverse sort
  };
  std::vector<resultinfo> result;
  for (int a = 0; a < atoms.size(); a++)
    if (top_site[a])
    {
      resultinfo topsite;
      topsite.a = a;
      topsite.zscore = atoms[a].XTRA.zscore;
      typedef std::multimap<int,int>::iterator I;
      std::pair<I,I> c = cluster.equal_range(a);
      for (I iter=c.first; iter != c.second; iter++)
      { //these are already is zscore order so just store as a vector (ordered)
        topsite.j.push_back(iter->second);
      }
      result.push_back(topsite);
    }
  std::sort(result.begin(),result.end()); //large to small zscore
  int io_nprint=20;
  if (natoms)
  {
    output.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %12s %4s %7s", "#", "X","Y","Z","occupancy","Atom","Z/M"));
    int b(0);
    for (b = 0; b < result.size(); b++)
    {
      int a = result[b].a;
      output.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %12.4f %4s %7.1f",
      ++numAtoms,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str(),atoms[a].XTRA.zscore));
      for (auto j : result[b].j)
      {
        output.push_back(phasertng::snprintftos("%5s %6.3f %6.3f %6.3f %12.4f %4s %7.1f",
        "=",atoms[j].SCAT.site[0],atoms[j].SCAT.site[1],atoms[j].SCAT.site[2],atoms[j].SCAT.occupancy,atoms[j].SCAT.scattering_type.c_str(),atoms[j].XTRA.zscore));
      }
      if (b == io_nprint) break;
    }
    if (b < natoms)
    {
      int more = natoms-b;
      output.push_back("--- etc " + std::to_string(more) + " more");
      output.push_back("");
    }
  }
  //erase from end to preserve numbering
  for (int j = 0; j < atoms.size(); j++)
    atoms[j].generic_bool = top_site[j]; //so carried with sort
  auto cmp = [](const data_atom& a, const data_atom& b)
  { return a.XTRA.zscore > b.XTRA.zscore; };
  std::sort(atoms.begin(),atoms.end(),cmp);
  int last(0);
  for (int j = 0; j < atoms.size(); j++)
  if (atoms[j].generic_bool) //aka top_site
    atoms[last++] = atoms[j];
  atoms.erase(atoms.begin()+last,atoms.end()); //erase the rubbish
  return output;
}

}//phaser
