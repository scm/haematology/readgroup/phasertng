#ifndef __phasertng_rotdist__
#define __phasertng_rotdist__
#include <utility>
#include <scitbx/math/euler_angles.h>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/array_family/shared.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::af::shared<dvect3> af_dvect3;

namespace phasertng {

  inline dmat33
  zyz_matrix(const dvect3& euler)
  { return scitbx::math::euler_angles::zyz_matrix(euler[0],euler[1],euler[2]); }

  std::pair<dvect3,double>
  axis_and_angle_deg(const dmat33&,const double=5);

  std::pair<dvect3,double>
  axis_and_angle_deg(const dvect3&,const double=5);

  double
  rotation_angle_cosine(const dmat33&);

  double
  regularize_angle(const double& );

  double
  euler_alpha_plus_gamma(const dvect3& );

  dvect3
  regularize_angle(const dvect3& );

  double
  euler_delta_deg(const dvect3& ,const dvect3& );

  double
  matrix_delta_rad(const dmat33& ,const dmat33& );

  double
  matrix_delta_deg(const dmat33& ,const dmat33& );

  double
  axis_delta_deg(const dmat33& ,const dmat33& );

} //end namespace phasertng

#endif
