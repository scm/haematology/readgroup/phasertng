//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_SingleAtoms_class__
#define __phasertng_SingleAtoms_class__
#include <phasertng/io/entry/Atoms.h>

namespace phasertng {

class SingleAtoms : public Atoms
{
  public:
    std::vector<pod::gradient::mean::scatterer> GRAD;
    std::vector<pod::hessian::mean::scatterer>  HESS;
    std::map<std::string,double> dReF_by_dFdp; //initialize below
    std::map<std::string,double> dImF_by_dFdp; //initialize below
    bool REFINE_SITE = false;
    bool REFINE_BFAC = false;
    bool REFINE_OCC = false;

  public: //constructors
    SingleAtoms() : Atoms()
    { }

  public:
    //setters
    SingleAtoms& refine_site(bool b) { REFINE_SITE = b; return *this; }
    SingleAtoms& refine_bfac(bool b) { REFINE_BFAC = b; return *this; }
    SingleAtoms& refine_occ(bool b)  { REFINE_OCC = b; return *this; }

    cmplex
    calcAtomicDataSum(
        const millnx&,
        const double,
        const double,
        const Scatterers&,
        bool,
        bool
    );

};

} //phasertng
#endif
