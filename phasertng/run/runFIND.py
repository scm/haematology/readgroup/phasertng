import sys
from cctbx.array_family import flex
from libtbx.utils import Sorry
from mmtbx import utils
from iotbx import pdb,reflection_file_utils,pdb,extract_xtal_data
import iotbx,iotbx.phil
from six.moves import cStringIO as StringIO
import mmtbx.model,mmtbx.f_model
import argparse,copy
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
import os, os.path

#these are internal parameters not interesting for the user
class runFIND:

  def run_from_basetng_and_input(self,basetng):
    self.inner_find(basetng=basetng)

  def inner_find(self, basetng):
 #{{{
   #this is a little jiffy that reads the ellg output file and prints the search
   #the file dag.cards.filename or dag.cards.filename_in_subdir is read on input
    basetng.logUnderLine(enum_out_stream.summary,"Load Dag into Memory")
    DAGDB = basetng.DAGDATABASE
    if int(DAGDB.size()) == 0:
        raise ValueError(enum_err_code.input,"No nodes added")
    DAGDB.shift_tracker(str(basetng.hashing()+basetng.input.generic_string),"find")
    if (basetng.input.generic_int == 3):
   #{{{
        basetng.logTab(enum_out_stream.summary,"Seek information cleared")
        DAGDB.clear_seeks()
    #   pid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()] #yes this is correct here and not below
        return
   #}}}
  # pid = [DAGDB.PATHWAY.ULTIMATE.identifier(),DAGDB.PATHWAY.ULTIMATE.tag()]
    basetng.logTab(enum_out_stream.summary,"Pathway: " + DAGDB.PATHWAY.ULTIMATE.str() + "<-" + DAGDB.PATHWAY.PENULTIMATE.str())
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    wf = basetng.Write()
    io_directory = os.path.join(basetng.DataBase(),subdir)
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(io_directory):
      os.makedirs(io_directory)
    wf = basetng.WriteData()
    if (len(basetng.input.generic_string)): #context of call
      basetng.logTab(enum_out_stream.verbose,basetng.input.generic_string)
      basetng.logBlank(enum_out_stream.verbose)
    basetng.logUnderLine(enum_out_stream.summary,"Seek Information")
    REFLECTIONS = basetng.REFLECTIONS
    basetng.logTab(enum_out_stream.summary,"Matthews Z: " + str(REFLECTIONS.Z))
    basetng.logTab(enum_out_stream.summary,"tNCS Order: " + str(REFLECTIONS.TNCS_ORDER))
    lines = DAGDB.seek_info_all().split('\n')
    for line in lines:
      basetng.logTab(enum_out_stream.summary,line)
    WORK = DAGDB.NODES.LIST[0]
    ztotalscat = REFLECTIONS.get_ztotalscat();
    assert(ztotalscat>0)
    fs = WORK.fraction_scattering_seek(ztotalscat);
    fsstr = str(round(fs*1000)/1000.)
    basetng.logTab(enum_out_stream.summary,"Fraction scattering of total seek = " + str(fsstr));
    basetng.logTab(enum_out_stream.verbose,"total scattering = " + str(REFLECTIONS.TOTAL_SCATTERING));
    basetng.logTab(enum_out_stream.verbose,"Z total scattering = " + str(ztotalscat));
    basetng.logTab(enum_out_stream.verbose,"nseek = " + str(len(WORK.SEEK)));
    basetng.logTab(enum_out_stream.verbose,"total scattering seek = " + str(WORK.scattering_seek()));
    if (float(fs) > 1):
      basetng.logWarning("Z of biological unit is not sufficient for search")
      DAGDB.NODES.clear() #force deadend for this search but don't clear entries
    # raise ValueError(enum_err_code.input,"Fraction scattering for total seek is greater than 1");
    else:
      if not wf: #stupid option for this mode, so just exit
        basetng.logTab(enum_out_stream.logfile,str("No files written: exit"))
    #includes next in search
    basetng.logBlank(enum_out_stream.logfile)
    return basetng
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  cards = None
  if args.cards is not None:
    cards = args.cards.read()
  if directory is not None:
    cards = cards + "\nphasertng suite database = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng.DAGDATABASE.parse_cards(cards)
  basetng = runFIND().inner_find(basetng=basetng)
