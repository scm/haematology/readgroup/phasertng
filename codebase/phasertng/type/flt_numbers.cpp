//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_numbers.h>
#include <sstream>
#include <iomanip>
#include <set>

namespace phasertng {
namespace type {

  flt_numbers::flt_numbers(
      const std::string parameter,
      const bool pminset_,const double pminval_,
      const bool pmaxset_,const double pmaxval_,
      const sv_double dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void flt_numbers::set_value(sv_double other)
  {
    user = true;
    uvalue = other;
  }

  void flt_numbers::set_value(dmat6 other)
  {
    user = true;
    uvalue = { other[0],other[1],other[2],other[3],other[4],other[5] };
  }

  int
  flt_numbers::size() const
  { return flt_numbers::value_or_default().size(); }

  void flt_numbers::clear() { return uvalue.clear(); }

  void
  flt_numbers::push_back(const double& other)
  {
    user = true;
    uvalue.push_back(other);
  }

  void
  flt_numbers::pop_back()
  {
    user = true;
    uvalue.pop_back();
  }

  void flt_numbers::set_value(const sv_string& vcards,const sv_double& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_numbers::is_default() const
  { return uvalue == dephalt; }

  void
  flt_numbers::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_numbers::to_python()
  { return unparse_parameter(false); }

  void
  flt_numbers::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    uvalue.resize(txt.size());
    for (int i = 0; i < txt.size(); i++)
      uvalue[i] = std::stod(txt[i]);
  }

  sv_double
  flt_numbers::value_or_default() const
  { return user ? uvalue : dephalt; }

  sv_double
  flt_numbers::value_or_default_unique() const
  {
    sv_double tmp = value_or_default();
    std::set<double> tmp_set;
    for (auto item : tmp)
      tmp_set.insert(item);
    tmp.clear();
    for (auto item : tmp_set)
      tmp.push_back(item);
    return tmp; //sorted and unique
  }

  std::string
  flt_numbers::unparse_parameter(bool phil) const
  {
    if (!value_or_default().size())
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < value_or_default().size(); i++)
      ss << value_or_default()[i] << " ";
    return ss.str().size() ? ss.str() : None();
  }

  std::string
  flt_numbers::show_default(bool phil) const
  {
    if (!dephalt.size())
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    if (!dephalt.size()) return None();
    for (int i = 0; i < dephalt.size(); i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
