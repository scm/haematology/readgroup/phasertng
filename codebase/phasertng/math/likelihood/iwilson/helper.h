//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_iwilson_helper_functions__
#define __phasertng_likelihood_iwilson_helper_functions__
#include <scitbx/vec3.h>
#include <cctbx/miller.h>
#include <complex>

typedef scitbx::vec3<double> dvect3;
typedef cctbx::miller::index<int> millnx;
typedef std::complex<double> cmplex;

namespace phasertng { namespace likelihood { namespace iwilson {

  double
  DobsSigaSqr(
      double SSQR,double SigaSqr,double FS,double DOBS,double DRMS,double BFAC,double G_DRMS);

  cmplex
  EcalcTerm(
      double DobsSigaSqr,double EPS,double NSYMP,millnx SymHKL,dvect3 TRA,double symTra);

}}} //phasertng
#endif
