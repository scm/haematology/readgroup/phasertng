#Writes evenly distributed points on a sphere
import matplotlib.pyplot as pp
import argparse
from phasertng import AnglesUnique
from phasertng import AnglesStored
from scipy.spatial.distance import cdist
import numpy as np

#https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
def Circle(x,y):
    return (x*x+y*y)

def sunflower_spirals_on_the_unit_disc(num_pts):
  num_pts = int(num_pts)
  indices = np.arange(0, num_pts, dtype=float) + 0.5
  r = np.sqrt(indices/num_pts)
  theta = np.pi * (1 + 5**0.5) * indices
  x,y,z = r*np.cos(theta), r*np.sin(theta), theta
  ptlist = []
  for i in range(len(x)):
    ptlist.append((x[i],y[i]))
  ptlist = np.array(ptlist)
  #print(ptlist)
  distmatrix = (cdist(ptlist,ptlist))
  sampling = []
  for distlist in distmatrix:
    sampling.append(np.sort(distlist)[1:2]) # lowest is 0!, this is second lowest
  sampling = np.average(sampling)
  #set the figure size before plotting
  fig = pp.figure(figsize=(6,6))
  ax = fig.add_subplot(111, aspect = 1)
  ax.scatter(x,y)
  pp.show()
  return sampling

def sunflower_spirals_on_the_unit_sphere(num_pts):
  from numpy import pi, cos, sin, arccos, arange
  import matplotlib.pyplot as pp
  indices = arange(0, num_pts, dtype=float) + 0.5
  phi = arccos(1 - 2*indices/num_pts)
  phi = phi%(2*pi)
  theta = pi * (1 + 5**0.5) * indices
  theta = theta%(2*pi)-pi
  x, y, z = cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi);
  #set the figure size before plotting
  pp.figure(figsize=(6,6)).add_subplot(111, projection='3d').scatter(x, y, z);
# pp.figure(figsize=(6,6)).add_subplot(111, aspect=1).scatter(x,y);
  pp.show()

def angles_hexagonal_sampling(num_pts):
  if int(num_pts) < 2:
    return
  a = AnglesUnique()
  a.set_sampling(sampling)
  ptlist = a.all_sites()
  if True:
    #print(len(ptlist))
    x = []
    y = []
    z = []
    beta = set()
    for pt in ptlist:
      beta.add(pt[1])
    for b in beta:
      for pt in ptlist:
        if b == pt[1]:
          x.append(pt[0])
          y.append(pt[1])
          z.append(pt[2])
      pp.figure(figsize=(6,6))
      pp.scatter(x,z);
      pp.show()
  return a.point_distance()

def angles_flower(num_pts):
  if int(num_pts) < 2:
    return
  a = AnglesStored()
  a.fill_memory_points_on_sphere(num_pts)
  ptlist = a.all_sites()
  print(len(ptlist))
  if True:
    x = []
    y = []
    for pt in ptlist:
      x.append(pt[0])
      y.append(pt[1])
    pp.figure(figsize=(6,6))
    pp.scatter(x,y);
    pp.show()
  return a.point_distance()

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-n',"--number",help="number of points",required=True)
  args = parser.parse_args()
 #sampling = sunflower_spirals_on_the_unit_disc(int(args.number))
 #sunflower_spirals_on_the_unit_sphere(int(args.number))
 #sampling = angles_hexagonal_sampling(int(args.number))
  sampling = angles_flower(int(args.number))
