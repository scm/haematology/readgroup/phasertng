//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineGYRE.h>
#include <phasertng/minimizer/RefineSPIN.h>
#include <phasertng/math/rotation/rotdist.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>

namespace phasertng {

  //Gyre_refinement
  void Phasertng::runGYRE()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_turn())
    throw Error(err::INPUT,"Dag not prepared with correct mode (frf failed?)");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete, skipping");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    setmodlid_t set_of_searches;
    {{
    out::stream where = out::LOGFILE;
    if (input.size(".phasertng.gyration.id."))
    {
      logUnderLine(where,"Gyration");
      logTab(where,"Split components for gyration");
      int nsearch = input.size(".phasertng.gyration.id.");
      for (int p = 0; p < nsearch; p++)
      {
        auto UUID = input.array__uuid_number.at(".phasertng.gyration.id.").at(p).value_or_default();
        auto TAG = input.array__string.at(".phasertng.gyration.tag.").at(p).value_or_default();
        Identifier identifier(UUID,TAG);
        auto search = DAGDB.lookup_entry_tag(identifier,DataBase());
        if (!search->identify().is_set_valid())
          throw Error(err::INPUT,"Gyration identifier not defined");
        set_of_searches.insert(search->identify());
      }
      for (auto item : set_of_searches)
        logTab(where,"Gyration identifier: " + item.str());
    }
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    //this may load lots of interpolations
    //if it runs out of memory, throws memory fail during read
    for (auto item : set_of_searches)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,item,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);
    //we will convert to eight point interpolation before the refinement step

    int io_nprint = input.value__int_number.at(".phasertng.macgyre.maximum_printed.").value_or_default();

    //initialize tNCS parameters from reflections and calculate correction terms
    //GYRE
    bool store_halfR(true);
    bool store_fullR(DAGDB.NODES.number_of_poses());
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    double io_purge = input.value__boolean.at(".phasertng.macgyre.purge_duplicates.").value_or_default();

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    if (!DAGDB.NODES.is_all_turn())
    throw Error(err::FILEOPEN,"Dag not prepared with rotation function mode");

    if (!DAGDB.size())
    throw Error(err::FILEOPEN,"Dag not prepared");

    if (input.size(".phasertng.gyration.id."))
    {
      DAGDB.restart(); //very important
      while (!DAGDB.at_end())
      {
        DAGDB.WORK->convert_turn_to_gyre(set_of_searches);
      }
      DAGDB.reset_entry_pointers(DataBase());
    }

    bool refine_off =
        input.value__choice.at(".phasertng.macgyre.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macgyre.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macgyre.macrocycle3.").value_or_default_equals("off");
    bool protocol_off =
        input.value__choice.at(".phasertng.macgyre.protocol.").value_or_default_equals("off");
    if (!input.value__boolean.at(".phasertng.macgyre.refine_helices.").value_or_default())
      if (DAGDB.WORK->NEXT.ENTRY->HELIX and  //only for cases of turn, not gyre
          input.value__boolean.at(".phasertng.coiled_coil.").value_or_default())
      {
        logBlank(out::SUMMARY);
        DAGDB.WORK->NEXT.ENTRY->HELIX ?
          logTab(out::SUMMARY,"All-helical: no refinement"):
          logTab(out::SUMMARY,"Coiled-coil: no refinement");
        logBlank(out::SUMMARY);
        protocol_off = true;
      }

    int z(1);
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      node->generic_int2 = z++;
    }


    double defllg = std::numeric_limits<double>::lowest();
    if (protocol_off)
    {
      out::stream where = out::LOGFILE;
      logBlank(where);
      logProgressBarStart(where,"Annotating gyrations",DAGDB.NODES.size(),1,false);
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        dag::Node* node = DAGDB.WORK;
        node->ANNOTATION += " GYRE=X";
        node->generic_float = defllg;
        logProgressBarNext(where);
      }
      logProgressBarEnd(where);
    }
    else if (refine_off)
    {
      out::stream where = out::LOGFILE;
      logBlank(where);
      logProgressBarStart(where,"Rescoring gyrations",DAGDB.NODES.size(),1,false);
      DAGDB.restart();
      auto set_of_parents = DAGDB.set_of_parents();
      //rescore with the LLGI target (refinement uses Wilson)
      int n(0);
      for (auto parent : set_of_parents)
      {
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.restart(); //very important
        while (!DAGDB.at_end())
        {
          out::stream where = (n++ < io_nprint) ? out::VERBOSE : out::TESTING;
          if (DAGDB.WORK->PARENTS.back() != parent)
            continue;
          bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where);
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
          if (changed or diffsg)
          {
            if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str());
            double resolution = DAGDB.WORK->search_resolution(io_hires);
            auto seltxt = selected.set_resolution(
                REFLECTIONS->data_hires(),
                DAGDB.resolution_range(),
                resolution);
            if (changed) logTabArray(where,seltxt);
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          }
          if (!ecalcs_sigmaa_pose.precalculated)
          {
            DAGDB.apply_interpolation(interp::FOUR);
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS);
          }
          DAGDB.apply_interpolation(interp::FOUR); //no lattice translocation for gyred component
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose); //if not the first parent node, then this will be precalculated
          dag::Node* node = DAGDB.WORK;
          node->ANNOTATION += " GYRE=" + dtoi(node->LLG);
          node->generic_float = DAGDB.WORK->LLG; //store
          logProgressBarNext(where);
        }
      }
      logProgressBarEnd(where);
    }
    else
    {
      //when the dag nodes are entered
      std::vector<sv_string> macro = {
        input.value__choice.at(".phasertng.macgyre.macrocycle1.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macgyre.macrocycle2.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macgyre.macrocycle3.").value_or_default_multi()
      };
      std::vector<int> ncyc = {
        input.value__int_vector.at(".phasertng.macgyre.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macgyre.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macgyre.ncyc.").value_or_default(2)
      };
      auto method = input.value__choice.at(".phasertng.macgyre.minimizer.").value_or_default();
      auto prmethod = (DAGDB.WORK->NEXT.ngyre() == 0) ? method+" ('spin' refinement)" : method;
      logProtocol(out::LOGFILE,macro,ncyc,prmethod);

      int n(1),N(DAGDB.size());
      DAGDB.restart(); //very important
      logUnderLine(out::LOGFILE,"Gyre Refinement");
      logTab(out::LOGFILE,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
      out::stream progress = out::LOGFILE;
      logProgressBarStart(progress,"Refining Gyres",DAGDB.size());
      auto set_of_parents = DAGDB.set_of_parents();
      bool breakit(false);
      double topllg = std::numeric_limits<double>::lowest();
      for (auto parent : set_of_parents)
      {
        if (breakit) break;
        //group the gyres by background so that the ecalcs_sigmaa_pose is done first time only
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.restart(); //very important
        while (!DAGDB.at_end() and !breakit)
        {
          if (DAGDB.WORK->PARENTS.back() != parent)
            continue;
          std::string nN = nNtos(n++,N);
          out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
          logUnderLine(where,"Gyre Refinement" + nN );
          bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where);
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
          if (changed or diffsg)
          {
            if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str());
            double resolution = DAGDB.WORK->search_resolution(io_hires);
            auto seltxt = selected.set_resolution(
                REFLECTIONS->data_hires(),
                DAGDB.resolution_range(),
                resolution);
            if (changed) logTabArray(where,seltxt);
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          }
          if (!ecalcs_sigmaa_pose.precalculated)
          {
            DAGDB.apply_interpolation(interp::FOUR);
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS);
          }
          DAGDB.apply_interpolation(interp::FOUR);
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose); //if not the first parent node, then this will be precalculated
          DAGDB.WORK->generic_float = DAGDB.WORK->LLG; //store
          if (Suite::Extra())
            logTabArray(out::TESTING,DAGDB.WORK->logPose("Known components"));

          DAGDB.apply_interpolation(interp::EIGHT);
          if (DAGDB.WORK->NEXT.ngyre() == 0)
          {
            RefineSPIN refine(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon);
            refine.SIGMA_ROT = input.value__flt_number.at(".phasertng.macgyre.restraint_sigma.rotation.").value_or_default();
            refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
            refine.init_node(DAGDB.WORK,ecalcs_sigmaa_pose);

            dtmin::Minimizer minimizer(this);
            minimizer.set_logfile_level(out::TESTING);
            minimizer.run( refine, macro, ncyc, method,
              input.value__boolean.at(".phasertng.macgyre.study_parameters.").value_or_default(),
              input.value__flt_number.at(".phasertng.macgyre.small_target.").value_or_default()
              );
            if (refine.negvar) logWarning("Negative variance ignored, contact developers");
          }
          else
          {
            RefineGYRE refine(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon);
            refine.SIGMA_ROT = input.value__flt_number.at(".phasertng.macgyre.restraint_sigma.rotation.").value_or_default();
            refine.SIGMA_TRA = input.value__flt_number.at(".phasertng.macgyre.restraint_sigma.position.").value_or_default();
            refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
            refine.init_node(DAGDB.WORK,ecalcs_sigmaa_pose);

            dtmin::Minimizer minimizer(this);
            minimizer.set_logfile_level(out::TESTING); //no output to logfile
            minimizer.run( refine, macro, ncyc, method,
              input.value__boolean.at(".phasertng.macgyre.study_parameters.").value_or_default(),
              input.value__flt_number.at(".phasertng.macgyre.small_target.").value_or_default()
              );
            if (refine.negvar) logWarning("Negative variance ignored, contact developers");
          }
          DAGDB.apply_interpolation(interp::FOUR);
          //double llg = refine.likelihood(); //store llg in node
          //the likelihood is the wilson llg not the llgi, so we need to calculate node likelihood again
          //also the refinement is done with eight point interpolation
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
          logBlank(where);
          DAGDB.WORK->ANNOTATION += " GYRE=" + dtoi(DAGDB.WORK->LLG); //likelihood reports the negative
          if (DAGDB.WORK->NEXT.ngyre() == 0)
            logTabArray(where,DAGDB.WORK->logTurn("Refinement results"));
          else
            logTabArray(where,DAGDB.WORK->logGyre("Refinement results"));
          if (input.value__boolean.at(".phasertng.macgyre.study_parameters.").value_or_default())
          {
            throw Error(err::DEVELOPER,"Study parameters: break after first refinement");
          }
          if (DAGDB.WORK->LLG > topllg)
          {
            topllg = DAGDB.WORK->LLG;
            logProgressBarAgain(progress,"New Best LLG=" + dtos(topllg,2)+nN);
          }
          if (!logProgressBarNext(progress)) breakit = true;
        }
      }
      logProgressBarEnd(progress);
    }

    {{
    out::stream where = out::LOGFILE;
    //can't use generic int here as it contains symmetry op of the duplicate
    logEllipsisOpen(where,"Sorting");
    DAGDB.NODES.apply_sort_llg();
    logEllipsisShut(where);
    }} //generic_float overwritten

    int isize = itow(DAGDB.size());
#ifdef PHASERTNG_DEBUG_EULER_FRACT
    if (Suite::Extra())
    {
    out::stream where = out::TESTING;
    logTableTop(where,"Gyre Refinement (All)");
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    int w = itow(io_nprint);
    if (DAGDB.NODES.size())
    {
      logTab(where,snprintftos(
           "%-*s %10s %10s %7s",
           w,"#","LLG0","LLG","RFZ0"));
      if (DAGDB.NODES.number_of_poses())
        logTab(where,snprintftos(
             "%*s %5s  %7s %7s %7s  %7s %7s %7s ",
             w,"#","Pose#","Euler1","Euler2","Euler3","FracX","FracY","FracZ"));
      logTab(where,snprintftos(
           "%-*s %5s  %7s %7s %7s %24s  %*s %*s",
           w,"#","Turn ","Euler1","Euler2","Euler3","Euler-Shift",uuid_w,"modlid",uuid_w,"reflid"));
      logTab(where,snprintftos(
           "%-*s %5s {%7s %7s %7s} {%7s %7s %7s} {%8s}",
           w,"#","Gyre#","Rot(x)","Rot(y)","Rot(z)","Tra(x)","Tra(y)","Tra(z)","DRMS"));
      logTab(where,"--------");
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      logTab(where,snprintftos(
          "%*d % 10s %10.*f %7.2f",
          w,i,
          (node->generic_float == defllg) ? "-n/a-":dtos(10,q).c_str(),
          q,node->LLG,
          node->ZSCORE
          ));
      for (int p = 0; p < node->POSE.size(); p++)
      {
        dag::Pose& pose = node->POSE[p];
        logTab(where,snprintftos(
            "%*s %-5i  % 7.1f % 7.1f % 7.1f   % 7.4f % 7.4f % 7.4f  %s",
            w,"",
            p+1,
            pose.EULER[0],pose.EULER[1],pose.EULER[2],
            pose.FRACT[0],pose.FRACT[1],pose.FRACT[2],
            pose.IDENTIFIER.tag().c_str()));
      }
      auto& turn = node->NEXT;
      double ang = turn.delta_angle();
      int pa = ang >= 10 ? 2 : 3;
      logTab(where,snprintftos(
          "%*s %-5s  % 7.1f % 7.1f % 7.1f   %7s %7s %7s  %s",
          w,"",
          "",
          turn.EULER[0],turn.EULER[1],turn.EULER[2],
          "","","",
          turn.IDENTIFIER.string().c_str()
          ));
      logTab(where,snprintftos(
            "%*s %5s {%+7.3f %+7.3f %+7.3f}=(%5.*f) {%+6.2f=%05.3f}",
            w,"",
            "",
            turn.ORTHR[0],turn.ORTHR[1],turn.ORTHR[2],
            pa,ang,
            node.DRMS_SHIFT[turn.IDENTIFIER].VAL,
            node.VRMS_VALUE[turn.IDENTIFIER].VAL
            ));
      phaser_assert(node.CELL_SCALE(turn.IDENTIFIER));
      for (int g = 0; g < node->ngyre(); g++)
      {
        auto& gyre = node->NEXT.GYRE[g];
        logTab(where,snprintftos(
            "%*s %5i {%+7.3f %+7.3f %+7.3f}=(%5.*f) {%+7.3f %+7.3f %+7.3f}=(%5.*f) {%+6.2f=%05.3f} %s",
            w,"",
            g+1,
            gyre.ORTHR[0],gyre.ORTHR[1],gyre.ORTHR[2],
            gyre.ORTHT[0],gyre.ORTHT[1],gyre.ORTHT[2],
            node.DRMS_SHIFT[gyre.IDENTIFIER].VAL,
            node.VRMS_VALUE[gyre.IDENTIFIER].VAL,
            gyre.IDENTIFIER.str()
            ));
      }
      logTab(where,"---");
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logBlank(where);
    logTableEnd(where);
    }
#endif

    if (!protocol_off)
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Flagging duplicates");
      bool has_tncs = nmol > 1;
      int dupl = DAGDB.WORK->NEXT.ngyre() ?
         DAGDB.calculate_duplicate_gyres(selected.STICKY_HIRES,has_tncs,USE_STRICTLY_NTHREADS,NTHREADS):
         DAGDB.calculate_duplicate_turns(selected.STICKY_HIRES,has_tncs,USE_STRICTLY_NTHREADS,NTHREADS);
      logEllipsisShut(where);
      logTab(where,"Duplicates: " + itos(dupl) + " of " + itos(DAGDB.size()));
      if (!io_purge) logTab(where,"No purge of duplicates");
    } //generic_float overwritten
    else
    {
      for (int i = 0; i < DAGDB.NODES.size(); i++)
      {
        DAGDB.NODES[i].EQUIV = i;
        DAGDB.NODES[i].generic_flag = true;
        DAGDB.NODES[i].generic_int = -999; //flag
      }
    }

    for (auto& node : DAGDB.NODES)
    {
      for (int g = 0; g < node.NEXT.ngyre(); g++)
      {
        auto& gyre = node.NEXT.GYRE[g];
        dmat33 PR = gyre.ENTRY->PRINCIPAL_ORIENTATION;
        dmat33 ROT = node.NEXT.gyre_finalR(g);
        dvect3 eulermt = scitbx::math::euler_angles::zyz_angles(ROT); //with symmetry
        std::tie(std::ignore,gyre.SHIFT_MATRIX) =
            dag::print_euler_wrt_in_from_mt(eulermt,PR);
      }
    }


    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::max(3,isize); //-n/a-
    out::stream where = out::LOGFILE;
    logTableTop(where,"Gyre Refinement Results");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"Rotations with respect to original coordinates");
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#r *&   Refined rank of the peak and Unique/Duplicate");
      logTab(where,"#i #o   Input/Output rank of the peak");
      logTab(where,"#= #Y   Equivalent to peak number via symmetry operator");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%*s%c %*s %*s %*s %2s %10s %10s %7s %*s",
          w,"#r",' ',
          ww,"#i",
          ww,"#o",
          ww,"#=",
          "#Y",
          "LLG0","LLG","RFZ",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
      logTab(where,snprintftos(
          "%*s %s (%5s) %6s=%-5s",
          w+2+(ww+1)*3+2+12,"Final",
          dag::header_polar().c_str(),
          "Angle","DRMS","VRMS"));
      logTab(where,snprintftos(
          "%*s %s %14s %*s %*s",
          w+2+(ww+1)*3+2+12,"Start",
          dag::header_polar().c_str(),
          "",uuid_w,"parent",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid"));
    }
    logTab(where,"--------");
    int i(1),j(1);
    for (auto& node : DAGDB.NODES)
    {
      int onum = i; //refined position in list if not purged
      if (node.generic_flag and io_purge) onum = node.NUM;
      logTab(where,snprintftos(
          "%*i%c %*s %*s %*s %2s %10s %10s %7s %*s",
          w,i,node.generic_flag ? '*': '&',
          ww,itos(node.generic_int2).c_str(),
          ww,itos(onum).c_str(),
          ww,node.generic_flag ? "--" : itos(node.EQUIV+1).c_str(),
          node.generic_flag ? "--" : itos(node.generic_int+1).c_str(), //symmetry
          (node.generic_float == defllg) ? "n/a":dtos(node.generic_float,q).c_str(),
          dtos(node.LLG,q).c_str(),
          dtos(node.ZSCORE,2).c_str(),
          unique_search ? 0:uuid_w,unique_search ? "":node.REFLID.string().c_str()));
      auto& turn = node.NEXT;
      logTab(where,snprintftos(
            "%*s %s (%5.*f) %+6.2f=%05.3f",
            w+1+(ww+1)*3+3+12,"Final",
            dag::print_polar(turn.SHIFT_MATRIX).c_str(),
            (turn.delta_angle() > 10) ? 2 : 3,turn.delta_angle(),
            node.DRMS_SHIFT[turn.IDENTIFIER].VAL,
            node.VRMS_VALUE[turn.IDENTIFIER].VAL
            ));
      for (int g = 0; g < node.NEXT.ngyre(); g++)
      {
        auto& gyre = node.NEXT.GYRE[g];
        logTab(where,snprintftos(
            "%*s %s (%5.*f) %+6.2f=%05.3f",
            w+1+(ww+1)*3+3+12,"Final",
            dag::print_polar(gyre.SHIFT_MATRIX).c_str(),
            (gyre.delta_angle() > 10) ? 2 : 3,gyre.delta_angle(),
            node.DRMS_SHIFT[gyre.IDENTIFIER].VAL,
            node.VRMS_VALUE[gyre.IDENTIFIER].VAL
            ));
      }
      auto modlid = node.NEXT.IDENTIFIER;
      logTab(where,snprintftos(
            "%*s %s %14s %*s %*s",
            w+1+(ww+1)*3+3+12,"Start",
            dag::print_polar(node.NEXT.SHIFT_MATRIX).c_str(),
            "",
            uuid_w,std::to_string(node.PARENTS.back()).c_str(),
            unique_search ? 0:uuid_w,unique_search ? "":modlid.string().c_str()));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    if (io_purge)
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge duplicates");
      int before = DAGDB.size();
      DAGDB.NODES.erase_invalid();
      logEllipsisShut(where);
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.size()));
    }

    {{
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int2,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::max(3,itow(io_nprint2)); //-n/a-
    out::stream where = out::SUMMARY;
    logTableTop(where,"Gyre Refinement Summary");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      auto& node = DAGDB.NODES[0];
      logTab(where,"#o #i   Output/Input rank of the peak");
      logTab(where,"--------");
      int i(1),j(1);
      if (node.NEXT.ngyre() == 0) //normal case
      {
        logTab(where,snprintftos(
            "%*s %*s %10s %10s %7s %7s %6s=%-5s %-13s",
            ww,"#o",
            ww,"#i",
            "LLG0","LLG","RFZ",
            "(Angle)","DRMS","VRMS","Space Group"));
        for (auto& node : DAGDB.NODES)
        {
          int onum = i; //refined position in list if not purged
          if (node.generic_flag and io_purge) onum = node.NUM;
          const auto& modlid = node.NEXT.IDENTIFIER;
          double initial = node.NEXT.ENTRY->VRMS_START[0]; //alphafold esm
          double drms = node.DRMS_SHIFT[modlid].VAL;
          double vrms = pod::apply_drms_to_initial(drms,initial);
          std::string angle = dtos(node.NEXT.delta_angle(),5,(node.NEXT.delta_angle() >= 10) ? 2 : 3);
          logTab(where,snprintftos(
              "%*s %*s %10s %10s %7.2f (%5s) %+6.2f=%05.3f %-13s",
              ww,itos(onum).c_str(),
              ww,itos(node.generic_int2).c_str(),
              (node.generic_float == defllg) ? "n/a":dtos(node.generic_float,q).c_str(),
              dtos(node.LLG,q).c_str(),
              node.ZSCORE,
              angle.c_str(),
              drms,vrms,
              node.SG->sstr().c_str()
              ));
          if (i == io_nprint)
          {
            int more = DAGDB.size()-io_nprint;
            if (DAGDB.size() > io_nprint)
              logTab(where,"--- etc " + itos(more) + " more" );
            break;
          }
          i++;
        }
      }
      else
      {
        logTab(where,snprintftos(
            "%*s %*s %10s %10s %7s %-13s  %-s",
            ww,"#o",
            ww,"#i",
            "LLG0","LLG","RFZ","Space Group",
            "Shifts [Turn:{Gyre}] (Angle) (DRMS)=(VRMS)"));
        for (auto& node : DAGDB.NODES)
        {
          int onum = i; //refined position in list if not purged
          if (node.generic_flag and io_purge) onum = node.NUM;
          logTab(where,snprintftos(
              "%*s %*s %10s %10s %7s %-13s  %-s",
              ww,itos(onum).c_str(),
              ww,itos(node.generic_int2).c_str(),
              (node.generic_float == defllg) ? "n/a":dtos(node.generic_float,q).c_str(),
              dtos(node.LLG,q).c_str(),
              dtos(node.ZSCORE,2).c_str(),
              node.SG->sstr().c_str(),
              node.shift_str("").c_str()
              ));
          if (i == io_nprint)
          {
            int more = DAGDB.size()-io_nprint;
            if (DAGDB.size() > io_nprint)
              logTab(where,"--- etc " + itos(more) + " more" );
            break;
          }
          i++;
        }
      }
    }
    logTableEnd(where);
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);

    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        if (entry.second.INTERPOLATION.Warnings.size())
          logWarning(entry.second.INTERPOLATION.Warnings);
    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        entry.second.INTERPOLATION.Warnings = "";
  }

} //phasertng
