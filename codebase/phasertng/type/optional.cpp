//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/hoist.h>
#include <phasertng/type/optional.h>
#include <phasertng/main/includes.h>
#include <autogen/include/Scope.h>
#include <vector>
#include <string>

namespace phasertng {
namespace type {

  optional::optional(std::string parameter)
  {
    if (parameter.size())
    {
      hoist::replace_all(parameter,std::string(SCOPE_SEPARATOR())," ");
      std::stringstream ss(parameter);
      std::istream_iterator<std::string> begin(ss);
      std::istream_iterator<std::string> end;
      keywords = std::vector<std::string>(begin, end);
    }
  }

  std::string optional::parameter()
  {
    std::string p(SCOPE_SEPARATOR());
    for (auto item : keywords)
      p += item + std::string(SCOPE_SEPARATOR());
    return p;
  }

  const bool& optional::is_user_value() const { return user; }

  std::string optional::unparse(show::type show_type,bool phil) const
  {
    if (show_type == show::VALUEORDEFAULT)//return the value or default
      return scoped(keywords) + " " + unparse_parameter(phil) + "\n";
    if (show_type == show::VALUE) //return the user value only
      return is_default() ? "": scoped(keywords) + " " + unparse_parameter(phil) + "\n";
    if (show_type == show::DEFAULT) //return the default only
      return scoped(keywords) + " " + show_default(phil) + "\n";
    return std::string("");
  }

  std::string optional::scoped(const std::vector<std::string>& vec) const
  {
    std::string Card(SCOPE_SEPARATOR());
   // phaser_assert(vec.size());
    for (int i = 0; i < vec.size(); i++)
      Card += vec[i] + std::string(SCOPE_SEPARATOR());
    return Card;
  }

  std::string optional::unscoped() const
  {
    std::string Card;
    for (int i = 0; i < keywords.size(); i++)
      Card += keywords[i] + " ";
    Card.pop_back();
    return Card;
  }

  sv_string
  optional::to_words(const std::string str) const
  {
    std::istringstream ss(str);
    std::string word; // for storing each word
    // Traverse through all words
    // while loop till we get
    // strings to store in string word
    sv_string words;
    while (ss >> word)
    {
      words.push_back(word);
    }
    return words;
  }

  void optional::paranoia_check(const std::vector<std::string>& vcards) const
  {
    if (scoped(keywords) != scoped(vcards))
      std::cerr <<  "\"" << scoped(keywords) << "\" (stored) != \"" << scoped(vcards) << "\" (input)" <<  std::endl;
    assert(scoped(keywords) == scoped(vcards));
  }

}}
