#ifndef __phasertng_table_nucleic_composition_class__
#define __phasertng_table_nucleic_composition_class__
#include <map>

namespace phasertng {
namespace table {

class nucleic_composition
{
   //AJM TODO check these again
   //C10H14N5O7P Adenisine monophosphate
   //C10H14N2O8P Thymidine monophosphate
   //C10H14N5O8P Guanidine monophosphate
   //C9 H14N3O8P Cytosine monophosphate
   //C9 H13N2O9P Uridine monophosphate
   //C10H13N4O8P Inosine monophosphate
  private:
    std::map<char,std::map<std::string,double>> nucleic= {
      { 'A', { {"H",11}, {"C",10}, {"N",5}, {"O",5}, {"S",0}, {"P",1} } },
      { 'C', { {"H",11}, {"C", 9}, {"N",3}, {"O",6}, {"S",0}, {"P",1} } },
      { 'G', { {"H",11}, {"C",10}, {"N",5}, {"O",6}, {"S",0}, {"P",1} } },
      { 'T', { {"H",12}, {"C",10}, {"N",2}, {"O",7}, {"S",0}, {"P",1} } },
      { 'U', { {"H",10}, {"C", 9}, {"N",2}, {"O",8}, {"S",0}, {"P",1} } },
      { 'I', { {"H", 9}, {"C",10}, {"N",4}, {"O",7}, {"S",0}, {"P",1} } }
    };
    std::map<std::string, char> single_letter_code = {
      { "A  ",'A' }, { "A+ ",'A' }, { " A ",'A' }, { " +A",'A' }, { "DA ",'A' }, { " DA",'A' },
      { "C  ",'C' }, { "C+ ",'C' }, { " C ",'C' }, { " +C",'C' }, { "DC ",'C' }, { " DC",'C' },
      { "G  ",'G' }, { "G+ ",'G' }, { " G ",'G' }, { " +G",'G' }, { "DG ",'G' }, { " DG",'G' },
      { "T  ",'T' }, { "T+ ",'T' }, { " T ",'T' }, { " +T",'T' }, { "DT ",'T' }, { " DT",'T' },
      { "U  ",'U' }, { "U+ ",'U' }, { " U ",'U' }, { " +U",'U' }, { "DU ",'U' }, { " DU",'U' },
      { "I  ",'I' }, { "I+ ",'I' }, { " I ",'I' }, { " +I",'I' }, { "DI ",'I' }, { " DI",'I' }
    };
    std::map<std::string,double> empty;

  public:
  nucleic_composition(){}

  bool
  valid_residue(const char& r) const
  { return (nucleic.find(r) != nucleic.end()); }

  bool
  valid_residue(const std::string& r) const
  { return (single_letter_code.count(r)); }

  std::map<std::string,double>
  composition(const char& r) const
  { return valid_residue(r) ? nucleic.at(r) : empty; }

  char
  single_letter(std::string residue) { return single_letter_code.at(residue); }

};

}}
#endif
