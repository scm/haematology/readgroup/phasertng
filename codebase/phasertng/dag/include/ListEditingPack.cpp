//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/dag/DuplicateData.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/euler_angles.h>
#include <future>

//#define DEBUG_PHASERTNG_LISTEDITPACK

namespace phasertng {
namespace dag {

  //ListEditing
  int
  DagDatabase::calculate_duplicate_packs(
      double dmin,
      bool is_a_map,
      std::string input_sg,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = size();
    // Compare solutions to find duplicates further down list
    bool all_have_pose(true);
    for (int k = 0; k < NODES.size(); k++)
    {
      NODES[k].generic_flag = true;
      NODES[k].EQUIV = k;
      if (NODES[k].POSE.size() == 0)
        all_have_pose = false;
    }
    if (!all_have_pose)
      return 0;

    HallDuplicateData map_data2;
    bool is_atom(false);
    for (int k = 0; k < tsize; k++)
    {
      dag::Node& node_k = NODES[k];
      if (!map_data2.count(node_k.HALL))
        map_data2[node_k.HALL] = dag::DuplicateData(node_k,false);
    }

    duplicate_packs_combine(map_data2,dmin,is_a_map,input_sg,USE_STRICTLY_NTHREADS,NTHREADS);

    int count(0);
    for (int k = 0; k < NODES.size(); k++)
    {
      if (NODES[k].generic_float2 >= 0) count++;
    }
    return count;
  }

  void
  DagDatabase::duplicate_packs_combine(
      HallDuplicateData map_data2,
      double dmin,
      bool is_a_map,
      std::string input_sg,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = NODES.size();
    int minimum_nchk_for_parallel(1000); //must be this to make it worthwhile AJM TODO run tests
    if (NTHREADS == 1)
    {
      int beg = 0;
      int end = tsize;
      duplicate_packs_parallel(map_data2,dmin,is_a_map,input_sg,beg,end);
    }
    else if (tsize < minimum_nchk_for_parallel)// because the granularity is too small
    {
      int beg = 0;
      int end = tsize;
      duplicate_packs_parallel(map_data2,dmin,is_a_map,input_sg,beg,end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(
          HallDuplicateData,
          double,
          bool,
          std::string,
          int,
          int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      int grain = (tsize/NTHREADS)+1; //granularity of threading, +1 makes the last thread the shortest
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = t*grain; //start of thread reflection
        int end = std::min((t+1)*grain,tsize); //end of thread
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(
            &DagDatabase::duplicate_packs_parallel, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              map_data2,
              dmin,
              is_a_map,
              input_sg,
              beg,
              end );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async,
            &DagDatabase::duplicate_packs_parallel, this,
              map_data2,
              dmin,
              is_a_map,
              input_sg,
              beg,
              end ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
       int beg = nthreads_1*grain; //start of thread reflection
       int end = std::min((nthreads_1+1)*grain,tsize); //end of thread
       duplicate_packs_parallel(
              map_data2,
              dmin,
              is_a_map,
              input_sg,
              beg,
              end );
       //generic_float2 remain -999 if nothing was found
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
      }
    }
  }

  void
  DagDatabase::duplicate_packs_parallel(
      HallDuplicateData map_data2,
      double dmin,
      bool is_a_map,
      std::string input_sg,
      int beg,int end)
  {
    std::map<Identifier,double> maprotMax;
    for (auto& entry : ENTRIES)
      if (entry.second.ATOM) //not an atom
      maprotMax[entry.second.identify()] = 0;
      else
      maprotMax[entry.second.identify()] = 2.0*std::atan(dmin/(4.*entry.second.MEAN_RADIUS));
    for (int k_chk = beg; k_chk < end; k_chk++) //check those further down the list
    {
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack k_chk " << itos(k_chk) << std::endl;
#endif
      bool originDefined(false);
      dag::Node& node_k_chk = NODES[k_chk];
      node_k_chk.generic_float2 = -999; //an unused flag
      dmat33 orthmat_kchk = node_k_chk.CELL.orthogonalization_matrix();
      dmat33 fracmat_kchk = node_k_chk.CELL.fractionalization_matrix();
      DuplicateData& data2 = map_data2[node_k_chk.HALL];
      auto Euclid_orthRtrfrac = data2.Euclid_Rmat(node_k_chk.CELL);
      dvect3 zero3(0,0,0), originShift(0,0,0);
      dvect3 originEuler(0,0,0);
      // Store some information about first model in reference solution
      // we are only going to pack to the first component if possible
      dag::Pose& pose0 = node_k_chk.POSE[0]; //not the first pose, just the first model
      Identifier firstmodlid = pose0.ENTRY->identify();
      dmat33 reference_solR;
      dvect3 reference_fract(0,0,0);
      { //set the finalRT for each entry to no rotation no translation
     //change rotation to wrt mt
     //first is not first it is the position for the non translation/orientation
      auto& search = pose0.ENTRY; //nothing to do with the EULER/FRACT
      const dmat33 PR = search->PRINCIPAL_ORIENTATION;
      reference_solR = PR.transpose();
      const dvect3 PT = search->PRINCIPAL_TRANSLATION;
      dvect3 fract = dag::fract_wrt_mt_from_in(zero3,reference_solR,PT,node_k_chk.CELL);
      reference_fract = fract;
      }
      double rotMax = maprotMax[firstmodlid];
      // Find possible choices of origin shift of first pose that make a match with first reference model
      //we are going to find the minimum distance!!
//loop over the poses
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack first model wrt mt PT=" << dvtos(reference_fract,6,4) << " rotMax=" << dtos(rotMax,4) << std::endl;
#endif
      int best_isym(0);
      dvect3 best_originShift(0,0,0);
      dvect3 best_cellTrans(0,0,0);
      for (int s_chk_ori = 0; s_chk_ori < node_k_chk.POSE.size(); s_chk_ori++)
      {
        double transMax = dmin; //restart the transMax each pose, after picking the lowest rotdist preferentially
        dag::Pose& pose_k_chk_s_chk_ori = node_k_chk.POSE[s_chk_ori];
        bool different_id(pose_k_chk_s_chk_ori.IDENTIFIER != firstmodlid);
        if (different_id) continue;
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack spacegroup=" << data2.SG.CCP4 << " pose=" << input_sg<< std::endl;
#endif
        bool different_sg(data2.SG.CCP4 != input_sg);
        if (different_sg) continue;
        dmat33 solR_chk;
        dvect3 fracSolC_chk;
        {
        dvect3 fracT_chk_ori;
        std::tie(solR_chk,fracT_chk_ori) = pose_k_chk_s_chk_ori.finalRT(orthmat_kchk,fracmat_kchk);
        fracSolC_chk = fracT_chk_ori;
        }
        for (int isym = 0; isym < data2.Euclid.group().order_z() and !originDefined; isym++)
        {
          // Check that orientations are sufficiently similar
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          dmat33 solR_chk_sym = Euclid_orthRtrfrac[isym]*solR_chk;
          {
            double rotdist = matrix_delta_rad(reference_solR,solR_chk_sym);
            //allow tolerance so that if there is another one about the same it will check the transMax lowest
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack p=" << s_chk_ori << " isym=" << isym << " rotdist=" << dtos(rotdist,6,4) << " radians " << std::endl;
#endif
            if (rotdist < (rotMax + scitbx::deg_as_rad(DEF_PPH)))
            {
              rotMax = rotdist; //new minimum value for next loop, 1y9r requires smallest distance
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack min rotdist " << dtos(rotdist,6,4) << " " << std::endl;
#endif
              // Check that centres of mass are sufficiently similar
              // Centre of mass of ensemble won't change with application
              // of point group symmetry.
              dvect3 fracSolC_chk_sym = data2.Euclid.doSymXYZ(isym,fracSolC_chk);
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack p=" << s_chk_ori << " frac first=" << dvtos(fracSolC_chk_sym,6,4) << std::endl;
#endif
              if (!is_a_map)
              { //old code where all the unit cells are the same
                // Account for any continuous shifts as origin shift
                dvect3 fracdelta = fracSolC_chk_sym;
                dvect3 originshift = fracSolC_chk - fracSolC_chk_sym;
                // Account for any continuous shifts as origin shift
                dvect3 cellTrans(0,0,0);
                ivect3 i(3,3,3);
                for (int j = 0; j < 3; j++) { //continuous shifts, not cell translation
                  if (!is_a_map and data2.isshift[j]) { i[j] = 0; }}
                for (cellTrans[0] = -i[0]; cellTrans[0] <= i[0] and !originDefined; cellTrans[0]++)
                for (cellTrans[1] = -i[1]; cellTrans[1] <= i[1] and !originDefined; cellTrans[1]++)
                for (cellTrans[2] = -i[2]; cellTrans[2] <= i[2] and !originDefined; cellTrans[2]++)
                {
                  dvect3 fracdelta_reference = data2.cb_op_r_to_reference_setting * fracdelta;
                  fracdelta_reference += cellTrans;
                  //dvect3 originShift_reference = fracdelta_reference - fracSolC_chk;
                  dvect3 distance = fracdelta_reference - reference_fract;
                  dvect3 originShift_reference(cellTrans);
                  for (int j = 0; j < 3; j++) {
                    if (!is_a_map and data2.isshift[j]) {
                      originShift_reference[j] = -distance[j];
                      distance[j] = 0; //for measuring the translation
                    }
                  }
                 // fracdelta = data2.cb_op_r_from_reference_setting*fracdelta_reference;
                 // originShift = data2.cb_op_r_from_reference_setting*originShift_reference;
                  dvect3 delta = orthmat_kchk*distance;
                  double tdist = delta.length();
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack  delta=" << dvtos(delta,6,4) << " tdist="  << dtos(tdist,4)<< " orishift=" << dvtos(originShift_reference,6,4) << std::endl;
#endif
                  if (tdist < (transMax + DEF_PPH))
                  {
                    originShift = originShift_reference;
                   // originShift = data2.cb_op_r_from_reference_setting*originShift_reference;
                    best_isym = isym;
                    best_originShift = originShift_reference;
                    best_cellTrans = cellTrans;
                    originDefined = true;
                    transMax = tdist; //new minimum value for next loop, 1y9r requires smallest distance
                    node_k_chk.generic_float2 = s_chk_ori; //an unused flag
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug listeditpack success with min tdist=" << dtos(tdist,6,4) << " origin_shift=" << dvtos(originShift) << std::endl;
#endif
                  }
                }
              }
            }
          } // ensym
        } // isym
      } // s_chk_ori
      if (originDefined)
      {
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
std::cout << "debug move " << std::endl;
std::cout << "debug euler sym rot=" << dmtos(Euclid_orthRtrfrac[best_isym]) << std::endl;
        auto bstr = node_k_chk.logPose("Before");
        for (auto item: bstr) std::cout << "debug " << item << std::endl;
#endif
        //rotation does not change the centre of mass
        for (int p = 0; p < node_k_chk.POSE.size(); p++)
        {
          auto& pose = node_k_chk.POSE[p];
          //note that the ORTHR and ORTHT have been squashed before thus code is called in runMOVE
          dmat33 ROT; dvect3 FRACT;
          std::tie(ROT,FRACT) = pose.finalRT(orthmat_kchk,fracmat_kchk);
          dvect3 fracSolC_chk_sym = data2.Euclid.doSymXYZ(best_isym,FRACT);
          fracSolC_chk_sym = fracSolC_chk_sym + best_cellTrans + best_originShift;
          pose.FRACT = fracSolC_chk_sym;
          dmat33 ROT0 = zyz_matrix(pose.EULER);
          ROT0 = Euclid_orthRtrfrac[best_isym]*ROT0;
          pose.ORTHR = dvect3(0,0,0);
          pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT0);
          const dvect3 PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
          const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
          std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(pose.EULER,PR);
          dvect3 frac = dag::fract_wrt_in_from_mt(pose.FRACT,pose.EULER,PT,node_k_chk.CELL);
          pose.SHIFT_ORTHT = orthmat_kchk*frac;
        }
#ifdef DEBUG_PHASERTNG_LISTEDITPACK
        auto astr = node_k_chk.logPose("After");
std::cout << "debug 2 euler =" << dvtos(node_k_chk.POSE[0].EULER) << std::endl;
        for (auto item: astr) std::cout << "debug " << item << std::endl;
#endif
      }
    } //k_chk
  }

}}
