import sys
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
import os, os.path
from numpy import array

#these are internal parameters not interesting for the user
class runSSCC:

  def run_from_basetng_and_input(self,basetng):
    self.inner_get_cc_mtz_pdb(basetng=basetng)

  def inner_get_cc_mtz_pdb(self, basetng):
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0):
   #{
      basetng.logTab(enum_out_stream.logfile,"No nodes")
      return
   #}
    import os
    DAGDB.shift_tracker(str(basetng.hashing()),"get_cc_mtz_pdb")
    try:
        from phenix.command_line.get_cc_mtz_pdb import get_cc_mtz_pdb
    except:
        basetng.logTab(enum_out_stream.logfile,"phenix.get_cc_mtz_pdb not installed")
        return
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    root = os.path.join(basetng.DataBase(),subdir)
    wf = basetng.Write()
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(root):
      os.makedirs(root)
    wf = basetng.WriteData()
    pdbin = basetng.input.get_as_str(".phasertng.pdbin.filename.")
    if isNone(pdbin) is None:
      basetng.logTab(enum_out_stream.logfile,"No reference pdb file for CC analysis")
      return
    iopdb = basetng.input.get_as_str(".phasertng.pdbin.filename.")[1:-1]
    basetng.logTab(enum_out_stream.logfile,"Reading: " + str(iopdb))
    if not os.path.exists(iopdb):
      basetng.logTab(enum_out_stream.logfile,"Reference pdb file does not exist")
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
      return
    pdbobj = iotbx.file_reader.any_file(iopdb)
    pdbsg = pdbobj.crystal_symmetry().space_group().match_tabulated_settings().hermann_mauguin()
    basetng.logTab(enum_out_stream.logfile,"Space group " + pdbsg)
    pdbcell = pdbobj.crystal_symmetry().unit_cell().parameters()
    basetng.logTab(enum_out_stream.logfile,"Pdb cell " +
           '{:7.2f}'.format(pdbcell[0]) + " " +
           '{:7.2f}'.format(pdbcell[1]) + " " +
           '{:7.2f}'.format(pdbcell[2]) + "  " +
           '{:4.1f}'.format(pdbcell[3]) + " " +
           '{:4.1f}'.format(pdbcell[4]) + " " +
           '{:4.1f}'.format(pdbcell[5]))
    tableinfo = []
    for i,node in enumerate(DAGDB.NODES.LIST):
   #{
      node.generic_float = 0
      node.generic_float2 = 0;
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
      basetng.logUnderLine(enum_out_stream.logfile,"Coorelation coefficient #" + str(i+1) + " of #" + str(len(DAGDB.NODES.LIST)))
      if not node.PART.PRESENT:
        basetng.logTab(enum_out_stream.logfile,"No density (\"part\" not present in node), skipping")
        continue
      subdir = node.PART.IDENTIFIER.database_subdir()
      entfile = node.PART.IDENTIFIER.entry_data_stem_ext(enum_entry_data.density_mtz)
      iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iomtz.qfstrq())
      if not os.path.exists(iomtz.fstr()):
        basetng.logTab(enum_out_stream.logfile,"Mtz file does not exist")
        continue
      mtzobj = iotbx.file_reader.any_file(iomtz.fstr())
      mtzcell = mtzobj.crystal_symmetry().unit_cell().parameters()
      mtzsg = pdbobj.crystal_symmetry().space_group().match_tabulated_settings().hermann_mauguin()
      if (pdbsg != mtzsg):
        basetng.logTab(enum_out_stream.logfile,"Space group " + mtzsg)
        basetng.logTab(enum_out_stream.logfile,"Space group does not match reference pdb")
        tableinfo.append(str(
             '{:<5d}'.format(i+1) +
             '{:14.1f}'.format(node.LLG) +"     "+
             '{:s}'.format("Space group mismatch")))
        continue
      #cell needed for looking at the offset
      basetng.logTab(enum_out_stream.verbose,"Mtz cell " +
           '{:7.2f}'.format(mtzcell[0]) + " " +
           '{:7.2f}'.format(mtzcell[1]) + " " +
           '{:7.2f}'.format(mtzcell[2]) + "  " +
           '{:4.1f}'.format(mtzcell[3]) + " " +
           '{:4.1f}'.format(mtzcell[4]) + " " +
           '{:4.1f}'.format(mtzcell[5]))
      fullpathroot = os.path.join(root,subdir)
      outstr = TextIO()
      args=["mtz_in="+str(iomtz.fstr()),
        "pdb_in="+str(iopdb),
        "quick=True",
        "temp_dir="+str(fullpathroot),
        "output_dir="+str(fullpathroot)]
      g=get_cc_mtz_pdb(args,quiet=True,out=outstr)
      if g.found_overall is None or g.found_region is None:
        basetng.logTab(enum_out_stream.logfile,"No model correlation obtained")
        tableinfo.append(str(
             '{:<5d}'.format(i+1) +
             '{:14.1f}'.format(node.LLG) +"    "+
             '{:s}'.format("Error")))
        continue
      tableinfo.append(str(
             '{:<5d}'.format(i+1) +
             '{:14.1f}'.format(node.LLG) +
             '{:9.2f}'.format(g.found_overall) +
             '{:9.2f}'.format(g.found_region)))
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
      lines = outstr.getvalue().split("\n")
   #  for line in lines:
   #    basetng.logTab(enum_out_stream.logfile,str(line))
      interesting_lines = ["Copied",
                           "Offsetting",
                           "Translation",
                           "Correlation in",
                           ]
      for line in lines:
        lline = line.lstrip();
        for interesting in interesting_lines:
          if lline.startswith(interesting):
            basetng.logTab(enum_out_stream.logfile,str(lline))
            if lline.startswith("Translation applied"):
              lline = lline.replace("Translation applied: (","")
              lline = lline.replace(") A","")
              tline = lline.split(",")
              trans = [float(tline[0]),float(tline[1]),float(tline[2])]
              fracmat1 = array(list(mtzobj.crystal_symmetry().unit_cell().fractionalization_matrix())).reshape((3, 3))
              trans = fracmat1.dot(trans)
              basetng.logTab(enum_out_stream.logfile,"Translation applied (fractional): (" +
                   '{:6.3f}'.format(trans[0]) + " " +
                   '{:6.3f}'.format(trans[1]) + " " +
                   '{:6.3f}'.format(trans[2]) + ")")
    # return g.found_overall,g.found_region
   #}
    basetng.logTableTop(enum_out_stream.summary,"Correlation Coefficient Summary",True)
    basetng.logTab(enum_out_stream.summary,"#    Log-Likelihood  Overall    Local")
    for info in tableinfo:
      basetng.logTab(enum_out_stream.summary,info)
    basetng.logTableEnd(enum_out_stream.summary)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  args = parser.parse_args()
  cards = ""
  if args.cards is not None:
    cards = args.cards.read()
  if directory is not None:
    cards = cards + "\nphasertng suite database = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng.DAGDATABASE.parse_cards(cards)
  basetng = runSSCC().inner_get_cc_mtz_pdb(basetng=basetng)
  print(basetng.summary())
