import sys,os,copy,json
import argparse
import iotbx
import cctbx
import cctbx.uctbx
from iotbx.file_reader import any_file
import traceback
import iotbx.pdb.fetch
from libtbx.phil import parse
from phasertng.phil.converter_registry import converter_registry
from phasertng.scripts.jiffy import cleanup_hierarchy
from phasertng.scripts.jiffy import format_sequence
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.scripts.jiffy import generate_list_of_pdb_hierarchy_by_chain
from scitbx.array_family import flex
from cctbx.crystal import reindex
from cctbx import crystal
import iotbx.pdb
from cctbx.array_family import flex
from cctbx.sgtbx import change_of_basis_op
from six.moves import cStringIO as StringIO
import phasertng.scripts.simple_tools as simple_tools
import phasertng.scripts.system_utility as system_utility
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
from phasertng.phil.master_phil_file import *
import math

class runBEAM:

  def run_from_basetng_and_input(self,basetng):
 #{{{
    try:
      self.inner_isostructure(
        basetng=basetng,
        io_hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename.")),
        io_tute_ignore_best = bool(basetng.input.get_as_str(".phasertng.isostructure.tutorial.ignore_best.") == "True"),
        io_tute_force_match = isNone(basetng.input.get_as_str(".phasertng.isostructure.tutorial.force_match.")),
        io_tute_ignore_match = isNone(basetng.input.get_as_str(".phasertng.isostructure.tutorial.ignore_match.")),
        io_perfect_match_cc = float(basetng.input.get_as_str(".phasertng.isostructure.perfect_match_cc.")),
        io_mirrors = basetng.input.get_as_str(".phasertng.mirrors.").strip().split(),
        delta_dim = float(basetng.input.get_as_str(".phasertng.isostructure.delta.maximum_angstroms.")),
        delta_angle = float(basetng.input.get_as_str(".phasertng.isostructure.delta.maximum_degrees.")),
        MAXNCDIST = float(basetng.input.get_as_str(".phasertng.isostructure.delta.maximum_ncdist.")),
        TOPEXPLORE = int(basetng.input.get_as_str(".phasertng.isostructure.delta.maximum_explored.")),
        MINCCTHRESH = float(basetng.input.get_as_str(".phasertng.isostructure.delta.minimum_correlation_coefficient.")),
        high_resolution = float(basetng.input.get_as_str(".phasertng.isostructure.delta.resolution.")),
        matches_cut_off = float(basetng.input.get_as_str(".phasertng.isostructure.delta.matches_cut_off.")),
        directd = None
        )
    except ValueError as e:
      raise e
    except Exception as e:
      raise ValueError(enum_err_code.developer,str(e))
 #}}}

  #crazy names from first implementation
  def inner_isostructure(self,
          basetng,
          io_hklin,
          io_tute_ignore_best,
          io_tute_force_match,
          io_tute_ignore_match,
          io_perfect_match_cc,
          io_mirrors,
          delta_dim,
          delta_angle,
          MAXNCDIST,
          TOPEXPLORE,
          MINCCTHRESH,
          high_resolution,
          matches_cut_off,
          directd,
         ):
 #{{{
    def ispdb(name):
      return (name is not None) and len(name) != 4
    basetng.logUnderLine(enum_out_stream.logfile, "Query")
    basetng.logTab(enum_out_stream.logfile, "Database: " + str(basetng.DataBase()))
    DAGDB = basetng.DAGDATABASE
    #isostructure node will always be connected to the root
    if bool(io_tute_ignore_best):
      basetng.logWarning("Running in \"tutorial\" mode: ignore the best match")
      basetng.logBlank(enum_out_stream.logfile)
    if (io_tute_force_match is not None):
     #io_stem = os.path.splitext(os.path.basename(io_tute_force_match))[0] # include the ext
      io_stem = os.path.basename(io_tute_force_match) # include the ext
      basetng.logTab(enum_out_stream.summary,"Force match with "+io_stem)
      if ispdb(io_tute_force_match):
        basetng.logTab(enum_out_stream.summary,"Force match is pdb")

    basetng.logBlank(enum_out_stream.logfile)
    basetng.logTab(enum_out_stream.logfile, "Maximum Niggli-cone distance: " + str("{:.3f}".format(MAXNCDIST)))
    basetng.logTab(enum_out_stream.logfile, "Minimum correlation (CC): " + str("{:.3f}".format(MINCCTHRESH)))
    basetng.logTab(enum_out_stream.logfile, "Maximum number analysed over minimum CC: " + str(int(TOPEXPLORE)))
    basetng.logTab(enum_out_stream.logfile, "CC high resolution limit: " +str("{:.2f}".format(high_resolution)) + " angstroms")
    basetng.logTab(enum_out_stream.logfile, "CC reflection matches minimum: " +str("{:.2f}".format(matches_cut_off)) + "%")

    if io_hklin is None:
      raise ValueError(enum_err_code.fileset,"hklin")
    if not os.path.exists(io_hklin):
      raise ValueError(enum_err_code.fileopen,str(io_hklin))
    basetng.logTab(enum_out_stream.logfile, "Reflection File: " + str(io_hklin))

    mtzobj = iotbx.file_reader.any_file(io_hklin)
    if mtzobj.file_type == "pdb" and mtzobj.crystal_symmetry() is None:
      raise ValueError(enum_err_code.fatal,message)

    original_crystal = mtzobj.crystal_symmetry()

   #for 5cj2 cell 59.19, 80.73, 91.65, 97.66, **89.996***, 90.01 the server thinks this is monoclinic
   #it therefore switches alpha and gamma to 180-alpha 180-gamma, gets the wrong niggli cell and fails spectacularly
   #if the mtz file is changed so that the beta angle is 90 all is fine
   #the hack is the round the angles to 2 significant figures, which must be what is done on the server
    mtzcs = mtzobj.crystal_symmetry()
    basetng.logTab(enum_out_stream.verbose, "Unrounded Unit Cell: " + str(mtzcs.unit_cell()))
    rounded_cell = tuple(round(x*100)/100 for x in mtzcs.unit_cell().parameters())
    mtzcs._unit_cell = cctbx.uctbx.unit_cell(rounded_cell)
    mtzcs = mtzobj.crystal_symmetry()
    niggli_cell = mtzcs.niggli_cell().unit_cell().parameters()
    reciprocal_cell = mtzcs.unit_cell().reciprocal_parameters()
    unit_cell_volume = mtzcs.unit_cell().niggli_cell().volume()
    niggli_cell_str = mtzcs.niggli_cell().unit_cell()
    niggli_op = mtzcs.change_of_basis_op_to_niggli_cell()

    if (mtzobj.file_type == 'hkl'):
      basetng.logTab(enum_out_stream.logfile, "File type:     reflections")
    elif (mtzobj.file_type == 'pdb'):
      basetng.logTab(enum_out_stream.logfile, "File type:     coordinates")
    basetng.logTab(enum_out_stream.logfile, "Space Group:   " + str(
      mtzcs.space_group().match_tabulated_settings().hermann_mauguin()))
    basetng.logTab(enum_out_stream.logfile, "Original Cell: " + str(mtzcs.unit_cell()))
    basetng.logTab(enum_out_stream.logfile, "Niggli Cell:   " + str(niggli_cell))
    basetng.logTab(enum_out_stream.logfile, "Reindexing:    " + str(niggli_op.as_hkl()))

    basetng.logUnderLine(enum_out_stream.logfile, "Isostructure Search")

    min_a = str(niggli_cell[0] - delta_dim)
    max_a = str(niggli_cell[0] + delta_dim)
    min_b = str(niggli_cell[1] - delta_dim)
    max_b = str(niggli_cell[1] + delta_dim)
    min_c = str(niggli_cell[2] - delta_dim)
    max_c = str(niggli_cell[2] + delta_dim)
    min_alpha = str(niggli_cell[3] - delta_angle)
    max_alpha = str(niggli_cell[3] + delta_angle)
    min_beta = str(niggli_cell[4] - delta_angle)
    max_beta = str(niggli_cell[4] + delta_angle)
    min_gamma = str(niggli_cell[5] - delta_angle)
    max_gamma = str(niggli_cell[5] + delta_angle)

    server = "www.ebi.ac.uk"
    cell_search = None
    if io_tute_force_match is None:
      cell_search = "https://wwwdev.ebi.ac.uk/pdbe/search/pdb/select?q=nigli_cell_a:[" + min_a + "%20TO%20" + max_a + "]%20AND%20nigli_cell_b:[" + min_b + "%20TO%20" + max_b + "]%20AND%20nigli_cell_c:[" + min_c + "%20TO%20" + max_c + "]%20AND%20nigli_cell_alpha:[" + min_alpha + "%20TO%20" + max_alpha + "]%20AND%20nigli_cell_beta:[" + min_beta + "%20TO%20" + max_beta + "]%20AND%20nigli_cell_gamma:[" + min_gamma + "%20TO%20" + max_gamma + "]&group=true&group.field=pdb_id&rows=100000&group.ngroups=true&omitHeader=true&fl=*"
    elif not ispdb(io_tute_force_match):
      cell_search = "https://wwwdev.ebi.ac.uk/pdbe/search/pdb/select?q=pdb_id:"+str(io_tute_force_match)+"%20AND%20nigli_cell_a:[" + min_a + "%20TO%20" + max_a + "]%20AND%20nigli_cell_b:[" + min_b + "%20TO%20" + max_b + "]%20AND%20nigli_cell_c:[" + min_c + "%20TO%20" + max_c + "]%20AND%20nigli_cell_alpha:[" + min_alpha + "%20TO%20" + max_alpha + "]%20AND%20nigli_cell_beta:[" + min_beta + "%20TO%20" + max_beta + "]%20AND%20nigli_cell_gamma:[" + min_gamma + "%20TO%20" + max_gamma + "]&group=true&group.field=pdb_id&rows=100000&group.ngroups=true&omitHeader=true&fl=*"
    z = None
    if cell_search is not None:
      basetng.logEllipsisOpen(enum_out_stream.logfile, "Unit cell search performed on server " + server)
      z = system_utility.request_url(cell_search, server)
      basetng.logEllipsisShut(enum_out_stream.logfile)

    if (z is None)  and (io_tute_force_match is None):
      result_list = []
    else:
   #{
      a1, b1, c1, alpha1, beta1, gamma1 = niggli_cell

      if ispdb(io_tute_force_match):
        try:
          pdbobj = iotbx.pdb.input(io_tute_force_match)
        except Exception as e:
          raise ValueError(enum_err_code.input,"Error opening pdb file")
        try:
          pdbcs = pdbobj.crystal_symmetry()
          basetng.logTab(enum_out_stream.logfile, "Crystal Symmetry from File:")
          basetng.logTab(enum_out_stream.logfile, "--Space Group:   " + str(
            pdbcs.space_group().match_tabulated_settings().hermann_mauguin()))
          basetng.logTab(enum_out_stream.logfile, "--Original Cell: " + str(
            pdbcs.unit_cell()))
          basetng.logTab(enum_out_stream.logfile, "--Niggli Cell:   " + str(
            pdbcs.niggli_cell().unit_cell()))
          basetng.logTab(enum_out_stream.logfile, "--Reindexing:    " + str(
            pdbcs.change_of_basis_op_to_niggli_cell().as_hkl()))
          basetng.logBlank(enum_out_stream.logfile)
          result_list = [[ io_stem, pdbcs.niggli_cell().unit_cell().parameters() ]]
        except Exception as e:
          raise ValueError(enum_err_code.fatal,"Error reading pdb file in cryst1 cards")
      else:
        resu = json.loads(z)
        result_list = [[gr["groupValue"], [(s["nigli_cell_a"], s["nigli_cell_b"], s["nigli_cell_c"],
                                          s["nigli_cell_alpha"], s["nigli_cell_beta"], s["nigli_cell_gamma"]) for
                                         s in gr["doclist"]["docs"]][0]] for gr in
                     resu["grouped"]["pdb_id"]["groups"]]
      UC1 = [a1, b1, c1, alpha1 / 57.2957795130823, beta1 / 57.2957795130823, gamma1 / 57.2957795130823]
      # v7 = simple_tools.V7(UC1)
      rsn = []

      for resu in result_list:
        pdbid, niggli = resu
        a2, b2, c2, alpha2, beta2, gamma2 = niggli
        UC2 = [a2, b2, c2, alpha2 / 57.2957795130823, beta2 / 57.2957795130823, gamma2 / 57.2957795130823]
        cctbx_ncdist = 0.1 * math.sqrt(cctbx.uctbx.determine_unit_cell.NCDist(simple_tools.g6_form(UC1), simple_tools.g6_form(UC2)))
        rsn.append((pdbid, cctbx_ncdist, niggli))

      if (len(result_list) > 1):
        basetng.logBlank(enum_out_stream.logfile)
        basetng.logEllipsisOpen(enum_out_stream.logfile, "Sorting")
        #  result_list = sorted(rsn, key=lambda x: x[1])[:TOPEXPLORE+5]
        result_list = sorted(rsn, key=lambda x: x[1])
        basetng.logEllipsisShut(enum_out_stream.logfile)
      else:
        result_list = sorted(rsn, key=lambda x: x[1])

      first1 = True
      first2 = True
      count = 1
      maxprint = None
      nebi = len(result_list)
      if io_tute_force_match is None:
        basetng.logTab(enum_out_stream.logfile, "Number returned from server = " + str(nebi))
      for resu in result_list:
        pdbid, cctbx_ncdist, niggli = resu
        if (cctbx_ncdist > MAXNCDIST and first1):
          basetng.logTab(enum_out_stream.logfile, "----------- Niggli-cone distance limit " + str(MAXNCDIST))
          first1 = False
        where = enum_out_stream.logfile
        if (first1 and (maxprint is None or count > maxprint + 1) and cctbx_ncdist > MAXNCDIST and (maxprint is None or nebi > maxprint)):
          where = enum_out_stream.verbose
        basetng.logTab(where, str(count).ljust(5) + " pdbid: " + str(pdbid) + " Niggli-cone distance: " + str(
          round(cctbx_ncdist, 3)) + " cell: " + str(niggli))
        if first2 and (maxprint is None or count >= maxprint) and (maxprint is None or nebi > maxprint) and cctbx_ncdist > MAXNCDIST:
          basetng.logTab(enum_out_stream.logfile, "----------- and " + str(nebi - count) + " more")
          first2 = False
        count = count + 1
   #}

    if mtzobj.file_type != "hkl":
      basetng.logBlank(enum_out_stream.logfile)
      basetng.logTab(enum_out_stream.logfile, "No data for correlation")
      basetng.logBlank(enum_out_stream.logfile)
    # return

    #ic = simple_tools.Intensity_Correlation(io_hklin, logger=self.logger)
    basetng.logTab(enum_out_stream.verbose,"Setup intensity correlation")
    ic = simple_tools.Intensity_Correlation(io_hklin,high_resolution,matches_cut_off)
    if ic.high_resolution > high_resolution:
      basetng.logWarning("High resolution for CC restricted by data resolution")
      basetng.logTab(enum_out_stream.logfile, "CC high resolution limit: " +str("{:.2f}".format(high_resolution)) + " angstroms")
      basetng.logBlank(enum_out_stream.logfile)
    basetng.logBlank(enum_out_stream.logfile)

    foundpdb = None
    best_pdbid = None
    best_maxcc = -1.0
    best_crystal = None
    best_niggli = None
    best_ope = None
    best_title = None

    if (io_tute_ignore_match is not None):
      io_tute_ignore_match = io_tute_ignore_match.split()

    topnlist = []
    nfoundpdb = 0
    basetng.logTab(enum_out_stream.verbose, "Result list length: " +str(len(result_list)))
    for r, (pdbid, dist, tmp) in enumerate(result_list):
   #{ breaks out early
      thisfoundpdb = False
      if dist >= MAXNCDIST:
        basetng.logBlank(enum_out_stream.logfile)
        basetng.logTab(enum_out_stream.logfile, "----------- Niggli-cone distance limit")
        break
      basetng.logBlank(enum_out_stream.logfile)
      basetng.logUnderLine(enum_out_stream.logfile, "Evaluating #" + str(r + 1) + " " + str(pdbid))
      basetng.logTab(enum_out_stream.logfile, "Niggli-cone distance = " + str("{:.3f}".format(dist)))
      if (io_tute_force_match is None or not ispdb(io_tute_force_match)):
        z = system_utility.request_url("https://www.ebi.ac.uk/pdbe/api/pdb/entry/summary/" + str(pdbid.lower()),
                                     "www.ebi.ac.uk")
      elif ispdb(io_tute_force_match):
        z = {io_stem:[{"title":io_stem}]}

      if not z:
        basetng.logTab(enum_out_stream.logfile, "Failed server request, skipping...")
        continue
      elif ispdb(io_tute_force_match):
        infol = {io_stem.lower():[{"title":io_stem}]}
      else:
        infol = json.loads(z)

      cannot_download = False
      calculate_fmodel = False
      pdbid_title = str(infol[pdbid.lower()][0]["title"])
      basetng.logTab(enum_out_stream.logfile, "title: " + pdbid_title)

      if (io_tute_force_match is None or str(io_tute_force_match) != str(pdbid)):
        if io_tute_ignore_best and (r == 0 or dist < 0.0001):
          basetng.logWarning("Top solution ignored")
          basetng.logBlank(enum_out_stream.logfile)
          continue
      if (io_tute_ignore_match is not None) and str(pdbid) in io_tute_ignore_match:
        basetng.logTab(enum_out_stream.logfile,"Ignore match")
        basetng.logBlank(enum_out_stream.logfile)
        continue

      mirrors = io_mirrors
      can_download_or_read = False
      basetng.logTab(enum_out_stream.verbose,str(pdbid.lower()))

      basetng.logTab(enum_out_stream.verbose,"Try data type: sf")
      for mirror in mirrors:
          if can_download_or_read: break
          basetng.logTab(enum_out_stream.verbose,"Mirror: " + mirror)
          try:
            datap = iotbx.pdb.fetch.fetch(pdbid.lower(),
                       entity="sf",
                       mirror=mirror,
                       ).read().decode(system_utility.get_encoding())
            can_download_or_read = True
            basetng.logTab(enum_out_stream.verbose,"Success")
          except Exception as e:
            basetng.logTab(enum_out_stream.verbose,"Failure")
            basetng.logTab(enum_out_stream.verbose,str(e))
            can_download_or_read = False #if this is the last time through, this sticks

      if not can_download_or_read:
        basetng.logTab(enum_out_stream.verbose,"Try data type: model cif and calculate sf")
        for mirror in mirrors:
          # can get a pdb file and calculate structure factors
          if can_download_or_read: break
          try:
            basetng.logTab(enum_out_stream.verbose,"Mirror: " + mirror)
            datap = iotbx.pdb.fetch.fetch(pdbid.lower(),
                        entity="model_cif",
                        mirror=mirror,
                        ).read().decode(system_utility.get_encoding())
            calculate_fmodel = True
            can_download_or_read = True
            basetng.logTab(enum_out_stream.verbose,"Success")
          except Exception as e:
            can_download_or_read = False #if this is the last time through, this sticks
            basetng.logTab(enum_out_stream.verbose,"Failure")
            basetng.logTab(enum_out_stream.verbose,str(e))
            if False:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)

      if not can_download_or_read:
          basetng.logTab(enum_out_stream.logfile,"Try read local data: pdb and calculate")
          try:
              # can get a pdb file and calculate structure factors
              datap = iotbx.pdb.input(io_tute_force_match).as_pdb_string()
              calculate_fmodel = True
              basetng.logTab(enum_out_stream.verbose,"Success")
              can_download_or_read = True
              thisfoundpdb = True
          except Exception as e:
              basetng.logTab(enum_out_stream.verbose,"Failure")
              basetng.logTab(enum_out_stream.verbose,str(e))
              can_download_or_read = False
              if False:
                print(sys.exc_info())
                traceback.print_exc(file=sys.stdout)

      # NOTE: It looks like cctbx needs to have a filename associated with the cif file string
      # dp = cStringIO.StringIO(datap)
      if can_download_or_read:
        basetng.logTab(enum_out_stream.verbose, "Download successful")
        pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(datap)))
     #  cifile = str(hash(str(pdbid + ".tmp.cif"))) + ".tmp.cif"
     #  with open(cifile, "w") as fg:
     #    fg.write(datap)
        try:
          # CALCULATE CC HERE
          basetng.logTab(enum_out_stream.verbose,"Compare structure factors with fmodel="+str(calculate_fmodel))
          goodcc, result = ic.compare(pdbid, datap, calculate_fmodel, thresh=MINCCTHRESH)
     #    os.remove(cifile) #delete after writing because this is a tmp file
          # end calculate
          basetng.logTab(enum_out_stream.logfile, "Space Group: " + str(ic.spg2))
          basetng.logTab(enum_out_stream.verbose, "Niggli Basis: " + str(ic.niggli2.as_hkl()))
          basetng.logTab(enum_out_stream.verbose, "Unit Cell: " + str(ic.cell2))
          if (calculate_fmodel):
            basetng.logTab(enum_out_stream.logfile, "Structure factors calculated from model")
          else:
            basetng.logTab(enum_out_stream.verbose, "Structure factors downloaded")

         #we must extract the labels from the results object
         #multiple copies if there are mulitple m1 datasets
          labels = []
          for item in result:
             txt = "Labels: ##%-3d=%s Resolution: %5.2f" % (item["labin"], item["labels"], item["reso"])
             if txt not in labels: # order important
               labels.append(txt)

          header = True
          if (len(result) == 0):
            basetng.logTab(enum_out_stream.logfile, "No simple reindexing relation found between unit cells")
            basetng.logBlank(enum_out_stream.logfile)
          for k,item in enumerate(result):
            if header:
              basetng.logTableTop(enum_out_stream.logfile, "Correlations for " + str(pdbid), False)
              for labin in labels:
                basetng.logTab(enum_out_stream.logfile,labin)
              header = False
              basetng.logTab(enum_out_stream.logfile,
                                "%-8s %7s%c %7s%c %7s %s" % (
                                  "Compare",
                                  "CC("+str(round(ic.high_resolution))+"A)",' ',
                                  "matches",' ',
                                  "#Refl",
                                  "Reindex"
                                ))
            failure = '!' if (item["matches"] < ic.matches_cut_off) else ' '
            basetng.logTab(enum_out_stream.logfile,
                              "#%-2d:##%-2d %7.3f%c %6.1f%c%c %7d %s" % (
                                int(item["miller1"]),
                                int(item["miller2"]),
                                item["cc"],failure,
                                item["matches"],'%',failure,
                                item["number_in_cc"],
                                str(k+1)+":"+item["reindex"]))
          if header == False:
            basetng.logTableEnd(enum_out_stream.logfile)
          print_current_best = False
          if best_maxcc is not None and best_maxcc > -1.0:
            if best_pdbid is None:  # hack, set the first one if not set
              best_pdbid = str(pdbid)
              best_title = str(pdbid_title)
            if best_maxcc is not None:
              print_current_best = True
          if io_tute_force_match is not None:
            if str(io_tute_force_match) == str(pdbid) or ispdb(io_tute_force_match):
              best_pdbid = str(pdbid)
              best_title = str(pdbid_title)
              best_maxcc = ic.cc
              if best_maxcc is not None:
                basetng.logTab(enum_out_stream.logfile, "New force match " + best_pdbid + " CC " + str(round(best_maxcc, 3)))
                if best_maxcc >= io_perfect_match_cc:
                  basetng.logChevron(enum_out_stream.logfile, "Perfect match with CC>=" + str(io_perfect_match_cc))
                  basetng.input.set_as_str(".phasertng.isostructure.found.perfect_match.","True")
                basetng.logTab(enum_out_stream.logfile, "title: " + pdbid_title)
                foundpdb = infol
                thisfoundpdb = True
                best_ope = ic.operation
                best_crystal = ic.crystal_match
                best_niggli = ic.niggli_match
                basetng.logChevron(enum_out_stream.logfile, "New match")
                topnlist.append((pdbid, copy.deepcopy(ic.cc), copy.deepcopy(ic.operation)))
              break
          elif ic.cc is not None and best_maxcc is not None and ic.cc > best_maxcc:
            best_pdbid = str(pdbid)
            best_title = str(pdbid_title)
            best_maxcc = ic.cc
            basetng.logTab(enum_out_stream.logfile, "New best CC: " + str(round(best_maxcc, 3)) + " for " + best_pdbid)
            basetng.logTab(enum_out_stream.logfile, "title: " + pdbid_title)
            print_current_best = False
            if goodcc:
              foundpdb = infol
              thisfoundpdb = True
              best_ope = ic.operation
              best_crystal = ic.crystal_match
              best_niggli = ic.niggli_match
              # miller_indexed = copy.deepcopy(ic.miller_indexed)
              # basetng.logTab(enum_out_stream.logfile,"  The deposited data associated to the structure shows a CC of "+str(ic.cc)+" against the given data")
              basetng.logChevron(enum_out_stream.logfile, "New isostructure match")
              topnlist.append((pdbid, copy.deepcopy(ic.cc), copy.deepcopy(ic.operation)))
              if best_maxcc >= io_perfect_match_cc:
                basetng.logChevron(enum_out_stream.logfile, "Perfect match with CC>=" + str(io_perfect_match_cc))
                break
          if print_current_best:
            basetng.logTab(enum_out_stream.logfile, "Current best CC: " + str(round(best_maxcc, 3)) + " for " + best_pdbid)
            basetng.logTab(enum_out_stream.logfile, "title: " + best_title)

      # except Exception:
        except Exception as e:
          message = "" if str(e) is None else str(e)
          basetng.logTab(enum_out_stream.logfile,message)
          basetng.logTab(enum_out_stream.logfile,
                            "Data for PDBid \"" + str(pdbid) + "\" cannot be correctly interpreted")
          basetng.logTab(enum_out_stream.logfile, "Skipping...")

        if os.path.exists(pdbid + ".cif"):
          os.remove(pdbid + ".cif")
      else:
        basetng.logTab(enum_out_stream.logfile, "Download/Read unsuccessful")

      if thisfoundpdb:
        nfoundpdb = nfoundpdb + 1
      if nfoundpdb >= TOPEXPLORE:  # if there are many matches, just look at the first 10
        basetng.logBlank(enum_out_stream.logfile)
        basetng.logBlank(enum_out_stream.logfile)
        basetng.logTab(enum_out_stream.logfile, "Reached limit of matching isostructures to explore")
        basetng.logBlank(enum_out_stream.logfile)
        break
   #}

    basetng.logBlank(enum_out_stream.logfile)
    basetng.logUnderLine(enum_out_stream.summary, "Isostructure")
    if foundpdb is None:
   #{
      basetng.logTab(enum_out_stream.summary, "No isostructure matching criteria")
      basetng.input.set_as_str(".phasertng.isostructure.found.match.","False")
      basetng.input.set_as_str(".phasertng.isostructure.found.perfect_match.","False")
   #}
    elif best_ope is not None:
   #{
      basetng.input.set_as_str(".phasertng.isostructure.found.match.","True")
      basetng.input.set_as_str(".phasertng.isostructure.found.perfect_match.","False")
      if (best_maxcc >= io_perfect_match_cc):
        basetng.input.set_as_str(".phasertng.isostructure.found.perfect_match.","True")
      pdbidmatch = str(list(foundpdb.keys())[0])
      if best_maxcc is not None:
        basetng.logTab(enum_out_stream.summary, "CC: " + str(round(best_maxcc, 2)))
        if (best_maxcc <= 0):
          basetng.logTab(enum_out_stream.summary, "Force Isostructure: " + pdbidmatch)
        else:
          basetng.logTab(enum_out_stream.summary, "Found Isostructure: " + pdbidmatch)
      # the operator returned from the compare algorithm is that for the setting of the crystal
      assert(DAGDB.size() == 1)
      node = DAGDB.NODES.LIST[0]
      node.PDBID = pdbidmatch
      DAGDB.NODES.set_node(0,node)

      cannot_download = False
      datap = None
      pdbid = list(foundpdb.keys())[0]
      verbose = False
      # must use cif format, pdb format is not guaranteed eg 1bos 8ouc
      basetng.logTab(enum_out_stream.verbose,"Try data type: model cif")
      can_download = False
      for mirror in mirrors:
        if can_download: break
        basetng.logTab(enum_out_stream.verbose,"Mirror: " + mirror)
        try:
          datap = iotbx.pdb.fetch(pdbid.lower(),
                        entity="model_cif",
                        mirror=mirror,
                        ).read().decode( system_utility.get_encoding())
          can_download = True
          basetng.logTab(enum_out_stream.verbose,"Success")
        except Exception as e:
          basetng.logTab(enum_out_stream.verbose,"Failure")
          basetng.logTab(enum_out_stream.verbose,str(e))

      if not can_download:
        basetng.logTab(enum_out_stream.verbose,"Try data type: model pdb")
        for mirror in mirrors:
          if can_download: break
          basetng.logTab(enum_out_stream.verbose,"Mirror: " + mirror)
          try:
            datap = iotbx.pdb.fetch.fetch(pdbid.lower(),
                        entity="model_pdb",
                        mirror=mirror,
                        ).read().decode(system_utility.get_encoding())
            can_download = True
            basetng.logTab(enum_out_stream.verbose,"Success")
          except Exception as e:
            basetng.logTab(enum_out_stream.verbose,"Failure")
            basetng.logTab(enum_out_stream.verbose,str(e))

      if not can_download:
        basetng.logTab(enum_out_stream.logfile,"Try read local data type: Pdb")
        try:
            # can get a pdb file and calculate structure factors
            datap = iotbx.pdb.input(io_tute_force_match).as_pdb_string()
            can_download = True
            basetng.logTab(enum_out_stream.verbose,"Success")
        except Exception as e:
            basetng.logTab(enum_out_stream.verbose,"Failure")
            basetng.logTab(enum_out_stream.verbose,str(e))
            if False:
              print(sys.exc_info())
              traceback.print_exc(file=sys.stdout)
            cannot_download = True

      if cannot_download:
     #{
        basetng.logTab(enum_out_stream.summary, "Cannot download")
     #}
      else:
     #{
        pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(datap)))
        miller_arrays = mtzobj.file_object.as_miller_arrays(crystal_symmetry=mtzobj.crystal_symmetry())
        mtz_dataset = None
        left_out_labels = []
        for k,ma in enumerate(miller_arrays):
          if simple_tools.ma_selection(ma):
            try:
              label,labels = simple_tools.ma_mtz_labels(miller_arrays,k,ma)
              if not mtz_dataset:
                mtz_dataset = ma.as_mtz_dataset(column_root_label=label)
              else:
                mtz_dataset.add_miller_array(ma, column_root_label=label)
            except Exception:
              message = "Cannot add miller array 1"
              raise ValueError(enum_err_code.fatal,message)
          else:
            # AJM TODO don't know how to add miller arrays that are not as above
            # looks like you have to use add column with flex array of data
            # there might be an easier way (?)
            label,labels = simple_tools.ma_mtz_labels(miller_arrays,k,ma)
            left_out_labels.append(str(labels))  # str labels because it is an array
        #  mtz_dataset.add_miller_array(ma, column_root_label=labels)
        assert(mtz_dataset is not None)
        mtz_object = mtz_dataset.mtz_object()
        assert(mtz_object is not None)
        basetng.logTab(enum_out_stream.summary, "Reindexing operator:    " + str(best_ope))
        mtz_object.change_basis_in_place(cb_op=change_of_basis_op(best_ope))
        basetng.logTab(enum_out_stream.summary, "Query Crystal:")
        for line in flex.split_lines(str(mtzobj.crystal_symmetry())):
          basetng.logTab(enum_out_stream.summary, "| " + line)
        pg1 = mtzobj.crystal_symmetry().space_group().build_derived_point_group().info().type().lookup_symbol()
        basetng.logTab(enum_out_stream.summary, "| Point Group: " + str(pg1))
        # pdb_input is the new crystal
        basetng.logTab(enum_out_stream.summary, "Isostructure Crystal:")
        for line in flex.split_lines(str(pdb_input.crystal_symmetry())):
          basetng.logTab(enum_out_stream.summary, "| " + line)
        pg2 = pdb_input.crystal_symmetry().space_group().build_derived_point_group().info().type().lookup_symbol()
        basetng.logTab(enum_out_stream.summary, "| Point Group: " + str(pg2))
        import re
        pg1str = re.sub(r'\([^)]*\)', '', pg1).strip()
        pg2str = re.sub(r'\([^)]*\)', '', pg2).strip()

      # mtz_object.set_space_group(pdb_input.crystal_symmetry().space_group())
      # mtz_object = mtz_dataset.mtz_object()
        newspacegroup = str(mtz_object.space_group_info())
        basetng.input.set_as_str(".phasertng.isostructure.found.spacegroup.",str(newspacegroup))
        basetng.input.set_as_str(".phasertng.isostructure.found.pdbid.",str(pdbidmatch))
        if (pg1str != pg2str):
       #{
          basetng.logBlank(enum_out_stream.summary)
          basetng.logTab(enum_out_stream.summary, "Point Group 1: " + str(pg1))
          basetng.logTab(enum_out_stream.summary, "Point Group 2: " + str(pg2))
          basetng.logWarning("Point groups are not the same between structure and isostructure, expand or merge input data")
          pg_dataset = None
          sg = pdb_input.crystal_symmetry().space_group().info()
          millers = mtz_object.as_miller_arrays()
          for k,ma in enumerate(millers):
            if ma.info() is not None and ma.info().labels is not None:
               #minimal selection condition
              outstr = TextIO()
              label,labels = simple_tools.ma_mtz_labels(millers,k,ma)
              ma1 = ma.change_symmetry(space_group_info=sg,log=outstr)
              ma1.set_info(ma.info())
              ma = ma1
              for line in outstr.getvalue().split("\n"):
                basetng.logTab(enum_out_stream.verbose,line)
              if pg_dataset is None:
                pg_dataset = ma1.as_mtz_dataset(column_root_label=label)
              else:
                #2evr fails passing just ma to add_miller_array
                pg_dataset.add_miller_array(miller_array=ma1, column_root_label=label)
          mtz_dataset = pg_dataset
          mtz_object = mtz_dataset.mtz_object()
          if (pg1str > pg2str):
            basetng.input.set_as_str(".phasertng.isostructure.found.pointgroup.","expansion")
          else:
            basetng.input.set_as_str(".phasertng.isostructure.found.pointgroup.","reduction")
       #}
        else:
       #{
          basetng.input.set_as_str(".phasertng.isostructure.found.pointgroup.","same")
          mtz_object.set_space_group(pdb_input.crystal_symmetry().space_group())
          mtz_object = mtz_dataset.mtz_object()
          basetng.logTab(enum_out_stream.summary,
            "Reindexed Space Group : " + str(mtz_object.space_group_info()))
          newspacegroup = str(mtz_object.space_group_info())

          basetng.logTab(enum_out_stream.summary, "Columns removed from mtz file: ")
          if len(left_out_labels) > 0:
            basetng.logTabArray(enum_out_stream.summary, left_out_labels)
          else:
            basetng.logTab(enum_out_stream.summary, "(none)")
       #}

        basetng.logTab(enum_out_stream.summary, "Reindexed Crystal:")
        for line in flex.split_lines(str(mtz_object.crystals()[0].crystal_symmetry())):
          basetng.logTab(enum_out_stream.summary, "| " + line)
        pg2 = mtz_object.crystals()[0].crystal_symmetry().space_group().build_derived_point_group().info().type().lookup_symbol()

       #{
        where = enum_out_stream.verbose
        basetng.logUnderLine(where, "Mtz file summary")
        sio = StringIO()
        mtz_object.show_summary(out=sio, prefix="")
        lines = flex.split_lines(str(sio.getvalue()))
        # need to convert to standard setting
        outxt = []
        for line in lines:
          outxt.append(line)
        basetng.logTabArray(where, outxt)
       #}

        where = enum_out_stream.logfile
        basetng.logUnderLine(where,"Prepare structure")
       #{ generate output data whether output or none
        reflid = DAGDB.NODES.LIST[0].REFLID #type hack
        reflid.initialize_from_text(pdbidmatch)
        reflid.initialize_tag(pdbidmatch)
        basetng.logTab(where,"Identifier: " + str(reflid.str()))
        basetng.logTab(where,"Cleanup structure to single model with no hetero atoms")
        pdb_input = iotbx.pdb.input(source_info=str(pdbid),lines=flex.split_lines(str(datap)))
        pdbih = pdb_input.construct_hierarchy()
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        wf = basetng.Write()
        if wf and not FileSystem().flat_filesystem() and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
          os.makedirs(os.path.join(basetng.DataBase(),subdir))
        nchains,unknown,pdbih = cleanup_hierarchy(pdbih)
        if (nchains == 0):
          basetng.logWarning("Cleanup of structure resulted in no chains")
          basetng.logTab(where,"Reverting to full structure")
          pdb_input = iotbx.pdb.input(source_info=str(pdbid), lines=flex.split_lines(str(datap)))
          pdbih = pdb_input.construct_hierarchy()
       #}
       #{
        where = enum_out_stream.verbose
        basetng.logTab(where,"Sequence from structure")
        seqlist = generate_list_of_pdb_hierarchy_by_chain(pdbih)
        seqdata = ""
        for seq in seqlist:
          for chain in seq.models()[0].chains():
            basetng.logTab(where,"Chain: \'" + str(chain.id) + "\'" + " residues=" + str(len(chain.as_padded_sequence())))
            lines = ''.join(chain.as_sequence())
            lines = format_sequence(lines)
            for line in lines:
              basetng.logTab(where,line)
            seqdata += ">" +reflid.str() + " chain " + str(chain.id) + "\n"
            seqdata += chain.as_padded_sequence() + "\n"
       #}
        where = enum_out_stream.summary
        basetng.logUnderLine(where,"Database Entries")
       #{
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        wf = basetng.Write()
        if wf and not FileSystem().flat_filesystem() and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
            os.makedirs(os.path.join(basetng.DataBase(),subdir))
        pdbout = reflid.extra_file_stem_ext(".pdb")
        seqout = reflid.extra_file_stem_ext(".seq")
        hklout = reflid.other_data_stem_ext(enum_other_data.data_mtz)
        wf = basetng.WriteData()
        newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
        basetng.logFileWritten(where,wf,"Coordinates",newpdbname)
        newseqname = FileSystem(basetng.DataBase(),subdir,seqout)
        basetng.logFileWritten(where,wf,"Sequence",newseqname)
        newmtzname = FileSystem(basetng.DataBase(),subdir,hklout)
        basetng.logFileWritten(where,wf,"Reflections",newmtzname)
        if (wf):
          pdbih.write_pdb_file(newpdbname.fstr())
          basetng.input.set_as_str(".phasertng.pdbout.filename.",str(newpdbname.fstr()))
        if (wf):
          with open(newseqname.fstr(), "w") as f:
            f.write(seqdata)
          basetng.input.set_as_str(".phasertng.seqout.filename.",str(newseqname.fstr()))
        if (wf):
          mtz_object.write(newmtzname.fstr())
          basetng.input.set_as_str(".phasertng.hklout.filename.",str(newmtzname.fstr()))
       #}

        if basetng.input.get_as_str(".phasertng.isostructure.found.perfect_match.").strip() == "False":
       #{ also split the structure into chains for rigid body refinement
          where = enum_out_stream.summary
          basetng.logUnderLine(where,"Split Pdb File by Chain")
          if len(pdbih.models()) != 1:
            pdbih = pdbih.pdbih.models()[0].as_new_hierarchy()
          model = pdbih.models()[0]
          sel_cache = pdbih.atom_selection_cache()
          nothetero = sel_cache.selection("not hetero and not water") #water will give wrong number of nres
          pdbih = pdbih.select(nothetero)
          chainlist = []
          for chain in model.chains():
            nres = 0
            main_conf = chain.conformers()[0]
            good_chain_type = (main_conf.is_na() or main_conf.is_protein())
            if not good_chain_type:
              basetng.logTab(where,"Chain type not recognised as protein or rna or dna")
            else:
              for rg in chain.residue_groups():
                nres += 1
              basetng.logTab(where,"Chain \"" + chain.id + "\" number of residues=" + str(nres))
              chainh = chain.as_new_hierarchy()
              pdbout = reflid.extra_file_stem_ext("_" + chain.id + ".pdb") #underscore for ->tag
              newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
              basetng.logFileWritten(where,wf,"Chain Coordinates",newpdbname)
              if (wf):
                chainh.write_pdb_file(newpdbname.fstr())
                chainlist.append(newpdbname.fstr())
          n = len(chainlist)
          basetng.input.resize(".phasertng.search_combination.",n)
          for i,combo in enumerate(chainlist):
            combo = combo.lstrip().rstrip()
            basetng.input.set_as_str_i(".phasertng.search_combination.",str(combo),int(i))
       #}
        else:
          basetng.logUnderLine(where,"Split Pdb File by Chain")
          basetng.logTab(where,"Pdb not split by chains as the match is a perfect match")
          basetng.input.resize(".phasertng.search_combination.",1)
          combo = basetng.input.get_as_str(".phasertng.pdbout.filename.")
          basetng.input.set_as_str_i(".phasertng.search_combination.",str(combo),int(0))
     #}
   #}
    return basetng
 #}}}

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-i',"--input",help="directory path walked for data files",required=True)
  parser.add_argument('-o',"--output",help="database directory for output",required=True)
  parser.add_argument('-t',"--tutorial",dest='tutorial',action='store_true',help="tutorial mode",required=False)
  parser.add_argument('-f',"--force",dest='force',default=None,help="force match with pdbid",required=False)
  args = parser.parse_args()
  basetng = Phasertng("VOYAGER")
  basetng.SetDataBase(args.output)
  runBEAM().inner_isostructure(
        basetng=basetng,
        io_hklin=str(args.input),
        io_tute_ignore_best=bool(args.tutorial),
        io_tute_force_match=(args.force),
        io_mirrors=['rscb','pdbe','pdbj'],
        io_tute_ignore_match=None, #use the phil
        io_perfect_match_cc=None, #use the phil
        delta_dim=float(10),
        delta_angle=float(5),
        MAXNCDIST=float(4.0),
        TOPEXPLORE=float(3),
        MINCCTHRESH=float(0.4),
        high_resolution=float(3),
        matches_cut_off=float(75),
        directd=".")
