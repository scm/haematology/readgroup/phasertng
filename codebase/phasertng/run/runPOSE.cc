//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Pose_scoring
  void Phasertng::runPOSE()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_curl())
    throw Error(err::INPUT,"Dag not prepared with correct mode (translation function failed?)");
    if (input.value__boolean.at(".phasertng.dag.skip_if_complete.").value_or_default() and
        DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete, skipping");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("curl");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    bool io_rescore =  input.value__boolean.at(".phasertng.pose_scoring.rescore.").value_or_default();
    int io_nprint =  input.value__int_number.at(".phasertng.pose_scoring.maximum_printed.").value_or_default();
    double lowerB = REFLECTIONS->bfactor_minimum();
    if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
      lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    double upperB = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();

    //initialize tNCS parameters from reflections and calculate correction terms
    //POSE
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Sort");
    if (REFLECTIONS->MAP)
      DAGDB.NODES.apply_sort_mapllg();
    else
      DAGDB.NODES.apply_sort_llg();
    logEllipsisShut(where);
    }}

    if (!REFLECTIONS->MAP and Suite::Extra())
    { //curl map likelihood not implemened
      out::stream where = out::VERBOSE;
      int n(1),N(DAGDB.size());
      DAGDB.restart();
      //progress will not appear in verbose mode
      bool store_halfR(true);
      bool store_fullR(false);
      Epsilon epsilon;
      epsilon.setup_parameters(REFLECTIONS.get());
      epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);
      while (!DAGDB.at_end())
      {
        auto& work = DAGDB.WORK;
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Scoring Curl" +nN);
        double llg = REFLECTIONS->MAP ? work->MAPLLG : work->LLG;
        logTab(where,"Stored Curl llg " + dtos(llg));
        bool changed = load_reflid_for_node(work->REFLID,where);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work->SG);
        if (changed or diffsg)
        {
          if (changed) logTab(where,"Reflections changed to " + work->REFLID.str());
          double resolution = work->search_resolution(io_hires);
          auto seltxt = selected.set_resolution(
              REFLECTIONS->data_hires(),
              DAGDB.resolution_range(),
              resolution);
          if (changed) logTabArray(where,seltxt);
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          PHASER_ASSERT(selected.nsel());
        }
        work->reset_parameters(REFLECTIONS->get_ztotalscat());
        bool adjusted = work->initKnownMR(
                     REFLECTIONS->data_hires(),upperB,lowerB);
        logTab(where,"B-factors adjusted for variance checks: " + btos(adjusted));
        DAGDB.apply_interpolation(interp::FOUR);
        if (REFLECTIONS->MAP)
        {
          DAGDB.work_node_phased_likelihood(
              &REFLCOLSMAP, //we have to use the base reflections here, not specialized code
              selected.get_selected_array(),
              NTHREADS,
              USE_STRICTLY_NTHREADS);
        }
        else
        {
          pod::FastPoseType ecalcs_sigmaa_pose;
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
        }
        if (DAGDB.negvar) logWarning("Negative variance ignored, contact developers");
        double generic_float = REFLECTIONS->MAP ? work->MAPLLG : work->LLG;
        logTab(where,"Calculated Curl llg " + dtos(generic_float));
        if (n == 2 and Suite::Extra())
          logTabArray(out::TESTING,work->logNode("Before conversion to Pose"+nN));
      }
    }

    //when the dag nodes are entered
    {{
    int n(1),N(DAGDB.size());
    DAGDB.restart();
    //progress will not appear in verbose mode
    out::stream progress = out::LOGFILE;
    logProgressBarStart(progress,"Scoring Poses",N,1,false);
    while (!DAGDB.at_end())
    {
      auto& node = DAGDB.WORK;
      node->generic_float = REFLECTIONS->MAP ? node->MAPLLG : node->LLG;
      node->NUM = n; //original number
      std::string nN = nNtos(n++,N);
      out::stream where = out::TESTING;
      logChevron(where,"Scoring Pose" +nN);
      logTab(where,"Stored Curl llg " + dtos(node->generic_float));
      bool changed = load_reflid_for_node(node->REFLID,where);
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
      if (changed or diffsg)
      {
        if (changed) logTab(where,"Reflections changed to " + node->REFLID.str());
        double resolution = node->search_resolution(io_hires);
        auto seltxt = selected.set_resolution(
            REFLECTIONS->data_hires(),
            DAGDB.resolution_range(),
            resolution);
        if (changed) logTabArray(where,seltxt);
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      }
      node->convert_curl_to_pose(REFLECTIONS->TNCS_VECTOR);
     // for (auto& pose : node->POSE)
     //    pose.BFAC = std::max(REFLECTIONS->bfactor_minimum(),pose.BFAC);
      if (n == 2 and Suite::Extra())
        logTabArray(out::TESTING,node->logNode("After conversion to Pose"+nN));
      if (node->fraction_scattering(REFLECTIONS->get_ztotalscat()) > 1)
      { //checked internally to work_node_likelihood but also check here, with extra logNode
        //if it fails, will fail here first
        logTabArray(where,node->logNode(nN+""));
        throw Error(err::DEVELOPER,"Total fraction scattering of posed ensembles is more than 1");
      }
      if (io_rescore)
      {
        node->reset_parameters(REFLECTIONS->get_ztotalscat());
        node->initKnownMR(REFLECTIONS->data_hires(),upperB,lowerB);
        DAGDB.apply_interpolation(interp::FOUR);
        if (REFLECTIONS->MAP)
        {
          DAGDB.work_node_phased_likelihood(
              &REFLCOLSMAP, //we have to use the base reflections here, not specialized code
              selected.get_selected_array(),
              NTHREADS,
              USE_STRICTLY_NTHREADS);
        }
        else
        {
          pod::FastPoseType ecalcs_sigmaa_pose;
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
        }
        if (DAGDB.negvar) logWarning("Negative variance ignored, contact developers");
        double generic_float2 = REFLECTIONS->MAP ? node->MAPLLG : node->LLG;
        logTab(where,"Calculated Pose llg " + dtos(generic_float2));
        //logTabArray(out::TESTING,node->logNode(nN+""));
      }
      if (!logProgressBarNext(progress)) break;
    }
    logProgressBarEnd(progress);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Sort");
    if (REFLECTIONS->MAP)
      DAGDB.NODES.apply_sort_mapllg();
    else
      DAGDB.NODES.apply_sort_llg();
    logEllipsisShut(where);
    }}

    {{
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      auto& work = DAGDB.WORK;
      double llg = REFLECTIONS->MAP ? work->MAPLLG : work->LLG;
      if (io_rescore)
        work->ANNOTATION += " POSE=" + dtoi(llg);
      else
        work->ANNOTATION += " POSE";
      if (nmol > 1)
        work->ANNOTATION += "+TNCS" + itos(work->TNCS_ORDER);
    }
    }}

    {{
    //set the star flags for the seek components corrrectly, with pose they become found
    out::stream where = out::LOGFILE;
    DAGDB.restart();
    int packs(0),clashes(0),unknown(0);
    while (!DAGDB.at_end())
    {
      DAGDB.WORK->set_star_from_pose();
      if (DAGDB.WORK->PACKS == 'Y') packs++;
      else if (DAGDB.WORK->PACKS == 'N') clashes++;
      else unknown++;
    }
    logTab(where,"Number of poses that are known to pack:  " + itos(packs));
    logTab(where,"Number of poses that are known to clash: " + itos(clashes));
    logTab(where,"Number of poses with packing unknown:    " + itos(unknown));
    }}

    {{
    auto& work = DAGDB.WORK;
    out::stream where = out::SUMMARY;
    logTableTop(where,"Pose");
    logTab(where,"Number of poses per solution: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"tNCS Order = " + itos(work->TNCS_ORDER));
    (nmol > 1) ?
      logTab(where,"tNCS applied to node entries") :
      logTab(where,"No tNCS to apply to node entries");
    std::string curlstr = (DAGDB.NODES[0].FSS == 'Y') ? "Curl-FSS" : "Curl-LLG";
    int w = std::max(4,itow(io_nprint));
    int ww = std::max(4,itow(DAGDB.size()));
    logTab(where,"Shift:[up(^)down(v)equal(=)]");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    logTab(where,snprintftos(
        "%-*s %*s Shift   %6s %10s %10s %-13s",
        ww,"in#",w,"out#","TFZ",curlstr.c_str(),"Pose-LLG","Space Group"));
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      char arrow = '=';
      if (i < node.NUM) arrow = '^'; //has been sorted higher in the list
      if (i > node.NUM) arrow = 'v';
      std::string diff("");
      if (i < node.NUM) diff = itos(node.NUM-i);
      if (i > node.NUM) diff = itos(i-node.NUM);
      double llg_printed = REFLECTIONS->MAP ? node.MAPLLG : node.LLG;
      logTab(where,snprintftos(
          "%-*d %*d %5s:%c %6.2f %10.*f %10s %-13s",
          ww,node.NUM,
          w,i,
          diff.c_str(),arrow,
          node.ZSCORE,
          q,node.generic_float,
          io_rescore ? dtos(llg_printed,10,q).c_str(): "-n/a-" ,
          node.SG->sstr().c_str()
          ));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (more > 0)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
