//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <cctype>
#include <phaser/src/SpaceGroup.h>
#include <phasertng/main/Error.h>
#include <cctbx/sgtbx/seminvariant.h>

namespace phaser {

SpaceGroup::SpaceGroup(std::string SG_HALL)
{ setSpaceGroupHall(SG_HALL); }

SpaceGroup::SpaceGroup(cctbx::sgtbx::space_group const& CctbxSG)
{ setSpaceGroup(CctbxSG.type().hall_symbol()); }

void SpaceGroup::setSpaceGroupHall(std::string SG_HALL)
{
  cctbxSG = cctbx::sgtbx::space_group(SG_HALL,"A1983");
  NSYMM = cctbxSG.order_z();
  NSYMP = cctbxSG.order_p();
  SYMFAC = NSYMM*(NSYMM/NSYMP);
  trasym.resize(NSYMM);
  rotsym.resize(NSYMM);
  for (unsigned isym = 0; isym < cctbxSG.order_z(); isym++)
  {
    for (int i = 0; i < 3; i++)
    {
      trasym[isym][i] = static_cast<floatType>(cctbxSG(isym).t()[i])/cctbxSG(isym).t().den();
      for (int j = 0; j < 3; j++)
        rotsym[isym](i,j) = static_cast<floatType>(cctbxSG(isym).r()(i,j))/cctbxSG(isym).r().den();
    }
    rotsym[isym] = rotsym[isym].transpose();
  }
 //should store the number as this is used for volumes
  spcgrpnum = cctbxSG.type().number();
}

dvect3 SpaceGroup::doSymXYZ(const int& isym, const dvect3& v)
{ return rotsym[isym].transpose()*v + trasym[isym]; }

std::string SpaceGroup::pgname()
{
  cctbx::sgtbx::space_group PG(cctbxSG.build_derived_point_group());
  std::string Point_Group("PG");
  for (int i = 1; i < PG.type().lookup_symbol().size(); i++)
    if (!(std::isspace)(PG.type().lookup_symbol()[i]))
      Point_Group += (std::toupper)(PG.type().lookup_symbol()[i]);
  return Point_Group;
}

std::string SpaceGroup::spcgrpname()
{
  space_group_name sg(cctbxSG.type().hall_symbol(),true);
  return sg.CCP4;
}

int  SpaceGroup::spcgrpnumber()
{ return spcgrpnum; }

char SpaceGroup::spcgrpcentring()
{ //fudge for cctbx
  std::string Extended_Hermann_Mauguin = cctbxSG.type().universal_hermann_mauguin_symbol();
  if (Extended_Hermann_Mauguin.find("R 3 :H (") != std::string::npos) return 'R';
  if (Extended_Hermann_Mauguin.find("R 3 2 :H (") != std::string::npos) return 'R';
  if (Extended_Hermann_Mauguin.find("R 3 :H") != std::string::npos) return 'H';
  if (Extended_Hermann_Mauguin.find("R 3 2 :H") != std::string::npos) return 'H';
  return cctbxSG.conventional_centring_type_symbol();
}

std::string SpaceGroup::getHall() const
{ return cctbxSG.type().hall_symbol(); }

bool SpaceGroup::is_polar()
{
  cctbx::sgtbx::structure_seminvariants seminvariants(cctbxSG);
  af::small<cctbx::sgtbx::ss_vec_mod, 3> const &vm = seminvariants.vectors_and_moduli();
  for (int i=0; i<vm.size(); ++i) {
    if (vm[i].m != 0) continue;
    // allowed continuous origin shift:
    return true;
  }
  return false;
}

}//phaser
