#ifndef BOOST_ADAPTBX_TRIBOOL_CONVERSION_H
#define BOOST_ADAPTBX_TRIBOOL_CONVERSION_H

#include <boost/python/tuple.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/to_python_converter.hpp>
#include <boost/logic/tribool.hpp>

namespace boost_adaptbx { namespace boost_tribool_conversions {

/*
https://stackoverflow.com/questions/20823067/boost-python-to-python-converter-for-user-defined-python-class-in-extension-modu
The following C++ code will convert a tribool into the Python values True, False and None which is a partial solution:
Below may not work though, since the example is only for to_tribool not from_tribool
*/

  namespace detail {
    struct to_tribool
    {
      static PyObject* convert(boost::logic::tribool const& p) {
        using namespace boost::python;
        if (p == true) return boost::python::incref(boost::python::object(true).ptr());
        else if (p == false) return boost::python::incref(boost::python::object(false).ptr());
        else return boost::python::incref(boost::python::object().ptr());
      }

      static PyTypeObject const *get_pytype() { return &PyTuple_Type; }
    };
  }

  struct to_tribool
  {
    to_tribool() {
      using namespace boost::python;
      to_python_converter<boost::logic::tribool, detail::to_tribool
#ifdef BOOST_PYTHON_SUPPORTS_PY_SIGNATURES
                                    , true
#endif
      >();
    }
  };

  struct from_tribool
  {
   typedef typename boost::logic::tribool::value_t value_type;

    from_tribool() {
      using namespace boost::python::converter;
      registry::push_back(&convertible,
                          &construct,
                          boost::python::type_id<boost::logic::tribool>()
                          #ifdef BOOST_PYTHON_SUPPORTS_PY_SIGNATURES
                          , get_pytype
                          #endif
      );
    }

    static const PyTypeObject *get_pytype() { return &PyTuple_Type; }

    static void *convertible(PyObject *obj_ptr)
    {
      if (obj_ptr != Py_None) {
        boost::python::extract<value_type> proxy(obj_ptr);
        if (!proxy.check()) return 0;
      }
      return obj_ptr;
    }

    static void construct(
      PyObject *obj_ptr,
      boost::python::converter::rvalue_from_python_stage1_data *data)
    {
      using boost::python::extract;
      using namespace boost::python::converter;
      boost::logic::tribool value;
      if (obj_ptr != Py_None) {
        boost::python::extract<value_type> proxy(obj_ptr);
        value = proxy();
      }
      void* storage = (
        (boost::python::converter::rvalue_from_python_storage<
          boost::logic::tribool>*)
            data)->storage.bytes;
      new (storage) boost::logic::tribool(value);
      data->convertible = storage;
    }
  };

  struct to_and_from_tribool
  {
    to_and_from_tribool() {
      to_tribool();
      from_tribool();
    }
  };


}}

#endif
