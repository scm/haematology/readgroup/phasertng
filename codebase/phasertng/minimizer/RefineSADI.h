//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineSADI_class__
#define __phasertng_RefineSADI_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phasertng/math/likelihood/rfactor/function.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <phasertng/io/entry/Substructure.h>

namespace phasertng {

class RefineSADI : public dtmin::RefineBase
{
  private:
    int    fixed_number_of_integration_steps = 0; //flag for not set

  private: //members
    int    NBINS = 0;
    bool   USE_WILB =  false;
    bool   REFINE_SIGA = false;
    bool   REFINE_PART = false;
    const double LOWER_O = 0.01;
    const double LOWER_B = 0.1;
    const double UPPER_B = 1000.;
    const double LOWER_FDP = 0.00001;
    double WILSON_U = 0;
    bool   HAS_PART;
    std::string SADI_ATOMTYPE;
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;
    std::map<std::string,double> INITIAL_FDP;

  private: //because complicated setter
    double SIGMA_SPHERICITY = 0;
    Substructure atoms;

  public:
    double SIGMA_FDP = 0;
    double SIGMA_WILSON_U = 0;

  private:
    dag::Node*           NODE;
    //working arrays
    dmat6 u_star_coeffs;
    dmat6 u_iso_coeffs;
    imat6 hess21_diagonal = imat6(0,6,11,15,18,20);
    dmat6 u_star_sphericity;
    dmat6 dBeq_by_du_star = dmat6(0,0,0,0,0,0);
    af_cmplex FP = af_cmplex();
    af_int    integration_points; //integration points per reflections

  public:
    //refined parameters stored here
 //   std::map<std::string,double> AtomFdp;
    const_ReflectionsPtr REFLECTIONS;
    SelectedPtr          SELECTED;
    ScatterersPtr        SCATTERERS;
    af_cmplex            SADI;

  public:  //constructor
    RefineSADI() : dtmin::RefineBase() {}
    RefineSADI(
        const_ReflectionsPtr,
        SelectedPtr,
        ScatterersPtr);
    ~RefineSADI() throw() {}

   public:
    void init_node(dag::Node*);
    void setup_completion_scatterer(std::string atomtype)
    { SADI_ATOMTYPE = atomtype; SADI.resize(REFLECTIONS->NREFL); }
    std::string completion_scatterer()
    { return SADI_ATOMTYPE; }

  private:
    TargetGradientHessianType
    combined_tgh( bool, bool, bool);

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string setup_parameters();
    af_string reject_outliers();
    af_string finalize_parameters();

  public:
    double Rfactor();
    TargetType likelihood();

  public:
    void set_fixed_number_of_integration_steps(int);
    void calculate_number_of_integration_steps();
    void set_sigma_sphericity(double);
    void set_threading(bool,int);
    af_string logSigmaa(std::string) const;
    std::pair<af_string,Loggraph> logFOM();
};

} //phasertng
#endif
