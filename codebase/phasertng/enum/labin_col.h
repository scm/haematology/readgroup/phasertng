#ifndef __phasertng_labin_col_enum__
#define __phasertng_labin_col_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace labin {
  //order in the order they will be written to mtz file
  //all data first I then F then Fmap
  //then space group or unit cell terms
  //then data derived terms feff dobs
  MakeEnum ( col,
             (UNDEFINED)
             (H)
             (K)
             (L)
             (INAT)   //data
             (SIGINAT)//data
             (IPOS)   //data
             (SIGIPOS)//data
             (INEG)   //data
             (SIGINEG)//data
             (FNAT)   //data
             (SIGFNAT)//data
             (FPOS)   //data
             (SIGFPOS)//data
             (FNEG)   //data
             (SIGFNEG)//data
             (MAPF)   //data
             (MAPPH)  //data
             (MAPDOBS)//data
             (BIN)    //constant for data
             (EPS)    //constant sg/uc
             (PHSR)   //constant sg/uc
             (SSQR)   //constant sg/uc
             (CENT)   //constant sg/uc
             (BOTH)   //constant sg/uc
             (PLUS)   //constant sg/uc
             (FEFFNAT)//data likelihood
             (DOBSNAT)//data likelihood
             (FEFFPOS)//data likelihood
             (DOBSPOS)//data likelihood
             (FEFFNEG)//data likelihood
             (DOBSNEG)//data likelihood
             (RESN)   //likelihood (includes ano)
             (ANISO)  //likelihood (anistropy with Biso)
             (ANISOBETA)  //likelihood (anistropy)
             (TEPS)   //likelihood
             (TBIN)   //likelihood
             (WLL)    //likelihood
             (SIGA)   //likelihood
             (FPART)  //model
             (PHPART) //model
             (FCNAT)  //model
             (PHICNAT)//model
             (FCPOS)  //model
             (PHICPOS)//model
             (FCNEG)  //model
             (PHICNEG)//model
             (FOM)    //hklout
             (FWT)    //hklout
             (PHWT)   //hklout
             (DELFWT) //hklout
             (DELPHWT)//hklout
             (LLGF)   //hklout
             (LLGPH)  //hklout
             (HLA)    //hklout
             (HLB)    //hklout
             (HLC)    //hklout
             (HLD)    //hklout
             (EINFO)  //hklout
             (INFO)   //hklout
             (SINFO)  //hklout
             (SELLG)  //hklout
             (SEFOM)  //hklout
             (FREE)   //carried
           ); //must make any additions to the boost enumerations also

struct EnumHash
{
  template <typename T>
  std::size_t operator()(T t) const
  {
    return static_cast<std::size_t>(t);
  }
};
}}
#endif
