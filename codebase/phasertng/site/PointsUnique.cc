//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/PointsUnique.h>
#include <scitbx/math/euler_angles.h>

namespace phasertng {

  PointsUnique::PointsUnique(
      cctbx::sgtbx::change_of_basis_op const &cb_op_original_to_sampling,
      cctbx::crystal::direct_space_asu::float_asu<double> const &float_asu,
      af::tiny< bool, 3 > const &continuous_shift_flags,
      double const &point_distance):
      SiteList()
  {
   generator = cctbx::crystal::close_packing::hexagonal_sampling_generator< double >(
      cb_op_original_to_sampling,
      float_asu,
      continuous_shift_flags,
      point_distance);
  }

  void
  PointsUnique::restart(unsigned seed_)
  { generator.restart(); }

  bool
  PointsUnique::at_end()
  { return generator.at_end(); }

  af_dvect3
  PointsUnique::all_sites()
  { return generator.all_sites_frac(); }

  dvect3
  PointsUnique::next_site()
  { return generator.next_site_frac(); }

  std::size_t
  PointsUnique::count_sites()
  { return generator.count_sites(); }

  double
  PointsUnique::point_distance()
  { return generator.point_distance(); }

}// phasertng
