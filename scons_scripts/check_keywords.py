from __future__ import print_function
from __future__ import division

from io import open #this covers the python 2 and python 3 encoding

import libtbx.load_env
import iotbx.phil
import sys, os, os.path
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from phasertng.phil.mode_generator import *
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
from master_phil_file import *
from converter_registry import *

def make_check_keywords_file():
  initparams = build_phil_str()
  assert(len(initparams))
  master_phil = parse(initparams,converter_registry=converter_registry)
  styles = {}
  for e in master_phil.all_definitions():
    keyword = str(e.path.split(".")[1])
    child_style = getattr(e.object, "style")
    for style in {child_style,e.parent.style,e.parent.primary_parent_scope.style}: #also check the parent
      if (style is not None):
        style = str(style)
        if ('tng:input' in style or 'tng:result' in style):
          styles[keyword] = style
# for k,v in sorted(styles.items()):
#   print(k,"\n   " +v)
#   print('styles',k)
# print(1/0)
  return styles

def mutable_input(line):
   #if "set_value" in line or \
   #   "push_back" in line or \
   #   "set_default" in line or \
   #   "process_" in line or \
   #   'from_python' in line:
   indeterminate = ").size()" in line or "input.size" in line or "clear()" in line
   i =  "value_or_default" in line or \
        ("get_as_str" in line and "ignore tng:" not in line) or \
        "is_default" in line or \
        ".name" in line or \
        "minval" in line or \
        "maxval" in line or \
        ("ext" in line and "labin" in line) #labin
   return (not i and not indeterminate) #output parameter
def not_mutable_input(line):
   indeterminate = ").size()" in line or "input.size" in line or "clear()" in line
   i =  "value_or_default" in line or \
        "input.size" in line or \
        "set_as_def" in line or \
        ("get_as_str" in line and "logTab" not in line and "ignore tng:" not in line) or \
        "is_default" in line or \
        "minval" in line or \
        "maxval" in line or \
        ".name" in line or \
        ("ext" in line and "labin" in line) #labin
   return (i and not indeterminate) #input parameter

#print os.path.dirname(__file__)
def find_code_params(file_path,find_params,code_params,flat_params,name,bad_params,iattributes,oattributes):
#   print('-- file %s' % (file_path))
    modestr = name[:-3][3:].lower()
    if modestr.endswith("."): #cpp
      if modestr.startswith("e"):
        modestr = "emm"
      elif modestr.startswith("m"):
        modestr = "msm"
    with open(file_path, 'r',encoding="utf8", errors='ignore') as f:
      f_content = f.read()
      if file_path.endswith(".py"): #check python scripts
        for param in flat_params:
          for ext in " :=)":
            pyparam = param[:-1].lower() + ext #remove final .
            if pyparam in f_content:
              find_params[param] = True
      lines = f_content.split("\n")
      for line in lines:
        comment = line.replace(" ","")
        if not comment.startswith("//"): # not a c++ commented out line
          for key,val in flat_params.items():
            param_str = "\"." + phil_scope + key + "\"" #const input
            if param_str in line:
              find_params[key] = True
              newval = "value__" + val[6:].lower()
             #print(key,newval)
              if ("value__" in line or "array__" in line) and not val in line:
                bad_params.add(key + " (" + file_path.split("/")[-1] + ") (" + line + ")")
            #hardwire the hidden parses here
          # if "scatterers.parse" in line:
          #   iattributes[modestr].add("fluorescence_scan")
          #   iattributes[modestr].add("sasaki_table")
          #   iattributes[modestr].add("form_factors")
          # if "selected.parse" in line:
          #   iattributes[modestr].add("outlier")
          #   iattributes[modestr].add("resolution")
          # if "resultnma.parse" in line:
          #   iattributes[modestr].add("ddm")
          #   iattributes[modestr].add("sceds")
      #  print name , line
          code_param = ""
          try:
            first = "\"." + phil_scope #\".phasertng.
            last =  ".\""
            start = line.index( first ) + len( first )
            end = line.index( last, start )
            code_param = line[start:end] + "."
            if name.startswith("run"):
              code_paramkey = code_param.split('.')[1]
            # if mutable_input(line) or not_mutable_input(line):
            #   print("Debug: ",name,code_paramkey,line,mutable_input(line),not_mutable_input(line))
              if mutable_input(line):
                oattributes[modestr].add(code_paramkey)
              if not_mutable_input(line):
                iattributes[modestr].add(code_paramkey)
          except ValueError:
            code_param = ""
          if len(code_param) and \
             not (code_param in flat_params.keys()) and \
             not ("ext" in code_param) and \
             not (".labin." == code_param): #keyword generated with variable
            code_params.add(name + "::    " + code_param)

def check_keywords(source,target,env):
  #read phil file
  flat_params = dict()
  initparams = build_phil_str() # not just Input!!
  assert(len(initparams))
 #print(initparams)
  master_phil = parse(initparams,converter_registry=converter_registry)

  for e in master_phil.all_definitions():
   #if (str(e.path.split(".")[-1])) == "file":
   #  print("Error: phil input cannot use name \"file\", use \"filename\" instead")
   # ( assert(False)
     extract_attribute_error = False
     if (getattr(e.object, "type", extract_attribute_error) is not extract_attribute_error):
       valuetype = str(getattr(e.object, "type"))
       phasertype = ""
       if valuetype.startswith("bool"):
         phasertype = "type::boolean"
       elif valuetype.startswith("space_group"):
         phasertype = "type::spacegroup"
       elif valuetype.startswith("uuids"):
         phasertype = "type::uuid_numbers"
       elif valuetype.startswith("uuid"):
         phasertype = "type::uuid_number"
       elif valuetype.startswith("filesystem"):
         phasertype = "type::path"
       elif valuetype.startswith("path"):
         phasertype = "type::path"
       elif valuetype.startswith("strings"):
         phasertype = "type::strings"
       elif valuetype.startswith("stream"):
         phasertype = "out::stream"
       elif valuetype.startswith("str"):
         phasertype = "type::string"
       elif valuetype.startswith("atom_selection"):
         phasertype = "type::string"
       elif valuetype.startswith("scatterers"):
         phasertype = "type::strings"
       elif valuetype.startswith("scatterer"):
         phasertype = "type::string"
       elif valuetype.startswith("choice"):
         phasertype = "type::choice"
       elif valuetype.startswith("ints(size=2"):
         phasertype = "type::int_paired"
       elif valuetype.startswith("ints(size=3"):
         phasertype = "type::int_vector"
       elif valuetype.startswith("ints"):
         phasertype = "type::int_numbers"
       elif valuetype.startswith("int"):
         phasertype = "type::int_number"
       elif valuetype.startswith("floats(size=2") and "value_max=100" in valuetype:
         phasertype = "type::percent_paired"
       elif valuetype.startswith("floats(size=2"):
         phasertype = "type::flt_paired"
       elif valuetype.startswith("floats(size=3"):
         phasertype = "type::flt_vector"
       elif valuetype.startswith("unit_cell"):
         phasertype = "type::flt_cell"
       elif valuetype.startswith("floats(size=9"):
         phasertype = "type::flt_matrix"
       elif valuetype.startswith("floats"):
         phasertype = "type::flt_numbers"
       elif valuetype.startswith("float(value_min=0") and "value_max=100" in valuetype: #problems other ways
         phasertype = "type::percent"
       elif valuetype.startswith("float"):
         phasertype = "type::flt_number"
       elif valuetype.startswith("mtzcol"):
         phasertype = "type::mtzcol"
       if (len(phasertype) == 0):
         print(e.path,valuetype)
         assert(len(phasertype) > 0)
       fp = e.path.replace(phil_scope,"").lower() + "."
      #phasertype = "value__" + phasertype[6:].lower()
       phasertype = phasertype[6:].lower()
       flat_params[fp] = phasertype

  #print(flat_params)
  find_params = {}
  iattributes = defaultdict(set)
  oattributes = defaultdict(set)
  code_params = set([])
  bad_params = set([])
  for key,val in flat_params.items():
    if ("labin" not in key): # special, MTZCOL
      find_params[key] = False
 #print('')
  maincc =  os.path.join(env.dist_path,"codebase","phasertng","main","Phasertng.cc")
  #load all the attributes that are in every mode because they are in Phasertng.cc
  print('Checking File: ' + os.path.abspath(maincc))
  modestrs = sorted(mode_generator().possible_modes())
  with open(maincc, 'r',encoding="utf8", errors='ignore') as f:
      f_content = f.read()
      lines = f_content.split("\n")
      for line in lines:
        comment = line.replace(" ","")
        if not comment.startswith("//"): # not a c++ commented out line
          first = "\"." + phil_scope #\".phasertng.
          last =  ".\""
          if len(line.split(first)) > 1:
            lstart = line.split(first)[1]
            if len(lstart.split(".")) > 1:
              code_param = str(lstart.split(".")[1])
              if code_param != "mode" and code_param != "put_solution": #silly to put mode in list? #put_solution for put1 etc
                for modestr in modestrs:
                  if mutable_input(line):
                    oattributes[modestr].add(code_param)
                  if not_mutable_input(line):
                    iattributes[modestr].add(code_param)
 #print(oattributes)
 #print(iattributes["mode"])
  for subdir in ["main","run","data"]: #speed, select subdirs of interest
    walk_dir =  os.path.join(env.dist_path,"codebase","phasertng",subdir)
    print('Checking Path: ' + os.path.abspath(walk_dir))
    for (dirpath, dirnames, filenames) in os.walk(walk_dir):
      for name in filenames:
        if not name.startswith(".") and name.endswith(".cc"):
          file_path = os.path.join(dirpath, name)
          find_code_params(file_path=file_path,find_params=find_params,code_params=code_params,flat_params=flat_params,name=name,bad_params=bad_params,iattributes=iattributes,oattributes=oattributes)
        elif subdir == "run" and not name.startswith(".") and name.endswith(".cpp"):
          file_path = os.path.join(dirpath, name)
          find_code_params(file_path=file_path,find_params=find_params,code_params=code_params,flat_params=flat_params,name=name,bad_params=bad_params,iattributes=iattributes,oattributes=oattributes)
  for pysubdir in ['scripts','run']:
    walk_dir = os.path.join(env.dist_path, "phasertng",pysubdir)
    print('Checking Path: ' + os.path.abspath(walk_dir))
    for (dirpath, dirnames, filenames) in os.walk(walk_dir):
      for name in filenames:
        if not name.startswith(".") and name.endswith(".py"):
          file_path = os.path.join(dirpath, name)
          find_code_params(file_path=file_path,find_params=find_params,code_params=code_params,flat_params=flat_params,name=name,bad_params=bad_params,iattributes=iattributes,oattributes=oattributes)

 #print("Checking styles:")
  attributes = defaultdict(list) # two element list, inputstr and outputstr
  inv_iattributes = defaultdict(set)
  for key,val in iattributes.items():
    for v in val:
      inv_iattributes[v].add(key)
  for key,val in inv_iattributes.items():
    inputstr = " tng:input:"
    lval = sorted(val)
    for v in lval:
      if key == 'reflid' and v in [ "chk","test","tree","sceds","bub","walk","rfac","beam","xref","srfp" ]:
        continue
      if key == 'dag' and v in [ "chk","test","sceds","tree" ]:
        continue
      if key == 'threads' and v in [ "tree" ]:
        continue
      if len(v): #paranoia
        inputstr = inputstr + "+" + v;
    attributes[key].append(inputstr)
  #---
  inv_oattributes = defaultdict(set)
  for key,val in oattributes.items():
    for v in val:
      inv_oattributes[v].add(key)
  for key,val in inv_oattributes.items():
    outputstr = " tng:result:"
    lval = sorted(val)
    for v in lval:
      if key == 'dag' and v in [ "tree" ]:
        continue
      if len(v): #paranoia
        outputstr = outputstr + "+" + v;
    attributes[key].append(outputstr)
  #---
  f1_text = {}
  for key,val in sorted(attributes.items()):
    stylestr = ""
    for v in sorted(val):
      stylestr = stylestr + v # append inputstr and outputstr
    f1_text[str(key)] = str(stylestr).strip()
  f2_text = make_check_keywords_file()
  difftext = ""
  for key,v1 in f1_text.items():
    if key in f2_text:
      v2 = f2_text[key]
      if v1 != v2:
        difftext += key + " -fail\n   ---" + v2 + "\n   +++" + v1 + "\n"
    # else:
    #   difftext +=  key + " -ok\n"
    else:
      print(key,"not in phil file")
  if difftext == "":
    print("No styles differences")
  else:
    print(difftext)
# with open(os.path.join(env.dist_path,"scons_scripts","check_styles.txt"), 'w') as cs:
#   for line in f1_text:
#     cs.write(line+"\n")
#   cs.close()

  there_are_warnings = False
  for keys,values in sorted(find_params.items()):
    if not values:
      print("Warning:     phil parameter not used in code \"" + keys + "\"")
      there_are_warnings = True
  if not there_are_warnings:
      print("No Warnings: all phil parameters used in code")
  there_are_errors = False
  for item in code_params:
      print("Error:       program parameter not in phil \"" + item + "\"")
      there_are_errors = True
  for item in bad_params:
      print("Error:       program parameter wrong type in code \"" + item + "\"")
      there_are_errors = True
  if not there_are_errors:
      print("No Errors:   all program parameters are in phil")
  print("Number of keywords = " + str(len(implemented)))
  print("Windows compiler max nested control structures about 100")
  print('')

# if 'blabla' in open('example.txt').read():
#   print("true")

if __name__ == "__main__":
  source = ""
  target = ""
  os.phil_params_file  = os.path.join("..","phasertng","phenix_interface","__init__.params")
  os.dist_path  = os.path.join("..")
  os.autogen  = os.path.join("..","..","..","build","phasertng_autogen")
  check_keywords(source,target,env=os)
