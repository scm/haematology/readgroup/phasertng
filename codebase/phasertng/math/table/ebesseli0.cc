#include <phasertng/math/table/ebesseli0.h>
#include <cmath>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace table {

constexpr const double table::ebesseli0::eBesselI0Table1[4097][2];
constexpr const double table::ebesseli0::eBesselI0Table2[4097][2];
constexpr const double table::ebesseli0::eBesselI0InvTable[4097][2];
  ebesseli0::ebesseli0()
  {
    argfac1 = double(tbllen/argcut1);
    argfac2 = double(tbllen/argcut2);
    invArgFac=(tbllen*argcut2);
  }

  double
  ebesseli0:: get(double arg) const
  {
    if (arg < argcut1)
    {
      double fracTableArg = argfac1*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return eBesselI0Table1[index][0] +
                 (fracTableArg-index)*eBesselI0Table1[index][1];
    }
    else if (arg < argcut2)
    {
      double fracTableArg = argfac2*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return eBesselI0Table2[index][0] +
                 (fracTableArg-index)*eBesselI0Table2[index][1];
    }
    else
    {
      double invArg(1./arg);
      double fracTableArg = invArgFac*invArg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      phaser_assert(invArg > 0);
      return std::sqrt(invArg)*(eBesselI0InvTable[index][0] +
                 (fracTableArg-index)*eBesselI0InvTable[index][1]);
    }
    return 0;
  }

  ebesseli0 ebesseli0::inst_;

}}
