//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Spanning.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <phasertng/io/entry/Coordinates.h>
#include <phasertng/io/entry/Monostructure.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>
#include <phasertng/dag/Helper.h>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> Graph;

namespace phasertng {

  void
  Spanning::init_node(dag::Node* WORK)
  {
//AJM TODO
//if the clash matrix is already partly defined, then the pose order for the p loop
//MUST match the pose indexing in the pose clash_matrix (first element)
//otherwise the poses and the clash matrix will not be in sync
    NODE = WORK;
    UC = UnitCell(NODE->CELL);
    UC.scale_orthogonal_cell(NODE->DATA_SCALE);
    PHASER_ASSERT(!UC.is_default());
    SG = *NODE->SG;
  }

  void
  Spanning::structure_for_spanning(Coordinates& coordinates)
  { //in place modification of coordinates
    //if outputting all to the one file, use this loop below
    phaser_assert(NODE->POSE.size());
    for (int p = 0; p < NODE->POSE.size(); p++)
    {
      dag::Pose& pose = NODE->POSE[p];
      const Monostructure& ptrace = pose.ENTRY->MONOSTRUCTURE;
      phaser_assert(ptrace.size());
      dmat33 ROTmt;dvect3 FRACT;
      std::tie(ROTmt,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
      int natoms = ptrace.size();
      for (int isym = 0; isym < SG.rotsym.size(); isym++)
      {
        for (int a = 0; a < natoms; a++)
        {
          PdbRecord card = ptrace.PDB[a];
          //use the trace fixed on atom distances not the trace used for the packing
          if (card.is_protein_trace() or card.is_nucleic_trace()) //speed up the calculation
          if (card.AltLoc == " " or card.AltLoc == "A") //if after lattice translocation, ignore B
          {
            card.X = ROTmt*card.X + UC.orthogonalization_matrix()*FRACT;
            dvect3 frac = UC.fractionalization_matrix()*card.X;
            dvect3 site = SG.rotsym[isym].transpose()*frac + SG.trasym[isym];
            for (int i = 0; i < 3; i++)
            {
              while (site[i] >= 1) site[i] -= 1;
              while (site[i] <  0) site[i] += 1;
            }
            //take all values in the unit cell
            card.X = UC.orthogonalization_matrix()*site;
            coordinates.AddAtom(card);
          }
        }
      }
    }
    phaser_assert(coordinates.size());
  }

  std::pair<ivect3,af_string>
  Spanning::spans(double dist,Coordinates& coordinates,
     bool debug_dir,std::map<std::string,Coordinates>& debug_coords)
  {
    af_string output;
    int ncell = 1;
    const ivect3 ncells(ncell,ncell,ncell);
    dvect3 cellTrans(0,0,0);
    //list of those in unit box
    //edges in unit box
    std::vector<std::pair<int,int>> edges;
    double min_delta_sqr = fn::pow2(dist);
    for (int i = 0; i < coordinates.size(); i++)
    for (int j = i+1; j < coordinates.size(); j++)
    {
      auto delta = coordinates.PDB[i].X-coordinates.PDB[j].X;
      double delta_sqr = delta*delta;
      if (delta_sqr < min_delta_sqr)
        edges.push_back(std::pair<int,int>({i, j}));
    }
    int ncards = coordinates.size();
    sv_string dirstr = { "X","Y","Z" };
    int natoms = ncards*2;
    ivect3 connected(3);
    int dir(0);
    sv_dvect3 directions = { dvect3(1,0,0),dvect3(0,1,0),dvect3(0,0,1) };
    pdb_chains_2chainid chains;
    for (auto cellTrans : directions)
    { //list in unit box and the translated unit box: upwards,sideways,forwards
      sv_dvect3 coordXX(natoms);
      for (int x = 0; x < ncards; x++)
      {
        coordXX[x] = coordinates.PDB[x].X; //unit box
        dvect3 frac = UC.fractionalization_matrix()*coordinates.PDB[x].X + cellTrans;
        dvect3 orth = UC.orthogonalization_matrix()*frac; //translated unit box
        coordXX[x+ncards] = orth;
      }
      Graph c(coordXX.size()); //indexes 0 to natoms
      for (int e = 0; e < edges.size(); e++)
      { // edges of unit box, how the natoms are connected
        boost::add_edge(edges[e].first,edges[e].second, c); //put edges in data format
        boost::add_edge(edges[e].second,edges[e].first, c); //put edges in data format
        boost::add_edge(edges[e].first+ncards,edges[e].second+ncards, c);
        boost::add_edge(edges[e].second+ncards,edges[e].first+ncards, c);
      }
      //add edges of cross terms between boxes which give the connections
      for (int i = 0; i < ncards; i++)
      for (int j = ncards; j < coordXX.size(); j++)
      {
        auto delta = coordXX[i]-coordXX[j];
        double delta_sqr = delta*delta;
        if (delta_sqr < min_delta_sqr)
        {
          boost::add_edge(i,j, c); // add an edge between vertices 0 and 1 in the graph
          boost::add_edge(j,i, c); // add an edge between vertices 0 and 1 in the graph
        }
      }
      PHASER_ASSERT(boost::num_vertices(c) == natoms); //paranoia
      std::vector<int> component(natoms);
      size_t num_components = boost::connected_components(c,&component[0]);
      sv_int count(num_components);
      output.push_back(">>Doubled cell direction = " + dirstr[dir] + "<<");
      output.push_back("Number of atoms = 2*" + std::to_string(ncards));
      output.push_back("Number of connected components = " + std::to_string(num_components));
      //the component number that ends up being the largest seems to be random
      // are always a heap of components with only one atom in them
      //find the largest connecte_component and
      //and see if it connects the same atom in neighbouring cells
      for (size_t g = 0; g < num_components; g++)
      {
        for (size_t i = 0; i < natoms; i++)
          if (component[i] == g)
            count[g]++;
      }
      int maxg(0);
      for (size_t g = 0; g < num_components; g++)
      {
        if (count[g] > count[maxg])
          maxg = g;
      }
      //PHASER_ASSERT(maxg == 0); //??//paranoia
      output.push_back("Largest component = #" + std::to_string(maxg) + " with " + std::to_string(count[maxg]) + " atoms");
      //is the first atom in the same component as the first atom in neighbouring cell
      //component is indexed on atom -> component
      size_t g = maxg; //largest component
      int first_atom_g = 0; //first atom of largest component
      for (size_t i = 0; i < component.size(); i++)
      {
        if (component[i] == g)
        {
          first_atom_g = i;
          break;
        }
      }
      PHASER_ASSERT(first_atom_g < ncards);
      connected[dir] = (component[first_atom_g+ncards] == g) ? 1 : 0;
      output.push_back("First atom in largest component = " + std::to_string(first_atom_g));
      output.push_back("Connected = " + std::string(connected[dir] == 1 ?"Yes":"No"));
      if (debug_dir)
      {
        //init map of coordinates on direction
        debug_coords[dirstr[dir]] = coordinates; //half of final
        Coordinates& cdir = debug_coords[dirstr[dir]];
        for (size_t a = 0; a < ncards; a++)
        {
          PdbRecord& card = cdir.PDB[a]; //orig
          card.Chain = chains.index(component[a]);
        }
        for (size_t a = ncards; a < natoms; a++)
        {
          int aa = a-ncards;
          PdbRecord& card = coordinates.PDB[aa];
          card.X = coordXX[a];
          card.Chain = chains.index(component[a]);
          cdir.AddAtom(card);
        }
      }
      dir++;
    }
    return {connected,output};
  }

} //phasertng
