#ifndef __phasertng_table_gfunction_of_ssqr_class__
#define __phasertng_table_gfunction_of_ssqr_class__
#include <scitbx/constants.h>
#include <scitbx/array_family/shared.h>

// Assumption for this code is that G-function will be used when approximating
// the shape of a molecule as a sphere, in which case it doesn't make much sense
// to go past the first zero of the function.  Another approach should be used
// in other circumstances.

namespace phasertng {
namespace table {

class gfunction_of_ssqr
{
  private:
  // Accuracy about 1 part in 10^8 or better, zero to first root (return zero for larger args)
  // Quadratic interpolation would use smaller tables, but is slower
  // An alternative over this range would be a polynomial to about order 8 or 10
  int    tbllen;
  double argcut;
  double argfac;
  scitbx::af::shared<std::pair<double,double> > GfuncOfRSsqrTable;

  public:
  gfunction_of_ssqr(int tbllen_=8192,
                    double argcut_=0.51143712) :
          tbllen(tbllen_),
          argcut(argcut_)
  {
    argfac = double(tbllen/argcut);
    GfuncOfRSsqrTable = getGfuncOfRSsqrTable(tbllen,argcut);
  }

  double
  get(double arg) const
  {
    if (arg < argcut)
    {
      double fracTableArg = argfac*arg;
      int index = static_cast<int>(fracTableArg);
      return GfuncOfRSsqrTable[index].first + (fracTableArg-index)*GfuncOfRSsqrTable[index].second;
    }
    return Fn_GfuncOfRSsqr(arg);
  }

  private:
  double
  Fn_Gfunction(double twoPiRS) const
  {
    // Calculates G-function (Fourier transform of a sphere of radius r at resolution s.
    static double SMALL(0.1);
    if (std::abs(twoPiRS) > SMALL)
      return 3.*(std::sin(twoPiRS)-twoPiRS*std::cos(twoPiRS))/fn::pow3(twoPiRS);
    else
      return 1.-fn::pow2(twoPiRS)/10+fn::pow4(twoPiRS)/280; // good to 1 in 10^10
  }

  double
  Fn_GfuncOfRSsqr(double rsSqr) const
  {
    return Fn_Gfunction(scitbx::constants::two_pi*std::sqrt(rsSqr));
  }

  scitbx::af::shared<std::pair<double,double> >
  getGfuncOfRSsqrTable(int tbllen, double argmax)
  {
    scitbx::af::shared<std::pair<double,double> > GfuncOfRSsqrTable;
    GfuncOfRSsqrTable.resize(tbllen+1);
    double divarg(argmax/tbllen);
    for (unsigned i = 0; i < tbllen+1; i++)
    {
      double x0(Fn_GfuncOfRSsqr((i  )*divarg));
      double x1(Fn_GfuncOfRSsqr((i+1)*divarg));
      GfuncOfRSsqrTable[i].first = x0;
      GfuncOfRSsqrTable[i].second = x1 - x0;
    }
    return GfuncOfRSsqrTable;
  }

};

}}
#endif
