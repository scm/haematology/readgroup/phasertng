#There is no automatic code generation from this file
#This is only used for cards to phil and phil to cards
#The parse and unparse functions are hand-coded for speed

auto_implemented = {

#dryrun is expert level 5 to remain silent in documentation
"dryrun" : '''
  dryrun = False
    .type = bool
    .expert_level = 5
    .short_caption = Track solution pathway as dry run
''' ,

"picard" : '''
  control
    .expert_level = 1
  {
    terminate_on_rfactor = False
      .type = bool
      .expert_level = 1
      .short_caption = Terminate search on reaching termination rfactor
    termination_rfactor = 25
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Rfactor for acceptance of solution
    terminate_tncs_loop_on_rfactor = False
      .type = bool
      .expert_level = 1
      .short_caption = Terminate tncs loop on reaching termination rfactor for loops
    terminate_cell_content_scaling_loop_on_rfactor = True
      .type = bool
      .expert_level = 1
      .short_caption = Terminate restart at cell content scaling loop on reaching low rfactor, implies incremental search is successful
    loop_termination_rfactor = 35
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Rfactor on right track for solution, terminate tncs or cell content scaling loop
    restart_at_cell_content_scaling = None
      .type = bool
      .expert_level = 1
      .short_caption = Clear partial solutions accumulated in previous loops over cell content, default is to loop over both incremental cell content increases and starting afresh with correct cell content from start
    use_shortcuts = True
      .type = bool
      .expert_level = 1
      .short_caption = Shortcut search in loops by shortcutting solution pathways that have been explored previously
    use_jog = False
      .type = bool
      .expert_level = 1
      .short_caption = Do jogging refinement, which may be slow, when translational non-crystallographic symmetry is present
    use_fuse = True
      .type = bool
      .expert_level = 1
      .short_caption = Do fuse, which will be faster, but may miss solutions
    use_xref = True
      .type = bool
      .expert_level = 1
      .short_caption = Do coordinate refinement, which may be slow, if the initial R-factor is less than a R-factor of 0.50
    use_xref_only_if_complete = False
      .type = bool
      .expert_level = 1
      .short_caption = Do coordinate refinement, which may be slow, only if the solution is complete for the current estimate of the number of copies
    use_xref_only_if_low_rfactor = False
      .type = bool
      .expert_level = 1
      .short_caption = If the initial R-factor calculation from the solution is higher than 0.50, then full coordinate refinement is not performed
    xref_software = *phenix refmac
      .type = choice(multi=True)
      .expert_level = 0
      .short_caption = Programs to use for refinement
    vrms_estimates = 0.6 1.2
      .type = floats(value_min=0)
      .expert_level = 1
      .short_caption = New vrms estimate for search ensemble limited by vrms range, which overrides the rmsd of the default for the fast rotation function
    subgroup_alt = *original_only original_and_p1 p1_only all_subgroups subgroup_list
      .type = choice
      .expert_level = 0
      .short_caption = Loop over space group subgroups
    subgroup_list = None
      .type = strings
      .expert_level = 0
      .short_caption = Space group symbol
  }
''' ,

"riker" : '''
  control
    .expert_level = 1
  {
    use_xref = True
      .type = bool
      .expert_level = 1
      .short_caption = Do coordinate refinement, which may be slow, if the initial R-factor is less than a R-factor of 0.50
    xref_software = *phenix refmac
      .type = choice(multi=True)
      .expert_level = 0
      .short_caption = Programs to use for refinement
  }
''' ,

#expert_level 4 not interpreted by program template when maximum_expert_level is 3
#note that mtzout keyword WHEN DEFINED AS PATH is interpreted by the data_manager as an INPUT mtz file!!
#must be .type = str

"self_rotation_function" : '''
  self_rotation_function = False
    .type = bool
    .expert_level = 0
    .short_caption = Peform self rotation function analysis
''' ,

"probe" : '''
  probe
    .expert_level = 0
  {
    source = *contaminants pdbid directory
      .type = choice
      .expert_level = 0
      .short_caption = Choice of probe structures, list of contaminants, pdbids input, or directory of pdb files as for file_discovery
      .help = Choice of probe structures, list of contaminants, pdbids input, or directory of pdb files as for file_discovery. Default list of contaminants in file contaminants/data.py generated from contaminer website.
    pdbid = None
      .type = strings
      .expert_level = 0
      .short_caption = List of pdbid for pdb fetch and/or pdb filenames, mixed list allowed
  }
''' ,

"changeling" : '''
  control
    .expert_level = 0
  {
    termination_rfz = 10
      .type = float(value_min=0)
      .expert_level = 0
      .short_caption = Rotation function z-score for acceptance of solution
    terminate_on_rfz = False
      .type = bool
      .expert_level = 0
      .short_caption = Terminate search on reaching high rotation function z-score
    resolutions = 8.0  2.0
      .type = floats(value_min=0)
      .expert_level = 0
      .short_caption = Resolutions for loops in changeling
    significant_rfz = 4.5
      .type = float(value_min=0)
      .expert_level = 0
      .short_caption = Significant rfz for pooling results
  }
''' ,

#identical scopes in phil are not duplicated, because of the merge functionality
#hence to get mulitsearches of the same component we have to add the search keyword to make the scopes different
#adding keyword search also means that the search order is explicitly specified which is a good thing

"model" : '''
  model
    .multiple = True
    .expert_level = 0
  {
    filename = None
      .type = str
      .expert_level = 0
      .short_caption = Filename for model to add to the solution where mutiple additions of the same model are possible but adding the same model with different search ranks
    tag = None
      .type = str
      .expert_level = 0
      .short_caption =  Tag can be used in place of filename to define model provided model with tag is already in database
    search_rank = None
      .type = int(value_min=1)
      .expert_level = 0
      .short_caption = Defines the order in which a multi-model search will be performed where the ranking numbers do not need to be consecutive and models defined with lower numbers are searched before higher numbers
  }
''' ,

"method" : '''
  method = *phassade hyss external
    .type = choice
    .expert_level = 1
    .short_caption = Choice of method for substructure determination
''' ,

"analyse_anomalous" : '''
  analyse_anomalous = False
    .type = bool
    .expert_level = 0
    .short_caption = Analyse the anomalous signal if anomalous data are present
''' ,

#note that these keywords need to be the same as those in isostructure becuase they are copied for scripting
"match" : '''
  match
    .expert_level = 0
  {
    ignore_best_match = False
      .type = bool
      .expert_level = 1
      .short_caption = Exclude best match from pdb, for testing or tutorials
    ignore_pdbid_list = None
      .type = strings
      .expert_level = 0
      .short_caption = Ignore match with given list of pdbids
    force_match_with_pdbid = None
      .type = str
      .expert_level = 0
      .short_caption = Force match with given pdbid or coordinate file
  }
''' ,

"hklin" : '''
  hklin = None
    .type = path
    .expert_level = 0
    .short_caption = Reflection file in mtz format
''' ,

"hklout" : '''
  hklout = "nacelle.mtz"
    .type = path
    .expert_level = 0
    .short_caption = Reflection file in mtz format
''' ,

"seqin" : '''
  seqin = None
    .type = path
    .expert_level = 0
    .short_caption = Sequence file in fasta format
''' ,

"dagin" : '''
  dagin = None
    .type = path
    .expert_level = 0
    .short_caption = Dag file input in phasertng format
''' ,

"xyzin" : '''
  xyzin = None
    .type = path
    .expert_level = 0
    .short_caption = Coordinates file in pdb format
''' ,

#here we define xyzins as a multiple version of xyzin
#cannot have xyzin and xyzins at the same time
"xyzins" : '''
  xyzin = None
    .type = path
    .multiple = True
    .expert_level = 0
    .short_caption = Coordinates files in pdb format
''' ,

#here we define another xyzin, when xyzin and xyzins are both needed
"fixed" : '''
  fixed = None
    .type = path
    .expert_level = 0
    .short_caption = Coordinates files in pdb format
''' ,

"mapin" : '''
  mapin = None
    .type = path
    .expert_level = 0
    .short_caption = Map file in map format
''' ,

} # end of implemented dictionary

keyword_subsets = {
  "xtricorder" : [
                 "hklin",
                 "seqin",
                 "self_rotation_function",
                 "analyse_anomalous",
                 ],
  "picard":      [
                 "dryrun",
                 "hklin",
                 "seqin",
                 "xyzins",
                 "picard", #control
                 ],
  "bones":       [ #must be the same as picard
                 "dryrun",
                 "hklin",
                 "seqin",
                 "xyzins",
                 "picard", #control
                 ],
  "riker":       [
                 "hklin",
                 "fixed",
                 "xyzins",
                 "seqin",
                 "riker", #control
                 ],
  "scotty":      [
                 "match",
                 "riker", #control
                 "hklin",
                 ],
  "nomad":       [
                 "seqin",
                 "xyzins",
                 ],
  "changeling":  [
                 "self_rotation_function",
                 "changeling", #control
                 "probe",
                 "hklin",
                 ],
  "nanoprobe":   [
                 "hklin",
                 "seqin",
                 ],
  "spock":     [
                 "hklin",
                 "seqin",
                 "xyzin",
                 "mapin",
                 ],
  "nanite":     [
                 "hklin",
                 "xyzin",
                 "seqin",
                 "method",
                 ],
  "nacelle":     [
                 "hklin",
                 "hklout",
                 ],
  "caldos":      [
                 "hklin",
                 "xyzin",
                 ],
  "exocomp":     [ #uses graph_report keyword
                 ],
} # end of keyword_subsets_by_class_name dictionary

def build_auto_str(class_name=None):
  if class_name is not None and not class_name in keyword_subsets.keys():
    raise Exception('No dictionary entry for program name in build_auto_str, contact developer')
  k = keyword_subsets[class_name]
  master_phil = class_name + " {\n"
  for keyword in k:
    master_phil += auto_implemented[keyword]
  master_phil += "}"
  return master_phil

if __name__ == "__main__":
  for k in keyword_subsets.keys():
    print(build_auto_str(k))
    print("------\n")
