//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Sequence_class____
#define __phasertng_Sequence_class____
#include <phasertng/main/includes.h>
#include <phasertng/enum/composition_type.h>
#include <phasertng/data/Cluster.h>
#include <phasertng/io/FileSystem.h>
#include <boost/logic/tribool.hpp>
#include <unordered_map>
#include <phasertng/pod/composition.h>

namespace phasertng {

class PdbRecord;

class Sequence : public FileSystem
{
  private: //members
    sv_string seq;
    sv_bool uniquely_protein;
    sv_bool uniquely_nucleic;
    sv_bool uniquely_alanine;
    std::vector<boost::logic::tribool> user_defined_nucleic;
    composition::type CompType = composition::UNDEFINED;
    composition_t COMPOSITION;

  private: //constructor variables
    bool selenomethionine = false;
    bool disulphide = false;
    bool deuterium = false;
    int  count_ssbond = 0;
    Cluster xx; //for user cluster definitions
    double water_inflation = 1.00; //leave some slack to avoid negative variances with exact seq=model

  public: //constructors
    Sequence(
        bool selenomethionine_,
        bool disulphide_=false,
        bool deuterium_=false,
        Cluster xx_=Cluster(),
        double water_inflation_=1.0) :
        selenomethionine(selenomethionine_),disulphide(disulphide_),deuterium(deuterium_),xx(xx_),
        water_inflation(water_inflation_),
        FileSystem()
        { }

  public:
    void ParseModel(const std::vector<PdbRecord>&,const int,bool);
    void ReadSeq(int=1);
    void SetSeq(af_string);
    void AddProtein(std::string);
    void AddNucleic(std::string);
    void ReadPdb();
    void SetPdb(af_string);
    void AddAtom(std::string,int);
    void initialize(bool,bool); //add_terminal_atoms

    double total_molecular_weight() const;
    double minimum_radius_from_molecular_weight() const;
    int total_scattering() const;
    composition_t composition() const { return COMPOSITION; }
    std::string sequence(int i) const { return seq.at(i); }
    const int  size() const;
    composition::type composition_type() const { return CompType; }
    const bool has_protein() const;
    const bool has_nucleic() const;
    const bool all_protein() const;
    const bool all_nucleic() const;

  //psv Rupp 2003 + RNA doi:10.1016/j.jmb.2004.11.072,
  //note latter has different number for protein psv than Rupp
  double volume() const;

  af_string logComposition(std::string);
  af_string logSequence(std::string);
};

}
#endif
