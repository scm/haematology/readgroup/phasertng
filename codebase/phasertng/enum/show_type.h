#ifndef __phasertng_show_type_enum__
#define __phasertng_show_type_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace show {
  enum type {
             VALUEORDEFAULT,
             VALUE,
             DEFAULT
           };
/*
  MakeEnum ( type,
             (VALUEORDEFAULT) //phasertng namespace, use unique name
             (VALUE)
             (DEFAULT)
           );
*/
}} //phasertng

#endif
