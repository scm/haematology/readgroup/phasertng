//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_EELLG_analytical_class__
#define __phasertng_EELLG_analytical_class__

namespace phasertng {
namespace likelihood {

class EELLG_analytical
{
  public:
    EELLG_analytical() {}

  public:
    double get(double fs, double vrms, double smax,
               double fsol = 0, double bsol = 0, int nref = 1);
};

}}
#endif
