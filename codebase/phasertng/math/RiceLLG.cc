#include <scitbx/constants.h>
#include <scitbx/math/erf.h>
#include <limits>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
}

using namespace scitbx;

// Functions needed to compute Rice approximation to the LLG including
// measurement error on intensities

namespace phasertng {

double effSigaRootAcen(double ee, double eesq, double sigaeff)
{
/* Acentric: Equation for which a root is the solution for the value of an
   effective sigmaA giving a Rice function that approximates the effect of
   measurement error in the observed intensities on an effective E value.
   The sigmaA (along with the associated effective E value) is determined
   by matching the first two moments of the Rice function (<E> and <E^2>)
   to the first two moments of p(E;Iobs,sigIobs), which are equivalent to
   the French-Wilson posterior expected values of E and E^2.
   There are one or two roots; if two (which occurs for allowed values of
   sigaeff when eesq<1), the lower one corresponds to an imaginary solution
   for the associated effective E value.
*/

  assert(sigaeff > 0. && sigaeff < 1.);
  double sigbsqr(1.-fn::pow2(sigaeff));
  double x(0.5*(eesq-sigbsqr)/sigbsqr);

  double root = std::sqrt(scitbx::constants::pi*sigbsqr)/(2.*sigbsqr) *
                   (eesq*tbl_ebesseli0.get(x) + (eesq - sigbsqr)*tbl_ebesseli1.get(x)) - ee;

  return root;
}

double DobsSigaRootAcen_by_dsa(double eesq, double sigaeff)
{
/* Acentric: Derivative of effSigaRootAcen with respect to sigaeff
   The derivative is zero (corresponding to the minimum between two roots) if
   the argument of the Bessel function I1 is zero.  This can only happen for
   allowed values of sigaeff (0-1) if eesq<1.
*/

  assert(sigaeff > 0. && sigaeff < 1.);
  double sigbsqr(1.-fn::pow2(sigaeff));
  double x(0.5*(eesq-sigbsqr)/sigbsqr);

  double droot = std::sqrt(scitbx::constants::pi/sigbsqr) * (sigaeff/2.) *
                    tbl_ebesseli1.get(x);

  return droot;
}

double d2effSigaRootAcen_by_dsa2(double eesq, double sigaeff)
{
// Acentric: Second derivative of effSigaRootAcen with respect to sigaeff

  assert(sigaeff > 0. && sigaeff < 1. && eesq > 0.);
  double sigasqr(fn::pow2(sigaeff));
  double sigapow4(fn::pow2(sigasqr));
  double sigbsqr(1.-sigasqr);
  double xnum(eesq-sigbsqr);
  double x(0.5*xnum/sigbsqr);
  assert(x >= 0.); // x>0 is condition for flt root

  double d2root;
  if (xnum > 1.e-10) // Avoid potential divide-by-zero
    d2root = std::sqrt(scitbx::constants::pi/sigbsqr) /
             (2.*fn::pow2(sigbsqr)) * (eesq*sigasqr*tbl_ebesseli0.get(x) +
             (eesq - 1. - (-2. + eesq*(2.+eesq))*sigasqr +
             (eesq - 1.)*sigapow4) * tbl_ebesseli1.get(x) / xnum);
  else // Linear approximation near xnum=0
  {
    double samin(std::sqrt(1.-eesq));
    d2root = std::sqrt(scitbx::constants::pi)*samin *
             ((3.+fn::pow2(samin))*sigaeff - 2.*(samin + std::pow(samin,3))) /
             (4.*std::pow(eesq,2.5));
  }

  return d2root;
}

double effSigaRootCen(double ee, double eesq, double sigaeff)
{
/* Centric: Equation for which a root is the solution for the value of an
   effective sigmaA giving a Rice function that approximates the effect of
   measurement error in the observed intensities on an effective E value.
   The sigmaA (along with the associated effective E value) is determined
   by matching the first two moments of the Rice function (<E> and <E^2>)
   to the first two moments of p(E;Iobs,sigIobs), which are equivalent to
   the French-Wilson posterior expected values of E and E^2.
   There are one or two roots; if two (which occurs for allowed values of
   sigaeff when eesq<1), the lower one corresponds to an imaginary solution
   for the associated effective E value.
*/

  assert(sigaeff > 0. && sigaeff < 1.);
  double sigbsqr(1.-fn::pow2(sigaeff));
  double x(0.5*(eesq-sigbsqr)/sigbsqr);

  double root = std::exp(-x)*std::sqrt(2.*sigbsqr/scitbx::constants::pi) +
                   std::sqrt(eesq-sigbsqr)*scitbx::math::erf(std::sqrt(x)) - ee;

  return root;
}

double DobsSigaRootCen_by_dsa(double eesq, double sigaeff)
{
/* Centric: Derivative of effSigaRootCen with respect to sigaeff
   The derivative is zero (corresponding to the minimum between two roots) if
   the argument of the Bessel function I1 is zero.  This can only happen for
   allowed values of sigaeff (0-1) if eesq<1.
*/

  assert(sigaeff > 0. && sigaeff < 1.);
  double sigbsqr(1.-fn::pow2(sigaeff));
  double xnum(eesq-sigbsqr);
  double x(0.5*xnum/sigbsqr);

  double droot;
  if (std::abs(xnum) > 1.e-10) // Avoid potential divide-by-zero
    droot = sigaeff*scitbx::math::erf(std::sqrt(x)) / std::sqrt(xnum) -
            std::exp(-x)*std::sqrt(2.*sigbsqr/scitbx::constants::pi) *
            sigaeff / sigbsqr;
  else // Linear approximation near zero
    droot = xnum * std::sqrt(2./scitbx::constants::pi) * sigaeff /
            (3.*std::pow(sigbsqr,1.5));

  return droot;
}

double d2effSigaRootCen_by_dsa2(double eesq, double sigaeff)
{
// Centric: Second derivative of effSigaRootCen with respect to sigaeff

  assert(sigaeff > 0. && sigaeff < 1.);
  double sigasqr(fn::pow2(sigaeff));
  double sigapow4(fn::pow2(sigasqr));
  double sigbsqr(1.-sigasqr);
  double xnum(eesq-sigbsqr);
  double x(0.5*xnum/sigbsqr);
  double sigbsqrtpi(std::sqrt(scitbx::constants::pi*sigbsqr));

  double d2root,d2rnum;
  if (std::abs(xnum) > 1.e-10) // Avoid potential divide-by-zero
  {
    d2rnum = (eesq - 1.)*fn::pow2(sigbsqr)*sigbsqrtpi *
             scitbx::math::erf(std::sqrt(x));
    if (x < 20.) // Avoid potential underflow with insignificant exp(x).
      d2rnum += std::sqrt(2.*xnum)*std::exp(-x) *
                (1.-eesq + sigasqr*(eesq + fn::pow2(eesq) - 2.) + sigapow4);
    d2root = d2rnum / (fn::pow2(sigbsqr)*std::pow(xnum,1.5)*sigbsqrtpi);
  }
  else // Constant approximation near zero
    d2root = std::sqrt(2./scitbx::constants::pi) * sigaeff /
             (1.5*std::pow(sigbsqr,1.5));

  return d2root;
}

double effSigaRoot(double ee, double eesq, double sigaeff, bool centric)
{
  double root;
  if (centric)
    root = effSigaRootCen(ee,eesq,sigaeff);
  else
    root = effSigaRootAcen(ee,eesq,sigaeff);
  return root;
}

double DobsSigaRoot_by_dsa(double eesq, double sigaeff, bool centric)
{
  double droot;
  if (centric)
    droot = DobsSigaRootCen_by_dsa(eesq,sigaeff);
  else
    droot = DobsSigaRootAcen_by_dsa(eesq,sigaeff);
  return droot;
}

double d2effSigaRoot_by_dsa2(double eesq, double sigaeff, bool centric)
{
  double d2root;
  if (centric)
    d2root = d2effSigaRootCen_by_dsa2(eesq,sigaeff);
  else
    d2root = d2effSigaRootAcen_by_dsa2(eesq,sigaeff);
  return d2root;
}

double getDfactor(double ee, double eesq, bool centric)
{
/* Use Halley's method to perform a line search to find the root of the equation
   that finds a Dobs value to matches the first two moments of the exact probability
   of E given Iobs/SIGIobs with the first two moments of the Rice function.
   This Dobs is then a factor that can be applied to Luzzati D-values (or
   sigmaA values) arising from coordinate error, thus accounting very well for
   the effect of observation error in the likelihood targets.
   If there are two roots, we want the top one (the bottom one corresponds to
   an imaginary effective Eobs), so constrain the search between the point
   where the slope is zero and a D-factor of 1.
*/
  const double EPS1(1.e-7),EPS2(1.e-10);
  static double MAXDOBS(1.-EPS1); // No point refining to something higher

  assert(eesq-fn::pow2(ee) > -0.01); // Not FW expected values?
  if (eesq - fn::pow2(ee) <= 0.) return 1.; // No observational error

  // Point where slope is zero (if any in range 0-1) will be below desired root
  // Avoid going as low as zero, because slope is zero here
  // Numerical stability: start slightly to the right to avoid taking the square
  // root of the difference between two nearly identical numbers in effSigaRootCen
  double dflo(std::max(std::sqrt(1.-std::min(eesq,1.))+EPS1,EPS1));
  // If this is higher than our effective maximum, return this value
  if (dflo >= MAXDOBS) return MAXDOBS;

  //double flo = effSigaRoot(ee,eesq,dflo,centric);
  double dfhi(MAXDOBS);
 // double fhi = effSigaRoot(ee,eesq,dfhi,centric);
  double dfmid = (dflo+dfhi)/2.;
  double fmid = effSigaRoot(ee,eesq,dfmid,centric);

  double slope,curve,dfnew;
  int nloop(1);
  while ((nloop<=50) && (dfhi-dflo > EPS1) && (std::abs(fmid) > EPS2))
  {
    slope = DobsSigaRoot_by_dsa(eesq,dfmid,centric);
    curve = d2effSigaRoot_by_dsa2(eesq,dfmid,centric);
    dfnew = dfmid - ( (curve > 0.) ?
            2.*fmid*slope/(2.*(fn::pow2(slope)-fmid*curve)) :
            fmid*slope ); // Halley's method, falling back on Newton-Raphson
    dfmid = ((dfnew > dflo) && (dfnew < dfhi)) ?
            dfnew : // new value is between old limits
            (dflo + dfhi)/2.; // shift failed, so bisect limits
    fmid = effSigaRoot(ee,eesq,dfmid,centric);

    if (fmid < 0.)
    {
   //   flo = fmid; // Desired root is higher than midpoint
      dflo = dfmid;
    }
    else
    {
    //  fhi = fmid; // Desired root is lower than midpoint
      dfhi = dfmid;
    }
    nloop++;
  }
  return dfmid;
}

double getEffectiveEobs(double eesq, double Dobstor)
{
  double Dobssqr(fn::pow2(Dobstor));
  double eEobs((eesq+(Dobssqr-1.))/Dobssqr);
  assert(eEobs >= -std::numeric_limits<double>::epsilon()); // Ensure we got the correct root of the equation
  if (eEobs > 0.)
    eEobs = std::sqrt(eEobs);
  else
    eEobs = 0.;
  return eEobs;
}

}
