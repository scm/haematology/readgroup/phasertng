#ifndef __phasertng_RiceWoolfson_function__
#define __phasertng_RiceWoolfson_function__

namespace phasertng {

double pRiceF(double F1, double DF2, double V);

double pRiceI(double I1, double D2I2, double V);

double pWoolfsonF(double F1, double DF2, double V);

double pWoolfsonI(double I1, double D2I2, double V);

double logRelWoolfson(double F1, double DF2, double V);

double logRelRice(double F1, double DF2, double V);

} //phasertng
#endif
