//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/strings.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  strings::strings(const std::string parameter,
          const sv_string dephalt_
         ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  { }

  void
  strings::set_value(sv_string others)
  {
    user = true;
    if (!others.size()) return;
//stored unquoted - filename cannot start with quote
    uvalue.clear();
    for (auto other : others)
    {
      if (other[0] != '"')
      {
        uvalue.push_back(other);
      }
      else
      {
        std::string unquoted = other;
        while (unquoted.size() && unquoted[0] == '"')
        {
          std::string tmp = "";
          for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
            tmp += std::string(1,unquoted[i]);
          unquoted = tmp;
        }
        //phaser_assert(!unquoted.size() || unquoted[0] != '"');
        uvalue.push_back(unquoted);
      }
    }
  }

  void
  strings::push_back(const std::string& other)
  {
    user = true;
    if (!other.size()) return;
//stored unquoted - filename cannot start with quote
    if (other[0] != '"')
    {
      uvalue.push_back(other);
    }
    else
    {
      std::string unquoted = other;
      while (unquoted.size() && unquoted[0] == '"')
      {
        std::string tmp = "";
        for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
          tmp += std::string(1,unquoted[i]);
        unquoted = tmp;
      }
     // phaser_assert(!unquoted.size() || unquoted[0] != '"');
      uvalue.push_back(unquoted);
    }
  }

  void
  strings::set_value(const sv_string& vcards,const sv_string& others)
  {
    paranoia_check(vcards);
    set_value(others);
  }

  //virtual
  bool
  strings::is_default() const
  { return uvalue == dephalt; }

  void
  strings::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  strings::to_python()
  { return unparse_parameter(false); }

  void
  strings::from_python(std::string str)
  {
    sv_string txt = to_words(str);
    set_value(txt);
  }

  sv_string
  strings::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  strings::value_or_default(size_t i) const
  { return i < uvalue.size() ? uvalue[i] : ""; }

  sv_string
  strings::value_or_default_unique() const
  {
    sv_string tmp = value_or_default();
    std::set<std::string> tmp_set;
    for (auto item : tmp)
      tmp_set.insert(item);
    tmp.clear();
    for (auto item : tmp_set)
      tmp.push_back(item);
    return tmp; //sorted and unique
  }

  std::set<std::string>
  strings::value_or_default_set() const
  {
    sv_string tmp = value_or_default();
    std::set<std::string> tmp_set;
    for (auto item : tmp)
      tmp_set.insert(item);
    return tmp_set; //sorted and unique
  }

  //if any of the strings contains a space, unparse it with quotes around all
  //AJM?? TODO??
  std::string
  strings::unparse_parameter(bool phil) const
  {
    std::stringstream ss_space_test;
    std::stringstream ss_unquoted;
    for (int i = 0; i < value_or_default().size(); i++)
    {
      ss_space_test << value_or_default()[i];
      ss_unquoted << value_or_default()[i] << " ";
    }
    if (!ss_space_test.str().size())
      return None();
    return ss_unquoted.str();
  }

  //if any of the strings contains a space, unparse it with quotes around all
  std::string
  strings::show_default(bool phil) const
  {
    std::stringstream ss_space_test;
    std::stringstream ss_unquoted;
    for (int i = 0; i < dephalt.size(); i++)
    {
      ss_space_test << value_or_default()[i];
      ss_unquoted << value_or_default()[i] << " ";
    }
    if (!ss_space_test.str().size())
      return None();
    return ss_unquoted.str();
  }

}}
