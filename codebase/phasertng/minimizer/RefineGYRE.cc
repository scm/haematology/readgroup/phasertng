//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/minimizer/RefineGYRE.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/main/Error.h>
#include <phasertng/math/xyzRotMatDeg.h>
#include <phasertng/math/rotation/rotdist.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <phasertng/math/Matrix.h>
#include <phasertng/math/Tensor.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <unordered_map>
#include <set>
#include <future>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

  RefineGYRE::RefineGYRE(
      const_ReflectionsPtr REFLECTIONS_,
      sv_bool  SELECTED_,
      const_EpsilonPtr  EPSILON_)
    : RefineBase(),
      REFLECTIONS(REFLECTIONS_),
      SELECTED(SELECTED_),
      EPSILON(EPSILON_)
  {
  }

  void
  RefineGYRE::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineGYRE::init_node(dag::Node* work,pod::FastPoseType& ecalcs_sigmaa_pose_)
  {
    negvar = false;
    NODE = work;
    ecalcs_sigmaa_pose = ecalcs_sigmaa_pose_;
    PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
    PHASER_ASSERT(ecalcs_sigmaa_pose.size() == REFLECTIONS->NREFL);
    if (NODE->HALL.size() and NODE->HALL != REFLECTIONS->SG.HALL)
    throw Error(err::INPUT,"Node space group is not that of data");
    std::map<Identifier,dag::Entry*> entries;
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      auto modlid = NODE->NEXT.GYRE[g].IDENTIFIER;
      if (!entries.count(modlid))
      {
        USED_MODLID.insert(g); //will be accessed as p
        entries[modlid] = NODE->NEXT.GYRE[g].ENTRY;
      }
    }
    LARGE_ROTN.resize(NODE->NEXT.ngyre());
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      const auto& entry = NODE->NEXT.GYRE[g].ENTRY;
      dvect3 maxperp =
          { std::max(entry->INTERPOLATION.UC.B(),entry->INTERPOLATION.UC.C()),
            std::max(entry->INTERPOLATION.UC.C(),entry->INTERPOLATION.UC.A()),
            std::max(entry->INTERPOLATION.UC.A(),entry->INTERPOLATION.UC.B()) } ;
      for (int rot = 0; rot < 3; rot++)
      {
        LARGE_ROTN[g][rot] = 4*scitbx::rad_as_deg(REFLECTIONS->data_hires()/maxperp[rot]);
      }
    }
    for (const auto& p : USED_MODLID)
    {
      auto entry = NODE->NEXT.GYRE[p].ENTRY;
      auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
      auto& item = NODE->DRMS_SHIFT[modlid];
      item.VAL = std::max(entry->DRMS_MIN,item.VAL);
      item.VAL = std::min(entry->DRMS_MAX,item.VAL);
    }
    //SYMHKL = REFLECTIONS->get_symhkl();
  }

  void
  RefineGYRE::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::unordered_set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(item);
    REFINE_ROTN = (protocol.find("rotn") != protocol.end());
    REFINE_TRAN = (protocol.find("tran") != protocol.end());
    REFINE_VRMS = (protocol.find("vrms") != protocol.end());
    REFINE_VRMS = REFINE_VRMS and NODE->turn_is_novel();
    if (protocol.find("off") != protocol.end())
       REFINE_ROTN = REFINE_TRAN = REFINE_VRMS = false;

    double maxscat(0);
    int maxscat_g(0);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      double scat = REFLECTIONS->fracscat(NODE->NEXT.GYRE[g].ENTRY->SCATTERING);
      if (scat > maxscat) { maxscat_g = g; maxscat = scat;}
    }

    bool REFINE_ON(true),REFINE_OFF(false);
    refine_parameter_.clear();
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      for (int rot = 0; rot < 3; rot++)
        REFINE_ROTN ? refine_parameter_.push_back(REFINE_ON) : refine_parameter_.push_back(REFINE_OFF);
      for (int tra = 0; tra < 3; tra++)
        (REFINE_TRAN and g != maxscat_g) ? //isshift not relevant for relative shifts
         refine_parameter_.push_back(REFINE_ON) : refine_parameter_.push_back(REFINE_OFF);
    }
    for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
    {
      REFINE_VRMS ? refine_parameter_.push_back(REFINE_ON) : refine_parameter_.push_back(REFINE_OFF);
    }

    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();

    //make sure parameters are within bounds
    auto x = get_macrocycle_parameters();
    auto bound = bounds();
    if (x.size() == bound.size())
    {
      for (int i = 0; i < x.size(); i++)
      {
        if (bound[i].upper_bounded() and (x[i] > bound[i].upper_limit() ))
        {
          x[i] = bound[i].upper_limit();
        }
        if (bound[i].lower_bounded() and (x[i] < bound[i].lower_limit() ))
        {
          x[i] = bound[i].lower_limit();
        }
      }
    }
    set_macrocycle_parameters(x);
  }

  sv_string
  RefineGYRE::macrocycle_parameter_names()
  {
    if (!refine_parameter_.size()) return sv_string();
    sv_string names(nmp);
    int i(0),m(0);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation X Gyre " + std::to_string(g+1);
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation Y Gyre " + std::to_string(g+1);
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Rotation Z Gyre " + std::to_string(g+1);
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Shift X Gyre " + std::to_string(g+1);
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Shift Y Gyre " + std::to_string(g+1);
      if (refine_parameter_[m++]) names[i++] = "Orthogonal Shift Z Gyre " + std::to_string(g+1);
    }
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
      if (refine_parameter_[m++]) names[i++] = "Drms for " + modlid.str();
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return names;
  }

  sv_double
  RefineGYRE::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      auto& gyre = NODE->NEXT.GYRE[g];
      for (int rot = 0; rot < 3; rot++)
        if (refine_parameter_[m++])
          pars[i++] = gyre.ORTHR[rot];
      for (int tra = 0; tra < 3; tra++)
        if (refine_parameter_[m++])
          pars[i++] = gyre.ORTHT[tra];
    }
    for (const auto& p : USED_MODLID)
    {
      if (refine_parameter_[m++])
      {
        auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
        pars[i++] = NODE->DRMS_SHIFT[modlid].VAL;
      }
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return pars;
  }

  void
  RefineGYRE::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      auto& gyre = NODE->NEXT.GYRE[g];
      for (int rot = 0; rot < 3; rot++)
      {
        if (refine_parameter_[m++]) //ROT
        {
          gyre.ORTHR[rot] = newx[i++];
        }
      }
      for (int tra = 0; tra < 3; tra++)
      {
        if (refine_parameter_[m++]) //TRA
        {
          gyre.ORTHT[tra] = newx[i++];
        }
      }
    }
    for (const auto& p : USED_MODLID)
    {
      if (refine_parameter_[m++]) //VRMS
      {
        auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
        NODE->DRMS_SHIFT.at(modlid).VAL = newx[i++];
      }
    }

    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineGYRE::macrocycle_large_shifts()
  {
    double large_tra = REFLECTIONS->data_hires()/4.0;
    double large_vrms = REFLECTIONS->data_hires()/20.0;
    sv_double largeShifts(nmp);
    int m(0),i(0);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      for (int rot = 0; rot < 3; rot++)
        if (refine_parameter_[m++])
        {
          double large_rot = LARGE_ROTN[g][rot];
          largeShifts[i++] = large_rot;
        }
      for (int tra = 0; tra < 3; tra++)
        if (refine_parameter_[m++])
          largeShifts[i++] = large_tra;
    }
    //if too large, molecule will get washed out and won't come back from limits
    for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
    {
      if (refine_parameter_[m++])
        largeShifts[i++] = large_vrms; //VRMS
    }
    phaser_assert(i == nmp);
    phaser_assert(m == total_number_of_parameters_);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineGYRE::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      for (int rot = 0; rot < 3; rot++)
        if (refine_parameter_[m++])
          bounds[i++].on(-9.99, 9.99); // Any sensible perturbation will be much smaller
          //limit to <10 so that format is 4.2f
      for (int tra = 0; tra < 3; tra++)
        if (refine_parameter_[m++])
          bounds[i++].on(-REFLECTIONS->data_hires()/1.5, REFLECTIONS->data_hires()/1.5); // Any sensible perturbation will be much smaller
    }
    //if too large, molecule will get washed out and won't come back from limits
    for (const auto& p : USED_MODLID)
    {
     // const auto& entry = DAGDB->lookup_entry(modlid);
      auto entry = NODE->NEXT.GYRE[p].ENTRY;
      //auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
      if (refine_parameter_[m++])
        bounds[i++].on(entry->DRMS_MIN, entry->DRMS_MAX); //VRMS
    }
    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineGYRE::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    sv_double pars(nmp);
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      for (int rot = 0; rot < 3; rot++) //ROT
        if (refine_parameter_[m++])
          repar[i++].off();
      for (int tra = 0; tra < 3; tra++) //TRA
        if (refine_parameter_[m++])
          repar[i++].off();
    }
    for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
    {
      if (refine_parameter_[m++]) //VRMS
        repar[i++].off();
    }

    phaser_assert(m == total_number_of_parameters_);
    phaser_assert(i == nmp);
    return repar;
  }

  TargetType
  RefineGYRE::likelihood()
  {
    bool do_gradient(false), do_hessian(false), do_restraint(false);
    TargetGradientHessianType Tuple = combined_tgh(do_gradient,do_hessian,do_restraint);
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don' want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineGYRE::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineGYRE::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*TargetGradientType(Tuple);
  }

  TargetGradientHessianType
  RefineGYRE::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineGYRE::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    //initialization
    do_gradient = (do_gradient or do_hessian);
    TargetGradientHessianType tgh(nmp,true);
    bool do_grad_fc(true); //may want to change for no-fc gradient refinement
    bool do_hess_fc(true); //may want to change for no-fc gradient refinement

    // tNCS case: curvatures (particularly vrms) should take account of correlations between
    // tNCS-related copies.  For now, rely on BFGS algorithm to compensate, but it would be better
    // to deal with this explicitly.
    const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;

    //AJM TODO look at the speed of this and set pointers between map lookups
    //and std::vectors indexed on p
    //and make this part of set protocol or init step so it is not done each time!!!

    //Gradient
    sv_dvect3          dLL_by_dRotn(NODE->NEXT.ngyre(),{0,0,0});
    sv_dvect3          dLL_by_dTran(NODE->NEXT.ngyre(),{0,0,0});
    std::map<Identifier,double>  dLL_by_dDrms;
    //Hessian
    sv_dvect3          d2LL_by_dRotn2(NODE->NEXT.ngyre(),{0,0,0});
    sv_dvect3          d2LL_by_dTran2(NODE->NEXT.ngyre(),{0,0,0});
    std::map<Identifier,double>  d2LL_by_dDrms2;

    //collect the terms that will not change
    for (const auto& p : USED_MODLID)
    {
      auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
      dLL_by_dDrms[modlid] = 0;
      d2LL_by_dDrms2[modlid] = 0;
    }

    auto pose_terms = NODE->pose_terms_interpolate();
    auto gyre_terms = NODE->gyre_terms_interpolate();
    sv_cvect3  gyre_cos_sin(NODE->NEXT.ngyre());
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      auto& gyre = NODE->NEXT.GYRE[g];
      for (int dir = 0; dir < 3; dir++)
        gyre_cos_sin[g][dir] = tbl_cos_sin.get(gyre.ORTHR[dir]/360.);
    }

    int NSYMP = REFLECTIONS->SG.group().order_p();
    cmplex TwoPiI = scitbx::constants::two_pi*cmplex(0,1);
    likelihood::iwilson::tgh_flags flags;
    flags.target(true).gradient(do_gradient).hessian(do_hessian);
    likelihood::iwilson::function IWILSON;
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::FEFFNAT));
    phaser_assert(REFLECTIONS->has_col(labin::DOBSNAT));
    phaser_assert(REFLECTIONS->has_col(labin::SSQR));
    phaser_assert(REFLECTIONS->has_col(labin::EPS));
    PHASER_ASSERT(NODE->NEXT.ngyre() > 0);
   // PHASER_ASSERT(NODE->POSE.size() == 0);

    //NSYMP is the largest rsym.unique will get, when there are duplicates it will be fewer
    std::vector<int> dimensions = {static_cast<int>(NODE->NEXT.ngyre()),
                                   static_cast<int>(NSYMP),
                                   static_cast<int>(3)};
    math::Tensor<3,cmplex> dEC_g_isym_by_dRotn(dimensions,{0.,0.});
    math::Tensor<3,cmplex> d2EC_g_isym_by_dRotn2(dimensions,{0.,0.});
    math::Tensor<3,cmplex> dEC_g_isym_by_dTran(dimensions,{0.,0.});
    math::Tensor<3,cmplex> d2EC_g_isym_by_dTran2(dimensions,{0.,0.});
    math::Matrix<cmplex> EC_g_isym(NODE->NEXT.ngyre(),NSYMP,{0.,0.});

    int nmol = NODE->TNCS_ORDER;
    bool tncs_modelled = NODE->TNCS_MODELLED;
    bool tncs_not_modelled = !NODE->TNCS_MODELLED;
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    auto& array_Gsqr_Vterm = EPSILON->array_Gsqr_Vterm;
    auto& matrix_G = EPSILON->matrix_G;

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    xyzRotMatDeg rotmat;
    for (int r = beg; r < end; r++)
    {
      if (SELECTED[r])
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const millnx& miller = work_row->get_miller();
        const double& teps = work_row->get_teps();
        const double& resn = work_row->get_resn();
        const double& dobs = work_row->get_dobsnat();
        const double& feff = work_row->get_feffnat();
        const double& ssqr = work_row->get_ssqr();
        const double& eps =  work_row->get_eps();
        const bool&   cent = work_row->get_cent();
        const double  g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1; //halfR=true
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);

        //result of summation
        //first pass calculate EC and V
        //for likelihood target, need summed EC and V value
        //no fixed components allowed

        //This loop should be separate if the InterpV is separate from the InterpE
        //since most of the time is in the interpolation
        //if not, then this loop can be combined below
        sv_double DobsSigaSqr_gyre(NODE->NEXT.ngyre());
        double DobsSigaSqr(0);
        for (int g = 0; g < NODE->NEXT.ngyre(); g++) //fixed phase relative to each other
        {
         // double scale_ssqr = ssqr*(CELL[modlid]/NODE->GRID);
          auto& gyre = NODE->NEXT.GYRE[g];
          const auto& entry = gyre.ENTRY;
          const auto& modlid = entry->identify();
          double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
          double bfac(0);
          double fs = REFLECTIONS->fracscat(entry->SCATTERING);
          double drms = NODE->DRMS_SHIFT[modlid].VAL;
          double thisDobsSigaSqr = likelihood::iwilson::DobsSigaSqr(
            ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
          DobsSigaSqr_gyre[g] = thisDobsSigaSqr;
          if (nmol > 1)
          { //halfR = true
            double Vterm = (nmol == 2 and tncs_not_modelled) ? array_Gsqr_Vterm[r] : array_G_Vterm[r];
            if (tncs_modelled) Vterm /= nmol;
            thisDobsSigaSqr *= Vterm;
          }
          DobsSigaSqr += thisDobsSigaSqr; //one per pose, sum the squares
        }
        DobsSigaSqr += fn::pow2(ecalcs_sigmaa_pose.sigmaa[r]);

        af_cmplex ECsym(rsym.unique,{0.,0.});
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
        {
          for (int g = 0; g < NODE->NEXT.ngyre(); g++) //fixed phase relative to each other
          {
            auto& gyre = NODE->NEXT.GYRE[g];
            const auto& entry = gyre.ENTRY;
            const millnx& SymHKL = rsym.rotMiller[isym];
            dvect3 RotSymHKL = gyre_terms[g].Q1tr*SymHKL;
            cmplex iTarget; cvect3 iGradient; cmat33 iHessian;
            std::tie(iTarget,iGradient,iHessian) =
                entry->INTERPOLATION.callTGH(RotSymHKL,do_grad_fc,do_hess_fc);
            cmplex scalefac = likelihood::iwilson::EcalcTerm(
                DobsSigaSqr_gyre[g],eps,NSYMP,
                SymHKL,gyre_terms[g].TRA,rsym.traMiller[isym]);
            cmplex thisE = scalefac*iTarget;
            cmplex ptncs_scat = cmplxONE; //imol=0
            if (nmol > 1 and tncs_not_modelled)
            { //halfR = true
              const double theta = SymHKL*REFLECTIONS->TNCS_VECTOR;
              for (double imol = 1; imol < nmol; imol++)
                ptncs_scat += std::exp(TWOPII*imol*theta);
              ptncs_scat *= (nmol == 2) ? matrix_G(r,isym) : 1;
            }
            thisE *= ptncs_scat;
            ECsym[isym] += thisE;
            EC_g_isym(g,isym) = thisE;
            //no G function correction (pfun), models in separate (real) rotations
            if (do_gradient or do_hessian)
            {
              cvect3 dthisE = ptncs_scat * scalefac * iGradient;
              cmat33 hthisE = ptncs_scat * scalefac * iHessian;
              if (REFINE_ROTN)
              {
                dmat33 orthtr = entry->INTERPOLATION.UC.orthogonalization_matrix().transpose();
                for (int rot = 0; rot < 3; rot++)
                {
                  dmat33 dQ1tr_by_dRotn = gyre_terms[g].Rtr_Dtr; //CELL in here somewhere?? AJM fix
                  for (int dir = 2; dir >= 0; dir--)
                  {
                    bool do_grad(dir==rot);
                    dQ1tr_by_dRotn = rotmat.grad_tr(dir,gyre_cos_sin[g][dir],do_grad)*dQ1tr_by_dRotn;
                  }
                  dQ1tr_by_dRotn = orthtr * dQ1tr_by_dRotn;
                  dvect3  dQ1tr_h_by_dRotn_p = dQ1tr_by_dRotn*SymHKL;
                  dEC_g_isym_by_dRotn(g,isym,rot) = dthisE*dQ1tr_h_by_dRotn_p;
                  if (do_hessian)
                  {
                    dmat33 d2Q1tr_by_dRotn2 = gyre_terms[g].Rtr_Dtr;
                    for (int dir = 2; dir >= 0; dir--)
                    {
                      bool do_hess(dir==rot);
                      d2Q1tr_by_dRotn2 = rotmat.hess_tr(dir,gyre_cos_sin[g][dir],do_hess)*d2Q1tr_by_dRotn2;
                    }
                    d2Q1tr_by_dRotn2 = orthtr * d2Q1tr_by_dRotn2;
                    dvect3 d2Q1tr_h_by_dRotn2_p = d2Q1tr_by_dRotn2 * SymHKL;
                    d2EC_g_isym_by_dRotn2(g,isym,rot) =
                        dQ1tr_h_by_dRotn_p*(hthisE*dQ1tr_h_by_dRotn_p) +
                        dthisE*d2Q1tr_h_by_dRotn2_p;
                  }
                }
              }

              if (REFINE_TRAN)
              {
                for (int tra = 0; tra < 3; tra++)
                {
                  dvect3 dTRA_by_du(tra==0 ? 1 : 0, tra==1 ? 1 : 0, tra==2 ? 1 : 0);
                  dvect3 dRotSymHKL(SymHKL[0], SymHKL[1], SymHKL[2]);
                  cmplex C = TwoPiI*(dRotSymHKL*(cctbxUC.fractionalization_matrix()*dTRA_by_du));
                  dEC_g_isym_by_dTran(g,isym,tra) = C*thisE;
                  if (do_hessian)
                  {
                    d2EC_g_isym_by_dTran2(g,isym,tra) = fn::pow2(C)*thisE;
                  }
                }
              }

            } //gradient/hessian
          } //gyres
          //gyres have fixed relative phase for each symmetry component
          //ECsym is summed over all gyres, for one symmetry operator
        }
        double sum_Esqr_search(0);
        for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          sum_Esqr_search += std::norm(ECsym[isym]);

        sum_Esqr_search += std::norm(ecalcs_sigmaa_pose.sigmaa[r]*ecalcs_sigmaa_pose.ecalcs[r]);
        //use wilson approximation even when there is a placed component

        //====
        //IWILSON calculation
        //====
        double V = teps - DobsSigaSqr + sum_Esqr_search;
        double eobs = feff/resn;
        IWILSON.target_gradient_hessian(eobs, teps, V, cent, flags);
        tgh.Target -= IWILSON.LL;
        NODE->LLG = -tgh.Target; //always at last value without restraints

        if (V <= 0) negvar = true;
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
        if (V <= 0)
        {
          std::cout << "Debugging negative variance" << std::endl;
          std::cout << "V=" + std::to_string(IWILSON.variance()) << std::endl;
          std::cout << "reflection #" + std::to_string(r) << std::endl;
          std::cout << "teps="+ std::to_string(teps) << std::endl;
          std::cout << "DobsSigaSqr="+ std::to_string(DobsSigaSqr) << std::endl;
          std::cout << "sum_Esqr_search="+ std::to_string(sum_Esqr_search) << std::endl;
          std::cout << "ssqr="+ std::to_string(ssqr) << std::endl;
          std::cout << "reso="+ std::to_string(1/std::sqrt(ssqr)) << std::endl;
          std::cout << "npose="+ std::to_string(NODE->POSE.size()) << std::endl;
          std::cout << "NEXT.ngyre="+ std::to_string(NODE->NEXT.ngyre()) << std::endl;
          std::cout << "eps="+ std::to_string(eps) << std::endl;
          std::cout << "nsymp="+ std::to_string(NSYMP) << std::endl;
          std::string dg = std::to_string(DobsSigaSqr_gyre[0]) + " "+
                           std::to_string(DobsSigaSqr_gyre[1]) + " "+
                           std::to_string(DobsSigaSqr_gyre[2]);
          std::cout << "DobsSigaSqr_gyre (list)=["+ dg +"]" << std::endl;
          for (int g = 0; g < NODE->NEXT.ngyre(); g++)
          {
            auto& gyre = NODE->NEXT.GYRE[g];
            std::string eg = std::to_string(gyre.ORTHR[0]) + " "+
                             std::to_string(gyre.ORTHR[1]) + " "+
                             std::to_string(gyre.ORTHR[2]);
            std::cout << "Euler Angle #" + std::to_string(g+1) + "=" + eg << std::endl;
                        eg = std::to_string(gyre.ORTHT[0]) + " "+
                             std::to_string(gyre.ORTHT[1]) + " "+
                             std::to_string(gyre.ORTHT[2]);
            std::cout << "Euler Shift #" + std::to_string(g+1) + "=" + eg << std::endl;
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              std::cout << "Ecalc #" + std::to_string(g+1) + " isym="+ std::to_string(isym) + " " + std::to_string(std::norm(EC_g_isym(g,isym))) << std::endl;
            }
          }
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
             std::cout << "Ecalc isym #"+ std::to_string(isym) + " " + std::to_string(std::norm(ECsym[isym])) << std::endl;
          }
          for (const auto& drms : NODE->DRMS_SHIFT)
          {
            std::cout << "Drms #" + drms.second.ID.str() + "=" + std::to_string(drms.second.VAL) << std::endl;
          }
          std::cout << "Eobs="+ std::to_string(eobs) << std::endl;
          std::cout << "dobs="+ std::to_string(dobs) << std::endl;
          std::cout << "centric="+ std::string(cent?"Y":"N") << std::endl;
          PHASER_ASSERT(!IWILSON.negative_variance());
        }//debug
#endif

        if (do_gradient or do_hessian)
        {
          for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
          {
            double EA_sym = std::real(ECsym[isym]);
            double EB_sym = std::imag(ECsym[isym]);
            for (int g = 0; g < NODE->NEXT.ngyre(); g++)
            {
              if (REFINE_ROTN)
              {
                for (int rot = 0; rot < 3; rot++)
                {
                  double dEA_by_dRotn = dEC_g_isym_by_dRotn(g,isym,rot).real();
                  double dEB_by_dRotn = dEC_g_isym_by_dRotn(g,isym,rot).imag();
                  double dV_by_dRotn = 2*EA_sym*dEA_by_dRotn + 2*EB_sym*dEB_by_dRotn;
                  dLL_by_dRotn[g][rot] += IWILSON.dLL_by_dV*dV_by_dRotn;
                  if (do_hessian)
                  {
                    double d2EA_by_dRotn2 = d2EC_g_isym_by_dRotn2(g,isym,rot).real();
                    double d2EB_by_dRotn2 = d2EC_g_isym_by_dRotn2(g,isym,rot).imag();
                    double d2V_by_dRotn2 = 2*fn::pow2(dEA_by_dRotn) + 2*fn::pow2(dEB_by_dRotn) +
                                          2*EA_sym*d2EA_by_dRotn2 + 2*EB_sym*d2EB_by_dRotn2;
                    d2LL_by_dRotn2[g][rot] +=
                        IWILSON.d2LL_by_dV2*fn::pow2(dV_by_dRotn) +
                        IWILSON.dLL_by_dV*d2V_by_dRotn2;
                  }
                }
              }
              if (REFINE_TRAN)
              {
                for (int tra = 0; tra < 3; tra++)
                {
                  double dEA_by_dTran = dEC_g_isym_by_dTran(g,isym,tra).real();
                  double dEB_by_dTran = dEC_g_isym_by_dTran(g,isym,tra).imag();
                  double dV_by_dTran = 2*EA_sym*dEA_by_dTran + 2*EB_sym*dEB_by_dTran;
                  dLL_by_dTran[g][tra] += IWILSON.dLL_by_dV*dV_by_dTran;
                  if (do_hessian)
                  {
                    double d2EA_by_dTran2 = d2EC_g_isym_by_dTran2(g,isym,tra).real();
                    double d2EB_by_dTran2 = d2EC_g_isym_by_dTran2(g,isym,tra).imag();
                    double d2V_by_dTran2 = 2*fn::pow2(dEA_by_dTran) + 2*fn::pow2(dEB_by_dTran) +
                                          2*EA_sym*d2EA_by_dTran2 + 2*EB_sym*d2EB_by_dTran2;
                    d2LL_by_dTran2[g][tra] +=
                        IWILSON.d2LL_by_dV2*fn::pow2(dV_by_dTran) +
                        IWILSON.dLL_by_dV*d2V_by_dTran2;
                  }
                }
              }
            }
          }
          if (REFINE_VRMS)
          {
            double minusSsqrTwoPiSqrOn3 = -2*DEF_TWOPISQ_ON_THREE*ssqr;
            sv_double dV_by_dDrms(NODE->NEXT.ngyre(),0);
            sv_double d2V_by_dDrms2(NODE->NEXT.ngyre(),0);
            for (int g = 0; g < NODE->NEXT.ngyre(); g++) //fixed phase relative to each other
            {
              double Vconst = minusSsqrTwoPiSqrOn3;
              double Vconst2 = fn::pow2(Vconst);
              double& totvar_known_s = DobsSigaSqr_gyre[g];
              dV_by_dDrms[g] = -Vconst*totvar_known_s;
              d2V_by_dDrms2[g] = -Vconst2*totvar_known_s;
              for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
              {
                double EA = std::real(EC_g_isym(g,isym))*std::real(ECsym[isym]);
                double EB = std::imag(EC_g_isym(g,isym))*std::imag(ECsym[isym]);
                dV_by_dDrms[g] += Vconst*(EA+EB);//sumEsqr term
                if (do_hessian)
                {
                  d2V_by_dDrms2[g] += Vconst2*(EA+EB);
                }
              }
            }
            for (int g = 0; g < NODE->NEXT.ngyre(); g++) //fixed phase relative to each other
            {
              const auto& entry = NODE->NEXT.GYRE[g].ENTRY;
              const auto& modlid = entry->identify();
              dLL_by_dDrms[modlid] += IWILSON.dLL_by_dV*dV_by_dDrms[g];
              d2LL_by_dDrms2[modlid] += IWILSON.d2LL_by_dV2*fn::pow2(dV_by_dDrms[g]) +
                                        IWILSON.dLL_by_dV*d2V_by_dDrms2[g];
            }
          }
        } //gradient or hessian
      } //selected
    } //reflections

    if (do_gradient)
    {
      int m(0),i(0);
      for (int g = 0; g < NODE->NEXT.ngyre(); g++)
      {
        for (int rot = 0; rot < 3; rot++)
        {
          if (refine_parameter_[m++])
          {
            tgh.Gradient[i] = -dLL_by_dRotn[g][rot];
            if (do_hessian)
              tgh.Hessian(i,i) = -d2LL_by_dRotn2[g][rot];
            i++;
          }
        }
        for (int tra = 0; tra < 3; tra++)
        {
          if (refine_parameter_[m++])
          {
            tgh.Gradient[i] = -dLL_by_dTran[g][tra];
            if (do_hessian)
              tgh.Hessian(i,i) = -d2LL_by_dTran2[g][tra];
            i++;
          }
        }
      }
      for (const auto& p : USED_MODLID)
      {
        if (refine_parameter_[m++])
        {
          auto modlid = NODE->NEXT.GYRE[p].IDENTIFIER;
          tgh.Gradient[i] = -dLL_by_dDrms[modlid];
          if (do_hessian)
            tgh.Hessian(i,i) = -d2LL_by_dDrms2[modlid];
          i++;
        }
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }

    return tgh;
  }

  TargetGradientHessianType
  RefineGYRE::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
    //initialization
    do_gradient = (do_gradient or do_hessian);
    TargetGradientHessianType tgh(nmp,true);

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< TargetGradientHessianType > > results;
      std::vector< std::packaged_task< TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineGYRE::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineGYRE::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end ) );
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
             do_gradient,
             do_hessian,
             beg,
             end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh += result;
      }
    }

    //subtract restraint terms
    //double f = Target;
    if (do_restraint)
    {
      int m(0),i(0);
      for (int g = 0; g < NODE->NEXT.ngyre(); g++)
      {
        auto& gyre = NODE->NEXT.GYRE[g];
        for (int rot = 0; rot < 3; rot++)
        {
          if (refine_parameter_[m++])
          {
            if (SIGMA_ROT)
            {
              tgh.Target += fn::pow2(gyre.ORTHR[rot]/SIGMA_ROT)/2.;
              if (do_gradient)
                tgh.Gradient[i] -= gyre.ORTHR[rot]/fn::pow2(SIGMA_ROT);
              if (do_hessian)
                tgh.Hessian(i,i) -= 1.0/fn::pow2(SIGMA_ROT);
            }
            i++;
          }
        }
        for (int tra = 0; tra < 3; tra++)
        {
          if (refine_parameter_[m++])
          {
            if (SIGMA_TRA)
            {
              tgh.Target += fn::pow2(gyre.ORTHT[tra]/SIGMA_TRA)/2.;
              if (do_gradient)
                tgh.Gradient[i] -= gyre.ORTHT[tra]/fn::pow2(SIGMA_TRA);
              if (do_hessian)
                tgh.Hessian(i,i) -= 1.0/fn::pow2(SIGMA_TRA);
            }
            i++;
          }
        }
      }
      for (int j = 0; j < USED_MODLID.size(); j++) //only interestd in size
      {
        if (refine_parameter_[m++])
          i++; //VRMS AJM restrain to input
      }
      phaser_assert(m == total_number_of_parameters_);
      phaser_assert(i == nmp);
    }
    //negate target here for minimization
    return tgh;
  }

  af_string
  RefineGYRE::initial_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logGyre("Initial");
  }

  af_string
  RefineGYRE::current_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logGyre("Current");
  }

  af_string
  RefineGYRE::final_statistics()
  {
    likelihood(); //so that LLG is correct in the logging
    return NODE->logGyre("Final");
  }

  af_string
  RefineGYRE::finalize_parameters()
  {
    for (int g = 0; g < NODE->NEXT.ngyre(); g++)
    {
      auto& gyre = NODE->NEXT.GYRE[g];
      auto initial = gyre.ENTRY->VRMS_START[0];
      auto modlid = gyre.IDENTIFIER;
      phaser_assert(NODE->DRMS_SHIFT.count(modlid));
      phaser_assert(NODE->VRMS_VALUE.count(modlid));
      phaser_assert(NODE->CELL_SCALE.count(modlid));
      auto drms = NODE->DRMS_SHIFT[modlid].VAL;
      NODE->VRMS_VALUE[modlid].VAL = pod::apply_drms_to_initial(drms,initial);
    }
    af_string output;
    output.push_back("Calculated shifted vrms values");
    return output;
  }
} //phasertng
