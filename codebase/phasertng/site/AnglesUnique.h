//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_AnglesUnique_class__
#define __phasertng_AnglesUnique_class__
#include <phasertng/site/SiteList.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>

namespace phasertng {

class AnglesUnique : public SiteList
{
  private:
    sv_dmat33 osmat = sv_dmat33(0);
    int    NSYMP = 0;
    double tpmax = 0;
    double dtp = 0;
    double tmmax = 0;
    double dtm = 0;
    double alpha = 0;
    double gamma = 0;
    double cosb2 = 0;
    double sinb2 = 0;
    double offm = 0;;
    double epsilon;
    double beta = 0;
    double thetp = 0;
    double thetm = 0;
    double dbeta = 0;
    double nbeta = 0;
    double betmin = 0;
    double betmax = 0;
    double SAMP = 0;
    double offp = 0;
    SpaceGroup SG = SpaceGroup("P 1");
    UnitCell   UC = UnitCell(af::double6(1,1,1,90,90,90));

    int    Size = 0;
    dvect3 Site = {0,0,0};
    int    inext = 0;
    int    iloop = 0;

  public:
    AnglesUnique(double,SpaceGroup&,UnitCell&);
    AnglesUnique(double=3.);

  public:
    bool        at_end();
    void        restart(unsigned=0);
    dvect3      next_site() { return Site; }
    std::size_t count_sites() { return Size; }
    af_dvect3   all_sites();
    double      point_distance() { return SAMP; }
    double      sampling_for_approx_number(double);
    void        set_sampling(double);

//-----------------------------------------------------------------------
// fmod_pos - return modulus if pos or neg
//-----------------------------------------------------------------------
double fmod_pos(double x, double y)
{
  if (x < 0) return fmod(x+1,y) + y - 1;
  return fmod(x,y);
}

};

}
#endif
