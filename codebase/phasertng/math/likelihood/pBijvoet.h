#ifndef __phasertng_math_pBijvoet_class__
#define __phasertng_math_pBijvoet_class__

namespace phasertng {
namespace math {

class pBijvoet
{
  public:
    pBijvoet() {}

  private:
    const double MAXEXPTERM = std::log(std::numeric_limits<double>::max());

  public:
    double expectedE1GivenE2(double,double) const;
    double expectedZ1GivenZ2(double,double) const;
    double sigmaE1GivenE2(double,double) const;
    std::pair<double,double>
      expectedESIGEnegGivenPair(double,double,double,double,double) const;
    std::pair<double,double>
      expectedESIGEposGivenZoposEneg(double,double,double,double) const;
    double pZposZneg(double,double,double) const;
    double pE1GivenE2(double,double,double) const;
    double pSmallerFeffGivenOther(double,double,double,double,double,double,double) const;
    double pLargerFeffGivenOther(double,double,double,double,double,double,double) const;
};

}}
#endif
