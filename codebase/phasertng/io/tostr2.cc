//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/tostr2.h>
#include <assert.h>
#include <cctype>

namespace phasertng {

  std::pair<std::string,std::string> tostr2(std::string atomtype)
  {
    std::string Element2,Element;
    assert(atomtype.size() == 1 || atomtype.size() == 2);
    Element2 = (atomtype.size() == 1) ? " " + atomtype : atomtype;
    if (Element2[1] == ' ')  //then right justify by switch
    { Element2[1] = Element2[0]; Element2[0] = ' '; }
    Element2[0] = std::toupper(Element2[0]);
    Element2[1] = std::toupper(Element2[1]);
    Element = (Element2[0] == ' ') ? std::string(1,Element2[1]) : Element2;
    return { Element2,Element }; //Element2 is rj-str2, Element is str1 or str2
  }
} //phasertng
