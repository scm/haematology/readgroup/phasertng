//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Helper_class__
#define __phasertng_dag_Helper_class__
#include <cctbx/uctbx.h>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <string>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;

namespace phasertng {

namespace dag {

  // Note: PT is the principal translation applied to center the molecule
  // on the origin before computing the molecular transform (i.e. minus
  // the centre)

  dvect3
  fract_wrt_in_from_mt(
      const dvect3 fract,
      const dvect3 eulermt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL);

  dvect3
  fract_wrt_in_from_mt(
      const dvect3 fract,
      const dmat33 ROTmt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL);

  dvect3
  fract_wrt_mt_from_in(
      const dvect3 fract,
      const dvect3 eulermt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL);

  dvect3
  fract_wrt_mt_from_in(
      const dvect3 fract,
      const dmat33 ROTmt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL);

  dvect3
  euler_wrt_in_from_mt(
      const dvect3 euler,
      const dmat33 pr);

  dvect3
  euler_wrt_mt_from_in(
      const dvect3 euler,
      const dmat33 pr);

  std::pair<dvect3,dmat33>
  print_euler_wrt_in_from_mt(dvect3 EULER,dmat33 PR);

  std::string print_polar(dmat33);
  std::string print_fract(dvect3);
  std::string print_ortht(dvect3,int=7);
  std::string header_polar();
  std::string header_fract();
  std::string header_ortht(int=7);

}} //phasertng
#endif
