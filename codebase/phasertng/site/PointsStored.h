//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PointsStored_class__
#define __phasertng_PointsStored_class__
#include <phasertng/site/SiteList.h>
#include <phasertng/site/Site.h>
#include <phasertng/io/Loggraph.h>

namespace phasertng {

class PointsStored : public SiteList
{
  //use vector as clustering will be faster with elements in cache
  public:
    std::vector<Site> translation_function_data;
    std::vector<Site>::iterator inext = translation_function_data.begin();
    double f_mean = 0;
    double f_sigma = 0;
    double f_top = 0;
    double z_top = 0;
    double SAMP = 0;

  public:
    PointsStored() : SiteList() { translation_function_data.resize(0); }
    double  peak() { return inext->value; }

  public:
    bool         at_end();
    void         restart(unsigned=0);
    dvect3       next_site();
    std::size_t  count_sites();
    af_dvect3    all_sites();
    double       point_distance() { return SAMP; }

  public:
    void partial_sort_and_erase(const int&);
    void full_sort();
    void reverse();
    Loggraph logGraph(std::string);
    void select_topsites_and_erase();
    void mean_sigma();
    void fill_memory_points_on_disc(int);
    void fill_memory_points_on_sphere(int);
};

}
#endif
