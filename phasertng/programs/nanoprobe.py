import os
from libtbx.program_template import ProgramTemplate
from libtbx.utils import Sorry
from phasertng import Phasertng
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.phil.master_phil_file import maximum_expert_level
from phasertng.phil.build_voyager_phil_file import *
from phasertng.phil.converter_registry import *
from phasertng.voyager.voyager_generator import *
from phasertng.voyager.nanoprobe import *

# =============================================================================
# Program class (location should be phasertng/programs)
class Program(ProgramTemplate):

  # Class variables are used to define basic program properties
  description = voyager_generator().wrapped_text("nanoprobe")

  # Define PHIL scope
  master_phil_str = build_voyager_phil_file(program_name="nanoprobe")
  master_phil_str = master_phil_str + "\n" + build_auto_str("nanoprobe")

  phil_converters = additional_converters

  # Datatypes expected by program
  # Datatypes not listed will not be recognized and show up as unused files
  # The list of available datatypes is in iotbx/data_manager
  # Each datatype is in its own file with some basic functions defined
  datatypes = ['phil','sequence','miller_array']

  # Citations
  # The list of known citations is in libtbx/citations.params
  # Custom citations can be added via the "citations" class variable
  known_article_ids = ['phasertng']

  # The constructor is not needed since all it does is store the DataManager and
  # PHIL scope extract as instance variables, self.data_manager and self.params,
  # respectively. Also, a multi_out instance is created for logging (self.logger)

  def validate(self):
    # The DataManager can do some basic checks
    # More basic checks are planned and they will exist in a central location
    # Also, do checks on the parameters

    # Other datatypes have a similar function (has_sequences)
    #set the filenames on the phil input
    if self.data_manager.has_sequences():
      self.params.phasertng.biological_unit.sequence.filename =  self.data_manager.get_sequence_names()[0]
  #  silent running, no database
    self.params = default_voyager_database(self.params,"nanoprobe")
    if self.data_manager.has_miller_arrays():
      #returns list, so take first
      self.params.phasertng.hklin.filename =  self.data_manager.get_miller_array_names()[0]
    #new self.params (phil extract)

  def run(self):
    # Actual code for scientific stuff
    # The Program template is basically a wrapper for handling I/O with the user
    print('Run nanoprobe...', file=self.logger)
    if self.params.output.overwrite:
      print('Overwrite working directory',file=self.logger)
    self.params.phasertng.overwrite = self.params.output.overwrite
    # rmtree for directory is dangerous
    phil_scope = self.master_phil.format(python_object=self.params)  # converts extract to scope
    phil_str = phil_scope.as_str(expert_level=maximum_expert_level, attributes_level=0) # converts scope to str
    if False:
      phil_diff = self.master_phil.fetch_diff(source=phil_scope) #testing different parsing
      for e in phil_diff.all_definitions():
        print(e.path, str(getattr(e.object, "type")), e.object.as_str())
    try:
      basetng = nanoprobe(user_phil_str=phil_str)
    except Exception as e:
      rawmsg = "" if str(e) is None else str(e)
      #the phasertng error type is encoded in the return as an integer (enum)
      err,message = rawmsg.split(maxsplit=1)
      if err.isnumeric(): #special type
        print("Exit Code:",int(err),":",message)
      else:
        print("Exit Code:",int(enum_err_code.fatal),":",rawmsg)

  def get_results(self):
    # The GUI will use this for displaying any final results that are important
    pass

# =============================================================================
# Command-line tool (location should be phaser/command_line)
from iotbx.cli_parser import run_program

if (__name__ == '__main__'):
  run_program(Program)

# Some example command-line commands
#   libtbx.python example.py
#   libtbx.python example.py --show-defaults
#   libtbx.python example.py --show-defaults --show-attributes-level=1
#   libtbx.python example.py --citations
#   libtbx.python example.py model_filename=<model file>
#   libtbx.python example.py sequence_filename=<sequence file>

# Arguments should be grouped. The "--" flags should be together.

# The CCTBXParser (iotbx.cli_parser) will automatically show the program name
# based on the dispatcher name and construct appropriate filenames for PHIL
# files. Some default filenames for the output is planned.

# For multiple ensembles in this example, a PHIL file will need to be defined
# to keep models and sequences together. Files (.type=path) in a PHIL file
# will be automatically tracked by the DataManager.

# The CCTBXParse will read and parse PHIL files
