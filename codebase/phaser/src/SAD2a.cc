//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/main/Phaser.h>
#include <phaser/src/RefineSAD.h>
#include <phasertng/math/RiceWoolfson.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
}

namespace phaser {

//-- acentric reflections

acentric
RefineSAD::acentricReflIntgSAD2(unsigned& r)
{
  int i = ibin[r];
  acentric thisIntg(acentPhsIntg[i].nStep);
  floatType Like(0);
  floatType TWO(2.0);
  cmplxType Dphi(DphiA[r],DphiB[r]);
  floatType SigmaNeg(sigDsqr[r] + fn::pow2(NEG.SIGF[r]));
  floatType SigmaPos(sigPlus[r] + (fn::pow2(POS.SIGF[r]) + fn::pow2(NEG.SIGF[r])));
  assert(SigmaNeg > 0);
  assert(SigmaPos > 0);
  floatType SigmaNegInv(1.0/SigmaNeg);
  floatType SigmaPosInv(1.0/SigmaPos);

  //Arguments for exponential will be negative, with the maximum typically close to zero.
  //To attempt to avoid underflow, set initial exponential offset that is small enough
  //to still avoid overflow when summing and squaring for FOM calculation.
  //If necessary, this will be updated and the integral repeated.
  static floatType logMaxFloat(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxArgAllow(logMaxFloat/TWO-std::log(360.)); //maximum arg to allow for exponential
  static floatType minArgAllow(-maxArgAllow); // minimum arg, below which probs will be set to zero

  floatType foneg(NEG.F[r]),fopos(POS.F[r]);
  cmplxType fhneg(FHneg[r]),fhpos(FHpos[r]);
  floatType topExpArg(-std::numeric_limits<floatType>::max());
  floatType expArgOffset(maxArgAllow);
  for (int oloop = 0; oloop < 2; oloop++)
  {
    Like = 0.0;
    for (int p = 0; p < acentPhsIntg[i].nStep; p++)
    {
      cmplxType FCneg(foneg*acentPhsIntg[i].CosAng[p],foneg*acentPhsIntg[i].SinAng[p]);
      floatType absFCpos(std::abs(fhpos + Dphi*(FCneg - fhneg)));
      floatType expFHneg(std::norm(FCneg - fhneg)*SigmaNegInv);
      floatType expFOpos(fn::pow2(fopos - absFCpos)*SigmaPosInv);
      floatType expArg(-expFHneg-expFOpos);
      topExpArg = std::max(topExpArg,expArg);
      floatType BessTerm(phasertng::tbl_ebesseli0.get(TWO*fopos*absFCpos*SigmaPosInv));
      floatType expArgTotal(expArg+expArgOffset);
      floatType expTerm = (expArgTotal > minArgAllow) ? std::exp(expArgTotal) : 0.;
      floatType phsProb(expTerm*BessTerm);
      Like += phsProb;
      thisIntg.setProb(p,phsProb,acentPhsIntg[i].Ang[p]);
    } //loop over p
    if (Like > 0.) break;
    expArgOffset = maxArgAllow - topExpArg;
  } //loop to apply new offset if necessary

  //Overall scaling factors, except for exponential offset
  Like *= acentPhsIntg[i].Weight;
  Like *= TWO*fopos*foneg/(scitbx::constants::pi*SigmaPos*SigmaNeg);

  assert(Like > 0); // Could probably eliminate now
  thisIntg.LogLike = -log(Like) + expArgOffset; // Remove offset
  //Store measure of relative influence of partial structure for
  //bias-corrected map coefficients
  floatType Xano(std::norm(fhpos-fhneg)*SigmaPosInv);
  floatType Xpart(std::abs(fhneg)*foneg*SigmaNegInv);
  thisIntg.WtPart = (Xpart > 0.) ? Xpart/(Xano + Xpart) : 0.;
  return thisIntg;
}

floatType
RefineSAD::acentricReflProbSAD2(unsigned& r,bool negative,floatType FOlarge)
//Compute conditional probability of supplied value of one Friedel mate (nominally
//the one that had the largest measured value) given the other.
{
  int i = ibin[r];
  floatType FOsmall = negative ? POS.F[r] : NEG.F[r];
  cmplxType FHsmall = negative ? FHpos[r] : FHneg[r];
  cmplxType FHlarge = negative ? FHneg[r] : FHpos[r];
  floatType VarFOsmall = fn::pow2(negative ? POS.SIGF[r] : NEG.SIGF[r]);
  floatType VarFOlarge = fn::pow2(negative ? NEG.SIGF[r] : POS.SIGF[r]);

  floatType Prob(0);
  floatType TWO(2.0);
  cmplxType Dphi(DphiA[r],negative ? -DphiB[r] : DphiB[r]);
  floatType SigmaSmall(sigDsqr[r] + VarFOsmall);
  floatType SigmaLarge(sigPlus[r] + (VarFOsmall + VarFOlarge));
  assert(SigmaSmall > 0);
  assert(SigmaLarge > 0);
  assert(FOsmall > 0);
  assert(FOlarge > 0);
  floatType SigmaSmallInv(1.0/SigmaSmall);
  floatType SigmaLargeInv(1.0/SigmaLarge);

  //Arguments for exponential will be negative, with the maximum typically close to zero.
  //To attempt to avoid underflow, set initial exponential offset that is small enough
  //to still avoid overflow when summing and squaring for FOM calculation.
  //If necessary, this will be updated and the integral repeated.
  static floatType logMaxFloat(std::log(std::numeric_limits<floatType>::max()));
  static floatType logMinFloat(std::log(std::numeric_limits<floatType>::min()));
  static floatType minLogProb(logMinFloat+std::log(TWO)); // minimum log of probability to allow
  static floatType maxArgAllow(logMaxFloat/TWO-std::log(360.)); //maximum arg to allow for exponential
  static floatType minArgAllow(-maxArgAllow); // minimum arg, below which probs will be set to zero
  floatType topExpArg(-std::numeric_limits<floatType>::max());
  floatType expArgOffset(maxArgAllow);

  //First compute joint probability
  for (int oloop = 0; oloop < 2; oloop++)
  {
    Prob = 0.0;
    for (int p = 0; p < acentPhsIntg[i].nStep; p++)
    {
      cmplxType FCsmall(FOsmall*acentPhsIntg[i].CosAng[p],FOsmall*acentPhsIntg[i].SinAng[p]);
      floatType absFClarge(std::abs(FHlarge + Dphi*(FCsmall - FHsmall)));
      floatType expFHsmall(std::norm(FCsmall - FHsmall)*SigmaSmallInv);
      floatType expFOlarge(fn::pow2(FOlarge - absFClarge)*SigmaLargeInv);
      floatType expArg(-expFHsmall-expFOlarge);
      topExpArg = std::max(topExpArg,expArg);
      floatType expArgTotal(expArg+expArgOffset);
      if (expArgTotal > minArgAllow) Prob += std::exp(expArgTotal)*phasertng::tbl_ebesseli0.get(TWO*FOlarge*absFClarge*SigmaLargeInv);
    } //loop over p
    if (Prob > 0.) break;
    expArgOffset = maxArgAllow - topExpArg;
  } //loop to apply new offset if necessary
  //Normalise, divide by probability of smaller F to get conditional probability of larger one
  floatType pFsmall(phasertng::pRiceF(FOsmall,std::abs(FHsmall),SigmaSmall));
  Prob *= TWO*FOsmall*FOlarge/(scitbx::constants::pi*SigmaLarge*SigmaSmall);
  floatType logProb(std::log(Prob*acentPhsIntg[i].Weight/pFsmall) - expArgOffset);
  logProb = std::max(logProb,minLogProb); // Ensure there will not be underflow
  Prob = std::exp(logProb);
  return Prob;
}

floatType
RefineSAD::acentricReflGradSAD2(unsigned& r,bool atomic,bool sigmaa)
{
  int i = ibin[r];
  floatType Like(0);
  floatType LogLike(0.);
//#define __FD_GRAD_ACENT__
#ifdef __FD_GRAD_ACENT__
 if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_MATHEMATICA") == 0)
    { std::cout << "setenv PHASER_TEST_MATHEMATICA\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
///mathematica file
std::cout << "===Set Mathematica===\n";
FHpos[r] = cmplxType(33.3106,1.16551);
FHneg[r] = cmplxType(33.1877,3.08727);
POS.F[r] = 93.4268;
SIGPOS.F[r] = 0.79811;
NEG.F[r] = 90.8979;
SIGNEG.F[r] = 0.85036;
DphiA[r] = 1.0;
DphiB[r] = 2e-07;
sigDsqr[r] = 13481.6;
sigPlus[r] = 0.00269632;
//vpFm = 0 //GAUSS 1
std::cout << "FHpos    " << FHpos[r] << std::endl;
std::cout << "FHneg    " << FHneg[r] << std::endl;
std::cout << "POS.F    " << POS.F[r] << std::endl;
std::cout << "POS.SIGF " << POS.SIGF[r] << std::endl;
std::cout << "NEG.F    " << NEG.F[r] << std::endl;
std::cout << "NEG.SIGF " << NEG.SIGF[r] << std::endl;
std::cout << "SA       " << DphiA[r] << std::endl;
std::cout << "SB       " << DphiB[r] << std::endl;
std::cout << "SDsqr    " << sigDsqr[r] << std::endl;
std::cout << "SP       " << sigPlus[r] << std::endl;
std::cout << "======\n";
}
#endif

#ifdef __FD_GRAD_ACENT__
  floatType epsilon(shift);
  floatType *test = &(sigDsqr[r]);
  floatType &dL_by_dtest = dL_by_dSDsqr;
  LogLike = acentricReflIntgSAD2(r).LogLike;
  (*test) += epsilon;
  floatType DLogLikeForward = acentricReflIntgSAD2(r).LogLike;
  floatType FD_test_forward = (DLogLikeForward-LogLike)/shift;
  (*test) -= epsilon;
  (*test) -= epsilon;
  floatType DLogLikeBackward = acentricReflIntgSAD2(r).LogLike;
  floatType FD_test_backward = (DLogLikeBackward-LogLike)/shift;
  (*test) += epsilon;
#endif

  floatType TWO(2.0);
  cmplxType Dphi(DphiA[r],DphiB[r]);
  floatType VarFOneg = fn::pow2(NEG.SIGF[r]);
  floatType VarFOpos = fn::pow2(POS.SIGF[r]);
  floatType SigmaNeg(sigDsqr[r] + VarFOneg);
  assert(SigmaNeg > 0);
  floatType SIGFsqr(VarFOpos + VarFOneg);
  floatType SigmaPos(sigPlus[r] + SIGFsqr);
  assert(SigmaPos > 0);
 // floatType SigPosSqr(fn::pow2(SigmaPos));
  //floatType SigNegSqr(fn::pow2(SigmaNeg));
  //floatType SN2SP(SigNegSqr*SigmaPos);
  //floatType SNSP2(SigmaNeg*SigPosSqr);
 // floatType SN2SP2(SigNegSqr*SigPosSqr);
  floatType ReFHpos(std::real(FHpos[r]));
  floatType ImFHpos(std::imag(FHpos[r]));
  floatType ReFHneg(std::real(FHneg[r]));
  floatType ImFHneg(std::imag(FHneg[r]));

  //Arguments for exponential will be negative, with the maximum typically close to zero.
  //To attempt to avoid underflow, set initial exponential offset that is small enough
  //to still avoid overflow when summing and squaring for FOM calculation.
  //If necessary, this will be updated and the integral repeated.
  //Exponential offset cancels out in derivative calculation, but correction must be made for Like.
  static floatType logMaxFloat(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxArgAllow(logMaxFloat/TWO-std::log(360.)); //maximum arg to allow for exponential
  static floatType minArgAllow(-maxArgAllow); // minimum arg, below which probs will be set to zero

  floatType foneg(NEG.F[r]),fopos(POS.F[r]),sigphia(DphiA[r]),sigphib(DphiB[r]);
  cmplxType fhneg(FHneg[r]),fhpos(FHpos[r]);
  floatType topExpArg(-std::numeric_limits<floatType>::max());
  floatType expArgOffset(maxArgAllow);
  for (int oloop = 0; oloop < 2; oloop++)
  {
    //initialization
    dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0.;
    dL_by_dSDsqr = dL_by_dSA = dL_by_dSB = dL_by_dSP = 0.;
    Like = 0.0;

    for (int p = 0; p < acentPhsIntg[i].nStep; p++)
    {
      cmplxType FCneg(foneg*acentPhsIntg[i].CosAng[p],foneg*acentPhsIntg[i].SinAng[p]);
      floatType absFCpos(std::abs(fhpos + Dphi*(FCneg - fhneg)));
      floatType expFHneg(std::norm(FCneg - fhneg)/SigmaNeg);
      floatType expFOpos(fn::pow2(fopos - absFCpos)/SigmaPos);
      floatType expArg(-expFHneg-expFOpos);
      topExpArg = std::max(topExpArg,expArg);
      floatType expArgTotal(expArg+expArgOffset);
      floatType expTerm((expArgTotal > minArgAllow) ? std::exp(expArgTotal) : 0.);
      floatType BessArg(TWO*fopos*absFCpos/SigmaPos);
      floatType eBessI0(phasertng::tbl_ebesseli0.get(BessArg));
      floatType eBessI1(phasertng::tbl_ebesseli1.get(BessArg));
      floatType diffBessTerm((TWO*expTerm/SigmaPos)*(fopos*eBessI1-absFCpos*eBessI0));
      floatType ReFHnFOnCospFOn(-ReFHneg + foneg*acentPhsIntg[i].CosAng[p]);
      floatType ImFHnFOnSinpFOn(-ImFHneg + foneg*acentPhsIntg[i].SinAng[p]);
      floatType FHterm1(ImFHpos + sigphib*ReFHnFOnCospFOn + sigphia*ImFHnFOnSinpFOn);
      floatType FHterm2(ReFHpos + sigphia*ReFHnFOnCospFOn - sigphib*ImFHnFOnSinpFOn);
      floatType sqroot(std::sqrt(fn::pow2(FHterm1) + fn::pow2(FHterm2)));

      Like += expTerm*eBessI0;

      if (atomic)
      {
        floatType dFC_by_dReFHpos(0.),dFC_by_dImFHpos(0.),dFC_by_dReFHneg(0.),dFC_by_dImFHneg(0.);
        if (sqroot > 0.)
        {
          dFC_by_dReFHpos = FHterm2/sqroot;
          dFC_by_dImFHpos = FHterm1/sqroot;
          dFC_by_dReFHneg = (-sigphib*FHterm1 - sigphia*FHterm2)/sqroot;
          dFC_by_dImFHneg = (-sigphia*FHterm1 + sigphib*FHterm2)/sqroot;
        }
        dL_by_dReFHpos -= dFC_by_dReFHpos*diffBessTerm;
        dL_by_dImFHpos -= dFC_by_dImFHpos*diffBessTerm;
        floatType TWOexpTermBessIO(expTerm*eBessI0*TWO/SigmaNeg);
        dL_by_dReFHneg -= dFC_by_dReFHneg*diffBessTerm + TWOexpTermBessIO*ReFHnFOnCospFOn;
        dL_by_dImFHneg -= dFC_by_dImFHneg*diffBessTerm + TWOexpTermBessIO*ImFHnFOnSinpFOn;
      }

      if (sigmaa)
      {
        floatType dFC_by_dSA(0.),dFC_by_dSB(0.);
        if (sqroot > 0.)
        {
          dFC_by_dSA = (ImFHnFOnSinpFOn*FHterm1 + ReFHnFOnCospFOn*FHterm2)/sqroot;
          dFC_by_dSB = (ReFHnFOnCospFOn*FHterm1 - ImFHnFOnSinpFOn*FHterm2)/sqroot;
        }
        dL_by_dSA   -= dFC_by_dSA*diffBessTerm;
        dL_by_dSB   -= dFC_by_dSB*diffBessTerm;
        dL_by_dSP -= (1/SigmaPos)*expTerm*((BessArg+expFOpos-1)*eBessI0 - BessArg*eBessI1);
        floatType dexpFHnegbySD = -((fn::pow2(-ReFHnFOnCospFOn) + fn::pow2(-ImFHnFOnSinpFOn))/fn::pow2(SigmaNeg));
        dL_by_dSDsqr   -= -expTerm*dexpFHnegbySD*eBessI0-(1.0/SigmaNeg)*expTerm*eBessI0;
      }

      //Like = expTerm*eBessI0
      //Derivatives = derivative of [(1/SN*SP)*expTerm*eBessI0] * SNSP
      //At the end divide by Like (which doesn't have the SNSP factor at the front) to get the
      //derivative of [(1/SN*SP)*expTerm*eBessI0], the SAD function without the constant factors

      #ifdef __FD_GRAD_ACENT__
      if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
        {
        std::cout << "p = " << acentPhsIntg[i].Ang[p] << std::endl <<
                    " expFOpos " << expFOpos  <<
                    " expTerm " << expTerm  <<
                    " BessArg " << BessArg  << std::endl <<
                    " Like " << expTerm*eBessI0 <<
                    " first derivative  " << dL_by_dtest <<std::endl;
        }
      #endif

    } //loop over p
    if (Like > 0.) break;
    expArgOffset = maxArgAllow - topExpArg;
  } //loop to apply new offset if necessary

  assert(Like > 0); // Could probably eliminate now
  floatType LikeFactor(1.0/Like);//before weighting, as derivatives are unweighted
  Like *= acentPhsIntg[i].Weight;
  Like *= TWO*fopos*foneg/(scitbx::constants::pi*SigmaPos*SigmaNeg);
  LogLike = -log(Like) + expArgOffset; //Remove offset

  dL_by_dReFHpos *= LikeFactor;
  dL_by_dImFHpos *= LikeFactor;
  dL_by_dReFHneg *= LikeFactor;
  dL_by_dImFHneg *= LikeFactor;
  dL_by_dSDsqr   *= epsn[r]*LikeFactor;
  dL_by_dSA      *= LikeFactor;
  dL_by_dSB      *= LikeFactor;
  dL_by_dSP      *= epsn[r]*LikeFactor;
  /* if (r%1000 == 0) // Some debug code: remove later
  {
    std::cout << "\nacentGrad r, SDsqr, Sigma+, LL, dLL_by_dRe/ImFHpos/neg: " << r << " " << sigDsqr[r] << " " << sigPlus[r] << " " << -LogLike << " " << dL_by_dReFHpos << " " << dL_by_dImFHpos << " " << dL_by_dReFHneg << " " << dL_by_dImFHneg << std::endl;
    std::cout << "Log[SADfunc2[" << fopos << "," << foneg << "," << ReFHpos << "+I*" << ImFHpos << "," << ReFHneg << "+I*" << ImFHneg << "," << SigmaPos << "," << SigmaNeg << "]]" << std::endl;
  } */

#ifdef __FD_GRAD_ACENT__
  std::cout << "=== First Derivative FD test (r=" << r << ") ===\n";
  std::cout << "First derivative " << dL_by_dtest  <<std::endl;
  std::cout << "Ratio forward "  << dL_by_dtest/FD_test_forward <<
                    " backward " << dL_by_dtest/FD_test_backward << "\n";
  if (r >= std::atof(getenv("PHASER_TEST_NREFL")) || std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
#endif

  return LogLike;
}

floatType
RefineSAD::acentricReflHessSAD2(unsigned& r,bool atomic,bool sigmaa)
{
  int i = ibin[r];
  floatType Like(0.),LogLike(0.),pLike(0.);
//#define __FD_HESS_ACENT__
#ifdef __FD_HESS_ACENT__
 if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT 0.0001\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_SHIFTB") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFTB 0.0001\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL 1\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_MATHEMATICA") == 0)
    { std::cout << "setenv PHASER_TEST_MATHEMATICA 1\n" ; std::exit(1); }
 floatType shift_a = std::atof(getenv("PHASER_TEST_SHIFT"));
 floatType shift_b = std::atof(getenv("PHASER_TEST_SHIFT"));
//hint for mathematica GAUSS NUM 1
if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
///mathematica file
std::cout << "===Set Mathematica===\n";
FHpos[r] = cmplxType(33.3106,1.16551);
FHneg[r] = cmplxType(33.1877,3.08727);
POS.F[r] = 93.4268;
POS.SIGF[r] = std::sqrt(0.79811);
NEG.F[r] = 90.8979;
NEG.SIGF[r] = std::sqrt(0.85036);
DphiA[r] = 1.0;
DphiB[r] = 2e-07;
sigDsqr[r] = 13481.6;
sigPlus[r] = 0.00269632;
//vpFm = 0 //GAUSS 1
std::cout << "FHpos    " << FHpos[r] << std::endl;
std::cout << "FHneg    " << FHneg[r] << std::endl;
std::cout << "POS.F    " << POS.F[r] << std::endl;
std::cout << "POS.SIGF " << POS.SIGF[r] << std::endl;
std::cout << "NEG.F    " << NEG.F[r] << std::endl;
std::cout << "NEG.SIGF " << NEG.SIGF[r] << std::endl;
std::cout << "SA       " << DphiA[r] << std::endl;
std::cout << "SB       " << DphiB[r] << std::endl;
std::cout << "SDsqr    " << sigDsqr[r] << std::endl;
std::cout << "SP       " << sigPlus[r] << std::endl;
std::cout << "======\n";
}
#endif

#ifdef __FD_HESS_ACENT__
  //tests single first _a, _b and same or mixed second derivatives _a_b
  //epsilons and test type depend on types of derivatives
  //d2L_by_dReFHpos_dReFHneg etc
#if 0
  cmplxType epsilon_a(0,shift_a);
  floatType epsilon_b(shift_b);
  cmplxType *test_a = &(FHneg[r]);
  floatType *test_b = &(sigDsqr[r]);
  floatType &dL_by_dtest_a = dL_by_dImFHneg;
  floatType &dL_by_dtest_b = dL_by_dSDsqr;
  floatType &d2L_by_dtest2 = d2L_by_dImFHneg_dSDsqr;
#endif
#if 1
  floatType epsilon_a(shift_a);
  floatType epsilon_b(shift_b);
  floatType *test_a = &(sigDsqr[r]);
  floatType *test_b = &(sigDsqr[r]);
  floatType &dL_by_dtest_a = dL_by_dSDsqr;
  floatType &dL_by_dtest_b = dL_by_dSDsqr;
  floatType &d2L_by_dtest2 = d2L_by_dSDsqr2;
#endif

  floatType f_start = acentricReflGradSAD2(r,true,true);
  (*test_a) += shift_a;
  floatType f_forward = acentricReflGradSAD2(r,true,true);
  (*test_a) -= shift_a;
  (*test_a) -= shift_a;
  floatType f_backward = acentricReflGradSAD2(r,true,true);
  (*test_a) += shift_a;
  floatType FD_test_on_f = (f_forward-2*f_start+f_backward)/shift_a/shift_a;

#endif

  floatType TWO(2.0);
  cmplxType Dphi(DphiA[r],DphiB[r]);
  floatType SigmaNeg(sigDsqr[r] + fn::pow2(NEG.SIGF[r]));
  floatType SIGFsqr(fn::pow2(POS.SIGF[r]) + fn::pow2(NEG.SIGF[r]));
  floatType SigmaPos(sigPlus[r] + SIGFsqr);
  assert(SigmaNeg > 0);
  assert(SigmaPos > 0);
  floatType SigPosSqr(fn::pow2(SigmaPos)),SigNegSqr(fn::pow2(SigmaNeg));
  floatType ONEonSPSN(1/(SigmaPos*SigmaNeg));
  floatType SNSP2(SigmaNeg*SigPosSqr),SNSP3(SNSP2*SigmaPos);
 // floatType SN2SP(SigNegSqr*SigmaPos);
  floatType SN2SP2(SigNegSqr*SigPosSqr);
  //floatType SN3SP3(SN2SP2*SigmaPos*SigmaNeg);
  //floatType SN4SP(SN4*SigmaPos),SN3SP(SN3*SigmaPos),SN5SP5(SN2SP2*SN3SP3);
  floatType SN3(SigNegSqr*SigmaNeg);
  floatType SN3SP(SN3*SigmaPos);
  //floatType SN4(SigNegSqr*SigNegSqr);
  floatType ReFHpos(std::real(FHpos[r])),ImFHpos(std::imag(FHpos[r]));
  floatType ReFHneg(std::real(FHneg[r])),ImFHneg(std::imag(FHneg[r]));



  //Arguments for exponential will be negative, with the maximum typically close to zero.
  //To attempt to avoid underflow, set initial exponential offset that is small enough
  //to still avoid overflow when summing and squaring for FOM calculation.
  //If necessary, this will be updated and the integral repeated.
  //Exponential offset cancels out in derivative calculation, but correction must be made for Like.
  static floatType logMaxFloat(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxArgAllow(logMaxFloat/TWO-std::log(360.)); //maximum arg to allow for exponential
  static floatType minArgAllow(-maxArgAllow); // minimum arg, below which probs will be set to zero

  floatType topExpArg(-std::numeric_limits<floatType>::max());
  floatType expArgOffset(maxArgAllow);
  for (int oloop = 0; oloop < 2; oloop++)
  {
    //initialization
    dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0;
    d2L_by_dReFHpos2 = d2L_by_dReFHpos_dImFHpos = d2L_by_dReFHpos_dReFHneg = d2L_by_dReFHpos_dImFHneg = 0;
    d2L_by_dImFHpos2 = d2L_by_dImFHpos_dReFHneg = d2L_by_dImFHpos_dImFHneg = 0;
    d2L_by_dReFHneg2 = d2L_by_dReFHneg_dImFHneg = 0;
    d2L_by_dImFHneg2 = 0;
    dL_by_dSDsqr = dL_by_dSA = dL_by_dSB = dL_by_dSP = 0;
    d2L_by_dSDsqr2 = d2L_by_dSA2 = d2L_by_dSB2 = d2L_by_dSP2 = 0;
    Like = 0.0;
    for (int p = 0; p < acentPhsIntg[i].nStep; p++)
    {
      cmplxType FCneg(NEG.F[r]*acentPhsIntg[i].CosAng[p],NEG.F[r]*acentPhsIntg[i].SinAng[p]);
      floatType absFCpos(std::abs(FHpos[r] + Dphi*(FCneg - FHneg[r])));
      floatType expFHneg(std::norm(FCneg - FHneg[r])/SigmaNeg);
      floatType expFOpos(fn::pow2(POS.F[r] - absFCpos)/SigmaPos);
      floatType expArg(-expFHneg-expFOpos);
      topExpArg = std::max(topExpArg,expArg);
      floatType expArgTotal(expArg+expArgOffset);
      floatType expTerm((expArgTotal > minArgAllow) ? std::exp(expArgTotal) : 0.);
      floatType ReFHnFOnCospFOn(-ReFHneg + NEG.F[r]*acentPhsIntg[i].CosAng[p]);
      floatType ImFHnFOnSinpFOn(-ImFHneg + NEG.F[r]*acentPhsIntg[i].SinAng[p]);
      floatType ReFHnFOnCospFOnSqr(fn::pow2(ReFHnFOnCospFOn));
      floatType ImFHnFOnSinpFOnSqr(fn::pow2(ImFHnFOnSinpFOn));
      floatType FHterm1(ImFHpos + DphiB[r]*ReFHnFOnCospFOn + DphiA[r]*ImFHnFOnSinpFOn);
      floatType FHterm2(ReFHpos + DphiA[r]*ReFHnFOnCospFOn - DphiB[r]*ImFHnFOnSinpFOn);
      floatType sqroot(std::sqrt(fn::pow2(FHterm1) + fn::pow2(FHterm2)));
      floatType SASBterm(0.),ReFHImFHterm(0.);
      if (sqroot > 0.)
      {
        SASBterm = (fn::pow2(DphiA[r])+fn::pow2(DphiB[r]))/sqroot;
        ReFHImFHterm = (ReFHnFOnCospFOnSqr+ImFHnFOnSinpFOnSqr)/sqroot;
      }
      floatType ReFHterm(-DphiB[r]*FHterm1 - DphiA[r]*FHterm2);
      floatType ImFHterm(-DphiA[r]*FHterm1 + DphiB[r]*FHterm2);
      floatType BessArg(TWO*POS.F[r]*absFCpos/SigmaPos);
      floatType eBessI0(phasertng::tbl_ebesseli0.get(BessArg));
      floatType eBessI1(phasertng::tbl_ebesseli1.get(BessArg));
      floatType diffBessTerm((TWO*expTerm/SigmaPos)*(POS.F[r]*eBessI1-absFCpos*eBessI0));
      floatType FOposSqr(fn::pow2(POS.F[r]));
      floatType FCposSqr(fn::pow2(absFCpos));
      floatType FOsqrSPFCsqr(2*FOposSqr-SigmaPos+2*FCposSqr);
      floatType FOSPBessI0(2*expTerm*(FOsqrSPFCsqr*eBessI0 - 2*(FOposSqr/BessArg +2*POS.F[r]*absFCpos)*eBessI1)/SigPosSqr);

      pLike += ONEonSPSN*expTerm*eBessI0;
      Like += expTerm*eBessI0;

      if (atomic)
      {
      floatType dFC_by_dReFHpos(0.),dFC_by_dImFHpos(0.),dFC_by_dReFHneg(0.),dFC_by_dImFHneg(0.);
      floatType d2FC_by_dReFHpos2(0.),d2FC_by_dImFHpos2(0.),d2FC_by_dReFHneg2(0.),d2FC_by_dImFHneg2(0.);
      floatType d2FC_by_dReFHpos_dImFHpos(0.),d2FC_by_dReFHpos_dReFHneg(0.),d2FC_by_dReFHpos_dImFHneg(0.);
      floatType d2FC_by_dImFHpos_dReFHneg(0.),d2FC_by_dImFHpos_dImFHneg(0.),d2FC_by_dReFHneg_dImFHneg(0.);
      if (sqroot > 0.)
      {
        dFC_by_dReFHpos = FHterm2/sqroot;
        dFC_by_dImFHpos = FHterm1/sqroot;
        dFC_by_dReFHneg = ReFHterm/sqroot;
        dFC_by_dImFHneg = ImFHterm/sqroot;
        d2FC_by_dReFHpos2         = (1.0-fn::pow2(dFC_by_dReFHpos))/sqroot;
        d2FC_by_dImFHpos2         = (1.0-fn::pow2(dFC_by_dImFHpos))/sqroot;
        d2FC_by_dReFHneg2         = -fn::pow2(dFC_by_dReFHneg)/sqroot + SASBterm;
        d2FC_by_dImFHneg2         = -fn::pow2(dFC_by_dImFHneg)/sqroot + SASBterm;
        d2FC_by_dReFHpos_dImFHpos = -dFC_by_dReFHpos*dFC_by_dImFHpos/sqroot;
        d2FC_by_dReFHpos_dReFHneg = (-dFC_by_dReFHpos*dFC_by_dReFHneg - DphiA[r])/sqroot;
        d2FC_by_dReFHpos_dImFHneg = (-dFC_by_dReFHpos*dFC_by_dImFHneg + DphiB[r])/sqroot;
        d2FC_by_dImFHpos_dReFHneg = (-dFC_by_dImFHpos*dFC_by_dReFHneg - DphiB[r])/sqroot;
        d2FC_by_dImFHpos_dImFHneg = (-dFC_by_dImFHpos*dFC_by_dImFHneg - DphiA[r])/sqroot;
        d2FC_by_dReFHneg_dImFHneg = -dFC_by_dReFHneg*dFC_by_dImFHneg/sqroot;
      }

      floatType eBessI1FOonFC(eBessI1*POS.F[r]/absFCpos);
      floatType FOURFCSP(4*FCposSqr + SigmaPos);
      floatType FOURFCSPSN(FOURFCSP*SigmaNeg);
      floatType FOsqrSPFCsqrSN(FOsqrSPFCsqr*SigmaNeg);
      floatType FCSNSP(absFCpos*SigmaNeg*SigmaPos);
      floatType TWOFCSP(2*absFCpos*SigmaPos);

      dL_by_dReFHpos -= dFC_by_dReFHpos*diffBessTerm;
      d2L_by_dReFHpos2 -= FOSPBessI0*fn::pow2(dFC_by_dReFHpos) + diffBessTerm*d2FC_by_dReFHpos2;
      dL_by_dImFHpos -= dFC_by_dImFHpos*diffBessTerm;
      d2L_by_dImFHpos2 -= FOSPBessI0*fn::pow2(dFC_by_dImFHpos) + diffBessTerm*d2FC_by_dImFHpos2;
      d2L_by_dReFHpos_dImFHpos -= FOSPBessI0*dFC_by_dReFHpos*dFC_by_dImFHpos + diffBessTerm*d2FC_by_dReFHpos_dImFHpos;
      d2L_by_dReFHpos_dReFHneg -= (expTerm*(2*eBessI0*((-TWOFCSP*ReFHnFOnCospFOn + FOsqrSPFCsqrSN*dFC_by_dReFHneg)*dFC_by_dReFHpos - FCSNSP*d2FC_by_dReFHpos_dReFHneg) + (2*eBessI1FOonFC*((TWOFCSP*ReFHnFOnCospFOn - FOURFCSPSN*dFC_by_dReFHneg)* dFC_by_dReFHpos + FCSNSP*d2FC_by_dReFHpos_dReFHneg))))/SNSP2;
      d2L_by_dReFHpos_dImFHneg -= (expTerm*(2*eBessI0*((-TWOFCSP*ImFHnFOnSinpFOn + FOsqrSPFCsqrSN*dFC_by_dImFHneg)*dFC_by_dReFHpos - FCSNSP*d2FC_by_dReFHpos_dImFHneg) + (2*eBessI1FOonFC*((TWOFCSP*ImFHnFOnSinpFOn - FOURFCSPSN*dFC_by_dImFHneg)* dFC_by_dReFHpos + FCSNSP*d2FC_by_dReFHpos_dImFHneg))))/SNSP2;
      d2L_by_dImFHpos_dReFHneg -= (expTerm*(2*eBessI0*((-TWOFCSP*ReFHnFOnCospFOn + FOsqrSPFCsqrSN*dFC_by_dReFHneg)*dFC_by_dImFHpos - FCSNSP*d2FC_by_dImFHpos_dReFHneg) + (2*eBessI1FOonFC*((TWOFCSP*ReFHnFOnCospFOn - FOURFCSPSN*dFC_by_dReFHneg)* dFC_by_dImFHpos + FCSNSP*d2FC_by_dImFHpos_dReFHneg))))/SNSP2;
      d2L_by_dImFHpos_dImFHneg -= (expTerm*(2*eBessI0*((-TWOFCSP*ImFHnFOnSinpFOn + FOsqrSPFCsqrSN*dFC_by_dImFHneg)*dFC_by_dImFHpos - FCSNSP*d2FC_by_dImFHpos_dImFHneg) + (2*eBessI1FOonFC*((TWOFCSP*ImFHnFOnSinpFOn - FOURFCSPSN*dFC_by_dImFHneg)* dFC_by_dImFHpos + FCSNSP*d2FC_by_dImFHpos_dImFHneg))))/SNSP2;
      d2L_by_dReFHneg_dImFHneg -= (expTerm*(2*SigmaNeg*dFC_by_dImFHneg* (eBessI0*(-TWOFCSP*ReFHnFOnCospFOn + FOsqrSPFCsqrSN*dFC_by_dReFHneg) - (eBessI1*POS.F[r]*(-TWOFCSP*ReFHnFOnCospFOn + FOURFCSPSN*dFC_by_dReFHneg))/absFCpos) + 2*SigmaPos*(2*eBessI0*ImFHnFOnSinpFOn*ReFHnFOnCospFOn*SigmaPos + (-(absFCpos*eBessI0) + eBessI1*POS.F[r])*SigmaNeg* (2*ImFHnFOnSinpFOn*dFC_by_dReFHneg + SigmaNeg*d2FC_by_dReFHneg_dImFHneg))))/SN2SP2;

      floatType TWOexpTermBessIO(expTerm*eBessI0*TWO/SigmaNeg);
      dL_by_dReFHneg -= dFC_by_dReFHneg*diffBessTerm + TWOexpTermBessIO*ReFHnFOnCospFOn;
      d2L_by_dReFHneg2 -= FOSPBessI0*fn::pow2(dFC_by_dReFHneg) + diffBessTerm*d2FC_by_dReFHneg2
                          + (4*dFC_by_dReFHneg*ReFHnFOnCospFOn*diffBessTerm)/SigmaNeg
                          + TWOexpTermBessIO*(2*ReFHnFOnCospFOnSqr-SigmaNeg)/SigmaNeg;

      dL_by_dImFHneg -= dFC_by_dImFHneg*diffBessTerm + TWOexpTermBessIO*ImFHnFOnSinpFOn;
      d2L_by_dImFHneg2 -= FOSPBessI0*fn::pow2(dFC_by_dImFHneg) + diffBessTerm*d2FC_by_dImFHneg2
                          + (4*dFC_by_dImFHneg*ImFHnFOnSinpFOn*diffBessTerm)/SigmaNeg
                          + TWOexpTermBessIO*(2*ImFHnFOnSinpFOnSqr-SigmaNeg)/SigmaNeg;

      }

      if (sigmaa)
      {
      floatType SANUM(ImFHnFOnSinpFOn*FHterm1 + ReFHnFOnCospFOn*FHterm2);
      floatType SBNUM(ReFHnFOnCospFOn*FHterm1 - ImFHnFOnSinpFOn*FHterm2);
      floatType dFC_by_dSA = SANUM/sqroot;
      floatType d2FC_by_dSA2 = -fn::pow2(SANUM)/fn::pow3(sqroot) + ReFHImFHterm;
      floatType dFC_by_dSB = SBNUM/sqroot;
      floatType d2FC_by_dSB2 = -fn::pow2(SBNUM)/fn::pow3(sqroot) + ReFHImFHterm;

      dL_by_dSA   -= dFC_by_dSA*diffBessTerm;
      d2L_by_dSA2 -= FOSPBessI0*fn::pow2(dFC_by_dSA) + (d2FC_by_dSA2*diffBessTerm);
      dL_by_dSB   -= dFC_by_dSB*diffBessTerm;
      d2L_by_dSB2 -= FOSPBessI0*fn::pow2(dFC_by_dSB) + (d2FC_by_dSB2*diffBessTerm);

      floatType BessExpm1(BessArg+expFOpos-1);
      dL_by_dSP -= expTerm*(BessExpm1*eBessI0 - BessArg*eBessI1)/SigmaPos;
      d2L_by_dSP2 -= expTerm*(eBessI0*(fn::pow2(BessExpm1) +fn::pow2(BessArg-1) -2*expFOpos) - BessArg*eBessI1*(2*BessExpm1-1))/SNSP3;

  //could be simplified more?
      floatType CosSinSqr = (ReFHnFOnCospFOnSqr + ImFHnFOnSinpFOnSqr)/SigmaNeg;
      dL_by_dSDsqr   -= expTerm*(CosSinSqr*eBessI0-eBessI0)/SigmaNeg;
   //with (1/SNSP) term
   //   dL_by_dSDsqr   -= expTerm*(CosSinSqr*eBessI0-eBessI0)/SN2SP;
      d2L_by_dSDsqr2 -= ((2 - 4*CosSinSqr + fn::pow2(CosSinSqr))*std::exp(-CosSinSqr - expFOpos + expArgOffset)*eBessI0)/SN3SP;
      }

  #ifdef __FD_HESS_ACENT__
      //For testing against mathematica with GAUSS 1 (phase = 0 only)
      if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
      std::cout << "p = " << acentPhsIntg[i].Ang[p] << std::endl <<
                  " expFOpos " << expFOpos  <<
                  " expTerm " << expTerm  <<
                  " BessArg " << BessArg  << std::endl <<
                  " pLike " << ONEonSPSN*expTerm*eBessI0 <<
                  " Like " << expTerm*eBessI0 <<
                  " first a= " << dL_by_dtest_a  << " b= " << dL_by_dtest_b <<
                  " second " << d2L_by_dtest2  <<std::endl;
  #endif
    } //loop over p
    if (Like > 0.) break;
    expArgOffset = maxArgAllow - topExpArg;
  } // loop to apply new offset if necessary
  assert(Like > 0); // Could probably eliminate now
  floatType LikeFactor(1.0/Like);
  assert(pLike > 0);
  floatType pLikeFactor(1.0/pLike);//before weighting, as derivatives are unweighted

  //Overall scaling factors
  Like *= acentPhsIntg[i].Weight;
  Like *= TWO*POS.F[r]*NEG.F[r]/(scitbx::constants::pi*SigmaPos*SigmaNeg);

  LogLike = -log(Like) + expArgOffset; //Remove offset
  floatType epssqr = fn::pow2(epsn[r]);

  d2L_by_dReFHpos2         = fn::pow2(dL_by_dReFHpos*LikeFactor) + d2L_by_dReFHpos2*LikeFactor;
  d2L_by_dReFHpos_dImFHpos = (dL_by_dReFHpos*LikeFactor)*(dL_by_dImFHpos*LikeFactor) + d2L_by_dReFHpos_dImFHpos*LikeFactor;
  d2L_by_dReFHpos_dReFHneg = (dL_by_dReFHpos*LikeFactor)*(dL_by_dReFHneg*LikeFactor) + d2L_by_dReFHpos_dReFHneg*LikeFactor;
  d2L_by_dReFHpos_dImFHneg = (dL_by_dReFHpos*LikeFactor)*(dL_by_dImFHneg*LikeFactor) + d2L_by_dReFHpos_dImFHneg*LikeFactor;
  d2L_by_dImFHpos2         = fn::pow2(dL_by_dImFHpos*LikeFactor) + d2L_by_dImFHpos2*LikeFactor;
  d2L_by_dImFHpos_dReFHneg = (dL_by_dImFHpos*LikeFactor)*(dL_by_dReFHneg*LikeFactor) + d2L_by_dImFHpos_dReFHneg*LikeFactor;
  d2L_by_dImFHpos_dImFHneg = (dL_by_dImFHpos*LikeFactor)*(dL_by_dImFHneg*LikeFactor) + d2L_by_dImFHpos_dImFHneg*LikeFactor;
  d2L_by_dReFHneg2         = fn::pow2(dL_by_dReFHneg*LikeFactor) + d2L_by_dReFHneg2*LikeFactor;
  d2L_by_dReFHneg_dImFHneg = (dL_by_dReFHneg*LikeFactor)*(dL_by_dImFHneg*LikeFactor) + d2L_by_dReFHneg_dImFHneg*LikeFactor;
  d2L_by_dImFHneg2         = fn::pow2(dL_by_dImFHneg*LikeFactor) + d2L_by_dImFHneg2*LikeFactor;
  d2L_by_dSA2      = fn::pow2(dL_by_dSA*LikeFactor) + d2L_by_dSA2*LikeFactor;
  d2L_by_dSB2      = fn::pow2(dL_by_dSB*LikeFactor) + d2L_by_dSB2*LikeFactor;
  d2L_by_dSP2      = epssqr*(fn::pow2(dL_by_dSP*LikeFactor) + d2L_by_dSP2*pLikeFactor);
  d2L_by_dSDsqr2   = epssqr*(fn::pow2(dL_by_dSDsqr*LikeFactor) + d2L_by_dSDsqr2*pLikeFactor);

  //now correct gradients as these are also required for the chain rule later
  dL_by_dReFHpos *= LikeFactor;
  dL_by_dImFHpos *= LikeFactor;
  dL_by_dReFHneg *= LikeFactor;
  dL_by_dImFHneg *= LikeFactor;
  dL_by_dSDsqr   *= epsn[r]*LikeFactor;
  dL_by_dSA      *= LikeFactor;
  dL_by_dSB      *= LikeFactor;
  dL_by_dSP      *= epsn[r]*LikeFactor;

#ifdef __FD_HESS_ACENT__
  std::cout << "=== Second Derivative test acentricReflHessSAD2 (r=" << r << ") ===\n";
  std::cout <<"on function " << d2L_by_dtest2 << " fd " << FD_test_on_f << " ratio " << d2L_by_dtest2/FD_test_on_f << "\n";
  if (r >= std::atof(getenv("PHASER_TEST_NREFL")) || std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
//NOTE values can be nan or 0 or inf if not enough GAUSS points are included
#endif

  return LogLike;
}

}//phaser
