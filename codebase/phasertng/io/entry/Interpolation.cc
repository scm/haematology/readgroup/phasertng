//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Interpolation.h>
#include <phasertng/io/entry/Ensemble.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/hoist.h>
#include <phasertng/io/MtzHandler.h>
#include <cstring>
#include <array>

#define PHASERTNG_FAST_NO_CHECKS
//#define PHASERTNG_DEBUG_BUFFER

namespace phasertng {

  Interpolation::Interpolation(af_string histlines,std::string hall,af::double6 cell,
                 af_double SIGMAA_,af_cmplex MAPE_) :
    SIGMAA(SIGMAA_),
    EMAP(MAPE_)
  {
    SetHistory(histlines);
    UC = UnitCell(cell);
    SG = SpaceGroup(hall);
    set_four_point();
    PHASER_ASSERT(EMAP.size());
    PHASER_ASSERT(SIGMAA.size());
  }

  cmplex
  Interpolation::getE(int h,int k,int l)
  {
#ifdef PHASERTNG_DEBUG_BUFFER
    int hh(h),kk(k),ll(l);
#endif
    bool pos_l(true);
    if (l < 0 or (l == 0 and k < 0) or (l == 0 and k == 0 and h < 0))
      {pos_l = false; h *= -1; k *= -1; l *= -1; }
    if (h < 0) h += sizeh;
    if (k < 0) k += sizek;
    int buf_int = ((h*sizek)+k)*sizel+l;
    //int i = ((h*sizek)+k)*sizel+l;
    //buf_int = (index[i] > 0) ? index[i] : -index[i];
#ifdef PHASERTNG_DEBUG_BUFFER
    miller::index<int> hkl(hh,kk,ll);
    double reso = UC.cctbxUC.d(hkl);
    if (buf_int < 0 or buf_int >= EMAP.size())
    { //can sort by resol
      std::cout << "debug: reso= " << reso << " hkl=(" << hh << "," << kk << "," << ll << ") " << " hkl'=(" << h << "," << k << "," << l << ") buf_int=" << buf_int << " [>=0 <" << EMAP.size() << "] limit=(" << sizeh << "," << sizek << "," << sizel << ") pos_l=" << btos(pos_l) << " UC " << UC.A() << " " << UC.B() << " " << UC.C() << " hires=" << BIN.HiRes(BIN.numbins()-1) << std::endl;
    }
    bool continue_regardless(true);
    if (continue_regardless)
    {
      if (buf_int < 0) return {0,0};
      if (buf_int >= EMAP.size()) return {0,0};
    }
   // else if (buf_int < 0 or buf_int >= EMAP.size()) { throw Error(err::FATAL,"Catch me"); }
    if (buf_int < 0 or buf_int >= EMAP.size())
    {
      bool interpolation_in_range(false);
      PHASER_ASSERT(interpolation_in_range);
    }
#endif
#ifndef PHASERTNG_FAST_NO_CHECKS
    if (buf_int < 0 or buf_int >= EMAP.size())
    {
      //this is a race condition when the interpolation is being called in parallel
      //cannot add warnings to a vector in a push_back list
      //mutex in the class does not compile
      Warnings = "Interpolation range error";
      return {0,0};
    }
    PHASER_ASSERT(buf_int >= 0);
    PHASER_ASSERT(buf_int < EMAP.size());
#endif
    cmplex buf_cmplex = EMAP[buf_int];
    if(!pos_l) buf_cmplex = std::conj(buf_cmplex);
#ifdef PHASERTNG_DEBUG_BUFFER
    //std::cout << "debug: hkl=(" << hh << "," << kk << "," << ll << ") reso=" << reso << " buf_int=" << buf_int << std::endl;
#endif
    return buf_cmplex;
  //the time here is taken loading the relevant section of the map into memory
  }

  ivect3
  Interpolation::getHKL(int h,int k,int l)
  {
    if (h > sizeh/2) h -=sizeh;
    if (k > sizek/2) k -=sizek;
  //bool pos_l(true);
    if (l < 0 or (l == 0 and k < 0) or (l == 0 and k == 0 and h < 0))
      { //pos_l = false;
        h *= -1; k *= -1; l *= -1; }
    return ivect3(h,k,l);
  }

  void
  Interpolation::ReadMtz()
  {
    af::shared< std::tuple<millnx,cmplex,double,int> > mtzdata = Interpolation::VanillaReadMtz();
    int hmax(0),kmax(0),lmax(0),maxbin(0);
    af_double twostol(mtzdata.size());
    for (int r = 0; r < mtzdata.size(); r++)
    {
      millnx MILLER; int ibin;
      std::tie(MILLER,std::ignore,std::ignore,ibin) = mtzdata[r];
      hmax = std::max(hmax,abs(MILLER[0]));
      kmax = std::max(kmax,abs(MILLER[1]));
      lmax = std::max(lmax,abs(MILLER[2]));
      twostol[r] = UC.cctbxUC.two_stol(MILLER);
      maxbin = std::max(maxbin,ibin);
    }
    //sizes input with VanillaReadMtz
    PHASER_ASSERT(sizeh == 2*hmax+1);
    PHASER_ASSERT(sizek == 2*kmax+1);
    PHASER_ASSERT(sizel == lmax+1); //no anomalous
    int nrefl = 0;
    EMAP = af_cmplex(sizeh*sizek*sizel,{0.,0.});
    SIGMAA.resize(maxbin+1);
    BIN.NUMINBIN.resize(maxbin+1);
    std::fill(BIN.NUMINBIN.begin(),BIN.NUMINBIN.end(),0);
    BIN.RBIN.clear(); //not relevant
    EMAP[0] = { 0., 0. }; //will cause divide by zero if called
    nrefl++; //hkl(0,0,0) above
    for (int r = 0; r < mtzdata.size(); r++)
    {
      millnx miller; cmplex ECALC; double Siga; int ibin;
      std::tie(miller,ECALC,Siga,ibin) = mtzdata[r];
      SIGMAA[ibin] = Siga; //keeps overwriting
      { //set ECALC
        bool pos_l(true);
        int h = miller[0];
        int k = miller[1];
        int l = miller[2];
        if (l < 0 or (l == 0 and k < 0) or (l == 0 and k == 0 and h < 0))
          {pos_l = false; h *= -1; k *= -1; l *= -1; }
        if (h < 0) h += sizeh;
        if (k < 0) k += sizek;
        //PHASER_ASSERT((((h*sizek)+k)*sizel+l) < index.size());
        //index[((h*sizek)+k)*sizel+l] = 0; //should be 0 already from resizeMaps
       // PHASER_ASSERT(index[((h*sizek)+k)*sizel+l] == 0); //one user ended up getting this - instability?
        //store whether Friedel mate was stored
        PHASER_ASSERT((((h*sizek)+k)*sizel+l) < EMAP.size());
        EMAP[((h*sizek)+k)*sizel+l] = pos_l ? ECALC : std::conj(ECALC);
        //if (!pos_l) e = std::conj(e);
        //EMAP.push_back(std::complex<mapFloat>(e));
        nrefl++;
        BIN.NUMINBIN[ibin]++; //set_numinbin relies on RBIN, which is not relevant
      }
    }
    BIN.store_ssqr_range(twostol,maxbin+1);
    PHASER_ASSERT(NREFL == nrefl);
    PHASER_ASSERT(EMAP.size());
  }

  void
  Interpolation::WriteMtz()
  {
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(REFLID.is_set_valid());

    //nref must be set before columns are added as this defines their size
    int nrefl(0);
    for (int h = 0; h < sizeh; h++)
    for (int k = 0; k < sizek; k++)
    for (int l = 0; l < sizel; l++)
    { if (std::abs(EMAP[((h*sizek)+k)*sizel+l]) != 0) { nrefl++; } }

    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,0.0,nrefl,get_history());
    CMtz::MTZCOL* mtzE  = mtzOut.addCol("EC",'F');
    CMtz::MTZCOL* mtzP  = mtzOut.addCol("PHIC",'P');
    CMtz::MTZCOL* mtzV  = mtzOut.addCol("SIGA",'W');
    CMtz::MTZCOL* mtzB  = mtzOut.addCol("BIN",'I');

    int r = 0;
    for (int h = 0; h < sizeh; h++)
    for (int k = 0; k < sizek; k++)
    for (int l = 0; l < sizel; l++)
    {
      if (std::abs(EMAP[((h*sizek)+k)*sizel+l]) != 0)
      {
        ivect3 hkl(getHKL(h,k,l));
        miller::index<int> millerhkl(hkl);
        cmplex ECALC(getE(h,k,l));
        double rssqr = UC.cctbxUC.d_star_sq(millerhkl);
        int ibin = BIN.get_bin_ssqr(rssqr);
        mtzOut.mtzH->ref[r] = hkl[0];
        mtzOut.mtzK->ref[r] = hkl[1];
        mtzOut.mtzL->ref[r] = hkl[2];
        mtzE->ref[r] = std::abs(ECALC);
        mtzP->ref[r] = scitbx::rad_as_deg(std::arg(ECALC));
        mtzV->ref[r] = SIGMAA[ibin];
        mtzB->ref[r] = ibin;
        r++;
      }
    }
    mtzOut.Put("Phasertng Interpolation");
  }

  af_string
  Interpolation::get_history()
  {
    af_string HISTLABINLINE;
    //will be written in reverse order
    HISTLABINLINE.push_back("NREFL " + itos(NREFL) + " " + itos(sizeh) + " " + itos(sizek) + " " + itos(sizel));
    HISTLABINLINE.push_back("BINS " + BIN.unparse());
    HISTLABINLINE.push_back("MODLID " + MODLID.string() + " " + MODLID.tag());
    HISTLABINLINE.push_back("REFLID " + REFLID.string() + " " + REFLID.tag());
    return HISTLABINLINE;
  }

  void
  Interpolation::SetHistory(af_string histlines)
  {
    for (auto line : histlines)
    {
      sv_string token;
      hoist::algorithm::split(token,line," ");
      if (!line.find("MODLID"))
      {
        int i = 1; //after MODLID
        assert(i < token.size());
        std::string number = token[i++];
        assert(i < token.size());
        std::string tag = token[i++];
        MODLID.parse_string(number,tag);
      }
      if (!line.find("REFLID"))
      {
        int i = 1; //after REFLID
        assert(i < token.size());
        std::string number = token[i++];
        assert(i < token.size());
        std::string tag = token[i++];
        REFLID.parse_string(number,tag);
      }
      if (!line.find("BINS"))
      {
        assert(token.size() == 5);
        BIN.parse(std::stod(token[1]),
                  std::stod(token[2]),
                  std::stod(token[3]),
                  std::atoi(token[4].c_str()));
      }
      if (!line.find("NREFL"))
      {
        int i = 1; //after MODLID
        assert(i < token.size());
        NREFL = std::atoi(token[i++].c_str());
        assert(i < token.size());
        sizeh = std::atoi(token[i++].c_str());
        assert(i < token.size());
        sizek = std::atoi(token[i++].c_str());
        assert(i < token.size());
        sizel = std::atoi(token[i++].c_str());
      }
    }
  }

  af_string
  Interpolation::logInterp(int nprint)
  {
    int sampling = nprint ? NREFL/nprint  : 0;
    af_string output;
    output.push_back("Nrefl: "+itos(NREFL));
    output.push_back("Gridding: "+itos(sizeh)+" "+itos(sizek)+" "+itos(sizel));
    int r = 0;
    for (int h = 0; h < sizeh; h++)
    for (int k = 0; k < sizek; k++)
    for (int l = 0; l < sizel; l++)
    {
      if (std::abs(EMAP[((h*sizek)+k)*sizel+l]) != 0)
      {
        ivect3 hkl(getHKL(h,k,l));
        miller::index<int> millerhkl(hkl);
        cmplex ECALC(getE(h,k,l));
        double rssqr = UC.cctbxUC.d_star_sq(millerhkl);
        int ibin = BIN.get_bin_ssqr(rssqr);
        if (sampling and r%sampling == 0)
          output.push_back(snprintftos("#%-9d % 3i % 3i % 3i   % 12.4f % 12.4f %6.4f %d",
            r+1,
            hkl[0],
            hkl[1],
            hkl[2],
            std::abs(ECALC),
            scitbx::rad_as_deg(std::arg(ECALC)),
            SIGMAA[ibin],
            ibin));
        r++;
      }
    }
    return output;
  }

  af::shared< std::tuple<millnx,cmplex,double,int> >
  Interpolation::VanillaReadMtz()
  {
    const std::string HKLIN = FileSystem::fstr();
    if (!HKLIN.size())
    throw Error(err::INPUT,"No reflection file for reading");
    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(HKLIN,read_reflections);
    //MILLER.resize(mtzIn.NREFL); //so that setup_data_present_init sets array sizes
    { //SPACEGROUP
      SG = mtzIn.SG;
    } //SPACEGROUP
    { //HISTORY
      //read the history and setup up parsing of cards to see what is there already
      //read what is in file and see if it agrees with values about to be written
      SetHistory(mtzIn.HISTORY);
    } //HISTORY
    { //UNITCELL
      mtzIn.load_dataset("EC");
      UC = mtzIn.UC;
    } //UNITCELL
    PHASER_ASSERT(mtzIn.NREFL != 0);
    af::shared< std::tuple<millnx,cmplex,double,int> >  mtzdata(mtzIn.NREFL);
    { //read mtz file
      //reset position in file
      mtzIn.resetRefl();
      sv_string col = {"H","K","L","EC","PHIC","SIGA","BIN"};
      int ncols = col.size();
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      int n(0);
      for (int i = 0; i < col.size(); i++)
      {
        lookup[n++] = mtzIn.getCol(col[i]);
      }
      for (int mtzr = 0; mtzr < mtzIn.NREFL ; mtzr++)
      {
        int ifail = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!ifail);
        const int H = static_cast<int>(adata[0]);
        const int K = static_cast<int>(adata[1]);
        const int L = static_cast<int>(adata[2]);
        millnx MILLER(H,K,L);
        cmplex E = std::polar(static_cast<double>(adata[3]),
                              scitbx::deg_as_rad(static_cast<double>(adata[4])));
        double SIGA = adata[5];
        int ibin = static_cast<int>(adata[6]);
        mtzdata[mtzr] = std::make_tuple( MILLER, E, SIGA, ibin );
      }
    }
    return mtzdata;
  }

  std::tuple<cmplex, cvect3, cmat33>
  Interpolation::errorTGH(
      const dvect3& offGrid,
      const bool do_gradient,
      const bool do_hessian)
  {
    throw Error(err::DEVELOPER,"Do not call refinement with four point interpolation");
  }

  std::tuple<cmplex, cvect3, cmat33>
  Interpolation::linearTGH(
      const dvect3& offGrid,
      const bool do_gradient,
      const bool do_hessian)
  {
    cmplex Target;
    cvect3 Gradient;
    cmat33 Hessian;
    int H = floor_double2int(offGrid[0]);
    int K = floor_double2int(offGrid[1]);
    int L = floor_double2int(offGrid[2]);

    //truncate all coordinates to lower floor of box
    double fracH = offGrid[0]-H;
    double fracK = offGrid[1]-K;
    double fracL = offGrid[2]-L;
    double invsH = 1.0-fracH;
    double invsK = 1.0-fracK;
    double invsL = 1.0-fracL;
    std::array<cmplex,8> cube;
    cube[H0K0L0] = getE(H,  K,  L  );
    cube[H1K0L0] = getE(H+1,K,  L  );
    cube[H0K1L0] = getE(H,  K+1,L  );
    cube[H1K1L0] = getE(H+1,K+1,L  );
    cube[H0K0L1] = getE(H,  K,  L+1);
    cube[H1K0L1] = getE(H+1,K,  L+1);
    cube[H0K1L1] = getE(H,  K+1,L+1);
    cube[H1K1L1] = getE(H+1,K+1,L+1);
    Target=  invsL*(invsK*(invsH*cube[H0K0L0]+fracH*cube[H1K0L0])
            +fracK*(invsH*cube[H0K1L0]+fracH*cube[H1K1L0]))
            +fracL*(invsK*(invsH*cube[H0K0L1]+fracH*cube[H1K0L1])
            +fracK*(invsH*cube[H0K1L1]+fracH*cube[H1K1L1]));
    if (do_gradient)
    {
      Gradient[0]=invsL*(invsK*(-cube[H0K0L0]+cube[H1K0L0])+
                         fracK*(-cube[H0K1L0]+cube[H1K1L0]))+
                  fracL*(invsK*(-cube[H0K0L1]+cube[H1K0L1])+
                         fracK*(-cube[H0K1L1]+cube[H1K1L1]));
      Gradient[1]=invsL*(-1.0*(invsH*cube[H0K0L0]+fracH*cube[H1K0L0])+
                              (invsH*cube[H0K1L0]+fracH*cube[H1K1L0]))+
                  fracL*(-1.0*(invsH*cube[H0K0L1]+fracH*cube[H1K0L1])+
                              (invsH*cube[H0K1L1]+fracH*cube[H1K1L1]));
      Gradient[2]=-1.0*(invsK*(invsH*cube[H0K0L0]+fracH*cube[H1K0L0])+
                        fracK*(invsH*cube[H0K1L0]+fracH*cube[H1K1L0]))+
                       (invsK*(invsH*cube[H0K0L1]+fracH*cube[H1K0L1])+
                        fracK*(invsH*cube[H0K1L1]+fracH*cube[H1K1L1]));
    }
    if (do_hessian)
    {
      // Extend outside box (towards lower resolution, i.e. within resolution of molecular transform) to get
      // diagonal curvature terms from finite differences
      cmplex foutside,TWO(2.,0.);
      if (H >= 0)
      {
        foutside = getE(H-1,K,L);
        Hessian(0,0) = (foutside - TWO*cube[H0K0L0] + cube[H1K0L0]);
      }
      else
      {
        foutside = getE(H+2,K,L);
        Hessian(0,0) = (cube[H0K0L0] - TWO*cube[H1K0L0] + foutside);
      }
      if (K >= 0)
      {
        foutside = getE(H,K-1,L);
        Hessian(1,1) = (foutside - TWO*cube[H0K0L0] + cube[H0K1L0]);
      }
      else
      {
        foutside = getE(H,K+2,L);
        Hessian(1,1) = (cube[H0K0L0] - TWO*cube[H0K1L0] + foutside);
      }
      if (L >= 0)
      {
        foutside = getE(H,K,L-1);
        Hessian(2,2) = (foutside - TWO*cube[H0K0L0] + cube[H0K0L1]);
      }
      else
      {
        foutside = getE(H,K,L+2);
        Hessian(2,2) = (cube[H0K0L0] - TWO*cube[H0K0L1] + foutside);
      }
  -
      // Off-diagonal Hessian terms are analytic derivatives of gradients above
      Hessian(0,1)=  invsL * (cube[H0K0L0] - cube[H1K0L0] - cube[H0K1L0] + cube[H1K1L0]) +
                     fracL * (cube[H0K0L1] - cube[H1K0L1] - cube[H0K1L1] + cube[H1K1L1]);
      Hessian(0,2)=  invsL * (cube[H0K0L0] - cube[H1K0L0] - cube[H0K0L1] + cube[H1K0L1]) +
                     fracL * (cube[H0K1L0] - cube[H1K1L0] - cube[H0K1L1] + cube[H1K1L1]);
      Hessian(1,2)=  invsL * (cube[H0K0L0] - cube[H0K1L0] - cube[H0K0L1] + cube[H0K1L1]) +
                     fracL * (cube[H1K0L0] - cube[H1K1L0] - cube[H1K0L1] + cube[H1K1L1]);
      Hessian(1,0) = Hessian(0,1);
      Hessian(2,0) = Hessian(0,2);
      Hessian(2,1) = Hessian(1,2);
    }
    return std::make_tuple(Target, Gradient, Hessian);
  }

  std::tuple<cmplex, cvect3, cmat33>
  Interpolation::tricubicTGH(
      const dvect3& offGrid,
      const bool do_gradient,
      const bool do_hessian)
  {
    const double& x = offGrid[0];
    const double& y = offGrid[1];
    const double& z = offGrid[2];
    cmplex Target;
    cvect3 Gradient;
    cmat33 Hessian;
    Target = TricubicXYZ(x,y,z);
    if (do_gradient)
    {
      // Interpolated values at neighboring points
      cmplex f_x_plus = TricubicXYZ(x + 1, y, z);
      cmplex f_x_minus = TricubicXYZ( x - 1, y, z);
      cmplex f_y_plus = TricubicXYZ(x, y + 1, z);
      cmplex f_y_minus = TricubicXYZ(x, y - 1, z);
      cmplex f_z_plus = TricubicXYZ(x, y, z + 1);
      cmplex f_z_minus = TricubicXYZ(x, y, z - 1);

      // Compute the first derivatives using central differences
      cmplex dfdx = (f_x_plus - f_x_minus) / 2.;
      cmplex dfdy = (f_y_plus - f_y_minus) / 2.;
      cmplex dfdz = (f_z_plus - f_z_minus) / 2.;

      Gradient = {dfdx, dfdy, dfdz};
    }
    if (do_hessian)
    {
      // Central second partial derivatives (diagonals of the Hessian)
      cmplex f_x_plus = TricubicXYZ(x + 1, y, z);
      cmplex f_x_minus = TricubicXYZ(x - 1, y, z);
      cmplex f_y_plus = TricubicXYZ(x, y + 1, z);
      cmplex f_y_minus = TricubicXYZ(x, y - 1, z);
      cmplex f_z_plus = TricubicXYZ(x, y, z + 1);
      cmplex f_z_minus = TricubicXYZ(x, y, z - 1);
      cmplex f_center = TricubicXYZ(x, y, z);

      cmplex d2fdx2 = (f_x_plus - 2. * f_center + f_x_minus) / 1.;
      cmplex d2fdy2 = (f_y_plus - 2. * f_center + f_y_minus) / 1.;
      cmplex d2fdz2 = (f_z_plus - 2. * f_center + f_z_minus) / 1.;

      // Mixed second partial derivatives (off-diagonals of the Hessian)
      cmplex f_xy_plus_plus = TricubicXYZ(x + 1, y + 1, z);
      cmplex f_xy_plus_minus = TricubicXYZ(x + 1, y - 1, z);
      cmplex f_xy_minus_plus = TricubicXYZ(x - 1, y + 1, z);
      cmplex f_xy_minus_minus = TricubicXYZ(x - 1, y - 1, z);

      cmplex d2fdxdy = (f_xy_plus_plus - f_xy_plus_minus - f_xy_minus_plus + f_xy_minus_minus) / 4.;

      cmplex f_xz_plus_plus = TricubicXYZ(x + 1, y, z + 1);
      cmplex f_xz_plus_minus = TricubicXYZ(x + 1, y, z - 1);
      cmplex f_xz_minus_plus = TricubicXYZ(x - 1, y, z + 1);
      cmplex f_xz_minus_minus = TricubicXYZ(x - 1, y, z - 1);

      cmplex d2fdxdz = (f_xz_plus_plus - f_xz_plus_minus - f_xz_minus_plus + f_xz_minus_minus) / 4.;

      cmplex f_yz_plus_plus = TricubicXYZ(x, y + 1, z + 1);
      cmplex f_yz_plus_minus = TricubicXYZ(x, y + 1, z - 1);
      cmplex f_yz_minus_plus = TricubicXYZ(x, y - 1, z + 1);
      cmplex f_yz_minus_minus = TricubicXYZ(x, y - 1, z - 1);

      cmplex d2fdydz = (f_yz_plus_plus - f_yz_plus_minus - f_yz_minus_plus + f_yz_minus_minus) / 4.;

      // Return the full 3x3 Hessian matrix
      Hessian = { d2fdx2, d2fdxdy, d2fdxdz ,
                  d2fdxdy, d2fdy2, d2fdydz ,
                  d2fdxdz, d2fdydz, d2fdz2};
    }
    return std::make_tuple(Target, Gradient, Hessian);
  }

  cmplex
  Interpolation::EightPt(const dvect3& offGrid)
  {
    cmplex Target;
    //truncate all coordinates to lower floor of box
    int H = floor_double2int(offGrid[0]);
    int K = floor_double2int(offGrid[1]);
    int L = floor_double2int(offGrid[2]);
    double fracH = offGrid[0]-H;
    double fracK = offGrid[1]-K;
    double fracL = offGrid[2]-L;
    double invsH = 1.0-fracH;
    double invsK = 1.0-fracK;
    double invsL = 1.0-fracL;
    Target=  invsL*(invsK*(invsH*getE(H,  K,  L  )+fracH*getE(H+1,K,  L  ))
            +fracK*(invsH*getE(H,  K+1,L  )+fracH*getE(H+1,K+1,L  )))
            +fracL*(invsK*(invsH*getE(H,  K,  L+1)+fracH*getE(H+1,K,  L+1))
            +fracK*(invsH*getE(H,  K+1,L+1)+fracH*getE(H+1,K+1,L+1)));
    return Target;
  }

  cmplex
  Interpolation::FourPt(const dvect3& offGrid)
  {
    cmplex Target;
    //truncate all coordinates to lower floor of box
    int H = floor_double2int(offGrid[0]);
    int K = floor_double2int(offGrid[1]);
    int L = floor_double2int(offGrid[2]);
    double fracH = offGrid[0]-H;
    double fracK = offGrid[1]-K;
    double fracL = offGrid[2]-L;
    cmplex near(getE(H,K,L));
    Target =  near;
    Target += (fracH >= 0) ? fracH * (getE(H+1,K,L) - near) : fracH * (near - getE(H-1,K,L));
    Target += (fracK >= 0) ? fracK * (getE(H,K+1,L) - near) : fracK * (near - getE(H,K-1,L));
    Target += (fracL >= 0) ? fracL * (getE(H,K,L+1) - near) : fracL * (near - getE(H,K,L-1));
    return Target;
  }


  cmplex
  Interpolation::ConstLTD(const dvect3& offGrid) const
  {
    cmplex A(0,0);
    for (int ltd = 0; ltd < DISORDER.size(); ltd++)
    {
      double dotProduct = offGrid*DISORDER[ltd].first; //one of which is 0 0 0, in fractional
      cmplex phsft = std::exp(cmplex(0.,scitbx::constants::two_pi*dotProduct));
      A += DISORDER[ltd].second*phsft;
    }
    return A;
  }

  cmplex
  Interpolation::FourPtLTD(const dvect3& offGrid)
  { return ConstLTD(offGrid)*FourPt(offGrid); }

  cmplex
  Interpolation::EightPtLTD(const dvect3& offGrid)
  { return ConstLTD(offGrid)*EightPt(offGrid); }

  cmplex
  Interpolation::TricubicPtLTD(const dvect3& offGrid)
  { return ConstLTD(offGrid)*TricubicPt(offGrid); }

  std::tuple<cmplex, cvect3, cmat33>
  Interpolation::linearTGHLTD(
      const dvect3& offGrid,
      const bool do_gradient,
      const bool do_hessian)
  {
    cmplex Target; cvect3 Gradient; cmat33 Hessian;
    std::tie(Target,Gradient,Hessian) = linearTGH(offGrid, do_gradient, do_hessian);
    auto A = ConstLTD(offGrid);
    return std::make_tuple(A*Target, A*Gradient, A*Hessian);
  }

  std::tuple<cmplex, cvect3, cmat33>
  Interpolation::tricubicTGHLTD(
      const dvect3& offGrid,
      const bool do_gradient,
      const bool do_hessian)
  {
    cmplex Target; cvect3 Gradient; cmat33 Hessian;
    std::tie(Target,Gradient,Hessian) = tricubicTGH(offGrid, do_gradient, do_hessian);
    auto A = ConstLTD(offGrid);
    return std::make_tuple(A*Target, A*Gradient, A*Hessian);
  }

  void
  Interpolation::ParseEnsemble(Ensemble& ENSEMBLE)
  {
    MODLID = ENSEMBLE.MODLID; //base class Interpolate
    REFLID = ENSEMBLE.REFLID; //base class Interpolate
    UC = ENSEMBLE.UC; //base class Interpolate
    SG = SpaceGroup("P 1"); //base class Interpolate

    phaser_assert(ENSEMBLE.BIN.numbins());
    phaser_assert(ENSEMBLE.MILLER.size());
    phaser_assert(ENSEMBLE.SIGMAA.size());
    phaser_assert(ENSEMBLE.ECALC.size());
    int hmax(0),kmax(0),lmax(0);
    for (auto& MILLER : ENSEMBLE.MILLER)
    {
      hmax = std::max(hmax,std::abs(MILLER[0]));
      kmax = std::max(kmax,std::abs(MILLER[1]));
      lmax = std::max(lmax,std::abs(MILLER[2]));
    }
    sizeh = 2*hmax+1;
    sizek = 2*kmax+1;
    sizel = lmax+1; //no anomalous
    NREFL = 0;
    EMAP = af_cmplex(sizeh*sizek*sizel,{0.,0.});
    EMAP[0] = { 0., 0. }; //will cause divide by zero if called
    NREFL++; //so first reflection below is non-zero
    BIN = ENSEMBLE.BIN;
    SIGMAA = ENSEMBLE.SIGMAA.deep_copy();
    for (int r = 0; r < ENSEMBLE.MILLER.size(); r++)
    {
      millnx MILLER = ENSEMBLE.MILLER[r];
      phaser_assert(r < ENSEMBLE.ECALC.size());
      cmplex ECALC = ENSEMBLE.ECALC[r];
      { //set ECALC
        bool pos_l(true);
        int h = MILLER[0];
        int k = MILLER[1];
        int l = MILLER[2];
        if (l < 0 or (l == 0 and k < 0) or (l == 0 and k == 0 and h < 0))
          {pos_l = false; h *= -1; k *= -1; l *= -1; }
        if (h < 0) h += sizeh;
        if (k < 0) k += sizek;
        int buf_int = ((h*sizek)+k)*sizel+l;
        PHASER_ASSERT(buf_int < EMAP.size());
        PHASER_ASSERT(buf_int >= 0);
        EMAP[buf_int] = pos_l ? ECALC : std::conj(ECALC);
        NREFL++;
      }
    }
    phaser_assert(in_memory());
  }

  void
  Interpolation::setup_sigmaa_sqr(af_double ssqr_array)
  {
    int nrefl = ssqr_array.size();
    rSigaSqr.resize(nrefl,0);
    for (int r = 0; r < ssqr_array.size(); r++)
    {
      rSigaSqr[r] = 0;
      double ssqr = ssqr_array[r];
      if (!BIN.ssqr_in_range(ssqr)) continue;
      int ibin = BIN.get_bin_ssqr(ssqr); //expensive
      double Siga = SIGMAA.at(ibin);
      rSigaSqr[r] = fn::pow2(Siga);
    }
  }

  cmplex Interpolation::monocubic(cmplex p[4] ,double x)
  {
    return p[1]+0.5*x*(p[2]-p[0]+x*(2.0*p[0]-5.0*p[1]+4.0*p[2]-p[3]+x*(3.0*(p[1]-p[2])+p[3]-p[0])));
  }

  cmplex Interpolation::bicubic(cmplex p[4][4],double x,double y)
  {
    cmplex arr[4];
    for (int i = 0; i < 4.; i++) { arr[i] = monocubic(p[i], y); }
    return monocubic(arr, x);
  }

  cmplex Interpolation::tricubic(cmplex p[4][4][4], double x, double y, double z)
  {
    cmplex arr[4];
    for (int i = 0; i < 4.; i++) { arr[i] = bicubic(p[i], y, z); }
    return monocubic(arr, x);
  }

  cmplex
  Interpolation::TricubicPt(const dvect3& offGrid)
  {
    const double& x = offGrid[0];
    const double& y = offGrid[1];
    const double& z = offGrid[2];
    return TricubicXYZ(x,y,z);
  }

  cmplex
  Interpolation::TricubicXYZ(double x, double y, double z)
  {
    //truncate all coordinates to lower floor of box
    int ix = floor_double2int(x);
    int iy = floor_double2int(y);
    int iz = floor_double2int(z);
    double fracH = x-ix;
    double fracK = y-iy;
    double fracL = z-iz;
    // Extract the 4x4x4 block of points surrounding the desired interpolation point
    cmplex p[4][4][4];
    for (int i = 0; i < 4.; i++) {
      for (int j = 0; j < 4.; j++) {
        for (int k = 0; k < 4.; k++) {
          p[i][j][k] = getE(ix - 1 + i,iy - 1 + j,iz - 1 + k);
        }
      }
    }
    return tricubic(p, x, y, z);
  }

}//phasertng
