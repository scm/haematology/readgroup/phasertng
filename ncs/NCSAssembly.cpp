#include "NCSAssembly.h"

using namespace iotbx::pdb::hierarchy;

namespace phaser {  namespace ncs {

//
// class NCSAssembly
//


// Constructor
NCSAssembly::NCSAssembly(const chain& reference, double angular, double spatial)
    : NCSSymmetry( std::vector< NCSOperator >(), angular, spatial )
{
    chains_.push_back( reference );
}

// Destructor
NCSAssembly::~NCSAssembly()
{
}

// Accessor
const std::vector< chain >&
NCSAssembly::get_chains() const
{
    return chains_;
}

// Display methods
std::string
NCSAssembly::name() const
{
    return "NCSAssembly";
}


// Other methods
void
NCSAssembly::insert(const NCSOperator& op, const chain& ch)
{
    operators_.push_back( op );
    chains_.push_back( ch );
}

}  } // namespace phaser::ncs
