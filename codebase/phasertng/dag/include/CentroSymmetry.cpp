//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>

namespace phasertng {
namespace dag {

  //ListEditing
  int
  DagDatabase::find_centrosymmetry(double transMax)
  {
    int tsize = NODES.size();
    cctbx::sgtbx::space_group SgOps(NODES[0].SG->hall(),"A1983");
    cctbx::sgtbx::space_group_type SgInfo(SgOps);
    cctbx::sgtbx::change_of_basis_op cb_op = SgInfo.change_of_hand_op();
   //add centre of symmetry so that other hand identified as duplicate
   //not for centrosymmetry test!
   // if (SgInfo.is_enantiomorphic()) return 0;
    cctbx::sgtbx::structure_seminvariants semis(SgOps);
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
    SpaceGroup sgEuclid("Hall: "+ssym.subgroup().type().hall_symbol());
    // Repeat setup work for reference setting.
    cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
    cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
    cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
    cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
    dmat33 cb_op_r_to_reference_setting(SgInfo.cb_op().c().r().as_double());
    dmat33 cb_op_r_from_reference_setting(SgInfo.cb_op().c_inv().r().as_double());

    // Compare solutions to find duplicates further down list
    for (int k = 0; k < tsize; k++)
      NODES[k].generic_flag = true; //ok, NOT centrosymmetric
    for (int k = 0; k < tsize; k++)
    {
      int nmodels = NODES[k].POSE.size();
      //af_atom HAND = EPSET[k].other_hand(SG_HALL,CELL);
      cctbx::fractional<double> site = NODES[k].POSE[0].FRACT;
      auto HAND = cb_op(site);
      {//k_chk (hand)
        bool matched(true);
        bool allmatched(true),originDefined(false); //-Wall init
        dvect3 zero3(0,0,0), originShift(0,0,0);
        int isymEuclid(-1);
        // Store some information about first model in reference solution
        //dvect3 fracSolC_first = EPSET[k][0].SCAT.site;
        dvect3 fracSolC_first = NODES[k].POSE[0].FRACT;
        std::vector<dvect3> matched_site(nmodels);
        std::vector<double> matched_dist(nmodels,-999);
        std::vector<int> matched_symop(nmodels);
        std::vector<int> matched_s(nmodels);
        // Find possible choices of origin shift of checked solution that make a match with first reference model
        for (unsigned s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
        {
          allmatched = false;
          originDefined = false;
          originShift = zero3;

          dvect3 fracSolC_chk(HAND[s_chk_ori]);

          for (unsigned isym = 0; isym < sgEuclid.group().order_z(); isym++)
          {
            // Determine sensible distance thresholds from dmin and model mean radius
            // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
            {
              // Check that centres of mass are sufficiently similar
              // Centre of mass of ensemble won't change with application
              // of point group symmetry.
              dvect3 fracSolC_chk_sym = sgEuclid.doSymXYZ(isym,fracSolC_chk);
              for (unsigned j = 0; j < 3; j++)
              {
                while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += 1; }
                while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= 1; }
              }
              dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
              for (unsigned j = 0; j < 3; j++)
              {
                while (fracdelta[j] < -0.5) fracdelta[j] += 1;
                while (fracdelta[j] >= 0.5) fracdelta[j] -= 1;
              }
              // Account for any continuous shifts as origin shift
              dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
              dvect3 originShift_reference(0,0,0);
              af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
              for (unsigned j = 0; j < 3; j++) {
                if (isshift[j]) {
                  originShift_reference[j] = -fracdelta_reference[j];
                  fracdelta_reference[j] = 0;
                }
              }
              fracdelta=cb_op_r_from_reference_setting*fracdelta_reference;
              originShift=cb_op_r_from_reference_setting*originShift_reference;
              dvect3 delta = UnitCell(NODES[0].CELL).orthogonalization_matrix()*fracdelta;
              double tdist = delta.length();
              if (tdist <= transMax)
              {
                isymEuclid = isym;
                originDefined = true;
                break;
              }
            } // ensym
            if (originDefined) break;
          } // isym
          if (!originDefined) continue; // Test different origins

          PHASER_ASSERT(originDefined);

          // Now use choice of relative origin (if defined) to test equivalence
          allmatched = true; //So far at least one match.  Negate if one fails.
          sv_bool used(nmodels,false);
          for (unsigned s = 0; s < nmodels; s++)
          { // For each model in the reference, check whether any matching models are the same
            // Determine sensible distance thresholds from dmin and model mean radius
            //dvect3 fracSolC = EPSET[k][s].SCAT.site;
            dvect3 fracSolC = NODES[k].POSE[s].FRACT;
            matched = false;
            for (unsigned s_chk = 0; s_chk < nmodels; s_chk++)
            {
            dvect3 fracSolC_chk = HAND[s_chk];
            if (originDefined)
              fracSolC_chk = sgEuclid.doSymXYZ(isymEuclid,fracSolC_chk);
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += 1; }
              while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= 1; }
            }
            if (used[s_chk]) continue;
            if (s_chk == s_chk_ori)
            {
              matched = true;
              used[s_chk] = true;
              matched_site[s_chk] = fracSolC_chk;
              matched_symop[s_chk] = -999;
              matched_s[s] = s_chk;
            }
            else
            {
              SpaceGroup sg = !originDefined ? sgEuclid : SpaceGroup(NODES[0].HALL);
              double ONE(1);
              for (unsigned isym = 0; isym < sg.group().order_z(); isym++)
              {
                {
                  // Check that centres of mass are
                  // sufficiently similar. Centre of mass of ensemble won't
                  // change with application of point group symmetry.
                  dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
                  dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
                  dvect3 shiftdelta(0,0,0);
                  for (unsigned j = 0; j < 3; j++)
                  {
                    while (fracdelta[j] < -0.5) { fracdelta[j] += 1; shiftdelta[j]++; }
                    while (fracdelta[j] >= 0.5) { fracdelta[j] -= 1; shiftdelta[j]--; }
                  }
                  dvect3 delta = UnitCell(NODES[0].CELL).orthogonalization_matrix()*fracdelta;
                  double tdist = delta.length();
                  if (tdist > transMax) continue;
                  matched = true;
                  used[s_chk] = true;
                  matched_site[s] = fracSolC_chk_sym-shiftdelta;
                  matched_dist[s] = tdist;
                  matched_symop[s] = isym;
                  matched_s[s] = s_chk;
                  if (matched) break;
                } // ensym
                if (matched) break;
              } // isym
              if (matched) break;
            } // s_chk
            } // s_chk
            if (!matched) allmatched = false;
          } // s
          if (allmatched)
          {
           // for (unsigned s = 0; s < HAND.size(); s++)
           //   HAND[s].SCAT.site = matched_site[s];
            break;
          }
          // Failed to match everything.  Try again with another possible origin shift.
          for (unsigned m = 0; m < nmodels; m++)
            used[m] = false;
        } // s_chk_ori
        if (allmatched)
        {
          //EPSET[k].generic_bool = true;
          NODES[k].generic_flag = false; //centrosymmetric for erase_invalid
        }
      } //k_chk
    } // end #pragma omp parallel
    int ncent = 0;
    for (int k = 0; k < tsize; k++)
      if (NODES[k].generic_flag == false) ncent++; //not not centrosymmetric
    return ncent;
  }

}}
