//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineSAD.h>
#include <phasertng/main/jiffy.h>
#include <cctbx/eltbx/xray_scattering.h>

namespace phaser {

RefineSAD::RefineSAD() : DataSAD(), RefineBase() {}  //default constructor

//C++ constructor
std::pair<af_string,std::string>
RefineSAD::setRefineSAD(
                     std::string SG_HALL,
                     af::double6 UNIT_CELL,
                     af::shared<miller::index<int> > MILLER,
                     data_spots POS_,data_spots NEG_,
                     data_tncs PTNCS_,
                     af_atom SADSET,
                     std::string CLUSTER_PDB,
                     data_bins DATA_BINS,
                     std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES,
                     data_outl OUTLIER,
                     data_restraint WILSON,double WILSON_B,
                     data_restraint SPHERICITY,
                     data_restraint FDP,
                     data_part PARTIAL,
                     data_ffts FFTvDIRECT,
                     float1D WILSON_LLG,
                     bool verbose
)
{
  SpaceGroup SG(SG_HALL);
  UnitCell UC(UNIT_CELL);
  af_string output = setDataSAD(SG,UC,MILLER,POS_,NEG_, PTNCS_, DATA_BINS,OUTLIER,PARTIAL,ATOMTYPES);
  setSpaceGroup(SG_HALL); //DataSAD->HandEP->SpaceGroup
  af_bool WORKING(MILLER.size(),true);
  af_string txt;std::string advisory;
  std::tie(txt,advisory) = init(WORKING,
       WILSON,WILSON_B,
       SPHERICITY,
       FDP,
       FFTvDIRECT,
       WILSON_LLG
       );
  output.insert(output.end(),txt.begin(),txt.end());
  txt = initAtomic(SADSET,ATOMTYPES,CLUSTER_PDB,verbose);
  output.insert(output.end(),txt.begin(),txt.end());
  calcVariances(); //also done in addAtoms
  calcIntegrationPoints();
  //after variances, called in addAtoms from initAtomic
  calcAtomicData();
  calcScalesData();
  calcOutliers();
  return {output,advisory};
}//end c++ constructor

std::pair<af_string,std::string>
RefineSAD::init(af_bool WORKING,
                data_restraint WILSON_,double WILSON_B,
                data_restraint SPHERICITY_,
                data_restraint FDP_,
                data_ffts FFTvDIRECT_,
                float1D WILSON_LLG
)
{
  af_string output;
  std::string advisory;
  sad_target_anom_only = false;
  ScaleK = 1; ScaleU = 0;
  input_atoms = false;
  input_partial = false; //set in calcPartialStructure from initAtomic after this call to init
  reversed = false;
  use_fft = false;
  FFTvDIRECT = FFTvDIRECT_;
  WILSON = WILSON_;
  FDP = FDP_;
  if (WILSON.RESTRAINT) assert(WILSON.SIGMA > 0);
  WilsonB = WILSON_B;
  double upperB(cctbx::adptbx::u_as_b(RefineSAD::upperU()));
  double lowerB(cctbx::adptbx::u_as_b(RefineSAD::lowerU()));
  if (WILSON.RESTRAINT && WilsonB > upperB)
  {
    output.push_back("Wilson B-factor (" + std::to_string(WilsonB) + ") greater than upper limit of B-factors (" +
        std::to_string(upperB) + "). Wilson B-Factor set to maximum.");
    WilsonB = upperB;
  }
  else if (WILSON.RESTRAINT && WilsonB < lowerB)
  {
    advisory= "Wilson B-factor (" + std::to_string(WilsonB) + ") less than lower limit of B-factors (" + std::to_string(lowerB) + "). Wilson B-Factor set to minimum.";
    output.push_back(advisory);
    WilsonB = lowerB;
  }

  SPHERICITY = SPHERICITY_;
  if (SPHERICITY.RESTRAINT) assert(SPHERICITY.SIGMA > 0);
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();

  working = WORKING;
  wilson_llg = 0;
  assert(working.size() == NREFL);
  if (!WILSON_LLG.size())
  {
    wilson_llg_refl.resize(NREFL);
    for (int r = 0; r < NREFL; r++) wilson_llg_refl[r] = 0;
  }
  else wilson_llg_refl = WILSON_LLG;

  //allocate memory
  FHpos.resize(NREFL);
  FHneg.resize(NREFL);
  FApos.resize(NREFL);
  FAneg.resize(NREFL);
  sigDsqr.resize(NREFL);
  sigPlus.resize(NREFL);
  DphiA.resize(NREFL);
  DphiB.resize(NREFL);

  //Compute initial values for variance parameters
  //iff the setVarianceEP has not been set already, externally
  if (SDsqr_bin.size() != bin.numbins())
  {
  SDsqr_bin.resize(bin.numbins());
  DphiA_bin.resize(bin.numbins());
  DphiB_bin.resize(bin.numbins());
  SP_bin.resize(bin.numbins());

  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    DphiA_bin[s] = 1;
    DphiB_bin[s] = 0;
    SP_bin[s] = DANOVAR_bin[s]/4.; // Start with variance of mean-square(DANO)/4.
  }
  //SD code depends on scattering (FH)
  }

  //setup DEFAULT step sizes for integration for this macrocycle
  fixed_num_integration_points = false;
  acentPhsIntg.clear();
  for (int p = 3; p <= 120; p++) //min and max ranges for number of points
    if (360 % p == 0) //number of points fits into 360
      acentPhsIntg.push_back(EqualUnity(p));
  return {output,advisory};
}

void RefineSAD::setFixFdp(std::map<std::string,bool> FIX_ATOMTYPE)
{
  input_fix_fdp.resize(AtomFdp.size());
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    std::string atomtype = t2atomtype[t];
    if (FIX_ATOMTYPE.find(atomtype) != FIX_ATOMTYPE.end())
      input_fix_fdp[t] = FIX_ATOMTYPE[atomtype];
    else input_fix_fdp[t] = false;
  }
}

void RefineSAD::setAtomsInput(af_atom new_atoms)
{
  //reset atom_input to starting values or defaults for partial atoms
  for (int a = 0; a < atoms.size(); a++)
  {
    if (a < new_atoms.size()) atoms[a].copy_input(new_atoms[a]);
    else
    {
      //check that any extra atoms have been deleted from partial structure
      assert(atoms[a].SCAT.label == "px");
      atoms[a].set_input_defaults();
    }
  }
}

af_string RefineSAD::addAtoms(af_atom new_atoms,bool verbose)
{
  af_string output;
  output.push_back("");
  output.push_back("--Adding Atoms--");
  output.push_back("Adding " +std::to_string(new_atoms.size()) + " atoms to current atom list of " +std::to_string(atoms.size()));

  int start_natoms(0);
  for (int a = 0; a < atoms.size(); a++) if (!atoms[a].XTRA.rejected) start_natoms++;

  int new_natoms(0);
  //move close to partial
  if (input_partial and PTNCS.NMOL == 1) //don't move ptncs pairs, too confusing in output
  {
    auto& pdb = COORDINATES->PDB;
    for (unsigned a = 0; a < new_atoms.size(); a++)
    {
      double min_Delta = std::numeric_limits<double>::max();
      dvect3 closest_site(0,0,0),cellTrans(0,0,0);
      for (int p = 0; p < pdb.size(); p++)
      for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
      for (cellTrans[0] = -2; cellTrans[0] <= 2; cellTrans[0]++)
      for (cellTrans[1] = -2; cellTrans[1] <= 2; cellTrans[1]++)
      for (cellTrans[2] = -2; cellTrans[2] <= 2; cellTrans[2]++)
      {
        dvect3 frac_i = SpaceGroup::doSymXYZ(isym,new_atoms[a].SCAT.site) + cellTrans;
        dvect3 orth_i = UnitCell::doFrac2Orth(frac_i);
        double Delta(std::fabs((orth_i-pdb[p].X)*(orth_i-pdb[p].X)));
        if (Delta < min_Delta) { min_Delta = Delta; closest_site = frac_i; }
      }
      new_atoms[a].SCAT.site = closest_site;
    }
  }

  for (unsigned a = 0; a < new_atoms.size(); a++)
  {
    //move the atom onto a special position if it is close to one
    cctbx::sgtbx::site_symmetry this_site_sym(getCctbxUC(),getCctbxSG(),new_atoms[a].SCAT.site);
    new_atoms[a].SCAT.site = this_site_sym.exact_site();

    //Extra parameters
    new_atoms[a].XTRA.n_adp = this_site_sym.adp_constraints().n_independent_params();
    new_atoms[a].XTRA.n_xyz = this_site_sym.site_constraints().n_independent_params();
    site_sym_table.process(new_atoms[a].SCAT.apply_symmetry(getCctbxUC(),getCctbxSG()));
    new_atoms[a].set_input_defaults();

    //finally add processed atom to stored list
    atoms.push_back(new_atoms[a]);
    if (!new_atoms[a].XTRA.rejected) new_natoms++;
  }

  int natoms(0);
  bool non_standard_scattering_type(false);
  for (int a = 0; a < atoms.size(); a++)
  {
    if (!atoms[a].XTRA.rejected) natoms++;
    if (atoms[a].SCAT.scattering_type == "RX" || atoms[a].SCAT.scattering_type == "AX"
        || !isElement(atoms[a].SCAT.scattering_type)) //cluster
        non_standard_scattering_type = true;
  }

  //work out whether direct summation or fft is faster for sf and gradient calculations
  //Don't use FFT if partial structure, because won't work (at least now) for gradients
  if (verbose) output.push_back("  Setting use_fft");
  if (non_standard_scattering_type)
    use_fft = false;
  else if (new_natoms)
  {
    if (input_partial ||
        (COORDINATES != nullptr && COORDINATES->PDB.size())) // input_partial may not yet be set
    {
     //AJM TODO why did phaser make id direct summation for large ss&&partial?
     // if (verbose) output.push_back("  Partial structure case: use summation");
     // use_fft = false;
    }
    else
    {
      if (verbose)
      output.push_back("  Range for summation/fft test = " + std::to_string(FFTvDIRECT.MIN) + " to " + std::to_string(FFTvDIRECT.MAX));
      if (natoms < FFTvDIRECT.MIN) use_fft = false;
      else if (natoms > FFTvDIRECT.MAX) //class limits
      {
        if (!use_fft) prepareAtomicFFT();
        use_fft = true;
      }
      else //grey zone, speed depends on cell, resolution, symmetry, #reflections and #atoms unpredictably
      {
        if (verbose) output.push_back("  Testing Summation Method");
        use_fft = false;
        double start_clock = std::clock();
        calcAtomicData();
        std::clock_t now_clock = std::clock();
        double time_for_sum = (now_clock-start_clock)/double(CLOCKS_PER_SEC);
        if (verbose) output.push_back("  Time = " + std::to_string(time_for_sum) + " secs");
        if (verbose) output.push_back("  Testing FFT Method");
        start_clock = std::clock();
        use_fft = true;
        prepareAtomicFFT();
        calcAtomicData();
        now_clock = std::clock();
        double time_for_fft = (now_clock-start_clock)/double(CLOCKS_PER_SEC);
        if (verbose) output.push_back("  Time = " + std::to_string(time_for_fft) + " secs");
        use_fft = time_for_fft < time_for_sum;
      }
    }
  }
  calcAtomicData();

  if (verbose)
  use_fft?
    output.push_back("  FFT method will be used for sf calculation and gradients") :
    output.push_back("  Sum method will be used for sf calculation and gradients");

  auto lines = calcPartialStructure();
  for (auto txt : lines.second) output.push_back(txt);

  if (verbose) output.push_back("  Memory allocation dependent on number of atoms");
  resizeAtomArrays();
  calcAtomicData();
  calcScalesData();

  if (natoms > 2*start_natoms && !input_partial)
  { // natoms more than doubled without partial structure.
    if (verbose) output.push_back("  Calculating variances");
    calcVariances();
    calcSigmaaData(); //transfer to reflections
  }
  input_atoms = atoms.size();
  bool1D outlier(NREFL,false);
  for (int o = 0; o < OUTLIER.ANO.size(); o++)
    outlier[OUTLIER.ANO[o].refl] = true;
  for (int o = 0; o < OUTLIER.SAD.size(); o++)
    outlier[OUTLIER.SAD[o].refl] = true;
  for (int r = 0; r < NREFL; r++)
  {
    //check partial map extends to resolution limit
    bool phase_info_present((!input_partial) || (input_partial && FPpos[r] != cmplxType(0)));
    selected[r] = !bad_data[r] && working[r] && !outlier[r] && phase_info_present;
  }
  return output;
}

af_string RefineSAD::restoreAtoms(int nrestored,std::multimap<int,restored_data>& restored,int c)
{
  af_string output;
  output.push_back("--Restore Sites--");
  output.push_back("Restoring " +std::to_string(nrestored) + " atoms in current atom list of " +std::to_string(atoms.size()));
  if (restored.size())
  {
    output.push_back("Restored Atoms Atomtype/Z-score (*=best)");
    typedef std::multimap<int,restored_data>::iterator I;
    for (I iter = restored.begin(); iter != restored.end(); iter++)
    {
      int a = iter->first;
      if (!atoms[a].XTRA.restored) //since a appears multiple times in list
      {
        std::pair<I,I> b = restored.equal_range(a);
        std::string best_atomtype;
        double best_occupancy(0),best_zscore(-9999);
        for (I i = b.first; i != b.second; i++)
        {
          restored_data elem = i->second;
          if (elem.zscore > best_zscore)
          {
            best_atomtype = elem.atomtype;
            best_occupancy = elem.occupancy;
            best_zscore = elem.zscore;
          }
        }
        std::string last_atomtype("");
        std::string line = "#";
        line += std::string(a<10?" ":"");
        line += std::string(a<100?" ":"");
        line += std::string(a<1000?" ":"");
        line += std::to_string(a+1) + "         ";
        for (I i = b.first; i != b.second; i++)
        {
          restored_data elem = i->second;
          if (last_atomtype != elem.atomtype)
          {
            line += std::string(elem.atomtype.size()<2?" ":"") + elem.atomtype + "/" + phasertng::dtos(elem.zscore,4,2);
            line += std::string(elem.zscore==best_zscore?"*  ":"   ");
            last_atomtype = elem.atomtype;
          }
        }
        output.push_back(line);
        atoms[a].XTRA.rejected = false;
        atoms[a].XTRA.restored = true;
        atoms[a].SCAT.scattering_type = best_atomtype;
        atoms[a].SCAT.occupancy = best_occupancy; //occupancy set to 0 in deletion
        atoms[a].SCAT.label += " Restored NCYC="+std::to_string(c);
      }
    }
    calcAtomicData();
    calcScalesData();
  }
  output.push_back("");
  return output;
}

af_string RefineSAD::anisoAtoms(int nanisotropic,std::set<int>& anisotropic,int c)
{
  af_string output;
  output.push_back("--Make Sites Anisotropic--");
  output.push_back("Making " +std::to_string(nanisotropic) + " atoms in current atom list of " +std::to_string(atoms.size()) + " anisotropic");
  if (anisotropic.size())
  {
    bool first(true);
    std::string message;
    int nline(0);
    for (std::set<int>::iterator iter = anisotropic.begin(); iter != anisotropic.end(); iter++)
    {
      int a = (*iter);
      //converts u_iso to u_star in place
      atoms[a].SCAT.convert_to_anisotropic(UnitCell::getCctbxUC());
      atoms[a].SCAT.label += " Anisotropic NCYC=" + std::to_string(c);
      message += std::to_string(a+1) + " ";
      //make paired also anisotropic so that restraints work
      int b(-999); //flag
      if (b >= 0 && ! atoms[b].SCAT.flags.use_u_aniso_only())
      {
        atoms[b].SCAT.convert_to_anisotropic(UnitCell::getCctbxUC());
        atoms[b].SCAT.label += " Anisotropic NCYC="+std::to_string(c);
        if (first) output.push_back("Anisotropic Atom #'s:");
        first = false;
        message += phasertng::ntos(b+1,nanisotropic) + " ";
        if (nline == 10) { output.push_back(message); message = ""; nline = 0; }
      }
    }
    output.push_back(message);
    calcAtomicData();
    calcScalesData();
  }
  output.push_back("");
  return output;
}

af_string RefineSAD::deleteAtoms(int ndeleted,std::set<int>& deleted,int c)
{
  af_string output;
  output.push_back("");
  output.push_back("--Delete Low Occupancy Sites--");
  output.push_back("Deleting " +std::to_string(ndeleted) + " atoms in current atom list of " +std::to_string(atoms.size()));
  output.push_back("Deleted Atom #'s: ");
  bool first(false);
  std::string message;
  for (std::set<int>::iterator iter = deleted.begin(); iter != deleted.end(); iter++)
  {
    int a = *iter;
    if (!atoms[a].XTRA.rejected)
    {
      atoms[a].XTRA.rejected = true;
      atoms[a].SCAT.label += " Rejected NCYC=" + std::to_string(c);
      atoms[a].SCAT.occupancy = 0; //no structure factors contributed from this atom
      message += phasertng::itos(a+1,4) + " ";
      first = true;
      if (message.length() > 5*10)
      {
        output.push_back(message);
        message = "";
      }
    }
  }
  if (message.length())
    output.push_back(message);
  if (!first) output.push_back("None");
  calcAtomicData();
  calcScalesData();
  output.push_back("");
  return output;
}

void RefineSAD::calcVariances()
{
  float1D SH_bin(bin.numbins(),0.0),NumInBin(bin.numbins(),0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    unsigned s = rbin[r];
    SH_bin[s] += SigmaH(r);
    NumInBin[s]++;
  }
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (NumInBin[s]) SH_bin[s] /= NumInBin[s];
    SDsqr_bin[s] = std::max(SN_bin[s]/2.,SN_bin[s]-SH_bin[s]); // from scattering power of model, if < half
    SDsqr_bin[s] = std::max(SDsqr_bin[s],SH_bin[s]/2.); // cope with case where Fc much bigger than Fo
  }
  calcSigmaaData(); //transfer to reflection-wise information
}

double RefineSAD::SigmaH(unsigned & r)
{
  double sigmaH(0);
  double symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
  //add partial scattering, which has no anomalous component
  if (input_partial)
  {
    double debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
    cmplxType partial_scat = PartK*std::exp(-ssqr[r]*debye_PartB)*FPpos[r];
    sigmaH += std::norm(partial_scat);
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].XTRA.rejected)
  {
    int t = atomtype2t[atoms[a].SCAT.scattering_type];
   // int rt = t*NREFL+r;
    double debye_waller_u_iso = atoms[a].SCAT.flags.use_u_aniso_only() ? 0 : cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
    double isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    double symmOccBfac(symFacPCIF*ScaleK*atoms[a].SCAT.occupancy*isoB/M);
    double scat(fo_plus_fp[r][t]*symmOccBfac);
    double fpp(AtomFdp[t]*symmOccBfac*CLUSTER[t].debye(ssqr[r]));
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      double anisoB(1);
      if (atoms[a].SCAT.flags.use_u_aniso_only())
        anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],atoms[a].SCAT.u_star);
      double anisoScat = scat*anisoB;
      double anisoFpp = fpp*anisoB;
      sigmaH += fn::pow2(anisoScat) + fn::pow2(anisoFpp);
    }//end sym
  } //end  loop over atoms
  return sigmaH;
}

void RefineSAD::resizeAtomArrays()
{
  input_fix_fdp.resize(AtomFdp.size());
  //these don't change, but only called when grads by summation
  dReFHpos_by_dFdp.resize(AtomFdp.size()), dImFHpos_by_dFdp.resize(AtomFdp.size());
  dReFHneg_by_dFdp.resize(AtomFdp.size()), dImFHneg_by_dFdp.resize(AtomFdp.size());
  d2ReFHpos_by_dFdp2.resize(AtomFdp.size()),d2ImFHpos_by_dFdp2.resize(AtomFdp.size());
  d2ReFHneg_by_dFdp2.resize(AtomFdp.size()),d2ImFHneg_by_dFdp2.resize(AtomFdp.size());

  //these change with NATOM
  unsigned NATOM = atoms.size();
  dReFHpos_by_dX.resize(NATOM), dImFHpos_by_dX.resize(NATOM);
  dReFHpos_by_dY.resize(NATOM), dImFHpos_by_dY.resize(NATOM);
  dReFHpos_by_dZ.resize(NATOM), dImFHpos_by_dZ.resize(NATOM);
  dReFHpos_by_dO.resize(NATOM), dImFHpos_by_dO.resize(NATOM);
  dReFHpos_by_dIB.resize(NATOM), dImFHpos_by_dIB.resize(NATOM);
  dReFHpos_by_dAB.resize(NATOM), dImFHpos_by_dAB.resize(NATOM);
  dReFHneg_by_dX.resize(NATOM), dImFHneg_by_dX.resize(NATOM);
  dReFHneg_by_dY.resize(NATOM), dImFHneg_by_dY.resize(NATOM);
  dReFHneg_by_dZ.resize(NATOM), dImFHneg_by_dZ.resize(NATOM);
  dReFHneg_by_dO.resize(NATOM), dImFHneg_by_dO.resize(NATOM);
  dReFHneg_by_dIB.resize(NATOM), dImFHneg_by_dIB.resize(NATOM);
  dReFHneg_by_dAB.resize(NATOM), dImFHneg_by_dAB.resize(NATOM);

  d2ReFHpos_by_dX2.resize(NATOM), d2ImFHpos_by_dX2.resize(NATOM);
  d2ReFHpos_by_dY2.resize(NATOM), d2ImFHpos_by_dY2.resize(NATOM);
  d2ReFHpos_by_dZ2.resize(NATOM), d2ImFHpos_by_dZ2.resize(NATOM);
  d2ReFHpos_by_dO2.resize(NATOM), d2ImFHpos_by_dO2.resize(NATOM);
  d2ReFHpos_by_dIB2.resize(NATOM), d2ImFHpos_by_dIB2.resize(NATOM);
  d2ReFHpos_by_dAB2.resize(NATOM),d2ImFHpos_by_dAB2.resize(NATOM);
  d2ReFHneg_by_dX2.resize(NATOM), d2ImFHneg_by_dX2.resize(NATOM);
  d2ReFHneg_by_dY2.resize(NATOM), d2ImFHneg_by_dY2.resize(NATOM);
  d2ReFHneg_by_dZ2.resize(NATOM), d2ImFHneg_by_dZ2.resize(NATOM);
  d2ReFHneg_by_dO2.resize(NATOM), d2ImFHneg_by_dO2.resize(NATOM);
  d2ReFHneg_by_dIB2.resize(NATOM), d2ImFHneg_by_dIB2.resize(NATOM);
  d2ReFHneg_by_dAB2.resize(NATOM),d2ImFHneg_by_dAB2.resize(NATOM);
}

void RefineSAD::setTarget(std::string target)
{
  sad_target_anom_only = (target == "ANOM_ONLY");
  if (sad_target_anom_only)
  {
    int nboth(0);
    for (int r = 0; r < NREFL; r++)
      if (!bad_data[r] && both[r])
        nboth++;
    assert(nboth && sad_target_anom_only);
  }
}

void RefineSAD::setupIntegrationPoints(int INTG_STEP)
{
  ibin.resize(NREFL);
  fixed_num_integration_points = true;
  assert(360 % INTG_STEP == 0); //steps must divide into 360 degrees
  assert(INTG_STEP >=3 && INTG_STEP <=120); //limits in constructor
  for (int i = 0; i < acentPhsIntg.size(); i++)
    if (acentPhsIntg[i].nStep == INTG_STEP)
      for (int r = 0; r < NREFL; r++)
        if (!bad_data[r])
          ibin[r] = i;
}

void RefineSAD::calcIntegrationPoints()
{
  ibin.resize(NREFL);
  if (!fixed_num_integration_points)
  {
    double mean_nstep(0.);
    double gaussFac(1.);
    if (getenv("PHASER_INTG_FAC") != NULL)
      gaussFac = std::atof(getenv("PHASER_INTG_FAC"));
    for (int r = 0; r < NREFL; r++)
    if (!bad_data[r] && both[r])
    {
      double SigmaNeg(sigDsqr[r] + fn::pow2(NEG.SIGF[r]));
      double SigmaPos(sigPlus[r] + (fn::pow2(POS.SIGF[r]) + fn::pow2(NEG.SIGF[r])));
      assert(SigmaNeg > 0);
      assert(SigmaPos > 0);
      double dFH(std::abs(FHpos[r]-FHneg[r]));
      double X(std::abs(FHneg[r])*NEG.F[r]/SigmaNeg);
      double ndano(dFH/std::sqrt(SigmaPos));
      double nsim(std::sqrt(X));
      //double nstep_raw = 5 + 8*std::sqrt(ndano*ndano+nsim*nsim); //1 in 10^8 from simulations
      double nstep_raw = 5 + 9*std::sqrt(ndano*ndano+nsim*nsim); // Better in several test cases
      unsigned nstep = gaussFac*nstep_raw + 0.5; // scale and round
      /*if (!(r%500))
      {
        int nstep2 = nstep;
        std::cout << "\nr,ndano,nsim,nstep: " << r << " " << ndano << " " << nsim << " " << nstep_raw << "\n";
        std::cout << "ListPlot[Table[{ng,Log[10,SADfuncRelRMS[" << POS.F[r] << "," << NEG.F[r] << "," << std::real(FHpos[r]) << "+I*" << std::imag(FHpos[r]) << "," << std::real(FHneg[r]) << "+I*" << std::imag(FHneg[r]) << "," << SigmaPos << "," << SigmaNeg << ",ng]]},{ng," << std::max(1,nstep2-10) << "," << nstep2+10 << "}],PlotJoined->True,PlotRange->All]\n";
      }*/
      assert(acentPhsIntg.size());
      nstep = std::max(nstep,acentPhsIntg[0].nStep);
      //set ibin to largest nstep value, the default value
      ibin[r] = acentPhsIntg.size()-1;
      //search to acentPhsIntg.size() -1 and bail out if value >= nstep is found
      for (int i = 0; i < acentPhsIntg.size()-1; i++) //find the index of with nstep value
        if (acentPhsIntg[i].nStep >= nstep)
        {
          ibin[r] = i;
          mean_nstep += acentPhsIntg[i].nStep;
          break;
        }
    }
    //mean_nstep /= NREFL;
    //std::cout << "      Mean number of integration points: " << mean_nstep << "\n";
  }
}

af_string RefineSAD::initAtomic(af_atom SADSET,std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES,
                           std::string CLUSTER_PDB,bool verbose)
{
  int t(0);
  //by using a map the list is guaranteed to be unique
  for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator
         iter = ATOMTYPES.begin(); iter != ATOMTYPES.end(); iter++)
  {
    //create the lookup tables for the atomtype (string) to T (int)
    //for fast array indexing
    std::string atomtype = iter->first;
    t2atomtype[t] = atomtype;
    atomtype2t[atomtype] = t;
    //override any values of Fp and Fdp set with any other method with
    //the values on the SCAT ATOM card
    AtomFp.push_back(ATOMTYPES[atomtype].fp());
    AtomFdp.push_back(ATOMTYPES[atomtype].fdp());
    AtomFdpInit.push_back(ATOMTYPES[atomtype].fdp());
    phasertng::Cluster cluster; //initializes with values for "NOT CLUSTER"
    if (atomtype == "XX")
      CLUSTER.push_back(phasertng::Cluster(atomtype,CLUSTER_PDB));
    else if (atomtype == "TX")
      CLUSTER.push_back(phasertng::Cluster(atomtype));
    else
      CLUSTER.push_back(phasertng::Cluster()); //even if not cluster - spacer
    t++;
  }

  //store fo
  PHASER_ASSERT(NREFL);
  PHASER_ASSERT(ATOMTYPES.size());
  fo_plus_fp.resize(NREFL);
  for (int r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    fo_plus_fp[r].resize(ATOMTYPES.size());
    for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator iter = ATOMTYPES.begin(); iter != ATOMTYPES.end(); iter++)
    {
      std::string atomtype = (*iter).first;
      int t = atomtype2t[atomtype];
      if (atomtype == "RX")
        fo_plus_fp[r][t] = 1;
      else if (atomtype == "AX")
        fo_plus_fp[r][t] = 0;
      else if (!isElement(atomtype))
        fo_plus_fp[r][t] = CLUSTER[t].scattering(ssqr[r]) + AtomFp[t]*CLUSTER[t].debye(ssqr[r]);
      else
      {
        cctbx::eltbx::xray_scattering::wk1995 ff(atomtype);
        fo_plus_fp[r][t] = ff.fetch().at_d_star_sq(ssqr[r]) + AtomFp[t];
      }
    }
  }

  //have to addAtoms in constructor in order to initialize variance terms
  for (unsigned a = 0; a < SADSET.size(); a++)
  {
    // SADSET[a].set_extra_defaults(); //bug, wipes REJECTED when phasing at the end
    if (!SADSET[a].SCAT.occupancy) SADSET[a].SCAT.occupancy = lowerO(); //replace input zero occupancy with lowerO
  }
  return addAtoms(SADSET,verbose);
}


double RefineSAD::wilsonFn(float1D& WILSON_LLG)
{
  WILSON_LLG.resize(NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    if (both[r])
      WILSON_LLG[r] = acentricReflIntgSAD2(r).LogLike;
    else if (!sad_target_anom_only && cent[r])
      WILSON_LLG[r] = centricReflIntgSAD2(r).LogLike;
  else if (!sad_target_anom_only)
      WILSON_LLG[r] = singletonReflIntgSAD2(r,plus[r]).LogLike;
  }
  double REFLLG(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    REFLLG += WILSON_LLG[r];
  return REFLLG;
}

} //phaser
