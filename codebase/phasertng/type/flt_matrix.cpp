//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_matrix.h>
#include <iomanip>

namespace phasertng {
namespace type {

  flt_matrix::flt_matrix(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const dmat33 dephalt_
   ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void flt_matrix::set_value(dmat33 other)
  {
    user = true;
    uvalue = other;
  }

  void
  flt_matrix::set_value(
      double aa, double ab, double ac,
      double ba, double bb, double bc,
      double ca, double cb, double cc
    )
  {
    user = true;
    uvalue = dmat33(aa,ab,ac,ba,bb,bc,ca,cb,cc);
  }

  void flt_matrix::set_value(const sv_string& vcards,const dmat33& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_matrix::is_default() const
  { return uvalue == dephalt; }

  void
  flt_matrix::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_matrix::to_python()
  { return unparse_parameter(false); }

  void
  flt_matrix::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    for (int i = 0; i < 9; i++)
      uvalue[i] = std::stod(txt[i]);
  }

  dmat33
  flt_matrix::value_or_default() const
  { return user ? uvalue : dephalt; }

  dmat33
  flt_matrix::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  flt_matrix::unparse_parameter(bool phil) const
  {
    if (value_or_default() ==
        dmat33(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
               DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
               DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG))
    {
      return None();
    }
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 9; i++)
      ss << value_or_default()[i] << " ";
    return ss.str();
  }

  std::string
  flt_matrix::show_default(bool phil) const
  {
    if (dephalt ==
        dmat33(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
               DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
               DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG))
    {
      return None();
    }
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 9; i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
