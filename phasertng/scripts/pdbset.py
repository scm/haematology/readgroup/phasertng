import argparse, os
import iotbx
import numpy as np
from scipy.spatial.transform import Rotation
from iotbx.pdb import hierarchy

def extract_coordinates_from_pdb(pdbin):
      coordinates = []
      for atom in pdbin.atoms():
          # Extract the coordinates of each atom
          xyz = atom.xyz
          coordinates.append(xyz)
      return coordinates

def perturb_coordinates(
        pdbin,
        seed,
        angle_limit = 1,
        distance_limit = 1):
    np.random.seed(seed)
    def random_orientation(coordinates, angle_limit):
       # Calculate the center of mass
       center_of_mass = np.mean(coordinates, axis=0)
       # Subtract the center of mass to make rotations around it
       centered_coordinates = coordinates - center_of_mass
       # Generate a random rotation vector
       rotation_vector = np.random.uniform(-1, 1, size=3)
       # Normalize the rotation vector
       rotation_vector /= np.linalg.norm(rotation_vector)
       # Generate a random rotation angle within the limit
       rotation_angle = np.random.uniform(-angle_limit, angle_limit)
       # Create a rotation matrix
       if rotation_angle <= 0.0001: #pseudo-identity, unstable
          return coordinates,0
       rotation_matrix = Rotation.from_rotvec(rotation_vector * rotation_angle,degrees=True).as_matrix()
       # Apply the rotation to the centered coordinates
       rotated_coordinates = np.dot(centered_coordinates, rotation_matrix.T) + center_of_mass
       return rotated_coordinates,rotation_angle
    def random_translation(coordinates, distance_limit):
      translation_vector = np.random.uniform(-1, 1, size=3)
      translation_vector /= np.linalg.norm(translation_vector)
      distance = np.random.uniform(0, distance_limit, size=1)
      translation_vector = distance*translation_vector
      delta_translation = np.linalg.norm(translation_vector)
      # Add the translation vector to each coordinate
      translated_coordinates = coordinates + translation_vector
      return translated_coordinates,delta_translation
    def update_coordinates_in_pdb(pdbin, new_coordinates):
       # Iterate through the atoms in the hierarchy and update their coordinates
      for atom, new_coord in zip(pdbin.atoms(), new_coordinates):
        atom.xyz = new_coord
    new_coordinates = extract_coordinates_from_pdb(pdbin)
    new_coordinates,delta_orientation = random_orientation(new_coordinates, angle_limit)
    new_coordinates,delta_translation = random_translation(new_coordinates, distance_limit)
    update_coordinates_in_pdb(pdbin, new_coordinates)
    return pdbin,delta_orientation,delta_translation


def rmsd(pdb1, pdb2):
    coordinates1 = extract_coordinates_from_pdb(pdb1)
    coordinates2 = extract_coordinates_from_pdb(pdb2)
    # Center the coordinates
    center1 = np.mean(coordinates1, axis=0)
    center2 = np.mean(coordinates2, axis=0)
    centered1 = coordinates1 - center1
    centered2 = coordinates2 - center2
    # Compute the optimal rotation matrix using Kabsch algorithm
    rmsd = np.sqrt(np.mean(np.sum((centered1 - centered2) ** 2, axis=1)))
    if rmsd>=10: #two decimal places
      return round(rmsd*1000.)/1000.
    return round(rmsd*100.)/100.

def pdbset(xyzin=None,
           stem=None,
           preset=None, #one of the fixed functionality options, chain split, perturb etc
           tout=False,
           write_files=False,
           seed=0,
           angle_limit=1,
           distance_limit=1):
  import time
# seed = int(time.time())
  #either a set of residue-wise pdb files
  #or one pdb file with perturbed coordinates by chain
  txtout = ""
  filenames = []
  tags = []
 #txtout = txtout + str(xyzin) + "\n"
  if tout:
    print("File in:   ",xyzin)
  if xyzin is not None and stem is not None:
      if stem.endswith(".pdb"):
        stem = stem[:-4]
      xyzout = stem + ".pdb"
      if tout:
        print("Stem:   ",stem)
      pdb_in = hierarchy.input(file_name=xyzin)
      crystal_symmetry = pdb_in.input.crystal_symmetry()
      sel_cache = pdb_in.hierarchy.atom_selection_cache()
      working_sel = sel_cache.selection("not hetero and not water and not element H")
      pdb_in = pdb_in.hierarchy.select(working_sel)
      #select the not hetero first so that odd stuff in chain doesn't exclude the chain, eg nag
      #do the selection first so that the number of atoms at the end for the rmsd match what is perturbed
      if True:
        selection = "not hetero and not water and not element H"
      # selection=selection+"model " + str(pdb_in.models()[0].id)
        if tout:
          print("Models: ",len(pdb_in.models()))
        if len(pdb_in.models()) > 1: #can be blank in pdb file
          if len(pdb_in.models()[0].id) > 0: #can be blank in pdb file, paranoia
            selection = selection + " and model " + str(pdb_in.models()[0].id)
        for chain in pdb_in.models()[0].chains():
          main_conf = chain.conformers()[0]
          good_chain_type = (main_conf.is_na() or main_conf.is_protein())
          if not good_chain_type:
            selection = selection + " and not chain " + chain.id
        pdb_in = hierarchy.input(file_name=xyzin)
        sel_cache = pdb_in.hierarchy.atom_selection_cache()
        working_sel = sel_cache.selection(selection)
        pdb_in = pdb_in.hierarchy.select(working_sel)
        if tout:
          print("Selection: ",selection)
        assert(len(pdb_in.models()))
        assert(len(pdb_in.atoms()))
      if preset is None:
        f = open(xyzout, "w")
        f.write(pdb_in.as_pdb_string( crystal_symmetry=crystal_symmetry))
        f.close()
        txtout = txtout + xyzout + "\n"
        if tout:
          print("File out:  ",xyzout)
      else:
        if preset.startswith('perturb_chains'):
          txtout = txtout + "Seed = " + str(seed) + "\n"
          #create hierarchy with one empty model
          new_perturb = iotbx.pdb.hierarchy.root()
          mm_perturb = iotbx.pdb.hierarchy.model()
          new_perturb.append_model(mm_perturb)
        for ichain,chain in enumerate(pdb_in.models()[0].chains()):
          if preset.startswith('perturb_chains'):
            nres = len(chain.residue_groups())
            #delay writing until the end, perturb the chain not the chain hierarchy
            chain_perturb = chain.detached_copy()
            hashseed = int(seed+hash(chain.id)%1000000+ichain)
            #because with only the input seed all chains are perturbed the same
            #need chain.id so that incremental seeds in different jobs do not permute perturbations through chains
            #ichain is an added hash on the chain.id
            #result is reproducible but random
            chain_perturb,dr,dt = perturb_coordinates(
               chain_perturb,
               hashseed,
               angle_limit=angle_limit,
               distance_limit=distance_limit)
            mm_perturb.append_chain(chain_perturb)
            rms = rmsd(chain,chain_perturb)
            cstr = "{:2s}".format(chain.id)
            nstr = "{:5s}".format(str(nres))
            dr = "{:5.2f} degrees".format(dr)
            dt = "{:6.3f} angstroms".format(dt)
            msg = str("Chain=\"" + cstr + "\" nres=" + nstr+ " shift=" + str(dr) +"/"+ str(dt)+ " centred-rmsd=" +  str(rms))
            txtout = txtout + msg + "\n"
            if tout:
              print(msg)
            if preset == 'perturb_chains':
              #store the individual chains
              chainh = chain.as_new_hierarchy()
              pdbout = stem + "_" + chain.id + ".pdb"
              tag="perturbed_" + chain.id.strip()
              pdbstr = chainh.as_pdb_string( crystal_symmetry=crystal_symmetry)
              filenames.append(tag + ":unique_separator\n" + pdbstr) #to keep in memory for esm without writing files
          elif preset == 'split_chains':
            #write the individual chains
            chainh = chain.as_new_hierarchy()
            pdbout = stem + "_" + chain.id + ".pdb"
            f = open(pdbout, "w")
            f.write(chainh.as_pdb_string( crystal_symmetry=crystal_symmetry))
            f.close()
            filenames.append(pdbout)
            nres = len(chain.residue_groups())
            msg = ("Chain \"" + chain.id + "\" number of residues=" + str(nres))
            tags.append(msg)
            txtout = txtout + msg + "\n"
            if tout:
              print("File out:  ",pdbout)
          elif preset == 'split_residues':
            ires = 0
            for rg in chain.residue_groups():
              ires += 1
              new_h = iotbx.pdb.hierarchy.root()
              mm = iotbx.pdb.hierarchy.model()
              new_h.append_model(mm)
              cc = iotbx.pdb.hierarchy.chain(chain.id)
              mm.append_chain(cc)
              cc.append_residue_group(rg.detached_copy())
              nstr = "{:05d}".format(ires)
              pdbout = stem + "_" + chain.id + "_" + str(nstr) + ".pdb"
              if (new_h.atoms_size()):
                msg = str("#"+str(ires)+" chain="+chain.id+" resid="+str(rg.resseq_as_int()) + " atoms=" + str(len(rg.atoms())))
                txtout = txtout + msg + "\n"
                pdbstr = new_h.as_pdb_string( crystal_symmetry=crystal_symmetry)
                if write_files:
                  f = open(pdbout, "w")
                  f.write(pdbstr)
                  f.close()
                  filenames.append(pdbout)
                  txtout = txtout + pdbout + "\n"
                  if tout:
                    print("File out:  ",pdbout)
                else:
                  tag=os.path.basename(stem) + "_" + chain.id + "_" + str(nstr)
                  filenames.append(tag + ":unique_separator\n" + pdbstr) #to keep in memory, for esm without writing files
                tags.append(msg)
                if tout:
                  print(msg)
      if preset.startswith('perturb_chains_single_model'):
        wf = False
        if wf:
          pdbout = stem + "_perturb.pdb"
          f = open(pdbout, "w")
          f.write(new_perturb.as_pdb_string( crystal_symmetry=crystal_symmetry))
          f.close()
          filenames.append(pdbout)
          txtout = txtout + pdbout + "\n"
          print("File out:  ",pdbout)
        else:
          filenames.append(new_perturb.as_pdb_string( crystal_symmetry=crystal_symmetry))
      if preset.startswith('perturb_chains'):
        rms = rmsd(pdb_in,new_perturb)
        msg = str("Overall Rmsd=" + str(rms))
        tags.append(msg)
        txtout = txtout + msg + "\n"
        if tout:
          print(str(msg))
  return filenames,tags,txtout


if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-i','--xyzin',help="input file",type=str,required=True)
  parser.add_argument('-o','--xyzout',help="output pdb file or output file stem",type=str,required=True)
  args = parser.parse_args()
  pdbset(xyzin=args.xyzin,
         stem=args.xyzout,
         preset="split_chains",
         write_files=True,
         tout=True,
        )
