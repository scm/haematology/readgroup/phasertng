//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_mtzcol_class__
#define __phasertng_type_mtzcol_class__
#include <phasertng/type/optional.h>
#include <phasertng/enum/labin_col.h>

namespace phasertng {
namespace type {

class mtzcol : public optional
{
  char        coltype = ' ';
  std::string internal = "";
  std::string labincol = "";
  std::string external = "";

  public:
  mtzcol(
      const std::string parameter = "",
      const std::string coltype_= "",
      const std::string internal_ = "", // name for output including special chars e.g. F(+)
      const std::string labincol_ = "" //handle for labin::col e.g. FPOS
  );

  mtzcol(
      const char coltype_,
      const std::string internal_, // name for output including special chars e.g. F(+)
      const labin::col labincol_ // handle for labin::col e.g. FPOS
  );

  void set_value(std::string other);
  void set_value(const std::vector<std::string>& vcards,const std::string& other);
  bool is_default() const;
  bool is_not_set() const;
  void set_default();
  std::string name() const; //alias
  type::mtzcol value_or_default() const;
  std::string type_str() const;
  char type() const;
  std::string labin_col() const;
  labin::col enumeration() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::mtzcol& other) const
  { return name() == other.name(); }

};

}}
#endif
