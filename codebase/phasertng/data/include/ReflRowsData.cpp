//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/ReflRowsData.h>
#include <phasertng/data/ReflColsMap.h>

namespace phasertng {

  void
  ReflRowsData::copy_reflections(const ReflColsMap& refl)
  { copy_and_reformat_data(&refl); } //for python

  ReflRowsData::ReflRowsData(int nrefl) : Reflections()
  { set_base_defaults(); setup_miller(nrefl); }

  void
  ReflRowsData::set_base_defaults()
  {
    LABIN_ALLOWED = {
              labin::INAT,
              labin::SIGINAT,
              labin::FNAT,
              labin::SIGFNAT,
              labin::FEFFNAT,
              labin::DOBSNAT,
              labin::ANISOBETA,
              labin::WLL,
              labin::TEPS,
              labin::TBIN,
              labin::RESN,
              labin::SSQR,
              labin::INFO,
              labin::EINFO,
              labin::EPS,
              labin::BIN,
              labin::CENT,
            };
  }

  void
  ReflRowsData::set_miller(const millnx v,const int r)
  {
    ROW[r].miller = v;
  }

  void
  ReflRowsData::set_present(friedel::type f,const bool v,const int r)
  {
    if (f == friedel::UNDEFINED) return;
    if (f == friedel::NAT) ROW[r].present = v;
    else throw Error(err::DEVELOPER,"Disallowed friedel");
  }

  void
  ReflRowsData::setup_mtzcol(type::mtzcol col)
  {
    if (NREFL == 0) throw Error(err::DEVELOPER,"Miller array size has not been set");
    if (!LABIN_ALLOWED.count(col.enumeration()))
      throw Error(err::DEVELOPER,"Disallowed column " + col.labin_col());
    CreateLabin(col);
    //all are NAT, present is part of row data structure,
    friedel::type f = convert_labin_to_friedel(col.enumeration());
    FRIEDEL.insert(f);
  }

  void
  ReflRowsData::setup_miller(int nrefl)
  {
    NREFL = nrefl;
    ROW.resize(NREFL);
  }

  void
  ReflRowsData::erase_from_reflection(int nrefl)
  {
    NREFL = nrefl;
    ROW.erase(ROW.begin()+nrefl,ROW.end());
  }

  void
  ReflRowsData::set_flt(labin::col col,const double value,const int r)
  {
    reflection::ReflRowData* work_row = &ROW[r];
         if (col == labin::INAT) work_row->inat = value;
    else if (col == labin::SIGINAT) work_row->siginat = value;
    else if (col == labin::FNAT) work_row->fnat = value;
    else if (col == labin::SIGFNAT) work_row->sigfnat = value;
    else if (col == labin::FEFFNAT) work_row->feffnat = value;
    else if (col == labin::DOBSNAT) work_row->dobsnat = value;
    else if (col == labin::ANISOBETA) work_row->anisobeta = value;
    else if (col == labin::WLL) work_row->wll = value;
    else if (col == labin::TEPS) work_row->teps = value;
    else if (col == labin::EPS) work_row->eps = value;
    else if (col == labin::TBIN) work_row->tbin = value;
    else if (col == labin::RESN) work_row->resn = value;
    else if (col == labin::SSQR) work_row->ssqr = value;
    else if (col == labin::INFO) work_row->info = value;
    else if (col == labin::EINFO) work_row->einfo = value;
    else throw Error(err::DEVELOPER,"ReflRowsData: Tried to set_flt(" + labin::col2String(col) + ") ");
  }

  void
  ReflRowsData::set_int(labin::col col,const int value,const int r)
  {
    reflection::ReflRowData* work_row = &ROW[r];
         if (col == labin::BIN) work_row->bin = value;
    else throw Error(err::DEVELOPER,"ReflRowsData: Tried to set_int(" + labin::col2String(col) + ") ");
  }

  void
  ReflRowsData::set_bln(labin::col col,const bool value,const int r)
  {
    reflection::ReflRowData* work_row = &ROW[r];
    if (col == labin::CENT) work_row->cent = value;
    else throw Error(err::DEVELOPER,"ReflRowsData: Tried to set_bln(" + labin::col2String(col) + ") ");
  }

  const bool
  ReflRowsData::get_present(friedel::type f,const int r) const
  {
    if (f == friedel::UNDEFINED) return true;
    if (f == friedel::NAT) return ROW[r].present;
    throw Error(err::DEVELOPER,"ReflRowsData: Tried to get_present(" + friedel::type2String(f) + ") ");
  }

  const millnx
  ReflRowsData::get_miller(const int r) const
  {
    return ROW[r].miller;
  }

  const double
  ReflRowsData::get_flt(labin::col col,const int r) const
  {
    const reflection::ReflRowData* work_row = &ROW[r];
    if (col == labin::INAT) return work_row->inat;
    if (col == labin::SIGINAT) return work_row->siginat;
    if (col == labin::FNAT) return work_row->fnat;
    if (col == labin::SIGFNAT) return work_row->sigfnat;
    if (col == labin::FEFFNAT) return work_row->feffnat;
    if (col == labin::DOBSNAT) return work_row->dobsnat;
    if (col == labin::ANISOBETA) return work_row->anisobeta;
    if (col == labin::WLL) return work_row->wll;
    if (col == labin::TEPS) return work_row->teps;
    if (col == labin::EPS) return work_row->eps;
    if (col == labin::TBIN) return work_row->tbin;
    if (col == labin::RESN) return work_row->resn;
    if (col == labin::SSQR) return work_row->ssqr;
    if (col == labin::INFO) return work_row->info;
    if (col == labin::EINFO) return work_row->einfo;
    throw Error(err::DEVELOPER,"ReflRowsData: Catch this get_flt(" + labin::col2String(col) +")");
    return DEF_ONE;
  }

  const int ReflRowsData::get_int(labin::col col,const int r) const
  {
    const reflection::ReflRowData* work_row = &ROW[r];
    if (col == labin::BIN) return work_row->bin;
    throw Error(err::DEVELOPER,"ReflRowsData: Catch this get_int(" + labin::col2String(col) +")");
    return default_int;
  }

  const bool
  ReflRowsData::get_bln(labin::col col,const int r) const
  {
    const reflection::ReflRowData* work_row = &ROW[r];
    if (col == labin::CENT) return work_row->cent;
    throw Error(err::DEVELOPER,"ReflRowsData: Catch this get_bln(" + labin::col2String(col) +")");
    return default_bln;
  }

}
