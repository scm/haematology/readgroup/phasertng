//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineTNCS_class__
#define __phasertng_RefineTNCS_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/includes.h>
#include <phasertng/math/likelihood/pIntensity.h>
#include <utility>

namespace phasertng {

typedef std::pair<TargetGradientHessianType,sv_double>  TargetGradientHessianTncsType;

class RefineTNCS : public dtmin::RefineBase
{
    math::pIntensity pintensity;

  private: //members
    double INIT_GFNRAD = 0;
    double INIT_RMS = 0;
    dvect3 INIT_VECTOR = {0,0,0};
    double INIT_ORDER = 0; //constant
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;
    double LLG = 0;

  public:
    double SIGMA_ROT = 0;
    double SIGMA_TRA = 0;
    double SIGMA_GFNRAD = 0;
    double SIGMA_RMSD = 0;
    double SIGMA_FS = 0;
    double SIGMA_BINS = 0;
    double SIGMA_SMOOTH = 0;

  public: //getter
    sv_double arrayTEPS;
    double min_fs();

  public: //members
    const_ReflectionsPtr  REFLECTIONS;
    SelectedPtr SELECTED;
    EpsilonPtr  EPSILON; //this is where all the parameters that change are stored

  public:  //constructor
    RefineTNCS() : dtmin::RefineBase() {}
    RefineTNCS(
        const_ReflectionsPtr,
        SelectedPtr,
        EpsilonPtr
        );
    ~RefineTNCS() throw() {}

  public:
    TargetGradientHessianTncsType
    parallel_combined_tgh(
      const bool&, //gradient
      const bool&, //hessian
      const int&, //beginning
      const int& //end
    ) const;

    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
      );

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    likelihood();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string reject_outliers();
    af_string finalize_parameters();

  public:
    af_string logNcsEpsn(std::string,bool=false);
    std::tuple<Epsilon,sv_double,double> parameters();
    sv_double labin_wll;

  public:
    void set_threading(bool,int);
    void setup_wll();
};

} //phasertng
#endif
