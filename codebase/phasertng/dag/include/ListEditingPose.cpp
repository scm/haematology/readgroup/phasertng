//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/dag/DuplicateData.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/euler_angles.h>
#include <future>

//#define DEBUG_PHASERTNG_LISTEDITPOSE
//#define DEBUG_PHASERTNG_CELLSHIFTS

namespace phasertng {
namespace dag {

  //ListEditing
  int
  DagDatabase::calculate_duplicate_poses(
      double dmin,
      bool is_a_map,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = size();
    // Compare solutions to find duplicates further down list
    bool all_have_pose(true);
    for (int k = 0; k < NODES.size(); k++)
    {
      NODES[k].generic_flag = true;
      NODES[k].EQUIV = k;
      if (NODES[k].POSE.size() == 0)
        all_have_pose = false;
    }
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose not all_have_pose=" << btoch(!all_have_pose) << std::endl;
#endif
    if (!all_have_pose)
      return 0;

    HallDuplicateData map_data2;
    bool is_atom(false);
    for (int k = 0; k < tsize; k++)
    {
      dag::Node& node_k = NODES[k];
      if (!map_data2.count(node_k.HALL))
        map_data2[node_k.HALL] = dag::DuplicateData(node_k,false);
    }

    for (int k = 0; k < tsize; k++)
    {
      duplicate_poses_combine(map_data2,dmin,k,is_a_map,USE_STRICTLY_NTHREADS,NTHREADS);
    }

    //after paralled section, when all KEEP is flagged
    for (int k = 0; k < NODES.size(); k++)
      NODES[k].NUM = -999;
    //first pass for known
    int na(1);
    for (int k = 0; k < NODES.size(); k++)
      if (NODES[k].generic_flag)
        NODES[k].NUM = na++; //output number equivalent
    bool all_numbered(true);
    int count_loop(0);
    do
    {
      all_numbered = true;
      for (int k = 0; k < NODES.size(); k++)
        if (!NODES[k].generic_flag) //if this is equivalent to something
          NODES[k].NUM = NODES[NODES[k].EQUIV].NUM; //may be negative
      for (int k = 0; k < NODES.size(); k++)
        if (NODES[k].NUM < 0)
          all_numbered = false; //flag that we are not yet done
      PHASER_ASSERT(count_loop++ < 100); //infinite loop check
    }
    while (!all_numbered);

    int count(0);
    for (int k = 0; k < NODES.size(); k++)
    {
      if (!NODES[k].generic_flag) count++;
    }
    return count;
  }

  void
  DagDatabase::duplicate_poses_combine(
      HallDuplicateData map_data2,
      double dmin,
      int k,
      bool is_a_map,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
   // if (k > 1) return; //limit the debugging output for speed
#endif
    int tsize = NODES.size();
    int nchk = tsize - k;
    int minimum_nchk_for_parallel(1000); //must be this to make it worthwhile AJM TODO run tests
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
NTHREADS = 1;
#endif
    if (NTHREADS == 1)
    {
      int beg = k+1;
      int end = tsize;
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose here=" << itos(end) << std::endl;
#endif
      duplicate_poses_parallel(map_data2,dmin,k,is_a_map,beg,end);
    }
    else if (nchk < minimum_nchk_for_parallel)// because the granularity is too small
    {
      int beg = k+1;
      int end = tsize;
      duplicate_poses_parallel(map_data2,dmin,k,is_a_map,beg,end);
    }
    else
    {
      std::vector< std::future< void > > results;
      std::vector< std::packaged_task< void(HallDuplicateData,double,int,bool,int,int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      int grain = (nchk/NTHREADS)+1; //granularity of threading, +1 makes the last thread the shortest
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = k+1 + t*grain; //start of thread reflection
        int end = std::min(k+1 + (t+1)*grain,tsize)-1; //end of thread
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(
            &DagDatabase::duplicate_poses_parallel, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              map_data2,
              dmin,
              k,
              is_a_map,
              beg,
              end  );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async,
            &DagDatabase::duplicate_poses_parallel, this,
              map_data2,
              dmin,
              k,
              is_a_map,
              beg,
              end  ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
       int beg = k+1 + nthreads_1*grain; //start of thread reflection
       int end = std::min(k+1 + (nthreads_1+1)*grain,tsize)-1; //end of thread
       duplicate_poses_parallel(
              map_data2,
              dmin,
              k,
              is_a_map,
              beg,
              end  );
       //rot_prev will remain -999 if nothing was found
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
      }
    }
  }

  void
  DagDatabase::duplicate_poses_parallel(
      HallDuplicateData map_data2,
      double dmin,
      int k,
      bool is_a_map,
      int beg,int end)
  {
#ifdef DEBUG_PHASERTNG_CELLSHIFTS
    auto AddCell = [](dvect3 site) //Cool! Inline function! but does not throw!!
    {
      af_dvect3 sitelist;
      dvect3 cellTrans(0,0,0);
      for (int j = 0; j < 3; j++) {
        while (site[j] < -0.5) site[j] += 1;
        while (site[j] >= 0.5) site[j] -= 1;
      }
      for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
      for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
      for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
        sitelist.push_back(site + cellTrans);
      return sitelist;
    };
#endif

    dag::Node& node_k = NODES[k];
    double transMax = dmin/5.0;
    DuplicateData& data2 = map_data2[node_k.HALL];
    auto Euclid_orthRtrfrac = data2.Euclid_Rmat(node_k.CELL);
    dmat33 orthmat_k = node_k.CELL.orthogonalization_matrix();
    dmat33 fracmat_k = node_k.CELL.fractionalization_matrix();
    std::vector<Identifier> comp1;
    for (int s = 0; s < node_k.POSE.size(); s++)
      comp1.push_back(node_k.POSE[s].IDENTIFIER);
    std::sort(comp1.begin(),comp1.end());
    std::map<Identifier,double> maprotMax;
    for (int s = 0; s < node_k.POSE.size(); s++)
      if (node_k.POSE[s].ENTRY->ATOM) //not an atom
      maprotMax[node_k.POSE[s].IDENTIFIER] = 0;
      else
      maprotMax[node_k.POSE[s].IDENTIFIER] = 2.0*std::atan(dmin/(4.*node_k.POSE[s].ENTRY->MEAN_RADIUS));
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose k=" << (k) << std::endl;
#endif
    for (int k_chk = beg; k_chk < end; k_chk++) //check those further down the list
    {
      dag::Node& node_k_chk = NODES[k_chk];
      dmat33 orthmat_kchk = node_k_chk.CELL.orthogonalization_matrix();
      dmat33 fracmat_kchk = node_k_chk.CELL.fractionalization_matrix();
      if (!node_k_chk.generic_flag) continue;
      // skip comparison if different space groups
      bool different_space_group(node_k_chk.HALL != node_k.HALL);
      if (different_space_group) continue;
      // skip comparison if different composition
      int nmodels = node_k.POSE.size();

      // skip comparison if not the same number of models in trial solutions
      bool different_size(node_k_chk.POSE.size() != nmodels);
      if (different_size) continue;
      std::vector<Identifier> comp2;
      for (int s = 0; s < nmodels; s++)
        comp2.push_back(node_k.POSE[s].IDENTIFIER);
      std::sort(comp2.begin(),comp2.end());
      bool different_comp(comp1 != comp2);
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose k=" << k<< " k_chk=" << k_chk<< " different_comp=" << btoch(different_comp) << std::endl;
#endif
      if (different_comp) continue;

      sv_bool used(nmodels,false);
      bool matched(true);
      bool allmatched(false),originDefined(false);
      dvect3 zero3(0,0,0), originShift(0,0,0);
      int isymEuclid(-1);
      // Store some information about first model in reference solution
      dag::Pose& pose0 = node_k.POSE[0];
      Identifier& firstmodlid = pose0.IDENTIFIER;
      dmat33 solR_first;
      dvect3 fracSolC_first;
      {
      dvect3 fracT; std::tie(solR_first,fracT) = pose0.finalRT(orthmat_k,fracmat_k);
      dvect3 orthSolC(solR_first*pose0.ENTRY->CENTRE + orthmat_k*fracT);
      fracSolC_first = fracmat_k*orthSolC;
      }
      double rotMax = maprotMax[firstmodlid];
      // Find possible choices of origin shift of checked solution that make a match with first reference model
      for (int s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
      {
        allmatched = false;
        originDefined = false;
        originShift = zero3;

        dag::Pose& pose_k_chk_s_chk_ori = node_k_chk.POSE[s_chk_ori];
        bool different_id(pose_k_chk_s_chk_ori.IDENTIFIER != firstmodlid);
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose k_chk=" << k_chk << " s_chk_ori=" << s_chk_ori << " different_id=" << btoch(different_id) << std::endl;
std::cout << "debug listeditingpose k_chk euler=" << dvtos(pose_k_chk_s_chk_ori.EULER,6,1) << " fract=" << dvtos(pose_k_chk_s_chk_ori.FRACT,6,3) << std::endl;
#endif
        if (different_id) continue;
        dmat33 solR_chk;
        dvect3 fracSolC_chk;
        {
        dvect3 fracT_chk_ori;
        std::tie(solR_chk,fracT_chk_ori) = pose_k_chk_s_chk_ori.finalRT(orthmat_kchk,fracmat_kchk);
        dvect3 orthSolC_chk = solR_chk*pose_k_chk_s_chk_ori.ENTRY->CENTRE + orthmat_kchk*fracT_chk_ori;
        fracSolC_chk = (fracmat_kchk*orthSolC_chk);
        }

        double ONE(1);
        for (int isym = 0; isym < data2.Euclid.group().order_z(); isym++)
        {
          // Check that orientations are sufficiently similar
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          dmat33 solR_chk_sym = Euclid_orthRtrfrac[isym]*solR_chk;
          //Identifier& modlid = pose_k_s_chk_ori.IDENTIFIER;
          sv_dvect3 get_euler = pose_k_chk_s_chk_ori.ENTRY->POINT_GROUP_EULER;
          int multiplicity(get_euler.size());
          for (int ensym = 0; ensym < multiplicity; ensym++)
          {
            dmat33 ensymR = zyz_matrix(get_euler[ensym]);
            dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
            double rotdist = matrix_delta_rad(solR_first,solR_chk_sym_ensym);
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose k_chk=" << k_chk << " s_chk_ori=" << s_chk_ori << " isym=" << isym << " ensym=" << ensym << " continue=" << btoch(rotdist > rotMax) <<  std::endl;
#endif
            if (rotdist > rotMax) continue;

            // Check that centres of mass are sufficiently similar
            // Centre of mass of ensemble won't change with application
            // of point group symmetry.
            dvect3 fracSolC_chk_sym = data2.Euclid.doSymXYZ(isym,fracSolC_chk);
            double tdist(std::numeric_limits<double>::max());
            if (!is_a_map)
            { //old code where all the unit cells are the same
              dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
#ifndef DEBUG_PHASERTNG_CELLSHIFTS
              for (int j = 0; j < 3; j++)
              {
                while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
                while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
              }
              // Account for any continuous shifts as origin shift
              dvect3 fracdelta_reference = data2.cb_op_r_to_reference_setting * fracdelta;
              dvect3 originShift_reference(0,0,0);
              for (int j = 0; j < 3; j++) {
                if (!is_a_map and data2.isshift[j]) {
                  originShift_reference[j] = -fracdelta_reference[j];
                  fracdelta_reference[j] = 0;
                }
              }
              fracdelta = data2.cb_op_r_from_reference_setting*fracdelta_reference;
              originShift = data2.cb_op_r_from_reference_setting*originShift_reference;
              dvect3 delta = orthmat_k*fracdelta;
              tdist = delta.length();
#else
              af_dvect3 symmetry = AddCell(fracdelta);
              for (int isym = 0; isym < symmetry.size(); isym++)
              {
                // Account for any continuous shifts as origin shift
                dvect3 fracdelta_reference = data2.cb_op_r_to_reference_setting * symmetry[isym];
                dvect3 originShift_reference(0,0,0);
                for (int j = 0; j < 3; j++) {
                  if (!is_a_map and data2.isshift[j]) {
                    originShift_reference[j] = -fracdelta_reference[j];
                    fracdelta_reference[j] = 0;
                  }
                }
                fracdelta = data2.cb_op_r_from_reference_setting*fracdelta_reference;
                originShift = data2.cb_op_r_from_reference_setting*originShift_reference;
                dvect3 delta = orthmat_k*symmetry[isym];
                tdist = std::min(tdist,delta.length());
                if (tdist <= transMax)
                  break;
              }
#endif
            }
            else
            { //use the in different orthmat for each
              dvect3 orthSolC_first = orthmat_k*fracSolC_first;
              dvect3 orthSolC_chk_sym = orthmat_kchk*fracSolC_chk_sym;
              dvect3 delta = orthSolC_first - orthSolC_chk_sym;
              tdist = delta.length();
            }
            if (tdist <= transMax)
            {
              isymEuclid = isym;
              originDefined = true;
              break;
            }
          } // ensym
          if (originDefined) break;
        } // isym
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose origin defined continue=" << btoch(!originDefined) << std::endl;
#endif
        if (!originDefined) continue; // Test different origins

        // Now use choice of relative origin (if defined) to test equivalence
        allmatched = true; //So far at least one match.  Negate if one fails.
        for (int s = 0; s < nmodels; s++)
        { // For each model in the reference, check whether any matching models are the same
          // Determine sensible distance thresholds from dmin and model mean radius
          dag::Pose& pose_k_s = node_k.POSE[s];
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose pose_k_s k=" << k << " s=" <<s << " euler=" << dvtos(pose_k_s.EULER,6,1) << " fract=" << dvtos(pose_k_s.FRACT,6,3) << std::endl;
#endif
          dmat33 solR;
          dvect3 fracSolC;
          {
          dvect3 fracT; std::tie(solR,fracT) = pose_k_s.finalRT(orthmat_k,fracmat_k);
          dvect3 orthSolC = solR*pose_k_s.ENTRY->CENTRE + orthmat_k*fracT;
          fracSolC = fracmat_k*orthSolC;
          }
          Identifier& modlid = pose_k_s.IDENTIFIER;
          matched = false;
          for (int s_chk = 0; s_chk < nmodels; s_chk++)
          {
            dag::Pose& pose_k_chk_s_chk = node_k_chk.POSE[s_chk];
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose pose_k_chk_s_chk euler=" << dvtos(pose_k_chk_s_chk.EULER,6,1) << " fract=" << dvtos(pose_k_chk_s_chk.FRACT,6,3) << std::endl;
#endif
            if (used[s_chk] or pose_k_chk_s_chk.IDENTIFIER != modlid) continue;
            dmat33 solR_chk;
            dvect3 fracSolC_chk;
            {
            dvect3 fracT; std::tie(solR_chk,fracT) = pose_k_chk_s_chk.finalRT(orthmat_kchk,fracmat_kchk);
            dvect3 orthSolC_chk = solR_chk*pose_k_chk_s_chk.ENTRY->CENTRE + orthmat_kchk*fracT;
            fracSolC_chk = fracmat_kchk*orthSolC_chk;
            }
            if (originDefined)
            {
              //  Apply operation selected for first molecule from space group expanded to include allowed origin shifts
              solR_chk = orthmat_k*data2.Euclid.rotsym[isymEuclid].transpose()*fracmat_k*solR_chk;
              fracSolC_chk = data2.Euclid.doSymXYZ(isymEuclid,fracSolC_chk);
            }

            SpaceGroup* SG = !originDefined ? &data2.Euclid : node_k.SG;
            double ONE(1);
            for (int isym = 0; isym < SG->group().order_z(); isym++)
            {
              // Check that orientations are sufficiently similar
              dmat33 Rotsym_chk = SG->rotsym[isym];
              dmat33 solR_chk_sym = orthmat_kchk*Rotsym_chk.transpose()*fracmat_kchk*solR_chk;
              sv_dvect3 get_euler = node_k_chk.POSE[s_chk].ENTRY->POINT_GROUP_EULER;
              int multiplicity(get_euler.size());
              for (int ensym = 0; ensym < multiplicity; ensym++)
              {
                dmat33 ensymR = zyz_matrix(get_euler[ensym]);
                dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
                double rotdist = matrix_delta_rad(solR,solR_chk_sym_ensym);
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose s=" << s << " s_chk=" << s_chk << " isym=" << isym << " ensym=" << ensym << " continue=" << btoch(rotdist > rotMax) << " rot:" << rotdist << " > " << rotMax << " rad" << std::endl;
#endif
                if (rotdist > rotMax) continue;

                double tdist(std::numeric_limits<double>::max());
                dvect3 fracSolC_chk_sym = SG->doSymXYZ(isym,fracSolC_chk);
                if (!is_a_map)
                {
                  // Check that centres of mass are
                  // sufficiently similar. Centre of mass of ensemble won't
                  // change with application of point group symmetry.
                  dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
#ifndef DEBUG_PHASERTNG_CELLSHIFTS
                  for (int j = 0; j < 3; j++)
                  {
                    while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
                    while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
                  }
                  dvect3 delta = orthmat_k*fracdelta;
                  tdist = delta.length();
#else
                  af_dvect3 symmetry = AddCell(fracdelta);
                  for (int isym = 0; isym < symmetry.size(); isym++)
                  {
                    dvect3 delta = orthmat_k*symmetry[isym];
                    tdist = std::min(tdist,delta.length());
                    if (tdist <= transMax)
                      break;
                  }
#endif
                }
                else
                { //use the in different orthmat for each
                  dvect3 orthSolC = orthmat_k*fracSolC;
                  dvect3 orthSolC_chk_sym = orthmat_kchk*fracSolC_chk_sym;
                  dvect3 delta = orthSolC - orthSolC_chk_sym;
                  tdist = delta.length();
                }
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose s=" << s << " s_chk=" << s_chk << " isym=" << isym << " ensym=" << ensym << " continue=" << btoch(tdist > transMax) << " tra:" << tdist << " > " << transMax <<std::endl;
#endif
                if (tdist > transMax) continue;
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose s=" << s << " s_chk=" << s_chk << " isym=" << isym << " ensym=" << ensym << " matched=" << btoch(true) << std::endl;
#endif
                matched = true;
                used[s_chk] = true;
                if (matched) break;
              } // ensym
              if (matched) break;
            } // isym
            if (matched) break;
          } // s_chk
          if (!matched) allmatched = false;
        } // s
        if (allmatched) break;
        // Failed to match everything.  Try again with another possible origin shift.
        for (int m = 0; m < nmodels; m++)
          used[m] = false;
      } // s_chk_ori
#ifdef DEBUG_PHASERTNG_LISTEDITPOSE
std::cout << "debug listeditingpose k_chk= " << k_chk << " allmatched=" << btoch(allmatched) << std::endl;
#endif
      if (allmatched)
      {
        node_k_chk.generic_flag = false;
        node_k_chk.EQUIV = k;
        //replace the kept TFZ with the TFZ highest from those found to be equivalent
        node_k.ZSCORE = std::max(node_k_chk.ZSCORE,node_k.ZSCORE);
        //replace the kept RFACTOR
        node_k.RFACTOR = std::max(node_k_chk.RFACTOR,node_k.RFACTOR);
      }
    } //k_chk
  }

}}
