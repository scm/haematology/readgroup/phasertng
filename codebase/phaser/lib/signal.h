//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_SIGNAL__
#define __PHASER_SIGNAL__
#include <phaser/main/Phaser.h>

namespace phaser {

inline floatType signal(floatType value,floatType mean,floatType sigma)
{
  if (sigma > 0) return (value-mean)/sigma;
  return 0;
}

}//end namespace phaser

#endif
