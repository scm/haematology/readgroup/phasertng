//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/llgp/function.h>
#include <scitbx/array_family/misc_functions.h>
#include <cmath>
#include <iso646.h>

using namespace scitbx;

namespace phasertng { namespace likelihood { namespace llgp {

  void
  function::target_gradient_hessian(
      const double& DobsSigaSqr,
      const cmplex& EOBS,
      const cmplex& DobsSigaEcalc, //this sums individual SigaEcalc from components
      const double& V_,
      const bool&   CENT,
      const likelihood::llgp::tgh_flags& flags
    )
  {
    // NB: All derivatives wrt EC are actually wrt DobsSiga-weighted EC
    LL = 0;
    dLL_by_dReEC = 0;
    dLL_by_dImEC = 0;
    dLL_by_dV = 0;
    d2LL_by_dReEC2 = 0;
    d2LL_by_dImEC2 = 0;
    d2LL_by_dReEC_dImEC = 0;
    d2LL_by_dReEC_dV = 0;
    d2LL_by_dImEC_dV = 0;
    d2LL_by_dV2 = 0;
    V = V_;

    if (V <= 0) {
      return; // V will be negative and function::negative_variance() will be false
    }

    double EOBS2 = std::norm(EOBS);
    double wEOBS2 = EOBS2 * DobsSigaSqr;
    double wEC2 = std::norm(DobsSigaEcalc);
    double costerm = 2 * std::real(EOBS * std::conj(DobsSigaEcalc));

    // Target, phased log-likelihood-gain
    LL = (costerm - wEOBS2 - wEC2) / V - std::log(V);
    if (CENT)
      LL /= 2.0;
    //gradient
    if (flags.gradient())
    {
      //derivatives
      double ReEobs = EOBS.real();
      double ImEobs = EOBS.imag();
      double ReEC = DobsSigaEcalc.real();
      double ImEC = DobsSigaEcalc.imag();
      //always calculate EC terms, needed for all
      double dEC2_by_dReEC = 2 * ReEC;
      double dEC2_by_dImEC = 2 * ImEC;
      double dcosterm_by_dReEC = 2 * ReEobs;
      double dcosterm_by_dImEC = 2 * ImEobs;
      double V2 = fn::pow2(V);
      dLL_by_dReEC = -(dEC2_by_dReEC - dcosterm_by_dReEC) / V;
      dLL_by_dImEC = -(dEC2_by_dImEC - dcosterm_by_dImEC) / V;
      //numerical limits mean that V > 0 does not imply V2 > 0
      if (flags.variances() and V2 > 0)
      {
        dLL_by_dV = (EOBS2 + wEC2 - costerm) / V2 - 1. / V;
      }
      if (CENT) // Divide these by 2
      {
        dLL_by_dReEC /= 2.;
        dLL_by_dImEC /= 2.;
        dLL_by_dV /= 2.;
      }
      if (flags.hessian())
      {
        d2LL_by_dReEC2 = d2LL_by_dImEC2 = CENT ? -1./V : -2./V;
        // d2LL_by_dReEC_dImEC = 0;
        if (flags.variances())
        {
          double V3 = fn::pow3(V);
          if (V3 > 0)
          {
            d2LL_by_dV2 = 1. / V2 - 2*(EOBS2 + wEC2 - costerm) / V3;
            if (CENT)
              d2LL_by_dV2 /= 2.;
          }
          d2LL_by_dReEC_dV = -dLL_by_dReEC / V; // Already corrected for centric
          d2LL_by_dImEC_dV = -dLL_by_dImEC / V;
        }
      }
    }
  }

}}} //phasertng
