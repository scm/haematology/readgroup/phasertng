//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/ElmnData.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Epsilon.h>
#include <phasertng/pod/fast_types.h>

namespace phasertng {

  af_string
  ElmnData::ELMNxR2(
      double sphereOuter,
      const_ReflectionsPtr REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int nmol,
      bool tncs_modelled,
      double ELMN_RESO,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      double SEARCH_BFAC
   )
  {
    af_string output;
    const double ONE(1.),TWO(2.);

    double HIRES(10000);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r])
      {
        HIRES = std::min(static_cast<double>(REFLECTIONS->UC.cctbxUC.d(REFLECTIONS->get_miller(r))),HIRES);
      }
    }
 //   int NSYMP = REFLECTIONS->SG.group().order_p();

    double V(0),cweight(0);

    HKL_clustered HKL_list;
    if (!ecalcs_sigmaa_pose->precalculated)
    {
      output.push_back("Ecalcs/Sigmaa not precalculated");
      ecalcs_sigmaa_pose->resize(REFLECTIONS->NREFL);
    }
    else
      phaser_assert(ecalcs_sigmaa_pose->size() == REFLECTIONS->NREFL);

    // convert limit to ssqr as we have ssqrs already stored
    double ssqr_elmn_reso_limit = 1/(ELMN_RESO*ELMN_RESO);

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& matrix_Gsqr = EPSILON->matrix_Gsqr;
    bool tncs_not_modelled = !tncs_modelled;
    if (nmol > 1) phaser_assert(array_G_DRMS.size() == REFLECTIONS->NREFL);
    if (nmol == 2) phaser_assert(matrix_Gsqr.size() == REFLECTIONS->NREFL*REFLECTIONS->SG.group().order_p());

    millnx miller(0,0,0);
    double dobs(0),feff(0),resn(0),teps(0),ssqr(0),g_drms(0),Gsqr_Vterm(0),eps(0);
    bool   cent(0);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (SELECTED[r] and ssqr <= ssqr_elmn_reso_limit)
      {
        if (!REFLECTIONS->has_rows())
        {
          miller = REFLECTIONS->get_miller(r);
          dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
          feff =   REFLECTIONS->get_flt(labin::FEFFNAT,r);
          teps =   REFLECTIONS->get_flt(labin::TEPS,r);
          resn =   REFLECTIONS->get_flt(labin::RESN,r);
          cent =   REFLECTIONS->get_bln(labin::CENT,r);
          ssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
          eps  =   REFLECTIONS->get_flt(labin::EPS,r);
          g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        }
        else
        {
          const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
          miller = work_row->get_miller();
          dobs = work_row->get_dobsnat();
          feff = work_row->get_feffnat();
          teps = tncs_modelled ? 1. : work_row->get_teps();
          resn = work_row->get_resn();
          cent = work_row->get_cent();
          ssqr = work_row->get_ssqr();
          eps =  work_row->get_eps();
          g_drms = (nmol > 1 and tncs_not_modelled) ? array_G_DRMS[r] : 1;
        }
        double Esqr = fn::pow2(feff/resn);
        phaser_assert(Esqr >= 0);
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);
        /* EMfixSqr is (sigmaA*Ecalc)^2 for sum of known components,
           i.e. this accounts for fraction of scattering and coordinate error.
           sum_Esqr_known will be zero unless SOLU 3DIM is resurrected.
           variance (V) is SigmaN'/SigmaN from FRF paper:
           SigmaN'/SigmaN = 1 + (sigmaA*Ecalc)^2 - sigmaA^2
           Note that totvar_known is basically sigmaA^2 (possibly corrected by G-function)
           Comparing what is here with equation (18) of the FRF paper, the
           observed coefficients have been multiplied by SigmaN but then the
           calculated coefficients (It term) are computed in terms of E-values, which
           is where the other factor of SigmaN goes.  For the fixed model terms in
           V, any factors of sigmaA include the Dobs factor for the LLGI
           target to account for the effect of measurement error.
           For the moving component, the factor of Dobs squared is applied
           to the normalised intensity coming from Feff, instead of being combined
           with sigmaA weighting of the E values for the model. This is because the
           hkl values do not remain constant for the rotating model. In equation (17)
           of the FRF paper, it's equivalent to apply the factor of Dobs^2 to the
           It (data and fixed model) term rather than trying to associate it with the
           rotating model terms in Is.
        */
        cweight = cent ? ONE : TWO;
        double EMfixSqr(std::norm(ecalcs_sigmaa_pose->ecalcs[r]));
        // Note that sigmaa here includes G-function term if relevant
        V = teps - fn::pow2(ecalcs_sigmaa_pose->sigmaa[r]) + EMfixSqr;
        PHASER_ASSERT(V > 0);
        double intensity = cweight*(Esqr-V)/fn::pow2(V);
        intensity *= fn::pow2(dobs); //this is already included in terms
        for (unsigned isym = 0; isym < rsym.unique; isym++)
        {
          p3Dc hkl;
          hkl.rhkl = rsym.rotMiller[isym];
          int indexh = hkl.rhkl[0];
          int indexk = hkl.rhkl[1];
          int indexl = hkl.rhkl[2];
          dvect3 hklscaled = REFLECTIONS->UC.HKLtoVecS(indexh,indexk,indexl);
          if (HIGHSYMM.second == 'Z')
          {
            hkl.x = hklscaled[0];
            hkl.y = hklscaled[1];
            hkl.z = hklscaled[2];
          }
          else if (HIGHSYMM.second == 'Y')
          {
            hkl.y = hklscaled[0];
            hkl.z = hklscaled[1];
            hkl.x = hklscaled[2];
          }
          else if (HIGHSYMM.second == 'X')
          {
            hkl.z = hklscaled[0];
            hkl.x = hklscaled[1];
            hkl.y = hklscaled[2];
          }
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
          hkl.flipped = false;
#endif
          if (SEARCH_BFAC)
            intensity *= std::exp(-ssqr*SEARCH_BFAC); //minusSsqrOn2,squared->exp(2x)
          hkl.intensity = intensity;
          Gsqr_Vterm = (nmol == 2) ? matrix_Gsqr(r,isym) : 1.;
          hkl.intensity *= g_drms*Gsqr_Vterm;
          hkl.toPolar();
       // if (reso(r) > LMAX_RESO)
            HKL_list.add(hkl);
        }
      }
    }

    phaser_assert(HKL_list.clustered.size());
    //int LMAX = scitbx::constants::two_pi*sphereOuter/ELMN_RESO;
    //int two_pi_b = LMAX * ELMN_RESO;
    int two_pi_b = 2*scitbx::constants::pi*sphereOuter;
    output.push_back("Number of Data HKL is "+std::to_string(HKL_list.size()));
    af_string txt = hkl_to_elmn(HKL_list,two_pi_b,HIGHSYMM);
    for (auto item : txt)
      output.push_back(item);
    return output;
  }

} //phasertng
