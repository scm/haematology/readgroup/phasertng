//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineRM.h>
#include <phasertng/src/Packing.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/math/rotation/rotdist.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Rigid_map_refinement
  void Phasertng::runRMR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with pose mode");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
    {
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false);
    }
    }}
    DAGDB.apply_interpolation(interp::EIGHT); //eight throughout

    //dag nodes are already loaded in Phasertng.cc
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(), store_halfR, store_fullR);

    int z(1);
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      node->generic_int2 = z++;
    }

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    bool refine_off =
        input.value__choice.at(".phasertng.macrm.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macrm.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macrm.macrocycle3.").value_or_default_equals("off");
    refine_off = refine_off or
        input.value__choice.at(".phasertng.macrm.protocol.").value_or_default_equals("off");

    //double lowerB = REFLECTIONS->bfactor_minimum();
    //if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
    //  lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    double lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    double upperB = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();


    if (refine_off)
    {
      out::stream progress = out::LOGFILE;
      logBlank(progress);
      logProgressBarStart(progress,"Rescoring poses",DAGDB.size());
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        //rescore with the LLGI target (refinement uses Wilson)
        //no selection for top peaks, recore them all
        auto& node = DAGDB.WORK;
        out::stream where = out::VERBOSE;
        bool changed = load_reflid_for_node(node->REFLID,where);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
        if (changed or diffsg)
        {
          if (changed) logTab(where,"Reflections changed to " + node->REFLID.str());
          double resolution = DAGDB.WORK->search_resolution(io_hires);
          auto seltxt = selected.set_resolution(
              REFLECTIONS->data_hires(),
              DAGDB.resolution_range(),
              resolution);
          if (changed) logTabArray(where,seltxt);
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        }
        DAGDB.work_node_phased_likelihood( //previously used interp::FOUR
            REFLECTIONS.get(),
            selected.get_selected_array(),
            NTHREADS,
            USE_STRICTLY_NTHREADS);
        node->ANNOTATION += " RMR=" + dtoi(node->MAPLLG);
        node->generic_float = node->MAPLLG;
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
    }
    else
    {
      std::vector<sv_string> macro = {
        input.value__choice.at(".phasertng.macrm.macrocycle1.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macrm.macrocycle2.").value_or_default_multi(),
        input.value__choice.at(".phasertng.macrm.macrocycle3.").value_or_default_multi()
      };
      sv_int ncyc = {
        input.value__int_vector.at(".phasertng.macrm.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macrm.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macrm.ncyc.").value_or_default(2)
      };
      auto method = input.value__choice.at(".phasertng.macrm.minimizer.").value_or_default();
      logProtocol(out::LOGFILE,macro,ncyc,method);
      logTab(out::LOGFILE,"B-factor range: " + dtos(lowerB,2) + " " + dtos(upperB,2));
      logBlank(out::LOGFILE);
      out::stream progress = out::LOGFILE;
      logProgressBarStart(progress,"Refining Poses",DAGDB.size());
      int n(1),N(DAGDB.size());
      DAGDB.restart(); //very important to call this to set WORK
      double topllg = std::numeric_limits<double>::lowest();
      while (!DAGDB.at_end())
      {
        out::stream where = out::VERBOSE;
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Rigid Map Refinement" +nN);
        auto& node = DAGDB.WORK;
        bool changed = load_reflid_for_node(node->REFLID,out::VERBOSE);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
        if (changed or diffsg)
        {
          if (changed) logTab(where,"Reflections changed to " + node->REFLID.str());
          double resolution = DAGDB.WORK->search_resolution(io_hires);
          auto seltxt = selected.set_resolution(
              REFLECTIONS->data_hires(),
              DAGDB.resolution_range(),
              resolution);
          if (changed) logTabArray(where,seltxt);
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        }
        /* use start_likelihood to record starting value, not this recalculation
           calculating work_node_phased_likelihood with interp::FOUR gives different starting value
        DAGDB.work_node_phased_likelihood( previously used interp::FOUR
            REFLECTIONS.get(),
            selected.get_selected_array(),
            NTHREADS,
            USE_STRICTLY_NTHREADS);
        node->generic_float = node->MAPLLG;
        logTabArray(where,node->logPose(""));
        */
        if (Suite::Extra())
          logTabArray(out::TESTING,node->logNode(nN+"Before refinement"));
        RefineRM refine(
            REFLECTIONS.get(),
            selected.get_selected_array());
        refine.SIGMA_ROT = input.value__flt_number.at(".phasertng.macrm.restraint_sigma.rotation.").value_or_default();
        refine.SIGMA_TRA = input.value__flt_number.at(".phasertng.macrm.restraint_sigma.translation.").value_or_default();
        refine.SIGMA_BFAC = input.value__flt_number.at(".phasertng.macrm.restraint_sigma.bfactor.").value_or_default();
        refine.SIGMA_DRMS = input.value__flt_number.at(".phasertng.macrm.restraint_sigma.drms.").value_or_default();
        refine.SIGMA_CELL = input.value__flt_number.at(".phasertng.macrm.restraint_sigma.cell.").value_or_default();
        refine.BFAC_MIN = lowerB;
        refine.BFAC_MAX = upperB;
        refine.CELL_MIN = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_min();
        refine.CELL_MAX = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_max();
        refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
        refine.init_node(DAGDB.WORK);

        dtmin::Minimizer minimizer(this);
        minimizer.set_logfile_level(out::TESTING);
        minimizer.run(
          refine,
          macro,
          ncyc,
          input.value__choice.at(".phasertng.macrm.minimizer.").value_or_default(),
          input.value__boolean.at(".phasertng.macrm.study_parameters.").value_or_default(),
          input.value__flt_number.at(".phasertng.macrm.small_target.").value_or_default()
        );
        //double llg = refine.likelihood(); //store llg in node //previous, required recalculation
        node->generic_float = -REFLECTIONS->oversampling_correction()*refine.start_likelihood; //interp::EIGHT //new, no recalculation
        double llg = -REFLECTIONS->oversampling_correction()*refine.final_likelihood; //interp::EIGHT //new, no recalculation
        node->MAPLLG = llg;
        if (refine.negvar) logWarning("Negative variance ignored, contact developers");
        logBlank(where);
        node->ANNOTATION += " RMR=" + dtoi(llg);
        node->generic_bool = minimizer.convergence();
        logTabArray(where,node->logPose("Refined"));
        if (input.value__boolean.at(".phasertng.macrm.study_parameters.").value_or_default())
        {
          throw Error(err::DEVELOPER,"Study parameters: break after first refinement");
        }
        if (llg > topllg)
        {
          topllg = llg;
          logProgressBarAgain(progress,"New Best LLG=" + dtos(topllg,2)+nN);
        }
        if (!logProgressBarNext(progress)) break;
      }
      logProgressBarEnd(progress);
    }

    //assume all POSE sizes are the same

    {{
    out::stream where = out::VERBOSE;
    for (int i = 0; i < DAGDB.NODES.size(); i++)
      DAGDB.NODES[i].generic_int = i+1;
    logEllipsisOpen(where,"Sort");
    DAGDB.NODES.apply_sort_mapllg();
    logEllipsisShut(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Flagging duplicates");
    bool is_a_map(REFLECTIONS->MAP); //cast from tribool
    PHASER_ASSERT(is_a_map); //paranoia
    DAGDB.calculate_duplicate_poses(selected.STICKY_HIRES,is_a_map,USE_STRICTLY_NTHREADS,NTHREADS);
    logEllipsisShut(where);
    }}

    int ww = itow(DAGDB.NODES.size());
    int io_percent = input.value__percent.at(".phasertng.macrm.percent.").value_or_default();
    if (false) //don't do this because it assumes the mean is zero
    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by percent");
    int before = DAGDB.NODES.size();
    //here we assume that the mean value of the search is zero
    double percent_mean = 0;
    double top = DAGDB.NODES.top_mapllg();
    if (top > percent_mean)
    {
      double cut = DAGDB.NODES.apply_partial_sort_and_percent_mapllg(io_percent,percent_mean);
      logEllipsisShut(where);
      logTab(where,"Percent: "  + dtos(io_percent,5,1) + "%");
      logTab(where,"Top/Mean/Cutoff LLG: " + dtos(top,2) + "/" + dtos(percent_mean,2) + "/" + dtos(cut,2));
      logTab(where,"Mean FSS: " + dtos(percent_mean));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
    }
    else
    {
      logWarning("Purge by percent not applied as top value was less than mean (0)");
    }
    }}

    int io_nprint = input.value__int_number.at(".phasertng.macrm.maximum_printed.").value_or_default();

    for (auto& node : DAGDB.NODES)
    { //load shifts wrt original
      UnitCell UC(node.CELL);
      UC.scale_orthogonal_cell(node.DATA_SCALE);
      dmat33 orthmat = UC.orthogonalization_matrix();
      dmat33 fracmat = UC.fractionalization_matrix();
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
        const dvect3& PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
        dmat33 ROTmt; dvect3 FRACT;
        std::tie(ROTmt,FRACT) = node.POSE[p].finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
        dvect3 eulermt = scitbx::math::euler_angles::zyz_angles(ROTmt);
        std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(eulermt,PR);
        dvect3 fract = dag::fract_wrt_in_from_mt(FRACT,ROTmt,PT,node.CELL);
        dmat33 orthmat = node.CELL.orthogonalization_matrix();
        pose.SHIFT_ORTHT = orthmat*fract;
      }
    }

    {{
    out::stream where = out::LOGFILE;
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int2,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::min(3,itow(io_nprint2)); //-n/a-
    logTableTop(where,"Rigid Map Refinement Results");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    int cellw = REFLECTIONS->UC.width();
    if (DAGDB.size())
    {
      logTab(where,"#r      Refined rank of the peak");
      logTab(where,"*/&     Unique/Duplicate");
      logTab(where,"#i/#o   Input/Output rank of the peak");
      logTab(where,"=/<     Converged/Not-Converged");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s%c %-*s %*s %*s  %10s %10s%c  %7s %5s %*s",
          w,"#r",' ',
          ww,"#i",
          ww,"#o",
          ww,"#=",
          "LLG0","LLG",' ',"TFZ","CELL",uuid_w,"reflid"));
      logTab(where,snprintftos(
          "%*s %2s  %s %s (%5s/%-5s) %7s %6s %*s",
          w+1+(ww+1)*3,"Final","#",
          dag::header_polar().c_str(),
          dag::header_ortht(cellw).c_str(),
          "Angle","Shift","DRMS","BFAC",uuid_w,"modlid"));
    }
    logTab(where,"--------");
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      logTab(where,snprintftos(
          "%*i%c %-*i %*s %*s  %10s %10s%c %7s %5.3f %*s",
          w,i,node.generic_flag ? '*': '&',
          ww,node.generic_int2,
          ww,node.generic_flag ? itos(node.NUM).c_str() : "",
          ww,node.generic_flag ? "n/a" : itos(node.EQUIV+1).c_str(),
          dtos(node.generic_float,q).c_str(),
          dtos(node.MAPLLG,q).c_str(),
          node.generic_bool ? '=' : '<',
          dtos(node.ZSCORE,2).c_str(),
          node.DATA_SCALE,
          uuid_w,node.REFLID.string().c_str()
          ));
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        double ang = pose.delta_angle();
        double sft = pose.delta_shift();
        int da = ang >= 10 ? 2 : 3;
        int ds = sft >= 10 ? 2 : 3;
        logTab(where,snprintftos(
            "%*s %2s %s  %s (%5.*f/%5.*f) %+7.3f %+6.1f %*s",
            w+1+(ww+1)*3,"",
            ntos(p+1,node.POSE.size()).c_str(),
            dag::print_polar(pose.SHIFT_MATRIX).c_str(),
            dag::print_ortht(pose.SHIFT_ORTHT,cellw).c_str(),
            da,ang,
            ds,sft,
            node.DRMS_SHIFT[pose.IDENTIFIER].VAL,
            pose.BFAC,
            uuid_w,pose.IDENTIFIER.string().c_str()
            ));
      }
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge duplicates");
    int before = DAGDB.size();
    DAGDB.NODES.erase_invalid();
    logEllipsisShut(where);
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(DAGDB.size()));
    }}

    int io_maxstored = input.value__int_number.at(".phasertng.macrm.maximum_stored.").value_or_default();
    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by maximum stored");
    int before = DAGDB.NODES.size();
    DAGDB.NODES.apply_partial_sort_and_maximum_stored_mapllg(io_maxstored);
    logEllipsisShut(where);
    if (io_maxstored == 0)
    logTab(where,"Maximum stored: all");
    else
    logTab(where,"Maximum stored: " + itos(io_maxstored));
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(DAGDB.NODES.size()));
    }}

    {{
    out::stream where = out::SUMMARY;
    int io_nprint2(0);
    for (auto node : DAGDB.NODES) io_nprint2 = std::max(node.generic_int,io_nprint2);
    int w = std::max(itow(io_nprint),2); //-2
    int ww = std::min(3,itow(io_nprint2)); //-n/a-
    logTableTop(where,"Rigid Map Refinement Summary");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"Shifts reported for last placed");
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#o #i   Output/Input rank of the peak");
      logTab(where,"=/<     Converged/Not-Converged");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %*s %10s %10s%c %7s (%5s/%-5s) %7s %6s %5s %*s %*s",
          ww,"#o",
          ww,"#i",
          "LLG0","LLG",' ',"TFZ","Angle","Shift","DRMS","BFAC","CELL",uuid_w,"modlid",uuid_w,"reflid"));
    }
    logTab(where,"--------");
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      dag::Pose& pose = node.POSE.back();
      double ang = pose.delta_angle();
      double sft = pose.delta_shift();
      int da = ang >= 10 ? 2 : 3;
      int ds = sft >= 10 ? 2 : 3;
      logTab(where,snprintftos(
          "%*s %*i %10s %10s%c %7s (%5.*f/%5.*f) %+7.3f %+6.1f %5.3f %*s %*s",
          ww,node.generic_flag ? itos(node.NUM).c_str() : "",
          ww,node.generic_int2,
          dtos(node.generic_float,q).c_str(),
          dtos(node.MAPLLG,q).c_str(),
          node.generic_bool ? '=' : '<',
          dtos(node.ZSCORE,2).c_str(),
          da,ang,
          ds,sft,
          node.DRMS_SHIFT[pose.IDENTIFIER].VAL,
          pose.BFAC,
          node.DATA_SCALE,
          uuid_w,pose.IDENTIFIER.string().c_str(),
          uuid_w,node.REFLID.string().c_str()
          ));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    //when the dag nodes are entered
    DAGDB.shift_tracker(hashing(),input.running_mode);
    int n(1),N(DAGDB.size());
    //only one set of reflections is stored in memory
    //otherwise files must be written out
    DAGDB.restart();
    logUnderLine(out::LOGFILE,"Rigid Map Refinement Coordinates");
    if (io_maxstored)
      logTab(out::LOGFILE,"A maximum of " + itos(io_maxstored) + " nodes will have coordinates generated");
    af_string chain_mapping;
    while (!DAGDB.at_end())
    {
      if (io_maxstored and n > io_maxstored)
        continue;
      std::string nN = nNtos(n++,N);
      out::stream where(out::LOGFILE);
      auto node = DAGDB.WORK; //pointer so in place changes

      //The logic here is that we make a new entry with the current node information
      //so that the map files and coordinates form a new database entry on their own
      //independent of the poses from which they are derived
      Identifier& modlid = node->TRACKER.ULTIMATE; //modify tag here
     // auto pathway = DAGDB.PATHWAY.ULTIMATE.string();
     // modlid.initialize_tag("rmr" + ntos(n-1,N));
      auto entry = DAGDB.add_entry(modlid);

      {{
        //use packing as a cheap way of creating coordinates
        Packing packing;
        packing.init_node(node);
       // packing.move_to_origin();
       //chain mapping will be same for all, but only log once
       //data_scale is applied internally to set coordinates
        chain_mapping = packing.structure_for_refinement(entry->COORDINATES);
        entry->COORDINATES.MODLID = entry->identify();
        entry->COORDINATES.REFLID = REFLECTIONS->reflid();
        entry->COORDINATES.TIMESTAMP = TimeStamp();
        Models models; //tmp for getting entry values
        models.SetPdb(entry->COORDINATES.get_pdb_cards());
        models.set_principal_statistics();
        entry->PRINCIPAL_TRANSLATION = -models.statistics.CENTRE;
        entry->MEAN_RADIUS = models.statistics.MEAN_RADIUS;
        entry->CENTRE = models.statistics.CENTRE;
        entry->EXTENT = models.statistics.EXTENT;
        {
        //calculate the volume from the sequence, same as for the map
        Sequence seq(models.SELENOMETHIONINE);
        bool use_unknown(false);
        seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
        seq.initialize(true,true);
        entry->VOLUME = seq.volume();
        }
        entry->SCATTERING = models.scattering()[0];
        entry->MOLECULAR_WEIGHT = models.molecular_weight()[0];
      }}
      //don't clear the poses because we may want to recycle them, through next frf etc
      //the full is then an snapshot of the current solution
      //node->POSE.clear();
      if (Suite::Extra())
        logTabArray(out::TESTING,node->logNode(nN+"After refinement"));

      {{
      entry->COORDINATES.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::COORDINATES_PDB));
      logFileWritten(out::LOGFILE,WriteFiles(),"Coordinates",entry->COORDINATES);
      if (WriteFiles()) entry->COORDINATES.write_to_disk();
      }}
    }
    logUnderLine(out::LOGFILE,"Chain Mapping");
    logTabArray(out::LOGFILE,chain_mapping);

    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        if (entry.second.INTERPOLATION.Warnings.size())
          logWarning(entry.second.INTERPOLATION.Warnings);
    for (auto& entry: DAGDB.ENTRIES)
      if (entry.second.INTERPOLATION.in_memory())
        entry.second.INTERPOLATION.Warnings = "";
  }

} //phasertng
