//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Integration.h>
#include <phaser/lib/maths.h>
#include <scitbx/constants.h>
#include <phasertng/math/table/sim.h>

namespace phasertng {
extern const table::sim& tbl_sim;
}

namespace phaser {

// -- ACENTRIC

floatType acentric::FOM()
{
  floatType totPhsProb(0),probCosAng(0),probSinAng(0);
  for (unsigned p = 0; p < phsProb.size(); p++)
  {
    totPhsProb += phsProb[p];
    probCosAng += phsProb[p]*cos(Ang[p]);
    probSinAng += phsProb[p]*sin(Ang[p]);
  }
  return (totPhsProb > 0) ? std::sqrt(probCosAng*probCosAng + probSinAng*probSinAng)/totPhsProb : 0;
}
floatType acentric::PHIB()
{
  floatType probCosAng(0),probSinAng(0);
  for (unsigned p = 0; p < phsProb.size(); p++)
  {
    probCosAng += phsProb[p]*cos(Ang[p]);
    probSinAng += phsProb[p]*sin(Ang[p]);
  }
  return atan2(probSinAng,probCosAng);
}
cctbx::hendrickson_lattman<double> acentric::HL()
{
 floatType HLA(0),HLB(0),HLC(0),HLD(0);
 for (unsigned p = 0; p < phsProb.size(); p++)
  {
    if (phsProb[p] > 0)
    {
      floatType logThisPhsProb = log(phsProb[p]);
      HLA += logThisPhsProb*cos(Ang[p]);
      HLB += logThisPhsProb*sin(Ang[p]);
      HLC += logThisPhsProb*cos(2*Ang[p]);
      HLD += logThisPhsProb*sin(2*Ang[p]);
    }
  }
  HLA *= (2./static_cast<floatType>(phsProb.size()));
  HLB *= (2./static_cast<floatType>(phsProb.size()));
  HLC *= (2./static_cast<floatType>(phsProb.size()));
  HLD *= (2./static_cast<floatType>(phsProb.size()));
  return cctbx::hendrickson_lattman<double>(HLA,HLB,HLC,HLD);
}

// -- CENTRIC

floatType centric::FOM()
{
  floatType totPhsProb = phsProb1 + phsProb2;
  return (totPhsProb > 0) ? std::fabs(phsProb1 - phsProb2)/totPhsProb : 0;
}
floatType centric::PHIB()
{
  return (phsProb1 > phsProb2) ? phsr : phsr + scitbx::constants::pi;
}
cctbx::hendrickson_lattman<double> centric::HL()
{
  floatType HLA(0),HLB(0);
  floatType X = fabs(log(phsProb1)-log(phsProb2))/2;
  floatType rPHIB = PHIB();
  HLA = X*cos(rPHIB);
  HLB = X*sin(rPHIB);
  return cctbx::hendrickson_lattman<double>(HLA,HLB,0,0);
}

// -- SINGLETON


floatType singleton::FOM()
{ return phasertng::tbl_sim.get(X); }
floatType singleton::PHIB()
{ return phib; }
cctbx::hendrickson_lattman<double> singleton::HL()
{
  floatType HLA(0),HLB(0);
  floatType rPHIB = PHIB();
  HLA = X*cos(rPHIB);
  HLB = X*sin(rPHIB);
  return cctbx::hendrickson_lattman<double>(HLA,HLB,0,0);
}

} //phaser
