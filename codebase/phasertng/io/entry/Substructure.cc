//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Substructure.h>
#include <phasertng/main/includes.h>
#include <phasertng/main/Assert.h>
#include <phasertng/data/Scatterers.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;

  std::pair<cmplex,cmplex>
  Substructure::calcAtomicDataSum(
      const millnx& miller,
      const double SSQR,
      const double eps,
      const_ScatterersPtr SCATTERERS,
      bool do_gradient,
      bool do_hessian
    )
  {
    //because atomtypes can be clusters, this cannot be done by fft
    pod::rsymhkl rsym(SG.NSYMP);
    SG.symmetry_hkl_primitive(miller,eps,rsym);

    if (do_hessian) do_gradient = true;
    if (do_gradient)
    {
      for (int a = 0; a < this->size(); a++)
      {
        pod::xray::scatterer& A = this->XRAY[a];
        const std::string& t = A.scattering_type;
        //create and init, because these are cumulative over atoms
        dReFHpos_by_dFdp[t] = 0;
        dReFHneg_by_dFdp[t] = 0;
        dImFHpos_by_dFdp[t] = 0;
        dImFHneg_by_dFdp[t] = 0;
        d2ReFHpos_by_dFdp2[t] = 0;
        d2ReFHneg_by_dFdp2[t] = 0;
        d2ImFHpos_by_dFdp2[t] = 0;
        d2ImFHneg_by_dFdp2[t] = 0;
        A.fdp = SCATTERERS->FP_FDP.at(t).fdp();
      }
    }

    cmplex FHPOS = {0,0};
    cmplex FHNEG = {0,0};
    for (int a = 0; a < this->size(); a++)
    {
      pod::xray::scatterer& A = this->XRAY[a];
      pod::xtra::scatterer& X = this->XTRA[a];
      pod::gradient::anom::scatterer& G = this->GRAD[a];
      pod::hessian::anom::scatterer& H = this->HESS[a];
      if (!X.rejected)
      {
        const std::string& t = A.scattering_type;
        double linearTerm = SG.symFacPCIF*A.occupancy/X.multiplicity;
        double isoB = A.flags.use_u_aniso_only() ?
                      1.0 :
                      std::exp(-SSQR*cctbx::adptbx::u_as_b(A.u_iso) * 0.25);
        double symmOccBfac = linearTerm*isoB;
        double fo_plus_fp = SCATTERERS->fo_plus_fp(t,SSQR);
        double scat(fo_plus_fp*symmOccBfac);
        double fpp(A.fdp*symmOccBfac);
        double ReFHpos(0),ImFHpos(0),ReFHneg(0),ImFHneg(0);
        for (int isym = 0; isym < rsym.unique; isym++)
        {
          double anisoB(1);
          double anisoScat(scat);
          double anisoFpp(fpp);
          if (A.flags.use_u_aniso_only())
          {
            anisoB = cctbx::adptbx::debye_waller_factor_u_star(rsym.rotMiller[isym],A.u_star);
            anisoScat = scat*anisoB;
            anisoFpp = fpp*anisoB;
          }
          double theta = rsym.rotMiller[isym]*A.site + rsym.traMiller[isym];
          cmplex sincostheta = tbl_cos_sin.get(theta); //multiplies by 2*pi internally
          double costheta = std::real(sincostheta);
          double sintheta = std::imag(sincostheta);
          double anisoScat_costheta(anisoScat*costheta);
          double anisoScat_sintheta(anisoScat*sintheta);
          double anisoFpp_costheta(anisoFpp*costheta);
          double anisoFpp_sintheta(anisoFpp*sintheta);
          double symReFHpos(anisoScat_costheta - anisoFpp_sintheta);
          double symImFHpos(anisoScat_sintheta + anisoFpp_costheta);
          ReFHpos += symReFHpos;
          ImFHpos += symImFHpos;
          //Store the complex conjugate
          double symReFHneg(anisoScat_costheta + anisoFpp_sintheta);
          double symImFHneg(anisoScat_sintheta - anisoFpp_costheta);
          ReFHneg += symReFHneg;
          ImFHneg += symImFHneg;

          if (do_gradient and REFINE_SITE)
          {
            for (int x = 0; x < 3; x++)
            {
              double gradfac = scitbx::constants::two_pi*rsym.rotMiller[isym][x];
              G.dReFHpos_by_dX[x] = -gradfac*symImFHpos;
              G.dImFHpos_by_dX[x] = +gradfac*symReFHpos;
              G.dReFHneg_by_dX[x] = -gradfac*symImFHneg;
              G.dImFHneg_by_dX[x] = +gradfac*symReFHneg;
              if (do_hessian)
              {
                double hessfac = fn::pow2(gradfac);
                H.d2ReFHpos_by_dX2[x] = -hessfac*symReFHpos;
                H.d2ImFHpos_by_dX2[x] = -hessfac*symImFHpos;
                H.d2ReFHneg_by_dX2[x] = -hessfac*symReFHneg;
                H.d2ImFHneg_by_dX2[x] = -hessfac*symImFHneg;
              }
            }
          }

          if (do_gradient and REFINE_FDP)
          {
            this->dReFHpos_by_dFdp[t] -= sintheta*symmOccBfac*anisoB;
            this->dImFHpos_by_dFdp[t] += costheta*symmOccBfac*anisoB;
            this->dReFHneg_by_dFdp[t] += sintheta*symmOccBfac*anisoB;
            this->dImFHneg_by_dFdp[t] -= costheta*symmOccBfac*anisoB;
            //hessian = 0
          }

          if (do_gradient and A.flags.use_u_aniso_only() and REFINE_BFAC)
          {
            dmat6 millerSqr = cctbx::adptbx::debye_waller_factor_u_star_gradient_coefficients(
                                rsym.rotMiller[isym],scitbx::type_holder<double>());
            for (int n = 0; n < 6; n++)
            {
              double gradfac = -millerSqr[n]*scitbx::constants::two_pi_sq;
              G.dReFHpos_by_dAB[n] = gradfac*symReFHpos;
              G.dImFHpos_by_dAB[n] = gradfac*symImFHpos;
              G.dReFHneg_by_dAB[n] = gradfac*symReFHneg;
              G.dImFHneg_by_dAB[n] = gradfac*symImFHneg;
              if (do_hessian)
              {
                double hessfac = fn::pow2(gradfac);
                H.d2ReFHpos_by_dAB2[n] = hessfac*symReFHpos;
                H.d2ImFHpos_by_dAB2[n] = hessfac*symImFHpos;
                H.d2ReFHneg_by_dAB2[n] = hessfac*symReFHneg;
                H.d2ImFHneg_by_dAB2[n] = hessfac*symImFHneg;
              }
            }
          }
        }//end sym
        FHPOS += cmplex(ReFHpos,ImFHpos);
        FHNEG += cmplex(ReFHneg,ImFHneg);

        if (do_gradient and REFINE_OCC and A.occupancy)
        {
          PHASER_ASSERT(A.occupancy > std::numeric_limits<double>::min());
          G.dReFHpos_by_dO = ReFHpos/A.occupancy;
          G.dImFHpos_by_dO = ImFHpos/A.occupancy;
          G.dReFHneg_by_dO = ReFHneg/A.occupancy;
          G.dImFHneg_by_dO = ImFHneg/A.occupancy;
          if (do_hessian)
          {
            H.d2ReFHpos_by_dO2 = 0;
            H.d2ImFHpos_by_dO2 = 0;
            H.d2ReFHneg_by_dO2 = 0;
            H.d2ImFHneg_by_dO2 = 0;
          }
          //hessian = 0
        }

        if (do_gradient and !A.flags.use_u_aniso_only() and REFINE_BFAC)
        {
          double b_as_u_term(0.25*scitbx::constants::eight_pi_sq);
          double gradfac = -SSQR*b_as_u_term;
          G.dReFHpos_by_dIB = gradfac*ReFHpos;
          G.dImFHpos_by_dIB = gradfac*ImFHpos;
          G.dReFHneg_by_dIB = gradfac*ReFHneg;
          G.dImFHneg_by_dIB = gradfac*ImFHneg;
          if (do_hessian)
          {
            double hessfac = fn::pow2(gradfac);
            H.d2ReFHpos_by_dIB2 = hessfac*ReFHpos;
            H.d2ImFHpos_by_dIB2 = hessfac*ImFHpos;
            H.d2ReFHneg_by_dIB2 = hessfac*ReFHneg;
            H.d2ImFHneg_by_dIB2 = hessfac*ImFHneg;
          }
        }
      }
    } //end  loop over atoms
    return { FHPOS, FHNEG };
  }

}//phasertng
