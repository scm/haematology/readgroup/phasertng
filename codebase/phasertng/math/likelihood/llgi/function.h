//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_llgi_function_class__
#define __phasertng_likelihood_llgi_function_class__
#include <phasertng/math/likelihood/llgi/tgh_flags.h>
#include <complex>

typedef std::complex<double> cmplex;

namespace phasertng { namespace likelihood { namespace llgi {

class function
{
  private: //members
    double V = 0;

  public:
    double LL = 0;
    double dLL_by_dReEC = 0;
    double dLL_by_dImEC = 0;
    double dLL_by_dV = 0;
    double d2LL_by_dReEC2 = 0;
    double d2LL_by_dImEC2 = 0;
    double d2LL_by_dReEC_dImEC = 0;
    double d2LL_by_dReEC_dV = 0;
    double d2LL_by_dImEC_dV = 0;
    double d2LL_by_dV2 = 0;

  public:  //constructor
    function() {}
    bool negative_variance() { return V <= 0; };
    double variance() { return V; };

  public:
    void
    target_gradient_hessian(
      const double& EO,
      const cmplex& EC, //siga and dobs weighted
      const double& teps,
      const double& V,
      const bool&   CENT,
      const likelihood::llgi::tgh_flags&
    );
};

}}} //phasertng
#endif
