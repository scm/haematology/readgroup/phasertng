#include <phasertng/math/likelihood/isad/EFOM.h>
#include <scitbx/math/bessel.h>
#include <scitbx/constants.h>
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/misc_functions.h>
#include <scitbx/math/erf.h>

using namespace scitbx;

namespace phasertng {
namespace likelihood {

  double EFOM::get(double Eeff, double DobsSigA, bool cent)
  {
    // Expected value of FOM given observed E and model completeness/accuracy
    return cent ?centric(Eeff,DobsSigA) :acentric(Eeff,DobsSigA);
  }

  double EFOM::centric(double Eeff, double DobsSigA)
  {
    // Centric case
    double var = 1.-fn::pow2(DobsSigA);
    double eFOM = scitbx::math::erf(DobsSigA*Eeff/std::sqrt(2.*var));
    return eFOM;
  }

  double EFOM::acentric(double Eeff, double DobsSigA)
  {
    // Acentric case
    double var = 1.-fn::pow2(DobsSigA);
    double X = fn::pow2(DobsSigA*Eeff)/(2*var);
    double eFOM = DobsSigA*Eeff/2 * std::sqrt(scitbx::constants::pi/var) *
      std::exp(-X) * (scitbx::math::bessel::i0(X)+scitbx::math::bessel::i1(X));
    return eFOM;
  }

}
}
