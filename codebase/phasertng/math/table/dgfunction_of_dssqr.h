#ifndef __phasertng_table_dgfunction_of_dssqr_class__
#define __phasertng_table_dgfunction_of_dssqr_class__
#include <scitbx/constants.h>
#include <scitbx/array_family/shared.h>

// Assumption for this code is that G-function will be used when approximating
// the shape of a molecule as a sphere, in which case it doesn't make much sense
// to go past the first zero of the function.  Another approach should be used
// in other circumstances.

namespace phasertng {
namespace table {

class dgfunction_by_dssqr
{
  private:
  // Good Accuracy, zero to first root (return zero for larger args)
  int    tbllen;
  double argcut;
  double argfac;
  scitbx::af::shared<std::pair<double,double> > dGfunc_by_dRSsqrTable;

  public:
  dgfunction_by_dssqr(int tbllen_=8192,
                    double argcut_=0.51143712) :
          tbllen(tbllen_),
          argcut(argcut_)
  {
    argfac = double(tbllen/argcut);
    dGfunc_by_dRSsqrTable = getdGfunc_by_dRSsqrTable(tbllen,argcut);
  }

  double
  get(double arg) const
  {
    if (arg < argcut)
    {
      double fracTableArg = argfac*arg;
      int index = static_cast<int>(fracTableArg);
      return dGfunc_by_dRSsqrTable[index].first +
        (fracTableArg-index)*dGfunc_by_dRSsqrTable[index].second;
    }
    return Fn_dGfunc_by_dRSsqr(arg);
  }

  private:
  double
  Fn_dGfunc_by_dRSsqr(double rsSqr) const
  {
    // Derivative of G-function wrt rs^2.
    static double SMALL(0.1);
    double twoPiRS = scitbx::constants::two_pi*std::sqrt(rsSqr);
    if (std::abs(twoPiRS) > SMALL)
      return (9*twoPiRS*std::cos(twoPiRS)+3*(fn::pow2(twoPiRS)-3.)*std::sin(twoPiRS)) /
        (2*rsSqr*fn::pow3(twoPiRS));
    else
      return -scitbx::constants::two_pi_sq/5. +
        rsSqr*fn::pow2(scitbx::constants::two_pi_sq)/35.;
  }

  scitbx::af::shared<std::pair<double,double> >
  getdGfunc_by_dRSsqrTable(int tbllen, double argmax)
  {
    scitbx::af::shared<std::pair<double,double> > dGfunc_by_dRSsqrTable;
    dGfunc_by_dRSsqrTable.resize(tbllen+1);
    double divarg(argmax/tbllen);
    for (unsigned i = 0; i < tbllen+1; i++)
    {
      double x0(Fn_dGfunc_by_dRSsqr((i  )*divarg));
      double x1(Fn_dGfunc_by_dRSsqr((i+1)*divarg));
      dGfunc_by_dRSsqrTable[i].first = x0;
      dGfunc_by_dRSsqrTable[i].second = x1 - x0;
    }
    return dGfunc_by_dRSsqrTable;
  }

};

}}
#endif
