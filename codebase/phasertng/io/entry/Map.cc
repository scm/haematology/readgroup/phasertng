//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/DogTag.h>
#include <phasertng/io/entry/Map.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/math/fft/real_to_complex_3d.h> //concise way of importing headers
#include <ccp4_errno.h>
#include <phasertng/io/MtzHandler.h>
#include <numeric>
#include <unordered_set>
#include <phasertng/cctbx_project/cctbx/maptbx/utils_extra.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <boost/functional/hash.hpp>

namespace phasertng {

  bool
  Map::is_mtz()
  { try {//have to do something more than just open
      int read_reflections(0);
      CMtz::MTZ *result;
        result = CMtz::MtzGet(FileSystem::fstr().c_str(), read_reflections);
      if (result == 0)
        throw Error(err::FILEOPEN,FileSystem::fstr());
      if (result->nref <= 0)
        throw Error(err::INPUT,"Not mtz file");
    } catch(...) { return false; } return true; //read for is_mtz
  }

  bool
  Map::is_map()
  { try { //have to do something more than just open
      iotbx::ccp4_map::map_reader ccp4_map(FileSystem::fstr());
      cctbx::uctbx::unit_cell     readcell(ccp4_map.unit_cell_parameters);
    } catch(...) { return false; } return true; //read for is_map
  }

  af_string
  Map::ReadMtz(std::string fmap,std::string phmap,bool no_phases,bool require_box)
  {
    PHASER_ASSERT(MODLID.is_set_valid());
    af_string output;
    int read_reflections(1);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    mtzIn.find_fmap_phmap(fmap,phmap,no_phases);
    auto txt = mtzIn.XtalSetColInfo();
    for (auto line : txt)
      output.push_back(line);
    bool map_wanted = !no_phases;
    if (map_wanted and mtzIn.MapCount == 0)
      throw Error(err::INPUT,"No phased data");
    if (map_wanted and mtzIn.MapCount >= 2 and fmap == "" and phmap == "")
      throw Error(err::INPUT,"More than one phased data set, specify data in input");
    if (map_wanted and !mtzIn.Fcount)
      throw Error(err::INPUT,"No default data FENS/PHENS set, specify columns in input");
    if (no_phases and !mtzIn.Fcount)
      throw Error(err::INPUT,"No default data FENS set, specify columns in input");
    output.push_back("Mtz columns: " + mtzIn.Flabel + "/" + mtzIn.Plabel);
    output.push_back("");
    output.push_back("Reference column for unit cell: " + mtzIn.Flabel);
    mtzIn.load_dataset(mtzIn.Flabel);
    //UNITCELL
    {
      UC = mtzIn.UC;
      if (require_box)
        if (std::fabs(UC.Alpha()-90) > DEF_PPM or
            std::fabs(UC.Beta() -90) > DEF_PPM or
            std::fabs(UC.Gamma()-90) > DEF_PPM)
          throw Error(err::INPUT,"Unit cell of map must be orthogonal");
      output.push_back("Unit Cell:  " + UC.str());
      //AJM EnsembleMap has multiply by CELL_SCALE here
      //I think I will deal with this on the fly in tng
    }
    //SPACEGROUP
    {
      SG = mtzIn.SG;
      output.push_back("Space Group: " + SG.CCP4);
      if (require_box)
        if (SG.CCP4 != "P 1")
          throw Error(err::INPUT,"Space group of map must be P1");
      output.push_back("");
    }
    output.push_back("Number of reflections in file: " + std::to_string(mtzIn.NREFL));
    output.push_back("Columns of reflections in file: " + mtzIn.Flabel + " " + mtzIn.Plabel);

    CMtz::MTZCOL *Acol = mtzIn.getCol(mtzIn.Flabel);
    CMtz::MTZCOL *Pcol;
    if (!no_phases)
      Pcol = mtzIn.getCol(mtzIn.Plabel);

    mtzIn.resetRefl();
    MILLER.clear();
    FCALC.clear();
    double AVAILABLE_HIRES = std::numeric_limits<double>::max();
    af_double f,phase;
    for (int mtzr = 0; mtzr < mtzIn.NREFL ; mtzr++)
    {
      phaser_assert(mtzIn.mtzH);
      phaser_assert(mtzIn.mtzK);
      phaser_assert(mtzIn.mtzL);
      int H = mtzIn.mtzH->ref[mtzr];
      int K = mtzIn.mtzK->ref[mtzr];
      int L = mtzIn.mtzL->ref[mtzr];
      millnx hkl(H,K,L);
      if (hkl == millnx(0,0,0)) continue;
      if (!mtz_isnan(Acol->ref[mtzr]))
      {
        AVAILABLE_HIRES = std::min(AVAILABLE_HIRES,UC.cctbxUC.d(hkl));
        MILLER.push_back(hkl);
        f.push_back(static_cast<double>(Acol->ref[mtzr]));
        bool mtzr_no_phases = no_phases or mtz_isnan(Pcol->ref[mtzr]);
        phase.push_back(mtzr_no_phases ? 0 : scitbx::deg_as_rad(static_cast<double>(Pcol->ref[mtzr])));
      }
    }
    if (SG.CCP4 != "P 1")
    {
      bool deg(false),FriedelFlag(false);
      f = cctbx::miller::expand_to_p1<double>(
           SG.group(), FriedelFlag, MILLER.const_ref(), f.const_ref()).data;
      phase = cctbx::miller::expand_to_p1_phases<double>(
           SG.group(), FriedelFlag, MILLER.const_ref(), phase.const_ref(), deg).data;
        //now expand the miller indices
      MILLER = cctbx::miller::expand_to_p1_indices(
           SG.group(), FriedelFlag, MILLER.const_ref());
      SG = SpaceGroup("P 1");
    }
    for (int mtzr = 0; mtzr < MILLER.size() ; mtzr++)
        FCALC.push_back(std::polar(f[mtzr],phase[mtzr]));
    //read the history and setup up parsing of cards to see what is there already
    //read what is in file and see if it agrees with values about to be written
    HEADER.SetHistory(mtzIn.HISTORY);
    output.push_back("Number of reflections read: " + std::to_string(MILLER.size()));
    return output;
  }

  af_string
  Map::ReadMap(double oversample_resolution_limit,bool write_files,std::string database,Identifier pathway)
  {
    af_string output;
    PHASER_ASSERT(MODLID.is_set_valid());
    try {
      iotbx::ccp4_map::map_reader ccp4_map(FileSystem::fstr());
      cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
      cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
      cctbx::uctbx::unit_cell        readcell(ccp4_map.unit_cell_parameters);
      af::int3 origin = af::adapt(ccp4_map.data.accessor().origin());

      scitbx::fftpack::real_to_complex_3d<double> rfft(ccp4_map.unit_cell_grid);

     // if (!ccp4_map.data.accessor().is_0_based()) ccp4_map.data.accessor().shift_origin();

      //these are the contortions required to read the map
      //get it to zero based for the fft
      //but not shift it from original position
      //shift_origin on the accessor shifts the map
      af::const_ref<double, cctbx::maptbx::c_grid_padded_p1<3> > map_data(
         ccp4_map.data.begin(),
         cctbx::maptbx::c_grid_padded_p1<3>(ccp4_map.data.accessor().shift_origin()));

      af::versa<double, af::flex_grid<> > read_map(
        af::flex_grid<>(af::adapt(rfft.m_real()))
          .set_focus(af::adapt(rfft.n_real())));

      af::int3 maxnnn = af::adapt(rfft.n_real()); //m_real?

      af::int3 nnn,rrr;
      for (nnn[0] = 0; nnn[0] < maxnnn[0]; nnn[0]++)
      for (nnn[1] = 0; nnn[1] < maxnnn[1]; nnn[1]++)
      for (nnn[2] = 0; nnn[2] < maxnnn[2]; nnn[2]++)
      {
        rrr = nnn+origin;
        for (int i = 0; i < 3; i++)
        {
          while (rrr[i] >= maxnnn[i]) rrr[i] -= maxnnn[i];
          while (rrr[i] < 0) rrr[i] += maxnnn[i];
        }
        read_map(rrr[0],rrr[1],rrr[2]) = map_data(nnn[0],nnn[1],nnn[2]);
      }

      if (write_files) //output->Level() >= out::TESTING and output->WriteFiles())
      {
        af::const_ref<double, af::c_grid_padded_periodic<3> > test(
            read_map.begin(), af::c_grid_padded_periodic<3>(read_map.accessor()));
        af::const_ref<std::string> labels(0,0);
        af::int3 gridding_first(0,0,0);
        af::int3 gridding_last = map_data.accessor().all();
        DogTag dogtag(pathway,MODLID);
        FileSystem mapout(database,dogtag.FileName(".debug.read.map"));
        output.push_back("Writing map after reading " + mapout.fstr());
        iotbx::ccp4_map::write_ccp4_map_p1_cell(
            mapout.fstr(),
            readcell,
            SgOpsP1,
            gridding_first,
            gridding_last,
            test,
            labels);
      }
   // --- do the fft
      rfft.forward(read_map);
      {{
        double scale = 1.0 /sqrt(rfft.n_real()[0]*rfft.n_real()[1]*rfft.n_real()[2]);
        for (int i  = 0; i < read_map.size(); i++)
          read_map[i]*=scale;
      }}

      af::ref<std::complex<double>, af::c_grid_padded<3> > complex_map(
          reinterpret_cast<std::complex<double>*>(&*read_map.begin()),
          af::c_grid_padded<3>(af::adapt(rfft.n_complex())));

      dvect3 box(ccp4_map.unit_cell_parameters[0],
                 ccp4_map.unit_cell_parameters[1],
                 ccp4_map.unit_cell_parameters[2]);
     // box *= double(CELL_SCALE_MAX);
      UC = UnitCell(box);

      bool anomalous_flag(false);
      bool conjugate_flag(true);
      bool discard_indices_affected_by_aliasing(true);
      double AVAILABLE_HIRES(0);
      { //fake out, to find available hires
      double d_min(oversample_resolution_limit); //maximum available resolution, possible for very oversampled maps
      cctbx::maptbx::structure_factors::from_map<double> from_map(
          UC.cctbxUC,
          SgInfoP1,
          anomalous_flag,
          d_min,
          complex_map,
          conjugate_flag,
          discard_indices_affected_by_aliasing
          );
      MILLER = from_map.miller_indices().deep_copy();
      double max_d_star_sq = UC.cctbxUC.min_max_d_star_sq(MILLER.const_ref())[1];
      AVAILABLE_HIRES = cctbx::uctbx::d_star_sq_as_d(max_d_star_sq);
      } //throw away map

      //now extract for real to resolution limit
      cctbx::maptbx::structure_factors::from_map<double> from_map(
          UC.cctbxUC,
          SgInfoP1,
          anomalous_flag,
          AVAILABLE_HIRES,
          complex_map,
          conjugate_flag,
          discard_indices_affected_by_aliasing
          );
      MILLER = from_map.miller_indices().deep_copy();
      FCALC = from_map.data().deep_copy();

      if (write_files)
      {
        rfft.backward(read_map);
        {
          double scale = 1.0 /sqrt(rfft.n_real()[0]*rfft.n_real()[1]*rfft.n_real()[2]);
          for (int i  = 0; i < read_map.size(); i++)
            read_map[i] *= scale;
        }

        af::ref<double, af::c_grid_padded_periodic<3> > test(
           read_map.begin(), af::c_grid_padded_periodic<3>(read_map.accessor()));
        af::int3 gridding_first(0,0,0);
        af::int3 gridding_last = map_data.accessor().all();
        af::const_ref<std::string> labels(0,0);
        DogTag dogtag(pathway,MODLID);
        FileSystem mapout(database,dogtag.FileName(".debug.read.fft.fft.map"));
        output.push_back("Writing map after map->sf->map " + mapout.fstr());
        iotbx::ccp4_map::write_ccp4_map_p1_cell(
            mapout.fstr(),
            readcell,
            SgOpsP1,
            gridding_first,
            gridding_last,
            test,
            labels);
      }
    }
    catch (...)
    { throw Error(err::PARSE,"Not a (correctly formatted) map file"); }
    return output;
  }

  std::pair<dvect3,dvect3>
  Map::solvent_constant(af_dvect3 trace_orth,double sampling)
  {
    fft::real_to_complex_3d rfft(SG.type(),UC.cctbxUC);
    rfft.shannon(2);
    std::pair<double,double> cell_scale = {1,1};
    rfft.calculate_gridding(available_hires(cell_scale),cell_scale,false);
    rfft.allocate();
    //should be 5A map

    // --- do the fft
    rfft.backward(MILLER,FCALC);

    //convert mask points to frac coordinates
    //for peak search
    ivect3 n = {
        int(rfft.real_map_unpadded.accessor()[0]),
        int(rfft.real_map_unpadded.accessor()[1]),
        int(rfft.real_map_unpadded.accessor()[2]) };
    af::versa<double, af::flex_grid<>> rfft_solvent(af::flex_grid<>(n[0],n[1],n[2]));
    const int sol = 0;
    const int trc = 1;
    for (int i = 0; i < rfft_solvent.size(); i++)
      rfft_solvent[i] = sol;
    ivect3 grid_padding; //padding is a box rather than a radius because it is easier
    for (int i = 0; i < 3; i++)
      grid_padding[i] = std::round(n[i]*(sampling/UC.GetBox()[i]));
    for (auto& orth : trace_orth)
    {
      ivect3 g;
      for (int i = 0; i < 3; i++)
        g[i] = std::round(n[i]*(orth[i]/UC.GetBox()[i]));
      ivect3 box;
      for (box[0] = -grid_padding[0]; box[0] <= grid_padding[0]; box[0]++) {
      for (box[1] = -grid_padding[1]; box[1] <= grid_padding[1]; box[1]++) {
      for (box[2] = -grid_padding[2]; box[2] <= grid_padding[2]; box[2]++) {
      ivect3 gbox = g+box; //the grid values before wrapping
      for (int i = 0; i < 3; i++)
        while (gbox[i] >= n[i]) gbox[i]-=n[i];
      for (int i = 0; i < 3; i++)
        while (gbox[i] < 0) gbox[i]+=n[i];
      rfft_solvent(gbox[0],gbox[1],gbox[2]) = trc;
      }}}
    }
    std::pair<sv_double,sv_double> values;
    for (int i = 0; i < rfft.real_map_unpadded.size(); i++) {
      double map_value = rfft.real_map_unpadded[i];
      if (rfft_solvent[i] == trc)
        std::get<trc>(values).push_back(map_value);
      else
        std::get<sol>(values).push_back(map_value);
    }
    std::pair<dvect3,dvect3> stats;
    sv_int loop = {sol,trc};
    for (auto j : loop)
    {
      auto& s = (j == sol) ? stats.first : stats.second;
      auto& v = (j == sol) ? values.first : values.second;
      s = dvect3(0,0,0);
      if (v.size() <= 10) continue;
      double sum = std::accumulate(std::begin(v), std::end(v), 0.0);
      double mean =  sum / v.size();
      double accum = 0.0;
      std::for_each(std::begin(v), std::end(v), [&](const double d)
      { accum += (d - mean) * (d - mean); });
      double stddev = std::sqrt(accum /v.size());
      int psol = (100.*v.size())/double(rfft_solvent.size());
      s = dvect3(psol,mean,stddev);
    }
    return stats;
  }

  void
  Map::sort_reflections()
  {
    int NREFL = MILLER.size();
    //sorted on cctbx default miller indices which are k first (0 1 0)
    std::vector<std::pair<millnx,int> > sorted(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
     // double resolution = UC.cctbxUC.d(MILLER[r]);
    //  double d_star_sq = UC.cctbxUC.d_star_sq(MILLER[r]);
    //  sorted[r] = { resolution, r }; //SORT ORDER on first
      sorted[r] = { MILLER[r], r }; //SORT ORDER on first
    }
    auto cmp = []( const std::pair<millnx,int> &a, const std::pair<millnx,int> &b )
    { if (a.first == b.first) return a.second < b.second;
      return a.first < b.first; };
    std::sort(sorted.begin(),sorted.end(),cmp);
    // AJM awaiting BOOST 1.70 :: boost::parallel_stable_sort(sorted.begin(),sorted.end());
    af_millnx newmiller(NREFL);
    af_cmplex newfcalcs(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      newmiller[r] = MILLER[sorted[r].second];
      newfcalcs[r] = FCALC[sorted[r].second];
    }
    MILLER = newmiller;
    FCALC = newfcalcs;
  }

  void
  Map::copy_to_resolution(UnitCell UC_,af_millnx MILLER_,af_cmplex FCALC_,double config_hires)
  {
    UC = UC_;
    MILLER.clear();
    FCALC.clear();
    for (int r = 0; r < MILLER_.size(); r++)
    {
      if (UC.cctbxUC.d(MILLER_[r]) >= config_hires)
      {
        MILLER.push_back(MILLER_[r]);
        FCALC.push_back(FCALC_[r]);
      }
    }
  }

  double
  Map::hires() const
  {
    double hires = std::numeric_limits<double>::max();
    for (int r = 0; r < MILLER.size(); r++)
    {
      hires = std::min(hires,UC.cctbxUC.d(MILLER[r]));
    }
    return hires;
  }

  void
  Map::truncate_to_resolution(double config_hires)
  {
    size_t last = 0;
    for (int r = 0; r < MILLER.size(); r++)
    {
      double reso = UC.cctbxUC.d(MILLER[r]);
      if (reso >= config_hires)
      {
        MILLER[last] = MILLER[r];
        FCALC[last] = FCALC[r];
        last++;
      }
    }
    MILLER.erase(MILLER.begin() + last, MILLER.end()); //erase the end, which is rubbish
    FCALC.erase(FCALC.begin() + last, FCALC.end()); //erase the end, which is rubbish
    phaser_assert(FCALC.size());
    phaser_assert(MILLER.size());
  }

  scitbx::af::shared<double>
  Map::sigmap_with_binning(Bin& bin)
  {
    scitbx::af::shared<double> SigmaP;
    SigmaP.resize(bin.numbins(),0);
    for (int s = 0; s < bin.numbins(); s++)
      SigmaP[s] = 0;
    std::vector<double> SumWeight(bin.numbins(),0);
    phaser_assert(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
    {
      double sqrtssqr = UC.cctbxUC.two_stol(MILLER[r]);
      double reso = 1/sqrtssqr;
      if (reso < bin.lores() and reso >= bin.hires())
      {
        int s; int valid;
        std::tie(s,valid) = bin.get_bin_s_and_valid(sqrtssqr);
        //the binning may not extend to the full resolution of the map
        PHASER_ASSERT(valid == 0);
        SigmaP[s] += std::norm(FCALC[r]);
      //P1, no weight, no centrics
        SumWeight[s]++;
      }
    }
    for (int s = 0; s < bin.numbins(); s++)
    {
      PHASER_ASSERT(SumWeight[s]);
      SigmaP[s] /= SumWeight[s];
    }
    return SigmaP;
  }

  void
  Map::apply_bfactor(double b_iso)
  {
    //apply bfactor to unsharpen ecalc values
    for (int r = 0; r < MILLER.size(); r++)
    {
      double ssqr = UC.cctbxUC.d_star_sq(MILLER[r]);
      FCALC[r] *= std::exp(-ssqr*b_iso* 0.25);
    }
  }

  double Map::available_hires(std::pair<double,double> cell_scale) const
  {
    double hires = std::numeric_limits<double>::max();
    double scale = std::max(cell_scale.first,cell_scale.second);
    UnitCell minuc(scale*UC.GetBox());
    for (int r = 0; r < MILLER.size(); r++)
    {
      hires = std::min(hires,minuc.cctbxUC.d(MILLER[r]));
    }
    //further limit by cell scale interpolation requirement
    hires = UC.limit_resolution(hires,cell_scale);
    return hires;
  }

  double Map::solvent_fraction(double PROTEIN_VOLUME) const
  {
    // --- convert to mask
    double map_volume = UC.A()*UC.B()*UC.C();//orthogonal!
    //allow for 25% wrong, tight edges
    //wang is a scale factor, more or less
    return 1.0-(PROTEIN_VOLUME/map_volume);
  }

  scitbx::af::shared<double>
  Map::density(std::pair<double,double> cell_scale,bool interpolate)
  {
    fft::real_to_complex_3d rfft(SG.type(),UC.cctbxUC);
    rfft.shannon(2);
    double hires = available_hires(cell_scale);
    rfft.calculate_gridding(hires,cell_scale,interpolate);
    rfft.allocate();
    //should be 5A map
    // --- do the fft
    rfft.backward(MILLER,FCALC);
    scitbx::af::shared<double> mapdensity(rfft.real_map_unpadded.size());
    for (int i = 0; i < rfft.real_map_unpadded.size(); i++)
      mapdensity[i] = rfft.real_map_unpadded[i];
    return mapdensity;
  }

  std::pair<double,principal_statistics>
  Map::wang_trace(
       double VOLUME_FACTOR,
       double PROTEIN_VOLUME,
       dvect3 CENTRE,
       std::vector<PdbRecord>& trace
       )
  {
    SpaceGroup SG("P 1");
    fft::real_to_complex_3d rfft(SG.type(),UC.cctbxUC);
    rfft.shannon(2);
    std::pair<double,double> cell_scale = {1,1};
    rfft.calculate_gridding(available_hires(cell_scale),cell_scale,false);
    rfft.allocate();
    //should be 5A map

    // --- do the fft
    rfft.backward(MILLER,FCALC);

    //put weights on reasonable scale 0-1
    double maxocc(0);
    for (int i = 0; i < rfft.real_map_unpadded.size(); i++)
      maxocc = std::max(maxocc,rfft.real_map_unpadded[i]);
    for (int i = 0; i < rfft.real_map_unpadded.size(); i++)
      rfft.real_map_unpadded[i] /= maxocc;

    // --- convert to mask
    //allow for 25% wrong, tight edges
    //wang is a scale factor, more or less
    double protein_volume = PROTEIN_VOLUME * VOLUME_FACTOR;
    double fracsol = solvent_fraction(protein_volume);
    double wang_radius = 3.0; //default is 5
    cctbx::maptbx::map_to_wang_mask(rfft.real_map_unpadded,UC.cctbxUC,fracsol,wang_radius);

    //convert mask points to frac coordinates
    //for peak search
    double nx = rfft.real_map_unpadded.accessor()[0];
    double ny = rfft.real_map_unpadded.accessor()[1];
    double nz = rfft.real_map_unpadded.accessor()[2];

    double max_radius = std::numeric_limits<double>::max();
    principal_statistics statistics;
    {
      std::vector<PdbRecord> thisTrace;
      for (int lx = 0; lx < nx; lx++) {
      for (int ly = 0; ly < ny; ly++) {
      for (int lz = 0; lz < nz; lz++) {
        if (rfft.real_map_unpadded(lx,ly,lz) > 0.5) // wang mask is 0 or 1
        {
          PdbRecord card;
          dvect3 X(lx/nx,ly/ny,lz/nz);
          //these coordinates could be in the corners of the box but
          // we want them to be centred on the centre
          af_dvect3 SYMMETRY = SG.symmetry_xyz_cell(X,1);
          //AJM change from phaser which is +/- 2 cells
          double mindistsqr = std::numeric_limits<double>::max();
          for (int isym = 0; isym < SYMMETRY.size(); isym++)
          {
            dvect3 orth = SYMMETRY[isym]; //frac
            orth[0] *= UC.A();
            orth[1] *= UC.B();
            orth[2] *= UC.C();
            double distsqr = (orth-CENTRE)*(orth-CENTRE);
            //use the x that minimizes the distance to the centre
            if (distsqr < mindistsqr)
            {
              card.X = orth;
              mindistsqr = distsqr;
            }
          }
       //   card.O = rfft.real_map_unpadded(lx,ly,lz);
          thisTrace.push_back(card);
        }
      }}}
      af_dvect3 sites;
      for (int a = 0; a < thisTrace.size(); a++)
        sites.push_back(thisTrace[a].X);
      statistics.set(sites);
      if (statistics.MAX_RADIUS < max_radius)
      {
        trace = thisTrace;
        max_radius = statistics.MAX_RADIUS;
      }
    }
    dvect3 grid_diagonal(UC.A()/nx,UC.B()/ny,UC.C()/nz);
    double sampling_distance = std::sqrt(grid_diagonal*grid_diagonal) + DEF_PPT;
    return { sampling_distance,statistics };
  }

} //phasertng
