//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/src/Spanning.h>

namespace phasertng {

  //Span Lattice
  void Phasertng::runSPAN()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with correct mode");

    DAGDB.shift_tracker(hashing(),input.running_mode);

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false);
    logBlank(where);
    }}

    logUnderLine(out::LOGFILE,"Calculate Spanning");
    logTab(out::LOGFILE,"Determine if poses span asymmetric unit within tolerances for gaps");
    double minrad(5.);
    double maxrad(minrad);
    for (auto& entry : DAGDB.ENTRIES)
      maxrad = std::max(maxrad,entry.second.MEAN_RADIUS);
    maxrad *= 2.;
    double steprad = std::floor(std::max(minrad,(maxrad-minrad)/5.)); //5 steps max
    bool io_gaps = input.value__boolean.at(".phasertng.spanning.find_gaps.").value_or_default();
    size_t  io_nprint = input.value__int_number.at(".phasertng.spanning.maximum_printed.").value_or_default();
    size_t  io_nspan = input.value__int_number.at(".phasertng.spanning.maximum_stored.").value_or_default();
    if (io_nspan)
      io_nprint = std::min(io_nspan,io_nprint);
    int  io_distance = input.value__flt_number.at(".phasertng.spanning.distance.").value_or_default();
    //identifier for output coordinates, if required
    logTab(out::LOGFILE,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logProgressBarStart(out::LOGFILE,"Spanning",std::max(io_nprint,DAGDB.size()));
    int i(-1);
    sv_ivect3 spans(DAGDB.size()); //ivect3 because bool vec3 not available
    std::map<double,sv_ivect3> distspans; //ivect3 because bool vec3 not available
    DAGDB.restart();
    if (io_gaps) //repeats the default distance
      for (double distance = minrad; distance <= maxrad; distance += steprad)
        distspans[distance] = sv_ivect3(DAGDB.size());
    int n(1),N(DAGDB.size());
    while (!DAGDB.at_end())
    {
      i++; //starting from -1
      out::stream where = out::VERBOSE;
      std::string nN = nNtos(n++,N);
      ivect3& ispans = spans[i];
      //first is n=2
      if (io_nspan > 0 and i > io_nspan)
      {
        logProgressBarNext(out::LOGFILE);
        continue;
      }
      logUnderLine(where,"Spanning" + nN);
      if (DAGDB.WORK->SPANS == 'Y')
      {
        logTab(where,"Spanning already achieved by previous substructure");
        logProgressBarNext(out::LOGFILE);
        continue;
      }
      DAGDB.WORK->SPANS = '?';
      if (REFLCOLSMAP.MAP)
      {
        DAGDB.WORK->generic_flag = true;
        ispans = ivect3(2,1,1);
        DAGDB.WORK->SPANS = 'U';
        logTab(where,"Map: spanning ignored");
        logProgressBarNext(out::LOGFILE);
        continue;
      }
     // Trace trace;
     //we could use trace rather than coordinates
     //but we would need to combine structure_for_refinement with spans
     //so that the network generation accounted for differences in sampling between the pose components
     //here we calculate the asu first then find the network (based on uniform sampling)
      DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,DAGDB.WORK->TRACKER.ULTIMATE);
      Coordinates coordinates;
      //init a few things
      coordinates.REFLID = REFLCOLSMAP.reflid();
      coordinates.MODLID = dogtag.identify();
      coordinates.TIMESTAMP = TimeStamp();
      coordinates.SG = SpaceGroup("P 1"); // because the symmetry is expanded
      coordinates.UC = REFLCOLSMAP.UC;
      std::map<std::string,Coordinates> debug_coords;
      Spanning spanning;
      spanning.init_node(DAGDB.WORK);
      spanning.structure_for_spanning(coordinates); //reference,forward declaration
      af_string output;
      bool testing(Suite::Extra());
      std::tie(ispans,output) = spanning.spans(io_distance,coordinates,testing,debug_coords);
      logTabArray(out::VERBOSE,output);
      if (Suite::Extra())
      {
        out::stream where = out::TESTING;
        coordinates.SetFileSystem(DataBase(),dogtag.FileName(".spans.pdb"));
        logFileWritten(where,WriteFiles(),"Debug Spans",coordinates);
        coordinates.create_directories_for_file();
        if (WriteFiles()) coordinates.write_to_disk();
        phaser_assert(debug_coords.size());
        for (auto& item : debug_coords)
        {
          std::string  dir = item.first;
          Coordinates& cdir = item.second;
          cdir.SetFileSystem(DataBase(),dogtag.FileName(".spans." + dir + ".pdb"));
          logFileWritten(where,WriteFiles(),"Debug Spans " + dir,cdir);
          cdir.create_directories_for_file();
          if (WriteFiles()) cdir.write_to_disk();
        }
      }
      DAGDB.WORK->SPANS = (ispans[0] and ispans[1] and ispans[2]) ? 'Y':'N';
      DAGDB.WORK->generic_flag = DAGDB.WORK->SPANS == 'Y'; //for purge, if used
      if (io_gaps)
      {
        for (auto& distspan : distspans)
        {
          std::tie(distspan.second[n],std::ignore) =
              spanning.spans(distspan.first,coordinates,false,debug_coords);
        }
      }
      if (!logProgressBarNext(out::LOGFILE)) break;
    }
    logProgressBarEnd(out::LOGFILE);

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Spanning Summary");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"Allowed Gap Distance: " + dtos(io_distance,2));
    if (io_gaps) logTab(where,"Additional distances as listed");
    logTab(where,"--------");
    int w = itow(io_nprint);
    int i(1);
    std::string line = snprintftos( "%-*s %-13s  %5s  ", w,"#","Space Group","Spans");
    if (io_gaps)
    {
      for (auto distspan : distspans)
        line += snprintftos("|%5s", dtoi(distspan.first).c_str());
    }
    logTab(where,line);
    std::string line2 = snprintftos( "%-*s %-13s  %5s  ", w,"","","xyz=a");
    logTab(where,line2);
    logTab(where,"--------");
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      ivect3& ispan = spans[i-1];
      if (REFLCOLSMAP.MAP)
        logTab(where,snprintftos( "%-*d %-13s UUU=%c", w,i, node->SG->CCP4.c_str(),node->SPANS));
      else
      {
        std::string line = snprintftos(
            "%-*d %-13s  %c%c%c=%c  ",
            w,i,
            node->SG->CCP4.c_str(),
            ispan[0]?'+':'-',ispan[1]?'+':'-',ispan[2]?'+':'-',node->SPANS
        );
        if (io_gaps)
        {
          for (auto& distspan : distspans)
          {
            ivect3& dspan = distspan.second[i-1];
            char dspans = dspan[0] and dspan[1] and dspan[2] ? 'Y':'N';
            line += snprintftos(
            "|%c%c%c=%c",
            dspan[0]?'+':'-',dspan[1]?'+':'-',dspan[2]?'+':'-',dspans
            );
          }
        }
        logTab(where,line);
      }
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      node->ANNOTATION += " SPAN=" + chtos(node->SPANS);
    }

    if (false) //no purge
    {
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge by spanning cutoff");
      int before = DAGDB.size();
      auto before_list = DAGDB.NODES;
      DAGDB.NODES.erase_invalid();
      logEllipsisShut(where);
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(DAGDB.size()));
    }
  }

}  //end namespace phasertng
