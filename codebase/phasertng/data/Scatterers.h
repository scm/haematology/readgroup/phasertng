//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Scatterers_class__
#define __phasertng_Scatterers_class__
#include <phasertng/main/Error.h>
#include <phasertng/enum/formfactor_particle.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/eltbx/fp_fdp.h>
#include <boost/logic/tribool.hpp>
#include <scitbx/array_family/shared.h>
#include <tuple>
#include <map>
#include <set>
#include <complex>
#include <string>
#include <vector>

typedef std::complex<double> cmplex;
typedef scitbx::af::shared<std::string> af_string;

namespace phasertng {

class Cluster; //forward declaration

typedef std::string elementType;
typedef std::string scatterType;
typedef std::string clusterType;
typedef std::tuple<cctbx::eltbx::xray_scattering::gaussian, //form factor
                   cmplex, //fpfdp
                   double //radius
                   >  clusterHarkerData;
typedef std::tuple<cctbx::eltbx::xray_scattering::gaussian,
                   cctbx::eltbx::xray_scattering::gaussian,
                   cmplex,
                   cmplex,
                   double>  clusterDebyeData;

class Scatterers
{
  private:
    //the lookups for these are periodic table elements
    const double dwave = 0.02; //wavelength step for near-edge test
    const double dfdp = 0.2; //fractional change in fdp that determines edge

  public:
    formfactor::particle  PARTICLE;
    std::map<scatterType,bool> MOVE_TO_EDGE;
    std::map<elementType,std::tuple<double,double,std::string>> FLUORESCENCE_SCAN;
    std::map<scatterType,cctbx::eltbx::fp_fdp> FP_FDP; //strongest

  private:
    double WAVELENGTH = 0;
    bool ANOMALOUS = false;
    std::map<elementType,cctbx::eltbx::xray_scattering::gaussian> FORM_FACTOR;
    std::map<scatterType,bool> NEAR_EDGE;
    std::map<scatterType,double> PEAKWAVE;
    std::map<scatterType,std::string> FIX_FDP;
    std::map<scatterType,double> VDW;
    std::map<clusterType,std::vector<clusterHarkerData> > HARKER;
    std::map<clusterType,std::vector<clusterDebyeData> > DEBYE;
    void create_cluster_harker(clusterType,Cluster*);
    void create_cluster_debye(clusterType,Cluster*);

  public:
    Scatterers() {}
    Scatterers(const boost::logic::tribool,const double);
    void insert(scatterType);
    void insert(Cluster);

    int  size() { return FP_FDP.size(); }

    std::pair<std::string,cctbx::eltbx::fp_fdp> strongest_anomalous_scatterer() const;
    std::string TABLE();

    cctbx::eltbx::fp_fdp  fp_fdp(std::string,const double=0) const; //default ssqr 0 for elements
    double                fo(scatterType,const double) const; //no default ssqr, even for elements
    double                fp(scatterType,const double=0) const; //default ssqr 0 for elements
    double                fo_plus_fp(scatterType,const double) const;
    double                fdp(scatterType,const double=0) const; //default ssqr 0 for elements
    bool                  fix_fdp(scatterType) const;
    void                  set_fdp(scatterType,const double);
    int                   number_of_atomtypes() const;
    bool                  has_atomtype(std::string) const;
    std::set<scatterType> atomtypes() const;
    std::string           atomtype(int) const;
    double                van_der_waals_radius(scatterType) const;
    cctbx::eltbx::xray_scattering::gaussian form_factor(std::string) const;

    std::string formfactor_table_name() const;

  public:
    af_string logScatterers(std::string) const;
};

} //phasertng
#endif
