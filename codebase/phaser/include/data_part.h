//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_PART_CLASS__
#define __PHASER_DATA_PART_CLASS__
#include <string>

namespace phaser {

struct data_part
{
  bool        ANOMALOUS;
  std::string ESTIMATOR = "RMS"; //only option
  double      RMSID;
  bool        map_format;
};

} //phaser

#endif
