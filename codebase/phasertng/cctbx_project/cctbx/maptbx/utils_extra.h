#ifndef CCTBX_MAPTBX_UTILS_H_EXTRA
#define CCTBX_MAPTBX_UTILS_H_EXTRA

#include <cctbx/maptbx/utils.h>

namespace cctbx { namespace maptbx {

/* Follows maskdemo.cpp from clipper revision 153 */

void
map_to_wang_mask(
  af::versa<double, af::c_grid<3> >& map_data,
  cctbx::uctbx::unit_cell& unit_cell,
  double solvent_fraction = 0.5,
  double wang_radius = 5.0,
  double rho_weight = 1.0,
  double sig_weight = 2.0
  );

void
map_to_wang_cutout(
  af::versa<double, af::c_grid<3> >& map_data,
  cctbx::uctbx::unit_cell& unit_cell,
  double solvent_fraction = 0.5,
  double wang_radius = 5.0,
  double rho_weight = 1.0,
  double sig_weight = 2.0
  );

}} // namespace cctbx::maptbx

#endif // CCTBX_MAPTBX_UTILS_H
