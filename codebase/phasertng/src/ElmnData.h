//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ElmnData_class__
#define __phasertng_ElmnData_class__
#include <phasertng/src/Elmn.h>
#include <phasertng/main/pointers.h>
#include <set>

namespace phasertng {

namespace pod { class FastPoseType; } //forward declaration

  class ElmnData : public Elmn
  {
    public:
      ElmnData(int L,std::pair<int,char> H) : Elmn(L),HIGHSYMM(H) { resize_elmn(H.first); }

      std::pair<int,char> HIGHSYMM={1,'Z'}; //degree of rotational symmetry about axis

    public:
      af_string
      ELMNxR2(
          double,
          const_ReflectionsPtr,
          sv_bool,
          const_EpsilonPtr,
          int,
          bool,
          double,
          pod::FastPoseType*,
          double = 0 //search bfactor, for data as ensemble in self rotation function
        );
  };

}//phasertng
#endif
