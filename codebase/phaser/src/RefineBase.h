//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_REFINE_BASE_CLASS__
#define __PHASER_REFINE_BASE_CLASS__
#include <phaser/main/Phaser.h>

namespace phaser {

class RefineBase  //AJM no longer abstract
{
  public:
    RefineBase() { npars_ref = npars_all = 0; refinePar.clear(); }

  protected:
    int    npars_ref;
    int    npars_all;
    bool1D refinePar;
};

} //phaser

#endif
