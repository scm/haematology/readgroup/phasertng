//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_REFINE_SAD_CLASS__
#define __PHASER_REFINE_SAD_CLASS__
#include <phaser/src/DataSAD.h>
#include <phaser/ep_objects/data_atom.h>
#include <phaser/include/data_ffts.h>
#include <phaser/include/data_restraint.h>
#include <phaser/include/VarianceEP.h>
#include <phaser/include/ProtocolSAD.h>
#include <phasertng/dtmin/bounds.h>
#include <phasertng/dtmin/reparams.h>
#include <phasertng/dtmin/includes.h>
#include <phaser/pod/Derivatives.h>
#include <phaser/pod/Curvatures.h>
#include <phaser/include/data_llgc.h>
#include <phaser/src/RefineBase.h> //AJM from Base2
#include <phaser/src/EqualUnity.h>
#include <phaser/src/LlgcMapSites.h>
#include <phaser/src/Integration.h>
#include <phasertng/data/Cluster.h>
#include <phaser/pod/probType.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/Assert.h>
#include <phasertng/io/entry/Coordinates.h>
#include <phasertng/io/entry/ModelsFcalc.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <tuple>

namespace phaser {

typedef scitbx::af::shared<cctbx::hendrickson_lattman<double> > af_hl;

class RefineSAD : public DataSAD, public RefineBase
{
double DEF_EP_BMIN=0.1;
double DEF_EP_BMAX=1000.0;

  public:
    //phasertng::CoordinatesPtr COORDINATES; //moved to DataSAD.h
    phasertng::ModelsFcalcPtr FCALCS = nullptr;
    bool                      ANOMALOUS;

  public:
    //Single value
    double   ScaleK,ScaleU;
    std::map<std::string,int> atomtype2t; //lookup array for t
    data_ffts FFTvDIRECT;
    bool input_atoms,input_partial;
    bool1D input_fix_fdp;
    double wilson_llg;
    float1D wilson_llg_refl;
    ProtocolSAD protocol;

    //Data dependent on atom [a]
    std::vector<phasertng::Cluster> CLUSTER;

    //Data dependent on atomtype [t]
    float1D AtomFdpInit;

    //set
    std::set<int> pdb_deleted_set;

  public:
    af_string logDataStats(unsigned);
    af_string logScale();
    af_string logAtoms();
    af_string logScat();
    af_string logSigmaA(bool=false);
    af_string logStatInfo();
    af_string logFOM(bool=true);
    af_string logOutliers(bool);
    af_string logCurrent();
    af_string logInitial();
    af_string logFinal();
    af_string logProtocolPars(bool);

  public:
    RefineSAD();
    //constructor for C++/phaser
    std::pair<af_string,std::string>
    setRefineSAD(
              std::string,
              af::double6,
              af::shared<miller::index<int> >,
              data_spots, //POS
              data_spots, //NEG
              data_tncs, //tncs epsn
              af_atom,
              std::string, //cluster_pdb
              data_bins,
              std::map<std::string,cctbx::eltbx::fp_fdp>, //atomtype fp fdp
              data_outl, //outliers
              data_restraint, double, //wilson restraints
              data_restraint, //sphericity restraints
              data_restraint, //fdp restraints
              data_part,
              data_ffts,
              float1D, //wilson_llg_refl
              bool //verbose
    );
    ~RefineSAD() {}

  private:
    //real contructor
    std::pair<af_string,std::string>
         init(af_bool, //working set
              data_restraint, double, //wilson restraints
              data_restraint, //sphericity restraints
              data_restraint, //fdp restraints
              data_ffts, //partial structure
              float1D //wilson_llg_refl
    );

    af_string initAtomic(af_atom,std::map<std::string,cctbx::eltbx::fp_fdp>,std::string,bool);

  public:
    unsigned    nsigmaa_ref,nsigmaa_all,nscales_ref,nscales_all;
    bool        reversed;
    bool        use_fft;
    bool        sad_target_anom_only;
    double      WilsonB;
    data_restraint SPHERICITY,WILSON,FDP;
    bool        FIX_ATOMIC,FIX_SIGMAA,FIX_SCALES;
    cctbx::sgtbx::site_symmetry_table site_sym_table;

    // -- Data dependent on reflection [r]
    unsigned1D  ibin; // integration steps
    af_cmplx    FHpos; // calculated heavy atom sf
    af_cmplx    FHneg; // calculated heavy atom sf
    af_cmplx    FApos;
    af_cmplx    FAneg;
    af_cmplx    LLGRAD_SF; // LLG coefficient
    float1D     sigDsqr;
    float1D     sigPlus;
    float1D     DphiA;
    float1D     DphiB;
    af_bool     working; // LLG coefficient
    af_float    FWTA,PHWTA;
    af_float    FOMpos,PHIBpos,FOMneg,PHIBneg;
    af_hl       HLpos,HLneg;

    //Data dependent on reflection and atomtype [r][t]
    float2D  fo_plus_fp; // FormFactor value for reflection

    std::vector<EqualUnity>  acentPhsIntg;
    bool fixed_num_integration_points;
    scitbx::fftpack::complex_to_complex_3d<double> cfft;
    double u_base;

//dL_by_dAtomPar [a] atom
  void dL_by_dAtomicPar(unsigned);
  void d2L_by_dAtomicPar2(unsigned);
  void dL_by_dSigmaaPar(unsigned);
  void d2L_by_dSigmaaPar2(unsigned);
  void dL_by_dScalesPar(unsigned);
  void d2L_by_dScalesPar2(unsigned);
  float1D dL_by_dPar,d2L_by_dPar2;

//dL_by_dFunctionPar function for each reflection
  double dL_by_dReFHpos,dL_by_dImFHpos,dL_by_dReFHneg,dL_by_dImFHneg,dL_by_dSDsqr,dL_by_dSA,dL_by_dSB,dL_by_dSP;

//dFH_by_dAtomPar
//constants
  double dReFHpos_by_dK, dImFHpos_by_dK, dReFHneg_by_dK, dImFHneg_by_dK;
  double dReFHpos_by_dB, dImFHpos_by_dB, dReFHneg_by_dB, dImFHneg_by_dB;
  double dReFHpos_by_dPartK, dImFHpos_by_dPartK, dReFHneg_by_dPartK, dImFHneg_by_dPartK;
  double dReFHpos_by_dPartU, dImFHpos_by_dPartU, dReFHneg_by_dPartU, dImFHneg_by_dPartU;
  float1D dReFHpos_by_dX, dImFHpos_by_dX, dReFHneg_by_dX, dImFHneg_by_dX;
  float1D dReFHpos_by_dY, dImFHpos_by_dY, dReFHneg_by_dY, dImFHneg_by_dY;
  float1D dReFHpos_by_dZ, dImFHpos_by_dZ, dReFHneg_by_dZ, dImFHneg_by_dZ;
  float1D dReFHpos_by_dO, dImFHpos_by_dO, dReFHneg_by_dO, dImFHneg_by_dO;
  double dReFHpos_by_dO_px, dImFHpos_by_dO_px, dReFHneg_by_dO_px, dImFHneg_by_dO_px;
  float1D dReFHpos_by_dIB, dImFHpos_by_dIB, dReFHneg_by_dIB, dImFHneg_by_dIB;
  std::vector<dmat6 >
          dReFHpos_by_dAB,dImFHpos_by_dAB,dReFHneg_by_dAB,dImFHneg_by_dAB;
  float1D dReFHpos_by_dFdp,dImFHpos_by_dFdp,dReFHneg_by_dFdp,dImFHneg_by_dFdp;

  double d2ReFHpos_by_dK2, d2ImFHpos_by_dK2, d2ReFHneg_by_dK2, d2ImFHneg_by_dK2;
  double d2ReFHpos_by_dB2, d2ImFHpos_by_dB2, d2ReFHneg_by_dB2, d2ImFHneg_by_dB2;
  double d2ReFHpos_by_dPartK2, d2ImFHpos_by_dPartK2, d2ReFHneg_by_dPartK2, d2ImFHneg_by_dPartK2;
  double d2ReFHpos_by_dPartU2, d2ImFHpos_by_dPartU2, d2ReFHneg_by_dPartU2, d2ImFHneg_by_dPartU2;
  float1D d2ReFHpos_by_dX2, d2ImFHpos_by_dX2, d2ReFHneg_by_dX2, d2ImFHneg_by_dX2;
  float1D d2ReFHpos_by_dY2, d2ImFHpos_by_dY2, d2ReFHneg_by_dY2, d2ImFHneg_by_dY2;
  float1D d2ReFHpos_by_dZ2, d2ImFHpos_by_dZ2, d2ReFHneg_by_dZ2, d2ImFHneg_by_dZ2;
  float1D d2ReFHpos_by_dO2, d2ImFHpos_by_dO2, d2ReFHneg_by_dO2, d2ImFHneg_by_dO2;
  double d2ReFHpos_by_dO2_px, d2ImFHpos_by_dO2_px, d2ReFHneg_by_dO2_px, d2ImFHneg_by_dO2_px;
  float1D d2ReFHpos_by_dIB2, d2ImFHpos_by_dIB2, d2ReFHneg_by_dIB2, d2ImFHneg_by_dIB2;
  std::vector<dmat6 >
          d2ReFHpos_by_dAB2,d2ImFHpos_by_dAB2,d2ReFHneg_by_dAB2,d2ImFHneg_by_dAB2;
  float1D d2ReFHpos_by_dFdp2,d2ImFHpos_by_dFdp2,d2ReFHneg_by_dFdp2,d2ImFHneg_by_dFdp2;

  //reflection hessian
  double d2L_by_dSDsqr2,d2L_by_dSA2,d2L_by_dSB2,d2L_by_dSP2;
  double d2L_by_dReFHpos2,d2L_by_dReFHpos_dImFHpos,d2L_by_dReFHpos_dReFHneg,d2L_by_dReFHpos_dImFHneg;
  double d2L_by_dImFHpos2,d2L_by_dImFHpos_dReFHneg,d2L_by_dImFHpos_dImFHneg;
  double d2L_by_dReFHneg2,d2L_by_dReFHneg_dImFHneg;
  double d2L_by_dImFHneg2;

  public:
    void applyShift(sv_double&);
    void setProtocol(protocolPtr);
    af_string cleanUp();
    std::vector<phasertng::dtmin::Reparams>  getRepar();
    bool1D getRefineMask(protocolPtr);
    double Rfactor();
    double overall_fom();
    double targetFn(int,int);
    double wilson_restraint_likelihood();
    double sphericity_restraint_likelihood();
    double fdp_restraint_likelihood();
    double gradientFn(sv_double&);
    Derivatives dL_by_dFC();
    Curvatures d2L_by_dFC2();
    double hessianFn(versa_flex_double&,bool&);
    std::string whatAmI(int&);
    std::vector<double> getRefinePars();
    std::vector<double> getLargeShifts();
    std::vector<phasertng::dtmin::bounds> getLowerBounds();
    std::vector<phasertng::dtmin::bounds> getUpperBounds();

    //-- Log Functions

  private:
    void    calcAtomicData();
    void    calcScalesData();
    double  lowerO();
    double  lowerU();
    double  upperU();
    double  tolU();
    double  largeUshift();
    double  SigmaH(unsigned&);
    void    prepareAtomicFFT();
    void    calcAtomicDataFFT();
    void    calcAtomicDataSum();
    void    resizeAtomArrays();
    void    calcGradFFT(af::shared<miller::index<int> >,af_cmplx);
    double  constrain_restrain_gradient(sv_double&);
    double  constrain_restrain_hessian(versa_flex_double&);

    //acentric reflections, F+ and F- present
    acentric acentricReflIntgSAD1(unsigned&,bool=false);
    acentric acentricReflIntgSAD2(unsigned&);
    double   acentricReflProbSAD2(unsigned&,bool,double);
    double   acentricReflGradSAD2(unsigned&,bool=true,bool=true);
    double   acentricReflHessSAD2(unsigned&,bool=true,bool=true);

    //centric reflections
    centric  centricReflIntgSAD2(unsigned&);
    double   centricReflGradSAD2(unsigned&,bool=true,bool=true);
    double   centricReflHessSAD2(unsigned&,bool=true,bool=true);

    //acentric singleton reflections, only one of F+ and F- present (bool indicates which)
    singleton   singletonReflIntgSAD2(unsigned&,bool);
    double   singletonReflGradSAD2(unsigned&,bool,bool=true,bool=true);
    double   singletonReflHessSAD2(unsigned&,bool,bool=true,bool=true);

  public:
    void setFC(af_cmplx,af_cmplx);
    void calcSigmaaData();
    void calcIntegrationPoints();
    void calcOutliers();
    af_string rejectOutliers(bool);
    void calcBinStats();
    double calcFOMsubset(af_float);
    void calcPhsStats();
    void calcVariances();
    af_string addAtoms(af_atom,bool);
    int  numAtoms() { return atoms.size(); }
    af_string restoreAtoms(int,std::multimap<int,restored_data>&,int);
    af_string anisoAtoms(int,std::set<int>&,int);
    af_string deleteAtoms(int,std::set<int>&,int);
    af_string changeAtoms(changed_list&);
    std::pair<changed_list,af_string> findChangedAtoms(data_llgc);
    std::pair<std::set<int>,af_string> findDeletedAtoms(std::vector<std::pair<int,std::string> >);
    void setAtomsInput(af_atom);

    af_cmplx     calcGradCoefs(std::string,af_float=af_float(),af_float=af_float());
    std::tuple<LlgcMapSites,af_string,af_string> calcGradMaps(std::string,data_llgc,std::string,int);

    void setupIntegrationPoints(int);
    void setTarget(std::string);
    void setFixFdp(std::map<std::string,bool> );
    double get_refined_scaleK() { return ScaleK; }
    double get_refined_scaleU() { return ScaleU; }
    void set_scaleK(double K) { ScaleK = K; }
    void set_scaleU(double U) { ScaleU = U; }
    af_bool  getSelected() const { return selected; }
    af_bool  getCentric()  { return cent; }
    af_hl    getHL(int=0);

  public:
    double getMaxDistSpecial(sv_double&,sv_double&,bool1D&,sv_double&,double&);
    std::vector<std::pair<int,int> >  getAniso();
    std::pair<bool,af_string> calcPartialStructure();
    double wilsonFn(float1D&);

    //sub-structure determination
  public:
    //std::tuple< af::shared<cmplxType>, af::shared<cmplxType>,double> fast_search(std::string,double);
    std::pair<double,af_string> phase_o_phrenia(size_t=60);
    af::shared<cmplxType> calcFpart(bool=false); //selected_only
    double LESSTF1(std::string,double,af::shared<cmplxType>,af_float);
  private:
    std::pair<double,double> LdL_by_dUsqr_at_0(unsigned r, double fp, double fdp);

  public: //python
    double stats_numbins() { return allStats.numbins(); }
    double stats_hires() { return allStats.HiRes(); }
    double stats_lores() { return allStats.LoRes(); }
    double stats_acentric_fom() { return acentStats.FOM(); }
    double stats_acentric_num() { return acentStats.Num(); }
    double stats_centric_fom() { return centStats.FOM(); }
    double stats_centric_num() { return centStats.Num(); }
    double stats_fom() { return allStats.FOM(); }
    double stats_num() { return allStats.Num(); }

};

} //phaser
#endif
