//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/table/matthews_probability.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/table/average_scattering.h>

namespace phasertng {

  //Cell_content_analysis
  void Phasertng::runCCA()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain only one node");
    dag::Node& node = DAGDB.NODES.front();

    bool io_average_solvent = false;
    const double& hires = REFLECTIONS->data_hires();
    int io_mult = input.value__int_number.at(".phasertng.composition.multiplicity_restriction.").value_or_default();
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    int multiplicity_restriction(1);
    if (io_mult == nmol) //there is no tNCS or they are the same
      multiplicity_restriction = io_mult;
    else if (io_mult > nmol and (io_mult % nmol == 0))
      multiplicity_restriction = io_mult;
    else if (io_mult > nmol and (io_mult % nmol != 0))
      multiplicity_restriction = io_mult*nmol;
    else if (nmol > io_mult and (nmol % io_mult == 0))
      multiplicity_restriction = nmol;
    else if (nmol > io_mult and (nmol % io_mult != 0))
      multiplicity_restriction = io_mult*nmol;
    int io_zmult = 1;
    if (input.value__boolean.at(".phasertng.composition.use_order_z_expansion_ratio.").value_or_default())
    if (!input.value__int_number.at(".phasertng.composition.order_z_expansion_ratio.").is_default())
    {
      //expand any already applied composition restriction and then add the order_z_expansion_ratio condition
      io_zmult = input.value__int_number.at(".phasertng.composition.order_z_expansion_ratio.").value_or_default();
      multiplicity_restriction *= io_zmult;
    }
    double io_sc_min = input.value__percent_paired.at(".phasertng.composition.solvent_range.").value_or_default_min();
    double io_sc_max = input.value__percent_paired.at(".phasertng.composition.solvent_range.").value_or_default_max();
    double io_max = (io_sc_min == 100.) ? 12300. : 1.23/(1-io_sc_max/100.);
    double io_min = (io_sc_max == 100.) ? 12300. : 1.23/(1-io_sc_min/100.);
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Multiplicity Restriction;");
    logTab(where,"From User Request: " + itos(io_mult));
    logTab(where,"From tNCS Restriction: " + itos(DAGDB.WORK->TNCS_ORDER));
    if (DAGDB.WORK->TNCS_ORDER and DAGDB.WORK->TNCS_MODELLED)
       logTab(where,"--tNCS present in model so restriction not applied");
    logTab(where,"From Space Group Expansion: " + itos(io_zmult));
    logTab(where,"---");
    logTab(where,"Combined  : " + itos(multiplicity_restriction));

    //only allow one file, so that concatenation does not accidentally increase the composition
    dag::Node node_model = node;
    dag::Node node_sequence = node;
    bool biological_unit_as_model =
        !input.value__path.at(".phasertng.biological_unit.model.filename.").is_default();
    bool biological_unit_as_sequence =
      !input.value__path.at(".phasertng.biological_unit.sequence.filename.").is_default() or
      input.size(".phasertng.biological_unit.hetatm.");
    if (biological_unit_as_model)
    {
      Models models;
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Model");
      logTab(out::LOGFILE,"Biological unit from model;");
      models.SetFileSystem( input.value__path.at(".phasertng.biological_unit.model.filename.").value_or_default());
      logTab(where,"Model read from file: " + models.qfstrq());
      models.read_from_disk(); ///ReadPdb !!!
      if (models.has_read_errors())
      throw Error(err::FATAL,models.read_errors());
      phaser_assert(models.PDB.size() == 1);
      int m(0);
      if (!input.value__boolean.at(".phasertng.biological_unit.model.via_sequence.").value_or_default())
      {
        Sequence sequence(models.SELENOMETHIONINE);
        bool use_unknown(false);
        sequence.ParseModel(models.PDB[m],models.COUNT_SSBOND,use_unknown);
        sequence.initialize(true,true);
        logTab(where,"Use atomic composition from model");
        node_model.COMPOSITION = models.composition(m);
        node_model.generic_float = models.scattering()[m];
        node_model.generic_float2 = models.molecular_weight()[m];
        node_model.generic_int = static_cast<int>(sequence.composition_type());
      }
      else
      {
        logTab(where,"Use model as composition via the sequence");
        //models.sequence(m,sequence);
        Sequence sequence(models.SELENOMETHIONINE);
        bool use_unknown(false);
        sequence.ParseModel(models.PDB[m],models.COUNT_SSBOND,use_unknown);
        sequence.initialize(true,true);
        logTabArray(out::VERBOSE,sequence.logComposition(""));
        logTabArray(out::LOGFILE,sequence.logSequence(""));
        node_model.COMPOSITION = sequence.composition();
        node_model.generic_float = sequence.total_scattering();
        node_model.generic_float2 = sequence.total_molecular_weight();
        node_model.generic_int = static_cast<int>(sequence.composition_type());
      }
    }
    if (biological_unit_as_sequence) //get the data for both, but only use one in the end
    {
      logUnderLine(out::LOGFILE,"Sequence");
      logTab(out::LOGFILE,"Biological unit from sequence or atomic composition");
      Cluster cluster;
      if (!input.value__path.at(".phasertng.cluster_compound.filename.").is_default())
      {
        logTab(out::LOGFILE,"Cluster read from file: " + cluster.qfstrq());
        cluster.SetFileSystem(input.value__path.at(".phasertng.cluster_compound.filename.").value_or_default());
        cluster.ReadPdb();
        logTab(out::LOGFILE,"Number of atoms in cluster: " + itos(cluster.composition().size()));
      }
      double io_water_inflation = 1.0;
      io_water_inflation += input.value__percent.at(".phasertng.biological_unit.water_percent.").value_or_default_fraction();
      Sequence sequence(
        input.value__boolean.at(".phasertng.biological_unit.selenomethionine.").value_or_default(),
        input.value__boolean.at(".phasertng.biological_unit.disulphide.").value_or_default(),
        input.value__boolean.at(".phasertng.biological_unit.deuterium.").value_or_default(),
        cluster, io_water_inflation);
      if (!input.value__path.at(".phasertng.biological_unit.sequence.filename.").is_default())
      {
        sequence.SetFileSystem( input.value__path.at(".phasertng.biological_unit.sequence.filename.").value_or_default());
        logTab(out::LOGFILE,"Sequence file: " + sequence.qfstrq());
        int m = input.value__int_number.at(".phasertng.biological_unit.sequence.multiplicity.").value_or_default();
        logTab(out::LOGFILE,"Sequence multiplicity: " + itos(m));
        try {
          sequence.ReadSeq(m);
        } catch (...)
        { throw Error(err::FILEOPEN,sequence.qfstrq()); }
      }
      //add any extra (heavy) atoms not deduced from the sequence
      logTab(out::VERBOSE,"Hetatms = " + itos(input.size(".phasertng.biological_unit.hetatm.")));
      for (int c = 0; c < input.size(".phasertng.biological_unit.hetatm."); c++)
      {
        auto hetatm = input.array__strings.at(".phasertng.biological_unit.hetatm.").at(c).value_or_default();
        std::string msg = "Use e.g. phasertng biological_unit hetatm Fe 2";
        if (hetatm.size() != 2) throw Error(err::INPUT,msg);;
        try {
          logTab(out::LOGFILE,"Hetatm: " + hetatm[0] + " " + hetatm[1]);
          std::string element = hetatm[0];
          int number = std::atoi(hetatm[1].c_str());
          sequence.AddAtom(element,number);
        } catch (...)
        { throw Error(err::INPUT,msg); }
      }
      sequence.initialize(true,true);
      logTabArray(out::LOGFILE,sequence.logComposition(""));
      logTabArray(out::LOGFILE,sequence.logSequence(""));
      composition_t AssemblyComp = sequence.composition();
      node_sequence.COMPOSITION = AssemblyComp;
      node_sequence.generic_float = sequence.total_scattering();
      node_sequence.generic_float2 = sequence.total_molecular_weight();
      node_sequence.generic_int = static_cast<int>(sequence.composition_type());
    }

    int sg_nsymm = REFLECTIONS->SG.group().order_z();
    double uc_volume = REFLECTIONS->UC.cctbxUC.volume();

    if (biological_unit_as_model and biological_unit_as_sequence)
    {
      node = node_sequence;
      if (node_model.generic_float > node_sequence.generic_float)
      {
        node = node_model;
        out::stream where = out::LOGFILE;
        logTab(where,"Composition from sequence lower than composition from model, use composition from model");
        logBlank(where);
      }
    }
    else if (biological_unit_as_model)
    {
      node = node_model;
    }
    else if (biological_unit_as_sequence)
    {
      node = node_sequence;
    }
    else
    {
      out::stream where = out::LOGFILE;
      io_average_solvent = true;
      logUnderLine(where,"Average Solvent");
      logTab(where,"No input of biological unit sequence or model");
      logTab(where,"Unit cell contents assumed to be protein");
      composition::type ctype = composition::PROTEIN;
      node.generic_int = static_cast<int>(ctype);
      double VM,solvent;
      if (input.value__percent.at(".phasertng.biological_unit.solvent.").is_default())
      {
        VM = table::matthews_vm(hires,ctype);
        solvent = (1-1.23/VM)*100;
        logTab(where,"Resolution for Matthews analysis: " + dtos(hires,2));
        logTab(where,"Average VM for resolution: " + dtos(VM,2));
        logTab(where,"Average solvent for resolution: " + dtos(solvent,0) + "%");
        logTab(where,"Scattering determined by Matthews coefficient");
      }
      else
      {
        solvent = input.value__percent.at(".phasertng.biological_unit.solvent.").value_or_default();
        VM = 1.23/(1-solvent/100.);
        logTab(where,"Solvent content: " + dtos(solvent,0) + "%");
        logTab(where,"Matthews coefficient: " + dtos(VM,2));
      }
      double meanDa = uc_volume/sg_nsymm/VM; //composition is per asu
      node.generic_float = table::average_scattering(meanDa,ctype).scattering;
      node.COMPOSITION = table::average_scattering(meanDa,ctype).composition;
      node.generic_float2 = meanDa;
      logWarning("Solvent content used for total scattering in asymmetric unit");
      logBlank(where);
    }
    double total_molecular_weight = node.generic_float2;
    REFLECTIONS->TOTAL_SCATTERING = node.generic_float;
    if (REFLECTIONS->TOTAL_SCATTERING == 0)
    {
      DAGDB.shift_tracker(hashing(),input.running_mode);
      logWarning("No scattering recognised in assembly");
      return;
    }
    logBlank(out::SUMMARY);
    logTab(out::SUMMARY,"Scattering from biological unit: " + itos(REFLECTIONS->TOTAL_SCATTERING));
    logBlank(out::SUMMARY);

    bool io_map = input.value__boolean.at(".phasertng.composition.map.").value_or_default();

    logUnderLine(out::LOGFILE,"Matthews cell content analysis");
    if (io_map)
    {
      out::stream where = out::LOGFILE;
      logTab(where,"Data from map, so Matthews analysis not applicable");
      logBlank(where);
    }

    //calculate the matthews anyway just change the position of the logging

    double maximum_z = (uc_volume/(sg_nsymm*(1.23*total_molecular_weight)));
    int imaximum_z = std::max(0.,std::floor(maximum_z));
    {{
    out::stream where = out::LOGFILE;
    logTab(where,"Biological unit scattering:   " + itos(REFLECTIONS->TOTAL_SCATTERING));
    logTab(where,"Biological unit weight (Da):  " + dtos(total_molecular_weight,2));
    logTab(where,"Volume of crystal unit cell:  " + dtos(uc_volume,2));
    logTab(where,"Number of symmetry operators: " + itos(sg_nsymm));
    logTab(where,"Volume of asymmetric unit:    " + dtos(uc_volume/sg_nsymm,2));
    logTab(where,"Maximum Z number:             " + dtos(maximum_z));
    logTab(where,"Maximum integer Z number:     " + itos(imaximum_z));
    logTab(where,"Translational NCS order:      " + itos(multiplicity_restriction));
    logBlank(where);
    }}

    bool fits_in_crystal(true);
    if (!io_map and imaximum_z < 0)
    {
      logWarning("The assembly will not fit in the unit cell volume");
      fits_in_crystal = false;
    }

    struct sort_data { double VM,probVM,solvent,i; double Z = 0; bool outlier;
       bool operator<(const sort_data &a) const { return Z < a.Z; } //for std::set unique in Z
       bool isint() const { return std::round(Z) == Z; }
       std::string Zstr() const { return isint() ? itos(Z,7,false) : dtos(Z,7,4,false,true,true); } };
    auto cmpprob = []( const sort_data &a, const sort_data &b )
         { return a.probVM >  //> for reverse sort
                  b.probVM; };
    auto cmpz = []( const sort_data &a, const sort_data &b )
         { return a.Z < b.Z; };
    auto eqvz = []( const sort_data &a, const sort_data &b )
         { return a.Z == b.Z; };

    bool compzisdef = input.value__flt_number.at(".phasertng.composition.number_in_asymmetric_unit.").is_default();
    double compz = compzisdef ? 1 : input.value__flt_number.at(".phasertng.composition.number_in_asymmetric_unit.").value_or_default();
    int ratioz = input.value__int_number.at(".phasertng.space_group_expansion.order_z_expansion_ratio.").value_or_default();
    if (ratioz > 1 and !compzisdef)
    {
      logChevron(out::LOGFILE,"Data Expansion Ratio Z");
      logTab(out::LOGFILE,"Data have been expanded with ratio " + itos(ratioz));
      compz *= ratioz;
      logTab(out::LOGFILE,"Number in asymmetric unit after application of ratio " + dtos(compz,0));
      logBlank(out::LOGFILE);
    }

    composition::type ctype = static_cast<composition::type>(node.generic_int);
    std::vector<sort_data> sort_order;
    sort_data singlez;
    if (fits_in_crystal)
    {
      double minVM(1.23); //solvent content 0 for specific gravity of 0.74 cm^3/gm
      double maxVM(100); //infinite for 100% solvent
      int maxz(10000); // pure programming paranoia, should exit before, could loop forever
      if (ctype == composition::UNDEFINED)
        ctype = composition::PROTEIN; //default when only hetatm entered

      //if (!compzisdef)
      { //always report the single case for interest and
        // when io_mult is incorrectly applied eg when the poses are input directly in scotty
        double z = compz;
        double MW(z*total_molecular_weight);
        double thisVM(uc_volume/(sg_nsymm*MW));
        singlez.VM = thisVM;
        singlez.probVM = table::matthews_probability(hires,thisVM,ctype);
        singlez.solvent = (1-1.23/thisVM)*100;
        singlez.Z = z;
        singlez.i = -999; //flag for first
        singlez.outlier = (thisVM > maxVM or thisVM < minVM);
        sort_order.push_back(singlez);
      }
      for (int z = 1; z <= maxz; z++) //so that there isn't an infinite loop
      {
        double MW(z*total_molecular_weight);
        double thisVM(uc_volume/(sg_nsymm*MW));
        if (thisVM >= maxVM) continue; //very high solvent content
        if (thisVM <= minVM) break; //very low solvent content - end
        if (z%multiplicity_restriction == 0) //allowed with tNCS applied to biological unit
        {
          sort_data next;
          next.VM = thisVM;
          next.probVM = table::matthews_probability(hires,thisVM,ctype);
          next.solvent = (1-1.23/thisVM)*100;
          next.Z = z;
          next.i = sort_order.size();
          next.outlier = false; //irrelevant, only interesting for singlez
          sort_order.push_back(next);
        }
        if (z == maxz) //paranoia
        {
          logTab(out::LOGFILE,"Z value of " + itos(maxz) + " reached - no more considered");
        }
      }
    }

    int p(10),q(10);
    std::set<sort_data> sort_accepted; //unique list
    std::vector<sort_data> sort_order_in_range;
    if (sort_order.size() > 0)
    {
      std::sort(sort_order.begin(),sort_order.end(),cmpprob);
      int imaxProb = sort_order[0].i;

      std::sort(sort_order.begin(),sort_order.end(),cmpz);
      {{
      std::sort(sort_order.begin(),sort_order.end(),cmpprob);
      out::stream where = out::LOGFILE;
      logBlank(where);
      logTab(where,"Solvent range = " + dtos(io_sc_min,2) + "->" + dtos(io_sc_max,2));
      logTab(where,"Most probable solvent for resolution = " + dtos(sort_order[0].solvent,2));
      double MW(uc_volume/(sg_nsymm*sort_order[0].VM));
      logTab(where,"Most probable Z for resolution =  " + dtos(sort_order[0].Z,0));

      std::sort(sort_order.begin(),sort_order.end(),cmpz);
      sv_int Z;
      for (auto next : sort_order)
        if (std::find(Z.begin(), Z.end(), next.Z) == Z.end())
          Z.push_back(next.Z);
      logTab(where,"Search Z = " + ivtos(Z));
      logBlank(where);
      if (Z.size())
      {
        size_t pt = dtos(Z.back()*REFLECTIONS->TOTAL_SCATTERING,0).size();
        p = std::max(std::string("scattering").size(),pt);
        q = dtos(Z.back()*total_molecular_weight,0).size();
      }
      }}

      {{
      out::stream where = out::SUMMARY;
      logTableTop(where,"Matthews Analysis");
      logTab(where,snprintftos(
          "Resolution for Matthews calculation: %5.2f",
          hires));
      logTab(where,"Z is the number of copies of the assembly");
      logTab(where,snprintftos(
          "%-7s %*s %*s %5s  %c%-7s %10s",
          "Z",p,"scattering",q,"MW","VM",'%',"solvent","rel-freq"));
      std::sort(sort_order.begin(),sort_order.end(),cmpz);
      sort_order.erase(std::unique(sort_order.begin(),sort_order.end(),eqvz), sort_order.end());
      for (auto next : sort_order)
      {
        logTab(where,snprintftos(
              "%-7s %*.0f %*.0f %5.2f  %8.2f %10.0f %s",
              io_average_solvent ? "--" : next.Zstr().c_str(), //if average solvent there is only one entry
              p,next.Z*REFLECTIONS->TOTAL_SCATTERING,
              q,next.Z*total_molecular_weight,
              next.VM,
              next.solvent,
              1000*next.probVM,
              (next.i != imaxProb) ? "" : "<== most probable"));
      }
      logBlank(where);
      logTableEnd(where);
      }}

      {{
      out::stream where = io_map ? out::VERBOSE : out::SUMMARY;
      logUnderLine(where,"Histogram of relative frequencies of solvent content");
      logTab(where,"Frequency of most common solvent content normalized to 1");
      double sc_inc(2.5);
      logTab(where,"In range: " + dtos(io_sc_min,2) + "% -> " + dtos(io_sc_max,2) + "%");
      logTab(where,"+/* outside/inside requested solvent range");
      logBlank(where);
      double grad(0.1);
      logTab(where,"     <--- relative frequency --->");
      //10 lots of 5 width = 50, so use prob*50
      logTab(where,snprintftos(
          "     %-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f",
          grad*0,grad*1,grad*2,grad*3,grad*4,grad*5,grad*6,grad*7,grad*8,grad*9,grad*10));
      logTab(where,snprintftos(
          "     %-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s",
          "|","|","|","|","|","|","|","|","|","|","|"));
      for (double sc = 100; sc >= sc_inc; sc -= sc_inc)
      {
        double vm = (sc == 100.) ? 12300. : 1.23/(1-sc/100.);
        std::string stars;
        double prob = (sc == 100.) ? 0.001 : table::matthews_probability(hires,vm,ctype);
        std::string h_char("-");
        for (auto next : sort_order)
        {
          if (next.solvent < sc+sc_inc and next.solvent >= sc )
            h_char = (next.solvent>=io_sc_min and next.solvent<=io_sc_max) ? "*" : "+";
        }
        for (int star = 0; star < prob*50; star++)
          stars += h_char;
        for (auto next : sort_order)
        {
          if (next.isint() and next.solvent < sc+sc_inc and next.solvent >= sc)
            stars += io_average_solvent ? " (average solvent)" : " (Z=" + dtos(next.Z,0) + ")";
        }
        logTab(where,snprintftos(
            "%5.1f %s",
            sc,stars.c_str()));
      }
      logBlank(where);
      }}

      {{
        out::stream where = out::VERBOSE;
        Loggraph loggraph;
        loggraph.title = "Matthews Analysis";
        loggraph.scatter = true;
        loggraph.data_labels = "Z Probability";
        for (auto next : sort_order)
          loggraph.data_text += dtos(next.Z) + " "  +
                                dtos(next.probVM) + " " +
                                "\n";
        loggraph.graph.resize(1);
        loggraph.graph[0] = "Matthews Probability:N:1,2";
        logGraph(where,loggraph);
      }}

      phaser_assert(io_sc_min <= io_sc_max);
      phaser_assert(io_min <= io_max);

      {{
      input.value__boolean.at(".phasertng.composition.sort_by_probability.").value_or_default() ?
        std::sort(sort_order.begin(),sort_order.end(),cmpprob):
        std::sort(sort_order.begin(),sort_order.end(),cmpz);
        //because the molecular replacement autosearch will do incremental addition
        //we must add in order of z not probability
        //but for substructure searches
        //we can search in order of probability as the searches are independent
      for (auto next : sort_order)
      {
        if (next.VM >= io_min and next.VM <= io_max and (nmol == 1 or int(next.Z)%nmol == 0))
        { //we need to exclude some if nmol > 1 because this feeds into the ccs mode, and 1 is incompativle with seek=2 from permutations
          sort_order_in_range.push_back(next);
          sort_accepted.insert(next);
        }
      }
      }}
    }
    if (sort_order_in_range.size() == 0) //backup result
    { //see below for where this is selected
      if (!compzisdef)
      {
        sort_accepted.insert(singlez);
      }
      else if (sort_order.size())
      {
        std::sort(sort_order.begin(),sort_order.end(),cmpprob);
        sort_accepted.insert(sort_order[0]);
      }
      else if (singlez.solvent > 0 and (nmol == 1)) //backup backup result
      { //we need to exclude this if nmol > 1 because this feeds into the ccs mode, and 1 is incompativle with seek=2 from permutations
        sort_accepted.insert(singlez);
      }
    }
    if (!compzisdef)
    {
      if (!singlez.outlier) //if this is unique, not in the allowed list by multiplicity ie an override
      {
        sort_accepted = { singlez };
      }
    }

    //add here with unique, sorted, list
    for (int i = 0; i < input.size(".phasertng.matthews.vm."); i++)
    {
      input.pop_back(".phasertng.matthews.vm.");
      input.pop_back(".phasertng.matthews.probability.");
      input.pop_back(".phasertng.matthews.z.");
    }
    //add here with unique, sorted, list
    for (auto accepted : sort_accepted)
    {
      input.push_back(".phasertng.matthews.vm.",accepted.VM);
      input.push_back(".phasertng.matthews.probability.",accepted.probVM);
      input.push_back(".phasertng.matthews.z.",accepted.Z);
    }

    {{
      out::stream where = out::LOGFILE;
      logTableTop(where,"Matthews Analysis (Sorted)");
      logTab(where,"Maximum Z = " + dtos(maximum_z));
      logTab(where,"In range: " + dtos(io_sc_min,2) + "% -> " + dtos(io_sc_max,2) + "%");
      logTab(where,snprintftos(
          "%-7s %*s %*s %5s %c%-7s %10s",
          "Z",p,"e",q,"MW","VM",'%',"solvent","rel-freq"));
      bool inaccept = false;
      for (auto next : sort_accepted)
        inaccept = (inaccept or next.Z == singlez.Z);
      if (singlez.Z and !inaccept)
      {
        logTab(where,snprintftos(
              "%-7s %*.0f %*.0f %5.2f %8.2f %10.0f",
              io_average_solvent ? "--" : singlez.Zstr().c_str(), //if average solvent there is only one entry
              p,singlez.Z*REFLECTIONS->TOTAL_SCATTERING,
              q,singlez.Z*total_molecular_weight,
              singlez.VM,
              singlez.solvent,
              1000*singlez.probVM));
        logTab(where,snprintftos(
              "%-7s %*s %*s %5s %8s %10s",
               "---",p,"---",q,"---","---","---","---"));
      }
      std::sort(sort_order.begin(),sort_order.end(),cmpprob);
      //for (auto next : sort_order)
      for (auto next : sort_accepted)
      {
        logTab(where,snprintftos(
              "%-7s %*.0f %*.0f %5.2f %8.2f %10.0f",
              io_average_solvent ? "--" : next.Zstr().c_str(), //if average solvent there is only one entry
              p,next.Z*REFLECTIONS->TOTAL_SCATTERING,
              q,next.Z*total_molecular_weight,
              next.VM,
              next.solvent,
              1000*next.probVM));
      }
      logTab(where,snprintftos(
              "%-7s %*s %*s %5s %8s %10s",
               "---",p,"---",q,"---","---","---","---"));
      logBlank(where);
      logTableEnd(where);
    }}

    if (!sort_order_in_range.size() and !io_map)
    {
      logWarning("No compositions within allowed Matthews coefficient range");
      if (maximum_z < multiplicity_restriction)
        logWarning("Tncs restriction is higher than maximum Z");
    }
    else if (io_map)
    {
      logAdvisory("Unit cell for map is arbitrary so cell content analysis is for information only");
    }
    //even if there are none within the limits, the default below is still taken as the best

    //this value is set here to default most likely
    //but can be reset to different values to give different scales to the data
    //using the probabilities output above
    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Composition Summary");
    if (!compzisdef)
    { //use the given value, with basic checks
      //allow values outside of range if they are input
      std::sort(sort_order.begin(),sort_order.end(),cmpz);
      double io_z = compz; //after application of ratio
      (io_z == std::round(io_z)) ?
        logTab(where,"Input Z = " + dtos(io_z,7,4,false,true,true)) :
        logTab(where,"Input Z = " + dtos(io_z));
      input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(io_z);
      std::sort(sort_order.begin(),sort_order.end(),cmpprob);
      std::string messz = "Input number in asymmetric unit";
      if (io_map)
      { //input may exceed the allowed number, if the map is smaller than the model
        if (singlez.solvent <= 0) //singlez is the input value
          logAdvisory(messz + " exceeds volume of map");
        else logTab(where,messz + " does not exceed volume of input map");
      }
      else if (!io_map)
      {
        if (sort_order.size() and io_z != sort_order[0].Z)
          logTab(where,messz + " is not best predicted number");
        if (singlez.solvent > 0) //singlez is the input value
          logTab(where,messz + " is compatible with cell volume");
        else
          throw Error(err::INPUT,messz + " is incompatible with cell volume");
        logTab(where,"Solvent Content = " + dtos(singlez.solvent,0) + "%");
      }
    }
    else //default calculations
    {
      std::string messz = "Number in asymmetric unit defaults to";
      if (io_map)
      {
        logTab(where,messz + " 1");
        input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(1);
      }
      else if (!io_map)
      {
        if (sort_order_in_range.size())
        {
          std::sort(sort_order_in_range.begin(),sort_order_in_range.end(),cmpprob);
          logTab(where,messz + " highest probable in range");
          input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(sort_order_in_range[0].Z);
        }
        else if (sort_order.size())
        {
          std::sort(sort_order.begin(),sort_order.end(),cmpprob);
          logTab(where,messz + " highest probable outside range");
          input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(sort_order[0].Z);
        }
        else if (singlez.solvent > 0) //singlez is the z=1 value if not io_z
        {
          if (multiplicity_restriction > 1)
//this should be an assert, because it will be in sort_order if multiplicity_restrictions == 1
            logWarning("Multiplicity restrictions are incompatible with cell volume");
          logTab(where,messz + " 1");
          logAdvisory("If multiplicity restrictions are due to tncs, tncs must be already present in the model and tncs order set to 1");
          input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(1);
        }
        else //there is no hope
        {
          input.value__flt_number.at(".phasertng.cell_content_scaling.z.").set_value(0);
          throw Error(err::INPUT,"Single copy is incompatible with cell volume");
        }
      }
    }
/*
    if (io_map)
    {
      //maximum value must be higher than the value input as the composition
      //this may exceed the allowed number, if the map is smaller than the model
      double nasu = input.value__flt_number.at(".phasertng.composition.number_in_asymmetric_unit.").value_or_default();
      maximum_z = std::max(maximum_z,static_cast<int>(std::ceil(nasu)));
      input.value__int_number.at(".phasertng.composition.maximum_z.").set_value(maximum_z);
    }
*/
    input.value__int_number.at(".phasertng.composition.maximum_z.").set_value(imaximum_z);
    }}
    if (imaximum_z > 100)
      logWarning("Biological unit represents less than 1% of asymmetric unit");

    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::CCA);
    }}

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //end namespace phaser
