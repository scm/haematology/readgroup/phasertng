#ifndef __phasertng_mintbx_includes__
#define __phasertng_mintbx_includes__
#include <phasertng/dtmin/bounds.h>
#include <phasertng/dtmin/reparams.h>
#include <string>
#include <vector>
#include <utility> // for std::pair
#include <tuple>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/accessors/c_grid.h>

namespace af = scitbx::af;
namespace fn = scitbx::fn;

typedef scitbx::af::shared<std::string>     af_string;
typedef std::vector<std::string>            sv_string;
typedef std::vector<double>                 sv_double;
typedef std::vector<bool>                   sv_bool;
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;

typedef double              TargetType;
typedef std::vector<double> GradientType;
typedef versa_flex_double   HessianType;
typedef bool                DiagonalType;

struct TargetGradientType
{
  TargetType Target;
  GradientType Gradient;
  TargetGradientType(int n=0)
       : Target(0),
         Gradient(n,0) {}
};

inline TargetGradientType operator*(const double&A, const TargetGradientType &B)
{
  TargetGradientType tmp;
  tmp.Target = A*B.Target;
  tmp.Gradient = B.Gradient;
  std::for_each(tmp.Gradient.begin(), tmp.Gradient.end(), [&A](double &x){x *= A; });
  return tmp;
}

struct TargetGradientHessianType : public TargetGradientType
{
  HessianType Hessian;
  DiagonalType Diagonal;
  TargetGradientHessianType(int n=0,bool d=false)
       : TargetGradientType(n),
         Hessian(scitbx::af::flex_grid<>(n,n),0),
         Diagonal(d)
       {}

  TargetGradientHessianType& operator+=(const TargetGradientHessianType &B)
  {
    assert(this->Diagonal == B.Diagonal);
    Target += B.Target;
    int nmp = Gradient.size();
    assert(nmp == B.Gradient.size());
    assert(Hessian.size() == nmp*nmp);
    assert(B.Hessian.size() == nmp*nmp);
    for (int i=0; i<nmp; i++)
    {
      Gradient[i] += B.Gradient[i];
      for (int j=0; j<nmp; j++)
        Hessian(i,j) += B.Hessian(i,j);
    }
    return *this;
  }

};

inline TargetGradientHessianType operator*(const double&A, const TargetGradientHessianType &B)
{
  TargetGradientHessianType tmp;
  tmp.Target = A*B.Target;
  tmp.Gradient = B.Gradient;
  tmp.Hessian = B.Hessian;
  tmp.Diagonal = B.Diagonal;
  std::for_each(tmp.Gradient.begin(), tmp.Gradient.end(), [&A](double &x){x *= A; });
  std::for_each(tmp.Hessian.begin(), tmp.Hessian.end(), [&A](double &x){x *= A; });
  return tmp;
}


#endif
