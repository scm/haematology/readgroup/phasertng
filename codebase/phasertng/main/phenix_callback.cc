//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/phenix_callback.h>

namespace phasertng {

// placeholders for phenix_callback object
void phenix_callback::warn(const std::string& message) {}
void phenix_callback::advise(const std::string& message) {}

void phenix_callback::startProgressBar(const std::string& label, int size) {}

void phenix_callback::incrementProgressBar() {}

void phenix_callback::endProgressBar() {}

void phenix_callback::loggraph(const std::string& title,
                               const std::string& data) {}

void phenix_callback::call_back(const std::string& message,
                                const std::string& data) {}

phenix_callback::~phenix_callback() {}

}//namespace phaser
