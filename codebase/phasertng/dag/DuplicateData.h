//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_DuplicateData_class_
#define __phasertng_dag_DuplicateData_class_
#include <cctbx/sgtbx/search_symmetry.h>
#include <phasertng/dag/Node.h>
#include <phasertng/src/SpaceGroup.h>

typedef std::vector<dmat33> sv_dmat33;

namespace phasertng {
namespace dag {

class DuplicateData
{
  public:
  DuplicateData() {}

  DuplicateData(dag::Node& node_k,bool);

  sv_dmat33
  Euclid_Rmat(cctbx::uctbx::unit_cell CELL);

  SpaceGroup Euclid;
  SpaceGroup SG;
  dmat33 cb_op_r_to_reference_setting;
  dmat33 cb_op_r_from_reference_setting;
  af::tiny<bool, 3> isshift;
};

}}
#endif
