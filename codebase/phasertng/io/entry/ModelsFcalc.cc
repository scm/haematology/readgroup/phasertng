//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/ModelsFcalc.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/hoist.h>
#include <phasertng/io/entry/Models.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/cctbx_project/iotbx/ccp4_map.h>
#include <phasertng/math/fft/real_to_complex_3d.h>

namespace phasertng {

  void
  ModelsFcalc::SetData(af_cmplex DATA_)
  {
    //SetHistory must be called first to set the NMODELS parameter correctly
    phaser_assert(NMODELS == DATA_.size()/MILLER.size());
    FCALC = math::Matrix<cmplex>(NMODELS,MILLER.size(),DATA_.begin());
  }

  ModelsFcalc::ModelsFcalc(
      af_string HISTORY_,af::double6 CELL_,af_millnx MILLER_,af_cmplex DATA_) : EntryBase()
  {
    SetHistory(HISTORY_);
    UC = UnitCell(CELL_);
    MILLER = MILLER_;
    SetData(DATA_);
    initialize_bin(BIN);
  }

  void
  ModelsFcalc::initialize_bin(Bin bin)
  {
    BIN = bin;
    BIN.RBIN.resize(MILLER.size());
    bool has_invalid(false);
    int ibin; int error;
    for (int r = 0; r < MILLER.size(); r++)
    {
      double s = UC.cctbxUC.two_stol(MILLER[r]);
      std::tie(ibin,error) = BIN.get_bin_s_and_valid(s);
      BIN.RBIN[r] = ibin;
      if (error>0) has_invalid = true;
    }
    PHASER_ASSERT(!has_invalid);
    BIN.set_numinbin();
  }

  void
  ModelsFcalc::apply_sigmap(const math::Matrix<double>& SigmaP)
  {
    phaser_assert(FCALC.num_rows());
    for (int m = 0; m < FCALC.num_rows(); m++)
    {
      //EC = FC/sqrt(epsilon*SIGMAP)
      phaser_assert(FCALC.num_cols());
      for (int r = 0; r < FCALC.num_cols(); r++)
      {
        int ibin = BIN.RBIN[r];
        FCALC(m,r) /= std::sqrt(SigmaP(m,ibin));
      }
    }
  }

  void
  ModelsFcalc::SetModelsFcalc(ModelsFcalc& other)
  {
    SetHistory(other.get_history());
    UC = UnitCell(other.get_cell()); //cell used for structure factor calculation
    MILLER = other.get_miller();
    SetData(other.get_data());
    initialize_bin(other.BIN);
  }

  void
  ModelsFcalc::p1_structure_factor_calculation(
      Models* MODELS,
      UnitCell UC_,
      const double savereso,
      const cctbx::uctbx::unit_cell gridding_uc,
      const double SHANNON,
      const int NTHREADS,
      const bool   BULK_USE,
      const double FSOL,
      const double BSOL
    )
  {
    UC = UC_; //save
    cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
    cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
    fft::real_to_complex_3d rfft(SgInfoP1,UC.cctbxUC);
    rfft.shannon(SHANNON);
    rfft.calculate_gridding(savereso,gridding_uc);
    rfft.allocate();
    FCALC_HIRES = rfft.dmin; //save this value
    phaser_assert(MODELS->PDB.size() > 0);
    this->NMODELS = MODELS->PDB.size();

    //Use the B-factor present in the pdb file!!!
    //This will have been corrected for Wilson-B
    //first structure factor calculate calculates the miller list
    //second and subsequent extract FCALCs using this miller list to assure consistency
    //  although we would proably get the same miller list each time from cctbx
    {//first sf calc memory
      // --- collect the scatterers
      af::shared<cctbx::xray::scatterer<> > xray_scatterers;
      int mzero = 0; //not mtrace, the default
      const int natoms = MODELS->PDB[mzero].size();
      xray_scatterers.resize(natoms);
      for (int a = 0; a < natoms; a++) //m = 0, not the lowest rms (default)
      {
        const PdbRecord& A = MODELS->PDB[mzero][a];
        dvect3 orthXyz = A.X; //coordinates in situ, no PT
        double BISO(A.B); //change to U below
        double UISO = cctbx::adptbx::b_as_u(BISO);
        cctbx::xray::scatterer<> scatterer(
          "another atom", // label
          UC.fractionalization_matrix()*(orthXyz), // site
          UISO, // u_iso
          A.O, // occupancy
          A.get_element(),
          0, // fp
          0); // fdp
        scatterer.apply_symmetry(UC.cctbxUC, SgOpsP1);
        xray_scatterers[a] = scatterer;
      }
      af::const_ref<cmplex, af::c_grid_padded<3> > complex_map = rfft.forward(xray_scatterers);
      //first pdb, calculate millers and structure factors to required resolution limit
      bool discard_indices_affected_by_aliasing(true);
      cctbx::maptbx::structure_factors::from_map<double> from_map(
             UC.cctbxUC,
             SgInfoP1,
             rfft.anomalous_flag,
             rfft.dmin, //extract to full resolution with any slack added
             complex_map,
             rfft.conjugate_flag,
             discard_indices_affected_by_aliasing
           );

      MILLER = from_map.miller_indices();
      af_cmplex mfcalc = from_map.data();
      cctbx::xray::apply_u_extra(
          UC.cctbxUC,
          rfft.u_extra,
          MILLER.const_ref(),
          mfcalc.ref(),
          rfft.multiplier()
       );
      //add bulk solvent correction
      //bulk solvent addition after eliminate_u_extra_and_normalize.
      //Much higher correlation with fmodel FC's in test case
      if (BULK_USE)
      {
        rfft.add_bulk_solvent( xray_scatterers, FSOL, BSOL, MILLER, mfcalc);
      }
      phaser_assert(MILLER.size());

      FCALC.resize(MODELS->PDB.size(),MILLER.size());
      for (int r = 0; r < MILLER.size(); r++)
        FCALC(0,r) = mfcalc[r];
    }//memory


    for (int m = 1; m < MODELS->PDB.size(); m++)
    {
      // --- collect the scatterers
      af::shared<cctbx::xray::scatterer<> > xray_scatterers;
      const int natoms = MODELS->PDB[m].size();
      xray_scatterers.resize(natoms);
      for (int a = 0; a < natoms; a++)
      {
        const PdbRecord& A = MODELS->PDB[m][a];
        dvect3 orthXyz = A.X; //reoriented at origin
        double occupancy = A.O;
        double UISO(A.B); //change to U below
        //different from phaser, line below, relative wilson b correction
        if (MODELS->RELATIVE_WILSON_B.size()) UISO -= MODELS->RELATIVE_WILSON_B[m];
        UISO = cctbx::adptbx::b_as_u(UISO);
        cctbx::xray::scatterer<> scatterer(
            "another atom", // label
            UC.fractionalization_matrix()*(orthXyz), // site
            UISO, // u_iso
            occupancy, // occupancy
            A.get_element(), // scattering_type
            0, // fp
            0); // fdp
        scatterer.apply_symmetry(UC.cctbxUC, SgOpsP1);
        xray_scatterers[a] = scatterer;
      }
      af::const_ref<cmplex, af::c_grid_padded<3> > complex_map = rfft.forward(xray_scatterers);

      bool discard_indices_affected_by_aliasing(true);
      cctbx::maptbx::structure_factors::from_map<double> from_map(
                rfft.anomalous_flag,
                MILLER.const_ref(),
                complex_map,
                rfft.conjugate_flag,
                discard_indices_affected_by_aliasing
              );
      af_cmplex mfcalc = from_map.data();
      cctbx::xray::apply_u_extra(
          UC.cctbxUC,
          rfft.u_extra,
          MILLER.const_ref(),
          mfcalc.ref(),
          rfft.multiplier()
       );

      //add bulk solvent correction
      if (BULK_USE)
      {
        rfft.add_bulk_solvent( xray_scatterers, FSOL, BSOL, MILLER, mfcalc);
      }
      for (int r = 0; r < MILLER.size(); r++)
        FCALC(m,r) = mfcalc[r];
    }
  }

  void
  ModelsFcalc::p1_structure_factor_calculation(
      Models* MODELS,
      UnitCell UC_,
      const double HIRES,
      const int NTHREADS,
      const bool   BULK_USE,
      const double FSOL,
      const double BSOL
    )
  {
    UC = UC_; //save
    cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
    cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
    std::pair<double,double> CELL_SCALE = {1,1};
    bool interpolate = false;
    fft::real_to_complex_3d rfft(SgInfoP1,UC.cctbxUC);
    rfft.shannon(3);
    rfft.calculate_gridding(HIRES,CELL_SCALE,interpolate);
    rfft.allocate();
    FCALC_HIRES = rfft.dmin; //save this value

    //Use the B-factor present in the pdb file!!!
    //This will have been corrected for Wilson-B
    //first structure factor calculate calculates the miller list
    //second and subsequent extract FCALCs using this miller list to assure consistency
    //  although we would proably get the same miller list each time from cctbx
    {//first sf calc memory
      // --- collect the scatterers
      af::shared<cctbx::xray::scatterer<> > xray_scatterers;
      int mzero = 0; //not mtrace, the default
      const int natoms = MODELS->PDB[mzero].size();
      xray_scatterers.resize(natoms);
      for (int a = 0; a < natoms; a++) //m = 0, not the lowest rms (default)
      {
        const PdbRecord& A = MODELS->PDB[mzero][a];
        dvect3 orthXyz = A.X; //reoriented at origin
        double BISO(A.B); //change to U below
        double UISO = cctbx::adptbx::b_as_u(BISO);
        cctbx::xray::scatterer<> scatterer(
          "another atom", // label
          UC.fractionalization_matrix()*(orthXyz), // site
          UISO, // u_iso
          A.O, // occupancy
          A.get_element(),
          0, // fp
          0); // fdp
        scatterer.apply_symmetry(UC.cctbxUC, SgOpsP1);
        xray_scatterers[a] = scatterer;
      }
      af::const_ref<cmplex, af::c_grid_padded<3> > complex_map = rfft.forward(xray_scatterers);
      //first pdb, calculate millers and structure factors to required resolution limit
      bool discard_indices_affected_by_aliasing(true);
      cctbx::maptbx::structure_factors::from_map<double> from_map(
             UC.cctbxUC,
             SgInfoP1,
             rfft.anomalous_flag,
             rfft.dmin, //extract to full resolution with any slack added
             complex_map,
             rfft.conjugate_flag,
             discard_indices_affected_by_aliasing
           );

      MILLER = from_map.miller_indices();
      af_cmplex mfcalc = from_map.data();
      cctbx::xray::apply_u_extra(
          UC.cctbxUC,
          rfft.u_extra,
          MILLER.const_ref(),
          mfcalc.ref(),
          rfft.multiplier()
       );
      //add bulk solvent correction
      //bulk solvent addition after eliminate_u_extra_and_normalize.
      //Much higher correlation with fmodel FC's in test case
      if (BULK_USE)
      {
        rfft.add_bulk_solvent( xray_scatterers, FSOL, BSOL, MILLER, mfcalc);
      }
      phaser_assert(MILLER.size());

      std::vector<std::pair<millnx,cmplex>> sorted(MILLER.size());
      for (int r = 0; r < MILLER.size(); r++)
        sorted[r] = { MILLER[r],mfcalc[r] };
      auto cmp = []( const std::pair<millnx,cmplex> &a, const std::pair<millnx,cmplex> &b )
      { return a.first < b.first; }; //millnx all unique
      std::sort(sorted.begin(),sorted.end(),cmp);
      for (int r = 0; r < MILLER.size(); r++)
        MILLER[r] = sorted[r].first;
      FCALC.resize(MODELS->PDB.size(),MILLER.size());
      for (int r = 0; r < sorted.size(); r++)
        FCALC(0,r) = sorted[r].second;
    }//memory

    for (int m = 1; m < MODELS->PDB.size(); m++)
    {
      // --- collect the scatterers
      af::shared<cctbx::xray::scatterer<> > xray_scatterers;
      const int natoms = MODELS->PDB[m].size();
      xray_scatterers.resize(natoms);
      for (int a = 0; a < natoms; a++)
      {
        const PdbRecord& A = MODELS->PDB[m][a];
        dvect3 orthXyz = A.X; //reoriented at origin
        double occupancy = A.O;
        double UISO(A.B); //change to U below
        //different from phaser, line below, relative wilson b correction
        if (MODELS->RELATIVE_WILSON_B.size()) UISO -= MODELS->RELATIVE_WILSON_B[m];
        UISO = cctbx::adptbx::b_as_u(UISO);
        cctbx::xray::scatterer<> scatterer(
            "another atom", // label
            UC.fractionalization_matrix()*(orthXyz), // site
            UISO, // u_iso
            occupancy, // occupancy
            A.get_element(), // scattering_type
            0, // fp
            0); // fdp
        scatterer.apply_symmetry(UC.cctbxUC, SgOpsP1);
        xray_scatterers[a] = scatterer;
      }
      af::const_ref<cmplex, af::c_grid_padded<3> > complex_map = rfft.forward(xray_scatterers);

      bool discard_indices_affected_by_aliasing(true);
      cctbx::maptbx::structure_factors::from_map<double> from_map(
                rfft.anomalous_flag,
                MILLER.const_ref(),
                complex_map,
                rfft.conjugate_flag,
                discard_indices_affected_by_aliasing
              );
      af_cmplex mfcalc = from_map.data();
      cctbx::xray::apply_u_extra(
          UC.cctbxUC,
          rfft.u_extra,
          MILLER.const_ref(),
          mfcalc.ref(),
          rfft.multiplier()
       );

      //add bulk solvent correction
      if (BULK_USE)
      {
        rfft.add_bulk_solvent( xray_scatterers, FSOL, BSOL, MILLER, mfcalc);
      }
      for (int r = 0; r < MILLER.size(); r++)
        FCALC(m,r) = mfcalc[r];
    }
  }

  double
  ModelsFcalc::setup_bins_with_statistically_weighted_bin_width(dvect3 BOX,int M,int N,int W)
  {
    cctbx::uctbx::unit_cell& uc = UC.cctbxUC;
    double vcell = uc.volume();
    double videal = 8*BOX[0]*BOX[1]*BOX[2]; // Double x,y,z dimensions for Shannon sampling of molecular transform
    double f = std::max(1.,vcell/videal); // Correct desired bin width for oversampling if present
    int sW = W*std::max(1.,vcell/videal); // Correct desired bin width for oversampling if present
    //logTab(out::VERBOSE,"resolution limits " + dtos(hires) + " - " + dtos(lores));
   // logTab(out::VERBOSE,"number of reflections " + itos(MILLER.size()));
    //logTab(out::VERBOSE,"min max width " + itos(M) + " - " + itos(N) + ":" + itos(sW));
    af_double TWOSTOL(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
    {
      TWOSTOL[r] = uc.two_stol(MILLER[r]);
    }
    bool do_recount(true); //AJM not done in phaser!!
    BIN.setup(M,N,sW,TWOSTOL,do_recount,FCALC_HIRES);
    for (int s = 0; s < BIN.NUMINBIN.size(); s++) PHASER_ASSERT(BIN.NUMINBIN[s] > 0);
    return f; //statistically weighted bin width
  }

  math::Matrix<double>
  ModelsFcalc::sigmap_with_binning()
  {
    math::Matrix<double> SigmaP(FCALC.num_rows(),BIN.numbins());
    phaser_assert(FCALC.size());
    phaser_assert(BIN.numbins());
    for (int m = 0; m < FCALC.num_rows(); m++)
      for (int s = 0; s < BIN.numbins(); s++)
        SigmaP(m,s) = 0;

    phaser_assert(FCALC.num_rows());
    phaser_assert(BIN.numbins());
    for (int m = 0; m < FCALC.num_rows(); m++)
    {
      sv_double SumWeight(BIN.numbins(),0);
      for (int r = 0; r < MILLER.size(); r++)
      {
        double SSQR = UC.cctbxUC.d_star_sq(MILLER[r]);
        int s = BIN.get_bin_ssqr(SSQR);
        SigmaP(m,s) += std::norm(FCALC(m,r));
        //P1, no weight, no centrics
        SumWeight[s]++;
      }
      for (int s = 0; s < BIN.numbins(); s++)
      {
        phaser_assert(SigmaP(m,s));
        phaser_assert(SumWeight[s]);
        SigmaP(m,s) /= SumWeight[s];
      }
    }
    return SigmaP;
  }

  double
  ModelsFcalc::available_lores()
  {
    //we need to restrict the interpolation so that it doesn't use 000
    //because this is the constant in real space, and is not interpolateable
    return std::min(DEF_LORES,UC.cctbxUC.d(millnx(1,1,1)));
  }

  std::pair<double,versa_flex_double>
  ModelsFcalc::calculate_correlation_matrix(
      double lowest_reso,
      double targetZ_CC, // Target CC/sigma(CC) for worst off-diagonal element
      double resolution_d_min
    )
  {
    //uses internal binning
    math::Matrix<double> SigmaP = this->sigmap_with_binning();
    double midSsqr(0);
    phaser_assert(FCALC.size());
    //find the bin that corresponds with the lowest resolution
    //s=0 is the lowest resolution bin
    int loresbin(0),startbin(0);
    for (loresbin = 0; loresbin < BIN.numbins(); loresbin++)
    {
      if (BIN.LoRes(loresbin) < lowest_reso or loresbin == BIN.numbins()-1)
        break;
    }
    for (startbin = 0; startbin < BIN.numbins(); startbin++)
    {
      if (BIN.LoRes(startbin) < resolution_d_min or startbin == BIN.numbins()-1)
        break;
    }
    versa_flex_double CorMat;
    phaser_assert(NMODELS > 0);
    CorMat.resize(scitbx::af::flex_grid<>(NMODELS,NMODELS));
    // Carry out correlation calculations at specified resolution limit, but back off if the contribution
    // from the worst pair of model will not be significant at that resolution.
    // Aim for 500-2000 reflections for statistics
    double targetOffDiag(0),minOffDiag(0);
    int ibin = startbin;
    PHASER_ASSERT(ibin < BIN.numbins());
    do
    {
      minOffDiag = 1.;

      // Compute correlation matrix
      sv_double sqrtSigmaP(NMODELS,0.); // Normalisation factors
      for (int m = 0; m < NMODELS; m++)
      {
        phaser_assert(m < SigmaP.num_rows());
        PHASER_ASSERT(ibin < SigmaP.num_cols());
        sqrtSigmaP[m] = std::sqrt(SigmaP(m,ibin));
        PHASER_ASSERT(sqrtSigmaP[m] > 0);
      }
      int nref(0);
      for (int m = 0; m < NMODELS; m++)
      {
        for (int n = m; n < NMODELS; n++)
        {
          if (n == m)
          {
            CorMat(m,n) = 1.;
          }
          else
          {
            double sumF1F2(0.);
            int nrefl(0);
            for (int r = 0; r < MILLER.size(); r++)
            {
              int s = BIN.RBIN[r];
              if (s == ibin)
              {
                sumF1F2 += std::real((FCALC(m,r))*std::conj(FCALC(n,r)));
                nrefl++;
              }
            }
            nref = nrefl;
            PHASER_ASSERT(nref > 0);
            CorMat(m,n) = CorMat(n,m) = (sumF1F2/nrefl)/(sqrtSigmaP[m]*sqrtSigmaP[n]);
            minOffDiag = std::min(minOffDiag,CorMat(m,n));
          }
        }
      }
      // Target for lowest off-diagonal CC allowed is relative error of CC/sigma(CC) >= targetZ_CC
      // sigma for complex correlation: (1-CC^2)/sqrt(2*nref)
      // Factor of 2 in sqrt (cf sigma for linear CC) comes from complex number having two parts
      // (twice as much information)
      // Solve for CC that gives desired relative error for the current value of nref
      double tzsqr(fn::pow2(targetZ_CC));
      targetOffDiag = std::sqrt(nref + tzsqr - std::sqrt(2*nref*tzsqr + fn::pow2(nref))) / targetZ_CC;
      midSsqr = 1.0/fn::pow2(BIN.MidRes(ibin));
      ibin--;
    }
    while ((minOffDiag < targetOffDiag) &&
           (ibin > loresbin));
    return { midSsqr,CorMat };
  }

  void
  ModelsFcalc::VanillaWriteMtz()
  {
    double wavelength = 0;
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,wavelength,MILLER.size(),get_history());
    for (int m = 1; m <= NMODELS; m++)
      mtzOut.addSet("Model" + ntos(m,NMODELS));
    //nref must be set before columns are added as this defines their size
    //set[0] contains the miller indices and the bins
    CMtz::MTZCOL* mtzB  = mtzOut.addCol("BIN",'I'); //set 0
    std::vector<CMtz::MTZCOL*> mtzF(NMODELS);
    std::vector<CMtz::MTZCOL*> mtzP(NMODELS);
    for (int m = 0; m < NMODELS; m++)
    {
      int s = m+1; //set
      std::string counter = ntos(s,NMODELS);
      mtzF[m]  = mtzOut.addCol("FC"+counter,'F',s);
      mtzP[m]  = mtzOut.addCol("PHIC"+counter,'P',s);
    }
    //set[1] to set[m+1] contains the reflection information per model
    int mtzr(0);
    phaser_assert(NMODELS);
    for (int r = 0; r < MILLER.size(); r++)
    {
      PHASER_ASSERT(r < BIN.RBIN.size());
      int ibin = BIN.RBIN[r];
      mtzOut.mtzH->ref[mtzr] = MILLER[r][0];
      mtzOut.mtzK->ref[mtzr] = MILLER[r][1];
      mtzOut.mtzL->ref[mtzr] = MILLER[r][2];
      mtzB->ref[mtzr] = ibin;
      for (int m = 0; m < NMODELS; m++)
      {
        mtzF[m]->ref[mtzr] = std::abs(FCALC(m,r));
        mtzP[m]->ref[mtzr] = scitbx::rad_as_deg(std::arg(FCALC(m,r)));
      }
      mtzr++;
    }
    create_directories_for_file();
    mtzOut.Put("Model from Phaser");
  }

  af_string
  ModelsFcalc::get_history()
  {
    af_string HISTLABINLINE;
    HISTLABINLINE.push_back("BINS " + BIN.unparse());
    HISTLABINLINE.push_back("HIRES " + dtos(FCALC_HIRES));
    HISTLABINLINE.push_back("TIMESTAMP " + TIMESTAMP);
    HISTLABINLINE.push_back("MODLID " + MODLID.string() + " " + MODLID.tag());
    HISTLABINLINE.push_back("NMODELS " + itos(NMODELS));
    return HISTLABINLINE;
  }

  void
  ModelsFcalc::SetHistory(af_string histlines)
  {
    for (auto line : histlines)
    {
      sv_string token;
      hoist::algorithm::split(token,line," ");
      if (!line.find("MODLID"))
      {
        phaser_assert(token.size() == 3);
        MODLID.parse_string(token[1],token[2]);
      }
      if (!line.find("HIRES"))
      {
        FCALC_HIRES = std::stod(token[1]);
      }
      if (!line.find("TIMESTAMP"))
      {
        phaser_assert(token.size() == 2);
        TIMESTAMP = token[1];
      }
      if (!line.find("BINS"))
      {
        phaser_assert(token.size() == 5);
        BIN.parse(std::stod(token[1]),
                  std::stod(token[2]),
                  std::stod(token[3]),
                  std::stod(token[4]));
      }
      if (!line.find("NMODELS"))
      {
        phaser_assert(token.size() == 2);
        NMODELS = std::atoi(token[1].c_str());
      }
    }
  }

  void
  ModelsFcalc::VanillaReadMtz()
  {
    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    //set[1] to set[m+1] contains the reflection information per model

    int nset = mtzIn.load_dataset("");
    NMODELS = nset-1; //hkl separate
    phaser_assert(NMODELS > 0);

    MILLER.resize(mtzIn.NREFL); //so that setup_data_present_init sets array sizes
    SetHistory(mtzIn.HISTORY);
    { //UNITCELL
      UC =  mtzIn.UC;
    } //UNITCELL
    { //DATA
      int ncols = 4+NMODELS*2; //H+K+L+B + NMODELS*(FC+PHIC)
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      lookup[0] = mtzIn.getCol("H");
      lookup[1] = mtzIn.getCol("K");
      lookup[2] = mtzIn.getCol("L");
      lookup[3] = mtzIn.getCol("BIN");
      int i(4);
      for (int m = 0; m < NMODELS; m++)
      {
        std::string counter = ntos(m+1,NMODELS);
        std::string FC = "FC"+counter;
        std::string PHIC = "PHIC"+counter;
        lookup[i++] = mtzIn.getCol(FC.c_str());
        lookup[i++] = mtzIn.getCol(PHIC.c_str());
      }
      //reset position in file
      mtzIn.resetRefl();
      MILLER.resize(mtzIn.NREFL);
      FCALC.resize(NMODELS,mtzIn.NREFL);
      BIN.RBIN.resize(mtzIn.NREFL);
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H(adata[0]);
        int K(adata[1]);
        int L(adata[2]);
        millnx miller(H,K,L);
        MILLER[mtzr] = miller;
        int ibin = adata[3];
        BIN.RBIN[mtzr] = ibin;
        int i(4);
        for (int m = 0; m < NMODELS; m++,i+=2)
        {
          FCALC(m,mtzr) = std::polar(static_cast<double>(adata[i]),
                                     static_cast<double>(scitbx::deg_as_rad(adata[i+1])));
        }
      }
      BIN.set_numinbin();
    } //DATA
  }

  af_cmplex
  ModelsFcalc::fcalc(int m)
  {
    af_cmplex data(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
       data[r] = FCALC(m,r);
    return data;
  }

  void
  ModelsFcalc::initialize_ecalc(int m, af_cmplex ecalc)
  {
    //this is not a general function, just a hack
    phaser_assert(m == 0); //for now, since only called from map
    phaser_assert(ecalc.size() == MILLER.size());
    FCALC.resize(1,MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
    {
      FCALC(m,r) = ecalc[r];
    }
  }

  std::pair<af_millnx,af_cmplex>
  ModelsFcalc::interpolate(
      int m,
      UnitCell newUC,
      double constant,
      dvect3 orthminbox,
      dvect3 orthmaxbox,
      std::pair<double,double> cell_scale,
      bool interpolate)
  {
    SpaceGroup SG("P 1");
    fft::real_to_complex_3d oldmap(SG.type(),UC.cctbxUC);
    oldmap.shannon(2);
    phaser_assert(FCALC_HIRES > 0);
    oldmap.calculate_gridding(FCALC_HIRES,cell_scale,interpolate);
    oldmap.allocate();

    //move the interesting bit to the corner
    //for some reason the cctbx::maptbx::interpolation does not work in the zone z < 0
    //the density is shifted towards the origin for -z, but fine for +z
    auto& oldUC = UC;
    auto ecalc = fcalc(m);
    dvect3 sorthminbox(0,0,0); //shifted minbox
    dvect3 sorthmaxbox = orthmaxbox-orthminbox; //shifted maxbox
    {
      dvect3 frac = oldUC.fractionalization_matrix()*orthminbox;
      cmplex two_pii(-cmplex(0,2.)*scitbx::constants::pi);
      for (int r = 0; r < MILLER.size(); r++)
      {
        const millnx miller = MILLER[r];
        cmplex PhaseShift = std::exp(two_pii*(frac*miller));
        ecalc[r] *= PhaseShift;
      }
    }

    // --- do the fft
    oldmap.backward(MILLER,ecalc);

    fft::real_to_complex_3d newmap(SG.type(),newUC.cctbxUC);
    newmap.shannon(2);
    newmap.calculate_gridding(FCALC_HIRES,cell_scale,interpolate);
    newmap.allocate();

    //make a map the right size for the fft by doing  blank miller to map run
    af::versa<std::complex<double>, af::c_grid<3> > map_grid(
        af::c_grid<3>(newmap.rfft.n_complex()));
    af::versa<double, af::flex_grid<> > new_real_map(
    //  &*map_grid.begin(),
        af::flex_grid<>(af::adapt((newmap.rfft.m_real())))
             .set_focus(af::adapt(newmap.rfft.n_real())));

    //convert mask points to frac coordinates
    {{
      double newnx = newmap.rfft.m_real()[0];
      double newny = newmap.rfft.m_real()[1];
      double newnz = newmap.rfft.m_real()[2];
      //we have to index on the new grid since these are the points for interpolation
      for (int newx = 0; newx < newnx; newx++)
      for (int newy = 0; newy < newny; newy++)
      for (int newz = 0; newz < newnz; newz++)
      {
        double newfracx = double(newx)/newnx;
        double newfracy = double(newy)/newny;
        double newfracz = double(newz)/newnz;
        double neworthx = newfracx*newUC.A();
        double neworthy = newfracy*newUC.B();
        double neworthz = newfracz*newUC.C();
        dvect3 neworth(neworthx,neworthy,neworthz);
        if ((neworth[0] >= sorthminbox[0]) and (neworth[0] < sorthmaxbox[0]) and
            (neworth[1] >= sorthminbox[1]) and (neworth[1] < sorthmaxbox[1]) and
            (neworth[2] >= sorthminbox[2]) and (neworth[2] < sorthmaxbox[2]))
        {
          dvect3 oldorth = neworth;
          double oldfracx = oldorth[0]/oldUC.A();
          double oldfracy = oldorth[1]/oldUC.B();
          double oldfracz = oldorth[2]/oldUC.C();
          dvect3 oldfrac(oldfracx,oldfracy,oldfracz);
          new_real_map(newx,newy,newz) = cctbx::maptbx::eight_point_interpolation(
                                             oldmap.real_map_unpadded.const_ref(),oldfrac);
        }
        else new_real_map(newx,newy,newz) = constant;
      }
    }}

    // --- do the fft
    af::versa<double, af::flex_grid<> > read_map(
        af::flex_grid<>(af::adapt(newmap.rfft.m_real()))
          .set_focus(af::adapt(newmap.rfft.n_real())));
    newmap.rfft.forward(new_real_map);
    {{
      double scale = 1.0/sqrt(newmap.rfft.n_real()[0]*newmap.rfft.n_real()[1]*newmap.rfft.n_real()[2]);
      for (int i  = 0; i < new_real_map.size(); i++)
        new_real_map[i]*=scale;
    }}

    // --- extract the complex structure factors
    af::const_ref<std::complex<double>, af::c_grid_padded<3> > complex_map(
        reinterpret_cast<std::complex<double>*>(&*new_real_map.begin()),
        af::c_grid_padded<3>(af::adapt(newmap.rfft.n_complex())));

    bool anomalous_flag(false);
    cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
    cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
    bool conjugate_flag(true);
    bool discard_indices_affected_by_aliasing = false;
    cctbx::maptbx::structure_factors::from_map<double> from_map(
        newUC.cctbxUC,
        SgInfoP1,
        anomalous_flag,
        FCALC_HIRES,
        complex_map,
        conjugate_flag,
        discard_indices_affected_by_aliasing);

    //PhaseShift back to original position
    auto from_map_data = from_map.data().deep_copy();
    auto from_map_mill = from_map.miller_indices().deep_copy();
    {
      dvect3 frac = -newUC.fractionalization_matrix()*orthminbox;
      cmplex two_pii(-cmplex(0,2.)*scitbx::constants::pi);
      for (int r = 0; r < from_map_mill.size(); r++)
      {
        const millnx miller = from_map_mill[r];
        cmplex PhaseShift = std::exp(two_pii*(frac*miller));
        from_map_data[r] *= PhaseShift;
      }
    }
    return  { from_map_mill, from_map_data };
  }

  af_double
  ModelsFcalc::density(std::pair<double,double> cell_scale,bool interpolate)
  {
    SpaceGroup SG("P 1");
    fft::real_to_complex_3d rfft(SG.type(),UC.cctbxUC);
    rfft.shannon(2);
    rfft.calculate_gridding(FCALC_HIRES,cell_scale,interpolate);
    rfft.allocate();
    //should be 5A map
    // --- do the fft
    rfft.backward(MILLER,fcalc(0));
    af_double mapdensity(rfft.real_map_unpadded.size());
    for (int i = 0; i < rfft.real_map_unpadded.size(); i++)
      mapdensity[i] = rfft.real_map_unpadded[i];
    return mapdensity;
  }

  std::pair<double,double>  ModelsFcalc::polar_deg(int m, int r)
  {
    auto   FC = FCALC(m,r);
    double F = std::abs(FC);
    double PHI = scitbx::rad_as_deg(std::arg(FC));
    while (PHI > 180) PHI -= 360;
    while (PHI <= -180) PHI += 360;
    return {F, PHI};
  }

}//phasertng
