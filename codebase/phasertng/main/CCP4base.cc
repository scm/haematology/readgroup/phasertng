//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <cstdarg>
#include <cctype>
#include <fstream>
#include <numeric>
#include <phasertng/main/constants.h>
#include <phasertng/main/CCP4base.h>
#include <phasertng/main/QuotedString.h>
#include <phasertng/enum/end_keys.h>

namespace phasertng {

  std::string
  CCP4base::skip_line(std::istringstream& input_stream)
  {
    //This routine returns either Token::END or Token::ENDLINE after skipping to the end
   // get_token(input_stream);
    if (curr_tok == Token::ENDLINE || curr_tok == Token::END || curr_tok == Token::COMMENT)
    {
      return "";
    }
    std::string skip;
    get_token(input_stream);
    bool additional(false);
    while (curr_tok != Token::ENDLINE && curr_tok != Token::END)
    {
      skip += string_value + " ";
      if (!additional)
      {
        ADDITIONAL_TOKENS.push_back("# ");
      }
      additional = true;
      ADDITIONAL_TOKENS.back() += string_value + " ";
      get_token(input_stream);
    }
    if (skip.size()) skip.pop_back(); //final space
    return skip;
  }

  Token::value
  CCP4base::get_token(std::istringstream& input_stream)
  {
    bool test_number = true;
    bool is_digits = true; //and therefore possibly identifier uuid
    bool is_special_char = false; //+ - . which could be a number but isn't
    bool is_int64_identifier = false;
    char ch;
    do { if (!input_stream.get(ch))
         {
           return curr_tok = Token::END;
         }
       } while ((isspace)(ch) && ch != '\n'); //skip space, tabs, feedforms


    //if line continuation, skip all spaces including \n
    if (ch == '&')
      do { if (!input_stream.get(ch))
           {
             return curr_tok = Token::END;
           }
         } while ((isspace)(ch));

    switch (ch)
    {
      case '#': case '!':
      case '@': //"include" file dealt with by preprocessor
        getline(input_stream,string_value,'\n');
        return curr_tok = Token::COMMENT;
        break;

      case '\n':
        return curr_tok = Token::ENDLINE;
        break;

      case '=':
        return curr_tok = Token::ASSIGN;
        break;

      case '+': case '-': case '.':
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9':
        string_value = ch;
        if (ch=='.' || ch=='+'|| ch=='-')
        {
          is_special_char = true; //could be flags for some things rather than numbers
          is_digits = false;
        }
        // Obsolete: allow digits and dots only for subsequent chars with
        // while(input_stream.get(ch) && (isdigit(ch) || ch=='.')) here
        while (input_stream.get(ch) && (isprint)(ch) && !(isspace)(ch) && ch != '=' && ch !='#')
        {
  //rest of std::string is all digits or . or e or E or + or -(exponent)
          if (!((isdigit)(ch) || ch=='.' || ch=='e' || ch=='E' || ch=='+'|| ch=='-'))
            test_number = false; //[A-D,F-Z] etc because we know it cannot be double or int
          if (!((isdigit)(ch)))
            is_digits = false; //can't be integer, int, llint
          string_value += ch;
        }
        input_stream.putback(ch); // oops - read one too far
        is_special_char = is_special_char && (string_value.size() == 1);
        is_int64_identifier = is_digits && (string_value.size() > max_len_uint32);
        if (is_int64_identifier) //short strings cannot be ullint
        { //number could come from python which has unlimited precision
          //so it could even be larger that ullint unsigned long long int
          //current default is, uuid type is not ullint, it is only size_t, so should not be called
          try {
            error_ullint = false;
            ullint_value = 999; //additional flag if fails, should always check for error_ullint
            ullint_value = std::stoull(string_value); //throws
          }
          catch (...)
          {
            error_ullint = true;
          }
          //number_value cannot be ullint and double(/int)
          //must separate the two cases with different variables
          //mantissa 2^53 is max double to int exact conversion for IEEEE
          //have to convert from string direct to ullint
          //can't check exact conversion because all tests (eg. to_string) end up making same approximation
          return curr_tok = Token::NUMBER; //at end, string_value is the number too
        }
        else if (is_digits) //short strings cannot be ullint
        {
          ullint_value = std::stoull(string_value); //should not throw because short and only digits
          number_value = std::stod(string_value); //should not throw because and only digits
          return curr_tok = Token::NUMBER; //at end, string_value is the number too
        }
        else if (test_number and !is_special_char)
        {
          //error_ullint = true;
          try {
            error_double = false;
            number_value = 999; //additional flag if fails, should always check for error_double
            number_value = std::stod(string_value); //throws
          }
          catch (...)
          {
            error_double = true;
          }
          return curr_tok = Token::NUMBER; //at end, string_value is the number too
        }
        else //this allows std::strings starting with any character except = or #
        {
          return curr_tok = Token::NAME;
        }
        break;

      default:
      {
        if (!(isprint)(ch)) //unicode! "ch" does not have to be printable
            storeWarning("Unrecognised (unicode) character");
            //storeError(err::SYNTAX,"Unrecognised (unicode) character");
        string_value = ch;
        //allow printable characters except spaces and ASSIGN
        while(input_stream.get(ch) && !(isspace)(ch) && ch != '=')
        {
          if (!(isprint)(ch)) //unicode! "ch" does not have to be printable
            storeWarning("Unrecognised (unicode) character");
            //storeError(err::SYNTAX,"Unrecognised (unicode) character");
          string_value += ch;
        }
        input_stream.putback(ch); // oops - read one too far
        return curr_tok = Token::NAME;
      }
    }

    storeError(err::SYNTAX,"Unrecognised (unicode) character");
    return curr_tok = Token::ENDLINE;
  }


  std::string
  CCP4base::Boolean(std::string raw)
  {
    for (int s = 0; s < raw.size(); s++)
      raw[s] = std::tolower(raw[s]);
    if (raw == "true")
      raw = "on";
    else if (raw == "false")
      raw = "off";
    else if (raw == "yes")
      raw = "on";
    else if (raw == "y")
      raw = "on";
    else if (raw == "1")
      raw = "on";
    else if (raw == "no")
      raw = "off";
    else if (raw == "n")
      raw = "off";
    else if (raw == "0")
      raw = "off";
    return raw;
  }

  bool
  CCP4base::get_boolean(std::istringstream& input_stream)
  {
    if (!tokenIsInList(input_stream,{Token::NAME,Token::NUMBER}))
    {
      storeError(err::PARSE,"Boolean not present or not valid");
    }
    string_value = Boolean(string_value); //for the compulsoryKey test
    if (string_value != "on" && string_value != "off") //so that error is thrown
    {
      storeError(err::PARSE,"Boolean flag not present");
    }
    return (string_value == "on");
  }

  char
  CCP4base::get_character(std::istringstream& input_stream)
  {
    //special code because the char can be '-' and look like a number, and is unquoted
    //also allow '*' for star type in seek
    //convert boolean/tribool to Y/N/?
    get_token(input_stream);
    std::string chstr = string_value;
    if (chstr == "None") chstr = "?"; //special case
    if (Boolean(chstr) == "on") chstr = "Y"; //special case
    if (Boolean(chstr) == "off") chstr = "N"; //special case
    if (chstr.size() > 1 or chstr.size() == 0)
       storeError(err::PARSE,"Character not present or not valid");
    if (chstr.size() > 0) return chstr[0];
    return ' ';
  }

  std::string
  CCP4base::get_string(std::istringstream& input_stream)
  {
    return getQuotedString(input_stream);
  }

  std::string
  CCP4base::get_mtzcol(std::istringstream& input_stream)
  {
    get_token(input_stream);
    if (!tokenIsAlreadyInList({Token::NAME,Token::NUMBER}))
    {
      storeError(err::PARSE,"Mtz column string not present or not valid");
    }
    return string_value;
  }

  std::string
  CCP4base::get_atom_selection(std::istringstream& input_stream)
  {
    return getQuotedString(input_stream);
  }

  std::string
  CCP4base::get_path(std::istringstream& input_stream)
  {
    return getQuotedString(input_stream);
  }

  std::string
  CCP4base::get_spacegroup(std::istringstream& input_stream)
  {
    return getQuotedString(input_stream);
  }

  std::string
  CCP4base::getQuotedString(std::istringstream& input_stream)
  {
    QuotedString Quoted(input_stream);
    string_value = Quoted.string_value;
    //order is important
    if (Quoted.None)
      return "";
    if (Quoted.error)
      storeError(err::SYNTAX,"Quoted string not found");
    if (Quoted.blank)
      return "";
    return Quoted.string_value;
  }

  std::string
  CCP4base::getAssignString(std::istringstream& input_stream)
  {
    get_token(input_stream);
    if (tokenIsAlready(Token::ASSIGN))
    {
      get_token(input_stream); //assignment token is optional
    }
    if (!tokenIsAlreadyInList({Token::NAME,Token::NUMBER}))
    {
      storeError(err::PARSE,"Assignment string not present or not valid");
    }
    return string_value;
  }

  int
  CCP4base::get_int_number(std::istringstream& input_stream,bool minset,int minval,bool maxset,int maxval)
  {
    double round = std::round(get_flt_number(input_stream,minset,minval,maxset,maxval));
    if (number_value != round)
    {
      storeError(err::PARSE,"number is not an integer");
    }
    return static_cast<int>(round);
  }

  std::pair<int,int>
  CCP4base::get_int_paired(std::istringstream& input_stream,bool minset,int minval,bool maxset,int maxval)
  {
    std::pair<double,double> flt_paired = get_flt_paired(input_stream,minset,minval,maxset,maxval);
    if (flt_paired.first != std::round(flt_paired.first) ||
        flt_paired.second != std::round(flt_paired.second))
    {
      storeError(err::PARSE,"number is not an integer");
    }
    return std::pair<int,int>(static_cast<int>(flt_paired.first),static_cast<int>(flt_paired.second));
  }

  std::pair<double,double>
  CCP4base::get_percent_paired(std::istringstream& input_stream)
  {
    return get_flt_paired(input_stream,true,0.,true,100.);
  }

  sv_int
  CCP4base::get_int_numbers(std::istringstream& input_stream,bool minset,int minval,bool maxset,int maxval)
  {
    sv_int numbers;
    sv_double flts = get_flt_numbers(input_stream,minset,minval,maxset,maxval);
    for (auto flt : flts)
    {
      if (flt != std::round(flt))
        storeError(err::PARSE,"number is not an integer (" + string_value + ")");
      numbers.push_back(static_cast<int>(flt));
    }
    return numbers;
  }

  uuid_t
  CCP4base::get_uuid_number(std::istringstream& input_stream,bool minset,uuid_t minval,bool maxset,uuid_t maxval)
  {
    get_token(input_stream);
    if (string_value == "None")
      ullint_value = 0;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_ullint)
      storeError(err::PARSE,error_message_ullint());
    else if (minset && ullint_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset && ullint_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    else if (ullint_value >= std::numeric_limits<uuid_t>::max())
      storeError(err::PARSE,"number greater than numeric limit of identifier type");
    return static_cast<uuid_t>(ullint_value);
    //size_t is not larger than unsigned long long
  }

  sv_uuid
  CCP4base::get_uuid_numbers(std::istringstream& input_stream,bool minset,uuid_t minval,bool maxset,uuid_t maxval)
  {
    //only handles number list that end in eol token
    sv_uuid numbers;
    while (tokenIs(input_stream,Token::NUMBER))
    {
      if (error_ullint)
        storeError(err::PARSE,error_message_ullint());
      else if (minset && ullint_value < minval)
        storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
      else if (maxset && ullint_value > maxval)
        storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
      else if (ullint_value >= std::numeric_limits<uuid_t>::max())
        storeError(err::PARSE,"number greater than numeric limit of identifier type");
      numbers.push_back(static_cast<uuid_t>(ullint_value));
    }
    return numbers;
  }

  double
  CCP4base::get_flt_number(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    if (!tokenIs(input_stream,Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
    else if (minset and number_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and number_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    return number_value;
  }

  std::pair<double,double>
  CCP4base::get_flt_paired(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    double a,b;
    if (!tokenIs(input_stream,Token::NUMBER))
    {
      storeError(err::PARSE,"First number not present or not valid (" + string_value + ")");
      return std::pair<double,double>(0,0);
    }
    else if (error_double)
    {
      storeError(err::PARSE,error_message_double());
      return std::pair<double,double>(0,0);
    }
    a = number_value;
    if (!tokenIs(input_stream,Token::NUMBER))
    {
      storeError(err::PARSE,"Second number not present or not valid (" + string_value + ")");
      return std::pair<double,double>(0,0);
    }
    else if (error_double)
    {
      storeError(err::PARSE,error_message_double());
      return std::pair<double,double>(0,0);
    }
    b = number_value;
    if (minset and (a < minval or b < minval))
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and (a > maxval or b > maxval))
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    return std::pair<double,double>(a,b);
  }

  std::complex<double>
  CCP4base::get_flt_complx(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    std::pair<double,double> flt_paired = get_flt_paired(input_stream,minset,minval,maxset,maxval);
    return { flt_paired.first, flt_paired.second };
  }

  sv_dvect3
  CCP4base::get_flt_vectors(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    std::vector<double> v = get_flt_numbers(input_stream,minset,minval,maxset,maxval);
    if (v.size()%3)
      storeError(err::PARSE,"vector array length not divisible by 3");
    int n(v.size()/3);
    sv_dvect3 v3;
    for (int i = 0; i < n; i++)
    {
      dvect3 tmp(v[i*3],v[i*3+1],v[i*3+2]);
      v3.push_back(tmp);
    }
    return v3;
  }

  double
  CCP4base::get_percent(std::istringstream& input_stream)
  {
    get_token(input_stream);
    if (tokenIsAlready(Token::NAME) and (string_value == "None" or string_value == "NONE"))
      return DEF_FLT_FLAG;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
  // else if (number_value > 0 && number_value < 1)
  //   storeWarning("Percent between zero and one, check input is not a fraction");
    else if (number_value < 0 || number_value > 100)
      storeError(err::PARSE,"Value is not in range 0-100%");
    return number_value;
  }

  scitbx::vec3<double>
  CCP4base::get_flt_vector(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    scitbx::vec3<double> temp(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG);
    get_token(input_stream);
    if (tokenIsAlready(Token::NAME) and (string_value == "None" or string_value == "NONE"))
      return temp;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
    else if (minset and number_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and number_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    else
    {
      temp[0] = number_value;
      temp[1] = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp[2] = get_flt_number(input_stream,minset,minval,maxset,maxval);
    }
    return temp;
  }

  scitbx::vec3<int>
  CCP4base::get_int_vector(std::istringstream& input_stream,bool minset,int minval,bool maxset,int maxval)
  {
    scitbx::vec3<int> temp(DEF_INT_FLAG,DEF_INT_FLAG,DEF_INT_FLAG);
    get_token(input_stream);
    if (tokenIsAlready(Token::NAME) and (string_value == "None" or string_value == "NONE"))
      return temp;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
    else if (minset and number_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and number_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    else
    {
      temp[0] = number_value;
      temp[1] = get_int_number(input_stream,minset,minval,maxset,maxval);
      temp[2] = get_int_number(input_stream,minset,minval,maxset,maxval);
    }
    return temp;
  }

  scitbx::af::double6
  CCP4base::get_flt_cell(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    scitbx::af::double6 temp(1,1,1,90,90,90);
    get_token(input_stream);
    if (tokenIsAlready(Token::NAME) and (string_value == "None" or string_value == "NONE"))
      return temp;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
    else if (minset and number_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and number_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    else
    {
      temp[0] = number_value;
      temp[1] = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp[2] = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp[3] = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp[4] = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp[5] = get_flt_number(input_stream,minset,minval,maxset,maxval);
    }
    return temp;
  }

  scitbx::mat3<double>
  CCP4base::get_flt_matrix(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    scitbx::mat3<double> temp(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
                              DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
                              DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG);
    get_token(input_stream);
    if (tokenIsAlready(Token::NAME) and (string_value == "None" or string_value == "NONE"))
      return temp;
    else if (!tokenIsAlready(Token::NUMBER))
      storeError(err::PARSE,"number not present or not valid (" + string_value + ")");
    else if (error_double)
      storeError(err::PARSE,error_message_double());
    else if (minset and number_value < minval)
      storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
    else if (maxset and number_value > maxval)
      storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
    else
    {
      temp(0,0) = number_value;
      temp(0,1) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(0,2) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(1,0) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(1,1) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(1,2) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(2,0) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(2,1) = get_flt_number(input_stream,minset,minval,maxset,maxval);
      temp(2,2) = get_flt_number(input_stream,minset,minval,maxset,maxval);
    }
    return temp;
  }

  sv_double
  CCP4base::get_flt_numbers(std::istringstream& input_stream,bool minset,double minval,bool maxset,double maxval)
  {
    sv_double numbers;
    for (;;)
    {
      //This next section of code replaces get_token in the special case of parsing
      //an unknown number of numbers into an array, followed by a new keyword
      //We need to peek into the stream and see if we are about to stop
      //entering numbers, in which case a whitespace has be be left on the stream
      //so that get_token has the whitespace as a separator
      bool next_token_will_be_a_number(true);
      bool next_token_could_be_none(false);
      char ch = ' ';
      for (;;)
      {
        char pk = input_stream.peek();
        //ch will be space, digit or other
        if (isspace(pk))
        {
          input_stream.get(ch);
        }
        else if (isdigit(pk) or pk=='+' or pk=='-' or pk=='.')
        {
          next_token_will_be_a_number = true;
          break;
        }
        else if (pk=='N')
        {
          next_token_could_be_none = true;
          break;
        }
        else
        {
          next_token_will_be_a_number = false;
          break;
        }
      } //skip space, tabs, feedforms
      //put the last ws back onto the stream so that the next get_token can see the ws
      input_stream.putback(ch);
      //if next is not a digit, don't even try to get the token
      if (next_token_could_be_none)
      {
        get_token(input_stream);
        if (string_value != "None" and string_value != "NONE")
        storeError(err::PARSE,"token not recognised in number list (" + string_value + ")");
        return numbers; //if None
      }
      if (!next_token_will_be_a_number) break;
      get_token(input_stream);
      if (!tokenIsAlready(Token::NUMBER)) //should NEVER be triggered, something odd happening in file
        storeError(err::PARSE,"number not recognised in number list (" + string_value + ")");
      else if (error_double)
        storeError(err::PARSE,error_message_double());
      else if (minset && number_value < minval)
        storeError(err::PARSE,"number is less than minimum (value_min=" + std::to_string(minval) + ")");
      else if (maxset && number_value > maxval)
        storeError(err::PARSE,"number is greater than maximum (value_max=" + std::to_string(maxval) + ")");
      numbers.push_back(number_value);
    }
    return numbers;
  }

  sv_string
  CCP4base::get_strings(std::istringstream& input_stream)
  {
    sv_string Strings;
    while (tokenIsInList(input_stream,{Token::NAME,Token::NUMBER}))
    {
      Strings.push_back(string_value);
    }
    return Strings;
  }

  bool
  CCP4base::keyIs(const char* keych)
  {
    std::string key(keych);
    if (curr_tok == Token::ENDLINE || curr_tok == Token::END)
      return false;
   // if (string_value[0] =='*') //erase the * from the front of choice keywords
   //   string_value.erase(0,1);
    if (string_value != key)
      return false;
    variable.push_back(key);
    return true;
  }

  bool
  CCP4base::keyIsAdd(std::string key,bool addvar)
  {
    if (curr_tok == Token::ENDLINE || curr_tok == Token::END)
      return false;
   // if (string_value[0] =='*') //erase the * from the front of choice keywords
   //   string_value.erase(0,1);
    if (string_value != key)
      return false;
    if (addvar) variable.push_back(key);
    return true;
  }

  bool
  CCP4base::keyIsOptional(bool addvar)
  {
    if (curr_tok == Token::ENDLINE || curr_tok == Token::END)
      return false;
    if (!string_value.size())
      return false;
/*
    for (int s = 0; s < string_value.size(); s++)
    {
      ////change string value to the template case!! which is always lowercase
      string_value[s] = static_cast<char>(std::tolower(string_value[s]));
    }
*/
    if (addvar) variable.push_back(string_value);
    return true;
  }

  bool
  CCP4base::keyIsEnd()
  {
    bool end_key(false);
    for (int i = 0; i < End::KeysCount; i++)
    {
      std::string s(string_value); //stoupper, without include of jiffy.h
      std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return toupper(c); });
      if (s == End::Keys2String(static_cast<End::Keys>(i)))
      {
        end_key = true;
      }
    }
    if (end_key or curr_tok == Token::END)
      return true;
    return false;
  }

  bool
  CCP4base::keyIsEol()
  {
    return (curr_tok == Token::ENDLINE);
  }

  bool
  CCP4base::tokenIsInList(std::istringstream& input_stream,std::vector<Token::value> keys)
  {
    get_token(input_stream);
    bool tokenFound(false);
    for (int i = 0; i < keys.size(); i++)
      if (curr_tok == keys[i])
        tokenFound = true;
    return tokenFound;
  }

  bool
  CCP4base::tokenIs(std::istringstream& input_stream,Token::value key)
  {
    get_token(input_stream);
    bool tokenFound(false);
    if (curr_tok == key)
      tokenFound = true;
    return tokenFound;
  }

  bool
  CCP4base::tokenIsAlreadyInList(std::vector<Token::value> keys)
  {
    bool tokenFound(false);
    for (int i = 0; i < keys.size(); i++)
      if (curr_tok == keys[i])
        tokenFound = true;
    return tokenFound;
  }

  bool
  CCP4base::tokenIsAlready(Token::value key)
  {
    bool tokenFound(false);
    if (curr_tok == key)
      tokenFound = true;
    return tokenFound;
  }

  bool
  CCP4base::compulsoryKey(std::istringstream& input_stream,std::unordered_set<const char*> keyset)
  {
    sv_string keys;
    for (auto key : keyset) keys.push_back(std::string(key));
    std::sort(keys.begin(),keys.end());
    get_token(input_stream);
    bool keyFound(false);
    bool keyMissing(false);
    if (tokenIsAlready(Token::NAME))
    {
      for (int i = 0; i < keys.size(); i++)
        if (keyIsAdd(keys[i],false)) //checked later in parse
          keyFound = true;
    }
    else
    {
      keyFound = false;
      keyMissing = true;
    }

    if (!(keyFound))
    {
      std::string useMessage = keyMissing ?
           " ( Missing (compulsory) ":
           "\"" + string_value + "\" ( Use (compulsory) ";
      if (keys.size() > 1)
      {
        for (int i = 0; i < keys.size()-1; i++)
          useMessage += keys[i] + " ";
        useMessage += "or " + keys.back() + " )";
      }
      else
      {
        useMessage += keys.back() + " )";
      }
      storeError(err::PARSE,useMessage);
    }
    return keyFound;
  }

  //plural to differentiate call with vector (construct in place) and with set (reference)
  bool
  CCP4base::compulsoryKeySet(std::istringstream& input_stream,const std::unordered_set<std::string>& keys)
  {
    get_token(input_stream);
    if (keys.count(string_value))
      return true;
    if (tokenIsAlready(Token::NAME))
    {
      std::string useMessage = "\"" + string_value + "\" ( Use (compulsory) ";
      for (auto& item : keys)
        useMessage += item + " ";
      useMessage += ")";
      storeError(err::PARSE,useMessage);
    }
    else
    {
      std::string useMessage = " ( Missing (compulsory) ";
      for (auto& item : keys)
        useMessage += item + " ";
      useMessage += ")";
      storeError(err::PARSE,useMessage);
    }
    return false;
  }

  bool
  CCP4base::compulsoryKeyStr(std::istringstream& input_stream,const std::string& key)
  {
    get_token(input_stream);
    if (string_value == key) //don't test for token type, just check match
    {
      return true;
    }
    else if (tokenIsAlready(Token::NAME))
    {
      std::string useMessage = "\"" + string_value + "\" ( Use (compulsory) " + key + " )";
      storeError(err::PARSE,useMessage);
    }
    else
    {
      std::string useMessage = " ( Missing (compulsory) " + key + " )";
      storeError(err::PARSE,useMessage);
    }
    return false;
  }

  bool
  CCP4base::optionalKey(std::istringstream& input_stream,std::unordered_set<const char*> keyset)
  {
    sv_string keys;
    for (auto key : keyset) keys.push_back(std::string(key));
    std::sort(keys.begin(),keys.end());
    get_token(input_stream);
    bool keyFound(false);
    if (tokenIsAlready(Token::NAME))
    {
      for (int i = 0; i < keys.size(); i++)
        if (keyIsAdd(keys[i],false))
          keyFound = true;
    }

    //we expect Token::ENDLINE or Token::END if none of the keywords are found
    if ((tokenIsAlready(Token::NAME) && !keyFound) || !tokenIsAlreadyInList({Token::NAME,Token::ENDLINE,Token::END}))
    {
      std::string useMessage = "\"" + string_value + "\" ( Use (optional) ";
      if (keys.size() > 1)
      {
        for (int i = 0; i < keys.size() - 1; i++)
          useMessage += keys[i] + " ";
        useMessage += "or " + keys.back() + " )";
      }
      else useMessage += keys.back() + " )";
      storeError(err::PARSE,useMessage);
    }
    return keyFound;
  }

  //plural to differentiate call with vector (construct in place) and with set (reference)
  bool
  CCP4base::optionalKeySet(std::istringstream& input_stream,const std::unordered_set<std::string>& keys)
  {
    get_token(input_stream);
    bool keyFound(false);
    if (tokenIsAlready(Token::NAME))
    {
      for (auto& item : keys)
        if (keyIsAdd(item,false))
          keyFound = true;
    }

    //we expect Token::ENDLINE or Token::END if none of the keywords are found
    if ((tokenIsAlready(Token::NAME) && !keyFound) || !tokenIsAlreadyInList({Token::NAME,Token::ENDLINE,Token::END}))
    {
      std::string useMessage = "\"" + string_value + "\" ( Use (optional) ";
      for (auto& item : keys)
        useMessage += item + " ";
      useMessage += ")";
      storeError(err::PARSE,useMessage);
    }
    return keyFound;
  }

  void
  CCP4base::linkKey(std::string linkroot,std::string linkstem)
  {
    if (linkages.find(linkroot) == linkages.end())
      linkages[linkroot] = {};
    sv_string& tmp = linkages.at(linkroot);
    if (std::find(tmp.begin(),tmp.end(),linkstem) == tmp.end())
      linkages[linkroot].push_back(linkstem);
  }

  void
  CCP4base::storeError(err::code Code,std::string Message)
  {
    //cannot specify operator== with base class exception
    std::string this_keyword;
    for (auto item : variable) this_keyword += item + " ";
    std::string KeyMessage = " [ " + this_keyword + ": " + Message + " ]";
    if (std::find(ERRORS[Code].begin(),ERRORS[Code].end(),KeyMessage) == ERRORS[Code].end())
      ERRORS[Code].push_back(KeyMessage);
  }

  void
  CCP4base::storeWarning(std::string Message)
  {
    //cannot specify operator== with base class exception
    std::string this_keyword;
    for (auto item : variable) this_keyword += item + " ";
    std::string KeyMessage = " [ " + this_keyword + ": " + Message + " ]";
    WARNINGS.push_back(KeyMessage);
  }

  bool
  CCP4base::has_errors() const
  { return ERRORS.size(); }

  bool
  CCP4base::has_warnings() const
  { return WARNINGS.size(); }

  std::string
  CCP4base::error_message_ullint()
  {
    std::stringstream s;
    s << std::numeric_limits<ullint>::max();
    return std::string("Number (" + string_value + ") out of integer range (0-" + s.str() + ")");
  }

  std::string
  CCP4base::error_message_double()
  {
    std::stringstream s;
    s << std::numeric_limits<double>::lowest() << "/" << std::numeric_limits<double>::max();
    return std::string("Number (" + string_value + ") out of double range (" + s.str() + ")");
  }


  std::pair<std::pair<uuid_t,std::string>,std::pair<uuid_t,std::string>>
  CCP4base::get_dogtag(std::istringstream& input_stream)
  {
    std::string identifier = "id"; std::string tag ="tag";
    compulsoryKeyStr(input_stream,identifier);
    uuid_t id_number1 = get_uuid_number(input_stream);
    uuid_t id_number2 = get_uuid_number(input_stream);
    compulsoryKeyStr(input_stream,tag);
    tokenIsInList(input_stream,{Token::NAME,Token::NUMBER});
    std::string id_tag1 = string_value;
    tokenIsInList(input_stream,{Token::NAME,Token::NUMBER});
    std::string id_tag2 = string_value;
    return { { id_number1,id_tag1}, {id_number2,id_tag2} };
  }

  std::pair<uuid_t,std::string>
  CCP4base::get_identifier(std::istringstream& input_stream)
  {
    std::string identifier = "id"; std::string tag ="tag";
    compulsoryKeyStr(input_stream,identifier);
    uuid_t id_number = get_uuid_number(input_stream);
    compulsoryKeyStr(input_stream,tag);
    std::string id_tag = get_string(input_stream);
    return { id_number,id_tag };
  }

}//phasertng
