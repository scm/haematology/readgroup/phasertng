#ifndef __phasertng__pLogNormal__function__
#define __phasertng__pLogNormal__function__
#include <cassert>

// Common probability distribution

namespace phasertng {

inline double pLogNormal(double& mu, double& sigma, double& x)
{
  // Returns probability density for a variable x where the log of x
  // has a normal distribution with mean mu and standard deviation sigma
  const double ZERO(0.);
  assert(x >= ZERO);
  assert(sigma > ZERO);
  if (x == ZERO) return ZERO;
  double logxmu = std::log(x)-mu;
  double logxmu2 = logxmu*logxmu;
  return std::exp(-logxmu2/(2*sigma*sigma)) /
    (x*sigma*std::sqrt(scitbx::constants::two_pi));
}

}
#endif
