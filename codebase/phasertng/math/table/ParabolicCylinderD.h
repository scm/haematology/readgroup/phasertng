#ifndef __phasertng_paraboliccylinderd_functions__
#define __phasertng_paraboliccylinderd_functions__
#include <scitbx/constants.h>
#include <phasertng/math/table/tgamma_by_quarters.h>

namespace phasertng {
namespace table {

class ParabolicCylinderD
{
   public:
      static ParabolicCylinderD& getInstance() { return inst_; }
      static ParabolicCylinderD inst_;
   table::tgamma_by_quarters tgamma_;
   ParabolicCylinderD()
   {
/* Compute parabolic cylinder function Dv(x)
   Equivalent to Mathematica ParabolicCylinderD[va,x]

   Derived from routines in program mpbdv.for by Shanjie Zhang and Jianming Jin
   distributed with their book "Computation of Special Functions",
   Copyright 1996 by John Wiley & Sons, Inc.
   Permission has been granted for purchasers of the book to incorporate these
   programs as long as the copyright is acknowledged.

   va = order of the parabolic cylinder function Dv
   x  = argument
*/
   // arguments for tgamma are restricted to multiples of 1/4 in the table lookup
   tgamma_ = table::tgamma_by_quarters();
   }

  public:
  double
  get(double va, double x) const
  {
    double ax(std::abs(x));
    double value = (ax <= 5.8) ? dvsa(va,x) : dvla(va,x);
    return value;
  }

  private:
  double dvsa(double va, double x) const;
  double vvla(double va,double x) const;
  double dvla(double va,double x) const;

};

}}
#endif
