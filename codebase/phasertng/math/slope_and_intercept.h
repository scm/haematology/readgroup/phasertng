#ifndef __phasertng_slope_and_intercept_function__
#define __phasertng_slope_and_intercept_function__
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>

using namespace scitbx;

namespace phasertng {

typedef scitbx::af::shared<double> af_double;
typedef af::versa<double, af::c_grid<3> >   versa_grid_double;

  double
  correlation_coefficient(versa_grid_double,versa_grid_double);

  std::pair<double,double>
  slope_and_intercept(af_double,af_double,af_double=af_double());

  std::pair<double,double>
  wilson_slope_and_intercept(af_double,af_double,af_double=af_double());

}
#endif
