//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Trace_class____
#define __phasertng_Trace_class____
#include <phasertng/io/entry/Monostructure.h>
#include <phasertng/io/PdbHandler.h>
#include <set>

namespace phasertng {

//Trace are a set of points masquerading as atoms in a pdb file for packing
  class Trace : public Monostructure
  {
    //trace does not care about REFLID
    public: //members
      double SAMPLING_DISTANCE = 0; //flag for not set
      sv_dvect3 POINT_GROUP_EULER = sv_dvect3(0);
      dvect3 CENTRE = dvect3(0,0,0);
      double MAX_RADIUS = 0;

    public: //constructors
      Trace() : Monostructure() {}
      Trace(af_string pdblines) : Monostructure(pdblines) { }
      bool in_memory() { return PDB.size(); }
      void read_from_disk() { ReadPdb(); }
      void write_to_disk() { WritePdb(); }

    public:
      bool is_atom() const;
      void trim_asa();
      void build_hexgrid_box(double,dvect3,dvect3);
      void select_overlapping(const Trace&,const sv_double&);
      int  trim_surface(double=0); //distance in
      void set_valid_element(bool);
      void trim_to_calphas();
      void trim_incomplete_sidechains();
      void renumber();
      int  number_of_calphas() const;
      af_string find_and_move_centre(UnitCell,sv_dvect3);

    public:
      void       ReadPdb();
      void       WritePdb();
      af_string  GetPdb();
      void       GetPdbHandler(PdbHandler&);
      void       SetPdb(af_string);
  };

}
#endif
