//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Header_class__
#define __phasertng_Header_class__
#include <phasertng/main/constants.h>
#include <phasertng/enum/labin_col.h>
#include <phasertng/enum/friedel_type.h>
#include <phasertng/enum/reflprep_flag.h>
#include <phasertng/type/mtzcol.h>
#include <phasertng/main/Error.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/main/Identifier.h>
#include <boost/logic/tribool.hpp>
#include <map>
#include <set>

namespace phasertng {

class Header
{
  //remark string stored to ensure parse if io flags are the same
  public:
  std::string remark_identifier = constants::remark_tag + constants::remark_identifier + " ";
  std::string remark_preparation = constants::remark_tag + " PREPARATION ";
  std::string remark_data = constants::remark_tag + " DATA ";
  std::string remark_labin = constants::remark_tag + " LABIN ";
  std::string remark_wilson = constants::remark_tag + " WILSON ";
  std::string remark_tncs = constants::remark_tag + " TNCS ";
  std::string remark_anisotropy = constants::remark_tag + " A "; //really short to fit 9x+6.4f
  std::string remark_twinned = constants::remark_tag + " TWINNED ";
  std::string remark_asu = constants::remark_tag + " ASU ";
  std::string remark_oversampling = constants::remark_tag + " OSAMP ";
  std::string remark_isg = constants::remark_tag + " ISG ";
  std::string map_origin = "MAPORI";

  boost::logic::tribool token_to_tribool(std::string) const;
  std::string tribool_to_token(boost::logic::tribool) const;
  bool token_to_bool(std::string) const;
  std::string bool_to_token(bool) const;


  public:
    Header() { init_miller(); }
    void init_miller()
    {
      //miller types
      MTZCOLTYPE[labin::H] = 'H';
      MTZCOLTYPE[labin::K] = 'H';
      MTZCOLTYPE[labin::L] = 'H';
      //miller names
      MTZCOLNAME[labin::H] = "H";
      MTZCOLNAME[labin::K] = "K";
      MTZCOLNAME[labin::L] = "L";
    }

  protected:
    double DEF_ONE = 1;
    bool   DEF_TRUE = true;
    int    default_int = 1;
    bool   default_bln = false;
    double default_flt = 1.0;
    void CreateLabin(type::mtzcol);

  public: //these are stored for convenience, derived from the MILLER array
    af::double2           RESOLUTION = {0,0}; //getter and setter define min and max
    int                   NREFL = 0; //stored separately from MILLER

  public:
    SpaceGroup            iSG = SpaceGroup("P 1"); //input spacegroup, recorded for later analysis (move)
    SpaceGroup            SG = SpaceGroup("P 1");
    UnitCell              UC;
    double                WAVELENGTH = 0;
    double                OVERSAMPLING = 1;
    dvect3                MAPORI = {0,0,0};

  public:
    std::string get_hall() const { return SG.HALL; }
    af::double6 get_cell() const { return UC.get_cell(); }
    double      get_wave() const { return WAVELENGTH; }
    double      get_oversampling() const { return OVERSAMPLING; }
    // dvect3      get_offset() const { return MAPORI; }
    double      get_z() const { return Z; }
    double      get_totalscat() const { return TOTAL_SCATTERING; }
    double      get_ztotalscat() const { return Z*TOTAL_SCATTERING; }
    double      fracscat(double v) const { return v/get_ztotalscat(); }
    void set_hall(std::string v) { SG = SpaceGroup(v); }
    void set_cell(af::double6 v) { UC = UnitCell(v); }
    void set_wave(double v)      { WAVELENGTH = v; }
    void set_oversampling(double v) { OVERSAMPLING = v; }
    // void set_offset(dvect3 v)    { MAPORI = v; }
    const double oversampling_correction() const { return 1./OVERSAMPLING; }

  public: //parameters get_history/SetHistory
    std::map<int,char>        MTZCOLTYPE; //int=labin::col
    std::map<int,std::string> MTZCOLNAME; //int=labin::col
    std::set<labin::col>      LABIN; //contains all the columns currently with data
    Identifier            REFLID; //data preparation identifier
    std::set<reflprep::flag> REFLPREP; //data preparation history
    boost::logic::tribool MAP = boost::logic::indeterminate;
    boost::logic::tribool EVALUES = boost::logic::indeterminate;
    boost::logic::tribool TNCS_PRESENT = boost::logic::indeterminate;
    boost::logic::tribool TWINNED = boost::logic::indeterminate;
    boost::logic::tribool ANOMALOUS = boost::logic::indeterminate;
    double                SHARPENING = 0;
    double                WILSON_K_F = 0; //flag for set, start with impossible value
    double                WILSON_B_F = 0;
    int                   TNCS_ORDER = 1;
    dvect3                TNCS_ANGLE = dvect3(0,0,0);
    bool                  ANISO_PRESENT = false; //cosines, not magnitude
    dmat33                ANISO_DIRECTION_COSINES = dmat33(0,0,0,0,0,0,0,0,0);
    dvect3                TNCS_VECTOR = dvect3(0,0,0);
    double                TNCS_RMS = 0;
    double                TNCS_FS = 1;
    double                TNCS_GFNRAD = 0;
    int                   TOTAL_SCATTERING = 0;
    double                Z = 0;
    boost::logic::tribool FRENCH_WILSON = false;
    boost::logic::tribool INTENSITIES = false;
    bool                  WILSON_SCALED = false;

  public:
    void clear();
    void SetHistory(af_string);
    af_string get_history() const;
    type::mtzcol get_mtzcol(labin::col) const;
    const bool prepared(reflprep::flag) const;
};

} //phasertng
#endif
