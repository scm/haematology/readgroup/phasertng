//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_paired_class__
#define __phasertng_type_flt_paired_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class flt_paired : public optional
{
  std::pair<double,double> dephalt = {0,0};
  std::pair<double,double> uvalue = {0,0};
  bool    pminset = false;
  bool    pmaxset = false;
  double  pminval = 0;
  double  pmaxval = 0;

  public:
  const bool&    minset() const { return pminset; }
  const double&  minval() const { return pminval; }
  const bool&    maxset() const { return pmaxset; }
  const double&  maxval() const { return pmaxval; }

  flt_paired() : optional() {}
  flt_paired(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const std::pair<double,double> dephalt_ = { DEF_FLT_FLAG, DEF_FLT_FLAG }
  ) ;
  void set_value(std::pair<double,double> other); //allow in place contructor {x,y}
  void set_value(const sv_string& vcards,std::pair<double,double> other);
  bool is_default() const;
  void set_default();
  std::pair<double,double> value_or_default() const;
  double value_or_default_min() const;
  double value_or_default_max() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::flt_paired& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
