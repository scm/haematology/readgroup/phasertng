#ifndef __phasertng_sad_stats_enum__
#define __phasertng_sad_stats_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace sad {
  enum  stats {
              CENTRIC,
              ACENTRIC,
              SINGLE,
              ALL
           };
/*
  MakeEnum ( stats,
              (CENTRIC)
              (ACENTRIC)
              (SINGLE)
              (ALL)
           );
*/
}}
#endif
