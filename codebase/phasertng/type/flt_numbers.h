//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_numbers_class__
#define __phasertng_type_flt_numbers_class__
#include <phasertng/type/optional.h>
#include <scitbx/sym_mat3.h>

namespace phasertng {
namespace type {

typedef std::vector<double> sv_double;
typedef scitbx::sym_mat3<double> dmat6;

class flt_numbers : public optional
{
  private:
    sv_double dephalt;
    sv_double uvalue;
    bool    pminset = false;
    bool    pmaxset = false;
    double  pminval = 0;
    double  pmaxval = 0;

  public:
    const bool&    minset() const { return pminset; }
    const double&  minval() const { return pminval; }
    const bool&    maxset() const { return pmaxset; }
    const double&  maxval() const { return pmaxval; }

  flt_numbers() : optional() {}
  flt_numbers(
      const std::string parameter,
      const bool pminset_,const double pminval_,
      const bool pmaxset_,const double pmaxval_,
      const sv_double dephalt_ = {}
  );

  void set_value(sv_double other);
  void set_value(dmat6 other);
  int size() const;
  void clear();
  void push_back(const double& other);
  void pop_back();
  void set_value(const sv_string& vcards,const sv_double& other);
  bool is_default() const;
  void set_default();
  sv_double value_or_default() const;
  sv_double value_or_default_unique() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::flt_numbers& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
