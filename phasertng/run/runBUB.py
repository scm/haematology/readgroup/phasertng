from iotbx.file_reader import any_file
from phasertng.scripts.jiffy import *
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
from phasertng.phil.mode_generator import isNone
import argparse
import iotbx,mmtbx
import iotbx.pdb
import itertools, os, sys
import mmtbx.validation.sequence
import os.path
import time
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))

def edit_and_log_unique_searches(basetng, unique_searches):
#{
      basetng.logChevron(enum_out_stream.summary,"Unique Searches")
      if unique_searches is None:
        basetng.logTab(enum_out_stream.summary,"No unique searches")
        return [[]]
      basetng.logTab(enum_out_stream.summary,"Number of searches = " + str(len(unique_searches)))
      if len(unique_searches) == 0:
        basetng.logTab(enum_out_stream.summary,"No search combinations overlap sequence with total required sequence coverage")
      else:
     #{
        basename_dict = {}
        for i,s in enumerate(unique_searches):
          for ifile in s:
            basename = os.path.basename(ifile)
            if basename in basename_dict:
              basename_dict[basename].add(ifile)
            else: #create set
              basename_dict[basename] = {ifile}
        duplicates = {basename: paths for basename, paths in basename_dict.items() if len(paths) > 1}
        if duplicates:
          raise ValueError(enum_err_code.input,"Files with same basename in different directories.\nTag identifiers derived from basename will not be unique.\nRename files to make basenames unique.")

        for i,s in enumerate(unique_searches):
          basetng.logTab(enum_out_stream.summary,"#" + str(i+1))# + " " + str(s))
          for tag in s:
          # tag = os.path.basename(tag)
          # tag = os.path.splitext(tag)[0]
            basetng.logTab(enum_out_stream.summary,"--" + str(tag))

        tmp = list(unique_searches)
        for i,search in enumerate(unique_searches):
          for j,tag in enumerate(search):
          # tag = os.path.basename(tag)
          # tag = os.path.splitext(tag)[0]
            tmp[i][j] = tag
        unique_searches = tmp

      #don't expand unique searches since expansion will be done in ellg
        basetng.logUnderLine(enum_out_stream.concise,"Unique Search Supersets")
        from collections import Counter
        def is_second_list_in_first(first_list, second_list):
             # get counts of two lists
            c1 = Counter(first_list)
            c2 = Counter(second_list)
            return all(c2[key] <= c1[key] for key in c2)

        l2 = copy.deepcopy(unique_searches[:])
        for m in unique_searches:
          for n in unique_searches:
            if is_second_list_in_first(n,m) and m != n:
              l2.remove(m)
              break
        unique_searches = l2
        for i,s in enumerate(unique_searches):
          basetng.logTab(enum_out_stream.concise,"#" + str(i+1))# + " " + str(s))
          for ss in s:
            basetng.logTab(enum_out_stream.concise,"--" + str(ss))
     #}
      return unique_searches
#}

class sequence_analysis:
#{{{
  def get_seqobj_asu(self,seqobj_bu,io_z):
        seqobj_asu = [] #of iotbx.bioinformatics.sequence
        unique_seq = set()
        for seq_in in seqobj_bu:
          for z in range(int(io_z)): #matthews value, expand the sequence by number in asymmetric unit
            seqobj_asu.append(seq_in) #this is the object that will be used below
            unique_seq.add(seq_in)
        return seqobj_asu,len(unique_seq),len(seqobj_asu)

  def print_query_sequence(self,
          basetng,
          io_seqin,
          ):
    where = enum_out_stream.summary
    basetng.logChevron(where,"Biological Unit")
    if (io_seqin is None):
      basetng.logTab(where,"No Sequence File")
    else:
      basetng.logTab(where,"Sequence File: " + str(io_seqin))
      seqobj_bu = any_file(io_seqin)
      if not seqobj_bu.file_type == "seq":
        raise ValueError(enum_err_code.fileopen,str(io_seqin))
      basetng.logTab(where,"Full sequence:")
      for i,seq in enumerate(seqobj_bu.file_content):
        if (len(seq.name) == 0):
          seq.name =  "Seq #" + str(i+1)
        basetng.logTab(where,">"+seq.name)
        lines = format_sequence(seq.sequence)
        for line in lines:
          basetng.logTab(where,line)
        basetng.logTab(where,"Length = " + str(len(seq.sequence)))

  def run_sequence_analysis(self,
          basetng,
          found_pdb,
          io_seqin,
          io_bu_multiplicity,
          io_identity,
          io_coverage,
          io_level,
          io_z, # 1 constant
          ):
   #{{{
    io_level = io_level.strip()
    verbose_flag = (io_level == "verbose" or io_level == "testing")
    testing_flag = (io_level == "testing")
    verbose = enum_out_stream.logfile if io_level == "verbose" else enum_out_stream.verbose
    basetng.logUnderLine(enum_out_stream.logfile,"Sequence Analysis")
  # if found_pdb is None:
  #   raise ValueError(enum_err_code.input,"No files for sequence analysis")

    alignment_params = setup_alignment_params()

    basetng.logChevron(verbose,"Query")
    basetng.logTab(verbose,"Biological Unit Sequence File: " + str(io_seqin))
    basetng.logTab(verbose,"Biological Unit Multiplicity: " + str(io_bu_multiplicity))
    seqobj_bu = any_file(io_seqin)
    # type(seqobj_bu.file_content)) is a list, of sequence objects
    seqobj_bu_file_content = seqobj_bu.file_content*io_bu_multiplicity

    if not seqobj_bu.file_type == "seq":
      raise ValueError(enum_err_code.fileopen,str(io_seqin))
      #flag for there was not sequence
    #at the end close file log_file.close()
    basetng.logTab(verbose,"Full sequence:")
    for i,seq in enumerate(seqobj_bu_file_content):
      if (len(seq.name) == 0):
        seq.name =  "Seq #" + str(i+1)
      basetng.logTab(verbose,">"+seq.name)
      lines = format_sequence(seq.sequence)
      for line in lines:
        basetng.logTab(verbose,line)
      basetng.logTab(verbose,"Length = " + str(len(seq.sequence)))
      basetng.logTab(verbose,"Multiplicity = " + str(io_bu_multiplicity))

    failed_pdbf = [] #for later
    #found_pdb modified by acceptable sequence present
    pdb_file_input,found_pdb = get_pdb_file_input(
                                     phaser_out=basetng,
                                     found_pdb=found_pdb)
    npdb = int(len(pdb_file_input))
    basetng.logTab(enum_out_stream.logfile,"Number of structures = " + str(npdb))
    for pdbf,pdbobj,unknown in pdb_file_input:
      if len(unknown):
        basetng.logTab(enum_out_stream.logfile,"Not protein or na: " + str(unknown))
    if npdb == 0:
        message = "No structures"
        basetng.logTab(enum_out_stream.summary,message)
       #raise ValueError(enum_err_code.fatal,message)
        return seqobj_bu.file_object, []

 #  basetng.logBlank(enum_out_stream.logfile)
 #  countp = 1
 #  for ifile,pdbobj,unknown in pdb_file_input:
 #    basetng.logChevron(enum_out_stream.logfile,"Model #: " + str(countp))
 #    countp = countp + 1
 #    basetng.logTab(enum_out_stream.logfile,"Model File: " + str(ifile))
 #    model = pdbobj.models()[0]
 #    for chain in model.chains():
 #       basetng.logTab(enum_out_stream.logfile,"Chain: \'" + str(chain.id) + "\'" + " residues=" + str(len(chain.as_padded_sequence())))
 #       lines = ''.join(chain.as_sequence())
 #       lines = format_sequence(lines)
 #       for line in lines:
 #         basetng.logTab(enum_out_stream.logfile,line)

  # basetng.logTab(enum_out_stream.logfile,"Number of copies in biological unit : " + str(io_z))

    assert(io_z == 1)
    seqobj_bu,nunique,nseq_bu = self.get_seqobj_asu(seqobj_bu_file_content,io_z)

    basetng.logBlank(enum_out_stream.logfile)
    basetng.logTab(enum_out_stream.logfile,"Number of sequences: " + str(nseq_bu))
    basetng.logTab(enum_out_stream.logfile,"Number of unique sequences: " + str(nunique))
    if nseq_bu == 0:
      basetng.logTab(enum_out_stream.summary,"No sequences")
      return seqobj_bu, found_pdb
    if testing_flag:
        basetng.logTab(verbose,"Full sequence:")
        for seq in seqobj_bu:
          basetng.logTab(verbose,">"+seq.name)
          lines = format_sequence(seq.sequence)
          for line in lines:
            basetng.logTab(verbose,line)

    io_identity = float(io_identity)
    io_coverage = float(io_coverage)
    #AJM TODO
    # this works best when the coverage of each chain is 100%
    # this could be done when the model is always 100% of the sequence but the occupancy is set to
    #    zero for excluded atoms
    # then the overlap section below would handle the cases of merging models
    basetng.logChevron(enum_out_stream.logfile,"Model Clash")
    basetng.logTab(enum_out_stream.logfile,"Check that model is within the asu")
    basetng.logTab(enum_out_stream.logfile,"Reorder chains to sequence order")
    basetng.logTab(enum_out_stream.logfile,"Assign chains to sets")
    pdb_sequences = []
    coverage_set = set()
    for ipdb,tmp in enumerate(pdb_file_input):
        pdbf,pdbobj,unknown = tmp
        ichainset_not_matched= set()
        iseqset_seqid_high = set()
        iseqset = set()
        this_coverage_set = set()
        seqobj_bu_used = [] #deep copy cheat
        for iseq,sequence in enumerate(seqobj_bu):
          seqobj_bu_used.append(False)
        basetng.logBlank(enum_out_stream.logfile)
        basetng.logTab(enum_out_stream.logfile,"Structure: " + os.path.basename(pdbf))
        #seqobj_bu will not be modified
        #models may match different parts of same sequence
        list_of_pdb_hierarchy_by_chain = generate_list_of_pdb_hierarchy_by_chain(pdbobj)
        match_chain_to_seq = {} # chain,sequence order does not matter
        best_identity = None
        best_coverage = None
        for pdb_hierarchy_by_chain in list_of_pdb_hierarchy_by_chain:
          assert(len(pdb_hierarchy_by_chain.models()) == 1)
          assert(len(pdb_hierarchy_by_chain.models()[0].chains()) == 1)
          chainid = pdb_hierarchy_by_chain.models()[0].chains()[0].id
          match_chain_to_seq[chainid] = None
          #gets shorter each time if there is full coverage
          best_results = None
          high_seqid = False
          best_iseq = None
          # the point of this loop is to work out if the chain matches a sequence exactly
          #this is independent of coverage of the biological unit in the search
          #this coverage is to match sequence with the chain, no hybrid matches allowed
          for iseq,sequence in enumerate(seqobj_bu):
            iseq = int(iseq)
            if not seqobj_bu_used[iseq]:
           #{
              dict_results = sequence_alignment_validation(
                               pdb_hierarchy_by_chain,
                               [sequence],
                               alignment_params,
                               verbose=verbose_flag)
              if len(dict_results["failed_to_align"]):
                e = dict_results["failed_to_align"]
                basetng.logWarning("Failed to align: " + str(e))
                continue
              if verbose_flag:
                basetng.logTab(verbose,"Sequence #" + str(iseq+1))
                for line in dict_results['output']:
                   basetng.logTab(verbose,line)
              match_codes = dict_results['match_codes']
              if (best_identity is None) or (dict_results['seqid'] > best_identity):
                best_identity = dict_results['seqid']
              if dict_results['seqid'] < float(round(io_identity,2)):
                #try next
                continue
              coverage = float(round(dict_results['coverage'],2))
              #set a minimal coverage, this may not be justified unless AJM TODO zero occ applied
              #the sequence identity is unreliable for very low coverage
              #this is where the alphafold assumption of exact sequences is really helpful
              #we could then better rely on sequence identities and coverage
              #at the moment this is a heuristic hack
              if float(coverage) < 10: # tolerance 1
                #try next
                continue
              high_seqid = True
              iseqset_seqid_high.add(iseq)
              # add to set as this could be multiple high seqid,
              # don't know which one is real in the context of others
              # this coverage not io_coverage
              # however due to numerical instablity allow two residues tolerance in coverage eg. 2ixp with X at start of chain
              # otherwise identical sequences are not flagged as having >= coverage, and only the top is taken
              # tol not required when coverage is always 100% as there is no numerical instability in 1/1
              # A B C D sequences identical with 1 model -> A BCD -> A B*CD -> 2 models not 4
              tol = 100.*2./len(sequence)
            # basetng.logTab(verbose,"Tol " + str(tol) + " len sequence " + str(len(sequence)))
              if (best_coverage is None or coverage > (best_coverage - tol) ):
                 best_coverage = coverage
              if coverage < io_coverage: # tolerance 1
                #try next
                continue
              if best_results is None or coverage >= (best_results['coverage'] - tol):
                best_results = dict_results
                best_results['sequence'] = sequence
                best_iseq = iseq
                this_coverage_set.add(iseq)
           #}
          if not high_seqid:
            ichainset_not_matched.add(chainid)
          if best_results is not None and best_results['coverage'] >= io_coverage:
            #also for homotetramer searches in the asu, want to match with different chains
            seqobj_bu_used[best_iseq] = True
            match_chain_to_seq[chainid] = best_iseq #match
            iseqset.add(best_iseq)
        if testing_flag:
          for chainid in match_chain_to_seq:
            if match_chain_to_seq[chainid] is not None:
              basetng.logTab(verbose,"Match Chain " + str(chainid) + " to sequence #" + str(match_chain_to_seq[chainid]+1))
        #now that the chain order is known, reorder through the list of hierarchies by chain
        #it may be possible to insert the chains directly with insert_chain or something
        iseqset = sorted(iseqset)
        basetng.logTab(enum_out_stream.logfile,"Set of pdb chains with poor identity to biological unit sequences")
        basetng.logTab(enum_out_stream.logfile,str(sorted(list(ichainset_not_matched))))
        basetng.logTab(enum_out_stream.logfile,"Set of sequences with good identity within pdb")
        p1 = [x+1 for x in list(iseqset_seqid_high)]
        basetng.logTab(enum_out_stream.logfile,str(p1))
        basetng.logTab(enum_out_stream.logfile,"Set of sequences with good identity and full coverage within pdb")
        p2 = [x+1 for x in list(iseqset)]
        basetng.logTab(enum_out_stream.logfile,str(p2))
        basetng.logTab(enum_out_stream.logfile,"Best identity " + str(round(100*best_identity)/100.))
        basetng.logTab(enum_out_stream.logfile,"Best coverage " + str(round(100*best_coverage)/100.))
        if len(ichainset_not_matched):
          failed_pdbf.append(pdbf) #append this to the array that already exists
          #in place modification does not work
          continue
        elif len(iseqset) > 0:
          coverage_set.update(this_coverage_set) #not if this pdb rejected
          basetng.logTab(verbose,"Reorder chains")
          if testing_flag:
            testing_reordered = []
            for chain in pdbobj.models()[0].chains():
              chainid = chain.id
              testing_reordered.append(chainid)
            basetng.logTab(verbose,"Input chains   " + str(sorted(testing_reordered)))
            basetng.logTab(enum_out_stream.logfile,"Matched chains " + str((match_chain_to_seq)))
          for k,v in match_chain_to_seq.items():
            basetng.logTab(verbose,"Matched chain " + str(k) + " to sequence #" + str(int(v)+1))
          #replace with reordered chains, in the order of the sequence file
          m = iotbx.pdb.hierarchy.model()
          iseqset = sorted(iseqset)
          nchain = len(pdbobj.models()[0].chains())
          for iseq in range(len(seqobj_bu)):
            for pdb_hierarchy_by_chain in list_of_pdb_hierarchy_by_chain:
              chainid = pdb_hierarchy_by_chain.models()[0].chains()[0].id
              if iseq == match_chain_to_seq[chainid]:
                ccp = pdb_hierarchy_by_chain.models()[0].chains()[0].detached_copy()
                m.append_chain(chain = ccp)
                continue
          for pdb_hierarchy_by_chain in list_of_pdb_hierarchy_by_chain:
            chainid = pdb_hierarchy_by_chain.models()[0].chains()[0].id
            if match_chain_to_seq[chainid] is None:
              ccp = pdb_hierarchy_by_chain.models()[0].chains()[0].detached_copy()
              m.append_chain(chain = ccp)
          r = iotbx.pdb.hierarchy.root()
          r.append_model(model=m)
          pdbobj = r
          pdb_file_input[ipdb] = [pdbf,pdbobj,unknown] # replace
          assert(nchain == len(pdbobj.models()[0].chains()))
          reordered = []
          for chain in pdbobj.models()[0].chains():
            chainid = chain.id
            reordered.append(chainid)
          basetng.logTab(enum_out_stream.logfile,"Output chain order " + str(reordered))
        else:
          coverage_set.update(this_coverage_set) #not if this pdb rejected
          basetng.logTab(verbose,"No chain reordering")
        #convert to list, with sorted elements
        #this does not get reached if not matched, continue above
        pdb_sequences.append(iseqset)
        if testing_flag: #write the file and read back, next time through gets picked up though!
          #we don't need to write the file because we are keeping all the coordinates in memory
          tag = str(pdbf)
          tag = os.path.basename(tag)
          tag = os.path.splitext(tag)[0]
          DAGDB = basetng.DAGDATABASE
          subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
          if (os.path.exists(os.path.join(basetng.DataBase(),subdir))):
            ffname = FileSystem(basetng.DataBase(),subdir,tag+".reordered.pdb")
            pdbff = open(ffname.fstr(), "w")
            pdbff.write(pdbobj.as_pdb_string()) #slow
            pdbff.close()
            where = enum_out_stream.testing
            basetng.logBlank(where)
            basetng.logTab(where,"Write reordered pdb file: " + ffname.qfstrq())
            basetng.logBlank(where)

    #delete the pdbs from the list if there are unmatched chains, done with a copy op
    cp = []
    for tmp in pdb_file_input:
      if tmp[0] not in failed_pdbf:
        cp.append(tmp)
    pdb_file_input = cp
    assert(len(pdb_file_input) == len(pdb_sequences))

    if (pdb_sequences) == [[]]:
      basetng.logBlank(enum_out_stream.summary)
      basetng.logTab(enum_out_stream.summary,"No structures with full coverage and high sequence identity to sequence")
      basetng.logTab(enum_out_stream.summary,"The sequences will not be concatenated to match structure complexes")
      return seqobj_bu, []

    list_of_iseq_in_all_pdb = set()
    for iseqset in pdb_sequences:
    # print("iseqset",iseqset)
      for iseq in iseqset:
        list_of_iseq_in_all_pdb.add(iseq)
    list_of_iseq_in_all_pdb = sorted(list_of_iseq_in_all_pdb)

    if not len(list_of_iseq_in_all_pdb):
      basetng.logBlank(enum_out_stream.concise)
      basetng.logTab(enum_out_stream.logfile,"Biological unit sequence is not found in any pdb files")
      basetng.logTab(enum_out_stream.logfile,"Check sequence identity or coverage")
      return seqobj_bu, []

    basetng.logChevron(enum_out_stream.logfile,"Concatenate Associated Sequences")
    basetng.logTab(enum_out_stream.logfile,"Concatenate unmodelled sequences")
    basetng.logTab(enum_out_stream.logfile,"Only consider biological unit for concatenation")
  # print("list_of_iseq_in_chain",list_of_iseq_in_all_pdb)
    cols_of_flags_for_iseq_in_pdb = []
    for iseq in list_of_iseq_in_all_pdb:
      column_of_flags = []
      for iseqset in pdb_sequences:
        if iseq in iseqset:
          column_of_flags.append('+') # this must be marked the same for all so that match works below
        else:
          column_of_flags.append('-')
      cols_of_flags_for_iseq_in_pdb.append(column_of_flags)
    p1 = [x+1 for x in list(list_of_iseq_in_all_pdb)]
    flags = str(["%2d" % elem for elem in list_of_iseq_in_all_pdb]).replace('\'','').replace(',','')
    pflags = str(["%2d" % elem for elem in p1]).replace('\'','').replace(',','')
    basetng.logTab(enum_out_stream.logfile,           "biological unit sequence = " + str(pflags))
    flipped = map(list, zip(*cols_of_flags_for_iseq_in_pdb))
    for i,v in enumerate(flipped):
        flags = str(["%2s" % elem for elem in v]).replace('\'','').replace(',','')
        n = "%02d" % (i+1)
        basetng.logTab(enum_out_stream.logfile,"Model #" + n + " sequence flags = " + str(flags))

    p1 = []
    connect = []
    connected = [0] #zero is index into list_of_iseq_in_chain array
    p2 = [1] #zero is index into list_of_iseq_in_chain array
    for v in range(len(cols_of_flags_for_iseq_in_pdb)):
      if v > 0:
        if cols_of_flags_for_iseq_in_pdb[v] != cols_of_flags_for_iseq_in_pdb[v-1]:
          connect.append(connected)
          connected = [v] # start new set with this one
          p2 = [v+1] # start new set with this one
        else:
          connected.append(v)
          p2.append(v+1) # start new set with this one
    connect.append(connected)
    p1.append(p2)
    basetng.logTab(verbose,"Connected flags = " + str(p1))
    for connected_list in connect: #convert flag number to sequence number
      for i in range(len(connected_list)):
        connected_list[i] = list_of_iseq_in_all_pdb[connected_list[i]]
    p1 = [x+1 for x in list(connected_list)]
   #basetng.logTab(verbose,"Connected sequences including singletons = " + str(connect))
    basetng.logTab(verbose,"Connected sequences including singletons = " + str(p1))
    #now pick out the ones that have a length greater than one
    tmp = []
    p1 = []
    tmp2 = []
    p2 = []
    for c in connect:
      if len(c) > 1:
        tmp.append(c)
        p1.append(list([x+1 for x in list(c)]))
      else:
        tmp2.append(c)
        p2.append(list([x+1 for x in list(c)]))
    basetng.logTab(verbose,"Unconnected Sequences = " + str(p2))
    connect = tmp
    #connect is tmp but print p1
    basetng.logTab(enum_out_stream.logfile,"Connected Sequences = " + str(p1))
    p1 = list([x+1 for x in list(coverage_set)])
   #basetng.logTab(enum_out_stream.logfile,"Covered Sequences = " + str(list(coverage_set)))
    basetng.logTab(enum_out_stream.logfile,"Covered Sequences = " + str(p1))

    basetng.logTab(enum_out_stream.logfile,"Merge Sequences")
    basetng.logTab(enum_out_stream.logfile,"Merge sequences that are always modelled together")
    basetng.logTab(enum_out_stream.logfile,"Keep other sequences that are modelled")
    basetng.logTab(enum_out_stream.logfile,"Merge sequences that are not modelled")
    seqobj_bu_cat = [] #iotbx.bioinformatics.sequence
    used = [] #int
    dna = 'ACTGX'
    rna = 'ACUGX'
    basetng.logTab(enum_out_stream.logfile,"Number of connected sequences=" +str(len(connect)))
    for i,connected_list in enumerate(connect): #convert flag number to sequence number
        dnaseq = iotbx.bioinformatics.sequence(sequence="",name=(">merged dna ["))
        rnaseq = iotbx.bioinformatics.sequence(sequence="",name=(">merged rna ["))
        proseq = iotbx.bioinformatics.sequence(sequence="",name=(">merged protein ["))
        for j in connected_list:
          s1 = seqobj_bu[j].sequence
          if all(i in rna for i in s1): # more likely, priority
            rnaseq.name = rnaseq.name + " " + str(j)
            rnaseq.sequence = rnaseq.sequence + s1
          elif all(i in dna for i in s1):
            dnaseq.name = dnaseq.name + " " + str(j)
            dnaseq.sequence = dnaseq.sequence + s1
          else:
            proseq.name = proseq.name + " " + str(j)
            proseq.sequence = proseq.sequence + s1
          used.append(j)
        if len(rnaseq.sequence) > 0:
          rnaseq.name = rnaseq.name + "]"
          seqobj_bu_cat.append(rnaseq) # rna
        if len(dnaseq.sequence) > 0:
          dnaseq.name = dnaseq.name + "]"
          seqobj_bu_cat.append(dnaseq) # rna
        if len(proseq.sequence) > 0:
          proseq.name = proseq.name + "]"
          seqobj_bu_cat.append(proseq) # rna
    for j,seqobj in enumerate(seqobj_bu):
      if j not in used and j in coverage_set: # the singletons
        seqobj_bu_cat.append(seqobj)
    if False:
      for seqobj in seqobj_bu_cat:
        basetng.logTab(enum_out_stream.logfile,"Name: " + seqobj.name)
        lines = format_sequence(seqobj.sequence)
        for line in lines:
          basetng.logTab(enum_out_stream.logfile,line)
      basetng.logBlank(enum_out_stream.logfile)

    dnaseq = iotbx.bioinformatics.sequence(sequence="",name=(">not modelled dna ["))
    rnaseq = iotbx.bioinformatics.sequence(sequence="",name=(">not modelled rna ["))
    proseq = iotbx.bioinformatics.sequence(sequence="",name=(">not modelled protein ["))
    for j,sequence in enumerate(seqobj_bu):
      if j not in coverage_set:
        s1 = seqobj_bu[j].sequence
        if all(i in rna for i in s1): # more likely, priority
          rnaseq.name = rnaseq.name + " " + str(j)
          rnaseq.sequence = rnaseq.sequence + s1
          basetng.logAdvisory("Biological unit sequence contains RNA")
        elif all(i in dna for i in s1):
          dnaseq.name = dnaseq.name + " " + str(j)
          dnaseq.sequence = dnaseq.sequence + s1
          basetng.logAdvisory("Biological unit sequence contains DNA")
        else:
          proseq.name = proseq.name + " " + str(j)
          proseq.sequence = proseq.sequence + s1
    if len(rnaseq.sequence) > 0:
      rnaseq.name = rnaseq.name + "]"
      seqobj_bu_cat.append(rnaseq) # rna
    if len(dnaseq.sequence) > 0:
      dnaseq.name = dnaseq.name + "]"
      seqobj_bu_cat.append(dnaseq) # rna
    if len(proseq.sequence) > 0:
      proseq.name = proseq.name + "]"
      seqobj_bu_cat.append(proseq)
    basetng.logTab(enum_out_stream.logfile,"Number of sequences after merging = " + str(len(seqobj_bu_cat)))

    basetng.logChevron(verbose,"Biological Unit Sequence with respect to Models")
    for iseq,seqobj in enumerate(seqobj_bu_cat):
        basetng.logTab(verbose,">"+seqobj.name)
        lines = format_sequence(seqobj.sequence)
        for line in lines:
          basetng.logTab(verbose,line)
    basetng.logBlank(verbose)

    if testing_flag:
      where = enum_out_stream.testing
      ffname = io_seqin + ".analysis.seq"
      ffname = os.path.basename(ffname)
      ffname = FileSystem(basetng.DataBase(),subdir,ffname)
      if (os.path.exists(os.path.join(basetng.DataBase(),subdir))):
        seqff = open(ffname.fstr(), "w")
        for iseq,seqobj in enumerate(seqobj_bu_cat):
          seqff.write(">"+seqobj.name + "\n")
          lines = format_sequence(seqobj.sequence)
          for line in lines:
            seqff.write(line + "\n")
        seqff.close()
        basetng.logBlank(where)
        basetng.logTab(where,"Write analysed sequence to file: " + ffname.qfstrq())
        basetng.logBlank(where)

    basetng.logChevron(enum_out_stream.logfile,"Accepted files:")
    for pdb in found_pdb:
       basetng.logTab(enum_out_stream.logfile,"valid model file: "+pdb)

    return seqobj_bu_cat, found_pdb
  #}}}

  def ignore_sequence_analysis(self,
          basetng,
          found_pdb,
          io_seqin,
          io_bu_multiplicity,
          io_level,
          ):
   #{{{
    basetng.logAdvisory("Ignore coverage and sequence identity checks")
    io_level = io_level.strip()
    verbose_flag = (io_level == "verbose" or io_level == "testing")
    testing_flag = (io_level == "testing")
    verbose = enum_out_stream.logfile if io_level == "verbose" else enum_out_stream.verbose
  # if found_pdb is None:
  #   raise ValueError(enum_err_code.input,"No files for sequence analysis")

    alignment_params = setup_alignment_params()
    seqobj_bu_file_content = None #for return
    if io_seqin is not None:
      basetng.logChevron(verbose,"Query")
      basetng.logTab(verbose,"Biological Unit Sequence File: " + str(io_seqin))
      basetng.logTab(verbose,"Biological Unit Multiplicity: " + str(io_bu_multiplicity))
      seqobj_bu = any_file(io_seqin)
      # type(seqobj_bu.file_content)) is a list, of sequence objects
      seqobj_bu_file_content = seqobj_bu.file_content*io_bu_multiplicity

      if not seqobj_bu.file_type == "seq":
        raise ValueError(enum_err_code.fileopen,str(io_seqin))
        #flag for there was not sequence
      #at the end close file log_file.close()
      basetng.logTab(verbose,"Full sequence:")
      for i,seq in enumerate(seqobj_bu_file_content):
        if (len(seq.name) == 0):
          spname = (os.path.normpath(io_seqin).split(os.path.sep))
          seq.name = spname[-1] + " " + str(i+1)
        basetng.logTab(verbose,">"+seq.name)
        lines = format_sequence(seq.sequence)
        for line in lines:
          basetng.logTab(verbose,line)
        basetng.logTab(verbose,"Length = " + str(len(seq.sequence)))
        basetng.logTab(verbose,"Multiplicity = " + str(io_bu_multiplicity))

    basetng.logTab(verbose,"Ignore sequence analysis")
    return seqobj_bu_file_content, found_pdb
  #}}}
#}}}

class unique_element:
    def __init__(self,value,occurrences):
        self.value = value
        self.occurrences = occurrences

class search_builder:
#{{{
  def perm_unique(self,elements):
      eset=set(elements)
      listunique = [unique_element(i,elements.count(i)) for i in eset]
      u=len(elements)
      return perm_unique_helper(listunique,[0]*u,u-1)

  def perm_unique_helper(self,listunique,result_list,d):
      if d < 0:
          yield tuple(result_list)
      else:
          for i in listunique:
              if i.occurrences > 0:
                  result_list[d]=i.value
                  i.occurrences-=1
                  for g in  perm_unique_helper(listunique,result_list,d-1):
                      yield g
                  i.occurrences+=1

  #recursive algorithm to generate all combinations of sequences
  def permutation(self,start_time,
                    perm_list,remaining,acc=[],lookup=[],depth=None):
        r = range(len(remaining))
        excessive = 60
        if (time.time()-start_time) > excessive:
          return True
        for nselect in r:
          if depth is None or nselect+1 in depth:
            perms = itertools.permutations(r, nselect+1)
            lperms = self.unique_perms(perms,lookup)
            for perm in lperms: #perms are all the perms, perm is a list
              selection = []
              for p in perm:
                selection.append(remaining[p])
              remain1a = set(remaining) # for erase
              for a in selection:
                remain1a.remove(a)
              remain1 = list(remain1a)
              acc1 = []
              acc1.extend(acc)
              acc1.append(tuple(selection))
              acc1.sort()
              if (len(remain1) == 0):
                if acc1 not in perm_list:
                  perm_list.append(acc1)
              #   perm_list = self.unique_acc(perm_list,lookup)
              self.permutation(start_time=start_time,
                          perm_list=perm_list,remaining=remain1,acc=acc1,lookup=lookup,depth=depth)
        return False

  def unique_perms(self,perms,lookup):
       lperms = list(perms)
       tperms = []
       fperms = []
       for p in lperms:
         t = []
         for i in p:
           t.append(lookup[i])
         if t not in tperms:
           tperms.append(t)
           fperms.append(p)
       return fperms

  def unique_acc(self,lperms,lookup):
       tperms = []
       fperms = []
       for p in lperms:
         t = []
         for i in p:
           tt = []
           for j in i:
             tt.append(lookup[j])
           t.append(tt)
         t.sort()
         if t not in tperms:
         # print("New",p,t)
           tperms.append(t)
           fperms.append(p)
      #  else:
      #    print("Old",p,t)
       return fperms

  def run_search_builder(self,
          basetng,
          found_pdb,
          seqobj,
          io_identity,
          io_overlap,
          io_coverage,
          io_level,
          io_z,
          ):
   #{{{
    io_level = io_level.strip()
    verbose_flag = (io_level == "verbose" or io_level == "testing")
    testing_flag = (io_level == "testing")
    verbose = enum_out_stream.logfile if io_level == "verbose" else enum_out_stream.verbose
    if seqobj is None:
      return None
    if (found_pdb is None):
      return None #already logged errors
    alignment_params = setup_alignment_params()
    sequences = []
    names = []
    seqobj_asu = [] #of iotbx.bioinformatics.sequence
    nseq = len(seqobj)
    basetng.logTab(enum_out_stream.logfile,"Number of sequences: " + str(nseq))
    for seq_in in seqobj:
      for z in range(int(io_z)): #matthews value, expane the sequence by number in asymmetric unit
        sequences.append(seq_in.sequence) # this is for detecting unique
        names.append(seq_in.name) # this is for detecting unique
        seqobj_asu.append(seq_in) #this is the object that will be used below
    nseq = len(set(sequences))
    basetng.logTab(enum_out_stream.logfile,"Number of unique sequences: " + str(nseq))
    basetng.logTab(enum_out_stream.logfile,"Number of copies in biological unit : " + str(io_z))
    if nseq == 0:
      raise ValueError(enum_err_code.fileopen,str("No sequences"))
    for seq in set(sequences):
      basetng.logTab(verbose,"unique sequence:")
      lines = format_sequence(seq)
      for line in lines:
        basetng.logTab(verbose,line)
    basetng.logBlank(verbose)
    basetng.logTab(verbose,"Full asu sequence:")
    for seq in seqobj_asu:
      basetng.logTab(verbose,seq.name)
      lines = format_sequence(seq)
      for line in lines:
        basetng.logTab(verbose,line)

    lookup=sequences

    basetng.logChevron(enum_out_stream.logfile,"Permutations")
    nseq = len(seqobj_asu)
    assert(nseq > 0)


    min_pdb_length = None
    sum_pdb_length = int(0)
    npdb = int(0)
    for ifile in found_pdb:
      pdbf = os.path.join(ifile) #new
      pdbobj = iotbx.pdb.input(pdbf)
      #hack to single chain
      structure_probe,nres,chain_type = pdb_models_as_single_chain(pdbobj.construct_hierarchy())
      npdb += 1
      if min_pdb_length is None:
        min_pdb_length = nres
        basetng.logTab(enum_out_stream.logfile,os.path.basename(pdbf) + " nres=" + str(nres))
      elif nres < min_pdb_length:
        min_pdb_length = nres
        basetng.logTab(enum_out_stream.logfile,os.path.basename(pdbf) + " nres=" + str(nres))
      sum_pdb_length += nres
    basetng.logTab(enum_out_stream.logfile,"Number of structures = " + str(npdb))
    basetng.logTab(enum_out_stream.logfile,"Minimum pdb length =   " + str(min_pdb_length))
    basetng.logTab(enum_out_stream.logfile,"Summation pdb length = " + str(sum_pdb_length))
    if npdb == 0:
      message = "No sequences"
      basetng.logTab(enum_out_stream.summary,message)
      return
     #raise ValueError(enum_err_code.fatal,str(message))

    # the minimum number of sequences that must be concatenated to cover the smallest structure
    # if the structure is a monomer of a chain in the sequence this is 1
    # if the structure is a dimer of the two chains, then it will be 2
    # the maximum number of sequences that can be concatenated and cover all the pdb files
    # AJM TODO this could take into account alternatives for the same part of the sequence
    # but here it would model identical chains with different structures (conformations)
    # without  assuming that identical sequences are the same structure

    basetng.logChevron(enum_out_stream.summary,"Coverage requirement")
    mindepth = 1
    lensequences = []
    for s in sequences:
      lensequences.append(len(s))
    #sort the sequence in reverse order of size
    #the minimum depth will come from the longest sequences
    lensequences = sorted(lensequences,reverse=True)
    lenseq = int(0)
    for lens in lensequences:
      lenseq = lenseq + lens;
      if int(lenseq) >= int(min_pdb_length):
        break # because we have hit the limit
      else:
        mindepth = mindepth+1 # gone over limit, this one is allowed
    basetng.logTab(enum_out_stream.summary,"Lengths of sequences = " + str(lensequences))

    maxcoverage = 100*float(sum_pdb_length)/float(sum(lensequences))
    #above takes account of inherently partial searches
    basetng.logTab(enum_out_stream.summary,"Requested coverage =   " + str(io_coverage))
    if float(maxcoverage) < float(io_coverage):
      io_coverage = min(float(io_coverage),maxcoverage);
      io_coverage = round(io_coverage*100-0.5)/100
    basetng.logTab(enum_out_stream.summary,"Modified coverage =   " + str(io_coverage))

    basetng.logChevron(enum_out_stream.logfile,"Depth of permutations")
    maxdepth = 1
    lensequences = sorted(lensequences,reverse=False)
    #sort the sequence in order of size
    #the maximum depth will come from the shortest sequences
    lenseq = int(0)
    for lens in lensequences:
      basetng.logTab(enum_out_stream.testing,"lenseq " + str(lenseq) + " lens " + str(lens))
      lenseq = lenseq + lens
      basetng.logTab(enum_out_stream.testing,"lenseq " + str(int(lenseq)) + " sum pdb length " + str(int(sum_pdb_length)))
      if int(lenseq) >= int(sum_pdb_length):
        break # because we have hit the limit
      else:
        maxdepth = maxdepth+1 # gone over limit, this one is allowed
    #print(lookup)
    maxdepth = min(len(set(lookup)),maxdepth) #AJM TODO 4n3e with full structure 28 copies
    basetng.logTab(enum_out_stream.logfile,"Depth of permutations (range) = " + str(mindepth)+" "+str(maxdepth))

    maxdepth = max(maxdepth,mindepth)
    maxdepth += 1 # for the range
    assert(mindepth < maxdepth)
    depth = []
    for d in range(mindepth,maxdepth,1):
      depth.append(d)
    basetng.logTab(enum_out_stream.logfile,"Depth of permutations = " + str(depth))

   #depth = None
    indices = range(nseq) # can't use seqobj directly
   #lookup = ['A','A','A','A','A','A','A','A','A']
    perm_list = []
    basetng.logEllipsisOpen(enum_out_stream.logfile,"Permuting")
    excessive = self.permutation(start_time=time.time(),
                perm_list=perm_list,remaining=indices,lookup=lookup,depth=depth)
    basetng.logEllipsisShut(enum_out_stream.logfile)
    if (excessive):
      basetng.logTab(enum_out_stream.logfile,"Excessive permutation: elapsed time exceeded limit")
      basetng.logWarning("Sequence permutation truncated")
    basetng.logTab(enum_out_stream.logfile,"Sequence permutation list length = " + str(len(perm_list)))
    perm_list = self.unique_acc(perm_list,lookup)
    basetng.logTab(enum_out_stream.logfile,"unique permutation list length = " + str(len(perm_list)))
    basetng.logTab(verbose,"unique permutations = " + str(len(perm_list)))
    plist = set()
    for p in perm_list:
      basetng.logTab(verbose,list_to_str(p))
      for pp in p:
        for ppp in pp:
          plist.add(ppp)
    for ppp in plist:
      basetng.logTab(verbose,"index #" + str(ppp) + " " + os.path.basename(found_pdb[ppp]))

    sequence_perm_list = set() #list of lists
    for perm in perm_list:
        for tuple_group in list(perm):
           sequence_perm_list.add(tuple_group)
    sequence_perm_list = self.unique_perms(sequence_perm_list,lookup)
    basetng.logTab(verbose,"unique tuple list = " + str(len(sequence_perm_list)))
    for p in sequence_perm_list:
      basetng.logTab(verbose,list_to_str([p]))

    sequence_perm_list2 = []
    for perm in sequence_perm_list:
      sequence_perm_list2.append(tuple(perm))
    sequence_perm_list = sequence_perm_list2
    sequence_perm_list.sort()

    basetng.logChevron(enum_out_stream.logfile,"Unique Sequence Combinations")
    count = 0
    sequence_perm_list2 = []
    for tuple_group in sequence_perm_list:
        count = count + 1
        basetng.logTab(enum_out_stream.logfile,"#" + str(count))
        namestr = list_to_str([tuple_group])
        basetng.logTab(enum_out_stream.logfile,"Sequence = " + namestr)
        file_content = []
        newseq = iotbx.bioinformatics.sequence(sequence="",name=("P#"+str(count)))
        for p in list(tuple_group):
            newseq.name = newseq.name + " ++[" + names[p] + "]"
            newseq.sequence = newseq.sequence + sequences[p].replace('\n','')
        file_content.append(newseq)
        basetng.logTab(enum_out_stream.logfile,newseq.name)
        basetng.logTab(enum_out_stream.logfile,"Residues = " + str(len(newseq.sequence)))
        sequence_perm_list2.append(file_content)
        lines = format_sequence(newseq.sequence)
        for line in lines:
            basetng.logTab(verbose,line)
    basetng.logBlank(verbose)
    sequence_perm_list = sequence_perm_list2

    #array for each seqence permutation
    #variable length, one for each group_sequence in permutation
    #array of models for each sequence
    pcount = int(0)
    results = []
    where = verbose
    basetng.logTab(verbose,"Sequence perm list size = " + str(len(sequence_perm_list)))
    for file_content in sequence_perm_list:
   #{{{
      pcount = pcount + 1
      basetng.logChevron(where,"Sequence Alignment Permutation: #" + str(pcount) + " " + " of " + str(len(sequence_perm_list)))
      scount = int(0)
      list_of_models_for_permutation = []
      for sequence in file_content:
        basetng.logLine(where,4,'-')
        scount = scount + 1
      # basetng.logTab(where,"Sequence #" + str(scount)+ " of " + str(len(file_content)))
        basetng.logTab(where,str(sequence.name))
        basetng.logBlank(where)
        list_of_models_for_sequence = []
        for ifile in found_pdb:
       #{
          pdbf = os.path.join(ifile) #new
          pdbobj = iotbx.pdb.input(pdbf)
          tag = ifile
        # tag = os.path.basename(tag)
        # tag = os.path.splitext(tag)[0]
          DAGDB = basetng.DAGDATABASE
          subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
          ffname = FileSystem(basetng.DataBase(),subdir,tag+".A.pdb")
          structure_probe,nres,chain_type = pdb_models_as_single_chain(pdbobj.construct_hierarchy(),ffname.fstr(),testing_flag)
          if testing_flag:
            try:
              if len(ffname) > int(0):
                where = enum_out_stream.testing
                basetng.logBlank(where)
                basetng.logTab(where,"Write Single Chain Model File: "+ffname.qfstrq())
                basetng.logBlank(where)
            except:
              pass
          basetng.logTab(where,"Model File: "+os.path.basename(ifile)+" (residues="+str(nres)+")")
         #{{{
          if not chain_type:
            basetng.logWarning("Chain type not recognised as protein or rna or dna")
            continue
          if False: #write the sequence
            ssname,nseq,nnres = write_pdb_fasta(pdbf,alignment_params)
            basetng.logTab(where,"Write Sequence: " + ssname)
            basetng.logTab(where,"Number of sequences: " + str(nseq))
            basetng.logTab(where,"Number of residues: " + str(nnres))
          try:
            v_class = mmtbx.validation.sequence.validation(
                  pdb_hierarchy=structure_probe,
                  sequences=[sequence],
                  params=alignment_params,
                  )
          except Exception as e:
            basetng.logWarning("Failed to align: " + str(e))
            continue
          assert(len(v_class.chains) == 1) #chain is pdb chain which has been hacked to all one chain
          chain = v_class.chains[0]
          match_length = 0.
          for i in chain.alignment.match_codes:
            if i == 'm':
              match_length = match_length+1
          seqid = 100.*chain.alignment.calculate_sequence_identity(skip_chars=['X'])
        # print(chain.alignment.match_codes)
          match_percent = 100.*match_length/float(nres)
         #}}}
          basetng.logTab(where,"Match percent = " + str(int(match_percent)) + " [" + str(int(match_length)) + "/" + str(nres) + "]")
          basetng.logTab(where,"Sequence identity in contiguous segment = " + str(int(seqid)))
          if verbose_flag: # match codes
            basetng.logTab(where,"Match codes")
            lines = format_sequence(chain.alignment.match_codes)
            for line in lines:
              basetng.logTab(where,line)
          if (seqid < float(io_identity)):
            basetng.logTab(where,"--Low sequence identity (<" + str(io_identity) +")")
            basetng.logTab(where,"----Structure NOT match to sequence")
          #match_percent is not the same as the coverage for the sequences below
          #the match percent is so that the sequence identity is achieved with same number of residues
          #AJM this may need to be an input parameter, and may need to be linked to coverage
          #but at the moment I don't think so
          elif (match_percent < float(95)):
            basetng.logTab(where,"--Low match percent (<"+ str(95) +")")
            basetng.logTab(where,"----Structure NOT match to sequence")
          else:
            list_of_models_for_sequence.append(pdbf)
            npdb = len(list_of_models_for_sequence) # cheat
            basetng.logTab(where,"Structure match to sequence: match #" + str(npdb))
          basetng.logBlank(where)
          if verbose_flag:
            outstr = TextIO()
            v_class.show(out=outstr)
            lines = outstr.getvalue().split("\n")
            for line in lines:
              basetng.logTab(where,str(line))
       #}
        list_of_models_for_permutation.append(list_of_models_for_sequence) #flag for not a match
      results.append(list_of_models_for_permutation) #flag for not a match
    # break
      # for chain in v_class.chains:
      #   print(chain.sequence_name,chain.n_missing_start,chain.n_missing_end)
      #   print(chain.alignment.a[chain.n_missing_start:-chain.n_missing_end])
      #   print(chain.alignment.match_codes)
      #   print(chain.alignment.matches())
   #}}}

    assert(len(sequence_perm_list) == len(results))
    where = enum_out_stream.logfile
    where2 = verbose
   #{{{
    basetng.logChevron(where,"Overlap and Completeness")
  # basetng.logChevron(where2,"Number = " + str(len(list_of_models_for_permutation)))
    pcount = int(0)
    auto_search_map = {} # these are the results
    for list_of_models_for_permutation in results:
      match = True
      for list_of_models_for_sequence in list_of_models_for_permutation:
        for m in list_of_models_for_sequence:
          if len(m) == 0:
            match = False
      basetng.logLine(where,4,'-')
      basetng.logTab(where,"Sequence Permutation: #" + str(pcount+1) + " of " + str(len(results)) )
      basetng.logTab(where,"Match =  " + str(match))
      auto_search_sequences = [] # these are the results
      if match:
        scount = int(0)
        for sequence in sequence_perm_list[pcount]:
          basetng.logTab(where2,"Sequence #" + str(scount+1) +" (residues="+str(len(sequence.sequence))+")")
          basetng.logTab(where2,str(sequence.name))
          if verbose_flag: # print sequence
              lines = format_sequence(sequence.sequence)
              for line in lines:
                basetng.logTab(verbose,line)
          list_of_models_for_sequence = list_of_models_for_permutation[scount]
          if verbose_flag:
            #these are with paths
            basetng.logTab(where2,"Models = " + str(list_of_models_for_sequence))
          best_complete = 0
          scount = scount + 1
          r = range(len(list_of_models_for_sequence))
          allowed_combination = [] # these are the results
          allowed_combination_file_list = [] # these are the results
          disallowed_combination = [] # this is only for speed so as not to repeat bad combos
          for nselect in r:
            basetng.logTab(verbose,"Number selected from combinations = " + str(nselect+1))
            perms = itertools.combinations(r, nselect+1)
           #perms = itertools.permutations(r, nselect+1)
            #sequences are guaranteed to be longer that pdb file
            unique_combination = []
            for perm in list(perms): #perms are all the perms, perm is a list
              combination = list(perm)
              combination.sort()
              basetng.logLine(verbose,4,'-')
              basetng.logTab(verbose,"Combination = " + tuple_to_str(perm))
              if combination in unique_combination:
                basetng.logTab(verbose,"Old combination")
                continue
              else:
                basetng.logTab(verbose,"New combination")
                unique_combination.append(combination)
              sublist = False
              for dis in disallowed_combination:
                if sublist:
                  break # because can't break from rule below
                if (all(x in combination for x in dis)):
                  basetng.logTab(verbose,"Contains Sublist of Disallowed " + tuple_to_str(dis))
                  sublist = True
              if sublist:
                continue
              match_codes = None
              file_list = []
              pdbcount = int(0)
              for p in perm:
             #{{{
                pdbcount += 1
                basetng.logTab(verbose,"Number of Model Files: "+str(len(perm))+" ")
                pdbf = list_of_models_for_sequence[p]
                ifile = pdbf
             #  ifile = os.path.basename(ifile)
                file_list.append(ifile)
                pdbobj = iotbx.pdb.input(pdbf)
                structure_probe,nres,chain_type = pdb_models_as_single_chain(pdbobj.construct_hierarchy())
                basetng.logTab(verbose,"Model File: "+os.path.basename(ifile)+" (residues="+str(nres)+")")
                if not chain_type:
                  basetng.logWarning("Chain type not recognised as protein or rna or dna")
                  continue
                # this alignment matches the pdb structure to the sequence
                # if seq > pdb then insertions
                # if pdb > seq then insertions
                v_class = mmtbx.validation.sequence.validation(
                    pdb_hierarchy=structure_probe,
                    sequences=[sequence],
                    params=alignment_params,
                    )
                if verbose_flag:
                  outstr = TextIO()
                  v_class.show(out=outstr)
                  lines = outstr.getvalue().split("\n")
                  for line in lines:
                    basetng.logTab(verbose,str(line))
                assert(len(v_class.chains) == 1)
                chain = v_class.chains[0]
                if True: # print match codes
                  basetng.logTab(verbose,"Model match codes " + str(pdbcount))
                  lines = format_sequence(chain.alignment.match_codes)
                  for line in lines:
                    basetng.logTab(verbose,line)
                # basetng.logBlank(verbose)
                if match_codes == None:
                  basetng.logTab(verbose,"Match none")
                  match_codes = chain.alignment.match_codes
                else:
                  new_match_codes = chain.alignment.match_codes
                  if (len(match_codes) != len(new_match_codes)):
                    basetng.logTab(verbose,"Match " + str(len(match_codes)) + " != " + str(len(new_match_codes)))
                    basetng.logTab(verbose,"Match codes length different: continue")
                  # raise ValueError(enum_err_code.fatal,str("Match codes length"))
                    continue
                    return None
                  index = range(len(match_codes))
                  s = list(match_codes)
                  for i in index:
                    if match_codes[i] == 'x':
                      pass
                    elif match_codes[i] == 'm' and new_match_codes[i] == 'm':
                      s[i] = 'x'
                    elif new_match_codes[i] == 'm':
                      s[i] = 'm'
                  match_codes = "".join(s)
                  if True: # print match codes
                    basetng.logTab(verbose,"Combined match codes 1->" + str(pdbcount))
                    lines = format_sequence(match_codes)
                    for line in lines:
                      basetng.logTab(verbose,line)
                  # basetng.logBlank(verbose)
             #}}}
              index = range(len(match_codes))
              overlap = int(0)
              complete = int(0)
              #ignore the leading and trailing insert values, as these are going to be missing
              first_not_i = next((i for i, ch in enumerate(match_codes) if ch != 'i'), None)
              final_not_i = next((i for i, ch in enumerate(reversed(match_codes)) if ch != 'i'), None)
              if first_not_i is not None and final_not_i is not None:
                final_not_i = len(match_codes) - 1 - final_not_i  # Adjust for reversed index
              basetng.logTab(verbose,"Range first/last=" + str(first_not_i) + "/" + str(final_not_i))
              denominator = final_not_i + 1 - first_not_i;
             #AJM there must have been a reason for not just using 'm'....
              for i in range(first_not_i, final_not_i + 1):
                if match_codes[i] == 'i':
                  complete = complete + 1
                elif match_codes[i] == 'x':
                  overlap = overlap + 1
              basetng.logTab(verbose,"Match codes for completeness")
              lines = format_sequence(match_codes[first_not_i: final_not_i+1])
              for line in lines:
                      basetng.logTab(verbose,line)
              percent_overlap = 100.*float(overlap) / float(denominator)
              percent_complete = 100. - 100.*(float(complete) / float(denominator))
            # basetng.logTab(verbose,"Files = " + str(file_list))
              basetng.logTab(verbose,"Overlap = " + str(round(percent_overlap,2)) + " [" + str(overlap) + "/" + str(len(match_codes)) + "]")
              basetng.logTab(verbose,"Complete = " + str(round(percent_complete,2)) + " [" + str(len(match_codes)-complete) + "/" + str(len(match_codes)) + "]")
              if (percent_complete > best_complete):
                best_complete = percent_complete
              #AJM below may need to be an input parameter, but will cause packing clashes if not 0
              max_overlap_percent = float(io_overlap)
              if (percent_overlap <= max_overlap_percent and percent_complete > float(io_coverage)):
                basetng.logTab(verbose,"Allowed " + tuple_to_str(perm))
                allowed_combination.append(perm)
                allowed_combination_file_list.append(file_list)
              elif percent_overlap > max_overlap_percent:
                basetng.logTab(verbose,"Disallowed " + tuple_to_str(perm))
                disallowed_combination.append(perm)
              basetng.logLine(verbose,4,'-')
          basetng.logTab(where,"Best Coverage = " + str(round(best_complete,2)))
          for a in allowed_combination:
            basetng.logTab(verbose,"Allowed Combination Indices = " + str(a))
          for a in allowed_combination_file_list:
            basetng.logTab(where2,"Allowed Combination = " + str(a))
      #   if len(allowed_combination_file_list):
      #     auto_search_sequences.append(allowed_combination_file_list)
      if len(allowed_combination_file_list):
        auto_search_map[sequence.sequence] = (allowed_combination_file_list)
      basetng.logBlank(where2)
      pcount = pcount + 1
   #}}}

    #now we need to assemble the allowed structures for each seq combo into the perm_list
    basetng.logChevron(verbose,"Coverage Check")
    auto_search2 = []
    for perm in perm_list:
      basetng.logTab(verbose,"Perm list: " + str(perm))
      file_lists = []
      for tuple_group in perm:
        seq = ""
        for p in tuple_group:
          seq += sequences[p]
        if seq in (auto_search_map):
          file_lists.append(auto_search_map[seq])
     #this is where we make sure we have the whole biological unit
      if len(file_lists) == 0: # all of the tuple_groups must also be complete!!
        basetng.logTab(verbose,"Fail")
      elif len(file_lists) == len(perm): # all of the tuple_groups must also be complete!!
        auto_search2.append(file_lists)
        basetng.logTab(verbose,"Pass")
      else:
        basetng.logTab(verbose,"Fail")
    auto_search = auto_search2
  # for a in auto_search:
  #   print(a)

    basetng.logChevron(verbose,"Model File Combinations for Sequence Coverage")
    basetng.logTab(verbose,"Number of auto search " + str(len(auto_search)))
    unique_searches = []
    for pcount in range(len(auto_search)):
      basetng.logLine(verbose,4,'-')
      basetng.logTab(verbose,"Sequence Permutation: #" + str(pcount+1) + " of " + str(len(auto_search)) )
    # scount = int(0)
    # for sequence in sequence_perm_list[pcount]:
    #     basetng.logTab(verbose,"Sequence #" + str(scount+1))
    #     basetng.logTab(verbose,str(sequence.name))
    #     basetng.logTab(verbose,str(auto_search[pcount][scount]))
    #     scount = scount + 1
      file_lists = auto_search[pcount]
      #takes the combinations of the different alternatives for the different sequences
      # the unqiue sets are the searches
      searches = (list(itertools.product(*file_lists)))
      for search in searches:
       #s = list[s] # tuple
        basetng.logTab(verbose,str(search))
        #split the search up into components
        sorted_search = []
        for s in search:
          for ss in s:
            sorted_search.append(ss)
        sorted_search.sort()
        if sorted_search not in unique_searches:
          unique_searches.append(sorted_search)
      basetng.logBlank(verbose)

    return unique_searches
   #}}}

  def ignore_search_builder(self,
          basetng,
          found_pdb,
          found_duplicates,
          io_level,
          ):
   #{{{
    basetng.logAdvisory("Ignore search builder")
    io_level = io_level.strip()
    verbose_flag = (io_level == "verbose" or io_level == "testing")
    testing_flag = (io_level == "testing")
    verbose = enum_out_stream.logfile if io_level == "verbose" else enum_out_stream.verbose
    basetng.logChevron(verbose,"Model File Combinations for Sequence Coverage")
    unique_searches = [ [] ]
    for pdbf in found_pdb:
      tag = pdbf
    # tag = os.path.basename(pdbf)
    # tag = os.path.splitext(tag)[0]
      for n in range(found_duplicates[pdbf]):
        unique_searches[0].append(tag)
    basetng.logChevron(enum_out_stream.concise,"Unique Searches")
    if unique_searches is None:
        basetng.logTab(enum_out_stream.summary,"No unique searches")
        return [[]]
    basetng.logTab(enum_out_stream.concise,"Number of searches = " + str(len(unique_searches)))
    if True:
   #{
      for i,s in enumerate(unique_searches):
        basetng.logTab(enum_out_stream.concise,"#" + str(i+1))# + " " + str(s))
        for ss in s:
          basetng.logTab(enum_out_stream.concise,"--" + str(os.path.basename(ss)))

    #don't expand unique searches since expansion will be done in ellg
      basetng.logUnderLine(enum_out_stream.concise,"Unique Search Supersets")
      for i,s in enumerate(unique_searches):
        basetng.logTab(enum_out_stream.concise,"#" + str(i+1))# + " " + str(s))
        for ss in s:
          basetng.logTab(enum_out_stream.concise,"--" + str(os.path.basename(ss)))
   #}
    return unique_searches
   #}}}
#}}}

#these are internal parameters not interesting for the user
class runBUB:

  def find_valid_pdb_files(self,
          basetng,
          io_build,
          io_seqin,
          io_bu_multiplicity,
          io_filenames,
          io_identity,
          io_level,
          ):
  #{{{
      basetng.logBlank(enum_out_stream.logfile)
      found_pdb = []
      for ifile in io_filenames:
        if ifile is not None:
          found_pdb.append(ifile)
      basetng.logChevron(enum_out_stream.logfile,"Models")
      verbose = enum_out_stream.logfile if io_level == "verbose" else enum_out_stream.verbose
      if not io_build:
     #{
        where = enum_out_stream.logfile
        basetng.logBlank(where)
        countp = 1
        for ifile in found_pdb:
          pdbf = os.path.join(ifile) #new
          pdbobj = iotbx.pdb.input(pdbf)
          pdbobj = pdbobj.construct_hierarchy()
          basetng.logChevron(where,"Valid #: " + str(countp))
          countp = countp + 1
          basetng.logTab(where,"Model File: " + str(os.path.basename(ifile)))
          model = pdbobj.models()[0]
          for chain in model.chains():
               basetng.logTab(where,"Chain: \'" + str(chain.id) + "\'")
               if (chain.is_na() or chain.is_protein()):
                 basetng.logTab(where,"residues=" + str(len(chain.as_padded_sequence())))
                 lines = ''.join(chain.as_sequence())
                 lines = format_sequence(lines)
                 for line in lines:
                   basetng.logTab(where,line)
               else:
                 basetng.logTab(where,"not protein, dna or rna")
        basetng.logBlank(where)
     #}
      if io_build and io_seqin is not None:
     #{
        where = verbose
        basetng.logUnderLine(enum_out_stream.logfile,str("Model Acceptance"))
        basetng.logTab(where,str("Check that models match the biological unit"))
        found_pdb_ok = []
        basetng.logTab(where,"Sequence: " + str(io_seqin))
        seqobj = any_file(io_seqin)
        if seqobj.file_type != "seq":
          raise ValueError(enum_err_code.input,"Sequence not valid")
        alignment_params = setup_alignment_params()
       #seqobj = seqobj.file_content
        for ifile in found_pdb:
       #{
          basetng.logChevron(enum_out_stream.logfile,"Model File: "+os.path.basename(ifile))
          pdbf = os.path.join(ifile) #new
          pdbobj = iotbx.pdb.input(pdbf)
          pdb_hierarchy = pdbobj.construct_hierarchy()
          #alignment only works with one-model hierarchy
          if len(pdb_hierarchy.models()) != 1:
            sel_cache = pdb_hierarchy.atom_selection_cache()
            first_model = sel_cache.selection("model 1")
            pdb_hierarchy = pdb_hierarchy.select(first_model)
          sel_cache = pdb_hierarchy.atom_selection_cache()
          nothetero = sel_cache.selection("not hetero and not water") #water will give wrong number of nres
          pdb_hierarchy = pdb_hierarchy.select(nothetero)
          structure_probe = pdb_hierarchy
          model = structure_probe.models()[0]
          nres = 0
          bad_chain_type = []
          for chain in model.chains():
            main_conf = chain.conformers()[0]
            if not (main_conf.is_na() or main_conf.is_protein()):
              bad_chain_type.append(chain.id)
            for rg in chain.residue_groups():
              nres += 1
          structure_probe = pdb_hierarchy
          if len(bad_chain_type) > 0:
            basetng.logAdvisory("Chain type not recognised as protein or nucleic acid " + str(bad_chain_type))
          try:
            v_class = mmtbx.validation.sequence.validation(
                  pdb_hierarchy=structure_probe,
                 #sequences=[sequence],
                  sequences=seqobj.file_content, #[list]
                  params=alignment_params,
                  )
          except Exception as e:
            basetng.logWarning("Failed to align: " + str(e))
            continue
          match = False
        # attrs = vars(v_class.chains[0])
        # print(', '.join("%s: %s" % item for item in attrs.items()))
          #we have not amalgamated all chains into one
          #look for a match between any chain and the biological unit chains
          if True: # but to verbose output
              where = enum_out_stream.logfile
              outstr = TextIO()
              v_class.show(out=outstr)
              lines = outstr.getvalue().split("\n")
              skip = False
              basetng.logTab(where,"biological unit sequence #" + str(int(v_class.chains[0].sequence_id) + 1))
              for line in lines:
                if "residue IDs:" in line:
                  skip = True
                if "PDB" in line:
                  skip = False
                if not skip:
                  basetng.logTab(where,str(line)) #alignment
              basetng.logBlank(where)
          where = verbose
          for chain in v_class.chains:
            match_length = 0.
            for i in chain.alignment.match_codes:
              if i == 'm':
                match_length = match_length+1
            seqid = 100.*chain.alignment.calculate_sequence_identity(skip_chars=['X'])
          # print(chain.alignment.match_codes)
            match_percent = min(100,100.*match_length/float(nres)) #can be >100 with XXX
            basetng.logTab(where,"Chain \'" + str(chain.chain_id) + "\'")
            basetng.logTab(where,"Match percent = " + str(int(match_percent)) + " [" + str(int(match_length)) + "/" + str(nres) + "]")
            basetng.logTab(where,"Sequence identity in contiguous segment = " + str(int(seqid)))
            if True: # but to verbose output
              basetng.logTab(where,"Match codes")
              lines = format_sequence(chain.alignment.match_codes)
              for line in lines:
                basetng.logTab(where,line)
              basetng.logBlank(where)
            if (seqid >= float(io_identity)):
              match = True
              break # because we only need one match
          if (match):
            found_pdb_ok.append(ifile)
          else:
            where = enum_out_stream.logfile
            basetng.logTab(where,"--Low sequence identity")
            basetng.logTab(where,"----Structure NOT match to sequence")
       #}
        found_pdb = found_pdb_ok
     #}
      basetng.logChevron(enum_out_stream.logfile,"Accepted files:")
      for ifile in found_pdb:
         pdbf = os.path.join(ifile) #new
         basetng.logTab(enum_out_stream.logfile,"valid model file: "+os.path.basename(ifile))
      if len(found_pdb) == 0:
        message = "No pdb files"
        basetng.logTab(enum_out_stream.summary,message)
        #raise ValueError(enum_err_code.input,str(message))
      basetng.logBlank(enum_out_stream.logfile)
      basetng.logChevron(enum_out_stream.logfile,"Check for unique model files")
      def count_duplicates_with_comparator(found_pdb, comparator):
          count_dict = {}
          for item in found_pdb:
              found_duplicate = False
              for key in count_dict:
                  if comparator(item, key):
                      count_dict[key].append(item)
                      found_duplicate = True
                      break
              if not found_duplicate:
                  count_dict[item] = []
          return count_dict
      # Custom comparator function to check if two items are considered duplicates
      def custom_comparator(a, b):
          aa = os.path.join(a) #new
          bb = os.path.join(b) #new
          with open(aa, 'r',encoding="utf8", errors='ignore') as f1:
            a_content = f1.read()
          with open(bb, 'r',encoding="utf8", errors='ignore') as f2:
            b_content = f2.read()
          return a_content == b_content
      # Example usage:
      result = count_duplicates_with_comparator(found_pdb, custom_comparator)
      duplicates = []
      #found_duplicates will only be used with build=False to indicate that a model is duplicated in the biological unit
      #the user should copy the file verbatim and enter it twice/or put in same directory for file discovery
      found_duplicates = {}
      for k,v in result.items():
        tag = k
      # tag = os.path.basename(k)
        basetng.logTab(enum_out_stream.logfile,    "unique: " + tag)
        found_duplicates[k] = 1
      # tag = os.path.splitext(tag)[0]
        if (len(v) > 0):
          for vv in v:
            duplicates.append(vv)
            found_duplicates[k] += 1
            tag = vv
       #    tag = os.path.basename(vv)
       #    tag = os.path.splitext(tag)[0]
            basetng.logTab(enum_out_stream.logfile,"==copy: " + tag)
      found_pdb = [x for x in found_pdb if not x in duplicates]
      basetng.logBlank(enum_out_stream.logfile)
      for pdbf in found_pdb:
        basetng.logTab(enum_out_stream.logfile,str(found_duplicates[pdbf]) + " x unique valid model file: "+pdbf)
      return found_pdb,found_duplicates
 #}}}

  def run_from_basetng_and_input(self,basetng):
      io_filenames = []
      n = basetng.input.size(".phasertng.biological_unit_builder.model.")
      for i in range(n):
        io_filenames.append(isNone(basetng.input.get_as_str_i(".phasertng.biological_unit_builder.model.",i)))
      io_seqin=isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename."))
      io_build=(basetng.input.get_as_str(".phasertng.biological_unit_builder.build_from_sequence.").strip() == "True")
      if io_seqin is None:
        io_build = False
      # raise ValueError(enum_err_code.input,"No sequence file")
      unique_searches,found_pdb = self.inner_bub(
        basetng=basetng,
        io_hklin=isNone(basetng.input.get_as_str(".phasertng.hklin.filename.")),
        io_seqin=isNone(basetng.input.get_as_str(".phasertng.biological_unit.sequence.filename.")),
        io_bu_multiplicity=int(basetng.input.get_as_str(".phasertng.biological_unit.sequence.multiplicity.")),
        io_filenames=io_filenames,
        io_level=basetng.input.get_as_str(".phasertng.suite.level."),
        io_coverage=basetng.input.get_as_str(".phasertng.biological_unit_builder.coverage."),
        io_identity=basetng.input.get_as_str(".phasertng.biological_unit_builder.identity."),
        io_overlap=basetng.input.get_as_str(".phasertng.biological_unit_builder.overlap."),
        io_build=io_build,
        )
      if unique_searches is None or len(unique_searches) == 0 or found_pdb is None or len(found_pdb) == 0:
        basetng.logWarning("No search combinations")
        #print the input to help the user
        io_coverage=basetng.input.get_as_str(".phasertng.biological_unit_builder.coverage.")
        io_identity=basetng.input.get_as_str(".phasertng.biological_unit_builder.identity.")
        io_overlap=basetng.input.get_as_str(".phasertng.biological_unit_builder.overlap.")
        basetng.logTab(enum_out_stream.summary,"Consider altering input parameters from current:")
        basetng.logTab(enum_out_stream.summary,"phasertng biological_unit_builder coverage " + str(io_coverage))
        basetng.logTab(enum_out_stream.summary,"phasertng biological_unit_builder identity " + str(io_identity))
        basetng.logTab(enum_out_stream.summary,"phasertng biological_unit_builder overlap " + str(io_overlap))
        #and throw error
      # raise ValueError(enum_err_code.input,"No search combinations")
        return #if throw above is not done
      n = len(unique_searches)
      basetng.input.resize(".phasertng.search_combination.",n)
      for i,combo in enumerate(unique_searches):
        #unparse str(combo) with ast.literal_eval()
        basetng.input.set_as_str_i(".phasertng.search_combination.",str(combo),int(i))

  def inner_bub(self,
            basetng,
            io_hklin,
            io_seqin,
            io_bu_multiplicity,
            io_level,
            io_coverage,
            io_identity,
            io_overlap,
            io_build,
            io_filenames,
            ):
 #{{{
    #remove trailing file separators from directory
    DAGDB = basetng.DAGDATABASE
    saclass = sequence_analysis()
    saclass.print_query_sequence(
      basetng=basetng,
      io_seqin=io_seqin)
    #io_build can come in as a string or as a boolean
    if io_build is False:
        io_identity = 0
        io_coverage = 0
    basetng.logBlank(enum_out_stream.summary)
    basetng.logTab(enum_out_stream.summary,"Target identity requirement = " + str(io_identity))
    basetng.logTab(enum_out_stream.summary,"Target coverage requirement = " + str(io_coverage))
    found_pdb,found_duplicates = self.find_valid_pdb_files(
      basetng=basetng,
      io_build=io_build,
      io_seqin=io_seqin,
      io_bu_multiplicity=io_bu_multiplicity,
      io_identity=io_identity,
      io_filenames=io_filenames,
      io_level=io_level,
      )
    if len(found_pdb) == 0:
      found_pdb = []
    basetng.logUnderLine(enum_out_stream.summary,"Search Builder")
    #further modified below
    if io_build is False:
      seqobj,found_pdb = saclass.ignore_sequence_analysis(
        basetng=basetng,
        found_pdb=found_pdb,
        io_seqin=io_seqin,
        io_bu_multiplicity=io_bu_multiplicity,
        io_level=io_level,
        )
    else:
      seqobj,found_pdb = saclass.run_sequence_analysis(
        basetng=basetng,
        found_pdb=found_pdb,
        io_seqin=io_seqin,
        io_bu_multiplicity=io_bu_multiplicity,
        io_level=io_level,
        io_identity=io_identity,
        io_coverage=io_coverage,
        io_z=1,# because we are just looking at the bu sequence itself, otherwise too compilcated
        )
    #too complicated means that initially the code was going to find eg a dimer pdb file matching z=2 for sequence
    #combinatorial explosion ended this approach
    #the max that can be modelled is now one copy of the input bu sequence
 #  if (seqobj is None or len(seqobj) == 0) and len(found_pdb) > 0:
 #    unique_searches = found_pdb
    if io_build is False:
      sb = search_builder()
      unique_searches = sb.ignore_search_builder(
        basetng=basetng,
        found_pdb=found_pdb,
        found_duplicates=found_duplicates,
        io_level=io_level,
        )
    else:
      sb = search_builder()
      unique_searches = sb.run_search_builder(
        basetng=basetng,
        found_pdb=found_pdb,
        seqobj=seqobj,
        io_identity=io_identity,
        io_coverage=io_coverage,
        io_overlap=io_overlap,
        io_level=io_level,
        io_z=1,
        )
    unique_searches = edit_and_log_unique_searches(basetng=basetng,unique_searches=unique_searches)
    return unique_searches,found_pdb
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="directory path for models representing parts of the biological unit, which may in include all biological unit, sub-complexes, components and/or domains",required=True)
  parser.add_argument('-s',"--sequence",help="sequence of biological unit (biological complex in multiple chains)",required=True)
  parser.add_argument('-c',"--coverage",help="minimum coverage of biological unit by models",required=False,default=95.)
  parser.add_argument('-i',"--identity",help="minimum sequence identity for sequence match",required=False,default=90.)
  parser.add_argument('-o',"--overlap",help="maximum overlap of sequences",required=False,default=0)
  parser.add_argument('-v',"--verbose",dest='verbose',action='store_true',help="Verbose output",required=False)
  parser.add_argument('-a',"--analyse_sequence",help="analyse the sequence in the context of the models available",required=False,default=True)
  args = parser.parse_args()
  vstr = str(args.verbose).upper()
  if vstr.startswith('TRUE'): #also accepts true/false for 1 or 2
    args.verbose = "verbose"
  if vstr.startswith('FALSE'):
    args.verbose = "logfile"
  basetng = Phasertng("VOYAGER")
  runBUB().inner_bub(
             basetng,
             io_hklin = None,
             io_seqin=args.sequence,
             io_bu_multiplicity=1,
             io_level=args.verbose,
             io_coverage=args.coverage,
             io_identity=args.identity,
             io_overlap=args.overlap,
             io_build=True,
             io_filenames=[],
            )
  print(basetng.summary())
