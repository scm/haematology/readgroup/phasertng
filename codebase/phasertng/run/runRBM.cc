//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/src/Packing.h>
#include <phasertng/data/Sequence.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Rigid_body_maps_for_intensities
  void Phasertng::runRBM()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with pose mode");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->prepared(reflprep::FEFF))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (feff)");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
    {
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false);
    }
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    //initialize tNCS parameters from reflections and calculate correction terms
    bool store_halfR(false);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    int io_maxstored = input.value__int_number.at(".phasertng.rigid_body_maps.maximum_stored.").value_or_default();
    double maxres(3);

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Map calculation and coordinate generation");
    if (io_maxstored == 0 or io_maxstored >= DAGDB.size())
    logTab(where,"Maps for all nodes will be calculated");
    else if (io_maxstored == 1)
    logTab(where,"Only maps for first node will be calculated");
    else
    logTab(where,"Only maps for first " + itos(io_maxstored) + " nodes will be calculated");
    logBlank(where);
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    auto poses = DAGDB.WORK->POSE;
    int p(1);
    for (auto& pose : poses)
      logTab(where,"Pose#" + ntos(p++,poses.size()) + " " + pose.IDENTIFIER.str());
    logBlank(where);
    }}

    //when the dag nodes are entered
    //shift includes number on the end rbm-1 rbm-2 for each node
    //filename will be tracker-rbm-1
    std::string tag = input.running_mode;
    DAGDB.shift_tracker(hashing(),tag); //not shift_edge! this shifts the tracker for each node
    int n(1),N(DAGDB.size());
    //only one set of reflections is stored in memory
    //otherwise files must be written out
    DAGDB.restart();
    af_string chain_mapping;
    while (!DAGDB.at_end())
    {
      auto node = DAGDB.WORK; //pointer so in place changes
      node->RFACTOR = 100;
      if (io_maxstored and n > io_maxstored)
      {
        node->FULL.PRESENT = false;
        continue;
      }
      std::string nN = nNtos(n++,N);
      out::stream where(out::LOGFILE);
      logUnderLine(where,"Rigid Body Map"+nN);

      //The logic here is that we make a new entry with the current node information
      //so that the map files and coordinates form a new database entry on their own
      //independent of the poses from which they are derived
      Identifier& modlid = node->TRACKER.ULTIMATE; //modify tag here
      auto entry = DAGDB.add_entry(modlid);

      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      pod::FastPoseType ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS);
      double llg;
      std::tie(llg) = DAGDB.work_node_likelihood(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS,
          &ecalcs_sigmaa_pose);
      logTabArray(where,node->logAnnotation(nN+""));
      if (Suite::Extra())
        logTabArray(out::TESTING,node->logPose(nN+""));

      node->FULL.PRESENT = true;
      node->FULL.IDENTIFIER = modlid;
      node->FULL.ENTRY = entry;
      node->DRMS_SHIFT[modlid] = dag::node_t(modlid,0); //nonsense
      node->VRMS_VALUE[modlid] = dag::node_t(modlid,1); //nonsense
      node->CELL_SCALE[modlid] = dag::node_t(modlid,1); //nonsense
      //don't clear the POSE, need for packing

      logTab(out::VERBOSE,"Copy and reformat data");
      entry->DENSITY.copy_and_reformat_header(REFLECTIONS.get());
      for (auto ext : {"fwt","phwt","delfwt","delphwt","fcnat","phicnat","fom","hla","hlb","hlc","hld"} )
      {
        std::string key = ".phasertng.labin." + std::string(ext) + ".";
        entry->DENSITY.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
        input.value__mtzcol.at(key).set_value(input.value__mtzcol.at(key).name());
      }
      logTab(out::VERBOSE,"Calculate density");
      auto txt = entry->DENSITY.calculate_density(
          REFLECTIONS.get(),
          ecalcs_sigmaa_pose,
          selected.get_selected_array(),
          DAGDB.NODES.is_all_tncs_modelled());
      logTabArray(out::LOGFILE,txt);

      node->RFACTOR = entry->DENSITY.calculate_rfactor(
          REFLECTIONS.get(),
          ecalcs_sigmaa_pose,
          selected.get_selected_array(),
          maxres); //resolution

      logTab(out::VERBOSE,"Calculate coordinates");
      {{
        //use packing as a cheap way of creating coordinates
        Packing packing;
        packing.init_node(node);
       // packing.move_to_origin();
        bool keep_chains = input.value__boolean.at(".phasertng.rigid_body_maps.keep_chains.").value_or_default();
        chain_mapping = packing.structure_for_refinement(entry->COORDINATES,keep_chains);
        entry->COORDINATES.MODLID = entry->identify();
        entry->COORDINATES.REFLID = REFLECTIONS->reflid();
        entry->COORDINATES.TIMESTAMP = TimeStamp();
        Models models; //tmp for getting entry values
        models.SetPdb(entry->COORDINATES.get_pdb_cards());
        models.set_principal_statistics();
        entry->PRINCIPAL_TRANSLATION = -models.statistics.CENTRE;
        entry->MEAN_RADIUS = models.statistics.MEAN_RADIUS;
        entry->CENTRE = models.statistics.CENTRE;
        entry->EXTENT = models.statistics.EXTENT;
        {
        //calculate the volume from the sequence, same as for the map
        Sequence seq(models.SELENOMETHIONINE);
        bool use_unknown(false);
        seq.ParseModel(models.PDB[0],models.COUNT_SSBOND,use_unknown);
        seq.initialize(true,true);
        entry->VOLUME = seq.volume();
        }
        entry->SCATTERING = models.scattering()[0];
        entry->MOLECULAR_WEIGHT = models.molecular_weight()[0];
      }}
      node->FULL.FS = entry->fraction_scattering(REFLECTIONS->get_ztotalscat());
      logTab(out::VERBOSE,"Fraction scattering: " + dtos(node->FULL.FS));
      //don't clear the poses because we may want to recycle them, through next frf etc
      //the full is then an snapshot of the current solution
      //node->POSE.clear();

      {{
      out::stream where = out::LOGFILE;
      logChevron(where,"Database Entries");
      bool wf = WriteFiles() and WriteDensity();
      entry->DENSITY.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::DENSITY_MTZ));
      logFileWritten(out::LOGFILE,wf,"Density",entry->DENSITY);
      if (wf) entry->DENSITY.write_to_disk();
      entry->COORDINATES.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::COORDINATES_PDB));
      logFileWritten(out::LOGFILE,WriteFiles(),"Coordinate",entry->COORDINATES);
      if (WriteFiles()) entry->COORDINATES.write_to_disk();
      //store in pdbout for reporting single nodes
      if (WriteFiles() and DAGDB.size() == 1)
        input.value__path.at(".phasertng.pdbout.filename.").set_value(entry->COORDINATES.fstr());
      FileSystem filesystem;
      filesystem.SetFileSystem(DataBase(),entry->identify().FileDataBase(other::ENTRY_CARDS));
      wf = WriteCards();
      logFileWritten(out::LOGFILE,wf,"Entry cards",filesystem);
      auto dagcards = DAGDB.entry_unparse_card(DAGDB.ENTRIES[modlid]);
      if (wf) filesystem.write(dagcards);
      //the files are read if the entry is not in memory, with the filename reconstructed from the modlid
      }}
    }

    if (Suite::Extra())
    {{ //these are completely uninformative, R=53% can be solved, use phenix.refine
    out::stream where = out::TESTING;
    logTableTop(where,"R-factors");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTab(where,"R-factor resolution limit: " + dtos(maxres,1));
    logTab(where,snprintftos("%-6s %8s%c %10s %6s %5s%c",
                             "#","R-factor",' ',"LLG","TFZ","FTF",' '));
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      logTab(where,snprintftos("%-6d %8.1f%c %10.0f %6.2f %5.1f%c",
             i,node.RFACTOR,'%',node.LLG,node.ZSCORE,node.PERCENT,'%'));
      if (i == io_maxstored)
      {
        if (DAGDB.size() > io_maxstored)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_maxstored) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    logUnderLine(out::SUMMARY,"Chain Mapping");
    if (chain_mapping.size())
      logTabArray(out::SUMMARY,chain_mapping);
    else
      logTab(out::SUMMARY,"Input chain identifiers retained");

    if (input.generic_int == 9)
    { //output for b-factor refinement of residues
      DAGDB.NODES.front().HOLD.clear();
    }
  }

} //phasertng
