//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/jiffy.h>
#include <phasertng/dag/DagDatabase.h>
#include <autogen/include/Scope.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/hoist.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/table/pdb_chains_2chainid.h>
#include <fstream>
#include <phasertng/src/SpaceGroup.h>

//#define PHASERTNG_DEBUG_WORKING_NODE
//#define PHASERTNG_FAST_INTERPV

namespace phasertng {
namespace dag {

  void
  DagDatabase::append_parse_cards_raw(std::string cards)
  {
    constexpr char annotation[] = "annotation";
    constexpr char variance[] = "variance";
    constexpr char cell_scale[] = "cell_scale";
    constexpr char clash_worst[] = "clash_worst";
    constexpr char packs[] = "packs";
    constexpr char composition[] = "composition";
    constexpr char next[] = "next";
    constexpr char data_scale[] = "data_scale";
    constexpr char drms_shift[] = "drms_shift";
    constexpr char vrms_value[] = "vrms_value";
    constexpr char expected_information_content[] = "expected_information_content";
    constexpr char fss[] = "fss";
    constexpr char fs[] = "fs";
    constexpr char full[] = "full";
    constexpr char part[] = "part";
    constexpr char gyre[] = "gyre";
    constexpr char hermann_mauguin[] = "hermann_mauguin";
    constexpr char hklin[] = "hklin";
    constexpr char hold[] = "hold";
    constexpr char information_content[] = "information_content";
    constexpr char llg[] = "llg";
    constexpr char mapllg[] = "mapllg";
    constexpr char parents[] = "parents";
    constexpr char pose[] = "pose";
    constexpr char rellg[] = "rellg";
    constexpr char rfactor[] = "rfactor";
    constexpr char sad_expected_llg[] = "sad_expected_llg";
    constexpr char sad_information_content[] = "sad_information_content";
    constexpr char seek[] = "seek";
    constexpr char seek_components[] = "seek_components";
    constexpr char seek_components_ellg[] = "seek_components_ellg";
    constexpr char seek_resolution[] = "seek_resolution";
    constexpr char seek_resolution_phased[] = "seek_resolution_phased";
    constexpr char seek_complete[] = "seek_complete";
    constexpr char sigmaa[] = "sigmaa";
    constexpr char signal_resolution[] = "signal_resolution";
    constexpr char signal_resolution_phased[] = "signal_resolution_phased";
    constexpr char spacegroup[] = "spacegroup";
    constexpr char special_position[] = "special_position";
    constexpr char spans[] = "spans";
    constexpr char tncs_indicated[] = "tncs_indicated";
    constexpr char tncs_present_in_model[] = "tncs_present_in_model";
    constexpr char tncs_order[] = "tncs_order";
    constexpr char tncs_vector[] = "tncs_vector";
    constexpr char twinned[] = "twinned";
    constexpr char twinop[] = "twinop";
    constexpr char unitcell[] = "unitcell";
    constexpr char zscore[] = "zscore";
    constexpr char cellz[] = "cellz";
    constexpr char percent[] = "percent";
    constexpr char models_pdb[] = "models_pdb";
    constexpr char trace_pdb[] = "trace_pdb";
    constexpr char monostructure_pdb[] = "monostructure_pdb";
    constexpr char substructure_pdb[] = "substructure_pdb";
    constexpr char coordinates_pdb[] = "coordinates_pdb";
    constexpr char fcalcs_mtz[] = "fcalcs_mtz";
    constexpr char interpolation_mtz[] = "interpolation_mtz";
    constexpr char decomposition_mtz[] = "decomposition_mtz";
    constexpr char variance_mtz[] = "variance_mtz";
    constexpr char density_mtz[] = "density_mtz";
    constexpr char gradient_mtz[] = "gradient_mtz";
    constexpr char ultimate[] = "ultimate";
    constexpr char penultimate[] = "penultimate";
    constexpr char node[] = "node";
    constexpr char scale[] = "scale";
    constexpr char element[] = "element";
    constexpr char number[] = "number";
    constexpr char rxyz[] = "rxyz";
    constexpr char txyz[] = "txyz";
    constexpr char fractional[] = "fractional";
    constexpr char translated[] = "translated";
    constexpr char entry[] = "entry";
    constexpr char reflections[] = "reflections";
    constexpr char pathway[] = "pathway";
    constexpr char pathlog[] = "pathlog";
    constexpr char tracker[] = "tracker";
    constexpr char present[] = "present";
    constexpr char euler[] = "euler";
    constexpr char bfactor[] = "bfactor";
    constexpr char multiplicity[] = "m";
    constexpr char tncs_group[] = "tncs_group";
    constexpr char clash[] = "clash";
    constexpr char rmat[] = "rmat";
    constexpr char shift_orthogonal[] = "shift_orthogonal";
    constexpr char aellg[] = "aellg";
    constexpr char tfz[] = "tfz";
    constexpr char star[] = "star";
    constexpr char midssqr[] = "midssqr";
    constexpr char sigman[] = "sigman";
    constexpr char sigmap[] = "sigmap";
    constexpr char sigmaq[] = "sigmaq";
    constexpr char sigmah[] = "sigmah";
    constexpr char absrhoff[] = "absrhoff";
    constexpr char sigmapp[] = "sigmapp";
    constexpr char sigmaqq[] = "sigmaqq";
    constexpr char sigmahh[] = "sigmahh";
    constexpr char fdelta[] = "fdelta";
    constexpr char rhopm[] = "rhopm";

    //does not clear first, call parse_cards to clear first
    //it takes time to construct the strings in place each time, const string is a speed optimization
    const std::unordered_set<std::string> set_of_edge_keywords = {
      std::string(ultimate),
      std::string(penultimate)};
    const std::string scope = NODE_SCOPE();
    Identifier last_pathway_ultimate;
    Identifier last_pathway_penultimate;
    Identifier last_pathlog_ultimate;
    Identifier last_pathlog_penultimate;
    std::istringstream input_stream(cards);
    input_stream.seekg(0);
    Identifier ID;
    while (input_stream)
    {
      if (!NODES.size()) NODES.push_back(Node());
      dag::Node* item = &NODES.back();

      get_token(input_stream);
      if (curr_tok == Token::END) //keyIsEnd, without the opportunity for "kill" etc
      {
        break;
      }
      else if (tokenIsAlready(Token::ASSIGN)) //node_separator
      {
        NODES.push_back(Node());
        item = &NODES.back();
        skip_line(input_stream);
      }
      else if (tokenIsAlready(Token::COMMENT))
      {
        skip_line(input_stream);
      }
      else if (string_value == scope)
      {
        compulsoryKeyStr(input_stream,node);
        if (curr_tok == Token::END) { //keyIsEnd, without the opportunity for "kill" etc
          break;
        }
        else if (string_value == node) {
          //don't maintain compulsory list, just throw error if get to the end without finding keyword
          //compulsoryKeySet(input_stream,set_of_node_keywords);
          get_token(input_stream);
          if (curr_tok == Token::END) { //keyIsEnd, without the opportunity for "kill" etc
            break;
          }
          else if (string_value == zscore) {
            item->ZSCORE = get_flt_number(input_stream);
          }
          else if (string_value == percent) {
            item->PERCENT = get_percent(input_stream);
          }
          else if (string_value == cellz) {
            item->CELLZ = get_flt_number(input_stream);
          }
          else if (string_value == rfactor) {
            item->RFACTOR = get_percent(input_stream);
          }
          else if (string_value == llg) {
            item->LLG = get_flt_number(input_stream);
          }
          else if (string_value == fss) {
            item->FSS = get_character(input_stream);
          }
          else if (string_value == mapllg) {
            item->MAPLLG = get_flt_number(input_stream);
          }
          else if (string_value == hklin) {
            item->REFLID.initialize(get_identifier(input_stream)); //with the option to throw errors
          }
          else if (string_value == composition) {
            compulsoryKeyStr(input_stream,element);
            std::string element = get_string(input_stream);
            compulsoryKeyStr(input_stream,number);
            item->COMPOSITION[element] = get_int_number(input_stream,true,0,false,0);
          }
          else if (string_value == annotation) {
            item->ANNOTATION = get_string(input_stream);
          }
          else if (string_value == spacegroup) {
            item->HALL = skip_line(input_stream); //because it may include quotes
            //special implementation of getQuotedString
            if (item->HALL.size() > 2 and item->HALL.front() == '"' and item->HALL.back() == '"')
            { item->HALL.pop_back(); item->HALL.erase(item->HALL.begin()); }
          }
          else if (string_value == hermann_mauguin) {
            //auto HM = skip_line(input_stream); //because it may include quotes
            //information only, output derived from HALL
            //node object, but not unparsed and parsed with node elements
          }
          else if (string_value == unitcell) {
            item->CELL = cctbx::uctbx::unit_cell(get_flt_cell(input_stream));
          }
          else if (string_value == tncs_order) {
            item->TNCS_ORDER = get_int_number(input_stream);
          }
          else if (string_value == tncs_vector) {
            item->TNCS_VECTOR = get_flt_vector(input_stream);
          }
          else if (string_value == tncs_present_in_model) {
            item->TNCS_PRESENT_IN_MODEL = get_character(input_stream);
            if (item->TNCS_PRESENT_IN_MODEL == 'Y') item->TNCS_MODELLED = true; //sync
            if (item->TNCS_PRESENT_IN_MODEL == 'N') item->TNCS_MODELLED = false; //sync
          }
          else if (string_value == tncs_indicated) {
            item->TNCS_INDICATED = get_character(input_stream);
          }
          else if (string_value == twinned) {
            item->TWINNED = get_character(input_stream);
          }
          else if (string_value == twinop) {
            auto twinops = get_strings(input_stream); //could be multiple (h,k,-l) (h,-k,l)
            item->TWINOP = ""; //now convert the entry as a list of operators (words) to just a string
            for (auto twinop : twinops) //note never use h k -l without the commas
              item->TWINOP = twinop + " ";
            item->TWINOP.pop_back(); //will need to split this later
          }
          else if (string_value == packs) {
            item->PACKS = get_character(input_stream);
          }
          else if (string_value == clash_worst) {
            item->CLASH_WORST = get_percent(input_stream);
          }
          else if (string_value == special_position) {
            item->SPECIAL_POSITION = get_character(input_stream);
          }
          else if (string_value == spans) {
            item->SPANS = get_character(input_stream);
          }
          else if (string_value == information_content) {
            item->INFORMATION_CONTENT = get_flt_number(input_stream);
          }
          else if (string_value == expected_information_content) {
            item->EXPECTED_INFORMATION_CONTENT = get_flt_number(input_stream);
          }
          else if (string_value == sad_information_content) {
            item->SAD_INFORMATION_CONTENT = get_flt_number(input_stream);
          }
          else if (string_value == sad_expected_llg) {
            item->SAD_EXPECTED_LLG = get_flt_number(input_stream);
          }
          else if (string_value == parents) {
            auto parents = get_uuid_numbers(input_stream);
            if (parents.size() > 1 or (parents.size() == 1 and parents[0]))
              item->PARENTS = parents;
          }
          else if (string_value == rellg) {
            item->RELLG = get_flt_number(input_stream);
          }
          else if (string_value == signal_resolution) {
            item->SIGNAL_RESOLUTION = get_flt_number(input_stream);
          }
          else if (string_value == signal_resolution_phased) {
            item->SIGNAL_RESOLUTION_PHASED = get_flt_number(input_stream);
          }
          else if (string_value == seek_resolution) {
            item->SEEK_RESOLUTION = get_flt_number(input_stream);
          }
          else if (string_value == seek_resolution_phased) {
            item->SEEK_RESOLUTION_PHASED = get_flt_number(input_stream);
          }
          else if (string_value == seek_components) {
            item->SEEK_COMPONENTS = get_int_number(input_stream);
          }
          else if (string_value == seek_components_ellg) {
            item->SEEK_COMPONENTS_ELLG = get_flt_number(input_stream);
          }
          else if (string_value == seek_complete) {
            item->SEEK_COMPLETE = get_boolean(input_stream);
          }
          else if (string_value == data_scale) {
            item->DATA_SCALE = get_flt_number(input_stream);
          }
          else if (string_value == pose) {
            dag::Pose pose;
            pose.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors

            compulsoryKeyStr(input_stream,euler);
            pose.EULER = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,rmat);
            pose.SHIFT_MATRIX = get_flt_matrix(input_stream);

            compulsoryKeyStr(input_stream,rxyz);
            pose.ORTHR = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,fractional);
            pose.FRACT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,shift_orthogonal);
            pose.SHIFT_ORTHT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,txyz);
            pose.ORTHT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,bfactor);
            pose.BFAC = get_flt_number(input_stream,true,-999,true,999);

            compulsoryKeyStr(input_stream,multiplicity);
            pose.MULT = get_flt_number(input_stream,true,1,false,0);

            compulsoryKeyStr(input_stream,tncs_group);
            pose.TNCS_GROUP = get_int_paired(input_stream,true,1,false,0);

            compulsoryKeyStr(input_stream,clash);
            pose.CLASH = get_int_vector(input_stream,true,0,false,0);

            compulsoryKeyStr(input_stream,aellg);
            pose.AELLG = get_flt_number(input_stream);

            compulsoryKeyStr(input_stream,tfz);
            pose.TFZ = get_flt_number(input_stream);
            item->POSE.push_back(pose);
          }
          else if (string_value == next) {
            dag::Next& next = item->NEXT;
            next.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors

            compulsoryKeyStr(input_stream,euler);
            next.EULER = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,rmat);
            next.SHIFT_MATRIX = get_flt_matrix(input_stream);

            compulsoryKeyStr(input_stream,rxyz);
            next.ORTHR = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,translated);
            next.TRANSLATED = get_boolean(input_stream);

            compulsoryKeyStr(input_stream,fractional);
            next.FRACT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,shift_orthogonal);
            next.SHIFT_ORTHT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,tfz);
            next.TFZ = get_flt_number(input_stream);
            item->NEXT.PRESENT = true;
          }
          else if (string_value == gyre) {
            dag::Gyre gyre;
            gyre.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors

            compulsoryKeyStr(input_stream,rxyz);
            gyre.ORTHR = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,txyz);
            gyre.ORTHT = get_flt_vector(input_stream);

            compulsoryKeyStr(input_stream,rmat);
            gyre.SHIFT_MATRIX = get_flt_matrix(input_stream);

            item->NEXT.GYRE.push_back(gyre);
          }
          else if (string_value == seek) {
            dag::Seek seek;
            seek.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors

            compulsoryKeyStr(input_stream,star);
            seek.STAR = get_character(input_stream);

            item->SEEK.push_back(seek);
          }
          else if (string_value == hold) {
            dag::Hold hold;
            hold.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors
            item->HOLD.push_back(hold);
          }
          else if (string_value == full) {
            compulsoryKeyStr(input_stream,present);
            item->FULL.PRESENT = get_boolean(input_stream);
            item->FULL.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors
            compulsoryKeyStr(input_stream,fs);
            item->FULL.FS = get_flt_number(input_stream);
          }
          else if (string_value == part) {
            compulsoryKeyStr(input_stream,present);
            item->PART.PRESENT = get_boolean(input_stream);
            item->PART.IDENTIFIER.initialize(get_identifier(input_stream)); //with the option to throw errors
          }
          else if (string_value == variance) {
            ID.initialize(get_identifier(input_stream)); //with the option to throw errors
            compulsoryKeyStr(input_stream,drms_shift);
            item->DRMS_SHIFT[ID] = node_t(ID,get_flt_number(input_stream));
            compulsoryKeyStr(input_stream,vrms_value);
            item->VRMS_VALUE[ID] = node_t(ID,get_flt_number(input_stream));
            compulsoryKeyStr(input_stream,cell_scale);
            item->CELL_SCALE[ID] = node_t(ID,get_flt_number(input_stream,true,0.8,true,1.2));
          }
          else if (string_value == sigmaa) {
            compulsoryKeyStr(input_stream,midssqr);
            item->SADSIGMAA.MIDSSQR.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,sigman);
            item->SADSIGMAA.SigmaN_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,sigmap);
            item->SADSIGMAA.SigmaP_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,sigmaq);
            item->SADSIGMAA.SigmaQ_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,sigmah);
            item->SADSIGMAA.SigmaH_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,absrhoff);
            item->SADSIGMAA.absrhoFF_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,sigmapp);
            item->SADSIGMAA.sigmaPP_bin.push_back(get_flt_complx(input_stream));

            compulsoryKeyStr(input_stream,sigmaqq);
            item->SADSIGMAA.sigmaQQ_bin.push_back(get_flt_complx(input_stream));

            compulsoryKeyStr(input_stream,sigmahh);
            item->SADSIGMAA.sigmaHH_bin.push_back(get_flt_complx(input_stream));

            compulsoryKeyStr(input_stream,fdelta);
            item->SADSIGMAA.fDelta_bin.push_back(get_flt_number(input_stream));

            compulsoryKeyStr(input_stream,rhopm);
            item->SADSIGMAA.rhopm_bin.push_back(get_flt_number(input_stream));
          }
          else
            throw Error(err::PARSE,"Node keyword \"" + string_value + "\" not recognised");
        }
        else if (string_value == entry) {
          DogTag dogtag;
          dogtag.initialize(get_dogtag(input_stream));
          if (!ENTRIES.count(dogtag.IDENTIFIER))
          {
            parse_entry_data(input_stream,dogtag);
          }
          else
          {
            std::string skip;
            std::getline(input_stream,skip);
            curr_tok = Token::ENDLINE; //could actually be Token::END
          }
        }
        else if (string_value == reflections) {
          DogTag dogtag;
          dogtag.initialize(get_dogtag(input_stream));
          REFLIDS[dogtag.IDENTIFIER] = dogtag;
        }
        else if (string_value == pathway) {
          compulsoryKeySet(input_stream,set_of_edge_keywords);
          if (curr_tok == Token::END) { //keyIsEnd, without the opportunity for "kill" etc
            break;
          }
          else if (string_value == ultimate) {
            ID.initialize(get_identifier(input_stream)); //with the option to throw errors
            //database object
            if (!last_pathway_ultimate.is_set_valid()) //first
              PATHWAY.ULTIMATE = ID;
            else if (last_pathway_ultimate != ID)
              storeError(err::SYNTAX,"Ultimate identifier inconsistent");
            last_pathway_ultimate = ID;
            //else leave as it is ie consistent
          }
          else if (string_value == penultimate) {
            ID.initialize(get_identifier(input_stream)); //with the option to throw errors
            //database object
            if (!last_pathway_penultimate.is_set_valid()) //first
              PATHWAY.PENULTIMATE = ID;
            else if (last_pathway_penultimate != ID)
              storeError(err::SYNTAX,"Penultimate identifier inconsistent");
            last_pathway_penultimate = ID;
            //else leave as it is ie consistent
          }
        }
        else if (string_value == pathlog) {
          compulsoryKeySet(input_stream,set_of_edge_keywords);
          if (curr_tok == Token::END) { //keyIsEnd, without the opportunity for "kill" etc
            break;
          }
          else if (string_value == ultimate) {
            ID.initialize(get_identifier(input_stream)); //with the option to throw errors
            //database object
            if (!last_pathlog_ultimate.is_set_valid()) //first
              PATHLOG.ULTIMATE = ID;
            else if (last_pathlog_ultimate != ID)
              storeError(err::SYNTAX,"Ultimate identifier inconsistent");
            last_pathlog_ultimate = ID;
            //else leave as it is ie consistent
          }
          else if (string_value == penultimate) {
            ID.initialize(get_identifier(input_stream)); //with the option to throw errors
            //database object
            if (!last_pathlog_penultimate.is_set_valid()) //first
              PATHLOG.PENULTIMATE = ID;
            else if (last_pathlog_penultimate != ID)
              storeError(err::SYNTAX,"Penultimate identifier inconsistent");
            last_pathlog_penultimate = ID;
            //else leave as it is ie consistent
          }
        }
        else if (string_value == tracker) {
          compulsoryKeySet(input_stream,set_of_edge_keywords);
          if (curr_tok == Token::END) { //keyIsEnd, without the opportunity for "kill" etc
            break;
          }
          else if (string_value == ultimate) {
            item->TRACKER.ULTIMATE.initialize(get_identifier(input_stream)); //with the option to throw errors
            //node object, but not unparsed and parsed with node elements
          }
          else if (string_value == penultimate) {
            item->TRACKER.PENULTIMATE.initialize(get_identifier(input_stream)); //with the option to throw errors
            //node object, but not unparsed and parsed with node elements
          }
        }
        skip_line(input_stream);
      }
      else {
        bool ignore_unknown_keys(false);
        if (!ignore_unknown_keys and string_value.size())
          storeError(err::SYNTAX,"Unknown dag card keyword: " + string_value);
        skip_line(input_stream);
      }
    }
    //ASSIGN at end of file introduced default node
    while (NODES.size() and NODES.back().is_default())
    {
      NODES.pop_back();
    }
    /*
    bool rm_default_nodes(true);
    //loop through all nodes and check for empties==defaults
    if (rm_default_nodes)
    {
      for (auto& item : NODES)
        item.generic_flag = !item.is_default();
      size_t last = 0;
      //fill start with interesting, in order
      for (size_t i = 0; i < NODES.size(); i++)
        if (NODES[i].generic_flag)
          NODES[last++] = NODES[i];
      NODES.erase(NODES.begin() + last, NODES.end());//erase the end, which is not rubbish
    }
    */
    //below is only relied upon when calling from python outside of a mode
    reset_entry_pointers(""); //don't refer to database here
 //don't do this because used_modlid is not a good indicator of whether things must stay in memory
 // auto modlid = NODES.used_modlid();
 // auto iter = ENTRIES.begin();
 // while (iter != ENTRIES.end())
 // {
 //   if (modlid.count(iter->first) == 0) {
 //     iter = ENTRIES.erase(iter);
 //   } else iter++;
 // }
  }

  void
  DagDatabase::parse_entry_data(std::istringstream& input_stream,DogTag modlid)
  {
    if (ENTRIES.count(modlid.identify())) return;
    constexpr char atom[] = "atom";
    constexpr char helix[] = "helix";
    constexpr char map[] = "map";
    constexpr char scattering[] = "scattering";
    constexpr char molecular_weight[] = "mw"; //fewer characters to keep file small
    constexpr char principal_translation[] = "pt"; //fewer characters to keep file small
    constexpr char principal_orientation[] = "pr"; //fewer characters to keep file small
    constexpr char mean_radius[] = "radius"; //fewer characters to keep file small
    constexpr char volume[] = "volume";
    constexpr char centre[] = "centre";
    constexpr char extent[] = "extent";
    constexpr char point_group_symbol[] = "pg_symbol"; //fewer characters to keep file small
    constexpr char point_group_euler[] = "pg_euler"; //fewer characters to keep file small
    constexpr char vrms_start[] = "vrms";
    constexpr char drms_range[] = "drms";
    constexpr char hires[] = "hires";
    constexpr char lores[] = "lores";
    constexpr char data[] = "data";

    //if the entry already exists we have exited already
    ENTRIES[modlid.identify()] = dag::Entry();
    dag::Entry& entry_data = ENTRIES[modlid.identify()]; //alias

    entry_data.DOGTAG = modlid;

    compulsoryKeyStr(input_stream,atom);
    entry_data.ATOM = get_boolean(input_stream);

    compulsoryKeyStr(input_stream,helix);
    entry_data.HELIX = get_boolean(input_stream);

    compulsoryKeyStr(input_stream,map);
    entry_data.MAP = get_boolean(input_stream);

    compulsoryKeyStr(input_stream,scattering);
    entry_data.SCATTERING = get_int_number(input_stream,true,0);

    compulsoryKeyStr(input_stream,molecular_weight);
    entry_data.MOLECULAR_WEIGHT = get_flt_number(input_stream,true,0);

    compulsoryKeyStr(input_stream,principal_translation);
    entry_data.PRINCIPAL_TRANSLATION = get_flt_vector(input_stream);

    compulsoryKeyStr(input_stream,principal_orientation);
    entry_data.PRINCIPAL_ORIENTATION = get_flt_matrix(input_stream);

    compulsoryKeyStr(input_stream,mean_radius);
    entry_data.MEAN_RADIUS = get_flt_number(input_stream,true,0);

    compulsoryKeyStr(input_stream,volume);
    entry_data.VOLUME = get_flt_number(input_stream,true,0);

    compulsoryKeyStr(input_stream,centre);
    entry_data.CENTRE = get_flt_vector(input_stream);

    compulsoryKeyStr(input_stream,extent);
    entry_data.EXTENT = get_flt_vector(input_stream);

    compulsoryKeyStr(input_stream,point_group_symbol);
    entry_data.POINT_GROUP_SYMBOL = get_string(input_stream);

    compulsoryKeyStr(input_stream,point_group_euler);
    entry_data.POINT_GROUP_EULER  = get_flt_vectors(input_stream);

    compulsoryKeyStr(input_stream,vrms_start);
    entry_data.VRMS_START  = get_flt_numbers(input_stream);

    compulsoryKeyStr(input_stream,drms_range);
    std::tie(entry_data.DRMS_MIN,entry_data.DRMS_MAX)  = get_flt_paired(input_stream);

    compulsoryKeyStr(input_stream,hires);
    entry_data.HIRES  = get_flt_number(input_stream);

    compulsoryKeyStr(input_stream,lores);
    entry_data.LORES  = get_flt_number(input_stream);

    compulsoryKeyStr(input_stream,data);
    entry_data.set_data_choice_selection_int(get_int_numbers(input_stream));
  }

  af_string
  DagDatabase::unparse_card_summary()
  {
    af_string output;
    if (NODES.size() == 0)
    {
      output.push_back("No nodes");
      return output;
    }
    //store the components for unparsing that do not change
    std::map<Identifier,std::string> entries;
    for (const auto& entry : ENTRIES)
    { //unparse all entries to node, overkill but much faster on write
      entries[entry.first] = entry_unparse_card(entry.second);
    }
    std::set<std::string> reflids;
    for (const auto& reflid : REFLIDS)
    { //unparse all entries to node, overkill but much faster on write
      if (reflid.second.is_set_valid())
      reflids.insert(reflid_unparse_card(reflid.second));
    }
    std::ostringstream ss;
    //unparse the first node
    {
      auto& node = NODES[0];
      ss << edge_unparse_card(PATHWAY,"pathway");
      ss << edge_unparse_card(PATHLOG,"pathlog");
      ss << edge_unparse_card(node.TRACKER,"tracker");
      node_unparse_card(node,ss);
      auto used_modlid = node.used_modlid(); //uuid
      for (const auto& e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e;
      ss << NODE_SEPARATOR() << std::endl;
    }
    //skip nodes
    if (NODES.size()>1)
    {
      int j = NODES.size()-1;
      ss << std::string("\n plus " + std::to_string(j) + " extra node" + std::string(j>1 ? "s":"") + "\n");
      ss << std::endl;
      ss << NODE_SEPARATOR() << std::endl; //(first keyword in line)
    }
    std::string text = ss.str();
    sv_string lines; //for split lines
    hoist::algorithm::split(lines, text, ("\n"));
    for (auto& item : lines)
    {
      hoist::algorithm::trim(item);
      output.push_back(item); //format change to af_string
    }
    return output;
  }

  void
  DagDatabase::parse_card()
  {
    clear_cards();
    append_parse_card(*this);
  }

  dag::Edge
  DagDatabase::parse_cards_retaining_seeks(FileSystem DAGFILE)
  {
    //does not clear first
    PHASER_ASSERT(NODES.size()); //there must be at least one node with the new cca etc
    auto newellg = NODES[0]; //deep copy of new cca elements, including new seek
    dag::Edge newpathway = PATHWAY;
    clear_cards(); //now clear the solutions

    std::ifstream infile(DAGFILE.fstr().c_str(), std::ios::in | std::ios::binary);
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    std::string cards = buffer.str();

    append_parse_cards_raw(cards);
    infile.close();
    dag::Edge filepathway = PATHWAY;

    auto& partial = NODES[0];
    if (newellg.SEEK.size())
      PHASER_ASSERT(partial.POSE.size() <= newellg.SEEK.size());
   //put the poses in order in the seek first
   //erase these from the order in the newellg as you go
   //then patch the missing to the end of the poses, in order in newellg
    std::vector<Identifier> erasure;
    std::vector<Identifier> sorted;
    for (int i = 0; i < newellg.SEEK.size(); i++)
    {
      erasure.push_back(newellg.SEEK[i].IDENTIFIER);
    }
    for (int i = 0; i < partial.POSE.size(); i++)
    {
      for (int j = 0; j < erasure.size(); j++)
      { //only called when this matching works out
        if  (partial.POSE[i].IDENTIFIER == erasure[j])
        {
          sorted.push_back(erasure[j]);
          erasure.erase(erasure.begin()+j);
          break;
        }
      }
    }
    for (int j = 0; j < erasure.size(); j++)
    {
      sorted.push_back(erasure[j]);
    }
    newellg.SEEK.clear();
    for (int j = 0; j < sorted.size(); j++)
    {
      dag::Seek seek;
      seek.IDENTIFIER = sorted[j];
      newellg.SEEK.push_back(seek);
    }
    int i(0);
    for (auto& node : NODES) //restored partial
    {
     // the poses will match the seeks if it is from a previous complete search, but not necessary
      if (newellg.SEEK.size())
        PHASER_ASSERT(node.POSE.size() <= newellg.SEEK.size());
      node.SEEK = newellg.SEEK; //append those yet to be found
      node.SEEK_COMPONENTS = newellg.SEEK_COMPONENTS; //append those yet to be found
      node.SEEK_RESOLUTION = newellg.SEEK_RESOLUTION; //append those yet to be found
      node.SEEK_COMPONENTS_ELLG = newellg.SEEK_COMPONENTS_ELLG; //append those yet to be found
      node.SEEK_COMPLETE = newellg.SEEK_COMPLETE; //append those yet to be found
      if (newellg.SEEK.size())
        for (int i = 0; i < node.POSE.size(); i++) //if there are, in fact, any poses
          PHASER_ASSERT(node.POSE[i].IDENTIFIER == node.SEEK[i].IDENTIFIER);
      //set by the cca mode, copied here to graft new seek onto old partial solutions
      node.NEXT.PRESENT = false; //paranoia
      node.NEXT.IDENTIFIER = Identifier(); //necessary for reset
      node.FULL.PRESENT = false; //paranoia
      node.FULL.IDENTIFIER = Identifier(); //necessary for reset
      //node.PARENTS = newellg.PARENTS; //these are ok as is?
      //node.HOLD.clear(); //do not clear!!!
      node.REFLID = newellg.REFLID; //scaled to composition, stored z
      node.COMPOSITION = newellg.COMPOSITION;
      auto used = node.used_modlid(); //uuid
      typedef mapnode_t::const_iterator citer;
      for (citer iter = node.DRMS_SHIFT.cbegin() ; iter !=  node.DRMS_SHIFT.cend() ; )
      {
        auto modlid = iter->first; //uuid
        iter = used.count(modlid) == 0 ? node.DRMS_SHIFT.erase(iter) : std::next(iter);
      }
      for (citer iter = node.VRMS_VALUE.cbegin() ; iter !=  node.VRMS_VALUE.cend() ; )
      {
        auto modlid = iter->first; //uuid
        iter = used.count(modlid) == 0 ? node.VRMS_VALUE.erase(iter) : std::next(iter);
      }
      for (citer iter = node.CELL_SCALE.cbegin() ; iter !=  node.CELL_SCALE.cend() ; )
      {
        auto modlid = iter->first; //uuid
        iter = used.count(modlid) == 0 ? node.CELL_SCALE.erase(iter) : std::next(iter);
      }
      i++;
      newellg.set_star_from_pose(); //only found or missing
    }
    //pathlog is retained for tracking logfiles
    PATHWAY = newpathway; //swap the pathway back to the one that it was on input, tracking cca NOT the solutions
    return filepathway; //we can do something with this to track the solutions separately
  }

  void
  DagDatabase::append_parse_card(FileSystem DAGFILE)
  {
    std::ifstream infile(DAGFILE.fstr().c_str(), std::ios::in | std::ios::binary);
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    std::string cards = buffer.str();

    //force the first line of the new file to be a node separator
    //so that the new node is generated for append
    //rather than just taking the last node and overwriting with first of new
    if (NODES.size() and cards.size() and cards.find(NODE_SEPARATOR())!=0)
      cards = NODE_SEPARATOR() + "\n" + cards;
    append_parse_cards_raw(cards);
    infile.close();
  }

  void
  DagDatabase::append_parse_cards(std::string cards)
  {
    if (NODES.size() and cards.size() and cards.find(NODE_SEPARATOR())!=0)
      cards = NODE_SEPARATOR() + "\n" + cards;
    append_parse_cards_raw(cards);
  }

  void
  DagDatabase::read_file(std::string DAGFILE)
  {
    clear_cards();
    SetFileSystem(DAGFILE);
    std::ifstream infile(DAGFILE.c_str(), std::ios::in | std::ios::binary);
    std::stringstream buffer;
    buffer << infile.rdbuf();  // Read file content into the stringstream
    std::string cards = buffer.str();
    append_parse_cards_raw(cards);
    infile.close();
  }

  void
  DagDatabase::node_unparse_card(const dag::Node& node,std::ostream& ss) const
  {
    //unparses node to ss
    //does not unparse the ENTRY part of the node[ this has to be done from DagDatabase
    auto text = node_unparse(node);
    for (auto item : text)
    {
      if (item.first.size())
      {
        char ch = item.first[0];
        item.first.erase(0,1);
        if (ch == '-')
          ss << NODE_SCOPE() << " node " << item.first << " ";
        else if (ch == '_')
          ss << NODE_SCOPE() << " node " << item.first << " " << item.second << std::endl;
        else if (ch == '=')
          ss << " " << item.first << " " << item.second << std::endl;
        else if (ch == ' ')
          ss << " " << item.first << " " << item.second;
        else
          assert(false);
      }
    }
  }

  void
  DagDatabase::node_unparse_html(const dag::Node& node,std::ostream& ss,bool header) const
  {
    //unparses node to ss
    //does not unparse the ENTRY part of the node[ this has to be done from DagDatabase
    auto text = node_unparse(node,true); //html flag
    std::string row1,row2,row3;
    std::set<std::string> disallowed = { "annotation","unitcell","spacegroup" };
    row1 += "<tr> <th>identifier</th>";
    row1 += "<th>tag</th>";
    row1 += "<th>poses</th>";
    row2 += "<tr> <td id=identifier>"+node.TRACKER.ULTIMATE.id_unparse()+"</td> ";
    row2 += "<td id=tag>"+node.TRACKER.ULTIMATE.tag_unparse()+"</td> ";
    row2 += "<td id=poses>"+itos(node.POSE.size())+"</td> ";
    for (auto item : text)
    {
      if (item.first.size())
      { // all the simple ones
        char ch = item.first[0];
        item.first.erase(0,1);
        if (ch == '_' and disallowed.count(item.first)==0) //because it is long
        {
          hoist::replace_all(item.first,"_"," ");
          row1 += "<th>" + item.first + "</th> ";
          std::string rounded = item.second;
          if (item.first != "parents") //because stod removes whitespaces, and parents is "int int"
            try { rounded = dtoss(std::stod(rounded),2); } catch(...) {}
          row2 += "<td id=\"" + item.first + "\"> " + rounded+ "</td> ";
        }
      }
    }
    row1 += " </tr>";
    row2 += " </tr>";
    if (header)
      ss << row1 << std::endl;
    else
      ss << row2 << std::endl;
  }

  std::vector<std::pair<std::string,std::string>>
  DagDatabase::node_unparse(const dag::Node& node,bool html) const
  {
    //NOTE TRACKER is unparsed separately in DagDatabase
    int precision(6);
    //start with "-" add "scope node" or start bracket
    //start with "=" add a return since last of the group
    //start with "_" add scope node and a return since last of the group
    //blank,(blank) is end bracket
    //start with space for the rest
    std::vector<std::pair<std::string,std::string>> text;
    //order here important for graph analysis
    text.push_back({"_zscore",dtoss(node.ZSCORE,precision)}); //at the top for fastest parsing
    text.push_back({"_percent",dtoss(node.PERCENT,precision)}); //at the top for fastest parsing
    text.push_back({"_cellz",dtoss(node.CELLZ,precision)}); //at the top for fastest parsing
    //take z-score over rfactor
    //if (node.RFACTOR)
    text.push_back({"_rfactor",dtoss(node.RFACTOR,precision)}); //at the top for fastest parsing
    //top 6 lines read only for graph analysis parsing
    if (node.LLG)
      text.push_back({"_llg",dtoss(node.LLG,precision)});
    if (node.FSS != '?')
      text.push_back({"_fss",otos(node.FSS)});
    if (node.MAPLLG)
      text.push_back({"_mapllg",dtoss(node.MAPLLG,precision)});
    if (node.REFLID.is_set_valid())
    {
      text.push_back({"-hklin",""});
      text.push_back({" id",node.REFLID.id_unparse()});
      text.push_back({"=tag",node.REFLID.tag_unparse()});
    }
    text.push_back({"",""});
    std::map<std::string,int> ordered;
    for (auto item : node.COMPOSITION)
      ordered[item.first] = item.second;
    for (auto item : ordered)
    {
      text.push_back({"-composition",""});
      text.push_back({" element",stoq(item.first)});
      text.push_back({"=number",itos(item.second)});
      text.push_back({"",""});
    }
    if (node.ANNOTATION.size())
      text.push_back({"_annotation",stoq(node.ANNOTATION)});
    text.push_back({"_spacegroup",stoq(node.HALL)}); //no need to quote, skip_line
    text.push_back({"_hermann_mauguin",stoq(SpaceGroup(node.HALL).CCP4)});
    text.push_back({"_unitcell", dvtos(node.CELL.parameters(),precision)});
    text.push_back({"_tncs_order",itos(node.TNCS_ORDER)});
    if (node.TNCS_VECTOR != dvect3(0,0,0))
      text.push_back({"_tncs_vector",dvtos(node.TNCS_VECTOR,6,4)});
    //for below, ? could be written as None but defaults to None
    if (node.TNCS_PRESENT_IN_MODEL != '?')
      text.push_back({"_tncs_present_in_model",otos(node.TNCS_PRESENT_IN_MODEL)});
    if (node.TNCS_INDICATED != '?')
      text.push_back({"_tncs_indicated",otos(node.TNCS_INDICATED)});
    if (node.TWINNED != '?')
      text.push_back({"_twinned",otos(node.TWINNED)});
    if (node.TWINNED == 'Y' and (node.TWINOP != "" or html)) //mixed list, html flag required
      text.push_back({"_twinop",node.TWINOP});
    if (node.PACKS != '?' or html) ///mixed list, html flag required
      text.push_back({"_packs",chtos(node.PACKS)});
    if ((node.PACKS != '?') or html)  //mixed list, html flag required, print even if 0
      text.push_back({"_clash_worst",dtos(node.CLASH_WORST)});
    if (node.SPECIAL_POSITION != '?')
      text.push_back({"_special_position",otos(node.SPECIAL_POSITION)});
    if (node.SPANS != '?' or html) //mixed list, html flag required
      text.push_back({"_spans",otos(node.SPANS)});
    if (node.INFORMATION_CONTENT)
      text.push_back({"_information_content",dtoss(node.INFORMATION_CONTENT,precision)});
    if (node.EXPECTED_INFORMATION_CONTENT)
      text.push_back({"_expected_information_content",dtoss(node.EXPECTED_INFORMATION_CONTENT,precision)});
    if (node.SAD_INFORMATION_CONTENT)
      text.push_back({"_sad_information_content",dtoss(node.SAD_INFORMATION_CONTENT,precision)});
    if (node.SAD_EXPECTED_LLG)
      text.push_back({"_sad_expected_llg",dtoss(node.SAD_EXPECTED_LLG,precision)});
    if (node.PARENTS.size())
      text.push_back({"_parents",std::to_string(node.PARENTS[0])+" "+std::to_string(node.PARENTS[1])});
    if (node.RELLG)
      text.push_back({"_rellg",dtoss(node.RELLG,precision)});
    if (node.SIGNAL_RESOLUTION)
      text.push_back({"_signal_resolution",dtoss(node.SIGNAL_RESOLUTION,precision)});
    if (node.SIGNAL_RESOLUTION_PHASED)
      text.push_back({"_signal_resolution_phased",dtoss(node.SIGNAL_RESOLUTION_PHASED,precision)});
    if (node.SEEK_COMPONENTS)
      text.push_back({"_seek_components",itos(node.SEEK_COMPONENTS)});
    if (node.SEEK_RESOLUTION)
      text.push_back({"_seek_resolution",dtoss(node.SEEK_RESOLUTION,precision)});
    if (node.SEEK_RESOLUTION_PHASED)
      text.push_back({"_seek_resolution_phased",dtoss(node.SEEK_RESOLUTION_PHASED,precision)});
    if (node.SEEK_COMPONENTS_ELLG)
      text.push_back({"_seek_components_ellg",dtoss(node.SEEK_COMPONENTS_ELLG,precision)});
    if (node.SEEK_COMPLETE)
      text.push_back({"_seek_complete",btos(node.SEEK_COMPLETE)});
    if (node.DATA_SCALE != 1)
      text.push_back({"_data_scale",dtoss(node.DATA_SCALE,precision)});
    {
      int npose(node.POSE.size());
      for (int p = 0; p < npose; p++)
      {
        const dag::Pose& pose = node.POSE[p];
        text.push_back({"-pose",""});
        text.push_back({" id",pose.IDENTIFIER.id_unparse()});
        text.push_back({" tag",pose.IDENTIFIER.tag_unparse()});
        text.push_back({" euler",dvtoss(pose.EULER,precision)});
        text.push_back({" rmat",dmtoss(pose.SHIFT_MATRIX,precision)});
        text.push_back({" rxyz",dvtoss(pose.ORTHR,precision)});
        text.push_back({" fractional",dvtoss(pose.FRACT,precision)});
        text.push_back({" shift_orthogonal", dvtoss(pose.SHIFT_ORTHT,precision)});
        text.push_back({" txyz",dvtoss(pose.ORTHT,precision)});
        text.push_back({" bfactor",dtoss(pose.BFAC,precision)});
        text.push_back({" m",itos(pose.MULT)});
        text.push_back({" tncs_group",ivtos(pose.TNCS_GROUP)});
        text.push_back({" clash",ivtos(pose.CLASH.as_vector())});
        text.push_back({" aellg",dtoss(pose.AELLG,precision)});
        text.push_back({"=tfz",dtoss(pose.TFZ,precision)});
        text.push_back({"",""});
      }
    }
    const auto& next = node.NEXT;
    if (next.IDENTIFIER.is_set_valid() and next.PRESENT)
    {
      text.push_back({"-next",""});
      text.push_back({" id",next.IDENTIFIER.id_unparse()});
      text.push_back({" tag",next.IDENTIFIER.tag_unparse()});
      text.push_back({" euler",dvtoss(next.EULER,precision)});
      text.push_back({" rmat",dmtoss(next.SHIFT_MATRIX,precision)});
      text.push_back({" rxyz",dvtoss(next.ORTHR,precision)});
      text.push_back({" translated",btos(next.TRANSLATED)});
      text.push_back({" fractional",dvtoss(next.FRACT,precision)});
      text.push_back({" shift_orthogonal",dvtoss(next.SHIFT_ORTHT,precision)});
      text.push_back({"=tfz",dtoss(next.TFZ,precision)});
      text.push_back({"",""});
      for (int g = 0; g < next.ngyre(); g++)
      { //these are entered on a separate line even though they are members of next
        const dag::Gyre& gyre = next.GYRE[g];
        text.push_back({"-gyre",""});
        text.push_back({" id",gyre.IDENTIFIER.id_unparse()});
        text.push_back({" tag",gyre.IDENTIFIER.tag_unparse()});
        text.push_back({" rxyz",dvtoss(gyre.ORTHR,precision)});
        text.push_back({" txyz",dvtoss(gyre.ORTHT,precision)});
        text.push_back({"=rmat",dmtoss(gyre.SHIFT_MATRIX,precision)});
        text.push_back({"",""});
      }
    }
    {
      int nseek(node.SEEK.size());
      for (int s = 0; s < nseek; s++)
      {
        const dag::Seek& seek = node.SEEK[s];
        text.push_back({"-seek",""});
        text.push_back({" id",seek.IDENTIFIER.id_unparse()});
        text.push_back({" tag",seek.IDENTIFIER.tag_unparse()});
        text.push_back({"=star",chtos(seek.STAR)});
        text.push_back({"",""});
      }
    }
    {
      int nhold(node.HOLD.size());
      for (int h = 0; h < nhold; h++)
      {
        const dag::Hold& hold = node.HOLD[h];
        text.push_back({"-hold",""});
        text.push_back({" id",hold.IDENTIFIER.id_unparse()});
        text.push_back({"=tag",hold.IDENTIFIER.tag_unparse()});
        text.push_back({"",""});
      }
    }
    const dag::Full& full = node.FULL;
    if (full.IDENTIFIER.is_set_valid() and full.PRESENT)
    {
      text.push_back({"-full",""});
      text.push_back({" present",btos(full.PRESENT)});
      text.push_back({" id",full.IDENTIFIER.id_unparse()});
      text.push_back({" tag",full.IDENTIFIER.tag_unparse()});
      text.push_back({"=fs",dtoss(full.FS,precision)});
      text.push_back({"",""});
    }
    const dag::Part& part = node.PART;
    if (part.IDENTIFIER.is_set_valid() and part.PRESENT)
    {
      text.push_back({"-part",""});
      text.push_back({" present",btos(part.PRESENT)});
      text.push_back({" id",part.IDENTIFIER.id_unparse()});
      text.push_back({"=tag",part.IDENTIFIER.tag_unparse()});
      text.push_back({"",""});
    }
    {
      mapnode_t ordered;
      for (auto item : node.DRMS_SHIFT)
        ordered[item.first] = item.second;
   //   phaser_assert(node.DRMS_SHIFT.size() == node.VRMS_VALUE.size());
   //   phaser_assert(node.DRMS_SHIFT.size() == node.CELL_SCALE.size());
      for (auto item : ordered)
      {
        auto id = item.first;
        auto drms_shift = node.DRMS_SHIFT.at(id).VAL;
        if (!node.VRMS_VALUE.count(id))
          throw Error(err::DEVELOPER,"vrms_shift id fail: " + id.str());
        phaser_assert(node.VRMS_VALUE.count(id));
        auto vrms_value = node.VRMS_VALUE.at(id).VAL;
        phaser_assert(node.CELL_SCALE.count(id));
        auto cell_scale = node.CELL_SCALE.at(id).VAL;
        text.push_back({"-variance",""});
        text.push_back({" id",id.id_unparse()});
        text.push_back({" tag",id.tag_unparse()});
        text.push_back({" drms",dtoss(drms_shift,precision)});
        text.push_back({" vrms",dtoss(vrms_value,precision)});
        text.push_back({"=cell",dtoss(cell_scale,precision)});
        text.push_back({"",""});
      }
    }
    auto& sadsigmaa = node.SADSIGMAA;
    if (sadsigmaa.size())
    {
      for (int i = 0; i < sadsigmaa.MIDSSQR.size(); i++)
      {
        text.push_back({"-sigmaa",""});
        text.push_back({" midssqr",dtoss(sadsigmaa.MIDSSQR[i],precision)});
        text.push_back({" sigman",dtoss(sadsigmaa.SigmaN_bin[i],precision)});
        text.push_back({" sigmap",dtoss(sadsigmaa.SigmaP_bin[i],precision)});
        text.push_back({" sigmaq",dtoss(sadsigmaa.SigmaQ_bin[i],precision)});
        text.push_back({" sigmah",dtoss(sadsigmaa.SigmaH_bin[i],precision)});
        text.push_back({" absrhoff",dtoss(sadsigmaa.absrhoFF_bin[i],precision)});
        text.push_back({" sigmapp",ctos(sadsigmaa.sigmaPP_bin[i],precision,false)});
        text.push_back({" sigmaqq",ctos(sadsigmaa.sigmaQQ_bin[i],precision,false)});
        text.push_back({" sigmahh",ctos(sadsigmaa.sigmaHH_bin[i],precision,false)});
        text.push_back({" fdelta",dtoss(sadsigmaa.fDelta_bin[i],precision)});
        text.push_back({"=rhopm",dtoss(sadsigmaa.rhopm_bin[i],precision)});
        text.push_back({"",""});
      }
    }
    return text;
  }


  void
  DagDatabase::unparse_card()
  {
    std::stringstream ss;
    //store the components for unparsing that do not change
    std::map<Identifier,std::string> entries;
    for (const auto& entry : ENTRIES)
    { //unparse all entries to node, overkill but much faster on write
      entries[entry.first] = entry_unparse_card(entry.second);
    }
    std::set<std::string> reflids;
    for (const auto& reflid : REFLIDS)
    { //unparse all entries to node, overkill but much faster on write
      if (reflid.second.is_set_valid())
      reflids.insert(reflid_unparse_card(reflid.second));
    }
    //now unparse the nodes
    for (int i = 0; i < NODES.size(); i++)
    {
      auto& node = NODES[i];
      ss << edge_unparse_card(PATHWAY,"pathway");
      ss << edge_unparse_card(PATHLOG,"pathlog");;
      ss << edge_unparse_card(node.TRACKER,"tracker");
      node_unparse_card(node,ss);
      auto used_modlid = node.used_modlid();
      for (auto e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e;
      ss << NODE_SEPARATOR() << std::endl;
    }
    std::fstream fsm;
    this->create_directories_and_stream(fsm);
    fsm << ss.str();
    fsm.close();
  }

  void
  DagDatabase::unparse_card_node(FileSystem DAGFILE,int i)
  {
    std::fstream ss;
    DAGFILE.create_directories_and_stream(ss);
    //store the components for unparsing that do not change
    auto& node = NODES[i];
    auto used_modlid = node.used_modlid();
    std::map<Identifier,std::string> entries;
    for (const auto& entry : ENTRIES)
    { //unparse all entries to node, overkill but much faster on write
      if (used_modlid.count(entry.first))
        entries[entry.first] = entry_unparse_card(entry.second);
    }
    std::set<std::string> reflids;
    for (const auto& reflid : REFLIDS)
    { //unparse all entries to node, overkill but much faster on write
      if (reflid.second.is_set_valid())
      reflids.insert(reflid_unparse_card(reflid.second));
    }
    //now unparse the nodes
    {
      ss << edge_unparse_card(PATHWAY,"pathway");
      ss << edge_unparse_card(PATHLOG,"pathlog");;
      ss << edge_unparse_card(node.TRACKER,"tracker");
      node_unparse_card(node,ss);
      for (auto e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e;
      ss << NODE_SEPARATOR() << std::endl;
    }
    ss.close();
  }

  void
  DagDatabase::unparse_html(FileSystem DAGFILE)
  {
    std::fstream ss;
    DAGFILE.create_directories_and_stream(ss);
    //store the components for unparsing that do not change
    std::map<Identifier,std::string> entries;
    //add some info for the pyvis output to the comments in the html file, for extraction
    ss << "<!-- " << PATHWAY.ULTIMATE.string() << " " << PATHWAY.ULTIMATE.tag() << " -->" <<std::endl;
    ss << "<!-- " << PATHWAY.PENULTIMATE.string() << " " << PATHWAY.PENULTIMATE.tag() << " -->" <<std::endl;
    ss << "<!-- " << PATHLOG.ULTIMATE.string() << " " << PATHLOG.ULTIMATE.tag() << " -->" <<std::endl;
    ss << "<!-- " << PATHLOG.PENULTIMATE.string() << " " << PATHLOG.PENULTIMATE.tag() << " -->" <<std::endl;
    if (NODES.size())
    {
      auto& node = NODES[0]; //tracker is just any unique hash for the graph
      ss << "<!-- " << node.TRACKER.ULTIMATE.string() << " " << node.ZSCORE << " " << node.RFACTOR << " -->" <<std::endl;
    }
    else ss << "<!-- 0 0 0 -->" <<std::endl;
    //very long list of the tracker nodes, for fast read and lookup
    ss << "<!-- ";
    for (int n = 0; n < NODES.size(); n++)
        ss << NODES[n].TRACKER.ULTIMATE.string() << " ";
    ss << "-->" <<std::endl;
    //end of comments for parsing
//    if (ENTRIES.size())
//    {
//      ss << "<h4>Entries</h4>" << std::endl;
//      ss << "<table border=\"1\">" << std::endl;
//      ss << ENTRIES.begin()->second.unparse_html(true);
//    }
//    for (const auto& entry : ENTRIES)
//    {
//      ss << entry.second.unparse_html(false);
//    }
//    if (ENTRIES.size())
//      ss << "</table>" << std::endl;
    int maxnodes = std::min(10,static_cast<int>(NODES.size()));
    ss << "<h4>Top Nodes (" << maxnodes << ") for " + PATHWAY.ULTIMATE.str()+"</h4>" << std::endl;
    ss << "<table border=\"1\">" << std::endl;
    //now unparse the nodes
    if (NODES.size())//header true,any node will do
      node_unparse_html(NODES[0],ss,true);
    for (int i = 0; i < maxnodes; i++)
      node_unparse_html(NODES[i],ss,false); //simple row
    ss << "</table>" << std::endl;
    ss << std::string("<p>Total "+itos(NODES.size())+" nodes</p>") << std::endl;
    ss.close();
  }

  std::string
  DagDatabase::cards()
  {
    std::stringstream ss;
    //store the components for unparsing that do not change
    std::map<Identifier,std::string> entries;
    for (const auto& entry : ENTRIES)
    { //unparse all entries to node, overkill but much faster on write
      entries[entry.first] = entry_unparse_card(entry.second);
    }
    std::set<std::string> reflids;
    for (const auto& reflid : REFLIDS)
    { //unparse all entries to node, overkill but much faster on write
      if (reflid.second.is_set_valid())
      reflids.insert(reflid_unparse_card(reflid.second));
    }
    //now unparse the nodes
    for (int i = 0; i < NODES.size(); i++)
    {
      auto& node = NODES[i];
      ss << edge_unparse_card(PATHWAY,"pathway");
      ss << edge_unparse_card(PATHLOG,"pathlog");
      ss << edge_unparse_card(node.TRACKER,"tracker");
      node_unparse_card(node,ss);
      auto used_modlid = node.used_modlid();
      for (const auto& e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e;
      ss << NODE_SEPARATOR() << std::endl;
    }
    return ss.str();
  }

  std::string
  DagDatabase::node_cards(int i)
  {
    std::stringstream ss;
    //store the components for unparsing that do not change
    std::map<Identifier,std::string> entries;
    for (const auto& entry : ENTRIES)
    { //unparse all entries to node, overkill but much faster on write
      entries[entry.first] = entry_unparse_card(entry.second);
    }
    std::set<std::string> reflids;
    for (const auto& reflid : REFLIDS)
    { //unparse all entries to node, overkill but much faster on write
      if (reflid.second.is_set_valid())
      reflids.insert(reflid_unparse_card(reflid.second));
    }
    //now unparse the nodes
    auto& node = NODES[i];
    ss << edge_unparse_card(PATHWAY,"pathway");
    ss << edge_unparse_card(PATHLOG,"pathlog");
    ss << edge_unparse_card(node.TRACKER,"tracker");
    node_unparse_card(node,ss);
    auto used_modlid = node.used_modlid();
    for (const auto& e : used_modlid)
      ss << entries[e];
    for (const auto& e : reflids)
      ss << e;
    return ss.str();
  }

  af_string
  DagDatabase::node_cards_list()
  {
    af_string cards_list;
    std::stringstream ss;
    for (int i = 0; i < NODES.size(); i++)
    {
      auto& node = NODES[i];
      //store the components for unparsing that do not change
      std::map<Identifier,std::string> entries;
      for (const auto& entry : ENTRIES)
      { //unparse all entries to node, overkill but much faster on write
        entries[entry.first] = entry_unparse_card(entry.second);
      }
      std::set<std::string> reflids;
      for (const auto& reflid : REFLIDS)
      { //unparse all entries to node, overkill but much faster on write
        if (reflid.second.is_set_valid())
        reflids.insert(reflid_unparse_card(reflid.second));
      }
      //now unparse the nodes
      ss << edge_unparse_card(PATHWAY,"pathway");
      ss << edge_unparse_card(PATHLOG,"pathlog");
      ss << edge_unparse_card(node.TRACKER,"tracker");
      node_unparse_card(node,ss);
      auto used_modlid = node.used_modlid();
      for (const auto& e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e;
      cards_list.push_back(ss.str());
      ss.str(std::string()); //clear the stream
    }
    return cards_list;
  }

  af_string DagDatabase::logEntries()
  {
    af_string output;
    typedef mapentry_t::const_iterator citer; //can't copy, problen with Entry destructor
    for (citer iter = ENTRIES.cbegin(); iter != ENTRIES.cend() ; iter++)
    {
      auto& ENTRY = iter->second;
      auto txt = ENTRY.logEntry();
      for (auto line : txt)
        output.push_back(line);
      output.push_back("------");
    }
    return output;
  }

  af_string DagDatabase::logReflids()
  {
    af_string output;
    for (auto& reflid : REFLIDS)
    {
      std::string line = "Reflection id "+reflid.second.id_unparse()+" tag "+reflid.second.tag_unparse();
      output.push_back(line);
    }
    output.push_back("------");
    return output;
  }

  std::vector<std::pair<std::string,std::string>>
  DagDatabase::entry_unparse(const dag::Entry& entry) const
  {
    int precision(6);
    std::vector<std::pair<std::string,std::string>> text;
    text.push_back({" id ",entry.DOGTAG.id_unparse()});
    text.push_back({" tag ",entry.DOGTAG.tag_unparse()}); //not quoted
    text.push_back({" atom ",btos(entry.ATOM)});
    text.push_back({" helix ",btos(entry.HELIX)});
    text.push_back({" map ",btos(entry.MAP)});
    text.push_back({" scattering ",itos(entry.SCATTERING)});
    text.push_back({" mw ",dtoss(entry.MOLECULAR_WEIGHT,2)});
    text.push_back({" pt ",dvtoss(entry.PRINCIPAL_TRANSLATION,precision)});
    text.push_back({" pr ",dmtoss(entry.PRINCIPAL_ORIENTATION,precision)});
    text.push_back({" radius ",dtoss(entry.MEAN_RADIUS,2)});
    text.push_back({" volume ",dtoss(entry.VOLUME,2)});
    text.push_back({" centre ",dvtoss(entry.CENTRE,precision)});
    text.push_back({" extent ",dvtoss(entry.EXTENT,precision)});
    text.push_back({" pg_symbol ",stoq(entry.POINT_GROUP_SYMBOL)});
    //point group eulers has to be a single list of numbers that are parsed in groups of three
    //because we can't have an array of an array of (dvect3/anything) in the InputBase data structure
    sv_double eulers;
    for (int i = 0; i < entry.POINT_GROUP_EULER.size(); i++)
    {
      auto& pgeuler = entry.POINT_GROUP_EULER[i];
      eulers.push_back(pgeuler[0]);
      eulers.push_back(pgeuler[1]);
      eulers.push_back(pgeuler[2]);
    }
    sv_double vzero(3,0);
    if (!eulers.size()) eulers = vzero; //must be something to print out
    text.push_back({" pg_euler ",std::string(eulers != vzero ? dvtos(eulers) : "0 0 0")});
    text.push_back({" vrms ",dvtos(entry.VRMS_START,precision)});
    text.push_back({" drms ",dtoss(entry.DRMS_MIN,precision) + " " + dtoss(entry.DRMS_MAX,precision)});
    text.push_back({" hires ",dtoss(entry.HIRES,precision)});
    text.push_back({" lores ",dtoss(entry.LORES,precision)});
    text.push_back({" data ",entry.get_data_choice_selection_int()}); //highly compact, not readable
    return text;
  }

  std::string
  DagDatabase::reflid_unparse_card(const DogTag& entry) const
  {
    return NODE_SCOPE()+" reflections  id "+entry.id_unparse()+" tag "+entry.tag_unparse()+"\n";
  }

  std::string
  DagDatabase::entry_unparse_card(const dag::Entry& entry) const
  {
    std::string cards;
    cards += NODE_SCOPE() +  " entry ";
    auto text = entry_unparse(entry);
    for (auto item : text)
      cards += item.first + item.second;
    cards += "\n";
    return cards;
  }

  std::string
  DagDatabase::entry_unparse_html(const dag::Entry& entry,bool header) const
  {
    std::string row1;
    std::string row2;
    auto text = entry_unparse(entry);
    row1 += "<tr>";
    row2 += "<tr>";
    for (auto item : text)
    {
      hoist::replace_all(item.first,"_"," ");
      row1 += "<td>" + item.first + "</td>";
      std::string rounded = item.second;
      try { rounded = dtoss(std::stod(rounded),2); } catch(...) {}
      row2 += "<td>" + rounded+ "</td>";
    }
    row1 += "</tr>\n";
    row2 += "</tr>\n";
    if (header) return row1;
    return row2;
  }

  std::vector<std::pair<std::string,std::string>>
  DagDatabase::edge_unparse(const dag::Edge& edge,bool last) const
  {
    std::vector<std::pair<std::string,std::string>> text;
    if (last)
    {
      text.push_back({" id ",edge.ULTIMATE.id_unparse()});
      text.push_back({" tag ",edge.ULTIMATE.tag_unparse()});
    }
    else
    {
      text.push_back({" id ",edge.PENULTIMATE.id_unparse()});
      text.push_back({" tag ",edge.PENULTIMATE.tag_unparse()});
    }
    return text;
  }

  std::string
  DagDatabase::edge_unparse_card(const dag::Edge& edge,std::string key) const
  {
    std::string cards;
    {
    cards += NODE_SCOPE() +  " " + key + " ultimate ";
    auto text = edge_unparse(edge,true);
    for (auto item : text)
      cards += item.first + item.second;
    cards += "\n";
    }
    {
    cards += NODE_SCOPE() +  " " + key + " penultimate ";
    auto text = edge_unparse(edge,false);
    for (auto item : text)
      cards += item.first + item.second;
    cards += "\n";
    }
    return cards;
  }

}} //phasertng
