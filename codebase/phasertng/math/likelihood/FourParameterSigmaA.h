//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_FourParameterSigmaA_class__
#define __phasertng_FourParameterSigmaA_class__

namespace phasertng {

  /*
    Four-parameter Sigma(A) curve given by
    Sigma(A) = sqrt( fp*(1-fs*exp(-Bs*stlsq)) ) *
       exp(-8*Pi*Pi/3 * Sigma(rmsx)^2 * stlsq)
    where fp = fraction of ordered protein in model
          fs = fraction of cell occupied by solvent
          Bs = B-factor to smear disordered solvent (typically 50-200)
          rmsx = e.s.d. of atoms in model
    stlsq = Ssqr/4 (because Ssqr=1/d**2)
  */

  class FourParameterSigmaA
  {
    //unreasonably high RMSD (e.g 28) gives FourParameterSigmaA=0
    //sigmaa=0 causes failure elsewhere, so set a minimum value here as failsafe
    //note that it is better to catch the unreasonable RMSD first
    double SIGA_MIN = 0.001;
    double RMSD = 0;
    double FSOL = 1;
    double BSOL = 0;
    double BMIN = 0;
    double TWOPISQ_ON_THREE = 6.579736267392905; //double is 15 decimal places

    public:
      FourParameterSigmaA(
             const double RMSD_=0,
             const double FSOL_=1,
             const double BSOL_=0,
             const double BMIN_=0,
             const double SIGA_MIN_=1.0e-06) :
         RMSD(RMSD_),
         FSOL(FSOL_),
         BSOL(BSOL_),
         BMIN(BMIN_),
         SIGA_MIN(SIGA_MIN_)
      { }

      //allow reset of RMSD
      void set_rmsd(const double& RMSD_) { RMSD = RMSD_; }
      bool is_default() { return (!RMSD && !BSOL && !BMIN && FSOL == 1); }

    public:
      double get(const double& SSQR) const;
      double worst_rms(const double config_hires);
      double pow2(const double x) { return x*x; }
  };

} //phasertng

#endif
