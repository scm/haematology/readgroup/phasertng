#ifndef __phasertng_dag_xyzRotMatDeg__class__
#define __phasertng_dag_xyzRotMatDeg__class__
#include <complex>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>

using namespace scitbx;
typedef std::complex<double> cmplex;
typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;

namespace phasertng {
namespace dag {

  class xyzRotMatDeg
  {
    public:
      xyzRotMatDeg() {}
      dmat33 perturbed(const dvect3&) const;
  };
}}

#endif
