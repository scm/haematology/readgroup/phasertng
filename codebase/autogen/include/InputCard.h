#ifndef __phasertng_inputcard_class__
#define __phasertng_inputcard_class__
#include <phasertng/main/InputBase.h>

namespace phasertng {

class InputCard: public InputBase
{
  friend class Suite;

  public:
  InputCard();
  ~InputCard() {}

  bool parse(std::string,bool ignore_unknown_keys=false);
  void parse_multi(const std::string& a, const std::string& b, const std::string& c, bool iuk=false)
   { /*clear first, externally*/ parse(a,iuk); parse(b,iuk); parse(c,iuk); }
  void analyse();
  std::string header() const;
  std::set<std::string> python_modes() const;

  private:
  void add_keywords();
  void add_linkKeys();
  void add_phil_input_parameters();
  void add_phil_result_parameters();
};

}
#endif
