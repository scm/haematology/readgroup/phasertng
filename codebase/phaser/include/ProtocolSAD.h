//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_PROTOCOL_SAD_CLASS__
#define __PHASER_PROTOCOL_SAD_CLASS__

namespace phaser {

class ProtocolSAD
{
  public:
    bool FIX_K,FIX_B;
    bool FIX_XYZ,FIX_OCC,FIX_BFAC,FIX_FDP;
    bool FIX_SA,FIX_SB,FIX_SP,FIX_SD;
    bool FIX_PARTK,FIX_PARTU;

    void off()
    {
      FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_OCC = FIX_SA = FIX_SB = FIX_SP = FIX_SD = FIX_PARTK = FIX_PARTU = true;
    }

    void var_only()
    {
      FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_OCC = FIX_PARTK = FIX_PARTU = true;
      FIX_SA = FIX_SB = FIX_SP = FIX_SD = false;
    }

    bool is_off() const
    { return ((FIX_XYZ && FIX_BFAC && FIX_FDP && FIX_OCC && FIX_SA && FIX_SB && FIX_SP && FIX_SD && FIX_PARTK && FIX_PARTU)); }

};

typedef ProtocolSAD* protocolPtr;

} //phaser

#endif
