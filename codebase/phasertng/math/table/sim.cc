#include <phasertng/math/table/sim.h>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace table {

constexpr const double table::sim::simTable1[4097][2];
constexpr const double table::sim::simTable2[4097][2];
constexpr const double table::sim::simInvTable[4097][2];
  sim::sim()
  {
    argfac1 = double(tbllen/argcut1);
    argfac2 = double(tbllen/argcut2);
    invArgFac = double(tbllen*argcut2);
  }

  double
  sim::get(double arg) const
  {
    if (arg < argcut1)
    {
      double fracTableArg = argfac1*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return sim::simTable1[index][0] +
             (fracTableArg-index)*sim::simTable1[index][1];
    }
    else if (arg < argcut2)
    {
      double fracTableArg = argfac2*arg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return sim::simTable2[index][0] +
             (fracTableArg-index)*sim::simTable2[index][1];
    }
    else
    {
      double invArg(1./arg);
      double fracTableArg = invArgFac*invArg;
      int index = static_cast<int>(fracTableArg);
      phaser_assert(index < tbllen1);
      return std::sqrt(invArg)*(sim::simInvTable[index][0] +
                      (fracTableArg-index)*sim::simInvTable[index][1]);
    }
    return 0;
  }

  sim sim::inst_;
}}
