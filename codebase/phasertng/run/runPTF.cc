//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Phased_translation_function
  void Phasertng::runPTF()
  {
    //but the peak picking is done in the space group of the node
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_turn())
    throw Error(err::INPUT,"Dag not prepared with correct mode (rotation function failed?)");
    if (!REFLECTIONS->MAP)
    throw Error(err::INPUT,"Data is not a map");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}

    double io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    double resolution = DAGDB.WORK->search_resolution(io_hires);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    //can't use standard flags because reflections are wrong base type
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    //check that search_uuid is not an atom

    //calculate the default sampling
    double cluster_distance,sampling;
    {{
    std::tie(cluster_distance,sampling) = EntryHelper::clustdist(
      selected.STICKY_HIRES,
      false, //helix
      false, //coiled coil
      1.0, //helixfactor
      input.value__flt_number.at(".phasertng.phased_translation_function.sampling_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.phased_translation_function.sampling.").value_or_default());
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Sampling");
    logTab(where,"Sampling: " + dtos(sampling,5,2) + " Angstroms");
    logTab(where,"Cluster Distance: " + dtos(cluster_distance,5,2) + " Angstroms");
    logBlank(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    int nstart = DAGDB.size();
    if (input.value__boolean.at(".phasertng.phased_translation_function.add_pose_orientations.").value_or_default())
    {
      logUnderLine(where,"Add Pose Orientations");
      logTab(where,"Orientation list expanded from list of current poses");
      int before = DAGDB.size();
      auto txt = DAGDB.NODES.add_pose_orientations();
      logTab(where,"Number before addition: " + itos(before));
      logTab(where,"Number after addition:  " + itos(DAGDB.size()));
      if (txt.size())
      {
        out::stream where = out::VERBOSE;
        logTableTop(where,"Added Orientations");
        logTabArray(where,txt);
        logTableEnd(where);
      }
    }
    int nfinal = DAGDB.size();
    if (nfinal > nstart)
      logTab(out::LOGFILE,"Number of nodes expanded from " + itos(nstart) + " to " + itos(nfinal));
    else
      logTab(out::LOGFILE,"Number of nodes not expanded");
    }}

    //initialize tNCS parameters from reflections and calculate correction terms
    //PTF
    bool store_halfR(false);
    bool store_fullR(false);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    int io_nprint = input.value__int_number.at(".phasertng.phased_translation_function.maximum_printed.").value_or_default();
    int io_maxstored = input.value__int_number.at(".phasertng.phased_translation_function.maximum_stored.").value_or_default();
    double io_percent = input.value__percent.at(".phasertng.phased_translation_function.percent.").value_or_default();

    int numtfs = DAGDB.size();
    sv_string tfsg(numtfs);
    for (int i = 0; i < DAGDB.NODES.size(); i++)
     tfsg[i] = DAGDB.NODES[i].SG->CCP4;

    dag::NodeList output_dag;

    pod::parent_mean_sigma_top stats;
    out::stream where = out::VERBOSE;
    bool haveFpart = true; //phases
    logUnderLine(where,"Phased Translation Function Setup");
    DAGDB.restart(); //very important to call this to set WORK
    FastTranslationFunction trafun(
        REFLECTIONS.get(),
        selected.get_selected_array(),
        &epsilon
        );
    trafun.allocate_memory(REFLECTIONS.get(),sampling,haveFpart);
    logTabArray(where,trafun.logSetup());
    logTab(where,"Has partial structure: " + btos(haveFpart));
    logTab(where,"Gridding: " + ivtos(trafun.gridding));

    int n(1),N(DAGDB.size());
    DAGDB.restart();
    out::stream progress = out::LOGFILE;
    logUnderLine(progress,"Phased Translation Function");
    logProgressBarStart(progress,"Translation Functions",DAGDB.size(),1,false);
    while (!DAGDB.at_end())
    {
      std::string nN = nNtos(n++,N);
      dag::Node* work = DAGDB.WORK;
      {{
      out::stream where = out::VERBOSE;
      logUnderLine(where,"Phased Translation Function" + nN);
      //reflections stored as pointer, so below changes PTF reflections object
      bool changed = load_reflid_for_node(DAGDB.WORK->REFLID,where);
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
      if (changed or diffsg)
      {
        if (changed) logTab(where,"Reflections changed to " + DAGDB.WORK->REFLID.str() + " Z=" + dtos(REFLECTIONS->Z));
        double resolution = DAGDB.WORK->search_resolution(io_hires);
        auto seltxt = selected.set_resolution(
            REFLECTIONS->data_hires(),
            DAGDB.resolution_range(),
            resolution);
        if (changed) logTabArray(where,seltxt);
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        trafun.setup(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon
        );
        trafun.allocate_memory(REFLECTIONS.get(),sampling,haveFpart);
        logTabArray(where,trafun.logSetup());
        logTab(where,"Gridding: " + ivtos(trafun.gridding));
      }
      logTabArray(where,DAGDB.WORK->logGyre("Known components"));
      }}

      trafun.translation_function_data.clear();
      DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
      trafun.calculate_ptf(REFLECTIONS.get(),DAGDB.WORK);

      {{
      out::stream where = out::VERBOSE;
      logTab(where,"Mean and Standard Deviation");
      int p = DAGDB.NODES.llg_precision(true);
      int q = DAGDB.NODES.llg_width(true);
      trafun.statistics();
      logTab(where,snprintftos(
          "%*s %*s %*s %10s",
          q,"Mean",q,"Sigma",q,"Top","Top-Z"));
      logTab(where,snprintftos(
          "%*.*f %*.*f %*.*f %10.3f",
          q,p,trafun.f_mean,q,p,trafun.f_sigma,q,p,trafun.f_top,trafun.z_top));
      logBlank(where);
      (trafun.tf_max()->f == std::numeric_limits<double>::lowest()) ?
        logTab(where, "Current Max-Top = n/a "):
        logTab(where, "Current Max-Top = " + dtos(trafun.tf_max()->f,2));
      stats.mst[n] = dvect3(trafun.f_mean,trafun.f_sigma,trafun.f_top);
      }}

      //peak search from map, but only store the ones that are over the percent
      //the percent cutoff is used here as only the fast search has a mean and stddev
      //continuous shifts and score is slow if done on all peaks
      af_double histogram;
      {{
      out::stream where = out::VERBOSE;
      //AJM TODO check the application of the perturbations here,
      //for both true turn and just rotref
      dvect3 PT = DAGDB.WORK->NEXT.ENTRY->PRINCIPAL_TRANSLATION;
      //below makes sites wrt input
      af_double peak_heights; af_dvect3 peak_sites;
      histogram = trafun.peak_search(peak_heights,peak_sites);
      logEllipsisOpen(where,"Apply Mean Cutoff");
      trafun.apply_mean_cutoff(peak_heights,peak_sites);
      logEllipsisShut(where);
      int num_peaks(peak_sites.size());

      logEllipsisOpen(where,"Peak search and purge by percent");
      auto txt = trafun.apply_percent_cutoff(peak_heights,peak_sites,io_percent);
      trafun.continuous_shifts_and_store(peak_heights,peak_sites,DAGDB.WORK->NEXT.EULER,PT);
      logEllipsisShut(where);
      logTab(where,"Percent: "  + dtos(io_percent,5,1) + "%");
      logTabArray(where,txt);
      logTab(where,"Number before purge: " + itos(num_peaks));
      logTab(where,"Number after purge:  " + itos(trafun.translation_function_data.size()));
      logBlank(where);
      }}

      logHistogram(out::VERBOSE,histogram,20,true,"Phased Translation Function Peak Heights (Z)",0,0,false);

      if (trafun.count_sites() == 0)
      {
        logProgressBarNext(progress);
        continue;
      }

      if (trafun.tf_max()->newmax)
      {
        logProgressBarAgain(progress,"Best FSS=" + dtos(trafun.tf_max()->f,1) + " TFZ=" + dtos(trafun.tf_max()->z,1) + nN );
      }

      // trafun.full_sort(); //search puts them in order
      trafun.clustering_off(); //reset all to top

      {{
      out::stream where = out::VERBOSE;
      bool AllSites(false);
      logTableTop(where,"Phased Translation Function (before clustering)"+nN,false);
      logTab(where,("Background Poses: " + itos(DAGDB.NODES.number_of_poses())));
      logTabArray(where,trafun.logFastTable(io_nprint,AllSites));
      logTableEnd(where);
      }}

      if (input.value__boolean.at(".phasertng.phased_translation_function.write_maps.").value_or_default())
      {
        std::string cc = ntos(n,DAGDB.size());
        Identifier modlid = DAGDB.PATHWAY.ULTIMATE;
        FileSystem tfmap(DataBase(),modlid.FileName(cc+".translation_function.mtz"));
        logFileWritten(out::LOGFILE,WriteFiles(),"Translation Function",tfmap);
        if (WriteFiles()) trafun.WriteMap(tfmap);
      }

      //if the sampling is not inflated, then each point has no neighbours!!!
      {{
      out::stream where = out::VERBOSE;
      if (input.value__boolean.at(".phasertng.phased_translation_function.cluster.").value_or_default())
      {
        {{
        logEllipsisOpen(where,"Cluster Phased Translation Function Search Peaks");
        //if the sampling is not inflated, then each point has no neighbours!!!
        int cluster_back = input.value__int_number.at(".phasertng.phased_translation_function.cluster_back.").value_or_default();
        trafun.cluster(cluster_distance,cluster_back);
        logEllipsisShut(where);
        logEllipsisOpen(where,"Purge by cluster");
        int before = trafun.count_sites();
        trafun.select_topsites_and_erase();
        logEllipsisShut(where);
        logTab(where,"Number before purge: " + itos(before));
        logTab(where,"Number after purge:  " + itos(trafun.count_sites()));
        }}

        {{
        out::stream where = out::VERBOSE;
        bool AllSites(false);
        logTableTop(where,"Phased Translation Function (after clustering)"+nN,false);
        logTab(where,("Background Poses: " + itos(DAGDB.NODES.number_of_poses())));
        logTabArray(where,trafun.logFastTable(io_nprint,AllSites));
        logTableEnd(where);
        }}

      }
      else
      {
        logBlank(where);
        logTab(where,"No clustering of Phased Translation Function");
        logBlank(where);
      }
      }}

      {{
      out::stream where = out::VERBOSE;
      logEllipsisOpen(where,"Purge by maximum number");
      int before = trafun.count_sites();
      trafun.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
      logEllipsisShut(where);
      if (io_maxstored == 0)
      logTab(where,"Maximum stored: all");
      else
      logTab(where,"Maximum stored: " + itos(io_maxstored));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(trafun.count_sites()));
      }}

      {{
      out::stream where = out::VERBOSE;
      Loggraph loggraph;
      loggraph.title = "Top peaks in Phased Translation Function "+nN ;
      loggraph.scatter = true;
      loggraph.graph.resize(1);
      loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
      int i(1);
      for (auto& item : trafun.translation_function_data)
      {
        loggraph.data_text += itos(i++) + " " + dtos(item.value,10,2) + "\n";
      }
      logGraph(where,loggraph);
      }}

      {{
      out::stream where = out::VERBOSE;
      logEllipsisOpen(where,"Append to DAG");
      //add the growing list to the DAG, note that this will not be sorted overall
      DAGDB.WORK->convert_turn_to_curl();
      DAGDB.reset_work_entry_pointers(DataBase());
      for (auto& item : trafun.translation_function_data)
      {
        //no selection for top peaks, recore them all
        dag::Node next = *DAGDB.WORK; //deep copy so that the dag node is replicated
        next.MAPLLG = item.value; //fast search score FSS
        next.FSS = 'Y'; //fast search score FSS
        double zscore = (item.value - trafun.f_mean)/trafun.f_sigma;
        next.ZSCORE = zscore;
        next.NUM = n-1;
        double perc = 100.*(item.value-trafun.f_mean)/(trafun.tf_max()->f-trafun.f_mean);
        next.generic_float = perc;
        std::string annotation = dtoi(item.value)+"/"+dtoi(perc)+"%/"+dtos(std::floor(zscore),0)+"z";
        next.ANNOTATION += " PTF=" + annotation;
        //use PARENT at initiating rf only
        auto& curl = next.NEXT;
        const dmat33 PR = curl.ENTRY->PRINCIPAL_ORIENTATION;
        const dvect3 PT = curl.ENTRY->PRINCIPAL_TRANSLATION;
        dmat33 orthmat = next.CELL.orthogonalization_matrix();
        curl.SHIFT_ORTHT = orthmat*item.grid;
        curl.FRACT = dag::fract_wrt_mt_from_in(item.grid,curl.EULER,PT,next.CELL);
        output_dag.push_back(next); //pointer to last in list
      }
      logEllipsisShut(where);
      }}

      logTab(out::VERBOSE,"== End Phased Translation Function" + nN);
      logProgressBarNext(progress);
    } //dagdb
    logProgressBarEnd(progress);

    logUnderLine(out::LOGFILE,"Merged Results");

    if (stats.size() > 1)
    {
      out::stream where = out::LOGFILE;
      logTab(where,"Mean FSS: " + dtos(stats.merged_lowest_mean()));
      stats.calculate_histogram_mean();
      logHistogram(where,stats.histdat,10,false,"TF means",stats.histmin,stats.histmax);
      stats.calculate_histogram_sigma();
      logHistogram(where,stats.histdat,10,false,"TF sigmas",stats.histmin,stats.histmax);
    }

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by percent of FSS");
    int before = output_dag.size();
    double lowest_mean = stats.merged_lowest_mean();
    double top = output_dag.top_mapllg();
    double cut = output_dag.apply_partial_sort_and_percent_mapllg(io_percent,lowest_mean);
    logEllipsisShut(where);
    logTab(where,"Percent: "  + dtos(io_percent,5,1) + "%");
    logTab(where,"Top/Mean/Cutoff FSS: " + dtos(top,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
    logTab(where,"Mean FSS: " + dtos(lowest_mean));
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(output_dag.size()));
    }}

    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by maximum stored");
    int before = output_dag.size();
    output_dag.apply_partial_sort_and_maximum_stored_mapllg(io_maxstored);
    logEllipsisShut(where);
    if (io_maxstored == 0)
    logTab(where,"Maximum stored: all");
    else
    logTab(where,"Maximum stored: " + itos(io_maxstored));
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(output_dag.size()));
    }}

    {{
    out::stream where = out::LOGFILE;
    Loggraph loggraph;
    loggraph.title = "Top peaks in Phased Translation Function";
    loggraph.scatter = true;
    loggraph.graph.resize(1);
    loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
    int i(1);
    for (auto& item : output_dag)
    {
      loggraph.data_text += itos(i++) + " " + dtos(item.LLG,10,2) + "\n";
    }
    logGraph(where,loggraph);
    }}

    //just replace the node list
    DAGDB.NODES = output_dag;
    DAGDB.reset_entry_pointers(DataBase());

    {{
    int w = std::max(2,itow(io_nprint));
    out::stream where = out::LOGFILE;
    logTableTop(where,"Phased Translation Function (Final)");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int p = DAGDB.NODES.llg_precision(true);
    int q = DAGDB.NODES.llg_width(true);
    int cellw = REFLECTIONS->UC.width();
    if (DAGDB.size())
    {
      logTab(where,"#       Rank of the peak");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %5s %s  %s %*s %5s %7s  %*s %*s",
          w,"#","",
          dag::header_polar().c_str(),
          dag::header_ortht(cellw).c_str(),
          q,"FSS",
          "FSS%","TFZ",uuid_w,"modlid",uuid_w,"reflid"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      auto& curl = node->NEXT;
      logTab(where,snprintftos(
          "%*d %s  %s %*.*f %5.1f %7.2f  %*s %*s",
          w,i,
          dag::print_polar(curl.SHIFT_MATRIX).c_str(),
          dag::print_ortht(curl.SHIFT_ORTHT,cellw).c_str(),
          q,p,node->MAPLLG,
          node->generic_float, //100% for first in p1, otherwise value from tf
          node->ZSCORE,
          uuid_w,curl.IDENTIFIER.string().c_str(),
          uuid_w,node->REFLID.string().c_str()
          ));
      logTab(where,"---");
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    bool unique_search(true);
    for (auto& node : DAGDB.NODES)
    {
      auto& fnode = DAGDB.NODES.front();
      if (fnode.REFLID  != node.REFLID) unique_search = false;
      if (fnode.NEXT.IDENTIFIER != node.NEXT.IDENTIFIER) unique_search = false;
    }

    {{
    int w = itow(io_nprint);
    out::stream where = out::LOGFILE;
    logTableTop(where,"Phased Translation Function (Final)");
    logTab(where,("Background Poses: " + itos(DAGDB.NODES.number_of_poses())));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#       Rank of the peak");
      logTab(where,"Euler   R_z(gamma)R_y(beta)R_z(alpha)");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %7s %7s %7s  %7s %7s %7s %10s %7s  %*s %*s",
          w,"#",
          "Euler1","Euler2","Euler3","FracX","FracY","FracZ","FSS","TFZ",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      const auto& curl = node->NEXT;
      const dvect3 PT = curl.ENTRY->PRINCIPAL_TRANSLATION;
      dvect3  FRACT = dag::fract_wrt_in_from_mt(curl.FRACT,curl.EULER,PT,node->CELL); //for output
      logTab(where,snprintftos(
          "%-*i % 7.1f % 7.1f % 7.1f  % 7.4f % 7.4f % 7.4f % 10.*f %7.2f  %*s %*s",
          w,i,
          curl.EULER[0],curl.EULER[1],curl.EULER[2],
          FRACT[0],FRACT[1],FRACT[2],
          q,node->MAPLLG,
          node->ZSCORE,
          uuid_w,curl.IDENTIFIER.string().c_str(),
          uuid_w,node->REFLID.string().c_str()
          ));
      logTab(where,snprintftos(
          "%-*i {%+7.3f %+7.3f %+7.3f} {%+6.3f}",
          w,i,
          curl.ORTHR[0],curl.ORTHR[1],curl.ORTHR[2],
          node->DRMS_SHIFT[curl.IDENTIFIER].VAL
          ));
      logTab(where,"---");
      if (i == io_nprint)
      {
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(DAGDB.size()-io_nprint) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    {{
    int w = std::max(4,itow(numtfs)); //#SET=4
    out::stream where = out::SUMMARY;
    logTableTop(where,"Phased Translation Function Summary");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()));
    DAGDB.restart(); //very important to call this to set WORK
    logTab(where,"Last placed: " + DAGDB.WORK->NEXT.IDENTIFIER.str());
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"There " + std::string(numtfs==1?"is ":"are ") + itos(numtfs) + " translation function" + std::string(numtfs==1?"":"s"));
    if (DAGDB.size())
    {
      logTab(where,snprintftos(
          "%*s %10s %7s %10s %7s %10s %7s %-13s  %*s %*s",
          w,"#SET","Top","(Z)","Second","(Z)","Third","(Z)","Space Group",
          unique_search ? 0:uuid_w,unique_search ? "":"modlid",
          unique_search ? 0:uuid_w,unique_search ? "":"reflid"));
    }
    for (int itf = 0; itf < numtfs; itf++) //original size of DAGDB
    {
      int j(0);
      std::string line = itos(itf+1,w,false,false);
      Identifier modlid,reflid;
      for (auto& node : DAGDB.NODES)
      {
        if (node.NUM == itf+1)
        {
          modlid = node.NEXT.IDENTIFIER;
          reflid = node.REFLID;
          line += snprintftos("%10.1f % 7.2f ",node.MAPLLG,node.ZSCORE);
          j++;
          if (j==3) break;
        }
      }
      if (j<1) line += snprintftos("%10s %7s ","---","---");
      if (j<2) line += snprintftos("%10s %7s ","---","---");
      if (j<3) line += snprintftos("%10s %7s ","---","---");
      line += snprintftos("%-13s  %*s %*s",
                  tfsg[itf].c_str(),
                  uuid_w,modlid.string().c_str(),
                  uuid_w,reflid.string().c_str());
      logTab(where,line);
    }
    logTableEnd(where);
    }}

    {{
    out::stream where = out::TESTING;
    if (Suite::Store() >= where)
    {
      int n(1),N(DAGDB.size());
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Phased Translation Function Node #" +nN);
        logTabArray(where,DAGDB.WORK->logNode(nN+""));
      }
    }
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
