//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/PointsAround.h>
#include <phasertng/site/PointsRandom.h>
#include <phasertng/site/PointsUnique.h>
#include <phasertng/site/PointsVolume.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Brute_translation_function
  void Phasertng::runBTF()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    if (nmol > 1 and !REFLECTIONS->SG.is_p1())
    throw Error(err::INPUT,"Translation function with tNCS must be done in space group P1");
    if (!DAGDB.NODES.is_all_turn())
    throw Error(err::INPUT,"Dag not prepared with rotation function mode");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    bool io_coiled_coil = false; //no accommodations

    //determine if this is the first tf or not
    auto tf1 = !DAGDB.NODES.number_of_poses();
    auto tf2 = !tf1;
    if (!tf1 and !tf2)
    throw Error(err::INPUT,"Node not compatible for translation function");
    if (tf1 and REFLECTIONS->SG.is_p1())
    throw Error(err::INPUT,"First translation function in P1: not compatible with modefunction");

    unsigned random_seed = 0;

    //initialize tNCS parameters from reflections and calculate correction terms
    //BTF
    bool store_halfR(true);
    bool store_fullR(tf2);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    double cluster_distance,sampling;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Sampling");
    if (!input.value__flt_number.at(".phasertng.brute_translation_function.sampling.").is_default())
      sampling = input.value__flt_number.at(".phasertng.brute_translation_function.sampling.").value_or_default();
    //calculate the default sampling
    std::tie(cluster_distance,sampling) = EntryHelper::clustdist(
      selected.STICKY_HIRES,
      DAGDB.WORK->NEXT.ENTRY->HELIX,
      io_coiled_coil, //false
      1.0, //helix factor, ignored
      input.value__flt_number.at(".phasertng.brute_translation_function.sampling_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.brute_translation_function.sampling.").value_or_default());
    logUnderLine(where,"Sampling");
    logTab(where,"Sampling: " + dtos(sampling,5,2) + " Angstroms");
    logTab(where,"Cluster Distance: " + dtos(cluster_distance,5,2) + " Angstroms");
    logBlank(where);
    }}

    std::unique_ptr<SiteList> traList;
    std::string volume = input.value__choice.at(".phasertng.brute_translation_function.volume.").value_or_default();
    {
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Translation Function Point Generation");
    //store the cut planes if they are set
    if (volume == "around")
    {
      dvect3 point = input.value__flt_vector.at(".phasertng.brute_translation_function.point.").value_or_default();
      double range = input.value__flt_number.at(".phasertng.brute_translation_function.range.").value_or_default();
      bool isfrac = input.value__boolean.at(".phasertng.brute_translation_function.fractional.").value_or_default();
      if (isfrac)
        point = REFLECTIONS->UC.orthogonalization_matrix()*point;
      traList.reset(new PointsAround(sampling,point,range));
      logTab(out::LOGFILE,"Translations around fractional point " + dvtos(point));
      logTab(out::LOGFILE,"Translations range " + dtos(range));
    }
    else if (volume == "random")
    {
      int nrand  = input.value__int_number.at(".phasertng.brute_translation_function.nrand.").value_or_default();
      traList.reset(new PointsRandom(nrand,random_seed++));
      logTab(out::LOGFILE,"Random points (#" + itos(nrand) + ")");
    }
    else if (volume == "box")
    {
      dvect3 box(0.5,0.5,0.5);
      //dvect3 box = input.value__flt_vector.at(".phasertng.brute_translation_function.point.").value_or_default();
      box = REFLECTIONS->UC.orthogonalization_matrix()*box;
      traList.reset(new PointsVolume(sampling,box));
      logTab(out::LOGFILE,"Fill points in fractional unit cell box " + dvtos(box));
    }
    else if (volume == "unique")
    {
      if (input.value__string.at(".phasertng.sampling_generator.change_of_basis_op.").is_default() and
          input.value__flt_numbers.at(".phasertng.sampling_generator.cuts_normals.").is_default())
        throw Error(err::INPUT,"Set unique volume with sampling generator cuts");
      af::small<cctbx::crystal::direct_space_asu::float_cut_plane<double>,12> cuts;
      std::string chop = input.value__string.at(".phasertng.sampling_generator.change_of_basis_op.").value_or_default();
      af::tiny<bool,3> continuous_shifts = {
          bool(input.value__int_vector.at(".phasertng.sampling_generator.continuous_shifts.").value_or_default()[0]),
          bool(input.value__int_vector.at(".phasertng.sampling_generator.continuous_shifts.").value_or_default()[1]),
          bool(input.value__int_vector.at(".phasertng.sampling_generator.continuous_shifts.").value_or_default()[2])
       };
      sv_double normals = input.value__flt_numbers.at(".phasertng.sampling_generator.cuts_normals.").value_or_default();
      sv_double constants = input.value__flt_numbers.at(".phasertng.sampling_generator.cuts_constants.").value_or_default();
      if (normals.size()%3)
        throw Error(err::INPUT,"Invalid normals for cuts");
      if (normals.size()/3 != constants.size())
        throw Error(err::INPUT,"Invalid constants for cuts");
      int i(0);
      for (int c = 0; c < constants.size(); c++,i+=3)
      {
        cctbx::fractional<double> frac(normals[i],normals[i+1],normals[i+2]);
        cuts[c] = cctbx::crystal::direct_space_asu::float_cut_plane<double>(frac,constants[c]);
      }
      const cctbx::uctbx::unit_cell& cctbxUC = REFLECTIONS->UC.cctbxUC;
      cctbx::crystal::direct_space_asu::float_asu<double > float_asu( cctbxUC, cuts);
      traList.reset(new PointsUnique( chop, float_asu, continuous_shifts, sampling));
    }
    logTab(out::VERBOSE,"Number of points: " + itos(traList->count_sites()));
    logBlank(out::VERBOSE);
    }

    int io_maxstored = input.value__int_number.at(".phasertng.brute_translation_function.maximum_stored.").value_or_default();
    int io_nprint = input.value__int_number.at(".phasertng.brute_translation_function.maximum_printed.").value_or_default();

    double best_tf(std::numeric_limits<double>::lowest());
    logTab(out::LOGFILE,"Maximum stored  = " + itos(io_maxstored));

    dag::NodeList output_dag;

    int n(1),N(DAGDB.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      out::stream where = out::LOGFILE;
      std::string nN = nNtos(n++,N);
      dag::Node* node = DAGDB.WORK;
      auto& turn = node->NEXT;
      logUnderLine(where,"Brute Translation Function" + nN);
      logTab(where,"Turn angle: " + dag::print_polar(turn.SHIFT_MATRIX));
      FastTranslationFunction trafun(
          REFLECTIONS.get(),
          selected.get_selected_array(),
          &epsilon
          );
      trafun.setup_spacegroup(node);
      node->convert_turn_to_curl();
      DAGDB.reset_work_entry_pointers(DataBase());
      //if translation is not the same for each
      //logTabArray(out::TESTING,node->logNode("Initial"));
      int l(1);
      int L(traList->count_sites());
      logProgressBarStart(where,"Scoring positions",L);
      const dmat33& fracmat = node->CELL.fractionalization_matrix();
      const dmat33& orthmat = node->CELL.orthogonalization_matrix();
      dmat33 PR = turn.ENTRY->PRINCIPAL_ORIENTATION;
      dvect3 PT = turn.ENTRY->PRINCIPAL_TRANSLATION;
      dmat33 ROTmt = zyz_matrix(turn.EULER);
      traList->restart();
      while (!traList->at_end())
      {
        std::string lL = nNtos(l++,L);
        dvect3 frac = traList->next_site(); //random and unique
        if (volume == "around") //calculated in terms of orthogonal points
        {
          dvect3 orth = traList->next_site();
          frac = fracmat*orth; //wrt in
        }
        dag::Next& curl = node->NEXT;
        curl.FRACT = dag::fract_wrt_mt_from_in(frac,ROTmt,PT,node->CELL);
        curl.SHIFT_ORTHT = orthmat*frac;
        pod::FastPoseType ecalcs_sigmaa_pose;
        DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &ecalcs_sigmaa_pose);
        //all the same space group
        Site newsite(frac,node->LLG); //wrt in
        trafun.translation_function_data.push_back(newsite);
        if (!(l%1000))
          trafun.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
        //could store in the dag here, but we select first, then append at the end
        if (node->LLG > best_tf)
        {
          best_tf = node->LLG;
          logProgressBarAgain(where,"Best llg = " + dtos(best_tf) + lL );
        }
        if (!logProgressBarNext(where)) break;
      }
      logProgressBarEnd(where);

      {{ //cluster regardless
      out::stream where = out::LOGFILE;
      trafun.full_sort(); //already sorted
      int cluster_back = input.value__int_number.at(".phasertng.brute_translation_function.cluster_back.").value_or_default();
      trafun.cluster(cluster_distance,cluster_back);
      bool AllSites(false);
      logTableTop(where,"Brute Translation Function (" + REFLECTIONS->SG.CCP4 + ")" + nN);
      logTabArray(where,trafun.logSlowTable(io_nprint,AllSites));
      logTableEnd(where);
      }}

      {{
      Loggraph loggraph;
      loggraph.title = "Top peaks in Brute Translation Function" +nN;
      loggraph.scatter = true;
      loggraph.graph.resize(1);
      loggraph.graph[0] = "TF Number vs LL-gain:AUTO:1,2";
      int i(1);
      for (auto& item : trafun.translation_function_data)
      {
        loggraph.data_text += itos(i++) + " " +
                              dtos(item.value,10,2) + " " +
                              "\n";
      }
      logGraph(where,loggraph);
      }}

      {{ //purge regardless
      out::stream where = out::LOGFILE;
      logEllipsisOpen(where,"Purge by cluster");
      int before = trafun.count_sites();
      trafun.select_topsites_and_erase();
      logEllipsisShut(where);
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(trafun.count_sites()));
      }}

      {{
      logEllipsisOpen(where,"Append to DAG");
      //add the growing list to the DAG, note that this will not be sorted overall
      for (auto& item : trafun.translation_function_data)
      {
        dag::Node next = *node;
        next.LLG = item.value;
        next.ZSCORE = 0; //because it is no longer the rotation function value
        next.FSS = 'N';
        next.ANNOTATION += " TF=" + dtoi(item.value);
        dag::Next& curl = next.NEXT;
        curl.SHIFT_ORTHT = orthmat*item.grid;
        curl.FRACT = dag::fract_wrt_mt_from_in(item.grid,ROTmt,PT,node->CELL);
        output_dag.push_back(next);
      }
      logEllipsisShut(where);
      }}
    }

    //use ftfr to purge by percent
    {{
    out::stream where = out::LOGFILE;
    logEllipsisOpen(where,"Purge by maximum stored");
    int before = output_dag.size();
    output_dag.apply_partial_sort_and_maximum_stored_llg(io_maxstored);
    logEllipsisShut(where);
    if (io_maxstored == 0)
    logTab(where,"Maximum stored: all");
    else
    logTab(where,"Maximum stored:      " + itos(io_maxstored));
    logTab(where,"Number before purge: " + itos(before));
    logTab(where,"Number after purge:  " + itos(output_dag.size()));
    }}

    DAGDB.NODES = output_dag;
    DAGDB.reset_entry_pointers(DataBase());
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
