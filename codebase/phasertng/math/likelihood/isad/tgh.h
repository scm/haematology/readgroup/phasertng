//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_isad_tgh_class__
#define __phasertng_likelihood_isad_tgh_class__
//#include <cctbx/hendrickson_lattman.h> expensive

namespace phasertng { namespace likelihood { namespace isad {

//this function is variously populated from function.cc
class tgh
{
  public: //always
    double SigmaNeg = 0;
    double SigmaPos = 0;

  public: //do_target
    double LogLike = 0;

  public: //do_phsprob
    double FOM = 0;
    double PHIB = 0;
    //cctbx::hendrickson_lattman<double> HL;
    double HLA,HLB,HLC,HLD;

  public: //do_gradient
    double dLL_by_dReFHpos = 0;
    double dLL_by_dImFHpos = 0;
    double dLL_by_dReFHneg = 0;
    double dLL_by_dImFHneg = 0;
    double dLL_by_dSigmaH = 0;
    double dLL_by_dfDelta = 0;
    double dLL_by_drhopm = 0;
    double dLL_by_dabsrhoFF = 0;

  public: //do_hessian
    double d2LL_by_dReFHpos2 = 0;
    double d2LL_by_dImFHpos2 = 0;
    double d2LL_by_dReFHneg2 = 0;
    double d2LL_by_dImFHneg2 = 0;
    double d2LL_by_dReFHpos_dImFHpos = 0;
    double d2LL_by_dReFHpos_dReFHneg = 0;
    double d2LL_by_dReFHpos_dImFHneg = 0;
    double d2LL_by_dImFHpos_dReFHneg = 0;
    double d2LL_by_dImFHpos_dImFHneg = 0;
    double d2LL_by_dReFHneg_dImFHneg = 0;
    double d2LL_by_dSigmaH2 = 0;
    double d2LL_by_dfDelta2 = 0;
    double d2LL_by_drhopm2 = 0;
    double d2LL_by_dabsrhoFF2 = 0;
    double d2LL_by_dabsrhoFF_by_drhopm = 0;
};

}}} //phaser
#endif
