//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Bin_class__
#define __phasertng_Bin_class__
#include <phasertng/main/includes.h>
#include <phasertng/main/Assert.h>

//#define PHASERTNG_CUBIC_BINNING

namespace phasertng {

class Bin
{
  private: //members
    double SMIN = 0;
    double SMAX = 0;
    double FNSSHELL = 0;
    int    NUMBINS = 0;
    double FnS_SMIN = 0;

  public:
    sv_int RBIN; //deep copy
    sv_int NUMINBIN; //deep copy

  public: //constructor
    Bin() {}
    Bin(int m,int n,int w,af_double s);

  public: //these determine the binning function
  double
  InvFnS(const double num) const  //inverse binning function
#ifdef PHASERTNG_CUBIC_BINNING
  { return std::cbrt(num); }
#else
  { return std::sqrt(num); }
#endif

  double
  FnS(const double& s)  const //binning function on S
#ifdef PHASERTNG_CUBIC_BINNING
  { return s*s*s; }
#else
  { return s*s; }
#endif


  public:
    void   setup(int,int,int,af_double,bool,double);
    void   setup_rbin_with_ssqr(af_double);
    void   store_ssqr_range(af_double,int);
    void   parse(double,double,double,int);
    std::string   unparse() const;
    int    numbins() const;
    bool   set_numbins(int);
    void   set_numinbin();
    double LoRes(const int) const;
    double HiRes(const int) const;
    double lores() const { PHASER_ASSERT(SMIN); return 1./SMIN; }
    void   set_hires(const double hires) { PHASER_ASSERT(hires); SMAX = 1/hires; } //reset hires limit
    double hires() const { PHASER_ASSERT(SMAX); return 1./SMAX; }
    sv_int wilson_bins(const double,const double) const;
    double MidRes(const int) const;
    double MidSsqr(const int) const;
    int    get_bin_s(const double) const;
    int    get_bin_ssqr(const double) const;
    double get_midSsqr_ssqr(const double) const;
    bool   ssqr_in_range(const double) const;
    std::pair<int,int> get_bin_s_and_valid(const double) const;
    af_string logBin(const std::string) const;
    dvect3 shells() const { return {SMIN,SMAX,FNSSHELL}; }
};

}
#endif
