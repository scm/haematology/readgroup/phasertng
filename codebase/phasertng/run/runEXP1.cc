//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/ReflColsMap.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/site/PointsRandom.h>

namespace phasertng {

  //Space_group_expansion
  void Phasertng::runEXP1()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
 //   if (!REFLECTIONS->prepared(reflprep::DATA))
 //   throw Error(err::INPUT,"Reflections not prepared with correct mode (data)");
 //   if (REFLECTIONS->prepared(reflprep::ANISO))
  //  throw Error(err::INPUT,"Reflections not prepared with correct mode (data)");

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Initial Data"));
    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Initial"));

    out::stream where = out::LOGFILE;
    std::unique_ptr<ReflColsMap> EXP1REFL(new ReflColsMap());
    EXP1REFL->copy_and_reformat_data(REFLECTIONS.get());
    logTab(where,"Expand data to p1");
    bool change_spacegroup_to_p1(true);
    EXP1REFL->expand_to_p1(change_spacegroup_to_p1);
    if (Suite::Extra())
      logTabArray(out::TESTING,EXP1REFL->logReflections("Expanded"));
    logTab(where,"Copy data back to reflection class");
    REFLECTIONS->copy_and_reformat_data(EXP1REFL.get()); //replace REFLECTIONS data structure

    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int n(1),N(DAGDB.size());
    DAGDB.restart(); //very important to call this to set WORK
    auto& UC = REFLECTIONS->UC;
    while (!DAGDB.at_end())
    {
      auto& work = DAGDB.WORK;
      work->cleanUp(); //new
      auto P1POSE = work->POSE;
      P1POSE.clear();
      auto& SG = work->SG;
      for (int p = 0; p < work->POSE.size(); p++)
      {
        for (int isym = 0; isym < SG->rotsym.size(); isym++)
        {
          //we have run cleanup so orthr ortht will be zero
          dag::Pose pose = work->POSE[p];
          dmat33 ROT;dvect3 FRACT;
          std::tie(ROT,FRACT) = pose.finalRT(UC.orthogonalization_matrix(),UC.fractionalization_matrix());
          dmat33 Rsymt = SG->rotsym[isym].transpose();
          dmat33 RsymOrth = UC.orthogonalization_matrix()*Rsymt*UC.fractionalization_matrix();
          dvect3 ORTHT_new = RsymOrth*pose.ORTHT;
          pose.FRACT = SG->rotsym[isym].transpose()*FRACT + SG->trasym[isym] - UC.fractionalization_matrix()*ORTHT_new;
          for (int i = 0; i < 3; i++)
          {
            while (pose.FRACT[i] < 0) pose.FRACT[i] += 1;
            while (pose.FRACT[i] >= 1) pose.FRACT[i] -= 1;
          }
          pose.ORTHT = ORTHT_new;
          dmat33 ROT0 = zyz_matrix(pose.EULER);
          ROT0 = RsymOrth*ROT0;
          pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT0);
          const dvect3 PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
          dvect3 frac = dag::fract_wrt_in_from_mt(pose.FRACT,pose.EULER,PT,work->CELL);
          pose.SHIFT_ORTHT = UC.orthogonalization_matrix()*frac;
          P1POSE.push_back(pose);
        }
      }
      work->POSE = P1POSE;
    }

    int ncyc = input.value__int_number.at(".phasertng.perturbations.number.").value_or_default();
    if (ncyc > 0)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Perturbation");
      logTab(where,"Break the symmetry by perturbing the solutions with respect to the axial translations");
      auto range = input.value__flt_paired.at(".phasertng.perturbations.range.").value_or_default();
      logTab(where,"There will be " + itos(ncyc) + " perturbations");
      if (input.value__choice.at(".phasertng.perturbations.selection.").value_or_default_equals("both"))
      {
        logTab(where,"Orientation and translation perturbation");
      }
      if (input.value__choice.at(".phasertng.perturbations.selection.").value_or_default_equals("rot"))
      {
        logTab(where,"Orientation perturbation");
        range.second = 0;
      }
      if (input.value__choice.at(".phasertng.perturbations.selection.").value_or_default_equals("tra"))
      {
        logTab(where,"Translation perturbation");
        range.first = 0;
      }
      //separate so that if you turn of both, the list is the same as for just rot or tra
      PointsRandom rotList(1),traList(2); //use different seeds
      logTab(where,"Maximum orientation: +/-" + dtos(range.first,5,2) + " degrees");
      logTab(where,"Maximum translation: +/-" + dtos(range.second,5,2) + " Angstroms");
      logBlank(where);
      dag::NodeList output_dag = DAGDB.NODES;//init with the original
      for (int i = 0; i < DAGDB.size(); i++)
      {
        out::stream where = out::VERBOSE;
        std::string nN = nNtos(i+1,DAGDB.size());
        logTab(where,"Node"+nN);
        for (int c = 0; c < ncyc; c++)
        {
          dag::Node work = DAGDB.NODES[i];
          std::string cC = nNtos(c+1,ncyc);
          logTab(where,"Perturbation"+ cC);
          work.cleanUp();
          for (int p = 0; p < work.POSE.size(); p++)
          {
            dag::Pose& pose = work.POSE[p];
            auto rot = rotList.next_site(range.first);
            auto tra = traList.next_site(range.second);
            pose.ORTHR += rot;
            pose.ORTHT += tra;
            std::string pP = nNtos(p+1,work.POSE.size());
            logTab(where,"Pose"+ pP + dvtos(rot,6,2) + " degrees " +
                                      dvtos(tra,6,2) + " Angstroms");
          }
          output_dag.push_back(work);
        }
      }
      DAGDB.NODES = output_dag;
    }

    //must change the identifier because we have changed the reflection list
    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::LOGFILE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
   // REFLECTIONS->REFLPREP.insert(reflprep::EXP1);
    //clang complains PHASER_ASSERT(typeid(*REFLECTIONS).name() == typeid(REFLCOLSMAP).name());
    }}

    //change the space group on the nodes
    DAGDB.NODES.set_hall(REFLECTIONS->SG.HALL);
    DAGDB.NODES.apply_unit_cell(REFLECTIONS->UC.cctbxUC);
    DAGDB.reset_entry_pointers(DataBase()); //for spacegroup
    logTabArray(out::VERBOSE,REFLECTIONS->logMtzHeader(""));

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
