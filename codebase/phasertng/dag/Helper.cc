//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/Helper.h>
#include <scitbx/math/euler_angles.h>
#include <phasertng/math/rotation/rotdist.h>

namespace phasertng {
namespace dag {

  // Note: PT is the principal translation applied to center the molecule
  // on the origin before computing the molecular transform (i.e. minus
  // the centre)

  dvect3
  fract_wrt_in_from_mt(
      const dvect3 fract,
      const dvect3 eulermt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL)
  {
    dmat33 ROTmt = zyz_matrix(eulermt);
    return fract + CELL.fractionalization_matrix()*ROTmt*PT;
  }

  dvect3
  fract_wrt_in_from_mt(
      const dvect3 fract,
      const dmat33 ROTmt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL)
  {
    return fract + CELL.fractionalization_matrix()*ROTmt*PT;
  }

  dvect3
  fract_wrt_mt_from_in(
      const dvect3 fract,
      const dvect3 eulermt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL)
  {
    dmat33 ROTmt = zyz_matrix(eulermt);
    return fract - CELL.fractionalization_matrix()*ROTmt*PT;
  }

  dvect3
  fract_wrt_mt_from_in(
      const dvect3 fract,
      const dmat33 ROTmt,
      const dvect3 PT,
      const cctbx::uctbx::unit_cell CELL)
  {
    return fract - CELL.fractionalization_matrix()*ROTmt*PT;
  }

  dvect3
  euler_wrt_in_from_mt(
      const dvect3 euler,
      const dmat33 pr)
  {
    dmat33 rot = zyz_matrix(euler);
    dmat33 mrot = rot*pr;
    return scitbx::math::euler_angles::zyz_angles(mrot);
    //return regularize_angle(scitbx::math::euler_angles::zyz_angles(mrot));
  }

  dvect3
  euler_wrt_mt_from_in(
      const dvect3 euler,
      const dmat33 pr)
  {
    dmat33 rot = zyz_matrix(euler);
    dmat33 mrot = rot*pr.transpose();
    return scitbx::math::euler_angles::zyz_angles(mrot);
    //return regularize_angle(scitbx::math::euler_angles::zyz_angles(mrot));
  }

  std::pair<dvect3,dmat33>
  print_euler_wrt_in_from_mt(dvect3 eulermt,dmat33 PR)
  {
    dvect3 eulerin = dag::euler_wrt_in_from_mt(eulermt,PR);
    eulerin = regularize_angle(eulerin);
    dmat33 ROTin = zyz_matrix(eulerin);
    return {eulerin,ROTin};
  }

  std::string
  print_polar(dmat33 ROTin)
  {
    dvect3 axis; double angle;
    std::tie(axis,angle) = axis_and_angle_deg(ROTin);
    char buf[80];
    for (int i=0; i<3; i++) if (std::fabs(axis[i]) < 1.0e-04) axis[i] = 0;
    snprintf(buf,80,"[% 7.4f % 7.4f % 7.4f]:[%5.1f]",axis[0],axis[1],axis[2],angle);
    return std::string(buf);
  }

  std::string
  print_ortht(dvect3 ortht,int w)
  {
    w = std::max(w,5);//OrthX
    for (int i = 0; i < 3; i++)
    { //max w=7
      ortht[i] = std::min(ortht[i],9999999.);
      ortht[i] = std::max(ortht[i],-99999.);
    }
    int p1 = std::fabs(ortht[0]) > 9999. ? 0 : 1;
    int p2 = std::fabs(ortht[1]) > 9999. ? 0 : 1;
    int p3 = std::fabs(ortht[2]) > 9999. ? 0 : 1;
    w=w+1; //this is the spacer for the minus if requiring all of cellw
    //there will be no space in this case, which is optimal for width
    char buf[80];
    snprintf(buf,80,"[%*.*f%*.*f%*.*f]",w,p1,ortht[0],w,p2,ortht[1],w,p3,ortht[2]);
    return std::string(buf);
  }

  std::string
  print_fract(dvect3 fract)
  {
    for (int i=0; i<3; i++) if (std::fabs(fract[i]) < 1.0e-03) fract[i] = 0;
    char buf[80];
    snprintf(buf,80,"[%5.2f %5.2f %5.2f]",fract[0],fract[1],fract[2]);
    return std::string(buf);
  }
  std::string header_polar() { return "Polar wrt original [Axis]:[Angle]"; }
  std::string header_ortht(int w)
  {
    w = std::max(w,5);
    char buf[80];
    snprintf(buf,80,"[ %*s %*s %*s]",w,"OrthX",w,"OrthY",w,"OrthZ");
    return std::string(buf);
  }
  std::string header_fract() { return "[FracX FracY FracZ]"; }

}} //phasertng
