//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_DirectCorMat_class__
#define __phasertng_DirectCorMat_class__
#include <phasertng/main/includes.h>

#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <scitbx/array_family/accessors/c_grid.h>
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;
typedef scitbx::af::versa<double, scitbx::af::c_grid<3> >   versa_grid_double;

namespace phasertng {

class Models; //forward declaration
class Scatterers; //forward declaration

  class DirectCorMat
  {
    private:
      double lowest_reso = 10.;
      double targetZ_CC = 6.; // Target CC/sigma(CC) for worst off-diagonal element
      double cell_padding = 5.;

    public:
      versa_flex_double CorMat;
      bool   uncorrelated = false;
      double midSsqr = 0;
      dvect3 CELL;

    public:
      DirectCorMat(
          double lowest_reso_ = 10.,
          double targetZ_CC_ = 6., // Target CC/sigma(CC) for worst off-diagonal element
          double cell_padding_ = 5.
        ) :
        lowest_reso(lowest_reso_),
        targetZ_CC(targetZ_CC_),
        cell_padding(cell_padding_)
        { }

    public:
      void setup(Models&,Scatterers&,double);
  };

} //phasertng
#endif
