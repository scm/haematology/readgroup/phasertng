//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Expected_llg_for_map
  void Phasertng::runEMAP()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (DAGDB.size() != 1)
    throw Error(err::INPUT,"Dag must contain one node");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Guide to eLLG values");
    logTab(where,"eLLG   Top solution correct?");
    logTab(where,"<25    :no");
    logTab(where,"25-36  :unlikely");
    logTab(where,"36-49  :possibly");
    logTab(where,"49-64  :probably");
    logTab(where,">64    :yes");
    logBlank(where);
    }}

    //make sure the reflections are sorted low resolution to high resolution
    //this should be done as part of the data preparation step
    if (!REFLECTIONS->check_sort_in_resolution_order())
      throw Error(err::INPUT,"Reflection file not sorted by resolution, aborting");

    logUnderLine(out::LOGFILE,"Map eLLG Calculation");
    logTab(out::LOGFILE,"Resolution = " + dtos(REFLECTIONS->data_hires()));
    logBlank(out::LOGFILE);

    sv_double io_rmsd = input.value__flt_numbers.at(".phasertng.map_ellg.rmsd.").value_or_default_unique();
    sv_double io_fs = input.value__flt_numbers.at(".phasertng.map_ellg.fs.").value_or_default_unique();
    if (!io_rmsd.size())
      throw Error(err::FATAL,"No Rmsd: check input");
    if (!io_fs.size())
      throw Error(err::FATAL,"No Fraction Scattering: check input");

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    double fsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
    double bsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
    double minb = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();

    likelihood::mapELLG likelihood_emap;
    for (auto Rmsd : io_rmsd)
    {
      for (auto fp : io_fs) //more ordered
      {
        //Calculation assumes that there is no change in the total scattering
        //caused by the change in the B-factor of a single atom
        //This calculation will not work changing the B-factor of larger components
        FourParameterSigmaA solTerm(Rmsd,fsol,bsol,minb);
        double eLLG(0);
        af_double bin_reso = REFLECTIONS->bin_midres(0);
        af_double Siga(bin_reso.size());
        //calculate the resolution dependent parameters by bin, for speed
        for (int b = 0; b < bin_reso.size(); b++)
        {
          double ssqr = 1./bin_reso[b];
          double bin_sigmaa = solTerm.get(ssqr);
          Siga[b] = bin_sigmaa*std::sqrt(fp); //take sqrt binwise
        }
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          if (selected.get_selected(r))
          {
            const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
            const double feff = work_row->get_feffnat();
            const double dobs = work_row->get_dobsnat();
            const double resn = work_row->get_resn();
            // teps and cent aren't relevant for usual cryo-EM but might be with
            // point-group or helical symmetry
            const double teps = work_row->get_teps();
            const bool cent = work_row->get_cent();
            const int& b = work_row->get_bin();
            double Emean = feff / (resn * std::sqrt(teps));
            double DobsSiga = dobs * Siga[b];
            double expected_LLG = likelihood_emap.get(Emean, DobsSiga, cent);
            eLLG += expected_LLG;
          }
        }
        input.push_back(".phasertng.map_ellg.result.rmsd.",Rmsd);
        input.push_back(".phasertng.map_ellg.result.fs.",fp);
        input.push_back(".phasertng.map_ellg.result.ellg.",eLLG);
      }
    }

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Map eLLG");
    int NSA = input.size(".phasertng.map_ellg.result.ellg.");
    double last_fs = -999; //flag
    for (int i = 0; i < NSA; i++)
    {
      double fs = input.array__flt_number.at(".phasertng.map_ellg.result.fs.").at(i).value_or_default();
      double rmsd = input.array__flt_number.at(".phasertng.map_ellg.result.rmsd.").at(i).value_or_default();
      double ellg =  input.array__flt_number.at(".phasertng.map_ellg.result.ellg.").at(i).value_or_default();
      std::string sellg(ellg > DEF_PPT ? dtos(ellg,12,3).c_str() : "(< 0.001)");
      if (last_fs != fs)
      {
        logTab(where,snprintftos("Fraction scattering: %8.2f",fs));
        logTab(where,snprintftos("%8s  %12s","Rmsd","Expected-llg"));
      }
      logTab(where,snprintftos("%8.3f  %12s",rmsd,sellg.c_str()));
      last_fs = fs;
    }
    logTableEnd(where);
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
