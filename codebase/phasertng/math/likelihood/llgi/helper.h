//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_likelihood_llgi_helper_functions__
#define __phasertng_likelihood_llgi_helper_functions__
#include <complex>
#include <scitbx/vec3.h>
#include <cctbx/miller.h>

typedef std::complex<double> cmplex;
typedef cctbx::miller::index<int> millnx;
typedef scitbx::vec3<double> dvect3;

namespace phasertng { namespace likelihood { namespace llgi {

  double
  DobsSigaSqr(
      double SSQR,double SigaSqr,double FS,double DOBS,double DRMS,double BFAC,double G_DRMS);

  cmplex
  EcalcTerm(
      double DobsSigaSqr,double EPS,double NSYMP,millnx SymHKL,dvect3 TRA,double symTra);

  double
  EcalcSqrTerm(
      double DobsSigaSqr,double EPS,double NSYMP);

}}} //phasertng
#endif
