//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Output_class__
#define __phasertng_Output_class__
#include <phasertng/main/Error.h>
#include <phasertng/main/includes.h>
#include <phasertng/main/Version.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/main/phenix_out.h>
#include <phasertng/main/phenix_callback.h>
#include <phasertng/main/Suite.h>
#include <chrono>
#include <ctime>
#include <unordered_map>
#include <map>

namespace phasertng {

class Loggraph;//forward declaration
class FileSystem;//forward declaration

class Output : public Suite, public Error, public Version
{
  struct loopstruct {
    std::string current = "";
    int count = 0;
    int max = 0;
  };
  struct barstruct {
    std::string  Title = "";
    int  Length = 0;
    int  Count = 0;
    int  Bars = 0;
    int  BarSize = 0;
    int  Chunk = 0;
    std::chrono::time_point<std::chrono::system_clock> WallClock;
    bool Killed = false;
    bool UseKillTime = true;
    std::string line = "";
  };
    out::stream CCP4_SUMMARY_LEVEL = out::SUMMARY;
    bool KILLFLAG = false;

  public:
    Output(std::string);
    Output(Suite);
    ~Output() throw() { }

    Output& operator << (const Output &rhs)
    {
      for (auto item : rhs.STREAM_STRING)
        logOutput(item.first,item.second);
      return *this;
    }

  private:
    double        cumulativeCpusSeconds = 0;
    double        cumulativeWallSeconds = 0;
    std::clock_t  CpuStart;
    double        mainCpuClock = 0;
    double        modeCpuClock = 0;
    std::chrono::time_point<std::chrono::system_clock> mainWallClock;
    std::chrono::time_point<std::chrono::system_clock> modeWallClock;
    bool          collect_table = false;
    int           imode = 0; //collect it info in multi jobs in maps per imode
    std::map<std::string,loopstruct> loop_info;
    std::map<int,barstruct>  progress; //map is for nexted progress bars, key is a flag
    int last_header = 0;
    bool last_blank_line; //sort order important

  public:
    //these are collected per mode
    std::unordered_map<int,af_string> WARNINGS;
    std::unordered_map<int,af_string> ADVISORIES;
    std::unordered_map<int,af_string> LOGGRAPHS;
    std::unordered_map<int,af_string> TABLES;
    std::vector<std::tuple<std::string,std::string,std::string>> FILEOUT;
    //we use tuple rather than FileSystem here to avoid FileSystem in the Output.h header
    //allows forward declaration of FileSystem as only used in function call below

  protected:
    std::vector<std::pair<out::stream,std::string>> STREAM_STRING;
    std::vector<std::string> LOOPS;

  public:
    std::shared_ptr<phenix_out> outstream_ptr = nullptr;
    std::shared_ptr<phenix_callback> callback_ptr = nullptr;
    bool USE_CALLBACKS = false;
    std::string LOOPXML;

    double get_mode_wall_seconds();
    double get_mode_cpus_seconds();
    double get_cumulative_wall_seconds();
    double get_cumulative_cpus_seconds();
    void   set_imode(int r) { imode = r; }
    void   set_cumulative_wall_seconds(double);
    void   set_cumulative_cpus_seconds(double);
    void   clear_memory();
    std::string   tab(out::stream);

  private:
    void   logOutput(out::stream,const std::string); //master output function
    void   checkForKill();
    void   logLoop(out::stream,std::string,std::string,std::string,char);

  public://python
    void  put_advisory(std::string);
    void  put_warning(std::string);
    af_string warnings() const;
    af_string advisories() const;
    af_string loggraphs() const;
    af_string tables() const;
    std::string loopxml() const;
    std::string looptitle() const;
    void        add_loopxml(std::string,std::string);
    std::string logfile() { return Print(out::LOGFILE); }
    std::string summary() { return Print(out::SUMMARY); }
    std::string concise() { return Print(out::CONCISE); }
    std::string process() { return Print(out::PROCESS); }
    std::string verbose() { return Print(out::VERBOSE); }
    //std::string further() { return Print(out::FURTHER); }
    std::string testing() { return Print(out::TESTING); }
    std::vector<std::pair<out::stream,std::string> > StreamString() const { return STREAM_STRING; }

  public:
    void incrementRunTime();
    void logWarning(const std::string);
    void logAdvisory(const std::string);
    std::string get_level(std::string);
    void setOutStream(std::shared_ptr<phenix_out> const&);
    void setCallBack(std::shared_ptr<phenix_callback> const&);
    void phenixCallback(std::string,std::string);
    void StartModeTime();
    std::string command_line(bool); //output with flags to command line

    void logProgramStart(out::stream);
    void logProgramEnd(out::stream);
    void logHeader(out::stream,std::string);
    void logHeaderTitle(out::stream,std::string,std::string);
    void logSectionHeader(out::stream,std::string);
    void logError(out::stream,Error);
    void logAssert(out::stream,std::string);
    void logBlank(out::stream);
    void logFlush();
    void logUnderLine(out::stream,const std::string);
    void logChevron(out::stream,const std::string);
    void logKeywords(out::stream,std::string);
    void logLine(out::stream,const int,char='-');
    void logTab(out::stream,const std::string);
    void logFileWritten(out::stream,bool,const std::string,FileSystem);
    void logTabArray(out::stream,const af_string);
    void logEllipsisOpen(out::stream,const std::string);
    void logEllipsisShut(out::stream);
    void logLoopOpen(out::stream,std::string,int,std::string);
    void logLoopNext(out::stream,std::string,std::string);
    void logLoopExit(out::stream,std::string);
    void logLoopHere(out::stream,std::string);
    void logLoopInfo(out::stream,std::string);
    void logLoopShut(out::stream,std::string);
    void logTableTop(out::stream,const std::string,bool=true);
    void logTableEnd(out::stream);
    void logProgressBarStart(out::stream,const std::string,const int,int=1,bool=true);
    bool logProgressBarNext(out::stream,int=1);
    void logProgressBarEnd(out::stream,int=1);
    void logProgressBarAgain(out::stream,const std::string,int=1);
    void logWarnings(out::stream);
    void logAdvisories(out::stream);
    void logUserTime(out::stream, double);
    void logGraph(out::stream,const Loggraph&);
    void logProtocol(out::stream,std::vector<sv_string>,sv_int,std::string);
    void logHistogram(out::stream,af_double,int,bool,std::string,double=0,double=0,bool=true,bool=false);
    void logHistogram(out::stream,sv_double,int,bool,std::string);
    void logTrailer(out::stream);
    std::string format_time(std::string,double,bool=true);
    af_string   format_trailer();
    std::string Print(out::stream);
    void        clear_storage();
    void        clear_mode_storage();
    sv_string   Banner();
    sv_string   Footer();
    void  add_trailer(out::stream);
    void  subtract_trailer();
    std::string  TimeStamp();
};

} //phasertng

#endif
