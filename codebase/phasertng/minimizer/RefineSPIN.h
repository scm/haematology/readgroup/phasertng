//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineSPIN_class__
#define __phasertng_RefineSPIN_class__
#include <phasertng/main/includes.h>
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/uuid.h>
#include <phasertng/main/pointers.h>
#include <phasertng/pod/fast_types.h>
#include <phasertng/math/likelihood/iwilson/function.h>
#include <phasertng/math/likelihood/iwilson/helper.h>
#include <scitbx/constants.h>
#include <unordered_set>

namespace phasertng {

/* This function refines orientation with no poses using the Wilson target function
   not the Rice function used when poses are present or for rescoring orientations
   in the rotation function
   TNCS is treated as a fixed difference in position and unknown orientation (halfR=true)
   i.e. this is NOT designed for refining the orientation of tncs-related molecules in
   the absence of a translation component
   This would cause problems when taking this forward into the translation function
   (have these gyres already been doubled+ by the tncs vector or not)
   and also what halfR correction terms would need to be used
*/

class RefineSPIN : public dtmin::RefineBase
{
   const cmplex cmplxONE = cmplex(1.,0.);
   const cmplex TWOPII = cmplex(0.,scitbx::constants::two_pi);

  private: //members
    bool   REFINE_ROTN = false;
    bool   REFINE_VRMS = false;
    double WILSON_K = 0;
    double WILSON_B = 0;
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;
    dvect3 LARGE_ROTN;

  public:
    double SIGMA_ROT = 0;
    bool   negvar = false;

  private:
    //Parameters derived from Dag node parameters
    dag::Node* NODE;
    pod::FastPoseType ecalcs_sigmaa_pose; //fast terms for background pose

  public: //members
    const_ReflectionsPtr  REFLECTIONS;
    sv_bool               SELECTED;
    const_EpsilonPtr      EPSILON;

  public:  //constructor
    RefineSPIN() : dtmin::RefineBase() {}
    RefineSPIN(
        const_ReflectionsPtr,
        sv_bool,
        const_EpsilonPtr);
    ~RefineSPIN() throw() {}

  public:
    void init_node(dag::Node*,pod::FastPoseType&);

  private:
    TargetGradientHessianType
    parallel_combined_tgh(
      bool, //gradient
      bool, //hessian
      int, //beginning
      int //end
    );

    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
      );

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string finalize_parameters();

  public:
    TargetType
    likelihood();

  public:
    void set_threading(bool,int);
};

} //phasertng
#endif
