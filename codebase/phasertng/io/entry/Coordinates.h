//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Coordinates_class__
#define __phasertng_Coordinates_class__
#include <phasertng/main/Identifier.h>
#include <phasertng/pod/scatterer.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/io/EntryBase.h>
#include <cctbx/sgtbx/site_symmetry_table.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <set>

namespace phasertng {

//Coordinates are a single set (not ensemble) of atomic coordinates that
//have the space group and unit cell of the reflections
//c.f. Models, which are in P1 large unit cell
//and  Trace, which are in P1 and arbitrary cell

  class Coordinates : public EntryBase
  {
    public:
      af_string  REMARKS;
      std::set<std::string> pdb_errors = {};
      std::vector<PdbRecord>  PDB; //use AddAtom, but copy required for spr
      //xray and xtra must be kept same length
      //isotropic b-factors only
      Identifier MODLID = Identifier();
      Identifier REFLID = Identifier();
      std::string TIMESTAMP = "";
      SpaceGroup SG = SpaceGroup("P 1");
      UnitCell   UC = UnitCell();
      af::shared<pod::xray::scatterer>  XRAY; //for direct pass to cctbx library
      cctbx::sgtbx::site_symmetry_table site_sym_table;
      cctbx::xray::scattering_type_registry scattering_type_registry;
      double SCATTERING = 0;
      double PARTK = 1;
      double PARTU = 0;
      double BMIN = 2;
      double BMAX = 999.99; //pdb format limit

    public: //constructors
      Coordinates() : EntryBase() {} //for class member declaration
      Coordinates(af_string pdblines) : EntryBase() { SetPdb(pdblines); }
      bool in_memory() { return MODLID.is_set_valid(); }
      void read_from_disk() { ReadPdb(); }
      void write_to_disk() { WritePdb(); }

    private:
      std::pair<PdbRecord,cctbx::xray::scatterer<double>> ProcessAtom(PdbRecord);

    public:
      void       ReadPdb();
      void       WritePdb();
      void       GetPdbHandler(PdbHandler&);
      af_string  get_pdb_cards();
      void       SetPdb(af_string);
      void       AddAtom(PdbRecord);
      void       SetAtom(int,PdbRecord);
      void       SetIsotropicU(int,double);
      void       SetOccupancy(int,double);
      const int  size() const { return XRAY.size(); }
      void       process(const Scatterers&);
      std::set<std::string> set_of_atomtypes();
      void       clear();

    public: //segment code
      int   nresidues();
      bool  calculate_segment_identifiers(const int);
      int   nsegments() const;
      std::pair<double,double>   bfactor_range() const;
      double percent_pruned() const;
      double scattering();
      af_string  logSegmentTable() const;
      af_string  logSegmentStats() const;
      af_string  logSegmentGraphic(bool=false) const;
      void add_lattice_translocation(std::vector<std::pair<dvect3,double>>);
  };

} //phasertng
#endif
