//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/table/pdb_chains_2chainid.h>
#include <iso646.h>

namespace phasertng {

constexpr const char* pdb_chains_2chainid::allowed_chains[8163];
  std::string
  pdb_chains_2chainid::allocate(std::string pdb_chain)
  {
    //if the pdb chain id is in the allowed set, use it,
    //otherwise return the next available chainid
    if (used_chain_ids.size() < tbllen)
    {
      //chain must be legal to start with
      if (pdb_chain.size() == 1)
        pdb_chain = " " + pdb_chain;
      if ((pdb_chain.size() == 2) and //legal
          (pdb_chain != "  ") and //illegal
          (used_chain_ids.count(pdb_chain) == 0)) //not already used
      {
        used_chain_ids.insert(pdb_chain);
        return pdb_chain;
      }
      //next not already used
      int i(0);
      for (i = 0; i < tbllen; i++)
      {
        auto item = allowed_chains[i];
        if (item == pdb_chain)
          break; //skip to where this pdb_chain is found
      }
      if (i == tbllen)
        i = 0; //the chain was not allowed in the first place, start the beginning
      for (int j = i; j < tbllen; j++)
      { //first will be a match but already returned above
        auto item = allowed_chains[j];
        if (used_chain_ids.count(item) == 0)
        {
          used_chain_ids.insert(item);
          return item;
        }
      }
    }
    allocate_failed_ = true;
    return "  ";
  }

  std::string
  pdb_chains_2chainid::index(int i)
  {
    if (i < size()) return allowed_chains[i];
    return "  ";
  }

} //phasertng
