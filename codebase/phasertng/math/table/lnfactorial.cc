#include <phasertng/math/table/lnfactorial.h>

namespace phasertng {
namespace table {

  lnfactorial::lnfactorial(int n)
  {
    ntabmax = std::max(80,n); // Approx good to 1 in 10^15 above 80
    table.clear();
    table.push_back(0);
    for (int i = 1; i <= n; i++)
      table.push_back(table[i-1]+log(static_cast<double>(i)));
  }

  double lnfactorial::get(int n) const
  {
    if (n <= ntabmax)
      return table[n];
    else // Use Nemes approximation to logGamma (related to Stirling approx)
    {
      double arg(static_cast<double>(n)+1.);
      return log2pi2 - log(arg)/2. + arg*(log(arg+1./(12.*arg-0.1/arg))-1.);
    }
  }

  lnfactorial lnfactorial::inst_;
}}

