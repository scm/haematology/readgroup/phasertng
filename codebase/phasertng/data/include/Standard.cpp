//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Selected.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/main/pointers.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/math/likelihood/pIntensity.h>
#include <phasertng/math/likelihood/mapELLG.h>
#include <future>

namespace phasertng {

  std::set<rejected::code>
  Selected::standard_criteria()
  {
    return {
        rejected::RESO,
        rejected::MISSING,
        rejected::SYSABS,
        rejected::WILSONNAT,
        rejected::LOINFONAT,
        rejected::NOMEAN
        };
  }

  std::set<rejected::code>
  Selected::minimal_criteria()
  {
    return {
        rejected::RESO,
        rejected::MISSING,
        rejected::NOMEAN
        };
  }

  std::set<rejected::code>
  Selected::hires_criteria()
  {
    return {
        rejected::MISSING,
        rejected::WILSONNAT,
        rejected::LOINFONAT,
        rejected::NOMEAN
        };
  }

  void
  Selected::flag_standard_outliers_and_select(
      const_ReflectionsPtr reflections
    )
  {
    CRITERIA = standard_criteria();
    PROBABILITIES = ProbType(); //filled by flag_standard
    //PHASER_ASSERT(reflections->has_col(labin::INAT));
    PHASER_ASSERT(reflections->has_col(labin::SSQR));
    if (NTHREADS == 1)
    {
      int beg(0);
      int end(reflections->NREFL);
      auto reflprobs = flag_standard(
          reflections,
          beg,
          end);
      append_probabilities(reflprobs);
    }
    else
    {
      std::vector< std::future< ProbType > > results;
      std::vector< std::packaged_task< ProbType(
              const_ReflectionsPtr,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS)
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = reflections->head_thread(t,NTHREADS);
        int end = reflections->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind( &Selected::flag_standard, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3
            ));
          results.push_back(packaged_tasks[t].get_future());
          threads.emplace_back(std::move(packaged_tasks[t]),
              reflections,
              beg,
              end );
        }
        else
        {
          results.push_back(std::async(std::launch::async, &Selected::flag_standard, this,
              reflections,
              beg,
              end));
        }
      }

      {
      int beg = reflections->head_thread(nthreads_1,NTHREADS);
      int end = reflections->tail_thread(nthreads_1,NTHREADS);
      auto reflprobs = flag_standard(
          reflections,
          beg,
          end);
      append_probabilities(reflprobs);
      }

      for (int t = 0; t < nthreads_1; t++)
      {
        auto reflprobs = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join();
        append_probabilities(reflprobs);
      }
    }
    //now flag the outliers based on the criteria selected
    SELECTED.resize(reflections->NREFL,true); //note that resize does not init if not actual resize
    for (int r = 0; r < SELECTED.size(); r++)
      SELECTED[r] = true;
    for (auto refl : PROBABILITIES) //set of rejected::code
    {
      for (auto code : refl.second.CODES) //set of rejected::code
      {
        if (CRITERIA.count(code))
        {
          PHASER_ASSERT(refl.first < SELECTED.size());
          SELECTED[refl.first] = false;
        }
      }
    }
    {{
    double ssqrmax = std::numeric_limits<double>::lowest();
    for (int r = 0; r < SELECTED.size(); r++)
    if (SELECTED[r])
    {
      const double& ssqr = reflections->work_ssqr(r);
      ssqrmax = std::max(ssqrmax,ssqr);
    }
    resolution_selected = 1.0/std::sqrt(ssqrmax); //store for logging
    }}
  }

  ProbType
  Selected::flag_standard(
      const_ReflectionsPtr reflections,
      int beg,
      int end) const
  {
    ProbType reflprobs;
    math::pIntensity pintensity;
    double sigesqr_cent,sigesqr_acent;
    std::tie(sigesqr_cent,sigesqr_acent) = sigesqr_values();
    //do the test in terms of ssqr to avoid sqrt unless required
    double tol = DEF_PPM;
    double ssqrmax = 1.0/fn::pow2(std::max(STICKY_HIRES-tol,tol));
    double ssqrmin = 1.0/fn::pow2(std::min(STICKY_LORES-tol,DEF_LORES));
    bool hasinat = reflections->has_col(labin::INAT);
    for (int r = beg; r < end; r++)
    {
      const millnx& miller = reflections->get_miller(r);
      const double& ssqr = reflections->get_flt(labin::SSQR,r);
      if (STICKY_HIRES and ssqr > ssqrmax)
      {
        const double  reso = 1/std::sqrt(ssqr);
        if (reflprobs.count(r))
          reflprobs[r].CODES.insert(rejected::RESO); else
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::RESO)); //creates
      }
      else if (ssqr < ssqrmin)
      {
        const double  reso = 1/std::sqrt(ssqr);
        if (reflprobs.count(r))
          reflprobs[r].CODES.insert(rejected::RESO); else
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::RESO)); //creates
      }
      if (reflections->SG.group().is_sys_absent(miller))
      {
        const double  reso = 1/std::sqrt(ssqr);
        if (reflprobs.count(r))
          reflprobs[r].CODES.insert(rejected::SYSABS); else
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::SYSABS)); //creates
      }
      if (!reflections->get_present(friedel::NAT,r))
      {
        const double  reso = 1/std::sqrt(ssqr);
        //rejected::MISSING reserved for e.g. free-r flag only?
        if (reflprobs.count(r))
          reflprobs[r].CODES.insert(rejected::NOMEAN); else
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::NOMEAN)); //creates
      }
      else if (hasinat and !reflections->get_flt(labin::INAT,r))
      {
        const double  reso = 1/std::sqrt(ssqr);
        //rejected::MISSING reserved for e.g. free-r flag only?
        if (reflprobs.count(r))
          reflprobs[r].CODES.insert(rejected::MISSING); else
          reflprobs.emplace(r,pod::probtype(miller,reso,rejected::MISSING)); //creates
      }
      else if (hasinat and !reflections->MAP)
      {
        const double  reso = 1/std::sqrt(ssqr);
        const bool    cent = reflections->work_cent(miller);
        const double& resn = reflections->get_flt(labin::RESN,r);
        const double& inat = reflections->get_flt(labin::INAT,r);
        const double& siginat = reflections->get_flt(labin::SIGINAT,r);
#if 0
        std::set<bool> loop_ignore_tncs = { false, tncs_modelled };
        //if we ask to flag outliers ignoring tncs we need a loop
        //first flag those that are outliers with tncs
        //then flag outliers without the tncs
        //this is because some maths will fail if we don't exclude one and some with other
        //we could be more sophisticated and check for each usage case AJM TODO
#else
        //no looping
        std::set<bool> loop_ignore_tncs = { false };
#endif
        for (auto loop : loop_ignore_tncs)
        {
          const double  teps = loop ? 1 : reflections->get_flt(labin::TEPS,r);
          const double  esn = fn::pow2(resn)*teps;
          double eosqr   = inat/esn;
          double sigesqr = siginat/esn;
          double prob = pintensity.probability(cent,eosqr,sigesqr);
          if (prob <= OUT_PROB)
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::WILSONNAT); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::WILSONNAT)); //creates
            reflprobs[r].set_probability(friedel::NAT,prob);
          }
          if ( (cent and (sigesqr > sigesqr_cent) ) or
               (!cent and (sigesqr > sigesqr_acent) ))
          {
            if (reflprobs.count(r))
              reflprobs[r].CODES.insert(rejected::LOINFONAT); else
              reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFONAT)); //creates
            reflprobs[r].set_sigessqr(sigesqr);
          }
        }
      }
      else //MAP
      {
        const double  reso = 1/std::sqrt(ssqr);
        const bool    cent = reflections->work_cent(miller);
        const double  teps = reflections->get_flt(labin::TEPS,r);
        const double  resn = reflections->get_flt(labin::RESN,r);
        const double  esn = fn::pow2(resn)*teps;
        likelihood::mapELLG mapELLG;
        double log2 = std::log(2.);
        const double Emean = reflections->get_flt(labin::FEFFNAT,r)/resn;
        const double dobs = reflections->get_flt(labin::DOBSNAT,r);
        double prob = mapELLG.get(Emean, dobs, false)/log2;
        if (prob <= OUT_INFO)
        {
          if (reflprobs.count(r))
            reflprobs[r].CODES.insert(rejected::LOINFONAT); else
            reflprobs.emplace(r,pod::probtype(miller,reso,rejected::LOINFONAT)); //creates
          reflprobs[r].set_probability(friedel::NAT,prob);
        }
      }
    }
    return reflprobs;
  }

}//phasertng
