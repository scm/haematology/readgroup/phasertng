//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Seek_class__
#define __phasertng_dag_Seek_class__
#include <phasertng/main/Identifier.h>

namespace phasertng {
namespace dag {

  class Entry;

  class Seek
  { //component of the full composition to search for (-> poses)
    public:
      Identifier  IDENTIFIER;
      dag::Entry* ENTRY = nullptr;
      char        STAR = '?';

      void set_search() { STAR = '*'; }
      void set_found() { STAR = '+'; }
      void set_missing() { STAR = '-'; }
      bool is_search() const { return STAR == '*'; }
      bool is_found() const { return STAR == '+'; }
      bool is_missing() const { return STAR == '-'; }
      int  order() const { return (is_found() ? 0 : (is_search() ? 1 : 2)); }
    //ascii code for allowed star values
    //'*' 42 search
    //'+' 43 found
    //',' 44 search
    //'-' 45 not found

    public:
      double seek_increases = 0; //seek_increases

    // Overloaded operators
    bool
    operator<( const Seek& rhs ) const
    { //for std::stable_sort
      return order() < rhs.order();
    }

    bool operator==(const Seek& other) const
    { return IDENTIFIER == other.IDENTIFIER; }
  };

}} //phasertng
#endif
