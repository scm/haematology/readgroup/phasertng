//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phasertng/main/constants.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <phaser/lib/scattering.h>
#include <boost/assign.hpp>

namespace phaser {

//not in class
bool isElement(std::string scattering_type)
{
  try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(scattering_type,true);
  }
  catch (std::exception const& err) {
    return false;
  }
  return true;
}

/*
double scattering::average_scattering_at_ssqr(double ssqr)
{
  double total_scat(0);
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
  {
    assert(iter->second.atomic_number);
    total_scat += fraction_by_atomtype(iter->first)*
                  fn::pow2(fetch[iter->first].at_d_star_sq(ssqr)/iter->second.atomic_number);
  }
  return total_scat;
}

double scattering::PERC(std::string atomid)
{
 // atomid = stoup(atomid);
  return (atomtypes.find(atomid) == atomtypes.end()) ? 0 : atomtypes[atomid].percent;
}

double scattering::avScat()
{
  double scat(0);
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
    if (iter->second.weight)
    {
      scat += (iter->second.percent/iter->second.weight)*scitbx::fn::pow2(iter->second.atomic_number);
    }
  return scat;
}

double scattering::fraction_by_atomtype(std::string atomid)
{
  if (!PERC(atomid)) { return 0; }
  cctbx::eltbx::tiny_pse::table atm(atomid);
  return PERC(atomid)/atm.weight()/total_weight;
}
*/

}
