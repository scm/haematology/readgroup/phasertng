//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_fft_real_to_complex_3d_class__
#define __phasertng_fft_real_to_complex_3d_class__
#include <phasertng/math/fft/fftbase.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <scitbx/array_family/accessors/c_grid_padded.h>

namespace phasertng { namespace fft {

  class real_to_complex_3d : public fftbase
  {
    public:
      real_to_complex_3d() {}
      real_to_complex_3d(cctbx::sgtbx::space_group_type, cctbx::uctbx::unit_cell);
      bool anomalous_flag  = false;
      scitbx::fftpack::real_to_complex_3d<double> rfft;
      void allocate();
      cctbx::af::c_interval_grid<3>::index_type n_grid() const { return rfft.n_real(); }

      //setters
 //     real_to_complex_3d& shannon(double s) { Shannon = s; return *this; }
 //     real_to_complex_3d& translation_search_symmetry_flags( bool, bool );
  //    real_to_complex_3d& mandatory_factor(int i) { mandatory_factors = {i,i,i}; return *this; }

      af::versa<double, af::c_grid<3> > real_map_unpadded;
    //private:
      af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref;
      af::versa<double, af::c_grid_padded_periodic<3> > real_map;
     // af::const_ref<double, af::c_grid_padded_periodic<3> > map_data_const_ref;
     //aka cctbx::xray::sampled_model_density<>::real_map_type real_map;

    public:
      af::versa<double, af::c_grid<3> >
      backward( af::shared<millnx>, af::shared<cmplex>);

      af::const_ref<cmplex, af::c_grid_padded<3> >
      forward(af::shared<cctbx::xray::scatterer<double> >,
              std::string FORMFACTOR_TABLE_NAME = "WK1995");

      void
      WriteMap(std::string);
  };
}}
#endif
