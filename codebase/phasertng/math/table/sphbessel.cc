#include <phasertng/math/table/sphbessel.h>
#include <scitbx/constants.h>
#include <complex>
#include <algorithm>

//#define __DO_ELMN_NUMERICAL_STABILITY_TEST__
#define PHASERTNG_SPHARM_LOOKUP
#ifdef PHASERTNG_SPHARM_LOOKUP
#include <phasertng/math/table/cos_sin.h>
#endif

namespace phasertng {
#ifdef PHASERTNG_SPHARM_LOOKUP
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
#endif

namespace table {

  sphbessel::sphbessel(int lmax)
  {
    lmax = 120; //hardwire but separate from default constructor
    m_table.resize(3*lmax);
    sqrt_table.resize(3*lmax);
    normalization.resize(3*lmax);
    double TWO(2.0);
    for (int i = 0; i < 3*lmax; i++)
    {
      sqrt_table[i] = std::sqrt(TWO*i+1);
      normalization[i] = std::sqrt((TWO*i+1)/scitbx::constants::four_pi);
      int SD = 6;
      double sqrti = std::sqrt(static_cast<double>(i)); //different from sqrt_table
      //double sqrti = std::sqrt(TWO*i+1); //doesn't make much difference which is disturbing
      m_table[i] = 2*((i+static_cast<int>(SD*sqrti))/2);
    }
  }

  double
  sphbessel::get(int n, double x) const
  {
    /* Calculates spherical Bessel polynomials from the recurrence relation
       j(n)=(2n-1)/x j(n-1)-j(n-2)
       For x>n use the forward recurrence
       For x<n use the backward recurrence
       Written 10.01.2002
       Tested for stability 10.01.2002
    */

    double j0=0;
    double j1=0;
    double jn=0;
    double norm=0;
    double res=0;
    double sinxonx;

    j0=0; j1=0; jn=0; norm=0; res=0;
    if (x<SMALLNUM) {
      if (n==0) return 1;
      else return 0;
    }

    if (x>static_cast<double>(n)) {
#ifdef PHASERTNG_SPHARM_LOOKUP
      //std::complex<double> cos_sin(std::cos(x),std::sin(x));
      std::complex<double> cos_sin = tbl_cos_sin.get(x/scitbx::constants::two_pi); //multiplies by two_pi internally
      sinxonx = (std::imag(cos_sin)/x);
#else
      sinxonx = sin(x)/x;
#endif
      j0=sinxonx;
      if (n==0) return j0;
      else {
#ifdef PHASERTNG_SPHARM_LOOKUP
        j1=sinxonx/x-std::real(cos_sin)/x;
#else
        j1=sinxonx/x-cos(x)/x;
#endif
        if (n==1) return j1;
        else {
        for (int i=2; i<=n; i++) {
          jn = (2*i-1)/x*j1-j0;   // recurrence
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
            if (jn > BIGNUM)
            return 0;
#endif
          j0 = j1;  //increase index by one
          j1 = jn;
        }
        return jn;
        }
      }
    }
    else {   //recurrence is unstable use inverse recurrence
      int m = m_table[n]; //u
      jn = 0.0;
      j1 = 1.0;
      res = 0.0;
      for (int i=m; i>0; i--) {
        j0 = (2*i+1)/x*j1-jn; // inverse recurrence
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
        if (j0 > BIGNUM)
        return 0;
#endif
        jn = j1;
        j1 = j0;
        if (i==n) res = jn; // this is the one we want but unnormalized
      }
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
      if (j1 > BIGNUM)
      return 0;
#endif
      //****TEST FOR J1 = 0! ***
      if (j1 == 0) return 0;
      norm=(sin(x)/x)/j1; // compare j(0) calc with the flt one
      //sin function only called, no lookup
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
      double absnorm(fabs(norm));
      if (absnorm > 1)
        if (std::log(fabs(res)) > BIGLOG-std::log(absnorm))
          return 0;
#endif
      res *= norm; // and normalize
      return res;
    }
    return 0;
  }


  std::vector<double>
  sphbessel::forward_recurrance(int nmax,double x) const
  {
    nmax = std::min(nmax,static_cast<int>(std::ceil(x))); //up to the max we want, where stable
    nmax = std::max(2,nmax); //j0j1 exact
    std::vector<double> sph_bessel(nmax);
    if (x<SMALLNUM)
    {
      sph_bessel[0] = 1;
      for (int n = 1; n < nmax; n++)
        sph_bessel[n] = 0;
    }
    else
    {
      //foward recurrance relation where stable
      std::complex<double> cos_sin(std::cos(x),std::sin(x));
      double sinxonx = std::imag(cos_sin)/x;
      double cosxonx = std::real(cos_sin)/x;
      sph_bessel[0] = sinxonx;
      sph_bessel[1] = sph_bessel[0]/x - cosxonx;
      for (int i=2; i<nmax; i++)
      {
        sph_bessel[i] = (2*i-1)/x*sph_bessel[i-1]-sph_bessel[i-2];
      }
    }
    return sph_bessel;
  }

  sphbessel sphbessel::inst_;

}}
