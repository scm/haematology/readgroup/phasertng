import sys,os
from libtbx.phil import parse
from phasertng.phil.master_phil_file import build_phil_str
from phasertng.phil.converter_registry import converter_registry
from phasertng import Phasertng
from phasertng import PhaserError
from phasertng.phil.mode_generator import *
from phasertng.phil.phil_to_cards import phil_to_cards
import argparse

def mtz_generator(user_phil_str="",mtzin=""):

  basetng = Phasertng("PHENIX")
  try:
    initparams = build_phil_str(program_name="InputCard")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    user_phil = parse(user_phil_str,converter_registry=converter_registry)
    working_params = master_phil.fetch(sources=[user_phil]).extract()
    if len(mtzin):
      working_params.phasertng.hklin.filename = mtzin
    if working_params.phasertng.suite.database is None:
      working_params.phasertng.suite.database = "."
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    dbdir = working_params.phasertng.suite.database
    if dbdir is not None and not os.path.isdir(dbdir):
      os.makedirs(dbdir)

    working_params.phasertng.dag.consecutive = True
    turn_off_output = True
    if turn_off_output:
      working_params.phasertng.suite.write_files = False
      working_params.phasertng.suite.write_data = False
      working_params.phasertng.suite.write_log = False
      working_params.phasertng.suite.write_cards = False
      working_params.phasertng.suite.write_phil = False
    working_params.phasertng.mode = ["imtz","data","aniso","tncso","tncs"]
    cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    basetng.run(cards) #values on cards
    without_mode = False
    basecards = basetng.card_str(without_mode)
    if turn_off_output:
      working_params.phasertng.suite.write_files = False
      working_params.phasertng.suite.write_data = True
      working_params.phasertng.suite.write_log = False
      working_params.phasertng.suite.write_cards = False
      working_params.phasertng.suite.write_phil = False
    working_params.phasertng.mode = ["feff"]
    cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    cards = basecards + "\n" + cards
    basetng.run(cards)
    user_phil = parse(basetng.phil_str(),converter_registry=converter_registry)
    working_params = master_phil.fetch(sources=[user_phil]).extract()
    return working_params.phasertng.hklout.filename

  except AttributeError as e:
    message = "" if str(e) is None else str(e)
    print("Python error: " + __name__ + ": " + message)
    error = PhaserError()
    error.Set("PYTHON",message)
  except Exception as e:
    message = "" if str(e) is None else str(e)
    print("Python error: " + __name__ + ": " + message)
    error = PhaserError()
    error.Set("PYTHON",message)
  return basetng

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--mtzin",help="mtz file",type=str,required=True)
  args = parser.parse_args()
  user_phil = ""
  if args.phil is not None:
    user_phil = args.phil.read()
  mtzout = mtz_generator(user_phil_str=user_phil,mtzin=args.mtzin)
  print("mtzout=",mtzout)
