//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineRM_class__
#define __phasertng_RefineRM_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/includes.h>
#include <phasertng/main/pointers.h>
#include <phasertng/math/likelihood/llgp/function.h>
#include <phasertng/math/likelihood/llgp/helper.h>
#include <unordered_map>
#include <unordered_set>
#include <phasertng/dag/Node.h>
#include <map>
#include <phasertng/main/constants.h>

namespace phasertng {

class RefineRM : public dtmin::RefineBase
{
  private: //members
    bool   REFINE_ROTN = true;
    bool   REFINE_TRAN = true;
    bool   REFINE_VRMS = true;
    bool   REFINE_BFAC = true;
    bool   REFINE_CELL = true;

  public: //set externally
    double SIGMA_ROT = 0;
    double SIGMA_TRA = 0;
    double SIGMA_BFAC = 0;
    double SIGMA_DRMS = 0;
    double SIGMA_CELL = 0;
    double CELL_MIN = 1;
    double CELL_MAX = 1;
    double BFAC_MIN = DEF_BFAC_MIN;
    double BFAC_MAX = DEF_BFAC_MAX;
    bool   negvar = false;
    bool   adjusted = false;
    double start_likelihood = -999; //flag
    double final_likelihood = -999; //flag

  private:
    int    NTHREADS = 1;
    bool   USE_STRICTLY_NTHREADS = false;
    sv_dvect3 LARGE_ROTN;
    std::map<Identifier,double> LARGE_CELL;
    std::map<Identifier,double> INITIAL_DRMS;
    //keep the cell scales per modlid though all the same by constraint
    std::map<Identifier,double> INITIAL_CELL;

  private:
    //Parameters derived from Dag node parameters
    dag::Node* NODE;
    std::unordered_map<int,std::unordered_set<int> > TNCS_GROUP_POSES; //map on tgroup
    std::unordered_map<int,double> TNCS_GROUP_BFAC; //map on tgroup
    std::unordered_map<int,Identifier> TNCS_GROUP_MODLID; //map on tgroup
    std::unordered_map<int,double> TNCS_GROUP_FRACSCAT; //map on tgroup
    std::set<int> USED_MODLID; //p

  public: //members
    const_ReflectionsPtr REFLECTIONS;
    sv_bool              SELECTED;
    int                  pfirst = 0;

  public:  //constructor
    RefineRM() : dtmin::RefineBase() {}
    RefineRM(
        const_ReflectionsPtr,
        sv_bool);
    ~RefineRM() throw() {}

  public:
    void init_node(dag::Node*);

  private:
    TargetGradientHessianType
    parallel_combined_tgh(
      bool, //gradient
      bool, //hessian
      int, //beginning
      int //end
    );

    TargetGradientHessianType
    combined_tgh(
        bool, //gradient
        bool, //hessian
        bool  //restraint
      );

  void add_restraint(TargetGradientHessianType&,bool,bool);
  void add_constraint(TargetGradientHessianType&,bool,bool);

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    std::vector<dtmin::Bounds>
    bounds();

    std::vector<dtmin::Reparams>
    reparameterize();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string initial_statistics();
    af_string current_statistics();
    af_string final_statistics();
    af_string cleanup_parameters();

    double maximum_distance_special(
        sv_double&,
        sv_double&,
        sv_bool&,
        sv_double&,
        double&
    );

  public:
    TargetType likelihood();

  public:
    void set_threading(bool,int);
};

} //phasertng
#endif
