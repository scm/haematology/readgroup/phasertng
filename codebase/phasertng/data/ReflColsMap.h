//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ReflColsMap_class__
#define __phasertng_ReflColsMap_class__
#include <phasertng/data/Reflections.h>
#include <phasertng/io/EntryBase.h>
#include <unordered_map>

namespace phasertng {

namespace pod {
  class FastPoseType; //forward declaration
}

typedef std::unordered_map<labin::col,af_double,labin::EnumHash> map_col_flt;
typedef std::unordered_map<labin::col,af_int,labin::EnumHash>    map_col_int;
typedef std::unordered_map<labin::col,af_bool,labin::EnumHash>   map_col_bln;
typedef std::unordered_map<friedel::type,af_bool,friedel::EnumHash>   map_fdl_bln;

class ReflColsMap : public Reflections, public EntryBase
{
  public: //constructors
    ReflColsMap(int=0);
    af_string VanillaReadMtz(std::set<labin::col>);
    void      VanillaWriteMtz(af_bool=af_bool(),std::string="");
    void      VanillaWriteMtzOriginal();
    void      tiny_write_to_disk(sv_bool);
    bool in_memory() { return bool(NREFL > 0); }
    void read_from_disk() { VanillaReadMtz(std::set<labin::col>()); }
    void write_to_disk() { VanillaWriteMtz(); }
    ~ReflColsMap() {} //virtual base

  public: //members
    map_col_flt FLT;
    map_col_int INT; //out,bin are ints
    map_col_bln BLN; //cent,both,plus is boolean
    map_fdl_bln PRESENT; //one flag array for nat,pos,neg

  public: //members
    af_millnx   MILLER;

  public: //python
    ReflColsMap(
        af_string, //get_history
        std::string, //hall spacegroup
        af::double6, //unitcell
        double, //wavelength
        af_millnx, //miller
        std::map<labin::col,af_double>,
        std::map<labin::col,af_int>,
        std::map<labin::col,af_bool>,
        std::map<friedel::type,af_bool> //present
        );
    //Warning! This is not thread-safe if col data is used
    //i.e. the data are stored as columns and only interpreted as row data on the fly
    //AJM TODO change ReflRowPtr to smart pointer and create new row object for each
    //reflection when there is column data used
    //reflection::ReflRowMap   R;

  public:
    void store_new_and_changed_data(
        const_ReflectionsPtr);
    af_string calculate_density(
        const_ReflectionsPtr,
        pod::FastPoseType&,
        const sv_bool&,
        bool=true);
    af_string calculate_map(
        const_ReflectionsPtr,
        pod::FastPoseType&,
        const sv_bool&,
        bool=true);
    double calculate_rfactor(
        const_ReflectionsPtr,
        pod::FastPoseType&,
        const sv_bool&,
        double
        );

  public: //miscellaneous
    void expand_to_p1(bool);
    void reindex(SpaceGroup);
    void reduce_to_space_group(SpaceGroup);
    void set_base_defaults();
    std::map<labin::col,af_double> get_map_flt();
    std::map<labin::col,af_int> get_map_int();
    std::map<labin::col,af_bool> get_map_bln();
    std::map<friedel::type,af_bool> get_map_fdl();
   // void set_data(labin::col col,const double v,const int r);

  public: //setters
    void setup_miller(int);
    void erase_from_reflection(int);
    void setup_mtzcol(type::mtzcol);
    void setup_friedel(friedel::type);
    void setup_labin(labin::col); //default coltypes and name
    void set_present(friedel::type f,const bool v,const int r);
    void set_miller(const millnx v,const int r);
    void set_flt(labin::col col,const double v,const int r);
    void set_int(labin::col col,const int v,const int r);
    void set_bln(labin::col col,const bool v,const int r);

  public: //getters
    const bool   has_rows() const { return false; }
    const bool   get_present(friedel::type,const int r) const;
    const millnx get_miller(const int r) const { return MILLER[r]; }
    const double get_flt(labin::col,const int r) const;
    const bool   get_bln(labin::col,const int r) const;
    const int    get_int(labin::col,const int r) const;

  public: //fast getters
    //this will cause a crash, but is required for compilation
    const reflection::ReflRow* get_row(const int r) const { phaser_assert(false); return nullptr; }

std::pair<double,double> cmplex2polar_deg(cmplex FCALC)
{
  //not pass by reference, as needs to return new value anyway
  double F = std::abs(FCALC);
  double PHI = scitbx::rad_as_deg(std::arg(FCALC));
  while (PHI > 180) PHI -= 360;
  while (PHI <= -180) PHI += 360;
  return {F, PHI};
}

};

} //phasertng

#endif
