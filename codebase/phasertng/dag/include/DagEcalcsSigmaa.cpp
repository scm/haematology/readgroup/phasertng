//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/math/likelihood/llgi/helper.h>
#include <future>
#include <phasertng/src/SpaceGroup.h>

//#define PHASERTNG_DEBUG_WORKING_NODE
//#define PHASERTNG_FAST_INTERPV

namespace phasertng {
namespace dag {

  pod::FastPoseType
  DagDatabase::work_ecalcs_sigmaa(//of working node
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      int plast
    )
  {
    phaser_assert(WORK->HALL.size());
    phaser_assert(WORK->HALL == REFLECTIONS->SG.HALL);
    double ztotalscat = REFLECTIONS->get_ztotalscat();
    phaser_assert(WORK->fraction_scattering(ztotalscat)<=1);
    pod::FastPoseType ecalcs_sigmaa_pose;
    ecalcs_sigmaa_pose.resize(REFLECTIONS->NREFL);
    combined_calc(
        REFLECTIONS,
        SELECTED,
        EPSILON,
        NTHREADS,
        USE_STRICTLY_NTHREADS,
        &ecalcs_sigmaa_pose,
        plast
      );
    ecalcs_sigmaa_pose.precalculated = true;
    return ecalcs_sigmaa_pose;
  }

  void
  DagDatabase::combined_calc(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int NTHREADS,
      bool USE_STRICTLY_NTHREADS,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      int plast
    )
  {
    phaser_assert(NTHREADS> 0);
    phaser_assert(REFLECTIONS->NREFL > 0);
    phaser_assert(REFLECTIONS->NREFL > NTHREADS);
    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      parallel_combined_calc(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              plast
              );
    }
    else
    {
      std::vector<std::future< void > > results;
      std::vector<std::packaged_task< void(
              const_ReflectionsPtr,
              sv_bool,
              const_EpsilonPtr,
              int,
              int,
              pod::FastPoseType*,
              int
              )>> packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }
      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&DagDatabase::parallel_combined_calc, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4,
              std::placeholders::_5,
              std::placeholders::_6,
              std::placeholders::_7
              ));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              plast
              );
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &DagDatabase::parallel_combined_calc, this,
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              plast
              ));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      parallel_combined_calc(
              REFLECTIONS,
              SELECTED,
              EPSILON,
              beg,
              end,
              ecalcs_sigmaa_pose,
              plast
              );
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        //ecalcs_sigmaa_pose filled internally
      }
    }
  }

  void
  DagDatabase::parallel_combined_calc(
      const_ReflectionsPtr  REFLECTIONS,
      sv_bool SELECTED,
      const_EpsilonPtr EPSILON,
      int beg,
      int end,
      pod::FastPoseType* ecalcs_sigmaa_pose,
      int plast
    )
  {
    const SpaceGroup* SG = &REFLECTIONS->SG;
    auto used_modlid = WORK->used_modlid_pose();
    for (auto item : used_modlid)
    {
      auto modlid = this->lookup_entry(item,"");
      bool inmemory = modlid->INTERPOLATION.in_memory();
      if (!inmemory)
      throw Error(err::DEVELOPER,"Interpolation not in memory: " + item.str());
    }
    auto pose_terms = WORK->pose_terms_interpolate();

    int nmol = WORK->TNCS_ORDER;
    double nsymp = SG->group().order_p();

    pod::rsymhkl rsym(REFLECTIONS->SG.NSYMP);
    auto& array_G_DRMS = EPSILON->array_G_DRMS;
    auto& array_G_Vterm = EPSILON->array_G_Vterm;
    millnx miller(0,0,0);
    double dobs(0),ssqr(0),g_drms(0),eps(0);
    for (int r = beg; r < end; r++)
    {
      ecalcs_sigmaa_pose->ecalcs[r] = { 0.0, 0.0 };
      ecalcs_sigmaa_pose->sigmaa[r] = 0;
      if (SELECTED[r])
      {
        //don't use row optimization, works on all reflection data types
        if (!REFLECTIONS->has_rows())
        {
          miller = REFLECTIONS->get_miller(r);
          dobs =   REFLECTIONS->get_flt(labin::DOBSNAT,r);
          ssqr =   REFLECTIONS->get_flt(labin::SSQR,r);
          eps  =   REFLECTIONS->get_flt(labin::EPS,r);
          g_drms = (nmol > 1) ? array_G_DRMS[r] : 1;
        }
        else
        {
          const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
          miller = work_row->get_miller();
          dobs = work_row->get_dobsnat();
          ssqr = work_row->get_ssqr();
          eps =  work_row->get_eps();
          g_drms = (nmol > 1) ? array_G_DRMS[r] : 1;
        }
        //duplicates not included
        // If duplicate symm ops were included, interpE would be added epsn times.
        // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
        REFLECTIONS->SG.symmetry_hkl_primitive(miller,eps,rsym);

        //these are the terms that must be populated for the likelihood calculation
        cmplex DobsSigaEcalc = {0,0};
        double DobsSigaSqr(0);

        for (int p = 0; p < WORK->POSE.size(); p++)
        {
          if (plast == -999 or (plast != -999 and p < plast)) //exclude this from the stored
          {
            const auto& pose = WORK->POSE[p];
            const auto& entry = pose.ENTRY;
            double SigaSqr = entry->INTERPOLATION.rSigaSqr[r]; //cell/grid?? AJM fix
            const double bfac = WORK->POSE[p].BFAC;
            const double fs = REFLECTIONS->fracscat(entry->SCATTERING);
            const double drms = WORK->DRMS_SHIFT[pose.IDENTIFIER].VAL;
            double thisDobsSigaSqr = likelihood::llgi::DobsSigaSqr(
              ssqr, SigaSqr, fs, dobs, drms, bfac, g_drms);
            for (int isym = 0; isym < rsym.unique; isym++) //primitive symops
            {
              const millnx& SymHKL = rsym.rotMiller[isym];
              dvect3 RotSymHKL = pose_terms[p].Q1tr*SymHKL;
              cmplex iEcalc = entry->INTERPOLATION.callPt(RotSymHKL); //FourPt
              cmplex scalefac = likelihood::llgi::EcalcTerm(
                  thisDobsSigaSqr,eps,nsymp, //use thisDobsSigaSqr before tncs var correction
                  SymHKL,pose_terms[p].TRA,rsym.traMiller[isym]);
              cmplex thisE = scalefac * iEcalc;
              DobsSigaEcalc += thisE;
            }
            if (nmol > 1)
            { //halfR = false
              thisDobsSigaSqr *= array_G_Vterm[r];
            }
            DobsSigaSqr += thisDobsSigaSqr; //one per pose, includes dobs
          }
        } //poses
        //store values for map calculation etc
        phaser_assert(dobs > 0);
        ecalcs_sigmaa_pose->ecalcs[r] = DobsSigaEcalc/dobs;
        ecalcs_sigmaa_pose->sigmaa[r] = fn::sqrt(DobsSigaSqr);
      } //selected
    } //reflections
  }

}} //phasertng
