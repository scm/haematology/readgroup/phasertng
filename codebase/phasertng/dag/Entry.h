//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Entry_class__
#define __phasertng_dag_Entry_class__
#include <phasertng/main/DogTag.h>
#include <phasertng/data/ReflColsMap.h>
#include <phasertng/io/entry/Interpolation.h>
#include <phasertng/io/entry/Models.h>
#include <phasertng/io/entry/ModelsFcalc.h>
#include <phasertng/io/entry/Variance.h>
#include <phasertng/io/entry/Trace.h>
#include <phasertng/io/entry/Atoms.h>
#include <phasertng/io/entry/Monostructure.h>
#include <phasertng/io/entry/Coordinates.h>
#include <phasertng/io/entry/Ensemble.h>

namespace phasertng {
namespace dag {

  class Entry
  {
    public:
      DogTag      DOGTAG;
      bool        ATOM = false;
      bool        HELIX = false;
      bool        MAP = false;
      int         SCATTERING = 0;
      double      MOLECULAR_WEIGHT = 0;
      dvect3      PRINCIPAL_TRANSLATION = {0,0,0};
      dmat33      PRINCIPAL_ORIENTATION = {1,0,0,0,1,0,0,0,1};
      double      MEAN_RADIUS = 0;
      double      VOLUME = 0;
      dvect3      CENTRE = {0,0,0};
      dvect3      EXTENT = {0,0,0};
      std::string POINT_GROUP_SYMBOL = "1";
      sv_dvect3   POINT_GROUP_EULER = sv_dvect3(1,dvect3(0,0,0));
      sv_double   VRMS_START = sv_double(1,0);
      double      DRMS_MIN = 0;
      double      DRMS_MAX = 0;
      double      HIRES = 0;
      double      LORES = DEF_LORES;
      std::set<std::string> DATA = std::set<std::string>();
      //also add to master_node_file.py
      void copy_header(Entry&);
      //objects below
      // know if they are in memory (in_memory() function) and
      // know the name of the file on disk that they are built from
      //these are filled in Phasertng::setup_entries
      //could use a map based on the virtual base class for each entry
      // std::map<entry::data,EntryBase*> STORED;
      //but then must create new object
      //not all of these will be filled for all database entries
      //the entries are not subtyped
      Models        IMODELS; //the input ensemble models before b-factor
      Models        MODELS; //the input ensemble models after b-factor
      Atoms         SUBSTRUCTURE; //the heavy atom substructure with reject etc flags
      Coordinates   COORDINATES; //built from monostructures, placed with sg and uc
      Trace         TRACE; //includes point group information, ca or hexgrid
      Monostructure MONOSTRUCTURE; //the best structure for any given ensemble, unplaced no sg uc
      ModelsFcalc   IFCALCS; //structure factors calculated from ensemble before b-factor
      ModelsFcalc   FCALCS; //structure factors calculated from ensemble after b-factor
      Variance      VARIANCE; //structue factors with variances calculated from ensembles
      Interpolation INTERPOLATION; //Ensemble structure factors ready for interpolation
      Ensemble      DECOMPOSITION; //Ensemble structure factors ready for decomposition
      ReflColsMap   DENSITY; //the fwt map coefficients
      ReflColsMap   GRADIENT; //the llg map (as coefficients)

    public:
    //  bool generic_bool = false;

    public:
      Entry() {}
      Entry(DogTag dogtag)  : DOGTAG(dogtag) {}

      std::set<entry::data> get_data_choice_selection() const;
      std::string get_data_choice_selection_str() const;
      std::string get_data_choice_selection_int() const;
      void        set_data_choice_selection(std::string);
      void        set_data_choice_selection_int(std::vector<int>);
      double fraction_scattering(double ts) const { return SCATTERING/ts; }
      std::pair<double,double> clustang(
         double res,double samp,bool coiledcoil, bool dupl, double helixf,
         int highorderaxis);
      af_string logEntry() const;
      Identifier identify() { return DOGTAG.IDENTIFIER; }
      double sphereOuter(double,double); //decompositionradiusfactor, config hires
      int calculate_lmax(double,double); //lmax for this resolution and sphereOuter

    // Overloaded operators
    bool
    operator<( const Entry& rhs ) const
    { //for std::set
      return DOGTAG < rhs.DOGTAG;
    }

    bool operator==(const Entry& other) const
    { return DOGTAG == other.DOGTAG; }

  };

}
namespace EntryHelper {
      std::pair<double,double> clustang(
         double res,double samp,double helixf,
         int highorderaxis,
         double radius,
         bool finesamp,bool dupl);
      std::pair<double,double> clustdist(double,bool,bool,double,double,double);
      double sphereOuter(double,double); //config hires, limited by lmax
      int calculate_lmax(double,double); //lmax for this resolution and sphereOuter
}

} //phasertng
#endif
