#ifndef __phasertng_composition_type_enum__
#define __phasertng_composition_type_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace composition {
  MakeEnum ( type,
             (UNDEFINED)
             (PROTEIN)
             (NUCLEIC)
             (MIXED)
           );

}} //phasertng

#endif
