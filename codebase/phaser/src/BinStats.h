//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_BINSTATS_CLASS__
#define __PHASER_BINSTATS_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/src/Bin.h>
#include <valarray>

namespace phaser {

class BinStats
{
  public:
    BinStats() {};
    void setBinStats(const BinStats & init)
    {
      NUM_bin.resize(init.numbins());
      FOM_bin.resize(init.numbins());
      HiRes_bin.resize(init.numbins());
      LoRes_bin.resize(init.numbins());
      for (int s = 0; s < init.numbins(); s++) NUM_bin[s] = init.NUM_bin[s];
      for (int s = 0; s < init.numbins(); s++) FOM_bin[s] = init.FOM_bin[s];
      for (int s = 0; s < init.numbins(); s++) HiRes_bin[s] = init.HiRes_bin[s];
      for (int s = 0; s < init.numbins(); s++) LoRes_bin[s] = init.LoRes_bin[s];
     // FOM_bin = init.FOM_bin;
    }
    BinStats(const BinStats & init)
    { setBinStats(init); }
    const BinStats& operator=(const BinStats& right)
    { if (&right != this) setBinStats(right); return *this; }

  public:
    af_float  NUM_bin,FOM_bin,HiRes_bin,LoRes_bin;

  public:
    void      reset(Bin&);
    int       numbins() const;
    int       Num(int);
    floatType FOM(int);
    int       Num();
    floatType FOM();
    void      addFOM(int,floatType,int=1);
    floatType HiRes(int=-999); //flag for full resolution
    floatType LoRes(int=0);

    // Boost.Python pickling
    floatType getBinNum(int s);
    void      setBinNum(int s, floatType v);
    floatType getBinFOM(int s);
    void      setBinFOM(int s, floatType v);
};

} //phaser
#endif
