//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_BIN_CLASS__
#define __PHASER_BIN_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_bins.h>
#include <phaser/src/Cubic.h>

namespace phaser {

class Bin : public data_bins
{
  private:
    double SMIN, SMAX, SDELTA;
    double FnSShell;
    Cubic CUBIC;
    unsigned NUMBINS;

  public:
    Bin(data_bins=data_bins());

  public:
    unsigned  numbins() const { return NUMBINS; }
    unsigned  maxbins() { return MAXBINS; }
    unsigned  minbins() { return MINBINS; }
    unsigned  width()   { return WIDTH; }

    //initialization functions
    void setup(double,double,int);
    bool not_setup();

    //set functions
    void set_minbins(int);
    void set_maxbins(int);
    void set_width(int);
    bool set_numbins(int);
    void set_cubic(double,double,double);
    double LoRes(double);
    double HiRes(double);
    double MidRes(double);
    unsigned get_bin(double);
    float1D LoRes_array();
};

}
#endif
