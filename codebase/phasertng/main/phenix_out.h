//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng__phenix_out__class__
#define __phasertng__phenix_out__class__
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/shared_ptr.hpp>

namespace phasertng {

struct phenix_out {
  virtual ~phenix_out();
  virtual void write(const std::string& text);
  virtual void flush();
};

} //phaser

#endif
