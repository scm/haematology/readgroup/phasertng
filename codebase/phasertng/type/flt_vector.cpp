//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_vector.h>
#include <iomanip>

namespace phasertng {
namespace type {

  flt_vector::flt_vector(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const dvect3 dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void flt_vector::set_value(dvect3 other)
  {
    user = true;
    uvalue = other;
  }

  void flt_vector::set_value(double a, double b, double c)
  {
    user = true;
    uvalue = dvect3(a,b,c);
  }

  void flt_vector::set_value(int i, double a)
  {
    user = true;
    uvalue[i] = a;
  }

  void flt_vector::set_value(const sv_string& vcards,const dvect3& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_vector::is_default() const
  { return uvalue == dephalt; }

  void
  flt_vector::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_vector::to_python()
  { return unparse_parameter(false); }

  void
  flt_vector::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    for (int i = 0; i < 3; i++)
      uvalue[i] = std::stod(txt[i]);
  }

  dvect3
  flt_vector::value_or_default() const
  { return user ? uvalue : dephalt; }

  double
  flt_vector::value_or_default(int i) const
  {
    if (i>2 or i<0) return 0.;
    return user ? uvalue[i] : dephalt[i];
  }

  dvect3
  flt_vector::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  flt_vector::unparse_parameter(bool phil) const
  {
    if (value_or_default() == dvect3(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 3; i++)
      ss << value_or_default()[i] << " ";
    return ss.str();
  }

  std::string
  flt_vector::show_default(bool phil) const
  {
    if (dephalt == dvect3(DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 3; i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
