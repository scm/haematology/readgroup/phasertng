// Matrix class to clone the functionality of TNT::Fortran_Matrix class but
// using a std::vector for the underlying storage and with 0-indexing
// TNT:Fortran_Matrix was row oriented, but this is column oriented - like the
// scitbx versa type.
// MxN matrix
// i.e.
//         j
//       0->N-1
//   0  [     ]
// i |  [     ]
//   v  [     ]
//   M-1[     ]

#ifndef __phasertng_matrix_class__
#define __phasertng_matrix_class__
#include <vector>
#include <scitbx/array_family/shared.h>

//#define PHASERTNG_MATRIX_BOUNDS_CHECK

namespace phasertng {
namespace math
{

template <typename T>
class Matrix {

private:
  int M;               // number of rows
  int N;               // number of columns
  std::vector<T> v;    // underlying vector containing the data

public:
  typedef typename std::vector<T>::value_type value_type;
  typedef typename std::vector<T>::const_iterator const_iterator;
  typedef typename std::vector<T>::iterator iterator;
  void push_back(const value_type& x) { v.push_back(x); }
  iterator begin() { return v.begin(); }
  iterator end() { return v.end(); }
  const_iterator begin() const { return v.begin(); }
  const_iterator end() const { return v.end(); }

public:
  // Constructors
  Matrix () : M(0), N(0), v(0)  {}

  Matrix(int M_, int N_, const T& value = T())
  {
    M = M_;
    N = N_;
    v.resize(M*N); // std::vector resize method.
    for (int s=0; s<v.size(); s++)
      v[s] = value; // Initialise to value.
  }

  Matrix (int M_, int N_, const T* v_)
  // Given a pointer to an array of Ts in row oriented format with the correct stride
  // Absolutely no checks.
  {
    M = M_;
    N = N_;
    v.resize(M*N);
    for (int s=0; s<v.size(); s++)
      v[s] = v_[s];
  }

  scitbx::af::shared<T>
  ref()
  {
    scitbx::af::shared<T> r;
    r.resize(v.size());
    for (int s=0; s<v.size(); s++)
      r[s] = v[s];
    return r;
  }

  // Getters
  int num_rows() const { return M; }
  int num_cols() const { return N; }

  // Functors (element access, const and non-const)
  T& operator() (int i, int j) {
  #ifdef PHASERTNG_MATRIX_BOUNDS_CHECK
    return v.at(i*N + j);
  #else
    return v[i*N + j];
  #endif
  }
  const T& operator() (int i, int j) const {
  #ifdef PHASERTNG_MATRIX_BOUNDS_CHECK
    return v.at(i*N + j);
  #else
    return v[i*N + j];
  #endif
  }
  T& operator[] (int i) {
  #ifdef PHASERTNG_MATRIX_BOUNDS_CHECK
    return v.at(i);
  #else
    return v[i];
  #endif
  }
  const T& operator[](int i) const {
  #ifdef PHASERTNG_MATRIX_BOUNDS_CHECK
    return v.at(i);
  #else
    return v[i];
  #endif
  }

  // Assignment operator
  Matrix<T>& operator= (const Matrix<T> &A)
  {
    if ( !(M == A.num_rows() && N == A.num_cols()) ) // If they are not the same size ...
      resize(A.num_rows(), A.num_cols()); // resize the matrix being assigned to (updates M, N and v).

    for (int i=0; i<v.size(); i++) // Copy the values from A into the matrix.
      v[i] = A.v[i];
    return *this;
  }

  // Resize operation
  void resize (int M_, int N_)
  // Note that the data WILL BE JUNK after the resize
  {
    M = M_;
    N = N_;
    v.resize(M*N); // Note that this is the std::vector resize operation.
  }

  // Resize operation
  void resize (int M_, int N_, T val)
  // Note that the data WILL BE JUNK after the resize
  {
    M = M_;
    N = N_;
    v.resize(M*N,val); // Note that this is the std::vector resize operation.
  }

  int size() const
  {
    return v.size();
  }

  std::vector<T> as_vector() const
  {
    return v;
  }

  void square(std::vector<T> v_)
  {
    int N = std::sqrt(v_.size());
    resize(N,N);
    v = v_;
  }

  // Resize operation
  void fill (const T& value)
  {
    for (int i = 0; i < v.size(); i++)
      v[i] = value;
  }


};

/* ***************************  I/O  ********************************/

template <class T>
std::ostream& operator<<(std::ostream &s, const math::Matrix<T> &A)
{
  for (int i = 0; i < A.size(); i++)
  {
    s << A[i];
    if (i < A.size()-1) s << " ";
  }
  return s;
}

}} //namespace math
#endif
