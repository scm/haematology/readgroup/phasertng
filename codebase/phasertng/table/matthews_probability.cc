#include <phasertng/table/matthews_probability.h>
#include <cmath>
#include <algorithm>
#include <phasertng/main/Assert.h>

namespace phasertng {
namespace table {

//The "new" "new" Matthews coefficients
//Weichenberger CX and Rupp B. Ten years of probabilistic estimates of biocrystal solvent content: New insights via nonparamatric kernel density estimates
double matthews_probability(double hires,double x,composition::type TYPE)
{
  double res[15] = {1.199,1.501,1.65,1.801,1.901,2.001,2.201,2.401,2.601,2.801,3.101,3.501,5.001,5.001,5.001};
  double yo[15] = {1.186,2.944,4.271,5.396,6.440,7.974,10.55,13.04,16.69,18.96,21.67,24.03,26.18,1.768,2.152};
  double xc[15] = {2.160,2.075,2.071,2.070,2.092,2.097,2.110,2.112,2.118,2.112,2.105,2.099,2.095,2.202,3.019};
  double wt[15] = {0.259,0.213,0.208,0.208,0.221,0.223,0.230,0.231,0.235,0.232,0.229,0.226,0.224,0.306,0.623};
  double a[15] = {177.7,887.1,1364.,2201.,2783.,3374.,4064.,4525.,4845.,4942.,4945.,4891.,4853.,75.52,135.4};
  double s[15] = {1.450,0.816,0.723,0.643,0.661,0.625,0.606,0.569,0.555,0.517,0.483,0.461,0.451,0.696,1.882};
  double norm[15] = {195.381210,906.223041,1427.487371,2373.184071,2977.368503,3667.547885,4459.362839,5064.982337,5469.714281,5714.587610,5856.698844,5891.468081,5893.929732,81.300299,186.400964};
  //code for generating matthewsVM
  /*
  std::vector<double> norm(15);
  for (int i = 0; i < 15; i++)
  {
    double maxnorm(0),maxvm(0);
    for (double vm = 2; vm < 3; vm+=0.0000001)
    {
      double z((vm-xc[i])/wt[i]);
      double norm((yo[i]+a[i]*std::exp(-std::exp(-z)-z*s[i]+1)));
      if (norm > maxnorm) { maxnorm = norm; maxvm = vm; }
    }
    std::cout << "if (hires < " << res[i] << ") return " << maxvm << ";" << std::endl;
    norm[i] = maxnorm;
  }
  std::cout << "double norm[15] = {" << dvtos(norm,10,6) << "};" << std::endl;
  std::exit(1);
  */
  if (TYPE == composition::PROTEIN)
  {
    for (int i = 0; i < 13; i++)
    {
      if (hires < res[i])
      {
        double z((x-xc[i])/wt[i]);
        return std::max(0.,std::min(1.,double((yo[i]+a[i]*std::exp(-std::exp(-z)-z*s[i]+1))/norm[i])));
      }
    }
    int j(12);
    double z((x-xc[j])/wt[j]);
    return std::max(0.,std::min(1.,double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j])));
  }
  else if (TYPE == composition::NUCLEIC)
  {
    int j(13);
    double z((x-xc[j])/wt[j]);
    return std::max(0.,std::min(1.,double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j])));
  }
  else if (TYPE == composition::MIXED)
  {
    int j(14);
    double z((x-xc[j])/wt[j]);
    return std::max(0.,std::min(1.,double((yo[j]+a[j]*std::exp(-std::exp(-z)-z*s[j]+1))/norm[j])));
  }
  else
  {
    phaser_assert(false); //developer error
  }
  return 0;
}

double matthews_vm(double hires,composition::type TYPE)
{
  if (TYPE == composition::PROTEIN)
  {
    if (hires < 1.199) return 2.06376;
    if (hires < 1.501) return 2.11831;
    if (hires < 1.65) return 2.13846;
    if (hires < 1.801) return 2.16185;
    if (hires < 1.901) return 2.18349;
    if (hires < 2.001) return 2.20181;
    if (hires < 2.201) return 2.2252;
    if (hires < 2.401) return 2.24226;
    if (hires < 2.601) return 2.25636;
    if (hires < 2.801) return 2.26505;
    if (hires < 3.101) return 2.27165;
    if (hires < 3.501) return 2.274;
    return 2.27337;
  }
  else if (TYPE == composition::NUCLEIC)
  {
    return 2.3129;
  }
  else if (TYPE == composition::MIXED)
  {
    return 2.62506;
  }
  return 2.15; //original - will not core dump
}

}} //namespace phaser
