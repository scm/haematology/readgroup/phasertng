#include <phasertng/main/Assert.h>
#include <phasertng/math/french_wilson.h>
#include <phasertng/math/likelihood/pBijvoet.h>
#include <scitbx/constants.h>
#include <scitbx/math/erf.h>
#include <limits>
#include <algorithm>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
namespace math {

  double pBijvoet::expectedE1GivenE2(double e2, double rhoFFobs) const
  {
    // Expected value of one member of Friedel pair given the other: normalised amplitude
    // rhoFFobs can incorporate effect of measurement error, even correlated
    // Assumptions: rhoFFobs > 0., rhoFFobs <= 1.
    if (rhoFFobs >= 0.999999) // Effectively no anomalous scattering, negligible error in difference
      return e2;
    double rhoFFobssqr = fn::pow2(rhoFFobs);
    double varterm = (1.-rhoFFobssqr);
    double z2 = fn::pow2(e2);
    assert(varterm > 0);
    double X = z2*rhoFFobssqr/(2.*varterm);
    double ee1 = std::sqrt(scitbx::constants::pi/varterm)/2. *
      ( (z2*rhoFFobssqr + varterm) * tbl_ebesseli0.get(X) +
      z2*rhoFFobssqr*tbl_ebesseli1.get(X) );
    return ee1;
  }

  double pBijvoet::expectedZ1GivenZ2(double z2, double rhoFFobs) const
  {
    // Expected value of one member of Friedel pair given the other,
    // before measurement error: normalised intensity
    // Assumptions: rhoFFobs > 0., rhoFFobs <= 1.
    double rhoFFobssqr = fn::pow2(rhoFFobs);
    return 1. + (z2 - 1.)*rhoFFobssqr;
  }

  double pBijvoet::sigmaE1GivenE2(double e2, double rhoFFobs) const
  {
    // Standard deviation of expected value of one member of Friedel pair
    // given the other: normalised amplitude
    // Assumptions: rhoFFobs > 0., rhoFFobs <= 1.
    if (rhoFFobs >= 0.999999) // Effectively no anomalous scattering
      return 0.;
    return std::sqrt(expectedZ1GivenZ2(fn::pow2(e2),rhoFFobs) -
      fn::pow2(expectedE1GivenE2(e2,rhoFFobs)));
  }

  std::pair<double,double> pBijvoet::expectedESIGEnegGivenPair(double zopos,
    double sigzpos, double zoneg, double sigzneg, double absrhoFF) const
  {
    // Expected value and sigma of E- given both observed normalised intensities,
    // used to set numerical integration limits.
    // Approximate (but probably good enough) solution that ignores somewhat
    // nonlinear dependence of <E-> on the uncertain true value of E+
    // Assumptions: absrhoFF > 0., absrhoFF <= 1., sigz1 > 0., sigz2 > 0.
    french_wilson FW;
    double eeneg = FW.expectEFWacen(zoneg,sigzneg);
    double vareeneg = FW.expectEsqFWacen(zoneg,sigzneg) - fn::pow2(eeneg);
    double eepos = FW.expectEFWacen(zopos,sigzpos);
    double vareepos = FW.expectEsqFWacen(zopos,sigzpos) - fn::pow2(eepos);
    double eeneggivenpos = expectedE1GivenE2(eepos,absrhoFF);
    double vareeneggivenpos = fn::pow2(sigmaE1GivenE2(eepos,absrhoFF));
    double denom = 1./vareeneg + 1./(vareepos + vareeneggivenpos);
    double ee = (eeneg/vareeneg
      + eeneggivenpos/(vareepos + vareeneggivenpos)) / denom;
    double sigee = std::sqrt(1. / denom);
    return std::pair<double,double> (ee,sigee);
  }

  std::pair<double,double> pBijvoet::expectedESIGEposGivenZoposEneg(double zopos,
    double sigzpos,double eneg, double absrhoFF) const
  {
    french_wilson FW;
    double eepos = FW.expectEFWacen(zopos,sigzpos);
    double vareepos = FW.expectEsqFWacen(zopos,sigzpos) - fn::pow2(eepos);
    double eeposgivenneg = expectedE1GivenE2(eneg,absrhoFF);
    double vareeposgivenneg = fn::pow2(sigmaE1GivenE2(eneg,absrhoFF));
    double ee = (vareeposgivenneg*eepos + vareepos*eeposgivenneg) /
      (vareeposgivenneg + vareepos);
    double sigee = std::sqrt(vareeposgivenneg*vareepos /
      (vareeposgivenneg + vareepos));
    return std::pair<double,double> (ee,sigee);
  }

  double pBijvoet::pZposZneg(double zpos, double zneg, double rhoFFobs) const
  {
    double varterm = 1. - fn::pow2(rhoFFobs);
    double eprod = std::sqrt(zpos*zneg);
    double exparg =
      -(zpos + zneg - 2*rhoFFobs*eprod) / varterm;
    phaser_assert(exparg < MAXEXPTERM);
    double prob(0.);
    if (exparg > -50.)
    {
      double X = 2*eprod*rhoFFobs/varterm;
      prob = std::exp(exparg) / varterm * tbl_ebesseli0.get(X);
    }
    return prob;
  }

  double pBijvoet::pE1GivenE2(double e1, double e2, double rhoFFobs) const
  {
    // Probability of one normalised amplitude from a Bijvoet pair given the other
    // Account for (possibly correlated) measurement error as part of rhoFFobs,
    // where rhoFFobs = dobs1*dobs2*absrhoFF + rhopm*sqrt((1-dobs1^2)(1-dobs2^2))
    // and rhopm is the correlation between intensity measurement errors for I1 and I2
    double varterm = 1. - fn::pow2(rhoFFobs);
    assert(varterm > 0);
    double exparg = -fn::pow2(e1 - rhoFFobs*e2)/varterm;
    double prob(0.);
    if (exparg > -50.)
    {
      double X = 2*rhoFFobs*e1*e2/varterm;
      prob = 2*e1*std::exp(exparg) / varterm * tbl_ebesseli0.get(X);
    }
    return prob;
  }

  double pBijvoet::pSmallerFeffGivenOther(double feff1, double dobs1,
    double feff2, double dobs2, double RESN, double absrhoFF, double rhopm) const
  {
    // Outlier test: probability that smaller member of Bijvoet pair can be that
    // small or even smaller
    // RESN is sqrt(eps*SigmaN), i.e. sqrt of expected intensity
    // absrhoFF is complex correlation coefficient
    // rhopm is the correlation between intensity measurements for I1 and I2
    double esmall = std::min(feff1,feff2)/RESN;
    double elarge = std::max(feff1,feff2)/RESN;
    double rhoFFobs = dobs1*dobs2*absrhoFF +
      rhopm*std::sqrt( (1. - fn::pow2(dobs1)) * (1. - fn::pow2(dobs2)) );
    double prob = 0.;
    int npts = 100; // Enough for reasonable accuracy in numerical integral
    double de = esmall/npts;
    for (int i=0; i<npts; i++)
      prob += de*pE1GivenE2(de*(i+0.5),elarge,rhoFFobs); // Midpoint rule
    return prob;
  }

  double pBijvoet::pLargerFeffGivenOther(double feff1, double dobs1,
    double feff2, double dobs2, double RESN, double absrhoFF, double rhopm) const
  {
    // Outlier test: probability that larger member of Bijvoet pair can be that
    // large or even larger
    // RESN is sqrt(eps*SigmaN)
    // absrhoFF is complex correlation coefficient
    // rhopm is the correlation between intensity measurements for I1 and I2
    double esmall = std::min(feff1,feff2)/RESN;
    double elarge = std::max(feff1,feff2)/RESN;
    double rhoFFobs = dobs1*dobs2*absrhoFF +
      rhopm*std::sqrt( (1. - fn::pow2(dobs1)) * (1. - fn::pow2(dobs2)) );
    double prob = 0.;
    int npts = 100; // Enough for reasonable accuracy in numerical integral
    // Extend numerical integral to largest E consistent with conditional
    // probability of larger E given the smaller one, and ensuring it goes past
    // observed value of larger E
    double eelarge = expectedE1GivenE2(esmall,rhoFFobs);
    double sigeelarge = sigmaE1GivenE2(esmall,rhoFFobs);
    double ehigh = std::max(eelarge + 6.*sigeelarge,elarge + 2.*sigeelarge);
    double de = (ehigh - elarge)/npts;
    for (int i=0; i<npts; i++)
      prob += de*pE1GivenE2(elarge + de*(i+0.5),esmall,rhoFFobs); // Midpoint rule
    return prob;
  }

}}
