//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <cctbx/eltbx/tiny_pse.h>

namespace phasertng {

  //Substructure_determination
  void Phasertng::runSSM()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::SSA))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ssa)");

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }

    if (input.value__path.at(".phasertng.atoms.filename.").is_default())
    {
      bool is_all_pose_single_atom(true);
      for (const auto& node: DAGDB.NODES)
      {
        if (!node.POSE.size()) is_all_pose_single_atom = false;
        for (const auto& pose : node.POSE)
          if (!pose.ENTRY->ATOM) is_all_pose_single_atom = false;
      }
      int s = input.size(".phasertng.atoms.fractional.site.");
      if (!is_all_pose_single_atom and !s)
      throw Error(err::INPUT,"No substructure coordinates");
    }
    else if (DAGDB.size() > 1)
    {
      throw Error(err::INPUT,"Number of nodes greater than 1 for substructure preparation from filename");
    }

    DAGDB.shift_tracker(hashing(),input.running_mode);

    int n(1),N(DAGDB.size());
    for (int i = 0; i < DAGDB.size(); i++)
    {
      std::string nN = nNtos(n++,N);
      logUnderLine(out::LOGFILE,"Substructure Atoms"+nN);
      auto& node = DAGDB.NODES[i];
      Atoms atoms;
      atoms.SG = REFLECTIONS->SG;
      atoms.UC = REFLECTIONS->UC; //before AddAtoms
      if (!input.value__path.at(".phasertng.atoms.filename.").is_default())
      {
        atoms.SetFileSystem(input.value__path.at(".phasertng.atoms.filename.").value_or_default());
        if (atoms.has_read_errors())
        throw Error(err::FATAL,atoms.read_errors());
        atoms.REFLID = REFLECTIONS->reflid();
        logTab(out::LOGFILE,"Atoms read from file: " + atoms.qfstrq());
        atoms.read_from_disk();
        if (atoms.has_read_errors())
          throw Error(err::INPUT,atoms.read_errors());
      }
      else if (input.size(".phasertng.atoms.fractional.site.") > 0)
      {
        logTab(out::LOGFILE,"Atoms converted from fractional coordinates");
        int s = input.size(".phasertng.atoms.fractional.site.");
        pod::xtra::scatterer next_xtra;
        for (int i = 0; i < s; i++)
        {
          auto atm  = input.array__string.at(".phasertng.atoms.fractional.scatterer.").at(i).value_or_default();
          auto site = input.array__flt_vector.at(".phasertng.atoms.fractional.site.").at(i).value_or_default();
          auto occu = input.array__flt_number.at(".phasertng.atoms.fractional.occupancy.").at(i).value_or_default();
          PdbRecord card;
          card.set_element(atm);
          card.X = REFLECTIONS->UC.cctbxUC.orthogonalization_matrix()*site;
          card.O = occu;
          atoms.AddAtom(card,next_xtra);
        }
      }
      else
      {
        logTab(out::LOGFILE,"Atoms converted from poses");
        logTabArray(out::LOGFILE,node.logPose("Atoms"));
        pod::xtra::scatterer next_xtra;
        for (auto& pose : node.POSE)
        {
          PdbRecord card;
          phaser_assert(pose.ENTRY->PRINCIPAL_TRANSLATION == dvect3(0,0,0));
          card.X = REFLECTIONS->UC.cctbxUC.orthogonalization_matrix()*pose.FRACT;
          card.set_element(pose.ENTRY->identify().tag());
          atoms.AddAtom(card,next_xtra);
        }
      }
      logTab(out::LOGFILE,"Number of atoms: " + itos(atoms.size()));

      //set the scatterer
      if (!input.value__string.at(".phasertng.substructure.preparation.scatterer.").is_default())
      {
        auto scat = input.value__string.at(".phasertng.substructure.preparation.scatterer.").value_or_default();
        atoms.change_scatterers(scat);
        logTab(out::LOGFILE,"Atom scattering type changed: " + scat);
      }

      //set the b-factor of the sites to the wilson B for initial values
      if (input.value__boolean.at(".phasertng.substructure.preparation.wilson_bfactor.").value_or_default())
      {
        out::stream where = out::LOGFILE;
        logTab(where,"Wilson B-factor: " + dtos(REFLECTIONS->WILSON_B_F));
        double expected = input.value__flt_number.at(".phasertng.substructure.content_analysis.delta_bfactor.").value_or_default();
        logTab(where,"Expected B-factor: Wilson+(" + dtos(expected));
        auto io_bmin = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
        logTab(where,"Minimum B-factor: " + dtos(io_bmin));
        auto b_iso = std::max(REFLECTIONS->WILSON_B_F-expected,io_bmin);
        auto u_iso = cctbx::adptbx::b_as_u(b_iso);
        auto u_star = cctbx::adptbx::u_iso_as_u_star(REFLECTIONS->UC.cctbxUC,u_iso);
        logTab(where,"Atom B-factor changed: " + dtos(b_iso));
        for (auto& atom : atoms.XRAY)
        {
          if (atom.flags.use_u_aniso_only())
            atom.u_star = u_star;
          else
            atom.u_iso = u_iso;
        }
      }

      //Identifier modlid = DAGDB.NODES.front().TRACKER.ULTIMATE;
      Identifier modlid = node.TRACKER.ULTIMATE;
      logTab(out::LOGFILE,"Identifier: " + modlid.str());
      atoms.REFLID = REFLECTIONS->reflid();
      atoms.TIMESTAMP = TimeStamp();

      auto entry = DAGDB.add_entry(modlid);
      atoms.MODLID = modlid;
      //substructure in part, molecular replacement solution in full
      node.PART.PRESENT = true;
      node.PART.IDENTIFIER = modlid;
      node.PART.ENTRY = entry;
      entry->SUBSTRUCTURE = atoms;
      entry->SCATTERING = 0;
      entry->MOLECULAR_WEIGHT = 0;
      for (auto comp : node.COMPOSITION)
      {
        scatterers.insert(comp.first);
        double atomic_number(0),weight(0);
        try {
          cctbx::eltbx::tiny_pse::table cctbxAtom(comp.first);
          atomic_number = cctbxAtom.atomic_number();
          weight = cctbxAtom.weight();
        }
        catch (std::exception const& err)  {}
        entry->SCATTERING += comp.second*scitbx::fn::pow2(atomic_number);
        entry->MOLECULAR_WEIGHT += comp.second*weight;
      }
      auto set_of_atomtypes = atoms.set_of_atomtypes();
      for (auto item : set_of_atomtypes)
        if (!scatterers.has_atomtype(item))
          throw Error(err::INPUT,"Substructure atom type not in composition");

      logTabArray(out::VERBOSE,atoms.logAtoms(""));
      logTabArray(out::LOGFILE,atoms.logFracs("fractional coordinates"));

      out::stream where = out::LOGFILE;
      logUnderLine(where,"Database Entries");
      entry->SUBSTRUCTURE.SetFileSystem(DataBase(),entry->DOGTAG.FileEntry(entry::SUBSTRUCTURE_PDB));
      logFileWritten(where,WriteFiles(),"Substructure",entry->SUBSTRUCTURE);
      if (WriteFiles()) entry->SUBSTRUCTURE.write_to_disk();
      logBlank(out::LOGFILE);
    }
  }

} //phasertng
