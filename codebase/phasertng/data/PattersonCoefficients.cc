//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/PattersonCoefficients.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/main/Error.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <phasertng/math/likelihood/isad/SADeLLG.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <phasertng/math/table/ebesseli0.h>
#include <phasertng/math/table/ebesseli1.h>
#include <phasertng/math/table/sim.h>
#include <phaser/src/RefineSAD.h>

namespace phasertng {
extern const table::ebesseli0& tbl_ebesseli0;
extern const table::ebesseli1& tbl_ebesseli1;
extern const table::sim& tbl_sim;
}

namespace phasertng {

  PattersonCoefficients::PattersonCoefficients(
     SpaceGroup SG_,UnitCell UC_,af_millnx miller) :
     SG(SG_),UC(UC_)
  {
    MILLER = miller.deep_copy();
    int NREFL = miller.size();
    IOBS_EQUIVALENT.resize(NREFL);
    SIGIOBS_EQUIVALENT.resize(NREFL);
    PRESENT.resize(NREFL);
    init();
  }

  void PattersonCoefficients::init()
  {
    LL = 0;
    int NREFL = MILLER.size();
    for (int r = 0; r < NREFL; r++)
    {
      IOBS_EQUIVALENT[r] = 0;
      SIGIOBS_EQUIVALENT[r] = 0.01; //small value so some programs don't crash
      PRESENT[r] = false;
    }
    LABOUT = { "I","SIGI" };
  }

  void
  PattersonCoefficients::LLdLL_by_dUsqr_at_0(
      const_ReflectionsPtr REFLECTIONS,
      const sv_bool& SELECTED,
      std::string atomtype,
      double fp,
      double fdp,
      double dBscat,
      likelihood::isad::sigmaa ISAD_SIGMAA
    )
  {
    init();
    cctbx::eltbx::xray_scattering::gaussian FORM_FACTOR =
      cctbx::eltbx::xray_scattering::wk1995(atomtype).fetch();
    double WilsonB = REFLECTIONS->WILSON_B_F;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      MILLER[r] = REFLECTIONS->get_miller(r);
      if (SELECTED[r])
      {
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        if (resn == 0) continue; //because we don't select here, mathematical condition required
        const double feffpos = REFLECTIONS->get_flt(labin::FEFFPOS,r);
        const double feffneg = REFLECTIONS->get_flt(labin::FEFFNEG,r);
        const double dobspos = REFLECTIONS->get_flt(labin::DOBSPOS,r);
        const double dobsneg = REFLECTIONS->get_flt(labin::DOBSNEG,r);
        const double ssqr =    REFLECTIONS->get_flt(labin::SSQR,r);
        const int  s =    REFLECTIONS->get_int(labin::BIN,r);
        const bool cent = REFLECTIONS->get_bln(labin::CENT,r);
        const bool both = REFLECTIONS->get_bln(labin::BOTH,r);
        const bool plus = REFLECTIONS->get_bln(labin::PLUS,r);
        double esn(fn::pow2(resn));
        double LL_at_0(0.),dLL_by_dUsqr(0.);
        double bterm(std::exp(-(WilsonB+dBscat)*ssqr/4.));
        double f0fpB = bterm*(FORM_FACTOR.at_d_star_sq(ssqr) + fp);
        double fdpB = bterm*fdp;
        double f0fpSqr(fn::pow2(f0fpB));
        double fdpSqr(fn::pow2(fdpB));
        double ReFanomSqr(f0fpSqr-fdpSqr);
        double AbsFanomSqr(f0fpSqr+fdpSqr);
        if (both)
        {
          const double FOpos = feffpos;
          const double FOneg = feffneg;
          const double ABSRHOFF = ISAD_SIGMAA.absrhoFF_bin[s];
          const double RHOPM = ISAD_SIGMAA.rhopm_bin[s];
          double FOposSqr(fn::pow2(FOpos));
          double FOnegSqr(fn::pow2(FOneg));
          double dobspos2(fn::pow2(dobspos));
          double dobsneg2(fn::pow2(dobsneg));
          double rhoFFobs = dobspos*dobsneg*ABSRHOFF +
            RHOPM*std::sqrt((1.-dobspos2)*(1.-dobsneg2));
          double rhoFFobsSqr(fn::pow2(rhoFFobs));
          double Sigma0(esn*(1.-fn::pow2(rhoFFobs)));
          double X(2*FOpos*FOneg*rhoFFobs/Sigma0);
          LL_at_0 = std::log(4*FOpos*FOneg*tbl_ebesseli0.get(X)/(esn*Sigma0)) -
            fn::pow2(FOpos - FOneg)/Sigma0 - 2*FOpos*FOneg/(esn*(1.+rhoFFobs));
          dLL_by_dUsqr =
            (dobspos2*AbsFanomSqr*(FOposSqr-esn+(FOnegSqr+esn)*rhoFFobsSqr) +
             dobsneg2*AbsFanomSqr*(FOnegSqr-esn+(FOposSqr+esn)*rhoFFobsSqr) -
             2*dobspos*dobsneg*ReFanomSqr*rhoFFobs*(FOposSqr+FOnegSqr-Sigma0) -
             2*FOpos*FOneg*((dobspos2+dobsneg2)*AbsFanomSqr*rhoFFobs -
              dobspos*dobsneg*ReFanomSqr*(1.+rhoFFobsSqr))*tbl_sim.get(X)) /
            fn::pow2(Sigma0);
        }
        else if (cent) // double-check centric and singleton cases later
        {
          double FOsqr = fn::pow2(feffpos);
          double dobs2 = fn::pow2(dobspos);
          LL_at_0 = -FOsqr/(2.*esn) - std::log(scitbx::constants::pi*esn/2.)/2.;
          dLL_by_dUsqr = AbsFanomSqr*dobs2*(FOsqr-esn) / (2*fn::pow2(esn));
        }
        else
        {
          const double& FO = (plus ? feffpos : feffneg);
          if (FO > 0) //because we have not made a selection, mathematical condition required
          {
            double dobs2 = fn::pow2(plus ? dobspos : dobsneg);
            double FOsqr = fn::pow2(FO);
            LL_at_0 = -FOsqr/esn + std::log(2.*FO/esn);
            dLL_by_dUsqr = AbsFanomSqr*dobs2*(FOsqr-esn) / fn::pow2(esn);
          }
        }
        IOBS_EQUIVALENT[r] = dLL_by_dUsqr;
        PRESENT[r] = true;
        LL += LL_at_0;
      }
      else
      {
        IOBS_EQUIVALENT[r] = 0;
        PRESENT[r] = false;
      }
    }
  }

  void
  PattersonCoefficients::expectZHsigmaZH(
      const_ReflectionsPtr REFLECTIONS,
      const sv_bool& SELECTED,
      likelihood::isad::sigmaa ISAD_SIGMAA
    )
  {
    LL = 0;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      MILLER[r] = REFLECTIONS->get_miller(r);
      if (SELECTED[r])
      {
        const double resn = REFLECTIONS->get_flt(labin::RESN,r);
        if (resn == 0) continue; //because we don't select here, mathematical condition required
        const millnx miller = REFLECTIONS->get_miller(r);
        const double feffpos = REFLECTIONS->get_flt(labin::FEFFPOS,r);
        const double dobspos = REFLECTIONS->get_flt(labin::DOBSPOS,r);
        const double feffneg = REFLECTIONS->get_flt(labin::FEFFNEG,r);
        const double dobsneg = REFLECTIONS->get_flt(labin::DOBSNEG,r);
        const double teps = REFLECTIONS->get_flt(labin::TEPS,r);
        const double eps = REFLECTIONS->get_flt(labin::EPS,r);
        const int  s = REFLECTIONS->get_int(labin::BIN,r);
        const bool cent = REFLECTIONS->get_bln(labin::CENT,r);
        const bool both = REFLECTIONS->get_bln(labin::BOTH,r);
        const bool plus = REFLECTIONS->get_bln(labin::PLUS,r);
        double esn = fn::pow2(resn);
        const double SigmaN = ISAD_SIGMAA.SigmaN_bin[s];
        // SigmaH/sigmaHH must equal SigmaQ/sigmaQQ here: evaluating actual substructure
        double RhoH = ISAD_SIGMAA.SigmaH_bin[s]/SigmaN; // ratio
        cmplex rhoHH = ISAD_SIGMAA.sigmaHH_bin[s]/SigmaN;
        double thiseZH(0.), thissigmaZH(0.), thiseEH(0.), thissigmaEH(0.);
        cmplex thisdphiterm(0.);
        if (both)
        {
          const double absrhoFF = ISAD_SIGMAA.absrhoFF_bin[s];
          const double rhopm = ISAD_SIGMAA.rhopm_bin[s];
          // Sigma*_bin variables correspond to specified composition and inferred
          // relative anomalous scatterer content. Put on scale of actual data for
          // calcs, including eps-factor for REFLECTIONS reflection.
          double rescale = esn/SigmaN;
          //double sSigmaP = rescale*ISAD_SIGMAA.SigmaP_bin[s];
          cmplex ssigmaPP = rescale*ISAD_SIGMAA.sigmaPP_bin[s];
          double sSigmaQ = rescale*ISAD_SIGMAA.SigmaQ_bin[s];
          cmplex ssigmaQQ = rescale*ISAD_SIGMAA.sigmaQQ_bin[s];
          // Compute <ZH> using probabilities for perfect model
          double dobspos2 = fn::pow2(dobspos);
          double dobsneg2 = fn::pow2(dobsneg);
          double SigmaNeg = esn*(1. - dobsneg2*RhoH);
          const cmplex rhoFF = (ssigmaPP + ssigmaQQ)/esn;
          static double maxExpArg(std::log(std::numeric_limits<double>::max()));
          likelihood::isad::function isad_function(&ISAD_SIGMAA);
          likelihood::isad::tgh_flags flags;
          flags.target(true);
          flags.phsprob(true);
          likelihood::isad::tgh isad_tgh0,isad_tgh;
          isad_tgh0 = isad_function.tgh_null(s, dobspos, feffpos, dobsneg, feffneg, resn, teps, flags);
          double L0;
          PHASER_ASSERT(isad_tgh0.LogLike < maxExpArg); // Shouldn't happen for data screened for BADANOM
          L0 = std::exp(-isad_tgh0.LogLike);
#ifdef PHASERTNG_DEBUG_EZH
          // Check L0 with L0corr in SADeLLG.nb
          std::cout << "XXX L0, call: " << L0 << " L0corr[" << sSigmaP << "," << std::real(ssigmaPP)
          << " + I * (" << std::imag(ssigmaPP) << ")," << sSigmaQ << "," << std::real(ssigmaQQ)
            << " + I * (" << std::imag(ssigmaQQ) << ")," << dobspos << "," << feffpos << ","
            << dobsneg << "," << feffneg << "," << rhopm << ",1]" << std::endl;
#endif
          // Assign phase of theoretical rhoFF to rhoFFobs including the correlation term
          const cmplex rhoFFobs =
            (dobsneg*dobspos*absrhoFF + rhopm*std::sqrt((1.-dobspos2)*(1.-dobsneg2)))
            * rhoFF/std::abs(rhoFF);
          const cmplex sigmaPhi = esn*(rhoFFobs - dobsneg*dobspos*rhoHH);
          const double sigmaPhiSqr = std::norm(sigmaPhi);
          PHASER_ASSERT(SigmaNeg > 0); //divide by zero below
          double SigmaPos = esn*(1. - dobspos2*RhoH) - sigmaPhiSqr/SigmaNeg;

          // Calculate (weighted) scattering factor to put EH-value on scale of total FH
          // Explore possible EH-values to explore possible fhpos/fhneg pairs
          double sRefanom = std::sqrt((sSigmaQ + std::real(ssigmaQQ))/2);
          double sImfanom = std::sqrt((sSigmaQ - std::real(ssigmaQQ))/2);
          cmplex sfanom(sRefanom,sImfanom);
          double emax = 3.7; //Less than 1 part per million that FH will be bigger
          int maxpts(0),ndiv(100);
          double de = emax/ndiv;
          double pnorm(0);
          for (int idiv = 0; idiv < ndiv; idiv++)
          {
            double thisE = (idiv+0.5)*de; //Midpoint rule
            double thisZ = fn::pow2(thisE);
            double thisZsqr = fn::pow2(thisZ);
            cmplex fhpos = thisE*sfanom;
            cmplex fhneg = std::conj(fhpos);
            int nintpts = isad_function.calculate_integration_points(resn, teps,
              dobspos, feffpos, fhpos, dobsneg, feffneg, fhneg, SigmaPos, SigmaNeg);
            maxpts = std::max(maxpts,nintpts);
            double pe = 2*thisE*std::exp(-fn::pow2(thisE)); // Wilson probability
            isad_tgh = isad_function.tgh_acentric_canonical(s,resn,teps,dobspos,feffpos,fhpos,
              dobsneg,feffneg,fhneg,flags,nintpts);
            double LiSAD = std::exp(-isad_tgh.LogLike);
#ifdef PHASERTNG_DEBUG_EZH
            // Check LiSAD with LiSADPerfect in SADeLLG.nb
            if (std::abs(thisE-1) <= de/2)
                std::cout << "XXX LiSAD, call: " << LiSAD << " LiSADPerfect[" << sSigmaP << "," << std::real(ssigmaPP)
                  << " + I * (" << std::imag(ssigmaPP) << ")," << sSigmaQ << "," << std::real(ssigmaQQ)
                  << " + I * (" << std::imag(ssigmaQQ) << ")," << dobspos << "," << feffpos << ","
                  << dobsneg << "," << feffneg << "," << rhopm << "," << teps << "," << std::real(fhpos) << " + I * ("
                  << std::imag(fhpos) << ")," << std::real(fhneg) << " + I * (" << std::imag(fhneg) << "),"
                  << nintpts << "]" << std::endl;
#endif
            double Lratio = LiSAD/L0;
            if (Lratio > 1.e-10)
            {
              double normterm = de * pe * Lratio;
              pnorm += normterm;
              thiseEH += normterm * thisE;
              thisdphiterm += std::polar(normterm * thisE * isad_tgh.FOM , isad_tgh.PHIB);
              thiseZH += normterm * thisZ; // Also use for sigmaEH
              thissigmaZH += normterm * thisZsqr; // Take sqrt later
            }
          }
          if (pnorm > 0)
          {
            thiseEH /= pnorm;
            thiseZH /= pnorm;
            thissigmaEH = std::sqrt(thiseZH - fn::pow2(thiseEH));
            thissigmaZH /= pnorm;
            thissigmaZH = std::sqrt(thissigmaZH - fn::pow2(thiseZH));
            thisdphiterm /= pnorm;
// SHELXD does best with just the both class, not including the centrics and singletons calculated below
//Data that can replace SHELXC output: thisEH, thissigmaEH and phase
            int dphi = std::round(scitbx::rad_as_deg(std::atan2(thisdphiterm.imag(), thisdphiterm.real())));
            double epsscale = std::sqrt(eps*teps);
            if (dphi < 0)
              dphi += 360;
            SHELXC.push_back(pod::shelxc(miller,epsscale*thiseEH,epsscale*thissigmaEH,dphi));
          }
          else
          { // More paranoia: shouldn't happen if data screened for BADANOM
            thiseEH = thissigmaEH = thiseZH = thissigmaZH = 1.;
            thisdphiterm = 0.;
          }
#ifdef PHASERTNG_DEBUG_EZH
          // Check <ZH> and sigma(ZH) with eZHsigmaZH in SADeLLG.nb
          std::cout << "XXX <ZH>, sigma(ZH), call: " << thiseZH << " " << thissigmaZH << " eZHsigmaZH[" << sSigmaP << ","
            << std::real(ssigmaPP) << " + I * (" << std::imag(ssigmaPP) << "),1," << sRefanom
            << "," << sImfanom << "," <<  dobspos << "," << feffpos << "," << dobsneg
            << "," << feffneg << "," << rhopm << "," << teps << "," << maxpts << "," << ndiv << "]"
            << std::endl;
#endif
        }
        else // Either centric or singleton
        {
          double thisZ(0),dobs2(0);
          if (cent)
          {
            thisZ = fn::pow2(feffpos)/esn;
            dobs2 = fn::pow2(dobspos);
            double dosasqr = dobs2 * RhoH;
            double dosaeeff = std::sqrt(dosasqr * thisZ);
            double varterm = 2. - 2. * dosasqr;
            thiseEH = std::sqrt(varterm / scitbx::constants::pi) * std::exp(-(dosasqr * thisZ) / varterm) +
                      dosaeeff * scitbx::math::erf(dosaeeff / std::sqrt(varterm));
          }
          else // Singleton
          {
            const double& Fe = (plus ? feffpos :  feffneg);
            if (Fe > 0) //because we have not made a selection, mathematical condition required
            {
              thisZ = fn::pow2(Fe) / esn;
              dobs2 = fn::pow2(plus ? dobspos : dobsneg);
              double dosasqr = dobs2 * RhoH;
              double varterm = 1. - dosasqr;
              double X = dosasqr * thisZ / (2 * varterm);
              thiseEH = std::sqrt(scitbx::constants::pi / varterm) / 2. *
                        ((1. + dosasqr * (thisZ - 1.)) * tbl_ebesseli0.get(X) +
                         dosasqr * thisZ * tbl_ebesseli1.get(X));
            }
          }
          thiseZH = 1. + dobs2*RhoH*(thisZ - 1.);
          thissigmaZH = std::sqrt((1. - dobs2*RhoH) *
                        (1. + dobs2*RhoH*(2.*thisZ - 1.)));
          thissigmaEH = std::sqrt(thiseZH - fn::pow2(thiseEH));
          thisdphiterm = cmplex(std::sqrt(dobs2 * RhoH * thisZ), 0.);
        }
        if (cent)
        {
          thissigmaZH *= std::sqrt(2.);
        }
        // Patterson-like map coefficient should include epsilon factor
        double coeff = eps*(thiseZH-1); // Origin-removed coefficient
        double sigmacoeff = eps*thissigmaZH;
        IOBS_EQUIVALENT[r] = coeff; //eZH
        SIGIOBS_EQUIVALENT[r] = sigmacoeff; //sigmaZH
        PRESENT[r] = true;
      }
    }
  }

  void
  PattersonCoefficients::anomdiff(
      const_ReflectionsPtr REFLECTIONS)
  {
    double Nsig1 = 3;
    double Nsig2 = 3;
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      MILLER[r] = REFLECTIONS->get_miller(r);
      const double f1 = REFLECTIONS->get_flt(labin::FPOS,r);
      const double f2 = REFLECTIONS->get_flt(labin::FNEG,r);
      const double sigf1 = REFLECTIONS->get_flt(labin::SIGFPOS,r);
      const double sigf2 = REFLECTIONS->get_flt(labin::SIGFNEG,r);
      const bool cent = REFLECTIONS->get_bln(labin::CENT,r);
      const bool both = REFLECTIONS->get_bln(labin::BOTH,r);
      if (!cent and both)
      {
        bool ccp4_fft_exclusion(false);
        if (Nsig1) ccp4_fft_exclusion = ccp4_fft_exclusion and (f1 < Nsig1*sigf1);
        if (Nsig2) ccp4_fft_exclusion = ccp4_fft_exclusion and (f2 < Nsig2*sigf2);
        if (!ccp4_fft_exclusion)
        {
          const double anodiff = std::fabs(f1-f2);
          IOBS_EQUIVALENT[r] = fn::pow2(anodiff);
          PRESENT[r] = true;
        }
      }
    }
  }

  af_millnx
  PattersonCoefficients::get_miller_array()
  {
    af_millnx tmp(nrefl());
    int rr(0);
    for (int r = 0; r < MILLER.size(); r++)
    {
      if (PRESENT[r])
      {
        tmp[rr++] = MILLER[r];
      }
    }
    return tmp;
  }

  af_double
  PattersonCoefficients::get_intensity_array()
  {
    af_double tmp(nrefl());
    int rr(0);
    for (int r = 0; r < MILLER.size(); r++)
    {
      if (PRESENT[r])
      {
        tmp[rr++] = IOBS_EQUIVALENT[r];
      }
    }
    return tmp;
  }

  af_cmplex
  PattersonCoefficients::get_cmplex_intensity_array()
  {
    af_cmplex tmp(nrefl());
    int rr(0);
    for (int r = 0; r < MILLER.size(); r++)
    {
      if (PRESENT[r])
      {
        tmp[rr++] = { IOBS_EQUIVALENT[r], 0 };
      }
    }
    return tmp;
  }

  af_double
  PattersonCoefficients::get_sigma_array()
  {
    af_double tmp(nrefl());
    int rr(0);
    for (int r = 0; r < MILLER.size(); r++)
    {
      if (PRESENT[r])
      {
        tmp[rr++] = SIGIOBS_EQUIVALENT[r];
      }
    }
    return tmp;
  }

  int
  PattersonCoefficients::nrefl()
  {
    int nrefl(0);
    for (int r = 0; r < MILLER.size(); r++)
      if (PRESENT[r]) nrefl++;
    return nrefl;
  }

  void
  PattersonCoefficients::WriteHkl(FileSystem filename)
  {
    // Check whether Miller indices will fit fixed format.
    // Assume F and SIGF correspond to epsilon factor corrected <EH> and sigma(<EH>),
    // <EH> will never be greater than 3.7, so corrected <EH> will never exceed
    // fixed f8.* format used by SHELXD and SHELXE
    for (int r = 0; r < SHELXC.size(); r++)
    {
      std::string h = std::to_string(SHELXC[r].H);
      std::string k = std::to_string(SHELXC[r].K);
      std::string l = std::to_string(SHELXC[r].L);
      if (h.size() > 4 or k.size() > 4 or l.size() > 4)
        throw Error(err::FATAL,"Miller index outside range for shelxc format");
    }
    af_string output;
    for (int r = 0; r < SHELXC.size(); r++)
    {
      //3*4+2*8+4=32
      PHASER_ASSERT(SHELXC[r].F<1.0e08);
      PHASER_ASSERT(SHELXC[r].SIGMA<1.0e08);
      PHASER_ASSERT(SHELXC[r].PHI>=0);
      PHASER_ASSERT(SHELXC[r].PHI<=360);
      char buf[80];
      snprintf(buf,80,
          "%4i%4i%4i%8.4f%8.4f%4i",
            SHELXC[r].H,
            SHELXC[r].K,
            SHELXC[r].L,
            SHELXC[r].F,
            SHELXC[r].SIGMA,
            SHELXC[r].PHI);
       output.push_back(buf);
    }
    filename.write_array(output);
  }

  void
  PattersonCoefficients::LESSTF1(
      phaser::RefineSAD& refine,
      std::string atomtype,
      double dBscat
    )
  {
    init();
    // Structure factors for fixed atoms with unit scattering factor
    auto Fpart = refine.calcFpart();
    LL = refine.LESSTF1(atomtype,dBscat,Fpart,IOBS_EQUIVALENT);
    //set the present array so that the correct values can be extracted
    auto selected = refine.getSelected();
    PHASER_ASSERT(selected.size() == PRESENT.size());
    for (int r = 0; r < selected.size(); r++)
      if (selected[r])
        PRESENT[r] = true;
    PHASER_ASSERT(nrefl());
  }
} //phasertng
