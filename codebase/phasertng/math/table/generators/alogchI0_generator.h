#ifndef __phasertng_table_alogchI0_class__
#define __phasertng_table_alogchI0_class__
#include <vector>
#include <utility>
#include <phasertng/math/table/generators/ebesseli0_generator.h>

namespace phasertng {
namespace table {

class alogchI0
{
  private:
  // Finely-sampled linear interpolation trades a small amount
  // of memory for significant speed improvement, compared with
  // quadratic interpolation
     int    tbllen;
     double argcut1, argcut2, argcut3;
     double argfac1, argfac2, argfac3;
     double invArgFac;
     std::vector<std::pair<double,double> > lnI0Table1;
     std::vector<std::pair<double,double> > lnI0Table2;
     std::vector<std::pair<double,double> > lnI0Table3;
     std::vector<std::pair<double,double> > lnI0InvTable;

   public:
     alogchI0(
         int    tbllen_= 4096,
         double argcut1_= 0.5,
         double argcut2_= 4.,
         double argcut3_= 25.)
     : tbllen(tbllen_),
       argcut1(argcut1_),
       argcut2(argcut2_),
       argcut3(argcut3_)
    {
      argfac1 =      tbllen/argcut1;
      argfac2 =      tbllen/argcut2;
      argfac3 =      tbllen/argcut3;
      invArgFac =    tbllen*argcut3;
      lnI0Table1 =   getlnI0Table(tbllen,argcut1);
      lnI0Table2 =   getlnI0Table(tbllen,argcut2);
      lnI0Table3 =   getlnI0Table(tbllen,argcut3);
      lnI0InvTable = getlnI0InvTable(tbllen,argcut3);
   }

   void
   print_tables()
   {
  std::cout << "static constexpr const double lnI0Table1[4097][2] = {" << std::endl;
  for (int i = 0; i < lnI0Table1.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnI0Table1[i].first << "," << lnI0Table1[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr const double lnI0Table2[4097][2] = {" << std::endl;
  for (int i = 0; i < lnI0Table2.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnI0Table2[i].first << "," << lnI0Table2[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr const double lnI0Table3[4097][2] = {" << std::endl;
  for (int i = 0; i < lnI0Table3.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnI0Table3[i].first << "," << lnI0Table3[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
  std::cout << "static constexpr const double lnI0InvTable[4097][2] = {" << std::endl;
  for (int i = 0; i < lnI0InvTable.size(); i++)
    std::cout << std::setprecision(17) << "{ " << lnI0InvTable[i].first << "," << lnI0InvTable[i].second << "}," << std::endl;
  std::cout << "};" << std::endl;
    }

  private:
    std::vector<std::pair<double,double> >
    getlnI0Table(int tbllen_, double argmax_)
    {
      std::vector<std::pair<double,double> > lnI0Table;
      lnI0Table.resize(tbllen_+1);
      double divarg(argmax_/tbllen_);
      double arg0(0),arg1(0),x0(0),x1(0);
      for (unsigned i = 0; i < tbllen_+1; i++)
      {
        if (i > 0)
        {
          arg0 = (i  )*divarg;
          x0 = std::log(Fn_eBesselI0(arg0)) + arg0;
        }
        else
          x0 = 0.;
        arg1 = (i+1)*divarg;
        x1 = std::log(Fn_eBesselI0(arg1)) + arg1;
        lnI0Table[i].first = x0;
        lnI0Table[i].second = x1 - x0;
      }
      return lnI0Table;
    }

    std::vector<std::pair<double,double> >
    getlnI0InvTable(int tbllen_, double argcut_)
    {
      // Finely-sampled linear interpolation trades a small amount
      // of memory for significant speed improvement, compared with
      // quadratic interpolation
      std::vector<std::pair<double,double> > lnI0InvTable;
      lnI0InvTable.resize(tbllen_+1);
      double divarg(1./(argcut_*tbllen_)),arg0,arg1,x0,x1;
      double HALF(1./2);
      double logSqrt2Pi(std::log(scitbx::constants::two_pi)*HALF);

      for (unsigned i = 0; i < tbllen_+1; i++)
      {
        if (i > 0)
        {
          arg0 = 1./((i  )*divarg);
          x0 = std::log(Fn_eBesselI0(arg0)) + HALF*std::log(arg0);
        }
        else
          x0 = -logSqrt2Pi;
        arg1 = 1./((i+1)*divarg);
        x1 = std::log(Fn_eBesselI0(arg1)) + HALF*std::log(arg1);
        lnI0InvTable[i].first = x0;
        lnI0InvTable[i].second = x1 - x0;
      }
      return lnI0InvTable;
    }

};

}}
#endif
