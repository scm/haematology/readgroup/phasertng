  af_string
  FastRotationFunction::logSelfTable(
      const int nprint, //maximum number to print
      int maxorder,
      double refperc_cutoff
    ) //regardless of whether it is a top site in cluster or not
  {
    auto Rotn = Rotation();
    af_string output  = logTableHeader(nprint);
    bool p1(clustering_sg.NSYMP==1);
    bool clustered = rotation_function_data.size() ? rotation_function_data[0].clustered() : false;
    int w = itow(rotation_function_data.size()); //could print out of nprint range
    if (!rotation_function_data.size()) return output;
    output.push_back("--------");
    output.push_back("FSS%    Fast search score percent of top peak wrt mean");
    output.push_back("Rot'n   Rotational order, unrounded, *space group symmetry");
    output.push_back("A       Alignment parallel to a crystallographic symmetry axis");
    output.push_back("#Grp    Number of sites clustered in group");
    output.push_back("--------");
    output.push_back(snprintftos(
        "%-*s  %5s %5s %5s%c %5s %5s %c    %-20s  %7s %6s/%6s %6s/%6s  %5s",
        w,"#","FSS%","NCS%","Rot'n",' ',"Round","(+/-)",'A',"Euler","Theta","Phi+","Phi-","Chi+","Chi-","#Grp"));
    int i(1);
   // double f_top = top(); //this value is independent of what came before
    bool first(true);
    for (int r = 0; r < rotation_function_data.size() ; r++)
    {
      auto item = rotation_function_data[r];
      double refperc;
      if (rotation_function_data.size() > 1)
      {
        refperc = 100.*(item.value-f_mean)/(Rotn[r].ncs_top-f_mean);
        if (first and refperc < refperc_cutoff)
        {
          output.push_back("   ------- " + dtoss(refperc_cutoff,0) + "% of top NCS peak");
          first = false;
        }
      }
      auto Rot = Rotn[r];
      std::string topstr =  itos(i);
      std::string grpstr = item.is_topsite() ? itos(item.ifTopNumInGroup) : "--"; //clustered selection below
      if (!clustered) topstr = grpstr = "--";
      double pscore = 100.*(item.value-f_mean)/(f_top-f_mean);
      if (i > nprint)
        continue; //but print the ones that match the selected point group regardless
      std::string euler = dvtos(item.grid,6,1);
      output.push_back(snprintftos(
          "%-*i  %5.1f %5s %5.2f%c %5d %5.3f %c    %-20s  %7.1f % 6.1f/% 6.1f % 6.1f/% 6.1f  %5s",
          w,i,
          pscore,(r == 0) ? "-----" : dtos(refperc,1).c_str(),
          Rot.rotation,Rot.space_group_symmetry ? '*':' ',Rot.order,Rot.remainder,
          Rot.parallel ? 'Y' : 'N',
          euler.c_str(),
          Rot.cart_deg[1],Rot.cart_deg[0],Rot.cart_deg[0]-360,Rot.chi,Rot.cart_deg[2]-360,
          grpstr.c_str()));
      i++;
    }
    if (rotation_function_data.size() > nprint)
      output.push_back("--- etc " + itos(rotation_function_data.size()-nprint) + " more");
    output.push_back("");
    return output;
  }

  sv_int
  FastRotationFunction::peak_chi_sections(
      int maxorder
    ) //regardless of whether it is a top site in cluster or not
  {
    sv_int chis; //integer list
    if (!rotation_function_data.size()) return chis;
    auto Rotn = Rotation();
    for (int r = 0; r < rotation_function_data.size() ; r++)
    {
      auto Rot = Rotn[r];
      if (Rot.is_pointgroup(maxorder))
      {
        int jchi = std::round(360./Rot.order);
        if (std::find(chis.begin(), chis.end(), jchi) == chis.end())
          chis.push_back(jchi);
      }
    }
    return sv_int(chis.begin(),chis.end());
  }

  sv_bool
  FastRotationFunction::flag_space_group_symmetry()
  {
    sv_bool flags(rotation_function_data.size(),true);
    int rr(1); //keep the top one so there is a reference peak
    dmat33 identity(1,0,0,0,1,0,0,0,1);
    int NSYMP = SG.group().order_p();
    dmat33 fracmat = REFLECTIONS->UC.fractionalization_matrix();
    dmat33 orthmat = REFLECTIONS->UC.orthogonalization_matrix();
    orth_rotsymtr_frac.resize(NSYMP); //used in cluster function
    for (unsigned isym = 0; isym < NSYMP; isym++)
      orth_rotsymtr_frac[isym] = orthmat*SG.rotsym[isym].transpose()*fracmat;
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      double minchi(10000);
      for (int isym = 0; isym < NSYMP; isym++)
      {
        dmat33 ROT = zyz_matrix(rotation_function_data[r].grid); //euler in degrees
               ROT = orth_rotsymtr_frac[isym]*ROT;
        double chi = matrix_delta_deg(identity,ROT);
        minchi = std::min(minchi,chi);
      }
      double minimum_chi = 15;
      flags[r] = (minchi < minimum_chi);
    }
    return flags;
  }

  void
  FastRotationFunction::select_point_group_and_erase(
      int maxorder
    ) //regardless of whether it is a top site in cluster or not
  {
    auto Rotn = Rotation();
    int rr(1); //keep the top one so there is a reference peak
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      auto Rot = Rotn[r];
      if (Rot.order < maxorder+1) //e.g. 20 removed
      {
        dmat33 next_rotmat = zyz_matrix(rotation_function_data[r].grid);
        rotation_function_data[rr++] = rotation_function_data[r];
      }
    }
    rotation_function_data.erase(rotation_function_data.begin()+rr, rotation_function_data.end() );
  }

  void
  FastRotationFunction::expand_space_group_symmetry()
  {
    if (!rotation_function_data.size()) return;
    std::vector<Site> tmp;
    int NSYMP = SG.group().order_p();
    dmat33 fracmat = REFLECTIONS->UC.fractionalization_matrix();
    dmat33 orthmat = REFLECTIONS->UC.orthogonalization_matrix();
    orth_rotsymtr_frac.resize(NSYMP); //used in cluster function
    for (unsigned isym = 0; isym < NSYMP; isym++)
      orth_rotsymtr_frac[isym] = orthmat*SG.rotsym[isym].transpose()*fracmat;
    tmp.push_back(rotation_function_data[0]);
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      auto item = rotation_function_data[r];
      for (int isym = 0; isym < NSYMP; isym++)
      {
        dmat33 ROT = zyz_matrix(item.grid); //euler in degrees
               ROT = orth_rotsymtr_frac[isym]*ROT;
        dvect3 euler = scitbx::math::euler_angles::zyz_angles(ROT);
        tmp.push_back(Site(euler, rotation_function_data[r].value));
      }
    }
    rotation_function_data = tmp;
  }

  void
  FastRotationFunction::point_group_classes(
      double refperc_cutoff)
  {
    for (int o = 2; o <= 6; o++)
      hasX22[o] = 0;
    if (!rotation_function_data.size()) return;
    int NSYMP = SG.group().order_p();
    dmat33 fracmat = REFLECTIONS->UC.fractionalization_matrix();
    dmat33 orthmat = REFLECTIONS->UC.orthogonalization_matrix();
    orth_rotsymtr_frac.resize(NSYMP); //used in cluster function
    for (unsigned isym = 0; isym < NSYMP; isym++)
      orth_rotsymtr_frac[isym] = orthmat*SG.rotsym[isym].transpose()*fracmat;
    sv_bool issym(rotation_function_data.size(),false);
    dmat33 identity(1,0,0,0,1,0,0,0,1);
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      double minchi(10000);
      for (int isym = 0; isym < NSYMP; isym++)
      {
        dmat33 ROT = zyz_matrix(rotation_function_data[r].grid); //euler in degrees
               ROT = orth_rotsymtr_frac[isym]*ROT;
        double chi = matrix_delta_deg(identity,ROT);
        minchi = std::min(minchi,chi);
      }
      double minimum_chi = 15;
      issym[r] = (minchi < minimum_chi);
    }
    auto Rotn = Rotation();
    for (int r = 1; r < rotation_function_data.size(); r++)
    for (int s = r+1; s < rotation_function_data.size(); s++)
    for (int t = s+1; t < rotation_function_data.size(); t++)
    if (Rotn[r].remainder <= 0.04 and Rotn[s].remainder <= 0.04  and Rotn[t].remainder <= 0.04)
    if (!issym[r] or !issym[s] or !issym[t]) //one can be crystallographic (other two not)
    {
      dvect3 refperc;
      refperc[0] = 100.*(rotation_function_data[r].value-f_mean)/(Rotn[r].ncs_top-f_mean);
      refperc[1] = 100.*(rotation_function_data[s].value-f_mean)/(Rotn[r].ncs_top-f_mean);
      refperc[2] = 100.*(rotation_function_data[t].value-f_mean)/(Rotn[r].ncs_top-f_mean);
      dvect3 perc;
      perc[0] = 100.*(rotation_function_data[r].value-f_mean)/(f_top-f_mean);
      perc[1] = 100.*(rotation_function_data[s].value-f_mean)/(f_top-f_mean);
      perc[2] = 100.*(rotation_function_data[t].value-f_mean)/(f_top-f_mean);
      int ro = Rotn[r].order; //hasX is the maximum with that order over 75% of the reference top
      int so = Rotn[s].order; //hasX is the maximum with that order over 75% of the reference top
      int to = Rotn[t].order; //hasX is the maximum with that order over 75% of the reference top
      if (!issym[r] and refperc[0] >= refperc_cutoff)
      {
        hasX[ro] = hasX.count(ro) ? std::max(hasX[ro],perc[0]) : perc[0]; //store perc not refperc
      }
      if (refperc[0] >= refperc_cutoff and
          refperc[1] >= refperc_cutoff and
          refperc[2] >= refperc_cutoff) //percents should be above 75% of the top **NCS** peak
      {
        ivect3 rst(r,s,t);
        bool ninetydeg = true;
        for (int i = 0; i < 3; i++)
        for (int j = i+1; j < 3; j++)
        {
          dmat33 ROTi = zyz_matrix(rotation_function_data[rst[i]].grid);
          dmat33 ROTj = zyz_matrix(rotation_function_data[rst[j]].grid);
          double delta = axis_delta_deg(ROTi,ROTj);
          if (std::fabs(delta - 90) > 5.)
            ninetydeg = false;
        }
        sv_int pg({ro,so,to});
        std::sort(pg.begin(),pg.end());
        std::reverse(pg.begin(),pg.end());
        for (int o = 2; o <= 6; o++)
        {
          if (ninetydeg and pg == sv_int({o,2,2}))
          {
            //note that if one of these is aligned with the symmetry operation then it there will only be two peaks
            //so the lowest value perc[2] may end up higher in the edited list after removal of the aligned peaks
            hasX22[o] = std::max(hasX22[o],perc[2]);   //perc[2] must be the lowest
          }
        }
      }
    }
  }

  af_string
  FastRotationFunction::plot_of_chi_section(
      const double chi_deg,
      const int numpts)
  {
    auto Rotn = Rotation();
    std::vector<std::pair<double,std::string>> lines;
   // double chi = scitbx::deg_as_rad(chi_deg);
    PHASER_ASSERT(frfmap.size());
    af::const_ref<double, af::c_grid_padded<3> > real_map_const_ref(
        frfmap.begin(),
        af::c_grid_padded<3>(frfmap.accessor()));
    //this calculates points evenly spaced on a sphere radius 1
    PointsStored stored;
    stored.fill_memory_points_on_sphere(numpts);
    for (int i = 0; i < stored.translation_function_data.size(); i++)
    for (bool reverse : { true, false }) //because we are only going for half theta
    {
      dvect3 axis = stored.translation_function_data[i].grid;
      if (axis[2] < 0) continue; //negative z
      double chi = reverse ? -chi_deg : chi_deg; //reverse rotation instead
      bool deg(true);
      auto   rmat = scitbx::math::r3_rotation::axis_and_angle_as_matrix(axis,chi,deg);
      dvect3 euler = scitbx::math::euler_angles::zyz_angles(rmat);
      for (int e = 0; e < 3; e++)
      {
        while (euler[e] < 0) euler[e] += 360;
        while (euler[e] >= 360) euler[e] -= 360;
      }
      euler[0] = -euler[0]/360.; //fraction of full
      euler[1] = +euler[1]/180.; //fraction of full
      euler[2] = -euler[2]/360.; //fraction of full
      double interp = cctbx::maptbx::eight_point_interpolation(real_map_const_ref,euler);
      double pscore = 100.*(interp-f_mean)/(f_top-f_mean);
      cartesian_as_spherical cart(axis);
      auto cart_deg = cart.phi_theta_degrees(chi);
      phaser_assert(cart_deg[1] >= 0);
      phaser_assert(cart_deg[1] <= 90);
      phaser_assert(cart_deg[0] >= 0);
      phaser_assert(cart_deg[0] <= 360);
      double& theta = cart_deg[1];
      double& phi = cart_deg[0];
      std::string line = dtos(theta,7,2)+" "+dtos(phi,7,2)+" "+dtos(pscore,6,2);
      lines.push_back({pscore,line});
      double tol(15);
      if (phi < tol)
      { //extend the map in phi for interpolation
        std::string line = dtos(theta,7,2)+" "+dtos(phi+360,7,2)+" "+dtos(pscore,6,2);
        lines.push_back({pscore,line});
      }
    }
    //do the redundant section
    for (bool reverse : { true, false }) //because we are only going for half theta
    {
      double chi = reverse ? -chi_deg : chi_deg; //reverse rotation instead
      dvect3 euler(-chi/360,0,0); //close to axis
      double interp = cctbx::maptbx::eight_point_interpolation(real_map_const_ref,euler);
      double pscore = 100.*(interp-f_mean)/(f_top-f_mean);
      cartesian_as_spherical cart(dvect3(0,0,1));
      double theta = 0;
      for (double phi = 0; phi < 360; phi++) //redundant in phi
      {
        std::string line = dtos(theta,7,2)+" "+dtos(phi,7,2)+" "+dtos(pscore,6,2);
        lines.push_back({pscore,line});
        double tol(15);
        if (phi < tol)
        { //extend the map in phi for interpolation
          std::string line = dtos(theta,7,2)+" "+dtos(phi+360,7,2)+" "+dtos(pscore,6,2);
          lines.push_back({pscore,line});
        }
      }
    }
    std::sort(lines.begin(),lines.end());
    std::reverse(lines.begin(),lines.end());
    af_string output;
    for (auto& line: lines)
      output.push_back(line.second);
    return output;
  }

  af_string
  FastRotationFunction::plot_of_peaks()
  {
    auto Rotn = Rotation();
    af_string output;
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      auto item = rotation_function_data[r];
      double pscore = 100.*(item.value-f_mean)/(f_top-f_mean);
      double qscore = 100.*(item.value-f_mean)/(Rotn[r].ncs_top-f_mean); //relative to first NCS
      auto Rot = Rotn[r];
      output.push_back(snprintftos(
          "% 7.2f % 7.2f %6.0f %6i %5.3f %7.2f %7.2f %c %c",
          Rot.cart_deg[1],Rot.cart_deg[0],std::round(Rot.chi),Rot.order,Rot.remainder,pscore,qscore,
          Rot.parallel ? '1':'0', //integers for true/false parsing
          Rot.space_group_symmetry ? '1':'0' //integers for true/false parsing
          ));
      double tolerance = 5.;
      if (std::fabs(Rot.cart_deg[1]) > (90.0-tolerance))
      {
        output.push_back(snprintftos(
          "% 7.2f % 7.2f %6.0f %6i %5.3f %7.2f %7.2f %c %c",
          Rot.cart_deg[1],std::fmod(Rot.cart_deg[0]+180.,360.),std::round(Rot.chi),Rot.order,Rot.remainder,pscore,qscore,
          Rot.parallel ? '1':'0', //integers for true/false parsing
          Rot.space_group_symmetry ? '1':'0' //integers for true/false parsing
          ));
      }
    }
    return output;
  }

  std::vector<rotinfo>
  FastRotationFunction::Rotation()
  {
    auto flags = flag_space_group_symmetry();
    std::vector<rotinfo> Rotn(rotation_function_data.size());
    if (!rotation_function_data.size()) return Rotn;
    Rotn.push_back(rotinfo()); //top
    Rotn[0].parallel = true; //fixed, not minimum_chi
    Rotn[0].space_group_symmetry = true;
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      dmat33 ROT = zyz_matrix(rotation_function_data[r].grid); //euler in degrees
      dvect3 axis(0,0,0); double chi(0);
      std::tie(axis,chi) = axis_and_angle_deg(ROT);
      cartesian_as_spherical cart(axis);
      auto& Rot = Rotn[r];
      Rot.cart_deg = cart.phi_theta_degrees(chi);
      Rot.chi = Rot.cart_deg[2]; //corrected 0-360 for phi and chi
      double minabschi = std::min(Rot.chi,std::abs(Rot.chi-360));
      Rot.chi = minabschi;
      if (minabschi > DEF_PPH) //paranoia
      {
        Rot.rotation = 360./minabschi;
        Rot.order = std::round(Rot.rotation);
        Rot.remainder = std::abs(Rot.rotation - Rot.order);
      }
      Rot.parallel = false;
      Rot.space_group_symmetry = flags[r];
      for (int isym = 0; isym < SG.rotsym.size(); isym++)
      {
        double delta = axis_delta_deg(SG.rotsym[isym],ROT);
        if (delta < 5.) //if it is aligned it will snap on and be exactly aligned
          Rot.parallel = true;
        delta = axis_delta_deg(SG.rotsym[isym].transpose(),ROT);
        if (delta < 5.)
          Rot.parallel = true;
      }
    }
    double ncs_top(rotation_function_data[0].value);
    for (int r = 1; r < rotation_function_data.size() ; r++)
    {
      if (!Rotn[r].space_group_symmetry)
      {
        ncs_top = rotation_function_data[r].value;
        break;
      }
    }
    for (int r = 0; r < rotation_function_data.size() ; r++)
       Rotn[r].ncs_top = ncs_top; //overkill to store for every Rotn but better that pair return
    return Rotn;
  }
