//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Virtual_class__
#define __phasertng_Virtual_class__
#include <phasertng/main/Assert.h>
#include <phasertng/enum/labin_col.h>
#include <phasertng/data/reflection/ReflRow.h>

namespace phasertng {

namespace type { class mtzcol; }

class Virtual
{
  public:
    Virtual() { }
    virtual ~Virtual() {}

  public:
    virtual void setup_mtzcol(type::mtzcol) = 0;
    virtual void setup_miller(int) = 0;
    virtual void set_base_defaults() = 0;
    virtual void erase_from_reflection(int) = 0;
    virtual const reflection::ReflRow* get_row(const int r) const = 0;

  public: //setters
    virtual void set_present(friedel::type,const bool v,const int r) = 0;
    virtual void set_miller(const millnx v,const int r) = 0;
    virtual void set_flt(labin::col,const double v,const int r) = 0;
    virtual void set_int(labin::col,const int v,const int r) = 0;
    virtual void set_bln(labin::col,const bool v,const int r) = 0;

  public: //getters, very general, returns all as double for looping over all
    virtual const bool   has_rows() const = 0;
    virtual bool         has_col(labin::col) const = 0;
    virtual bool         has_friedel(friedel::type) const = 0;
    virtual bool         has_friedel(std::set<friedel::type>) const = 0;
    virtual const bool   get_present(friedel::type,const int r) const = 0;
    virtual const millnx get_miller(const int r) const = 0;
    virtual const double get_flt(labin::col,const int r) const = 0;
    virtual const int    get_int(labin::col,const int r) const = 0;
    virtual const bool   get_bln(labin::col,const int r) const = 0;

  public: //fast getters
    bool legal = false; //default is to crash so that developer can fix
    virtual const bool   get_both(const int r) const { phaser_assert(legal); return 0; }
    virtual const bool   get_cent(const int r) const { phaser_assert(legal); return 0; }
    virtual const bool   get_cent(const millnx r) const { phaser_assert(legal); return 0; }
    virtual const bool   get_plus(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_aniso(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_anisobeta(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_dobsnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_dobsneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_dobspos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fcnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fcneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fcpos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_feffnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_feffneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_feffpos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_fpos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_inat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_ineg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_ipos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_mapf(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_mapfom(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_mapph(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_phicnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_phicneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_phicpos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_phsr(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_resn(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_siga(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_sigfnat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_sigfneg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_sigfpos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_siginat(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_sigineg(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_sigipos(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_ssqr(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_tbin(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_teps(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_wll(const int r) const { phaser_assert(legal); return 0; }
    virtual const int    get_bin(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_eps(const int r) const { phaser_assert(legal); return 0; }
    virtual const int    get_free(const int r) const { phaser_assert(legal); return 0; }
    virtual const double get_eps(const millnx r) const { phaser_assert(legal); return 0; }
};

} //phasertng
#endif
