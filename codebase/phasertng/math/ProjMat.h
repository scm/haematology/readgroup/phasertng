//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_projmat_class__
#define __phasertng_projmat_class__
#include <vector>
#include <tntbx/include/tnt_vec.h>
#include <phasertng/cctbx_project/tntbx/include/tnt_fmat.h>

typedef std::vector<int> sv_int;

namespace phasertng {

  template<class T>
  class ProjMat
  {
  //Sparse matrix for RTB projection
    public:
    ProjMat() {nrows = ncols = 0; }
    ~ProjMat() {}
    private:
      std::vector<TNT::Fortran_Matrix<T> > D;
      sv_int start_na;
      int nrows,ncols;
    public:
    void push_back(TNT::Fortran_Matrix<T> P)
    {
      D.push_back(P);
      start_na.push_back(nrows);
      nrows += P.num_rows();
      ncols += P.num_cols();
    }
    const int& num_rows() const { return nrows; }
    const int& num_cols() const { return ncols; }
    const T operator() (int const& i, int const& j) const
    {
      if (i > nrows) return 0;
      if (j > ncols) return 0;
      int nb((j-1)/6); //block number from j
      int j_in_block(j-nb*6); //offset
      int i_in_block(i-start_na[nb]);
      if (i_in_block <= 0) return 0;
      if (i_in_block <= D[nb].num_rows() &&
          j_in_block <= D[nb].num_cols())
        return D[nb](i_in_block,j_in_block);
      return 0;
    }
    void clear() { start_na.clear(); D.clear(); }
  };

}

#endif
