#ifndef __phaser_vanderwaals_radii_class__
#define __phaser_vanderwaals_radii_class__
#include <unordered_map>
#include <cctype>

// Van-der-Waals radii for known elements.
//
// Created: Pavel Afonine.
//
// Sources:
// 1) Bondi, J.Phys.Chem., 68, 441, 1964  for atoms:
//      Ag,Ar,As,Au,Br,Cd,Cl,Cu,F,Ga,H,He,Hg,I,In,K,Kr,
//      Li,Mg,Na,Ne,Ni,Pb,Pd,Pt,Se,Si,Sn,Te,Tl,Xe,Zn
// 2) Fokine et al, J. Appl. Cryst. (2003). 36, 352-355 for "protein" atoms:
//    C, O, N, S, P
//
//
namespace phasertng {
namespace table {

class vanderwaals_radii
{
  std::unordered_map<std::string,double> radii;
  double point_radius = 0; //this is a default radius to return in case atomtype is not in table
                           //for example unknown atom for hexagonal grid

  public:

  double get(std::string t)
  {
    if (t.size()>=1) t[0] = std::toupper(t[0]);
    if (t.size()==2) t[1] = std::toupper(t[1]);
    return (radii.find(t) != radii.end()) ? radii[t] : point_radius;
  }

  vanderwaals_radii(double point_radius_= 0) : point_radius(point_radius_)
  {
    radii.insert(std::pair<std::string,double>(" H",  1.20 ));
    radii.insert(std::pair<std::string,double>(" D",  1.20 ));
    radii.insert(std::pair<std::string,double>("HE", 1.40 ));
    radii.insert(std::pair<std::string,double>("LI", 1.82 ));
    radii.insert(std::pair<std::string,double>("BE", 0.63 ));
    radii.insert(std::pair<std::string,double>(" B",  1.75 ));
    radii.insert(std::pair<std::string,double>(" C",  1.775));
    radii.insert(std::pair<std::string,double>(" N",  1.50 ));
    radii.insert(std::pair<std::string,double>(" O",  1.45 ));
    radii.insert(std::pair<std::string,double>(" F",  1.47 ));
    radii.insert(std::pair<std::string,double>("NE", 1.54 ));
    radii.insert(std::pair<std::string,double>("NA", 2.27 ));
    radii.insert(std::pair<std::string,double>("MG", 1.73 ));
    radii.insert(std::pair<std::string,double>("AL", 1.50 ));
    radii.insert(std::pair<std::string,double>("SI", 2.10 ));
    radii.insert(std::pair<std::string,double>(" P",  1.90 ));
    radii.insert(std::pair<std::string,double>(" S",  1.80 ));
    radii.insert(std::pair<std::string,double>("CL", 1.75 ));
    radii.insert(std::pair<std::string,double>("AR", 1.88 ));
    radii.insert(std::pair<std::string,double>(" K",  2.75 ));
    radii.insert(std::pair<std::string,double>("CA", 1.95 ));
    radii.insert(std::pair<std::string,double>("SC", 1.32 ));
    radii.insert(std::pair<std::string,double>("TI", 1.95 ));
    radii.insert(std::pair<std::string,double>(" V",  1.06 ));
    radii.insert(std::pair<std::string,double>("CR", 1.13 ));
    radii.insert(std::pair<std::string,double>("MN", 1.19 ));
    radii.insert(std::pair<std::string,double>("FE", 1.26 ));
    radii.insert(std::pair<std::string,double>("CO", 1.13 ));
    radii.insert(std::pair<std::string,double>("NI", 1.63 ));
    radii.insert(std::pair<std::string,double>("CU", 1.40 ));
    radii.insert(std::pair<std::string,double>("ZN", 1.39 ));
    radii.insert(std::pair<std::string,double>("GA", 1.87 ));
    radii.insert(std::pair<std::string,double>("GE", 1.48 ));
    radii.insert(std::pair<std::string,double>("AS", 0.83 ));
    radii.insert(std::pair<std::string,double>("SE", 1.90 ));
    radii.insert(std::pair<std::string,double>("BR", 1.85 ));
    radii.insert(std::pair<std::string,double>("KR", 2.02 ));
    radii.insert(std::pair<std::string,double>("RB", 2.65 ));
    radii.insert(std::pair<std::string,double>("SR", 2.02 ));
    radii.insert(std::pair<std::string,double>(" Y",  1.61 ));
    radii.insert(std::pair<std::string,double>("ZR", 1.42 ));
    radii.insert(std::pair<std::string,double>("NB", 1.33 ));
    radii.insert(std::pair<std::string,double>("Mo", 1.75 ));
    radii.insert(std::pair<std::string,double>("TC", 2.00 ));
    radii.insert(std::pair<std::string,double>("RU", 1.20 ));
    radii.insert(std::pair<std::string,double>("RH", 1.22 ));
    radii.insert(std::pair<std::string,double>("PD", 1.63 ));
    radii.insert(std::pair<std::string,double>("AG", 1.72 ));
    radii.insert(std::pair<std::string,double>("CD", 1.58 ));
    radii.insert(std::pair<std::string,double>("IN", 1.93 ));
    radii.insert(std::pair<std::string,double>("SN", 2.17 ));
    radii.insert(std::pair<std::string,double>("SB", 1.12 ));
    radii.insert(std::pair<std::string,double>("TE", 1.26 ));
    radii.insert(std::pair<std::string,double>(" I",  1.98 ));
    radii.insert(std::pair<std::string,double>("XE", 2.16 ));
    radii.insert(std::pair<std::string,double>("CS", 3.01 ));
    radii.insert(std::pair<std::string,double>("BA", 2.41 ));
    radii.insert(std::pair<std::string,double>("LA", 1.83 ));
    radii.insert(std::pair<std::string,double>("CE", 1.86 ));
    radii.insert(std::pair<std::string,double>("PR", 1.62 ));
    radii.insert(std::pair<std::string,double>("ND", 1.79 ));
    radii.insert(std::pair<std::string,double>("PM", 1.76 ));
    radii.insert(std::pair<std::string,double>("SM", 1.74 ));
    radii.insert(std::pair<std::string,double>("EU", 1.96 ));
    radii.insert(std::pair<std::string,double>("GD", 1.69 ));
    radii.insert(std::pair<std::string,double>("TB", 1.66 ));
    radii.insert(std::pair<std::string,double>("DY", 1.63 ));
    radii.insert(std::pair<std::string,double>("HO", 1.61 ));
    radii.insert(std::pair<std::string,double>("ER", 1.59 ));
    radii.insert(std::pair<std::string,double>("TM", 1.57 ));
    radii.insert(std::pair<std::string,double>("YB", 1.54 ));
    radii.insert(std::pair<std::string,double>("LU", 1.53 ));
    radii.insert(std::pair<std::string,double>("HF", 1.40 ));
    radii.insert(std::pair<std::string,double>("TA", 1.22 ));
    radii.insert(std::pair<std::string,double>(" W",  1.26 ));
    radii.insert(std::pair<std::string,double>("RE", 1.30 ));
    radii.insert(std::pair<std::string,double>("OS", 1.58 ));
    radii.insert(std::pair<std::string,double>("IR", 1.22 ));
    radii.insert(std::pair<std::string,double>("PT", 1.72 ));
    radii.insert(std::pair<std::string,double>("AU", 1.66 ));
    radii.insert(std::pair<std::string,double>("HG", 1.55 ));
    radii.insert(std::pair<std::string,double>("TL", 1.96 ));
    radii.insert(std::pair<std::string,double>("PB", 2.02 ));
    radii.insert(std::pair<std::string,double>("BI", 1.73 ));
    radii.insert(std::pair<std::string,double>("PO", 1.21 ));
    radii.insert(std::pair<std::string,double>("AT", 1.12 ));
    radii.insert(std::pair<std::string,double>("RN", 2.30 ));
    radii.insert(std::pair<std::string,double>("FR", 3.24 ));
    radii.insert(std::pair<std::string,double>("RA", 2.57 ));
    radii.insert(std::pair<std::string,double>("AC", 2.12 ));
    radii.insert(std::pair<std::string,double>("TH", 1.84 ));
    radii.insert(std::pair<std::string,double>("PA", 1.60 ));
    radii.insert(std::pair<std::string,double>(" U",  1.75 ));
    radii.insert(std::pair<std::string,double>("NP", 1.71 ));
    radii.insert(std::pair<std::string,double>("PU", 1.67 ));
    radii.insert(std::pair<std::string,double>("AM", 1.66 ));
    radii.insert(std::pair<std::string,double>("CM", 1.65 ));
    radii.insert(std::pair<std::string,double>("BK", 1.64 ));
    radii.insert(std::pair<std::string,double>("CF", 1.63 ));
    radii.insert(std::pair<std::string,double>("ES", 1.62 ));
    radii.insert(std::pair<std::string,double>("FM", 1.61 ));
    radii.insert(std::pair<std::string,double>("MD", 1.60 ));
    radii.insert(std::pair<std::string,double>("NO", 1.59 ));
    radii.insert(std::pair<std::string,double>("LR", 1.58 ));
  }
};

}}
#endif
