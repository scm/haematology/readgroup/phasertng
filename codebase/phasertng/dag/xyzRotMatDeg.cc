#include <phasertng/dag/xyzRotMatDeg.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/math/table/cos_sin.h>
#include <iso646.h>
#include <scitbx/array_family/misc_functions.h>
#include <scitbx/constants.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
namespace dag {

dmat33
xyzRotMatDeg::perturbed(const dvect3& perturbRot) const
{
  dmat33 rotmat;
  rotmat(0,0) = rotmat(1,1) = rotmat(2,2) = 1.;
  rotmat(0,1) = rotmat(1,0) = rotmat(2,0) = rotmat(0,2) = rotmat(2,1) = rotmat(1,2) = 0.;
  for (int dir = 0; dir < 3; dir++)
  {
    double theta_deg = perturbRot[dir];
    int ind0,ind1,ind2;
    if (dir == 0)      { ind0=0; ind1=1; ind2=2; }
    else if (dir == 1) { ind0=1; ind1=2; ind2=0; }
    else               { ind0=2; ind1=0; ind2=1; } //dir == 2
    cmplex cos_sin = tbl_cos_sin.get(theta_deg/360.); //multiplies by 2*pi internally
    double cosTheta = std::real(cos_sin);
    double sinTheta = std::imag(cos_sin);
    dmat33 xyzmat;
    xyzmat(ind0,ind0) = 1.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = cosTheta;
    xyzmat(ind1,ind2) = -sinTheta;
    xyzmat(ind2,ind1) = sinTheta;
    xyzmat(ind2,ind2) = cosTheta;
    rotmat = xyzmat*rotmat;
  }
  return rotmat;
}
}}
