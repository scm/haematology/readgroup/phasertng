//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_reflection_ReflRowData_class__
#define __phasertng_reflection_ReflRowData_class__
#include <cctbx/miller.h>
#include <phasertng/data/reflection/ReflRow.h>

namespace phasertng {
namespace reflection {

class ReflRowData : public ReflRow
{
  public:
    cctbx::miller::index<int> miller = {0,0,0};
    double inat = 0;
    double siginat = 0;
    double fnat = 0;
    double sigfnat = 0;
    double feffnat = 0;
    double dobsnat = 0;
    double anisobeta = 0;
    double wll = 0;
    double teps = 0;
    double tbin = 0;
    double resn = 0;
    double ssqr = 0;
    double info = 0;
    double einfo = 0;
    double eps = 0;
    int    bin = 0;
    bool   cent = 0;
    bool   present = true; //should always be true

  public:
    const cctbx::miller::index<int> get_miller() const { return miller; }
    const double get_inat() const { return inat; }
    const double get_siginat() const { return siginat; }
    const double get_fnat() const { return fnat; }
    const double get_sigfnat() const { return sigfnat; }
    const double get_feffnat() const { return feffnat; }
    const double get_dobsnat() const { return dobsnat; }
    const double get_anisobeta() const { return anisobeta; }
    const double get_wll() const { return wll; }
    const double get_teps() const { return teps; }
    const double get_eps() const { return eps; }
    const double get_tbin() const { return tbin; }
    const double get_resn() const { return resn; }
    const double get_ssqr() const { return ssqr; }
    const double get_info() const { return info; }
    const double get_einfo() const { return einfo; }
    const int get_bin() const { return bin; }
    const bool get_cent() const { return cent; }
    const bool get_pnat() const { return present; }
};

} //end reflection scope
} //phasertng
#endif
