//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/AnglesUnique.h>
#include <phasertng/main/Error.h>
#include <scitbx/math/euler_angles.h>

namespace phasertng {

  AnglesUnique::AnglesUnique(double SAMP_,SpaceGroup& SG_,UnitCell& UC_) :
    SiteList(),
    SAMP(SAMP_),SG(SG_),UC(UC_)
  {
// Returns a list of unique angles for the space group (DO_FULL)
    restart();
    Size = 0;
    while (!at_end()) { Size++; }
    restart(); //reset paranoia
  }

  AnglesUnique::AnglesUnique(double SAMP_) :
    SiteList(),
    SAMP(SAMP_) //defaults for SG, UC
  {
// Returns a list of unique angles for the space group (DO_FULL)
    restart();
    Size = 0;
    while (!at_end()) { Size++; }
    restart(); //reset paranoia
  }

  void
  AnglesUnique::set_sampling(double SAMP_)
  {
    SAMP = SAMP_;
    restart();
    Size = 0;
    while (!at_end()) { Size++; }
    restart(); //reset paranoia
  }

  void
  AnglesUnique::restart(unsigned seed_)
  {
    NSYMP = SG.group().order_p();
    osmat.resize(NSYMP);
    tpmax = 0; dtp = 0; tmmax = 0; dtm = 0;
    alpha = 0; gamma = 0; cosb2 = 0; sinb2 = 0; offm = 0;
    epsilon = 0.0002;
    beta = 0;
    dbeta = SAMP*std::sqrt(2.0/3.0);
    nbeta = std::floor(180./dbeta);
    betmin = (180.0-nbeta*dbeta)/2.0;
    betmax = 180.0;
    if (betmin < epsilon)
      betmin = 0;
//Combine orthogonalisation and symmetry matrices for use
//in generating symmetry equivalents of rotation matrices
    for (unsigned isym = 0; isym < NSYMP; isym++)
    {
      osmat[isym] = SG.rotsym[isym]*UC.fractionalization_matrix().transpose();
    }
    offp = 5.0/6.0;
    beta = betmin;
    {
      offp = 1.0 - offp;
      cosb2 = std::fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
      sinb2 = std::fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
      if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
      {
        tpmax = 1.0;
        dtp = 1.0;
        dtm = SAMP;
        tmmax = 360.0;
      }
      else if (sinb2 < SAMP/360.0)
      {
         dtp = std::sqrt(3.0/4.0)*SAMP;
         tpmax = 360.0;
         tmmax = 1.0;
         dtm = 1.0;
      }
      else
      {
        dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
        dtm = SAMP/sinb2;
        tpmax = 720.0;
        tmmax = 360.0;
      }
      offm = 3.0/4.0;
      thetp = offp*dtp;
      {
        offm = 1.0 - offm;
        thetm = offm*dtm;
      }
    }
    inext = 0;
  }

  bool
  AnglesUnique::at_end()
  {
    if (inext == 0)
    {
      inext++;
      Site = dvect3(0,0,0);
      return false;
    }
    while (beta < betmax)
    {
      alpha = fmod_pos(720.0+(thetp+thetm)/2.0,360.0);
      gamma = fmod_pos(720.0+(thetp-thetm)/2.0,360.0);
      Site = dvect3(alpha,beta,gamma);
      dvect3 search_euler_rad;
      search_euler_rad[0] = scitbx::deg_as_rad(alpha);
      search_euler_rad[1] = scitbx::deg_as_rad(beta);
      search_euler_rad[2] = scitbx::deg_as_rad(gamma);
      dmat33  rmat = scitbx::math::euler_angles::zyz_matrix<double>(alpha,beta,gamma);
      dmat33 drmat = UC.fractionalization_matrix()*(rmat);
             drmat = drmat.transpose();
      bool accept_angle = false;
      for (unsigned isym = 1; isym < NSYMP; isym++)
      {
        dmat33 rsmat = drmat*osmat[isym];
               rsmat = rsmat.transpose();
        dvect3 seuler = scitbx::math::euler_angles::zyz_angles(rsmat);
        for (int i = 0; i < 3; i++)
          seuler[i] = scitbx::deg_as_rad(seuler[i]);
        if (seuler[1] < search_euler_rad[1]-epsilon) goto next_angle;
        if (seuler[1] > search_euler_rad[1]+epsilon) continue;
        if (seuler[0] < search_euler_rad[0]-epsilon) goto next_angle;
        if (seuler[0] > search_euler_rad[0]+epsilon) continue;
        if (seuler[2] < search_euler_rad[2]-epsilon) goto next_angle;
      }
      accept_angle = true;
      next_angle:
      thetm += dtm;
      if (!(beta < betmax && thetp < tpmax && thetm < tmmax))
      {
        thetp += dtp;
        offm = 1.0 - offm;
        thetm = offm*dtm;
        if (!(beta < betmax && thetp < tpmax && thetm < tmmax))
        {
          beta += dbeta;
          offp = 1.0 - offp;
          cosb2 = std::fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
          sinb2 = std::fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
          if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
          {
            tpmax = 1.0;
            dtp = 1.0;
            dtm = SAMP;
            tmmax = 360.0;
          }
          else if (sinb2 < SAMP/360.0)
          {
             dtp = std::sqrt(3.0/4.0)*SAMP;
             tpmax = 360.0;
             tmmax = 1.0;
             dtm = 1.0;
          }
          else
          {
            dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
            dtm = SAMP/sinb2;
            tpmax = 720.0;
            tmmax = 360.0;
          }

          offm = 3.0/4.0;
          thetp = offp*dtp;
          offm = 1.0 - offm;
          thetm = offm*dtm;
        }
      }
      if (accept_angle)
      {
        inext++;
        return false;
        //leaving in state ready for next iteration
      }
    }
    restart();
    return true;
  }

  af_dvect3
  AnglesUnique::all_sites()
  {
    //local variables in scope not member variabels
    //don't interfere with stored values for next()
    sv_dmat33  osmat(NSYMP);
    double dbeta(0),betmin(0),betmax(0),nbeta(0),tpmax(0),dtp(0),tmmax(0),dtm(0);
    double alpha(0),gamma(0),cosb2(0),sinb2(0),offm(0),offp(0);
    dvect3 search_euler(0,0,0),seuler(0,0,0);
    dmat33 rmat,drmat,rsmat;
    double epsilon = 0.0002;

    NSYMP = SG.group().order_p();
    osmat.resize(NSYMP);
    tpmax = 0; dtp = 0; tmmax = 0; dtm = 0;
    alpha = 0; gamma = 0; cosb2 = 0; sinb2 = 0; offm = 0;
    dbeta = SAMP*std::sqrt(2.0/3.0);
    nbeta = std::floor(180./dbeta);
    betmin = (180.0-nbeta*dbeta)/2.0;
    betmax = 180.0;
    if (betmin < epsilon)
      betmin = 0;
//Combine orthogonalisation and symmetry matrices for use
//in generating symmetry equivalents of rotation matrices
    for (unsigned isym = 0; isym < NSYMP; isym++)
    {
      osmat[isym] = SG.rotsym[isym]*UC.fractionalization_matrix().transpose();
    }
    offp = 5.0/6.0;

    af_dvect3 rotlist;
    rotlist.push_back(dvect3(0,0,0));
    try {
      for (double beta = betmin; beta < betmax; beta += dbeta)
      {
        offp = 1.0 - offp;
        cosb2 = fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
        sinb2 = fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
        if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
        {
          tpmax = 1.0;
          dtp = 1.0;
          dtm = SAMP;
          tmmax = 360.0;
        }
        else if (sinb2 < SAMP/360.0)
        {
          dtp = std::sqrt(3.0/4.0)*SAMP;
          tpmax = 360.0;
          tmmax = 1.0;
          dtm = 1.0;
        }
        else
        {
          dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
          dtm = SAMP/sinb2;
          tpmax = 720.0;
          tmmax = 360.0;
        }

        offm = 3.0/4.0;
        for (double thetp = offp*dtp; thetp < tpmax; thetp += dtp)
        {
          offm = 1.0 - offm;
          for (double thetm = offm*dtm; thetm < tmmax; thetm += dtm)
          {
            alpha = fmod_pos(720.0+(thetp+thetm)/2.0,360.0);
            gamma = fmod_pos(720.0+(thetp-thetm)/2.0,360.0);
            dvect3 Site(alpha,beta,gamma);
            dvect3 search_euler_rad;
            search_euler_rad[0] = scitbx::deg_as_rad(alpha);
            search_euler_rad[1] = scitbx::deg_as_rad(beta);
            search_euler_rad[2] = scitbx::deg_as_rad(gamma);
// Check Euler angles chosen with same convention as symmetry related ones
// by doing a conversion/back-conversion
            dmat33  rmat = scitbx::math::euler_angles::zyz_matrix<double>(alpha,beta,gamma);
            dmat33 drmat = UC.fractionalization_matrix()*(rmat);
                   drmat = drmat.transpose();
// If a symmetry related orientation beats this orientation to
// the asymmetric unit, skip this orientation because we will find
// it's better again later in the search.
// A full search without symmetry covers 0-720 in one theta,
// 0-360 in the other. The search points are found by generating
// all these angles, but only keeping the orientation if, when
// compared to all its symmetry related equivalents:
//   it has uniquely the lowest beta, or
//   it shares the lowest beta and has uniquely the lowest alpha, or
//   it shares the lowest alpha and beta and has the lowest gamma
// This requires offsets for theta(+) and theta(-),
// as well as different metrics in each direction to make the
// nearest neighbour distances all equivalent.
            //bool accept_angle(false);
            for (unsigned isym = 1; isym < NSYMP; isym++)
            {
              dmat33 rsmat = drmat*osmat[isym];
                     rsmat = rsmat.transpose();
              dvect3 seuler = scitbx::math::euler_angles::zyz_angles(rsmat);
              for (int i = 0; i < 3; i++)
                seuler[i] = scitbx::deg_as_rad(seuler[i]);

              //If new beta is smaller, skip this orientation.
              if (seuler[1] < search_euler_rad[1]-epsilon) goto next_angle;

              //If new beta is bigger, try another symmetry operator.
              if (seuler[1] > search_euler_rad[1]+epsilon) continue;

              //New beta is the same within epsilon
              //Make decision on alpha
              // If new alpha is smaller, skip this orientation.
              if (seuler[0] < search_euler_rad[0]-epsilon) goto next_angle;

              //If it's bigger, try another symmetry operator.
              if (seuler[0] > search_euler_rad[0]+epsilon) continue;

              //New beta and alpha are the same within epsilon
              //Make decision on gamma
              //If new gamma is smaller, skip this orientation.
              if (seuler[2] < search_euler_rad[2]-epsilon) goto next_angle;

              //Success! Accept angles
              //If all angles are (nearly) the same, the orientation
              //will be duplicated in the search, but this will arise
              //very rarely and will be fixed in the cluster analysis
            }
            //accept these euler angles
            //accept_angle = true;
            rotlist.push_back(Site);
            next_angle:
              continue;
          }
        }
      }
    } //try
    catch (std::exception const& err) {
      //this catches the error  St9bad_alloc where it runs out of memory
      throw Error(err::MEMORY,"Memory exhausted while generating search points");
    }
    //restart(); no need to restart, don't use class variables
    return rotlist;
  }

  double
  AnglesUnique::sampling_for_approx_number(double num_pts)
  {
    //below is a fit quadratic fit to the log of the number of points, to give relevant sampling
    //number of points will be approximate after fit, better fit for higher number of points
    double x = std::log(num_pts);
    return 0.23322181528846905*x*x -7.345682646652843*x + 59.65663474869354;
  }

}// phasertng
