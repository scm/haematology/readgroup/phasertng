//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_RefineTest_class__
#define __phasertng_RefineTest_class__
#include <phasertng/dtmin/RefineBase.h>
#include <phasertng/main/pointers.h>
#include <utility>
#include <vector>

typedef std::vector<double> sv_double;
typedef std::vector<std::string> sv_string;

namespace phasertng {

class RefineTest : public dtmin::RefineBase
{
  private: //members
    sv_double x;
    double s11;
    double s12;
    double s22;
    double twist;

  public: //constructors
    RefineTest() : RefineBase() { }
    RefineTest(sv_double);
    ~RefineTest() throw() {}

  public:
    //concrete functions
    void
    set_macrocycle_protocol(const sv_string);

    sv_string
    macrocycle_parameter_names();

    sv_double
    get_macrocycle_parameters();

    void
    set_macrocycle_parameters(sv_double);

    sv_double
    macrocycle_large_shifts();

    TargetType
    target();

    TargetGradientType
    target_gradient();

    TargetGradientHessianType
    target_gradient_hessian();

    af_string
    current_statistics();

  public:
    void set_threading(bool,int);
};

} //phasertng
#endif
