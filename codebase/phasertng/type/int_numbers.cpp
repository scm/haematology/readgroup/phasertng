//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/int_numbers.h>
#include <sstream>
#include <iomanip>
#include <set>

namespace phasertng {
namespace type {

  int_numbers::int_numbers(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const sv_int dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  int_numbers::set_value(sv_int other)
  {
    user = true;
    uvalue = other;
  }

  void
  int_numbers::set_value(const sv_string& vcards,const sv_int& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  int_numbers::is_default() const
  { return uvalue == dephalt; }

  void
  int_numbers::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  int_numbers::to_python()
  { return unparse_parameter(false); }

  void
  int_numbers::from_python(std::string str)
  {
    user = true;
    sv_string txt = to_words(str);
    uvalue.resize(txt.size());
    for (int i = 0; i < txt.size(); i++)
      uvalue[i] = std::stoi(txt[i]);
  }

  sv_int
  int_numbers::value_or_default() const
  { return user ? uvalue : dephalt; }

  int
  int_numbers::size() const
  { return value_or_default().size(); }

  void int_numbers::clear() { return uvalue.clear(); }

  void
  int_numbers::push_back(const int& other)
  {
    user = true;
    uvalue.push_back(other);
  }

  void
  int_numbers::pop_back()
  {
    user = true;
    uvalue.pop_back();
  }

  sv_int
  int_numbers::value_or_default_unique() const
  {
    sv_int tmp = value_or_default();
    std::set<int> tmp_set;
    for (auto item : tmp)
      tmp_set.insert(item);
    tmp.clear();
    for (auto item : tmp_set)
      tmp.push_back(item);
    return tmp; //sorted and unique
  }

  std::string
  int_numbers::unparse_parameter(bool phil) const
  {
    if (!value_or_default().size())
      return None();
    std::stringstream ss;
    for (int i = 0; i < value_or_default().size(); i++)
      ss << value_or_default()[i] << " ";
    return value_or_default().size() ? ss.str() : None();
  }

  std::string
  int_numbers::show_default(bool phil) const
  {
    if (!dephalt.size())
      return None();
    std::stringstream ss;
    for (int i = 0; i < dephalt.size(); i++)
      ss << dephalt[i] << " ";
    return ss.str();
  }

}}
