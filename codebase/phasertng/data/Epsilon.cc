//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Epsilon.h>
#include <phasertng/data/Reflections.h>
#include <phasertng/math/xyzRotMatDeg.h>
#include <scitbx/math/r3_rotation.h>
#include <algorithm>
#include <numeric> //Windows accumulate
#include <phasertng/math/table/cos_sin.h>

namespace phasertng {
  extern const table::cos_sin_lin_interp_table& tbl_cos_sin;


  // Initialize static members of class
  table::gfunction_of_ssqr
      Epsilon::GfuncOfRSsqr = table::gfunction_of_ssqr(20000,2.5);
  table::dgfunction_by_dssqr
      Epsilon::dGfuncBydRSsqr = table::dgfunction_by_dssqr(20000,2.5);
  table::d2gfunction_by_dssqr2
      Epsilon::d2GfuncBydRSsqr2 = table::d2gfunction_by_dssqr2(20000,2.5);

  void
  Epsilon::parse(
    int TNCS_ORDER_,
    dvect3 TNCS_VECTOR_,
    double TNCS_RMS_,
    double MIN_RMS_,
    double MAX_RMS_,
    double TNCS_GFNRAD_,
    double MIN_GFNRAD_,
    double MAX_GFNRAD_
  )
  {
    TNCS_ORDER = TNCS_ORDER_;
    TNCS_VECTOR = TNCS_VECTOR_;
    TNCS_RMS = TNCS_RMS_;
    MIN_RMS = MIN_RMS_;
    MAX_RMS = MAX_RMS_;
    TNCS_GFNRAD = TNCS_GFNRAD_;
    MIN_GFNRAD = MIN_GFNRAD_;
    MAX_GFNRAD = MAX_GFNRAD_;
  }

  void
  Epsilon::setup_parameters(const_ReflectionsPtr reflections)
  {
    PHASER_ASSERT(reflections->has_col(labin::BIN));
    PHASER_ASSERT(reflections->has_col(labin::TBIN));
    TNCS_RMS = reflections->TNCS_RMS;
    TNCS_ORDER = reflections->TNCS_ORDER;
    TNCS_ANGLE = reflections->TNCS_ANGLE;
    TNCS_VECTOR = reflections->TNCS_VECTOR;
    TNCS_FS = reflections->TNCS_FS;
    TNCS_GFNRAD = reflections->TNCS_GFNRAD;
    VARBINS.resize(reflections->numbins());
    for (int r = 0; r < reflections->NREFL; r++)
    {
      //get BIN and TBIN slow using virtual functions, but only done once
      const int s = reflections->get_int(labin::BIN,r);
      VARBINS[s] = reflections->get_flt(labin::TBIN,r); //will overwrite multiple times with same value
    }
    //above should not need checking or reset since they will be after minimization
    apply_spacegroup_unitcell(reflections->SG,reflections->UC);
    PHASER_ASSERT(refl_G_Vterm != 0);
  }

  void
  Epsilon::store_arrays(const_ReflectionsPtr reflections,bool store_halfR,bool store_fullR,bool store_elmn)
  {
    //if we are going to refine the values, then halfR is always false
    //and the deriviatives and ncsRmat_tr should be left in the halfR=false state
    //if not, both halfR and !halfR terms will be required depending on
    // which of pose/curl/gyre/turn set in the node
    // we can save memory by not setting those that are not required
    //the flag set for halfR determines whether the halfR terms will ever be needed
    //the flag set for fullR determines whether the fullR terms will ever be needed
    if (store_halfR and TNCS_ORDER == 2)
    {
      initialize(true);
      store_halfR_arrays(reflections,store_elmn);
    }
    initialize(false); //always leave in fullR state (for deriviatives)
    if (store_fullR or TNCS_ORDER > 2)
    {
      store_fullR_arrays(reflections);
    }
  }

  void
  Epsilon::apply_spacegroup_unitcell(const SpaceGroup& SG_,const UnitCell& UC_)
  {
    PHASER_ASSERT(TNCS_ORDER > 0);
    SG = SG_;
    UC = UC_;
    nsymp = (SG.group().order_p());
    refl_Gsqr.resize(nsymp);
    refl_G.resize(nsymp);
    //set defaults
    refl_TEPS = 1;
    refl_G_Vterm = 1;
    std::fill(refl_Gsqr.begin(), refl_Gsqr.end(), 1.); //different from initialization for summation
    std::fill(refl_G.begin(), refl_G.end(), 1.); //different from initialization for summation
    refl_G_DRMS = 1;
    if (TNCS_ORDER == 1) return;

    symfac = (1./(TNCS_ORDER*nsymp)); // (diagonals in loop)
    Rotsymtr.resize(nsymp);
    GfnradRotsymtrFrac.resize(nsymp);
    PHASER_ASSERT(TNCS_GFNRAD > 0);
    for (int isym = 0; isym < nsymp; isym++)
    { // Precalculate terms for metric_matrix calculation
      // Note that SG.rotsym applies in reciprocal space
      Rotsymtr[isym] = SG.rotsym[isym].transpose(); // Real-space rotation matrix
      double radSqr = fn::pow2(TNCS_GFNRAD);
      GfnradRotsymtrFrac[isym] = radSqr * Rotsymtr[isym] * UC.fractionalization_matrix();
    }
    GfunTensor_ijsym.resize({TNCS_ORDER,TNCS_ORDER,nsymp});
    ncsDeltaT_ijsym.resize({TNCS_ORDER,TNCS_ORDER,nsymp});
    dGfunTensor_by_dRotn_ijsymdir.resize({TNCS_ORDER,TNCS_ORDER,nsymp,3});
    d2GfunTensor_by_dRotn2_ijsymdir12.resize({TNCS_ORDER,TNCS_ORDER,nsymp,3,3});
  }

  void
  Epsilon::store_halfR_arrays(const_ReflectionsPtr reflections,bool store_elmn)
  {
    PHASER_ASSERT(halfR);
    //initialize tncs parameters from reflections
    array_G_DRMS.resize(reflections->NREFL);
    array_Gsqr_Vterm.resize(reflections->NREFL);
    array_G_Vterm.resize(reflections->NREFL);
    matrix_G.resize(reflections->NREFL,reflections->SG.group().order_p());
    if (store_elmn)
      matrix_Gsqr.resize(reflections->NREFL,reflections->SG.group().order_p());
    for (int r = 0; r < reflections->NREFL; r++)
    {
      const millnx& miller = reflections->get_miller(r);
      const double ssqr =   reflections->get_flt(labin::SSQR,r);
      const int    s =      reflections->get_int(labin::BIN,r);
      this->calculate_reflection_parameters(miller,ssqr,s);
      array_G_DRMS[r] = refl_G_DRMS; //independent of angle
      array_Gsqr_Vterm[r] = refl_Gsqr_Vterm;
      for (int isym = 0; isym < refl_G.size(); isym++)
      {
        matrix_G(r,isym) = refl_G[isym];
        if (store_elmn)
          matrix_Gsqr(r,isym) = refl_Gsqr[isym];
      }
      array_G_Vterm[r] = refl_G_Vterm;
    }
  }

  void
  Epsilon::store_fullR_arrays(const_ReflectionsPtr reflections)
  {
    PHASER_ASSERT(!halfR);
    //initialize tncs parameters from reflections
    array_G_DRMS.resize(reflections->NREFL);
    array_G_Vterm.resize(reflections->NREFL);
    for (int r = 0; r < reflections->NREFL; r++)
    {
      const millnx miller = reflections->get_miller(r);
      const double ssqr =   reflections->get_flt(labin::SSQR,r);
      const int    s =      reflections->get_int(labin::BIN,r);
      this->calculate_reflection_parameters(miller,ssqr,s);
      array_G_DRMS[r] = refl_G_DRMS; //independent of angle
      array_G_Vterm[r] = refl_G_Vterm;
    }
  }

  af_string
  Epsilon::logEpsilon(
      std::string description,
      bool verbose
    ) const
  {
    af_string output;
    double max_bin(-std::numeric_limits<double>::max());
    double min_bin(std::numeric_limits<double>::max());
    double mean_bin(0.);
    for (int bin = 0; bin < VARBINS.size(); bin++)
    {
      const double var = VARBINS[bin];
      max_bin = std::max(var,max_bin);
      min_bin = std::min(var,min_bin);
      mean_bin += var;
    }
    output.push_back("Epsilon Parameters: " + description);
    output.push_back("TNCS Order =               " + std::to_string(TNCS_ORDER));
    if (TNCS_ORDER == 1)
    {
      output.push_back("\n");
      return output;
    }
    output.push_back("Translation Vector =       " + std::to_string(TNCS_VECTOR[0]) + " " +
                                                     std::to_string(TNCS_VECTOR[1]) + " " +
                                                     std::to_string(TNCS_VECTOR[2]));
    output.push_back("Rotational Deviation =     " + std::to_string(TNCS_ANGLE[0]) + " " +
                                                     std::to_string(TNCS_ANGLE[1]) + " " +
                                                     std::to_string(TNCS_ANGLE[2]));
    output.push_back("G-function Radius =        " + std::to_string(TNCS_GFNRAD));
    output.push_back("RMSD of TNCS copies =      " + std::to_string(TNCS_RMS));
    output.push_back("TNCS Fraction Scattering = " + std::to_string(TNCS_FS));
    output.push_back("Maximum bin scale factor = " + std::to_string(max_bin));
    output.push_back("Minimum bin scale factor = " + std::to_string(min_bin));
    output.push_back("Average bin scale factor = " + std::to_string(mean_bin/VARBINS.size()));
    output.push_back("\n");
    if (verbose)
    {
      for (int bin = 0; bin < VARBINS.size(); bin++)
        output.push_back("Resolution Bin Scale Factor #" + std::to_string(bin+1) + " = " +  std::to_string(VARBINS[bin]));
      if (array_G_DRMS.size())
      {
        double sum(0);
        sum = std::accumulate(array_G_DRMS.begin(), array_G_DRMS.end(), sum);
        double mean = sum/array_G_DRMS.size();
        output.push_back("Average G-drms= " + std::to_string(mean));
        PHASER_ASSERT(mean > 0);
      }
      if (array_G_Vterm.size())
      {
        double sum(0);
        sum = std::accumulate(array_G_Vterm.begin(), array_G_Vterm.end(), sum);
        double mean = sum/array_G_Vterm.size();
        output.push_back("Average G-Vterm= " + std::to_string(mean));
      }
      output.push_back("\n");
    }
    return output;
  }

  void
  Epsilon::initialize(bool halfR_)
  {
    halfR = halfR_; //for paranoia check before storing values
    //store the current status of the initialization, as halfR or not
    //this is relevant for refinement in GYRE and RBR
    //it also determines which arrays are stored
    //so that arrays are only filled as appropriate for halfR true/false
    //may need to call this twice, for halfR true and false when poses/gyres are mixed
    // Construct ncsRmat and its derivatives, then transpose for subsequent calcs
    PHASER_ASSERT(!UC.is_default());
    if (TNCS_ORDER == 1) return;

    //target
    xyzRotMatDeg rotmat;
    dmat33 ncsRmat = rotmat.perturbed(TNCS_ANGLE);
    if (halfR)
    {
      //convert to an angle about an axis
      dvect3 axis = scitbx::math::r3_rotation::axis_and_angle_from_matrix<double>(ncsRmat).axis;
      double angl = scitbx::math::r3_rotation::axis_and_angle_from_matrix<double>(ncsRmat).angle_rad;
      //convert back to matrix, with half the rotation
      ncsRmat = scitbx::math::r3_rotation::axis_and_angle_as_matrix(axis,angl/2);
    }
    ncsRmat_tr = ncsRmat.transpose();

    if (do_gradient)
    {
      dncsRmat_by_dRotn_tr.resize(3);
      for (int dir = 0; dir < 3; dir++)
        dncsRmat_by_dRotn_tr[dir] = identity;
      for (int dir = 0; dir < 3; dir++)
      {
        for (int dir2 = 0; dir2 < 3; dir2++)
        {
          bool thisdir2(dir==dir2);
          dncsRmat_by_dRotn_tr[dir2] =
            rotmat.tgh(dir,TNCS_ANGLE[dir],thisdir2,false)*dncsRmat_by_dRotn_tr[dir2];
        }
      }
      for (int dir = 0; dir < 3; dir++)
        dncsRmat_by_dRotn_tr[dir] = dncsRmat_by_dRotn_tr[dir].transpose();
      if (do_hessian)
      {
        d2ncsRmat_by_dRotn2_tr.resize(3);
        for (int dir = 0; dir < 3; dir++)
        {
          d2ncsRmat_by_dRotn2_tr[dir].resize(3,identity);
        }
        for (int dir = 0; dir < 3; dir++)
        {
          for (int dir2 = 0; dir2 < 3; dir2++)
          {
            bool thisdir2(dir==dir2);
            for (int dir3 = 0; dir3 < 3; dir3++)
            {
              if (dir2 == dir3)
              {
                d2ncsRmat_by_dRotn2_tr[dir2][dir3] =
                  rotmat.tgh(dir,TNCS_ANGLE[dir],false,thisdir2)*d2ncsRmat_by_dRotn2_tr[dir2][dir3];
              }
              else
              {
                bool thisdir3(dir==dir3);
                d2ncsRmat_by_dRotn2_tr[dir2][dir3] =
                    rotmat.tgh(dir,TNCS_ANGLE[dir],false,thisdir3)*d2ncsRmat_by_dRotn2_tr[dir2][dir3];
              }
            }
          }
        }
        for (int dir = 0; dir < 3; dir++)
          for (int dir2 = 0; dir2 < 3; dir2++)
            d2ncsRmat_by_dRotn2_tr[dir][dir2] = d2ncsRmat_by_dRotn2_tr[dir][dir2].transpose();
      }
    }

    for (int incs = 0; incs < TNCS_ORDER; incs++)
    for (int jncs = incs+1; jncs < TNCS_ORDER; jncs++)
    for (int isym = 0; isym < nsymp; isym++)
    {
      dmat33 ncsR_incs = (incs ? ncsRmat_tr : identity); //always identity for TNCS_ORDER>2
      dmat33 ncsR_jncs = (jncs ? ncsRmat_tr : identity); //always identity for TNCS_ORDER>2
      dvect3 ncsT_incs = double(incs)*TNCS_VECTOR;
      dvect3 ncsT_jncs = double(jncs)*TNCS_VECTOR;
      // NCS-related contribution to epsilon, weighted by interference, G-function and Drms terms
      dvect3 DeltaT = (ncsT_incs-ncsT_jncs);
      dvect3 ncsDeltaT = Rotsymtr[isym]*DeltaT; // symmetry translation cancels
      /*
      Metric matrix (represented as tensor) allows quick computation of r^2|s_klmm|^2
      and therefore rs^2 for G-function
      If F=fractionalization matrix, R=real-space rotational symmetry and (T) indicates transpose, then
      s_klmm = (ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h (equation 11 of Read et al, 2013)
      |s_klmm|^2 = s_klmm(T).s_klmm = h(T)*R*F*(ncsR_incs-ncsR_jncs)*(ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h
      The metrix matrix is the part between h(T) and h. In the GfunTensor array, factors of 2 are included so
      the unique terms need to be computed only once.
      */
      dmat33 delta_ncsRij = ncsR_incs-ncsR_jncs;
      dmat33 metmatTerm = GfnradRotsymtrFrac[isym]*delta_ncsRij;
      dmat33 metric_matrix = metmatTerm*metmatTerm.transpose();
      af::double6 GfunTensor(metric_matrix(0,0),
                             metric_matrix(1,1),
                             metric_matrix(2,2),
                             2*metric_matrix(0,1),
                             2*metric_matrix(0,2),
                             2*metric_matrix(1,2));
      //store the class variables below, used for reflection-wise calculations later
      GfunTensor_ijsym(incs,jncs,isym) = GfunTensor;
      ncsDeltaT_ijsym(incs,jncs,isym) = ncsDeltaT;
      //
      if (do_gradient or do_hessian)
      {
        if (REFINE_ROT) // Do all 3 rotation parameters together
        {
          sv_dmat33 dmetmatTerm(3);
          for (int dir = 0; dir < 3; dir++)
          {
            dmat33 dncsR_incs = (incs ? dncsRmat_by_dRotn_tr[dir] : zero33);
            dmat33 dncsR_jncs = (jncs ? dncsRmat_by_dRotn_tr[dir] : zero33);
            dmat33 delta_dncsRij = dncsR_incs-dncsR_jncs;
            dmetmatTerm[dir] = GfnradRotsymtrFrac[isym]*delta_dncsRij;
            dmat33 dmetmat_by_dRotn = metmatTerm*dmetmatTerm[dir].transpose();
                   dmetmat_by_dRotn += dmetmat_by_dRotn.transpose();
            af::double6 dGfunTensor_by_dRotn(
                  dmetmat_by_dRotn(0,0),
                  dmetmat_by_dRotn(1,1),
                  dmetmat_by_dRotn(2,2),
                  2*dmetmat_by_dRotn(0,1),
                  2*dmetmat_by_dRotn(0,2),
                  2*dmetmat_by_dRotn(1,2));
            dGfunTensor_by_dRotn_ijsymdir(incs,jncs,isym,dir) = dGfunTensor_by_dRotn;
          }
          if (do_hessian)
          {
            for (int dir = 0; dir < 3; dir++)
            {
              int dir2max = (is_diagonal) ? dir : 2;
              for (int dir2 = dir; dir2 <= dir2max; dir2++)
              {
                dmat33 d2ncsR_incs = (incs ? d2ncsRmat_by_dRotn2_tr[dir][dir2] : zero33);
                dmat33 d2ncsR_jncs = (jncs ? d2ncsRmat_by_dRotn2_tr[dir][dir2] : zero33);
                dmat33 delta_d2ncsRij = d2ncsR_incs-d2ncsR_jncs;
                dmat33 d2metmatTerm = GfnradRotsymtrFrac[isym]*delta_d2ncsRij;
                dmat33 d2metmat_by_dRotn2 =
                          metmatTerm*d2metmatTerm.transpose() +
                          dmetmatTerm[dir]*dmetmatTerm[dir2].transpose();
                       d2metmat_by_dRotn2 += d2metmat_by_dRotn2.transpose();
                af::double6 d2GfunTensor_by_dRotn2(
                      d2metmat_by_dRotn2(0,0),
                      d2metmat_by_dRotn2(1,1),
                      d2metmat_by_dRotn2(2,2),
                      2*d2metmat_by_dRotn2(0,1),
                      2*d2metmat_by_dRotn2(0,2),
                      2*d2metmat_by_dRotn2(1,2));
                d2GfunTensor_by_dRotn2_ijsymdir12(incs,jncs,isym,dir,dir2) = d2GfunTensor_by_dRotn2;
              }
            }
          }
        }
      }
    }
  }

  void
  Epsilon::flags(bool do_gradient_,bool do_hessian_,bool is_diagonal_)
  {
    do_gradient = do_gradient_;
    do_hessian = do_hessian_;
    is_diagonal = is_diagonal_; // Set false to use off-diagonal rotation terms
  }

  void
  Epsilon::calculate_reflection_parameters(
      const millnx miller,
      const double ssqr,
      const int    s
    )
  {
    if (TNCS_ORDER == 1) return; //rely on default parameters being returned
    dEps_by_dRot = {0,0,0};
    dEps_by_dGfn = 0;
    dEps_by_dTra = {0,0,0};
    dEps_by_dRms = 0;
    dEps_by_dFs = 0;
    dEps_by_dBin = 0;
    d2Eps_by_dRot2 = zero33;
    d2Eps_by_dGfn2 = 0;
    d2Eps_by_dTra2 = {0,0,0};
    d2Eps_by_dRms2 = 0;

    const int hh = miller[0]*miller[0];
    const int kk = miller[1]*miller[1];
    const int ll = miller[2]*miller[2];
    const int hk = miller[0]*miller[1];
    const int hl = miller[0]*miller[2];
    const int kl = miller[1]*miller[2];
    //recalculate tncs_epsn from changed parameters TNCS_ANGLE,TNCS_VECTOR,TNCS_VRMS
    double expterm = -2*fn::pow2(scitbx::constants::pi)*ssqr/3.;
    double fs_varbin = VARBINS[s]*TNCS_FS;
    double Drms = fs_varbin*std::exp(expterm*fn::pow2(TNCS_RMS));
    double dEps_by_dDrms(0);

    refl_TEPS = 0;
    refl_G_Vterm = 0;
    refl_Gsqr_Vterm = 0;
    std::fill(refl_Gsqr.begin(), refl_Gsqr.end(), 0.);
    std::fill(refl_G.begin(), refl_G.end(), 0.);
    refl_G_DRMS = fn::pow2(Drms); //for I; ==(phaser's G_DRMS)

    //Use half-triple summation, off-diagonal terms, rather than full quadruple summation
    double diagfac(1./(TNCS_ORDER-1));
    for (int incs = 0; incs < TNCS_ORDER; incs++)
    for (int jncs = incs; jncs < TNCS_ORDER; jncs++)
    for (int isym = 0; isym < nsymp; isym++)
    {
      if (incs == jncs)
      {
        refl_TEPS += symfac; // Normal symmetry contribution to epsilon
        refl_G_Vterm += symfac;
      }
      else
      {
        af::double6 GfunTensor = GfunTensor_ijsym(incs,jncs,isym);
        double rsSqr =
            GfunTensor[0]*hh +
            GfunTensor[1]*kk +
            GfunTensor[2]*ll +
            GfunTensor[3]*hk +
            GfunTensor[4]*hl +
            GfunTensor[5]*kl;
        double G = GfuncOfRSsqr.get(rsSqr);
        double h_dot_T = (miller*ncsDeltaT_ijsym(incs,jncs,isym));
        cmplex cos_sin = tbl_cos_sin.get(h_dot_T); //multiplies by 2*pi internally
        //double cosTerm = 2.*std::real(cos_sin); //factor of 2 ???
        double cosTerm = std::real(cos_sin);
        double TwoCosTerm = 2*cosTerm;
        double GcosTerm = 2*G*cosTerm;
        double sinTerm = std::imag(cos_sin);
        double GsinTerm = 2*G*sinTerm;
        double GsinTermSymDrms = -GsinTerm*symfac*Drms; // For derivative
        double GcosTermSym = GcosTerm*symfac;
        double thisGsqrTerm = 2*fn::pow2(G)*(diagfac + cosTerm)*symfac;
        refl_Gsqr_Vterm += thisGsqrTerm;
        refl_Gsqr[isym] += thisGsqrTerm; //weight for orientation error of symm copy in FRF
        refl_G[isym] = G;
        //refl_G is only used when halfR is true, i.e. when the same orientation
        //is used for two copies and refl_G compensates for an orientation error
        //of known size. For max_ncsT>2, refl_G will currently always be 1 but
        //this code would have to be modified if orientational differences were
        //characterised
        refl_TEPS += GcosTermSym*Drms;
        refl_G_Vterm += GcosTermSym;
        if (do_gradient or do_hessian)
        {
          //double cosSymDrms = cosTerm*symfac*Drms;
          double cosSymDrms = TwoCosTerm*symfac*Drms;
          double dEpsTerm_by_drsSqr   = cosSymDrms*dGfuncBydRSsqr.get(rsSqr);
          double d2EpsTerm_by_drsSqr2(0);
          if (REFINE_ROT) // Do all 3 rotation parameters together
          {
            dvect3 drsSqr_by_dRotn; //need dir/dir2 indexing
            for (int dir = 0; dir < 3; dir++)
            {
              af::double6 dGfunTensor_by_dRotn =
                dGfunTensor_by_dRotn_ijsymdir(incs,jncs,isym,dir);
              drsSqr_by_dRotn[dir] =
                dGfunTensor_by_dRotn[0]*hh +
                dGfunTensor_by_dRotn[1]*kk +
                dGfunTensor_by_dRotn[2]*ll +
                dGfunTensor_by_dRotn[3]*hk +
                dGfunTensor_by_dRotn[4]*hl +
                dGfunTensor_by_dRotn[5]*kl;
              dEps_by_dRot[dir] += dEpsTerm_by_drsSqr*drsSqr_by_dRotn[dir];
            }
            if (do_hessian)
            {
              d2EpsTerm_by_drsSqr2 = cosSymDrms*d2GfuncBydRSsqr2.get(rsSqr);
              for (int dir = 0; dir < 3; dir++)
              {
                int dir2max = (is_diagonal) ? dir : 2;
                for (int dir2 = dir; dir2 <= dir2max; dir2++)
                {
                  af::double6 d2GfunTensor_by_dRotn2 =
                    d2GfunTensor_by_dRotn2_ijsymdir12(incs,jncs,isym,dir,dir2);
                  double d2rsSqr_by_dRotn2 =
                    d2GfunTensor_by_dRotn2[0]*hh +
                    d2GfunTensor_by_dRotn2[1]*kk +
                    d2GfunTensor_by_dRotn2[2]*ll +
                    d2GfunTensor_by_dRotn2[3]*hk +
                    d2GfunTensor_by_dRotn2[4]*hl +
                    d2GfunTensor_by_dRotn2[5]*kl;
                  d2Eps_by_dRot2(dir,dir2) +=
                    (d2EpsTerm_by_drsSqr2*(drsSqr_by_dRotn[dir]*drsSqr_by_dRotn[dir2]) +
                    dEpsTerm_by_drsSqr*d2rsSqr_by_dRotn2);
                }
              }
            }
          }
          if (REFINE_GFN) // G-function radius
          {
            double drsSqr_by_dGfn = 2*rsSqr/TNCS_GFNRAD;
            dEps_by_dGfn += dEpsTerm_by_drsSqr*drsSqr_by_dGfn;
            if (do_hessian)
            {
              if (!d2EpsTerm_by_drsSqr2) //if not above!!
                d2EpsTerm_by_drsSqr2 = cosSymDrms*d2GfuncBydRSsqr2.get(rsSqr);
              double d2rsSqr_by_dGfn2 = 2*rsSqr/fn::pow2(TNCS_GFNRAD);
              d2Eps_by_dGfn2 += (d2EpsTerm_by_drsSqr2*fn::pow2(drsSqr_by_dGfn) +
                dEpsTerm_by_drsSqr*d2rsSqr_by_dGfn2);
            }
          }
          if (REFINE_TRA)
          {
            dvect3 twoPiRotH = scitbx::constants::two_pi*(SG.rotsym[isym]*miller);
            dEps_by_dTra += twoPiRotH*((incs-jncs)*GsinTermSymDrms);
            if (do_hessian)
            {
              for (int tra = 0; tra < 3; tra++)
              {
                double d2hdotT_by_dTra2 = -fn::pow2(twoPiRotH[tra]);
                d2Eps_by_dTra2[tra] += d2hdotT_by_dTra2*fn::pow2(incs-jncs)*GcosTermSym*Drms;
              }
            }
          }
          if (REFINE_RMSD or REFINE_FS or REFINE_BINS)
          {
            dEps_by_dDrms += GcosTermSym;
          }
          // Remaining parameters don't depend on symm op
        }
      }
    } // End of half-triple loop

    // Fill in matching off-diagonals for Hessian terms
    if (REFINE_ROT and do_hessian and !is_diagonal)
      for (int dir = 0; dir < 2; dir++)
        for (int dir2 = dir+1; dir2 < 3; dir2++)
          d2Eps_by_dRot2(dir2,dir) = d2Eps_by_dRot2(dir,dir2);

    if (REFINE_RMSD)
    {
      double dDrms_by_dRms = (2*Drms*expterm*TNCS_RMS);
      dEps_by_dRms = dEps_by_dDrms*dDrms_by_dRms;
      if (do_hessian)
      {
        double d2Drms_by_dRms2 = (2*Drms*expterm + 4*Drms*fn::pow2(TNCS_RMS*expterm));
        d2Eps_by_dRms2 = dEps_by_dDrms*d2Drms_by_dRms2;
      }
    }
    if (REFINE_FS)
    {
      double dDrms_by_dFs = (Drms/TNCS_FS);
      dEps_by_dFs = dEps_by_dDrms*dDrms_by_dFs;
      //double d2Drms_by_dFs2(0);
    }
    if (REFINE_BINS)
    {
      double dDrms_by_dBin = (Drms/VARBINS[s]);
      dEps_by_dBin = dEps_by_dDrms*dDrms_by_dBin;
      // double d2Drms_by_dBin2(0);
    }
  }

} //phasertng
