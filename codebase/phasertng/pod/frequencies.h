//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_pod_frequencies_class__
#define __phasertng_pod_frequencies_class__
#include <scitbx/vec3.h>

namespace phasertng {
namespace pod {

class frequencies
{
  public:
    frequencies() {}

    scitbx::vec3<int> index = scitbx::vec3<int>(0,0,0);
    int    nmol = 0; //freq
    double strength = 0;
    double distance = 0;
};
}

inline bool sort_freq_strength(const pod::frequencies &left,const pod::frequencies &right)
{ return left.strength < right.strength; }

} //phasertng

#endif
