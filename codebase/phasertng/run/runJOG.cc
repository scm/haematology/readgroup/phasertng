//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/src/CutoutDensity.h>
#include <phasertng/math/rotation/cmplex2polar_deg.h>
#include <phasertng/math/rotation/polar2cmplex_deg.h>
#include <phasertng/src/Packing.h>
#include <phasertng/minimizer/RefineRB.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/data/ReflRowsData.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/math/table/sim.h>

namespace phasertng {
extern const table::sim& tbl_sim;

  void Phasertng::runJOG()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    if (!REFLECTIONS->prepared(reflprep::FEFF))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (feff)");
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    //dag nodes are already loaded in Phasertng.cc

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose();
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::MONOSTRUCTURE_PDB,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR); //change to interp::EIGHT for refinement

    PHASER_ASSERT(!REFLECTIONS->MAP); //because we move the model to the unit cell, and this will break map solutions
    //no tNCS in maps anyway

    //initialize tNCS parameters from reflections and calculate correction terms
    //POSE
    bool store_halfR(true);
    bool store_fullR(true);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Prepared Data"));
    }}

    int io_nref = input.value__int_number.at(".phasertng.jog_refinement.maximum_stored.").value_or_default();
    int io_nprint = input.value__int_number.at(".phasertng.jog_refinement.maximum_printed.").value_or_default();
    double io_percent = input.value__percent.at(".phasertng.jog_refinement.percent.").value_or_default();
    double io_closest = input.value__flt_paired.at(".phasertng.jog_refinement.distance.").value_or_default_min();
    double io_farthest = input.value__flt_paired.at(".phasertng.jog_refinement.distance.").value_or_default_max();
    input.value__boolean.at(".phasertng.jog_refinement.jogged.").set_value(false);

    //calculate the default sampling
    double cluster_distance,sampling;
    {{
    std::tie(cluster_distance,sampling) = EntryHelper::clustdist(
      selected.STICKY_HIRES,
      false, //helix
      false, //coiled coil
      1.0, //helixfactor
      input.value__flt_number.at(".phasertng.jog_refinement.sampling_factor.").value_or_default(),
      input.value__flt_number.at(".phasertng.jog_refinement.sampling.").value_or_default());
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Sampling");
    logTab(where,"Sampling: " + dtos(sampling,5,2) + " Angstroms");
    logTab(where,"Cluster Distance: " + dtos(cluster_distance,5,2) + " Angstroms");
    logBlank(where);
    }}

    bool haveFpart = true; //phases
    REFLECTIONS->setup_mtzcol(type::mtzcol('F',"MAPF",labin::MAPF));
    REFLECTIONS->setup_mtzcol(type::mtzcol('P',"MAPPH",labin::MAPPH));
    REFLECTIONS->setup_mtzcol(type::mtzcol('F',"SIGA",labin::SIGA));
    REFLECTIONS->setup_mtzcol(type::mtzcol('F',"FC",labin::FCNAT));
    REFLECTIONS->setup_mtzcol(type::mtzcol('P',"PHIC",labin::PHICNAT));

    {{
    logTab(out::VERBOSE,"Pose orthogonal perturbations incorporated");
    auto& UC = REFLECTIONS->UC;
    dmat33 orthmat = UC.orthogonalization_matrix();
    dmat33 fracmat = UC.fractionalization_matrix();
    dmat33 ROT;dvect3 FRACT;
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      for (int p = 0; p < DAGDB.WORK->POSE.size(); p++)
      {
        dag::Pose& pose = DAGDB.WORK->POSE[p];
        std::tie(ROT,FRACT) = pose.finalRT(orthmat,fracmat);
        pose.FRACT = FRACT;
        pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT);
        pose.ORTHT = dvect3(0,0,0);
        pose.ORTHR = dvect3(0,0,0);
      }
      DAGDB.WORK->generic_flag = false;
    }
    }}

    dag::NodeList input_nodes = DAGDB.NODES; //replaced later
    DAGDB.shift_tracker(hashing(),input.running_mode);

    sv_string   jogged_info(DAGDB.size());
    std::vector<sv_bool> jogged_moved(DAGDB.size());
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Pose Optimization by Phased Translation Function");
    logTab(where,"Percent origin peak cutoff: "  + dtos(io_percent,5,1) + "%");
    logTab(where,"Jog Distance Range: "  + dtos(io_closest,5,1) + "->" + dtos(io_farthest,5,1));
    int n(1),N(DAGDB.size());
    int j(-1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      j++;
      if (n > 1)
      {//reset
        REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      }
      std::string nN = nNtos(n++,N);
      logUnderLine(where,"Jog Refinement" + nN);
      if (!DAGDB.WORK->is_all_full())
      {
        logTab(where,"No map calculated: check rbm mode");
        continue;
      }
      sv_dvect3 shifts(DAGDB.WORK->POSE.size(),dvect3(0,0,0));
      {{
        bool moved(false);
        for (int p = 0; p < DAGDB.WORK->POSE.size(); p++)
        { //move the coordinates to the unit cell, where the map is
          auto& pose = DAGDB.WORK->POSE[p];
          for (int j = 0; j < 3; j++)
          {
            if (pose.FRACT[j] < 0) moved = true;
            if (pose.FRACT[j] >= 1.0) moved = true;
            while (pose.FRACT[j] < 0) pose.FRACT[j] += 1.0,shifts[p][j]-=1;
            while (pose.FRACT[j] >= 1.0) pose.FRACT[j] -= 1.0,shifts[p][j]+=1;
          }
         // pose.BFAC = 0; //because we will change to 0 after jog and we want to compare with same b-factor
        }
        if (moved)
          logAdvisory("Poses moved to central unit cell");
      }}
      pod::FastPoseType ecalcs_sigmaa_pose;
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      auto selected_array = selected.get_selected_array();
      DAGDB.apply_interpolation(interp::FOUR); //change to interp::EIGHT after input-LLG calculation
      ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa( //interp::FOUR, input-LLG
          REFLECTIONS.get(),
          selected_array,
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS);
      ecalcs_sigmaa_pose.precalculated = true;
      phaser_assert(ecalcs_sigmaa_pose.ecalcs.size() == REFLECTIONS->NREFL);
      DAGDB.work_node_likelihood( //interp::FOUR, input-LLG
          REFLECTIONS.get(),
          selected_array,
          &epsilon,
          NTHREADS,
          USE_STRICTLY_NTHREADS,
          &ecalcs_sigmaa_pose);
    // input_nodes[j].LLG = DAGDB.WORK->LLG;
      input_nodes[j] = *DAGDB.WORK;
      input_nodes[j].generic_float = DAGDB.WORK->LLG;
      logTab(out::VERBOSE,"Input LLG = " + dtos(DAGDB.WORK->LLG,2));

      DAGDB.apply_interpolation(interp::EIGHT); //change to interp::EIGHT for refinement

      //calculate the structure factors for the map and expand to p1
      logEllipsisOpen(out::VERBOSE,"Calculate density");
      auto txt = REFLCOLSMAP.calculate_map(
          REFLECTIONS.get(),
          ecalcs_sigmaa_pose,
          selected.get_selected_array(),
          DAGDB.NODES.is_all_tncs_modelled());
      logEllipsisShut(out::VERBOSE);
      logTabArray(out::LOGFILE,txt);

      bool debugger(Suite::Extra());
      if (debugger)
      {
        Identifier modlid = DAGDB.WORK->TRACKER.ULTIMATE; //modify tag here
        modlid.initialize_tag("jog_n" + ntos(n-1,N));
        DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
        out::stream where = out::LOGFILE;
        REFLECTIONS->REFLID = modlid;
        REFLECTIONS->REFLPREP.insert(reflprep::SGX);
        REFLCOLSMAP.SetFileSystem(DataBase(),dogtag.FileName(".mtz"));
        logFileWritten(out::LOGFILE,WriteFiles(),"Jog",REFLCOLSMAP);
        if (WriteFiles()) REFLCOLSMAP.write_to_disk(); //ReflColsMap
        Coordinates coordinates;
        {{
        //use packing as a cheap way of creating monostructure
        Packing packing;
        packing.init_node(DAGDB.WORK);
        packing.structure_for_refinement(coordinates);
        coordinates.MODLID = modlid;
        coordinates.REFLID = modlid;
        coordinates.TIMESTAMP = TimeStamp();
        coordinates.SetFileSystem(DataBase(),dogtag.FileEntry(entry::COORDINATES_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Coordinates",coordinates);
        if (WriteFiles()) coordinates.write_to_disk();
        }}
      }

      logTab(out::VERBOSE,"Expand Reflections to P1");
      ReflColsMap P1;
      //REFLECTIONS.reset(new ReflColsMap()); //this is a copy
      P1.copy_and_reformat_data(REFLECTIONS.get()); //this is a copy
      bool change_spacegroup_to_p1(true);
      P1.expand_to_p1(change_spacegroup_to_p1);
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&P1,[](ReflColsMap *) {});
      DAGDB.WORK->HALL = SpaceGroup("P 1").HALL;
      DAGDB.WORK->SG = nullptr; //explicit set here
      DAGDB.reset_entry_pointers(DataBase()); //which resets space group also
      auto ssqr_array = P1.get_data_array(labin::SSQR);
      auto used_modlid = DAGDB.NODES.used_modlid_pose();
      for (auto id : used_modlid)
        DAGDB.ENTRIES[id].INTERPOLATION.setup_sigmaa_sqr(ssqr_array);

      //replace with p1 data
      Selected selected_p1(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
      auto seltxt = selected_p1.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
      logTabArray(out::VERBOSE,seltxt);
      seltxt = selected_p1.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
      selected_p1.flag_standard_outliers_and_select(REFLECTIONS.get());
      logTabArray(out::LOGFILE,selected_p1.logSelected(""));
      if (Suite::Extra())
        logTabArray(out::TESTING,selected_p1.logOutliers(""));
      if (Suite::Extra())
        logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Prepared Data"));
      Epsilon epsilon_p1;
      epsilon_p1.setup_parameters(REFLECTIONS.get());
      epsilon_p1.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

      //copy the expanded values to the separate vectors
      if (debugger)
      {
        ecalcs_sigmaa_pose.resize(REFLECTIONS->NREFL);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          if (selected_p1.get_selected(r))
          {
            double fc = REFLECTIONS->get_flt(labin::FCNAT,r);
            double phic = REFLECTIONS->get_flt(labin::PHICNAT,r);
            ecalcs_sigmaa_pose.sigmaa[r] = REFLECTIONS->get_flt(labin::SIGA,r);
            ecalcs_sigmaa_pose.ecalcs[r] = polar2cmplex_deg({fc,phic});
          }
        }
        sv_double fmap_stored(REFLECTIONS->NREFL,0);
        sv_double phmap_stored(REFLECTIONS->NREFL,0);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          fmap_stored[r] = REFLECTIONS->get_flt(labin::MAPF,r);
          phmap_stored[r] = REFLECTIONS->get_flt(labin::MAPPH,r);
        }
      }

      af_millnx miller_res_limits(REFLECTIONS->NREFL);
      af_cmplex coefficients(REFLECTIONS->NREFL);
      //we need to store these because they are going to be replaced with the cutout density
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const millnx miller = REFLECTIONS->get_miller(r);
        const double fmap =  REFLECTIONS->get_flt(labin::MAPF,r);
        const double phmap = REFLECTIONS->get_flt(labin::MAPPH,r);
        miller_res_limits[r] = miller;
        coefficients[r] = polar2cmplex_deg({fmap,phmap});
      }

      if (debugger)
      {
        Identifier modlid = DAGDB.WORK->TRACKER.ULTIMATE; //modify tag here
        modlid.initialize_tag("jog_i" + ntos(n-1,N));
        DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
        out::stream where = out::LOGFILE;
        REFLECTIONS->REFLID = modlid;
        REFLECTIONS->REFLPREP.insert(reflprep::SGX);
        P1.SetFileSystem(DataBase(),dogtag.FileName(".mtz"));
        logFileWritten(out::LOGFILE,WriteFiles(),"Jog",P1);
        if (WriteFiles())
          P1.write_to_disk();
        Coordinates coordinates;
        {{
        //use packing as a cheap way of creating monostructure
        Packing packing;
        packing.init_node(DAGDB.WORK);
        packing.structure_for_refinement(coordinates);
        coordinates.MODLID = modlid;
        coordinates.REFLID = modlid;
        coordinates.TIMESTAMP = TimeStamp();
        }}
        coordinates.SetFileSystem(DataBase(),dogtag.FileEntry(entry::COORDINATES_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Coordinates",coordinates);
        if (WriteFiles())
          coordinates.write_to_disk();
        //make sure the ecalcs sigmaa has been expanded above
        PHASER_ASSERT(ecalcs_sigmaa_pose.ecalcs.size() == REFLECTIONS->NREFL);
        DAGDB.work_node_likelihood( //previously interp::FOUR, now interp::EIGHT
                REFLECTIONS.get(),
                selected_p1.get_selected_array(),
                &epsilon_p1,
                NTHREADS,
                USE_STRICTLY_NTHREADS,
                &ecalcs_sigmaa_pose);
        logTab(where,"LLG = " + dtoi(DAGDB.WORK->LLG));
        logTabArray(out::VERBOSE,DAGDB.WORK->logPose("Input"));
      }
      logTabArray(out::VERBOSE,input_nodes[j].logPose("Node "+nN));
      DAGDB.WORK->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
    //  REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG); don't do this

      dag::Node original_work = *DAGDB.WORK; //deep copy original, need to modify for calculations
      std::vector<dag::Pose> original_pose = input_nodes[j].POSE;
      std::vector<dag::Pose> adjusted_pose = input_nodes[j].POSE;
      int maxp = input_nodes[j].POSE.size();
      DAGDB.WORK->POSE.clear();
      DAGDB.reset_entry_pointers(DataBase());

      sv_double jogged_distance(maxp,0);
      sv_double jogged_tfz(maxp,0);
      sv_double jogged_fss(maxp,0);
      std::string jogged_poses;

      for (int p = 0; p < maxp; p++) //there will be only one
      {
        std::string pP = nNtos(p+1,maxp);
        logChevron(where,"Optimizing Pose"+pP+" for"+nN);

        //replace reflections with stored P1 values
        //only required for p > 0
        REFLECTIONS = std::shared_ptr<ReflColsMap>(&P1,[](ReflColsMap *) {});
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          cmplex cmap = coefficients[r];
          std::pair<double,double> pmap = cmplex2polar_deg(cmap);
          REFLECTIONS->set_flt(labin::MAPF,pmap.first,r);
          REFLECTIONS->set_flt(labin::MAPPH,pmap.second,r);
        }

        FastTranslationFunction trafun( //interp::EIGHT, EightPt, note ftf is FourPt
            REFLECTIONS.get(),
            selected_p1.get_selected_array(),
            &epsilon_p1
            );
        trafun.allocate_memory(REFLECTIONS.get(),sampling,haveFpart);
        logTabArray(out::VERBOSE,trafun.logSetup());
        logTab(out::VERBOSE,"Gridding: " + ivtos(trafun.gridding));

        std::pair<dvect3,dvect3> extent;
        {{
        out::stream where = out::VERBOSE;
        dag::Pose& pose = DAGDB.WORK->POSE[p];
        const Monostructure& ptrace = pose.ENTRY->MONOSTRUCTURE;
        PHASER_ASSERT(ptrace.size());
        dmat33 ROT = zyz_matrix(pose.EULER);
        const dvect3 PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
        dvect3 FRACT = pose.FRACT;
        double maxn = std::numeric_limits<double>::max();
        double minn = std::numeric_limits<double>::lowest();
        auto& UC = REFLECTIONS->UC;
        dvect3 minlimits(maxn,maxn,maxn);
        dvect3 maxlimits(minn,minn,minn);
        SpaceGroup SG("P 1");
        PdbHandler pdbhandler;
        if (debugger)
        {
          pdbhandler.addCrystCard(SG,UC,1);
          pdbhandler.addModel();
        }
        for (int a_chk = 0; a_chk < ptrace.size(); a_chk++)
        {
          PdbRecord card = ptrace.PDB[a_chk]; //deep copy
          card.X = ROT*card.X + UC.orthogonalization_matrix()*FRACT;
          for (int i = 0; i < 3; i++)
          {
            minlimits[i] = std::min(minlimits[i],card.X[i]);
            maxlimits[i] = std::max(maxlimits[i],card.X[i]);
          }
          if (debugger)
          {
            pdbhandler.addRecord(card);
          }
        }
        extent =  { minlimits, maxlimits};
        if (debugger)
        {
          logBlank(where);
          FileSystem filename;
          Identifier modlid = DAGDB.WORK->TRACKER.ULTIMATE; //modify tag here
          modlid.initialize_tag("jog_n" + ntos(n-1,N) + "_p" + ntos(p+1,maxp));
          DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
          filename.SetFileSystem(DataBase(),dogtag.FileName(".cutout_density.pdb"));
          logFileWritten(out::LOGFILE,WriteFiles(),"Cutout Coordinates",filename);
          if (WriteFiles())
            pdbhandler.VanillaWritePdb(filename.fstr());
        }
        logBlank(where);
        logTab(where,"Extent Orthogonal " + dvtos(extent.first,6,2) + " " + dvtos(extent.second,6,2));
        dmat33 mfrac = REFLECTIONS->UC.cctbxUC.fractionalization_matrix();
        extent.first = mfrac*extent.first;
        extent.second = mfrac*extent.second;
        logTab(where,"Extent Fractional " + dvtos(extent.first,6,2) + " " + dvtos(extent.second,6,2));
        logBlank(where);
        }}

        {{ //replace the map with a cutout map
        out::stream where = out::VERBOSE;
        CutoutDensity cutout_density;
        auto text = cutout_density.calculate(
            REFLECTIONS->SG.group(),
            REFLECTIONS->UC.cctbxUC,
            miller_res_limits,
            coefficients,
            extent
          );
        logTabArray(where,text);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          cmplex cmap  = cutout_density.FCUT[r];
          std::pair<double,double> pmap = cmplex2polar_deg(cmap);
          REFLECTIONS->set_flt(labin::MAPF,pmap.first,r);
          REFLECTIONS->set_flt(labin::MAPPH,pmap.second,r);
        }
        if (debugger)
        {
          logBlank(where);
          FileSystem filename;
          Identifier modlid = DAGDB.WORK->TRACKER.ULTIMATE; //modify tag here
          modlid.initialize_tag("jog_n" + ntos(n-1,N) + "_p" + ntos(p+1,maxp));
          DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
          filename.SetFileSystem(DataBase(),dogtag.FileName(".cutout_density.mtz"));
          logFileWritten(out::LOGFILE,WriteFiles(),"Cutout Density",filename);
          if (WriteFiles())
            cutout_density.WriteMtz(filename.fstr());
        }
        }}

        {{
        out::stream where = out::VERBOSE;
        trafun.translation_function_data.clear();
        //strip the node back to just the rotation under consideration
        DAGDB.WORK->NEXT.PRESENT = true;
        DAGDB.WORK->NEXT.IDENTIFIER = original_pose[p].IDENTIFIER;
        DAGDB.WORK->NEXT.EULER = original_pose[p].EULER;
        DAGDB.WORK->NEXT.ORTHR = dvect3(0,0,0); //because we have used finalRT original_pose[p].ORTHR;
        DAGDB.reset_work_entry_pointers(DataBase());
        logTab(where,"Turn Rotation " + dvtos(original_pose[p].EULER,6,1));
        trafun.calculate_ptf(REFLECTIONS.get(),DAGDB.WORK);
        logTab(where,"Mean and Standard Deviation");
        trafun.statistics();
        logTab(where,snprintftos(
            "%11s %10s % 10s %10s",
            "Mean","Sigma","Top","Top-Z"));
        int q = trafun.precision();
        logTab(where,snprintftos(
            "% 10.*f %10.*f % 10.*f %10.3f",
            q,trafun.f_mean,q,trafun.f_sigma,q,trafun.f_top,trafun.z_top));
        logBlank(where);
        }}

        //peak search from map, but only store the ones that are over the percent
        //the percent cutoff is used here as only the fast search has a mean and stddev
        //continuous shifts and score is slow if done on all peaks
        dmat33 ROT = zyz_matrix(DAGDB.WORK->NEXT.EULER);
        dvect3 PT = DAGDB.WORK->NEXT.ENTRY->PRINCIPAL_TRANSLATION;
        {{
        out::stream where = out::VERBOSE;
        af_double peak_heights; af_dvect3 peak_sites;
        trafun.peak_search(peak_heights,peak_sites);
        logEllipsisOpen(where,"Apply Mean Cutoff");
        trafun.apply_mean_cutoff(peak_heights,peak_sites);
        logEllipsisShut(where);
        int num_peaks(peak_sites.size());
        logEllipsisOpen(where,"Peak search and purge by percent");
        auto txt = trafun.apply_percent_cutoff(peak_heights,peak_sites,io_percent);
        trafun.continuous_shifts_and_store(peak_heights,peak_sites,DAGDB.WORK->NEXT.EULER,PT);
        logEllipsisShut(where);
        logTabArray(where,txt);
        logTab(where,"Number before purge: " + itos(num_peaks));
        logTab(where,"Number after purge:  " + itos(trafun.translation_function_data.size()));
        logBlank(where);
        }}

        if (trafun.count_sites() == 0) continue;

        // trafun.full_sort(); //search puts them in order
        trafun.clustering_off(); //reset all to top

        dvect3& ref_fract = original_work.POSE[p].FRACT;
        //ptf is wrt in (see ptf) so convert fract (which is wrt mt) to wrt in also for comp

        {{
        out::stream where = out::VERBOSE;
        bool AllSites(true);
        logTableTop(where,"Phased Translation Function Pose"+pP+" for"+nN,false);
        logTabArray(where,trafun.logJogTable(io_nprint,AllSites,ref_fract,DAGDB.WORK->NEXT.EULER,PT));
        logTableEnd(where);
        }}

        {{
        out::stream where = out::LOGFILE;
        logTab(where,"Phased Translation Function Peaks in Range"+pP+" for"+nN);
        logTab(where,"Jog Distance Range: "  + dtos(io_closest,5,1) + "->" + dtos(io_farthest,5,1));
        PHASER_ASSERT(io_closest <= io_farthest);
        bool found_in_range(false),identity_peak(false);
        dvect3 ref_ortht = REFLECTIONS->UC.orthogonalization_matrix()*ref_fract;
        double mindist = std::numeric_limits<double>::max();
        dvect3 minvect(0,0,0); //i.e. no change if nothing found
        double zscore(0),pscore(0);
        PHASER_ASSERT(trafun.translation_function_data.size());
        double f_top = trafun.translation_function_data[0].value;
        af_string text;
        int w = itow(trafun.translation_function_data.size());
        text.push_back(snprintftos(
          "%*s %-s",
          w,"#","( FracX  FracY  FracZ) ( OrthX  OrthY  OrthZ)   TFZ  FSS%  Distance(A)"));
        bool top_in_range(true);
        for (auto item : trafun.translation_function_data)
        {
          for (int i = -1; i < 2; i++)
          for (int j = -1; j < 2; j++)
          for (int k = -1; k < 2; k++)
          {
            dvect3 item_fract = item.grid + dvect3(i,j,k);
            dvect3 chk_fract = dag::fract_wrt_mt_from_in(item_fract,DAGDB.WORK->NEXT.EULER,PT,DAGDB.WORK->CELL);
            dvect3 chk_orth = REFLECTIONS->UC.orthogonalization_matrix()*chk_fract;
            dvect3 diff_orth = chk_orth-ref_ortht;
            dvect3 diff_frac = REFLECTIONS->UC.fractionalization_matrix()*diff_orth;
            double distance = std::sqrt(diff_orth*diff_orth);
            double this_zscore = (item.value-trafun.f_mean)/trafun.f_sigma;
            double this_pscore = 100.*(item.value-trafun.f_mean)/(f_top-trafun.f_mean);
            std::string txt =
                itos(text.size(),w,false,false) + " ("  +
                dvtos(diff_frac,6,3) + ") (" + dvtos(diff_orth,6,2) + ")" +
                dtos(this_zscore,6,1) + " " + dtos(this_pscore,5,1) + " " +
                dtos(distance,6,2);
            if (distance <= io_farthest and distance >= io_closest)
            {
              //if (distance < mindistfound) //take closest
              if (!found_in_range and top_in_range) //take the top in range
              {
                text.push_back(txt + "*");
                found_in_range = true;
                zscore = this_zscore;
                pscore = this_pscore;
                mindist = distance;
                minvect = chk_fract;
              }
              else
              {
                text.push_back(txt); //without star
              }
            }
            else if (distance < io_closest)
            {
              text.push_back(txt); //without star
              identity_peak = true;
            }
            if (distance <= io_farthest)
              top_in_range = false;
          }
        }
        //write out result wrt in, which is the interesting ones
        if (p and (p%nmol)==0)
        {
          jogged_info[j].push_back(' '); //add a spacer to separate tNCS groups, to help locate patterns in the shifts
        }
        if (found_in_range)
        {
          //note that the closest delta does not include the ORTHT of the original, only the FRACT
          logTab(where,"Number of peaks in PTF in range = " + itos(text.size()-1));
          logTabArray(where,text);
          minvect = dag::fract_wrt_mt_from_in(minvect,ROT,PT,DAGDB.WORK->CELL);
          adjusted_pose[p].FRACT = minvect;
          adjusted_pose[p].ORTHT = dvect3(0,0,0); //because the  ORTHT was set to zero
          jogged_distance[p] = mindist;
          jogged_tfz[p] = zscore;
          jogged_fss[p] = pscore;
          jogged_info[j].push_back('+');
          DAGDB.WORK->generic_flag = true; //not actually used
          input.value__boolean.at(".phasertng.jog_refinement.jogged.").set_value(true);
          jogged_poses += itos(p+1) +' ';
          jogged_moved[j].push_back(true);
        }
        else if (identity_peak)
        {
          logTab(where,"Number of peaks in PTF in range = " + itos(text.size()-1));
          logTabArray(where,text);
          logTab(where,"Peak in PTF very close to current position, no change");
          jogged_info[j].push_back('-');
          jogged_moved[j].push_back(false);
        }
        else
        {
          logTab(where,"No peaks in PTF in range, no change");
          jogged_info[j].push_back('-');
          jogged_moved[j].push_back(false);
        }
        //convert ptf result which is wrt in to wrt mt for storage in internal pose
        //will be converted to wrt in on output
        logBlank(where);
        }}
      }

      {{
      logEllipsisOpen(where,"Calculating likelihood of initial pose"+nN);
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      auto ssqr_array = REFLECTIONS->get_data_array(labin::SSQR);
      for (auto id : used_modlid)
        DAGDB.ENTRIES[id].INTERPOLATION.setup_sigmaa_sqr(ssqr_array);
      //shift trackers  at the start ready for file output
      //BUT don't overwrite the trackers here!
      auto tracker = DAGDB.NODES[j].TRACKER;
      DAGDB.NODES[j] = input_nodes[j]; //reset space group et al
      DAGDB.WORK = &DAGDB.NODES[j]; //reset work pointer
      DAGDB.WORK->TRACKER = tracker;
      //not adjusted poses! just the b-factors
      DAGDB.reset_work_entry_pointers(DataBase());
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      ecalcs_sigmaa_pose.precalculated = false;
      DAGDB.work_node_likelihood( //interp::EIGHT, previously interp::FOUR
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS,
                &ecalcs_sigmaa_pose);
      input_nodes[j].generic_float2 = DAGDB.WORK->LLG;
      logEllipsisShut(where);
      logTab(where,"Initial LLG = " + dtos(DAGDB.WORK->LLG,2));
      logTabArray(out::VERBOSE,DAGDB.WORK->logPose("Initial"));
      }}

      {{
      logEllipsisOpen(where,"Calculating likelihood of jogged pose"+nN);
      REFLECTIONS = std::shared_ptr<ReflColsMap>(&REFLCOLSMAP,[](ReflColsMap *) {});
      //shift trackers  at the start ready for file output
      //BUT don't overwrite the trackers here!
      auto tracker = DAGDB.NODES[j].TRACKER;
      DAGDB.NODES[j] = input_nodes[j]; //reset space group et al
      DAGDB.WORK = &DAGDB.NODES[j]; //reset work pointer
      DAGDB.WORK->TRACKER = tracker;
      DAGDB.WORK->POSE = adjusted_pose; //copy over the pose
      for (int p = 0; p < DAGDB.WORK->POSE.size(); p++)
      { //move the coordinates back to original position in whole unit cell jumps
        DAGDB.WORK->POSE[p].FRACT += shifts[p];
       // DAGDB.WORK->POSE[p].BFAC = 0;
        //Reset bfactor because the ones that are in the wrong place will have screwed up b-factor
      }
      DAGDB.reset_work_entry_pointers(DataBase());
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      ecalcs_sigmaa_pose.precalculated = false;
      DAGDB.work_node_likelihood( //previously interp::FOUR
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS,
                &ecalcs_sigmaa_pose);
      DAGDB.WORK->generic_float = DAGDB.WORK->LLG;
      logEllipsisShut(where);
      logTab(where,"Jogged LLG = " + dtos(DAGDB.WORK->LLG,2));
      logTabArray(out::VERBOSE,DAGDB.WORK->logPose("Jogged"));
      }}

      bool refine_off =
        input.value__choice.at(".phasertng.macjog.macrocycle1.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macjog.macrocycle2.").value_or_default_equals("off") and
        input.value__choice.at(".phasertng.macjog.macrocycle3.").value_or_default_equals("off");
      refine_off = refine_off or
        input.value__choice.at(".phasertng.macjog.protocol.").value_or_default_equals("off");
      bool one_moved(false);
      for (int p = 0; p < jogged_moved[j].size(); p++)
         if (jogged_moved[j][p]) one_moved = true;
      if (!one_moved) refine_off = true;

      if (refine_off)
      {
        if (!one_moved)
          logTab(where,"No poses jogged");
        logTab(where,"No rigid body refinement");
        DAGDB.WORK->ANNOTATION += " JOG=" + dtoi(DAGDB.WORK->LLG);
      }
      else //use unphased data
      {
        logTab(out::LOGFILE,"Poses jogged: " + jogged_poses);
        std::vector<sv_string> macro = {
          input.value__choice.at(".phasertng.macjog.macrocycle1.").value_or_default_multi(),
          input.value__choice.at(".phasertng.macjog.macrocycle2.").value_or_default_multi(),
          input.value__choice.at(".phasertng.macjog.macrocycle3.").value_or_default_multi()
        };
        sv_int ncyc = {
          input.value__int_vector.at(".phasertng.macjog.ncyc.").value_or_default(0),
          input.value__int_vector.at(".phasertng.macjog.ncyc.").value_or_default(1),
          input.value__int_vector.at(".phasertng.macjog.ncyc.").value_or_default(2)
        };
        auto method = input.value__choice.at(".phasertng.macjog.minimizer.").value_or_default();
        double lowerB = REFLECTIONS->bfactor_minimum();
        if (!input.value__flt_number.at(".phasertng.bfactor.minimum.").is_default())
          lowerB = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
        double upperB = input.value__flt_number.at(".phasertng.bfactor.maximum.").value_or_default();
        logProtocol(out::LOGFILE,macro,ncyc,method);
        //logTab(out::LOGFILE,"B-factors reset to zero"); //bfactors not set to zero
        logTab(out::LOGFILE,"B-factor range: " + dtos(lowerB,2) + " " + dtos(upperB,2));
        logBlank(where);
        logEllipsisOpen(where,"Rigid body refinement");
        REFLECTIONS.reset(new ReflRowsData());
        REFLECTIONS->copy_and_reformat_data(&REFLCOLSMAP);
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(DAGDB.WORK->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        RefineRB refine(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon);
        refine.SIGMA_ROT = input.value__flt_number.at(".phasertng.macjog.restraint_sigma.rotation.").value_or_default();
        refine.SIGMA_TRA = input.value__flt_number.at(".phasertng.macjog.restraint_sigma.translation.").value_or_default();
        refine.SIGMA_BFAC = input.value__flt_number.at(".phasertng.macjog.restraint_sigma.bfactor.").value_or_default();
        refine.SIGMA_DRMS = input.value__flt_number.at(".phasertng.macjog.restraint_sigma.drms.").value_or_default();
        refine.SIGMA_CELL = input.value__flt_number.at(".phasertng.macjog.restraint_sigma.cell.").value_or_default();
        refine.BFAC_MIN = lowerB;
        refine.BFAC_MAX = upperB;
        refine.CELL_MIN = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_min();
        refine.CELL_MAX = input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default_max();
        refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
        refine.init_node(DAGDB.WORK);
        dtmin::Minimizer minimizer(this);
        minimizer.set_logfile_level(out::TESTING);
        bool study_params(false); //study these with rbr mode
        minimizer.run(
          refine,
          macro,
          ncyc,
          method,
          study_params,
          input.value__flt_number.at(".phasertng.macjog.small_target.").value_or_default()
        );
        if (refine.negvar) logWarning("Negative variance ignored, contact developers");
        DAGDB.WORK->generic_float2 = -refine.start_likelihood; //interp::EIGHT
        double llg = -refine.final_likelihood; //likelihood(); store llg in node //interp::EIGHT
        DAGDB.WORK->LLG = llg; //interp::EIGHT
        DAGDB.apply_interpolation(interp::FOUR); //change to interp::EIGHT for refinement
        DAGDB.WORK->ANNOTATION += " JOG=" + dtoi(llg);
        DAGDB.WORK->generic_bool = minimizer.convergence();
        logEllipsisShut(where);
        logTab(where,"Start Refined Jogged LLG = " + dtos(DAGDB.WORK->generic_float2,2));
        logTab(where,"Final Refined Jogged LLG = " + dtos(llg,2));
        logTabArray(out::VERBOSE,DAGDB.WORK->logPose("Refined"));
      }

      logTableTop(where,"Jogged Position");
      logTabArray(where,DAGDB.WORK->logAnnotation(""));
      int w = itow(maxp);
      logTab(where,snprintftos(
          "%*s %-6s %6s %5s %11s",
          w,"#","Jogged","TFZ","FSS%","Distance(A)"));
      for (int p = 0; p < maxp; p++) //there will be only one
      {
        if (jogged_tfz[p] == 0)
        {
          logTab(where,snprintftos(
            "%*i %-6s %6s %5s %5s",
            w,p+1,
            "no",
            "n/a","n/a","n/a"));
        }
        else
        {
          logTab(where,snprintftos(
            "%*i %-6s %6.2f %5.1f %5.1f",
            w,p+1,
            "yes",
            jogged_tfz[p],
            jogged_fss[p],
            jogged_distance[p]
            ));
        }
      }
      logTableEnd(where);

      if (debugger)
      {
        out::stream where = out::LOGFILE;
        logBlank(where);
        Identifier modlid = DAGDB.WORK->TRACKER.ULTIMATE; //modify tag here
        modlid.initialize_tag("jog_out_n" + ntos(n-1,N));
        DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
        Coordinates coordinates;
        {{
        //use packing as a cheap way of creating monostructure
        Packing packing;
        packing.init_node(DAGDB.WORK);
        packing.structure_for_refinement(coordinates);
        coordinates.MODLID = modlid;
        coordinates.REFLID = modlid;
        coordinates.TIMESTAMP = TimeStamp();
        }}
        coordinates.SetFileSystem(DataBase(),dogtag.FileEntry(entry::COORDINATES_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Coordinates",coordinates);
        if (WriteFiles())
          coordinates.write_to_disk();
      }
      if (io_nref and j+1 == io_nref)
        break;
    }
    }}

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Jogged Summary");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int w = itow(DAGDB.NODES.size());
    logTab(where,snprintftos(
        "%*s %11s %11s %11s %11s %11s%c  %s",
        w,"#","LLG-input","LLG-initial","LLG-jogged","LLG-start","LLG-final",' ',"Moved?[+/-]"));
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      int p = 1;
      if (std::fabs(input_nodes[i-1].generic_float) < 10) p = 2;
      if ((!io_nref or i < io_nref) and node.is_all_full())
        logTab(where,snprintftos(
          "%*d %11.*f %11.*f %11.*f %11.*f %11.*f%c  %s",
          w,i,
          p,input_nodes[i-1].generic_float, //this is the one without the bfactors adjusted, interp::FOUR
          p,input_nodes[i-1].generic_float2, //this is the one without the bfactors adjusted, interp::EIGHT
          p,node.generic_float, //interp::EIGHT
          p,node.generic_float2, //start after adjusted bfactors (init_node), interp::EIGHT
          p,node.LLG,
          node.generic_bool ? ' ' : '<',
          jogged_info[i-1].c_str()
          ));
      else
        logTab(where,snprintftos(
          "%*d (No jogging refinement)",
          w,i
          ));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}
    //the start of the DAG are the input solutions unmodified
  }

} //phasertng
