import sys
from cctbx.array_family import flex
from iotbx import pdb
from libtbx.utils import Sorry
from iotbx import reflection_file_utils
import iotbx
from mmtbx import utils
from iotbx import pdb
from six.moves import cStringIO as StringIO
import mmtbx.model
import iotbx.phil
import mmtbx.f_model
from iotbx import extract_xtal_data
import argparse
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from phasertng.phil.mode_generator import isNone
from phasertng.phil.master_node_file import node_separator
import os, os.path

#these are internal parameters not interesting for the user
class runPOOL:

  def run_from_basetng_and_input(self,basetng):
 #{{{
    try:
      self.inner_pool(
        basetng=basetng,
        )
    except ValueError as e:
      raise e
    except Exception as e:
      raise ValueError(enum_err_code.developer,str(e))
 #}}}

  def inner_pool(self,
          basetng,
          ):
  #{{{
    DAGDB = basetng.DAGDATABASE
    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    root = os.path.join(basetng.DataBase(),subdir)
    wf = basetng.Write()
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(root ):
      os.makedirs(root)
    basetng.logUnderLine(enum_out_stream.summary,"Pool Changeling Information")
    io_rfz=basetng.significant_rfz
    basetng.logTab(enum_out_stream.summary,"Cutoff for significance: RFZ="+str(io_rfz))
    if not hasattr(basetng,'pdbtncs'):
      basetng.logTab(enum_out_stream.summary,"No results for changeling")
      basetng.logBlank(enum_out_stream.summary)
    else:
      underline = True
      basetng.logTableTop(enum_out_stream.logfile,"Changeling Results",underline)
      locount = 0
      hicount = 0
      w1 = 5
      wi = 10
      for v in basetng.pdbtncs:
        w1 = max(w1,len(v['pdbid']))
        wi = max(wi,len(v['identifier']))
      basetng.logTab(enum_out_stream.logfile,"There are " + str(len(basetng.pdbtncs)) + " results")
      for i,v in enumerate(basetng.pdbtncs):
        try:
          if i == 0:
            header = '{x:{w}} '.format(w=w1,x='pdbid')
            header += '{x:{w}} '.format(w=4,x='tncs')
            header += '{x:{w}} '.format(w=wi,x='identifier')
            for vv in v['dminrfz']:
              header += '{x:>{w}} '.format(w=6,x='dmin')
            header += '{x:>}'.format(x='title')
            basetng.logTab(enum_out_stream.logfile,str(header))
            header = '{x:{w}} '.format(w=w1,x='-----')
            header += '{x:{w}} '.format(w=4,x='----')
            header += '{x:{w}} '.format(w=wi,x='----------')
            for vv in v['dminrfz']:
              header += '{x:6.2f} '.format(x=float(vv['dmin']))
            header += '{x:>}'.format(x='-----')
            basetng.logTab(enum_out_stream.logfile,str(header))
          line  = '{x:{w}} '.format(w=w1,x=v['pdbid'])
          line += '{x:{w}} '.format(w=4,x=v['tncs'])
          line += '{x:{w}} '.format(w=wi,x=v['identifier'])
          for vv in v['dminrfz']:
            line += '{x:6.2f} '.format(x=float(vv['rfz']))
          line += '{x}'.format(x=v['title'].replace("_"," ").title())
          if float(v['dminrfz'][0]['rfz']) < float(io_rfz):
            locount = locount + 1
          else:
            hicount = hicount + 1
          if locount >= 3:
            locount = locount + 1 # for etc
            break
          if locount == 1 and hicount == 0:
            basetng.logTab(enum_out_stream.logfile,str("(none)"))
          if locount == 1:
            basetng.logTab(enum_out_stream.logfile,str("--------"))
          basetng.logTab(enum_out_stream.logfile,str(line))
        except:
          continue #something odd on line
      if locount >= 4:
        basetng.logTab(enum_out_stream.logfile,str("--------etc"))
      basetng.logTableEnd(enum_out_stream.logfile)
      basetng.logBlank(enum_out_stream.logfile)
      basetng.logUnderLine(enum_out_stream.summary,"Changeling Summary")
      if hicount == 0:
        basetng.logTab(enum_out_stream.summary,"No probe structures with RFZ over threshold for significance")
        basetng.logTab(enum_out_stream.summary,"Try other resolutions or full searches")
      else:
        basetng.logTab(enum_out_stream.summary,"Possible match to contaminant")
      basetng.logBlank(enum_out_stream.summary)
    if hasattr(basetng,"pdb_files"):
      basetng.logUnderLine(enum_out_stream.summary,"Fetched pdb")
      for i,pdbf in enumerate(basetng.pdb_files):
        basetng.logTab(enum_out_stream.summary,pdbf)
      basetng.logBlank(enum_out_stream.summary)

   #because we may have reloaded a mode and modified, shift the trackers here
    DAGDB.shift_tracker(str(basetng.hashing()+basetng.input.generic_string),"pool")
 #}}}
