#ifndef __phasertng_Token_value_enum__
#define __phasertng_Token_value_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace Token {
enum value {
             NAME,
             NUMBER,
             END,
             ENDLINE,
             ASSIGN,
             COMMENT,
             UNDEFINED
};
/*
  MakeEnum ( value,
             (NAME)
             (NUMBER)
             (END)
             (ENDLINE)
             (ASSIGN)
             (COMMENT)
             (UNDEFINED)
           );
*/
}}
#endif
