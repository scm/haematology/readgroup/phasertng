//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/MtzHandler.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/Assert.h>
#include <cstring>
#include <iostream>

namespace phasertng {

CMtz::MTZ*
MtzHandler::safe_mtz_get(std::string const& HKLIN, int read_refs)
{
  CMtz::MTZ *result;
    result = CMtz::MtzGet(HKLIN.c_str(), read_refs);
  if (result == 0) {
    throw Error(err::FILEOPEN,HKLIN);
  }
  return result;
}

CMtz::MTZCOL*
MtzHandler::getCol(std::string label)
{
  CMtz::MTZCOL* result = CMtz::MtzColLookup(mtz,label.c_str());
  if (result == 0) {
    throw Error(err::FATAL,"Cannot find MTZ column \"" + label +"\"");
  }
  return result;
}

  void MtzHandler::read(FileSystem HKLIN, int read_reflections)
  {
    SetFileSystem(HKLIN.database,{HKLIN.filepath,HKLIN.filename});
    try { //we must catch this again since the first throw does not throw
      mtz = safe_mtz_get(HKLIN.fstr(),read_reflections);
    } catch (...)
    { throw Error(err::FILEOPEN,HKLIN.fstr()); }
    try {
      mtzH  = MtzHandler::getCol("H");
      mtzK  = MtzHandler::getCol("K");
      mtzL  = MtzHandler::getCol("L");
    }
    catch (...)
    { throw Error(err::FATAL,"Cannot find MTZ column (H,K,L)"); }
    { //HISTORY
      //read the history and setup up parsing of cards to see what is there already
      //read what is in file and see if it agrees with values about to be written
      for (int i = 0; i < mtz->histlines; i++)
      {
        char buffer[MTZRECORDLENGTH];
        strncpy(buffer,mtz->hist + MTZRECORDLENGTH*i,MTZRECORDLENGTH);
        std::string line(buffer,MTZRECORDLENGTH);
        HISTORY.push_back(line);
      }
    } //HISTORY
    NREFL = mtz->nref;
    {
      cctbx::sgtbx::space_group space_group;
      //now do reverse lookup for cctbx
      for (int isym = 0; isym < mtz->mtzsymm.nsym; isym++)
      {
        scitbx::vec3<double> trasym;
        scitbx::mat3<double> rotsym;
        for (int i = 0; i < 3; i++)
        {
          trasym[i] = mtz->mtzsymm.sym[isym][i][3];
          for (int j = 0; j < 3; j++)
            rotsym(i,j) = mtz->mtzsymm.sym[isym][j][i];
        }
        cctbx::sgtbx::rt_mx nextSym(rotsym.transpose(),trasym);
        space_group.expand_smx(nextSym);
      }
      SG = SpaceGroup("Hall: " + space_group.type().hall_symbol());
    }
  }

  int
  MtzHandler::load_dataset(std::string CELLCOL)
  {
    //below are dependent on a reference dataset to load (unit cell and wavelength)
    CMtz::MTZXTAL *xtal = mtz->xtal[0]; //default
    if (CELLCOL.size())
    {
      try {
        CMtz::MTZCOL *cellcol = MtzHandler::getCol(CELLCOL);
        xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, cellcol));
      } catch (...)
      { throw Error(err::FATAL,"Cannot find MTZ column \"" + CELLCOL +"\""); }
    }
    PHASER_ASSERT(xtal != 0);
    double a     = static_cast<double>(xtal->cell[0]);
    double b     = static_cast<double>(xtal->cell[1]);
    double c     = static_cast<double>(xtal->cell[2]);
    double alpha = static_cast<double>(xtal->cell[3]);
    double beta  = static_cast<double>(xtal->cell[4]);
    double gamma = static_cast<double>(xtal->cell[5]);
    UC  = UnitCell(af::double6(a,b,c,alpha,beta,gamma));
    WAVELENGTH = xtal->set[0]->wavelength;
    isomorphous = true;
    for (int i = 0; i < mtz->nxtal; ++i)
    {
      if (mtz->xtal[i]->cell[0] != UC.A())     isomorphous = false;
      if (mtz->xtal[i]->cell[1] != UC.B())     isomorphous = false;
      if (mtz->xtal[i]->cell[2] != UC.C())     isomorphous = false;
      if (mtz->xtal[i]->cell[3] != UC.Alpha()) isomorphous = false;
      if (mtz->xtal[i]->cell[4] != UC.Beta())  isomorphous = false;
      if (mtz->xtal[i]->cell[5] != UC.Gamma()) isomorphous = false;
    }
    return xtal->nset;
  }

  void MtzHandler::open(
      FileSystem HKLOUT,
      SpaceGroup& SG_,
      UnitCell& UC_,
      double WAVELENGTH_,
      int NREFL_,
      af_string HISTORY_
      )
  {
    SetFileSystem(HKLOUT.database,{HKLOUT.filepath,HKLOUT.filename});
    create_directories_for_file();
    SG = (SG_),UC = (UC_),WAVELENGTH = (WAVELENGTH_),NREFL=(NREFL_),HISTORY=(HISTORY_);
    //int nxtal(0),nset[1]; nset[0] = 1;
    mtz = CMtz::MtzMalloc(0,0);

    // SPACEGROUP
    {
      mtz->mtzsymm.nsym = SG.group().order_z();
      mtz->mtzsymm.nsymp = SG.group().order_p();
      mtz->mtzsymm.spcgrp = SG.type().number();
      mtz->mtzsymm.symtyp = SG.spcgrpcentring();
      std::strcpy(mtz->mtzsymm.spcgrpname,SG.CCP4.c_str());
      PHASER_ASSERT(SG.group().order_z());
      for (int isym = 0; isym < SG.group().order_z(); isym++)
      {
        dmat33 rotsym(1,0,0,0,1,0,0,0,1);
        dvect3 trasym(0,0,0);
        for (int i = 0; i < 3; i++)
        {
          trasym[i] = static_cast<double>(SG.group()(isym).t()[i])/SG.group()(isym).t().den();
          for (int j = 0; j < 3; j++)
            rotsym(i,j) = static_cast<double>(SG.group()(isym).r()(i,j))/SG.group()(isym).r().den();
        }
        rotsym = rotsym.transpose();
        for (int i = 0; i < 3; i++)
        {
          mtz->mtzsymm.sym[isym][i][3] = trasym[i];
          for (int j = 0; j < 3; j++)
            mtz->mtzsymm.sym[isym][j][i] = rotsym(i,j);
        }
      }
      std::strcpy(mtz->mtzsymm.pgname,SG.pgname().c_str());
    }
   //Unitcell and wavelength added at the end when number of xtals known
   // xname, dname cannot be set because it breaks the HKL_base/Id=0 of xtal#1
   //Put then generates another crystal (this is a bug in the mtz library)
   //more general case of more than one xtal/set
   // CMtz::MTZXTAL* xtal = mtz->xtal[0];
    CMtz::MTZXTAL* xtal = MtzHandler::add_HKL_base(WAVELENGTH,UC);
    {
      mtz->nref = NREFL;
      mtzH  = CMtz::MtzAddColumn(mtz,xtal->set[0],"H","H");
      mtzK  = CMtz::MtzAddColumn(mtz,xtal->set[0],"K","H");
      mtzL  = CMtz::MtzAddColumn(mtz,xtal->set[0],"L","H");
    }
    //history written in reverse order
    if (HISTORY_.size() > 30)
    throw Error(err::RESULT,"Mtz History Full");
    for (int i = HISTORY_.size()-1 ; i >=0; i--)
    {
      if (HISTORY_.size() > MTZRECORDLENGTH)
      throw Error(err::RESULT,"Mtz History - line too long");
      char chistory[MTZRECORDLENGTH];
      strcpy(chistory, HISTORY_[i].c_str());
      CMtz::MtzAddHistory(mtz, &chistory, 1);
    }
  }

  CMtz::MTZCOL*
  MtzHandler::addCol(std::string Name,char T,int s)
  {
    int x(0); //only ever one xtal, but sometimes different datasets
    std::string Type(1,T);
    CMtz::MTZCOL *mtzCol = CMtz::MtzAddColumn(mtz,mtz->xtal[x]->set[s],Name.c_str(),Type.c_str());
    PHASER_ASSERT(mtzCol != 0);
    for (unsigned mtzr = 0; mtzr < mtz->nref; mtzr++)
      mtzCol->ref[mtzr] = CCP4::ccp4_nan().f;
    return mtzCol;
  }

  CMtz::MTZXTAL*
  MtzHandler::add_HKL_base(double wavelength,UnitCell UC)
  {
/*
From CCP4 5.0, there should always be a base dataset, called HKL_base and with dataset ID of zero. This dataset will be added automatically by the library, if it doesn't already exist. The columns H, K and L are forced to belong to the Base dataset. Other columns will be assigned to the Base dataset if they are not assigned explicitly to another dataset.
*/
    const std::string Name = "HKL_base";
    float cell[6];
    cell[0] = UC.A();
    cell[1] = UC.B();
    cell[2] = UC.C();
    cell[3] = UC.Alpha();
    cell[4] = UC.Beta();
    cell[5] = UC.Gamma();
    CMtz::MTZXTAL* xtal = CMtz::MtzAddXtal(mtz, Name.c_str(), Name.c_str(), cell);
    std::strcpy(xtal->pname,Name.c_str());
    std::strcpy(xtal->xname,Name.c_str());
    CMtz::MtzAddDataset(mtz, xtal, Name.c_str(), wavelength);
    return xtal;
  }

  void
  MtzHandler::addSet(std::string Name)
  {
    CMtz::MTZXTAL *xtal = mtz->xtal[0]; //default
    CMtz::MtzAddDataset(mtz, xtal, Name.c_str(), WAVELENGTH);
  }

  void MtzHandler::Put(std::string TITLE)
  {
    std::strcpy(mtz->title,TITLE.c_str());
    for (int x = 0; x < mtz->nxtal; x++)  // not much point in optimising this
    {
      for (int a = 0; a < 6; a++)
        mtz->xtal[x]->cell[a] = UC.cctbxUC.parameters()[a];
/* //NO! don't rename the HKL_base or else it duplicates crystal
      std::strcpy(mtz->xtal[x]->xname,"crystal");
      std::strcpy(mtz->xtal[x]->pname,"project");
      for (int s = 0; s < mtz->xtal[x]->nset; s++)
      { //NO! don't rename the HKL_base or else it duplicates crystal
        std::string dname = "dataset" + std::to_string(s+1);
        std::strcpy(mtz->xtal[x]->set[s]->dname,dname.c_str());
        mtz->xtal[x]->set[s]->wavelength = WAVELENGTH;
      }
*/
    }
    CMtz::MtzPut(mtz, const_cast<char*>(FileSystem::fstr().c_str()));
    //destructor ;CMtz::MtzFree(mtz);
  }

  int MtzHandler::resetRefl()
  {
    int roffset1(0);
    int roffset2(0);
    //reset position in file
    int respos = roffset2 * CMtz::MtzNumSourceCol(mtz) + SIZE1 + roffset1;
    CCP4::ccp4_file_seek(mtz->filein,respos,SEEK_SET);
    return respos;
  }

  void
  MtzHandler::setupReadRefl(sv_string labels)
  {
    int ncols = labels.size();
    std::vector<float> adata_(ncols);
    float* adata = &*adata_.begin();
    sv_int logmss_(ncols);
    int* logmss = &*logmss_.begin();
    std::vector<CMtz::MTZCOL*> lookup_(ncols);
    CMtz::MTZCOL** lookup = &*lookup_.begin();
    int n(0);
    for (int i = 0; i < labels.size(); i++)
    {
      try {
      lookup[n] = MtzHandler::getCol(labels[i]);
      } catch(...)
      { throw Error(err::FATAL,"Cannot find MTZ column \"" + labels[i] +"\""); }
      n++;
    }
    //(float adata[], int logmss[], CMtz::MTZCOL *lookup[], int ncols, int iref)
  }

  int MtzHandler::readRefl(float adata[], int logmss[], CMtz::MTZCOL *lookup[], int ncols, int iref)
  {
    int icol(0);
    unsigned int colin(0);
    float refldata[MCOLUMNS];
    union float_uint_uchar uf;

    /* If we are past the last reflection, indicate this with return value. */
    if (iref > mtz->nref_filein || iref <= 0)
      return 1;

    /* If reflections not in memory, read next record from file. */
    if (!mtz->refs_in_memory) {
      if (CMtz::MtzRrefl( mtz->filein, mtz->ncol_read, refldata) == EOF) return 1;
    }

    if (strncmp (mtz->mnf.amnf,"NAN",3) == 0) {
      uf = CCP4::ccp4_nan();
    } else {
      uf.f = mtz->mnf.fmnf;
    }

    /* loop over columns requested in lookup array. */
    for (icol=0; icol < ncols; icol++) {
      logmss[icol] = 1;
      if (lookup[icol]) {
        if (mtz->refs_in_memory) {
          adata[icol] = lookup[icol]->ref[iref-1];
          logmss[icol] = CMtz::ccp4_ismnf(mtz, adata[icol]);
        } else {
          colin = lookup[icol]->source;
          if (colin) {
            adata[icol] = refldata[colin - 1];
            logmss[icol] = CMtz::ccp4_ismnf(mtz, adata[icol]);
          } else {
            adata[icol] = uf.f;
            logmss[icol] = 1;
          }
        }
      }
    }
    return 0;
  }

  af_string
  MtzHandler::XtalSetColInfo()
  {
    af_string output;
    for (int i = 0; i < mtz->nxtal; ++i)
    {
      for (int j = 0; j < mtz->xtal[i]->nset; ++j)
      {
        CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
        for (int k = 0; k < xset->ncol; ++k)
        {
          char buf[80];
          snprintf(buf,80,
              "xtal/set/col: %2d/%2d/%2d ColType: %c Label: %-20s",
              i,j,k,xset->col[k]->type[0],xset->col[k]->label);
          output.push_back(std::string(buf));
        }
      }
    }
    return output;
  }

  void
  MtzHandler::find_nonanomalous_data(
    std::string fnat,std::string sigfnat,std::string inat,std::string siginat)
  {
    MapCount = 0;
    Fcount = 0;
    Icount = 0;
    Ilabel = Flabel = SIGIlabel = SIGFlabel = "";
    for (int i = 0; i < mtz->nxtal; ++i)
    for (int j = 0; j < mtz->xtal[i]->nset; ++j)
    for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
    { //search for all the F (unphased) and I columns
      CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
      std::string label(xset->col[k]->label);
      if (xset->col[k]->type[0] == 'F') //amplitudes
      {
        bool isF(true);
        if (label == "FOM") //bug in phenix.fetch_pdb --mtz conversion
          isF=false;// this coltype 'F' is a FOM
        if (k < xset->ncol-1 and xset->col[k+1]->type[0] == 'P')
        {
          MapCount++; //extract the F from the map later
          isF=false;// this 'F' is an Fcalc
        }
        if (isF)
        {
          //if we have found the f or if it is the first f in the file
          //if first but not only (check for only below) it will be rejected later
          if (Fcount == 0 or fnat == label)
          {
            Flabel = label;
            //see if the next column is a sigma and store if it is regardless of match to sigf
            //will be overwritten below if user has specified a different SIGF
            //here only if it is the first in the file and/or matches second and subsequent
            if (k < xset->ncol-1 and xset->col[k+1]->type[0] == 'Q')
              SIGFlabel = xset->col[k+1]->label;
          }
          Fcount++;
        }
      }
      if (xset->col[k]->type[0] == 'J') //intensity
      {
        bool all_zero = true;
        if (Icount == 0 or inat == label)
        {
          Ilabel = label;
          //see if the next column is a sigma and store if it is regardless of match to sigi
          //will be overwritten below if user has specified SIGI
          //here only if it is the first in the file and/or matches second and subsequent
          if (k < xset->ncol-1 and xset->col[k+1]->type[0] == 'Q')
          {
            SIGIlabel = xset->col[k+1]->label;
            for (int mtzr = 0; mtzr < mtz->nref; mtzr++)
              if (xset->col[k+1]->ref[mtzr] > 0)
                all_zero = false;
          }
        }
        if (all_zero)
          Ilabel = SIGIlabel = "";
        else
          Icount++;
      }
      //if for some reason the sigf/sigi is not after the f/i then it can be located separately
      if (xset->col[k]->type[0] == 'Q')
      { //locate these if they are specified (or default) in the input
        if (sigfnat == label)
          SIGFlabel = label;
        if (siginat == label) //do the intensity while we are here, Q is either F or I
          SIGIlabel = label;
      }
    }
  }

  void
  MtzHandler::find_map(std::string mapf)
  {
    FMAPcount = 0;
    Flabel = Plabel = Wlabel = "";
    for (int i = 0; i < mtz->nxtal; ++i)
    for (int j = 0; j < mtz->xtal[i]->nset; ++j)
    for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
    { //search for all the F (unphased) and I columns
      CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
      std::string label(xset->col[k]->label);
      if (xset->col[k]->type[0] == 'F' and //amplitudes
          label != "FOM" and //bug in phenix.fetch_pdb --mtz conversion
          k+1 < xset->ncol and xset->col[k+1]->type[0] == 'P')
      {
        if (FMAPcount == 0 or mapf == label)
        {
          Flabel = label;
          Plabel = xset->col[k+1]->label;
          //read mapdobs if it is there
          if (k+2 < xset->ncol and xset->col[k+2]->type[0] == 'W')
          {
            Wlabel = xset->col[k+2]->label;
          }
        }
        FMAPcount++;
      }
    }
  }

  void
  MtzHandler::find_anomalous_data(
    std::string fpos,std::string sigfpos,std::string ipos,std::string sigipos,
    std::string fneg,std::string sigfneg,std::string ineg,std::string sigineg)
  {
    Fcount = Icount = 0 ;
    ISIGIlabel = { "","","","" };
    FSIGFlabel = { "","","","" };
  //first find the default column if present
  //and count the number of cols for F and I
    for (int i = 0; i < mtz->nxtal; ++i)
    for (int j = 0; j < mtz->xtal[i]->nset; ++j)
    for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
    {
      //assume I+ SIGI+ I- SIGI-
      CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
      if (xset->col[k]->type[0] == 'K') //intensity
      {
        if (Icount == 0)
        { //I with sigmas
          ISIGIlabel[0] = xset->col[k]->label;
          if ((k+1 < xset->ncol and xset->col[k+1]->type[0] == 'M') and
              (k+2 < xset->ncol and xset->col[k+2]->type[0] == 'K') and
              (k+3 < xset->ncol and xset->col[k+3]->type[0] == 'M'))
          {
            ISIGIlabel[1] = xset->col[k+1]->label;
            ISIGIlabel[2] = xset->col[k+2]->label;
            ISIGIlabel[3] = xset->col[k+3]->label;
            k+=3; //skip over these 4
            Icount++; //one complete set found
          }
          if (k+1 < xset->ncol and xset->col[k+1]->type[0] == 'K')
          { //I without sigmas
            ISIGIlabel[2] = xset->col[k+1]->label;
            k+=1; //skip over these 2
            Icount++; //one complete set found
          }
        }
      }
      else if (xset->col[k]->type[0] == 'G') //amplitudes
      {
        if (Fcount == 0)
        {
          FSIGFlabel[0] = xset->col[k]->label;
          if ((k+1 < xset->ncol and xset->col[k+1]->type[0] == 'L') and
              (k+2 < xset->ncol and xset->col[k+2]->type[0] == 'G') and
              (k+3 < xset->ncol and xset->col[k+3]->type[0] == 'L'))
          { //fs must have sigmas
            FSIGFlabel[1] = xset->col[k+1]->label;
            FSIGFlabel[2] = xset->col[k+2]->label;
            FSIGFlabel[3] = xset->col[k+3]->label;
            k+=3; //skip over these 4
            Fcount++; //one complete set found
          }
          if (k+1 < xset->ncol and xset->col[k+1]->type[0] == 'G')
          { //I without sigmas
            FSIGFlabel[2] = xset->col[k+1]->label;
            k+=1; //skip over these 2
            Fcount++; //one complete set found
          }
        }
      }
      //intensities, direct lookup
      char type = xset->col[k]->type[0];
      std::string label = xset->col[k]->label;
      if (type == 'K' and ipos == label)
        ISIGIlabel[0] = label;
      if (type == 'M' and sigipos == label)
        ISIGIlabel[1] = label;
      if (type == 'K' and ineg == label)
        ISIGIlabel[2] = label;
      if (type == 'M' and sigineg == label)
        ISIGIlabel[3] = label;
      //amplitudes, direct lookup
      if (type == 'G' and fpos == label)
        FSIGFlabel[0] = label;
      if (type == 'L' and sigfpos == label)
        FSIGFlabel[1] = label;
      if (type == 'G' and fneg == label)
        FSIGFlabel[2] = label;
      if (type == 'L' and sigfneg == label)
        FSIGFlabel[3] = label;
    }
  }

  void
  MtzHandler::find_freer(std::string freer)
  {
    Fcount = 0;
    for (int i = 0; i < mtz->nxtal; ++i)
    for (int j = 0; j < mtz->xtal[i]->nset; ++j)
    for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
    { //search for all the F (unphased) and I columns
      CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
      if (xset->col[k]->type[0] == 'I') //integer values
      {
        std::string label(xset->col[k]->label); //string conversion
        if (label == freer)
        {
          Fcount++;
          FREElabel = label;
        }
        else if (label.size())
        {
          if (label.size() >= 6 and //R-free-flags
                   std::toupper(label[2]) == 'F' and
                   std::toupper(label[3]) == 'R' and
                   std::toupper(label[4]) == 'E' and
                   std::toupper(label[5]) == 'E')
          {
            Fcount++;
            if (Fcount == 1) FREElabel = label; //use the first free dataset
          }
          if (label.size() >= 4 and //FreeR_flag
                   std::toupper(label[0]) == 'F' and
                   std::toupper(label[1]) == 'R' and
                   std::toupper(label[2]) == 'E' and
                   std::toupper(label[3]) == 'E')
          {
            Fcount++;
            if (Fcount == 1) FREElabel = label; //use the first free dataset
          }
        }
      }
    }
  }

  void
  MtzHandler::find_fmap_phmap(std::string fmap,std::string phmap,bool no_phases)
  {
    MapCount = 0;
    Fcount = 0;
    Flabel = Plabel = "";
    for (int i = 0; i < mtz->nxtal; ++i)
    for (int j = 0; j < mtz->xtal[i]->nset; ++j)
    for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
    { //search for all the F (unphased) and I columns
      CMtz::MTZSET* xset = mtz->xtal[i]->set[j];
      if (xset->col[k]->type[0] == 'F') //amplitudes
      {
        std::string label(xset->col[k]->label); //string conversion
        if (label == "FOM") //bug in phenix.fetch_pdb --mtz conversion
          continue;// this coltype 'F' is a FOM
        if (k < xset->ncol-1 and xset->col[k+1]->type[0] == 'P')
        {
          Fcount++;
          Flabel = xset->col[k]->label;
          if (k < xset->ncol-1 and xset->col[k+1]->type[0] == 'P')
          {
            Plabel = xset->col[k+1]->label;
            MapCount++;
          }
          if (no_phases)
            Plabel = "";
        }
      }
    }
  }

}//phasertng
