//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineTest.h>

namespace phasertng {

  void Phasertng::runTEST()
  {
    logSectionHeader(out::LOGFILE,"Minimizer test, Twisted gaussian");
    sv_double start = {5., 7.};
    RefineTest reftest(start);  // make the refine object
    sv_string MACRO1 = {"First", "Second"};
    std::vector<sv_string> PROTOCOL = {MACRO1};
    std::vector<int> NCYC = {50};
    std::string MINIMIZER = "bfgs";
    bool STUDY_PARAMS = true;
    dtmin::Minimizer minimizer(this);  // make the minimizer object
    minimizer.run(reftest,PROTOCOL,NCYC,MINIMIZER,STUDY_PARAMS);  // run the minimization
  }

} //phasertng
