//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/mtzcol.h>

namespace phasertng {
namespace type {

  mtzcol::mtzcol(
      const std::string parameter,
      const std::string coltype_,
      const std::string internal_, // name for output including special chars e.g. F(+)
      const std::string labincol_//handle for labin::col e.g. FPOS
  ) :
     optional(parameter),
     internal(internal_),
     labincol(labincol_)
  {
    if (coltype_.size()) coltype = coltype_[0];
  }

  mtzcol::mtzcol(
 //     const std::string parameter,
      const char coltype_,
      const std::string internal_, // name for output including special chars e.g. F(+)
      const labin::col labincol_ // handle for labin::col e.g. FPOS
  ) :
  //   optional(parameter),
     coltype(coltype_),
     internal(internal_),
     labincol(labin::col2String(labincol_))
  {
    user = true;
  }

  void
  mtzcol::set_value(std::string other)
  {
    if (other == None())
    {
      user = false;
      external = "";
      return; //leave as the default
    }
    user = true;
    if (!other.size()) return;
//stored unquoted - labin cannot start with quote
    if (other[0] != '"')
    {
      external = other;
    }
    else
    {
      std::string unquoted = other;
      while (unquoted.size() && unquoted[0] == '"')
      {
        std::string tmp = "";
        for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
          tmp += std::string(1,unquoted[i]);
        unquoted = tmp;
      }
    //  phaser_assert(!unquoted.size() || unquoted[0] != '"');
      external = unquoted;
    }
  }

  void
  mtzcol::set_value(const sv_string& vcards,const std::string& other)
  {
    //paranoia_check(vcards); stored as map, different check (would be) required
    set_value(other);
  }

  //virtual
  bool
  mtzcol::is_default() const
  { return !user or internal == external; }

  bool
  mtzcol::is_not_set() const
  { return !user; }

  void
  mtzcol::set_default()
  { user = false; }

  std::string
  mtzcol::to_python()
  { return unparse_parameter(false); }

  void
  mtzcol::from_python(std::string str)
  { set_value(str); }

  std::string
  mtzcol::name() const //alias
  { return external.size() ? external : internal; }

  type::mtzcol
  mtzcol::value_or_default() const
  { return *this; }

  std::string
  mtzcol::type_str() const
  { return std::string(1,coltype); }

  char
  mtzcol::type() const
  { return coltype; }

  std::string
  mtzcol::labin_col() const
  { return labincol; }

  labin::col
  mtzcol::enumeration() const
  { return labin::col2Enum(labincol); }

  std::string
  mtzcol::unparse_parameter(bool phil) const
  {
    std::string vv = external.size() ? external : None();
    std::string str = "\"" + vv + "\"";
    return vv; //no quotes
  }

  std::string
  mtzcol::show_default(bool phil) const
  {
    std::string vv = internal.size() ? internal : None();
    std::string str = "\"" + vv + "\"";
    return vv; //no quotes
  }

}}
