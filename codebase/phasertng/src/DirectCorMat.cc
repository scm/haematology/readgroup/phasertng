//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Assert.h>
#include <phasertng/src/DirectCorMat.h>
#include <phasertng/cctbx_project/cctbx/tntbx/include/tnt_fmat.h>
#include <cctbx/miller/index_generator.h>
#include <phasertng/io/entry/Models.h>
#include <phasertng/data/Scatterers.h>
#include <cctbx/xray/scatterer.h>

namespace phasertng {

  void
  DirectCorMat::setup(
      Models& MODELS,
      Scatterers& SCATTERERS,
      double resolution_d_min
    )
  {
    cctbx::eltbx::xray_scattering::gaussian ff_C = cctbx::eltbx::xray_scattering::wk1995("C").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_N = cctbx::eltbx::xray_scattering::wk1995("N").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_O = cctbx::eltbx::xray_scattering::wk1995("O").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_S = cctbx::eltbx::xray_scattering::wk1995("S").fetch();
    cctbx::eltbx::xray_scattering::gaussian ff_H = cctbx::eltbx::xray_scattering::wk1995("H").fetch();
    cctbx::sgtbx::space_group_type sg_type("P 1");
    // Set cell with Shannon double sampling to obtain maximum number of relatively uncorrelated reflections
    // Increase cell if necessary to get reasonable minimal dimensions and a significant number in total
    int nsigref(2000); // Minimum total number of reflections and desired number in shell for precise correlations
    double mincell = resolution_d_min*fn::pow(nsigref/(scitbx::constants::pi*4./3.),1./3.);
    mincell = std::max(cell_padding,mincell); // Allow for small fragments
    MODELS.set_principal_statistics();
    for (int i = 0; i < 3; i++)
    {
      CELL[i] = std::max(2.*MODELS.statistics.EXTENT[i],mincell);
    }
    cctbx::uctbx::unit_cell unit_cell(af::double6(CELL[0],CELL[1],CELL[2],90.0,90.0,90.0));

    int nmodels(MODELS.PDB.size());
    CorMat.resize(scitbx::af::flex_grid<>(nmodels,nmodels));
    // Carry out correlation calculations at specified resolution limit, but back off if the contribution
    // from the worst pair of model will not be significant at that resolution.
    // Aim for 500-2000 reflections for statistics
    double targetOffDiag,minOffDiag;
    do
    {
      minOffDiag = 1.;
      int ntot = scitbx::constants::two_pi*unit_cell.volume()/fn::pow3(resolution_d_min)/3.; // Approx unique reflections
      int ngoal = std::min(nsigref,std::max(nsigref,ntot)/4);
      double resolution_d_max = std::pow(double(ntot)/(ntot-ngoal),1./3.)*resolution_d_min;
      double ssqr_min(1.0/fn::pow2(resolution_d_max));

      // Generate reflection list
      midSsqr = 0.;
      bool anomalous_flag(false);
      cctbx::miller::index_generator generator(unit_cell,sg_type,anomalous_flag,resolution_d_min);
      scitbx::af::shared<millnx>  hires_miller;
      millnx hkl = generator.next();
      while (hkl != millnx(0,0,0)) //flags end
      {
        double ssqr(unit_cell.d_star_sq(hkl));
        if (ssqr > ssqr_min)
        {
          hires_miller.push_back(hkl);
          midSsqr += ssqr;
        }
        hkl = generator.next();
      };
      PHASER_ASSERT(hires_miller.size());
      midSsqr /= hires_miller.size();

      // Compute structure factor sets for high resolution shell, using direct summation
      std::vector<std::vector<cmplex > > hires_Fcalc;
      hires_Fcalc.resize(nmodels); //proxy for number of coordinate sets
      for (int m = 0; m < hires_Fcalc.size(); m++)
        hires_Fcalc[m].resize(hires_miller.size());

      cctbx::eltbx::xray_scattering::wk1995(element);
      for (int m = 0; m < MODELS.PDB.size(); m++)
      { //loop over MODELS in pdb files
        for (int r = 0; r < hires_miller.size(); r++)
        {
          double rssqr =  unit_cell.d_star_sq(hires_miller[r]);
          hires_Fcalc[m][r] = 0.;
          const int natoms = MODELS.PDB[m].size();
          for (int a = 0; a < natoms; a++)
          {
            const PdbRecord& A = MODELS.PDB[m][a];
            cctbx::xray::scatterer<> scatterer(
              "another atom", // label
              unit_cell.fractionalization_matrix()*(A.X), // site
              cctbx::adptbx::b_as_u(A.B), // u_iso
              A.O, // occupancy
              A.get_element(),
              0, // fp
              0); // fdp
            double fo_plus_fp = SCATTERERS.fo(A.get_element(),rssqr);
            double debye_waller_u_iso = cctbx::adptbx::u_as_b(scatterer.u_iso) * 0.25;
            double isoB = std::exp(-rssqr*debye_waller_u_iso);
            double scat(fo_plus_fp*isoB);
            double theta = hires_miller[r][0]*scatterer.site[0] + hires_miller[r][1]*scatterer.site[1] + hires_miller[r][2]*scatterer.site[2];
            theta *= scitbx::constants::two_pi;
            double costheta = std::cos(theta);
            double sintheta = std::sin(theta);
            hires_Fcalc[m][r] += scatterer.occupancy*scat*cmplex(costheta,sintheta);
          }
        }
      }

      // Compute correlation matrix
      sv_double sqrtSigmaP(nmodels,0.); // Normalisation factors
      for (int m = 0; m < nmodels; m++)
      {
        for (int r = 0; r < hires_miller.size(); r++)
          sqrtSigmaP[m] += std::norm(hires_Fcalc[m][r]);
        sqrtSigmaP[m] = std::sqrt(sqrtSigmaP[m]);
      }
      for (int m = 0; m < nmodels; m++)
      {
        for (int n = m; n < nmodels; n++)
        {
          if (n == m) CorMat(m,n) = 1.;
          else
          {
            double sumF1F2(0.);
            for (int r = 0; r < hires_miller.size(); r++)
              sumF1F2 += std::real((hires_Fcalc[m][r])*std::conj(hires_Fcalc[n][r]));
            //this doesn't need /hires_miller.size() becuase sigmaP is over same sum
            //i.e. both numerator and denominator are not normalized
            CorMat(m,n) = CorMat(n,m) = sumF1F2/(sqrtSigmaP[m]*sqrtSigmaP[n]);
            minOffDiag = std::min(minOffDiag,CorMat(m,n));
          }
        }
      }
      // Target for lowest off-diagonal CC allowed is relative error of CC/sigma(CC) >= targetZ_CC
      // sigma for complex correlation: (1-CC^2)/sqrt(2*nref)
      // Factor of 2 in sqrt (cf sigma for linear CC) comes from complex number having two parts
      // (twice as much information)
      // Solve for CC that gives desired relative error for the current value of nref

      double nref = double(hires_miller.size());
      double tzsqr(fn::pow2(targetZ_CC));
      targetOffDiag = std::sqrt(nref + tzsqr - std::sqrt(2*nref*tzsqr + fn::pow2(nref))) / targetZ_CC;
      resolution_d_min *= 1.2;
    }
    while ((minOffDiag < targetOffDiag) &&
           (resolution_d_min <= lowest_reso));
  }

}//phasertng
