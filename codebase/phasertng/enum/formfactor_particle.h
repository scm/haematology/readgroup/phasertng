#ifndef __phasertng_formfactor_particle_enum__
#define __phasertng_formfactor_particle_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace formfactor {
  MakeEnum ( particle,
             (XRAY)
             (ELECTRON)
             (NEUTRON)
           );
}}
#endif
