//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_BINS_CLASS__
#define __PHASER_DATA_BINS_CLASS__

namespace phaser {

struct data_bins
{
    int MAXBINS,MINBINS,WIDTH;

    data_bins()
    {
      MAXBINS = MINBINS = WIDTH = 0;
    }

    bool not_setup()
    { return !MAXBINS && !MINBINS && !WIDTH; }

    void set_default_data()
    {
      MAXBINS = 50;
      MINBINS = 6;
      WIDTH =   500;
    }
};

}

#endif
