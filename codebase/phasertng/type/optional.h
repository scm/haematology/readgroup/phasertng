//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_optional_class__
#define __phasertng_type_optional_class__
#include <phasertng/main/constants.h>
#include <phasertng/enum/show_type.h>
#include <vector>
#include <string>
#include <iso646.h> //Windows, for "and" and "or" syntax


namespace phasertng {
namespace type {

typedef std::vector<std::string> sv_string;

class optional
{
  protected:
    bool value_is_set_on_input = false; //for text from phaser_assert

  protected:
    sv_string  keywords = sv_string(0);
    bool user = false;
    const std::string None() const { return "None"; }

  public:
  optional() {}
  optional(std::string parameter);
  ~optional() {} //required for link

  std::string parameter();

  virtual std::string unparse_parameter(bool phil) const = 0;
  virtual std::string show_default(bool phil) const = 0;
  virtual bool        is_default() const { return false; }
  virtual void        set_default() = 0;
  virtual std::string to_python() = 0;
  virtual void        from_python(std::string) = 0;

  const bool& is_user_value() const;
  std::string unparse(show::type show_type,bool phil) const;
  std::string scoped(const sv_string& vec) const;
  std::string unscoped() const;
  sv_string to_words(const std::string str) const;
  void paranoia_check(const sv_string& vcards) const;
};

}}
#endif
