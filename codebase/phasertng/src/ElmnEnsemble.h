//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ElmnEnsemble_class__
#define __phasertng_ElmnEnsemble_class__
#include <phasertng/src/Elmn.h>

namespace phasertng {

  class Ensemble; //forward declaration

  class ElmnEnsemble : public Elmn
  {
    public:
      ElmnEnsemble(int LMAX_) : Elmn(LMAX_) { resize_elmn(1); }

    public:
      af_string
      ELMNxR2(
          const Ensemble*,
          double,
          double,
          double
        );
  };

}//phasertng
#endif
