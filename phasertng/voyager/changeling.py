import importlib
import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError,Identifier
from phasertng import enum_out_stream
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.run.ModeRunner import *
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.scripts.contaminants.data import DATA
import argparse

def changeling(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_changeling)

def inner_changeling(basetng,user_phil_str=""):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  debug = False
  try:
    #this logging is done before the true output parameters are set
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="changeling")
    initparams = initparams +  "\n" +  build_auto_str("changeling")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    assert(dbdir is not None)
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default
    DAGDB = basetng.DAGDATABASE

    #phil_to_cards selects out only the phasertng cards with the program name
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    io_rfz = float(working_params.changeling.control.significant_rfz)
    basetng.significant_rfz = io_rfz
    io_use_tfz = True

    wforig = basetng.WriteData()
    run_no_write_files = True # because this is a lot faster
    # you can change run_no_write_files but there will be shedloads of output that you don't want
    # and it will probably fill up your disk
    if run_no_write_files:
      #permanently turn off all the file writing, and only output the non-database file
      #this is a hack so if the user puts in a request for the output it is used even in dryrun mode
      import re
      level = re.search(r'phasertng\s+suite\s+level\s+(\w+)', user_cards)
      if level:
        dryrun_cards = silent_cards(level.group(1))
      else:
        dryrun_cards = silent_cards("summary") #override default by default
    else: #generate the database directory and clear the xml file
      dryrun_cards = ""
      dbdir = working_params.phasertng.suite.database
      assert(dbdir is not None)
      if not os.path.isdir(dbdir):
        os.makedirs(dbdir)
      xmlout = working_params.phasertng.graph_report.filestem + ".xml"
      xmlout = os.path.join(dbdir,xmlout)
      xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
      #clear the tracker before we start
      if os.path.exists(xmlout):
        os.remove(xmlout)

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,"",ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    loop_auto = str("Changeling")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    ModeRunner(basetng=basetng,mode_list=['chk'])

    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,"",ignore_unknown_keys)
    ModeRunner(basetng=basetng,mode_list=['walk'])

    #first choice
    hklin = working_params.changeling.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) > 0:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    resolutions = working_params.changeling.control.resolutions
    reso = max(working_params.phasertng.reflections.resolution,min(resolutions))
    basetng.input.set_as_str(".phasertng.reflections.resolution.",str(reso))
    #the tncso is not used because we are not writing files and therefore cannot restore at tncso
    tncso = ModeRunner(basetng=basetng,mode_list=['imtz','data','sga','sgx','aniso','tncso']).solution()
    basetng.input.set_as_str(".phasertng.biological_unit.solvent.","5")
    hires = float(basetng.input.get_as_str(".phasertng.notifications.hires."))
    from math import floor
    hires = float(f"{floor(hires * 100 + 0.5) / 100:.2f}")

    def append_suffix_to_duplicates(numbers):
      # Takes an array of integers and returns an array of strings.
      # Adds suffixes (a, b, ...) to all duplicates, and appends 'a' to the first occurrence of any duplicate.
        counts = {}
        nmolvec = []
        for number in numbers:
            if number in counts:
                # Increment the count and append the suffix for duplicates
                counts[number] += 1
                nmolvec.append(f"{number}-{chr(96 + counts[number])}")  # Convert count to a letter
            else:
                # First occurrence: add 'a' for duplicates, or the plain number for unique
                counts[number] = 1
                if numbers.count(number) > 1:
                    nmolvec.append(f"{number}-a")
                else:
                    nmolvec.append(str(number))
        return nmolvec
        # Example usage [1, 2, 1, 3, 2, 1, 4] = [1a, 2a, 1b, 3, 2b, 1c, 4]

    loop_tncs = "Tncs Analysis"
    assert(basetng.input.get_as_str(".phasertng.tncs_analysis.number.") != "None")
    tncs_selections = []
    tncs_integers = [] #these mean that multiple copies of same order with diff vectors are not flagged the same
    for i in range(int(basetng.input.get_as_str(".phasertng.tncs_analysis.number."))):
          tncs_integers.append(basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.order.",i))
    tncs_integers = append_suffix_to_duplicates(tncs_integers)
    for i in range(int(basetng.input.get_as_str(".phasertng.tncs_analysis.number."))):
          order = basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.order.",i)
          vector = basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.vector.",i).strip()
          tncs_selection = {
            'order': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.order.",i),
            'vector': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.vector.",i),
            'parity': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.parity.",i),
            'unique': basetng.input.get_as_str_i(".phasertng.tncs_analysis.result.unique.",i),
            'order-vector-1a': tncs_integers[i],
            'info': str(order) + ":(" + str(vector).replace(' ',',') +")"}
          tncs_selections.append(tncs_selection)
    tncs_integers = append_suffix_to_duplicates(tncs_integers)
    basetng.logLoopOpen(enum_out_stream.process,loop_tncs,len(tncs_selections),
          '\n'.join(n for n in [d['info'] for d in tncs_selections]))
    #make all the feff files required
    for t,tncs_selection in enumerate(tncs_selections):
   #{{{
        #should brance from tncso, but we are not storing files so this is not possible
        basetng.logLoopNext(enum_out_stream.process,loop_tncs,tncs_selection['info'])
        basetng.load_moderunner(tncso,user_cards,"")
        tncs_order = int(tncs_selection['order']) #cast to int
        basetng.input.set_as_str(".phasertng.tncs.order.",tncs_selection['order'])
        basetng.input.set_as_str(".phasertng.tncs.vector.",tncs_selection['vector'])
        basetng.input.set_as_str(".phasertng.tncs.parity.",tncs_selection['parity'])
        basetng.input.set_as_str(".phasertng.tncs.unique.",tncs_selection['unique'])
        #add entry
        tncs_selection["feff"] = ModeRunner(basetng=basetng,mode_list=['tncs','cca','ccs','feff']).solution()
        tncs_selection["node"] = DAGDB.NODES.LIST[0]
        #we can't run the srf because it is running in silent mode and the plotting needs files
        #use xtricorder for srf
   #}}}
    basetng.logLoopShut(enum_out_stream.process,loop_tncs)
    basetng.load_moderunner(tncso,user_cards,"")

    #now set the choice of silent running from now on
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keys) #need this
    basetng.set_user_input_for_suite(basetng.input) #we need this

    #generate the list of contaminants
    if working_params.changeling.probe.source == "contaminants":
      contaminants = DATA
    elif working_params.changeling.probe.source == "pdbid":
      contaminants = working_params.changeling.probe.pdbid
    elif working_params.changeling.probe.source == "directory":
      n = basetng.input.size(".phasertng.file_discovery.validated.pdb_files.")
      if int(n) == 0:
        raise ValueError(enum_err_code.python,"No pdb files in file discovery")
      contaminants = []
      for i in range(n):
        pdbin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",i)
        contaminants.append(pdbin)

    #this loop so that we can collapse all the contaminants in one loop
    #put this in an info so that it doesn't end up in the title
    from functools import reduce
    basetng.logLoopInfo(enum_out_stream.process,reduce(lambda x, y: str(x) + "\n" + str(y), contaminants))
    loop_collapse_conta = "Contaminants"
    if len(contaminants) > 3:
      contalist = contaminants[0] + "..." + contaminants[-1] + " (" + str(len(contaminants)) + ")"
    else:
      contalist = reduce(lambda x, y: str(x) + " " + str(y), contaminants)
    basetng.logLoopOpen(enum_out_stream.process,loop_collapse_conta,1,contalist)
    basetng.logLoopNext(enum_out_stream.process,loop_collapse_conta,contalist)

    basetng.result = [] #for results
    loop_conta = "Contaminant"
    basetng.logLoopOpen(enum_out_stream.process,loop_conta,len(contaminants),"Inner loop for fetch")
    resolutions = list(set(resolutions))
    resolutions = sorted(resolutions,reverse=True)
    #we need to remove the resolutions that are less that what is possible by the data
    newresolutions = []
    for reso in sorted(resolutions):
      if float(reso) <= float(hires):
        if hires not in newresolutions:
          newresolutions.append(float(hires))
      if float(reso) > float(hires):
        newresolutions.append(reso)
    resolutions = newresolutions

    #calculate the esm to the highest resolution and do frf at lower resolutions also
    class BreakLoopException(Exception):
        pass
    basetng.pdbtncs = []
    for ipdb,item in enumerate(contaminants): #contaminants
   #{{{
      #branch from tncso aka aniso1, for the esm
      basetng.load_moderunner(tncso,user_cards,dryrun_cards)
      try: #for throw from any given contaminant, either continue or exit
     #{{{
        pdbid = item
        shortpdbid = pdbid
        if len(pdbid) > 4:
          shortpdbid = os.path.basename(pdbid)
        basetng.logLoopNext(enum_out_stream.process,loop_conta,shortpdbid + "#" + str(ipdb+1))
        basetng.input.set_as_str(".phasertng.pdbout.pdbid.",str(pdbid))
        #downloaded for each tncs loop
        ModeRunner(basetng=basetng,mode_list=['fetch'])
        title = basetng.input.get_as_str(".phasertng.pdbout.deposited_title.")
        downloaded = len(basetng.input.generic_string)>0
        if not (downloaded):
          basetng.logLoopInfo(enum_out_stream.process,"Failure in fetch, continue")
          continue
        if io_use_tfz:
          basetng.input.set_as_str(".phasertng.molecular_transform.preparation.","decomposition interpolation")
        else:
          basetng.input.set_as_str(".phasertng.molecular_transform.preparation.","decomposition")
        minres = min(resolutions)
        basetng.input.set_as_str(".phasertng.resolution.",str(minres))
        msg = "pdbid=" + shortpdbid
        esm = ModeRunner(basetng=basetng,mode_list=['esm']).solution()
        #save the values for starting the frf
        searchid =  basetng.input.get_as_str(".phasertng.search.id.")
        searchtag = basetng.input.get_as_str(".phasertng.search.tag.")
        loop_tncs = "Tncs Order"
        basetng.logLoopOpen(enum_out_stream.process,loop_tncs,len(tncs_selections),
               '\n'.join(n for n in [d['info'] for d in tncs_selections]))
        for t,tncs_selection in enumerate(tncs_selections):
       #{{{
          basetng.logLoopNext(enum_out_stream.process,loop_tncs,tncs_selection['info'])
          basetng.input.generic_string = "changeling" #context
          basetng.load_moderunner(tncs_selection["feff"],user_cards,dryrun_cards)
          #something for this loop
          find1 = ModeRunner(basetng=basetng,mode_list=['find']).solution()
          loop_reso = "Resolutions"
          msg = "pdbid=" + shortpdbid + \
                  " dmin=" + str(reso)
          basetng.logLoopOpen(enum_out_stream.process,loop_reso,len(resolutions),
           '\n'.join(n for n in [str(d) for d in resolutions]))
          thispdbtncs = { "pdbid" : pdbid, #for fetch at the end
              "tncs" : str(tncs_selection['order-vector-1a']),
              "shortid" : shortpdbid,
              "identifier" : str(basetng.DAGDATABASE.PATHWAY.ULTIMATE.database_subdir()),
              "title" : title.replace(" ","_"),
              "dminrfz" : [],
              }
          for reso in resolutions:
         #{{{
            try:
              basetng.logLoopNext(enum_out_stream.process,loop_reso,str(reso))
              basetng.load_moderunner(find1,user_cards,dryrun_cards)
              msg = "pdbid=" + shortpdbid + \
                    " dmin=" + str(reso) + \
                    " tncs=" + str(tncs_selection['order-vector-1a'])
              basetng.input.set_as_str(".phasertng.resolution.",str(reso))
              basetng.input.set_as_str(".phasertng.search.id.",str(searchid))
              basetng.input.set_as_str(".phasertng.search.tag.",str(searchtag))
          #   basetng.input.set_as_str(".phasertng.fast_rotation_function.maximum_stored.",str(1))
              basetng.input.set_as_str(".phasertng.fast_translation_function.packing.use.",str(False))
              #needs cell scale etc
              ModeRunner(basetng=basetng,mode_list=['frf'])
              if basetng.DAGDATABASE.size():
                rfz = basetng.DAGDATABASE.NODES.LIST[0].ZSCORE
                rfzstr = str(round(rfz*100.)/100.) #two decimal places
              # basetng.logLoopInfo(enum_out_stream.process,"RFZ=(" + str(rfz) + ") " + str(io_rfz))
                if float(rfz) > float(io_rfz) and io_use_tfz:
                  basetng.input.set_as_str(".phasertng.fast_translation_function.maximum_stored.",str(1))
                  ModeRunner(basetng=basetng,mode_list=['frfr','gyre','ftf'])
                  tfz = basetng.DAGDATABASE.NODES.LIST[0].ZSCORE
                  basetng.logLoopInfo(enum_out_stream.process,"TFZ=(" + str(tfz) + ") " + shortpdbid)
              #We have to know what warning is thrown with the fraction scattering error in frf
              if any(s.startswith("Fraction scattering") for s in basetng.warnings()):
                raise ValueError(enum_err_code.input,"Fraction scattering error")
            except ValueError as e:
           #  if len(e.args) > 1:
           #    basetng.logLoopInfo(enum_out_stream.process,msg + " error=(" + str(e.args[1]) + ")")
              basetng.Set(str(enum_err_code.success).upper(),"") # clear the error code
              continue #no result stored, fracscat out of range

            #log the full results every cycle
            thispdbtncs["dminrfz"].append( {"rfz" : float(rfz),
                                            "dmin" : str(reso),
                                            "RFZ" : rfzstr } )
            if debug:
              basetng.logLoopInfo(enum_out_stream.process,"Number of results=" + str(len(basetng.result)))
         #}}}
          if len(thispdbtncs['dminrfz']) > 0:
            basetng.pdbtncs.append(thispdbtncs)
          basetng.pdbtncs = sorted(basetng.pdbtncs, key=lambda x: -x['dminrfz'][0]['rfz'] if len(x['dminrfz']) else -10000)
          basetng.logLoopShut(enum_out_stream.process,loop_reso)
       #}}}
        basetng.logLoopShut(enum_out_stream.process,loop_tncs)
        ModeRunner(basetng=basetng,mode_list=['pool'])
        if working_params.changeling.control.terminate_on_rfz:
            if float(bestrfz) >= working_params.changeling.control.termination_rfz:
              basetng.logLoopInfo(enum_out_stream.process,"Found termination RFZ")
              raise BreakLoopException
     #}}}
      except BreakLoopException:
        basetng.logLoopExit(enum_out_stream.process,tncs_conta)
        raise BreakLoopException #rethrow
      except ValueError as e:
        error = PhaserError()
        error.Set(str(e.args[0]).upper(),e.args[1])
        basetng.logLoopInfo(enum_out_stream.process,msg + " error=(" + str(error.error_message()) + ")")
        basetng.Set(str(enum_err_code.success).upper(),"") # clear the error code
        continue
   #}}}
    basetng.logLoopShut(enum_out_stream.process,loop_conta)
    basetng.logLoopShut(enum_out_stream.process,loop_collapse_conta)

    basetng.pdbtncs = sorted(basetng.pdbtncs, key=lambda x: -x['dminrfz'][0]['rfz'] if len(x['dminrfz']) else -10000)

    #reset the output
    basetng.input = InputCard()
    basetng.input.parse_multi("",user_cards,"",ignore_unknown_keys) #need this
    basetng.set_user_input_for_suite(basetng.input) #we need this
    #tree from tncso
    basetng.load_moderunner(tncso,user_cards,"")

    if wforig:
      #cannot append write_data on because there is a uniqueness check
      fetchcount = set()
      #cutoff is the same value hardwired into runPOOL
      for res in basetng.pdbtncs:
        if float(res['dminrfz'][0]['rfz']) > float(io_rfz):
          fetchcount.add(res['pdbid'])
      loop_fetch = "Fetch chains"
      basetng.logLoopOpen(enum_out_stream.process,loop_fetch,len(fetchcount),
             "")
      basetng.pdb_files = [] #hack to pass to pool
      for res in fetchcount:
        try:
          basetng.logLoopNext(enum_out_stream.process,loop_fetch,str(res))
          basetng.input.set_as_str(".phasertng.pdbout.pdbid.",str(res))
          ModeRunner(basetng=basetng,mode_list=['fetch'])
          basetng.pdb_files.append(basetng.input.get_as_str(".phasertng.pdbout.filename."))
        except Exception as e:
          pass
      basetng.logLoopShut(enum_out_stream.process,loop_fetch)

    ModeRunner(basetng=basetng,mode_list=['pool'])
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
    ModeRunner(basetng=basetng,mode_list=['tree'])
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="mtz data file",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  parser.add_argument('-v',"--verbose",dest='verbose',action='store_true',help="verbose output",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is None and args.output is None):
    print("At least one of either --phil or --output must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  if (args.input is not None):
    if not (os.path.exists(args.input)):
      print("mtz data file cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.hklin.filename=\"" + str(args.input)+"\"\n"
  if (args.verbose):
    user_phil_str = user_phil_str + "\nphasertng.suite.level=verbose\n"
  if (args.output is not None):
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      print("database directory for output cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.suite.database=\"" + str(args.output)+"\"\n"
  if (args.phil is not None or (args.input is not None and args.output is not None)):
    changeling( user_phil_str=user_phil_str)
  else:
    print("command line arguments insufficient")
    sys.exit(0)
