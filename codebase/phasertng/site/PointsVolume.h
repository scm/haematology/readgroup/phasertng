//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_PointsVolume_class__
#define __phasertng_PointsVolume_class__
#include <phasertng/site/SiteList.h>

namespace phasertng {

class PointsVolume : public SiteList
{
  private:
    double off0 = 0;
    double off1 = 0;
    dvect3 orthMin = {0,0,0};
    dvect3 orthMax = {0,0,0};
    dvect3 orthSite = {0,0,0};
    dvect3 orthSamp = {0,0,0};

    dvect3 BOX = {0,0,0};
    double SAMP = 0;
    dvect3 Site = {0,0,0};
    int    inext = 0;
    int    Size = 0;

  public:
    PointsVolume(double,dvect3);

  public:
    bool         at_end();
    void         restart(unsigned=0);
    dvect3       next_site() { return Site; }
    std::size_t  count_sites() { return Size; }
    af_dvect3    all_sites();
    double       point_distance() { return SAMP; }
};

}
#endif
