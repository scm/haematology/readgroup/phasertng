//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/boolean.h>

namespace phasertng {
namespace type {

  boolean::boolean(
      const std::string parameter,
      const boost::logic::tribool dephalt_
  ) :
      optional(parameter),
      dephalt(dephalt_),
      uvalue(dephalt_)
  { }

  void boolean::set_value(bool other)
  {
    user = true;
    uvalue = other;
  }

  void boolean::set_value(const sv_string& vcards,const bool& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  boost::logic::tribool boolean::value_or_default_tribool() const
  {
    if (user)
      return bool(uvalue);
    return dephalt;
  }

  bool boolean::value_or_default() const
  {
   // phaser_assert(!boost::logic::indeterminate(value_or_default_tribool()));
    return bool(value_or_default_tribool());
  }

  char boolean::value_or_default_char() const
  {
    if (user)
    {
      if (boost::logic::indeterminate(uvalue)) return 'U';
      if (bool(uvalue)) return 'Y';
      if (!bool(uvalue)) return 'N';
    }
    if (boost::logic::indeterminate(dephalt)) return 'U';
    if (bool(dephalt)) return 'Y';
    return 'N';
  }

  //virtual
  bool
  boolean::is_default() const
  {
    if (boost::logic::indeterminate(dephalt))
      return boost::logic::indeterminate(uvalue);
    return boost::logic::indeterminate(uvalue) ? false : (bool(uvalue) == bool(dephalt));
  }

  void
  boolean::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  boolean::to_python()
  { return unparse_parameter(false); }

  void
  boolean::from_python(std::string str)
  {
    user = true;
    if (str == "True") uvalue = true;
    if (str == "False") uvalue = false;
    if (str == None()) uvalue = boost::logic::indeterminate;
  }

  std::string
  boolean::unparse_parameter(bool phil) const
  {
    if (boost::logic::indeterminate(uvalue))
      return None();
    if (uvalue == true)
      return "True";
    return "False";
  }

  std::string
  boolean::show_default(bool phil) const
  {
    if (boost::logic::indeterminate(dephalt))
      return None();
    if (dephalt == true)
      return "True";
    return "False";
  }

}}
