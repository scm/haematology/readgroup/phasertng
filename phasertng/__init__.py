
import boost_adaptbx.boost.python as bp
try:
  ext = bp.import_ext("phasertng_ext")
  from cctbx.array_family import flex
  from phasertng_ext import *
  from phasertng import pickle_support
  pickle_support.enable()
except Exception:
  print("Import of phasertng failed")
  pass


class PhaserException(Exception):
    """
    Exception raised in the phaser module
    """

class PhaserValueError(PhaserException):
    """
    An object's value is not suitable
    """
