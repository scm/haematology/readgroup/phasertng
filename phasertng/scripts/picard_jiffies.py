import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError,FileSystem,enum_entry_data
from phasertng import enum_out_stream,enum_other_data
import copy

class termination:
    def __init__(self,control):
         self.io_termination_rfactor = float(control.termination_rfactor)
         self.io_terminate_on_rfactor = bool(control.terminate_on_rfactor)
         self.io_loop_termination_rfactor = float(control.loop_termination_rfactor)
         self.io_terminate_tncs_loop_on_rfactor = bool(control.terminate_tncs_loop_on_rfactor)
         self.io_terminate_cell_content_scaling_loop_on_rfactor = bool(control.terminate_cell_content_scaling_loop_on_rfactor)

    def stop_tncs_loop(self,results):
          if not self.io_terminate_tncs_loop_on_rfactor : return False #flag for activating termination
          if (len(results['found_solution']) == 0): return False
          rterminate = float(results['found_solution'][0]['rfac']) <= float(self.io_loop_termination_rfactor)
          return rterminate #regardless of cellz and tncs_order match

    def stop_ccs_loop(self,results):
          if not self.io_terminate_cell_content_scaling_loop_on_rfactor : return False #flag for activating termination
          if (len(results['found_solution']) == 0): return False
          rterminate = float(results['found_solution'][0]['rfac']) <= float(self.io_loop_termination_rfactor)
          return rterminate #regardless of cellz and tncs_order match

    def stop_loop(self,results):
          both = self.io_terminate_cell_content_scaling_loop_on_rfactor and self.io_terminate_tncs_loop_on_rfactor;
          if not both : return False #flag for activating termination
          if (len(results['found_solution']) == 0): return False
          rterminate = float(results['found_solution'][0]['rfac']) <= float(self.io_loop_termination_rfactor)
          return rterminate #regardless of cellz and tncs_order match

    def stop_search(self,results):
          if not self.io_terminate_on_rfactor : return False #flag for activating termination
          if (len(results['found_solution']) == 0): return False
          rterminate = float(results['found_solution'][0]['rfac']) <= float(self.io_termination_rfactor)
          #is None test prevents failure but is not used in logic since extra None test
          return rterminate #regardless of cellz and tncs_order match

    def search_rfac(self,) -> str:
        return f"{self.io_termination_rfactor:.0f}"

    def loop_rfac(self,) -> str:
        return f"{self.io_loop_termination_rfactor:.0f}"

class tfzscore:
    def __init__(self):
         self.tfz_nonpolar = float(8.0)
         self.tfz_polar = float(7.0)
         self.tfz_p1 = float(5.0)
         self.tfz_certain = float(11.0)

    def reached_certain_tfz(self,modedata) -> bool:
        number_of_poses = modedata.get_number_of_poses()
        top_tfz = modedata.get_top_tfz()
        if not int(number_of_poses) > 0:
            return False
        return float(top_tfz) >= float(self.tfz_certain)

    def solved_tfz(self,modedata) -> float:
        number_of_poses = modedata.get_number_of_poses()
        polarity = modedata.get_polarity()
        if int(number_of_poses) > 0:
            return float(self.tfz_nonpolar)
        if polarity == "is_nonpolar":
            return float(self.tfz_nonpolar)
        if polarity == "is_polar":
            return float(self.tfz_polar)
        return float(self.tfz_p1)

    def reached_tfz(self,modedata) -> bool:
        number_of_poses = modedata.get_number_of_poses()
        polarity = modedata.get_polarity()
        top_tfz = modedata.get_top_tfz()
        if not int(number_of_poses) > 0:
            return False
        target_tfz = self.solved_tfz(modedata)
        return float(top_tfz) >= float(target_tfz)

    def has_certain_tfz_solution(self,mrsolution):
        if mrsolution is None: return None
        for k,modedata in mrsolution.items():
            if self.reached_certain_tfz(modedata):
        #   if rundict['tncs_order'] == tncs_order:
        #   if rundict['ccs'] == ccs:
              return True
        return False

    def tfzstr(self,modedata) -> str:
        from math import floor
        top_tfz = modedata.get_top_tfz()
        return f"{floor(top_tfz * 100 + 0.5) / 100:.2f}"

#found solutions accumulates solutions that pass the tfz test, not necessarily the rfactor test
def found_solution_load(results,pathway,node,basetng):
    #{{{
      for sol in results['found_solution']:
        sol["new"] = ' ' #all the others are not new
      strR = "%.2f" % round(node.RFACTOR,2)
      #recreate the filename for the pdb file output from this node
    # subdir = basetng.DAGDATABASE.lookup_modlid(node.TRACKER.ULTIMATE).database_subdir()
    # entfile = node.TRACKER.ULTIMATE.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
    # iopdb = FileSystem(basetng.DataBase(),subdir,entfile)
      results['found_solution'].append(
         {"new":'>', #flag this as new, so that when reported we get the arrow
          "rfac":strR,
          "node":node.TRACKER.ULTIMATE,
          "cellz":node.CELLZ,
          "tncs_order":node.TNCS_ORDER,
          "pathway":pathway.ULTIMATE,
          "refinement.method" : basetng.input.get_as_str(".phasertng.refinement.method.").title(),
    #     "pdbfilename" : iopdb,
          "#":len(node.POSE)})
      results['found_solution'] = sorted(results['found_solution'], key = lambda i: i['rfac'])
      return results
    #}}}

def list_dag_cards_filenames(basetng):
   #{{{
    dag_cards_filenames = []
    s = int(basetng.input.size(".phasertng.expected.subdir."))
    n = int(basetng.input.size(".phasertng.expected.filenames_in_subdir."))
    assert(n == s)
    for i in range(n):
      dag_cards_filename = [ str(basetng.input.get_as_str_i(".phasertng.expected.subdir.",i)),
                             str(basetng.input.get_as_str_i(".phasertng.expected.filenames_in_subdir.",i))]
      if dag_cards_filename not in dag_cards_filenames:
        dag_cards_filenames.append(dag_cards_filename)
    return dag_cards_filenames
   #}}}

def store_mrsolution(modedata,current_seeks,basetng):
 #{{{
   #current_seeks is the list []
   #convert to string for index in mrsolution
    tfzobj = tfzscore() #does not depend on the input, values from research
    msg1 = "Store solution"
    txt = ' '.join(str(x) for x in current_seeks)
    msg1 = msg1 + ("\nSeek:\n---" + txt)
    if txt in basetng.mrsolution:
      msg1 = msg1 + ("\nSeek results overwritten for subsequent loops")
    else:
      msg1 = msg1 + ("\nNew seek results stored for subsequent loops")
    if modedata.get_size() == 0:
      msg2 = "Deadend of placements"
    elif tfzobj.reached_tfz(modedata):
      msg2 = "Partial placement, high tfz (TFZ=" + tfzobj.tfzstr(modedata) +")"
    else:
      msg2 = "Failure to place, low tfz (TFZ=" + tfzobj.tfzstr(modedata) +")"
    basetng.logLoopInfo(enum_out_stream.process,msg1+"\n"+msg2)
    modedata.set_current_seeks(list(current_seeks))
    modedata.message = msg2
    modedata.restart_at_cell_content_scaling = copy.deepcopy(basetng.restart_at_cell_content_scaling)
    basetng.mrsolution[txt] = modedata
    return basetng
 #}}}
