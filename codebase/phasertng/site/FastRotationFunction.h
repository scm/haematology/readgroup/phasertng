//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_FastRotationFunction_class__
#define __phasertng_FastRotationFunction_class__
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/main/pointers.h>
#include <phasertng/site/AnglesStored.h>
#include <phasertng/site/PointsStored.h>
#include <phasertng/src/Elmn.h>
#include <phasertng/cctbx_project/cctbx/maptbx/structure_factors.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <list>

namespace phasertng {

class UnitCell; //forward declaration

struct rotinfo
{
  double rotation = 1; //may be non-integer
  int order = 1; //rounded
  double remainder = 0; //remainder to integer
  dvect3 cart_deg = { 0,0,0 };
  double chi = 0;
  bool parallel = false;
  bool space_group_symmetry = false;
  double ncs_top = 0; //same for all, easier than pair return
  bool is_pointgroup(int maxorder)
  {
    double angtol = 360./(maxorder*2.); //dynamic difference
    double tolerance = angtol/360.;
    return (order > 1 and order < maxorder and remainder < tolerance);
  }
};

/*
 *  This could be sped up by finding peaks in 2D map rather than
 *  interpolating onto hexagonal grid first
 *  or could be sped up by merging the 2D maps into one 3D map
 *  and interpolating peaks in that map
 *  In both cases you would lose the number of points
 *  in a peak as a parameter - But number of of points in the peaks
 *  is not very good in the hexagonal method either
 *  because the hexagonal grid is only approximate - not evenly
 *  spaced (you can count the neighbours for each point to
 *  see that this is not the case)
*/

  namespace dag {
    class Node; //foward declaration
  }

  class FastRotationFunction : public AnglesStored
  {
    public:
      SpaceGroup SG; //space group for symmetry in self rotation function
      //the minimum percentage at which the value is present, or zero for absent
      std::map<int,double> hasX22;
      std::map<int,double> hasX;

    private:
      SpaceGroup clustering_sg;
      //Space group used for clustering and moving peaks
      //If there is tncs present this will be P1 not the reflection space group

    private: //fast clustering, constants for parallel section
      int    NTHREADS = 1;
      bool   USE_STRICTLY_NTHREADS = false;

    private: //fast clustering, constants for parallel section
      std::vector<dmat33> orth_rotsymtr_frac;
      double CLUSTER_ANG;
      double ANGTOL;
      double max_rmsd_for_angle;

    public:
    //base class AnglesStored of FastRotationFunction will contain the merged and sorted list
    //vectors of AnglesStored below contain the values on the beta section
      std::list<AnglesStored> beta_section; //so that memory allocation is not contiguous
      af::versa<double, af::c_grid<3>> frfmap;
      ivect3                  gridding;

    public: //members
      const_ReflectionsPtr  REFLECTIONS;
      double f_max = std::numeric_limits<double>::lowest();
      double z_max = std::numeric_limits<double>::lowest();

    public:
      FastRotationFunction(const_ReflectionsPtr,bool);

    public:
      void set_nthreads(int n,bool b)
      { NTHREADS = n; USE_STRICTLY_NTHREADS = b; }

      af_string calculate_fourier_transforms(
        const elmn_array_t&,
        const elmn_array_t&,
        const int,
        const double,
        const int,
        const bool,
        const bool
        );

    private:
      void
      parallel_beta_section(
          const sv_int,
          const elmn_array_t&,
          const double,
          const int,
          const bool,
          const bool
          );

      void
      calculate_beta_section(
          const int,
          const elmn_array_t&,
          const double,
          const int,
          const bool,
          const bool
          );

      scitbx::fftpack::real_to_complex_3d<double>
      DoRfftStuff(
          const elmn_array_t&,
          const double,
          const int,
          cctbx::maptbx::structure_factors::to_map<double>&
      );

    public:
      int total_number();
      void merge_beta_sections(bool);
      void move_to_rotational_asu();
      void clustering_off();
      void cluster(const double,int);
      void apply_partial_sort_and_maximum_stored_llg(const int);
      void apply_partial_sort_for_print(const int);
      void apply_nth_element_maximum_stored_llg(const int);
      double top_novel_llg(dag::Node);
      double apply_partial_sort_and_percent_fss(double,double,bool=true);
      void copy_value_to(std::string);
      bool statistics();
      sv_int peak_chi_sections(int);
      af_string plot_of_chi_section(const double,const int);
      af_string plot_of_peaks();
      std::vector<rotinfo> Rotation();
      void select_point_group_and_erase(int);
      sv_bool flag_space_group_symmetry();
      void expand_space_group_symmetry();
      void point_group_classes(double);

      af_string logTableHeader(int) const;
      af_string logFastTable(const int,const bool,dmat33);
      af_string logSelfTable(const int,int,double);
      af_string logTable(const int,const bool,dmat33);
      af_string logSlowTable(const int,const bool,dmat33);

      int cluster_parallel(dmat33,int,int);
      int cluster_combine(dmat33,int,int);
      std::pair<dvect3,dmat33> turn_best_sym(int,dmat33);
  };

}//phasertng
#endif
