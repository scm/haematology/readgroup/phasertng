//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_scope__
#define __phasertng_scope__
#include <string>

namespace phasertng {

//needs to also change in input_phil_keywords.py

const std::string PHIL_SCOPE();
const std::string NODE_SCOPE();
const std::string NODE_SEPARATOR();
inline const std::string SCOPE_SEPARATOR() { return "."; } // this is hardwired, not from python

}
#endif
