import os, sys
import iotbx.pdb
from iotbx.pdb import hierarchy
from phasertng.cctbx_project.mmtbx.ncs import tncs
import argparse

def convert_tncsresults_to_dict(tncsresults):
    assert(len(tncsresults) == 4)
    assert(len(tncsresults[2][0]) == 3)
    tncsdict = { "Largest TNCS order" : tncsresults[0],
                 "Peptide chain" : tncsresults[1],
                 "Fraction scattering" : round(tncsresults[3]*1000)/1000 }
    for i,vec in enumerate(tncsresults[2]):
      v0 = str("%6.3f %6.3f %6.3f"% (vec[0][0],vec[0][1],vec[0][2]))
      v1 = str("%6.1f %6.1f %6.1f"% (vec[1][0],vec[1][1],vec[1][2]))
      v2 = str(round(vec[2]*1000)/1000)
      tncsdict["transform#" + str(i+1)] = { "fractional" : v0, "orthogonal": v1, "angle" : v2 }
    return tncsdict


def FindTNCS(xyzin):
  resultsdict = {}
  xtal = iotbx.pdb.input(file_name= xyzin )
  hroot = xtal.construct_hierarchy()
  xtalsym = xtal.crystal_symmetry()

  print(xyzin)
  for angle in range(5,25,5):
      print("Angle=",angle)
      tncsinpdb = tncs.groups(pdb_hierarchy = hroot,
                              crystal_symmetry = xtalsym,
                              quiet=True,
                              angular_difference_threshold_deg=angle)
      resultsdict[angle] = convert_tncsresults_to_dict(tncsinpdb.tncsresults)
      for k,v in resultsdict[angle].items():
        print(k,":",v)
# print(resultsdict)
  return resultsdict

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--pdb",help="pdb file",type=str,required=True)
  args = parser.parse_args()
  FindTNCS(args.pdb)
