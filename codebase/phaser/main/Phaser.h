//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DEFS__
#define __PHASER_DEFS__
#include <string>
#include <vector>
#include <map>
#include <set>
#include <complex>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/sym_mat3.h>
#include <scitbx/array_family/shared.h>
#include <scitbx/constants.h>
#include <phaser/main/FloatType.h>

namespace cctbx { namespace miller { } }
namespace cctbx { namespace xray { } }
namespace phaser { namespace af = scitbx::af; }
namespace phaser { namespace fn = scitbx::fn; }
namespace phaser { namespace miller = cctbx::miller; }
namespace phaser { namespace xray = cctbx::xray; }

namespace phaser {

static const double u_cart_pdb_factor = 10000.;
static const double two_pi_sq_on_three = scitbx::constants::two_pi_sq/3.0;

typedef std::complex<double> cmplxType;

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::sym_mat3<double> dmat6;
typedef scitbx::vec3<int> ivect3;
typedef scitbx::af::shared<dvect3>      af_dvect3;
typedef scitbx::af::shared<dmat6>       af_dmat6;
typedef scitbx::af::shared<double>   af_float;
typedef scitbx::af::shared<double>      af_double;
typedef scitbx::af::shared<std::string> af_string;
typedef scitbx::af::shared<bool>        af_bool;
typedef scitbx::af::shared<cmplxType>   af_cmplx;
typedef std::vector<bool> bool1D;
typedef std::vector<double> float1D;
typedef std::vector<std::vector<double> > float2D;
typedef std::vector<int> int1D;
typedef std::vector<unsigned> unsigned1D;
typedef std::vector<std::string> string1D;
typedef std::map<std::string,int>       map_str_int;
typedef std::map<int,std::string>       map_int_str;
typedef std::map<std::string,double>    map_str_float;
typedef std::set<std::string>                      stringset;
typedef std::vector<std::pair<int,std::string> >   changed_list;

class llgc_coefs;
typedef af::shared<llgc_coefs>  af_llgc_coefs;

} //phaser

#endif
