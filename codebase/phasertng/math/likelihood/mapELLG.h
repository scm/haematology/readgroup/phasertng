//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_mapELLG_class__
#define __phasertng_mapELLG_class__

namespace phasertng {
namespace likelihood {

class mapELLG
{
  public:
    mapELLG() {}

  public:
    double get(double Emean, double DobsSigA, bool cent);

  private:
    double centric(double Emean, double DobsSiga);
    double acentric(double Emean, double DobsSigA);

};

}}
#endif
