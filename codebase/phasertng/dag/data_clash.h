//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_data_clash_class__
#define __phasertng_data_clash_class__
#include <scitbx/vec3.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::vec3<int> ivect3;

namespace phasertng {
namespace dag {

class data_clash
{
  public:
    int index = 0;
    int atoms_clash = 0;
    int atoms_trace = 0;

    data_clash() {}

    data_clash(
        int index_,
        int atoms_clash_,
        int atoms_trace_
      ) :
        index(index_),
        atoms_clash(atoms_clash_),
        atoms_trace(atoms_trace_)
    { }

    scitbx::vec3<int> getter() const
    { return { index,atoms_clash,atoms_trace }; }

    double overlap_fraction() const
    { return static_cast<double>(atoms_clash)/static_cast<double>(atoms_trace); }

    double overlap_percent() const
    { return 100.*overlap_fraction(); }

    ivect3 as_vector() const
    { return ivect3(index,atoms_clash,atoms_trace); }

  // Overloaded operators
  data_clash&
  operator=( const scitbx::vec3<int>& rhs )
  {
    // Normal assignment
    index = rhs[0];
    atoms_clash = rhs[1];
    atoms_trace = rhs[2];
    return *this;
  }

};

}} //phasertng
#endif
