//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/Hexagonal.h>

namespace phasertng {

  bool Hexagonal::at_end() const
  { return (t >= max_t); }

  void Hexagonal::restart()
  { t = 0; }

  std::size_t Hexagonal::num_sites()
  { return max_t; }

  void
  Hexagonal::setup_around_point(
      dvect3 P,
      double SAMP,
      double RANGE,
      bool store_points
    )
  {
    max_t = 0;
    POINT = P;
    sv_dvect3 comp;
    if (store_points)
    {
      comp.push_back(POINT);
    }
    max_t++;  //point

    dvect3 orthRange(RANGE,RANGE,RANGE);
    orthMin = POINT - orthRange;
    orthMax = POINT + orthRange;
    RANGEsqr = fn::pow2(RANGE);
    orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
    off1 = 5.0/6.0;
    bool first_point(true);
    dvect3 first_orthSite(orthSite);
    double first_off1(off1);
    for (orthSite[2] = orthMin[2]; orthSite[2] < orthMax[2]; orthSite[2] += orthSamp[2])
    {
      off1 = 1 - off1;
      off0 = 3.0/4.0;
      for (orthSite[1] = orthMin[1] + off1 * orthSamp[1]; orthSite[1] < orthMax[1]; orthSite[1] += orthSamp[1])
      {
        off0 = 1 - off0;
        for (orthSite[0] = orthMin[0] + off0 * orthSamp[0]; orthSite[0] < orthMax[0]; orthSite[0] += orthSamp[0])
        {
          if ((orthSite-POINT)*(orthSite-POINT) < RANGEsqr)
          {
            if (first_point) first_orthSite = orthSite;
            if (first_point) first_off1 = off1;
            first_point = false;
            if (store_points) comp.push_back(orthSite);
            max_t++;
          }
        }
      }
    }
    t = 0;
    orthSite = first_orthSite;
    off1 = first_off1;
  }

  dvect3
  Hexagonal::next_site(int ext_t)
  {
    //calculations done in terms of orthogonal sampling, output as fractional
    if (t == ext_t && t == 0) return fractionalization_matrix()*(POINT);
    while (t < ext_t)
    {
      do {
        orthSite[0] += orthSamp[0];
        if (orthSite[0] < orthMax[0]) { }
        else
        {
          orthSite[1] += orthSamp[1];
          if (orthSite[1] < orthMax[1])
          {
            orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          }
          else
          {
            orthSite[0] = orthMin[0] + off0 * orthSamp[0];
            orthSite[2] += orthSamp[2];
            if (orthSite[2] < orthMax[2])
            {
              off1 = 1 - off1;
              off0 = 3.0/4.0;
              orthSite[1] = orthMin[1] + off1 * orthSamp[1];
              off0 = 1 - off0;
              orthSite[0] = orthMin[0] + off0 * orthSamp[0];
            }
            else { break; } //out of region
          }
          off0 = 1 - off0;
        }
      }
      while ((orthSite-POINT)*(orthSite-POINT) > RANGEsqr);
      t++;
    }
    return  fractionalization_matrix()*(orthSite);
  }

} //phasertng
