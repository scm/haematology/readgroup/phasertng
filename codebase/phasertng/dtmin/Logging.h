//(c) 2000-2024 Cambridge University Technical Services Ltd
#ifndef __phasertng_mintbx_Logging_class__
#define __phasertng_mintbx_Logging_class__
#include <phasertng/dtmin/includes.h>

namespace phasertng {
namespace dtmin {

class Logging
{
  public:
    Logging() { }
    virtual ~Logging() {}

  public:
    virtual af_string
    initial_statistics() { return af_string(); }

    virtual af_string
    current_statistics() { return af_string(); }

    virtual af_string
    final_statistics() { return af_string(); }
};

}} //phasertng

#endif
