//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_AnglesAround_class__
#define __phasertng_AnglesAround_class__
#include <phasertng/site/SiteList.h>

namespace phasertng {

class AnglesAround : public SiteList
{
  private:
    double tpmin = 0;
    double tmmin = 0;
    double tpmax = 0;
    double dtp = 0;
    double tmmax = 0;
    double dtm = 0;
    double cosb2 = 0;
    double sinb2 = 0;
    double offm = 0;
    double tpstart = 0;
    double tmstart = 0;
    double betmin = 0;
    double betmax = 0;
    double dbeta = 0;
    double nbeta = 0;
    dvect3 starteuler;
    double offp = 0;
    double alpha = 0;
    double beta = 0;
    double gamma = 0;
    double thetp = 0;
    double thetm = 0;

  private:
    dvect3 ANGLE = {0,0,0};
    double SAMP = 0;
    double RANGE = 0;
    dvect3 Site = {0,0,0};
    int    inext = 0;
    int    Size = 0;

  public:
    AnglesAround(double,dvect3,double);

  public:
    bool        at_end();
    void        restart(unsigned=0);
    dvect3      next_site() { return Site; }
    std::size_t count_sites() { return Size; }
    af_dvect3   all_sites();
    double      point_distance() { return SAMP; }
};

}
#endif
