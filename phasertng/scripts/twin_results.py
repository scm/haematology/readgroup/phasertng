import sys, os, os.path, copy
import argparse
from cctbx.array_family import flex
#from iotbx import extract_xtal_data
#from iotbx import pdb
#from iotbx import reflection_file_utils
import iotbx
import iotbx.phil
from iotbx.cli_parser import run_program
from iotbx.pdb import crystal_symmetry_from_pdb as from_pdb
from libtbx.utils import Sorry
from mmtbx import utils
from mmtbx.scaling import twin_analyses
import mmtbx.f_model
import mmtbx.model
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from six.moves import cStringIO as StringIO

def get_twin_results(basetng,twinned,iomtz):
      basetng.logUnderLine(enum_out_stream.logfile,"Twinning and symmetry analyses")
      DAGDB = basetng.DAGDATABASE
      basetng.logTab(enum_out_stream.logfile,"Reported as twinned: " + str(twinned))
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iomtz.qfstrq())
      if not os.path.exists(iomtz.fstr()):
        raise ValueError(enum_err_code.fileopen,iomtz.fstr())
      outstr = TextIO()
      mtzobj = iotbx.file_reader.any_file(iomtz.fstr())
      d_min = sorted(mtzobj._file_object._file_content.max_min_resolution())[0]
      basetng.logTab(enum_out_stream.logfile,"Resolution of data = " + str(int(d_min*100)/100.))
      basetng.logBlank(enum_out_stream.logfile)
      miller_arrays = mtzobj.file_object.as_miller_arrays(crystal_symmetry=mtzobj.crystal_symmetry())
      for ma in miller_arrays:
        if ma.is_xray_data_array(): break
    # zero_sel = ma.sigmas()==0 #because not caught in twin_analysis
    # these_absent_millers = ma.select(~zero_sel)
    # assert(these_absent_millers.size())
    # AJM rather than catching as above I have modified phasertng to
    # add non-zero sigmas to 8ruc (only F deposited) in the same way they are added in xtriage
      twin_results = twin_analyses.twin_analyses(
              miller_array=ma,
              out=outstr,
              )
      outstr = TextIO()
      twin_results.show(outstr)
      wf = basetng.WriteFiles()
      subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
      if wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
         os.makedirs(os.path.join(basetng.DataBase(),subdir))
      if wf:
          lines = outstr.getvalue()
          logout = DAGDB.PATHWAY.ULTIMATE.extra_file_stem_ext(".xtriage.log")
          newlogname = FileSystem(basetng.DataBase(),subdir,logout)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Xtriage",newlogname)
          with open(newlogname.fstr(), "w") as f:
              basetng.logTab(enum_out_stream.verbose,newlogname.fstr())
              f.write("PHASERTNG XTRIAGE\n")
              f.write("-----------------\n")
              f.write(lines)
  #   for line in lines:
  #     lline = line.lstrip();
  #     basetng.logTab(enum_out_stream.verbose,str(lline))
      outstr = TextIO()
      if (not twinned):
        basetng.logTab(enum_out_stream.logfile,"No twinning indicated: twin laws will NOT be used")
      for ii in range(twin_results.n_twin_laws):
        line = "Twin alpha and law:  %5.3f (%s)"%(
               twin_results.twin_summary.murray_rust_alpha[ii],
               twin_results.twin_summary.twin_laws[ii])
        basetng.logTab(enum_out_stream.logfile,line)
      if twin_results.n_twin_laws == 0:
        basetng.logTab(enum_out_stream.logfile,"No twin laws for merohedray or pseudo-merohedral twinning")
      basetng.logBlank(enum_out_stream.logfile)
      twin_results.possible_twin_laws.show(out=outstr)
    # lines = outstr.getvalue().split("\n")
    # for line in lines:
    #   lline = line.lstrip();
    #   basetng.logTab(enum_out_stream.verbose,str(lline))
      if wf:
          lines = outstr.getvalue()
          logout = DAGDB.PATHWAY.ULTIMATE.extra_file_stem_ext(".twin.log")
          newlogname = FileSystem(basetng.DataBase(),subdir,logout)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Twin Laws",newlogname)
          with open(newlogname.fstr(), "w") as f:
              basetng.logTab(enum_out_stream.verbose,newlogname.fstr())
              f.write("PHASERTNG POSSIBLE TWIN LAWS\n")
              f.write("----------------------------\n")
              f.write(lines)
      return twin_results
