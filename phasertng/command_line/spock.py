# LIBTBX_SET_DISPATCHER_NAME phasertng.spock

from iotbx.cli_parser import run_program
from phasertng.programs import spock

if (__name__ == '__main__'):
  try:
    run_program(spock.Program)
  except Exception as e:
    print(str(e))
