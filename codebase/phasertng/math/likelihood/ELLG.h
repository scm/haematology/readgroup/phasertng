//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ELLG_class__
#define __phasertng_ELLG_class__
#include <scitbx/constants.h>

namespace phasertng {
namespace likelihood {

class ELLG
{
  public:
    ELLG() {}

  public:
    double get(double Eeff, double DobsSigA, bool cent);

  private:
    double centric(double Eeff, double DobsSiga);
    double acentric(double Eeff, double DobsSigA);

  private:
    const int npts = 200;
    const int nsigma = 6;
    const double SQRT2BYPI = std::sqrt(2./scitbx::constants::pi);
};

}}
#endif
