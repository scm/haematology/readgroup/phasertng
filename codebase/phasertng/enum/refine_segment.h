//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_refine_segment_enum__
#define __phasertng_refine_segment_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace refine {
  enum segment {
             UNDEFINED, //phasertng namespace, use unique name
             OCCUPANCY,
             BFACTOR,
             POSITION
           };
/*
  MakeEnum ( segment,
             (UNDEFINED) //phasertng namespace, use unique name
             (NONE)
             (XRAY)
             (EM)
           );
*/
}} //phasertng

#endif
