//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Atoms.h>
#include <phasertng/main/Error.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/main/includes.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/main/constants.h>
#include <phasertng/io/tostr2.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <fstream>

namespace phasertng {

  void
  Atoms::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    if (!infile.is_open()) throw Error(err::FILEOPEN,fstr());
    af_string pdblines;
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        //have to parse lines in optional pairs ATOM/ANISOU
        pdblines.push_back(buffer);
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    infile.close();
    SetPdb(pdblines);
  }

  void
  Atoms::SetPdb(af_string pdblines)
  {
    try
    {
      //keep the next_atom outside the ATOM parsing scope
      //because we are going to check that if the next record is an ANISOU or REMARK
      //that it corresponds with the revious ATOM record
      PdbRecord next_atom;
      pod::xtra::scatterer next_xtra;
      bool previous_atom_record(false);

      for (auto buffer : pdblines)
      {
        if (!buffer.find("REMARK") and (buffer.find("REFLID") != std::string::npos))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string remark,remarktag,reflid,number,tag;
            sstream >> remark;
            sstream >> remarktag;
            sstream >> reflid;
            sstream >> number;
            sstream >> tag;
            //REFLID is overwritten here
            try { REFLID.parse_string(number,tag); }
            catch (...) { pdb_errors.insert("REFLID read error"); }
          }
          catch (...) {}
        }
        else if (!buffer.find("REMARK") and (buffer.find("MODLID") != std::string::npos))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string remark,remarktag,modlid,number,tag;
            sstream >> remark;
            sstream >> remarktag;
            sstream >> modlid;
            sstream >> number;
            sstream >> tag;
            try { MODLID.parse_string(number,tag); }
            catch (...) { pdb_errors.insert("MODLID read error"); }
          }
          catch (...) {}
        }
        else if (!buffer.find("REMARK") and (buffer.find("TIMESTAMP") != std::string::npos))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string remark,remarktag,modlid;
            sstream >> remark;
            sstream >> remarktag;
            sstream >> modlid;
            sstream >> TIMESTAMP;
          }
          catch (...) {}
        }
        else if (!buffer.find("CRYST1"))
        {
          std::stringstream sstream(buffer);
          try
          {
            std::string cryst1; af::double6 uc;
            sstream >> cryst1;
            sstream >> uc[0];
            sstream >> uc[1];
            sstream >> uc[2];
            sstream >> uc[3];
            sstream >> uc[4];
            sstream >> uc[5];
            std::string sg = buffer.substr(55,10);
            //int z = std::atoi(buffer.substr(66,4).c_str()); //not used
            UC = UnitCell(uc);
            SG = SpaceGroup(sg);
          }
          catch (...) { read_pdb_errors.insert("Error reading CRYST1"); }
        }
        if (previous_atom_record and !buffer.find("ANISOU"))
        {
          bool match = next_atom.ReadAnisouCard(buffer);
          if (!match)
          read_pdb_errors.insert("ANISOU card does not match previous ATOM card ");
        }
        if (previous_atom_record and !buffer.find("REMARK  "+ constants::pdb_remark_atom_number)) //REMARK
        {
          std::string nonconst_pdb_remark_atom_number = constants::pdb_remark_atom_number;
          try {
            char Num5[6]; //leave room for \0
            char scattering_type[5],ResName[4],Chain[3]; //tmp
            std::sscanf(buffer.c_str(),
         "REMARK %2s ATOM%5s EXTRA FIX X%1dO%1dB%1dE%1d SSRE J%1dS%1d NP X%1dB%1d M%02d Z%07.3f",
         nonconst_pdb_remark_atom_number.c_str(),
         &Num5,
         &next_xtra.fix_site,&next_xtra.fix_occ,&next_xtra.fix_bfac,&next_xtra.fix_type,
         &next_xtra.rejected,&next_xtra.restored,
         &next_xtra.n_xyz,&next_xtra.n_adp,
         &next_xtra.multiplicity,
         &next_xtra.zscore);
            int AtomNum(-999);
            std::string hybrid36_AtomNum(Num5);
            hy36decode(5,hybrid36_AtomNum.c_str(),hybrid36_AtomNum.size(),&AtomNum);
            PHASER_ASSERT(AtomNum == next_atom.AtomNum);
          }
          catch(...) {} //catches generic errors at end of file read
          //if no match, just ignore this remark card
        }
        if (previous_atom_record and !read_pdb_errors.size() and
            (!buffer.find("ATOM") || !buffer.find("HETATM")))
        {
          //includes conversion of orth to frac
          AddAtom(next_atom,next_xtra);
          pdb_errors.insert(read_pdb_errors.begin(),read_pdb_errors.end());
          read_pdb_errors.clear();
        }
        if (!buffer.find("ATOM") || !buffer.find("HETATM"))
        {
          previous_atom_record = true;
          next_atom.ReadAtomCard(buffer);
          if (next_atom.O < 0.0 and next_atom.AtomName != "UNK ")
            read_pdb_errors.insert("Atom has negative occupancy");
          if (next_atom.at_xplor_infinity() and next_atom.O != 0)
            read_pdb_errors.insert("Atom has coordinates at \"infinity\"");
          if (!next_atom.valid_element and !next_atom.valid_cluster)
            read_pdb_errors.insert("Atom scatterer not recognised");
        }
      }
      //clean up from previous cycle
      if (previous_atom_record)
      {
        //includes conversion of orth to frac
        AddAtom(next_atom,next_xtra);
      }
    }
    catch (std::exception const& err) {
    }
  }

  void
  Atoms::AddAtom(pod::xray::scatterer new_xray, pod::xtra::scatterer new_xtra)
  {
    XRAY.push_back(new_xray);
    XTRA.push_back(new_xtra);
  }

  void
  Atoms::AddAtom(PdbRecord new_atom, pod::xtra::scatterer new_xtra)
  {
    cctbx::xray::scatterer<double> new_xray;

    new_xray.site = UC.fractionalization_matrix()*new_atom.X;
    new_xray.occupancy = new_atom.O;

    if (new_atom.valid_element)
    {
      new_xray.scattering_type = new_atom.get_element();
    }
    else if (new_atom.valid_cluster)
    {
      new_xray.scattering_type = new_atom.get_element();
     //or possibly something neutral and valid, should not be used!
      new_xtra.Ax = (new_atom.get_element() == "AX");
      new_xtra.Tx = (new_atom.get_element() == "TX");
    }
    if (!new_xray.scattering_type.size())
      throw Error(err::INPUT,"No scattering type for pdb record\n    " + new_atom.Card() );

    if (!new_atom.FlagAnisoU)
    {
      new_xray.u_iso = cctbx::adptbx::b_as_u(new_atom.B);
      new_xray.set_use_u(/* iso */ true, /* aniso */ false);
      new_xray.u_star = {0,0,0,0,0,0};
    }
    else //convert cartesian AnisoU records in different types
    {
      new_xray.u_iso = 0;
      new_xray.set_use_u(/* iso */ false, /* aniso */ true);
      double u_iso = cctbx::adptbx::b_as_u(new_atom.B);
      double u_cart_u_iso = cctbx::adptbx::u_cart_as_u_iso(new_atom.AnisoU/DEF_UCART_FACTOR);
      double tol(cctbx::adptbx::b_as_u(1.0e-02));
      //if the isotropic component of AnisoU is zero and not equal to the B, add together
      dmat6 u_cart;
      if (std::fabs(u_cart_u_iso) < tol and std::fabs(u_iso) > tol)  //not standard pdb format
      {
        u_cart = cctbx::adptbx::u_iso_as_u_cart(u_iso)*DEF_UCART_FACTOR;
        u_cart += new_atom.AnisoU;
        new_xray.u_star = cctbx::adptbx::u_cart_as_u_star(UC.cctbxUC,u_cart);
      }
      else //standard pdb format, or something we can't recognise
      {
        u_cart = new_atom.AnisoU;
        new_xray.u_star = cctbx::adptbx::u_cart_as_u_star(UC.cctbxUC,u_cart);
      }
      //new_xtra.USTAR = false; //not the default, cartesian
    }
    XRAY.push_back(new_xray);
    XTRA.push_back(new_xtra);
  }

  void
  Atoms::process(const_ScatterersPtr SCATTERERS)
  {
    site_sym_table = cctbx::sgtbx::site_symmetry_table();
    //overwrite some XTRAs that may have been written to pdb file
    cctbx::sgtbx::space_group sggroup = SG.group();
    for (int a = 0; a < XRAY.size(); a++)
    {
      const std::string& t = XRAY[a].scattering_type;
      //put the atom on the exacy symmetry site if near symmetry site
      cctbx::sgtbx::site_symmetry next_site_sym(UC.cctbxUC,sggroup,XRAY[a].site);
      XRAY[a].site = next_site_sym.exact_site();
      XRAY[a].fp = SCATTERERS->fp(t);
      XRAY[a].fdp = SCATTERERS->fdp(t);
      XTRA[a].n_xyz = next_site_sym.site_constraints().n_independent_params();
      XTRA[a].n_adp = next_site_sym.adp_constraints().n_independent_params();
      //stored multiplicity as we want to use it
      XTRA[a].multiplicity = sggroup.order_z()/static_cast<double>(next_site_sym.multiplicity());
     //if storing site_sym_table
      auto tmp = XRAY[a].apply_symmetry(UC.cctbxUC,sggroup);
      site_sym_table.process(tmp);
    }
  }

  void
  Atoms::GetPdbHandler(PdbHandler& pdbhandler)
  {
    pdbhandler = PdbHandler(SG,UC,1);
    PHASER_ASSERT(REFLID.is_set_valid());
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(TIMESTAMP.size());
    pdbhandler.addRemarkPhaser("REFLID " + REFLID.string());
    pdbhandler.addRemarkPhaser("MODLID " + MODLID.string() + " " + MODLID.tag());
    pdbhandler.addRemarkPhaser("TIMESTAMP " + TIMESTAMP);
    af::shared<PdbRecord> PDBRECORD = Cards();
    pdbhandler.addModel(PDBRECORD);
  }

  void
  Atoms::WritePdb()
  {
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    pdbhandler.VanillaWritePdb(*this);
  }

  af_string
  Atoms::GetPdb()
  {
    PdbHandler pdbhandler;
    GetPdbHandler(pdbhandler);
    return pdbhandler.PdbString();
  }

  //this prepares an af::shared structure ready for PdbHandler
  af::shared<PdbRecord>
  Atoms::Cards() const
  {
    af::shared<PdbRecord> PDB;
    for (int a = 0; a < size(); a++)
    {
      int Count = a+1;
      //load the data back into a pdb_record for writing with PdbHandler
      PdbRecord next_atom;
      next_atom.AtomNum = Count;
      auto& xraya = XRAY.at(a);
      dvect3 frac(xraya.site);
      dvect3 orth(UC.orthogonalization_matrix()*frac);
      if (orth[0] > 9999.999 || orth[1] > 9999.999 || orth[2] > 9999.999 ||
          orth[0] < -999.999 || orth[1] < -999.999 || orth[2] < -999.999)
      { //put into unit cell
        for (int i = 0; i < 3; i++)
        {
          while (frac[i] < 0) frac[i]++;
          while (frac[i] >= 1) frac[i]--;
        }
        orth = UC.orthogonalization_matrix()*frac;//reset
      }
      next_atom.X = orth;
      next_atom.O =  xraya.occupancy;
      next_atom.AtomName = xraya.scattering_type;
      next_atom.ResNum = Count;
      PHASER_ASSERT(next_atom.ResNum <= 9999); //AJM TODO not hybrid36 for substructure
      next_atom.ResName = "SUB";
      next_atom.Chain = " A";
      next_atom.set_element(xraya.scattering_type);
      if (xraya.flags.use_u_aniso_only()) //aniso
      { //B, AnisoU set from u_iso and u_star, one or the other
        next_atom.B = 0;
        auto u_iso = cctbx::adptbx::u_star_as_u_iso(UC.cctbxUC,xraya.u_star);
        auto u_cart = cctbx::adptbx::u_star_as_u_cart(UC.cctbxUC,xraya.u_star);
        for (int i = 0; i < 6; i++) //write limit
        {
          u_cart[i] -= cctbx::adptbx::u_iso_as_u_cart(u_iso)[i];
          u_cart[i] *= DEF_UCART_FACTOR;
          u_cart[i] = std::min(u_cart[i],9999999.);
        }
        next_atom.AnisoU =  u_cart;
      }
      else
      {
        next_atom.B =  cctbx::adptbx::u_as_b(xraya.u_iso);
      }
      char EXTRA[80]; //only works for Count < 999
      const pod::xtra::scatterer xtraa = XTRA.at(a);
      //REMARK parsed in ReadPdb above
      char Num5[6]; //leave room for \0
      hy36encode(5,next_atom.AtomNum,Num5);
      snprintf(EXTRA,80,//c++11
         "REMARK %2s ATOM%5s EXTRA FIX X%1dO%1dB%1dE%1d SSRE J%1dS%1d NP X%1dB%1d M%02d Z%07.3f",
         constants::pdb_remark_atom_number.c_str(),
         Num5,
         xtraa.fix_site,xtraa.fix_occ,xtraa.fix_bfac,xtraa.fix_type,
         xtraa.rejected,xtraa.restored,
         xtraa.n_xyz,xtraa.n_adp,
         xtraa.multiplicity,
         xtraa.zscore);
      next_atom.ExtraFlags = std::string(EXTRA);
      PDB.push_back(next_atom);
    }
    return PDB;
  }

  std::set<std::string>
  Atoms::set_of_atomtypes() const
  {
    std::set<std::string> ATOMTYPES;
    for (int a = 0; a < XRAY.size(); a++)
      ATOMTYPES.insert(XRAY[a].scattering_type);
    return ATOMTYPES;
  }

  int
  Atoms::number_of_atomtypes() const
  { return set_of_atomtypes().size(); }

  af_string
  Atoms::logChangeHand(std::string description)
  {
    af_string output;
    output.push_back(">>Sites before and after enantiomorph change: " + description+"<<");
    output.push_back(snprintftos(
        "     %-6s %-6s %-6s -> %-6s %-6s %-6s   %-s",
        "X","Y","Z","X","Y","Z","Atomtype"));
    std::vector<dvect3> orth_site(size());
    std::vector<dvect3> frac_site(size());
    std::vector<dvect3> orth_hand(size());
    std::vector<dvect3> frac_hand(size());
    for (int a = 0; a < size(); a++)
    {
      frac_site[a] = XRAY[a].site;
      orth_site[a] = UC.orthogonalization_matrix()*XRAY[a].site;
    }
    this->change_to_enantiomorph(); //other hand
    for (int a = 0; a < size(); a++)
    {
      orth_hand[a] = UC.orthogonalization_matrix()*XRAY[a].site;
    }
    for (int a = 0; a < size(); a++) //revert
      XRAY[a].site = frac_site[a];
    for (int a = 0; a < size(); a++)
    {
      output.push_back(snprintftos(
          "#%-3d % 6.1f % 6.1f % 6.1f -> % 6.1f % 6.1f % 6.1f   %-s",
          a+1,
          orth_site[a][0],orth_site[a][1],orth_site[a][2],
          orth_hand[a][0],orth_hand[a][1],orth_hand[a][2],
          XRAY[a].scattering_type.c_str()));
    }
    output.push_back("");
    return output;
  }

  af_string
  Atoms::logOccupancyHistogram(std::string description)
  {
    af_string output;
    double max_occ(0);
    for (int a = 0; a < size(); a++)
    {
      pod::xray::scatterer& A = XRAY[a];
      pod::xtra::scatterer& X = XTRA[a];
      if (!X.rejected)
        max_occ = std::max(A.occupancy,max_occ);
    }
    int div = 10;
    sv_double histogram(div,0);
    double maxStars(0);
    for (int a = 0; a < size(); a++)
    {
      pod::xray::scatterer& A = XRAY[a];
      pod::xtra::scatterer& X = XTRA[a];
      if (!X.rejected)
      {
        int h = div*std::floor(A.occupancy/max_occ);
        histogram[h]++;
        maxStars = std::max(maxStars,histogram[h]);
      }
    }
    double columns = 50; //not strict, could be one over if exactly divisible by 50
    int divStarFactor = std::floor(maxStars/columns);
    output.push_back("Mid Occupancy Bin Value");
    output.push_back(snprintftos(
        " |    %-10d%-10d%-10d%-10d%-10d%-10d",0,
        divStarFactor,divStarFactor*2,divStarFactor*3,divStarFactor*4,divStarFactor*5));
    output.push_back(snprintftos(
        " V    %-10s%-10s%-10s%-10s%-10s%-10s",
        "|","|","|","|","|","|"));
    for (int h = 0; h < div; h++)
    {
      int nstars = std::ceil(histogram[h]/divStarFactor);
      std::string stars(nstars,'*');
      output.push_back(snprintftos(
          "%5.2f %-51s :%10d",
          h*max_occ/div,stars.c_str(),histogram[h]));
    }
    output.push_back("");
    return output;
  }

  af_string
  Atoms::logAtoms(std::string description) const
  {
    af_string output;
    output.push_back(">>Atoms: " + description + "<<");
    int i = size();
    output.push_back("There " + std::string(i==1?"is ":"are ") + std::to_string(i) + " atom" + std::string(i==1?"":"s"));
    af::shared<PdbRecord> pdbrecords = Cards();
    for (int a = 0; a < pdbrecords.size(); a++)
    {
      std::array<std::string, 3> Card = pdbrecords[a].WriteAtomCard();
      for (int c = 0; c < Card.size(); c++)
        if (Card[c].size()) output.push_back(Card[c]);
    }
    return output;
  }

  af_string
  Atoms::logFracs(std::string description) const
  {
    af_string output;
    output.push_back(">>Atoms: " + description + "<<");
    for (int a = 0; a < XRAY.size(); a++)
    {
      auto& xraya = XRAY.at(a);
      output.push_back(itos(a+1,4) + " " + xraya.scattering_type +
        " fractional " + dvtos(xraya.site,7,4) + "    " + dtos(xraya.occupancy,7,4));
    }
    return output;
  }

  void
  Atoms::change_to_enantiomorph()
  {
    cctbx::sgtbx::change_of_basis_op cb_op = SG.type().change_of_hand_op();
    for (int a = 0; a < XRAY.size(); a++)
    {
      XRAY[a].site = cb_op(XRAY[a].site);
/*
      for (int j = 0; j < 3; j++)
      {
        while (XRAY[a].site[j] <= -1.0) { XRAY[a].site[j] += 1; }
        while (XRAY[a].site[j] >   0.0) { XRAY[a].site[j] -= 1; }
      }
*/
    }
  }

  void
  Atoms::change_scatterers(std::string element)
  {
    for (int a = 0; a < XRAY.size(); a++)
    {
      XRAY[a].scattering_type = tostr2(element).first;
    }
  }

  bool
  Atoms::is_duplicate(Atoms& OTHER,double transMax,bool centrosymmetric)
  {
    // skip comparison if not the same number of models in trial solutions
    if (size() != OTHER.size())
      return false;

    //cctbx::sgtbx::space_group_type SgInfo;
    cctbx::sgtbx::structure_seminvariants semis(SG.group());
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::search_symmetry ssym(flags,SG.type(),semis);
    SpaceGroup sgEuclid("Hall: " + ssym.subgroup().type().hall_symbol());
    // Repeat setup work for reference setting.
    cctbx::sgtbx::space_group reference_space_group(SG.group().change_basis(SG.cb_op()));
    cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
    cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
    cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
    dmat33 cb_op_r_to_reference_setting(SG.cb_op().c().r().as_double());
    dmat33 cb_op_r_from_reference_setting(SG.cb_op().c_inv().r().as_double());

    //if centrosymmetric is false, then set will have one element
    std::set<bool> hand_choice = { false, centrosymmetric };
    for (auto other_hand : hand_choice )
    {
      if (other_hand) //false comes before true in the set
        OTHER.change_to_enantiomorph();
      int    natoms = size();
      bool   matched(true);
      bool   allmatched(false);
      bool   originDefined(false);
      dvect3 originShift(0,0,0);
      int    isymEuclid(-1);
      // Store some information about first model in reference solution
      // Find possible choices of origin shift of checked solution that make a match with first reference model
      for (int chk_ori = 0; chk_ori < natoms; chk_ori++)
      {
        allmatched = false;
        originDefined = false;
        originShift = {0,0,0};
        dvect3 frac_chk = OTHER.XRAY[chk_ori].site;
        for (int isym = 0; isym < sgEuclid.group().order_z(); isym++)
        {
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          // Check that centres of mass are sufficiently similar
          // Centre of mass of ensemble won't change with application
          // of point group symmetry.
          dvect3 frac_chk_sym = sgEuclid.rotsym[isym].transpose()*frac_chk + sgEuclid.trasym[isym];
          for (int j = 0; j < 3; j++)
          {
            while (frac_chk[j] < -0.5) { frac_chk[j] += 1; }
            while (frac_chk[j] >= 0.5) { frac_chk[j] -= 1; }
          }
          dvect3 fracdelta = XRAY[0].site - frac_chk_sym;
          for (int j = 0; j < 3; j++)
          {
            while (fracdelta[j] < -0.5) fracdelta[j] += 1;
            while (fracdelta[j] >= 0.5) fracdelta[j] -= 1;
          }
          // Account for any continuous shifts as origin shift
          dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
          dvect3 originShift_reference(0,0,0);
          af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
          for (int j = 0; j < 3; j++)
          {
            if (isshift[j])
            {
              originShift_reference[j] = -fracdelta_reference[j];
              fracdelta_reference[j] = 0;
            }
          }
          fracdelta = cb_op_r_from_reference_setting*fracdelta_reference;
          originShift = cb_op_r_from_reference_setting * originShift_reference;
          dvect3 delta = UC.orthogonalization_matrix() * fracdelta;
          double tdist = delta.length();
          if (tdist <= transMax)
          {
            isymEuclid = isym;
            originDefined = true;
            break;
          }
          if (originDefined) break;
        } // isym
        if (!originDefined) continue; // Test different origins

        PHASER_ASSERT(originDefined);

        // Now use choice of relative origin (if defined) to test equivalence
        allmatched = true; //So far at least one match.  Negate if one fails.
        sv_dvect3 matched_site(natoms);
        sv_bool   used(natoms,false);
        for (int ref = 0; ref < natoms; ref++)
        { // For each model in the reference, check whether any matching models are the same
          // Determine sensible distance thresholds from dmin and model mean radius
          matched = false;
          for (int chk = 0; chk < natoms; chk++)
          {
            dvect3 frac_chk = OTHER.XRAY[chk].site;
            if (originDefined)
            {
              frac_chk = sgEuclid.rotsym[isymEuclid].transpose()*frac_chk + sgEuclid.trasym[isymEuclid];
            }
            for (int j = 0; j < 3; j++)
            {
              while (frac_chk[j] < -0.5) { frac_chk[j] += 1; }
              while (frac_chk[j] >= 0.5) { frac_chk[j] -= 1; }
            }
            if (used[chk]) continue;
            if (chk == chk_ori)
            {
              matched = true;
              used[chk] = true;
              matched_site[chk] = frac_chk;
            }
            else
            {
              SpaceGroup* sg = !originDefined ? &sgEuclid : &SG;
              af_dvect3 SYMMETRY = sg->symmetry_xyz_cell(frac_chk,1);
              for (int isym = 0; isym < SYMMETRY.size(); isym++)
              {
                // Check that centres of mass are
                // sufficiently similar. Centre of mass of ensemble won't
                // change with application of point group symmetry.
                dvect3 fracdelta = XRAY[ref].site - SYMMETRY[isym] + originShift;
                dvect3 delta = UC.orthogonalization_matrix() * fracdelta;
                double tdist = delta.length();
                if (tdist > transMax) continue;
                matched = true;
                used[chk] = true;
                matched_site[chk] = SYMMETRY[isym];
                if (matched) break;
              } // isym
              if (matched) break;
            } // chk
          } // chk
          if (!matched) allmatched = false;
        } // s
        if (allmatched)
          return true;
        // Failed to match everything.  Try again with another possible origin shift.
        for (int m = 0; m < natoms; m++)
          used[m] = false;
      } // chk_ori
    } // centrosymmetry
    return false;
  }

  std::pair<double,double>
  Atoms::average_occupancy_bfactor()
  {
    //-- calculate average occupancy of current atoms
    // 0.9 is "expected occupancy" for target of scattering type assignment
    double sum_occ(0),sum_u_iso(0),natoms(0);
    for (int a = 0; a < size(); a++)
    {
      pod::xray::scatterer& A = XRAY[a];
      pod::xtra::scatterer& X = XTRA[a];
      if (!X.rejected)
      {
        natoms++;
        sum_occ += A.occupancy;
        if (A.flags.use_u_aniso_only())
        {
          cctbx::adptbx::factor_u_star_u_iso<double> factor(UC.cctbxUC,A.u_star);
          sum_u_iso += factor.u_iso;
        }
        else
        {
          sum_u_iso += A.u_iso;
        }
      }
    }
    double av_occ = sum_occ/natoms;
    double av_u_iso = sum_u_iso/natoms;
    return { av_occ, cctbx::adptbx::u_as_b(av_u_iso) };
  }

  void Atoms::set_site_independent_params(int& a,scitbx::af::small<double,3> x_indep)
  { //move the atom onto a special position if it is close to one
    auto this_site_sym = site_sym_table.get(a);
    cctbx::fractional<double> new_xyz = this_site_sym.site_constraints().all_params(x_indep);
    XRAY[a].site = this_site_sym.special_op() * new_xyz;
  }
  scitbx::af::small<double,3> Atoms::site_independent_params(int& a,cctbx::fractional<double> x_param)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.site_constraints().independent_params(x_param);
  }
  scitbx::af::small<double,3> Atoms::site_independent_gradients(int& a,cctbx::fractional<double> x_grad)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.site_constraints().independent_gradients(x_grad);
  }
  scitbx::af::small<double,6> Atoms::site_independent_curvatures(int& a,af_double x_hess)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.site_constraints().independent_curvatures(x_hess.const_ref());
  }

  void Atoms::set_adp_independent_params(int& a,scitbx::af::small<double,6> u_indep)
  { //move the atom onto a special position if it is close to one
    auto this_site_sym = site_sym_table.get(a);
    XRAY[a].u_star = this_site_sym.adp_constraints().all_params(u_indep);
  }
  scitbx::af::small<double,6> Atoms::adp_independent_params(int& a,dmat6 u_param)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.adp_constraints().independent_params(u_param);
  }
  scitbx::af::small<double,6> Atoms::adp_independent_gradient(int& a,dmat6 u_grad)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.adp_constraints().independent_gradients(u_grad);
  }
  af_double Atoms::adp_independent_curvatures(int& a,af_double u_hess)
  {
    auto this_site_sym = site_sym_table.get(a);
    return this_site_sym.adp_constraints().independent_curvatures(u_hess.const_ref());
  }

  std::string
  Atoms::read_errors() const
  {
    std::string result;
    for (auto item : pdb_errors)
      result += "\n" + item;
    return result;
  }

  const int Atoms::size() const
  {
    phaser_assert(XRAY.size() == XTRA.size());
    return XRAY.size();
  }
  af::shared<pod::xray::scatterer> Atoms::scatterers_ref() const
  {
    af::shared<pod::xray::scatterer> tmp;
    for (int a = 0; a < XRAY.size(); a++)
      tmp.push_back(XRAY[a]);
    return tmp;
  }

  af_dvect3
  Atoms::get_sites_as_array()
  {
    af_dvect3 result;
    for (int a = 0; a < XRAY.size(); a++)
      result.push_back(XRAY[a].site);
    return result;
  }

}//phasertng
