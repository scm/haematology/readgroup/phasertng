#include <phasertng/math/likelihood/pIntensity.h>
#include <phasertng/math/likelihood/pNormal.h>
#include <phasertng/math/likelihood/pLogNormal.h>
#include <scitbx/constants.h>
#include <scitbx/math/erf.h>
#include <limits>
#include <algorithm>
#include <scitbx/array_family/misc_functions.h>
#include <phasertng/math/table/ParabolicCylinderD.h>

using namespace scitbx;

namespace phasertng {
extern const table::ParabolicCylinderD& tbl_pcf;
namespace math {

  double pIntensity::pEosqAcen(double eosq, double sigesq) const
  {
    assert(sigesq >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(6.);
    const double CROSSOVER2(LOGHALFMAX);
    double prob(0);
    if (sigesq > 0.)
    {
      double erfcarg((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
      if (erfcarg < CROSSOVER1)
        prob = std::exp(-eosq + fn::pow2(sigesq)/2.) *
               scitbx::math::erfc(erfcarg)/2.;
      else
      {
        double exparg(fn::pow2(eosq/sigesq)/2.);
        if (exparg < CROSSOVER2) // Used scaled erfc: erfcx(x) = exp(x^2)*erfc(x)
          prob = std::exp(-exparg) *
                 scitbx::math::erfcx(erfcarg)/2.;
        else
          prob = 0.;
      }
    }
    else // Check elsewhere for negative intensity with non-zero sigma
      prob = std::exp(-std::max(eosq,0.));
    return prob;
  }

  double pIntensity::pSmallerEosqAcen(double eosq, double sigesq) const
  {
    if (eosq > 0.) return 1.;  // Should only be called for negative intensities
    assert(sigesq > 0.); // which can only be paired with positive sigmas

    const double CROSSOVER1(6.);
    const double CROSSOVER2(LOGHALFMAX);
    double prob(0.);
    double erfcarg1((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
    double erfcarg2(-eosq/(SQRT2*sigesq));
    if (erfcarg1 < CROSSOVER1)
    {
      double exparg(-eosq+fn::pow2(sigesq)/2.);
      if (exparg > -CROSSOVER2)
        prob -= std::exp(exparg) * scitbx::math::erfc(erfcarg1)/2.;
    }
    else
    {
      double exparg(-fn::pow2(eosq/sigesq)/2.);
      if ((erfcarg1 > -CROSSOVER1) && (exparg > -CROSSOVER2))
        prob -= std::exp(exparg) * scitbx::math::erfcx(erfcarg1)/2.;
    }
    if (erfcarg2 < CROSSOVER1)
      prob += scitbx::math::erfc(erfcarg2)/2.;
    if (prob < 0.) prob = 0.;
    return prob;
  }

  double pIntensity::pLargerEosqAcen(double eosq, double sigesq) const
  {
    assert(sigesq >= 0.);
    const double CROSSOVER1(6.);
    const double CROSSOVER2(LOGHALFMAX);
    double prob(0.);
    if (eosq < 0.) return 1.; // Should only be called for positive intensities
    if (sigesq == 0.)
    {
      if (eosq < CROSSOVER2) // Avoid underflow
        prob = std::exp(-eosq);
    }
    else
    {
      double erfcarg1((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
      double erfcarg2(eosq/(SQRT2*sigesq));
      if (erfcarg1 < CROSSOVER1)
      {
        double exparg(-eosq+fn::pow2(sigesq)/2.);
        if (exparg > -CROSSOVER2)
          prob += std::exp(exparg) * scitbx::math::erfc(erfcarg1)/2.;
      }
      else
      {
        double exparg(-fn::pow2(eosq/sigesq)/2.);
        if ((erfcarg1 > -CROSSOVER1) && (exparg > -CROSSOVER2))
          prob += std::exp(exparg) * scitbx::math::erfcx(erfcarg1)/2.;
      }
      if (erfcarg2 < CROSSOVER1)
        prob += scitbx::math::erfc(erfcarg2)/2.;
    }
    return prob;
  }

  double pIntensity::pE2Acen_grad_hess_by_dE2(double eosq, double sigesq,
         bool do_grad, double& gradient, bool do_hess, double& hessian) const
  {
    // Return pEosqAcen and, optionally, 1st and 2nd derivatives wrt Eosq
    assert(sigesq >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(6.);
    const double CROSSOVER2(LOGHALFMAX);
    double prob;
    if (sigesq > 0.)
    {
      double exparg(fn::pow2(eosq/sigesq)/2.);
      double expterm = (exparg < CROSSOVER2) ? std::exp(-exparg) : 0.;
      double erfcarg((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
      if (erfcarg < CROSSOVER1)
      {
        prob = std::exp(-eosq + fn::pow2(sigesq)/2.) * scitbx::math::erfc(erfcarg)/2.;
        if (do_grad) gradient = expterm/(SQRT2PI*sigesq) - prob;
        if (do_hess) hessian = -expterm*eosq/(SQRT2PI*fn::pow3(sigesq)) -
                               (expterm/(SQRT2PI*sigesq) - prob);
      }
      else
      {
        if (exparg < CROSSOVER2)
        {
          prob = expterm * scitbx::math::erfcx(erfcarg)/2.;
          if (do_grad) gradient = expterm/(SQRT2PI*sigesq) - prob;
          if (do_hess) hessian = -expterm*eosq/(SQRT2PI*fn::pow3(sigesq)) -
                                 (expterm/(SQRT2PI*sigesq) - prob);
        }
        else
        {
          prob = 0.;
          if (do_grad) gradient = 0.;
          if (do_hess) hessian = 0.;
        }
      }
    }
    else
    {
      assert(eosq >= 0.); // Negative intensity not allowed with zero sigma
      prob = std::exp(-eosq);
      if (do_grad) gradient = -prob;
      if (do_hess) hessian = prob;
    }
    return prob;
  }

  double pIntensity::pIobsAcen(double iobs, double sigmaN, double sigiobs) const
  {
    // This will differ very slightly by rounding error compared to grad_hess version (computed in
    // terms of iobs and sigmaN directly), so it is probably better to only pair this with derivatives
    // also computed in terms of Eosq
    double ZERO(0.);
    if (sigmaN > 0.)
      return pEosqAcen(iobs/sigmaN,sigiobs/sigmaN)/sigmaN;
    if (sigiobs > 0) //not french_wilson
      return pNormal(iobs,ZERO,sigiobs); // Special case for systematically absent, flagged with sigmaN=0
    return 0;
  }

  double pIntensity::pIobsAcen_grad_hess_by_dSN(double iobs, double sigmaN, double sigiobs,
         bool do_grad, double& gradient, bool do_hess, double& hessian) const
  {
    //Acentric probability of Iobs plus 1st & 2nd derivatives wrt sigmaN
    assert(sigmaN > 0.);
    assert(sigiobs >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(6.);
    const double CROSSOVER2(LOGHALFMAX);
    double prob;
    double sigmaNsqr(fn::pow2(sigmaN));
    if (sigiobs > 0.)
    {
      double exparg(fn::pow2(iobs/sigiobs)/2.);
      double expterm = (exparg < CROSSOVER2) ? std::exp(-exparg) : 0.;
      double sigiobssqr(fn::pow2(sigiobs));
      double X((-iobs*sigmaN + sigiobssqr)/(SQRT2*sigiobs*sigmaN)); // arg of erfc
      if (X < CROSSOVER1) // Calculations stable for small arguments of erfc
      {
        prob = std::exp((sigiobssqr-2.*iobs*sigmaN)/(2.*sigmaNsqr)) * // see doi 10.1107/S2059798315013236 eqn 20a
               scitbx::math::erfc(X)/(2.*sigmaN);
        if (do_grad)
          gradient = (expterm*sigiobs/SQRT2PI -
                     (sigiobssqr + sigmaN*(sigmaN - iobs)) * prob) / fn::pow3(sigmaN);
        if (do_hess)
          hessian = ((fn::pow2(sigiobssqr) + sigiobssqr*sigmaN*(5.*sigmaN-2.*iobs) +
                    sigmaNsqr*(fn::pow2(iobs)-4.*iobs*sigmaN+2.*sigmaNsqr))*prob -
                    expterm*sigiobs*(sigiobssqr+sigmaN*(4.*sigmaN-iobs))/SQRT2PI) /
                    fn::pow3(sigmaNsqr);
      }
      else
      {
        if (exparg < CROSSOVER2)
        {
          // For large arguments of erfc, use erfcx to avoid underflow
          double erfcxterm = scitbx::math::erfcx(X);
          prob = expterm*erfcxterm/(2.*sigmaN);
          if (do_grad || do_hess)
          {
            // For derivatives, most methods tend to lead to catastrophic cancellation
            // Express erfcx(X) as asymptotic series (Wikipedia article on error function)
            // erfcx(X) = 1/(X Sqrt(Pi)) Sum(0,Infinity)(-1^n(2n-1)!!/(2X^2)^n)
            // Then derivatives of prob wrt sigmaN can be computed by collecting terms
            // in powers of X for the whole expression, avoiding problems of cancellation
            // Terms to order 4 are used here, which is sufficient for X as small as 6.
            double sNdX = -sigiobs/(SQRT2*sigmaN); // factor of sigmaN*dXbydsigmaN
            double iX1 =  1./X; // Make required inverse powers of X
            double iX2 = iX1/X;
            double iX3 = iX2/X;
            double iX4 = iX3/X;
            double iX5 = iX4/X;
            double iX6 = iX5/X;
            double iX7 = iX6/X;
            double iX8 = iX7/X;
            double iX9 = iX8/X;
            double iX10 = iX9/X;
            if (do_grad)
            {
              gradient = expterm/(SQRTPI*sigmaNsqr) *
                   (    -(iX1 +      sNdX*iX2)/2. +
                         (iX3 +   3.*sNdX*iX4)/4. -
                      (3.*iX5 +  15.*sNdX*iX6)/8. +
                     (15.*iX7 + 105.*sNdX*iX8)/16. -
                    (105.*iX9 + 945.*sNdX*iX10)/32. );
            }
            if (do_hess)
            {
              double iX11 = iX10/X;
              double sNdXsqr = fn::pow2(sNdX);
              double sNsqrdX2 = SQRT2*sigiobs/sigmaN; // sigmaN^2*d2XbydsigmaN2
              double twominus1 = 2.*sNdX-sNsqrdX2;
              hessian = expterm/(32.*SQRTPI*fn::pow3(sigmaN)) *
                (                  32.*iX1 +  16.*twominus1*iX2 +
                (-16. +   32.*sNdXsqr)*iX3 -  24.*twominus1*iX4 +
                ( 24. -   96.*sNdXsqr)*iX5 +  60.*twominus1*iX6 +
                (-60. +  360.*sNdXsqr)*iX7 - 210.*twominus1*iX8 +
                (210. - 1680.*sNdXsqr)*iX9 + 945.*twominus1*iX10 +
                (9450.*sNdXsqr)*iX11);
            }
          }
        }
        else // Exponential would (nearly) underflow so consider as zero
        {
          prob = 0.;
          if (do_grad) gradient = 0.;
          if (do_hess) hessian = 0.;
        }
      }
    }
    else
    {
      assert(iobs >= 0.); // Negative intensity not allowed with zero sigma
      assert(sigmaN > 0.);
      prob = std::exp(-iobs/sigmaN)/sigmaN;
      if (do_grad)
        gradient = prob*(iobs - sigmaN)/sigmaNsqr;
      if (do_hess)
        hessian = prob * (fn::pow2(iobs)-4.*iobs*sigmaN + 2.*sigmaNsqr) /
                  fn::pow2(sigmaNsqr);
    }
    return prob;
  }

  double pIntensity::pEosqCen(double eosq, double sigesq) const
  {
    assert(sigesq >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(-16.), CROSSOVER2(16.);
    double prob;
    if (sigesq > 0)
    {
      double darg(-eosq/sigesq + sigesq/2.);
      if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
      {
        double maxexpterm = LOGMAX;
        double expterm = ((-4.*eosq* (1.+eosq/fn::pow2(sigesq)) + fn::pow2(sigesq))/16.);
        if (expterm <= maxexpterm)
        {
          prob = std::exp((-4.*eosq* (1.+eosq/fn::pow2(sigesq)) + fn::pow2(sigesq))/16.)
               * tbl_pcf.get(-0.5,darg)
               / (2.*std::sqrt(scitbx::constants::pi*sigesq));
        }
        else
        {
          prob = 0; //AJM TODO what is the correct exponential overflow condition?
        }
      }
      else
      {
        if (darg < 0.) // Avoid overflow on ParabolicCylinderD with asymptotic approximation
          prob = (std::exp((fn::pow2(sigesq)-4.*eosq)/8.) /
                 std::sqrt(scitbx::constants::pi*(2.*eosq-fn::pow2(sigesq)))) *
                 wpcdmhalf_negarg(darg);
        else // Avoid underflow
          prob = (std::exp(-fn::pow2(eosq/sigesq)/2.) /
                 std::sqrt(scitbx::constants::pi*(fn::pow2(sigesq)-2.*eosq))) *
                 wpcdmhalf_posarg(darg);
      }
    }
    else // Check elsewhere for non-positive intensity with zero sigma
    {
      double eosq_pos(std::max(eosq,CUBERTEPS));
      double eo(std::sqrt(eosq_pos));
      if (eo > 1.e-6)
        prob = std::exp(-eosq_pos/2.) / (eo*SQRT2PI);
      else // Approximation good to better than 1 part in 10^12 for small eo
        prob = (1./eo - eo/2.) / SQRT2PI;
    }
    return prob;
  }

  double pIntensity::pSmallerEosqCen(double eosq, double sigesq) const
  {
    if (eosq > 0.) return 1.;   // Should only be called for negative intensities
    assert(sigesq > 0.); // which can only be paired with positive sigmas
    double prob;
    // Use numerical integration by Simpson's rule
    // 50 points is more than enough to get outlier threshold that agrees
    // to 4 sig figs for outlier probability of 10^-6
    int npts(50);
    double lower(std::min(eosq-3*sigesq,-6*sigesq));
    double dE2((eosq-lower)/(npts-1));
    prob = pEosqCen(lower,sigesq) + pEosqCen(eosq,sigesq);
    for (int i=1; i < npts-1; i++)
    {
      double wt = (i % 2) ? 2 : 4; // Alternating odd/even weights
      prob += wt*pEosqCen(lower+i*dE2,sigesq);
    }
    prob *= dE2/3;
    return prob;
  }

  double pIntensity::pLargerEosqCen(double eosq, double sigesq) const
  {
    // This calculation is sufficiently accurate for outlier detection, but
    // can be off significantly under certain circumstances for larger probabilities

    assert(sigesq >= 0.);
    if (eosq < 0.) return 1.; // Should only be called for positive intensities
    double prob(0.),erfcarg;
    double maxerfcarg(9.);
    if (sigesq > 22.) // Large measurement error Gaussian approximation
    {
      double z((eosq-1.)/std::sqrt(2.+fn::pow2(sigesq)));
      erfcarg = z/std::sqrt(2.);
      if (erfcarg < maxerfcarg) prob = scitbx::math::erfc(erfcarg)/2.;
      return prob;
    }
    erfcarg = std::sqrt(eosq/2.);
    double upper(4.+8.*sigesq);
    if (erfcarg > maxerfcarg) // Danger of underflow
      prob = 0.;
    else if (sigesq < 0.00001)
      prob = scitbx::math::erfc(erfcarg); // Sigma makes very little difference
    else
    {
      if (eosq > upper) // Good approximation to large outlier integral
        prob = scitbx::math::erfc(erfcarg) *
               pEosqCen(eosq,sigesq) / pEosqCen(eosq,0.);
      else
      {
        // Use numerical integration by Simpson's rule between eosq and
        // value where above approximation works
        // Need up to 200 points to get outlier threshold within a
        // factor of 10 for very large sigesq
        int npts(200);
        double dE2((upper-eosq)/(npts-1));
        prob = pEosqCen(eosq,sigesq) + pEosqCen(upper,sigesq);
        for (int i=1; i < npts-1; i++)
        {
          double wt = (i % 2) ? 2 : 4; // Alternating odd/even weights
          prob += wt*pEosqCen(eosq+i*dE2,sigesq);
        }
        prob *= dE2/3;
        if (std::sqrt(upper/2.) <= maxerfcarg)
          prob += scitbx::math::erfc(std::sqrt(upper/2.)) *
                  pEosqCen(upper,sigesq) / pEosqCen(upper,0.);
      }
    }
    return prob;
  }

  double pIntensity::pE2Cen_grad_hess_by_dE2(
         double eosq, double sigesq,
         bool do_grad, double& gradient, bool do_hess, double& hessian) const
  {
    assert(sigesq >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(-16.), CROSSOVER2(16.);
    double prob(0);
    if (sigesq > 0)
    {
      double darg(-eosq/sigesq + sigesq/2.);
      double wpcdmhalf(0),wpcd1half(0),wexpterm(0);
      if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
      {
        wpcdmhalf = tbl_pcf.get(-0.5,darg); // Not actually weighted for moderate arguments
        if (do_grad || do_hess)
          wpcd1half = tbl_pcf.get( 0.5,darg);
        // expterm is not actually weighted here
        wexpterm = std::exp((-4.*eosq* (1.+eosq/fn::pow2(sigesq)) + fn::pow2(sigesq))/16.);
      }
      else
      {
        if (darg < 0.) // Avoid overflow with asymptotic approximations to weighted ParabolicCylinderD
        {
          wpcdmhalf = wpcdmhalf_negarg(darg);
          if (do_grad || do_hess)
            wpcd1half = wpcd1half_negarg(darg);
          // expterm including inverse of weight from approximation to ParabolicCylinderD
          wexpterm = std::exp((fn::pow2(sigesq)-4.*eosq)/8.)/std::sqrt(-darg/2.);
        }
        else // Avoid underflow
        {
          wpcdmhalf = wpcdmhalf_posarg(darg);
          if (do_grad || do_hess)
            wpcd1half = wpcd1half_posarg(darg);
          wexpterm = std::exp(-fn::pow2(eosq/sigesq)/2.)/std::sqrt(darg/2.);
        }
      }
      prob = wexpterm*wpcdmhalf / (2.*std::sqrt(scitbx::constants::pi*sigesq));
      if (do_grad)
        gradient = wexpterm * (2.*wpcd1half-sigesq*wpcdmhalf)/(4.*std::sqrt(scitbx::constants::pi*fn::pow3(sigesq)));
      if (do_hess)
        hessian = wexpterm*(sigesq*(fn::pow2(sigesq)-2.)*wpcdmhalf - 2.*(2.*eosq+fn::pow2(sigesq))*wpcd1half) /
                  (8.*std::sqrt(scitbx::constants::pi*std::pow(sigesq,7)));
    }
    else // Check elsewhere for non-positive intensity with zero sigma
    {
      double eosq_pos(std::max(eosq,CUBERTEPS));
      double eo(std::sqrt(eosq_pos));
      if (eo > 1.e-6)
      {
        prob = std::exp(-eosq_pos/2.) / (eo*SQRT2PI);
        if (do_grad) gradient = -prob/2.;
        if (do_hess) hessian = prob/4.;
      }
      else // Approximation good to better than 1 part in 10^12 for small eo
      {
        prob = (1./eo - eo/2.) / SQRT2PI;
        if (do_grad) gradient = -1./(2.*fn::pow3(eo)*SQRT2PI);
        if (do_hess) hessian = 3./(4.*std::pow(eo,5)*SQRT2PI);
      }
    }
    return prob;
  }

  double pIntensity::pIobsCen( double iobs, double sigmaN, double sigiobs) const
  {
    // This will differ very slightly by rounding error compared to grad_hess version (computed in
    // terms of iobs and sigmaN directly), so it is probably better to only pair this with derivatives
    // also computed in terms of Eosq
    double ZERO(0.);
    if (sigmaN > 0.)
      return pEosqCen(iobs/sigmaN,sigiobs/sigmaN)/sigmaN;
    if (sigiobs > 0) //not french_wilson
      return pNormal(iobs,ZERO,sigiobs); // Special case for systematically absent, flagged with sigmaN=0
    return 0;
  }

  double pIntensity::pIobsCen_grad_hess_by_dSN(
         double iobs, double sigmaN, double sigiobs,
         bool do_grad, double& gradient, bool do_hess, double& hessian) const
  {
    assert(sigiobs >= 0.);
    // Allow for zero sigma, falling back on probability without errors
    const double CROSSOVER1(-16.), CROSSOVER2(16.);
    double prob;
    double zo(iobs/sigmaN),sigzo(sigiobs/sigmaN);
    double zosqr = fn::pow2(zo);
    double sigmaNsqr(fn::pow2(sigmaN));
    if (sigzo > 0)
    {
      double sigzosqr(fn::pow2(sigzo));
      double darg(sigzo/2. - zo/sigzo);
      if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
      {
        double pcdmhalf = tbl_pcf.get(-0.5,darg);
        // Calculate factors to apply to (linear functions of) D-functions for prob, gradient and hessian
        //double expterm = std::exp((sigzosqr-4.*(zo-fn::pow2(zo/sigzo)))/16.); // see doi 10.1107/S2059798315013236 eqn 20b
        double expterm = std::exp((fn::pow4(sigzo)-4.*(sigzosqr*zo+zosqr))/(16.*sigzosqr)); // mathematically equivalent to commented line above
        double pfactor = expterm / (2.*sigmaN*std::sqrt(scitbx::constants::pi*sigzo));
        prob = pfactor*pcdmhalf;
        if (do_grad || do_hess)
        {
          double pcd1half = tbl_pcf.get( 0.5,darg);
          if (do_grad)
          {
            gradient = expterm / (8.*std::sqrt(scitbx::constants::pi*sigzo)*sigmaNsqr) *
              (2.*sigzo*pcd1half - (sigzosqr+2.*(1.-zo))*pcdmhalf);
          }
          if (do_hess)
          {
            hessian = expterm / (32.*std::sqrt(scitbx::constants::pi*sigzo)*fn::pow3(sigmaN)) * (
              (12.+fn::pow4(sigzo)+sigzosqr*(14.-4.*zo)+4.*(zo-6.)*zo)*pcdmhalf -
              2.*sigzo * (12.+sigzosqr-2.*zo) * pcd1half );
          }
        }
      }
      else // Either large negative or large positive value of darg
      // For numerical stability, combine series approximations for derivative terms
      {
        double invdarg,invdarg2,invdarg3,invdarg4,invdarg5,invdarg6,invdarg7,invdarg8,invdarg9,invdarg10;
        if (do_grad || do_hess)
        {
          invdarg  = 1./darg;
          invdarg2 = invdarg*invdarg;
          invdarg3 = invdarg2*invdarg;
          invdarg4 = invdarg3*invdarg;
          invdarg5 = invdarg4*invdarg;
          invdarg6 = invdarg5*invdarg;
          invdarg7 = invdarg6*invdarg;
          invdarg8 = invdarg7*invdarg;
          invdarg9 = invdarg8*invdarg;
          if (do_hess) invdarg10 = invdarg9*invdarg;
        }
        if (darg < 0.) // Avoid overflow for large negative arguments of ParabolicCylinderD,
                       // using asymptotic approximations to weighted ParabolicCylinderD
        {
          double wpcdmhalf = wpcdmhalf_negarg(darg);
          // Factors including inverse of weight from approximation to ParabolicCylinderD
          double expterm = std::exp((sigzosqr-4.*zo)/8.);
          prob = expterm * wpcdmhalf /
                 (SQRTPI*sigmaN*std::sqrt(2.*zo-sigzosqr));
          if (do_grad || do_hess)
          {
            double efacn = expterm*(fn::pow2(sigzosqr-2.*zo)-4.*zo) /
              (4.*SQRTPI*sigmaNsqr*std::pow(2.*zo-sigzosqr,1.5));
            double ofacn = expterm*3.*sigzo/(8.*SQRTPI*sigmaNsqr*std::sqrt(2.*zo-sigzosqr));
            if (do_grad)
            {
              gradient = efacn
                +      (3.*efacn*invdarg2)/8.     +          invdarg3*ofacn
                +    (105.*efacn*invdarg4)/128.   +     (35.*invdarg5*ofacn)/8.
                +   (3465.*efacn*invdarg6)/1024.  +   (3465.*invdarg7*ofacn)/128.
                + (675675.*efacn*invdarg8)/32768. + (225225.*invdarg9*ofacn)/1024.;
            }
            if (do_hess)
            {
              double ddarg = -sigzo/(2.*sigmaN);
              double defacn = (expterm*
                (std::pow(sigzo,8)+std::pow(sigzo,6)*(12.-8.*zo)+
                24.*fn::pow4(sigzo)*(zo-3.)*zo+16.*sigzosqr*(9.-2.*zo)*
                zosqr+16.*zosqr*(3.+(zo-6.)*zo))) /
                (16.*SQRTPI*fn::pow3(sigmaN)*std::pow(2.*zo-sigzosqr,2.5));
              double dofacn = (3.*expterm*sigzo*(fn::pow4(sigzo)-4.*sigzosqr*(zo-2.)
                + 4.*(zo-5.)*zo)) /
                (32.*SQRTPI*fn::pow3(sigmaN)*std::pow(2.*zo-sigzosqr,1.5));
              hessian = defacn
                +       (3.*invdarg2*defacn)/8.
                +           invdarg3*(dofacn - (3.*ddarg*efacn)/4.)
                +           invdarg4*((105.*defacn)/128. - 3*ddarg*ofacn)
                +      (35.*invdarg5*   (4.*dofacn - 3.*ddarg*efacn))/32.
                +      (35.*invdarg6*  (99.*defacn - 640.*ddarg*ofacn))/1024.
                +    (3465.*invdarg7*   (4.*dofacn - 3.*ddarg*efacn))/512.
                +    (3465.*invdarg8* (195.*defacn - 1792.*ddarg*ofacn))/32768.
                +  (225225.*invdarg9*  (4.*dofacn - 3.*ddarg*efacn))/4096.
                -  (2027025.*invdarg10*ddarg*ofacn)/1024.;
            }
          }
        }
        else // Avoid underflow for large positive arguments of ParabolicCylinderD, different weights
        {
          double wpcdmhalf = wpcdmhalf_posarg(darg);
          double expterm = std::exp(-zosqr/(2.*sigzosqr));
          prob = expterm * wpcdmhalf /
                 (SQRTPI*sigmaN*std::sqrt(sigzosqr-2.*zo));
          if (do_grad || do_hess)
          {
            double efacp = expterm*zo /
              (SQRT2PI*sigmaNsqr*std::pow(sigzosqr-2.*zo,1.5));
            double ofacp = 3.*expterm*sigzo /
              (8.*SQRT2PI*sigmaNsqr*std::sqrt(sigzosqr-2.*zo));
            if (do_grad)
            {
              gradient = efacp
              -      (3.*efacp*invdarg2)/8.     -          invdarg3*ofacp
              +    (105.*efacp*invdarg4)/128.   +     (35.*invdarg5*ofacp)/8.
              -   (3465.*efacp*invdarg6)/1024.  -   (3465.*invdarg7*ofacp)/128.
              + (675675.*efacp*invdarg8)/32768. + (225225.*invdarg9*ofacp)/1024.;
            }
            if (do_hess)
            {
              double ddarg = -sigzo/(2.*sigmaN);
              double defacp = (3.*expterm*zosqr) /
                (SQRT2PI*fn::pow3(sigmaN)*std::pow(sigzosqr-2.*zo,2.5));
              double dofacp = (3.*expterm*sigzo*(5.*zo-2.*sigzosqr)) /
                (8.*SQRT2PI*fn::pow3(sigmaN)*std::pow(sigzosqr-2.*zo,1.5));
              hessian = defacp
              -       (3.*invdarg2*defacp)/8.
              -           invdarg3*(dofacp - 3.*ddarg*efacp/4.)
              +           invdarg4*(105.*defacp/128. + 3*ddarg*ofacp)
              +      (35.*invdarg5*   (4.*dofacp - 3.*ddarg*efacp))/32.
              -      (35.*invdarg6*  (99.*defacp + 640.*ddarg*ofacp))/1024.
              -    (3465.*invdarg7*   (4.*dofacp - 3.*ddarg*efacp))/512.
              +    (3465.*invdarg8* (195.*defacp + 1792.*ddarg*ofacp))/32768.
              +  (225225.*invdarg9*  (4.*dofacp - 3.*ddarg*efacp))/4096.
              -  (2027025.*invdarg10*ddarg*ofacp)/1024.;
            }
          }
        }
      }
    }
    else // Check elsewhere for non-positive intensity with zero sigma
    {
      double eosq(iobs/sigmaN);
      double eosq_pos(std::max(eosq,CUBERTEPS));
      double eo(std::sqrt(eosq_pos));
      if (eo > 1.e-6)
        prob = std::exp(-eosq_pos/2.) / (eo*sigmaN*SQRT2PI);
      else // Approximation good to better than 1 part in 10^12 for small eo
        prob = (1./eo - eo/2.) / (sigmaN*SQRT2PI);
      if (do_grad) gradient = prob*(iobs-sigmaN)/(2.*sigmaNsqr);
      if (do_hess)
        hessian = prob*(fn::pow2(iobs)-6.*iobs*sigmaN+3.*sigmaNsqr)/(4.*fn::pow2(sigmaNsqr));
    }
    return prob;
  }

  double pIntensity::pEosqTwinAcen(double eosq, double sigesq, double twinfrac) const
  {
    // Acentric likelihood function for probability of hemihedrally-twinned normalised intensity
    double f = (twinfrac <= 0.5) ? twinfrac : 1.-twinfrac;
    double fcompl(1.-f);
    assert(f >= 0.);
    double varesq(fn::pow2(sigesq));
    double prob;
    if (f < 0.001) // Should have continuous approximation if used for optimisation
      prob = pEosqAcen(eosq,sigesq);
    else if (f < 0.499)
    {
      double arg3 = (-eosq*f + varesq) / (SQRT2*f*sigesq);
      double term3 = (arg3 < 0.) ?
                     std::exp(-(2*eosq*f - varesq)/(2*fn::pow2(f)))*scitbx::math::erfc(arg3) :
                     std::exp(-fn::pow2(eosq)/(2*varesq)) * scitbx::math::erfcx(arg3);
      double erfcarg = -(eosq*fcompl - varesq)/(SQRT2*fcompl*sigesq);
      double exparg = -(2*eosq*fcompl - varesq)/(2*fn::pow2(fcompl));
      prob = (std::exp(exparg)*scitbx::math::erfc(erfcarg) - term3) / (2.-4*f);
    }
    else // Should be the quadratic approximation if used for optimisation
    {
      double diff1 = eosq - 2*varesq;
      double erfcarg = -diff1/(SQRT2*sigesq);
      prob = 2*(std::exp(-fn::pow2(eosq)/(2*varesq)) *
             SQRT2overPI*sigesq + std::exp(-2*(eosq-varesq))*diff1*scitbx::math::erfc(erfcarg));
    }
    return prob;
  }

  double pIntensity::wpcdmhalf_negarg(double darg) const
  {
    // Asymptotic approximation to wt*ParabolicCylinderD(-1/2,darg) for negative darg
    // where wt = Exp[-darg^2/4]*Sqrt[-darg/2]
    double wpcdmhalf = ( 675675. + fn::pow2(darg) *
                        (110880. + fn::pow2(darg) *
                        ( 26880. + fn::pow2(darg) *
                        ( 12288. + 32768.*fn::pow2(darg)))))
                        / (32768.*std::pow(darg,8));
    return wpcdmhalf;
  }

  double pIntensity::wpcd1half_negarg(double darg) const
  {
    // Asymptotic approximation to wt*ParabolicCylinderD(1/2,darg) for negativ darg
    // where wt = Exp[-darg^2/4]*Sqrt[-darg/2]
    double wpcd1half = (11486475. + fn::pow2(darg) *
                        (1441440. + fn::pow2(darg) *
                        ( 241920. + fn::pow2(darg) *
                        ( 61440. + 32768.*fn::pow2(darg)))))
                        / (65536.*std::pow(darg,9));
    return wpcd1half;
  }

  double pIntensity::wpcdmhalf_posarg(double darg) const
  {
    // Asymptotic approximation to wt*ParabolicCylinderD(-1/2,darg) for positive darg
    // where wt = Exp[darg^2/4]*Sqrt[darg/2]
    double wpcdmhalf = (  675675. + fn::pow2(darg) *
                        (-110880. + fn::pow2(darg) *
                        (  26880. + fn::pow2(darg) *
                        ( -12288. + 32768.*fn::pow2(darg)))))
                        / (32768.*SQRT2*std::pow(darg,8));
    return wpcdmhalf;
  }

  double pIntensity::wpcd1half_posarg(double darg) const
  {
    // Asymptotic approximation to wt*ParabolicCylinderD(1/2,darg) for positive darg
    // where wt = Exp[darg^2/4]*Sqrt[darg/2]
    double wpcd1half = ( 2297295. + fn::pow2(darg) *
                        (-360360. + fn::pow2(darg) *
                        (  80640. + fn::pow2(darg) *
                        ( -30720. + fn::pow2(darg) *
                        (  32768. + 262144.*fn::pow2(darg))))))
                        / (262144.*SQRT2*std::pow(darg,9));
    return wpcd1half;
  }

}}
