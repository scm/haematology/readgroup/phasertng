//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/ReflColsMap.h>
#include <cctbx/miller/index_span.h>
#include <phasertng/cctbx_project/cctbx/miller/expand_to_p1.h>
#include <phasertng/cctbx_project/cctbx/miller/asu.h>
#include <phasertng/io/MtzHandler.h>
#include <phasertng/pod/fast_types.h>
#include <phasertng/math/table/sim.h>
#include <phasertng/main/hoist.h>

namespace phasertng {
extern const table::sim& tbl_sim;
}

//if PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP is defined the generic get functions will
//throw informative messages about what is out of range, no just a map::at error
//#define PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
//#define PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA

namespace phasertng {

  ReflColsMap::ReflColsMap(int nrefl) : Reflections(), EntryBase()
  { set_base_defaults(); setup_miller(nrefl); }

  //python pickle constructor
  ReflColsMap::ReflColsMap(
        af_string history,
        std::string hall,
        af::double6 cell,
        double WAVELENGTH_,
        af_millnx   MILLER_,
        std::map<labin::col,af_double> FLT_,
        std::map<labin::col,af_int> INT_,
        std::map<labin::col,af_bool> BLN_,
        std::map<friedel::type,af_bool> PRESENT_
     ) : MILLER(MILLER_), EntryBase()
  {
    set_base_defaults(); //very important, sets the constant parameters of class
    SetHistory(history);
    SG = SpaceGroup(hall);
    WAVELENGTH = (WAVELENGTH_);
    UC = UnitCell(cell);
    NREFL = MILLER.size();
    set_data_resolution();
    //setup labin
    for (auto item : PRESENT_)
    {
      PRESENT[item.first] = item.second.deep_copy();
    }
    for (auto item : FLT_)
    {
      FLT[item.first] = item.second.deep_copy();
      LABIN.insert(item.first);
      FRIEDEL.insert(convert_labin_to_friedel(item.first));
    }
    for (auto item : INT_)
    {
      INT[item.first] = item.second.deep_copy();
      LABIN.insert(item.first);
      FRIEDEL.insert(convert_labin_to_friedel(item.first));
    }
    for (auto item : BLN_)
    {
      BLN[item.first] = item.second.deep_copy();
      LABIN.insert(item.first);
      FRIEDEL.insert(convert_labin_to_friedel(item.first));
    }
  }

  std::map<labin::col,af_double> ReflColsMap::get_map_flt()
  {
    std::map<labin::col,af_double> tmp;
    for (auto item : FLT)
      tmp[item.first] = item.second.deep_copy();
    return tmp;
  }
  std::map<labin::col,af_int> ReflColsMap::get_map_int()
  {
    std::map<labin::col,af_int> tmp;
    for (auto item : INT)
      tmp[item.first] = item.second.deep_copy();
    return tmp;
  }
  std::map<labin::col,af_bool> ReflColsMap::get_map_bln()
  {
    std::map<labin::col,af_bool> tmp;
    for (auto item : BLN)
      tmp[item.first] = item.second.deep_copy();
    return tmp;
  }
  std::map<friedel::type,af_bool> ReflColsMap::get_map_fdl()
  {
    std::map<friedel::type,af_bool> tmp;
    for (auto item : PRESENT)
      tmp[item.first] = item.second.deep_copy();
    return tmp;
  }

  void
  ReflColsMap::store_new_and_changed_data(const_ReflectionsPtr from)
  {
    if (!from) return;
#ifdef PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA
auto history = from->get_history();
for (auto item : history) std::cout << "From Header " << item << std::endl;
#endif
    for (auto item : from->MTZCOLTYPE) MTZCOLTYPE.insert(item);
    for (auto item : from->MTZCOLNAME) MTZCOLNAME.insert(item);
    for (auto item : from->FRIEDEL) FRIEDEL.insert(item);
    //below can change after inception, others not included are fixed
    REFLID = from->REFLID;
    REFLPREP = from->REFLPREP;
    MAP = from->MAP;
    TNCS_PRESENT = from->TNCS_PRESENT;
    TWINNED = from->TWINNED;
    ANOMALOUS = from->ANOMALOUS;
    SHARPENING = from->SHARPENING;
    WILSON_K_F = from->WILSON_K_F;
    WILSON_B_F = from->WILSON_B_F;
    TNCS_ORDER = from->TNCS_ORDER;
    TNCS_ANGLE = from->TNCS_ANGLE;
    TNCS_VECTOR = from->TNCS_VECTOR;
    ANISO_PRESENT = from->ANISO_PRESENT;
    ANISO_DIRECTION_COSINES = from->ANISO_DIRECTION_COSINES;
    TNCS_RMS = from->TNCS_RMS;
    TNCS_FS = from->TNCS_FS;
    TNCS_GFNRAD = from->TNCS_GFNRAD;
    TOTAL_SCATTERING = from->TOTAL_SCATTERING;
    Z = from->Z;
    //no need to copy miller indices also, these are fixed
    {{ //every column allowed
    for (auto col : from->LABIN)
    {
#ifdef PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA
std::cout << "Copy " << labin::col2String(col) << std::endl;
#endif
      //may already be present, in which case overwrite, otherwise create and fill
      this->setup_mtzcol(type::mtzcol(MTZCOLTYPE[col],MTZCOLNAME[col],col));
      if (this->MTZCOLTYPE.at(col) != 'H')
      {
        bool is_BLN(this->MTZCOLTYPE.at(col) == 'B');
        bool is_INT(this->MTZCOLTYPE.at(col) == 'I');
        bool is_FLT(!is_BLN and !is_INT);
        for (int r = 0; r < this->NREFL; r++)
        {
#ifdef PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA
int rmax(3);
if (r < rmax and is_FLT) std::cout << "Copy reflection #" << r << " " << this->get_flt(col,r) << " -> " << from->get_flt(col,r) << std::endl;
if (r < rmax and is_INT) std::cout << "Copy reflection #" << r << " " << this->get_int(col,r) << " -> " << from->get_int(col,r) << std::endl;
if (r < rmax and is_BLN) std::cout << "Copy reflection #" << r << " " << this->get_bln(col,r) << " -> " << from->get_bln(col,r) << std::endl;
#endif
               if (is_FLT) { this->set_flt(col,from->get_flt(col,r),r); }
          else if (is_INT) { this->set_int(col,from->get_int(col,r),r); }
          else if (is_BLN) { this->set_bln(col,from->get_bln(col,r),r); }
        }
      }
    }
    for (auto f : from->FRIEDEL ) //added during setup_mtzcol
    {
#ifdef PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA
std::cout << "Copy Friedel " << friedel::type2String(f) << std::endl;
#endif
      for (int r = 0; r < this->NREFL; r++)
      {
        this->set_present(f,from->get_present(f,r),r);
      }
    }
#ifdef PHASERTNG_DEBUG_STORE_NEW_AND_CHANGED_DATA
history = from->get_history();
for (auto item : history) std::cout << "To Header " << item << std::endl;
#endif
    }}
  }

  void
  ReflColsMap::set_base_defaults()
  {
    std::set<labin::col> disallowed = { labin::UNDEFINED,labin::H,labin::K,labin::L };
    for (int i = 0; i < labin::colCount; i++)
    { //all allowed
      labin::col col = static_cast<labin::col>(i);
      if (!disallowed.count(col))
        LABIN_ALLOWED.insert(col);
    }
  }

  void
  ReflColsMap::set_present(friedel::type f,const bool v,const int r)
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { PRESENT.at(f); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:present:friedel::type: " + friedel::type2String(f)); }
    try { PRESENT.at(f).at(r); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:present:r " + std::to_string(r)); }
#endif
    PRESENT.at(f)[r] = v;
  }

  void
  ReflColsMap::set_miller(const millnx v,const int r)
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { MILLER.at(r); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:miller:r " + std::to_string(r)); }
#endif
    MILLER[r] = v;
  }

  void
  ReflColsMap::set_flt(labin::col col,const double v,const int r)
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { FLT.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:flt:labin::col: " + labin::col2String(col)); }
    try { FLT.at(col).at(r); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:flt:r " + std::to_string(r)); }
#endif
    FLT.at(col)[r] = v;
  }

  void
  ReflColsMap::set_int(labin::col col,const int v,const int r)
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { INT.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:int:labin::col: " + labin::col2String(col)); }
    try { INT.at(col).at(r); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:int:r " + std::to_string(r)); }
#endif
    INT.at(col)[r] = v;
  }

  void
  ReflColsMap::set_bln(labin::col col,const bool v,const int r)
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { BLN.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:bln:labin::col: " + labin::col2String(col)); }
    try { BLN.at(col).at(r); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: set map::at:bln:r " + std::to_string(r)); }
#endif
    BLN.at(col)[r] = v;
  }

  const bool
  ReflColsMap::get_present(friedel::type f,const int r) const
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { PRESENT.at(f); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: get map::at:present:friedel::type: " + friedel::type2String(f)); }
#endif
    return PRESENT.at(f)[r];
  }

  const double
  ReflColsMap::get_flt(labin::col col,const int r) const
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { FLT.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: get map::at:flt:labin::col: " + labin::col2String(col)); }
#endif
    return FLT.at(col)[r];
  }

  const bool
  ReflColsMap::get_bln(labin::col col,const int r) const
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { BLN.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: get map::at:bln:labin::col: " + labin::col2String(col)); }
#endif
    return BLN.at(col)[r];
  }

  const int
  ReflColsMap::get_int(labin::col col,const int r) const
  {
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    try { INT.at(col); }
    catch(...) { throw Error(err::DEVELOPER,"ReflColsMap: get map::at:bln:labin::col: " + labin::col2String(col)); }
#endif
    return INT.at(col)[r];
  }

//--- END VIRTUAL

  void
  ReflColsMap::setup_miller(int nrefl)
  {
    NREFL = nrefl;
    MILLER.resize(NREFL);
  }

  void
  ReflColsMap::erase_from_reflection(int nrefl)
  {
    NREFL = nrefl;
    MILLER.erase(MILLER.begin()+nrefl,MILLER.end());
    for (auto item : BLN)
      item.second.erase(item.second.begin()+nrefl,item.second.end());
    for (auto item : INT)
      item.second.erase(item.second.begin()+nrefl,item.second.end());
    for (auto item : FLT)
      item.second.erase(item.second.begin()+nrefl,item.second.end());
  }

  void
  ReflColsMap::setup_mtzcol(type::mtzcol coltype)
  {
    if (NREFL == 0) throw Error(err::DEVELOPER,"Miller array size has not been set");
    CreateLabin(coltype);
    labin::col col = coltype.enumeration();
#ifdef PHASERTNG_DEBUG_MAP_AT_EXCEPTION_REFLCOLSMAP
    if (!MTZCOLTYPE.count(col)) std::cout << "debug setup_mtzcol: " << labin::col2String(col) << std::endl;
#endif
    phaser_assert(MTZCOLTYPE.count(col));
    char type = MTZCOLTYPE.at(col);
    phaser_assert(MTZCOLNAME.count(col));
    std::string name = MTZCOLNAME.at(col);
    phaser_assert(LABIN.count(col));
    bool is_BLN(type == 'B');
    bool is_INT(type == 'I');
    bool is_FLT(!is_BLN and !is_INT);
    if (is_BLN and (!BLN.count(col) or BLN[col].size() != MILLER.size()))
    {
      BLN[col] = af_bool(MILLER.size(),false);
    }
    else if (is_INT and (!INT.count(col) or INT[col].size() != MILLER.size()))
    {
      INT[col] = af_int(MILLER.size(),0);
    }
    else if (is_FLT and (!FLT.count(col) or FLT[col].size() != MILLER.size()))
    {
      FLT[col] = af_double(MILLER.size(),0.0);
    }
    //paranoia checks, if present but not resized
    if ((is_BLN and BLN[col].size() != MILLER.size()) or
        (is_INT and INT[col].size() != MILLER.size()) or
        (is_FLT and FLT[col].size() != MILLER.size()))
    {
      throw Error(err::DEVELOPER,"error in array.size==miller.size " + labin::col2String(col));
    }
    if (!LABIN.count(col))
      throw Error(err::DEVELOPER,"No labin col in mtzcol, check phil: " + labin::col2String(col));
    friedel::type f = convert_labin_to_friedel(col);
    setup_friedel(f);
  }

  void
  ReflColsMap::setup_friedel(friedel::type f)
  {
    FRIEDEL.insert(f);
    if (!PRESENT.count(f) or PRESENT[f].size() != MILLER.size())
    {
      phaser_assert(MILLER.size());
      PRESENT[f] = af_bool(MILLER.size(),true);
    }
  }

  //only allowed for column-wise data
  void
  ReflColsMap::expand_to_p1(bool change_spacegroup_to_p1)
  {
    bool FriedelFlag = false; //FriedelFlag
    //MILLER must be last!!! MILLER used unexpanded above
    for (auto f : FRIEDEL)
    {
      phaser_assert(PRESENT.count(f));
      PRESENT[f] = cctbx::miller::expand_to_p1<bool>(
          SG.group(), FriedelFlag, MILLER.const_ref(), PRESENT[f].const_ref()).data;
    }
    for (auto item : LABIN)
    {
      if (MTZCOLTYPE[item] == 'B')
      {
        phaser_assert(BLN.count(item));
        BLN[item] = cctbx::miller::expand_to_p1<bool>(
            SG.group(), FriedelFlag, MILLER.const_ref(), BLN[item].const_ref()).data;
      }
      else if (MTZCOLTYPE[item] == 'I')
      {
        phaser_assert(INT.count(item));
        INT[item] = cctbx::miller::expand_to_p1<int>(
           SG.group(), FriedelFlag, MILLER.const_ref(), INT[item].const_ref()).data;
      }
      else if (MTZCOLTYPE[item] == 'P')
      { //stored as degrees
        phaser_assert(FLT.count(item));
        FLT[item] = cctbx::miller::expand_to_p1_phases<double>(
           SG.group(), FriedelFlag, MILLER.const_ref(), FLT[item].const_ref(), true).data;
      }
      else //any other float
      {
        phaser_assert(FLT.count(item));
        FLT[item] = cctbx::miller::expand_to_p1<double>(
           SG.group(), FriedelFlag, MILLER.const_ref(), FLT[item].const_ref()).data;
      }
    }
    //now expand the miller indices
    MILLER = cctbx::miller::expand_to_p1_indices(
        SG.group(), FriedelFlag, MILLER.const_ref());
    NREFL = MILLER.size();

   //AJM ?? do we need  expand_conventional_centring_type
   // SG.group().expand_conventional_centring_type(SG.group().conventional_centring_type_symbol());
    Z *= SG.rotsym.size();
    if (change_spacegroup_to_p1)
    {
      SG = SpaceGroup("P 1");
      if (LABIN_ALLOWED.count(labin::CENT) and LABIN.count(labin::CENT))
        this->setup_cent(this->get_mtzcol(labin::CENT));
      if (LABIN_ALLOWED.count(labin::EPS) and LABIN.count(labin::EPS))
        this->setup_eps(this->get_mtzcol(labin::EPS));
      if (LABIN_ALLOWED.count(labin::PHSR) and LABIN.count(labin::PHSR))
        this->setup_phsr(this->get_mtzcol(labin::PHSR));
    }
  }

  void
  ReflColsMap::reindex(SpaceGroup NEWSG)
  {
    cctbx::sgtbx::change_of_basis_op cb_op = NEWSG.cb_op();
    bool FriedelFlag = false; //FriedelFlag
    SG = NEWSG;
    {
      cctbx::sgtbx::tr_vec const& t = cb_op.c_inv().t();
      for (int r = 0; r < NREFL; r++)
      {
        cctbx::miller::index<> const& h = MILLER[r];
        cctbx::miller::index<> hr = cb_op.apply(h);
        MILLER[r] = hr;
        for (auto item : LABIN)
        {
          bool is_bln(MTZCOLTYPE[item] == 'B');
          bool is_int(MTZCOLTYPE[item] == 'I');
          bool is_flt(!is_bln and !is_int);
          if (is_bln)
          {
          }
          else if (is_int)
          {
          }
          else if (is_flt)//any other float
          {
            if (this->MTZCOLTYPE[item] == 'P')
            {
              double value = this->get_flt(item,r);
              cctbx::miller::sym_equiv_index hr_ht(hr, h*t, t.den(), false);
              bool deg(true);
              value = hr_ht.phase_eq(value, deg);
              this->set_flt(item,value,r);
            }
          }
        }
      }
    }
  }

  void
  ReflColsMap::reduce_to_space_group(SpaceGroup NEWSG)
  {
    bool FriedelFlag = false; //FriedelFlag
    SG = NEWSG;
    if (true) //account for sysabs
    { //this is  a hack
      //for hyp1, we expand to the subgroups, but all the subgroups are P, even C 2y (x+y,x-y,z)
      //this is because there ARE systematic absences in the reflections (found below)
      //but they have not been captured in the space group centring
      //this code below effectively restores the centring to the space group
      //it does not work to add a centring operations to the space group since these are conventional direction
      //and do not concide with the sysabs reflections, and so they add another operator to the space group
      //the unit cell must be reindexed on the NEWSG not the SG below, since the reindex is with that UC
      cctbx::sgtbx::space_group_type Type = NEWSG.type();
      cctbx::sgtbx::space_group_symbols Symbol(Type.lookup_symbol());
      Symbol = cctbx::sgtbx::space_group_symbols(Symbol.hermann_mauguin() + " (x,y,z)");
      SG = SpaceGroup("Hall: " + Symbol.hall());
    }
    af::shared<bool> unique(MILLER.size());
    {
      af::shared<std::size_t> unique_list = cctbx::miller::unique_under_symmetry_selection(
             NEWSG.type(), FriedelFlag, MILLER.const_ref()); //e.g. after p1 expansion
      int rr(0);
      for (int i = 0; i < MILLER.size(); i++)
        if (i == unique_list[rr]) { unique[i] = true; rr++; }
        else {  unique[i] = false; }
      //now remove the systematically absent reflections
      //this is most important for centred space groups!
      //hyp1 case, p422 to c2
      for (int i = 0; i < MILLER.size(); i++)
        if (NEWSG.group().is_sys_absent(MILLER[i])) { unique[i] = false; }
      size_t last = 0;
      for (size_t i = 0; i < unique.size(); i++)
      { if (unique[i]) { MILLER[last++] = MILLER[i]; } }
      MILLER.erase(MILLER.begin()+last, MILLER.end());
      phaser_assert(cctbx::miller::is_unique_set_under_symmetry(
             NEWSG.type(), FriedelFlag, MILLER.const_ref()));
    }
    for (auto f : FRIEDEL)
    {
      size_t last = 0;
      for (size_t i = 0; i < unique.size(); i++)
      { if (unique[i]) { PRESENT[f][last++] = PRESENT[f][i]; } }
      PRESENT[f].erase(PRESENT[f].begin() + last, PRESENT[f].end());
    }
    for (auto item : LABIN)
    {
      bool is_bln(MTZCOLTYPE[item] == 'B');
      bool is_int(MTZCOLTYPE[item] == 'I');
      bool is_flt(!is_bln and !is_int);
      if (is_bln)
      {
        size_t last = 0;
        for (size_t i = 0; i < unique.size(); i++)
        { if (unique[i]) { BLN[item][last++] = BLN[item][i]; } }
        BLN[item].erase(BLN[item].begin() + last, BLN[item].end());
      }
      else if (is_int)
      {
        size_t last = 0;
        for (size_t i = 0; i < unique.size(); i++)
        { if (unique[i]) { INT[item][last++] = INT[item][i]; } }
        INT[item].erase(INT[item].begin() + last, INT[item].end());
      }
      else if (is_flt)//any other float
      {
        size_t last = 0;
        for (size_t i = 0; i < unique.size(); i++)
        { if (unique[i]) { FLT[item][last++] = FLT[item][i]; } }
        FLT[item].erase(FLT[item].begin() + last, FLT[item].end());
      }
    }
    //deal explicitly with the conversion of the eps in resn to the new spacegroup
    //in sgx, expand to p1 does not change the eps flags
    //change_spacegroup_to_p1 = False
    if (LABIN.count(labin::EPS) and LABIN.count(labin::RESN))
    {
      for (int r = 0; r < NREFL; r++)
      {
        double epsilon = this->work_eps(r); //this will be the new one
        double eps = this->get_flt(labin::EPS, r); //original space group, not p1 if via expand_to_p1
        double resn = this->get_flt(labin::RESN, r);
        double esn = fn::pow2(resn);
        double newresn = std::sqrt( epsilon*esn/eps);
      }
    }
    //if the space group has changed, recalculate these
    //ignored if column not available
    NREFL = MILLER.size();
    if (LABIN_ALLOWED.count(labin::CENT) and LABIN.count(labin::CENT))
      this->setup_cent(this->get_mtzcol(labin::CENT));
    if (LABIN_ALLOWED.count(labin::EPS) and LABIN.count(labin::EPS))
      this->setup_eps(this->get_mtzcol(labin::EPS));
    if (LABIN_ALLOWED.count(labin::PHSR) and LABIN.count(labin::PHSR))
      this->setup_phsr(this->get_mtzcol(labin::PHSR));
  }

  af_string
  ReflColsMap::calculate_density(
      const_ReflectionsPtr reflections,
      pod::FastPoseType& ecalcs_sigmaa_pose,
      const sv_bool& selected,
      bool tncs_modelled)
  {
    for (auto ext:{labin::FWT,labin::PHWT,labin::DELFWT,labin::DELPHWT,labin::FOM,labin::HLA,labin::HLB,labin::HLC,labin::HLD})
      PHASER_ASSERT(has_col(ext));
    //for (auto ext:{labin::FCNAT,labin::PHICNAT})
    //  phaser_assert(has_col(ext));
    //these are going to be the NAT
    af_string output;
    friedel::type f = friedel::NAT;
    setup_friedel(f); //because none of cols have friedel NAT type and so have not set this up
    phaser_assert(NREFL == reflections->NREFL);
    //reflections contains columns that are not in the density reflections
    for (int r = 0; r < reflections->NREFL; r++)
    {
      if (selected[r])
      {
        const millnx miller = reflections->get_miller(r);
        const double teps = reflections->get_flt(labin::TEPS, r);
        const bool   cent = reflections->get_bln(labin::CENT, r);
        const double resn = reflections->get_flt(labin::RESN, r);
        const double anisobeta = reflections->get_flt(labin::ANISOBETA, r);
        const double feff = reflections->get_flt(labin::FEFFNAT, r);
        const double ssqr = reflections->get_flt(labin::SSQR, r);
        double minusSsqrOn4 = -ssqr/4.0;
        const double dobssiga = ecalcs_sigmaa_pose.sigmaa[r];
        const cmplex sigaec = ecalcs_sigmaa_pose.ecalcs[r];
        double V = teps - fn::pow2(dobssiga);
        static int counter(0);
        if (V <= 0)
        {
          counter++;
          output.push_back("-------------------- " + itos(counter));
          output.push_back("ReflColsMap Debugging reflection #" + std::to_string(r));
          output.push_back("Miller=" + std::to_string(miller[0]) + " " +
                                       std::to_string(miller[1]) + " " +
                                       std::to_string(miller[2]) + " ");
          output.push_back("V="+ std::to_string(V));
          output.push_back("dobssiga="+ std::to_string(dobssiga));
          output.push_back("|sigaec|=" + std::to_string(std::abs(sigaec)));
          output.push_back("Feff="+ std::to_string(feff));
          output.push_back("Centric="+ std::string(cent ? "yes":"no"));
          output.push_back("ssqr="+ std::to_string(ssqr));
          output.push_back("reso="+ std::to_string(1/std::sqrt(ssqr)));
          output.push_back("tncs-epsfac="+ std::to_string(teps));
          output.push_back("--------------------");
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
          PHASER_ASSERT(V > 0);
#else
          continue;
#endif
        }//debug
        double E = feff/resn;
        double sigmaA_Ecalc,phase;
        std::tie(sigmaA_Ecalc,phase) = cmplex2polar_deg(sigaec);
        double X = 2.0*E*sigmaA_Ecalc/V;
        phaser_assert(X >= 0);
        double fom = cent ? std::tanh(X/2.) : tbl_sim.get(X);
        double fwt = cent ? fom*E : 2*fom*E-sigmaA_Ecalc;
        double sharpening = (std::exp(-SHARPENING*ssqr/4.));
        fwt *= sharpening;
        if (fwt >= 0)
        {
          set_flt(labin::FWT,fwt,r);
          set_flt(labin::PHWT,phase,r);
        }
        else
        {
          set_flt(labin::FWT,-fwt,r);
          set_flt(labin::PHWT,phase+180,r);
        }
        double delfwt = sharpening*(fom*E-sigmaA_Ecalc);
        if (delfwt >= 0)
        {
          set_flt(labin::DELFWT,delfwt,r);
          set_flt(labin::DELPHWT,phase,r);
        }
        else
        {
          set_flt(labin::DELFWT,-delfwt,r);
          set_flt(labin::DELPHWT,phase+180,r);
        }
        set_flt(labin::FOM,fom,r);
        double radphase(scitbx::deg_as_rad(phase));
        if (cent)
        {
          set_flt(labin::HLA,X*std::cos(radphase)/2.,r);
          set_flt(labin::HLB,X*std::sin(radphase)/2.,r);
          set_flt(labin::HLC,0,r);
          set_flt(labin::HLD,0,r);
        }
        else
        {
          set_flt(labin::HLA,X*std::cos(radphase),r);
          set_flt(labin::HLB,X*std::sin(radphase),r);
          set_flt(labin::HLC,0,r);
          set_flt(labin::HLD,0,r);
        }
// Get scale to put calculated E-values on absolute scale using isotropic component of SigmaN
        if (has_col(labin::FCNAT) and has_col(labin::PHICNAT))
        {
          double esn_iso = fn::pow2(resn)/anisobeta;
          double resn_iso = std::sqrt(esn_iso);
          double wilson_scale = WILSON_K_F*std::exp(minusSsqrOn4*WILSON_B_F)*resn_iso;
          double absFC = sigmaA_Ecalc*wilson_scale;
          set_flt(labin::FCNAT,absFC,r);
          set_flt(labin::PHICNAT,phase,r);
        }
        set_present(f,true,r);
      } //selected
      else
      {
        set_present(f,false,r);
      }
    }
    //erase the reflections not present
    int rr(0);
    for (std::size_t r = 0; r < NREFL; r++)
    {
      if (selected[r])
      {
        set_miller(get_miller(r),rr);
        for (auto item : LABIN)
        {
          auto coltype = MTZCOLTYPE.at(item);
          if (coltype == 'B')
            set_bln(item,get_bln(item,r),rr);
          else if (coltype == 'I')
            set_int(item,get_int(item,r),rr);
          else if (item != labin::H and item != labin::K and item != labin::K)
            set_flt(item,get_flt(item,r),rr);
        }
        rr++;
      }
    }
    erase_from_reflection(rr);
    return output;
  }

  af_string
  ReflColsMap::calculate_map(
      const_ReflectionsPtr reflections,
      pod::FastPoseType& ecalcs_sigmaa_pose,
      const sv_bool& selected,
      bool tncs_modelled)
  {
    for (auto ext:{labin::MAPF,labin::MAPPH,labin::SIGA,labin::FCNAT,labin::PHICNAT})
      phaser_assert(has_col(ext));
    //these are going to be the NAT
    af_string output;
    friedel::type f = friedel::NAT;
    setup_friedel(f); //because none of cols have friedel NAT type and so have not set this up
    phaser_assert(NREFL == reflections->NREFL);
    //reflections contains columns that are not in the density reflections
    for (int r = 0; r < reflections->NREFL; r++)
    {
      if (selected[r])
      {
        const millnx miller = reflections->get_miller(r);
        const double teps = reflections->get_flt(labin::TEPS, r);
        const bool   cent = reflections->get_bln(labin::CENT, r);
        const double resn = reflections->get_flt(labin::RESN, r);
        const double anisobeta = reflections->get_flt(labin::ANISOBETA, r);
        const double feff = reflections->get_flt(labin::FEFFNAT, r);
        const double ssqr = reflections->get_flt(labin::SSQR, r);
        double minusSsqrOn4 = -ssqr/4.0;
        const double dobssiga = ecalcs_sigmaa_pose.sigmaa[r];
        const cmplex sigaec = ecalcs_sigmaa_pose.ecalcs[r];
        double V = teps - fn::pow2(dobssiga);
        static int counter(0);
        if (V <= 0)
        {
          counter++;
          output.push_back("-------------------- " + itos(counter));
          output.push_back("ReflColsMap Debugging reflection #" + std::to_string(r));
          output.push_back("Miller=" + std::to_string(miller[0]) + " " +
                                       std::to_string(miller[1]) + " " +
                                       std::to_string(miller[2]) + " ");
          output.push_back("V="+ std::to_string(V));
          output.push_back("dobssiga="+ std::to_string(dobssiga));
          output.push_back("|sigaec|=" + std::to_string(std::abs(sigaec)));
          output.push_back("Feff="+ std::to_string(feff));
          output.push_back("Centric="+ std::string(cent ? "yes":"no"));
          output.push_back("ssqr="+ std::to_string(ssqr));
          output.push_back("reso="+ std::to_string(1/std::sqrt(ssqr)));
          output.push_back("tncs-epsfac="+ std::to_string(teps));
          output.push_back("--------------------");
#ifdef DEBUG_PHASERTNG_STOP_WITH_NEGVAR
          PHASER_ASSERT(V > 0);
#else
          continue;
#endif
        }//debug
        double E = feff/resn;
        double sigmaA_Ecalc,phase;
        std::tie(sigmaA_Ecalc,phase) = cmplex2polar_deg(sigaec);
        set_flt(labin::SIGA,dobssiga,r);
        set_flt(labin::FCNAT,sigmaA_Ecalc,r);
        set_flt(labin::PHICNAT,phase,r);
        double X = 2.0*E*sigmaA_Ecalc/V;
        phaser_assert(X >= 0);
        double fom = cent ? std::tanh(X/2.) : tbl_sim.get(X);
        double fwt = cent ? fom*E : 2*fom*E-sigmaA_Ecalc;
        double sharpening = (std::exp(-SHARPENING*ssqr/4.));
        fwt *= sharpening;
        if (fwt >= 0)
        {
          set_flt(labin::MAPF,fwt,r);
          set_flt(labin::MAPPH,phase,r);
        }
        else
        {
          set_flt(labin::MAPF,-fwt,r);
          set_flt(labin::MAPPH,phase+180,r);
        }
        double radphase(scitbx::deg_as_rad(phase));
// Get scale to put calculated E-values on absolute scale using isotropic component of SigmaN
        if (has_col(labin::FCNAT) and has_col(labin::PHICNAT))
        {
          double esn_iso = fn::pow2(resn)/anisobeta;
          double resn_iso = std::sqrt(esn_iso);
          double wilson_scale = WILSON_K_F*std::exp(minusSsqrOn4*WILSON_B_F)*resn_iso;
          double absFC = sigmaA_Ecalc*wilson_scale;
          set_flt(labin::FCNAT,absFC,r);
          set_flt(labin::PHICNAT,phase,r);
        }
        set_present(f,true,r);
      } //selected
      else
      {
        set_present(f,false,r);
      }
    }
    return output;
  }

  double
  ReflColsMap::calculate_rfactor(
      const_ReflectionsPtr reflections,
      pod::FastPoseType& ecalcs_sigmaa_pose,
      const sv_bool& selected,
      double resolution)
  {
    double totalR_numerator(0);
    double totalR_denominator(0);
    for (int r = 0; r < reflections->NREFL; r++)
    {
      double sigmaA_Ecalc;
      if (selected[r])
      {
        const double ssqr = reflections->get_flt(labin::SSQR, r);
        const double reso = 1/std::sqrt(ssqr);
        if (reso < resolution) continue;
        if (reso > 10.) continue; //because there is no modelling of bulk solvent
        const double resn = reflections->get_flt(labin::RESN, r);
        const double anisobeta = reflections->get_flt(labin::ANISOBETA, r);
        const double feff = reflections->get_flt(labin::FEFFNAT, r);
        const double dobs = reflections->get_flt(labin::DOBSNAT, r);
        double minusSsqrOn4 = -ssqr/4.0;
        const double dobssiga = ecalcs_sigmaa_pose.sigmaa[r];
        const cmplex sigaec = ecalcs_sigmaa_pose.ecalcs[r];
        std::tie(sigmaA_Ecalc,std::ignore) = cmplex2polar_deg(sigaec);
        double Ecalc = sigmaA_Ecalc/dobssiga*dobs;
        double esn_iso = fn::pow2(resn)/anisobeta;
        double resn_iso = std::sqrt(esn_iso);
       //double wilson_scale = WILSON_K_F*std::exp(minusSsqrOn4*WILSON_B_F)*resn_iso;
       //feff is on absolute scale already
        double wilson_scale = std::exp(minusSsqrOn4*WILSON_B_F)*resn_iso;
        double absFC = Ecalc*wilson_scale;
        totalR_numerator += std::fabs(absFC-feff);
        totalR_denominator += feff;
      } //selected
    } // r loop
    double rfactor = (totalR_denominator > 0.) ? 100*totalR_numerator/totalR_denominator : 100;
    return rfactor;
  }

  af_string
  ReflColsMap::VanillaReadMtz(
      std::set<labin::col> COLIN
      )
  {
    af_string output;
    std::string HKLIN = FileSystem::fstr();
    if (!HKLIN.size())
    throw Error(err::INPUT,"No reflection file for reading");

    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    bool phasertngheader = false;
    for (auto line : mtzIn.HISTORY)
    {
      if (line.find(constants::remark_tag) != std::string::npos)
        phasertngheader = true;
    }
    if (COLIN.size() or phasertngheader)
    {
      Header::clear();
     //we are reading something in phasertng header format and so will populate again
     //else we rely on the set header to tell us what to read
      ReflColsMap::set_base_defaults(); //very important, sets the constant parameters of class
      Header::SetHistory(mtzIn.HISTORY);
    }
    else
    {
      ReflColsMap::set_base_defaults(); //very important, sets the constant parameters of class
    }
    SG = mtzIn.SG;
    NREFL = mtzIn.NREFL;
    setup_miller(mtzIn.NREFL);

    LABIN.clear();
    { //LABIN
      //select columns for reading
      //if nothing is specified, read the whole lot
      PHASER_ASSERT(MTZCOLNAME.size());
      for (auto item : MTZCOLNAME)
      {
        labin::col col = static_cast<labin::col>(item.first);
        std::string name = item.second;
        PHASER_ASSERT(MTZCOLTYPE.count(col));
        char type = MTZCOLTYPE[col];
        if (LABIN_ALLOWED.count(col) and
           (COLIN.size() == 0 or COLIN.count(col)))
        {
          //MTZCOLNAME&MTZCOLTYPE already setup but doesn't matter, maps
          this->setup_mtzcol(type::mtzcol(type,name,col));
        }
      }
      if (!LABIN.size())
      throw Error(err::FATAL,"No readable data in mtz file");
    } //LABIN

    std::string CELLCOL = "";
    { //CELLCOL
      //here, select the unitcell and wavelength of ANOMALOUS data if present
      //otherwise use the native data
      //order is Ipos, Fpos, I, F, Fmap
      if (LABIN.find(labin::IPOS) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::IPOS];
      else if (LABIN.find(labin::FPOS) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::FPOS];
      if (LABIN.find(labin::INAT) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::INAT];
      else if (LABIN.find(labin::FNAT) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::FNAT];
      else if (LABIN.find(labin::MAPF) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::MAPF];
      else if (LABIN.find(labin::FEFFNAT) != LABIN.end())
        CELLCOL = MTZCOLNAME[labin::FEFFNAT]; //cross rotation function: nacelle output
      if (CELLCOL == "")
        throw Error(err::FATAL,"No reference data for unit cell");
      //not doing anything with isomorphous at the moment
    } //CELLCOL

    mtzIn.load_dataset(CELLCOL);
    UC = mtzIn.UC;
    WAVELENGTH = mtzIn.WAVELENGTH;

    //so that setup_data_present_init sets array sizes
    if (!REFLID.is_set_valid())
    throw Error(err::FATAL,"Input mtz file has not been prepared with phasertng");

    { //DATA
      int ncols = 3 + LABIN.size(); //HKL + data
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      int n(0);
      std::map<labin::col,int> column; //all reflections. not just the set ones
      sv_string hkl = {"H","K","L"};
      for (int i = 0; i < hkl.size(); i++)
      {
        lookup[n] = mtzIn.getCol(hkl[i]);
        column[labin::col2Enum(hkl[i])] = n; //these are special, will need for knowing which to read
        n++;
      }
      //The LABIN has to not only be allowed but also be present in the mtz file
      std::set<labin::col> LABIN_IN_MTZ;
      for (auto labin_col : LABIN)
      {
        PHASER_ASSERT(MTZCOLNAME.count(labin_col));
        try {
          lookup[n] = mtzIn.getCol(MTZCOLNAME[labin_col]);
          column[labin_col] = n; //these are special, will need for knowing which to read
          LABIN_IN_MTZ.insert(labin_col);
          n++;
        }
        catch(...)
        { output.push_back("Allowed column not in mtz: " + labin::col2String(labin_col)); }
      }
      LABIN = LABIN_IN_MTZ;
      struct labindatstruct {
        labin::col   labin_col;
        bool is_bln;
        bool is_int;
        bool is_flt;
        int  n;
        friedel::type f;
        af_bool col_bln;
        af_int col_int;
        af_double col_flt;
      };
      std::vector<labindatstruct> labindat;
      for (auto labin_col : LABIN)
      {
        labindatstruct dat;
        dat.labin_col = labin_col;
        PHASER_ASSERT(column.find(labin_col) != column.end());
        dat.is_bln = (MTZCOLTYPE[labin_col] == 'B');
        dat.is_int = (MTZCOLTYPE[labin_col] == 'I');
        dat.is_flt = (!dat.is_bln and !dat.is_int);
        dat.n = column[labin_col];
        dat.f = convert_labin_to_friedel(labin_col);
        if (dat.is_bln) dat.col_bln = BLN.at(labin_col);
        if (dat.is_int) dat.col_int = INT.at(labin_col);
        if (dat.is_flt) dat.col_flt = FLT.at(labin_col);
        labindat.push_back(dat);
      }
      //int respos = mtzIn.resetRefl();
      double all_data_lores = 0;
      double all_data_hires = std::numeric_limits<double>::max();
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H(adata[column[labin::H]]);
        int K(adata[column[labin::K]]);
        int L(adata[column[labin::L]]);
        millnx hkl(H,K,L);
        set_miller(hkl,mtzr);
        double resolution = UC.cctbxUC.d(hkl);
        if (resolution > all_data_lores) all_data_lores = resolution;
        if (resolution < all_data_hires) all_data_hires = resolution;
        for (auto dat : labindat)
        {
          bool ispresent(!mtz_isnan(adata[dat.n]));
          set_present(dat.f,ispresent,mtzr);
          if (dat.is_bln)
            dat.col_bln[mtzr] = (ispresent?static_cast<bool>(adata[dat.n]):false);
          else if (dat.is_int)
            dat.col_int[mtzr] = (ispresent?static_cast<int>(adata[dat.n]):0);
          else if (dat.is_flt)
            dat.col_flt[mtzr] = (ispresent?static_cast<double>(adata[dat.n]):0.);
        }
      }
      RESOLUTION[0] = all_data_lores;
      RESOLUTION[1] = all_data_hires;
    } //DATA
    return output;
  }

  void
  ReflColsMap::VanillaWriteMtz(
   // double CELL_SCALE = 1 //AJM cell scale!!
      af_bool selected,
      std::string selection
    )
  {
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,WAVELENGTH,NREFL,get_history());
    PHASER_ASSERT(LABIN.size());
    if (selected.size() == 0)
      selected.resize(NREFL,true);
    PHASER_ASSERT(NREFL == selected.size());

    for (int r = 0; r < NREFL; r++)
    {
      mtzOut.mtzH->ref[r] = get_miller(r)[0];
      mtzOut.mtzK->ref[r] = get_miller(r)[1];
      mtzOut.mtzL->ref[r] = get_miller(r)[2];
    } //miller

    //DATA
    auto minimal = LABIN;
    minimal.erase(labin::H);
    minimal.erase(labin::K);
    minimal.erase(labin::L);
    if (selection.size()) //this is a really broad valid-sigi
    {
      if (!INTENSITIES) minimal.erase(labin::INAT);
      if (!INTENSITIES) minimal.erase(labin::SIGINAT);
      minimal.erase(labin::TBIN);
    }
    for (auto item : minimal)
    {
      // NEW COLUMNS
      CMtz::MTZCOL* mtzCol = mtzOut.addCol(MTZCOLNAME.at(item), MTZCOLTYPE.at(item));
      assert(mtzCol != 0);

      friedel::type f = convert_labin_to_friedel(item);
      auto& fcol = PRESENT[f];

      //for speed, make type selection first, then loop over reflections and get_present
      auto coltype = MTZCOLTYPE.at(item);
      if (coltype == 'P')
      {
        auto& datcol = FLT[item];
        for (int r = 0; r < NREFL; r++)
        {
          if (fcol[r] and selected[r])
          { //put phases in degrees in range -180->180
            double phase = datcol[r];
            while (phase > 180) phase -= 360;
            while (phase <= -180) phase += 360;
            mtzCol->ref[r] = phase;
          } //not set remains as nan
        }
      }
      else if (coltype == 'B')
      {
        auto& datcol = BLN[item];
        for (int r = 0; r < NREFL; r++)
        {
          if (fcol[r] and selected[r])
          {
            mtzCol->ref[r] = datcol[r] ? 1.0 : 0.0;
          } //not set remains as nan
        }
      }
      else if (coltype == 'I')
      {
        auto& datcol = INT[item];
        for (int r = 0; r < NREFL; r++)
        {
          if (fcol[r] and selected[r])
          {
            mtzCol->ref[r] = static_cast<double>(datcol[r]);
          } //not set remains as nan
        }
      }
      else
      {
        auto& datcol = FLT[item];
        for (int r = 0; r < NREFL; r++)
        {
          if (fcol[r] and selected[r])
          {
            mtzCol->ref[r] = datcol[r];
          } //not set remains as nan
        }
      }
    }

    mtzOut.Put("Phasertng Reflections");
  }

  void
  ReflColsMap::tiny_write_to_disk(sv_bool tiny_selected)
  {
    if (NREFL != tiny_selected.size())
      tiny_selected = sv_bool(NREFL,true);
    int nrefl = 0;
    for (int r = 0; r < NREFL; r++)
      if (tiny_selected[r])
        nrefl++;
    //replace all the lines of the header that are wrong
    af_string newhistory;
    auto oldhistory = get_history();
    for (auto line : oldhistory)
      if (!hoist::algorithm::contains(line,remark_labin) and
          !hoist::algorithm::contains(line,remark_asu) and
          !hoist::algorithm::contains(line,remark_preparation))
        newhistory.push_back(line);
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,WAVELENGTH,nrefl,newhistory);
    //apply anisoterm correction
    //delete reflections that have no information content, or are outliers
    int rr(0);
    for (int r = 0; r < NREFL; r++)
    {
      if (tiny_selected[r])
      {
        mtzOut.mtzH->ref[rr] = get_miller(r)[0];
        mtzOut.mtzK->ref[rr] = get_miller(r)[1];
        mtzOut.mtzL->ref[rr] = get_miller(r)[2];
        rr++;
      }
    } //miller

    //DATA
    std::set<labin::col> minimal;
    minimal.insert(INTENSITIES ? labin::INAT : labin::FNAT);
    minimal.insert(INTENSITIES ? labin::SIGINAT : labin::SIGFNAT);
    minimal.insert(labin::FREE);
    //add free r flags here
    for (auto item : minimal)
    {
      if (minimal.count(item) == 0) continue;
      auto coltype = MTZCOLTYPE.at(item);
      auto colname = MTZCOLNAME.at(item);
      CMtz::MTZCOL* mtzCol = mtzOut.addCol(colname, coltype);
      assert(mtzCol != 0);
      friedel::type f = convert_labin_to_friedel(item);
      auto& fcol = PRESENT[f];
      if (coltype == 'I') // freer
      {
        auto& datcol = INT[item];
        int rr(0);
        for (int r = 0; r < NREFL; r++)
        {
          if (tiny_selected[r])
          {
            if (fcol[r])
            {
              mtzCol->ref[rr] = static_cast<double>(datcol[r]);
            } //not set remains as nan
            rr++;
          }
        }
      }
      else
      {
        auto& datcol = FLT[item];
        int rr(0);
        for (int r = 0; r < NREFL; r++)
        {
          if (tiny_selected[r])
          {
            double anisobeta = get_flt(labin::ANISOBETA,r); //does not include wilson B
            double anisoTerm = 1;
            if (item == labin::INAT or item == labin::SIGINAT)
              anisoTerm = 1./anisobeta;
            else if (item == labin::FNAT or item == labin::SIGFNAT)
              anisoTerm = std::sqrt(1./anisobeta);
            if (fcol[r])
            {
              mtzCol->ref[rr] = datcol[r]*anisoTerm;
            } //not set remains as nan
            rr++;
          }
        }
      }
    }
    mtzOut.Put("Phasertng Reflections");
  }

  void
  ReflColsMap::VanillaWriteMtzOriginal()
  {
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,WAVELENGTH,NREFL,get_history());
    assert(LABIN.size());
    for (int r = 0; r < NREFL; r++)
    {
      mtzOut.mtzH->ref[r] = get_miller(r)[0];
      mtzOut.mtzK->ref[r] = get_miller(r)[1];
      mtzOut.mtzL->ref[r] = get_miller(r)[2];
    } //miller

    for (auto item : LABIN)
    {
      bool write_col = false;
      friedel::type f = convert_labin_to_friedel(item);
      if (f == friedel::NAT and ANOMALOUS) continue;
      if (f == friedel::POS and !ANOMALOUS) continue;
      if (f == friedel::NEG and !ANOMALOUS) continue;
      if (this->MAP)
      {
        write_col = (item == labin::MAPF or item == labin::MAPPH);
      }
      else
      {
        write_col = this->INTENSITIES ?
            (item == labin::INAT or item == labin::SIGINAT):
            (item == labin::FNAT or item == labin::SIGFNAT);
        if (!write_col)
          write_col = this->INTENSITIES ?
            (item == labin::IPOS or item == labin::SIGIPOS):
            (item == labin::FPOS or item == labin::SIGFPOS);
        if (!write_col)
          write_col = this->INTENSITIES ?
            (item == labin::INEG or item == labin::SIGINEG):
            (item == labin::FNEG or item == labin::SIGFNEG);
      }
      if (write_col)
      {
        CMtz::MTZCOL* mtzCol = mtzOut.addCol(MTZCOLNAME.at(item), MTZCOLTYPE.at(item));
        assert(mtzCol != 0);
        for (int r = 0; r < NREFL; r++)
          if (get_present(f,r))
            mtzCol->ref[r] = get_flt(item,r);
      }
    }
    mtzOut.Put("Phasertng Reflections");
  }
} //phasertng
