//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
    Variance& variance = search->VARIANCE;
    variance.REFLID = REFLECTIONS->reflid();
    if (Suite::Extra())
      logTabArray(out::TESTING,search->FCALCS.BIN.logBin("Fcalcs"));
    variance.SetModelsFcalc(search->FCALCS); //deep copy constructor

    //bulk solvent terms are not added to map here
    //only the siga correction terms are used

    //resample the structure factors onto minimal cell
    {{
    logChevron(out::LOGFILE,"Shannon Sampling: " + entry::data2String(entry::VARIANCE_MTZ));
    logEllipsisOpen(out::LOGFILE,"Starting Interpolation for Variance");
    dvect3 io_minbox = input.value__flt_vector.at(".phasertng.model_as_map.extent_minimum.").value_or_default();
    dvect3 io_maxbox = input.value__flt_vector.at(".phasertng.model_as_map.extent_maximum.").value_or_default();
    dvect3 range = io_maxbox - io_minbox;
    logTab(out::VERBOSE,"Range " + dvtos(range));
    logTab(out::VERBOSE,"Hires " + dtos(search->HIRES) + " " + dtos(search->FCALCS.BIN.hires()));
    double vboxscale(2);
    {{
    af_cmplex ecalc;
#ifdef PHASERTNG_DEBUG_PHASER_MAPS
    variance.UC = search->FCALCS.UC;
    variance.MILLER = search->FCALCS.MILLER;
    variance.initialize_ecalc(0,search->FCALCS.fcalc(0)); //the binning is for the siga
#else
    variance.UC = UnitCell(range);
    variance.UC.build_variance_unit_cell(vboxscale,search->FCALCS.BIN.hires());
    std::tie(variance.MILLER,ecalc) = search->FCALCS.interpolate(0,
         variance.UC,
         input.value__flt_number.at(".phasertng.model_as_map.solvent_constant.").value_or_default(),
         io_minbox,
         io_maxbox,
         input.value__flt_paired.at(".phasertng.cell_scale.").value_or_default(),
         true);
    variance.initialize_ecalc(0,ecalc); //the binning is for the siga
#endif
    }}
    variance.setup_bins_with_statistically_weighted_bin_width(
        variance.UC.GetBox(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_min(),
        input.value__int_paired.at(".phasertng.bins.maps.range.").value_or_default_max(),
        input.value__int_number.at(".phasertng.bins.maps.width.").value_or_default()
        ); //generate new binning with variance cell
    //reset hires to that of FCALCS
    //this is required to prevent chinese whispers
    variance.BIN.set_hires(search->FCALCS.BIN.hires());
    logTabArray(out::VERBOSE,variance.BIN.logBin("variance"));
    logEllipsisShut(out::LOGFILE);
    auto SigmaP = variance.sigmap_with_binning();
    variance.apply_sigmap(SigmaP); //convert to evalues
    }}

    logHistogram(out::LOGFILE,variance.density({1,1},false),20,true,"Variance Density");

    //interpolation copied at the end
    logUnderLine(out::VERBOSE,"Model Weights Calculation");
    {
      double SIGA_RMSD = 0;
      FourParameterSigmaA solTerm(
          SIGA_RMSD,
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model_as_map.fsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model_as_map.bsol.").value_or_default(),
          input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model_as_map.babinet_minimum.").value_or_default());
      solTerm.set_rmsd(search->MODELS.SERIAL.vrms_start[0]);

      logEllipsisOpen(out::VERBOSE,"Calculate weights");
      sv_double Covariance(variance.BIN.numbins());
      variance.MODELWT.resize(variance.BIN.numbins(),1);
      for (int ibin = 0; ibin < variance.BIN.numbins(); ibin++)
      {
        variance.MODELWT(ibin,0) = 1; //only one map, and it is correlated with itself
        double midSsqr = 1/fn::pow2(variance.BIN.MidRes(ibin));
        double SolventSiga = solTerm.get(midSsqr);
        double DLuz = SolventSiga;
        if (std::fabs(SolventSiga) < DEF_PPM)
        throw Error(err::FATAL,"FourParameterSigmaA essentially zero");
        Covariance[ibin] = variance.MODELWT(ibin,0)*DLuz;
        PHASER_ASSERT(Covariance[ibin] > 0);
      }
      logEllipsisShut(out::VERBOSE);

      cmplex two_pii(cmplex(0,2.)*scitbx::constants::pi);
      variance.SIGMAA.resize(variance.BIN.numbins());
      // --- OK, finally happy with the weights
      logEllipsisOpen(out::VERBOSE,"Calculate E and Siga");
      for (int r = 0; r < variance.MILLER.size(); r++)
      {
        int ibin = variance.BIN.RBIN[r];
        cmplex WeightedE = variance.MODELWT(ibin,0)*variance.ecalc(0,r); //i.e. 1*ecalc
        double Siga = std::sqrt(Covariance[ibin]);
        variance.ecalc(0,r) = WeightedE; //i.e. ecalc
        variance.SIGMAA[ibin] = Siga;
      }
      logEllipsisShut(out::VERBOSE);
      logBlank(out::VERBOSE);
    }

    if (Suite::Extra())
      logTabArray(out::TESTING,variance.BIN.logBin("Variance"));


    for (int i = 0; i < DAGDB.NODES.size(); i++)
    {
      //add the models info to each dag entry
      dag::Hold hold;
      hold.IDENTIFIER = search->identify();
      hold.ENTRY = search;
      DAGDB.NODES[i].HOLD.push_back(hold);
      const auto& modlid = search->identify();
      DAGDB.NODES[i].DRMS_SHIFT[modlid] = dag::node_t(modlid,0.); //create
      DAGDB.NODES[i].VRMS_VALUE[modlid] = dag::node_t(modlid,search->VRMS_START[0]); //create
      DAGDB.NODES[i].CELL_SCALE[modlid] = dag::node_t(modlid,1.); //create
    }
