//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/string.h>
#include <algorithm>
#include <phasertng/main/hoist.h>
#include <cctype> //Windows toupper

namespace phasertng {
namespace type {

  string::string(const std::string parameter,
         const std::string dephalt_
        ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  { }

  void
  string::set_value(std::string other)
  {
    user = true;
    if (!other.size())
    { //None() reset the keyword as the default
      uvalue = "";
      user = false;
      return;
    }
//stored unquoted - filename cannot start with quote
    if (other[0] != '"')
    {
      uvalue = other;
    }
    else
    {
      std::string unquoted = other;
      while (unquoted.size() && unquoted[0] == '"')
      {
        std::string tmp = "";
        for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
          tmp += std::string(1,unquoted[i]);
        unquoted = tmp;
      }
      //phaser_assert(!unquoted.size() || unquoted[0] != '"');
      uvalue = unquoted;
    }
  }

  void
  string::set_value(const sv_string& vcards,const std::string& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  string::is_default() const
  { return (uvalue == dephalt or uvalue == None()); }

  void
  string::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  string::to_python()
  {
    std::string v = value_or_default();
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    //stored unquoted
    hoist::replace_all(vv,"\"","\\\"");
    return vv; //unquoted
  }

  void
  string::from_python(std::string str)
  { set_value(str); }

  std::string
  string::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  string::value_or_default_upper() const
  {
    std::string s = value_or_default();
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
    return s;
  }

  std::string
  string::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  //if the string has a space, unparse with quotes around it
  //if the string has a quote, esape the quote, and unparse with quotes around it
  //phil files:  quotes in the string eg Hall = P 3 2" cause chaos
  //  when quotes are added around that also i.e.  hall = " P 3 2" "
  //  unless the quote is escaped i.e.  hall = " P 3 2\" "
  //  phil is happy with  hall = P 3 2"
  //  and stores the string as P 3 2\"
  //CCP4base::get_qstr requires the string to be quoted (not optional)
  //  when unparsing or parsing ccp4 cards
  //  and accepts escaped quote and removes escape char
  //  defined in phil as qstr type, although it does not need quoting

  std::string
  string::unparse_parameter(bool phil) const
  {
    std::string v = value_or_default();
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    //stored unquoted
    hoist::replace_all(vv,"\"","\\\"");
    std::string str = "\"" + vv + "\"";
    return str;
  }

  std::string
  string::show_default(bool phil) const
  {
    std::string v = dephalt;
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    hoist::replace_all(vv,"\"","\\\"");
    return "\"" + vv + "\"";
  }
}}
