//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>

namespace phasertng {

  //Pdb_file_reader
  void Phasertng::runIPDB()
  {
    if (DAGDB.size() >= 2)
    throw Error(err::INPUT,"Dag must contain no nodes");

    logUnderLine(out::LOGFILE,"Data Preparation from Coordinates");

    Models models;
    {{
    out::stream where = out::LOGFILE;
    if (input.value__path.at(".phasertng.pdbin.filename.").is_default())
      throw Error(err::FILESET,"pdbin filename");
    models.SetFileSystem( input.value__path.at(".phasertng.pdbin.filename.").value_or_default());
    logTab(where,"Model read from file: " + models.qfstrq());
    models.read_from_disk(); ///ReadPdb !!!
    if (models.has_read_errors())
    throw Error(err::FATAL,models.read_errors());
    PHASER_ASSERT(models.PDB.size() == 1);
    }}

    models.SERIAL.resize(1); //also sets

    models.set_principal_statistics();
    {{
    out::stream where = out::VERBOSE;
    logTab(where,"Model fixed at original coordinates");
    logTab(where,"Centre: " + dvtos(models.statistics.CENTRE));
    logTab(where,"Extent: " + dvtos(models.statistics.EXTENT));
    }}

    if (input.value__boolean.at(".phasertng.pdbin.convert_rmsd_to_bfac.").value_or_default())
    {
      out::stream where = out::LOGFILE;
      models.convert_rmsd_to_bfac();
      logTab(where,"Rmsd converted to B-factors");
    }

    //calculate structure factors
    {{
    ModelsFcalc fcalcs;
   // fcalcs.MODLID = models.MODLID;
    fcalcs.TIMESTAMP = TimeStamp();
    fcalcs.NMODELS = 1;
    if (input.value__flt_number.at(".phasertng.resolution.").is_default() and
        input.value__flt_number.at(".phasertng.reflections.resolution.").is_default())
      throw Error(err::INPUT,"Reflection resolution not set");
    double config_hires = input.value__flt_number.at(".phasertng.reflections.resolution.").is_default() ?
        input.value__flt_number.at(".phasertng.resolution.").value_or_default() :
        input.value__flt_number.at(".phasertng.reflections.resolution.").value_or_default();

    {{
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Structure Factor Calculation");
      dvect3 extent = models.statistics.EXTENT;
      logTab(where,"Extent: " + dvtos(extent));
      auto use = input.value__boolean.at(".phasertng.pdbin.bulk_solvent.use.").value_or_default();
      auto fsol = input.value__flt_number.at(".phasertng.pdbin.bulk_solvent.fsol.").value_or_default();
      auto bsol = input.value__flt_number.at(".phasertng.pdbin.bulk_solvent.bsol.").value_or_default();
      logTab(where,"Use Bulk: " + btos(use));
      logTab(where,"Fsol: " + dtos(fsol));
      logTab(where,"Bsol: " + dtos(bsol));
      UnitCell box(extent);
      auto str = box.build_minimum_unit_cell(config_hires);
      //auto str = box.build_variance_unit_cell(2,config_hires);
      logTabArray(where,str);
      fcalcs.p1_structure_factor_calculation(
          &models,
          box,
          config_hires,
          NTHREADS,
          use,fsol,bsol
          );
      phaser_assert(fcalcs.FCALC.num_rows() == models.PDB.size());
      logTab(where,"Non-anomalous data have been calculated");
      logTab(where,"Data calculated as amplitudes");
    }}

    {
      std::string Flabel = "PDBF";
      std::string Plabel = "PDBPH";
      input.value__mtzcol.at(".phasertng.labin.mapf.").set_value(Flabel);
      input.value__mtzcol.at(".phasertng.labin.mapph.").set_value(Plabel);
    }

    REFLECTIONS->UC = fcalcs.UC;
    REFLECTIONS->WAVELENGTH = DEF_WAVELENGTH;
    REFLECTIONS->OVERSAMPLING = 1;
    REFLECTIONS->MAPORI = {0,0,0};
    REFLECTIONS->SG = SpaceGroup("Hall: P 1 (x,y,z)");
    int NREFL(0);
    for (int r = 0; r < fcalcs.FCALC.num_cols(); r++)
      if (fcalcs.MILLER[r] != millnx(0,0,0))
        NREFL++;
    input.value__int_number.at(".phasertng.data.number_of_reflections.").set_value(NREFL);
    REFLECTIONS->setup_miller(NREFL);
    {
      REFLECTIONS->setup_mtzcol(input.labin_mtzcol(labin::MAPF));
      REFLECTIONS->setup_mtzcol(input.labin_mtzcol(labin::MAPPH));
    }

    for (int r = 0; r < fcalcs.FCALC.num_cols(); r++)
    {
      if (fcalcs.MILLER[r] != millnx(0,0,0))
      {
        REFLECTIONS->set_miller(fcalcs.MILLER[r],r);
        friedel::type f = friedel::NAT;
        double fmap,phase;
        std::tie(fmap,phase) = fcalcs.polar_deg(0,r);
        REFLECTIONS->set_present(f,true,r); //flagged not present
        REFLECTIONS->set_flt(labin::MAPF,fmap,r);
        REFLECTIONS->set_flt(labin::MAPPH,phase,r);
      }
    }
    }}

    REFLECTIONS->MAP = true;
    REFLECTIONS->EVALUES = false;
    REFLECTIONS->INTENSITIES = false;
    REFLECTIONS->ANOMALOUS = false;

    REFLECTIONS->set_data_resolution();
    input.value__flt_paired.at(".phasertng.data.resolution_available.").set_value(
      { REFLECTIONS->RESOLUTION[1],REFLECTIONS->RESOLUTION[0]});
    input.value__flt_cell.at(".phasertng.data.unitcell.").set_value(
      REFLECTIONS->UC.cctbxUC.parameters());

    {{
    out::stream where = out::LOGFILE;
    logTab(where,"Space Group: " + REFLECTIONS->SG.CCP4);
    logTab(where,"Unit Cell:  " + dvtos(REFLECTIONS->UC.cctbxUC.parameters()));
    logTab(where,"Wavelength: " + dtos(REFLECTIONS->WAVELENGTH));
    logTab(where,"Resolution: " + dtos(REFLECTIONS->data_hires()));
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logReflections("Data"));

    DAGDB.initialize_first_node_from_hash_tag(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::LOGFILE;
    std::string tag;
    logUnderLine(where,"Reflection Identifier");
    //if tag is not input then tne default tag is the stem of the pdb file
    //(stem = the filename without the final extension)
    if (!input.value__string.at(".phasertng.pdbin.tag.").is_default())
    {
      tag = input.value__string.at(".phasertng.pdbin.tag.").value_or_default();
      logTab(where,"Model tag from input");
    }
    else
    {
      tag = models.stem();
      logTab(where,"Model tag from filename");
    }
    reflid = DAGDB.initialize_reflection_identifier(tag);
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::INIT);
    //Phasertng::calculate_dobs_feff();
    }}

    //set the reflection values on the node
    dag::Node& node = DAGDB.NODES.front();
    node.CELL = REFLECTIONS->UC.cctbxUC;
    node.HALL = REFLECTIONS->SG.HALL;
    PHASER_ASSERT(!boost::logic::indeterminate(REFLECTIONS->MAP));
    node.SG = nullptr;
    node.REFLID = reflid.identify();

    {{
    out::stream where = out::VERBOSE;
    logUnderLine(where,"History");
    logBlank(where);
    logTabArray(where,REFLECTIONS->get_history());
    }}

    //create the first node
    PHASER_ASSERT(REFLECTIONS->NREFL > 0); //check this is the type
    PHASER_ASSERT(REFLCOLSMAP.NREFL > 0); //check this is the type
    //don't shift, just initialize above

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }
} //phasertng
