# -*- coding: utf-8 -*-

# # Python 2 and 3: easiest option
from __future__ import absolute_import

import sys
from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request, build_opener, install_opener, HTTPRedirectHandler, ProxyHandler
from urllib.error import HTTPError

#TODO: here it is where can be configured the proxy for voyager
# # set up authentication info
# authinfo = urllib.request.HTTPBasicAuthHandler()
# authinfo.add_password(realm='PDQ Application',
#                       uri='https://mahler:8092/site-updates.py',
#                       user='klem',
#                       passwd='geheim$parole')
# proxy_support = urllib.request.ProxyHandler({"http" : "http://ahad-haam:3128"})
# proxy_support = ProxyHandler({})
#
# # build a new opener that adds authentication and caching FTP handlers
# opener = build_opener(proxy_support)
#
# # install it
# install_opener(opener)

#######################################################################################################
#                                                                                                     #
#######################################################################################################

def get_encoding():
    return "utf-8"
    #return locale.getpreferredencoding()

def request_url(search_string, rootsite, data=None):
    z = None
    tries = 1 # only try once or we could be waiting all day
    for trial in range(tries):
        try:
            if data is None:
                #print("METHOD A", search_string, data)
                req = Request(search_string)
                z = urlopen(req).read()
            else:
                #print("METHOD B", search_string, data)
                req = Request(search_string)
                z = urlopen(req, data).read()
            break
        except:
            # print(sys.exc_info())
            # traceback.print_exc(file=sys.stdout)
            #print("Error contacting",rootsite,"...Attempt ", trial + 1, "/", tries, search_string, data)
            if (tries > 1): time.sleep(1) #if you are going to try again you have to wait a sec
    if not z:
        #print("Connection Error")
        return None
    else:
        return z

