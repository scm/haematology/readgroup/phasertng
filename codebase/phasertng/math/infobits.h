#ifndef __phasertng_math_infobits_class__
#define __phasertng_math_infobits_class__
#include <cmath>
#include <limits>
#include <scitbx/constants.h>

namespace phasertng {
namespace math {

class infobits
{
  public:
    infobits() {}

  private:
    const double ZERO = (0.0);
    const double HALF = (0.5);
    const double ONE = (1.0);
    const double THREEHALF = (1.5);
    const double TWO = (2.0);
    const double FOUR = (4.0);
    const double EIGHT = (8.0);
    const double SQRT2 = (std::sqrt(2.));
    const double SQRT2PI = (std::sqrt(scitbx::constants::two_pi));
    const double term2 = ((std::log(2./scitbx::constants::pi)-1.)/2.);
    const double SQRTPI = (std::sqrt(scitbx::constants::pi));
    const double LOG2 = (std::log(2.));
    const double EPS = (std::pow(10.,-12));
    const double MAXEXPTERM = std::log(std::numeric_limits<double>::max());

  public:
    double EosqInfoBitsAcen(double,double);
    double EosqInfoBitsCen(double,double);
    double SADextraBits(double,double,double,double,double,double);
    double expectedFisherWtAcen(double,double,double);
    double expectedFisherWtCen(double,double,double);

  private:
    double scaledPCDmhalf(double);

};

}}
#endif
