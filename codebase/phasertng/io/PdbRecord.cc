//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/PdbRecord.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/hoist.h>
#include <iotbx/pdb/hierarchy.h>
#include <cctbx/adptbx.h>
#include <phasertng/io/tostr2.h>

namespace phasertng {

  std::pair<char,char>
  PdbRecord::chain_pair() const
  { return { Chain[0],Chain[1] }; }

  bool
  PdbRecord::at_xplor_infinity() const
  { return X == dvect3(9999,9999,9999); }

  bool
  PdbRecord::is_water() const
  {
    std::set<std::string> atom = { "OW","OH2","WAT" };
    std::set<std::string> res =  { "OW","OH2","WAT","HOH","H20","MOH","WTR","TIP" };
    return (atom.count(AtomName) > 0 or res.count(ResName) > 0);
  }

  bool
  PdbRecord::is_backbone() const
  {
    std::set<std::string> atom = { " CA "," C  "," N  "," O  "};
    return (atom.count(AtomName) > 0);
  }

  bool
  PdbRecord::is_polyala() const
  {
    std::set<std::string> atom = { " CA "," C  "," N  "," O  "," CB "};
    return (atom.count(AtomName) > 0);
  }

  bool
  PdbRecord::is_calpha() const
  { return (AtomName == " CA "); }

  bool
  PdbRecord::is_palpha() const
  { return (AtomName == " P  "); }

  bool
  PdbRecord::is_protein_trace() const
  { //not the same as in phaser, here we fill out the trace in the side chains also
    //these are used for spanning and fill space to 10A
    //std::set<std::string> nucleic_trace = { " CA"," CG"," CE"," NE"," SG"," OG"," OE"," CH"," OH" };
    //return (nucleic_trace.count(AtomName.substr(0,3)) > 0);
    std::set<std::string> nucleic_trace = { "A","G","E","H" };
    return (nucleic_trace.count(AtomName.substr(2,1)) > 0);
  }

  bool
  PdbRecord::is_nucleic_trace() const
  { //not the same as in phaser, here we fill out the trace in the bases also
    //these are used for spanning and fill space to 10A
    std::set<std::string> nucleic_trace = { " P "," C1"," C3"," C4" };
    return (nucleic_trace.count(AtomName.substr(0,3)) > 0);
  }


  std::string
  PdbRecord::hybrid36Num5(int n) const
  {
    char Num5[6]; //leave room for \0
    memset(Num5, ' ', 5); Num5[5] = '\0';
    if (n >= 0) //renumber in situ
      hy36encode(5,n,Num5);
    return std::string(Num5);
  }

  std::string
  PdbRecord::hybrid36Num4(int n) const
  {
    char Num4[5]; //leave room for \0
    memset(Num4, ' ', 4); Num4[4] = '\0';
    if (n >= 0) //renumber in situ
      hy36encode(4,n,Num4);
    return std::string(Num4);
  }

  std::string
  PdbRecord::Card(int ATOMNUM,int RESNUM) const
  {
    std::string atomcard = Hetatm ? "HETATM" : "ATOM  ";
    RESNUM = (RESNUM >= 0) ? RESNUM : ResNum;
    //hybrid36Num4(test) does not modify in place, result is return
    //negative values return blank strings
    //input negative atomnum/resnum are retained, but cannot be modified to negative number
    //since negative values passed to function are flag to retain input
    ATOMNUM = (ATOMNUM >= 0) ? ATOMNUM : AtomNum;
    bool outside_range =
       (X[0] > 9999.999 || X[1] > 9999.999 || X[2] > 9999.999 ||
        X[0] < -999.999 || X[1] < -999.999 || X[2] < -999.999);
    //if (outside_range)
    //throw Error(err::FATAL,"Atomic coordinate outside range [-999.999...9999.999]");
    dvect3 XX = X;
    if (outside_range)
      for (int i = 0; i < 3; i++)
         XX[i] = std::min(std::max(X[i], -999.999), 9999.999);

    char ATOM[80];
    std::string segid = hybrid36Num4(Segment);
    snprintf(ATOM,80, //!c++11
          "%6s%5s %4s%1s%3s%2s%4s%1s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s",
            Hetatm ? "HETATM" : "ATOM  ",
            (ATOMNUM <= 0) ? std::to_string(ATOMNUM).c_str() : hybrid36Num5(ATOMNUM).c_str(),
            AtomName.c_str(),
            AltLoc.c_str(),
            ResName.c_str(),
            Chain.c_str(),
            (RESNUM <= 0) ? std::to_string(RESNUM).c_str() : hybrid36Num4(RESNUM).c_str(),
            InsCode.c_str(),
            XX[0],
            XX[1],
            XX[2],
            O,
    //BFAC should be -relative_wilsonB+shift , +relative_wilsonB after commit 7068
            std::max(-99.99,std::min(999.99,B)),
            (Segment >= 0) ? segid.c_str() : "    ", //Chain.c_str(),
            Element2.c_str());
    return std::string(ATOM);
  }

  std::array<std::string,3>
  PdbRecord::WriteAtomCard(int ATOMNUM,int RESNUM) const
  {
    //returns a set of three, which is ATOM and ANISOU and REMARK
    if (TER) return { "TER", "", ""};
    RESNUM = (RESNUM >= 0) ? RESNUM : ResNum;
    ATOMNUM = (ATOMNUM >= 0) ? ATOMNUM : AtomNum;

    std::string ATOM = Card();
    //if the extraflags already start with REMARK then they are for writing
    //otherwise set to "" so they are not output
    //e.g. when they are for the tncs_group
    std::string remark_extraflags = !ExtraFlags.find("REMARK") ? ExtraFlags : "";
    if (AnisoU == dmat6(0,0,0,0,0,0)) //ExtraFlags normally ""
    return { ATOM, "" , remark_extraflags };

    char ANISOU[80];
    std::string segid = hybrid36Num4(Segment);
    snprintf(ANISOU,80,//c++11
            "%6s%5s %4s%1s%3s%2s%4s%1s %7.0f%7.0f%7.0f%7.0f%7.0f%7.0f  %-4s%2s",
            "ANISOU",
            hybrid36Num5(ATOMNUM).c_str(),
            AtomName.c_str(),
            AltLoc.c_str(),
            ResName.c_str(),
            Chain.c_str(),
            hybrid36Num4(RESNUM).c_str(),
            InsCode.c_str(),
            AnisoU[0],AnisoU[1],AnisoU[2],AnisoU[3],AnisoU[4],AnisoU[5],
            (Segment >= 0) ? segid.c_str() : "    ", //Chain.c_str(),
            Element2.c_str());
    //puts(ATOM);
    return { ATOM, std::string(ANISOU), remark_extraflags };
  }

  void
  PdbRecord::ReadAtomCard(std::string buffer)
  {
    PHASER_ASSERT(!buffer.find("ATOM") || !buffer.find("HETATM") || !buffer.find("TER"));
    if (!buffer.find("TER"))
    {
      TER = true;
      return;
    }
    bool has_element(false);
    try {
      if (buffer.size() < 66) throw;
      Hetatm = !buffer.find("HETATM");
      //can look at this to see if cols 5&6 are filled with atom numbers e.g ribosome
      //rather than using hyencode
      if (!Hetatm && buffer.substr(4,2) == "  ") //"ATOM  "
      { //could be a hy36encoded number
        std::string hybrid36_AtomNum = buffer.substr(6,5);
        hy36decode(5,hybrid36_AtomNum.c_str(),hybrid36_AtomNum.size(),&AtomNum);
      }
      else if (!Hetatm and std::isdigit(buffer.substr(5,1)[0]))
      { //can't be a hybrid36encoded number, 5&6 are filled with atom numbers
        AtomNum = std::atoi(buffer.substr(4,7).c_str());
      }
      else if (Hetatm)
      { //could be a hy36encoded number
        std::string hybrid36_AtomNum = buffer.substr(6,5);
        hy36decode(5,hybrid36_AtomNum.c_str(),hybrid36_AtomNum.size(),&AtomNum);
      }
      AtomName = buffer.substr(12,4);
      AltLoc = buffer.substr(16,1);
      ResName = buffer.substr(17,3);
      Chain = buffer.substr(20,2);
      std::string hybrid36_ResNum = buffer.substr(22,4);
      hy36decode(4,hybrid36_ResNum.c_str(),hybrid36_ResNum.size(),&ResNum);
      InsCode = buffer.substr(26,1);
      std::string Xstr = buffer.substr(30,8);
      X[0] = std::atof(Xstr.c_str());
      std::string Ystr = buffer.substr(38,8);
      X[1] = std::atof(Ystr.c_str());
      std::string Zstr = buffer.substr(46,8);
      X[2] = std::atof(Zstr.c_str());
      std::string Ostr = buffer.substr(54,6);
      O = std::atof(Ostr.c_str());
      std::string Bstr = buffer.substr(60,6);
      B = std::atof(Bstr.c_str());
      B = std::atof(buffer.substr(60,6).c_str());
      FlagAnisoU = false;
      if (buffer.size() >= 76)
      {
        std::string hybrid36_Segment = buffer.substr(72,4);
        if (hybrid36_Segment != "    ")
        {
          hy36decode(4,hybrid36_Segment.c_str(),hybrid36_Segment.size(),&Segment);
          std::string check = hybrid36Num4(Segment);
          //leave single char alone, or else it gets appended with 000 ie A->A000
          //if encoding the decode does not give the same string, then leave it out
          //  is probably just the chain repeated and we don't want A->A000
          //i.e. the segid MUST be encode/decode compliant
          if (check != hybrid36_Segment)
            Segment = -999;
        }
        else
        {
          Segment = -999; //flag for not set
        }
      }
      if (buffer.size() >= 78)
      {
        has_element = true;
        Element2 = buffer.substr(76,2);
      }
    }
    catch(...) { throw Error(err::INPUT,"Error reading: " + buffer); }

    Element = has_element ? Element2 : AtomName;
    hoist::algorithm::trim(Element);
    valid_element = false;
    valid_cluster = false;
    if (Element != "UNK") //allowed AtomName by pdb, not helpful for us
    {
      for (auto XX : { "AX","RX","TX" })
      {
        if (Element == XX)
        {
          valid_cluster = true;
        }
      }
      if (!valid_element && !valid_cluster)
      {
        iotbx::pdb::hierarchy::atom guessAtomType;
        guessAtomType.set_name(AtomName.c_str());
        guessAtomType.set_element(Element2.c_str());
        //guessAtomType has name and element set - if there are extra columns
        if (guessAtomType.determine_chemical_element_simple())
        {
          auto atomtype = guessAtomType.determine_chemical_element_simple().get();
          valid_element = true;
          set_element(atomtype);
        }
      }
    }
  }

  bool
  PdbRecord::ReadAnisouCard(std::string buffer)
  {
    PHASER_ASSERT(!buffer.find("ANISOU"));
    //check that this card corresponds with the ATOM card
    //minimal checks only, atom number and atom name
    int AnisoNum;
    std::string hybrid36_AtomNum = buffer.substr(6,5);
    hy36decode(5,hybrid36_AtomNum.c_str(),hybrid36_AtomNum.size(),&AnisoNum);
    std::string AnisoName = buffer.substr(12,4);
    if (AnisoNum != AtomNum) return false;
    if (AnisoName != AtomName) return false;
    //OK, proceed with parsing
    FlagAnisoU = true;
    AnisoU[0] = std::atof(buffer.substr(28,7).c_str());
    AnisoU[1] = std::atof(buffer.substr(35,7).c_str());
    AnisoU[2] = std::atof(buffer.substr(42,7).c_str());
    AnisoU[3] = std::atof(buffer.substr(49,7).c_str());
    AnisoU[4] = std::atof(buffer.substr(56,7).c_str());
    AnisoU[5] = std::atof(buffer.substr(63,7).c_str());
    if (buffer.size() >= 76)
    {
      std::string hybrid36_Segment = buffer.substr(72,4);
      //leave single char alone, or else it gets appended with 000 ie A->A000
      int AnisoSegment(0);
      if (hybrid36_Segment != "    ")
        hy36decode(4,hybrid36_Segment.c_str(),hybrid36_Segment.size(),&AnisoSegment);
      else
        Segment = -999; //flag for not set
      if (AnisoSegment != Segment) return false;
    }
    return true; //OK
  }

  std::pair<int,int>
  PdbRecord::tncs_group() const
  {
    std::pair<int,int> result = { 1,1 };
    std::stringstream ss;
    try {
    if (ExtraFlags.size())
    {
      ss << ExtraFlags;
      ss >> result.first;
      ss >> result.second;
    }}
    catch(...) {}; //if the parsing failed, default
    return result;
  }

  std::string PdbRecord::get_element() const
  {
    PHASER_ASSERT(Element2.size() == 2);
    return Element2;
  }

  void PdbRecord::set_element(std::string atomtype)
  {
    std::tie(Element2,Element) = tostr2(atomtype);
    valid_cluster = false;
    for (auto XX : { "AX","RX","TX" })
      if (Element == XX)
        valid_cluster = true;
    valid_element = !valid_cluster; //very lazy, assumes that this is OK internal to programme
  }

} //phasertng
