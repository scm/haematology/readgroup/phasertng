import sys
from libtbx.phil import parse
from phasertng.phil.master_node_file import build_node_str
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.converter_registry import converter_registry
import argparse

def dag_to_sol(filein,fileout):

    with open(filein, 'r') as f:
      dagstr = f.read()

    initparams = build_node_str()
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)

    dagstr = dagstr.split(node_separator)
    sol = ""
    phil = []
    for node in dagstr:
      if "node" in node: #not an empty string
        node = node_scope + " {\n" + node + "}\n"
        user_phil_str = parse(node,converter_registry=converter_registry)
        working_params = master_phil.fetch(source=user_phil_str).extract()
        phil.append(working_params)
        #now sol file
        node = working_params.phaserdag.node
        if node.annotation is not None:
          sol += "SOLUTION SET " + node.annotation + "\n"
        else:
          sol += "SOLUTION SET \n"
        if node.hall is not None:
          sol += "SOLUTION SPACEGROUP HALL " + node.hall + "\n"
        for pose in node.pose:
          ens = pose.tag
          euler = (str(pose.euler)[1:-1]).replace(","," ")
          frac = (str(pose.fractional)[1:-1]).replace(","," ")
          bfac = str(pose.bfactor)
          sol += "SOLU 6DIM ENSE " + ens + " EULER " + euler + " FRAC " + frac + " BFAC " + bfac + "\n"
        for ensemble in node.ensemble:
          ens = ensemble.tag
          drms = str(ensemble.drms)
          sol += "SOLU ENSE " + ens + " DRMS " + drms + "\n"
   #print sol
    f = open(fileout, "w")
    f.write(sol)
    return

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-f',"--filein",help="dag filein",required=True)
  parser.add_argument('-o',"--fileout",help="sol fileout",required=True)
  args = parser.parse_args()
  dag_to_sol(filein=args.filein,fileout=args.fileout)
  sys.exit(0)
