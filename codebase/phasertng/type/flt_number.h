//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_number_class__
#define __phasertng_type_flt_number_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class flt_number : public optional
{
  private:
    double dephalt = 0;
    double uvalue = 0;
    bool   pminset = false;
    bool   pmaxset = false;
    double pminval = 0;
    double pmaxval = 0;

  public:
    const bool&   minset() const { return pminset; }
    const double& minval() const { return pminval; }
    const bool&   maxset() const { return pmaxset; }
    const double& maxval() const { return pmaxval; }

  flt_number() : optional() {}
  flt_number(
      const std::string parameter,
      const bool pminset_,const double pminval,
      const bool pmaxset_,const double pmaxval,
      const double dephalt_=DEF_FLT_FLAG
  );
  void set_value(double other);
  void set_range(double a,double b);
  void set_value(const sv_string& vcards,const double other);
  bool is_default() const;
  void set_default();
  double value_or_default() const;
  double value() const;
  std::string unparse_parameter(bool phil) const;
  std::string to_python();
  void        from_python(std::string);
  std::string show_default(bool phil) const;

  bool operator==(const type::flt_number& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
