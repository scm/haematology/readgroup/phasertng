//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Hexagonal_class__
#define __phasertng_Hexagonal_class__
#include <phasertng/src/UnitCell.h>

namespace phasertng {

  class Hexagonal : public UnitCell
  {
    private: //members
      dvect3 START = dvect3(0,0,0);
      dvect3 SLOPE = dvect3(0,0,0);
      dvect3 POINT = dvect3(0,0,0);
      dvect3 orthSamp = dvect3(0,0,0);
      dvect3 orthMin = dvect3(0,0,0);
      dvect3 orthMax = dvect3(0,0,0);
      dvect3 orthSite = dvect3(0,0,0);
      double RANGEsqr = 0;
      double off0 = 0;
      double off1 = 0;
      int    U = 0;
      int    V = 0;
      int    L = 0;
      int    MAXL = 0;
      int    t = 0;
      int    max_t = 0;

    public: //constructors
      Hexagonal(UnitCell UC) : UnitCell(UC) { }

    public:
      bool         at_end() const;
      void         restart();
      std::size_t  num_sites();
      void         setup_around_point(dvect3,double,double,bool=false);
      dvect3       next_site(int);
  };

} //phasertng
#endif
