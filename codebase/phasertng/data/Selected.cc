//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Reflections.h>
#include <phasertng/data/Selected.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/jiffy.h>
#include <bitset>

namespace phasertng {

//#define PHASERTNG_DEBUG_SELECTED_CLASS

  af_string Selected::parse(
      bool OUT_REJ_,
      double OUT_INFO_,
      double OUT_PROB_,
      int LOGGED_
     )
  {
    af_string output;
    OUT_REJ = OUT_REJ_;
    OUT_INFO = OUT_INFO_;
    OUT_PROB = OUT_PROB_;
    LOGGED = LOGGED_;
    //if (OUT_REJ) output.push_back("Rejection for Information < " + gtos(OUT_INFO,6));
    //if (OUT_REJ) output.push_back("Rejection for Probability < " + gtos(OUT_PROB,6));
    return output;
  }

  af_string Selected::set_resolution(
      double data_hires,
      std::pair<double,double> database_resolution,
      double io_hires
    )
  {
    af_string output;
    //looks for the lowest resolution amongst
    //a) the data resolution
    //b) the ensembles used in this node (WORK)
    //a) the input resolution from the user
    output.push_back("High resolution limit of data: " + dtos(data_hires));
    double database_hires = std::min(database_resolution.first,database_resolution.second);
    double database_lores = std::max(database_resolution.first,database_resolution.second);
    if (database_hires > data_hires+DEF_PPT)
      output.push_back("High resolution limit of data truncated by database: " + dtos(database_hires));
    if (io_hires > data_hires+DEF_PPT)
      output.push_back("High resolution limit of data truncated by input: " + dtos(io_hires));
    STICKY_HIRES  = std::max(data_hires,std::max(database_hires,io_hires));
    STICKY_LORES  = std::min(STICKY_LORES,database_lores);
    output.push_back("High resolution limit: " + dtos(STICKY_HIRES));
    (STICKY_LORES == DEF_LORES) ?
      output.push_back("Low  resolution limit: None"):
      output.push_back("Low  resolution limit: " + dtos(STICKY_LORES));
    return output;
  }

  int
  Selected::nsel() const
  {
    int n(0);
    for (int r = 0; r < SELECTED.size(); r++)
      if (SELECTED[r]) n++;
    return n;
  }

  double
  Selected::percent_selected() const
  {
    double n(0);
    for (int r = 0; r < SELECTED.size(); r++)
      if (SELECTED[r])
        n++;
    return 100.0*n/SELECTED.size();
  }

  int
  Selected::nrej() const
  {
    int n(0);
    for (int r = 0; r < SELECTED.size(); r++)
      if (!SELECTED[r])
        n++;
    return n;
  }

  std::map<rejected::code,int>
  Selected::count_selected() const
  {
    std::map<rejected::code,int> Count;
    //initialize the map
    for (int o = 0; o < rejected::codeCount; o++)
      Count[static_cast<rejected::code>(o)] = 0;
    //count the outlier tests
    for (auto refl : this->PROBABILITIES)
      for (auto code : refl.second.CODES)
        Count[code]++;
    //inverse count for rejected::OK
    PHASER_ASSERT(SELECTED.size());
    Count[rejected::OK] = 0;
    for (int r = 0; r < SELECTED.size(); r++)
      if (!PROBABILITIES.count(r))
     // if (!SELECTED[r])
        Count[rejected::OK]++;
    return Count;
  }

  void Selected::append_probabilities(ProbType& reflprobs)
  {
    for (auto& item : reflprobs)
    {
      int r = item.first;
      auto& codes = item.second.CODES;
      if (PROBABILITIES.count(r))
        PROBABILITIES[r].CODES.insert(codes.begin(),codes.end()); else
        PROBABILITIES.emplace(r,item.second); //creates
    }
  }

  bool Selected::get_selected(int& r)
  {
    PHASER_ASSERT(r < SELECTED.size());
    return SELECTED[r];
  }

  af_bool Selected::get_selected_shared()
  {
    af_bool selected_array(SELECTED.size());
    for (int r = 0; r < SELECTED.size(); r++)
      selected_array[r] = get_selected(r);
    return selected_array;
  }

  sv_bool Selected::get_selected_array()
  {
    sv_bool selected_array(SELECTED.size());
    for (int r = 0; r < SELECTED.size(); r++)
      selected_array[r] = get_selected(r);
    return selected_array;
  }

  af_string
  Selected::logOutliers(std::string description ) const
  {
    af_string output;
    std::map<rejected::code,int> Count = count_selected();
    output.push_back("");
    output.push_back(">>Outlier Reflections: " + description + "<<");
    output.push_back("Wilson probability less than " + dtos(this->OUT_PROB));
    output.push_back("Fewer than " + dtos(this->OUT_INFO) + " bits of information");
    double sigesqr_cent,sigesqr_acent;
    std::tie(sigesqr_cent,sigesqr_acent) = sigesqr_values();
    output.push_back("  threshold sigma(Eobs^2) for  centrics is " + dtos(sigesqr_cent));
    output.push_back("  threshold sigma(Eobs^2) for acentrics is " + dtos(sigesqr_acent));
    output.push_back("Percent selected: " + dtos(percent_selected(),2) + "%");
    output.push_back("Resolution selected: " + dtos(resolution_selected) + "A");
    if (this->PROBABILITIES.size() == 0)
    {
      output.push_back("No reflections are outliers");
    }
    else
    {
      output.push_back("Outlier Counts: total " + itos(SELECTED.size()));
      output.push_back("OK: " + itos(Count[rejected::OK]));
      std::map<rejected::code,int> fmtstar;
      for (int o = 0; o < rejected::codeCount; o++)
      {
        //pad the columns with the width of the enum string
        //or the number in the column
        rejected::code code = static_cast<rejected::code>(o);
        std::string label = rejected::code2String(code);
        hoist::replace_all(label,"NAT","");
        hoist::replace_all(label,"POS","(+)");
        hoist::replace_all(label,"NEG","(-)");
        fmtstar[code] = label.size();
        int i = Count[code];
        int ndigits = (i > 0) ? (int) std::log10 ((double) i) + 1 : 1;
        fmtstar[code] = std::max(fmtstar[code],ndigits); //OK will be large
      }
      {{ //a line
      std::string line;
      for (int o = 0; o < rejected::codeCount; o++)
      {
        rejected::code code = static_cast<rejected::code>(o);
        std::string label = rejected::code2String(code);
        hoist::replace_all(label,"NAT","");
        hoist::replace_all(label,"POS","(+)");
        hoist::replace_all(label,"NEG","(-)");
        if (code != rejected::OK)
          line += snprintftos("%*s ",fmtstar[code],label.c_str());
      }
      output.push_back( line );
      }}
      {{//a line
      std::string line;
      for (int o = 0; o < rejected::codeCount; o++)
      {
        rejected::code code = static_cast<rejected::code>(o);
        if (code != rejected::OK)
        line += snprintftos("%*d ",fmtstar[code], Count[code]);
      }
      output.push_back(line ); //return
      }}
      output.push_back(""); //return
      bool print_all(false); //hack this if you want to see all outliers in the testing stream
      {{
      output.push_back("");
      output.push_back("Outlier Reflections: Worst");
      output.push_back(snprintftos(
          "%-7s %5s %5s %5s  %6s %9s %-*s %s",
          "r#","H","K","L","reso","prob",rejected::codeCount,"outlier","selected"));
      int nprint(0);
      //print out the reflections grouped on the number of rejection criteria
      for (int multi = rejected::codeCount-1; multi >= 1; multi--)
      {
        for (auto refl : this->PROBABILITIES)
        {
          std::bitset<rejected::codeCount-1> b;
          for (int o = 1; o < rejected::codeCount; o++)
          { //not the first, which is rejected::OK
            rejected::code code = static_cast<rejected::code>(o);
            if (refl.second.CODES.count(code))
              b.set(o-1);
          }
          std::string bstr =  b.to_string();
          //reverse so that it reads left to right
          std::reverse(bstr.begin(),bstr.end());
          if (refl.second.CODES.size() == multi)
          {
            int r = refl.first;
            double prob =  refl.second.lowest_wilson_probability();
            output.push_back(snprintftos(
               "%-7d %5i %5i %5i  %6.2f %9s %*s %c",
                r+1,
                refl.second.MILLER[0],
                refl.second.MILLER[1],
                refl.second.MILLER[2],
                refl.second.RESO,
                (prob == std::numeric_limits<double>::max()) ?  "n/a" : dtos(prob,6,2).c_str(),
                rejected::codeCount,
                bstr.c_str(),
                SELECTED[r] ? ' ':'x'
              ) );
            nprint++;
            if (nprint > LOGGED and !print_all) break;
            //or you can print last ones to the testing stream
          }
        }
      }
      if (this->PROBABILITIES.size() > LOGGED)
        output.push_back("More than " + itos(LOGGED) + " outliers: not printed");
      output.push_back("");
      }}
      //flip is sorted in order of probabilities
      std::multimap<pod::probtype, int> FLIP = flip_map(this->PROBABILITIES);
      if (FLIP.size()) //print the outliers that are due to wilson/lowinfo
      {
        output.push_back("");
        output.push_back("Outlier Reflections: Low Information and Wilson");
        output.push_back(snprintftos(
          "%-7s %5s %5s %5s  %6s %9s %9s %9s %9s %*s %s",
          "r#","H","K","L","reso","prob(n)","prob(+)","prob(-)","sigesqr",rejected::codeCount,"outlier","selected"));
        std::vector<rejected::code> CODES = {
              rejected::WILSONNAT, rejected::LOINFONAT,
              rejected::WILSONPOS, rejected::LOINFOPOS,
              rejected::WILSONNEG, rejected::LOINFONEG };
        for (int o = 0; o < CODES.size(); o++)
        {
          output.push_back("Criteria: " + rejected::code2String(CODES[o]));
          int nprint(0);
          for (auto refl : FLIP)
          {
            int r = refl.second;
            std::bitset<rejected::codeCount> b;
            for (int o = 0; o < rejected::codeCount; o++)
              if (refl.first.CODES.count(static_cast<rejected::code>(o)))
                b.set(o);
            std::string bstr = b.to_string();
            //reverse so that it reads left to right
            std::reverse(bstr.begin(),bstr.end());
            if (refl.first.CODES.count(CODES[o]))
            {
              if (nprint <= LOGGED)
              {
                std::string nat = dtos(refl.first.probability_str(friedel::NAT),9,2);
                if (!refl.first.probability_str(friedel::NAT)) nat = "n/a";
                std::string pos = dtos(refl.first.probability_str(friedel::POS),9,2);
                if (!refl.first.probability_str(friedel::POS)) pos = "n/a";
                std::string neg = dtos(refl.first.probability_str(friedel::NEG),9,2);
                if (!refl.first.probability_str(friedel::NEG)) neg = "n/a";
                std::string line = snprintftos(
                   "%-7d %5i %5i %5i  %6.2f %9s %9s %9s %9.2e %*s %c",
                    r+1,
                    refl.first.MILLER[0],
                    refl.first.MILLER[1],
                    refl.first.MILLER[2],
                    refl.first.RESO,
                    nat.c_str(),
                    pos.c_str(),
                    neg.c_str(),
                    refl.first.get_sigessqr(),
                    rejected::codeCount,
                    bstr.c_str(),
                    SELECTED[r] ? ' ':'x');
                output.push_back(line );
              }
              nprint++;
            }
          }
          if (nprint > LOGGED)
            output.push_back("More than " + itos(LOGGED) + " outliers: additional reflections not printed");
          if (!nprint)
            output.push_back("No reflections to print");
        }
        output.push_back("");
      }
    }
    output.push_back("");
    return output;
  }

  af_string
  Selected::logSelected(std::string description) const
  {
    af_string output;
    std::map<rejected::code,int> Count = count_selected();
    int nrej = this->SELECTED.size()-nsel();
    int nout = this->PROBABILITIES.size();
    int ndif(0),nres(0);
    for (auto refl : this->PROBABILITIES)
    {
      if (refl.second.CODES.size() and this->SELECTED[refl.first])
        ndif++;
      if (refl.second.CODES.count(rejected::RESO))
        nres++;
    }

    double selsize = this->SELECTED.size();
    output.push_back("");
    output.push_back(">>Selected Reflections: " + description + "<<");
 //   if (!boost::logic::indeterminate(TNCS_MODELLED))
 //     TNCS_MODELLED ? output.push_back("tncs present in models = true"):
 //     output.push_back("tncs present in models = false");
    output.push_back("Total of " + itos(this->SELECTED.size()) + " reflections: " + itos(nsel()) + " selected");
    {
    int nf = nout - nres;
    double pnf = 100.*nf/selsize;
    if (nf == 0)
      output.push_back("No reflections flagged as outlier");
    else if (nf == 1)
      output.push_back("There was 1 reflection (" + dtos(pnf,4) + "%) flagged as outlier");
    else
      output.push_back("There were " + itos(nf) + " reflections (" + dtos(pnf,4) + "%) flagged as outliers");
    }
    if (nres > 0)
    {
    double pnres = 100.*nres/selsize;
    if (nres == 1)
      output.push_back("There was 1 reflection (" + dtos(pnres,4) + "%) outside resolution range");
    else
      output.push_back("There were " + itos(nres) + " reflections (" + dtos(pnres,4) + "%) outside resolution range");
    }
    {
    double pnrej = 100.*nrej/selsize;
    if (nrej == 0)
      output.push_back("No reflections rejected");
    else if (nrej == 1)
      output.push_back("There was 1 reflection (" + dtos(pnrej,4) + "%) rejected");
    else
      output.push_back("There were " + itos(nrej) + " reflections (" + dtos(pnrej,4) + "%) rejected");
    }
    {
    double pndif = 100.*ndif/selsize;
    if (ndif == 0)
      output.push_back("No reflections were outliers but not rejected");
    else if (ndif == 1)
      output.push_back("There was 1 reflection (" + dtos(pndif,4) + "%) that was an outlier but not rejected");
    else
      output.push_back("There were " + itos(ndif) + " reflections (" + dtos(pndif,4) + "%) that were outliers but not rejected");
    }
    std::map<rejected::code,int> fmtstar;
    for (int o = 0; o < rejected::codeCount; o++)
    {
      //pad the columns with the width of the enum string
      //or the number in the column
      //len(OK)==2 but will have a large count
      rejected::code code = static_cast<rejected::code>(o);
      std::string label = rejected::code2String(code);
      hoist::replace_all(label,"NAT","");
      hoist::replace_all(label,"POS","(+)");
      hoist::replace_all(label,"NEG","(-)");
      fmtstar[code] = label.size();
      int i = Count[code];
      int ndigits = (i > 0) ? (int) std::log10 ((double) i) + 1 : 1;
      fmtstar[code] = std::max(fmtstar[code],ndigits);
    }
    //loop over outlier codes and print how many in each list
    //only print the criteria that have been used for the selection
    output.push_back("OK: " + itos(Count[rejected::OK]));
    {{
    std::string line;
    for (int o = 0; o < rejected::codeCount; o++) //this is the header
    {
      rejected::code code = static_cast<rejected::code>(o);
      std::string label = rejected::code2String(code);
      hoist::replace_all(label,"NAT","");
      hoist::replace_all(label,"POS","(+)");
      hoist::replace_all(label,"NEG","(-)");
      if (code != rejected::OK)
        line += this->CRITERIA.count(code) ?
          snprintftos("%*s ",fmtstar[code],label.c_str()): "";
    }
    output.push_back(line);
    }}
    {{
    std::string line;
    for (int o = 0; o < rejected::codeCount; o++) //these are the data
    {
      rejected::code code = static_cast<rejected::code>(o);
      if (code != rejected::OK)
      line += this->CRITERIA.count(code) ?
          snprintftos("%*d ",fmtstar[code],Count[code]):
          "";
    }
    output.push_back(line);
    }}
    output.push_back("");
    return output;
  }

  std::pair<double,double>
  Selected::sigesqr_values() const
  {
    double sigesqr_cent = def_sigesqr_cent; //OUT_INFO == 0.01
    double sigesqr_acent = def_sigesqr_acent; //OUT_INFO == 0.01
    if (OUT_INFO != 0.01)
    {
      sigesqr_cent = table::sigesq_centric().get(OUT_INFO);
      sigesqr_acent = table::sigesq_acentric().get(OUT_INFO);
    }
    return {sigesqr_cent,sigesqr_acent};
  }

}//phasertng

#include <phasertng/data/include/Rejected.cpp>
#include <phasertng/data/include/Standard.cpp>
