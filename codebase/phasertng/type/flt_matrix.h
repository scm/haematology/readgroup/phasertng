//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_flt_matrix_class__
#define __phasertng_type_flt_matrix_class__
#include <phasertng/type/optional.h>
#include <scitbx/mat3.h>

namespace phasertng {
namespace type {
typedef scitbx::mat3<double>                dmat33;

class flt_matrix : public optional
{
  dmat33  dephalt;
  dmat33  uvalue;
  bool    pminset = false;
  bool    pmaxset = false;
  double  pminval = 0.;
  double  pmaxval = 0.;

  public:
  const bool& minset() const { return pminset; }
  const double&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const double&  maxval() const { return pmaxval; }

  flt_matrix() : optional() {}
  flt_matrix(
      const std::string parameter,
      const bool pminset_, const double pminval_,
      const bool pmaxset_, const double pmaxval_,
      const dmat33 dephalt_ = { DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
                                DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG,
                                DEF_FLT_FLAG,DEF_FLT_FLAG,DEF_FLT_FLAG }
   );

  void set_value(dmat33 other);
  void set_value(
      double aa, double ab, double ac,
      double ba, double bb, double bc,
      double ca, double cb, double cc
    );
  void set_value(const sv_string& vcards,const dmat33& other);
  bool is_default() const;
  void set_default();
  dmat33 value_or_default() const;
  dmat33 value() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::flt_matrix& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
