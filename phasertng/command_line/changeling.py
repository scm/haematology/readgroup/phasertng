# LIBTBX_SET_DISPATCHER_NAME phasertng.changeling

from iotbx.cli_parser import run_program
from phasertng.programs import changeling

if (__name__ == '__main__'):
  try:
    run_program(changeling.Program)
  except Exception as e:
    print(str(e))
