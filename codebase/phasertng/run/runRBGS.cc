//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/site/PointsAround.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Brute_translation_function
  void Phasertng::runRBGS()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with poses");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("turn");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    //determine if this is the first tf or not
    auto tf1 = !DAGDB.NODES.number_of_poses();
    auto tf2 = !tf1;
    if (!tf1 and !tf2)
    throw Error(err::INPUT,"Node not compatible for translation function");
    if (tf1 and REFLECTIONS->SG.is_p1())
    throw Error(err::INPUT,"First translation function in P1: not compatible with modefunction");

    //initialize tNCS parameters from reflections and calculate correction terms
    //BTF
    bool store_halfR(true);
    bool store_fullR(tf2);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    int io_nref = input.value__int_number.at(".phasertng.grid_search.maximum_stored.").value_or_default();
    if (DAGDB.size() > io_nref)
    {
      DAGDB.NODES.erase(DAGDB.NODES.begin()+1,DAGDB.NODES.end());
      logTab(out::LOGFILE,"Number of nodes truncated to " + itos(io_nref));
    }

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    auto io_hires1 = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    auto io_hires2 = input.value__flt_number.at(".phasertng.grid_search.resolution.").value_or_default();
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        std::max(io_hires1,io_hires2));
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    {{
    logEllipsisOpen(out::VERBOSE,"Pose perturbations incorporated");
    auto& UC = REFLECTIONS->UC;
    dmat33 orthmat = UC.orthogonalization_matrix();
    dmat33 fracmat = UC.fractionalization_matrix();
    dmat33 ROT;dvect3 FRACT;
    for (auto& node : DAGDB.NODES)
    {
      for (int p = 0; p < node.POSE.size(); p++)
      {
        dag::Pose& pose = node.POSE[p];
        const dmat33& PR = pose.ENTRY->PRINCIPAL_ORIENTATION;
        const dvect3& PT = pose.ENTRY->PRINCIPAL_TRANSLATION;
        std::tie(ROT,FRACT) = pose.finalRT(orthmat,fracmat);
        pose.EULER = scitbx::math::euler_angles::zyz_angles(ROT);
        pose.FRACT = FRACT;
        std::tie(std::ignore,pose.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(pose.EULER,PR);
        pose.SHIFT_ORTHT = orthmat*dag::fract_wrt_in_from_mt(pose.FRACT,ROT,PT,node.CELL);
        pose.ORTHT = dvect3(0,0,0);
        pose.ORTHR = dvect3(0,0,0);
      }
    }
    logEllipsisShut(out::VERBOSE);
    }}

    typedef std::unique_ptr<SiteList> pointsaround;
    //ncyc protocol contains the rotList and traList pair for the cycle
    std::vector<std::pair<pointsaround,pointsaround>> ncyc_protocol;
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Grid Generation");
    //store the cut planes if they are set
    dvect3 point(0,0,0);
    auto range = input.value__flt_paired.at(".phasertng.grid_search.range.").value_or_default();
    auto sampling = input.value__flt_paired.at(".phasertng.grid_search.sampling.").value_or_default();
    bool use_orientation(false),use_translation(false);
    pointsaround rotList,traList,nullList;
    rotList.reset(new PointsAround(sampling.first,point,range.first));
    traList.reset(new PointsAround(sampling.second,point,range.second));
    nullList.reset(new PointsAround(0,point,0));
    logTab(where,"Sampling orientations:  " + dtos(sampling.first,6,2) + " degrees");
    logTab(where,"Sampling translations:  " + dtos(sampling.second,6,2) + " Angstroms");
    logTab(where,"Maximum orientation: +/-" + dtos(range.first,6,2) + " degrees");
    logTab(where,"Maximum translation: +/-" + dtos(range.second,6,2) + " Angstroms");
    logBlank(where);
    int ncyc = input.value__int_number.at(".phasertng.grid_search.ncyc.").value_or_default();
    ncyc_protocol.resize(ncyc);
    if (input.value__choice.at(".phasertng.grid_search.selection.").value_or_default_equals("both"))
    {
      logTab(where,"Orientation and translation perturbation");
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].first.reset(new PointsAround(sampling.first,point,range.first));
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].second.reset(new PointsAround(sampling.second,point,range.second));
    }
    else if (input.value__choice.at(".phasertng.grid_search.selection.").value_or_default_equals("rot"))
    {
      logTab(where,"Orientation perturbation");
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].first.reset(new PointsAround(sampling.first,point,range.first));
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].second.reset(new PointsAround(0,point,0));
    }
    else if (input.value__choice.at(".phasertng.grid_search.selection.").value_or_default_equals("tra"))
    {
      logTab(where,"Translation perturbation");
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].first.reset(new PointsAround(0,point,0));
      for (int c = 0; c < ncyc; c++) ncyc_protocol[c].second.reset(new PointsAround(sampling.second,point,range.second));
    }
    else if (input.value__choice.at(".phasertng.grid_search.selection.").value_or_default_equals("trarot"))
    {
      logTab(where,"Translation and orientation perturbation alternating");
      ncyc_protocol.resize(ncyc*2);
      int cc(0);
      for (int c = 0; c < ncyc; c++) {
        //add two cycles per c
        ncyc_protocol[cc].first.reset(new PointsAround(0,point,0));
        ncyc_protocol[cc++].second.reset(new PointsAround(sampling.second,point,range.second));
        ncyc_protocol[cc].first.reset(new PointsAround(sampling.first,point,range.first));
        ncyc_protocol[cc++].second.reset(new PointsAround(0,point,0));
      }
    }
    else if (input.value__choice.at(".phasertng.grid_search.selection.").value_or_default_equals("rottra"))
    {
      logTab(where,"Orientation and translation perturbation alternating");
      ncyc_protocol.resize(ncyc*2);
      int cc(0);
      for (int c = 0; c < ncyc; c++) {
        //add two cycles per c
        ncyc_protocol[cc].first.reset(new PointsAround(sampling.first,point,range.first));
        ncyc_protocol[cc++].second.reset(new PointsAround(0,point,0));
        ncyc_protocol[cc].first.reset(new PointsAround(0,point,0));
        ncyc_protocol[cc++].second.reset(new PointsAround(sampling.second,point,range.second));
      }
    }
    else throw Error(err::INPUT,"Grid search type not allowed");
    for (int c = 0; c < ncyc_protocol.size(); c++)
    {
      logTab(where,"Cycle" + nNtos(c+1,ncyc_protocol.size()));
      int nrot = ncyc_protocol[c].first->count_sites();
      int ntra = ncyc_protocol[c].second->count_sites();
      logTab(where,"Orientation points: " + itos(nrot));
      logTab(where,"Translation points: " + itos(ntra));
      logTab(where,"Number of perturbations: " + itos(nrot*ntra));
    }
    }}
    int ncyc = ncyc_protocol.size();
    std::set<int> bad_density; //= { 12, 28,44,52 };
    if (!input.value__int_numbers.at(".phasertng.grid_search.poses.").is_default())
    {
      auto v = input.value__int_numbers.at(".phasertng.grid_search.poses.").value_or_default();
      bad_density = std::set<int>(v.begin(), v.end());
    }

    struct macrocycledata
    {
      double initial_llg;
      sv_int    pcounter; //use ivtos to print
      sv_double increase; //use dvtos to print
      sv_double distance; //use dvtos to print
      sv_double rotation; //use dvtos to print
      std::string jogged;
      double final_llg;
    };
    std::vector<std::vector<macrocycledata>> macrocycle(DAGDB.size());
    for (int i = 0; i < DAGDB.size(); i++)
    {
      macrocycle[i].resize(ncyc);
      for (int c = 0; c < ncyc; c++)
      {
        int nposes = (bad_density.size() > 0) ? bad_density.size() : DAGDB.NODES[0].POSE.size();
        macrocycle[i][c].pcounter.resize(nposes,0);
        macrocycle[i][c].increase.resize(nposes,0);
        macrocycle[i][c].distance.resize(nposes,0);
        macrocycle[i][c].rotation.resize(nposes,0);
      }
    }
    pod::FastPoseType ecalcs_sigmaa_pose;

    double min_llg_increase=5;
    int n(1),N(DAGDB.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      std::string nN = nNtos(n++,N);
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Grid Search" + nN);
      dag::Node* work = DAGDB.WORK;
      int q = work->POSE.size()-1;
      bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work->SG);
      if (diffsg)
        selected.flag_standard_outliers_and_select(REFLECTIONS.get());
      for (int c = 0; c < ncyc; c++)
      {
        logTab(where,"Cycle" + nNtos(c+1,ncyc));
        auto& info = macrocycle[n-2][c];
        const dmat33& fracmat = work->CELL.fractionalization_matrix();
        const dmat33& orthmat = work->CELL.orthogonalization_matrix();
        int ip(0);
        for (int p = 0; p < work->POSE.size(); p++)
        {
          int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
          if (p and (p%nmol)==0)
            info.jogged += " "; //add a spacer to separate tNCS groups, to help locate patterns in the shifts
          info.jogged += "-"; //default
          if (!bad_density.size() or bad_density.count(p+1))
          {
            auto orig_pose = work->POSE;
            std::swap(work->POSE[q],work->POSE[p]);
            dag::Pose lastpose = work->POSE[q]; //copy last pose
            work->POSE.pop_back(); //delete last pose
            ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
                  REFLECTIONS.get(),
                  selected.get_selected_array(),
                  &epsilon,
                  NTHREADS,
                  USE_STRICTLY_NTHREADS);
            work->POSE.push_back(lastpose); //put back last pose
            PHASER_ASSERT(ecalcs_sigmaa_pose.precalculated);
            work->last_pose = true; //last one, doubles as flag in below
            dag::Pose& pose = work->POSE[q]; //alias for this modified
            DAGDB.work_node_likelihood(
                REFLECTIONS.get(),
                selected.get_selected_array(),
                &epsilon,
                NTHREADS,
                USE_STRICTLY_NTHREADS,
                &ecalcs_sigmaa_pose);
            if (ip == 0) info.initial_llg = work->LLG;
            double best_llg = work->LLG;
            double start_llg = work->LLG;
            dvect3 best_ortht = pose.SHIFT_ORTHT;
            dvect3 best_frac = pose.FRACT;
            dvect3 best_orthr = pose.ORTHR;
            PHASER_ASSERT(ip < info.pcounter.size());
            info.pcounter[ip] = p+1;
            info.increase[ip] = 0;
            info.distance[ip] = 0;
            info.rotation[ip] = 0;
            info.final_llg = work->LLG; //in case of no change
            auto& rotList = ncyc_protocol[c].first;
            auto& traList = ncyc_protocol[c].second;
            rotList->restart();
            traList->restart();
            int l(1),L(rotList->count_sites()*traList->count_sites());
            std::string pP = nNtos(p+1,work->POSE.size());
            logProgressBarStart(where,"Optimizing pose"+pP,L);
            int w = 0;
            while (!rotList->at_end())
            {
              dvect3 orthr = rotList->next_site();
              traList->restart();
              while (!traList->at_end())
              { //use_tran and use_rotn implicit in zero shifts if set
                std::string lL = nNtos(l++,L);
                dvect3 ortht = traList->next_site();
                pose.FRACT = orig_pose[p].FRACT+fracmat*ortht;
                pose.SHIFT_ORTHT = orig_pose[p].SHIFT_ORTHT+ortht;
                pose.ORTHR = orthr;
                DAGDB.work_node_likelihood(
                    REFLECTIONS.get(),
                    selected.get_selected_array(),
                    &epsilon,
                    NTHREADS,
                    USE_STRICTLY_NTHREADS,
                    &ecalcs_sigmaa_pose);
                //could store in the dag here, but we select first, then append at the end
                if (w == 0) w = itow(work->LLG)+5; //.12 + increase *10 + (-) optional
                if (work->LLG >= best_llg+min_llg_increase)
                {
                  best_llg = work->LLG;
                  best_frac = pose.FRACT;
                  best_ortht = pose.SHIFT_ORTHT;
                  info.distance[ip] = std::sqrt(ortht*ortht);
                  best_orthr = orthr;
                  info.rotation[ip] = orthr[0]+orthr[1]+orthr[2]; //total rotation angle = x+y+z
                  info.increase[ip] = work->LLG-start_llg;
                  info.final_llg = work->LLG;
                  info.jogged.back() = '+';
                  logProgressBarAgain(where,"New Best LLG = " + dtos(best_llg,w,2) +
                           " shift(o)=" + dtos(info.rotation[ip],5,2) + " shift(A)=" + dtos(info.distance[ip],5,1) + lL);
                }
               if (Suite::Extra())
                  logProgressBarAgain(where,"Next LLG = " + dtos(work->LLG,w,2) +
                           " shift(o)=" + dvtos(orthr,5,2) + " shift(A)=" + dvtos(ortht,5,1) + lL);
                if (!logProgressBarNext(where)) break;
              }
            }
            pose.SHIFT_ORTHT = best_ortht;
            pose.FRACT = best_frac;
            pose.ORTHR = best_orthr;
            logProgressBarEnd(where);
            std::swap(work->POSE[q],work->POSE[p]);
            work->last_pose = false;
            ip++;
          }
        }
      }
    }

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Rigid Body Grid Search");
    logTab(where,"Number of poses per solution: " + itos(DAGDB.NODES.number_of_poses()));
    int w = itow(DAGDB.NODES.size());
    logTab(where,snprintftos(
        "%*s %3s %11s %11s %s",
        w,"#","#","LLG-input","LLG-final","Pose #"));
    logTab(where,snprintftos(
        "%*s %3s %11s %11s %s",
        w,"#","","","","LLG-increase/"));
    logTab(where,snprintftos(
        "%*s %3s %11s %11s %s %s",
        w,"#","","","","","rotation(o)"));
    logTab(where,snprintftos(
        "%*s %3s %11s %11s %s %s",
        w,"#","","","","","distance(A)"));
    for (int n = 0; n < DAGDB.size(); n++)
    {
      for (int c = 0; c < ncyc; c++)
      {
        PHASER_ASSERT(n < macrocycle.size());
        PHASER_ASSERT(c < macrocycle[n].size());
        auto& info = macrocycle[n][c];
        logTab(where,snprintftos(
          "%*d %3d %11.1f %11.1f %s",
          w,n+1,c+1, info.initial_llg, info.final_llg, ivtos(info.pcounter,6,true).c_str()));
        logTab(where,snprintftos(
          "%*s %3s %11s %11s %s/",
          w,"","","","",dvtos(info.increase,6,1).c_str()));
        logTab(where,snprintftos(
          "%*s %3s %11s %11s %s",
          w,"","","","", dvtos(info.rotation,6,3).c_str()));
        logTab(where,snprintftos(
          "%*s %3s %11s %11s %s",
          w,"","","","", dvtos(info.distance,6,3).c_str()));
      }
      logTab(where,"---");
    }
    logTableEnd(where);
    }}

    {{
    out::stream where = out::SUMMARY;
    logTableTop(where,"Rigid Body Grid Search Summary");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    int w = itow(DAGDB.NODES.size());
    logTab(where,snprintftos(
        "%*s %3s %11s %11s  %s",
        w,"#","cyc","LLG-input","LLG-final","Moved?[+/-]"));
    for (int n = 0; n < DAGDB.size(); n++)
    {
      for (int c = 0; c < ncyc; c++)
      {
        auto& info = macrocycle[n][c];
        logTab(where,snprintftos(
          "%*d %3d %11.2f %11.2f  %s",
          w,n+1,c+1,
          info.initial_llg, info.final_llg,
          info.jogged.c_str()
          ));
      }
      int io_nprint = 20;
      if (n == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
    }
    logTableEnd(where);
    }}

    {{
    logUnderLine(out::VERBOSE,"Pose perturbations incorporated");
    for (auto& node : DAGDB.NODES)
    {
      node.cleanUp();
    }
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
