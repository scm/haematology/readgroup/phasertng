//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved

    Variance& variance = search->VARIANCE;
    variance.SetModelsFcalc(search->FCALCS); //deep copy constructor
    search->FCALCS = ModelsFcalc(); //clear the memory, in_memory=false because MILLER is empty
    auto SigmaP = variance.sigmap_with_binning();
    variance.apply_sigmap(SigmaP); //convert to evalues
    variance.REFLID = REFLECTIONS->reflid();
    variance.MODLID = search->identify();
    variance.TIMESTAMP = TimeStamp();

    if (search->MAP)
      throw Error(err::FATAL,"Model (" + models.REFLID.str() + ") is a map");
    if (models.REFLID != REFLECTIONS->reflid() and models.REFLID.is_set_valid())
      throw Error(err::FATAL,"Model (" + models.REFLID.str() + ") prepared with different reflection file (" + REFLECTIONS->reflid().str() + ")");
    int nmodels = models.PDB.size();
    if (models.SERIAL.size() != nmodels)
      throw Error(err::FATAL,"Model has no VRMS estimations");
    if (nmodels != variance.FCALC.num_rows())
      throw Error(err::FATAL,"Model (" + itos(nmodels) + ") and Variance structure factors (" + itos(variance.FCALC.size()) + ") model sizes don't match");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Ensemble Configuration");
    if (search->ATOM)
    throw Error(err::INPUT,"Cannot build ensemble from single atom");
    logTab(where,"Ensemble configured to resolution " + dtos(config_hires,5,2));
    logTab(where,"Ensemble configured with number of models = " + itos(nmodels));
    phaser_assert(nmodels);
    search->HIRES = config_hires;
    if (!models.has_relative_wilson_bfactor())
    throw Error(err::INPUT,"Ensemble Wilson-B not set");
    if (models.bfactors_damped_after_relative_wilson_b())
    logWarning("Ensemble Wilson-B damps B-factors");
    }}

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Ensemble Vrms Estimation");
    logTab(where,"DRMS lower/upper   : " + dtos(models.SERIAL.drms_lower(),8,6) + " / " + dtos(models.SERIAL.drms_upper(),8,6));
    logTab(where,"VRMS input value(s): " + dvtos(models.SERIAL.vrms_input,8,6));
    logTab(where,"VRMS start value(s): " + dvtos(models.SERIAL.vrms_start,8,6));
    logTab(where,"VRMS upper limit(s): " + dvtos(models.SERIAL.vrms_upper,8,6));
    logTab(where,"VRMS lower limit(s): " + dvtos(models.SERIAL.vrms_lower,8,6));
    logBlank(where);
    if (!models.SERIAL.is_serial())
    throw Error(err::INPUT,"Ensemble vrms estimations not set");
    search->DRMS_MIN = models.SERIAL.drms_lower();
    search->DRMS_MAX = models.SERIAL.drms_upper();
    search->VRMS_START = models.SERIAL.vrms_start;
    //the ranges will be adjusted more below
    for (int m = 0; m <  models.SERIAL.size(); m++)
    {
      const double& rms = models.SERIAL.vrms_start[m];
      const double& best_rms = models.SERIAL.vrms_lower[m];
      const double& worst_rms = models.SERIAL.vrms_upper[m];
      if (rms < best_rms)
      logWarning("Ensemble rmsd ("+dtos(rms)+") < best allowed ("+dtos(best_rms)+")");
      if (rms > worst_rms)
      logWarning("Ensemble rmsd ("+dtos(rms)+") > worst allowed ("+dtos(worst_rms)+")");
    }
    }}

    {{
    out::stream where = out::TESTING;
    double SIGA_RMSD = 0;
    double fsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.fsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.fsol.").value_or_default();
    double bsol = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.bsol.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.bsol.").value_or_default();
    double minb = REFLECTIONS->MAP ?
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_as_map_versus_model.babinet_minimum.").value_or_default():
        input.value__flt_number.at(".phasertng.solvent_sigmaa.reflections_versus_model.babinet_minimum.").value_or_default();
    FourParameterSigmaA solTerm(SIGA_RMSD,fsol,bsol,minb);

    if (models.SERIAL.size() > 1)
      logTabArray(out::LOGFILE,variance.logCalculationOfStatisticalWeighting());
    logUnderLine(where,"Ensemble Weight Calculation");
    //-------------------------------------------------------------------------------------
    {
      variance.NTHREADS = NTHREADS;
      if (Suite::Level() >= where)
      {
        variance.NTHREADS = 1;
        logBlank(where);
        logTab(where,"Output level set for debugging");
        logTab(where,"Switching to 1 thread for model weight calculation to avoid race conditions in logging");
        logBlank(where);
      }

      logEllipsisOpen(where,"Correlation matrices and LuzzatiD by resolution bin:");
      auto str = variance.calculate_modelweights(models.SERIAL.drms_lower(),models.SERIAL.vrms_lower,solTerm);
      logEllipsisShut(where);
      logTabArray(where,str);

      search->DRMS_MIN = models.SERIAL.drms_lower();
      search->DRMS_MAX = models.SERIAL.drms_upper();
      // --- OK, finally happy with the weights
    }
    logTabArray(where,variance.logVariancesAndModelWeightsByResolutionBin());
    }}

    logTabArray(out::SUMMARY,variance.logVariancesAndModelWeights());

    //now write the data as mtz file

    phaser_assert(search->VARIANCE.in_memory());
