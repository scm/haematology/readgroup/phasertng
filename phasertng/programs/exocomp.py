import os
from libtbx.program_template import ProgramTemplate
from phasertng.phil.master_phil_file import maximum_expert_level
from libtbx.utils import Sorry
from phasertng.phil.converter_registry import *
from phasertng.voyager.voyager_generator import *
from phasertng.voyager.exocomp import *

# =============================================================================
# Program class (location should be phasertng/programs)
class Program(ProgramTemplate):

  # Class variables are used to define basic program properties
  description = voyager_generator().wrapped_text("exocomp")

  # Define PHIL scope
  master_phil_str = build_voyager_phil_file(program_name="exocomp")
  master_phil_str = master_phil_str + "\n" + build_auto_str("exocomp")

  datatypes = [ 'phil' ]

  phil_converters = additional_converters

  # Citations
  # The list of known citations is in libtbx/citations.params
  # Custom citations can be added via the "citations" class variable
  known_article_ids = ['phasertng']

  # The constructor is not needed since all it does is store the DataManager and
  # PHIL scope extract as instance variables, self.data_manager and self.params,
  # respectively. Also, a multi_out instance is created for logging (self.logger)

  def validate(self):
    # The DataManager can do some basic checks
    # More basic checks are planned and they will exist in a central location
    # Also, do checks on the parameters

    # Require at least 1 model (see iotbx/data_manager/model.py for more
    # arguments). The raise_sorry will cause a Sorry to be raised if False
    # Other datatypes have a similar function (has_sequences)
    #new self.params (phil extract)
    if (not os.path.isdir(self.params.phasertng.suite.database)):
      raise Sorry("Database does not exist")
    return

  def run(self):
    # Actual code for scientific stuff
    # The Program template is basically a wrapper for handling I/O with the user
    print('Run exocomp...', file=self.logger)
    if self.params.output.overwrite:
      print('Overwrite working directory',file=self.logger)
    self.params.phasertng.overwrite = self.params.output.overwrite
    # rmtree for directory is dangerous
    phil_scope = self.master_phil.format(python_object=self.params)  # converts extract to scope
    phil_str = phil_scope.as_str(expert_level=maximum_expert_level, attributes_level=0) # converts scope to str
    try:
      basetng = exocomp(user_phil_str=phil_str)
    except Exception as e:
      rawmsg = "" if str(e) is None else str(e)
      #the phasertng error type is encoded in the return as an integer (enum)
      err,message = rawmsg.split(maxsplit=1)
      if err.isnumeric(): #special type
        print("Exit Code:",int(err),":",message)
      else:
        print("Exit Code:",int(enum_err_code.fatal),":",rawmsg)


  def get_results(self):
    # The GUI will use this for displaying any final results that are important
    pass

# =============================================================================
# Command-line tool (location should be phaser/command_line)
from iotbx.cli_parser import run_program

if (__name__ == '__main__'):
  run_program(Program)

# Some example command-line commands
#   libtbx.python example.py
#   libtbx.python example.py --show-defaults
#   libtbx.python example.py --show-defaults --show-attributes-level=1
#   libtbx.python example.py --citations
#   libtbx.python example.py model_filename=<model file>
#   libtbx.python example.py sequence_filename=<sequence file>

# Arguments should be grouped. The "--" flags should be together.

# The CCTBXParser (iotbx.cli_parser) will automatically show the program name
# based on the dispatcher name and construct appropriate filenames for PHIL
# files. Some default filenames for the output is planned.

# For multiple ensembles in this example, a PHIL file will need to be defined
# to keep models and sequences together. Files (.type=path) in a PHIL file
# will be automatically tracked by the DataManager.

# The CCTBXParse will read and parse PHIL files
