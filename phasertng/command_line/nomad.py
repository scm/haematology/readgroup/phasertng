# LIBTBX_SET_DISPATCHER_NAME phasertng.nomad

from iotbx.cli_parser import run_program
from phasertng.programs import nomad

if (__name__ == '__main__'):
  try:
    run_program(nomad.Program)
  except Exception as e:
    print(str(e))
