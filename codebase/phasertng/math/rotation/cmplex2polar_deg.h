#ifndef __phasertng_cmplex2polar_function__
#define __phasertng_cmplex2polar_function__
#include <scitbx/constants.h>
#include <complex>

namespace phasertng {

inline std::pair<double,double> cmplex2polar_deg(std::complex<double> FCALC)
{
  //not pass by reference, as needs to return new value anyway
  double F = std::abs(FCALC);
  double PHI = scitbx::rad_as_deg(std::arg(FCALC));
  while (PHI > 180) PHI -= 360;
  while (PHI <= -180) PHI += 360;
  return {F, PHI};
}

inline double rad2phi(double rad)
{
  //not pass by reference, as needs to return new value anyway
  double PHI = scitbx::rad_as_deg(rad);
  while (PHI > 180) PHI -= 360;
  while (PHI <= -180) PHI += 360;
  return PHI;
}

}
#endif
