//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <fstream>

namespace phasertng {
namespace dag {

  void
  DagDatabase::node_unparse_phil(const dag::Node& node,std::ostream& ss) const
  {
    //unparses node to ss
    //does not unparse the ENTRY part of the node: this has to be done from DagDatabase
    //does not unparse the TRACKER part of the node: this is done from DagDatabase
    //does not unparse the PATHWAY part of the node: this has to be done from DagDatabase
    //does not unparse the PATHLOG part of the node: this has to be done from DagDatabase
    auto text = node_unparse(node);
    for (auto item : text)
    {
      if (!item.first.size())
      {
         ss << "    }" << std::endl;
      }
      else
      {
        char ch = item.first[0];
        item.first.erase(0,1);
        if (ch == '-')
           ss << "    " << item.first << " {" << std::endl;
        else if (ch == '_')
           ss << "    " << item.first << " = " << item.second << std::endl;
        else if (ch == '=' or ch == ' ')
           ss << "      " << item.first << " = " << item.second << std::endl;
        else
          assert(false);
      }
    }
  }

  std::string
  DagDatabase::work_phil_str() const
  {
    //does not unparse the ENTRY part of the node[ this has to be done from DagDatabase
    //does not unparse the TRACKER part of the node[ this is done from DagDatabase
    //does not unparse the PATHWAY part of the node[ this has to be done from DagDatabase
    //does not unparse the PATHLOG part of the node[ this has to be done from DagDatabase
    std::ostringstream ss;
    auto text = node_unparse(*WORK);
    for (auto item : text)
    {
      if (!item.first.size())
      {
         ss << "    }" << std::endl;
      }
      else
      {
        char ch = item.first[0];
        item.first.erase(0,1);
        if (ch == '-')
           ss << "    " << item.first << " {" << std::endl;
        else if (ch == '_')
           ss << "    " << item.first << " = " << item.second << std::endl;
        else if (ch == '=' or ch == ' ')
           ss << "      " << item.first << " = " << item.second << std::endl;
        else
          assert(false);
      }
    }
    return ss.str();
  }

  std::string
  DagDatabase::entry_unparse_phil(const dag::Entry& entry) const
  {
    std::string cards;
    cards += "    entry {\n";
    auto text = entry_unparse(entry);
    for (auto item : text)
      cards += "     " + item.first + "= " + item.second + "\n";
    cards += "    }\n";
    return cards;
  }

  std::string
  DagDatabase::reflid_unparse_phil(const DogTag& reflid) const
  {
    std::string cards;
    cards += "    reflections {\n";
    cards += "      id = " + reflid.id_unparse() + "\n";
    cards += "      tag = " + reflid.tag_unparse() + "\n";
    cards += "    }\n";
    return cards;
  }

  std::string
  DagDatabase::edge_unparse_phil(const dag::Edge& edge,std::string key) const
  {
    std::string cards;
    cards += "  " + key + " {\n";
    {
    cards += "    ultimate {\n";
    auto text = edge_unparse(edge,true);
    for (auto item : text)
      cards += "     " + item.first + "= " + item.second + "\n";
    cards += "    }\n";
    }
    {
    cards += "    penultimate {\n";
    auto text = edge_unparse(edge,false);
    for (auto item : text)
      cards += "     " + item.first + "= " + item.second + "\n";
    cards += "    }\n";
    }
    cards += "  }\n";
    return cards;
  }

  std::string
  DagDatabase::node_phil_str(int i)
  {
    std::ostringstream ss;
    {
      auto& node = NODES[i];
      ss << NODE_SCOPE() << " {" << std::endl;
      ss << edge_unparse_phil(PATHWAY,"pathway");
      ss << edge_unparse_phil(PATHLOG,"pathlog");
      ss << edge_unparse_phil(node.TRACKER,"tracker");
      ss << "  node {" << std::endl;
      node_unparse_phil(node,ss);
      std::map<Identifier,std::string> entries;
      for (const auto& entry : ENTRIES)
      { //unparse all entries to node, overkill but much faster on write
        entries[entry.first] = entry_unparse_phil(entry.second);
      }
      std::map<Identifier,std::string> reflids;
      for (const auto& reflid : REFLIDS)
      { //unparse all reflids to node, overkill but much faster on write
        reflids[reflid.first] = reflid_unparse_phil(reflid.second);
      }
      auto used_modlid = node.used_modlid();
      for (const auto& e : used_modlid)
        ss << entries[e];
      for (const auto& e : reflids)
        ss << e.second;
      ss << "  }" << std::endl;
      ss << "}" << std::endl;
    }
    return ss.str();
  }

  void
  DagDatabase::unparse_phil(FileSystem DAGFILE)
  {
    std::fstream ss;
    DAGFILE.create_directories_and_stream(ss);
    //store the components for unparsing that do not change
    for (int i = 0; i < NODES.size(); i++)
    {
      ss << node_phil_str(i);
    }
    ss.close();
  }

  std::string
  DagDatabase::phil_str()
  {
    std::stringstream ss;
    for (int i = 0; i < NODES.size(); i++)
    {
      ss << node_phil_str(i);
    }
    return ss.str();
  }

}} //phasertng
