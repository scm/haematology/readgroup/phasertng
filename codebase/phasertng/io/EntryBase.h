//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_io_EntryBase_class__
#define __phasertng_io_EntryBase_class__
#include <phasertng/io/FileSystem.h>
//#include <phasertng/io/MtzHandler.h>
#include <phasertng/main/jiffy.h>

namespace phasertng {

//this is an abstract base class by virtue of inheritance from FileSystem

class EntryBase : public FileSystem
{
  public:
    EntryBase() { }
    virtual ~EntryBase() {}

    virtual bool in_memory() = 0;
    virtual void read_from_disk() = 0;
    virtual void write_to_disk() = 0;
};

} //phasertng

#endif
