//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/int_paired.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  int_paired::int_paired(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const std::pair<int,int> dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void
  int_paired::set_value(std::pair<int,int> other) //allow in place contructor {x,y}
  {
    user = true;
    uvalue = other;
  }

  void
  int_paired::set_value(const sv_string& vcards,std::pair<int,int> other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  int_paired::is_default() const
  { return uvalue == dephalt; }

  void
  int_paired::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  int_paired::to_python()
  { return  { unparse_parameter(false) }; }

  void
  int_paired::from_python(std::string str)
  {
    sv_string txt = to_words(str);
    set_value({std::stoi(txt[0]),std::stoi(txt[1])});
  }

  std::pair<int,int>
  int_paired::value_or_default() const
  { return user ? uvalue : dephalt; }

  int
  int_paired::value_or_default_min() const
  {
    std::pair<int,int> tmp = user ? uvalue : dephalt;
    return tmp.first < tmp.second ? tmp.first : tmp.second;
  }

  int
  int_paired::value_or_default_max() const
  {
    std::pair<int,int> tmp = user ? uvalue : dephalt;
    return tmp.first < tmp.second ? tmp.second : tmp.first;
  }

  std::string
  int_paired::unparse_parameter(bool phil) const
  {
    if (value_or_default() == std::pair<int,int>( DEF_INT_FLAG, DEF_INT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << uvalue.first << " " << uvalue.second;
    return ss.str();
  }

  std::string
  int_paired::show_default(bool phil) const
  {
    if (dephalt == std::pair<int,int>( DEF_INT_FLAG, DEF_INT_FLAG ))
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << dephalt.first << " " << dephalt.second;
    return is_default() ? None() : ss.str();
  }

}}
