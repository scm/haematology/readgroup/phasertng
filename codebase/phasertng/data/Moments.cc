//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Moments.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/data/Reflections.h>
#include <scitbx/math/erf.h>

namespace phasertng {

  Moments::Moments( const_ReflectionsPtr  REFLECTIONS_) :
      REFLECTIONS(REFLECTIONS_)
  {
  }

  void
  Moments::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  bool
  Moments::check_data_and_restrict_resolution(bool use_xtriage_data_selection,const sv_bool& selected)
  {
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::INAT));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    NO_I = !bool(REFLECTIONS->INTENSITIES);
    // For overall statistics, restrict data to resolution range where measurement
    // error will not affect the moments significantly, exclude individual reflections
    // where error would dominate, and ignore error.
    // For bin-wise statistics, account for expected effect of measurement error.
    int NUMBINS = REFLECTIONS->numbins();
    HiRes = REFLECTIONS->bin_hires(0);
    LoRes = REFLECTIONS->bin_lores(0);
    max_s = NUMBINS;
    hires = HiRes.back();
    if (use_xtriage_data_selection)
    { //set twin bin reso
      sv_double IonSIGIover3(NUMBINS,0);
      sv_double NumInBinIonSIGI(NUMBINS,0);
      sv_double NumInBin(NUMBINS,0);
      double maxSIGI(0.0);
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const double iobs = work_row->get_inat();
        const double sigi = work_row->get_siginat();
        const int& s = work_row->get_bin();
        if (REFLECTIONS->get_present(friedel::NAT,r))
        {
          if (selected[r])
          {
            maxSIGI = std::max(maxSIGI,sigi);
            if (sigi)
            {
              if (iobs/sigi > 3)
                IonSIGIover3[s]++;
              NumInBinIonSIGI[s]++;
            }
            NumInBin[s]++;
          }
        }
      }
      //bail out if all the SIGIs are zero, faked I's
      // AJM change - don't bail, just assume the data is bad and take over 3.5A data below
      //we throw a warning
      NO_SIGI = (maxSIGI == 0);
      //if (maxSIGI == 0) return;
      for (int s = 0; s < NUMBINS; s++) //bin before the break
      {
        double fraction_over_3_sigma = NumInBinIonSIGI[s] ? IonSIGIover3[s]/NumInBinIonSIGI[s] : 0;
        bool low_isigi = NO_SIGI or (fraction_over_3_sigma < 0.85);
        if (!NumInBin[s] or (low_isigi and HiRes[s] < 3.5))
        {
          max_s = s;
          break; //xtriage limits
        }
        hires = HiRes[s]; //the bin before break
      }
    }
    return (max_s != 0);
  }

  void
  Moments::setup_moments(int datatype,const sv_bool& selected)
  {
    //SIGMAN is only used when datatype = 0, after initialization of Moments
    SIGMAN = REFLECTIONS->CalculateSigmaN();
    meanE2_acent = meanE4_acent = expectE4_acent = sigmaE4_acent = 0;
    meanE2_cent = meanE4_cent = expectE4_cent = sigmaE4_cent = 0;
    has_centric = has_acentric = false;
    meanE2_acent_bin.clear();meanE4_acent_bin.clear();expectE4_acent_bin.clear();
    meanE2_cent_bin.clear(); meanE4_cent_bin.clear(); expectE4_cent_bin.clear();

    //initialize true/false as absent or not
    double sumwt2_cent(0.),sumwt2_acent(0.),sumwt4_cent(0.),sumwt4_acent(0.);
    int NUMBINS = REFLECTIONS->numbins();
    sv_int ncentsel_bin(NUMBINS,0);
    sv_int nacentsel_bin(NUMBINS,0);
    sv_double sumwt2_cent_bin(NUMBINS,0.), sumwt4_cent_bin(NUMBINS,0.);
    sv_double sumwt2_acent_bin(NUMBINS,0.),sumwt4_acent_bin(NUMBINS,0.);
    meanE2_cent_bin.resize(NUMBINS);
    meanE4_cent_bin.resize(NUMBINS);
    expectE4_cent_bin.resize(NUMBINS);
    meanE2_acent_bin.resize(NUMBINS);
    meanE4_acent_bin.resize(NUMBINS);
    expectE4_acent_bin.resize(NUMBINS);
    meanE2_cent_bin.fill(0);
    meanE4_cent_bin.fill(0);
    expectE4_cent_bin.fill(0);
    meanE2_acent_bin.fill(0);
    meanE4_acent_bin.fill(0);
    expectE4_acent_bin.fill(0);
    if (datatype == 0) phaser_assert(REFLECTIONS->has_col(labin::EPS));
    if (datatype >= 1) phaser_assert(REFLECTIONS->has_col(labin::RESN));
    if (datatype == 2) phaser_assert(REFLECTIONS->has_col(labin::TEPS));
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (REFLECTIONS->get_present(friedel::NAT,r))
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const int s = work_row->get_bin();
        const bool   cent = work_row->get_cent();
        const double iobs = work_row->get_inat();
        const double sigi = work_row->get_siginat();
        double epsnSigmaN = epsn_sigman(datatype,r);
        if (selected[r])
        {
          if (epsnSigmaN == 0) continue;
          // For French-Wilson, turn <F> and variance into <F^2> to estimate I
          // but keep SIGIdat as surrogate for true intensity sigma
          double Esqr(iobs/epsnSigmaN);
          double SigEsqr(sigi/epsnSigmaN);
          double VarEsqr(fn::pow2(SigEsqr));
          double E4(fn::pow2(Esqr));
          // Below, contributions to the moments are weighted using the variance of
          // each contribution.  If Z=E^2 and the observed Z is Zo=Z+dZ where dZ
          // is a Gaussian measurement error independent of Z:
          // var(Zo) = <(Zo-<Zo>)^2 = <Zo^2> - <Zo>^2
          // <Zo> = <Z> + <dZ> = 1
          // <Zo^2> = <(Z+dZ)^2> = <Z^2> + 2<Z><dZ> + <dZ>^2
          // Odd moments of dZ are zero, so
          // <Zo^2> = <Z^2> + <dZ>^2, and <dZ>^2 = sigma(Zo)^2 and then
          // var(Zo) = <Z^2> + sigma(Zo)^2 - 1
          // Similar reasoning applies to the variance of Zo^2:
          // var(Zo^2) = <Z^4> + 6*<Z^2><dZ^2> + <dZ^4> - (<Z^2> + <dZ^2>)^2
          //           = <Z^4> + 6*<Z^2>sigma(Zo)^2 + 3*sigma(Zo)^4 -
          //             <Z^2>^2 -2*<Z^2>sigma(Zo)^2 - sigma(Zo)^4
          //           = <Z^4> -<Z^2>^2 + 4*<Z^2>sigma(Zo)^2 + 2*sigma(Zo)^4
          // Finally, for centrics, <Z^4> = 105 and <Z^2> = 3
          // and for acentrics, <Z^4> = 24 and <Z^2> = 2
          if (cent)
          {
            double wt2(1./(2.+VarEsqr));
            double wt4(1./(96.+12*VarEsqr+2*fn::pow2(VarEsqr)));
            if (s < max_s)
            {// Overall moments for data not dominated by errors
              has_centric = true;
              sumwt2_cent += wt2;
              sumwt4_cent += wt4;
              meanE2_cent += wt2*Esqr;
              meanE4_cent += wt4*E4;
              expectE4_cent += wt4*(3. + VarEsqr);
            }
            ncentsel_bin[s]++; // Binwise stats account for errors
            sumwt2_cent_bin[s] += wt2;
            sumwt4_cent_bin[s] += wt4;
            meanE2_cent_bin[s] += wt2*Esqr;
            meanE4_cent_bin[s] += wt4*E4;
            expectE4_cent_bin[s] += wt4*(3. + VarEsqr);
          }
          else
          {
            double wt2(1./(1.+VarEsqr));
            double wt4(1./(20.+8*VarEsqr+2*fn::pow2(VarEsqr)));
            if (s < max_s)
            {// Overall moments for data not dominated by errors
              has_acentric = true;
              sumwt2_acent += wt2;
              sumwt4_acent += wt4;
              meanE2_acent += wt2*Esqr;
              meanE4_acent += wt4*E4;
              expectE4_acent += wt4*(2. + VarEsqr);
            }
            nacentsel_bin[s]++; // Binwise stats account for errors
            sumwt2_acent_bin[s] += wt2;
            sumwt4_acent_bin[s] += wt4;
            meanE2_acent_bin[s] += wt2*Esqr;
            meanE4_acent_bin[s] += wt4*E4;
            expectE4_acent_bin[s] += wt4*(2. + VarEsqr);
          }
        }
      }
    }
    if (has_centric)
    {
      meanE2_cent /= sumwt2_cent;
      meanE4_cent /= sumwt4_cent;
      expectE4_cent /= sumwt4_cent;
      sigmaE4_cent = std::sqrt(1./sumwt4_cent);
      for (int s = 0; s < NUMBINS; s++)
      {
        if (ncentsel_bin[s])
        {
          meanE2_cent_bin[s]   /= sumwt2_cent_bin[s];
          meanE4_cent_bin[s]   /= sumwt4_cent_bin[s];
          expectE4_cent_bin[s] /= sumwt4_cent_bin[s];
        }
        else expectE4_cent_bin[s] = 3.; // Something sensible for empty bins
      }
    }
    else
    {
      expectE4_cent = 3.;
    }
    if (has_acentric)
    {
      meanE2_acent /= sumwt2_acent;
      meanE4_acent /= sumwt4_acent;
      expectE4_acent /= sumwt4_acent;
      sigmaE4_acent = std::sqrt(1./sumwt4_acent);
      for (int s = 0; s < NUMBINS; s++)
      {
        if (nacentsel_bin[s])
        {
          meanE2_acent_bin[s]   /= sumwt2_acent_bin[s];
          meanE4_acent_bin[s]   /= sumwt4_acent_bin[s];
          expectE4_acent_bin[s] /= sumwt4_acent_bin[s];
        }
        else expectE4_acent_bin[s] = 2.;
      }
    }
    else
    {
      expectE4_acent = 2.;
    }
  }

  std::pair<double,double>
  Moments::second_moments_acentric(int sigfig)
  {
    if (sigfig < 0)
      return {meanE4_acent,sigmaE4_acent};
    double sigfigs(10*sigfig); //2
    double mean = std::round(meanE4_acent * sigfigs) / sigfigs;
    double sigma = std::round(sigmaE4_acent * sigfigs) / sigfigs;
    return {mean,sigma};
  }

  std::pair<double,double>
  Moments::second_moments_expected(int sigfig)
  {
    if (sigfig < 0)
      return {expectE4_cent,expectE4_acent};
    double sigfigs(10*sigfig); //2
    double cent = std::round(expectE4_cent * sigfigs) / sigfigs;
    double acent = std::round(expectE4_acent * sigfigs) / sigfigs;
    return {cent,acent};
  }

  double
  Moments::second_moments_centric(int sigfig)
  {
    if (sigfig < 0) return meanE4_cent;
    double sigfigs(10*sigfig); //2
    double mean = std::round(meanE4_cent * sigfigs) / sigfigs;
    return mean;
  }

  void
  Moments::setup_distributions(int datatype,const sv_bool& selected)
  {
    phaser_assert(REFLECTIONS->has_col(labin::BIN));
    phaser_assert(REFLECTIONS->has_col(labin::RESN));
    phaser_assert(REFLECTIONS->has_col(labin::INAT));
    phaser_assert(REFLECTIONS->has_col(labin::SIGINAT));
    ncentsel = 0;
    nacentsel = 0;
    cumulative_acentric.resize(nplot());
    cumulative_centric.resize(nplot());
    cumulative_acentric.fill(0);
    cumulative_centric.fill(0);
    if (datatype == 0) phaser_assert(REFLECTIONS->has_col(labin::EPS));
    if (datatype >= 1) phaser_assert(REFLECTIONS->has_col(labin::RESN));
    if (datatype == 2) phaser_assert(REFLECTIONS->has_col(labin::TEPS));
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      if (REFLECTIONS->get_present(friedel::NAT,r))
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const int    s    = work_row->get_bin();
        const double iobs = work_row->get_inat();
        const bool   cent = work_row->get_cent();
        double epsnSigmaN = epsn_sigman(datatype,r);
        if (selected[r] and s < max_s)
        {
          if (epsnSigmaN == 0) continue;
          double Esqr = iobs/epsnSigmaN;
          if (cent)
          {
            ncentsel++;
            for (int t = nplot()-1; t >= std::ceil(std::max(0.,Esqr)/deltax()); t--)
              cumulative_centric[t]++;
          }
          else
          {
            nacentsel++;
            for (int t = nplot()-1; t >= std::ceil(std::max(0.,Esqr)/deltax()); t--)
              cumulative_acentric[t]++;
          }
        }
      }
    }
  }

  double
  Moments::alpha()
  {
    return alpha_from_meanE4(meanE4_acent);
  }

  double
  Moments::pvalue(int sigfig)
  {
    double pval = (meanE4_acent >= 2 || meanE4_acent <= 0) ?
       1.0 : scitbx::math::erfc((2-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
    if (sigfig < 0) return pval;
    double sigfigs(10*sigfig);
    pval = std::round(pval * sigfigs) / sigfigs;
    return pval;
  }

  double
  Moments::ptwin5percent(int sigfig)
  {
    double pval = (meanE4_acent >= 1.905 || meanE4_acent <= 0) ?
       1.0 : scitbx::math::erfc((1.905-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
    if (sigfig < 0) return pval;
    double sigfigs(10*sigfig);
    pval = std::round(pval * sigfigs) / sigfigs;
    return pval;
  }

  double
  Moments::ptwinnedless(int sigfig)
  {
    double expectE4a(expectE4a_from_alpha(twinfrac()));
    double pval = (meanE4_acent >= expectE4a || meanE4_acent <= 0) ?
       1.0 : scitbx::math::erfc((expectE4a-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
    if (sigfig < 0) return pval;
    double sigfigs(10*sigfig);
    pval = std::round(pval * sigfigs) / sigfigs;
    return pval;
  }

  std::pair<double,double>
  Moments::alpha_conf_int()
  {
    // 99% confidence interval for hemihedral alpha (for 95%, use +/- 1.96sigma
    // However, the assumptions underlying this are questionable in the presence of tNCS
    return std::pair<double,double>
                    (alpha_from_meanE4(meanE4_acent+2.57583*sigmaE4_acent),
                     alpha_from_meanE4(meanE4_acent-2.57583*sigmaE4_acent));
  }

  double
  Moments::alpha_from_meanE4(double meanE4a)
  {
    if (!meanE4a) return 0;
    if (meanE4a< 1.5002)
      return 0.5; //value for alpha = 0.49: between 1.5 and 1.5002
    return std::max(0.0,((1 - std::sqrt(-3 + 2*meanE4a))/2.));
  }

  double
  Moments::expectE4a_from_alpha(double alpha)
  {
    double alpha2 = std::max(0.,std::min(alpha,1.-alpha));
    return 2.*(1.-alpha2+fn::pow2(alpha2));
  }

  double
  Moments::acentric(int s)
  {
    sv_double acen{0.0,0.0392106,0.0768837,0.1130796,0.1478562,0.1812692,0.2133721,0.2442163,0.2738510,0.3023237,0.3296800,0.3559636,0.3812166,0.4054795,0.4287909,0.4511884,0.4727076,0.4933830,0.5132477,0.5323336,0.5506710,0.5682895,0.5852171,0.6014810,0.6171071,0.6321206,0.6465453,0.6604045,0.6737202,0.6865138,0.6988058,0.7106158,0.7219627,0.7328647,0.7433392,0.7534030,0.7630722,0.7723623,0.7812881,0.7898639,0.7981035,0.8060200,0.8136260,0.8209339,0.8279551,0.8347011,0.8411826,0.8474099,0.8533930,0.8591416,0.8646647};
     return acen[s];
  }

  double
  Moments::centric(int s)
  {
    sv_double  cen{ 0.0,0.1585194,0.2227026,0.2709655,0.3108435,0.3452792,0.3757939,0.4032988,0.4283924,0.4514938,0.4729107,0.4928775,0.5115777,0.5291583,0.5457398,0.5614220,0.5762892,0.5904133,0.6038561,0.6166715,0.6289066,0.6406032,0.6517983,0.6625250,0.6728131,0.6826895,0.6921785,0.7013024,0.7100815,0.7185345,0.7266783,0.7345289,0.7421010,0.7494079,0.7564625,0.7632764,0.7698607,0.7762255,0.7823805,0.7883348,0.7940968,0.7996745,0.8050755,0.8103070,0.8153755,0.8202875,0.8250491,0.8296659,0.8341433,0.8384867,0.8427008};
    return cen[s];
  }

  double
  Moments::acentric_twinned(int s)
  {
    sv_double  acen_twin{ 0.0,0.0030343,0.0115132,0.0245815,0.0414833,0.0615519,0.0842006,0.1089139,0.1352404,0.1627861,0.1912079,0.2202081,0.2495299,0.2789524,0.3082868,0.3373727,0.3660750,0.3942806,0.4218963,0.4488460,0.4750691,0.5005177,0.5251562,0.5489585,0.5719077,0.5939942,0.6152149,0.6355726,0.6550744,0.6737317,0.6915590,0.7085736,0.7247951,0.7402450,0.7549459,0.7689218,0.7821971,0.7947971,0.8067470,0.8180725,0.8287987,0.8389511,0.8485543,0.8576328,0.8662106,0.8743109,0.8819565,0.8891694,0.8959710,0.9023818,0.9084218};
    return acen_twin[s];
  }

  bool
  Moments::twinned()
  {
    if (is_set())
      return ptwinnedless() < plimit();
    return false;
  }

  af_string
  Moments::logMoments(std::string description)
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Weighted Intensity Moments: " + description + "<<");
    output.push_back("Inverse variance-weighted 2nd Moment = <wE^4>/<wE^2>^2 == <wJ^2>/<wJ>^2");
    output.push_back("                     Untwinned   Perfect Twin");
    output.push_back("2nd Moment  Centric:    3.0          2.0");
    output.push_back("2nd Moment Acentric:    2.0          1.5");
    output.push_back("                            Measured");
    PHASER_ASSERT(meanE2_cent or meanE2_acent);
    if (meanE2_cent) //will be zero if no centric reflection
      output.push_back("2nd Moment  Centric:      " + dtos(meanE4_cent,7,2));
    if (meanE2_acent) //will be zero if no acentric reflections
      output.push_back("2nd Moment Acentric:      " + dtos(meanE4_acent,7,2));
    if (false) //previously to FURTHER
    {
      if (meanE2_cent) //will be zero if no centric reflection
        output.push_back("<E^2> for  Centric: " + dtos(meanE2_cent,7,4) + " (check=1)");
      if (meanE2_acent) //will be zero if no acentric reflections
        output.push_back("<E^2> for Acentric " + dtos(meanE2_acent,7,4) + " (check=1)");
    }
    output.push_back("");
    return output;
  }

  Loggraph
  Moments::logGraph1(std::string description)
  {
    Loggraph loggraph;
  //  if (NO_SIGI) return loggraph;
  //theoretical values taken from ctruncate
    if (!has_centric and !has_acentric)
    {
      loggraph.title = "Intensity distribution " + description;
      loggraph.scatter = false;
      loggraph.graph.resize(1);
      loggraph.data_labels = "Z ACent_theor ACent_twin Cent_theor";
      for (unsigned t = 0; t < nplot(); t++)
      {
        loggraph.data_text += dtos(deltax()*t) + " " +
                              dtos(acentric(t)) + " " +
                              dtos(acentric_twinned(t)) + " " +
                              dtos(centric(t)) + " " +
                              "\n";
      }
      loggraph.graph[0] = "Cumulative intensity distribution (acentric and centric):N:1,2,3,4";
    }
    else if (has_centric and has_acentric)
    {
      loggraph.title = "Intensity distribution " + description;
      loggraph.scatter = false;
      loggraph.graph.resize(1);
      loggraph.data_labels = "Z ACent_theor ACent_twin ACent_obser Cent_theor Cent_obser";
      for (unsigned t = 0; t < nplot(); t++)
      {
        loggraph.data_text += dtos(deltax()*t) + " " +
                              dtos(acentric(t)) + " " +
                              dtos(acentric_twinned(t)) + " " +
                              dtos(cumulative_acentric[t]/nacentsel) + " " +
                              dtos(centric(t)) + " " +
                              dtos(cumulative_centric[t]/ncentsel) + " " +
                              "\n";
      }
      loggraph.graph[0] = "Cumulative intensity distribution (acentric and centric):N:1,2,3,4,5,6";
    }
    else if (has_centric)
    {
      loggraph.title = "Intensity distribution " + description;
      loggraph.scatter = false;
      loggraph.graph.resize(1);
      loggraph.data_labels = "Z Cent_theor Cent_obser";
      for (unsigned t = 0; t < nplot(); t++)
      {
        loggraph.data_text += dtos(deltax()*t) + " " +
        dtos(centric(t)) + " " +
        dtos(cumulative_centric[t]/ncentsel) + " " +
        "\n";
      }
      loggraph.graph[0] = "Cumulative intensity distribution (centric only):N:1,2,3";
    }
    else if (has_acentric)
    {
      loggraph.title = "Intensity distribution " + description;
      loggraph.scatter = false;
      loggraph.graph.resize(1);
      loggraph.data_labels = "Z ACent_theor ACent_twin ACent_obser";
      for (unsigned t = 0; t < nplot(); t++)
      {
        loggraph.data_text += dtos(deltax()*t) + " " +
        dtos(acentric(t)) + " " +
        dtos(acentric_twinned(t)) + " " +
        dtos(cumulative_acentric[t]/nacentsel) + " " +
        "\n";
      }
      loggraph.graph[0] = "Cumulative intensity distribution (acentric only):N:1,2,3,4";
    }
    return loggraph;
  }

  Loggraph
  Moments::logGraph2(std::string description)
  {
    Loggraph loggraph2;
    if (NO_SIGI) return loggraph2;
    if (nacentsel) // 2nd moment vs resolution: only acentrics
    {
      loggraph2.title = "Weighted second moments " + description;
      loggraph2.scatter = false;
      loggraph2.graph.resize(1);
      loggraph2.data_labels = "1/d^2 ACent_theor ACent_twin ACent_obser";
      for (unsigned s = 0; s < HiRes.size(); s++)
      {
        double loS = 1/LoRes[s]; //from Bin calculation, so that it is the same
        double hiS = 1/HiRes[s];
        double midres = 1/(std::sqrt((loS*loS+hiS*hiS)/2));
        loggraph2.data_text += dtos(1.0/fn::pow2(midres),5,4) + " " +
                               dtos(expectE4_acent_bin[s]) + " " +
                               dtos(expectE4_acent_bin[s]-0.5) + " " +
                               dtos(meanE4_acent_bin[s]) + " " +
                               "\n";
      }
      loggraph2.graph[0] = "Weighted second moments vs resolution (acentric only):AUTO:1,2,3,4";
    }
    return loggraph2;
  }

  double
  Moments::epsn_sigman(int datatype,int r) const
  {
    const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
    double epsnSigmaN;
    if (datatype == 2)
    {
      const double resn = work_row->get_resn();
      const double teps = work_row->get_teps();
      epsnSigmaN = fn::pow2(resn)*teps;
    }
    else if (datatype == 1)
    {
      const double resn = work_row->get_resn();
      epsnSigmaN = fn::pow2(resn);
    }
    else if (datatype == 0)//raw data
    {
      const millnx miller = work_row->get_miller();
      const double ssqr = work_row->get_ssqr();
      const int s = work_row->get_bin();
      const double eps = work_row->get_eps();
      epsnSigmaN = eps*SIGMAN.term(miller,ssqr,s);
    }
    return epsnSigmaN;
  }

  MomentsData
  Moments::get_data()
  {
    MomentsData data;
    data.centric_mean = second_moments_centric(-999);
    data.acentric_mean = second_moments_acentric(-999).first;
    data.acentric_sigma = second_moments_acentric(-999).second;
    data.pvalue = pvalue(-999);
    data.ptwin = ptwinnedless(-999);
    return data;
  }

}//phasertng
