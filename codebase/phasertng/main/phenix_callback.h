//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng__phenix_callback__class__
#define __phasertng__phenix_callback__class__
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/shared_ptr.hpp>

namespace phasertng {

struct phenix_callback {
  virtual ~phenix_callback();
  virtual void startProgressBar(const std::string& label, int size);
  virtual void incrementProgressBar();
  virtual void endProgressBar();
  virtual void warn(const std::string& message);
  virtual void advise(const std::string& message);
  virtual void loggraph(const std::string& title, const std::string& data);
  virtual void call_back(const std::string& message, const std::string& data);
};

} //phaser

#endif
