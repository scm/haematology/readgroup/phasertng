#note that the .multiple keywords must have no defaults for all values

#otherwise parse and merge with phil files fails to output the values that are defaults
#and hence the parsing fails
#think this has been fixed, at least when there is a single entry with only the defaults set

''' ,
    THIS IS THE MASTER INPUT FILE USED FOR AUTOMATIC CODE GENERATION
    AFTER MODIFYING THIS FILE RUN COMPILATION THROUGH SCONS WITH --dev
    WHICH WILL GENERATE THE FILES AND COPY THEM TO THE PHASER CODEBASE DIRECTORY
''' ,

''' NOTE that this file must contain only phil input restricted to one level of nesting
    as automatic code generation cannot generate more than single values and lists of values.
    No lists of lists. This also simplifies gui generation considerably.
'''
''' This file has two parameters and two dictionaries and one function
    Parameters:
      phil_scope
        - the outer scope name for the phil file
        - used in the auto-code generation
    Dictionaries:
      implemented
        - key is the phil keyword under the phil_scope
        - value is the phil input associated with that keyword
    Function:
       build_phil_str
        - builds a phil string from the dictionaries

   Expert levels:
   0 - input (depending on mode and data preparation)
   1 - basic (change when required)
   2 - expert (don't change)
   3 - logging (changes reporting from a node)
   --maximum_expert_level = 3 (below)
   --limit for interpretation of keywords in program template
   --maximum_expert_level can be changed without recompilation to use higher levels in program template
   --or expert_level of a keyword can be changed to make individual keywords interpretable
   --this limit does not apply to calling voyager scripts directly e.g. python picard.py
   4 - scripting (results and control)
   5 - debugging (developer only)
'''

maximum_expert_level = 3 # for documentation and interpretation in the program template
phil_scope = "phasertng"
uuid_w = int(10) # or 20, same as in phasertng


from phasertng.phil.mode_generator import *

implemented = {

"mode" : mode_generator().implemented()
,

"mapin" : '''
  mapin
    .style = tng:input:+imap
    .expert_level = 0
  {
    filename = None
      .type = filesystem(directory=False,ext="map")
      .expert_level = 0
      .short_caption = Map file for map preparation in mtz format
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying map
      .help =  Tag can be used in place of identifier to define map, provided unique. By default, the tag is taken as the stem of the filename. Non-alphanumeric characters in the tag string are substituted with the underscore character.
    oversample_resolution_limit = 0.5
      .type = float(value_min=0.1)
      .expert_level = 2
      .short_caption = Resolution limit for read of oversampled maps
  }
''' ,

"reflections" : '''
  reflections
    .style = tng:input:+data+hyss+imap+imtz+ipdb tng:result:+imap+imtz
    .expert_level = 1
  {
    read_anomalous = False
      .type = bool
      .expert_level = 1
      .short_caption = Read anomalous reflections from mtz file
    resolution = 0
      .type = float(value_min=0,value_max=3.0)
      .expert_level = 1
      .short_caption = Resolution limit for data read
      .help = Resolution limit for data read. The resolution of the data must be better than 3 Angstroms for the Wilson scaling to be effective. Molecular replacement can be performed at lower resolutions by either limiting the ensemble resolution or adding a resolution limit.
    wavelength = None
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Wavelength of data collections
    evalues = False
      .type = bool
      .expert_level = 1
      .short_caption = Input structure factors are E-values
    read_map = False
      .type = bool
      .expert_level = 1
      .short_caption = Input is a map
    oversampling = 1
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Oversampling factor, may be a non-default for electron microscopy maps
    map_origin = 0 0 0
      .type = floats(size=3)
      .expert_level = 1
      .short_caption = Origin of the submap in whole map, in orthogonal Angstroms
  }
''' ,

"pdbin" : '''
  pdbin
    .style = tng:input:+ipdb+sscc
    .expert_level = 0
  {
    filename = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 0
      .short_caption = Single model coordinate file for data as model
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying coordinates
    convert_rmsd_to_bfac = False
      .type = bool
      .expert_level = 1
      .short_caption = B-factor column contains an rmsd on input rather than a real B-factor, usually as a result of a structure prediction protocol
    bulk_solvent
      .expert_level = 2
    {
      use = False
        .type = bool
        .expert_level = 2
        .short_caption = Apply mask bulk solvent terms
      fsol = 0.35
        .type = float(value_min=0.01,value_max=1)
        .expert_level = 2
        .short_caption = Mask bulk solvent correction solvent parameter
      bsol = 45
        .type = float
        .expert_level = 2
        .short_caption = Mask bulk solvent correction solvent parameter
    }
  }
''' ,

"space_group_expansion" : '''
  space_group_expansion
    .style = tng:input:+cca+sgx tng:result:+sgx
    .expert_level = 1
  {
    hall = None
      .type = str
      .expert_level = 1
      .short_caption = Space group Hall symbol, used for setting expansion
    spacegroup = None
      .type = str
      .expert_level = 4
      .short_caption = Space group Hermann Mauguin symbol
    hermann_mauguin_symbol = None
      .type = str
      .expert_level = 4
      .short_caption = Space group universal Hermann Mauguin symbol
    order_z_expansion_ratio = None
      .type = int(value_min=1)
      .expert_level = 4
      .short_caption = The order of the expansion of copies in the unit cell
  }
''' ,

"anisotropy" : '''
  anisotropy
    .style = tng:result:+aniso
    .expert_level = 4
  {
    sharpening_bfactor = 0
      .type = float
      .expert_level = 4
      .short_caption = Sharpening B-factor for the amplitudes
      .help = Sharpening B-factor for the data. Restores data to original B-factor falloff, before anisotropy correction sharpens the data.
    wilson_scale = 0
      .type = float
      .expert_level = 4
      .short_caption = Wilson Scale for the amplitides
    wilson_bfactor = 0
      .type = float
      .expert_level = 4
      .short_caption = Wilson-B for the amplitides
    delta_bfactor = 0
      .type = float
      .expert_level = 4
      .short_caption = Anisotropic Delta B-factor for the data
      .help = Difference in B-factor between weakest and strongest directions.
    direction_cosines = 0 0 0 0 0 0 0 0 0
      .type = floats(size=9)
      .expert_level = 4
      .short_caption = Direction cosines of anisotropy
      .help = Direction cosines in orthogonal coordinates of anisotropy.
    beta = 0 0 0 0 0 0
      .type = floats
      .expert_level = 4
      .short_caption = Anisotropic beta terms with isotropic B-factor removed
      .help = Anisotropic beta terms with isotropic B-factor removed. The anisotropic beta is applied as exp(-(beta11*h*h + beta22*k*k + beta33*l*l + beta12*h*k + beta13*h*l + beta23*k*l)).
    outer_bin_contrast = 0
      .type = float
      .expert_level = 4
      .short_caption = Directional contrast between reflection intensities in outer bin
  }
''' ,

"map_ellg" : '''
  map_ellg
    .style = tng:input:+emap tng:result:+emap
    .expert_level = 1
  {
    fs = 0.5 1.0
      .type = floats(value_min=0,value_max=1)
      .expert_level = 2
      .short_caption = Map fraction scattering
    rmsd = 0.4 0.8 1.2 1.6
      .type = floats(value_min=0)
      .expert_level = 1
      .short_caption = Map Rmsd
    result
      .multiple = True
      .expert_level = 4
    {
      fs = None
        .type = float
        .expert_level = 4
        .short_caption = Result of map ellg test: Bfactor
      rmsd = None
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Result of map test: Rmsd
      ellg = None
        .type = float
        .expert_level = 4
        .short_caption = Result of single atom ellg test: ellg
    }
  }
''' ,

"ensemble" : '''
  ensemble
    .style = tng:input:+emm
    .expert_level = 0
  {
    filename = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 1
      .short_caption = Coordinates for ensemble generation
      .help = PDB coordinates for ensemble generation, with rms to target defined in REMARK cards and models separated by MODEL cards. B-factor column may contain estimated coordinate error.
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying ensemble
      .help =  Tag can be used in place of identifier to define ensemble, provided unique. By default, the tag is taken as the stem of the filename. Non-alphanumeric characters in the tag string are substituted with the underscore character.
    disable_percent_deviation_check = False
      .type = bool
      .expert_level = 2
      .short_caption = Enable/disable check that coordinates in ensemble have same scattering
    disable_structure_factors_correlated_check = False
      .type = bool
      .expert_level = 2
      .short_caption = Enable/disable check that coordinates in ensemble are superimposed
    percent_deviation = 20
      .type = float(value_min=0,value_max=100)
      .expert_level = 2
      .short_caption = Allowed percent deviation of rms from reference
    vrms_estimate = 1.0
      .type = floats(value_min=0)
      .expert_level = 1
      .short_caption = Vrms estimate
    selenomethionine = False
      .type = bool
      .expert_level = 1
      .short_caption = Treat all selenomethionine as methionine if seleomethionine is false
    convert_rmsd_to_bfac = False
      .type = bool
      .expert_level = 0
      .short_caption = B-factor column contains rmsd on input so convert coordinate error to B-factor with 8pi^2/3
  }
''',

"model" : '''
  model
    .style = tng:input:+emm+esm+join
    .expert_level = 1
  {
    filename = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 1
      .short_caption = Coordinates for model
      .help = PDB coordinates for model. B-factor column may contain estimated coordinate error.
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying ensemble
      .help =  Tag can be used in place of identifier to define model, provided unique. By default, the tag is taken as the stem of the filename. Non-alphanumeric characters in the tag string are substituted with the underscore character.
    single_atom
      .expert_level = 4
    {
      use  = False
        .type = bool
        .expert_level = 4
        .short_caption = Model is a single atom (element) rather than pdb file, for single atom molecular replacement
      scatterer  = None
        .type = scatterer
        .expert_level = 4
        .short_caption = Scattering type for single atom (element) rather than pdb file, for single atom molecular replacement
      rmsd = 0.1
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Vrms estimate for single atom, for single atom molecular replacement
    }
    vrms_estimate = 1.0
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Vrms estimate
    selenomethionine = False
      .type = bool
      .expert_level = 1
      .short_caption = Treat all selenomethionine as methionine if seleomethionine is false
    convert_rmsd_to_bfac = False
      .type = bool
      .expert_level = 1
      .short_caption = B-factor column contains rmsd on input so convert coordinate error to B-factor with 8pi^2/3
  }
''',

"bins" : '''
  bins
    .style = tng:input:+data+emm+esm+imap+msm+sgx
    .expert_level = 5
  {
    reflections
      .expert_level = 5
    {
      range = 6 50
        .type = ints(size=2,value_min=1)
        .expert_level = 5
        .short_caption = Minimum/Maximum number of bins
      width = 500
        .type = int(value_min=100)
        .expert_level = 5
        .short_caption = Width of bins (number of reflections)
    }
    molecular_transforms
      .expert_level = 5
    {
      range = 6 1000
        .type = ints(size=2,value_min=1)
        .expert_level = 5
        .short_caption = Minimum/Maximum number of bins
      width = 1000
        .type = int(value_min=100)
        .expert_level = 5
        .short_caption = Width of bins (number of reflections)
    }
    maps
      .expert_level = 5
    {
      range = 6 1000
        .type = ints(size=2,value_min=1)
        .expert_level = 5
        .short_caption = Minimum/Maximum number of bins
      width = 1000
        .type = int(value_min=100)
        .expert_level = 5
        .short_caption = Width of bins (number of reflections)
    }
  }
''',

"molecular_transform" : '''
  molecular_transform
    .style = tng:input:+emm+esm+msm
    .expert_level = 1
  {
    bulk_solvent
      .expert_level = 5
    {
      use = False
        .type = bool
        .expert_level = 5
        .short_caption = Apply mask bulk solvent terms
      fsol = 0.35
        .type = float(value_min=0.01,value_max=1)
        .expert_level = 5
        .short_caption = Mask bulk solvent correction solvent parameter
      bsol = 45
        .type = float
        .expert_level = 5
        .short_caption = Mask bulk solvent correction solvent parameter
    }
    write_coordinates = False
      .type = bool
      .expert_level = 5
      .short_caption = Write file containing coordinates for molecular transform
    boxscale = 6
      .type = float(value_min=2)
      .expert_level = 3
      .short_caption = Boxscale for interpolation fourier transform
    resolution = 0
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Resolution limit
    skip_bfactor_analysis = False
      .type = bool
      .expert_level = 5
      .short_caption = Skip calculation of relative Wilson-B, and set to zero
    reorient = True
      .type = bool
      .expert_level = 3
      .short_caption = Reorient coordinates along principal axes
    fix = False
      .type = bool
      .expert_level = 5
      .short_caption = Fix coordinates without reorienting or translating, for use with muti-gyre
    bfactor_trim = 0
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = B-factor column trimming value, which may be an rmsd, zero means no trimming
    barbwire_test = True
      .type = bool
      .expert_level = 1
      .short_caption = Test the model for alphafold barbwire and fail if barbwire detected
    helix_test = True
      .type = bool
      .expert_level = 1
      .short_caption = Test the model for helix or coiled coil
    decomposition_radius_factor = 2
      .type = float(value_min=0.1)
      .expert_level = 3
      .short_caption = Factor of the radius for decomposition
    preparation = off *decomposition *interpolation *trace *monostructure *variance
      .type = choice(multi=True)
      .expert_level = 3
      .short_caption = Types of data structures to prepare for database
  }
''',

"rigid_body_maps" : '''
  rigid_body_maps
    .style = tng:input:+rbm
    .expert_level = 3
  {
    keep_chains = False
      .type = bool
      .expert_level = 3
      .help = Merge poses with incoming chainids
    maximum_stored = 5
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of maps calculated. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
  }
''' ,

"grid_search" : '''
  grid_search
    .style = tng:input:+rbgs
    .expert_level = 1
  {
    selection = *tra rot both trarot rottra
      .type = choice
      .expert_level = 1
      .short_caption = Use translation only, rotation only, both simultaneously 6D, or paired translation then rotation, for grid search
    range = 15 3
      .type = floats(size=2)
      .expert_level = 1
      .short_caption = Search range, degrees for rotation and angstroms for distance
    sampling = 5 0.5
      .type = floats(size=2)
      .expert_level = 1
      .short_caption = Search sampling, degrees for rotation and angstroms for distance
    maximum_stored = 1
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of grid searches. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    resolution = None
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = High resolution limit of the data for the grid search
    ncyc = 1
      .type = int(value_min=0)
      .expert_level = 1
      .short_caption = Number of cycles of grid_search per node
    poses = None
      .type = ints(value_min=1)
      .expert_level = 2
      .short_caption = Selection of poses to perturb, None for all
  }
''' ,

"perturbations" : '''
  perturbations
    .style = tng:input:+exp1
    .expert_level = 1
  {
    selection = *tra rot both
      .type = choice
      .expert_level = 1
      .short_caption = Use translation only, rotation only, both simultaneously 6D, or paired translation then rotation, for grid search
    range = 1 1
      .type = floats(size=2)
      .expert_level = 1
      .short_caption = Maximum perturbation, degrees for rotation and angstroms for translation
    number = 5
      .type = int(value_min=0)
      .expert_level = 1
      .short_caption = Number of perturbations per node
  }
''' ,

"cluster_compound" : '''
  cluster_compound
    .style = tng:input:+cca+ssa+ssr
    .expert_level = 1
  {
    filename = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 1
      .help = Cluster compound coordinate file, pdb format, specified as type 'XX'
  }
''' ,

"information" : '''
  information
    .style = tng:input:+info tng:result:+info
    .expert_level = 4
  {
    ellg_fraction_anomalous = 1.0
      .type = float(value_min=0.00001,value_max=1)
      .expert_level = 4
      .short_caption = Fraction of anomalous scatterer modeled in SAD eLLG
    ellg_rms_factor = 0.125
      .type = float(value_min=0)
      .expert_level = 4
      .short_caption = Factor of dmin for RMSD in SAD eLLG
    information_content = None
      .type = float
      .expert_level = 4
      .short_caption = Total data information content
    expected_information_content = None
      .type = float
      .expert_level = 4
      .short_caption = Total data information content
    sad_information_content = None
      .type = float
      .expert_level = 4
      .short_caption = Total sad substructure information content
    sad_expected_llg = None
      .type = float
      .expert_level = 4
      .short_caption = Total substructure expected llg
  }
''' ,

"expected" : '''
  expected
    .style = tng:input:+eatm+ellg+perm+rellg tng:result:+eatm+etfz+perm+rellg
    .expert_level = 1
  {
    mapllg
      .expert_level = 2
    {
      target = 225
        .type = float
        .expert_level = 2
        .short_caption = Target for map eLLG calculations
        .help = Target value for expected map eLLG for determining resolution limits
    }
    llg
      .expert_level = 1
    {
      rmsd = 0.4
        .type = float(value_min=0.1)
        .expert_level = 1
        .short_caption = Rmsd error in model
      id = None
        .type = uuid(value_min=0)
        .expert_level = 1
        .short_caption = Identifier for model
      tag = None
        .type = str
        .expert_level = 1
        .short_caption = Short tag identifying model
      rellg = 0
        .type = float
        .expert_level = 4
        .short_caption = Relative-expected-llg for single model
      ellg = 0
        .type = float
        .expert_level = 4
        .short_caption = Expected-llg for single model, calculated by the specified method
    }
    ellg
      .expert_level = 1
    {
      speed_versus_accuracy = aellg *eellg ellg
        .type = choice
        .expert_level = 1
        .short_caption = Use expected fast ellg calculation
        .help = Use analytical-expected-llg (instantaneous), expected-expected-llg (fast) or expected-llg (slow). Calculations slower than the selection will also be calculated.
      target = 225
        .type = float(value_min=0)
        .expert_level = 2
        .short_caption = Target ellg increase for ellg calculations and determining resolution for speed optimization
      maximum_stored = 20
        .type = int(value_min=0)
        .expert_level = 3
        .help = Maximum number of highest ellg search strategies stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
      maximum_first_search = 1 3
        .type = ints(size=2,value_min=1,value_max=12)
        .expert_level = 1
        .short_caption = Range for number of first search components within which the target ellg should be reached
        .help = Range for number of first search components within which the target ellg should be reached. The minimum is for combinations of different components and accounts for uncertainty in the rmsd. There is combinatorial explosion with increasing number and corresponding increase in computation times for search order generation.
      signal_resolution_factor = 1.5
        .type = float(value_min=1)
        .expert_level = 2
        .short_caption = Multiplication factor for signal resolution from rmsd
    }
    atom
      .expert_level = 1
    {
      scatterer = "S"
        .type = scatterers
        .expert_level = 1
        .short_caption = Single atom element type
      bfactor = 0 -1.0
        .type = floats
        .expert_level = 1
        .short_caption = Single atom Bfactors
      rmsd = 0.1
        .type = float
        .expert_level = 1
        .short_caption = Single atom Rmsd
      target = 20
        .type = float
        .expert_level = 1
        .short_caption = Target ellg for number
      minimum_search_number = 1
        .type = int
        .expert_level = 1
        .short_caption = Result of analysis giving minimum search number for the target llg
    }
    subdir = None
      .type = str
      .multiple = True
      .expert_level = 4
      .short_caption = Subdirectories under database for output etfz cards files in best order of etfz
    filenames_in_subdir = None
      .type = str
      .multiple = True
      .expert_level = 4
      .short_caption = Filenames for output etfz cards files in best order of etfz
  }
''' ,

"substructure" : '''
  substructure
    .style = tng:input:+hyss+ssa+ssd+ssm+ssp+ssr tng:result:+ssa+ssp
    .expert_level = 0
  {
    scatterer = None
      .type = scatterer
      .expert_level = 0
      .short_caption = Scattering type (element)
    content_analysis
      .expert_level = 4
    {
      number = 1
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Estimate of number of scattering type
      delta_bfactor = 0
        .type = float
        .expert_level = 4
        .short_caption = Estimate of difference to Wilson-B
      signal = false
        .type = bool
        .expert_level = 4
        .short_caption = There is an anomalous signal in the data for the substructure content analysis
    }
    patterson_analysis
      .expert_level = 1
    {
      target = anomdiff likelihood *expected
        .type = choice
        .expert_level = 1
        .short_caption = Calculate Patterson from anomalous differences, LLGI-gradient or expected ZH
      percent = 20
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Cutoff for selection of peaks from Patterson
        .help = Cutoff for selection of peaks from Patterson in terms of percent
      maximum_stored = 3
        .type = int(value_min=0)
        .expert_level = 3
        .help = Cutoff for selection of peaks from Patterson. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
        .help = Maximum number of peaks stored from Patterson
      shelxc = None
        .type = path
        .expert_level = 4
        .short_caption = Shelxc map coefficients
      constellation = False
        .type = bool
        .expert_level = 5
        .short_caption = Perform constellation analysis
      minimum_number_of_reflections = 50
        .type = int(value_min=10)
        .expert_level = 5
        .short_caption = Number of reflections at which there is insufficient data
      minimum_percent = 10
        .type = float(value_min=0,value_max=100)
        .expert_level = 2
        .short_caption = Minimum value for patterson cutoff
      insufficient_data = False
        .type = bool
        .expert_level = 4
        .short_caption = Warning for insufficent data
      minimum_resolution = 10
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Resolution of data at which there is insufficient resolution of data
      insufficient_resolution = False
        .type = bool
        .expert_level = 4
        .short_caption = Warning for insufficent resolution of data
    }
    constellation
      .expert_level = 1
    {
      size = 3
        .type = int
        .expert_level = 1
        .short_caption = Number of sites in constellation
      number_of_shifts = 1
        .type = int
        .expert_level = 1
        .short_caption = Number of shifts for deconvolution
      distance = 8.0
        .type = float
        .expert_level = 1
        .short_caption = Minimum Patterson vector length for SMF
    }
    preparation
      .expert_level = 1
    {
      scatterer = None
        .type = scatterer
        .expert_level = 1
        .short_caption = Change all element types to this scatterer type
      wilson_bfactor = True
        .type = bool
        .expert_level = 2
        .short_caption = Modify all B-factors on input to match Wilson-B of data
    }
    determination
      .expert_level = 1
    {
      scatterer = None
        .type = scatterer
        .expert_level = 1
        .short_caption = Scatterer type for hyss method only, since ssd uses strongest scatterer in composition
      occupancy = 0.9
        .type = float(value_min=0.1,value_max=10)
        .expert_level = 1
        .short_caption = Default occupancy for new sites at start of refinement post phassade search
      bfactor =  0.0
        .type = float
        .expert_level = 4
        .short_caption = Estimate of difference to Wilson-B for new sites in phassade search
      minimum_number = 1
        .type = int(value_min=1,value_max=10000)
        .expert_level = 1
        .short_caption = Minimum number of peaks to take from each Patterson even if peaks are lower than holes
      find = 1
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Number of sites to find in substructure determination
      maximum_find = 4
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Maximum number of sites in each phassade substructure determination
      percent = 75
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Percent for accepting sites in Phassade search provided peaks are over holes
      zscore = 3
        .type = float(value_min=2)
        .expert_level = 1
        .short_caption = Zscore for accepting sites in Phassade search provided peaks are over holes
      target_fom = 1.0
        .type = float(value_min=0,value_max=1.0)
        .expert_level = 1
        .short_caption = Target figure of merit for termination of substructure search
      peaks_over_deepest_hole = True
        .type = bool
        .expert_level = 1
        .short_caption = Only take the peaks over the deepest hole
      maximum_stored = 10
        .type = int(value_min=0)
        .expert_level = 3
        .help = Maximum number of substructures stored per find cycle. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    }
    completion
      .expert_level = 1
    {
      complete = True
        .type = bool
        .expert_level = 1
        .short_caption = Perform substructure completion
      ncyc = 50
        .type = int
        .expert_level = 1
        .short_caption = Maximum number of cycles of substructure completion
      scatterer = None
        .type = scatterers
        .expert_level = 1
        .short_caption = Complete with scatterers in selection list
      separation_distance = None
        .type = float(value_min=0.1,value_max=10)
        .expert_level = 1
        .short_caption = Distance to consider map peaks distinct
      zscore = 5.5
        .type = float(value_min=1)
        .expert_level = 1
        .short_caption = Z-score for significance of map peaks
      peaks_above_deepest_hole = True
        .type = bool
        .expert_level = 1
        .short_caption = Only take the peaks over the deepest hole
      top_peak_below_deepest_hole = True
        .type = bool
        .expert_level = 1
        .short_caption = Only take the peaks over the deepest hole
      top_peak_below_deepest_hole_zscore = 6.0
        .type = float(value_min=1)
        .expert_level = 1
        .short_caption = Z-score for significance of map peaks when below the deepest hole
      change_scatterers = True
        .type = bool
        .expert_level = 1
        .short_caption = Delete substructure atoms with low occupancy
      swap_to_anisotropic_scatterers = True
        .type = bool
        .expert_level = 1
        .short_caption = Allow B-factors of substructure to be anisotropic
      rfactor_termination = 30
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Terminate substructure completion when Rfactor is reached
      write_files = False
        .type = bool
        .expert_level = 3
        .short_caption = Write substructure files at each cycle
    }
    test_fft_versus_summation = 20 80
      .type = ints(size=2)
      .expert_level = 4
      .short_caption = Test speed of substructure structure factor calculation by fft versus summation
    hyss
      .expert_level = 1
    {
      phil = None
        .type = filesystem(directory=False)
        .expert_level = 1
        .short_caption = Phil file for setting parameters in hyss substructure determination
      command_line = None
        .type = strings
        .expert_level = 1
        .short_caption = Command line options for phil
    }
  }
''' ,

"atoms" : '''
  atoms
    .style = tng:input:+ssm tng:result:+hyss
    .expert_level = 0
  {
    filename  = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 0
      .short_caption = File from external source, such as hyss or shelxd
    fractional
      .expert_level = 1
      .multiple = True
    {
      scatterer  = None
        .type = scatterer
        .expert_level = 1
        .short_caption = Scattering type for atom
      site =  None
        .type = floats(size=3)
        .expert_level = 1
        .short_caption = Fractional coordinates of atomic site
      occupancy = None
        .type = float(value_min=0)
        .expert_level = 1
        .short_caption = Occupancy of atom at site
    }
  }
''' ,

"phased_translation_function" : '''
  phased_translation_function
    .style = tng:input:+ptf
    .expert_level = 1
  {
    cluster = True
      .type = bool
      .expert_level = 1
      .short_caption = Store clustered poses
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
    sampling = None
      .type = float
      .expert_level = 2
      .short_caption = Translation function sampling distance
      .help = Translation function sampling distance in Angstroms. Defaults depends on resolution of the data.
    sampling_factor = 4.0
      .type = float(value_min=0.1)
      .expert_level = 5
      .short_caption = Translation function sampling divisor of high resolution.
      .help = Translation function sampling fraction of high resolution. Used to determine sampling if explicit sampling is not given.
    add_pose_orientations = False
      .type = bool
      .expert_level = 2
      .short_caption = Add orientations of current poses to rotation list in case of translational-ncs
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
    percent = 75
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum 100*(value-mean)/(top-mean) for poses stored
    write_maps = False
      .type = bool
      .expert_level = 3
      .short_caption = Write translation function map
  }
''' ,

"jog_refinement" : '''
  jog_refinement
    .style = tng:input:+jog tng:result:+jog
    .expert_level = 2
  {
    sampling = None
      .type = float
      .expert_level = 2
      .short_caption = Translation function sampling distance
      .help = Translation function sampling distance in Angstroms. Defaults depends on resolution of the data.
    sampling_factor = 4.0
      .type = float(value_min=0.1)
      .expert_level = 5
      .short_caption = Translation function sampling divisor of high resolution.
    maximum_stored = 10
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of translation function positions refined. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of translation function positions printed to logfile
    percent = 10
      .type = float(value_min=0,value_max=100)
      .expert_level = 2
      .short_caption = Percentage of top phased translation function peak for match
    distance = 1 10
      .type = floats(size=2,value_min=0)
      .expert_level = 2
      .short_caption = Minimum and maximum distance for accepting close association in pose
      .help = Minimum and maximum distance for accepting close association in pose. Minimal distance set because close values should be within grid spacing of fft.
    jogged = None
      .type = bool
      .expert_level = 4
      .short_caption = Translation function solutions were jogged
  }
''' ,

"packing" : '''
  packing
    .style = tng:input:+ftf+ftfr+move+pak tng:result:+pak
    .expert_level = 1
  {
    percent = 5
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Cutoff value for worst clash
    keep_high_tfz = True
      .type = bool
      .expert_level = 1
      .short_caption = Keep high tfz regardless of packing clashes
    high_tfz = 11
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Translation function z-score for rescue of clashing solutions
    overlap = 20
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Percent clash that defines a gross overlap, and exclusion from high tfz rescue
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed
    move_to_origin = True
      .type = bool
      .expert_level = 1
      .short_caption = Move first pose to origin
    move_to_compact = True
      .type = bool
      .expert_level = 1
      .short_caption = Move poses to compact unit
    move_to_input = True
      .type = bool
      .expert_level = 1
      .short_caption = Move to overlay an input position, if possible
    move_when_tncs_present = True
      .type = bool
      .expert_level = 1
      .short_caption = Toggle to veto move to compact and move to origin, so that tncs is obvious in coordinates
    distance_factor = 0.5
      .type = float(value_min=0.1)
      .expert_level = 2
      .short_caption = Trace separation distance multiplication factor to consider distances between trace points for clash
    clash_coordinates = False
      .type = bool
      .expert_level = 3
      .short_caption = Write clashing coordinates to pdb file
    revert_on_failure = False
      .type = bool
      .expert_level = 4
      .short_caption = If no nodes survive the packing, revert to background by removing last tncs group placed
    reverted = None
      .type = bool
      .expert_level = 4
      .short_caption = Reverted to background by removing last tncs group placed
  }
''' ,

"spanning" : '''
  spanning
    .style = tng:input:+span
    .expert_level = 1
  {
    maximum_stored = 1
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of nodes for which spanning is calculated. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 1
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of nodes printed
    distance = 10
      .type = float(value_min=1)
      .expert_level = 1
      .short_caption = Distance between atoms for inclusion in connected network, the maximum distance in angstroms for the allowed gap
    find_gaps = False
      .type = bool
      .expert_level = 2
      .short_caption = Use a range of spanning distances to find the distance at which space is spanned, where the spanning distances are 5 divisions between 5 angstroms to twice the largest mean radius of a model
  }
''' ,

"fast_rotation_function" : '''
  fast_rotation_function
    .style = tng:input:+frf+frfr
    .expert_level = 1
  {
    percent = 50.
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Range of 100*(value-mean)/(top-mean) for orientations stored
    cluster = False
      .type = bool
      .expert_level = 1
      .short_caption = Store clustered orientations
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
    sampling = None
      .type = float
      .expert_level = 2
      .short_caption = Rotation function sampling angle
      .help = Rotation function sampling angle in degrees. Default depends on mean radius of the search model and resolution of the data.
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of orientations stored per vrms_estimate. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node. Note that the same number of solutions are passed to next node as if each vrms_estimate rotation function was performed separately. With many vrms_estimates, this number may need to be lowered proportionately.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of orientations printed to logfile
    helix_factor = 1
      .type = float(value_min=1,value_max=3)
      .expert_level = 2
      .short_caption = Fine sampling factor for division of default sampling if the model is a helix or data is coiled-coil
    top_not_in_pose = False
      .type = bool
      .expert_level = 5
      .short_caption = Use the top orientation that is not already in the pose list as the reference top for percent purge
    vrms_estimates = None
      .type = floats(value_min=0)
      .expert_level = 1
      .short_caption = New vrms estimate for search ensemble limited by vrms range, which overrides the rmsd of the initial input
    maps
      .multiple = True
    {
      id = None
        .type = uuid(value_min=0)
        .expert_level = 4
        .short_caption = List of reflection identifiers for expanding the search
      tag = None
        .type = str
        .expert_level = 4
        .short_caption = Short tag identifying model
        .short_caption = List of reflection tags for expanding the search
    }
    signal_resolution = 0
      .type = float(value_min=0)
      .expert_level = 4
      .short_caption = Signal resolution for rotation function
    purge_duplicates = True
      .type = bool
      .expert_level = 4
      .short_caption = Find and purge duplicates by clustering with small angle
  }
''' ,

"cross_rotation_function" : '''
  cross_rotation_function
    .style = tng:input:+srf
    .expert_level = 1
  {
    filename = None
      .type = filesystem(directory=False,ext="mtz")
      .expert_level = 1
      .short_caption = Reflections file with X-ray data for cross rotation function (mtz format)
  }
''',

"self_rotation_function" : '''
  self_rotation_function
    .style = tng:input:+srf
    .expert_level = 1
  {
    integration_radius = 20
      .type = float(value_min=3)
      .expert_level = 2
      .short_caption = User input of integration radius, default value if model or sequence not provided
    sequence
    {
      filename = None
        .type = filesystem(directory=False,ext="fa")
        .expert_level = 1
        .short_caption = Sequence file used for estimation of the molecular radius assuming spherical structure.
      integration_radius_factor = 2.
        .type = float(value_min=0.1,value_max=2)
        .expert_level = 1
        .short_caption = Multiple to apply to the sequence derived molecular radius
    }
    model
    {
      filename = None
        .type = filesystem(directory=False,ext="pdb")
        .expert_level = 1
        .short_caption = Model for calculation of the molecular radius, at radius that encompasses 90% of the atoms.
      integration_radius_factor = 2.
        .type = float(value_min=0.1,value_max=2)
        .expert_level = 1
        .short_caption = Multiple to apply to the model derived molecular radius
    }
    percent = 20.
      .type = float(value_min=0,value_max=100)
      .expert_level = 2
      .short_caption = Range of 100*(value-mean)/(top-mean) for orientations stored
    resolution = 3.0
      .type = float(value_min=0)
      .expert_level = 2
      .short_caption = High resolution limit
    plot_chi = 15
      .type = int(value_min=1,value_max=180)
      .expert_level = 1
      .short_caption = Minimal sampling interval for chi sections
    maximum_stored = 1000
      .type = int(value_min=0)
      .expert_level = 1
      .help = Maximum number of orientations stored. Zero for all.
    maximum_printed = 50
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of orientations printed to logfile
    maximum_order = 12
      .type = float(value_min=1)
      .expert_level = 3
      .short_caption = Maximum order of rotation
  }
''' ,

"rescore_rotation_function" : '''
  rescore_rotation_function
    .style = tng:input:+frfr
    .expert_level = 1
  {
    frf_percent_increase = 5.
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Value added to fast rotation function percent to reduce number of peaks selected.
      .help = Used in automation, where the value for the fast rotation function percent is used in rescoring for selection, but should be increased for use in rescoring
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of orientations stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of orientations printed to logfile
    cluster = True
      .type = bool
      .expert_level = 4
      .short_caption = Store clustered orientations
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
    nrand = 500
      .type = int(value_min=500,value_max=4000)
      .expert_level = 5
      .short_caption = Number of random points for determining mean and standard deviation.
    calculate_zscore = False
      .type = bool
      .expert_level = 5
      .short_caption = Calculate zscore using statistics from random angles or keep the zscore from the fast rotation function
  }
''' ,

"fuse_translation_function" : '''
  fuse_translation_function
    .style = tng:input:+fuse tng:result:+fuse
    .expert_level = 1
  {
    models
      .expert_level = 1
    {
      tags = None
        .type = strings
        .expert_level = 1
        .short_caption = List of search models by tag
    }
    tfz = 10
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = TFZ for considering solutions for amalgamation
    packing_percent = 5
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Cutoff value for worst clash allowed as the top peak
    maximum_remaining = 10
      .type = int(value_min=0,value_max=15)
      .expert_level = 2
      .short_caption = Maximum number of constituents to merge, limits combinatorial explosion
    fused = None
      .type = bool
      .expert_level = 4
      .short_caption = Translation function solutions were fused
    complete = None
      .type = bool
      .expert_level = 4
      .short_caption = Solution is complete
  }
''' ,

"brute_rotation_function" : '''
  brute_rotation_function
    .style = tng:input:+brf
    .expert_level = 1
  {
    volume = unique *around random
      .type = choice
      .expert_level = 1
      .short_caption = Brute point search volume
      .help = Around an angle, unique rotation search volume for space group, or random orientations
    range = 10
      .type = float
      .expert_level = 1
      .short_caption = Brute search \"around\" search range
    shift_matrix = 1 0 0 0 1 0 0 0 1
      .type = floats(size=9)
      .expert_level = 1
      .short_caption = Brute search \"around\" rotation from rotation matrix
    nrand = 1
      .type = int
      .expert_level = 1
      .short_caption = Brute search \"random\" number of random points
    sampling = None
      .type = float
      .expert_level = 2
      .short_caption = Rotation function sampling angle
      .help = Rotation function sampling angle in degrees. Default depends on mean radius of the search model and resolution of the data.
    helix_factor = 1
      .type = float(value_min=1)
      .expert_level = 2
      .short_caption = Division of default sampling if the model is a helix or data is coiled-coil
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of orientations stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of orientations printed to logfile
    generate_only = False
      .type = bool
      .expert_level = 4
      .help = Generate rotations only, no scoring, for 6D searches
    signal_resolution = 0
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Signal resolution for rotation function
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
  }
''' ,

"fast_translation_function" : '''
  fast_translation_function
    .style = tng:input:+ftf+ftfr
    .expert_level = 1
  {
    percent = 75
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum 100*(value-mean)/(top-mean) for poses stored
    single_atom
    {
      use = False
        .type = bool
        .expert_level = 1
        .short_caption = Translation function of single atom, initiate search without rotation function
      scatterer = S
        .type = scatterer
        .expert_level = 1
        .short_caption = Scattering type (element) for single atom molecular replacement
    }
    cluster = True
      .type = bool
      .expert_level = 2
      .short_caption = Store clustered poses
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
    report_cluster = True
      .type = bool
      .expert_level = 3
      .short_caption = For speed, don't perform clustering
    sampling_factor = 4.0
      .type = float(value_min=0.1)
      .expert_level = 5
      .short_caption = Translation function sampling divisor of high resolution.
      .help = Translation function sampling fraction of high resolution. Used to determine sampling if explicit sampling is not given.
    sampling = None
      .type = float(value_min=0.1)
      .expert_level = 2
      .short_caption = Translation function sampling distance
      .help = Translation function sampling distance in Angstroms. Defaults depends on resolution of the data.
    add_pose_orientations = True
      .type = bool
      .expert_level = 2
      .short_caption = Add orientations of current poses to rotation list in case of translational-ncs
    stop
    {
      best_packing_tfz_over = 99
        .type = float(value_min=0)
        .expert_level = 1
        .short_caption = Stop search only if best packing tfz over value, default value high to void stop
        .help = Stop searching in rotation function peak list when three stop conditions met
      rf_percent_under = 0
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Stop search only if rotation function peak is under value for percent, default value zero to void stop
        .help = Stop searching in rotation function peak list when three stop conditions met
      rf_zscore_under = 0
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Stop search only if rotation function peak is under value for zscore, default value zero to void stop
        .help = Stop searching in rotation function peak list when three stop conditions met
    }
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
    write_maps = False
      .type = bool
      .expert_level = 3
      .short_caption = Write fast translation function map
    packing
    {
      use = True
        .type = bool
        .expert_level = 2
        .short_caption = Use packing test for top peak
      low_tfz = 5
        .type = float(value_min=0)
        .expert_level = 2
        .short_caption = TFZ lower cutoff value for abandoning packing test
    }
    helix_factor = 1
      .type = float(value_min=1)
      .expert_level = 2
      .short_caption = Division of default sampling if the model is a helix or data is coiled-coil
  }
''' ,

"rescore_translation_function" : '''
  rescore_translation_function
    .style = tng:input:+ftfr
    .expert_level = 1
  {
    cluster = True
      .type = bool
      .expert_level = 2
      .short_caption = Store clustered solutions from rescored translation function
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
    percent = 75
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum 100*(value-mean)/(top-mean) for translation function positions stored
    calculate_zscore = False
      .type = bool
      .expert_level = 4
      .short_caption = Calculate zscore using statistics from random positions rather than keeping score from fast translation function
    nrand = 500
      .type = int(value_min=0,value_max=4000)
      .expert_level = 4
      .short_caption = Number of random points for determining mean and standard deviation.
    purge_tncs_duplicates = False
      .type = bool
      .expert_level = 1
      .short_caption = Purge tncs related duplicates from list
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
  }
''' ,

"brute_translation_function" : '''
  brute_translation_function
    .style = tng:input:+btf
    .expert_level = 1
  {
    volume = *around random unique box
      .type = choice
      .expert_level = 1
      .short_caption = Brute point search volume
      .help = Around a point or random points or the unique volume (direct space asymmetric unit volume or Cheshire cell)
    range = 10
      .type = float
      .expert_level = 1
      .short_caption = Brute search \"around\" search range in Angstroms
    point = 0 0 0
      .type = floats(size=3)
      .expert_level = 1
      .short_caption = Brute search \"around\" search point (orthogonal Angstroms or fractional)
    fractional = True
      .type = bool
      .expert_level = 1
      .short_caption = Brute search \"around\" search point is in fractional coordinates rather than orthogonal Angstroms
    nrand = 1
      .type = int
      .expert_level = 1
      .short_caption = Brute search \"random\" number of random points
    sampling = None
      .type = float
      .expert_level = 2
      .short_caption = Translation function sampling distance
      .help = Translation function sampling distance in Angstroms. Defaults depends on resolution of the data.
    sampling_factor = 4.0
      .type = float(value_min=0.1)
      .expert_level = 5
      .short_caption = Translation function sampling divisor of high resolution.
      .help = Translation function sampling fraction of high resolution. Used to determine sampling if explicit sampling is not given.
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
    cluster_back = 2000
      .type = int(value_min=1)
      .expert_level = 5
      .short_caption = Maximum number of peaks to look back in list for cluster analysis, prevents N*N complexity
  }
''' ,

"pose_scoring" : '''
  pose_scoring
    .style = tng:input:+pose
    .expert_level = 1
  {
    rescore = True
      .type = bool
      .expert_level = 1
      .help = Rescore poses
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
  }
''' ,


#end mode keywords

# mtz column types, everything stored as double in c++ code except
#   B = boolean
#   I = integer
#   J,Q,K,M,F,R etc = double
"labin" : '''
  labin
    .style = tng:input:+aniso+data+feff+imap+imtz+info+rbm+rfac+sgx+spr+ssr+xref tng:result:+data+imtz+ipdb
    .expert_level = 1
  {
    inat = None
      .type = mtzcol(type='J',name="I")
      .expert_level = 1
      .short_caption = I label
      .help = Intensities should be given
    siginat = None
      .type = mtzcol(type='Q',name="SIGI")
      .expert_level = 1
      .short_caption = SIGI label
      .help = If intensities are given, SIGI should be given but is optional
    ipos = None
      .type = mtzcol(type='K',name="I(+)")
      .expert_level = 1
      .short_caption = I(+) label
      .help = Intensities for SAD phasing
    sigipos = None
      .type = mtzcol(type='M',name="SIGI(+)")
      .expert_level = 1
      .short_caption = SIGI(+) label
      .help = If intensities are given, SIGI should be given but is optional
    ineg = None
      .type = mtzcol(type='K',name="I(-)")
      .expert_level = 1
      .short_caption = I(-) label
      .help = Intensities for SAD phasing
    sigineg = None
      .type = mtzcol(type='M',name="SIGI(-)")
      .expert_level = 1
      .short_caption = SIGI(-) label
      .help = If intensities are given, SIGI should be given but is optional
    fnat = None
      .type = mtzcol(type='F',name="F")
      .expert_level = 1
      .short_caption = F label
      .help = Intensities should be given
    sigfnat = None
      .type = mtzcol(type='Q',name="SIGF")
      .expert_level = 1
      .short_caption = SIGF label
      .help = If amplitudes are given, SIGF should be given but is optional
    fpos = None
      .type = mtzcol(type='G',name="F(+)")
      .expert_level = 1
      .short_caption = F(+) label
      .help = Amplitudes for SAD phasing
    sigfpos = None
      .type = mtzcol(type='L',name="SIGF(+)")
      .expert_level = 1
      .short_caption = SIGF(+) label
      .help = If amplitudes are given, SIGF should be given but is optional
    fneg = None
      .type = mtzcol(type='G',name="F(-)")
      .expert_level = 1
      .short_caption = F(-) label
      .help = Amplitudes for SAD phasing
    sigfneg = None
      .type = mtzcol(type='L',name="SIGF(-)")
      .expert_level = 1
      .short_caption = SIGF(-) label
      .help = If amplitudes are given, SIGF should be given but is optional
    fcpos = None
      .type = mtzcol(type='G',name="FC(+)")
      .expert_level = 1
      .short_caption = FC(+) label
      .help = Calculated structure factor amplitude for SAD phasing
    phicpos = None
      .type = mtzcol(type='G',name="PHIC(+)")
      .expert_level = 1
      .short_caption = PHIC(+) label
      .help = Calculate structure factor phases for SAD phasing
    fcneg = None
      .type = mtzcol(type='G',name="FC(-)")
      .expert_level = 1
      .short_caption = FC(-) label
      .help = Calculated structure factor amplitude for SAD phasing
    phicneg = None
      .type = mtzcol(type='G',name="PHIC(-)")
      .expert_level = 1
      .short_caption = PHIC(-) label
      .help = Calculate structure factor phases for SAD phasing
    mapf = None
      .type = mtzcol(type='F',name="FMAP")
      .expert_level = 1
      .short_caption = FMAP label
      .help = FMAP must have associated phase
    mapph = None
      .type = mtzcol(type='P',name="PHMAP")
      .expert_level = 1
      .short_caption = PHMAP label
      .help = PHMAP must have associated amplitude FMAP
    mapdobs = None
      .type = mtzcol(type='W',name="DOBSMAP")
      .expert_level = 1
      .short_caption = FOM label
      .help = Fourier term correction factors from anisotropy analysus
    dobsnat = None
      .type = mtzcol(type='R',name="DOBS")
      .expert_level = 4
      .short_caption = LLGI Dobs
      .help = LLGI Effective D accounting for SIGI in data
    feffnat = None
      .type = mtzcol(type='F',name="FEFF")
      .expert_level = 4
      .short_caption = LLGI Feff
      .help = LLGI Effective F accounting for SIGI in I
    dobspos = None
      .type = mtzcol(type='R',name="DOBS(+)")
      .expert_level = 4
      .short_caption = LLGI Dobs(+)
      .help = LLGI Effective D(+) accounting for SIGIPOS in I
    feffpos = None
      .type = mtzcol(type='F',name="FEFF(+)")
      .expert_level = 4
      .short_caption = LLGI Feff(+)
      .help = LLGI Effective F(+) accounting for SIGI(+) in I(+)
    dobsneg = None
      .type = mtzcol(type='R',name="DOBS(-)")
      .expert_level = 4
      .short_caption = LLGI Dobs(-)
      .help = LLGI Effective Dobs(-) accounting for SIGI(-) in I(-)
    feffneg = None
      .type = mtzcol(type='F',name="FEFF(-)")
      .expert_level = 4
      .short_caption = LLGI Feff(-)
      .help = LLGI Effective F(-) accounting for SIGI(-) in I(-)
    resn = None
      .type = mtzcol(type='R',name="RESN")
      .expert_level = 4
      .short_caption = Root Epsilon SigmaN
      .help = (Square) Root Epsilon*SigmaN for normalization of F to E including Anisotropy correction
    einfo = None
      .type = mtzcol(type='R',name="EINFO")
      .expert_level = 4
      .short_caption = Expected information Content
      .help = Expected information content of reflection
    info = None
      .type = mtzcol(type='R',name="INFO")
      .expert_level = 4
      .short_caption = Information Content
      .help = Information content of reflection
    sinfo = None
      .type = mtzcol(type='R',name="SINFO")
      .expert_level = 4
      .short_caption = SAD Extra Information Content
      .help = SAD extra information content of reflection
    sellg = None
      .type = mtzcol(type='R',name="SELLG")
      .expert_level = 4
      .short_caption = SAD eLLG
      .help = SAD expected log-likelihood-gain of reflection
    sefom = None
      .type = mtzcol(type='R',name="SEFOM")
      .expert_level = 4
      .short_caption = SAD eFOM
      .help = SAD expected figure-of-merit of reflection
    aniso = None
      .type = mtzcol(type='R',name="ANISO")
      .expert_level = 4
      .short_caption = Anisotropy Factor
      .help = Real value for anisotropy weighting of reflection including isotropic b-factor, for plotting.
    anisobeta = None
      .type = mtzcol(type='R',name="ANISOBETA")
      .expert_level = 4
      .short_caption = Anisotropy Factor
      .help = Real value for anisotropy weighting of reflection excluding isotropic b-factor. Corrected intensity is I*ANISO.
    teps = None
      .type = mtzcol(type='R',name="TEPS")
      .expert_level = 4
      .short_caption = Tncs Epsilon Factor
      .help = Real value for epsilon weighting of reflection in llgi due to tncs
    tbin = None
      .type = mtzcol(type='R',name="TBIN")
      .expert_level = 4
      .short_caption = Tncs Gfunction Variance Factor
      .help = Binning term in tncs correction
    fwt = None
      .type = mtzcol(type='F',name="FWT")
      .expert_level = 4
      .short_caption = Sigma-A weighted map coefficient (amplitude)
    phwt = None
      .type = mtzcol(type='P',name="PHWT")
      .expert_level = 4
      .short_caption = Sigma-A weighted map coefficient (phase)
    delfwt = None
      .type = mtzcol(type='F',name="DELFWT")
      .expert_level = 4
      .short_caption = Sigma-A weighted difference map coefficient (amplitude)
    delphwt = None
      .type = mtzcol(type='P',name="DELPHWT")
      .expert_level = 4
      .short_caption = Sigma-A weighted difference map coefficient (phase)
    fcnat = None
      .type = mtzcol(type='F',name="FC")
      .expert_level = 4
      .short_caption = Calculated map coefficient (amplitude)
    phicnat = None
      .type = mtzcol(type='P',name="PHIC")
      .expert_level = 4
      .short_caption = Calculated map coefficient (phase)
    llgf = None
      .type = mtzcol(type='F',name="LLGF")
      .expert_level = 4
      .short_caption = Calculated map coefficient (amplitude)
    llgph = None
      .type = mtzcol(type='P',name="LLGPHI")
      .expert_level = 4
      .short_caption = Calculated map coefficient (phase)
    fom = None
      .type = mtzcol(type='W',name="FOM")
      .expert_level = 4
      .short_caption = Calculated map coefficient (figure of merit)
    hla = None
      .type = mtzcol(type='A',name="HLA")
      .expert_level = 4
      .short_caption = Calculated map coefficient (hendrickson-lattman coefficients)
    hlb = None
      .type = mtzcol(type='A',name="HLB")
      .expert_level = 4
      .short_caption = Calculated map coefficient (hendrickson-lattman coefficients)
    hlc = None
      .type = mtzcol(type='A',name="HLC")
      .expert_level = 4
      .short_caption = Calculated map coefficient (hendrickson-lattman coefficients)
    hld = None
      .type = mtzcol(type='A',name="HLD")
      .expert_level = 4
      .short_caption = Calculated map coefficient (hendrickson-lattman coefficients)
    siga = None
      .type = mtzcol(type='R',name="SIGA")
      .expert_level = 4
      .short_caption = SigmaA
    ssqr = None
      .type = mtzcol(type='R',name="SSQR")
      .expert_level = 4
      .short_caption = Resolution as 1/d^2 (d in Angstroms)
      .help = Resolution of the reflection as 1/d^2 (d in Angstroms)
    eps = None
      .type = mtzcol(type='R',name="EPS")
      .expert_level = 4
      .short_caption = Space group symmetry epsilon factor
    phsr = None
      .type = mtzcol(type='R',name="PHSR")
      .expert_level = 4
      .short_caption = Space group symmetry phase restriction
    bin = None
      .type = mtzcol(type='I',name="BIN")
      .expert_level = 4
      .short_caption = Binning Flag
      .help = Integer flagging the resolution bin of the reflection
    free = None
      .type = mtzcol(type='I',name="FREE")
      .expert_level = 4
      .short_caption = Binning Flag
      .help = Free-R flag
    cent = None
      .type = mtzcol(type='B',name="CENT")
      .expert_level = 4
      .short_caption = Boolean flag for centric
      .help = Flag for centric (0) or acentric (1) reflection
    both = None
      .type = mtzcol(type='B',name="BOTH")
      .expert_level = 4
      .short_caption = Boolean flag for both (+) and (-) observed
    plus = None
      .type = mtzcol(type='B',name="PLUS")
      .expert_level = 4
      .short_caption = Boolean flag for whether (+) or (-) observed if not both observed
  }
''' ,

"data" : '''
  data
    .style = tng:input:+emm+esm tng:result:+data+imap+imtz+ipdb
    .expert_level = 4
  {
    resolution_available = None
      .type = floats(size=2,value_min=0)
      .expert_level = 4
      .short_caption = Resolution limits for data that are available as input
    unitcell = None
      .type = unit_cell
      .short_caption = Unit cell of data
      .expert_level = 4
    number_of_reflections = None
      .type = int(value_min=0)
      .expert_level = 4
      .short_caption = number of reflections
    anomalous = None
      .type = bool
      .expert_level = 4
      .short_caption = Anomalous data provided
    intensities = None
      .type = bool
      .expert_level = 4
      .short_caption = Intensities provided
    french_wilson = None
      .type = bool
      .expert_level = 4
      .short_caption = Amplitudes were derived from intensities by French-Wilson procedure
    map = None
      .type = bool
      .expert_level = 4
      .short_caption = Amplitudes from a map
  }
''' ,

"twinning" : '''
  twinning
    .style = tng:input:+twin tng:result:+twin
    .expert_level = 1
  {
    test = *moments ltest moments_or_ltest moments_and_ltest
      .type = choice
      .expert_level = 1
      .short_caption = Moments, Padilla-Yeates L-test or either or both tests to indicate twinning
    padilla_yeates_test = 0.5
      .type = float(value_max=0.5)
      .expert_level = 2
      .short_caption = Twinning L-test threshold
    parity = 2 2 2
      .type = ints(size=3,value_min=2)
      .expert_level = 4
      .short_caption = Twinning L-test parity
    indicated = None
      .type = bool
      .expert_level = 4
      .short_caption = Twinning is indicated after anisotropy and tncs correction
  }
''' ,

"subgroup" : '''
  subgroup
    .style = tng:input:+sga tng:result:+sga
    .expert_level = 1
  {
    expansion
      .multiple = True
      .expert_level = 4
    {
      hall = None
        .type = str
        .expert_level = 4
        .short_caption = Space group Hall symbol
      spacegroup = None
        .type = str
        .expert_level = 4
        .short_caption = Space group Hermann Mauguin symbol
      hermann_mauguin_symbol = None
        .type = str
        .expert_level = 4
        .short_caption = Space group universal Hermann Mauguin symbol
    }
    original
      .expert_level = 4
    {
      hall = None
        .type = str
        .expert_level = 4
        .short_caption = Space group Hall symbol
      spacegroup = None
        .type = str
        .expert_level = 4
        .short_caption = Space group Hermann Mauguin symbol
      hermann_mauguin_symbol = None
        .type = str
        .expert_level = 4
        .short_caption = Space group universal Hermann Mauguin symbol
    }
    use_all_sysabs = False
      .type = bool
      .expert_level = 1
      .short_caption = Use all possible systematic absences in space group expansion
      .help = Toggle between all possible systematic absences in space group expansion and only using those with where the data obey the systematic absences
    use_no_sysabs = False
      .type = bool
      .expert_level = 1
      .short_caption = Use only the original space group in space group expansion
      .help = Toggle using none of the  possible systematic absences in space group expansion and only using those with where the data obey the systematic absences
  }
''' ,

"sgalt" : '''
  sgalt
    .style = tng:input:+ftf+ssd
    .expert_level = 1
  {
    selection = *all hand list original
      .type = choice
      .expert_level = 1
      .short_caption = Choice of space group for expansion
    spacegroups = None
      .type = strings
      .expert_level = 1
      .short_caption = Space group symbol
  }
''' ,

"sampling_generator" : '''
  sampling_generator
    .style = tng:input:+btf
    .expert_level = 4
  {
    change_of_basis_op = None
      .type = str
      .expert_level = 4
      .short_caption = Change of basis op
      .help = Change of basis op in quoted string, abc notation
    continuous_shifts = None
      .type = ints(size=3)
      .expert_level = 4
      .short_caption = Continuous shifts flags 0/1 for False/True
    cuts_normals = None
      .type = floats
      .expert_level = 4
      .short_caption = Normals to cut planes to be interpreted in three per cut plane
    cuts_constants = None
      .type = floats
      .expert_level = 4
      .short_caption = Constants for cut plane to be interpreted one per cut plane
  }
''' ,

"composition" : '''
  composition
    .style = tng:input:+cca tng:result:+cca
    .expert_level = 1
  {
    number_in_asymmetric_unit = None
      .type = float(value_min=0.000001)
      .expert_level = 1
      .short_caption = Z of biological unit
      .help = Number of copies of biological unit in the asymmetric unit. Value can be less than one (for cutout maps).
    solvent_range = 25 75
      .type = floats(size=2,value_min=0,value_max=100)
      .expert_level = 1
      .help = The range of acceptable solvent content for composition
    map = False
      .type = bool
      .expert_level = 1
      .help = The composition is for a map, and the Matthews composition is therefore not applicable
    maximum_z = None
      .type = int(value_min=0)
      .expert_level = 1
      .short_caption = Maximum number of copies in the asymmetric unit
    multiplicity_restriction = 1
      .type = int(value_min=1)
      .expert_level = 1
      .short_caption = Restrict number of copies to a multiple
      .help = By default, the tncs order recorded in the reflections will be applied to the composition analysis, but the multiplicity can be input separately.
    order_z_expansion_ratio = 1
      .type = int(value_min=1)
      .expert_level = 4
      .short_caption = The order of the expansion of copies in the unit cell if the data has been expanded from lower symmetry. The number of copies is restricted by the expansion.
    use_order_z_expansion_ratio = True
      .type = bool
      .expert_level = 1
      .short_caption = Restrict the multiplicity by the space group expansion ratio
    sort_by_probability = False
      .type = bool
      .expert_level = 4
      .help = Sort the Z values in probability order rather than integer order
  }
''' ,

"cell_content_scaling" : '''
  cell_content_scaling
    .style = tng:input:+ccs tng:result:+cca
    .expert_level = 1
  {
    z = None
      .type = float(value_min=0.000001)
      .expert_level = 1
      .short_caption = Z of biological unit
      .help = Number of copies of biological unit in the asymmetric unit. Value can be less than one (for cutout maps).
  }
''' ,

"permutations" : '''
  permutations
    .style = tng:input:+perm
    .expert_level = 1
  {
    tags = None
      .type = strings
      .multiple = True
      .expert_level = 1
      .short_caption = List of search models by tag, unordered but covering biological unit, multiple possibilities for analysis
    matthews_z = 1
      .type = int
      .expert_level = 1
      .short_caption = Multiplier for tags in the biological unit
    tags_in_search_order = None
      .type = strings
      .expert_level = 1
      .short_caption = List of search models by tag in search order across all components in the asymmetric unit
  }
''' ,

"biological_unit" : '''
  biological_unit
    .style = tng:input:+bub+cca+ssa
    .expert_level = 1
  {
    sequence
      .help = Sequence in fasta format that contains the sequence of the biological unit in the crystallographic asymmetric unit/unit cell. This is the biological unit that you observe in solution. The file will be interpreted in terms of the number of atoms of each element type. Selenomethionine can be specified in the file with the char '$' or, alternatively, all methionines will be interpreted as seleomethionine if the seleomethionine flag is set. Cysteines in disulphides can be specified as '%' or, alternatively, all cysteines will be interpreted as half-disuphides if the disulphide flag is set. Heavy atoms known to be in (or associated with) the biological unit (e.g. Fe in haem) should be entered separately using the hetatm input.
      .expert_level = 1
    {
      filename = None
        .style = file_type:seq
        .type = filesystem(directory=False,ext="fa")
        .expert_level = 1
        .short_caption = Sequence file with sequences separated by fasta header specifier > and with text after > parsed for strings PROTEIN DNA RNA or NUCLEIC and result used to distinguish cases where there is sequence type ambiguity (all ACGT)
      multiplicity = 1
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Biological unit is a multiple of the sequence in the file
    }
    model
      .expert_level = 1
    {
      filename = None
        .type = filesystem(directory=False,ext="pdb")
        .expert_level = 1
        .short_caption = Single model coordinate file
        .help = Single model coordinate file, pdb format, for calculating biological unit. Alternative to using biological unit defined by sequence.
      via_sequence = True
        .type = bool
        .expert_level = 1
        .short_caption = Use the sequence of the model for the composition, not the atomic composition of the model
        .help = Use the sequence of the model for the composition, not the atomic composition of the model. Incomplete sidehains will be completed before calculation of scattering.
    }
    solvent = None
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Solvent content to determine scattering in the asymmetric unit
      .help = Solvent content to use to determine scattering in the asymmetric unit, assuming protein, when there is no sequence or model entered. The default value is average Matthews value for resolution of the data.
    selenomethionine = False
      .type = bool
      .expert_level = 1
      .short_caption = Treat methionine as selenomethionine
      .help = Treat all methionine as selenomethionine, or, alternatively, enter sequence with $ for selenomethionine for sequence-level control.
    disulphide = False
      .type = bool
      .expert_level = 1
      .short_caption = Treat cysteine pairs as disulphide clusters
      .help = Treat all cysteines as half-disulphides, or, alternatively, enter sequence with % for half-disulphide (replacing C) for sequence-level control.
    deuterium = False
      .type = bool
      .expert_level = 2
      .short_caption = Treat hydrogens as deuteriums
    water_percent = 0
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Percentage by which to inflate the scattering from the sequence before use as the total scattering, because the presence of ordered water increases the scattering over that calculated from just the sequence - values of around 5% are appropriate
    hetatm = None
      .multiple = True
      .type = strings
      .expert_level = 1
      .short_caption = Atom type (element) and number (integer) in the asymmetric unit, with zero meaning unknown
  }
''' ,

"matthews" : '''
  matthews
    .style = tng:result:+cca
    .multiple = True
    .expert_level = 4
  {
    vm = None
      .type = float(value_min=0)
      .expert_level = 4
      .help = Matthews coefficients for number of copies of biological unit in the asymmetric unit
      .short_caption = Matthews coefficients : Vm
    probability = None
      .type = float(value_min=0,value_max=1.0)
      .expert_level = 4
      .help = Probability for number of copies of biological unit in the asymmetric unit
      .short_caption = Matthews coefficients : probability
    z = None
      .type = float(value_min=0)
      .expert_level = 4
      .help = Z for number of copies in the asymmetric unit
      .short_caption = Matthews coefficients: Z
  }
''' ,

"search" : '''
  search
    .style = tng:input:+brf+ellg+frf+perm+put tng:result:+emm+esm+msm
    .expert_level = 1
  {
    id = None
      .type = uuid(value_min=0)
      .expert_level = 1
      .short_caption = Identifier for model
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying model
      .help =  Tag can be used in place of identifier to define model, provided unique.
  }
''' ,

"gyration" : '''
  gyration
    .style = tng:input:+gyre
    .multiple = True
    .expert_level = 1
  {
    id = None
      .type = uuid(value_min=0)
      .expert_level = 1
      .help = Identifier for model, previously prepared from splitting a model into chains or domains for separate gyre refinement. List of two or more identifiers from split of original model.
    tag = None
      .type = str
      .expert_level = 1
      .help =  Tag can be used in place of identifier to define model, provided unique. Leave either as None to use other specifier.
  }
''' ,

"model_as_map" : '''
  model_as_map
    .style = tng:input:+msm tng:result:+msm
    .expert_level = 1
  {
    filename = None
      .type = filesystem(directory=False,ext="map")
      .expert_level = 1
      .short_caption = Map file for map preparation in mtz format
    resolution = 0
      .type = float(value_min=0)
      .expert_level = 4
      .short_caption = Resolution limit for map read
    fmap = None
      .type = str
      .expert_level = 1
      .short_caption = F for model-map if map entered as mtz file, defaults to only F in mtz
    phimap  = None
      .type = str
      .expert_level = 1
      .short_caption = Phase for model-map if map entered as mtz file, defaults to only PHI in mtz
    tag = None
      .type = str
      .expert_level = 1
      .short_caption = Short tag identifying map
      .help =  Tag can be used in place of identifier to define map, provided unique. By default, the tag is taken as the stem of the filename. Non-alphanumeric characters in the tag string are substituted with the underscore character.
    sequence
    {
      filename = None
        .type = filesystem(directory=False,ext="fa")
        .expert_level = 1
        .short_caption = Sequence file, sequences separated by '>', with text after '>' parsed for PROTEIN,DNA,RNA,NUCLEIC in cases of sequence type ambiguity to declare type.
    }
    vrms_estimate = 1.0
      .type = float(value_min=0)
      .expert_level = 1
      .help = Estimate of experimental error between map and reflections, but may be modified by the value set in the first fast rotation function
    point_group_symbol = None
      .type = str
      .expert_level = 1
      .short_caption = Point group of biological unit
    point_group_euler = None
      .type = floats
      .expert_level = 1
      .short_caption = Point group euler angles for model
      .help = Point group euler angles for an model that is (in itself) an oligomer, read in groups of three
    centre = None
      .type = floats(size=3)
      .expert_level = 1
      .short_caption = Centre of map in cartesian coordinates
    solvent_constant = 0
      .type = float
      .expert_level = 1
      .short_caption = Solvent value in map padding
    extent_minimum = None
      .type = floats(size=3)
      .expert_level = 4
      .short_caption = Minimum box orthogonal coordinates for extent of map density
    extent_maximum = None
      .type = floats(size=3)
      .expert_level = 4
      .short_caption = Maximum box orthogonal coordinates for extent of map density
    wang_volume_factor = 0.5
      .type = float(value_min=0.5,value_max=2.0)
      .expert_level = 2
      .short_caption = Density volume factor for map volume selection based on molecular weight
    wang_unsharpen_bfactor = 100
      .type = float(value_min=0,value_max=150)
      .expert_level = 2
      .short_caption = Map bfactor applied to map before trace volume determination
    oversample_resolution_limit = 0.5
      .type = float(value_min=0.1)
      .expert_level = 2
      .short_caption = Resolution limit for read of oversampled maps
    no_phases = False
      .type = bool
      .expert_level = 4
      .short_caption = Read an mtz file with no phases
    require_box = True
      .type = bool
      .expert_level = 4
      .short_caption = Require the map to be a cuboid (box), which must be True for molecular replacement but can be False for molecular replacement with SAD phasing (MR-SAD)
  }
''' ,

"trace" : '''
  trace
    .style = tng:input:+emm+esm+msm
    .expert_level = 2
  {
    filename = None
      .type = filesystem(directory=False,ext="pdb")
      .expert_level = 2
      .short_caption = Single model coordinate file
      .short_caption = Coordinates for trace
    volume_sampling = all calpha hexgrid *optimal
      .type = choice
      .expert_level = 2
      .short_caption = Method for sampling trace volume
      .help = Sample trace volume using all atoms or a hexagonal grid filling the atomic volume or calpha atoms or use the optimal of all, calpha and hexgrid. Maps will use hexagonal grid.
    trim_asa = True
      .type = bool
      .expert_level = 2
      .short_caption = Trim surface atoms from model when generating trace
      .help = Trim surface atoms from model when generating trace. This is not recommended if the sequence identity between model and target is very high.
    hexgrid
    {
      number_of_points = 900 1100
        .type = ints(size=2)
        .expert_level = 5
        .short_caption = Target number of points in trace grid
        .help = Target number of points in grid filling trace volume, target will optimize to midpoint of range in ncyc cycles subject to ncyc, minimum_distance, and distance
      ncyc = 10
        .type = int(value_min=1)
        .expert_level = 5
        .short_caption = Maximum number of cycles for optimizing number of grid points
        .help = Maximum number of cycles for optimizing number of grid points
      minimum_distance = 1.0
        .type = float
        .expert_level = 5
        .short_caption = Minimum distance for the sampling
        .help = Minimum distance for the sampling of trace grid in Angstroms
      distance = None
        .type = float
        .expert_level = 5
        .help = Set the distance for the sampling of the hexagonal grid explicitly and do not use number_of_points to find the sampling distance
    }
  }
''' ,

"point_group" : '''
  point_group
    .style = tng:input:+emm+esm
    .expert_level = 2
  {
    calculate_from_coordinates = True
      .type = bool
      .expert_level = 5
      .short_caption = Calculate point group from coordinates
    coverage = 90
      .type = float(value_min=0,value_max=100)
      .expert_level = 5
      .short_caption = Coverage for point group analysis
    identity = 90
      .type = float(value_min=0,value_max=100)
      .expert_level = 5
      .short_caption = Identity for point group analysis
    rmsd = 3.0
      .type = float(value_min=0.1)
      .expert_level = 5
      .short_caption = Rms deviation for point group analysis
    angular_tolerance = 0.035
      .type = float(value_min=0.001)
      .expert_level = 5
      .short_caption = Angle tolerance for point group analysis
    spatial_tolerance = 1.0
      .type = float(value_min=0.01)
      .expert_level = 5
      .short_caption = Spatial tolerance for point group analysis
  }
''' ,

"cell_scale" : '''
  cell_scale = 0.9 1.1
    .style = tng:input:+emm+esm+jog+msm+rbr+rmr
    .expert_level = 2
    .type = floats(size=2,value_min=0.5,value_max=2)
    .short_caption = Minimum/Maximum scale for cell
''',

"bfactor" : '''
  bfactor
    .style = tng:input:+fuse+jog+move+pose+rbr+rmr+ssm
    .expert_level = 5
  {
    minimum = None
      .type = float(value_min=-1000,value_max=1000)
      .expert_level = 5
      .short_caption = Minimum value of B-factor relative to Wilson-B, default is to use -(Wilson-B)
    maximum = 500
      .type = float(value_min=-1000,value_max=1000)
      .expert_level = 5
      .short_caption = Maximum value of B-factor relative to Wilson-B
  }
''',

"scattering" : '''
  scattering
    .style = tng:input:+eatm+ssa+ssd+ssm+ssp+ssr
    .expert_level = 1
  {
    fluorescence_scan
      .multiple = True
      .expert_level = 1
    {
      scatterer = None
        .type = scatterer
        .expert_level = 1
        .short_caption = Scattering type (element)
      fp = None
        .type = float
        .expert_level = 1
        .short_caption = Scattering f-prime measured by fluorescence scan
        .help = Scattering f-prime measured by fluorescence scan. Default value is taken from tabulated value at wavelength.
      fdp = None
        .type = float
        .expert_level = 1
        .short_caption = Scattering f-double-prime measured by fluorescence scan
        .help = Scattering f-double-prime measured by fluorescence scan. Default value is taken from tabulated value at wavelength.
      fix_fdp = on off *edge
        .type = choice
        .expert_level = 1
        .short_caption = Fix f-double-prime measured by fluorescence scan
        .help = Fix f-double-prime measured by fluorescence scan. Default is to fix values near an edge.
    }
    sasaki_table
      .multiple = True
      .expert_level = 1
    {
      scatterer = None
        .type = scatterer
        .expert_level = 1
        .short_caption = Scattering type (element)
      move_to_edge = True
        .type = bool
        .expert_level = 2
        .short_caption = Move f-prime and f-double-prime to edge if near an edge
    }
    form_factors = *xray electron neutron
      .type = choice
      .expert_level = 0
      .short_caption = Scattering Form Factor Particle type
      .help = Scattering Form Factor Particle type
      .caption = Use_xray_scattering,  Use_electron_scattering,  Use_neutron_scattering
  }
''' ,


"resolution" : '''
  resolution = 0
    .style = tng:input:+brf+btf+data+ellg+emap+etfz+frf+frfr+ftf+ftfr+fuse+gyre+ipdb+jog+mapz+move+msm+perm+pose+ptf+rbgs+rbm+rbr+rmr+spr+srf+ssd+ssp+ssr+tfz+tncs
    .type = float(value_min=0)
    .expert_level = 1
    .short_caption = Soft resolution limit for calculations
    .help = Include reflections up to high resolution limit in calculations. The data preparation steps do not use this keyword as high resolution data are required for accurate data analysis. Use reflections.resolution for global rejection of high resolution data on input. Data resolution for calculations is also controlled by the signal resolution determined by the expected-llg.
''' ,

"put_solution" : '''
  put_solution
    .style = tng:input:+put
    .expert_level = 1
  {
    use_tncs = False
      .type = bool
      .expert_level = 1
      .short_caption = Put model and add tncs translations
    clear = True
      .type = bool
      .expert_level = 4
      .short_caption = Clear current poses before appending
    dag
    {
      filename = None
        .type = filesystem(directory=False,ext="dag.cards")
        .expert_level = 1
        .short_caption = Directed acyclic graph file
    }
    pose
    {
      euler = 0 0 0
        .type = floats(size=3)
        .expert_level = 1
        .short_caption = Euler angles for pose
      fractional = 0 0 0
        .type = floats(size=3)
        .expert_level = 1
        .short_caption = Fractional coordinates for pose
      orthogonal_rotation = 0 0 0
        .type = floats(size=3)
        .expert_level = 1
        .short_caption = Rotation shift in degrees relative to pose euler angle rotation
      orthogonal_position = 0 0 0
        .type = floats(size=3)
        .expert_level = 1
        .short_caption = Translation shift in Angstroms relative to pose fractional translation
      bfactor = 0
        .type = float(value_min=-999,value_max=999)
        .expert_level = 1
        .short_caption = B-factor for pose
    }
  }
''' ,

"tncs_order" : '''
  tncs_order
    .style = tng:input:+tncso
    .expert_level = 2
  {
    selection_by_rank = None
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Selection of tncso analysis result by rank as default for tncs, default is rank 1
    include_no_tncs = True
      .type = bool
      .expert_level = 2
      .short_caption = Include tncs order 1 for cases of lattice translocation defects
    minimum_number_of_reflections = 50
      .type = int(value_min=3)
      .expert_level = 5
      .short_caption = Number of reflections at which there is insufficient data
    minimum_resolution = 10
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Resolution of data at which there is insufficient resolution of data
    patterson
    {
      resolution = 5.0 10.0
        .type = floats(size=2,value_min=0)
        .expert_level = 5
        .short_caption = Resolution limits for Patterson calculation
      percent = 20
        .type = float(value_min=0,value_max=100)
        .expert_level = 2
        .short_caption = Minimum percent of Patterson origin peak height
        .help = Cutoff for selection of peaks from Patterson in terms of percent, value is minimum of zscore and percent
      origin_distance = 15
        .type = float(value_min=0)
        .expert_level = 2
        .short_caption = Minimum distance of Patterson peak from origin
    }
    frequency_cutoff = 50
      .type = float(value_min=0,value_max=100)
      .expert_level = 5
      .short_caption = Commensurate modulation test parameter
      .help = Percentage of maximum frequency analysis peak height to consider for commensurate modulation.
  }
''' ,

"tncs_analysis" : '''
  tncs_analysis
    .style = tng:result:+tncso
    .expert_level = 4
  {
    result
      .multiple = True
    {
      order = None
        .type = int
        .expert_level = 4
        .short_caption = Result of tNCS order (tncso) analysis, number of copies related by tncs
        .help = Result of tNCS order (tncso) analysis. Number of copies related by tncs. Possibilities for the number of molecules or complexes related by translation vector, either by commensurate or non-commensurate tncs.
      vector = None
        .type = floats(size=3)
        .expert_level = 4
        .short_caption = Result of tncso analysis, vector for tncs
        .help = Result of tncso analysis. Vector for tncs corresponding to tncso analysis result.
      parity = None
        .type = ints(size=3)
        .expert_level = 4
        .short_caption = Result of tncso analysis, frequency index for tncs
      unique = None
        .type = bool
        .expert_level = 4
        .short_caption = TNCS vector is unique under symmetry
    }
    number = None
      .type = int
      .expert_level = 2
      .short_caption = Number of options for tncs relationship in results
    insufficient_data = False
      .type = bool
      .expert_level = 4
      .short_caption = Warning for insufficent data
    insufficient_resolution = False
      .type = bool
      .expert_level = 4
      .short_caption = Warning for insufficent resolution of data
    coiled_coil = False
      .type = bool
      .expert_level = 4
      .short_caption = Warning for coiled coil
    indicated = None
      .type = bool
      .expert_level = 4
      .short_caption = translational non-crystallographic symmetry is indicated
  }
''' ,

"tncs" : '''
  tncs
    .style = tng:input:+tncs+tncso+twin tng:result:+tncs+tncso
    .expert_level = 2
  {
    order = None
      .type = int
      .expert_level = 1
      .short_caption = Number of molecules or complexes related by translation vector
    vector = None
      .type = floats(size=3)
      .expert_level = 1
      .short_caption = TNCS vector, default is to use internally determined vector
    parity = None
      .type = ints(size=3)
      .expert_level = 4
      .short_caption = TNCS frequency in reciprocal space used for reflection selection in twinning L-test
    unique = None
      .type = bool
      .expert_level = 4
      .short_caption = TNCS vector is unique under symmetry
    gfunction_radius = None
      .type = float(value_min=10)
      .expert_level = 2
      .short_caption = TNCS gfunction radius
    rmsd = 0.2
      .type = float(value_min=0.1,value_max=3.0)
      .expert_level = 5
      .short_caption = Initial rmsd between tNCS copies
    present_in_model = False
      .type = bool
      .expert_level = 0
      .short_caption = The tncs duplications are present in all search models
    refine
      .expert_level = 5
    {
      perturb_special_position = True
        .type = bool
        .expert_level = 5
        .short_caption = Perturb translation vector from patterson/input position if it is on a special position. Perturbation is hires/6/cell.
      rotation_sampling = None
        .type = float
        .expert_level = 5
        .short_caption = Sampling (degrees) around origin for initiating TNCS rotation refinements. Internally determined default depends on the G-function radius.
      rotation_range = None
        .type = float
        .expert_level = 5
        .short_caption = Range (degrees) around origin for initiating TNCS rotation refinements. Internally determined default.
    }
  }
''' ,

"coiled_coil" : '''
  coiled_coil = None
    .style = tng:input:+brf+frf+frfr+ftf+ftfr+gyre+tncso
    .type = bool
    .expert_level = 2
    .short_caption = The target contains a coiled coil, by default specified during the tncs analysis by looking for coiled-coiled Patterson peaks
''' ,

"outlier" : '''
  outlier
    .style = tng:input:+aniso+brf+btf+ccs+data+eatm+ellg+emap+etfz+feff+frf+frfr+ftf+ftfr+fuse+gyre+info+jog+mapz+move+msm+perm+pose+ptf+rbgs+rbm+rbr+rellg+rmr+spr+srf+ssa+ssp+tfz+tncs+tncso+twin
    .expert_level = 1
  {
    reject = True
      .type = bool
      .expert_level = 5
      .short_caption = Reject outliers
    probability = 1e-06
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Reject outlier probability threshold
    information = 0.01
      .type = float(value_min=0.00000001)
      .expert_level = 5
      .short_caption = Reject outliers information threshold
    maximum_printed = 20
      .type = int(value_min=0)
      .expert_level = 3
      .short_caption = Number to print to logfile
  }
''' ,

"solvent_sigmaa" : '''
  solvent_sigmaa
    .style = tng:input:+emap+emm+esm+msm+spr
    .expert_level = 2
  {
    reflections_versus_model
    {
      fsol = 1.05
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa scale parameter for model
      bsol = 501.605
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa B-factor parameter for model
      babinet_minimum = 0.1
        .type = float(value_min=0.01)
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa minimum value of sigmaa for model
    }
    reflections_as_map_versus_model
    {
      fsol = 0.9
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa scale parameter for map
      bsol = 501.605
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa B-factor parameter for map
      babinet_minimum = 0.1
        .type = float(value_min=0.01)
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa minimum value of sigmaa for map
    }
    reflections_versus_model_as_map
    {
      fsol = 0.30
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa scale parameter for density
      bsol = 501.605
        .type = float
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa B-factor parameter for density
      babinet_minimum = 0.1
        .type = float(value_min=0.01)
        .expert_level = 2
        .short_caption = Bulk solvent sigmaa minimum value of sigmaa for density
    }
  }
''' ,

"segmented_pruning" : '''
  segmented_pruning
    .style = tng:input:+spr
    .expert_level = 1
  {
    segment
      .expert_level = 1
    {
      delta_ellg = 5
        .type = float(value_min=5)
        .expert_level = 1
        .short_caption = Target eLLG for residue window selection
      rmsd = 1
        .type = float
        .expert_level = 1
        .short_caption = Residue window rmsd
      residues = None
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Number of residues for segmentation, default determined by target ellg
    }
    pruning
      .expert_level = 1
    {
      low_resolution = 10000
        .type = float(value_min=0)
        .expert_level = 1
        .short_caption = Low resolution for pruning analysis
      maximum_percent = 20
        .type = float(value_min=0,value_max=100)
        .expert_level = 1
        .short_caption = Maximum percent of the structure that will be pruned
      llg_bias_from_maximum_percent = 10
        .type = float(value_min=0)
        .expert_level = 1
        .short_caption = Reduction from maximum llg allowed to favour higher completeness
    }
    maximum_stored = 5
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number to refine. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
  }
''' ,

"zscore_equivalent" : '''
  zscore_equivalent
    .style = tng:input:+tfz
    .expert_level = 1
  {
    percent = 75
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum 100*(value-mean)/(top-mean) for positions stored
    nrand = 500
      .type = int(value_min=100,value_max=4000)
      .expert_level = 2
      .short_caption = Number of random points for determining mean and standard deviation
    range_zscore = 5 11
      .type = floats(size=2,value_min=0,value_max=99)
      .expert_level = 2
      .short_caption = Zscore range from fast translation function in which zscore is recalculated after refinement
    range_percent = 90
      .type = float(value_min=0,value_max=100)
      .expert_level = 2
      .short_caption = LLG (top-low)*range_percent+low cutoff for zscore calculation, for speed
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
  }
''',

"zscore_equivalent_map" : '''
  zscore_equivalent_map
    .style = tng:input:+mapz
    .expert_level = 1
  {
    percent = 75
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum 100*(value-mean)/(top-mean) for translation function positions stored
    nrand = 500
      .type = int(value_min=100,value_max=4000)
      .expert_level = 2
      .short_caption = Number of random points for determining mean and standard deviation
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
  }
''',

"map_correlation_coefficient" : '''
  map_correlation_coefficient
    .style = tng:input:+mcc
    .expert_level = 1
  {
    similarity = 0.95
      .type = float(value_min=0.4,value_max=1)
      .expert_level = 2
      .short_caption = Threshold for map similarity
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of nodes stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of nodes printed to logfile
  }
''',

"macaniso" : '''
  macaniso
    .style = tng:input:+aniso
    .short_caption = Anisotropy correction for data
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *scale *aniso *bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Anisotropy refinement macrocycle
      .help = Choice of parameters to refine in each anisotropy macrocycle. Constant scale factor (scale), resolution bin-wise scale factor (bin) and anisotropy parameters (aniso). Fix all parameters (off).
    macrocycle2 = *off scale aniso bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Anisotropy refinement macrocycle
      .help = See macrocycle1
    macrocycle3 = *off scale aniso bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Anisotropy refinement macrocycle
      .help = See macrocycle1
    ncyc = 100 100 100
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    best_curve_restraint = True
      .type = bool
      .expert_level = 5
      .short_caption = Impose restraint on bin-wise correction of best curve
    resolution = 0
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Resolution limit
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macbox" : '''
  macbox
    .style = tng:input:+msm
    .short_caption = Anisotropy correction for map
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *scale *aniso *bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Map anisotropy refinement macrocycle
      .help = Choice of parameters to refine in each map anisotropy macrocycle. Constant scale factor (scale), resolution bin-wise scale factor (bin) and anisotropy parameters (aniso). Fix all parameters (off).
    macrocycle2 = off *scale *aniso *bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Anisotropy refinement for map macrocycle
      .help = See macrocycle1
    macrocycle3 = off *scale *aniso *bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Anisotropy refinement for map macrocycle
      .help = See macrocycle1
    ncyc = 100 100 100
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    best_curve_restraint = True
      .type = bool
      .expert_level = 5
      .short_caption = Impose restraint on bin-wise correction of best curve
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"mactncs" : '''
  mactncs
    .style = tng:input:+tncs
    .short_caption = Tncs correction
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *rot gfn *tra *rmsd *fs bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = tNCS refinement macrocycle
      .help = Choice of parameters to refinem in each tNCS macrocycle. Tncs rotation (rot), translation (tra), G-function radius (gfn), rmsd between tncs related copies (rmsd) and either fraction of the scattering related by the tncs (fs) or bin-wise scattering (bins). Fix all parameters (off).
    macrocycle2 = off *rot gfn *tra rmsd fs *bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = tNCS refinement cycle
      .help = See macrocycle1
    macrocycle3 =  *off rot gfn tra rmsd fs bins
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = tNCS refinement cycle
      .help = See macrocycle1
    ncyc = 100 100 100
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    perturbation = None
      .type = ints(value_min=0)
      .expert_level = 5
      .short_caption = Selection of rotation angles for perturbation from default list
      .help = Selection of rotation angles for perturbation from default list where 0 is the unperturbed angle. Can be used to force the result of one of the perturbations. None for no selection.
    restraint_sigma
      .expert_level = 5
    {
      rot = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Rotation sigma, zero for no restraint
        .help = Restrain rotation angle to zero
      tra = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Traslation sigma, zero for no restraint
        .help = Restrain translation vector to initial value
      gfn = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = G-function radius sigma, zero for no restraint
        .help = Restrain G-function radius to input value, as a fraction of the input value
      rmsd = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = RMS sigma, zero for no restraint
        .help = Restrain rms deviation between tncs-copies to input value
      fs = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Fraction scattering sigma, zero for no restraint
        .help = Restrain fraction scattering related by tncs to 1
      binwise_unity = 0.01
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Bins scale factor sigma, zero for no restraint
        .help = Restrain log of fraction scattering related by tncs per resolution bin to 1
      smooth = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Bins scale factor sigma for smoothing, zero for no restraint
        .help = Restrain fraction scattering related to neighbouring bins
    }
    resolution = 0
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Resolution limit
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macrbr" : '''
  macrbr
    .style = tng:input:+rbr
    .short_caption = Refinement of coordinates against intensities
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *rotn *tran *bfac *vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = Choice of parameters to refine in each molecular replacement rigid body macrocycle. Rotation (rotn), translation (tran), individual B-factor (bfac), model vrms (vrms), cell scale factor for maps (cell). Alternatively, fix all parameters (off).
    macrocycle2 = *off rotn tran bfac vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = See macrocycle1
    macrocycle3 = *off rotn tran bfac vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = See macrocycle1
    ncyc = 50 50 50
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles per macrocycle
    last_only = False
      .type = bool
      .expert_level = 4
      .short_caption = Flag for last only in refinement
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    small_target = 0.01
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Limit of change in LLG taken as convergence
    restraint_sigma
      .expert_level = 5
    {
      rotation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Rotation sigma, zero for no restraint
      translation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Translation sigma, zero for no restraint
      bfactor = 6
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = B-factor sigma, zero for no restraint
      drms = 1
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Drms sigma restraint to input, zero for no restraint
      cell = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Cell sigma restraint to input, zero for no restraint
    }
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of poses stored. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of poses printed to logfile
    purge_duplicates = True
      .type = bool
      .expert_level = 1
      .short_caption = Find and purge duplicates
    cleanup = False
      .type = bool
      .expert_level = 1
      .short_caption = Roll the refinement perturbations into the pose orientation and translation
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macjog" : '''
  macjog
    .style = tng:input:+jog
    .short_caption = Refinement of coordinates against intensities after jog
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *rotn *tran *bfac *vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = Choice of parameters to refine in each molecular replacement rigid body macrocycle. Rotation (rotn), translation (tran), individual B-factor (bfac), model vrms (vrms), cell scale factor for maps (cell). Only refine the last placed tncs group (last). Fix all parameters (off).
    macrocycle2 = *off rotn tran bfac vrms cell last
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = See macrocycle1
    macrocycle3 = *off rotn tran bfac vrms cell last
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Molecular replacement rigid body refinement macrocycle
      .help = See macrocycle1
    ncyc = 50 0 0
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles per macrocycle
      .help = Maximum number of cycles per macrocycle. Jog refinement requires many cycles to converge.
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    small_target = 0.01
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Limit of change in LLG taken as convergence
    restraint_sigma
      .expert_level = 5
    {
      rotation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Rotation sigma, zero for no restraint
      translation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Translation sigma, zero for no restraint
      bfactor = 6
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = B-factor sigma, zero for no restraint
      drms = 1
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Drms sigma restraint to input, zero for no restraint
      cell = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Cell sigma restraint to input, zero for no restraint
    }
  }
''' ,

"macspr" : '''
  macspr
    .style = tng:input:+spr
    .short_caption = Refinement of occupancy or B-values against intensities
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off bfac *occ
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Segemented occupancy or B-factor refinement macrocycle
      .help = Choice of parameters to refine in each segmented B-factor refinement macrocycle. Segemented B-factor (bfac). Fix all parameters (off).
    macrocycle2 = *off bfac occ
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Segemented occupancy or B-factor refinement macrocycle
    macrocycle3 = *off bfac occ
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Segemented occupancy or B-factor refinement macrocycle
    ncyc = 50 50 50
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    small_target = 0.1
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Limit of change in LLG taken as convergence
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macrm" : '''
  macrm
    .style = tng:input:+rmr
    .short_caption = Refinement of coordinates against a map
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 =  off *rotn *tran bfac vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Map molecular placement refinement macrocycle
      .help = Choice of parameters to refine in each map molecular placement macrocycle. Rotation (rotn), translation (tran), individual B-factor (bfac), model vrms (vrms), cell scale factor for maps (cell). Fix all parameters (off).
    macrocycle2 = off *rotn *tran bfac *vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Map molecular placement refinement macrocycle
      .help = See macrocycle1
    macrocycle3 = *off rotn tran bfac vrms cell
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Map molecular placement refinement macrocycle
      .help = See macrocycle1
    ncyc = 50 50 50
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    small_target = 0.1
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Limit of change in LLG taken as convergence
    restraint_sigma
      .expert_level = 5
    {
      rotation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Rotation sigma, zero for no restraint
      translation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Translation sigma, zero for no restraint
      bfactor = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = B-factor sigma, zero for no restraint
      drms = 1
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Drms sigma restraint to input, zero for no restraint
      cell = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Cell sigma restraint to input, zero for no restraint
    }
    maximum_stored = 10000
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of nodes. Zero for all. Changing this number changes the reporting and also how many solutions are passed in the dag to the next node.
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of nodes printed to logfile
    percent = 75.
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Range of 100*(value/top) for poses stored
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macgyre" : '''
  macgyre
    .style = tng:input:+gyre
    .short_caption = Gyre refinement
    .expert_level = 1
  {
    protocol = off *on
      .type = choice
      .expert_level = 1
      .short_caption = Turn off all refinement
    macrocycle1 = off *rotn *tran *vrms
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Gyre refinement macrocycle
      .help = Choice of parameters to refine in each gyre macrocycle. Rotation (rotn), relative translation of second and subsequent gyre components with respect to first component (tran), individual B-factor (bfac), model vrms (vrms). Fix all parameters (off).
    macrocycle2 = *off rotn tran vrms
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Gyre refinement macrocycle
      .help = See macrocycle1
    macrocycle3 = *off rotn tran vrms
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Gyre refinement macrocycle
      .help = See macrocycle1
    ncyc = 50 50 50
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    small_target = 0.01
      .type = float(value_min=0)
      .expert_level = 5
      .short_caption = Limit of change in LLG taken as convergence
    restraint_sigma
      .expert_level = 5
    {
      rotation = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Rotation sigma, zero for no restraint
      position = 0
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Relatvie position sigma, zero for no restraint
    }
    maximum_printed = 20
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Maximum number of nodes printed to logfile
    purge_duplicates = True
      .type = bool
      .expert_level = 1
      .short_caption = Find and purge duplicates
    refine_helices = False
      .type = bool
      .expert_level = 5
      .short_caption = Helices are not refined by default, as the refinement is unstable
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macssa" : '''
  macssa
    .style = tng:input:+ssa
    .short_caption = Substructure content analysis
    .expert_level = 1
  {
    macrocycle1 = off *rhoff *rhopm
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure content analysis refinement macrocycle
    macrocycle2 = off *rhoff *rhopm
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure content analysis refinement macrocycle
    macrocycle3 = *off rhoff rhopm
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure content analysis refinement macrocycle
    ncyc = 100 100 100
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    restraint_sigma
      .expert_level = 5
    {
      rhopm = 0.2
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = rhopm sigma scale, zero for no restraint
      atoms = 1.5
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Atom number ratio sigma, zero for no restraint
    }
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

"macsad" : '''
  macsad
    .style = tng:input:+ssd+ssr
    .short_caption = Substructure content refinement
    .expert_level = 1
  {
    macrocycle1 = off data siga sigb sigp  sigd  site  bfac *occ fdp *part
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure refinement macrocycle
    macrocycle2 = off data siga sigb sigp *sigd *site *bfac *occ fdp *part
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure refinement macrocycle
    macrocycle3 = *off data siga sigb sigp sigd site bfac occ fdp part
      .type = choice(multi=True)
      .expert_level = 2
      .short_caption = Substructure refinement macrocycle
    ncyc = 50 50 50
      .type = ints(size=3,value_min=0)
      .expert_level = 2
      .short_caption = Maximum number of cycles
    minimizer = *bfgs newton
      .type = choice
      .expert_level = 5
      .short_caption = Minimization method
    hand = False
      .type = bool
      .expert_level = 1
      .short_caption = Refine other hand of substructure
    enantiomorphs = True
      .type = bool
      .expert_level = 1
      .short_caption = Refine both enantiomorphs of substructure if possible
    use_partial_anomalous = false
      .type = bool
      .expert_level = 1
      .short_caption = Use any anomalous scattering in partial structure
    restraint_sigma
      .expert_level = 5
    {
      sphericity = 10
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = sphericity sigma (also depends on resolution), zero for no restraint
      wilson_bfactor = 2
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = Wilson-B restraint sigma, zero for no restraint
      fdp = 5
        .type = float(value_min=0)
        .expert_level = 5
        .short_caption = fdp sigma, zero for no restraint
    }
    integration_steps = None
      .type = int(value_min=0)
      .expert_level = 5
      .short_caption = Set number of integration steps for numerical integral, for debugging
    study_parameters = False
      .type = bool
      .expert_level = 5
      .short_caption = Produce diagnostics for studying target, gradient, hessian
      .help = Check consistency of target, gradient and hessian by finite differences
  }
''' ,

#functions must also be added to the Phasertng::set_user_input_for_suite function

"hklin" : '''
  hklin
    .style = tng:input:+beam+bub+hyss+imtz
    .expert_level = 1
  {
    filename = None
      .style = file_type:mtz
      .type = filesystem(directory=False,ext="mtz")
      .expert_level = 1
      .short_caption = Reflections file with X-ray data (mtz format)
    tag = None
      .type = str
      .expert_level = 4
      .short_caption = Short tag identifying reflections
      .help = By default the stem of the filename will be used for the identifier tag.
  }
''' ,

"reflid" : '''
  reflid
    .style = tng:input:+aniso+brf+btf+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rmr+sga+sgx+span+spr+srf+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+twin
    .expert_level = 4
  {
    tag = None
      .type = str
      .expert_level = 4
      .short_caption = Short tag identifying reflections
      .help =  Tag can be used in place of identifier to define reflections, provided unique. By default, the tag is taken as the stem of the filename. Non-alphanumeric characters in the tag string are substituted with the underscore character.
    id = None
      .type = uuid(value_min=0)
      .expert_level = 4
      .short_caption = Identifier for reflections
    force_read = False
      .type = bool
      .expert_level = 4
      .short_caption = Force a read of the reflections, as specified by the identifier/tag, to override database reflection identifier/tag
  }
''' ,

"hklout" : '''
  hklout
    .style = tng:result:+aniso+beam+cca+ccs+data+exp1+feff+imap+imtz+info+ipdb+sgx+ssa+tncs+twin
    .expert_level = 4
  {
    filename = None
      .type = filesystem(directory=False,ext=".mtz")
      .expert_level = 4
      .short_caption = Output filename for mtz file
  }
''' ,

#when defined as path the data_manager decides it is an mtzin file !!
"mtzout" : '''
  mtzout = None
    .style = tng:input:+info
    .expert_level = 3
    .type = str
    .short_caption = Non-database filename for output of processed reflection data
''' ,

#keywords for all modes
"suite" : '''
  suite
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .expert_level = 0
  {
    level = *default silence process concise summary logfile verbose testing
      .type = choice
      .expert_level = 3
      .short_caption = Output level to send to output stream
    store = *default silence process concise summary logfile verbose testing
      .type = choice
      .expert_level = 3
      .short_caption = Highest output level to generate and store
    write_phil = None
      .type = bool
      .expert_level = 3
      .short_caption = Write phil files
    write_cards = None
      .type = bool
      .expert_level = 3
      .short_caption = Write cards files
    write_files = None
      .type = bool
      .expert_level = 3
      .short_caption = Write miscellaneous files
    write_data = None
      .type = bool
      .expert_level = 3
      .short_caption = Write data files
    write_density = None
      .type = bool
      .expert_level = 3
      .short_caption = Write density files for the solution coordinate files
    write_log = None
      .type = bool
      .expert_level = 3
      .short_caption = Write log files
    write_xml = None
      .type = bool
      .expert_level = 3
      .short_caption = Write xml files
    kill_file = None
      .type = path
      .expert_level = 1
      .short_caption = Kill execution if this file is present
    kill_time = 10
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Kill time in minutes, zero for no/infinite kill time.
      .help = Timeout in minutes for progress loop calculations within selected modes. Default value is suitable for default number of processors. Reduce this if you have more processors, increase if you have fewer. Zero for no termination.
    loggraphs = None
      .type = bool
      .expert_level = 3
      .short_caption = Print Loggraph output to logfile
    database = None
      .type = filesystem(directory=True)
      .optional = True
      .expert_level = 0
      .short_caption = Path to database
  }
''' ,

"graph_report" : '''
  graph_report
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .expert_level = 3
  {
    filestem = "graph_report"
      .type = str
      .expert_level = 3
      .short_caption = Filestem for additional output files reporting on the graph loops in pathways
    best_solution = 1
      .type = int(value_min=1)
      .expert_level = 3
      .short_caption = Selection of best solution from list of solutions
  }
''' ,

"dag" : '''
  dag
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+twin+walk+xref tng:result:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+twin+walk+xref
    .expert_level = 2
  {
    cards
      .expert_level = 4
    {
      subdir = None
        .type = filesystem(directory=True)
        .expert_level = 4
        .short_caption = Directed acyclic graph file subdir
      filename_in_subdir = None
        .type = filesystem(directory=False,ext="dag.cards")
        .expert_level = 4
        .short_caption = Directed acyclic graph file with subdir prefix
    }
    phil
      .expert_level = 4
    {
      filename = None
        .type = filesystem(directory=False,ext="phil.cards")
        .expert_level = 4
        .short_caption = Directed acyclic graph file
    }
    counters = 0 0 0
      .type = ints(size=3,value_min=0)
      .expert_level = 4
      .short_caption = Number of nodes, parents and grandparents
    consecutive = True
      .type = bool
      .expert_level = 2
      .short_caption = Identifier for pathway is consecutive rather than by hash
    skip_if_complete = False
      .type = bool
      .expert_level = 3
      .short_caption = Skip certain modes if constituents in dag are complete
    tncs_present_in_model = None
      .type = bool
      .expert_level = 2
      .short_caption = The tncs duplications are present in all search models
    read_file = False
      .type = bool
      .expert_level = 4
      .short_caption = Read the dag.cards.filename into memory, or keep the dag that is currently in memory
    maximum_stored = None
      .type = int(value_min=1)
      .expert_level = 2
      .short_caption = Truncate the number of nodes on input to a mode to the number given, default is no truncation
  }
''' ,

"notifications" : '''
  notifications
    .style = tng:result:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .expert_level = 4
  {
    hires = None
      .type = float(value_min=0)
      .expert_level = 4
      .short_caption = High resolution limit for data that are available
    advisory = None
      .type = str
      .multiple = True
      .expert_level = 4
      .short_caption = Advisory message
    warning = None
      .type = str
      .multiple = True
      .expert_level = 4
      .short_caption = Warning message
    git_revision = None
      .type = str
      .expert_level = 4
      .short_caption = git revision short hash
    success = True
      .type = bool
      .expert_level = 4
      .short_caption = success (true) or failure (false)
    error
    {
      type = *success parse syntax input fileset fileopen memory killfile killtime fatal result unhandled unknown stdin developer python assert
        .type = choice
        .expert_level = 4
        .short_caption = Error type thrown
      message = None
        .type = str
        .expert_level = 4
        .short_caption = Error message
      code = None
        .type = int
        .expert_level = 4
        .short_caption = Error code returned to operating system
    }
    result
    {
      subdir = None
        .type = filesystem(directory=True)
        .expert_level = 4
        .short_caption = Result cards subdir
      filename_in_subdir = None
        .type = filesystem(directory=False,ext="result.cards")
        .expert_level = 4
        .short_caption = Result cards without subdir prefix
    }
  }
''' ,

"time" : '''
  time
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref tng:result:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .expert_level = 4
  {
    mode
    {
      wall = 0
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Wall seconds spent in mode
      cpu = 0
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Cpu seconds spent in mode
    }
    cumulative
    {
      wall = 0
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Wall seconds spent since phil initialization
      cpu = 0
        .type = float(value_min=0)
        .expert_level = 4
        .short_caption = Cpu seconds spent since phil initialization
    }
  }
''' ,

"title" : '''
  title = None
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref tng:result:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .type = str
    .expert_level = 1
    .short_caption = Title
    .input_size = 140
''' ,

"threads" : '''
  threads
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+twin+walk+xref
    .expert_level = 1
  {
    number = 3
      .type = int(value_min=1)
      .expert_level = 1
      .short_caption = Maximum number of threads
    strictly = False
      .type = bool
      .expert_level = 1
      .short_caption = Must use number of threads, rather than allowing system to decide on number of threads
  }
''' ,

"overwrite" : '''
  overwrite = True
    .style = tng:input:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref tng:result:+aniso+beam+brf+btf+bub+cca+ccs+data+deep+eatm+ellg+emap+emm+esm+etfz+exp1+feff+fetch+find+frf+frfr+ftf+ftfr+fuse+gyre+hyss+imap+imtz+info+ipdb+jog+join+mapz+mcc+move+msm+pak+perm+pool+pose+ptf+put+rbgs+rbm+rbr+rellg+rfac+rmr+sga+sgx+span+spr+srf+srfp+ssa+sscc+ssd+ssm+ssp+ssr+tfz+tncs+tncso+tree+twin+walk+xref
    .type = bool
    .expert_level = 1
    .short_caption = Overwrite the results with new results even if job has been previously run
''' ,

#below are keywords for python modes of phasertng

"refinement" : '''
  refinement
    .style = tng:input:+rfac+xref
    .expert_level = 1
  {
    method = *phenix refmac
      .type = choice
      .expert_level = 1
      .short_caption = Choice of refinement software
    resolution = 2.8
      .type = float(value_min=0)
      .expert_level = 1
      .short_caption = Resolution of data for calculation
    top_files = 1
      .type = int(value_min=0)
      .expert_level = 3
      .help = Maximum number of nodes for calculation. Zero for all.
    ncyc
    {
      refmac = 10
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Number of cycles for refmac5 refinement
      phenix = 5
        .type = int(value_min=1)
        .expert_level = 1
        .short_caption = Number of cycles for phenix.refine refinement
    }
    significant_twin_fraction = 0.4
      .type = float(value_min=0,value_max=0.5)
      .expert_level = 1
      .short_caption = Refine using twinning operators in phenix.refine where twin fraction is over significance value
    treat_as_twinned = None
      .type = bool
      .expert_level = 1
      .short_caption = Refine using twinning operators in phenix.refine where twin fraction is over significance value, regardless of twinning test
    rigid_body = False
      .type = bool
      .expert_level = 1
      .short_caption = Use rigid body refinement in phenix.refine, with rigid bodies being chains
    tncs_correction = False
      .type = bool
      .expert_level = 1
      .short_caption = Use tncs correction in phenix.refine
  }
''',

"isostructure" : '''
  isostructure
    .style = tng:input:+beam tng:result:+beam
    .expert_level = 1
  {
    delta
      .expert_level = 2
    {
      maximum_angstroms = 10
        .type = int(value_min=0)
        .expert_level = 2
        .short_caption = Angstrom delta to the unit cell A,B,C parameters
      maximum_degrees = 5
        .type = int(value_min=0)
        .expert_level = 2
        .short_caption = Degree delta to the unit cell alpha,beta,gamma parameters
      maximum_ncdist = 4.0
        .type = float(value_min=0)
        .expert_level = 2
        .short_caption = Maximum Niggli-cone embedding distance score (NCDIST) to explore
      minimum_correlation_coefficient = 0.40
        .type = float(value_min=0.0, value_max=1.0)
        .expert_level = 2
        .short_caption = Minimum intensities CC ofthe given dataset vs isostructure
      maximum_explored = 3
        .type = int(value_min=0)
        .expert_level = 2
        .short_caption = Number of matches to be explored with the intensity correlation check
      resolution = 3
        .type = float(value_min=1)
        .expert_level = 2
        .short_caption = High resolution for correlation coefficient
      matches_cut_off = 75
        .type = float(value_min=0,value_max=100)
        .expert_level = 2
        .short_caption = Minimum reflection matching percent for correlation coefficient
    }
    tutorial
      .expert_level = 2
    {
      ignore_best = False
        .type = bool
        .expert_level = 2
        .short_caption = Exclude best match from pdb, for testing or tutorials
      ignore_match = None
        .type = strings
        .expert_level = 2
        .short_caption = Ignore match with given pdbids
      force_match = None
        .type = str
        .expert_level = 2
        .short_caption = Force match with given pdbid
    }
    perfect_match_cc = 0.95
      .type = float(value_min=0,value_max=1)
      .expert_level = 2
      .short_caption = Correlation coefficient above which a match is considered perfect and the search terminates
    found
      .expert_level = 4
    {
      match = False
        .type = bool
        .expert_level = 4
        .short_caption = Match found in the pdb
      perfect_match = False
        .type = bool
        .expert_level = 4
        .short_caption = Perfect match found in the pdb, with perfect match CC
      pointgroup = *same expansion reduction
        .type = choice
        .expert_level = 4
        .short_caption = The space group of the match require the data to be expanded or reduced, or neither
      spacegroup = None
        .type = str
        .expert_level = 4
        .short_caption = Space group of the match
      pdbid = None
        .type = str
        .expert_level = 4
        .short_caption = Pdb id of the match
    }
  }
''',

"mirrors" : '''
  mirrors = rcsb pdbe pdbj
    .style = tng:input:+beam+fetch
    .type = strings
    .expert_level = 1
    .short_caption = PDB mirror sites in order of preference
''',

"pdbout" : '''
  pdbout
    .style = tng:input:+beam+esm+fetch+join tng:result:+beam+emm+fetch+join+rbm+rfac+xref
    .expert_level = 4
  {
    filename = None
      .style = file_type:pdb
      .type = path
      .expert_level = 4
      .short_caption = Pdb file out
    pdbid = None
      .type = str
      .expert_level = 4
      .short_caption = Pdbid request
    tag = None
      .type = str
      .expert_level = 4
      .short_caption = Short tag identifying coordinates
    id = None
      .type = uuid(value_min=0)
      .expert_level = 4
      .short_caption = Identifier for coordinates
    deposited_title = None
      .type = str
      .expert_level = 4
      .short_caption = Fetched title
  }
''',

"seqout" : '''
  seqout
    .style = tng:result:+beam
    .expert_level = 4
  {
    filename = None
      .style = file_type:seq
      .type = path
      .expert_level = 4
      .short_caption = Sequence fasta file out
  }
''',

"file_discovery" : '''
  file_discovery
    .style = tng:input:+walk tng:result:+walk
    .expert_level = 0
  {
    directory = None
      .type = path
      .expert_level = 0
      .help = Directory path to discover data, models and sequence files. The pdb models for components of the biological unit may include all biological unit, sub-complexes, components and/or domains. The sequence file should be the sequence of the biological unit. The data should be an mtz file with reflection intensities (or amplitudes).
    filename_in_directory = None
      .type = path
      .expert_level = 0
      .multiple = True
      .short_caption = Individual files can be specified within the directory but all files must be in one directory, to ensure uniqueness of basename
    validated
      .expert_level = 4
    {
      mtz_files = None
        .type = str
        .multiple = True
        .expert_level = 4
        .short_caption = List of discovered mtz files with best reported in hklin
      seq_files = None
        .type = str
        .multiple = True
        .expert_level = 4
        .short_caption = List of discovered mtz files with best reported in biological_unit.sequence
      pdb_files = None
        .type = str
        .multiple = True
        .expert_level = 4
        .short_caption = List of discovered pdb files with best reported in model.filename
    }
  }
''' ,

"biological_unit_builder" : '''
  biological_unit_builder
    .style = tng:input:+bub
    .expert_level = 0
  {
    build_from_sequence = False
      .type = bool
      .expert_level = 0
      .help = Toggle to build biological unit using sequence information. If true, unique search combinations of models that match the biological unit given in the sequence file with the given coverage and sequence identity are determined. If false all models are used as a single entity in the search combination with no further consideration of multiplicity, which is useful in the cases of nucleic acid being present.
    coverage = 85
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum coverage of biological unit by models
    identity = 90
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Minimum sequence identity for sequence match
    overlap = 1
      .type = float(value_min=0,value_max=100)
      .expert_level = 1
      .short_caption = Maximum overlap of sequences
    model = None
      .type = path
      .expert_level = 1
      .multiple = True
      .short_caption = Models from which to build the biological unit, or define as the biological unit
  }
''' ,

"search_combination" : '''
  search_combination = None
    .style = tng:result:+beam+bub
    .multiple = True
    .type = strings
    .expert_level = 4
    .short_caption = Pdb file combination for biological unit
''' ,

} # end of implemented dictionary

import types

def build_phil_str(program_name=None,keyword_list=None):
  keyword_subset = []
  if keyword_list is not None:
    for keyword in keyword_list:
      if keyword not in implemented:
        raise Exception('keyword not recognised:  ' + keyword)
    keyword_subset = keyword_list
# construct list from all implemented
  else:
    for key in implemented:
      keyword_subset.append(key)
  master_phil = phil_scope + " {\n"
  for keyword in keyword_subset:
    master_phil += implemented[keyword] + "\n"
  master_phil += "}\n"
  return master_phil

if __name__ == "__main__":
  print( build_phil_str() )
