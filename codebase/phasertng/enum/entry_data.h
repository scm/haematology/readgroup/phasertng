#ifndef __phasertng_entry_data_enum__
#define __phasertng_entry_data_enum__
#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace entry {

MakeEnum ( data,
           (UNDEFINED)
           (MODELS_PDB)
           (TRACE_PDB)
           (MONOSTRUCTURE_PDB)
           (VARIANCE_MTZ)
           (INTERPOLATION_MTZ)
           (DECOMPOSITION_MTZ)
           (COORDINATES_PDB)
           (DENSITY_MTZ)
           (SUBSTRUCTURE_PDB)
           (GRADIENT_MTZ)
           (FCALCS_MTZ)
           (IMODELS_PDB)
           (IFCALCS_MTZ)
         );

struct EnumHash
{
  template <typename T>
  std::size_t operator()(T t) const
  {
    return static_cast<std::size_t>(t);
  }
};
}}
#endif
