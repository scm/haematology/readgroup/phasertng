//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <autogen/include/Scope.h>
#include <phasertng/main/constants.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/main/jiffy.h>

namespace phasertng {
namespace dag {

  void
  DagDatabase::clear_seeks()
  {
    for (auto& node : NODES)
    {
      node.SEEK.clear();
      node.SEEK_COMPONENTS = 0;
      node.SEEK_RESOLUTION = 0;
      node.SEEK_COMPONENTS_ELLG = 0;
   //   node.ZSCORE = 0;
   //   node.PERCENT = 0;
   //   node.LLG = 0;
    }
  }

  void
  DagDatabase::clear_holds()
  {
    //at the start of refinement where we are only using the FULL
    //free up memory for phenix refine
    for (auto& node : NODES)
    {
      node.HOLD.clear();
     // node.POSE.clear(); //keep the poses for reporting
      node.SEEK.clear();
    }
  }

  void
  DagDatabase::clear_poses_add_seeks(std::vector<Identifier> seeks)
  {
    for (auto& node : NODES)
    {
      node.POSE.clear();
      node.SEEK.clear();
      node.HOLD.clear(); //keep these for next search?
      node.SEEK_COMPONENTS = 0;
      node.SEEK_RESOLUTION = 0;
      node.SEEK_COMPONENTS_ELLG = 0;
      node.SPECIAL_POSITION = '?';
      node.PACKS = pod::pakflag().unset(); // '?'
      node.TNCS_PRESENT_IN_MODEL = '?';
      node.TNCS_MODELLED = false;
      node.ZSCORE = 0;
      node.PERCENT = 0;
      node.LLG = 0;
      for (auto seek : seeks)
      {
        dag::Seek next;
        next.set_missing();
        next.IDENTIFIER = seek;
        node.SEEK.push_back(next);
      }
      if (node.SEEK.size())
        node.SEEK[0].set_search();
    }
    Identifier dud; //default
    clear_frf_entries(dud);
  }

  void
  DagDatabase::clear_seeks_add_seeks(std::vector<Identifier> seeks)
  {
    if (!NODES.size()) return;
    if (!seeks.size()) return;
    dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    for (int i = 0; i < NODES.size(); i++)
      NODES[i].SEEK.clear();
    //PHASER_ASSERT(node.POSE.size()/nmol <= seeks.size());
    dag::Seek seek; //in groups of nmol
    for (int p = 0; p < node.POSE.size(); p+=nmol)
    {
      for (int s = 0; s < seeks.size(); s++)
      {
        if (seeks[s] == node.POSE[p].IDENTIFIER)
        {
          seek.IDENTIFIER = seeks[s];
          seek.set_found();
          node.SEEK.push_back(seek);
          seeks.erase(seeks.begin()+s);
          break;
        }
      }
    }
/* don't set the one to search, just the ones that are found
    if (seeks.size())
    {
      seek.IDENTIFIER = seeks[0];
      seek.set_search();
      node.SEEK.push_back(seek);
      seeks.erase(seeks.begin());
    }
*/
    for (int s = 0; s < seeks.size(); s++)
    {
      seek.IDENTIFIER = seeks[s];
      seek.set_missing();
      node.SEEK.push_back(seek);
    }
    for (int i = 0; i < NODES.size(); i++)
    {
      dag::Node& node = NODES[i];
      node.SEEK = NODES.front().SEEK;
    }
    //PHASER_ASSERT(NODES.front().SEEK[0].is_found());
  }

  af_string
  DagDatabase::seek_info_array(int i) const
  {
    af_string output;
    if (!NODES.size())
       return output;
    const dag::Node* node = &NODES[i];
    int nmol = node->TNCS_MODELLED ? 1 : node->TNCS_ORDER;
    if (!nmol) return output;
    if (nmol > 1)
      output.push_back("Seek with tNCS: " + itos(nmol));
    if (node->SEEK_RESOLUTION > 0)
      output.push_back("Seek Resolution: " + dtos(node->SEEK_RESOLUTION,2));
    else
      output.push_back("Seek Resolution: all");
    //the seeks are already divided by the nmol, to keep the same thing together in groups
    for (int s = 0; s < node->SEEK.size(); s++)
    {
      const dag::Seek& seek = node->SEEK[s];
      std::string tncs_info = (nmol>1) ? ("->" + ntos(s*nmol+nmol,node->SEEK.size()*nmol)) : "";
      std::string component_info = s < node->SEEK_COMPONENTS ? " <ellg": "";
      output.push_back("Seek #" + ntos(s*nmol+1,node->SEEK.size()*nmol) +
                 tncs_info +
                 " [" + seek.IDENTIFIER.string() + "]" +
                 " (" + seek.IDENTIFIER.tag() + ")" +
                  " :" + std::string(1,seek.STAR) +
                 component_info);
    }
    if (!output.size()) output.push_back("Not set");
    return output;
  }

  std::string
  DagDatabase::seek_info_all() const
  { //for python
    auto lines = seek_info_array();
    std::string output("");
    for (const auto& line : lines)
      output += line + "\n";
    return output;
  }

  std::string
  DagDatabase::seek_info_current_number() const
  {
    std::string output("");
    if (!NODES.size()) return output;
    const dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return output;
    int rem = nmol - 1;
    for (int s = 0; s < node.SEEK.size(); s++)
    {
      const dag::Seek& seek = node.SEEK[s];
      if (seek.is_missing() or seek.is_search())
      {
        std::string tncs_info = rem ? ("->" + ntos(s*nmol+1+rem,node.SEEK.size()*nmol)) : "";
        output = "Seek #" + ntos(s*nmol+1,node.SEEK.size()*nmol) +
                 tncs_info;
        return output;
      }
    }
    return output;
  }

  std::string
  DagDatabase::seek_info_current_name() const
  {
    std::string output("");
    if (!NODES.size()) return output;
    const dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return output;
    for (int s = 0; s < node.SEEK.size(); s++)
    {
      const dag::Seek& seek = node.SEEK[s];
      if (seek.is_missing() or seek.is_search())
      {
        //output = seek.IDENTIFIER.tag();
        output = seek.IDENTIFIER.string(); //report all in terms of id
        return output;
      }
    }
    return output;
  }

  std::string
  DagDatabase::permutation_info_all() const
  {
    std::string output;
    if (!NODES.size()) return output;
    const dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return output;
    std::map<Identifier,std::string> unique;
    {{
      pdb_chains_2chainid chains;
      for (const auto& node : NODES)
      {
        for (int s = 0; s < node.SEEK.size(); s++)
        {
          auto id = node.SEEK[s].IDENTIFIER;
          if (!unique.count(id))
            unique[id] = chains.allocate(" A");
        }
      }
      bool blank_first_char(true);
      for (const auto& chain : unique)
        if (chain.second[0] != ' ')
          blank_first_char = false;
      if (blank_first_char)
        for (auto& chain : unique)
          chain.second = std::string(1,chain.second[1]);
    }}
    for (const auto& node : NODES)
    {
      int count(0);
      PHASER_ASSERT(node.SEEK.size());
      auto last_id = node.SEEK[0].IDENTIFIER;
      for (int s = 0; s < node.SEEK.size(); s++)
      {
        if (node.SEEK[s].IDENTIFIER != last_id)
        {
          output += unique[last_id] + "*" + std::to_string(count) + ":";
          count = 0;
        }
        count++;
        last_id = node.SEEK[s].IDENTIFIER;
      }
      output += unique[last_id] + "*" + std::to_string(count) + "\n";
    }
    output.pop_back(); //final return
    return output;
  }

  Identifier
  DagDatabase::seek_next()
  {
    phaser_assert(NODES.size());
    const dag::Node& node = NODES.front();
    Identifier id;
    for (const auto& seek : node.SEEK)
    {
      if (seek.is_missing() or seek.is_search())
      {
        id = seek.IDENTIFIER;
        break;
      }
    }
    for (auto& node : NODES)
    {
      for (auto& seek : node.SEEK)
      {
        if (seek.is_missing() or seek.is_search())
        {
          phaser_assert(id == seek.IDENTIFIER);
          if (seek.is_missing())
            seek.set_search();
          break;
        }
      }
    }
    return id;
  }

  int
  DagDatabase::number_of_seek_loops_first() const
  {
    if (!NODES.size()) return 0;
    const dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return 0;
    int nmissing(0);
    int seek_components = node.SEEK_COMPONENTS/nmol;
    for (int s = 0; s < seek_components; s++)
      if (node.SEEK[s].is_missing())
        nmissing++;
    return nmissing;
  }

  int
  DagDatabase::number_of_seek_loops_final() const
  {
    if (!NODES.size()) return 0;
    const dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return 0;
    int nmissing = node.SEEK.size()*nmol - node.POSE.size();
    //seek is in nmol groups to keep the same models together virtually
    //for (const auto& seek : node.SEEK)
    //  if (seek.is_search() or seek.is_missing())
    //    nmissing++;
    return nmissing;
  }

  bool
  DagDatabase::constituents_complete() const
  {
    return (number_of_seek_loops_final() == 0);
    //if (!NODES.size()) return false;
    //const dag::Node& node = NODES.front();
    //if (!node.SEEK.size()) return false;
    //bool all_found = node.SEEK.back().is_found();
    //for (const auto& node : NODES)
    //  phaser_assert(all_found == node.SEEK.back().is_found());
    //return all_found;
  }

  bool
  DagDatabase::first_constituents_found() const
  {
    if (!NODES.size()) return false;
    const dag::Node& node = NODES.front();
    if (!node.SEEK.size()) return false;
    phaser_assert(node.SEEK_COMPONENTS > 0);
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    PHASER_ASSERT(nmol);
    return node.POSE.size() >= node.SEEK_COMPONENTS;
  }

  void
  DagDatabase::increment_current_seeks_with_identifier(Identifier search)
  {
    //includes the next to be searched for
    //this is based ONLY on the poses that are currently in place
    //hence, it can be run more than once
    //internal to frf and also before frf as soon as the identifier is known
    if (!NODES.size())
       return;
    dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    if (!nmol) return;
    node.set_star_from_pose(); //only found or missing
    for (int s = 0; s < node.SEEK.size(); s++)
    {
      dag::Seek& seek = node.SEEK[s];
      //comma is between + and - in the ascii table
      if (seek.is_missing() and seek.IDENTIFIER == search) seek.STAR = ',';
    }
    auto cmp = []( const dag::Seek &a, const dag::Seek &b )
                 { return int(a.STAR) < int(b.STAR); };
    std::stable_sort(node.SEEK.begin(), node.SEEK.end(),cmp);
    //int ntncs(0);
    //convert back again after the sort
    for (int s = 0; s < node.SEEK.size(); s++)
      if (node.SEEK[s].STAR == ',') node.SEEK[s].set_missing();
    for (int s = 0; s < node.SEEK.size(); s++)
    {
      dag::Seek& seek = node.SEEK[s];
      if (seek.is_missing())
      { //the seeks are set by tncs order groups
        seek.set_search();
      //  ntncs++;
       // if (ntncs == nmol)
        break;
      }
    }
    //copy to all nodes
    for (int i = 0; i < NODES.size(); i++)
    {
      NODES[i].SEEK = node.SEEK;
    }
  }

  void
  DagDatabase::set_join(int seek_components,double cellz,Identifier reflid)
  {
    for (int i = 0; i < NODES.size(); i++)
    {
      NODES[i].SEEK_COMPONENTS = seek_components;
      NODES[i].CELLZ = cellz;
      NODES[i].REFLID = reflid;
    }
  }

  void
  DagDatabase::reset_seek_components_from_pose()
  {
    if (!NODES.size()) return;
    dag::Node& node = NODES.front();
    int nmol = node.TNCS_MODELLED ? 1 : node.TNCS_ORDER;
    PHASER_ASSERT(node.SEEK.size());
    for (int s = 0; s < node.SEEK.size(); s++)
      node.SEEK[s].set_missing();
    for (int s = 0; s < node.POSE.size()/nmol; s++)
    {
      node.SEEK[s].set_found();
      //node.SEEK_COMPONENTS = std::min(int(node.SEEK.size()*nmol+nmol),s+nmol); //up to and including the current search
      node.SEEK_COMPONENTS = std::min(int(node.SEEK.size()*nmol),s+nmol); //up to and including the current search
    }
    for (int i = 0; i < NODES.size(); i++)
    {
      NODES[i].SEEK = node.SEEK;
      NODES[i].SEEK_COMPONENTS = node.SEEK_COMPONENTS;
    }
  }

  std::vector<uuid_t>
  DagDatabase::list_of_current_seeks() const
  {
    //includes the next to be searched for
    std::vector<uuid_t> output;
    if (!NODES.size())
      return output;
    const dag::Node& node = NODES.front();
    bool is_search_set(false);
    for (int s = 0; s < node.SEEK.size(); s++) //literally all, not nmol-jumps
    {
      const dag::Seek& seek = node.SEEK[s];
     //up to and including the current search
      if (seek.is_search()) is_search_set = true;
      if (seek.is_found() or seek.is_search())
        output.push_back(seek.IDENTIFIER.identifier());
      if (seek.is_found() and s == node.SEEK.size()-1) is_search_set = true;
    }
    PHASER_ASSERT(is_search_set); //is_search or all found
    return output;
  }

  std::vector<uuid_t>
  DagDatabase::list_of_all_seeks() const
  {
    std::vector<uuid_t> output;
    if (!NODES.size())
       return output;
    const dag::Node& node = NODES.front();
    for (int s = 0; s < node.SEEK.size(); s++) //literally all, not nmol-jumps
    {
      const dag::Seek& seek = node.SEEK[s];
      output.push_back(seek.IDENTIFIER.identifier());
    }
    return output;
  }

  void
  DagDatabase::append_entry_hold(std::string cards) //for adding previously added entries in memory
  { //mode join
    if (!NODES.size()) return;
    std::istringstream input_stream(cards);
    input_stream.seekg(0);
    if (input_stream)
    {
      compulsoryKeyStr(input_stream,NODE_SCOPE());
      compulsoryKeyStr(input_stream,"entry");
      DogTag dogtag;
      dogtag.initialize(get_dogtag(input_stream));
      auto modlid = dogtag.IDENTIFIER;
      PHASER_ASSERT(modlid.is_set_valid());
      parse_entry_data(input_stream,dogtag);
      skip_line(input_stream); //if there is already an entry, the parse will exit
     // if (!ENTRIES.count(modlid)) //hold needs to be added even if entry is present
      {
        for (auto& node : NODES)
        {
          bool found_hold = false;
          for (auto& hold : node.HOLD)
           if (hold.IDENTIFIER == modlid)
             found_hold = true;
          if (!found_hold)
          {
            dag::Hold hold;
            hold.IDENTIFIER = modlid;
            node.HOLD.push_back(hold);
          }
          if (!node.DRMS_SHIFT.count(modlid)) node.DRMS_SHIFT[modlid] = dag::node_t(modlid,0);
          auto& entry_data = ENTRIES[modlid];
          PHASER_ASSERT(entry_data.VRMS_START.size());
          if (!node.VRMS_VALUE.count(modlid)) node.VRMS_VALUE[modlid] = dag::node_t(modlid,entry_data.VRMS_START[0]);
          if (!node.CELL_SCALE.count(modlid)) node.CELL_SCALE[modlid] = dag::node_t(modlid,1);
        }
      }
    }
    reset_entry_pointers("");
  }

  void
  DagDatabase::set_filesystem(std::string a,std::string b, std::string c) //for adding previously added entries in memory
  { //mode join
    SetFileSystem(a,{b,c});
  }

  std::string
  DagDatabase::entry_unparse_modlid(Identifier modlid)
  {
    const dag::Entry& entry = ENTRIES[modlid];
    std::string cards;
    cards += NODE_SCOPE() +  " entry ";
    auto text = entry_unparse(entry);
    for (auto item : text)
      cards += item.first + item.second;
    cards += "\n";
    return cards;
  }

}} //phasertng
