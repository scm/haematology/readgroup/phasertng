// Tensor class to clone the functionality of TNT::Fortran_Tensor class but
// using a std::vector for the underlying storage and with 0-indexing
// TNT:Fortran_Tensor was row oriented, but this is column oriented - like the
// scitbx versa type.
// MxN matrix
// i.e.
//         j
//       0->N-1
//   0  [     ]
// i |  [     ]
//   v  [     ]
//   M-1[     ]

#ifndef __phasertng_tensor_class__
#define __phasertng_tensor_class__
#include <vector>
#include <cassert>

//#define PHASERTNG_TENSOR_BOUNDS_CHECK

namespace phasertng {
namespace math
{

//D is the dimension of the tensor
//only 3 4 or 5 implemented
template <unsigned int D, typename T>
class Tensor {

private:
  std::vector<int> N;  // dimensions
  std::vector<T> v;       // underlying vector containing the data

public:
  typedef typename std::vector<T>::value_type value_type;
  typedef typename std::vector<T>::const_iterator const_iterator;
  typedef typename std::vector<T>::iterator iterator;
  void push_back(const value_type& x) { v.push_back(x); }
  iterator begin() { return v.begin(); }
  iterator end() { return v.end(); }
  const_iterator begin() const { return v.begin(); }
  const_iterator end() const { return v.end(); }

public:
  // Constructors
  Tensor () : N(0),v(0)  { assert(D==3 or D==4 or D==5); }

  Tensor(std::vector<int> N_, const T& value = T())
  {
    assert(N_.size() == D);
    N = N_;
    int M(1);
    for (auto i : N)
      M = i*M;
    v.resize(M); // std::vector resize method.
    for (int s=0; s<v.size(); s++)
      v[s] = value; // Initialise to value.
  }

  Tensor (std::vector<int> N_, const T* v_)
  // Given a pointer to an array of Ts in row oriented format with the correct stride
  // Absolutely no checks.
  {
    assert(N_.size() == D);
    N = N_;
    int M(1);
    for (auto i : N)
      M = i*M;
    v.resize(M); // std::vector resize method.
    for (int s=0; s<v.size(); s++)
      v[s] = v_[s];
  }

  std::vector<int> num_dims() const { return N; }

  const T& operator[]( int const& i) const
  { return v[i]; }

  const T& operator()( int const& i, int const& j, int const& k) const
  { assert(D == 3); return v[(i * N[1] + j) * N[2] + k]; }

  const T& operator()( int const& i, int const& j, int const& k, int const& l) const
  { assert(D == 4); return v[((i * N[1] + j) * N[2] + k) * N[3] + l]; }

  const T& operator()( int const& i, int const& j, int const& k, int const& l, int const& m) const
  { assert(D == 5); return v[(((i * N[1] + j) * N[2] + k) * N[3] + l) * N[4] + m]; }

  T& operator[]( int const& i)
  { return v[i]; }

  T& operator()( int const& i, int const& j, int const& k)
  { assert(D == 3); return v[(i * N[1] + j) * N[2] + k]; }

  T& operator()( int const& i, int const& j, int const& k, int const& l)
  { assert(D == 4); return v[((i * N[1] + j) * N[2] + k) * N[3] + l]; }

  T& operator()( int const& i, int const& j, int const& k, int const& l, int const& m)
  { assert(D == 5); return v[(((i * N[1] + j) * N[2] + k) * N[3] + l) * N[4] + m]; }

  // Resize operation
  void resize(std::vector<int> N_)
  // Note that the data WILL BE JUNK after the resize
  {
    assert(N_.size() == D);
    N = N_;
    int M(1);
    for (auto i : N)
      M = i*M;
    v.resize(M); // std::vector resize method.
  }

  size_t size() const { return v.size(); }
  size_t size(const int i) const { assert(i < N.size()); return N[i]; }

  void
  fill(const T& value)
  {
    for (int s=0; s<v.size(); s++)
      v[s] = value; // Initialise to value.
  }
};

}} //namespace math
#endif
