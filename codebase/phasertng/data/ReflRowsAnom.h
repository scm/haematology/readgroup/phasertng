//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_ReflRowsAnom_class__
#define __phasertng_ReflRowsAnom_class__
#include <phasertng/data/Reflections.h>
#include <phasertng/data/reflection/ReflRowAnom.h>

namespace phasertng {

class ReflColsMap; //forward declaration

class ReflRowsAnom : public Reflections
{
  private: //getters
    std::vector<reflection::ReflRowAnom> ROW;

  public:
    ReflRowsAnom(int=0); //init fixed labin
    void copy_reflections(const ReflColsMap& refl);

  public: //miscellaneous
    const bool   has_rows() const { return true; }
    void setup_mtzcol(type::mtzcol);
    void setup_miller(int);
    void erase_from_reflection(int);
    void set_base_defaults();

  public: //setters
    void set_present(friedel::type,const bool v,const int r);
    void set_miller(const millnx v,const int r);
    //these concrete member functions from virtual base is very inefficient
    //use specific setters instead
    void set_flt(labin::col col,const double v,const int r);
    void set_int(labin::col col,const int v,const int r);
    void set_bln(labin::col col,const bool v,const int r);

  public: //getters
    const bool   get_present(friedel::type,const int r) const;
    const millnx get_miller(const int r) const;
    const double get_flt(labin::col,const int r) const; //!inefficient!
    const int    get_int(labin::col,const int r) const; //!inefficient!
    const bool   get_bln(labin::col,const int r) const; //!inefficient!

  public: //fast getters
    const reflection::ReflRow* get_row(const int r) const   { return &ROW[r]; }
};

} //phasertng
#endif
