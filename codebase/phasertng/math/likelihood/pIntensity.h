#ifndef __phasertng_math_pIntensity_class__
#define __phasertng_math_pIntensity_class__
#include <ciso646>
#include <limits>
#include <scitbx/constants.h>

namespace phasertng {
namespace math {

class pIntensity
{
  public:
    pIntensity() {}

  private:
    double SQRT2       = std::sqrt(2.);
    double SQRTPI      = std::sqrt(scitbx::constants::pi);
    double SQRT2PI     = std::sqrt(scitbx::constants::two_pi);
    double SQRT2overPI = std::sqrt(2./scitbx::constants::pi);
    double CUBERTEPS = std::pow(std::numeric_limits<double>::min(),1./3.);
    double LOGHALFMAX = std::log(std::numeric_limits<double>::max()/2.);
    double LOGMAX =     std::log(std::numeric_limits<double>::max());

  public:
    double wpcdmhalf_negarg(double) const;
    double wpcd1half_negarg(double) const;
    double wpcdmhalf_posarg(double) const;
    double wpcd1half_posarg(double) const;
    double pEosqAcen(double, double) const;
    double pSmallerEosqAcen(double,double) const;
    double pLargerEosqAcen(double,double) const;
    double pE2Acen_grad_hess_by_dE2(double,double,bool,double&,bool,double&) const;
    double pIobsAcen(double,double,double) const;
    double pIobsAcen_grad_hess_by_dSN(double,double,double,bool,double&,bool,double&) const;
    double pEosqCen(double,double) const;
    double pSmallerEosqCen(double,double) const;
    double pLargerEosqCen(double,double) const;
    double pE2Cen_grad_hess_by_dE2(double,double,bool,double&,bool,double&) const;
    double pIobsCen(double,double,double) const;
    double pIobsCen_grad_hess_by_dSN(double,double,double,bool,double&,bool,double&) const;
    double pEosqTwinAcen(double,double,double) const;

    double probability(bool CENT,double eosqr,double sigesqr)
    {
      double prob(0);
      if (CENT and eosqr < 0.) // Too negative?
        prob = pSmallerEosqCen(eosqr,sigesqr);
      else if (CENT and eosqr >= 0.) // Too large?
        prob = pLargerEosqCen(eosqr,sigesqr);
      else if (!CENT and eosqr < 0.) // Too negative?
        prob = pSmallerEosqAcen(eosqr,sigesqr);
      else if (!CENT and eosqr >= 0.) // Too large?
        prob = pLargerEosqAcen(eosqr,sigesqr);
     return prob;
   }
};

}}
#endif
