//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_cell.h>
#include <scitbx/array_family/tiny_types.h>
#include <algorithm>
#include <iomanip>

namespace phasertng {
namespace type {

  flt_cell::flt_cell(
      const std::string parameter,
      const af::double6 dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  { }

  void flt_cell::set_value(scitbx::af::double6 other)
  {
    user = true;
    uvalue = cctbx::uctbx::unit_cell(other);
  }

  void flt_cell::set_value(const sv_string& vcards,const scitbx::af::double6& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_cell::is_default() const
  { return uvalue.is_similar_to(dephalt,0.001,0.001); }

  void
  flt_cell::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_cell::to_python()
  { return unparse_parameter(false); }

  void
  flt_cell::from_python(std::string str)
  {
    af::double6 tmp;
    sv_string txt = to_words(str);
    for (int i = 0; i < 6; i++)
      tmp[i] = std::stod(txt[i]);
    set_value(tmp);
  }

  cctbx::uctbx::unit_cell
  flt_cell::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  flt_cell::unparse_parameter(bool phil) const
  {
    if (is_default())
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 6; i++)
      ss << value_or_default().parameters()[i] << " " ;
    return ss.str().size() ? ss.str() : None();
  }

  std::string
  flt_cell::show_default(bool phil) const
  {
    if (is_default())
      return None(); //special case! no other type of default cell ever
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    for (int i = 0; i < 6; i++)
      ss << dephalt.parameters()[i] << " ";
    return ss.str();
  }

}}
