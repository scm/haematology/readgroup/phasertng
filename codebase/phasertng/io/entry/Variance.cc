//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Variance.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/constants.h>
#include <phasertng/src/ConsistentDLuz.h>
#include <phasertng/math/vec_ops.h>
#include <phasertng/math/mat_ops.h>
#include <unordered_set>
#include <future>
#include <cstring> //strcpy
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/io/MtzHandler.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/flex_grid.h>
#include <scitbx/array_family/accessors/c_grid.h>
typedef scitbx::af::versa<double, scitbx::af::flex_grid<> > versa_flex_double;
typedef scitbx::af::versa<double, scitbx::af::c_grid<3> >   versa_grid_double;

namespace phasertng {

  Variance::Variance(
      af_string HISTORY_,af::double6 CELL_,af_millnx MILLER_,af_cmplex DATA_,
      af_double SIGMAA_,af_double WTS_) :
    ModelsFcalc(HISTORY_,CELL_,MILLER_,DATA_)
  {
    SIGMAA = SIGMAA_;
    SetWts(WTS_);
  }

  void
  Variance::SetWts(af_double WTS_)
  {
    PHASER_ASSERT(BIN.numbins() == WTS_.size()/NMODELS);
    MODELWT = math::Matrix<double>(BIN.numbins(),NMODELS,WTS_.begin());
  }

  void
  Variance::WriteMtz()
  {
    int nmodels = FCALC.num_rows();
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,0.0,MILLER.size(),get_history());
    for (int m = 0; m < nmodels; m++)
      mtzOut.addSet("Model" + ntos(m+1,nmodels));
    //nref must be set before columns are added as this defines their size
    //set[0] contains the miller indices and the bins
    CMtz::MTZCOL* mtzB  = mtzOut.addCol("BIN",'I');
    CMtz::MTZCOL* mtzS  = mtzOut.addCol("SIGA",'W');
    std::vector<CMtz::MTZCOL*> mtzE(nmodels);
    std::vector<CMtz::MTZCOL*> mtzP(nmodels);
    std::vector<CMtz::MTZCOL*> mtzW(nmodels);
    for (int m = 0; m < nmodels; m++)
    {
      int s = m+1;
      std::string counter = ntos(s,nmodels);
      mtzE[m]  = mtzOut.addCol("EC"+counter,'F',s);
      mtzP[m]  = mtzOut.addCol("PHIC"+counter,'P',s);
      mtzW[m]  = mtzOut.addCol("W"+counter,'W',s);
    }
    //set[1] to set[m+1] contains the reflection information per model
    int mtzr(0);
    phaser_assert(nmodels);
    for (int r = 0; r < MILLER.size(); r++)
    {
      int ibin = BIN.RBIN[r];
      mtzOut.mtzH->ref[mtzr] = MILLER[r][0];
      mtzOut.mtzK->ref[mtzr] = MILLER[r][1];
      mtzOut.mtzL->ref[mtzr] = MILLER[r][2];
      mtzB->ref[mtzr] = ibin;
      mtzS->ref[mtzr] = SIGMAA.size() ? SIGMAA[ibin] : 0;
      for (int m = 0; m < nmodels; m++)
      {
        mtzE[m]->ref[mtzr] = std::abs(FCALC(m,r));
        mtzP[m]->ref[mtzr] = scitbx::rad_as_deg(std::arg(FCALC(m,r)));
        mtzW[m]->ref[mtzr] = MODELWT.size() ? MODELWT(ibin,m) : 0;
      }
      mtzr++;
    }
    create_directories_for_file();
    mtzOut.Put("Phasertng Model");
  }

  af_string
  Variance::get_history()
  {
    af_string histlines = ModelsFcalc::get_history();
    histlines.push_back("REFLID " + REFLID.string() + " " + REFLID.tag());
    return histlines;
  }

  void
  Variance::ReadMtz()
  {
    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    //set[1] to set[m+1] contains the reflection information per model

    int nset = mtzIn.load_dataset("");
    phaser_assert(mtzIn.NREFL > 0);
    int nmodels = nset-1; //not hkl
    phaser_assert(nmodels > 0);

    MILLER.resize(mtzIn.NREFL); //so that setup_data_present_init sets array sizes
    //SPACEGROUP
      SG =  mtzIn.SG;
    { //HISTORY
      //read the history and setup up parsing of cards to see what is there already
      SetHistory(mtzIn.HISTORY);
    } //HISTORY
    { //UNITCELL
      UC =  mtzIn.UC;
    } //UNITCELL
    { //DATA
      int ncols = 5+nmodels*3; //H+K+L+B+V + nmodels*(EC+PHIC+W)
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      lookup[0] = mtzIn.getCol("H");
      lookup[1] = mtzIn.getCol("K");
      lookup[2] = mtzIn.getCol("L");
      lookup[3] = mtzIn.getCol("BIN");
      lookup[4] = mtzIn.getCol("SIGA");
      int i(5);
      for (int m = 0; m < nmodels; m++)
      {
        int s = m+1;
        std::string counter = ntos(s,nmodels);
        lookup[i++] = mtzIn.getCol("EC"+counter);
        lookup[i++] = mtzIn.getCol("PHIC"+counter);
        lookup[i++] = mtzIn.getCol("W"+counter);
      }
      mtzIn.resetRefl();
      MILLER.resize(mtzIn.NREFL);
      FCALC.resize(nmodels,mtzIn.NREFL);
      BIN.RBIN.resize(mtzIn.NREFL);
      MODELWT.resize(BIN.numbins(),nmodels);
      SIGMAA.resize(BIN.numbins());
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H(adata[0]);
        int K(adata[1]);
        int L(adata[2]);
        millnx miller(H,K,L);
        MILLER[mtzr] = miller;
        int ibin = adata[3];
        BIN.RBIN[mtzr] = ibin;
        SIGMAA[ibin] = adata[4]; //keeps overwriting
        int i(5);
        for (int m = 0; m < nmodels; m++,i+=3)
        {
          FCALC(m,mtzr) = std::polar(static_cast<double>(adata[i]),
                                     static_cast<double>(scitbx::deg_as_rad(adata[i+1])));
          MODELWT(ibin,m) = static_cast<double>(adata[i+2]); //keeps overwriting
        }
      }
      BIN.set_numinbin();
    } //DATA
  }

  sv_double
  Variance::modelwt(int ibin)
  {
    sv_double array(NMODELS);
    for (int m = 0; m < NMODELS; m++)
       array[m] = MODELWT(ibin,m);
    return array;
  }

  void
  Variance::SetHistory(af_string histlines)
  {
    ModelsFcalc::SetHistory(histlines);
    for (auto line : histlines)
    {
      sv_string token;
      hoist::algorithm::split(token,line," ");
      if (!line.find("REFLID"))
      {
        phaser_assert(token.size() == 3);
        REFLID.parse_string(token[1],token[2]);
      }
    }
  }

  af_string
  Variance::logCalculationOfStatisticalWeighting()
  {
    af_string output;
    output.push_back("");
    output.push_back("Calculation of Statistical Weighting");
    output.push_back("The Weights are calculated using the relationship:");
    output.push_back("[           ]   [      ] = [       ]");
    output.push_back("[Correlation] * [Weight] = [Luzzati]");
    output.push_back("[  Matrix   ]   [Vector] = [   D   ]");
    output.push_back("[           ]   [      ] = [       ]");
    output.push_back("The calculation is done in resolution bins of data");
    output.push_back("");
    output.push_back("[Correlation] is a matrix of correlations between");
    output.push_back("[  Matrix   ] structure factors of the models in the ensemble");
    output.push_back("");
    output.push_back("[  Luzzati  ] is calculated from a four-parameter Sigma(A) curve");
    output.push_back("[    D      ] using the estimated rmsd between model and target");
    output.push_back("");
    output.push_back("The ensemble of (i) number of structure factors consists of");
    output.push_back("Weighted E  = Sum(i) [ Weight(i)*E(i) ]");
    output.push_back("Variance E  = (1 - dotprod([ Weight ],[ DLuz ]");
    output.push_back("");
    return output;
  }

  af_string
  Variance::logVariancesAndModelWeightsByResolutionBin()
  {
    af_string output;
    output.push_back("");
    output.push_back("Variances and Model Weights by Resolution Bin");
    output.push_back("---------------------------------------------");
    for (int ibin = 0; ibin < BIN.numbins(); ibin++)
    {
      std::string modelwt;
      double Siga = SIGMAA[ibin];
      for (int m = 0; m < MODELWT.num_cols(); m++)
      {
        modelwt += dtos(MODELWT(ibin,m),5,3) + " ";
      }
      modelwt.pop_back();
      output.push_back(snprintftos(
          "Bin #%4i (%5s - %5.2f A) : FourParameterSigmaA=%7.4f V=%7.4f Wt=[%s]",
          ibin+1,
          ibin ? dtos(BIN.LoRes(ibin),5,2).c_str(): "inf",
          BIN.HiRes(ibin),
          Siga,
          1-fn::pow2(Siga),
          modelwt.c_str()));
    }
    return output;
  }

  af_string
  Variance::logVariancesAndModelWeights()
  {
    af_string output;
    output.push_back("");
    output.push_back("Variances and Model Weights");
    output.push_back("---------------------------");
    output.push_back("Four parameter sigmaa curve: half Angstrom resolution shell sampling");
    //just print the ones that straddle resolution shells
    sv_double selres = { 15.0, 10.0, 6.0, 4.2, 3.1, 2.2, 1.5, 1.0 };
    int width(50);
    output.push_back(snprintftos(
          "%6s %-19s %6s  |%-s",
          "Select","Resolution Range","SigmaA","Histogram"));
    for (auto& resolution : selres)
      for (int ibin = 0; ibin < BIN.numbins(); ibin++)
        if (resolution > BIN.HiRes(ibin) and resolution <= BIN.LoRes(ibin))
    {
      double Siga = SIGMAA[ibin];
      output.push_back(snprintftos(
          "%5.1f: (%5s - %5.2f A) : %6.4f  |%-s",
          resolution,
          ibin ? dtos(BIN.LoRes(ibin),5,2).c_str(): "inf",
          BIN.HiRes(ibin),
          Siga,
          std::string(std::ceil(Siga*width),'*').c_str()
          ));
    }
    output.push_back("");
    output.push_back(snprintftos(
          "%6s %-19s %6s  | Weights",
          "Select","Resolution Range","SigmaA"));
    for (auto& resolution : selres)
      for (int ibin = 0; ibin < BIN.numbins(); ibin++)
        if (resolution > BIN.HiRes(ibin) and resolution <= BIN.LoRes(ibin))
    {
      double Siga = SIGMAA[ibin];
      std::string modelwt;
      for (int m = 0; m < MODELWT.num_cols(); m++)
      {
        modelwt += dtos(MODELWT(ibin,m),5,3) + " ";
      }
      modelwt.pop_back();
      output.push_back(snprintftos(
          "%5.1f: (%5s - %5.2f A) : %6.4f  | Wt=[%s]",
          resolution,
          ibin ? dtos(BIN.LoRes(ibin),5,2).c_str(): "inf",
          BIN.HiRes(ibin),
          Siga,
          modelwt.c_str()));
    }
    return output;
  }

  sv_string
  Variance::calculate_partial_modelweights(
      int beg,
      int end,
      const double drms_lower,const sv_double vrms_lower,
      const FourParameterSigmaA solTerm
    )
  {
    sv_string output;
    //TODO DHS 08/05/2020 make models, bin and solterm const to enforce thread safety
    phaser_assert(this->NMODELS);
    int nmodels = this->NMODELS;
    phaser_assert(nmodels == this->FCALC.num_rows());
    for (int ibin = beg; ibin < end; ibin++)
    {
      versa_flex_double CorMat(af::flex_grid<>(nmodels,nmodels));
      for (int i = 0; i < nmodels; i++)
      {
        for (int j = i; j < nmodels; j++)
        {
          // Compute complex correlation from phased E-values. It is assumed that the
          // E-values are normalised so that the mean-square value is 1.
          double CMij(0);
          for (int r = 0; r < this->MILLER.size(); r++)
          {
            if (this->BIN.RBIN[r] == ibin)
            { //FCALCS are now Ecalcs
              CMij += std::real((this->FCALC(i,r))*std::conj(this->FCALC(j,r)));
            }
          }
          PHASER_ASSERT(ibin < this->BIN.NUMINBIN.size());
          PHASER_ASSERT(this->BIN.NUMINBIN[ibin] > 0);
          double Siga = CMij/this->BIN.NUMINBIN[ibin];
          CorMat(i,j) = CorMat(j,i) = Siga;
        }
      }

      ConsistentDLuz consistent_dluz;

      output.push_back("Bin #" + itos(ibin+1) + ": " + "Calculate weights");
      //output->logHessian(out::TESTING,CorMat,sv_string(0));
      sv_double DLuz_sharp,DLuz;
      // Biggest this problems for lowest vrms estimates, so run consistent_dluz with lower bounds
      // instead of starting value.  Correct results using drms_lower, which is difference between
      // starting values and lowest bounds for vrms-squared.
      double midSsqr = 1/fn::pow2(this->BIN.MidRes(ibin));
      double DLuz_factor = std::exp(DEF_TWOPISQ_ON_THREE*midSsqr*drms_lower);
      int whilecount = consistent_dluz.setup(solTerm,vrms_lower,CorMat,midSsqr);
      output.push_back("Number of loops in consistency correction = " + itos(whilecount));
      if (nmodels > 1)
      {
        DLuz_sharp = consistent_dluz.DLuz_out;
        DLuz.resize(nmodels);
        for (int m = 0; m < nmodels; m++)
          DLuz[m] = DLuz_factor*DLuz_sharp[m];
        auto modelwt = math::mat_vec_mult(consistent_dluz.pseudoCorMatInv, DLuz);
        for (int m = 0; m < nmodels; m++)
          this->MODELWT(ibin,m) = modelwt[m];
      }
      else
      {
        DLuz = consistent_dluz.DLuz_out;
        DLuz[0] *= DLuz_factor;
        this->MODELWT(ibin,0) = DLuz[0];
      }
      //else { DLuz = consistent_dluz.DLuz_in; this->MODELWT(ibin,0) = DLuz[0]; }
      //above condition gives results that to not degenerate to the ones for nmodels > 1

      {
        output.push_back("Bin #" + itos(ibin+1) + ": Model Weights and Luzzati D by resolution bin:");
        output.push_back("Bin #" + itos(ibin+1) + ": " + dtos(this->BIN.LoRes(ibin),5,2) + " - " + dtos(this->BIN.HiRes(ibin),5,2) + "A (weighted average " +  dtos(this->BIN.MidRes(ibin),5,2) + "A) #refl = " +itos(this->BIN.NUMINBIN[ibin]));
        for (int i = 0; i < nmodels; i++)
        {
          std::string cormat;
          for (int j = 0; j < nmodels; j++)
            cormat += dtos(CorMat(i,j),5,3) + " ";
          cormat.pop_back();
          output.push_back(snprintftos(
              "[%s]  [% 5.3f]   [%5.3f]",
              cormat.c_str(),
              this->MODELWT(ibin,i),
              DLuz[i]));
        }
        output.push_back("");
      }

      double Covariance = math::dot_prod(this->modelwt(ibin),DLuz); //even if this is negative!
      PHASER_ASSERT(Covariance > 0);
      this->SIGMAA[ibin] = std::sqrt(Covariance);
    }
    return output;
  }

  af_string
  Variance::calculate_modelweights(
      const double drms_lower,const sv_double vrms_lower,
      const FourParameterSigmaA solTerm
    )
  {
    af_string output;
    int nmodels = this->NMODELS;
    phaser_assert(nmodels > 0);
    phaser_assert(this->BIN.numbins() > 0);

    std::vector< std::future< sv_string > > results;

    this->SIGMAA.resize(this->BIN.numbins());
    this->MODELWT.resize(this->BIN.numbins(),nmodels);

    phaser_assert(this->BIN.numbins());
    int nthreads = std::min(NTHREADS,this->BIN.numbins());
    int grain = this->BIN.numbins() / nthreads;
    int nthreads_1 = nthreads -1;
    PHASER_ASSERT(grain > 0);

    for (int t = 0; t < nthreads_1; t++)
    {
      int beg = t*grain;
      int end = (t+1)*grain;
      results.push_back( std::async( std::launch::async, &Variance::calculate_partial_modelweights, this,
            beg,
            end,
            drms_lower,vrms_lower,
            solTerm));
    }

    // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
    sv_string foutput;
    {
    int beg = nthreads_1*grain;
    int end = this->BIN.numbins();
    foutput = calculate_partial_modelweights(
        beg,
        end,
        drms_lower,vrms_lower,
        solTerm);
    }

    // aggregate the results of the other threads
    for (int t = 0; t < nthreads_1; t++)
    {
      sv_string toutput = results[t].get();
      for (auto& item : toutput)
        output.push_back(item);
    }
    for (auto& item : foutput)
      output.push_back(item);
    return output;
  }

  void
  Variance::setup_sigmaa_sqr(af_double ssqr_array)
  {
    int nrefl = ssqr_array.size();
    rSigaSqr.resize(nrefl,0);
    for (int r = 0; r < ssqr_array.size(); r++)
    {
      rSigaSqr[r] = 0;
      double ssqr = ssqr_array[r];
      if (!BIN.ssqr_in_range(ssqr)) continue;
      int ibin = BIN.get_bin_ssqr(ssqr); //expensive
      double Siga = SIGMAA.at(ibin);
      rSigaSqr[r] = fn::pow2(Siga);
    }
  }
}//phasertng
