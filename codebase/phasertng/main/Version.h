#ifndef __phasertng_version_class__
#define __phasertng_version_class__
#include <string>
#include <scitbx/array_family/shared.h>

namespace phasertng {

class Version
{
  public:
  Version() {}
  ~Version() {}
  std::string version_date();
  std::string yyyymmdd();
  std::string program_name();
  std::string version_number();
  std::string full_name();
  std::string change_log();
  std::string git_revision();
  scitbx::af::shared<std::string> git_info();
};

} //phasertng
#endif
