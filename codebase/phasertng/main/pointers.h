//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_data_pointers_class__
#define __phasertng_data_pointers_class__
#include <memory>

namespace phasertng {

class Selected;
typedef Selected* SelectedPtr; //where don't want ownership to change
typedef const Selected* const_SelectedPtr; //where don't want ownership to change

class Reflections;
//whole of phasertng should compile with Reflections pointer type as shared_ptr or unique_ptr
//the logic is that of a unique_ptr, ie. one instantiation in Phasertng base class
//however shared_ptr is required for simple (default) python bindings
//there may be a compilicated way to wrap unique_ptr for boost.python AJM TODO
//typedef std::unique_ptr<Reflections> ReflectionsPtr; //either this
typedef std::shared_ptr<Reflections> ReflectionsPtr;   //or this should be fine, except for python
//Raw pointers are used for passing pointers to functions
//should be const
typedef const Reflections* const_ReflectionsPtr; //where don't want ownership to change

class Epsilon;
typedef Epsilon* EpsilonPtr;
typedef const Epsilon* const_EpsilonPtr;

//typedef std::shared_ptr<Output> OutputPtr;
//Output is a base of Phasertng
//Output* can be passed to all functions called from Phasertng via "this"
//Currently don't know how to pass from base class via shared_ptr
class Output;
typedef Output* OutputPtr;

//typedef in phasertng namespace
namespace dag {
class DagDatabase;
class Node;
}
typedef const dag::DagDatabase* const_DagDatabasePtr; //where don't want ownership to change
typedef dag::DagDatabase* DagDatabasePtr; //want in place modification of dag nodes

class SigmaN;
typedef SigmaN* SigmaNPtr;
typedef const SigmaN* const_SigmaNPtr;

class Scatterers;
typedef Scatterers* ScatterersPtr;
typedef const Scatterers* const_ScatterersPtr;

class Atoms;
typedef Atoms* AtomsPtr;
typedef const Atoms* const_AtomsPtr;

class Coordinates;
typedef Coordinates* CoordinatesPtr;
typedef const Coordinates* const_CoordinatesPtr;

class ModelsFcalc;
typedef ModelsFcalc* ModelsFcalcPtr;
typedef const ModelsFcalc* const_ModelsFcalcPtr;
}
#endif
