//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Information.h>
#include <phasertng/math/likelihood/isad/function.h>
#include <phasertng/data/Selected.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Information_content
  void Phasertng::runINFO()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::TNCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (tncs)");
    if (DAGDB.size() > 1)
      throw Error(err::INPUT,"More than one node in directed acyclic graph");

    bool isad_from_dag = DAGDB.WORK->SADSIGMAA.in_memory();
    if (!isad_from_dag)
      logTab(out::VERBOSE,"Sigmaa from Null hypothesis not in node");
    bool do_sad_information(isad_from_dag and REFLECTIONS->ANOMALOUS and REFLECTIONS->INTENSITIES);
    if (do_sad_information)
    {
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Composition for SAD Information Analysis");
      logTab(where,"Atom type Number");
      for (auto item : DAGDB.WORK->COMPOSITION)
        logTab(where,snprintftos("%9s %-10i",item.first.c_str(),item.second));
      logBlank(where);
    }

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    }}

    //information content need to add extra columns, use flexible data structure to do this
    sv_double epsnSigmaN;
    selected.flag_outliers_and_select(REFLECTIONS.get(),
          {
            rejected::RESO,
            rejected::MISSING,
            rejected::SYSABS,
            rejected::WILSONNAT,
            rejected::NOMEAN,
          },
          epsnSigmaN,
          DAGDB.WORK->SADSIGMAA
      );
    Information info(REFLECTIONS.get(), selected.get_selected_array(), NTHREADS, USE_STRICTLY_NTHREADS);
    dag::Node& node = DAGDB.NODES.front();
    node.INFORMATION_CONTENT = 0;
    node.EXPECTED_INFORMATION_CONTENT = 0;
    node.SAD_INFORMATION_CONTENT = 0;
    node.SAD_EXPECTED_LLG = 0;
    bool do_information(true);
    if (do_information)
    {
      {{
      logTabArray(out::LOGFILE,selected.logSelected(""));
      if (Suite::Extra())
        logTabArray(out::TESTING,selected.logOutliers(""));
      }}

      {{
      out::stream where = out::LOGFILE;
      logUnderLine(out::SUMMARY,"Information Content");
      if (!REFLECTIONS->FRENCH_WILSON) //because SIGI is zero after conversion so total non-starter
      {
        info.calculate_info();
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.info.").value_or_default());
        input.value__mtzcol.at(".phasertng.labin.info.").set_value(input.value__mtzcol.at(".phasertng.labin.info.").name());
        friedel::type f = REFLECTIONS->convert_labin_to_friedel(labin::INFO);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          REFLECTIONS->set_flt(labin::INFO,info.INFO[r],r);
          REFLECTIONS->set_present(f,info.INFO_PRESENT[r],r);
        }
        af_double information = REFLECTIONS->get_data_array(labin::INFO);
       // af_bool present = REFLECTIONS->get_present(labin::INFO);
        for (int r = 0; r < info.INFO.size(); r++)
          if (selected.get_selected(r) and info.INFO_PRESENT[r])
            node.INFORMATION_CONTENT += info.INFO[r];
        node.INFORMATION_CONTENT *= REFLECTIONS->oversampling_correction();
        //std::tie(IonSIGI,present) = REFLECTIONS->getIonSIGI();
        //std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(IonSIGI,present,present,"IonSigI","",true);
        //logTabArray(where,table);
        af_string table; Loggraph loggraph;
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.INFO,info.INFO_PRESENT,selected.get_selected_array(),"Information Content","Info in bits",true);
        logTabArray(out::SUMMARY,table);
        logGraph(where,loggraph);
        logTab(out::SUMMARY,"Information gain: " + dtos(node.INFORMATION_CONTENT,0) + " bits");
      }
      else
      {
        logAdvisory("Information gain not calculated because data given as French-Wilson amplitudes");
        logBlank(where);
      }
      if (!REFLECTIONS->INTENSITIES)
      {
        logWarning("Information gain not reliable because data not given as intensities");
        logBlank(where);
      }
      logBlank(out::SUMMARY);
      }}

      {{ //expected information_content
      out::stream where = out::LOGFILE;
      logUnderLine(out::SUMMARY,"Expected Information Gain");
      if (REFLECTIONS->INTENSITIES)
      {
        info.calculate_einfo();
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.einfo.").value_or_default());
        friedel::type f = REFLECTIONS->convert_labin_to_friedel(labin::EINFO);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          REFLECTIONS->set_flt(labin::EINFO,info.EINFO[r],r);
          REFLECTIONS->set_present(f,info.EINFO_PRESENT[r],r);
        }
        for (int r = 0; r < info.EINFO.size(); r++)
          if (selected.get_selected(r) and info.EINFO_PRESENT[r])
            node.EXPECTED_INFORMATION_CONTENT += info.EINFO[r];
        node.EXPECTED_INFORMATION_CONTENT *= REFLECTIONS->oversampling_correction();
        af_string table; Loggraph loggraph;
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.EINFO,info.EINFO_PRESENT,selected.get_selected_array(),"Expected Information Content","eInfo in bits",true);
        logTabArray(where,table);
        logGraph(where,loggraph);
        logTab(out::SUMMARY,"Expected information gain: " + dtos(node.EXPECTED_INFORMATION_CONTENT,0) + " bits");
      }
      else
      {
        logTab(out::SUMMARY,"Expected information gain can only be estimated for intensity data");
      }
      logBlank(out::SUMMARY);
      }} // expected information_content

      if (REFLECTIONS->MAP)
      {
        out::stream where = out::LOGFILE;
        logUnderLine(out::SUMMARY,"Map Information Gain");
        info.calculate_mapinfo();
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.info.").value_or_default());
        friedel::type f = REFLECTIONS->convert_labin_to_friedel(labin::INFO);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          REFLECTIONS->set_flt(labin::INFO,info.INFO[r],r);
          REFLECTIONS->set_present(f,info.INFO_PRESENT[r],r);
        }
        for (int r = 0; r < info.INFO.size(); r++)
          if (selected.get_selected(r) and info.INFO_PRESENT[r])
            node.INFORMATION_CONTENT += info.INFO[r];
        node.INFORMATION_CONTENT *= REFLECTIONS->oversampling_correction();
        af_string table; Loggraph loggraph;
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.INFO,info.INFO_PRESENT,selected.get_selected_array(),"Map Information Content","Info in bits",true);
        logTabArray(where,table);
        logGraph(where,loggraph);
        logTab(out::SUMMARY,"Map information gain: " + dtos(node.INFORMATION_CONTENT,0) + " bits");
        logBlank(out::SUMMARY);
      } // expected information_content
    }
    if (do_sad_information)
    {
      //change reflection selection
      {{
      sv_double epsnSigmaN;
      selected.flag_outliers_and_select(REFLECTIONS.get(),
          {
            rejected::RESO,
            rejected::MISSING,
            rejected::SYSABS,
            rejected::WILSONPOS,
            rejected::WILSONNEG,
            rejected::NOANOM,
            rejected::BADANOM,
          },
          epsnSigmaN,
          DAGDB.WORK->SADSIGMAA
      );
      logTabArray(out::VERBOSE,selected.logSelected(""));
      if (Suite::Extra())
        logTabArray(out::TESTING,selected.logOutliers(""));
      }}
      //replace with new
      Information info(REFLECTIONS.get(), selected.get_selected_array(), NTHREADS, USE_STRICTLY_NTHREADS);

      {{
      out::stream where = out::LOGFILE;
      logUnderLine(out::SUMMARY,"Anomalous Extra Information");
      if (!REFLECTIONS->ANOMALOUS)
      {
        logTab(out::SUMMARY,"SAD extra information: Data not entered as anomalous");
      }
      else if (REFLECTIONS->INTENSITIES and
               isad_from_dag)
      {
        info.calculate_sinfo(DAGDB.WORK->SADSIGMAA);
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sinfo.").value_or_default());
        friedel::type f = REFLECTIONS->convert_labin_to_friedel(labin::SINFO);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          REFLECTIONS->set_flt(labin::SINFO,info.SINFO[r],r);
          REFLECTIONS->set_present(f,info.SINFO_PRESENT[r],r);
        }
        for (int r = 0; r < info.SINFO.size(); r++)
          if (selected.get_selected(r) and info.SINFO_PRESENT[r])
            node.SAD_INFORMATION_CONTENT += info.SINFO[r];
        //SAD will not have oversampling correction
        af_string table; Loggraph loggraph;
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.SINFO,info.SINFO_PRESENT,selected.get_selected_array(),"SAD Extra Information Content","SAD Info in bits",true);
        logTabArray(where,table);
        logGraph(where,loggraph);
        logTab(out::SUMMARY,"SAD extra information: " + dtos(node.SAD_INFORMATION_CONTENT,0) + " bits");
      }
      else if (!isad_from_dag)
      {
        logTab(out::SUMMARY,"SAD extra information: SAD correlation terms not present in dag");
      }
      else
      {
        logTab(out::SUMMARY, "SAD extra information can only be estimated for intensity data");
      }
      logBlank(out::SUMMARY);
      }}

      {{
      out::stream where = out::LOGFILE;
      logUnderLine(out::SUMMARY,"Anomalous Expected Log-likelihood Gain");
      if (!REFLECTIONS->ANOMALOUS)
      {
        logTab(out::SUMMARY,"SAD expected llg : Data not entered as anomalous");
      }
      else if (isad_from_dag)
      {
        // Setup to compute SigmaH and sigmaHH for substructure model (perfect if HRMS=0 and fracH=1)
        double rmsfactor = input.value__flt_number.at(".phasertng.information.ellg_rms_factor.").value_or_default();
        double fracH = input.value__flt_number.at(".phasertng.information.ellg_fraction_anomalous.").value_or_default();
        double hrms = rmsfactor*REFLECTIONS->data_hires();
        info.calculate_sellg(DAGDB.WORK->SADSIGMAA, fracH, hrms);
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sellg.").value_or_default());
        REFLECTIONS->setup_mtzcol(input.value__mtzcol.at(".phasertng.labin.sefom.").value_or_default());
        friedel::type f = REFLECTIONS->convert_labin_to_friedel(labin::SELLG);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          REFLECTIONS->set_flt(labin::SELLG,info.SELLG[r],r);
          REFLECTIONS->set_present(f,info.SELLG_PRESENT[r],r);
          REFLECTIONS->set_flt(labin::SEFOM,info.SEFOM[r],r);
          REFLECTIONS->set_present(f,info.SEFOM_PRESENT[r],r);
        }
        node.SAD_EXPECTED_LLG = 0;
        for (int r = 0; r < info.SELLG.size(); r++)
          if (selected.get_selected(r) and info.SELLG_PRESENT[r])
            node.SAD_EXPECTED_LLG += info.SELLG[r];
        //SAD will not have oversampling correction
        af_string table; Loggraph loggraph;
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.SELLG,info.SELLG_PRESENT,selected.get_selected_array(),"SAD Expected Log-Likelihood Gain","SAD eLLG",false);
        logTabArray(where,table);
        logGraph(where,loggraph);
        logBlank(where);
        logTab(where,"SAD expected llg: " + dtos(node.SAD_EXPECTED_LLG,0));

        logUnderLine(where,"Anomalous Expected Figure Of Merit");
        std::tie(table,loggraph) = REFLECTIONS->logSignalMeasure(
          info.SEFOM,info.SEFOM_PRESENT,selected.get_selected_array(),"SAD Expected Figure-of-merit","SAD eFOM",false,false);
        logTabArray(where,table);
        logGraph(where,loggraph);

        // Use SEFOM data to compute SAD expected mapCC in bins and overall
        // Can't use logSignalMeasure but maybe refactoring would simplify things?

        int NUMBINS = REFLECTIONS->numbins();
        sv_double f2_bin(NUMBINS, 0.),m2f2_bin(NUMBINS, 0.);
        sv_int n_bin(NUMBINS, 0);
        double f2_tot(0.),m2f2_tot(0);
        int n_tot(0);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          if (selected.get_selected(r))
          {
            const int s = REFLECTIONS->get_int(labin::BIN,r);
            n_tot++;
            n_bin[s]++;
            const double SEFOMsqr = fn::pow2(info.SEFOM[r]);
            const double Feffsqr = fn::pow2(REFLECTIONS->get_flt(labin::FEFFNAT,r)); //AJM changed -  This was get_F
            f2_bin[s] += Feffsqr;
            f2_tot    += Feffsqr;
            m2f2_bin[s] += Feffsqr*SEFOMsqr;
            m2f2_tot    += Feffsqr*SEFOMsqr;
          }
        }
        std::string description = "Anomalous Expected Map Correlation";
        logUnderLine(out::SUMMARY, description);

        if (n_tot)
        {
          logBlank(out::SUMMARY);
          logTab(out::SUMMARY, "Overall expected mapCC: " + dtos(std::sqrt(m2f2_tot/f2_tot),2));
          logBlank(out::SUMMARY);
        }
        Loggraph loggraphcc;
        loggraphcc.title = description;
        loggraphcc.scatter = false;
        loggraphcc.graph.resize(1);
        loggraphcc.data_labels = "1/d^2 Shell Cumulative Cumulative_total";
        sv_double hires_bin(NUMBINS, std::numeric_limits<double>::max());
        sv_double lores_bin(NUMBINS, 0);
        double cumulative_f2(0),cumulative_m2f2(0);
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          const int s = REFLECTIONS->get_int(labin::BIN,r);
          const double ssqr = REFLECTIONS->get_flt(labin::SSQR,r);
          double reso = 1.0 / std::sqrt(ssqr);
          if (reso < hires_bin[s])
            hires_bin[s] = reso;
          if (reso > lores_bin[s])
            lores_bin[s] = reso;
        }
        for (unsigned s = 0; s < NUMBINS; s++)
        {
          if (n_bin[s])
          {
            double loS = 1 / lores_bin[s]; //from Bin calculation, so that it is the same
            double hiS = 1 / hires_bin[s];
            double midres = 1 / (std::sqrt((loS * loS + hiS * hiS) / 2));
            cumulative_m2f2 += m2f2_bin[s];
            cumulative_f2   += f2_bin[s];
            loggraphcc.data_text += dtos(1.0 / fn::pow2(midres), 5, 4) + " " +
                                  dtos(std::sqrt(m2f2_bin[s]/f2_bin[s])) + " " +
                                  dtos(std::sqrt(cumulative_m2f2/cumulative_f2)) + " " +
                                  dtos(std::sqrt(cumulative_m2f2/f2_tot)) + "\n";
          }
        }
        loggraphcc.graph.resize(1);
        loggraphcc.graph[0] = " Expected mapCC vs resolution:AUTO:1,2,3,4";
        logGraph(where, loggraphcc);
      }
      else if (!isad_from_dag)
      {
        logTab(out::SUMMARY,"SAD expected LLG and FOM: SAD correlation terms not present in dag");
      }
      else
      {
        logTab(out::SUMMARY,"SAD expected LLG and FOM: Data not entered as intensities");
      }
      logBlank(out::SUMMARY);
      }}
    }

    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    if (selected.nsel() == 0)
      throw Error(err::FATAL,"No reflections selected");
    }}

    input.value__flt_number.at(".phasertng.information.information_content.").set_value(node.INFORMATION_CONTENT);
    input.value__flt_number.at(".phasertng.information.expected_information_content.").set_value(node.EXPECTED_INFORMATION_CONTENT);
    input.value__flt_number.at(".phasertng.information.sad_information_content.").set_value(node.SAD_INFORMATION_CONTENT);
    input.value__flt_number.at(".phasertng.information.sad_expected_llg.").set_value(node.SAD_EXPECTED_LLG);

    DAGDB.shift_tracker(hashing(),input.running_mode);
    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::INFO);
    }}

    if (Suite::Extra())
      logTabArray(out::TESTING,REFLECTIONS->logMtzDump("Final Data"));

    //the only control here is the presence of the mtz file as non default, not controlled by Suite::Write
    if (!input.value__string.at(".phasertng.mtzout.").is_default())
    {
      out::stream where = out::LOGFILE;
      logTab(out::LOGFILE,"mtz file with information selection is requested");
      REFLCOLSMAP.store_new_and_changed_data(REFLECTIONS.get());
      REFLCOLSMAP.SetFileSystem(input.value__string.at(".phasertng.mtzout.").value_or_default());
      logFileWritten(out::LOGFILE,true,"Reflections with information content",REFLCOLSMAP);
      REFLCOLSMAP.VanillaWriteMtz(selected.get_selected_shared(),"valid-sigi"); //flag can be anything, only one selection
    }
    else logTab(out::LOGFILE,"mtz file with information selection is NOT requested");

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
