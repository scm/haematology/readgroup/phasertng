#cannot call this file pyvis.py because it conflicts with the "import pyvis"!!
from io import open #this covers the python 2 and python 3 encoding

import sys, os, os.path
#because it is not using libtbx.phasertng
#and because can't put __init__.py in top directory (breaks python path for phasertng.foobar )
#have to hack python path to look in the master_phil directory
#sys.path.append(os.path.join(os.path.dirname(__file__),"..","phasertng",'phil'))
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.phil.master_node_file import *
import networkx as nx
import argparse,shutil


#these are internal parameters not interesting for the user
class runTREE:

  def node_colour(self,uid,rfactor): #or srf since this is special too
      if uid.startswith('root'):
        return 'green'
      elif uid.startswith('find'):
        return 'green'
      elif uid.startswith('join'):
        return 'gold'
      elif uid.startswith('put'):
        return 'grey'
      elif uid.startswith('fuse'):
        return 'darkkhaki'
      elif uid in ['rfac','xref']:
        blue_red = True
        cyan_magenta = False
        if blue_red:
          col = '#0000ff' #blue  rfactor <= 24
          if rfactor is not None and float(rfactor) > 0:
            if float(rfactor) > 25 : col = '#2b00d5'
            if float(rfactor) > 28 : col = '#5500aa'
            if float(rfactor) > 33 : col = '#800080'
            if float(rfactor) > 38 : col = '#aa0055'
            if float(rfactor) > 42 : col = '#d5002a'
            if float(rfactor) > 50 : col = '#ff0000' #red
            return col
        elif cyan_magenta:
          col = '#00ffff' #cyan  rfactor < 24
          if rfactor is not None and float(rfactor) > 0:
            if float(rfactor) > 25 : col = '#2bd5ff'
            if float(rfactor) > 28 : col = '#55aaff'
            if float(rfactor) > 33 : col = '#8080ff'
            if float(rfactor) > 38 : col = '#aa55ff'
            if float(rfactor) > 42 : col = '#d52aff'
            if float(rfactor) > 50 : col = '#ff00ff' #magenta
            return col
        else:
          return 'skyblue'
      return 'orange'

  def node_shape(self,uid): #or srf since this is special too
      if uid.startswith('find'):
        return 'square'
      elif uid.startswith('join'):
      # return 'triangleDown'
        return 'triangle'
      elif uid.startswith('fuse'):
        return 'square'
      return 'dot'

  def node_size(self,uid,zscore): #or srf since this is special too
      basesize = 20
      if uid.startswith('find'):
        return basesize #regardless of zscore
      elif uid.startswith('join'):
        return basesize #regardless of zscore
    # elif uid.startswith('fuse'):
    #   return basesize #regardless of zscore
      if zscore > 1:
        basesize = basesize*zscore #min(11,zscore)
      return basesize

  def pathway_to_data(self,lines,nlines):
      uid = dict()
      pid = dict()
      tid = dict()
  #<!--- pathway ultimate  identifier tag
  #<!--- pathway penultimate  identifier tag
  #<!--- pathlog ultimate  identifier tag
  #<!--- pathlog penultimate  identifier tag
  #<!--- tracker ultimate  zscore rfactor
      assert(len(lines) == nlines)
      word0 = lines[0].split()
      uidtag =  word0[2]
      uid = { 'identifier' : word0[1], 'tag': word0[2] }
      word1 = lines[1].split()
      if str(word1[1]) == '---':
        pid = { 'identifier' : 0, 'tag': 'root' }
      else:
        pid = { 'identifier' : word1[1], 'tag': word1[2] }
      word4 = lines[4].split()
      tid = { 'identifier' : word4[1], 'tag':'nodes' }
      zscore = float(word4[2])
      rfactor = float(word4[3])
      uid['size'] = self.node_size(uidtag,zscore)
      uid['color'] = self.node_colour(uidtag,rfactor)
      uid['shape'] = self.node_shape(uidtag)
      return uid,pid,tid

  def pyvis_display_method(self,G):
      method = 'basic'
      if method == 'basic':
    #sortMethod hubsize or directed
    # "nodes": { "borderWidth" : 3 , "font": { "size": 31 }, "color": { "border" : "#FFA500" }},
          physics = False
          G.set_options('''var options = {
      "configure":{"enabled":false,"filter": ["physics"]},
      "nodes": { "font": { "size": 31 } },
      "edges": { "arrows": { "to": { "enabled": true } }, "color": { "inherit": true }, "smooth": false },
      "layout": { "hierarchical": { "blockShifting" : true, "edgeMinimization" : true, "enabled": true, "levelSeparation": 115, "nodeSpacing": 120, "sortMethod": "hubsize" , "shakeTowards" : "roots", "parentCentralization" : true} },
      "physics": { "enabled": false, "hierarchicalRepulsion": { "centralGravity": 0, "springLength": 55, "springConstant": 0.125, "avoidOverlap" : 1 }, "minVelocity": 0.75, "solver": "hierarchicalRepulsion", "timestep": 1 }
       }''')
      elif method == 'hr':
          physics = True
          G.set_options('''var options = {
      "configure":{"enabled":true,"filter": ["physics"]},
      "nodes": { "font": { "size": 31 } },
      "edges": { "arrows": { "to": { "enabled": true } }, "color": { "inherit": true }, "smooth": false },
      "layout": { "hierarchical": { "enabled": true, "levelSeparation": 115, "nodeSpacing": 120, "sortMethod": "directed" , "shakeTowards" : "roots"} },
      "physics": { "enabled": true, "hierarchicalRepulsion": { "centralGravity": 0, "springLength": 55, "springConstant": 0.125 }, "minVelocity": 0.75, "solver": "hierarchicalRepulsion", "timestep": 1 }
     }''')
      elif method == 'bh':
          physics = True
          basetng.logTab(enum_out_stream.testing,"barnesHut")
          G.set_options('''var options = {
      "configure":{"enabled":true,"filter": ["nodes","physics"]},
      "nodes": { "font": { "size": 31 } },
      "edges": { "arrows": { "to": { "enabled": true } }, "color": { "inherit": true }, "smooth": false },
      "physics": { "enabled": true, "barnesHut": { "damping" : 0, "centralGravity" : 0, "avoidOverlap": 1 } }
     }''')
      return G,physics

  def rfactor_from_walk(self,walk_dir):
 #{{{
      rfacdict = {}
      hasrfactors=False
      tmpfile = str(enum_other_data.solutions_log).replace("_",".")
      for tag in ['rfac','xref']:
        rfacdict[tag] = []
        for (dirpath, dirnames, filewalk) in os.walk(walk_dir):
          #sort in order of time of creation
          dirnames.sort(key=lambda x: os.path.getmtime(os.path.join(dirpath,x)))
          for name in filewalk:
              file_path = os.path.join(dirpath, name)
              if (file_path.endswith(tmpfile)):
                with open(file_path, 'r',encoding="utf8", errors='ignore') as f:
                      try:
                        if tag in file_path:
                          hasrfactors=True
                          f_content = f.readlines()
                          rfacdict[tag].extend(f_content)
                      except Exception:
                        continue
      def get_rfac_column(line):
        columns = line.split()
        col1 = 7
        col2 = 4
        try:
          reso = float(round(100*float(columns[col1]))/100)
          rfac = float(columns[col2])
          #sort on resolution first, highest best, then rfactor, in cases where riker has been run to polish solutions
          return (reso,rfac)
        except Exception as e:
      #   for i,col in enumerate(columns): print(i,col)
          raise ValueError(enum_err_code.developer,"wrong column parsed in rfactor list for sorting")
        return 100.
      if len(rfacdict['xref']) > 0:
        return sorted(rfacdict['xref'], key=get_rfac_column),hasrfactors
      return sorted(rfacdict['rfac'], key=get_rfac_column),hasrfactors
 #}}}

  def pathlog_to_data(self,lines,nlines):
  #<!--- pathway ultimate  identifier tag
  #<!--- pathway penultimate  identifier tag
  #<!--- pathlog ultimate  identifier tag         ie line 2
  #<!--- pathlog penultimate  identifier tag      ie line 3
  #<!--- tracker nultimate  zscore rfactor
      assert(len(lines) == nlines)
      uword = lines[2].split()
      uid = Identifier( int(uword[1]), str(uword[2]))
      pword = lines[3].split()
      if str(pword[1]) == '---':
        pid = Identifier()
      else:
        pid = Identifier( int(pword[1]), str(pword[2]))
      nword = lines[5].split()
      nodes = []
      for n in range(1,len(nword)-1):
        nodes.append(Identifier(int(nword[n]), str("tracker")))
      return uid,pid,nodes

  def node_to_pathway_graph(self,walk_dir,bestsol,basetng):
    columns = bestsol.split()
    col1 = 20
    col2 = 21
    modlid = None
    if col2 < len(columns):
          msd = columns[col1].split("-",1)
          mid = columns[col2].split("-",1)
          try:
            modlid = Identifier(int(mid[0]),str(mid[1]))
            subdir = Identifier(int(msd[0]),str(msd[1]))
            basetng.logTab(enum_out_stream.testing,'Best ' + subdir.str() + " " + modlid.str())
          except Exception as e:
           # for i,col in enumerate(columns): print(i,col)
            raise ValueError(enum_err_code.developer,"wrong column parsed in rfactor list for identifiers")
    if modlid is None:
      return None,None,None,None
    try:
   #  master_phil = parse(build_node_str(),converter_registry=converter_registry)
      PTG = nx.Graph()
      PTG.clear()
      #this is the path decorated with the uids
      tags = dict()
      bestfiles = []
      target = None
      testinglog = []
      daghtml = str(enum_other_data.dag_html).replace("_",".")
      endsdag = str(enum_other_data.dag_cards).replace("_",".")
      targetdir = None
      targetfiles = []
      if not FileSystem().flat_filesystem():
         class BreakOut(Exception): pass
         try:
           for (dirpath, dirnames, filewalk) in os.walk(walk_dir):
              relative_dir = os.path.relpath(dirpath, walk_dir)
              if subdir.str() in relative_dir:
                for name in filewalk:
                  #pick out the target from all the files in the directory
                  if str(modlid.str()) in name:
                    basetng.logTab(enum_out_stream.testing,"found file ="+ name)
                    targetdir = relative_dir
                 #  targetfiles = filewalk
                    node_number = subdir.identifier()
                    raise BreakOut()
         except BreakOut:
           #select the files from filewalk that are not a different modlid
           for name in filewalk:
             if name.startswith(subdir.str()):
               targetfiles.append(name)
             elif name.count(modlid.str()) == 1:
               targetfiles.append(name)
      else:
            targetdir = ""
            for (dirpath, dirnames, filewalk) in os.walk(walk_dir):
              #have to file subdir from startswith
              for name in filewalk:
                if name.startswith(subdir.str()):
                  #select the files from filewalk that are not a different modlid
                  if name.count(subdir.str()) == 2: #logfile, cardfile etc not a modlid
                    targetfiles.append(name)
                  elif name.count(modlid.str()) == 1:
                    targetfiles.append(name)
                  else:  #file is a different modlid file
                    pass
                  if str(modlid.str()) in name: #will repeatedly set because we have to find targetfiles anyway
                    basetng.logTab(enum_out_stream.testing,"found file ="+ name)
                    node_number = subdir.identifier()
      if (node_number is None or node_number == 0):
        return None,None,None,None # no result
      target = int(node_number)
      basetng.logTab(enum_out_stream.testing,"target ="+ str(target))
      for name in sorted(targetfiles): #so that output is always in the same order, whatever that is, can't sort FileSystem
        file_path  = FileSystem(walk_dir,targetdir,name)
        if name.endswith(endsdag):
            bestfiles.append(file_path)
        if str(modlid.str()) in name:
            bestfiles.append(file_path)
        if (name.endswith(daghtml)):
          basetng.logTab(enum_out_stream.testing,"try daghtml = " + file_path.fstr())
          with open(file_path.fstr(), 'r',encoding="utf8", errors='ignore') as f:
              nlines = 6 # read first 5 lines only
              try:
                f_content = [next(f) for x in range(nlines)]
              except Exception as e:
                basetng.logTab(enum_out_stream.testing,"exception "  + str(file_path.fstr()) )
                continue
              uid,pid,nodes = self.pathlog_to_data(f_content,nlines)
            # basetng.logTab(enum_out_stream.testing,"identifiers uid="+ uid.str() + " pid=" + pid.str())
              testinglog.append("identifiers uid="+ uid.str() + " pid=" + pid.str())
              PTG.add_edge(uid.identifier(),pid.identifier())
            #this attaches the data files to the nodes that have output the data
              if ("rfac" in file_path.fstr() or "xref" in file_path.fstr()):
                basetng.logTab(enum_out_stream.testing,"daghtml = " + file_path.fstr())
                for node in nodes:
                  basetng.logTab(enum_out_stream.testing,"identifiers node="+ str(uid.identifier()) + " data=" + str(node.identifier()))
                  PTG.add_edge(node.identifier(),uid.identifier())
              tags[uid.identifier()] = uid.tag()
              tags[pid.identifier()] = pid.tag()
      testinglog = sorted(testinglog)
      basetng.logTab(enum_out_stream.testing,"sorted:")
      for item in testinglog:
        basetng.logTab(enum_out_stream.testing,item)
    except Exception as e:
      message = "" if str(e) is None else str(e)
      print(e)
      return None,None,None,None
    return PTG,tags,bestfiles,target

  def inner_voyager_logfile(self,
         walk_dir,
         bestsol,
         basetng,
         root_number=None,
         ):
    filenames = []
    path = None
    try:
      beststem="best." + basetng.input.get_as_str(".phasertng.graph_report.best_solution.") + "."
      PTG,tags,bestfiles,target = self.node_to_pathway_graph(walk_dir=walk_dir,bestsol=bestsol,basetng=basetng)
      if PTG is None:
        basetng.logTab(enum_out_stream.verbose,"Graph size=0")
        return None
      if root_number is None:
        source = int(Identifier().identifier()) #default
      else:
        source = int(root_number)
      basetng.logTab(enum_out_stream.testing,"Source ="+ str(source))
      #PTG is an NX graph
      basetng.logTab(enum_out_stream.verbose,"Graph size="+ str(len(PTG.nodes)))
      if source in PTG and target in PTG:
        banner = Phasertng("VOYAGER").Banner()
        banner = "\n".join(str(e) for e in banner)
        footer = Phasertng("VOYAGER").Footer()
        footer = "\n".join(str(e) for e in footer)
        logfile = {}
        #cannot use dictionary because boosted enums are not iterable
        lookup = [ [enum_out_stream.concise, enum_other_data.concise_log ],
                   [enum_out_stream.summary, enum_other_data.summary_log ],
                   [enum_out_stream.logfile, enum_other_data.logfile_log ],
                   [enum_out_stream.verbose, enum_other_data.verbose_log ]
                 ]
        for item in lookup:
          k,v = item
          logfile[k] = ""
        try:
          path = nx.shortest_path(PTG, source=source, target=target)
          default_id = Identifier()
          for item in lookup: #write logfile_log
         #{
            f_content = ""
            for ident in path:
           #{
              if ident not in tags:
                continue # is a node
              rawIdentifier = Identifier( int(ident),str(tags[ident]))
              if rawIdentifier is None or rawIdentifier.stem() == default_id.stem():
                continue
              level,v = item
              file_path = os.path.join(walk_dir,rawIdentifier.database_subdir(),rawIdentifier.other_data_stem_ext(v))
              basetng.logTab(enum_out_stream.testing,"Opening logfile " + file_path)
              if (os.path.exists(file_path)):
                with open(file_path, 'r', encoding="utf-8") as f:
                  fread = f.read()
                  fread = fread.replace(banner,"").replace(footer,"")
                  f_content = f_content + fread
              else:
                basetng.logTab(enum_out_stream.testing,"No path to "+file_path)
              if rawIdentifier.identifier() == target: #end of the road!
                continue
           #}
            if len(f_content):
              f_content = banner + "\n" + f_content + "\n" + footer
              Target = Identifier()
              Target.initialize_from_ullint(target) # -> ####-pathway.logfile.log #tags[target])
              Target.initialize_node_tag()
              filename = FileSystem(basetng.DataBase(),"",beststem+ Target.other_data_stem_ext(v))
              with open(filename.fstr(), 'w') as f:
                f.write(f_content)
            # with open(filename, 'w', encoding="utf-8") as f:
            #   f.write(f_content.decode("utf-8"))
              filenames.append(filename)
         #}
        except Exception as e:
          message = "" if str(e) is None else str(e)
          basetng.logAdvisory(message)
         #raise ValueError(enum_err_code.developer,message)
      elif source == int(Identifier().identifier()):
        pass #basetng.logAdvisory("Source not set")
      elif target is None:
        basetng.logAdvisory("Target not set")
      elif source not in PTG:
        basetng.logAdvisory("Source not in graph")
      elif target not in PTG:
        basetng.logAdvisory("Target not in graph")
    except Exception as e:
      message = "" if str(e) is None else str(e)
      raise ValueError(enum_err_code.developer,message)
      #recreate the filename for the pdb file output from this node
    wf = basetng.WriteFiles()
    if (wf):
      basetng.logChevron(enum_out_stream.logfile,"Copying best solution files")
      endspdb = str(enum_entry_data.coordinates_pdb).replace("_",".")
      endsmtz = str(enum_other_data.refine_mtz).replace("_",".")
      endsdag = str(enum_other_data.dag_cards).replace("_",".")
      for best in bestfiles:
        #order will be from the sort in the construction
        basetng.logTab(enum_out_stream.logfile,"Best "+best.qfstrq())
        if best.fstr().endswith(endspdb):
          dst = FileSystem(basetng.DataBase(),"",beststem + endspdb)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Best",dst)
          shutil.copyfile(best.fstr(), dst.fstr())
        if best.fstr().endswith(endsmtz):
          dst = FileSystem(basetng.DataBase(),"",beststem + endsmtz)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Best",dst)
          shutil.copyfile(best.fstr(), dst.fstr())
        if best.fstr().endswith(endsdag):
          dst = FileSystem(basetng.DataBase(),"",beststem + endsdag)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Best",dst)
          shutil.copyfile(best.fstr(), dst.fstr())
      if len(bestfiles) == 0:
        basetng.logTab(enum_out_stream.logfile,"No files")
    if (path is not None):
      basetng.logChevron(enum_out_stream.logfile,"Shortest path through node graph to best solution")
      basetng.logTab(enum_out_stream.logfile,"Path:"+str(path))
      basetng.logTab(enum_out_stream.logfile,"Generating best solution pathway files")
      for item in filenames:
        basetng.logFileWritten(enum_out_stream.logfile,wf,"Best",item)
      if len(filenames) == 0:
        basetng.logTab(enum_out_stream.logfile,"No files")

  def inner_voyager_pyvis(self,
         walk_dir,
         filestem,
         io_output,
         io_number,
         io_database,
         io_labeltag,
         basetng,
         ):
  #{{{
    pyvis_import = False
    try:
      from pyvis.network import Network
      pyvis_import = True
    except Exception as e:
      basetng.logAdvisory("pyvis not installed: use python -m pip install pyvis")
    if (pyvis_import):
      DAGDB = basetng.DAGDATABASE
      DAGDB.shift_tracker(str(basetng.hashing()+basetng.input.generic_string),"tree")
      G = Network('700px', '700px')
      G,physics = self.pyvis_display_method(G)
      nodejoin = None
      nodesens = []
      maxuid = 0
      nodesize = dict()
      noderfactor = dict()
      count_dirs = 0
      for (dirpath, dirnames, filewalk) in os.walk(walk_dir):
        count_dirs = count_dirs+1
      w = max(3,len(str(count_dirs))) #rfr-202 # max probably not necessary becuse 003 becomes 3 anyway
      indexuid = dict()
      indexuid[0] = { 'size' : self.node_size('root',0), 'color':self.node_colour('root',0), 'shape': self.node_shape('root') }
      for (dirpath, dirnames, filewalk) in os.walk(walk_dir):
        dirnames.sort(key=lambda x: os.path.getmtime(os.path.join(dirpath,x)))
      # dirnames.sort() # consecutive numbering
        for name in filewalk:
          file_path = os.path.join(dirpath, name)
          daghtml = str(enum_other_data.dag_html).replace("_",".")
          if (False):
            basetng.logTab(enum_out_stream.testing,"dag.html " + daghtml)
          if (file_path.endswith(daghtml)):
            basetng.logTab(enum_out_stream.testing,'Reading File: ' + os.path.abspath(file_path))
            with open(file_path, 'r',encoding="utf8", errors='ignore') as f:
                  nlines = 5 # read first 5 lines only
                  try:
                    f_content = [next(f) for x in range(nlines)]
                  except Exception:
                    continue
                  uid,pid,tid = self.pathway_to_data(f_content,nlines)
                  indexuid[uid['identifier']] = uid #with other stuff
                  maxuid = max(int(maxuid),int(uid['identifier']))
                  if io_number is not None:
                    if int(maxuid) >= int(io_number):
                      if (int(maxuid) == int(io_number)): basetng.logTab(enum_out_stream.testing,"Skip")
                      continue
                  basetng.logTab(enum_out_stream.testing,"idents uid "+str(uid))
                  basetng.logTab(enum_out_stream.testing,"idents pid "+str(pid))
                  basetng.logTab(enum_out_stream.testing,"idents tid "+str(tid))
                  if (uid['tag'].startswith('join') and nodejoin is None): nodejoin = uid['identifier']
                  if (uid['tag'].startswith('esm')): nodesens.append(uid['identifier'])

                  def tooltips(uid):
                    jobid = Identifier( int(uid['identifier']), str(uid['tag']))
                    subdir = jobid.database_subdir()
                    summary_log = os.path.abspath(os.path.join(io_database,subdir,jobid.other_data_stem_ext(enum_other_data.summary_log)))
                    summary_href = "<a href=\"file:///" + summary_log + "\">summary</a>"
                    logfile_log = os.path.abspath(os.path.join(io_database,subdir,jobid.other_data_stem_ext(enum_other_data.logfile_log)))
                    logfile_href = "<a href=\"file:///" + logfile_log + "\">logfile</a>"
                    verbose_log = os.path.abspath(os.path.join(io_database,subdir,jobid.other_data_stem_ext(enum_other_data.verbose_log)))
                    verbose_href = "<a href=\"file:///" + verbose_log + "\">verbose</a>"
                    dagcard = os.path.abspath(os.path.join(io_database,subdir,jobid.other_data_stem_ext(enum_other_data.dag_html)))
                    dagcard_href = "<a href=\"file:///" + dagcard + "\">nodes</a>"
                #we need the identifier for srf node generation
                    link = str(subdir)
                    if os.path.exists(summary_log):
                      link = link +"/"+summary_href
                    if os.path.exists(logfile_log):
                      link = link +"/"+logfile_href
                    if os.path.exists(verbose_log):
                      link = link +"/"+verbose_href
                    if os.path.exists(dagcard):
                      link = link +"/"+dagcard_href
                    return link
                  rootname = os.path.basename(os.path.normpath(io_database))
                  if pid['identifier'] == int(0):
                      G.add_node(
                        0,
                        label=str(rootname) if len(rootname)>0 else 'root',
                        value=20,
                        title=str("<a href=\"file://"+walk_dir+"\">database</a>"),
                        color='root',
                        shape='dot',
                        physics=physics)
                      basetng.logTab(enum_out_stream.testing,"node root")
                  else:
                      G.add_node(
                        pid['identifier'],
                       #label=pid['tag'],
                        label=pid['tag']+"-"+str(int(pid['identifier']))[0:w] if io_labeltag else pid['tag'],
                        value=20,
                        title=tooltips(pid),
                        color=uid['tag'],
                        shape='dot',
                        physics=physics)
                      basetng.logTab(enum_out_stream.testing,"node pid "+str(pid))
                  G.add_node(
                        uid['identifier'],
                       #label=uid['tag'],
                        label=uid['tag']+"-"+str(int(uid['identifier']))[0:w] if io_labeltag else uid['tag'],
                        value=20,
                        title=tooltips(uid),
                        color=uid['tag'],
                        shape='dot',
                        physics=physics)
                  basetng.logTab(enum_out_stream.testing,"node uid "+str(uid))
                  basetng.logTab(enum_out_stream.testing,"edge "+str(pid)+" "+str(uid))
                  G.add_edge(pid['identifier'],uid['identifier'],
                                 physics=physics)
               #  G.add_node(
               #        tid['identifier'],
               #        label="nodes", #uid['tracker']['tag'],
               #        value=tid['size'],
               #        title=nodetips,
               #        color=self.node_colour(tid['tag'],None),
               #        mass = 10, #shorter edges with barnesHut
               #        physics=physics)
               #  G.add_edge(uid['identifier'],tid['identifier'],
               #                   physics=physics)
    # below works but makes the hierarchy messy
      if (G.num_nodes() > 0):
        if False:
          if nodejoin is not None:
            for ens in nodesens:
              G.add_edge(nodejoin,ens,physics=True)
        if G.num_nodes() == 0 and io_create_root_if_empty:
             G.add_node('0',
                          label='root',
                          value=100,
                          title=str("<a href=\"file://"+walk_dir+"\">database</a>"),
                          color=self.node_colour('root',0),
                          physics=physics)
        filename = os.path.join(io_output, filestem + ".html")
       # G.write_html(name=filename) #this broke for file writing
       #below is a hack, we overwrite the node properties at the end because they have not been set correctly, with pid/uid overwrites
       #one day, simplify this AJM TODO
        try:
          for i, node in enumerate(G.nodes):
            ident = node['id']
            mode = node['color'] #tmp holder for mode
            G.nodes[i]['color'] = indexuid[ident]['color']
            G.nodes[i]['value'] = indexuid[ident]['size']
            G.nodes[i]['shape'] = indexuid[ident]['shape']
           #print(G.nodes[i])
        except Exception as e:
           raise ValueError(enum_err_code.developer,"Overwrite of node properties: "+str(e))
        html = G.generate_html() + "\n"
        graphsize = 0
        try:
            if G.num_nodes():
              with open(filename, "w+") as out:
                 out.write(html)
            graphsize =  G.num_nodes()
        except Exception:
            graphsize = 0
        if True: #not silent:
          basetng.logTab(enum_out_stream.logfile,"Graph file: " + str(filename))
          basetng.logTab(enum_out_stream.logfile,"Graph size: " + str(max(0,graphsize-1))) #don't count the root, then highest node==graph size with consecutive numbering
          basetng.logTab(enum_out_stream.logfile,"Graph highest node #: " + str(maxuid))
      else:
          basetng.logTab(enum_out_stream.logfile,"No nodes")
 #}}}

  def run_from_basetng_and_input(self,basetng):
 #{{{
    limit_for_debugging=None
    database = FileSystem(basetng.DataBase(),"","").fstr()
    basetng = self.inner_tree(
                    basetng=basetng,
                    walk_dir=database,
                    filestem=basetng.input.get_as_str(".phasertng.graph_report.filestem."),
                    bestsolnum=basetng.input.get_as_str(".phasertng.graph_report.best_solution."),
                    io_labeltag=basetng.isLevel(enum_out_stream.logfile),
                    io_database=database,
                    silent=basetng.isLevel(enum_out_stream.silence),
                    io_output=database,
                    io_create_root_if_empty=False,
                    full=False,
                    io_number=limit_for_debugging,
                    )
 #}}}

  def inner_tree(self,
                    basetng,
                    walk_dir,
                    filestem,
                    bestsolnum,
                    full=False,
                    io_labeltag=True,
                    io_number=None,
                    silent=True,
                    io_create_root_if_empty=False,
                    io_database="",
                    io_output=""):
      rfactors,hasrfactors = self.rfactor_from_walk(walk_dir)
      if len(rfactors) > 0:
        basetng.logUnderLine(enum_out_stream.logfile,'R-factor Summary')
        basetng.input.generic_string = rfactors[0]
        for line in rfactors:
          basetng.logTab(enum_out_stream.logfile,line.strip())
       #this is a total hack that recreates the 12341234-pref-1 identifier

      sel = int(bestsolnum)
      if len(rfactors) < sel and hasrfactors:
        basetng.logAdvisory('Best Solution selected is outside range')
      if len(rfactors) > 0 and len(rfactors) >= sel:
        basetng.logUnderLine(enum_out_stream.logfile,'Best Solution')
        if (sel > 1):
          basetng.logAdvisory('Best Solution selected is #' + str(sel))
        bestsol = rfactors[sel-1].strip()
        basetng.logTab(enum_out_stream.logfile,'Best ' + bestsol)
        try:
            self.inner_voyager_logfile(
                          walk_dir=walk_dir,
                          bestsol=bestsol,
                          basetng=basetng,
                          root_number=None,
                          )
        except Exception as e:
          # basetng.logAdvisory(str(e))
            raise ValueError(enum_err_code.developer,str(e))

      basetng.logUnderLine(enum_out_stream.logfile,'Pyvis')
      try:
        self.inner_voyager_pyvis(
                          walk_dir =walk_dir,
                          filestem =filestem,
                          io_output=walk_dir,
                          io_number=io_number,
                          io_database=io_database,
                          io_labeltag=io_labeltag,
                          basetng=basetng,
                          )
      except Exception as e:
        raise ValueError(enum_err_code.developer,str(e))
      return basetng


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-d',"--directory",help="output directory",type=str,required=False)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=True)
  parser.add_argument('-o',"--output",help="output filestem",type=str,required=True)
  args = parser.parse_args()
  cards = ""
  if args.cards is not None:
    cards = args.cards.read()
  if cards is None:
    cards = cards + "\nphasertng suite directory = \"" + args.directory + "\""
  basetng = Phasertng("VOYAGER")
  basetng = runTREE().inner_tree(basetng=basetng,filestem=args.output)
