#ifndef __phasertng_xyzRotMatDeg__class__
#define __phasertng_xyzRotMatDeg__class__
#include <scitbx/constants.h>
#include <complex>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/misc_functions.h>

using namespace scitbx;
typedef std::complex<double> cmplex;
typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;
typedef scitbx::af::shared<dvect3> af_dvect3;

namespace phasertng {

  class xyzRotMatDeg
  {
    int ind0;
    int ind1;
    int ind2;
    cmplex cos_sin;
    double cosTheta;
    double sinTheta;
    double c = scitbx::deg_as_rad(1);
    double csqr = fn::pow2(scitbx::deg_as_rad(1));
    dmat33 xyzmat;
    dmat33 rotmat;
    public:
      xyzRotMatDeg() {}
      const dmat33& matrix(int, double);
      const dmat33& grad_tr(int, cmplex, bool);
      const dmat33& hess_tr(int, cmplex, bool);
      const dmat33& tgh(int, double, bool, bool);
      double        xyzAngle(const dvect3&);
      const dmat33& perturbed(const dvect3&);
      dvect3        xyz_angles(const dmat33&);
    public:
      void setup_theta(int dir,double theta_deg);
      void setup(int dir,cmplex cos_sin_);
  };
}

#endif
