#ifndef PHASER_NCS_EDGE_H
#define PHASER_NCS_EDGE_H

#include "Node.h"

#include <string>

namespace phaser {  namespace ncs {

    class EdgeData : boost::noncopyable
    {
        public:
            // Data members
            std::string _arc;
            Node _end_node;

            // Constructor and destructor
            EdgeData( const std::string& arc, const Node& end_node );
            virtual ~EdgeData();
    };

    class Edge
    {
        private:
            boost::shared_ptr< EdgeData > _edge_data;

        public:
            // Constructors and destructor
            Edge( const std::string& arc, const Node& end_node );
            Edge( const boost::shared_ptr< EdgeData >& edge_data );
            Edge( const char edge_key, const Node& parent_node );
            Edge( const Edge& rhs );

            virtual ~Edge();

            // Accessors
            size_t length() const;
            const std::string& get_arc() const;
            const Node& get_end_node() const;

            // Wrappers for managing edges in embedded node
            const Edge get_edge( const char edge_key ) const;
            bool has_edge( const std::string& edge_arc_fragment ) const;
            void connect_edge_to( const Node& node ) const;

            // Printing
            void dump_edge( const std::string& separator = "" ) const;

            // Edge behaviour
            void split_edge_at( const int index ) const;
    };

}  }

#endif /*PHASER_NCS_EDGE_H*/
