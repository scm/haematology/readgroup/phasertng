//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Output.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/constants.h>
#include <phasertng/io/Loggraph.h>
#include <phasertng/io/FileSystem.h>
#include <stdarg.h>
#include <ctime>
#include <utility>
#include <scitbx/histogram.h>

//this controls to global killtime
//the killtime for progressbars is controlled separately
//#define PHASERTNG_CHECK_FOR_KILLTIME
//#define PHASERTNG_CHECK_FOR_KILLFILE

namespace phasertng {

  Output::Output(std::string PACKAGE) : Suite(PACKAGE), Error(err::SUCCESS), Version()
  {
    STREAM_STRING.clear();
    CpuStart = std::clock();
    mainCpuClock = 0;
    modeCpuClock = 0;
    mainWallClock = std::chrono::system_clock::now();
    modeWallClock = std::chrono::system_clock::now();
    outstream_ptr = std::shared_ptr<phenix_out>(new phenix_out);
    callback_ptr  = std::shared_ptr<phenix_callback>(new phenix_callback);
    last_blank_line = false;
  }

  void Output::setOutStream(std::shared_ptr<phenix_out> const& ptr)
  {
    outstream_ptr = ptr;
  }

  void Output::setCallBack(std::shared_ptr<phenix_callback> const& ptr)
  {
    callback_ptr = ptr;
  }

  //----
  // master controller - every goes through this function
  //----
  std::string Output::tab(out::stream where)
  {
    int depth(static_cast<int>(where)-static_cast<int>(out::CONCISE));
    return std::string(std::max(0,DEF_TAB*depth),' ');
  }

  void Output::logOutput(out::stream where,std::string unformatted)
  {
    incrementRunTime();
  //set blank controls before STREAM_STRING has been extended, return if double blank
    if (where > Suite::Store()) return;
    std::string text = tab(where) + unformatted;
   // if (where == out::PROCESS and text.size() >= 2) { text[0] = '*'; text[1] = '*'; }
    bool this_blank_string(unformatted == "");
    bool double_blank(this_blank_string and STREAM_STRING.size() and last_blank_line and
             where <= Suite::Level());
    //we only care about detecting double blanks for the stream at this point
    STREAM_STRING.push_back({where,text}); //before double_blank return
    //the formatting derived from the STREAM_STRING has to account for double_blank separately
    //note that progress bars handle the write of the bars to STREAM_STRING separately
    //note that footer is temporarily added to STREAM_STRING for file writing (nasty but works)
    if (double_blank) return;
    //collect strings even if MUTE, to reconstruct strings
    //This is the bit that does the writing to the streams if it is open
    //if they aren't, the is just stored in the strings with the code above
    //until the streams are opened, and then the strings go to the streams
    //store string in reconstructable from
    //the handling of double blank lines might not be the same
    if (where <= Suite::Level()) //short logfile output
    { //regardless of storage, add line to dynamic output
      if (outstream_ptr)
        outstream_ptr->write(text+"\n");
      last_blank_line = this_blank_string;
    }
    logFlush();
  }

  void Output::logFlush()
  {
    if (outstream_ptr)
      outstream_ptr->flush();
  }

  void Output::incrementRunTime()
  {
  //This stops overflows in the number of ticks and prevents
  //negative times being output
  //Have to check that the integer std::clock() has increased,
  //otherwise just get accumulation of small numbers
    std::clock_t now_clock = std::clock();
    if (now_clock > CpuStart)
    {
      double difference = now_clock - CpuStart;
             difference /= double(CLOCKS_PER_SEC);
      mainCpuClock += difference;
      modeCpuClock += difference;
      CpuStart = now_clock;
    }
  //overflow - clock() has returned to start of long int
  //have to miss out the accumulated time between now_clock and CpuStart
    CpuStart = now_clock;
  }

  void Output::checkForKill()
  {
    if (KILLFLAG)
      throw Error(err::KILLFLAG,"Job killed");
    return;
#ifdef PHASERTNG_CHECK_FOR_KILLFILE
    if (Suite::KillTime() == 0 and Suite::KillFile() == std::string(""))
      return;
    if (error_code() == err::KILLTIME or error_code() == err::KILLFILE)
      return; //prevents recursion if checkForKill called in logging errors
    if (Suite::KillTime() > 0 and mainCpuClock/60. > Suite::KillTime())
    { //prioritize killtime because no disk access
#ifdef PHASERTNG_CHECK_FOR_KILLTIME
      throw Error(err::KILLTIME,"Job killed for elapsed time exceeding Kill-Time limit");
#endif
    }
    FileSystem killfile(Suite::KillFile());
    if (killfile.exists())
      throw Error(err::KILLFILE,"Job killed by detection of Kill-File");
#else
    if (Suite::KillTime() == 0)
      return;
    if (error_code() == err::KILLTIME)
      return; //prevents recursion if checkForKill called in logging errors
    if (mainCpuClock/60. > Suite::KillTime())
    {
#ifdef PHASERTNG_CHECK_FOR_KILLTIME
      throw Error(err::KILLTIME,"Job killed for elapsed time exceeding Kill-Time limit");
#endif
    }
#endif
  }

  std::string Output::format_time(std::string Description, double seconds,bool print_seconds)
  {
    int Days = seconds / 24 / 60 / 60;
    int Hrs = seconds / 60 / 60 - Days * 24;
    int Mins = seconds / 60 - Days * 24 * 60 - Hrs * 60;
    double Secs = seconds - Days * 24 * 60 * 60 - Hrs * 60 * 60 - Mins * 60;
    if (print_seconds)
      return snprintftos(
        "%*s %2i days %2i hrs %2i mins %5.2f secs (%10.2f secs)",
        Description.size(), Description.c_str(), Days, Hrs, Mins, Secs, seconds);
    if (seconds < 60) //log the prediction anyway
      return snprintftos("%*s less than one minute",Description.size(), Description.c_str());
    return snprintftos(
        "%*s %2i days %2i hrs %2i mins",
        Description.size(), Description.c_str(), Days, Hrs, Mins);
  }

  //================
  //HELPER FUNCTIONS
  //================
  void Output::logUnderLine(out::stream where, std::string text)
  {
    checkForKill(); //this is the only place we check
    logBlank(where);
    logTab(where,text);
    logLine(where,text.size());
  }

  void Output::logLine(out::stream where, const int t,char ch)
  {
    logTab(where,std::string(t,ch));
  }

  void Output::logChevron(out::stream where, std::string text)
  {
    logBlank(where);
    logTab(where,">>"+text+"<<");
  }

  void Output::logBlank(out::stream where)
  {
    //add the tab at the start of the line so that a regular expression looking for the blanks
    //will pick out the correct lines
    logOutput(where,"");
  }

  void Output::logKeywords(out::stream where,std::string text)
  {
    //out::stream where = LEVEL;//has to be here for phenix output
    sv_string lines;
    hoist::algorithm::split(lines, text, ("\n"));
    for (auto line : lines)
      logTab(where,line.c_str());
    //this should never happen now as solution files will be input as .sol files always!
    logBlank(where);
  }

  void Output::logTab(out::stream where,const std::string text)
  {
    logOutput(where,text);
    if (collect_table)
    {
      TABLES[imode].back().append(text);
      TABLES[imode].back().append("\n");
    }
  }

  void
  Output::logFileWritten(out::stream where,
      bool written,const std::string title,FileSystem f)
  {
    //and we replace the database part with $DAGDATABASE
    if (written)
    {
      logTab(where,title +" file: writing to " + f.qfstrq());
      FILEOUT.push_back(f.get_tuple());
      return;
    }
    logTab(where,title + " file: not written (" + f.qfstrq() + ")");
  }

  void Output::logTabArray(out::stream where,const af_string text)
  {
    for (auto item : text)
    {
      if (item.size() and item.back() == '\n') //if accidental return on end of the line
        item.pop_back(); //or else there are double blanks possible
      item.size() ? logTab(where,item) : logBlank(where);
    }
  }

  void Output::logEllipsisOpen(out::stream where,const std::string text)
  {
    logBlank(where);
    logTab(where,text+"...");
  }

  void Output::logEllipsisShut(out::stream where)
  {
    logTab(where,"...Done");
    logBlank(where);
  }

  void Output::logTableTop(out::stream where,const std::string text,bool underline)
  {
    logBlank(where);
    logChevron(where,"TableTop");
    std::string tabletop = "Table: " + text;
    collect_table = true;
    if (!TABLES.count(imode))
      TABLES[imode] = af_string();
    TABLES[imode].push_back(""); //new blank line
    if (underline)
    {
      logUnderLine(where,tabletop);
    }
    else
    {
      logBlank(where); //first line blank even if not underline
      logTab(where,tabletop);
    }
  }

  void Output::logTableEnd(out::stream where)
  {
    collect_table = false;
    logChevron(where,"TableEnd");
    logBlank(where);
  }

  void Output::logWarning(const std::string intext)
  {
    if (!WARNINGS.count(imode))
      WARNINGS[imode] = af_string();
    auto& warn = WARNINGS[imode];
    if (std::find(warn.begin(),warn.end(),intext) != warn.end())
      return; //only log a warning once
    warn.push_back(intext);
    out::stream where = out::SUMMARY;
    logBlank(where);
    logTab(where,"**Warning: " + intext);
    logBlank(where);
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("warn",intext);
  }

  void Output::logWarnings(out::stream where)
  {
    logUnderLine(where,"Warnings");
    if (!WARNINGS.count(imode))
      logTab(where,"None");
    else
    {
      for (auto intext : WARNINGS[imode])
      {
        logTab(where,"Warning: " + intext);
        //callback first time only
      }
    }
  }

  void Output::logAdvisory(const std::string intext)
  {
    if (!ADVISORIES.count(imode))
      ADVISORIES[imode] = af_string();
    auto& warn = ADVISORIES[imode];
    if (std::find(warn.begin(),warn.end(),intext) != warn.end())
      return; //only log a warning once
    warn.push_back(intext);
    out::stream where = out::SUMMARY;
    logBlank(where);
    logTab(where,"**Advisory: " + intext);
    logBlank(where);
    auto& advice = ADVISORIES[imode];
    if (std::find(advice.begin(),advice.end(),intext) == advice.end())
      advice.push_back(intext);
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("advise",intext);
  }

  void Output::logAdvisories(out::stream where)
  {
    logUnderLine(where,"Advisories");
    if (!ADVISORIES.count(imode))
      logTab(where,"None");
    else
    {
      for (auto intext : ADVISORIES[imode])
      {
        logTab(where,"Advisory: " + intext);
      }
    }
  }

  void Output::logProgressBarStart(out::stream where,const std::string title,const int length,int flag,bool usekilltime)
  {
    logTab(where,title + " [#" + itos(length) + "]");
    if (where != Suite::Level())
      return;
    if (length == 0)
    {
      logTab(where,"There will be no progress bars");
      return;
    }
    progress[flag] = barstruct();
    progress[flag].WallClock = std::chrono::system_clock::now();
    progress[flag].Length = length;
    progress[flag].Title = title;
    progress[flag].UseKillTime = usekilltime;
    double BarSizeIdeal(unsigned(DEF_LOGFILE_WIDTH)-tab(where).size()-7); //7 = "=| DONE"
    if (length == 500) BarSizeIdeal = 50;
    double ratio = double(length)/std::max(1.,BarSizeIdeal);
    double FloatChunk = ceil(ratio);
    progress[flag].Chunk = std::max(static_cast<int>(1),static_cast<int>(FloatChunk));
    progress[flag].BarSize = std::max(static_cast<int>(1),static_cast<int>(floor(length/static_cast<double>(progress[flag].Chunk))));
    logTab(where,"One progress bar represents " + itos(progress[flag].Chunk) + " steps of function");
    if (Suite::KillTime() and usekilltime)
    logTab(where,"There will be " + itos(progress[flag].BarSize) + " progress bars in total with time limit " + dtos(Suite::KillTime(),0) + " mins");
    else
    logTab(where,"There will be " + itos(progress[flag].BarSize) + " progress bars in total with no time limit");
    logTab(where,"0%" + std::string(progress[flag].BarSize,' ') + "100%");
    //end of fixed output, now move to dynamic output
    if (outstream_ptr)
      outstream_ptr->write(tab(where)+"|"); //no return
    progress[flag].line = tab(where)+"|"; //for separate call to storage
    logFlush();
  }

  bool Output::logProgressBarNext(out::stream where,int flag)
  {
    //Since a lot of time is spent on progress bars we need to increment
    //runtime here each time
    incrementRunTime();
    if (where != Suite::Level())
      return true;
    if (progress[flag].Length == 0)
      return true;
    progress[flag].Count++;
    bool kill_on_prediction(false);
    bool kill_on_elapsed(!kill_on_prediction);
    if (progress[flag].Count % progress[flag].Chunk == 0)
    {
      if (outstream_ptr)
        outstream_ptr->write("="); //no return
      progress[flag].line += "="; //for separate call to storage
      logFlush();
      progress[flag].Bars++;
      if (progress[flag].BarSize > 1 and //haven't already finished
          progress[flag].Bars == 1) //predict on the placement of the first bar
      {
        //print prediction even if not used
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = now - progress[flag].WallClock;
        double seconds = elapsed.count()*progress[flag].BarSize;
        logProgressBarAgain(where,format_time("Predicted Wall Time: ",seconds,false));
        if (kill_on_prediction and Suite::KillTime() and
            progress[flag].UseKillTime and seconds > Suite::KillTime()*60)
        {
          progress[flag].Killed = true;
          return false;
        }
      }
      //below is the default
      if (kill_on_elapsed and Suite::KillTime() and //kill on elapsed time instead
          progress[flag].UseKillTime)
      {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = now - progress[flag].WallClock;
        double seconds = elapsed.count();
        if (seconds > Suite::KillTime()*60)
        {
          logProgressBarAgain(where,format_time("Elapsed Wall Time: ",seconds,false));
          if (progress[flag].BarSize > 1) //haven't already finished
          {
            progress[flag].Killed = true;
            return false;
          }
        }
      }
    }
/*
    else if (progress[flag].Bars == 0) //before the placement of the first bar
    { //the first bar may be very very slow, so check each iteration for first
      //but checking each of the first bar is in itself very slow so overall not worth it
      //comment out
      std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
      std::chrono::duration<double> elapsed = now - progress[flag].WallClock;
      double inv_frac_of_first_bar = double(progress[flag].Chunk)/double(progress[flag].Count);
      double seconds = elapsed.count()*inv_frac_of_first_bar*progress[flag].BarSize;
      if (kill_on_prediction and Suite::KillTime() and
          progress[flag].UseKillTime and seconds > Suite::KillTime()*60)
      {
        logProgressBarAgain(where,format_time("Predicted Wall Time: ",seconds,false));
        progress[flag].Killed = true;
        return false;
      }
      seconds = elapsed.count(); //switch to elapsed time
      //below is the default
      if (kill_on_elapsed and Suite::KillTime() and //kill on elapsed time instead
          progress[flag].UseKillTime and seconds > Suite::KillTime()*60)
      {
        logProgressBarAgain(where,format_time("Elapsed Wall Time: ",seconds,false));
        progress[flag].Killed = true;
        return false;
      }
    }
*/
    return true;
  }

  void Output::logProgressBarAgain(out::stream where,const std::string text,int flag)
  {
    if (where != Suite::Level())
      return;
    if (progress[flag].Length == 0)
      return;
    PHASER_ASSERT(progress[flag].line.size());
    STREAM_STRING.push_back({where,progress[flag].line});
    logBlank(where); //eol
    logTab(where,text);
    logTab(where,"0%" + std::string(progress[flag].BarSize,' ') + "100%");
    //end of fixed output, now move to dynamic output
    std::string progress_bar = tab(where) + "|" + std::string(progress[flag].Bars,'=');
    if (outstream_ptr)
      outstream_ptr->write(progress_bar); //no return
    progress[flag].line = progress_bar; //for separate call to storage
  }

  void Output::logProgressBarEnd(out::stream where,int flag)
  {
    if (where != Suite::Level())
      return;
    if (progress[flag].Length == 0)
      return;
    if (progress[flag].Count == progress[flag].Length)
    {
      if (outstream_ptr)
        outstream_ptr->write("=| DONE\n");
      progress[flag].line += "=| DONE"; //for separte call to storage
    }
    else
    {
      auto c = itos(progress[flag].Count);
      auto C = itos(progress[flag].Length);
      double P = std::ceil((100.*progress[flag].Count)/progress[flag].Length); //implicit cast to double
      auto cC = " [#"+c+"/#"+C+"]=" + dtos(P,0) + "%";
      if (outstream_ptr)
        outstream_ptr->write("=| STOP"+cC+"\n");
      progress[flag].line += "=| STOP"+cC; //for separte call to storage
    }
    STREAM_STRING.push_back({where,progress[flag].line});
    logBlank(where);
    logFlush();
    if (progress[flag].Killed)
      logWarning("Progress killed: " + progress[flag].Title);
    progress.erase(flag);
  }

  void Output::logSectionHeader(out::stream where,std::string header)
  {
    logBlank(where);
    int len(0),max_len(0);
    for (int i = 0; i < header.size(); i++)
    {
      if (header[i] != '\n')
        max_len = std::max(max_len,++len);
      else
        len = 0;
    }
    logLine(where,max_len);
    logTab(where,header);
    logLine(where,max_len);
    logBlank(where);
    logFlush();
  }


  sv_string
  Output::Banner()
  {
    // ******************************************************************************************
    sv_string output;
    sv_string head1 = {
               "            __                                        __              ",
               "     ____  / /_  ____  ________  _____              _/ /_____  ____ _ ",
               "    / __ `/ __ `/ __ `/ ___/ _ `/ ___/=/=/=/=/=/=/=/  __/ __ `/ __ `/ ",
               "   / /_/ / / / / /_/ (__  )  __/ /                 / /_/ / / / /_/ /  ",
               "  / ,___/_/ /_/\\__,_/____/\\___/_/                  \\__/_/ /_/\\__, /   ",
               " /_/                                                        /____/    ",
               "                                                                      "
               };
    std::string star3= "***";
    std::string blank((DEF_LOGFILE_WIDTH-6-head1[0].size())/2,' ');
    std::string line(DEF_LOGFILE_WIDTH,'*');
    output.push_back(line);
    for (int i = 0; i < head1.size(); i++)
      output.push_back(star3 + blank +head1[i] + blank + star3);
    output.push_back(line);
    return output;
  }

  void Output::logProgramStart(out::stream where)
  {
    if (USE_CALLBACKS and callback_ptr)
    {
      callback_ptr->call_back("phenix_start","");
      callback_ptr->call_back("PROGRAM",program_name() + " " + version_number());
    }
    sv_string banner = Banner();
    for (auto b : banner)
      logTab(where,b);
  }

  void Output::logHeader(out::stream where,std::string modetxt)
  { logHeaderTitle(where,modetxt,""); }

  void Output::logHeaderTitle(out::stream where,std::string modetxt,std::string title)
  {
    last_header = STREAM_STRING.size();
    std::string phasertngline;
    {
    std::string gitinfo = "";
    if (git_revision().size())
      gitinfo = " git " + git_revision();
    std::string start_module = "*** " + program_name() + " " + version_number() + " " + version_date() + gitinfo;
    std::string end_module = " ***";
    int spacer_width = unsigned(DEF_LOGFILE_WIDTH) - start_module.size() - end_module.size();
    PHASER_ASSERT(spacer_width > 0);
    std::string spacer_module(spacer_width,' ');
    phasertngline = start_module + spacer_module + end_module;
    }
    std::string modeline;
    {
    std::string start_module = "*** Mode: " + modetxt;
    std::string end_module = " ***";
    int spacer_width = unsigned(DEF_LOGFILE_WIDTH) - start_module.size() - end_module.size();
    PHASER_ASSERT(spacer_width > 0);
    std::string spacer_module(spacer_width,' ');
    modeline = start_module + spacer_module + end_module;
    }
    logLine(where,DEF_LOGFILE_WIDTH,'*');
    logTab(where,phasertngline);
    logTab(where,modeline);
    logLine(where,DEF_LOGFILE_WIDTH,'*');
    time_t start_time;
    std::time(&start_time);
    std::string ct = std::ctime(&start_time);
    ct.pop_back(); //remove final \n
    logTab(where,"Run time:     " + ct);
    if (title.size())
      logTab(where,"Title: " + title);
    logBlank(where);
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
    logTab(out::PROCESS,"Compiled with phaser-like functionality implemented");
    logBlank(where);
#endif
    if (git_info().size())
      logTabArray(out::VERBOSE,git_info());
    logFlush();
  }

  void Output::logLoop(out::stream where,std::string title,std::string additional_text,std::string loop_marker_type,char ch)
  {
    clear_storage();
    std::string str(DEF_LOGFILE_WIDTH,ch);
    str[2] = 'L'; str[3] = 'O'; str[4] = 'O'; str[5] = 'P';
    logTab(where,str);
    std::string text;
    if (title.size())
      text = title + "\n";
    if (additional_text.size())
      text += additional_text + "\n";
    sv_string lines;
    hoist::algorithm::split(lines, text, ("\n"));
    for (auto line : lines)
    {
      if (line.size())
      {
        std::string leader(2,ch);
        std::string footer(2,ch);
        std::string formatted = leader + loop_marker_type + ": "+line;
        int l = int(DEF_LOGFILE_WIDTH)-formatted.size()-footer.size();
        if (l >= 0)
          formatted += std::string(l,' ') + footer;
        logTab(where,formatted);
      }
    }
    logTab(where,str);
    logBlank(where);
  }

  void Output::logLoopOpen(out::stream where,std::string title,int maxcount,std::string additional)
  {
    if (additional.back() == '\n') additional.pop_back();
    //make sure the LOOP is not currently present due to bad exit before restart
    auto iter = std::find(LOOPS.begin(), LOOPS.end(), title);
    if (iter != LOOPS.end()) LOOPS.erase(iter);
    loop_info[title].count = 0;
    loop_info[title].max = maxcount;
    LOOPS.push_back(title);
    std::string text = additional.size() ? std::string(additional + "\n") : "";
   // if (loop_info[title].max)
   //   text += "Number of loops will be " + itos(loop_info[title].max);
    logLoop(where,title,text,"LOOP OPEN",'v');
  }

  void Output::logLoopNext(out::stream where,std::string title,std::string current)
  {
    loop_info[title].count++;
    loop_info[title].current = current;
    std::string text = "";
    if (LOOPS.size())
      text += "All currently nested loops below ----\n";
    else
      text += "No currently nested loops\n";
    for (auto m : LOOPS)
    {
      text += loop_info[m].max ?
          "#"+itos(loop_info[m].count)+"/#"+itos(loop_info[m].max) + " of {" + m + "} " + loop_info[m].current + "\n":
          "#"+itos(loop_info[m].count) + " {" + m + + "} " + loop_info[m].current + "\n";
    }
    logLoop(where,title,text,"LOOP NEXT",'>');
    int count = loop_info[title].count;
    hoist::replace_all(title," ","_");
    if (count != 1)
      LOOPXML += std::string((LOOPS.size()-1)*4+2,' ') + "</" + title + ">\n";
    LOOPXML += std::string((LOOPS.size()-1)*4+2,' ') + "<" + title + " info=\"" + current + "\">\n";
  }

  void Output::logLoopExit(out::stream where,std::string title)
  {
    std::string text = loop_info[title].max ?
      "Break #"+itos(loop_info[title].count)+"/#"+itos(loop_info[title].max) + " of {" + title + "} " + loop_info[title].current + "\n":
      "Break #"+itos(loop_info[title].count) + " {" + title + "} " + loop_info[title].current + "\n";
    logLoop(where,title,text,"LOOP EXIT",'=');
    auto iter = std::find(LOOPS.begin(), LOOPS.end(), title);
    if (iter != LOOPS.end()) LOOPS.erase(iter);
    //should erase loop_info also
    //hoist::replace_all(title," ","_");
   // LOOPXML += std::string(LOOPS.size()*4+2,' ') + "</" + title + ">\n";
  }

  void Output::logLoopShut(out::stream where,std::string title)
  {
    std::string text = "";
    logLoop(where,title,text,"LOOP SHUT",'^');
    auto iter = std::find(LOOPS.begin(), LOOPS.end(), title);
    if (iter != LOOPS.end()) LOOPS.erase(iter);
    if (loop_info[title].count != 0)
    {
      hoist::replace_all(title," ","_");
      LOOPXML += std::string(LOOPS.size()*4+2,' ') + "</" + title + ">\n";
    }
    //write the loop file where there is a termination
  }

  void Output::logLoopHere(out::stream where,std::string additional)
  {
    if (additional.back() == '\n') additional.pop_back();
    std::string text = additional.size() ? std::string(additional + "\n") : "";
    if (LOOPS.size())
      text += "\nAll currently nested loops below ----\n";
    else
      text += "\nNo currently nested loops ----\n";
    for (auto m : LOOPS)
    {
      text += loop_info[m].max ?
          "#"+itos(loop_info[m].count)+"/#"+itos(loop_info[m].max) + " of {" + m + "} " + loop_info[m].current + "\n":
          "#"+itos(loop_info[m].count) + " {" + m + + "} " + loop_info[m].current + "\n";
    }
    logLoop(where,"",text,"LOOP HERE",'-');
  }

  std::string Output::looptitle() const
  {
    std::string text;
    for (auto m : LOOPS)
    {
      auto current = loop_info.at(m).current;
      hoist::algorithm::trim(current);
      if (current.size())
        text += "[" + m + "]=[" + current + "] ";
      else
        text += "[" + m + "] ";
    }
    if (text.size()) text.pop_back();
    if (text == " ") text = ""; //bugfix hack, set default if blank
    return text;
  }

  void Output::logLoopInfo(out::stream where,std::string additional)
  {
    if (additional.back() == '\n') additional.pop_back();
    if (!additional.size()) return;
    std::string text = additional.size() ? std::string(additional + "\n") : "";
    logLoop(where,"",text,"LOOP INFO",'-');
    hoist::replace_all(additional,"\n"," ");
    add_loopxml("Loop Information",additional);
  }

  void Output::add_loopxml(std::string title,std::string message)
  {
    hoist::replace_all(title," ","_");
    LOOPXML += std::string(LOOPS.size()*4+2,' ') + "<" + title + ">"; //no \n
    LOOPXML += message; //no \n
    LOOPXML += "</" + title + ">\n";
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("loopxml",loopxml());
  }

  //write the loop file where there is a termination
  std::string Output::loopxml() const
  {
    std::string text = LOOPXML;
    for (int i = LOOPS.size()-1; i>=0; i--)
    { //complete the unfinished loops
      std::string title = LOOPS[i];
      hoist::replace_all(title," ","_");
      text += std::string(i*4,' ') + "</" + title + ">\n";
    }
    return text;
  }

  void Output::StartModeTime()
  {
    modeCpuClock = 0;
    modeWallClock = std::chrono::system_clock::now();
  }

  void Output::logTrailer(out::stream where)
  {
    logTabArray(where,format_trailer());
  }

  void
  Output::add_trailer(out::stream where)
  { //nasty, but works
    //fake trailer for adding to written files, not output at this point
    //timings in the trailer will be before the logfile write
    auto trailer = format_trailer();//not formatted with where
    for (auto& item : trailer)
      STREAM_STRING.push_back({where,item}); //before double_blank return
  }

  void
  Output::subtract_trailer()
  { //nasty, but works
    //fake trailer for adding to written files, not output at this point
    //timings in the trailer will be before the logfile write
    auto trailer = format_trailer();//not formatted with where
    for (int o = 0; o < trailer.size(); o++)
      STREAM_STRING.pop_back();
  }

  af_string Output::format_trailer()
  {
    incrementRunTime();
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::chrono::duration<double> mainWall = now - mainWallClock;
    std::chrono::duration<double> modeWall = now - modeWallClock;
    af_string output;
    output.push_back("");
    output.push_back(format_time("Mode Elapsed Wall Time:",modeWall.count()));
    output.push_back(format_time("Mode Elapsed CPUs Time:",modeCpuClock)); //seconds
    output.push_back(format_time("Main Elapsed Wall Time:",mainWall.count()));
    output.push_back(format_time("Main Elapsed CPUs Time:",mainCpuClock));
    time_t finish_time;
    std::time(&finish_time);
    std::string ft = std::ctime(&finish_time);
    ft.pop_back(); //remove final \n or else double blank does not work
    output.push_back("Finished: " + ft);
   // output.push_back("");
    return output;
  }

  double Output::get_mode_cpus_seconds()
  { return modeCpuClock; }

  double Output::get_mode_wall_seconds()
  {
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::chrono::duration<double> modeWall = now - modeWallClock;
    return modeWall.count();
  }

  double Output::get_cumulative_cpus_seconds()
  { return mainCpuClock + cumulativeCpusSeconds; }

  double Output::get_cumulative_wall_seconds()
  {
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::chrono::duration<double> mainWall = now - mainWallClock;
    return mainWall.count() + cumulativeWallSeconds;
  }

  void   Output::set_cumulative_wall_seconds(double t)
  { cumulativeWallSeconds = t; }

  void   Output::set_cumulative_cpus_seconds(double t)
  { cumulativeCpusSeconds = t; }

  sv_string
  Output::Footer()
  {
    sv_string output;
    //failure was triggered from a non-escaped python error
    std::string exit_status = failure() ?
        "EXIT STATUS: FAILURE (" + itos(exit_code()) + ")":
        "EXIT STATUS: SUCCESS";
    output.push_back("");
    output.push_back(std::string(exit_status.size(),'-'));
    output.push_back(exit_status);
    output.push_back(std::string(exit_status.size(),'-'));
    output.push_back("");
    output.push_back("If you use this software please cite:");
    output.push_back("\"Phaser Crystallographic Software\"");
    output.push_back("A.J. McCoy, R.W. Grosse-Kunstleve, P.D. Adams, M.D. Winn, L.C. Storoni & R.J. Read");
    output.push_back("J. Appl. Cryst. (2007). 40, 658-674");
    output.push_back("<a href=\"http://journals.iucr.org/j/issues/2007/04/00/he5368/he5368.pdf\">PDF</a>");
    output.push_back("");
    return output;
  }

  void
  Output::logProgramEnd(out::stream where)
  {
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("phenix_end","");
    sv_string footer = Footer();
    for (auto f : footer)
      logTab(where,f);
    logFlush();
  }

  void Output::logError(out::stream where,Error error)
  {
    *(static_cast<Error *>(this)) = error;
    logBlank(where);
    std::string text("ERROR (" + error_type() + "): " + error_message());
    size_t maxlen = DEF_LOGFILE_WIDTH;
    logLine(where,std::min(text.size(),maxlen));
    logTab(where,text);
    logLine(where,std::min(text.size(),maxlen));
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("error",error_message());
    logFlush();
  }

  void Output::logAssert(out::stream where,std::string text)
  {
    *(static_cast<Error *>(this)) = Error(err::ASSERT,text);
    logBlank(where);
    size_t maxlen = DEF_LOGFILE_WIDTH;
    logLine(where,std::min(text.size(),maxlen));
    logTab(where,text);
    logLine(where,std::min(text.size(),maxlen));
    if (USE_CALLBACKS and callback_ptr)
      callback_ptr->call_back("error",error_message());
    logFlush();
  }

  std::string Output::get_level(std::string level_)
  {
    out::stream where = out::stream2Enum(level_);
    std::string tmp;
    for (auto item:STREAM_STRING)
      if (item.first <= where)
        tmp += item.second;
    return tmp;
  }

  std::string Output::command_line(bool with_changelog)
  {
    std::string commandline;
    std::string gittxt = git_revision();
    commandline +=   "Version:      " + program_name() + " " + version_number() + "\n";
    if (getenv("OSTYPE") != NULL)
      commandline += "OS type:      " + std::string(getenv("OSTYPE")) + "\n";
    commandline +=   "Release Date: " + version_date() + "\n";
    if (gittxt.size())
      commandline += "git revision: " + gittxt;
    return commandline;
  }

  void Output::logGraph(out::stream where,const Loggraph& loggraph)
  {
    //store if not written
    if (!loggraph.data_text.size())
      return; //nothing in loggraph
    if (Suite::Loggraphs())
    {
      logBlank(where);
      logTab(out::CONCISE,loggraph.print()); //always left justify
    }
    if (!LOGGRAPHS.count(imode))
      LOGGRAPHS[imode] = af_string();
    LOGGRAPHS[imode].push_back(loggraph.print());
  }

  void Output::logProtocol(out::stream where,
      std::vector<sv_string> protocol,
      sv_int NCYC,
      std::string MINIMIZER)
  {
    std::string itab = "--";
    logUnderLine(where,"Protocol");
    for (int macrocycle = 0; macrocycle < protocol.size(); macrocycle++)
    {
      PHASER_ASSERT(macrocycle < NCYC.size());
      PHASER_ASSERT(protocol[macrocycle].size());
      logTab(where,"Protocol: Macrocycle #" + itos(macrocycle+1));
      if (stoupper(protocol[macrocycle][0]) == "OFF") // Relies on consistent phil
      {
        logTab(where,itab + "No parameters to refine this macrocycle");
      }
      else
      {
        std::string txt = itab + "Refine: ";
        for (int i = 0; i < protocol[macrocycle].size(); i++)
          txt += stolower(protocol[macrocycle][i]) + " ";
        txt.pop_back();
        logTab(where,txt);
        logTab(where,itab + "Number of cycles: " + itos(NCYC[macrocycle]));
        logTab(where,itab + "Minimizer: " + MINIMIZER);
      }
    }
    //logBlank(where); may want to add extras
  }

  std::string Output::Print(out::stream where)
  {
    std::string result;
    //the start is the line before the logHeader (MODE 3-line header) not the Banner
    auto banner = Banner();
    bool banner_present = false;
    //below is a cheap trick to look for a unique string in the header
    int last_header10 = std::min(last_header+10,static_cast<int>(STREAM_STRING.size()));
    for (int o = last_header; o < last_header10; o++)
    {
      if (STREAM_STRING[o].second == banner[6]) //unique string
      {
        banner_present = true;
        break;
      }
    }
    if (!banner_present)
      for (int o = 0; o <  banner.size(); o++)
        result += banner[o]+"\n";
    bool last_blank_string = false;
    for (int o = last_header; o < STREAM_STRING.size(); o++)
    {
      auto& s = STREAM_STRING[o];
      if (s.first > where) continue;
      bool this_blank_string = std::all_of(s.second.begin(),s.second.end(),isspace);
      bool double_blank(this_blank_string and last_blank_string);
      if (!double_blank)
      {
        result += s.second + "\n";
        last_blank_string = this_blank_string;
      }
    }
    //below is a cheap trick to look for a unique string in the header
    bool footer_present = false;
    auto footer = Footer();
    int last_footer = std::min(0,static_cast<int>(STREAM_STRING.size())-10);
    for (int o = last_footer; o < STREAM_STRING.size(); o++)
    {
      if (STREAM_STRING[o].second == footer[6]) //unique string, If you use this
      {
        footer_present = true;
        break;
      }
    }
    if (!footer_present)
      for (int o = 0; o < footer.size(); o++)
        result += footer[o]+"\n";
    return result;
  }

  void Output::clear_storage()
  {
    WARNINGS.clear();
    ADVISORIES.clear();
    LOGGRAPHS.clear();
    TABLES.clear();
    STREAM_STRING = std::vector<std::pair<out::stream,std::string> >(0);
    last_header = 0; //this is speed for printing on output
  }

  void Output::clear_mode_storage()
  {
    WARNINGS.clear();
    ADVISORIES.clear();
    LOGGRAPHS.clear();
    TABLES.clear();
  }

  void Output::logHistogram(out::stream where,sv_double data,int nslots,bool logscale,std::string title)
  {
    double min_f_mean = std::numeric_limits<double>::max();
    double max_f_mean = std::numeric_limits<double>::lowest();
    int i(0);
    af_double histdata;
    for (auto item : data)
    {
      histdata.push_back(item);
      min_f_mean = std::min(item,min_f_mean); //histogram, data
      max_f_mean = std::max(item,max_f_mean); //histogram
    }
    double buffer = 10;
    double histmin = min_f_mean-buffer;
    double histmax = max_f_mean+buffer;
    logHistogram(where,histdata,nslots,logscale,title,histmin,histmax);
  }

  void Output::logHistogram( out::stream where,af_double data,int nslots,bool logscale,std::string title,double minh,double maxh,bool scatter,bool ints)
  {
    logChevron(where,"Histogram of " + title);
    logTab(where,"Total of " + itos(data.size()) + " datapoints");
    af::shared<long> values;
    af::shared<long> slots;
    af::shared<double> centres;
    int p;
    int d;
    if (minh==0 and maxh==0)
    {
      scitbx::histogram<double> histogram(data.const_ref(),nslots);
      double a = std::max(std::fabs(histogram.data_min()),std::fabs(histogram.data_max()));
      p = 10;
      d = (a == 0) ? 0 : std::min(p-4,std::max(0,static_cast<int>(p-3-std::ceil(std::log10(a)))));
      values = histogram.slots();
      slots = values.deep_copy();
      centres = histogram.slot_centers();
    }
    else
    {
      scitbx::histogram<double> histogram(data.const_ref(),minh,maxh,nslots);
      double a = std::max(std::fabs(minh),std::fabs(maxh));
      p = 10;
      d = (a == 0) ? 0 : std::min(p-4,std::max(0,static_cast<int>(p-3-std::ceil(std::log10(a)))));
      values = histogram.slots();
      slots = values.deep_copy();
      centres = histogram.slot_centers();
    }
    if (ints) //put the centres back on the integers not between the integers
      for (auto& item:centres)
        item=std::floor(item);
    int cols(60);
    long maxval(0);
    for (int i  = 0; i < values.size(); i++)
      maxval = std::max(maxval,values[i]);
    if (maxval == 0) return;
    sv_int grad((cols/10)+1,0);
    if (maxval <= cols/10) //special case for very very small histograms
    { //always linear
      for (int i = 0; i < grad.size(); i++)
      {
        grad[i] = i; //goes higher than maxval
      }
      for (int j  = 0; j < slots.size(); j++)
      {
        if (slots[j] > 0)
        {
          slots[j] = static_cast<long>(std::round(10*slots[j]));
        }
      }
    }
    else if (logscale)
    {
      double base = std::exp(static_cast<double>(cols)/std::log(maxval));
      for (int i = 0; i < grad.size(); i++)
      {
        grad[i] = static_cast<int>(std::exp((i*10)/std::log(base)));
      }
      for (int j  = 0; j < slots.size(); j++)
      {
        if (slots[j] > 0)
        {
          double logbase = std::log(slots[j]) * std::log(base);
          slots[j] = static_cast<long>(std::ceil(logbase))+1;
        }
      }
    }
    else
    {
      double base = static_cast<double>(maxval)/static_cast<double>(cols);
      for (int i = 0; i < grad.size(); i++)
      {
        grad[i] = i*10*base;
      }
      for (int j  = 0; j < slots.size(); j++)
      {
        slots[j] = static_cast<long>(std::round(slots[j]/base));
      }
    }
    {{
    logTab(where,"Mid-Bin-Value vs Number" + std::string(logscale ? "(log scale)":""));
    std::string line = snprintftos("%-*s",p," |");
    for (int i = 0; i < grad.size(); i++)
      line += snprintftos("%-10d",grad[i]);
    logTab(where,line);
    line = snprintftos("%-*s",p," V");
    for (int i = 0; i < grad.size(); i++)
      line += snprintftos("%-10s","|");
    logTab(where,line);
    for (int i  = 0; i < slots.size(); i++)
    {
      std::string stars(slots[i],'*');
      std::string numbs = " (" + std::to_string(values[i]) + ")";
      std::string line = snprintftos("% *.*f %s",
         p-1,d,centres[i],
         std::string(stars+numbs).c_str());
      if (ints)
        line = snprintftos("%2i%*s %s",
         static_cast<int>(centres[i]),p-2,"",
         std::string(stars+numbs).c_str());
      logTab(where,line);
    }
    logBlank(where);
    }}
    {{
      Loggraph loggraph;
      loggraph.title = title;
      loggraph.scatter = scatter;
      loggraph.data_labels = logscale ? "Bin log(Slots) Centres Number" : "Bin Slots Centres Number";
      for (int i = 0; i < slots.size(); i++)
        loggraph.data_text +=
                              itos(i) + " "  +
                              std::to_string(slots[i]) + " " +
                              dtos(centres[i]) + " "  +
                              std::to_string(values[i]) + " "  +
                              "\n";
      if (logscale)
      {
        loggraph.graph.resize(2);
        loggraph.graph[0] = title + " Log-scale Histogram:AUTO:1,2";
        loggraph.graph[1] = title + " Linear-scale Histogram:AUTO:3,4";
      }
      else
      {
        loggraph.graph.resize(1);
        loggraph.graph[0] = title + " Histogram:AUTO:3,4";
      }
      logGraph(where,loggraph);
    }}
  }

  af_string
  Output::warnings() const
  {
    af_string all;
    for (auto& item : WARNINGS) for (auto& line : item.second) all.push_back(line);
    return all;
  }

  af_string
  Output::advisories() const
  {
    af_string all;
    for (auto& item : ADVISORIES) for (auto& line : item.second) all.push_back(line);
    return all;
  }

  void
  Output::put_advisory(std::string item)
  {
    hoist::replace_all(item,"_"," ");
    ADVISORIES[imode].push_back(item);
  }
  void
  Output::put_warning(std::string item)
  {
    hoist::replace_all(item,"_"," ");
    WARNINGS[imode].push_back(item);
  }

  af_string
  Output::loggraphs() const
  {
    af_string all;
    for (auto& item : LOGGRAPHS) for (auto& line : item.second) all.push_back(line);
    return all;
  }

  af_string
  Output::tables() const
  {
    af_string all;
    for (auto& item : TABLES) for (auto& line : item.second) all.push_back(line);
    return all;
  }

  void
  Output::clear_memory()
  {
    WARNINGS.clear();
    ADVISORIES.clear();
    LOGGRAPHS.clear();
    TABLES.clear();
    STREAM_STRING.clear();
    last_blank_line = false;
    //LOOPS.clear(); don't clear this, as it tracks the python code externally
  }

  std::string
  Output::TimeStamp()
  {
    time_t start_time;
    std::time(&start_time);
    std::string ct = std::ctime(&start_time);
    ct.pop_back(); //remove final \n
    std::string ts = "";
    for (auto ch : ct)
      if (ch != ' ') ts += ch;
      else ts += '_';
    return ts;
  }
}//namespace phasertng
