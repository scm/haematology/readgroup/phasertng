//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/principal_statistics.h>
#include <phasertng/main/Assert.h>

namespace phasertng {

  void
  principal_statistics::set(af_dvect3 cw)
  {
    if (!cw.size()) return;
    const double& fltmax = std::numeric_limits<double>::max();
    const double& fltmin = std::numeric_limits<double>::lowest();
    MINBOX = dvect3(fltmax,fltmax,fltmax);
    MAXBOX = dvect3(fltmin,fltmin,fltmin);
    CENTRE = dvect3(0,0,0);
    double total_weight(0);
    for (int a = 0; a < cw.size(); a++)
    {
      for (int i = 0; i < 3; i++)
      {
        MINBOX[i] = std::min(MINBOX[i], cw[a][i]);
        MAXBOX[i] = std::max(MAXBOX[i], cw[a][i]);
        CENTRE[i] += cw[a][i];
      }
      total_weight++;
    }
    for (int i = 0; i < 3; i++)
    {
      MAXBOX[i] = std::max(MAXBOX[i], 1.0);
    }
    EXTENT = MAXBOX - MINBOX;
    PHASER_ASSERT(total_weight);
    for (int i = 0; i < 3; i++)
      CENTRE[i] /= total_weight;
    MEAN_RADIUS = MAX_RADIUS = 0;
    for (int a = 0; a < cw.size(); a++)
    {
      dvect3 offset = cw[a] - CENTRE;
      double radius = std::sqrt(offset*offset);
      MAX_RADIUS = std::max(MAX_RADIUS,radius);
    }
    // Find radius that encloses most (90%) of the atoms
    // First find maximum distance from centre, then make histogram
    double target_fraction = 0.9;
    int nhist(500);
    af_int disthist(nhist,0);
    double ddist = MAX_RADIUS / nhist;
    for (int a = 0; a < cw.size(); a++)
    {
      dvect3 offset = cw[a] - CENTRE;
      double radius = std::sqrt(offset*offset);
      int hindex = std::min(int(std::floor(radius/ddist)),nhist-1);
      disthist[hindex]++;
    }
    // Finally, find radius enclosing target fraction of atoms
    int ntarget = std::floor(target_fraction * cw.size());
    int ninsphere(0);
    for (int hindex = 0; hindex < nhist; hindex++)
    {
      ninsphere += disthist[hindex];
      MEAN_RADIUS = (hindex+1) * ddist;
      if (ninsphere >= ntarget)
        break;
    }

    // Old method
    // for (int i = 0; i < 3; i++)
    // {
    //   double radius = EXTENT[i]/2.0;
    //   MEAN_RADIUS += radius/3.0; //average, divide by 3
    //   MAX_RADIUS = std::max(MAX_RADIUS,radius);
    // }
    dvect3 com_extent(0,0,0);
    for (int a = 0; a < cw.size(); a++)
    {
      for (int i = 0; i < 3; i++)
      {

        com_extent[i] += std::fabs(cw[a][i]-CENTRE[i])/cw.size(); //average, divide by 3
      }
    }
    EXTENT_RATIO = com_extent.min() ? com_extent.max()/com_extent.min() : 0;
  }

  void
  principal_statistics::set_atom()
  {
    MEAN_RADIUS = 1;
    MAX_RADIUS = 1;
    MINBOX = dvect3(-1,-1,-1);
    MAXBOX = dvect3(1,1,1);
    EXTENT = dvect3(2,2,2);
    CENTRE = dvect3(0,0,0);
  }

} //phasertng
