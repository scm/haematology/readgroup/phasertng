//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/SigmaN.h>
#include <scitbx/constants.h>
#include <phasertng/main/constants.h>
#include <phasertng/main/jiffy.h>

namespace phasertng {

  double
  SigmaN::AnisoTerm(const millnx& miller,bool RemoveIsoB) const
  {
    double AnisoExp(0);
    auto anisobeta =  RemoveIsoB ? AnisoBetaRemoveIsoB() : ANISOBETA;
    AnisoExp -= anisobeta[0]*miller[0]*miller[0]; //hh
    AnisoExp -= anisobeta[1]*miller[1]*miller[1]; //kk
    AnisoExp -= anisobeta[2]*miller[2]*miller[2]; //ll
    AnisoExp -= anisobeta[3]*miller[0]*miller[1]; //hk
    AnisoExp -= anisobeta[4]*miller[0]*miller[2]; //hl
    AnisoExp -= anisobeta[5]*miller[1]*miller[2]; //kl
    return std::exp(AnisoExp);
  }

  cctbx::adptbx::eigensystem<double>
  SigmaN::EigenSystem() const
  {
// Get principal components of thermal motion tensor
    scitbx::mat3<double> betaAsMatrix(
        ANISOBETA[0],  ANISOBETA[3]/2,ANISOBETA[4]/2,
        ANISOBETA[3]/2,ANISOBETA[1],  ANISOBETA[5]/2,
        ANISOBETA[4]/2,ANISOBETA[5]/2,ANISOBETA[2]);
// See Int Tab IV, p315.  Factor of 4 for 1/4 term in B-factor expression
    scitbx::mat3<double> orthTensor = 4.0*UC.orthogonalization_matrix()*betaAsMatrix*UC.orthogonalization_matrix().transpose();
    dmat6 sym_tensor(orthTensor,DEF_PPT);
    cctbx::adptbx::eigensystem<double> eigenBs(sym_tensor);
    return eigenBs;
  }

  dvect3
  SigmaN::EigenBs() const
  {
    double isotropicB_I = IsoB_I();
    double tol(1.0e-06);
    dvect3 eigenBvals = EigenSystem().values();
    for (unsigned n = 0; n < 3; n++)
    {
      eigenBvals[n] = (eigenBvals[n] - isotropicB_I)/2;
      if (std::fabs(eigenBvals[n]) < tol) eigenBvals[n] = 0;
    }
    return eigenBvals;
  }

  double
  SigmaN::AnisoDeltaB() const
  {
    dvect3 eigenBvals = EigenBs();
    double minB(std::min(eigenBvals[0],std::min(eigenBvals[1],eigenBvals[2])));
    double maxB(std::max(eigenBvals[0],std::max(eigenBvals[1],eigenBvals[2])));
    return maxB-minB;
  }

  double
  SigmaN::IsoB_I() const
  {
//  Determine isotropic component of anisoB for I
    return AnisoBeta2IsoB();
  }

  double
  SigmaN::IsoB_F() const
  {
//  Determine isotropic component of anisoB for F
    double isotropicB_I = AnisoBeta2IsoB();
    return isotropicB_I/2.0; //value on F's
  }

  //-------------private functions

  //convert internal values of AnisoBeta to anisotropic Bs or Us
  dmat6
  SigmaN::AnisoBeta2AnisoB() const
  {
  /*
    The anisotropic beta is applied as
      exp( -(beta11*h*h + beta22*k*k + beta33*l*l + beta12*h*k + beta13*h*l + beta23*k*l) )

    The anisotropic B is applied as
      exp( -(1/4)*(B11*h*h*astar*astar + B22*k*k*bstar*bstar + B33*l*l*cstar*cstar +
                 2*B12*h*k*astar*bstar + 2*B13*h*l*astar*cstar + 2*B23*k*l*bstar*cstar) )
  */
    dmat6 anisoB;
    anisoB[0] = 4.*ANISOBETA[0]/(UC.aStar()*UC.aStar());
    anisoB[1] = 4.*ANISOBETA[1]/(UC.bStar()*UC.bStar());
    anisoB[2] = 4.*ANISOBETA[2]/(UC.cStar()*UC.cStar());
    anisoB[3] = 4.*ANISOBETA[3]/(2.0*UC.aStar()*UC.bStar());
    anisoB[4] = 4.*ANISOBETA[4]/(2.0*UC.aStar()*UC.cStar());
    anisoB[5] = 4.*ANISOBETA[5]/(2.0*UC.bStar()*UC.cStar());
    return anisoB;
  }

  dmat6
  SigmaN::AnisoBeta2AnisoU() const
  {
  /*
    The anisotropic beta is applied as
      exp( -(beta11*h*h + beta22*k*k + beta33*l*l + beta12*h*k + beta13*h*l + beta23*k*l) )

    The anisotropic U is applied as
      exp(-(2*pi*pi)*( U11*h*h*astar*astar + U22*k*k*bstar*bstar + U33*l*l*cstar*cstar +
                     2*U23*h*k*astar*bstar + 2*U13*h*l*astar*cstar + 2*U23*k*l*bstar*cstar ))

    So the U values of the anisotropy are
    U11 = beta11/(2*pi*pi*  astar*astar)
    U22 = beta22/(2*pi*pi*  bstar*bstar)
    U33 = beta33/(2*pi*pi*  cstar*cstar)
    U12 = beta12/(2*pi*pi*2*astar*bstar)
    U13 = beta13/(2*pi*pi*2*astar*cstar)
    U23 = beta23/(2*pi*pi*2*bstar*cstar)
  */
    dmat6 anisoU;
    anisoU[0] = ANISOBETA[0]/(scitbx::constants::two_pi_sq*UC.aStar()*UC.aStar());
    anisoU[1] = ANISOBETA[1]/(scitbx::constants::two_pi_sq*UC.bStar()*UC.bStar());
    anisoU[2] = ANISOBETA[2]/(scitbx::constants::two_pi_sq*UC.cStar()*UC.cStar());
    anisoU[3] = ANISOBETA[3]/(scitbx::constants::two_pi_sq*2.0*UC.aStar()*UC.bStar());
    anisoU[4] = ANISOBETA[4]/(scitbx::constants::two_pi_sq*2.0*UC.aStar()*UC.cStar());
    anisoU[5] = ANISOBETA[5]/(scitbx::constants::two_pi_sq*2.0*UC.bStar()*UC.cStar());
    return anisoU;
  }

  double
  SigmaN::AnisoBeta2IsoB() const
  {
  /*
    Use the equations for equivalent isotropic displacement
    parameters in Trueblood et al (Acta Cryst. A52, 770-781, 1996).
    Note that our off-diagonal beta elements are multiplied by two
    compared to the ones they use (for computational convenience in
    computing the anisotropy term).

    Ueq = 1/(6*pi*pi)*(beta11*a.a + beta22*b.b + beta33*c.c +
                       beta12*a.b.cosGamma + beta13*a.c.cosBeta + beta23*b.c.cosAlpha)
    Since Beq = 8*pi*pi*Ueq,
    Beq = (4/3)*(beta11*a.a + beta22*b.b + beta33*c.c +
                 beta12*a.b.cosGamma + beta13*a.c.cosBeta + beta23*b.c.cosAlpha)
  */
    return (4.0/3.0)*(ANISOBETA[0]*(UC.A()*UC.A())+
                      ANISOBETA[1]*(UC.B()*UC.B())+
                      ANISOBETA[2]*(UC.C()*UC.C())+
                      ANISOBETA[3]*(UC.A()*UC.B()*UC.cosGamma())+
                      ANISOBETA[4]*(UC.A()*UC.C()*UC.cosBeta())+
                      ANISOBETA[5]*(UC.B()*UC.C()*UC.cosAlpha()));
  }

  dmat6
  SigmaN::AnisoBeta2IsoBCoeffs() const
  {
    return dmat6(
               (4./3.)*UC.A()*UC.A(),
               (4./3.)*UC.B()*UC.B(),
               (4./3.)*UC.C()*UC.C(),
               (4./3.)*UC.A()*UC.B()*UC.cosGamma(),
               (4./3.)*UC.A()*UC.C()*UC.cosBeta(),
               (4./3.)*UC.B()*UC.C()*UC.cosAlpha() );
  }

  dmat6
  SigmaN::LargeShiftCoeffs() const
  {
    const double QUARTER(0.25);
    return dmat6(
               QUARTER*UC.aStar()*UC.aStar(),
               QUARTER*UC.bStar()*UC.bStar(),
               QUARTER*UC.cStar()*UC.cStar(),
               QUARTER*UC.aStar()*UC.bStar(),
               QUARTER*UC.aStar()*UC.cStar(),
               QUARTER*UC.bStar()*UC.cStar());
  }

  dmat6
  SigmaN::AnisoBetaRemoveIsoB() const
  {
    double isoB = AnisoBeta2IsoB();
    dmat6 isoBasAnisoBeta = IsoB2AnisoBeta(isoB);
    dmat6 anisoBeta = ANISOBETA;
    for (unsigned n = 0; n < 6; n++)
      anisoBeta[n] -= isoBasAnisoBeta[n];
    return anisoBeta;
  }

  // Reverse transformations using definitions in AnisoBeta2AnisoUorB.cc

  void
  SigmaN::AnisoU2AnisoBeta(const dmat6& anisoU)
  {
    ANISOBETA[0] = anisoU[0]*(scitbx::constants::two_pi_sq*UC.aStar()*UC.aStar());
    ANISOBETA[1] = anisoU[1]*(scitbx::constants::two_pi_sq*UC.bStar()*UC.bStar());
    ANISOBETA[2] = anisoU[2]*(scitbx::constants::two_pi_sq*UC.cStar()*UC.cStar());
    ANISOBETA[3] = anisoU[3]*(scitbx::constants::two_pi_sq*2.0*UC.aStar()*UC.bStar());
    ANISOBETA[4] = anisoU[4]*(scitbx::constants::two_pi_sq*2.0*UC.aStar()*UC.cStar());
    ANISOBETA[5] = anisoU[5]*(scitbx::constants::two_pi_sq*2.0*UC.bStar()*UC.cStar());
  }

  void
  SigmaN::AnisoB2AnisoBeta(const dmat6& anisoB)
  {
    ANISOBETA[0] = anisoB[0]*UC.aStar()*UC.aStar()/4.;
    ANISOBETA[1] = anisoB[1]*UC.bStar()*UC.bStar()/4.;
    ANISOBETA[2] = anisoB[2]*UC.cStar()*UC.cStar()/4.;
    ANISOBETA[3] = anisoB[3]*2.0*UC.aStar()*UC.bStar()/4.;
    ANISOBETA[4] = anisoB[4]*2.0*UC.aStar()*UC.cStar()/4.;
    ANISOBETA[5] = anisoB[5]*2.0*UC.bStar()*UC.cStar()/4.;
  }

  dmat6
  SigmaN::IsoB2AnisoBeta(const double isoB) const
  {
  /*
    The isotropic B-factor can be expressed in terms of hkl as:
      exp(-B/(4*d*d))  = exp(-(1/4)*B*dstar*dstar) =
      exp(-(1/4)*(B*h*h*astar*astar +
                  B*k*k*bstar*bstar +
                  B*l*l*cstar*cstar +
                2*B*h*k*astar*bstar*cosGammaStar +
                2*B*h*l*astar*cstar*cosBetaStar  +
                2*B*k*l*bstar*cstar*cosAlphaStar))

    The anisotropic beta is applied as
    exp( -(beta11*h*h +
           beta22*k*k +
           beta33*l*l +
           beta12*h*k +
           beta13*h*l +
           beta23*k*l) )

    So the stored values of the anisotropy are
    ANISOBETA[0] = beta11 = (1/4)  *B*astar*astar
    ANISOBETA[1] = beta22 = (1/4)  *B*bstar*bstar
    ANISOBETA[2] = beta33 = (1/4)  *B*cstar*cstar
    ANISOBETA[3] = beta12 = (1/4)*2*B*astar*bstar*cosGammaStar
    ANISOBETA[4] = beta13 = (1/4)*2*B*astar*cstar*cosBetaStar
    ANISOBETA[5] = beta23 = (1/4)*2*B*bstar*cstar*cosAlphaStar
  */
    double QUARTER(0.25);
    dmat6 anisoBeta;
    anisoBeta[0] = QUARTER   *isoB*UC.aStar()*UC.aStar();
    anisoBeta[1] = QUARTER   *isoB*UC.bStar()*UC.bStar();
    anisoBeta[2] = QUARTER   *isoB*UC.cStar()*UC.cStar();
    anisoBeta[3] = QUARTER*2.*isoB*UC.aStar()*UC.bStar()*UC.cosGammaStar();
    anisoBeta[4] = QUARTER*2.*isoB*UC.aStar()*UC.cStar()*UC.cosBetaStar();
    anisoBeta[5] = QUARTER*2.*isoB*UC.bStar()*UC.cStar()*UC.cosAlphaStar();
    return anisoBeta;
  }

  af_string
  SigmaN::logAnisotropy(std::string description) const
  {
    af_string output;
    output.push_back(">>SigmaN Anisotropy Parameters: " + description + "<<");
    //relative output, two levels up from request
    output.push_back("Anisotropy parameters are applied to output bins as");
    output.push_back("exp(-(betaHH.HH + betaKK.KK + betaLL.LL + betaHK.HK + betaHL.HL + betaKL.KL))");
    output.push_back("exp(-2*pi*pi*(UHH.HH.a*a* + UKK.KK.b*b* + ULL.LL.c*c* + 2UHK.HK.a*b* + 2UHL.HL.a*c* + 2UKL.KL.b*c*))");
    output.push_back("exp(-(1/4)*(BHH.HH.a*a* + BKK.KK.b*b* + BLL.LL.c*c* + 2BHK.HK.a*b* + 2BHL.HL.a*c* + 2BKL.KL.b*c*))");
    output.push_back("Refmac5 Bij applies to amplitudes not intensities, factor of 2");
    output.push_back("exp(-2*(B11.HH.a*a* + B22.KK.b*b* + B33.LL.c*c* + 2B12.HK.a*b* + 2B13.HL.a*c* + 2B23.KL.b*c*))");
    output.push_back(snprintftos(
        "%-24s %6.3f",
        "Overall Isotropic B-factor:", IsoB_I()/2));
    output.push_back("Anisotropy parameters:");
    output.push_back(snprintftos(
        "   %6s    %6s    %6s    %6s    %6s    %6s",
        "betaHH","betaKK","betaLL","betaHK","betaHL","betaKL"));
    output.push_back(snprintftos(
        "  %9.6f %9.6f %9.6f %9.6f %9.6f %9.6f",
        ANISOBETA[0],ANISOBETA[1],ANISOBETA[2],ANISOBETA[3],ANISOBETA[4],ANISOBETA[5]));
    {
      dmat6 anisoU = AnisoBeta2AnisoU();
      dmat6 anisoB = AnisoBeta2AnisoB();
      output.push_back("Alternative conventions");
      output.push_back(snprintftos(
          "%5s %-8s (%-9s) (%-9s) (%-9s)",
          "Aniso","beta","U","B","Refmac5 B"));
      for (int n = 0; n < 6; n++)
      {
        output.push_back(snprintftos(
            "%3d %9.6f (%9.6f) (%9.5f) (%9.5f)",
            n,ANISOBETA[n],anisoU[n],anisoB[n],anisoB[n]/8.0));
      }
    }
    output.push_back("");
    return output;
  }

  af_string
  SigmaN::logScaling(std::string description) const
  {
    af_string output;
    output.push_back(">>SigmaN Scaling Parameters: " + description + "<<");
    {
      output.push_back("Principal components of anisotropic part of B affecting observed amplitudes:");
      output.push_back("direction cosines (orthogonal coordinates)   eigenB (A^2)");
      dvect3 eigenBvals = EigenBs();  // Eigenvalues for anisotropic part of SigmaN.ANISO applying to F
      dmat33 direction = direction_cosines();
      for (int j = 0; j < 3; j++)
      {
        output.push_back(snprintftos(
            "%*s %7.4f  %7.4f  %7.4f %*s   %7.3f",
            9,"",direction(j,0), direction(j,1), direction(j,2),9,"",eigenBvals[j]));
      }
      output.push_back("Overall Isotropic Wilson B-factor (applied to amplitudes):   " + dtos(IsoB_F(),7,4));
      output.push_back("Anisotropic DeltaB (i.e. range of principal components):     " + dtos(AnisoDeltaB(),7,4));
      output.push_back("Resharpening B (to restore strong direction of diffraction): " + dtos(sharpening_bfactor(),7,4));
      output.push_back("Wilson K applied to observed amplitudes for absolute scale:   " + etos(wilson_k_f(),7,4));
    }
    output.push_back("");
    return output;
  }

  af_string
  SigmaN::logBinning(std::string description,af_int numinbin) const
  {
    af_string output;
    output.push_back(">>SigmaN Binning Parameters: " + description + "<<");
    {
      output.push_back("Bin Parameters:");
      for (int s = 0; s < BINSCALE.size(); s++)
        output.push_back(snprintftos(
          "#%-3d %5d %5.2fA %16.4f",
          s+1,numinbin[s],MIDRES[s],BINSCALE[s]));
      output.push_back(snprintftos(
          "Wilson scale applied to get 1 electron of scattering: %5.4e",
          1./SCALE_I));
    }
    output.push_back("");
    return output;
  }

  const double
  SigmaN::term(const millnx miller,const double ssqr,const int s) const
  { bool RemoveIsoB(false);
    return table_best.get(ssqr)*SCALE_I*BINSCALE[s]*AnisoTerm(miller,RemoveIsoB);
  }

  const double
  SigmaN::best_at_ssqr(const double ssqr) const
  {
    return table_best.get(ssqr);
  }

  double
  SigmaN::sharpening_bfactor() const
  {
    // Eigenvalues for anisotropic part of SigmaN.ANISO applying to F
    dvect3 eigenBvals = EigenBs();
    return (std::min(eigenBvals[0],std::min(eigenBvals[1],eigenBvals[2])));
  }

  double
  SigmaN::wilson_k_f() const
  { return 1./std::sqrt(SCALE_I); }

  dmat33
  SigmaN::direction_cosines() const
  {
    cctbx::adptbx::eigensystem<double> SIGMAN_eigenBs = EigenSystem();
    dmat33 direction;
    for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    {
      double dir = SIGMAN_eigenBs.vectors(i)[j];
      direction(i,j) = std::fabs(dir) < DEF_PPB ? 0 : dir;
    }
    return direction;
  }

  Loggraph
  SigmaN::logGraph1(std::string description,af_int numinbin)
  {
    Loggraph loggraph;
    loggraph.title = "SigmaN " + description;
    loggraph.scatter = false;
    loggraph.graph.resize(2);
    loggraph.data_labels = "1/d^2 Scale Reflections";
    for (int s = 0; s < BINSCALE.size(); s++)
    {
      loggraph.data_text += dtos(1./fn::pow2(MIDRES[s])) + " " +
                            dtos(BINSCALE[s]) + " " +
                            dtos(numinbin[s]) +
                            "\n";
    }
    loggraph.graph[0] = "Sigma-N Scaling vs Resolution:AUTO:1,2";
    loggraph.graph[1] = "Number of reflections per bin:AUTO:1,3";
    return loggraph;
  }
} //phasertng
