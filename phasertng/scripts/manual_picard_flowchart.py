import os, os.path
import networkx as nx

if __name__ == "__main__":

  alen= 0.1
  G = nx.DiGraph()
  G.add_edge("imtz","data",len=alen)
  G.add_edge("data","aniso",len=alen)
  G.add_edge("aniso","tncso",len=alen)
  G.add_edge("tncso","tncs",len=alen)
  G.add_edge("tncs","feff",len=alen)
  G.add_edge("feff","twin",len=alen)
  dtwin = ("D","twin?")
  G.add_edge("twin",dtwin,len=alen)
  G.add_edge(dtwin,"sga",len=alen)
  G.add_edge("sga","sglist",len=alen)
  G.add_edge(dtwin,"sglist",len=alen)
  lsg = ("D","loop sg")
  G.add_edge("sglist",lsg,len=alen)
  G.add_edge(lsg,"sgx",len=alen)
  G.add_edge("sgx","cca",len=alen)
  lae = ("D","loop ensembles")
  G.add_edge("cca",lae,len=alen)
  G.add_edge("ens",lae,len=alen)
  G.add_edge("ewa","ens",len=alen)
  G.add_edge("eba","ewa",len=alen)
  G.add_edge("eca","eba",len=alen)
  G.add_edge(lae,"eca",len=alen) #true
# G.add_edge("eca","eba",len=alen)
# G.add_edge("eba","ewa",len=alen)
# G.add_edge("ewa","ens",len=alen)
  G.add_edge(lae,"ccs",len=alen) #false
  G.add_edge("ccs","ellg",len=alen)
  deasy = ("D","easy?")
  dellg = ("D","ellg?") #reached ellg
  lnellg = ("D","loop nellg")
  G.add_edge("ellg",deasy,len=alen)
  G.add_edge(dellg,"invisible.1",len=alen) #true
  G.add_edge(deasy,"frf.1a",len=alen) #true
  G.add_edge("frf.1a","gyre.1a",len=alen)
  G.add_edge("gyre.1a","ftf.1a",len=alen)
  G.add_edge("ftf.1a","pose.1a",len=alen)
  G.add_edge("pose.1a","pak.1a",len=alen)
  G.add_edge("pak.1a","rbr.1a",len=alen)
  G.add_edge("rbr.1a",dellg,len=alen)
# G.add_edge("rbr.1a",lnellg,len=alen)
  G.add_edge(deasy,lnellg,len=alen) #false
  G.add_edge(lnellg,"frf.1b",len=alen)
  G.add_edge("frf.1b","frfr.1b",len=alen)
  G.add_edge("frfr.1b","gyre.1b",len=alen)
  G.add_edge("gyre.1b","ftf.1b",len=alen)
  G.add_edge("ftf.1b","ftfr.1b",len=alen)
  G.add_edge("ftfr.1b","pose.1b",len=alen)
  G.add_edge("pose.1b","pak.1b",len=alen)
  G.add_edge("pak.1b","rbr.1b",len=alen)
  G.add_edge("rbr.1b",lnellg,len=alen)
  G.add_edge(lnellg,dellg,len=alen)
  lnnellg = ("D","loop n-nellg")
  G.add_edge(dellg,lnnellg,len=alen) #true
  G.add_edge(dellg,'exit.1',minlen=alen*10) #true
  G.add_edge(lnnellg,"frf.2",len=alen)
  G.add_edge("frf.2","frfr.2",len=alen)
  G.add_edge("frfr.2","gyre.2",len=alen)
  G.add_edge("gyre.2","ftf.2",len=alen)
  dtfz = ("D","tfz?") #continuing high tfz
  G.add_edge("ftf.2",dtfz,len=alen)
  G.add_edge(dtfz,"pose.2",len=alen)
  G.add_edge(dtfz,"exit.2",minlen=alen*2)
  G.add_edge("pose.2","pak.2",len=alen)
  G.add_edge("pak.2","rbr.2",len=alen)
  G.add_edge("rbr.2","fuse.2",len=alen)
  fused = ("D","fused?")
  G.add_edge("fuse.2","rbr.3",len=alen)
  daf = ("D","complete?") #all found
  G.add_edge("rbr.3",daf,len=alen)
  G.add_edge(daf,lnnellg,len=alen) #false
  G.add_edge(daf,"rbm",len=alen)
  G.add_edge("rbm","solution",len=alen)
  G.add_edge("rbm",lsg,len=alen)
  G.add_edge(daf,lnnellg,len=alen)
  try:
    import matplotlib.pyplot as plt
    pydot_installed = True
  except Exception:
    pydot_installed = False
  io_pydot = True
  if io_pydot:
        plt.clf()
        color_map = []
        labeldict = {}
        size_map = []
        edge_labels = {
                       (deasy,'frf.1a'):'T',(deasy,lnellg):'F',
                       (dtfz,'pose.2'):'T',(dtfz,'exit.2'):'F',
                       (dellg,lnnellg):'T',(dellg,"exit.1"):'F',
                       (daf,lnnellg):'F',(daf,"rbm"):'T',
                      }
        start_nodes = []
        decision_nodes = []
        action_nodes = []
        invisible_nodes = []
        exit_nodes = []
        sol_nodes = []
        for node in G:
          if len(node) == 2:
            idtype,ident = node
            if idtype == 'D':
              decision_nodes.append(node)
              labeldict[node] = node[1]
          else:
            if node == 'imtz':
              start_nodes.append(node)
              labeldict[node] = "imtz"
            elif node == 'solution':
              sol_nodes.append(node)
              labeldict[node] = "solution"
            elif node.startswith("exit"):
              exit_nodes.append(node)
              labeldict[node] = "exit"
            elif node.startswith("invisible"):
              invisible_nodes.append(node)
              labeldict[node] = ""
            else:
              action_nodes.append(node)
              labeldict[node] = node.split(".")[0]
        from networkx.drawing.nx_pydot import graphviz_layout
      # pos = nx.spring_layout(G,k = 1, iterations = 500) #give a compact but readable layout
      # pos = graphviz_layout(G, prog="neato",args='-Goverlap=false -Gmode=KK') #give a compact but readable layout
      # pos = graphviz_layout(G, prog="neato") #give a compact but readable layout
      # pos = graphviz_layout(G, prog="dot") #give a compact but readable layout
        pos = nx.kamada_kawai_layout(G) #give a compact but readable layout
        fontsize=int(10) #change this to get different size fonts
      # arrowsize=fontsize/2 #change this to get different size arrows
        nodesize=400
        sz = 10
        plt.figure(figsize=(sz, 2.5*sz))
        plt.tight_layout(pad=0.05)
        plt.axis('off')
        nx.draw_networkx_labels(G,pos,
            font_size=fontsize,labels=labeldict)
        nx.draw_networkx_nodes(G, pos, nodelist=start_nodes,
            node_size=nodesize,
            node_color='skyblue',node_shape='o')
        nx.draw_networkx_nodes(G, pos, nodelist=decision_nodes,
            node_size=nodesize,
            node_color='orange',node_shape='D')
        nx.draw_networkx_nodes(G, pos, nodelist=exit_nodes,
            node_size=nodesize,
            node_color='red',node_shape='>')
        nx.draw_networkx_nodes(G, pos, nodelist=sol_nodes,
            node_size=nodesize,
            node_color='green',node_shape='>')
        nx.draw_networkx_nodes(G, pos, nodelist=action_nodes,
            node_size=nodesize,
            node_color='skyblue',node_shape='s')
        nx.draw_networkx_nodes(G, pos, nodelist=invisible_nodes,
            node_size=nodesize,
            node_color='white',node_shape='s')
        nx.draw_networkx_edge_labels(G, pos, edge_labels,
          # font_size=fontsize
            )
        nx.draw_networkx_edges(G, pos,
            min_source_margin=1,min_target_margin=1,
            arrowsize=10
           )
        filename = os.path.join("search_flowchart.png")
        plt.savefig(filename,dpi=96,bbox_inches='tight',pad_inches = 0)
        print(filename)
