//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/PattersonCoefficients.h>
#include <phasertng/data/PattersonFunction.h>
#include <phasertng/src/PeakPick.h>
#include <phasertng/math/table/cos_sin.h>
#include <phasertng/math/subgroups.h>
#include <phasertng/dag/Helper.h>
#include <phasertng/minimizer/RefineSSR.h>
#include <phasertng/dtmin/Minimizer.h>

namespace phasertng {
extern const table::cos_sin_lin_interp_table& tbl_cos_sin;
}

namespace phasertng {

  //Substructure_determination
  void Phasertng::runSSD()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->ANOMALOUS)
    throw Error(err::INPUT,"Reflections not prepared with anomalous data");
    if (DAGDB.size() > 1)
    throw Error(err::INPUT,"More than one node in directed acyclic graph");

    int cellw = REFLECTIONS->UC.width();
    int modlid_counter(1);
    Identifier modlid;
    modlid.initialize_from_text(svtos(DAGDB.WORK->logNode())+hashing());//same for same job
    modlid.initialize_tag("sad"+ntos(modlid_counter,10000)); //thrown away

    if (REFLECTIONS->check_native())
    logAdvisory("Reflections have no anomalous signal");

    //information content need to add extra columns, use flexible data structure to do this
    double sticky_hires(0);
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    //these selected are not needed because the selection is done in the phaser code
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    sticky_hires = selected.STICKY_HIRES;
    }}

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }
    if (!input.value__string.at(".phasertng.substructure.scatterer.").is_default())
    // AJM  cluster compounds not implemented
    PHASER_ASSERT(DAGDB.WORK->COMPOSITION.size());
    for (auto comp : DAGDB.WORK->COMPOSITION)
    {
      scatterers.insert(comp.first);
    }
    logTabArray(out::LOGFILE,scatterers.logScatterers("Phassade"));

    std::string atomtype = scatterers.strongest_anomalous_scatterer().first;
    double  fp = scatterers.strongest_anomalous_scatterer().second.fp();
    double  fdp = scatterers.strongest_anomalous_scatterer().second.fdp();

    dag::Entry* search;
    {{ //search will be for the strongest scatterer
      logUnderLine(out::SUMMARY,"Single Atom Search");
      auto atm = atomtype;
      atm = stoupper(atm);
      cctbx::eltbx::tiny_pse::table cctbxAtom;
      try { cctbxAtom = cctbx::eltbx::tiny_pse::table(atm); }
      catch (std::exception const& err) { throw Error(err::INPUT,"Search identifier not single atom"); }
      logTab(out::SUMMARY,"Single atom: " + atm );
      Identifier identifier; //no uuid
      identifier.initialize_tag(atm);
      search = DAGDB.lookup_entry_tag(identifier,DataBase());
      logTab(out::SUMMARY,"Search identifier: " + search->identify().str());
      if (!search->identify().is_set_valid())
        throw Error(err::INPUT,"Single atom not defined");
      if (!search->ATOM)
        throw Error(err::INPUT,"Search must be for single atom");
    }}

    {{
      //find all the spacegroups that will be used
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Space Groups");
      logTab(where,"Space groups from list of alternatives");
      std::string selection = input.value__choice.at(".phasertng.sgalt.selection.").value_or_default();
      auto spcgrps = input.value__strings.at(".phasertng.sgalt.spacegroups.").value_or_default();
      logTab(where,"Choice: " + selection);
      std::set<std::string> sglist;
      for (auto sg : spcgrps)
      {
        SpaceGroup tmp(sg);
        sglist.insert(tmp.HALL);
        logTab(out::VERBOSE,snprintftos("List %-13s  [ %-s ]",tmp.CCP4.c_str(),tmp.HALL.c_str()));
        sglist.insert(SpaceGroup(sg).HALL);
      }
      std::vector<cctbx::sgtbx::space_group_type>  sgalternative =
          sad_space_groups( REFLECTIONS->SG.type(),selection,sglist);
      //replace the DAGDB with a list that has been expanded by the spacegroup alternatives
      logTableTop(where,"Space Group Alternatives");
      for (const auto& sg : sgalternative)
      {
        SpaceGroup tmp("Hall: " + sg.hall_symbol());
        logTab(where,snprintftos("%-13s  [ %-s ]",tmp.CCP4.c_str(),tmp.HALL.c_str()));
      }
      logTableEnd(where);
      DAGDB.apply_space_group_expansion(sgalternative); //this expands the parents as well
      DAGDB.reset_entry_pointers(DataBase());
      DAGDB.restart();
      int nfinal = DAGDB.size();
      if (nfinal > 1)
        logTab(where,"Number of nodes expanded from 1 to " + itos(nfinal) + " by space group");
      else
        logTab(where,"Number of nodes not expanded by space group");
      logBlank(where);
    }}

    //now process the substructure to add fp and fdp as determined by input
    std::vector<std::set<std::string>> macrocycle(3);
    macrocycle[0] = input.value__choice.at(".phasertng.macsad.macrocycle1.").value_or_default_multi_set();
    macrocycle[1] = input.value__choice.at(".phasertng.macsad.macrocycle2.").value_or_default_multi_set();
    macrocycle[2] = input.value__choice.at(".phasertng.macsad.macrocycle3.").value_or_default_multi_set();
    for (int i = 0; i < 3 and REFLECTIONS->check_native(); i++)
      macrocycle[i].erase("sigp");
    std::vector<sv_string> macro = {
      sv_string(macrocycle[0].begin(), macrocycle[0].end()),
      sv_string(macrocycle[1].begin(), macrocycle[1].end()),
      sv_string(macrocycle[2].begin(), macrocycle[2].end())
    };
    std::vector<int> ncyc = {
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(2)
      };
    auto method = input.value__choice.at(".phasertng.macsad.minimizer.").value_or_default();

    bool extra_output = Suite::Level() >= out::VERBOSE;
    std::pair<int,int> use_fft = input.value__int_paired.at(".phasertng.substructure.test_fft_versus_summation.").value_or_default();
    sv_double wilson_llg;
    {{
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Null Hypothesis");
      logTab(out::VERBOSE,"Already scaled? " + btos(REFLECTIONS->WILSON_SCALED));
      double ztotalscat = REFLECTIONS->get_ztotalscat();
      double K = ztotalscat*REFLECTIONS->SG.SYMFAC*fn::pow2(REFLECTIONS->WILSON_K_F);
      double KI = (REFLECTIONS->WILSON_SCALED) ? 1 : K;
      REFLECTIONS->setup_absolute_scale(KI);
      logTab(where,"Scale Fs " + dtos(KI));
      logTab(where,"Wilson-B " + dtos(REFLECTIONS->WILSON_B_F));
      RefineSSR refine(
          REFLECTIONS.get(),
         // &selected,
         // &epsilon,
          &scatterers);
      if (!input.value__int_number.at(".phasertng.macsad.integration_steps.").is_default())
        refine.ISTEPS = input.value__int_number.at(".phasertng.macsad.integration_steps.").value_or_default();
      refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
      dag::Node nowork = *DAGDB.WORK;
      nowork.PART.PRESENT = false;
      nowork.FULL.PRESENT = false;
      auto txt = refine.init_node(&nowork,wilson_llg,use_fft,extra_output); //wilson_llg is null
      logTabArray(out::VERBOSE,txt.first);
      if (txt.second.size()) logAdvisory(txt.second);
      dtmin::Minimizer minimizer(this);
      minimizer.run( //null hypothesis
        refine,
        macro,
        ncyc,
        method,
        false //input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default()
        );
      auto llg = refine.likelihood();
      logTab(out::LOGFILE,"LLG for null hypothesis = " + dtos(llg));
      logBlank(out::LOGFILE);
      logTabArray(out::VERBOSE,refine.logSigmaA(true));
      refine.wilsonFn(wilson_llg);
    }}

    int nfind = input.value__int_number.at(".phasertng.substructure.determination.find.").value_or_default();
    int max_nfind = input.value__int_number.at(".phasertng.substructure.determination.maximum_find.").value_or_default();
    nfind = std::min(nfind,max_nfind);
    double zscore_cutoff = input.value__flt_number.at(".phasertng.substructure.determination.zscore.").value_or_default();
    double percent_cutoff = input.value__percent.at(".phasertng.substructure.determination.percent.").value_or_default_fraction();
    int minnum = input.value__int_number.at(".phasertng.substructure.determination.minimum_number.").value_or_default();
    double new_site_occupancy = input.value__flt_number.at(".phasertng.substructure.determination.occupancy.").value_or_default();
    //new site bfactor should be estimated from ssa
    double new_site_bfactor = input.value__flt_number.at(".phasertng.substructure.determination.bfactor.").value_or_default();
    double centrosymmetry_distance = 3.0; // fixed 3.0 is the phenix.emma default
    double close_contact_distance = 5.0; // fixed 5.0
    double target_fom = input.value__flt_number.at(".phasertng.substructure.determination.target_fom.").value_or_default();
    logUnderLine(out::LOGFILE,"Substructure Determination");
    double shannon(4);
    const double& hires = REFLECTIONS->data_hires();
    nfind == 1 ?
        logTab(out::LOGFILE,"Find 1 site per substructure"):
        logTab(out::LOGFILE,"Find " + itos(nfind) + " sites per substructure");
    bool peaks_over_deepest_hole = input.value__boolean.at(".phasertng.substructure.determination.peaks_over_deepest_hole.").value_or_default();
    //logTab(out::LOGFILE,"Patterson: Peaks over holes = " + btos(peaks_over_deepest_hole));
    logTab(out::LOGFILE,"New site occupancy = " + dtos(new_site_occupancy));
    logTab(out::LOGFILE,"New site bfactor = " + dtos(new_site_bfactor));
    logTab(out::LOGFILE,"Patterson: Zscore cutoff = " + dtos(zscore_cutoff));
    logTab(out::LOGFILE,"Patterson: Minimum number of peaks = " + itos(minnum));
    logTab(out::LOGFILE,"Centrosymmetry distance = " + dtos(centrosymmetry_distance,2));
    logTab(out::LOGFILE,"Close contact distance = " + dtos(close_contact_distance,2));
    logTab(out::LOGFILE,"Sampling: " + dtos(hires/shannon,5,2) + " Angstroms");
    logTab(out::LOGFILE,"Substructure search terminated if FOM > " + dtos(target_fom,2));

    int maxnum = input.value__int_number.at(".phasertng.substructure.determination.maximum_stored.").value_or_default();

    logBlank(out::LOGFILE);
    //make a vector of NodeList with one node per NodeList each node with one of the spacegroups
    std::vector<dag::NodeList> spacegroup_dag(DAGDB.size());
    for (int sg = 0; sg < spacegroup_dag.size(); sg++)
      spacegroup_dag[sg].push_back(DAGDB.NODES[sg]);
    dag::NodeList final_dag; //accumulates poses=f sites
    //sv_double bfac = { -0.5 };//, 0.0, +0.5};
    //sv_double occ = { 1.5 };//,  0.5};
    //for (int b = 0; b < bfac.size(); b++) //loop over the bfactors
    //for (int o = 0; o < occ.size(); o++) //loop over the occupancies
    af_string cycletable(DAGDB.size()); //sg//fin
    for (int sg = 0; sg < spacegroup_dag.size(); sg++) //loop over the spacegroups independently
    {
    //  new_site_occupancy = occ[o];
    //  new_site_bfactor = bfac[b];
    //  std::string oO = " Occupancy" +nNtos(o+1,occ.size());
    //  std::string bB = " B-factor" +nNtos(b+1,bfac.size());
    //  logTab(out::LOGFILE,"New site occupancy = " + dtos(new_site_occupancy));
    //  logTab(out::LOGFILE,"New site bfactor = " + dtos(new_site_bfactor));
      DAGDB.NODES = spacegroup_dag[sg];
      DAGDB.reset_entry_pointers(DataBase());
      DAGDB.restart();
      PHASER_ASSERT(DAGDB.size() == 1);
      std::string sS = nNtos(sg+1,spacegroup_dag.size());
     // sS +=  oO + bB;
      int f(1),F(nfind);
      for (int ifind = 0; ifind < nfind; ifind++)
      {
        bool haveFpart = ifind>0;
        DAGDB.restart();
        int n(1),N(DAGDB.size());
        std::string fF = nNtos(f++,F);
        dag::NodeList this_find_loop_dag; //accumulates poses=f sites
        auto last_find_loop_dag = DAGDB.NODES;
        std::string labelbase = "Space Group"+sS +" Find"+fF;
        for (int i = 0; i < DAGDB.size(); i++)
        {
          dag::Node* work = &DAGDB.NODES[i];
          //input dag needs to be restored at the end after DAGDB has been used for working
          std::string nN = nNtos(n++,N);
          std::string label = "Space Group"+sS +" Find"+fF+" Set"+nN;
          logUnderLine(out::LOGFILE,label);
          logTab(out::LOGFILE,"Space Group: " + work->SG->CCP4);
          bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work->SG);
          if (diffsg)
            selected.flag_standard_outliers_and_select(REFLECTIONS.get());
          //RefineSAD is a base class of RefineSSR
          //use init_node to set up the base class, then extract for Patterson
          RefineSSR refine(
              REFLECTIONS.get(),
             // &selected,
             // &epsilon,
              &scatterers);
          auto txt = refine.init_node(work,wilson_llg,use_fft,extra_output); //sigmaa etc
          logBlank(out::VERBOSE);
          logTabArray(out::VERBOSE,txt.first);
          if (txt.second.size()) logAdvisory(txt.second);
          logBlank(out::VERBOSE);

          PattersonCoefficients patterson_coefficients(
              REFLECTIONS->SG,
              REFLECTIONS->UC,
              REFLECTIONS->get_miller_array());
          double dBscat = new_site_bfactor; //tiny bit better than the average
          if (true) //amplitude target as in phaser
          {
            patterson_coefficients.LESSTF1(refine,atomtype,dBscat);
          }
          else //intensity target worse than the amplitude target (parameterization?) AJM TODO
          {
            if (!work->SADSIGMAA.in_memory())
            throw Error(err::INPUT,"No variances input");
            if (work->SADSIGMAA.size() != REFLECTIONS->numbins())
            throw Error(err::INPUT,"Input variance binning error");
            auto af_sel = refine.getSelected();
            sv_bool sv_sel(af_sel.size());
            for (int r=0;r<sv_sel.size();r++) sv_sel[r] = af_sel[r];
            patterson_coefficients.LLdLL_by_dUsqr_at_0(
              REFLECTIONS.get(),
              sv_sel,
              atomtype,fp,fdp,dBscat,work->SADSIGMAA);
          }

          bool selected_only(true);
          PattersonFunction patterson_function;
          versa_grid_double ssdmap;
          af::int3 gridding;
          std::tie(ssdmap,gridding) = patterson_function.calculate_substructure(
              REFLECTIONS->UC,
              REFLECTIONS->SG,
              haveFpart,
              refine.calcFpart(selected_only),
              patterson_coefficients.get_miller_array(),
              patterson_coefficients.get_intensity_array(),
              patterson_coefficients.LLconst()
           );

          PHASER_ASSERT(minnum >= 1);
          logEllipsisOpen(out::VERBOSE,"Fast Translation Function Peak Pick");
          PeakPick peakpick(
                     REFLECTIONS->SG,
                     REFLECTIONS->UC,
                     REFLECTIONS->data_hires(),
                     ssdmap,
                     gridding);
          peakpick.calculate(zscore_cutoff,true,haveFpart); //apply extra symmetry
          logEllipsisShut(out::VERBOSE);
          logTab(out::VERBOSE,"Peaks (from Patterson)="+itos(peakpick.SITES[pick::PEAK].size()));
          logTab(out::VERBOSE,"Holes (from Patterson)="+itos(peakpick.SITES[pick::HOLE].size()));

          int io_nprint = 20;
          logTabArray(out::VERBOSE,peakpick.logStatistics("Phassade (from Patterson)",io_nprint));
          logTabArray(out::LOGFILE,peakpick.logHistogram("Phassade"));
          (peaks_over_deepest_hole) ?
            logTab(out::LOGFILE,"Phassade sites are highest peaks height over deepest hole depth"):
            logTab(out::LOGFILE,"Phassade sites are highest peaks ignoring deepest hole");
          if (work->PART.PRESENT)
          {
            auto txt2 = peakpick.purge_close_contacts(
              work->PART.ENTRY->SUBSTRUCTURE.get_sites_as_array(),
              close_contact_distance); //close contact distance
            logTabArray(out::LOGFILE,txt2);
          }
          peakpick.reverse_sort_on_zscore_over_multiplicity(pick::PEAK);
          peakpick.reverse_sort_on_zscore_over_multiplicity(pick::HOLE);
          int num = peakpick.number_significant(
              zscore_cutoff,percent_cutoff,minnum);
          logTab(out::LOGFILE,"Number of sites from phassade map = " + itos(num));
          logBlank(out::LOGFILE);
          peakpick.purge_by_number(num);
          logTabArray(out::VERBOSE,peakpick.logStatistics("Phassade (after selection)",io_nprint));
          std::string cycletableinfo = label;
          double maxZ = (peakpick.STATS[pick::PEAK].max-peakpick.STATS[pick::PEAK].mean)/peakpick.STATS[pick::PEAK].sigma;
          double minZ = -(peakpick.STATS[pick::HOLE].max-peakpick.STATS[pick::HOLE].mean)/peakpick.STATS[pick::HOLE].sigma;
          cycletableinfo += " peak=" + dtos(maxZ,6,2)+" hole=" + dtos(minZ,6,2) + " num=" +itos(num);
          if (!num)
          {
            if (ifind > 0) //there are poses
            {
              logTab(out::LOGFILE,"No sites, revert to previous substructure(s)");
              //work node has not yet been modified
              final_dag.push_back(DAGDB.NODES[i]); //this is the end of the line
            }
            cycletable.push_back(cycletableinfo);
            continue;
          }

          //we store the peakpick information in the generic string
          //for printing after the sites have been pruned (duplicates)
          //since this is the only way to keep the nodes in sync with the peakpick
          std::string header_generic_str;
          {{
          //hack, put the sites in a dag temporarily to do the purge
          logTab(out::VERBOSE,"Create new nodes");
          dag::NodeList dag_for_duplicates;
          pick::Choice p = pick::PEAK;
          for (int s = 0; s < peakpick.SITES[p].size(); s++)
          {
            dag::Node newnode = *work; //deep copy of background
            auto& site = peakpick.SITES[p][s];
            dag::Pose newpose;
            newpose.FRACT = site.sites;
            newpose.ENTRY = search;
            newpose.IDENTIFIER = search->identify();
            newnode.POSE.push_back(newpose);
            newnode.ZSCORE = site.zscore_on_multiplicity();
            newnode.FSS = 'N'; //fast search score FSS
            newnode.LLG = site.zscore_on_multiplicity(); //check sort value, not really LLG yet
            dvect3 orth = REFLECTIONS->UC.orthogonalization_matrix()*site.sites;
            header_generic_str = snprintftos(
                "%s %s %7s%c %2s %10s %s",
                dag::header_fract().c_str(),
                dag::header_ortht(cellw).c_str(),
                "Z/M",'x',"M","Height","Element");
            newnode.generic_str = snprintftos( //original peaksite information before refinement
                "%s %s %7.2f%c %02d %10.1f %s",
                dag::print_fract(site.sites).c_str(),
                dag::print_ortht(orth,cellw).c_str(),
                site.zscore_on_multiplicity(),
                site.peak_over_deepest_hole ? ' ':'x',
                site.multiplicity,
                site.heights,
                atomtype.c_str());
            modlid.increment("sad"+ntos(modlid_counter++,10000));
            logTab(out::VERBOSE,"Identifier: " + modlid.str());
            auto entry = DAGDB.add_entry(modlid);
            newnode.PART.ENTRY = entry;
            //refine.setFixFdp()
            //PART==Substructure
            //FULL==MR solution, coordinates complete
            newnode.PART.PRESENT = true;
            newnode.PART.IDENTIFIER = entry->identify();
            auto& substructure = entry->SUBSTRUCTURE;
            substructure.SG = REFLECTIONS->SG;
            substructure.UC = REFLECTIONS->UC;
            substructure.MODLID = entry->identify();
            substructure.REFLID = REFLECTIONS->reflid();
            substructure.TIMESTAMP = TimeStamp();
            auto orthmat = REFLECTIONS->UC.orthogonalization_matrix();
            PHASER_ASSERT(newnode.POSE.size());
            for (int p = 0; p < newnode.POSE.size(); p++)
            {
              PdbRecord next_atom;
              next_atom.X = orthmat*newnode.POSE[p].FRACT;
              next_atom.O = new_site_occupancy;
              next_atom.set_element(atomtype);
              next_atom.AtomName =  " " + next_atom.get_element() + " "; // 1 or 2
              pod::xtra::scatterer next_xtra;
              substructure.AddAtom(next_atom,next_xtra);
            }
            if (!newnode.PARENTS.size())
            {
              newnode.PARENTS = {0,modlid.identifier()}; //resize and init
            }
            else
            {
              newnode.PARENTS[0] = newnode.PARENTS[1]; //front
              newnode.PARENTS[1] = modlid.identifier(); //back
            }
            dag_for_duplicates.push_back(newnode);
          }
          DAGDB.NODES = dag_for_duplicates;
          DAGDB.reset_entry_pointers(DataBase());

          out::stream where = out::LOGFILE;
          bool centrosymmetry(true);
          logEllipsisOpen(where,"Purging duplicates and centrosymmetrics");
          int dupl = DAGDB.calculate_duplicate_atoms(sticky_hires,centrosymmetry,USE_STRICTLY_NTHREADS,NTHREADS);
          (dupl == 1) ?
            logTab(where,"Duplicate (site): There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
            logTab(where,"Duplicate (site): There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
          DAGDB.NODES.erase_invalid(); //now can erase after peakpick
          cycletableinfo += (" ->unique ="+ itos(DAGDB.size()));
          dupl = DAGDB.calculate_duplicate_llg();
          (dupl == 1) ?
            logTab(where,"Duplicate (llg):  There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
            logTab(where,"Duplicate (llg):  There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
          DAGDB.NODES.erase_invalid(); //now can erase after peakpick
          cycletableinfo += " ->llg ="+itos(DAGDB.size());
          int ncent = DAGDB.find_centrosymmetry(centrosymmetry_distance);
          (ncent == 1) ?
            logTab(where,"Centrosymmetric:  There is " + itos(ncent) + " out of " + itos(DAGDB.size())):
            logTab(where,"Centrosymmetric:  There are " + itos(ncent) + " out of " + itos(DAGDB.size()));
         // DAGDB.NODES.erase_invalid(); //now can erase the centrosymmetric ones
         // cycletableinfo += "/cent->"+itos(DAGDB.size());
          logEllipsisShut(where);
          (DAGDB.size() == 1) ?
            logTab(where,"There is 1 node"):
            logTab(where,"There are " + itos(DAGDB.size()) + " nodes");
          cycletable.push_back(cycletableinfo);
          logBlank(where);
          //table of results with all atoms per node and centrosymmetry reported
          }}

          out::stream progress = out::LOGFILE;
          logBlank(progress);
          logProgressBarStart(progress,"Phassade Refinement for "+label,DAGDB.size());
          DAGDB.restart();
          int q(1),Q(DAGDB.size());
          while (!DAGDB.at_end())
          {
            auto qQ = nNtos(q++,Q);
            std::string labelpeak = label+" Peak"+qQ;
            auto& work = DAGDB.WORK;
            out::stream where = out::VERBOSE;
            RefineSSR refine(
                REFLECTIONS.get(),
               // &selected,
               // &epsilon,
                &scatterers);
            refine.ANOMALOUS = input.value__boolean.at(".phasertng.macsad.use_partial_anomalous.").value_or_default();
            refine.SPHERICITY.SIGMA = input.value__flt_number.at(".phasertng.macsad.restraint_sigma.sphericity.").value_or_default();
            refine.WILSON.SIGMA = cctbx::adptbx::b_as_u(input.value__flt_number.at(".phasertng.macsad.restraint_sigma.wilson_bfactor.").value_or_default());
            refine.FDP.SIGMA = input.value__flt_number.at(".phasertng.macsad.restraint_sigma.fdp.").value_or_default();
            if (!input.value__int_number.at(".phasertng.macsad.integration_steps.").is_default())
              refine.ISTEPS = input.value__int_number.at(".phasertng.macsad.integration_steps.").value_or_default();
            refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
            logTab(where,"--Initialize-- "+labelpeak);
            auto txt = refine.init_node(work,wilson_llg,use_fft,extra_output); //sigmaa etc
            logTabArray(where,txt.first);
            if (txt.second.size()) logAdvisory(txt.second);
            logBlank(where);
            logTabArray(where,refine.logAtoms());
            if (Suite::Extra())
            {
              out::stream where2 = out::TESTING;
              refine.target(); //must be called before likelihood to set atoms.LLG (returned)
              work->LLG = refine.likelihood();
              work->generic_float = refine.overall_fom();
              logTab(where2,"Input Log-Likelihood = " + dtos(work->LLG));
              logTab(where2,"Input Figure-of-Merit = " + dtos(work->generic_float,3));
              logBlank(where2);
            }
            logBlank(where);
            logTab(where,"--Refinement-- "+labelpeak);
            logTabArray(where,refine.logProtocolPars(Suite::Extra()));
            dtmin::Minimizer minimizer(this);
            double small_target(1.0); //for speed
            minimizer.run( //first
              refine,
              macro,
              ncyc,
              method,
              input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default(),
              small_target
              );
            if (input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default())
            {
              throw Error(err::DEVELOPER,"Study parameters: break after first refinement");
            }
            logTabArray(where,refine.logAtoms());
            logTabArray(where,refine.logFOM(false));
            work->LLG = refine.likelihood();
            work->generic_float = refine.overall_fom();
            work->generic_int = q-1;
            logTab(where,"Refined Log-Likelihood = " + dtos(work->LLG));
            logTab(where,"Refined Figure-of-Merit = " + dtos(work->generic_float,3));
            if (!logProgressBarNext(progress)) break;
          }
          logProgressBarEnd(progress);

          DAGDB.NODES.apply_sort_llg();
          if (DAGDB.size()) //purged from centrosymmetry
          {
            out::stream where = out::LOGFILE;
            int io_nprint = 20;
            logChevron(where,"Sites for Phassade "+label);
            logTab(where,"o: Centrosymmetric");
            logTab(where,"x: Peak below deepest hole");
            int num = DAGDB.size();
            logTab(where,snprintftos(
                "%-4s %4s %c %14s %5s %s",
                "#","#i",'o',"Log-Likelihood","FOM",
                header_generic_str.c_str()));
            for (int i = 0; i < DAGDB.size(); i++)
            {
              auto& node =  DAGDB.NODES[i];
              logTab(where,snprintftos(
                  "%-4d %-4d %c %14.*f %5.3f %s",
                  i+1,
                  node.generic_int, //original number
                  node.generic_flag ? ' ':'o', //true is good ie not centrosymmetric
                  (std::fabs(node.LLG)<1?6:1), node.LLG,
                  node.generic_float,
                  node.generic_str.c_str()) //all the information from the original peakpick
              );
              if (i == io_nprint)
              {
                logTab(where,"--- etc " + itos(num-io_nprint) + " more" );
                break;
              }
            }
            logBlank(where);
          }

          //output dag accumulates
          for (int i = 0; i < DAGDB.size(); i++)
            this_find_loop_dag.push_back(DAGDB.NODES[i]);
          DAGDB.NODES = last_find_loop_dag; //to reset
          DAGDB.reset_entry_pointers(DataBase());
        }

        DAGDB.NODES = this_find_loop_dag; //all the answers
        DAGDB.reset_entry_pointers(DataBase());
        if (!DAGDB.size())
        {
          logTab(out::LOGFILE,"No substructure sites for "+labelbase);
          continue;
        }
        //sort all the llg together, original order is nonsensical
        DAGDB.NODES.apply_sort_llg();

        std::string cycletableinfo;
        {{
        out::stream where = out::LOGFILE;
        bool centrosymmetry(true);
        logUnderLine(where,"Merge nodes for "+labelbase);
        cycletableinfo = labelbase + " " + itos(DAGDB.size());
        int dupl = DAGDB.calculate_duplicate_atoms(sticky_hires,centrosymmetry,USE_STRICTLY_NTHREADS,NTHREADS);
        (dupl == 1) ?
          logTab(where,"Duplicate (site): There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
          logTab(where,"Duplicate (site): There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
        DAGDB.NODES.erase_invalid(); //now can erase after peakpick
        cycletableinfo += (" ->unique ="+ itos(DAGDB.size()));
        dupl = DAGDB.calculate_duplicate_llg();
        (dupl == 1) ?
          logTab(where,"Duplicate (llg): There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
          logTab(where,"Duplicate (llg): There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
        DAGDB.NODES.erase_invalid(); //now can erase after peakpick
        cycletableinfo += (" ->llg ="+ itos(DAGDB.size()));
        //would like to do some sort of overall purge here after refinement, but no mean and sigma
        //just purge by maxnum instead to avoid explosions
        if (maxnum > 0)
        {
          logTab(out::LOGFILE,"Maximum Stored = " + itos(maxnum));
          if (maxnum < DAGDB.size())
            DAGDB.NODES.erase(DAGDB.NODES.begin()+maxnum,DAGDB.NODES.end());
        }
        (DAGDB.size() == 1) ?
            logTab(where,"There is 1 node"):
            logTab(where,"There are " + itos(DAGDB.size()) + " nodes");
        cycletableinfo += (" ->max ="+ itos(DAGDB.size()));
        cycletable.push_back(cycletableinfo);
        }}

        {{
          out::stream where = out::LOGFILE;
          logUnderLine(where,"Phassade Refinement Table ("+labelbase + ")");
          logTab(where,snprintftos("%-4s  %-13s %14s %5s  %s",
            "#","SpaceGroup","Log-Likelihood","FOM","Composition"));
          for (int i = 0; i < DAGDB.size(); i++)
          {
            dag::Node& node = DAGDB.NODES[i];
            logTab(where,snprintftos("%-4d  %-13s %14.*f %5.3f  %2s[%d]",
              i+1, //generic_int doesn't hold any sensible number
              node.SG->CCP4.c_str(),
              (std::fabs(node.LLG)<1?6:1), node.LLG,
              node.generic_float,
              node.POSE[0].ENTRY->identify().tag().c_str(),
              node.POSE.size()));
          }
          if (!DAGDB.size())
            logTab(where,"No Nodes");
          logTab(where,"-------");
        }}

        double best_fom(0);
        for (int i = 0; i < DAGDB.size(); i++)
          best_fom = std::max(best_fom,DAGDB.NODES[i].generic_float);
        logBlank(out::LOGFILE);
        if (best_fom >= target_fom)
        {
          logTab(out::LOGFILE,"Figure of merit has reached termination value: break search");
          break;
        }
        else
        {
          logTab(out::LOGFILE,"Figure of merit below termination value: continue search");
        }
      }
      for (int i = 0; i < DAGDB.size(); i++)
        final_dag.push_back(DAGDB.NODES[i]); //this is the end of the line

      //AJM TODO nposes is max each time
      {{
        for (int i = 0; i < DAGDB.size(); i++)
          DAGDB.NODES[i].generic_int = i+1;
        DAGDB.NODES.apply_sort_llg();
        out::stream where = out::LOGFILE;
        logUnderLine(where,"Phassade Refinement Table for Space Group"+sS);
        if (!DAGDB.NODES.front().POSE.size())
        {
          logTab(where,"No substructures");
        }
        else
        {
          logTab(where,snprintftos("%-4s  %-13s %14s %5s  %s",
            "#","SpaceGroup","Log-Likelihood","FOM","Composition"));
          for (auto& node : DAGDB.NODES)
          {
            logTab(where,snprintftos("%-4d  %-13s %14.*f %5.3f  %2s[%d]",
              node.generic_int,
              node.SG->CCP4.c_str(),
              (std::fabs(node.LLG)<1?6:1), node.LLG,
              node.generic_float,
              node.POSE[0].ENTRY->identify().tag().c_str(),
              node.POSE.size()));
          }
        }
        logTab(where,"-------");
      }}
    } //space groups
    DAGDB.NODES = final_dag; //all the in memory working sets
    DAGDB.reset_entry_pointers(DataBase());

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Cycle Information for Phassade");
    logTabArray(where,cycletable);
    logBlank(where);
    }}

    {{
    out::stream where = out::LOGFILE;
    DAGDB.NODES.apply_sort_llg();
    bool centrosymmetry(true);
    logUnderLine(where,"Final Merge");
    logEllipsisOpen(where,"Flagging duplicates");
    int dupl = DAGDB.calculate_duplicate_atoms(sticky_hires,centrosymmetry,USE_STRICTLY_NTHREADS,NTHREADS);
    logEllipsisShut(where);
    (dupl == 1) ?
      logTab(where,"Duplicates: There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
      logTab(where,"Duplicates: There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
    logEllipsisOpen(where,"Purge duplicates");
    int before = DAGDB.size();
    DAGDB.NODES.erase_invalid(); //now can erase after peakpick
    logEllipsisShut(where);
    logTab(where,"Number until purge: " + itos(before));
    logTab(where,"Number after purge: " + itos(DAGDB.size()));
    logBlank(where);
    }}

    {{
      for (int i = 0; i < DAGDB.size(); i++)
        DAGDB.NODES[i].generic_int = i+1;
      out::stream where = out::SUMMARY;
      logUnderLine(where,"Phassade Refinement Table Summary");
      logTab(where,snprintftos("%-4s  %-13s %14s %5s  %s",
          "#","SpaceGroup","Log-Likelihood","FOM","Composition"));
      bool found_pose(false);
      for (auto& node : DAGDB.NODES)
      {
        if (node.POSE.size())
        {
          logTab(where,snprintftos("%-4d  %-13s %14.*f %5.3f  %2s[%d]",
            node.generic_int,
            node.SG->CCP4.c_str(),
            (std::fabs(node.LLG)<1?6:1), node.LLG,
            node.generic_float,
            node.POSE[0].ENTRY->identify().tag().c_str(),
            node.POSE.size()));
          found_pose = true;
        }
      }
      if (!found_pose)
        logTab(where,"No substructures");
      logTab(where,"-------");
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);
    if (DAGDB.size() and DAGDB.NODES.front().POSE.size())
    {
      logUnderLine(out::LOGFILE,"Database Entries");
      //build a substructure for each atom
      for (int i = 0; i < DAGDB.size(); i++)
      {
        auto& substructure = DAGDB.NODES[i].PART.ENTRY->SUBSTRUCTURE;
        auto& modlid = substructure.MODLID;
        substructure.SetFileSystem(DataBase(),modlid.FileEntry(entry::SUBSTRUCTURE_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Substructure",substructure);
        if (WriteFiles()) substructure.write_to_disk();
      }
      logBlank(out::LOGFILE);
    }
  }

} //phasertng
