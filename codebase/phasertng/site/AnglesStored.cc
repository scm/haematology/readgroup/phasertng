//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/site/AnglesStored.h>
#include <phasertng/main/Error.h>
#include <scitbx/math/euler_angles.h>
#include <algorithm>
#include <numeric>

namespace phasertng {

  bool
  AnglesStored::at_end()
  { inext++; return inext != rotation_function_data.end(); }

  void
  AnglesStored::restart(unsigned seed)
  { inext = rotation_function_data.begin(); } //next is increment value

  dvect3
  AnglesStored::next_site()
  { return inext->grid; }

  std::size_t
  AnglesStored::count_sites()
  { return rotation_function_data.size(); }

  af_dvect3
  AnglesStored::all_sites()
  {
    af_dvect3 tmp(rotation_function_data.size());
    int i(0);
    for (auto& item : rotation_function_data)
      tmp[i++] = item.grid;
    return tmp;
  }

  void
  AnglesStored::partial_sort_and_erase(const int& maxstored)
  {
    int num = rotation_function_data.size();
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    if (num <= maxstored)
    {
      std::sort(
        rotation_function_data.begin(),
        rotation_function_data.end(),
        cmp);
    }
    else
    {
      std::partial_sort(
        rotation_function_data.begin(),
        rotation_function_data.begin()+num,
        rotation_function_data.end(),
        cmp);
      rotation_function_data.erase(
        rotation_function_data.begin()+num,
        rotation_function_data.end());
    }
  }

  void
  AnglesStored::full_sort()
  {
    auto cmp = []( const Site &a, const Site &b )
         { return a.value >  //> for reverse sort
                  b.value; };
    std::sort(
        rotation_function_data.begin(),
        rotation_function_data.end(),
        cmp);
  }

  void
  AnglesStored::select_topsites_and_erase()
  {
    std::vector<Site>::iterator iter = std::remove_if(
              rotation_function_data.begin(), rotation_function_data.end(),
              [](Site i){ return !i.is_topsite(); } );
    rotation_function_data.erase( iter, rotation_function_data.end() );
    for (int i = 0; i < rotation_function_data.size(); i++)
      rotation_function_data[i].clusterNum = i; //reset because old numbers not longer relevant
  }

  std::pair<double,double>
  AnglesStored::mean_sigma()
  {
    std::vector<Site>& v = rotation_function_data; //reference
    PHASER_ASSERT(v.size());
    Site sum = std::accumulate(v.begin(), v.end(), Site(0.0));
    double mean = sum.value / v.size();
    sv_double diff(v.size());
    for (int i = 0; i < v.size(); i++) diff[i] = v[i].value - mean;
    double sq_sum(0);
    for (int i = 0; i < diff.size(); i++) sq_sum += diff[i]*diff[i] ;
    double stdev = std::sqrt(sq_sum / v.size());
    return { mean , stdev };
  }

  double
  AnglesStored::top()
  {
    std::vector<Site>& v = rotation_function_data; //reference
    double top = std::numeric_limits<double>::lowest();
    for (int i = 0; i < v.size(); i++)
      top = std::max(top,v[i].value);
    return top;
  }

  Loggraph
  AnglesStored::logGraph(std::string description)
  {
    Loggraph loggraph;
    loggraph.title = "Top peaks " + description;
    loggraph.scatter = false;
    loggraph.graph.resize(1);
    loggraph.data_labels = "Peak_number  Score  Number_in_Cluster";
    int i(1);
    for (auto& item : rotation_function_data)
    {
      if (item.is_topsite())
        loggraph.data_text += std::to_string(i++) + " " +
                              std::to_string(item.value) + " " +
                              std::to_string(item.ifTopNumInGroup) + " " +
                              "\n";
    }
    loggraph.graph[0] = ":N:1,2,3";
    return loggraph;
  }

  void
  AnglesStored::allocate_memory(
      int b,
      double grid_sampling
    )
  {
    int requestRotlistSize(0);
    //for (int b = 0; b < bmax; b++)
    {
      double betaDEG = static_cast<double>(grid_sampling*b);
      double betaRAD = scitbx::deg_as_rad(betaDEG)/2.;
      int pmax = static_cast<int>(720.0/grid_sampling*std::cos(betaRAD));
      int qmax = static_cast<int>(360.0/grid_sampling*std::sin(betaRAD));
      if (b==0)
        requestRotlistSize += pmax/2;
      else if (betaDEG >= (180.-DEF_PPT))
        requestRotlistSize += qmax;
      else
        requestRotlistSize += pmax*qmax; //max possible, will be less
    }
    try
    {
      rotation_function_data.reserve(requestRotlistSize);//prevent grabbing
    }
    catch (std::exception const& err) {
      //this catches the error  St9bad_alloc where it runs out of memory
      throw Error(err::MEMORY,"Memory exhausted during fast rotation function");
    }
  }

  void
  AnglesStored::fill_memory(
      int b,
      double grid_sampling
    )
  {
    SAMP = grid_sampling;
    {
      double betaDEG = static_cast<double>(grid_sampling*b);
      double betaRAD =  scitbx::deg_as_rad(betaDEG);
      double cosbeta = std::cos(betaRAD/2.);
      double sinbeta = std::sin(betaRAD/2.);

      int pmax = static_cast<int>(720.0/grid_sampling*cosbeta);
      int qmax = static_cast<int>(360.0/grid_sampling*sinbeta);

      double alpha(0);
      double gamma(0);
      if (b==0)
      {
        for (int p = 0; p <= pmax; p++)
        {
          alpha = static_cast<double>(p)/pmax;
          gamma = alpha;
          if (alpha>=0.5) continue;
          rotation_function_data.push_back(Site(alpha,betaDEG,gamma));
        }
      }
      else if (betaDEG >= (180.-DEF_PPT))
      {
        for (int q = 0; q <= qmax; q++)
        {
          alpha = static_cast<double>(q)/qmax;
          gamma = 0;
          rotation_function_data.push_back(Site(alpha,betaDEG,gamma));
        }
      }
      else
      {
        double p_float(0);
        double epsilon(1.0e-06);
        double p_ratio(0);
        double q_float(0);
        double q_ratio(0);
        double p2_float(0);
        double p2_ratio(0);
        double q2_float(0);
        double q2_ratio(0);
        double alpha2(0);
        double gamma2(0);
        bool repeated_angle(false);
        double p2_ratio_plus_q2_ratio(0);
        double p2_ratio_minus_q2_ratio(0);
        double p_ratio_plus_q_ratio(0);
        double p_ratio_minus_q_ratio(0);
        for (int p = 0; p <= pmax; p++,p_float++)
        {
          p_ratio = p_float / pmax;
          q_float = 0;
          for (int q = 0; q <= qmax; q++,q_float++)
          {
            q_ratio = q_float / qmax;
            p_ratio_plus_q_ratio = ( p_ratio + q_ratio );
            //alpha = std::fmod( p_ratio_plus_q_ratio, 1 );
            alpha = p_ratio_plus_q_ratio - std::floor(p_ratio_plus_q_ratio);
            p_ratio_minus_q_ratio = p_ratio-q_ratio;
            if ( p_ratio_minus_q_ratio >= 0)
            {
             // gamma = std::fmod( p_ratio-q_ratio, 1 ) :
              gamma = p_ratio_minus_q_ratio - std::floor(p_ratio_minus_q_ratio);
            }
            else
            {
              p_ratio_minus_q_ratio = -p_ratio_minus_q_ratio;
              gamma = p_ratio_minus_q_ratio - std::floor(p_ratio_minus_q_ratio);
              gamma = 1.0 - gamma;
              //1.0 - std::fmod( q_ratio - p_ratio, 1 ) );
            }
         // there are cases where alpha and gamma are repeated because
         //the sum and difference of p/pmax and q/qmax are the same.
            repeated_angle = (false);
            if (p >= pmax/2)
            {
              p2_float = ( p_float-pmax/2. ); //repeats when p is +0.5
              p2_ratio =  p2_float / pmax;
              q2_float = 0;
              for (int q2 = 0; q2 <= qmax; q2++,q2_float++)
              {
                q2_ratio = q2_float / qmax;
                p2_ratio_plus_q2_ratio = ( p2_ratio + q2_ratio);
                //alpha2 = std::fmod( p2_ratio_plus_q2_ratio, 1);
                alpha2 = p2_ratio_plus_q2_ratio - std::floor(p2_ratio_plus_q2_ratio);
                p2_ratio_minus_q2_ratio = p2_ratio - q2_ratio;
                //gamma2 = ( p2_ratio >= q2_ratio ?
                if (p2_ratio_minus_q2_ratio >= 0)
                {
                  gamma2 = p2_ratio_minus_q2_ratio -std::floor(p2_ratio_minus_q2_ratio);
                //std::fmod( p2_ratio - q2_ratio, 1 ) :
                }
                else
                {
                  p2_ratio_minus_q2_ratio = -p2_ratio_minus_q2_ratio;
                  gamma2 = p2_ratio_minus_q2_ratio - std::floor(p2_ratio_minus_q2_ratio);
                  gamma2 = 1.0 - gamma2;
                  // 1.0 - std::fmod( q2_ratio - p2_ratio, 1 ) );
                }
                if ((alpha < alpha2+epsilon) and
                    (alpha > alpha2-epsilon) and
                    (gamma < gamma2+epsilon) and
                    (gamma > gamma2-epsilon))
                {
                  repeated_angle = true;
                  break;
                }
              }
            }
            if (!repeated_angle)
            {
              rotation_function_data.push_back(Site(alpha,betaDEG,gamma));
            } //repeated angle
          }
        }
      }
    }
  }

}// phasertng
