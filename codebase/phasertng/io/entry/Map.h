//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Map__class__
#define __phasertng_Map__class__
#include <phasertng/src/Bin.h>
#include <phasertng/main/Identifier.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/io/EntryBase.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/io/PdbRecord.h>
#include <phasertng/io/principal_statistics.h>
#include <phasertng/data/Header.h>
#include <tuple>

namespace phasertng {

  class Trace; //forward declaration

  class Map : public EntryBase
  {
    public: //members
      Identifier MODLID; //hash value from input
      //read only, so REFLID and TIMESTAMP not required
      af::shared<millnx> MILLER;
      af_cmplex FCALC;
      UnitCell  UC;
      SpaceGroup  SG = SpaceGroup("P 1");
      Header HEADER; //if the data come from an MTZ file then the header can be parsed

    public: //constructors
      Map() : EntryBase() {}
      af_string ReadMtz(std::string,std::string,bool,bool);
      af_string ReadMap(double,bool,std::string,Identifier);
      bool in_memory() { return FCALC.size(); }
      void read_from_disk() { PHASER_ASSERT(false); }
      void write_to_disk() { PHASER_ASSERT(false); }

    public:
      void      sort_reflections();
      void      copy_to_resolution(UnitCell,af_millnx,af_cmplex,double);
      void      apply_bfactor(double);
      void      truncate_to_resolution(double);
      bool      is_map();
      bool      is_mtz();
      std::pair<double,principal_statistics>  wang_trace(double,double,dvect3,std::vector<PdbRecord>&);
      scitbx::af::shared<double> sigmap_with_binning(Bin&);
      double    solvent_fraction(double) const;
      double    available_hires(std::pair<double,double>) const;
      double    hires() const;
      std::pair<dvect3,dvect3>  solvent_constant(af_dvect3,double);
      scitbx::af::shared<double> density(std::pair<double,double>,bool);
  };

}
#endif
