//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/io/entry/Ensemble.h>
#include <phasertng/main/Error.h>
#include <phasertng/main/hoist.h>
#include <phasertng/main/constants.h>
#include <phasertng/io/MtzHandler.h>

namespace phasertng {

  Ensemble::Ensemble(
    af_string histlines,
    af::double6 cell,
    af_millnx MILLER_,
    af_cmplex ECALC_,
    af_double SIGMAA_
    ) : MILLER(MILLER_),ECALC(ECALC_),SIGMAA(SIGMAA_)
  {
    SetHistory(histlines);
    UC = UnitCell(cell);
  }

  void
  Ensemble::initialize_bin(Bin BIN_)
  {
    PHASER_ASSERT(!UC.is_default());
    BIN = BIN_;
    BIN.RBIN.resize(MILLER.size());
    SSQR.resize(MILLER.size());
    std::string errors("");
    for (int r = 0; r < MILLER.size(); r++)
    {
      double s = UC.cctbxUC.two_stol(MILLER[r]);
      //these may be outside the resolution range of the new binning
      //don't check to see if it is valid at the low resolution end
      //just take bin 0
      double reso = UC.cctbxUC.d(MILLER[r]);
      if (reso < (BIN.hires()-DEF_PPT))
        errors += "\nBinning outside range " + dtos(reso) + "<" + dtos(BIN.hires()-DEF_PPT);
      BIN.RBIN[r] = BIN.get_bin_s(s);
      SSQR[r] = UC.cctbxUC.d_star_sq(MILLER[r]);
    }
    if (errors.size())
      throw Error(err::DEVELOPER,errors);
    SIGMAA.resize(BIN.numbins());
  }

  const double
  Ensemble::hires() const
  {
    PHASER_ASSERT(!UC.is_default());
    double hi = DEF_LORES;
    for (int r = 0; r < MILLER.size(); r++)
    {
      double reso = UC.cctbxUC.d(MILLER[r]);
      hi = std::min(hi,reso);
    }
    return hi;
  }

  void
  Ensemble::addData(
      int r,
      cmplex E,
      double V,
      int S
    )
  {
    phaser_assert(r < ECALC.size());
    ECALC[r] = E;
    phaser_assert(S < SIGMAA.size());
    SIGMAA[S] = V;
  }

  void
  Ensemble::VanillaReadMtz()
  {
    int read_reflections(0);
    MtzHandler mtzIn; mtzIn.read(*this,read_reflections);
    MILLER.resize(mtzIn.NREFL); //so that setup_data_present_init sets array sizes
    //SPACEGROUP
    { //HISTORY
      SetHistory(mtzIn.HISTORY); //the binning is set here
    } //HISTORY
    { //UNITCELL
      mtzIn.load_dataset("");
      UC =  mtzIn.UC;
    } //UNITCELL
    std::vector<af_double> Weights;
    { //DATA
      int ncols = 7; //HKL
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      sv_int logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      lookup[0] = mtzIn.getCol("H");
      lookup[1] = mtzIn.getCol("K");
      lookup[2] = mtzIn.getCol("L");
      lookup[3] = mtzIn.getCol("BIN");
      lookup[4] = mtzIn.getCol("EC");
      lookup[5] = mtzIn.getCol("PHIC");
      lookup[6] = mtzIn.getCol("SIGA");
      mtzIn.resetRefl();
      MILLER.resize(mtzIn.NREFL);
      ECALC.resize(mtzIn.NREFL);
      SIGMAA.resize(BIN.numbins());
      BIN.RBIN.resize(mtzIn.NREFL);
      BIN.NUMINBIN.resize(BIN.numbins());
      std::fill(BIN.NUMINBIN.begin(),BIN.NUMINBIN.end(),0);
      for (int mtzr = 0; mtzr < mtzIn.NREFL; mtzr++)
      {
        int failure = mtzIn.readRefl(adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H(adata[0]);
        int K(adata[1]);
        int L(adata[2]);
        int ibin = adata[3];
        MILLER[mtzr] = millnx(H,K,L);;
        ECALC[mtzr] = std::polar(static_cast<double>(adata[4]),
                                 static_cast<double>(scitbx::deg_as_rad(adata[5])));
        SIGMAA[ibin] = adata[6];
        BIN.RBIN[mtzr] = ibin;
        BIN.NUMINBIN[ibin]++;
      }
    } //DATA
    SSQR.resize(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
      SSQR[r] = UC.cctbxUC.d_star_sq(MILLER[r]);
  }

  void
  Ensemble::VanillaWriteMtz()
  {
    PHASER_ASSERT(MODLID.is_set_valid());
    PHASER_ASSERT(REFLID.is_set_valid());

    SpaceGroup SG;
    MtzHandler mtzOut; mtzOut.open(*this,SG,UC,1.0,MILLER.size(),get_history());
    CMtz::MTZCOL* mtzE  = mtzOut.addCol("EC",'F');
    CMtz::MTZCOL* mtzP  = mtzOut.addCol("PHIC",'P');
    CMtz::MTZCOL* mtzV  = mtzOut.addCol("SIGA",'W');
    CMtz::MTZCOL* mtzB  = mtzOut.addCol("BIN",'I');
    for (int r = 0; r < MILLER.size(); r++)
    {
      phaser_assert(r < BIN.RBIN.size());
      int ibin = BIN.RBIN[r];
      mtzOut.mtzH->ref[r] = MILLER[r][0];
      mtzOut.mtzK->ref[r] = MILLER[r][1];
      mtzOut.mtzL->ref[r] = MILLER[r][2];
      mtzE->ref[r] = std::abs(ECALC[r]);
      mtzP->ref[r] = scitbx::rad_as_deg(std::arg(ECALC[r]));
      mtzV->ref[r] = SIGMAA.size() ? SIGMAA[ibin] : 0;
      mtzB->ref[r] = ibin;
    }
    mtzOut.Put("Phasertng Model");
  }

  af_string
  Ensemble::get_history()
  {
    af_string histlines;
    histlines.push_back("REFLID " + REFLID.string() + " " + REFLID.tag());
    histlines.push_back("MODLID " + MODLID.string() + " " + MODLID.tag());
    histlines.push_back("SPHERE " + dtos(SPHERE));
    histlines.push_back("BINS " + BIN.unparse());
    return histlines;
  }

  void
  Ensemble::SetHistory(af_string histlines)
  {
    for (auto line : histlines)
    {
      sv_string token;
      hoist::algorithm::split(token,line," ");
      if (!line.find("MODLID"))
      {
        phaser_assert(token.size() == 3);
        MODLID.parse_string(token[1],token[2]);
      }
      if (!line.find("REFLID"))
      {
        phaser_assert(token.size() == 3);
        REFLID.parse_string(token[1],token[2]);
      }
      if (!line.find("TIMESTAMP"))
      {
        phaser_assert(token.size() == 2);
        TIMESTAMP = token[1];
      }
      if (!line.find("SPHERE"))
      {
        phaser_assert(token.size() == 2);
        SPHERE = std::atof(token[1].c_str());
      }
      if (!line.find("BINS"))
      {
        phaser_assert(token.size() == 5);
        BIN.parse(std::atof(token[1].c_str()),
                  std::atof(token[2].c_str()),
                  std::atof(token[3].c_str()),
                  std::atoi(token[4].c_str()));
      }
    }
  }

  void
  Ensemble::normalize()
  {
    //generate binning just for the normalization
    af_double twostol(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
      twostol[r] = UC.cctbxUC.two_stol(MILLER[r]);
    Bin bin(6,200,1000,twostol);
    sv_double SigmaP(bin.numbins(),0);
    phaser_assert(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
    {
      double reso = UC.cctbxUC.d(MILLER[r]);
      if (reso < bin.lores() and reso > bin.hires())
      {
        // int ibin(0); bool error(false);
        //std::tie(ibin,error) = bin.get_bin_s_and_valid(UC.cctbxUC.two_stol(MILLER[r]));
        //the binning may not extend to the full resolution of the map
        //phaser_assert(error == 0);
        int ibin = bin.RBIN[r];
        SigmaP[ibin] += std::norm(ECALC[r]);
        //P1, no weight, no centrics
      }
    }
    for (int ibin = 0; ibin < bin.numbins(); ibin++)
    {
      SigmaP[ibin] /= bin.NUMINBIN[ibin];
    }
    for (int r = 0; r < ECALC.size(); r++)
    {
     // int ibin(0); bool valid(false);
     // std::tie(ibin,valid) = bin.get_bin_s_and_valid(UC.cctbxUC.two_stol(MILLER[r]));
      int ibin = bin.RBIN[r];
      ECALC[r] /= std::sqrt(SigmaP[ibin]);
    }
    bool do_check(true);
    if (do_check)
    {
      sv_double evalues(bin.numbins(),0);
      for (int r = 0; r < MILLER.size(); r++)
      {
        int ibin = bin.RBIN[r];
        evalues[ibin] += std::norm(ECALC[r]);
      }
      for (int ibin = 0; ibin < evalues.size(); ibin++)
      {
        evalues[ibin] /= bin.NUMINBIN[ibin];
        PHASER_ASSERT(evalues[ibin] > 1-DEF_PPM and evalues[ibin] < 1+DEF_PPM);
      }
    }
  }

} //phasertng
