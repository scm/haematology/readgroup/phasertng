//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/flt_number.h>
#include <sstream>
#include <iomanip>
#include <algorithm> //min max on Windows

namespace phasertng {
namespace type {

  flt_number::flt_number(
      const std::string parameter,
      const bool pminset_,const double pminval_,
      const bool pmaxset_,const double pmaxval_,
      const double dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_),
     pminset(pminset_),
     pmaxset(pmaxset_),
     pminval(pminval_),
     pmaxval(pmaxval_)
  { }

  void flt_number::set_value(double other)
  {
    user = true;
    uvalue = other;
  }

  void flt_number::set_range(double a,double b)
  {
    pminset = true;
    pmaxset = true;
    pminval = std::min(a,b);
    pmaxval = std::max(a,b);
  }

  void flt_number::set_value(const sv_string& vcards,const double other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  flt_number::is_default() const
  { return uvalue == dephalt; }

  void
  flt_number::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  flt_number::to_python()
  { return unparse_parameter(false); }

  void
  flt_number::from_python(std::string str)
  { set_value(std::stod(str)); }

  double
  flt_number::value_or_default() const
  { return user ? uvalue : dephalt; }

  double
  flt_number::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  flt_number::unparse_parameter(bool phil) const
  {
    if (value_or_default() == DEF_FLT_FLAG)
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << uvalue;
    return ss.str();
  }

  std::string
  flt_number::show_default(bool phil) const
  {
    if (dephalt == DEF_FLT_FLAG)
      return None();
    std::stringstream ss;
    ss << std::setprecision(10); //same as floats formatting in phil __init__.py
    ss << dephalt;
    return ss.str();
  }

}}
