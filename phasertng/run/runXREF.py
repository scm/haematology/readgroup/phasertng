import sys, os, os.path, copy
import argparse
from cctbx.array_family import flex
#from iotbx import extract_xtal_data
#from iotbx import pdb
#from iotbx import reflection_file_utils
import iotbx
import iotbx.phil
from iotbx.cli_parser import run_program
from iotbx.pdb import crystal_symmetry_from_pdb as from_pdb
from libtbx.utils import Sorry
from mmtbx import utils
from mmtbx.scaling import twin_analyses
import mmtbx.f_model
import mmtbx.model
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data,enum_entry_data
from phasertng.scripts.jiffy import TextIO
from phasertng.scripts.twin_results import get_twin_results
from phasertng.phil.mode_generator import isNone
from six.moves import cStringIO as StringIO
import phasertng.scripts.simple_tools as simple_tools

#these are internal parameters not interesting for the user
class runXREF:

  def get_iomtz(self,basetng,node,f_obs_label):
      DAGDB = basetng.DAGDATABASE
      try: #from density
       #subdir = node.FULL.IDENTIFIER.database_subdir()
        subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
        entfile = node.REFLID.other_data_stem_ext(enum_other_data.tiny_mtz)
        iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
        if not os.path.exists(iomtz.fstr()): #from reflections
          subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
          entfile = node.REFLID.other_data_stem_ext(enum_other_data.data_mtz)
          iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
      except Exception as e:
        try: #from reflections
          subdir = DAGDB.lookup_reflid(node.REFLID).database_subdir()
          entfile = node.REFLID.other_data_stem_ext(enum_other_data.data_mtz)
          iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
        except Exception as e: #from original data
           wf = basetng.WriteFiles()
           if not wf:
             basetng.logAdvisory("Coordinate refine requires files on disk")
             return None, None
           else:
             raise e
        if f_obs_label is None: #because we have not set explicitly eg from __main__
          f_obs_label = reflid_label #backup to the original label
      iomtz = FileSystem(basetng.DataBase(),subdir,entfile)
      return iomtz,f_obs_label


  def run_ccp4_refmac5_node(self,
                            basetng,
                            node,
                            iopdb,
                            iomtz,
                            ncyc,
                            high_resolution,
                            sg_info,
                            tncs_info):
 #{
    DAGDB = basetng.DAGDATABASE
    def fname(f):
      return FileSystem(basetng.DataBase(),subdir,str(f)).fstr()
    modlid = node.TRACKER.ULTIMATE
    subdir = DAGDB.lookup_modlid(modlid).database_subdir()
    #no option because refmac will fail without write dir
    flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
    if not flat and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
       os.makedirs(os.path.join(basetng.DataBase(),subdir))
    pdbout = modlid.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
    newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
    mtzout = modlid.other_data_stem_ext(enum_other_data.refine_mtz)
    newmtzname = FileSystem(basetng.DataBase(),subdir,mtzout)
    xyzout = newpdbname.fstr()
    mtzout = newmtzname.fstr()
    #note that the space group may be different from the one on the input mtz file
    #refmac uses the space group on the mtz file so we need to change it (SYMM keyword doesn't work)
    def change_sg_on_mtz_file(iomtz,sg_info):
    #{{{
      from iotbx import mtz
      from cctbx import sgtbx
      mtz_obj = mtz.object(iomtz)
      old_space_group = mtz_obj.crystals()[0].crystal_symmetry().space_group_info().type().universal_hermann_mauguin_symbol()
      new_space_group = sgtbx.space_group_info(sg_info).type().universal_hermann_mauguin_symbol()
      if (old_space_group == new_space_group):
        return iomtz
      mtz_new = mtz_obj.set_space_group_info(sgtbx.space_group_info(new_space_group))
      output_mtz_file = os.path.splitext(iomtz)[0]+ "." + sg_info.replace(" ","") + ".mtz"
      #file may have been written out previously
      if not os.path.exists(output_mtz_file):
        mtz_new.write(str(output_mtz_file))
      return output_mtz_file
    #}}}
    iomtz = change_sg_on_mtz_file(iomtz,sg_info)
    refmac5 = ['refmac5','HKLIN',iomtz,'HKLOUT',mtzout,'XYZIN',iopdb,'XYZOUT',xyzout]
    keytxt = str("ncyc " + str(ncyc) + "\nrefi resolution " + str(high_resolution) + " 10000")
    intxt = ["twin\n" + keytxt + "\nEND", keytxt + "\nEND"]
    header = ["REFMAC with twinning","REFMAC without twinning"]

    for itwin,command_line in enumerate([ refmac5, refmac5 ]): #previously also included freerflag preparation
      rdict = {
           "Final R-free": 100,
           "Final R-work": 100,
           "Start R-free": 100,
           "Start R-work": 100,
           "sg_info":str(sg_info),
           "tncs_info":str(tncs_info),
           "hires":str(high_resolution),
           "Twin law":"None",
           "Twin alpha":"None",
           "ident":modlid.str(),
           "PhenixResultsObject":None, }
      outstr = None #TextIO()
      error_thrown = False
      use_subprocess = True
      result = None
      try:
        basetng.logChevron(enum_out_stream.logfile,"Refinement")
        if use_subprocess:
          import subprocess
          basetng.logEllipsisOpen(enum_out_stream.logfile,"Running " + header[itwin].lower())
          basetng.logTab(enum_out_stream.logfile,str(' '.join(command_line)))
          for line in intxt[itwin].split("\n"):
            basetng.logTab(enum_out_stream.verbose,line)
          #refmac can fail ungracefully in the twinning refinement, so capture stderr also
          result = subprocess.run(command_line,input=intxt[itwin],stdout=subprocess.PIPE,stderr=subprocess.STDOUT,text=True)
        else:
          basetng.logEllipsisOpen(enum_out_stream.logfile,"Running " + header[itwin].lower())
          basetng.logTab(enum_out_stream.logfile,str(' '.join(command_line)))
          for line in intxt[itwin].split("\n"):
            basetng.logTab(enum_out_stream.verbose,line)
          run(args=command_line[1:],out=outstr)
      except Exception as e:
          message = "" if str(e) is None else str(e)
          error_thrown = True
          print("error thrown",e)
         #don't log error yet
         #raise ValueError(enum_err_code.fatal),message)
      #report log first
      basetng.logEllipsisShut(enum_out_stream.logfile)
      wf = basetng.WriteFiles()
      basetng.logFileWritten(enum_out_stream.logfile,wf,"Coordinates",newpdbname)
      basetng.logFileWritten(enum_out_stream.logfile,wf,"Density",newmtzname)
      rwork = 100
      rfree = 100
      wf = basetng.WriteFiles()
      if (outstr is not None):
        basetng.logTabArray(enum_out_stream.logfile,outstr.getvalue().split("\n"))
      if (result is not None):
        logoutstr = result.stdout.split("\n")
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        logout = node.TRACKER.ULTIMATE.extra_file_stem_ext("." +header[itwin].lower().replace(" ","_")+ ".log")
        flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
        if not flat and wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
          os.makedirs(os.path.join(basetng.DataBase(),subdir))
        newlogname = FileSystem(basetng.DataBase(),subdir,logout)
        basetng.logFileWritten(enum_out_stream.logfile,wf,header[itwin].title().replace(" ","_"),newlogname)
        with open(newlogname.fstr(), "w") as f:
                f.write("PHASERTNG " + header[itwin].upper() +"\n")
                f.write("--------------------\n")
                f.write("\n".join(logoutstr))
        if wf and "REFMAC" in header[itwin]:
            rw_final = 100
            rw_start = 100
            rf_final = 100
            rf_start = 100
            for line in logoutstr[-15:-9]:
              if ("R factor" in line): #find the parameters
                try: rw_final = float(line.split()[-1])
                except: pass
                try: rw_start = float(line.split()[-2])
                except: pass
              if ("R free" in line): #find the parameters
                try: rf_final = float(line.split()[-1])
                except: pass
                try: rf_start = float(line.split()[-2])
                except: pass
            anyerror = False
            for line in logoutstr[-15:]:
              if "Error" in line or "error" in line or "IERR" in line: #refmac failed, most likely due to twinning eg 1gus 3mpw
                anyerror = True
                if (itwin == 0):
                  basetng.logWarning("Refmac failed with twin refinement")
                if (itwin == 1):
                  basetng.logWarning("Refmac failed without twin refinement")
            if "NaN" in result.stdout: #refmac failed, most likely due to twinning eg 1gus 3mpw
              anyerror = True
              if (itwin == 0):
                basetng.logWarning("Refmac failed with twin refinement")
              if (itwin == 1):
                basetng.logWarning("Refmac failed without twin refinement")
          # for line in logoutstr: ##improve TODO
          #     basetng.logTab(enum_out_stream.logfile,line)
            if anyerror:
              rdict = {
         "Final R-free": "NaN",
         "Final R-work": "NaN",
         "Start R-free": "NaN",
         "Start R-work": "NaN",
         "sg_info":str(sg_info),
         "tncs_info":str(tncs_info),
         "hires":str(high_resolution),
         "Twin law":"None",
         "Twin alpha": "NaN",
         "ident":"", #it failed
         "PhenixResultsObject":None, #the files will be written from this result
         "newpdbname" : None, #only in refmac5 rdict
         "newmtzname" : None, #only in refmac5 rdict
         }
              continue # to without twinning
            for line in logoutstr[-15:-9]:
              basetng.logTab(enum_out_stream.logfile,line) #the table at the end of the logfile
            twinop = ""
            twinalpha = ""
            for line in logoutstr: #find the parameters
              if ("Twin operator:" in line):
                line = line.replace("Twin operator:","").lstrip()
                line = line.split(": Fraction =")[0]
                twinop = twinop + "(" + line.lower().replace(" ","") + ") " # (-h,k,l) (h,k,-l)
              if ("Twin fractions                =" in line): # at end will be last one in the file
                twinalpha = line.split("=")[1].strip().split()
                twinalpha = ' '.join(twinalpha)
         #  basetng.logTab(enum_out_stream.verbose,"Twinop "+str(twinop)) #the table at the end of the logfile
            twinop = twinop.strip()
            rdict = {
           "Final R-free": "{:5.2f}".format(100.*rf_final),
           "Final R-work": "{:5.2f}".format(100.*rw_final),
           "Start R-free": "{:5.2f}".format(100.*rf_start),
           "Start R-work": "{:5.2f}".format(100.*rw_start),
           "sg_info":str(sg_info),
           "tncs_info":str(tncs_info),
           "hires" : str(high_resolution),
           "Twin law":twinop if len(twinop) > 0 else "None",
           "Twin alpha":twinalpha if len(twinalpha) > 0 else "None",
           "ident":modlid.str(),
           "PhenixResultsObject":None,
           "newpdbname" : newpdbname, #only in refmac5 rdict
           "newmtzname" : newmtzname, #only in refmac5 rdict
            }
            if rf_final is not None:
              break
        #   for line in logoutstr: ##improve TODO
        #     if "Twin" in line:
        #       basetng.logTab(enum_out_stream.logfile,line)
      if error_thrown: #now log the error
        basetng.logBlank(enum_out_stream.logfile)
        raise ValueError(enum_err_code.fatal,message)
      basetng.logBlank(enum_out_stream.logfile)
    return [ rdict ]
 #}

  def run_phenix_refine(self,
                        mtz_file,
                        pdb_file,
                        ncyc,
                        high_resolution,
                        nproc,
                        sg_info,
                        tncs_info,
                        twin_law=None,
                        rigid_body=None,
                        ls_target=None,
                        tncs_correction=False):
 #{
     args = [
       mtz_file, pdb_file,
       "data_manager.fmodel.xray_data.r_free_flags.generate=false", #should always be present on the input tiny.mtz file
                                                                    #if overwritten, only generated to refinement resolution, hence deletes full set on output
       "data_manager.fmodel.xray_data.high_resolution="+str(high_resolution),
       "refinement.main.tncs_correction="+str(tncs_correction),
       "refinement.main.nproc="+ str(nproc),
       "refinement.main.stop_for_unknowns=false",
       "refinement.main.number_of_macro_cycles=" + str(ncyc),
       "refinement.pdb_interpretation.allow_polymer_cross_special_position=true",
       "refinement.pdb_interpretation.clash_guard.nonbonded_distance_threshold=None",
       "refinement.output.write_eff_file=false",
       "refinement.output.write_geo_file=false",
       "refinement.output.write_final_geo_file=false",
       "refinement.output.write_def_file=false",
       "refinement.output.write_model_cif_file=false",
       "refinement.output.write_final_pdb_file=false",
       "refinement.output.write_mtz_file=false",
       "refinement.output.write_files=false",
       "refinement.output.write_reflection_cif_file=false",
       "refinement.output.export_final_f_model=false",
       "refinement.output.write_maps=false",
       "refinement.output.write_map_coefficients=false",
       "refinement.output.write_map_coefficients_only=false",
       "refinement.output.pickle_fmodel=false",
       "output.overwrite=true",
       ]
     if ls_target is not None and ls_target:
       args.append("main.target=ls")
     if rigid_body is not None and rigid_body:
       args.append("refine.strategy=rigid_body")
       args.append("refinement.rigid_body.mode=every_macro_cycle")
     else:
       pdbobj = iotbx.pdb.input(pdb_file)
       pdbih = pdbobj.construct_hierarchy()
       nchains = len(pdbih.models()[0].chains())
       if int(nchains) == 1: #7ENM, doesn't like one chain group Bfactors, bad map coefficients, correlation with overall b?
         args.append("refine.strategy = *individual_sites")
       else:
         args.append("refine.strategy = *individual_sites *group_adp")
    # occupancy is refined if the occupancies are not 1, which happens with lattice translocation disorder
    #  args.append("refine.strategy = *individual_sites *group_adp *individual_sites_real_space")
    #  args.append("refine.strategy = *individual_sites *individual_adp")
    #  args.append("refine.strategy = *individual_sites *individual_adp *individual_sites_real_space")
    #  args.append("refine.strategy = *individual_sites")
    #  args.append("refine.strategy = *individual_adp")
     if twin_law is not None:
       args.append("data_manager.fmodel.xray_data.twin_law="+str(twin_law))
     #note that the space group may be different from the one on the input mtz file
     #phenix.refine uses the space group on the pdb file not the mtz file
     outstr = TextIO()
     try:
       from phenix.programs import phenix_refine
       from phenix.programs.phenix_refine import custom_process_arguments
       r = run_program(
         program_class = phenix_refine.Program,
         args          = args,
         custom_process_arguments = custom_process_arguments,
         logger        = outstr # use default
         )
     except Exception as e:
      #r.show_start_final_stats_brief() #append to logfile
       logoutstr = outstr.getvalue().split("\n")
       summarystr = [x.strip() for x in str(e).split('\n')]
       if ls_target is True: #twin target uses ls target
         twin_law = str(twin_law) + ":ls-twin-target"
       rdict = {
         "Final R-free": "NaN",
         "Final R-work": "NaN",
         "Start R-free": "NaN",
         "Start R-work": "NaN",
         "sg_info":str(sg_info),
         "tncs_info":str(tncs_info),
         "hires":str(high_resolution),
         "Twin law":str(twin_law),
         "Twin alpha": "NaN",
         "PhenixResultsObject":None, #the files will be written from this result
         "newpdbname" : None, #only in refmac5 rdict
         "newmtzname" : None, #only in refmac5 rdict
         "Logfile":logoutstr,
         "Summary":summarystr,
         }
       return rdict
    #r.show_start_final_stats_brief() #append to logfile
     logoutstr = outstr.getvalue().split("\n")
     interesting_lines = [ "Resolution    Compl Nwork",
                           "Completeness"]
     summarystr = []
     for line in logoutstr:
        lline = line.lstrip()
        for interesting in interesting_lines:
          if lline.startswith(interesting):
            summarystr.append(str(lline))
        if len(lline) > 0 and lline[0].isdigit() and len(line.split())==10 and ":" not in line:
          summarystr.append(str(line)) #include left spaces for layout
     rw_start = r.monitor_xray.r_works[0] #doesn't include bulk solvent correction
     rf_start = r.monitor_xray.r_frees[0] #doesn't include bulk solvent correction
     if(len(r.monitor_xray.steps) > 1):
        if(r.monitor_xray.steps[1].count("bss")):
          rw_start = r.monitor_xray.r_works[1]
          rf_start = r.monitor_xray.r_frees[1]
     #r.fmodel.twin_fraction is type int with value zero if twinning not set
     if ls_target is True: #twin target uses ls target
         twin_law = str(twin_law) + ":ls-twin-target"
     rdict = {
         "Final R-free": "{:5.2f}".format(100.*r.fmodel.r_free()),
         "Final R-work": "{:5.2f}".format(100.*r.fmodel.r_work()),
         "Start R-free": "{:5.2f}".format(100.*rf_start),
         "Start R-work": "{:5.2f}".format(100.*rw_start),
         "sg_info":str(sg_info),
         "tncs_info":str(tncs_info),
         "hires":str(high_resolution),
         "Twin law":str(twin_law),
         "Twin alpha":"{:5.3f}".format(r.fmodel.twin_fraction),
         "PhenixResultsObject":r, #the files will be written from this result
         "Logfile":logoutstr,
         "Summary":summarystr,
         }
     debug = False
     if debug:
     # print(dir(r))
       print("logfile=",len(logoutstr))
       print(rdict)
       r.params.output.write_final_pdb_file=True
       r.write_refined_pdb(file_name="output_pdb_file")
       self.write_refined_mtz(r=r,file_name="output_mtz_file")
     return rdict
 #}

  def run_phenix_refine_node(self,basetng,node,twin_results,
                        mtz_file,
                        pdb_file,
                        ncyc,
                        high_resolution,
                        nproc,
                        sg_info,
                        tncs_info,
                        io_sigtwinfrac,
                        io_twinned=None,
                        twin_law=None,
                        rigid_body=None,
                        ls_target=None,
                        tncs_correction=False):
 #{
    node_rfactors = []
    try:
   #{
     #refine_eff_file_list=[]
     #cif_def_file_list=[]
      r = None
      if True: #turn off for debugging
     #{ always do the refinement without the twin laws first
        basetng.logChevron(enum_out_stream.logfile,"Refinement")
        basetng.logEllipsisOpen(enum_out_stream.logfile,"Running phenix refine")
      # we will use two things from r
      # r.params.output.write_final_pdb_file=True
      # r.write_refined_pdb(file_name=newpdbname.fstr())
        rdict = self.run_phenix_refine(
            mtz_file=mtz_file,
            pdb_file=pdb_file,
            ncyc=ncyc, #default in phenix.refine
            high_resolution=high_resolution,
            nproc=nproc,
            sg_info=node.sg_info(),
            tncs_info=node.tncs_info(),
            rigid_body=rigid_body,
            ls_target=ls_target,
            tncs_correction=tncs_correction,
           )
        basetng.logEllipsisShut(enum_out_stream.logfile)
        wf = basetng.WriteFiles()
       #write the file here even if it is not the best, and no coordinates are written
        if wf:
          DAGDB = basetng.DAGDATABASE
          subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
          logout = node.TRACKER.ULTIMATE.extra_file_stem_ext(".refine.log")
          flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
          if not flat and wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
            os.makedirs(os.path.join(basetng.DataBase(),subdir))
          newlogname = FileSystem(basetng.DataBase(),subdir,logout)
          basetng.logFileWritten(enum_out_stream.logfile,wf,"Phenix",newlogname)
          with open(newlogname.fstr(), "w") as f:
              f.write("PHASERTNG REFINEMENT\n")
              f.write("--------------------\n")
              f.write("\n".join(rdict["Logfile"]))
        if rdict['Final R-work'] == "NaN":
          basetng.logTab(enum_out_stream.logfile,"Phenix refine error")
        else:
          basetng.logTabArray(enum_out_stream.verbose,rdict["Summary"])
          basetng.logBlank(enum_out_stream.verbose)
          basetng.logTab(enum_out_stream.logfile,"Start R-work/R-free = "+(rdict['Start R-work'])+"/"+(rdict['Start R-free']))
          basetng.logTab(enum_out_stream.logfile,"Final R-work/R-free = "+(rdict['Final R-work'])+"/"+(rdict['Final R-free']))
        node_rfactors.append(rdict)
     #}
      # r and rdict set to no twin value
      if io_twinned is None:
        treat_as_twinned = (node.TWINNED == "Y")
      else:
        treat_as_twinned = bool(io_twinned.strip() == "True")
      if (treat_as_twinned) and int(twin_results.n_twin_laws) > 0:
     #{
        if True:
       #{
          basetng.logChevron(enum_out_stream.logfile,"Refinement")
          line = "Twin_law=None : least squares target (as for twin target)"
          basetng.logEllipsisOpen(enum_out_stream.logfile,"Running phenix refine, " + line)
          qdict = self.run_phenix_refine(
              mtz_file=mtz_file,
              pdb_file=pdb_file,
              ncyc=ncyc, #default in phenix.refine
              high_resolution=high_resolution,
              nproc=nproc,
              sg_info=node.sg_info(),
              tncs_info=node.tncs_info(),
              twin_law=None,
              rigid_body=rigid_body,
              ls_target=True, #twinning refinement uses ls target, so must run this for comparison
              tncs_correction=tncs_correction,
             )
          basetng.logEllipsisShut(enum_out_stream.logfile)
          ii = 0
          if wf:
            subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
            logout = node.TRACKER.ULTIMATE.extra_file_stem_ext(".twin."+str(ii+1)+".refine.log")
            newlogname = FileSystem(basetng.DataBase(),subdir,logout)
            basetng.logFileWritten(enum_out_stream.logfile,wf,"Phenix",newlogname)
            with open(newlogname.fstr(), "w") as f:
                f.write("PHASERTNG TWIN REFINEMENT\n")
                f.write(line+"\n")
                f.write("-------------------------\n")
                f.write("\n".join(qdict["Logfile"]))
          if rdict['Final R-work'] == "NaN":
            basetng.logTab(enum_out_stream.logfile,"Phenix refine error")
          else:
            basetng.logTabArray(enum_out_stream.verbose,qdict["Summary"])
            basetng.logBlank(enum_out_stream.verbose)
            basetng.logTab(enum_out_stream.logfile,"Start R-work/R-free = "+(qdict['Start R-work'])+"/"+(qdict['Start R-free']))
            basetng.logTab(enum_out_stream.logfile,"Final R-work/R-free = "+(qdict['Final R-work'])+"/"+(qdict['Final R-free']))
            basetng.logBlank(enum_out_stream.logfile)
          node_rfactors.append(qdict)
       #}
        for ii in range(twin_results.n_twin_laws):
       #{
          #node.RFACTOR starting value is the one from the refinement without the twin laws
          if (float(twin_results.twin_summary.murray_rust_alpha[ii]) > float(io_sigtwinfrac)):
         #{
            basetng.logChevron(enum_out_stream.logfile,"Refinement")
            line = "Twin_law=(%s) alpha=%5.3f"%(twin_results.twin_summary.twin_laws[ii],
                                         twin_results.twin_summary.murray_rust_alpha[ii])
            twin_law = twin_results.twin_summary.twin_laws[ii]
            basetng.logEllipsisOpen(enum_out_stream.logfile,"Running phenix refine, " + line)
            qdict = self.run_phenix_refine(
                mtz_file=mtz_file,
                pdb_file=pdb_file,
                ncyc=ncyc, #default in phenix.refine
                high_resolution=high_resolution,
                nproc=nproc,
                sg_info=node.sg_info(),
                tncs_info=node.tncs_info(),
                twin_law=twin_law,
                rigid_body=rigid_body,
                tncs_correction=tncs_correction,
               )
            basetng.logEllipsisShut(enum_out_stream.logfile)
            if wf:
              subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
              logout = node.TRACKER.ULTIMATE.extra_file_stem_ext(".twin."+str(ii+1)+".refine.log")
              newlogname = FileSystem(basetng.DataBase(),subdir,logout)
              basetng.logFileWritten(enum_out_stream.logfile,wf,"Phenix",newlogname)
              with open(newlogname.fstr(), "w") as f:
                  f.write("PHASERTNG TWIN REFINEMENT\n")
                  f.write(line+"\n")
                  f.write("-------------------------\n")
                  f.write("\n".join(qdict["Logfile"]))
            if rdict['Final R-work'] == "NaN":
              basetng.logTab(enum_out_stream.logfile,"Phenix refine error")
            else:
              basetng.logTabArray(enum_out_stream.verbose,qdict["Summary"])
              basetng.logBlank(enum_out_stream.verbose)
              basetng.logTab(enum_out_stream.logfile,"Start R-work/R-free = "+(qdict['Start R-work'])+"/"+(qdict['Start R-free']))
              basetng.logTab(enum_out_stream.logfile,"Final R-work/R-free = "+(qdict['Final R-work'])+"/"+(qdict['Final R-free']))
              basetng.logBlank(enum_out_stream.logfile)
            node_rfactors.append(qdict)
         #}
          else:
         #{
            line = "Twin_law=(%s) alpha=%5.3f"%(twin_results.twin_summary.twin_laws[ii],
                                         twin_results.twin_summary.murray_rust_alpha[ii])
            twin_law = twin_results.twin_summary.twin_laws[ii]
            basetng.logEllipsisOpen(enum_out_stream.logfile,"phenix refine, " + line)
            basetng.logTab(enum_out_stream.logfile,"Twin alpha below value for significance (" + str(io_sigtwinfrac) + ")")
            basetng.logEllipsisShut(enum_out_stream.logfile)
         #}
       #}
     #}
   #}
    except Exception as e: #from original data
      raise ValueError(enum_err_code.fatal,"Refine error %s" % str(e))
    return node_rfactors
 #}

  def write_refined_mtz(self,basetng,r,file_name):
     if r is None: return
     #this is a broken down version of write_after_run_outputs
     #r.write_after_run_outputs()
     fmx, fmn = r.fmodels.fmodel_xray(), None
     suff_x = ""
     if(fmn is not None): suff_x = "_xray"
     suff_n = "_neutron"
     mdo = r.mtz_dataset_orig
   # mdo.mtz_dataset.mtz_object().write(file_name=file_name) #copy of the original data only (boring)
     for _fm,_mdo,suff in zip([fmx,fmn],[mdo,None],[suff_x,suff_n]):
       if(_fm is None):
         assert _mdo is None
         continue
       cmo = mmtbx.maps.compute_map_coefficients(
         fmodel                   = _fm,
         params                   = copy.deepcopy(r.params.electron_density_maps.map_coefficients),
         post_processing_callback = None,
         log                      = None,
         pdb_hierarchy            = r.model.get_hierarchy())
       fcs = mmtbx.maps.compute_f_calc(
         fmodel                   = _fm,
         params                   = copy.deepcopy(r.params.electron_density_maps.map_coefficients),
         )
     #what a palava, copied from runBEAM
     cs=r.mtz_dataset_orig.mtz_dataset.mtz_crystal().crystal_symmetry()
     #first add the FC/PHIC which are useful when working out if the crystal is of higher symmery
     mtz_obj3 = fcs.as_mtz_dataset(column_root_label="FC").mtz_object()
     miller_arrays = mtz_obj3.as_miller_arrays(crystal_symmetry=cs)
     for k,ma in enumerate(miller_arrays):
       if ma.info() is not None and ma.info().labels is not None:
         basetng.logTab(enum_out_stream.verbose,"Labels:" + str(ma.info().labels))
         label,labels = simple_tools.ma_mtz_labels(miller_arrays,k,ma)
         r.mtz_dataset_orig.mtz_dataset.add_miller_array(ma, column_root_label=label)
     mtz_obj2 = cmo.mtz_dataset.mtz_object()
     miller_arrays = mtz_obj2.as_miller_arrays(crystal_symmetry=cs)
     for k,ma in enumerate(miller_arrays):
       if ma.info() is not None and ma.info().labels is not None:
         basetng.logTab(enum_out_stream.verbose,"Labels:" + str(ma.info().labels))
         label,labels = simple_tools.ma_mtz_labels(miller_arrays,k,ma)
         r.mtz_dataset_orig.mtz_dataset.add_miller_array(ma, column_root_label=label)
     mtz_obj1 = r.mtz_dataset_orig.mtz_dataset.mtz_object()
     mtz_obj1.write(file_name=file_name)
   # cmo.mtz_dataset.mtz_object().write(file_name=file_name) #mtz file with just the map coefficients

  def run_from_basetng_and_input(self,basetng):
    io_rigid_body=basetng.input.get_as_str(".phasertng.refinement.rigid_body.")
    io_tncs_correction=basetng.input.get_as_str(".phasertng.refinement.tncs_correction.")
    io_rigid_body = bool(io_rigid_body.strip() == "True")
    io_tncs_correction = bool(io_tncs_correction.strip() == "True")
    self.inner_xray_refine(basetng=basetng,
      reflid_label=basetng.input.get_as_str(".phasertng.labin.inat."),
      io_top_files=basetng.input.get_as_str(".phasertng.refinement.top_files."),
      io_phenix_ncyc=basetng.input.get_as_str(".phasertng.refinement.ncyc.phenix."),
      io_refmac_ncyc=basetng.input.get_as_str(".phasertng.refinement.ncyc.refmac."),
      io_method_refmac=bool(basetng.input.get_as_str(".phasertng.refinement.method.").strip() == "refmac"),
      io_rigid_body=io_rigid_body,
      io_tncs_correction=io_tncs_correction,
      io_sigtwinfrac=float(basetng.input.get_as_str(".phasertng.refinement.significant_twin_fraction.")),
      io_twinned=isNone(basetng.input.get_as_str(".phasertng.refinement.treat_as_twinned.")),
      high_resolution=basetng.input.get_as_str(".phasertng.refinement.resolution."),
      nproc=basetng.input.get_as_str(".phasertng.threads.number."))

  def inner_xray_refine(self,
          basetng,
          reflid_label,
          io_top_files,
          io_phenix_ncyc,
          io_refmac_ncyc,
          io_method_refmac,
          io_rigid_body,
          io_tncs_correction,
          io_sigtwinfrac,
          io_twinned,
          high_resolution,
          nproc,
          f_obs_label=None):
 #{
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0):
   #{
      basetng.logTab(enum_out_stream.logfile,"No nodes")
      return
   #}
   #if (!(DAGDB.NODES.is_all_pose() or DAGDB.NODES.has_no_turn_gyre_curl_pose()))
    DAGDB.clear_holds()
    basetng.logTab(enum_out_stream.logfile,"Refinement of coordinates and isotropic B-factors with bulk-solvent terms")
    basetng.logTab(enum_out_stream.logfile,"Resolution for calculation = " + str(high_resolution))
    basetng.logTab(enum_out_stream.logfile,"Maximum number refined = " + str(io_top_files))
    basetng.logTab(enum_out_stream.logfile,"Number of refinement cycles (phenix) = " + str(io_phenix_ncyc))
    basetng.logTab(enum_out_stream.logfile,"Number of refinement cycles (refmac) = " + str(io_refmac_ncyc))
    if io_rigid_body is None: #default
      basetng.logTab(enum_out_stream.logfile,"Individual coordinates and residue B-factor refinement (default)")
    elif io_rigid_body:
      basetng.logTab(enum_out_stream.logfile,"Rigid body refinement")
    else:
      basetng.logTab(enum_out_stream.logfile,"Individual coordinates and residue B-factor refinement")
    #the fact that all the files start in the one directory assures that the filenames are unique
    #adds terminal number xref-1 xref-2 added in shift
    DAGDB.shift_tracker(str(basetng.hashing())+str(io_method_refmac),"xref")
    all_node_rfactors = []
    io_top_files = int(io_top_files)
    top_files = min(len(DAGDB.NODES.LIST),io_top_files)
    for i,node in enumerate(DAGDB.NODES.LIST):
      node.RFACTOR = 100.
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
    twin_results = None
    if io_twinned is None:
      treat_as_twinned = (node.TWINNED == "Y")
    else:
      treat_as_twinned = bool(io_twinned.strip() == "True")
    basetng.logTab(enum_out_stream.logfile,"Treat as twinned = " + str(treat_as_twinned))
    if int(DAGDB.size()) > 0:
      node = DAGDB.NODES.LIST[0]
      iomtz,f_obs_label = self.get_iomtz(basetng,node,f_obs_label)
      if iomtz is None:
        return
      twin_results = get_twin_results(basetng,treat_as_twinned,iomtz)
    for i,node in enumerate(DAGDB.NODES.LIST):
   #{
      if ("SYM=N*" not in node.ANNOTATION): #analysis only, the ones matching input. SYM=Y* also but rare
        if (i >= io_top_files and io_top_files > 0):
          continue #because there might be another one that is input interesting
      basetng.logUnderLine(enum_out_stream.logfile,"Coordinate refinement #" + str(i+1) + " of " + str(top_files) + " [of " + str(len(DAGDB.NODES.LIST)) + " total]")
      if not node.FULL.PRESENT:
        basetng.logTab(enum_out_stream.logfile,"No coordinates (\"full\" not present in node), skipping")
        continue
      subdir = DAGDB.lookup_modlid(node.FULL.IDENTIFIER).database_subdir()
      entfile = node.FULL.IDENTIFIER.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
      iopdb = FileSystem(basetng.DataBase(),subdir,entfile)
      basetng.logTabArray(enum_out_stream.logfile,node.logAnnotation("",True))
      basetng.logTabArray(enum_out_stream.verbose,node.logNode(""))
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iopdb.qfstrq())
      if not os.path.exists(iopdb.fstr()):
        basetng.logTab(enum_out_stream.logfile,"Pdb file does not exist")
        DAGDB.NODES.set_node(i,node) #python not iterate by reference
        continue
      cmd_cs = from_pdb.extract_from(file_name = iopdb.fstr())
      #can either call the original reflection file here or the density file
      #if calling reflid then the f_obs_label as to be passed because it
      # cannot be deduced automatically
      #whereas it is deduced from the density file (fewer data items)
      iomtz,f_obs_label = self.get_iomtz(basetng,node,f_obs_label)
      basetng.logTab(enum_out_stream.logfile,"Reading: " + iomtz.qfstrq())
      for line in str(cmd_cs).split("\n"):
        basetng.logTab(enum_out_stream.logfile,str(line))
      if not os.path.exists(iomtz.fstr()):
        raise ValueError(enum_err_code.fileopen,iomtz.fstr())
      outstr = TextIO()
      mtzobj = iotbx.mtz.object(iomtz.fstr())
      d_min = sorted(mtzobj.max_min_resolution())[0]
      hires = max(float(high_resolution),float(d_min))

      #prepare the entry first, because we need to write the files direct from refmac5
      modlid = node.TRACKER.ULTIMATE
      DAGDB.add_entry_copy_header(modlid,node.FULL.IDENTIFIER)

      io_run_phenix_refine = not io_method_refmac
      io_run_ccp4_refmac5 = io_method_refmac

      if io_run_phenix_refine:
        #returns all the twin operator rdicts, without the pdb and mtz files written
        node_rfactors = self.run_phenix_refine_node(basetng,node,twin_results,
              mtz_file=iomtz.fstr(),
              pdb_file=iopdb.fstr(),
              ncyc=io_phenix_ncyc, #default in phenix.refine
              high_resolution=hires,
              io_sigtwinfrac=io_sigtwinfrac,
              io_twinned=io_twinned,
              nproc=nproc,
              sg_info=node.sg_info(),
              tncs_info=node.tncs_info(),
              rigid_body=io_rigid_body,
              ls_target=None,
              tncs_correction=io_tncs_correction,)

      if io_run_ccp4_refmac5:
        #returns rdict but as a single entry list with pdb and mtz written
        node_rfactors = self.run_ccp4_refmac5_node(basetng,node,
              sg_info=node.sg_info(),
              tncs_info=node.tncs_info(),
              ncyc=io_refmac_ncyc,
              high_resolution=hires,
              iomtz=iomtz.fstr(),
              iopdb=iopdb.fstr(),)

      if True:
     #{
        bestj = 0
        for j,node_rfactor in enumerate(node_rfactors):
       #{
          if io_run_phenix_refine: node_rfactor["ident"]=None
          node_rfactor["node#"]=i+1
          node_rfactor["rfac#"]=j+1
          if node_rfactor["Final R-work"] != "NaN":
            if float(node_rfactor["Final R-work"]) <= float(node_rfactors[bestj]["Final R-work"]):
              bestj = j
              node.RFACTOR = float(node_rfactor["Final R-work"])
              #twin law is a string of whitespace separated twinops, use get_strings to parse
              node.TWINOP = node_rfactor["Twin law"] if node_rfactor["Twin law"] != "None" else ""
              DAGDB.NODES.set_node(i,node) #python not iterate by reference
       #}

        r = node_rfactors[bestj]["PhenixResultsObject"] #None if it comes from refmac
        if r is not None and node_rfactors[bestj]["Final R-work"] != "NaN":
       #{
          basetng.logChevron(enum_out_stream.logfile,"Database Entries")
          node.FULL.IDENTIFIER = node.TRACKER.ULTIMATE
          node.ANNOTATION = node.ANNOTATION + " R="+str(round(node.RFACTOR))+"%"
          DAGDB.NODES.set_node(i,node) #python not iterate by reference
          subdir = DAGDB.lookup_modlid(modlid).database_subdir()
          wf = basetng.WriteFiles()
          if True:
         #{
            pdbout = modlid.entry_data_stem_ext(enum_entry_data.coordinates_pdb)
            newpdbname = FileSystem(basetng.DataBase(),subdir,pdbout)
            node_rfactors[bestj]["newpdbname"] = newpdbname
            basetng.logFileWritten(enum_out_stream.logfile,wf,"Coordinates",newpdbname)
            node_rfactors[bestj]["ident"]=modlid.str()
            if (wf and io_run_phenix_refine):
              r.params.output.write_final_pdb_file=True
              r.write_refined_pdb(file_name=newpdbname.fstr())

            #since this file is written directly, we should load into memory so that dag records it as being present
            basetng.load_entry_from_database(enum_out_stream.logfile,enum_entry_data.coordinates_pdb,modlid,False)

            #this is not a enum_entry_data.density_mtz file
            wf = wf #and basetng.WriteDensity() #we need this file for the analysis
            mtzout = modlid.other_data_stem_ext(enum_other_data.refine_mtz)
            newmtzname = FileSystem(basetng.DataBase(),subdir,mtzout)
            basetng.logFileWritten(enum_out_stream.logfile,wf,"Density",newmtzname)
            node_rfactors[bestj]["newmtzname"] = newmtzname
            if (wf and io_run_phenix_refine):
              r = node_rfactors[bestj]["PhenixResultsObject"]
              self.write_refined_mtz(basetng=basetng,r=r,file_name=newmtzname.fstr())

            wf = basetng.WriteCards()
            filesystem = FileSystem(basetng.DataBase(),modlid.reflections_and_models(),modlid.other_data_stem_ext(enum_other_data.entry_cards))
            basetng.logFileWritten(enum_out_stream.logfile,wf,"Entry cards",filesystem)
            dagcards = DAGDB.entry_unparse_modlid(modlid)
            if (wf):
              filesystem.write(dagcards)

            if i == 0: #first only
              basetng.logTab(enum_out_stream.verbose,"Pdbout recorded in results")
              basetng.input.set_as_str(".phasertng.pdbout.filename.",newpdbname.fstr())
              basetng.input.set_as_str(".phasertng.pdbout.pdbid.",newpdbname.fstr())
              basetng.input.set_as_str(".phasertng.pdbout.id.",str(modlid.identifier()))
              basetng.input.set_as_str(".phasertng.pdbout.tag.",modlid.tag())
         #}
       #}
        for j,node_rfactor in enumerate(node_rfactors):
          all_node_rfactors.append(node_rfactor) #append all
     #}
   #}
    if len(all_node_rfactors) > 0:
   #{
      basetng.logTableTop(enum_out_stream.summary,"Coordinate Refine",True)
      if io_method_refmac:
        basetng.logTab(enum_out_stream.summary,"Refmac")
      else:
        basetng.logTab(enum_out_stream.summary,"Phenix")
      basetng.logTab(enum_out_stream.summary,"Poses: " + str(DAGDB.NODES.number_of_poses()))
      basetng.logTab(enum_out_stream.summary,"{:<3}{:<16}{:<16s}{:<30}{:<11}{:<20}{:<17}".format(
         "#","Start-Rfactor","Final-Rfactor","Twin-Law","Twin-alpha","Identifier/Filename","Space Group"))
      all_node_rfactors.sort(key = lambda x: x["Final R-work"])
      for rdict in all_node_rfactors:
        if (rdict["ident"] is None): rdict["ident"] = ""
        basetng.logTab(enum_out_stream.summary,
              '{:<3d}'.format(rdict["node#"]) +
              '  {:5}/{:5}   '.format(rdict["Start R-work"],rdict["Start R-free"]) +
              '  {:5}/{:5}   '.format(rdict["Final R-work"],rdict["Final R-free"]) +
              '{:<30}'.format(rdict["Twin law"].split()[0]) +
              '{:<11}'.format(rdict["Twin alpha"].split()[0]) +
              '{:<20}'.format(rdict["ident"]) +
              '{:<17}'.format(rdict["sg_info"])
              )
        laws = rdict["Twin law"].split()
        afas = rdict["Twin alpha"].split()
        assert(len(laws) == len(afas))
        if io_run_ccp4_refmac5 and len(laws) > 1:
          for ilaw in range(len(laws)):
            if ilaw > 0:
           #  from cctbx import sgtbx
           #  cbop = sgtbx.change_of_basis_op( rdict["Twin law"].split()[ilaw][1:-1] ).as_abc()
              basetng.logTab(enum_out_stream.summary,
              '{:<3d}'.format(rdict["node#"]) +
              '  {:5} {:5}   '.format("","") +
              '  {:5} {:5}   '.format("","") +
              '{:<30}'.format(rdict["Twin law"].split()[ilaw]) +
              '{:<11}'.format(str(rdict["Twin alpha"].split()[ilaw]))
              )
    # if io_top_files > 0 and (len(DAGDB.NODES.LIST) > io_top_files):
    #   n = len(DAGDB.NODES.LIST)-io_top_files
    #   basetng.logTab(enum_out_stream.summary, "----------- and " + str(n) + " more not refined")
      basetng.logTableEnd(enum_out_stream.summary)
   #}
    basetng.logTab(enum_out_stream.verbose,"Flag nodes for purge")
    for i,node in enumerate(DAGDB.NODES.LIST):
      node.generic_flag = False #erase
      DAGDB.NODES.set_node(i,node) #python not iterate by reference
    basetng.logTab(enum_out_stream.testing,"end reset")
    good_node_rfactors = []
    for i,item in enumerate(all_node_rfactors):
      if item['Final R-work'] != "NaN":
        good_node_rfactors.append(item)
    basetng.logTab(enum_out_stream.testing,"Number of good nodes ="+str(len(good_node_rfactors)))
    for item in good_node_rfactors:
        i = int(item["node#"])-1
        basetng.logTab(enum_out_stream.verbose,"Good Node #"+str(i))
        assert(i < int(DAGDB.size()))
        node = DAGDB.NODES.LIST[i]
        node.generic_flag = True #keep
        DAGDB.NODES.set_node(i,node) #python not iterate by reference
    basetng.logBlank(enum_out_stream.verbose)
    if len(good_node_rfactors) > 0:
   #{
      wf = basetng.WriteFiles()
      if (wf):
        subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
        tmpfile = str(enum_other_data.solutions_log).replace("_",".")
        flat = FileSystem().flat_filesystem() #or anything to get flat_filesystem
        if not flat and wf and not os.path.exists(os.path.join(basetng.DataBase(),subdir)):
          os.makedirs(os.path.join(basetng.DataBase(),subdir))
        newlogname = FileSystem(basetng.DataBase(),subdir,tmpfile)
        basetng.logFileWritten(enum_out_stream.logfile,wf,"R-factors",newlogname)
        sel = "Refmac" if io_method_refmac else "Phenix"
        #if this is changed change runXREF and runTREE (col = 16) also
        with open(newlogname.fstr(), "w") as f:
          for rdict in sorted(good_node_rfactors,key = lambda i: i['Final R-work']):
            twinlaw = "N" if rdict["Twin law"] == "None" else "Y"
            line =("Solution: " +
              sel + ' R-factor = {:5}'.format(rdict["Final R-work"]) +
              " dmin = {:5.2f}".format(float(rdict["hires"])) +
              ' poses = {:2d}'.format(DAGDB.NODES.number_of_poses()) +
              ' sg = {:<7} '.format(rdict["sg_info"]) +
              " tncs = " + str(rdict["tncs_info"]) +
              " twin = " + twinlaw +
              " " + str(subdir) +
              " " + str(rdict["ident"]))
            basetng.logTab(enum_out_stream.logfile,line)
            f.write(line+"\n")
   #}
    else:
      basetng.logUnderLine(enum_out_stream.summary,"Coordinates Refinement")
      basetng.logTab(enum_out_stream.summary,"No successful refinements, see logfiles for details")
      basetng.logBlank(enum_out_stream.summary)
    basetng.logBlank(enum_out_stream.summary)
    basetng.logTab(enum_out_stream.summary, "Purge nodes that have not been refined")
    basetng.logTab(enum_out_stream.logfile, "Number until purge: " + str(DAGDB.size()))
    DAGDB.NODES.erase_invalid()
    basetng.logTab(enum_out_stream.logfile, "Number after purge: " + str(DAGDB.size()))
 #}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--pdb",help="pdb file",required=True)
  parser.add_argument('-m',"--mtz",help="mtz file",required=True)
  parser.add_argument('-r',"--resolution",type=float,required=False,default=2.8) #2.6 = (param/data~1)
  parser.add_argument('-n',"--ncyc",type=int,required=False,default=3)
  parser.add_argument('-t',"--nproc",type=int,required=False,default=3)
  parser.add_argument('-rb',"--rigid_body",dest='rigid_body',action='store_true',help="rigid body refinement",required=False)
  parser.add_argument('-s',"--refmac",dest='refmac',action='store_true',help="use ccp4 refmac",required=False)
  args = parser.parse_args()
  basetng = Phasertng("VERBOSE")
  mtzobj = iotbx.file_reader.any_file(args.mtz)
  print("Running xref from command line")
  if True:
      mtzobj = iotbx.file_reader.any_file(args.mtz)
      miller_arrays = mtzobj.file_object.as_miller_arrays(crystal_symmetry=mtzobj.crystal_symmetry())
      text_out = sys.stdout
      for ma in miller_arrays:
        if ma.is_xray_data_array(): break
      twin_results = twin_analyses.twin_analyses(
              miller_array=ma,
              )
     #twin_results.show(text_out)
      text_out.flush()
      outstr = TextIO()
      if twin_results.n_twin_laws:
        print("Number of laws=",(twin_results.n_twin_laws))
        print("Twin Laws:  ",(twin_results.twin_summary.twin_laws))
        print("Twin Alphas:",(twin_results.twin_summary.murray_rust_alpha))
        print("**twin law not applied (edit!)***")
      else:
        print("No twin laws")
  r,logfile,all_node_rfactors = runXREF().run_phenix_refine(
            mtz_file=args.mtz,
            pdb_file=args.pdb,
            ncyc=args.ncyc,
            refmac=args.refmac,
            high_resolution=args.resolution,
            nproc=args.nproc,
            sg_info=None,
            tncs_info=None,
            ls_target=None,
            twin_law=None, #twin_results.twin_summary.twin_laws[0],
            rigid_body=args.rigid_body,
           )
  #f_obs_label=args.f_obs_label)
  tmpfile = "xref.logfile.log"
  with open(tmpfile, "w") as f: f.write("\n".join(logfile))
  print("Logfile:",tmpfile)
  print("Start",all_node_rfactors['Start R-work'],"/",all_node_rfactors['Start R-free'])
  print("Final",all_node_rfactors['Final R-work'],"/",all_node_rfactors['Final R-free'])
