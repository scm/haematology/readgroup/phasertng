//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_uuid_numbers_class__
#define __phasertng_type_uuid_numbers_class__
#include <phasertng/type/optional.h>
#include <phasertng/main/uuid.h> //definition of uuid
#include <vector> //definition of uuid

namespace phasertng {
namespace type {

typedef std::vector<uuid_t>                   sv_uuid;

class uuid_numbers : public optional
{
  sv_uuid dephalt,uvalue;
  bool  pminset = false;
  bool  pmaxset = false;
  uuid_t  pminval = 0;
  uuid_t  pmaxval = 0;

  public:
  const bool& minset() const { return pminset; }
  const uuid_t&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const uuid_t&  maxval() const { return pmaxval; }

  uuid_numbers() : optional() {}
  uuid_numbers(
      const std::string parameter,
      const bool pminset_, const uuid_t pminval_,
      const bool pmaxset_, const uuid_t pmaxval_,
      const sv_uuid dephalt_ = {}
  );

  void set_value(sv_uuid other);
  void set_value(const sv_string& vcards,const sv_uuid& other);
  void push_back(const uuid_t& other);
  bool is_default() const;
  void set_default();
  int size() const;
  sv_uuid value_or_default() const;
  uuid_t value_or_default(int i) const;
  sv_uuid value_or_default_unique() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::uuid_numbers& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
