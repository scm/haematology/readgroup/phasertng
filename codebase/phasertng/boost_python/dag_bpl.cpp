//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
//#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost_adaptbx/std_pair_conversion.h>
//#include <scitbx/stl/set_wrapper.h>
#include <scitbx/stl/map_wrapper.h>
#include <phasertng/dag/DagDatabase.h> //node types
#include <phasertng/data/ReflColsMap.h> //node types

#include <boost_adaptbx/std_pair_conversion.h>
#include <boost_adaptbx/tuple_conversion.h>
#include <phasertng/boost_python/std_triple_conversion.h>
#include <phasertng/boost_python/std_quad_conversion.h>
#include <phasertng/boost_python/std_quint_conversion.h>

namespace phasertng { // so we don't have to write phasertng:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_phasertng_dag()
{
  using namespace scitbx::stl::boost_python;
  using namespace boost::python;
  map_wrapper< std::map< std::string, int>>::wrap( "map_str_int" );
  map_wrapper< std::map< labin::col, af_double>>::wrap( "map_col_flt" );
  map_wrapper< std::map< labin::col, af_int>>::wrap( "map_col_int" );
  map_wrapper< std::map< labin::col, af_bool>>::wrap( "map_col_bln" );
  map_wrapper< std::map< friedel::type, af_bool>>::wrap( "map_fdl_bln" );


  typedef return_value_policy<return_by_value> rbv;

  class_<Identifier>("Identifier",init<>())
    .enable_pickling()
    .def(init<ullint,std::string>())
    .def("string",&Identifier::string)
    .def("identifier",&Identifier::identifier)
    .def("initialize_node_tag",&Identifier::initialize_node_tag)
    .def("initialize_from_ullint",&Identifier::initialize_from_ullint)
    .def("initialize_from_text",&Identifier::initialize_from_text)
    .def("initialize_tag",&Identifier::initialize_tag)
    .def("tag",&Identifier::tag)
    .def("str",&Identifier::str)
    .def("file_stem",&Identifier::stem)
    .def("database_subdir",&Identifier::database_subdir)
    .def("stem",&Identifier::stem) //same as database_subdir
    .def("unstem",&Identifier::unstem) //same as database_subdir
    .def("extra_file_stem_ext",&Identifier::extra_file_stem_ext)
    .def("entry_data_stem_ext",&Identifier::entry_data_stem_ext)
    .def("other_data_stem_ext",&Identifier::other_data_stem_ext)
    .def("reflections_and_models",&Identifier::reflections_and_models)
  ;

  class_<DogTag>("DogTag",init<>())
    .enable_pickling()
    .def("database_subdir",&DogTag::database_subdir)
    .def("extra_file_stem_ext",&DogTag::extra_file_stem_ext)
    .def("entry_data_stem_ext",&DogTag::entry_data_stem_ext)
    .def("other_data_stem_ext",&DogTag::other_data_stem_ext)
    .def("identifier",&DogTag::identifier)
    .def("string",&DogTag::string)
    .def("tag",&DogTag::tag)
  ;

  class_<dag::Edge>("Edge",init<>())
    .enable_pickling()
    .def("set_default",   &dag::Edge::set_default)
    .def_readwrite("ULTIMATE",   &dag::Edge::ULTIMATE)
    .def_readwrite("PENULTIMATE",&dag::Edge::PENULTIMATE)
    .def("shift_edge",&dag::Edge::shift_edge)
  ;

  class_<dag::Node>("Node",init<>())
    .def_readwrite("TRACKER", &dag::Node::TRACKER) //change the name here
    .def_readwrite("ANNOTATION",&dag::Node::ANNOTATION)
    .def_readwrite("REFLID",&dag::Node::REFLID)
 //   .def_readwrite("COMPOSITION",&dag::Node::COMPOSITION) //map_str_int
    .def_readwrite("TNCS_INDICATED",&dag::Node::TNCS_INDICATED)
    .def_readwrite("TNCS_ORDER",&dag::Node::TNCS_ORDER)
    .def("tncs_vector",&dag::Node::tncs_vector) //because boosting dvect3 is a problem (none or duplicate??): returns str
    .def_readwrite("TNCS_MODELLED",&dag::Node::TNCS_MODELLED)
    .def_readwrite("HALL",&dag::Node::HALL)
    .def_readwrite("CELL",&dag::Node::CELL)
    .def_readwrite("TWINNED",&dag::Node::TWINNED)
    .def_readwrite("PACKS",&dag::Node::PACKS)
    .def_readwrite("CLASH_WORST",&dag::Node::CLASH_WORST)
    .def_readwrite("SPECIAL_POSITION",&dag::Node::SPECIAL_POSITION)
    .def_readwrite("LLG",&dag::Node::LLG)
    .def_readwrite("MAPLLG",&dag::Node::MAPLLG)
    .def_readwrite("FSS",&dag::Node::FSS)
    .def_readwrite("ZSCORE",&dag::Node::ZSCORE)
    .def_readwrite("CELLZ",&dag::Node::CELLZ)
    .def_readwrite("PERCENT",&dag::Node::PERCENT)
    .def_readwrite("RFACTOR",&dag::Node::RFACTOR)
    .def_readwrite("INFORMATION_CONTENT",&dag::Node::INFORMATION_CONTENT)
    .def_readwrite("EXPECTED_INFORMATION_CONTENT",&dag::Node::EXPECTED_INFORMATION_CONTENT)
    .def_readwrite("SAD_INFORMATION_CONTENT",&dag::Node::SAD_INFORMATION_CONTENT)
    .def_readwrite("SAD_EXPECTED_LLG",&dag::Node::SAD_EXPECTED_LLG)
    .def_readwrite("RELLG",&dag::Node::RELLG)
    .def_readwrite("SIGNAL_RESOLUTION",&dag::Node::SIGNAL_RESOLUTION)
    .def_readwrite("SIGNAL_RESOLUTION_PHASED",&dag::Node::SIGNAL_RESOLUTION_PHASED)
    .def_readwrite("SEEK_COMPONENTS",&dag::Node::SEEK_COMPONENTS)
    .def_readwrite("SEEK_RESOLUTION",&dag::Node::SEEK_RESOLUTION)
    .def_readwrite("SEEK_RESOLUTION_PHASED",&dag::Node::SEEK_RESOLUTION_PHASED)
    .def_readwrite("SEEK_COMPONENTS_ELLG",&dag::Node::SEEK_COMPONENTS_ELLG)
    .def_readwrite("SEEK_COMPLETE",&dag::Node::SEEK_COMPLETE)
    .def_readwrite("DATA_SCALE",&dag::Node::DATA_SCALE)
    .add_property("PARENTS",make_getter(&dag::Node::PARENTS, rbv()))
    .add_property("HOLD",make_getter(&dag::Node::HOLD, rbv()))
    .add_property("SEEK",make_getter(&dag::Node::SEEK, rbv()))
    .add_property("POSE",make_getter(&dag::Node::POSE, rbv()))
    .def_readwrite("FULL",&dag::Node::FULL)
    .def_readwrite("generic_flag",&dag::Node::generic_flag)
    .def_readwrite("PART",&dag::Node::PART)
  //  .def("phil_str",&dag::Node::phil_str) //no longer coded
  //  .def("set_star_from_pose",&dag::Node::set_star_from_pose)
    .def("logNode",&dag::Node::logNode) //returns af_string
  //  .def("logPose",&dag::Node::logPose) //returns af_string
    .def("logAnnotation",&dag::Node::logAnnotation) //returns af_string
    .def("set_cell",&dag::Node::set_cell)
    .def("pose_info",&dag::Node::pose_info)
    .def("sg_info",&dag::Node::sg_info)
    .def("tncs_info",&dag::Node::tncs_info)
    .def("scattering_seek",&dag::Node::scattering_seek)
    .def("fraction_scattering_seek",&dag::Node::fraction_scattering_seek)
  ;

  class_<dag::NodeList>("NodeList",init<>())
    .add_property("LIST",make_getter(&dag::NodeList::node_list, rbv()))
    .def("size",&dag::NodeList::size)
    .def("number_of_holds",&dag::NodeList::number_of_holds)
    .def("number_of_seeks",&dag::NodeList::number_of_seeks)
    .def("number_of_poses",&dag::NodeList::number_of_poses)
    .def("list_of_modlid",&dag::NodeList::list_of_modlid)
    .def("set_node",&dag::NodeList::set_node) //setter because python array is copy-only access
    .def("top_llg",&dag::NodeList::top_llg)
    .def("top_tfz",&dag::NodeList::top_tfz)
    .def("top_tfz_packs",&dag::NodeList::top_tfz_packs)
    .def("clear",&dag::NodeList::clear)
    .def("resize",&dag::NodeList::resize)
    .def("erase_invalid",&dag::NodeList::erase_invalid)
    .def("duplicate_equivalent",&dag::NodeList::duplicate_equivalent)
  ;

  class_<dag::Full>("Full",init<>())
    .def_readwrite("IDENTIFIER",&dag::Full::IDENTIFIER)
    .def_readwrite("PRESENT",&dag::Full::PRESENT)
    .def_readwrite("FS",&dag::Full::FS)
  ;

  class_<dag::Part>("Part",init<>())
    .def_readwrite("IDENTIFIER",&dag::Part::IDENTIFIER)
    .def_readwrite("PRESENT",&dag::Part::PRESENT)
  ;

  class_<dag::Hold>("Hold",init<>())
    .def_readwrite("IDENTIFIER",&dag::Hold::IDENTIFIER)
  ;

  class_<dag::Seek>("Seek",init<>())
    .enable_pickling()
    .def_readwrite("IDENTIFIER",&dag::Seek::IDENTIFIER)
    .def_readwrite("STAR",&dag::Seek::STAR)
    .def("set_search",&dag::Seek::set_search)
    .def("set_found",&dag::Seek::set_found)
    .def("set_missing",&dag::Seek::set_missing)
  ;

  class_<dag::Pose>("Pose",init<>())
    .def_readwrite("IDENTIFIER",&dag::Pose::IDENTIFIER)
    .def_readwrite("EULER",&dag::Pose::EULER)
    .def_readwrite("FRACT",&dag::Pose::FRACT)
    .def_readwrite("BFAC",&dag::Pose::BFAC)
    .def_readwrite("MULT",&dag::Pose::MULT)
  ;

  class_<dag::DagDatabase>("DagDatabase",init<>())
    .def("set_filesystem",&dag::DagDatabase::set_filesystem)
    .def("clear_holds",&dag::DagDatabase::clear_holds)
    .def("clear_unused_entries",&dag::DagDatabase::clear_unused_entries)
    .def("initialize_for_twilight_zone",&dag::DagDatabase::initialize_for_twilight_zone)
    .def("lookup_reflid",&dag::DagDatabase::lookup_reflid)
    .def("lookup_modlid",&dag::DagDatabase::lookup_modlid)
    .def("lookup_tag",&dag::DagDatabase::lookup_tag)
    .def("shift_tracker",&dag::DagDatabase::shift_tracker)
    .def("parse_cards", &dag::DagDatabase::parse_cards)
    .def("phil_str",&dag::DagDatabase::phil_str)
    .def("cards",&dag::DagDatabase::cards,
          ( arg( "cards" ) ) )
    .def("size",&dag::DagDatabase::size)
    .def("clear",&dag::DagDatabase::clear)
    .def("solved",&dag::DagDatabase::solved,
          ( arg( "llg" ), arg( "tfz" ) ) )
    .def("calculate_duplicate_poses", &dag::DagDatabase::calculate_duplicate_poses)
    .def("logEntries", &dag::DagDatabase::logEntries)
    .def("add_entry_copy_header", &dag::DagDatabase::add_entry_copy_header)
    //DagReadWrite.cpp
    .def("read_file", &dag::DagDatabase::read_file)
    //DagBoostPython.cpp
    .def("clear_cards",&dag::DagDatabase::clear_cards)
    .def("clear_seeks", &dag::DagDatabase::clear_seeks)
    .def("clear_poses_add_seeks", &dag::DagDatabase::clear_poses_add_seeks)
    .def("clear_seeks_add_seeks", &dag::DagDatabase::clear_seeks_add_seeks)
    .def("seek_info_array",&dag::DagDatabase::seek_info_array)
    .def("seek_info_all",&dag::DagDatabase::seek_info_all)
    .def("seek_info_current_number",&dag::DagDatabase::seek_info_current_number)
    .def("seek_info_current_name",&dag::DagDatabase::seek_info_current_name)
    .def("permutation_info_all",&dag::DagDatabase::permutation_info_all)
    .def("seek_next",&dag::DagDatabase::seek_next)
    .def("number_of_seek_loops_first",&dag::DagDatabase::number_of_seek_loops_first)
    .def("number_of_seek_loops_final",&dag::DagDatabase::number_of_seek_loops_final)
    .def("constituents_complete",&dag::DagDatabase::constituents_complete)
    .def("first_constituents_found",&dag::DagDatabase::first_constituents_found)
    .def("increment_current_seeks_with_identifier",&dag::DagDatabase::increment_current_seeks_with_identifier)
    .def("reset_seek_components_from_pose",&dag::DagDatabase::reset_seek_components_from_pose)
    .def("set_join",&dag::DagDatabase::set_join)
    .def("list_of_current_seeks",&dag::DagDatabase::list_of_current_seeks)
    .def("list_of_all_seeks",&dag::DagDatabase::list_of_all_seeks)
    .def("append_entry_hold", &dag::DagDatabase::append_entry_hold)
    .def("parse_dogtags", &dag::DagDatabase::parse_dogtags)
    .def("unparse_dogtags", &dag::DagDatabase::unparse_dogtags)
    .def("entry_unparse_modlid", &dag::DagDatabase::entry_unparse_modlid)
    .def("solved_tfz", &dag::DagDatabase::solved_tfz)
    .def_readwrite("ENTRIES",&dag::DagDatabase::ENTRIES)
    .def_readwrite("NODES",&dag::DagDatabase::NODES)
    .def_readwrite("PATHWAY", &dag::DagDatabase::PATHWAY)
    .def_readwrite("PATHLOG", &dag::DagDatabase::PATHLOG)
  ;

  class_<FileSystem>("FileSystem",init<>())
    .def(init<std::string,std::string,std::string>())
    .def("set_filename",&FileSystem::set_filename)
    .def("fstr",&FileSystem::fstr)
    .def("qfstrq",&FileSystem::qfstrq)
    .def("write",&FileSystem::write)
    .def("parent_path",&FileSystem::parent_path)
    .def("flat_filesystem",&FileSystem::flat_filesystem)
  ;

  class_<Header>("Header",init<>())
    .enable_pickling()
    .def_readonly("NREFL",&Header::NREFL)
    .def("set_history",&Header::SetHistory)
  ;

  class_<type::mtzcol>("mtzcol",init<char,std::string,labin::col>())
    .enable_pickling()
  ;

  //this is the class for storage of reflections in Phasertng
  class_<ReflColsMap, bases<Header,FileSystem> >("Reflections",init<int>()) //change name
    .enable_pickling()
    .def(init<
             af_string,
             std::string,
             af::double6,
             double,
             af_millnx,
             std::map<labin::col,af_double>,
             std::map<labin::col,af_int>,
             std::map<labin::col,af_bool>,
             std::map<friedel::type,af_bool>
             >())
    .def("write_to_disk",&ReflColsMap::write_to_disk)
    .def("read_from_disk",&ReflColsMap::read_from_disk)
    .def("in_memory",&ReflColsMap::in_memory)
    .def_readonly("WAVELENGTH",&ReflColsMap::WAVELENGTH)
    .def_readonly("TOTAL_SCATTERING",&ReflColsMap::TOTAL_SCATTERING)
    .def_readonly("TNCS_ORDER",&ReflColsMap::TNCS_ORDER)
    .def_readonly("Z",&ReflColsMap::Z)
    .def_readonly("SG",&ReflColsMap::SG)
    .add_property("MILLER",make_getter( &ReflColsMap::MILLER, rbv()), make_setter(&ReflColsMap::MILLER))
    .def("get_history",&ReflColsMap::get_history) //deepcopy
    .def("get_hall",&ReflColsMap::get_hall) //deepcopy
    .def("get_cell",&ReflColsMap::get_cell) //deepcopy
    .def("get_map_flt",&ReflColsMap::get_map_flt) //deepcopy
    .def("get_map_int",&ReflColsMap::get_map_int) //deepcopy
    .def("get_map_bln",&ReflColsMap::get_map_bln) //deepcopy
    .def("get_map_fdl",&ReflColsMap::get_map_fdl) //deepcopy
    .def("get_totalscat",&ReflColsMap::get_totalscat)
    .def("get_ztotalscat",&ReflColsMap::get_ztotalscat)
    .def("get_tncsinfo",&ReflColsMap::get_tncsinfo)
    .def("get_crystal",&ReflColsMap::get_crystal)
    .def("data_hires",&ReflColsMap::data_hires)
    .def_readonly("REFLID",&ReflColsMap::REFLID)
    .def_readonly("Z",&ReflColsMap::Z)
  ;

  class_<SpaceGroup>("SpaceGroup")
    .enable_pickling()
    .def(init<>())
    .def(init<const std::string>())
    .def_readwrite("HALL",&SpaceGroup::HALL)
    .def_readwrite("CCP4",&SpaceGroup::CCP4)
    .def("str",&SpaceGroup::str)
    .def("is_polar",&SpaceGroup::is_polar)
    .def("is_p1",&SpaceGroup::is_p1)
  ;

  class_<UnitCell>("UnitCell")
    .enable_pickling()
    .def( init<const af::double6>() )
  ;

  using namespace scitbx::boost_python::container_conversions;
  tuple_mapping<std::vector<std::string>, variable_capacity_policy >();
  tuple_mapping<std::vector<Identifier> , variable_capacity_policy >();
  //tuple_mapping<std::vector<uuid_t> , variable_capacity_policy >();
  tuple_mapping<std::vector<DogTag> , variable_capacity_policy >();
  tuple_mapping<std::vector<dag::Entry> , variable_capacity_policy >();
  tuple_mapping<std::vector<dag::Node> , variable_capacity_policy >();
  tuple_mapping<std::vector<dag::Hold> , variable_capacity_policy >();
  tuple_mapping<std::vector<dag::Seek> , variable_capacity_policy >();
  tuple_mapping<std::vector<dag::Pose> , variable_capacity_policy >();
  tuple_mapping<std::vector<dvect3> , variable_capacity_policy >();
  tuple_mapping<af::shared<Identifier> , variable_capacity_policy >();
  tuple_mapping<std::vector<std::tuple<std::string,std::string,Identifier>> , variable_capacity_policy >();
  boost_adaptbx::std_triple_conversions::to_and_from_tuple<std::string,std::string,Identifier >();

  boost::python::register_ptr_to_python<std::shared_ptr<dag::Node>>();
  boost::python::register_ptr_to_python<std::shared_ptr<ReflColsMap>>();
}

} // namespace phasertng::<anonymous>

void init_phasertng_dag()
{
  wrap_phasertng_dag();
}

} // namespace boost_python
} // namespace phasertng
