//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_dag_Term_class__
#define __phasertng_dag_Term_class__
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>

typedef scitbx::vec3<double> dvect3;
typedef scitbx::mat3<double> dmat33;

namespace phasertng {
namespace dag {

  class Term
  { //precalculated rotation and translation data
    public:
      Term() {}
      Term(dmat33 Q1tr_,dvect3 TRA_,dmat33 Rtr_Dtr_) : Q1tr(Q1tr_),TRA(TRA_),Rtr_Dtr(Rtr_Dtr_) {}
      dmat33 Q1tr =  {1,0,0,0,1,0,0,0,1};
      dvect3 TRA = {0,0,0};
      dmat33 Rtr_Dtr = {1,0,0,0,1,0,0,0,1};
  };

}} //phasertng
#endif
