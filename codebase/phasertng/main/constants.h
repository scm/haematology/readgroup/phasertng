//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_constants__
#define __phasertng_constants__
#include <string>

//literals only
constexpr int    DEF_WAVELENGTH       = 1;
constexpr int    DEF_TAB              = 2;
constexpr int    DEF_LOGFILE_WIDTH    = 80;
constexpr double DEF_UCART_FACTOR     = 10000.;
constexpr double DEF_LORES            = 10000;
constexpr double DEF_PPH              = 1.0e-02;
constexpr double DEF_PPT              = 1.0e-03;
constexpr double DEF_PPM              = 1.0e-06;
constexpr double DEF_PPB              = 1.0e-09;
constexpr double DEF_TWOPISQ_ON_THREE = 6.579736267392905; //double is 15 decimal places
constexpr double DEF_HEXFACTOR        = (2./3.*(0.5*1.73205080757));
constexpr int    DEF_UUID_FLAG = 0;
constexpr int    DEF_INT_FLAG = -999;
constexpr double DEF_FLT_FLAG = -999.;
constexpr int    DEF_CLMN_LMIN = 4;
constexpr int    DEF_CLMN_LMAX = 100;
constexpr bool   DEF_CLMN_RESO = false;
constexpr double DEF_BFAC_MIN = 0;
constexpr double DEF_BFAC_MAX = 500;

namespace phasertng {

namespace constants {
  static const std::string remark_tag = "PHASER";
  static const std::string pdb_remark_number = "471"; //previously PHASER
  static const std::string pdb_remark_atom_number = "472"; //previously PHASER
  static const std::string remark_identifier = " ID"; //with space
}

} //phasertng

#endif
