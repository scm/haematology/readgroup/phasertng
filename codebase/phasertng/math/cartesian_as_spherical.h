#ifndef __phasertng_cartesian_as_spherical_function__
#define __phasertng_cartesian_as_spherical_function__
#include <scitbx/constants.h>

namespace phasertng {

class cartesian_as_spherical
{
  double radius_=0;
  double phi=0; //azimuth
  double theta=0; //inclination

  public:
  cartesian_as_spherical(dvect3 v)
  {
    double& x = v[0];
    double& y = v[1];
    double& z = v[2];
    radius_ = std::sqrt(x*x+y*y+z*z);
    if (radius_ > 0)
    {
      //theta is the zenith angle
      //phi is azimuthal angle
      //this is the same as molrep, so we are going with that
      theta = std::acos(z)/radius_;
      if (x > 0)
        phi = atan(y/x);
      else if (x < 0)
        phi = std::atan(y/x) + scitbx::constants::pi;
      else
        phi = scitbx::constants::pi/2.;
      if (phi > scitbx::constants::pi)
        phi -= scitbx::constants::pi*2; //range -180->180
    }
  }

  double radius()
  { return radius_; }

  double theta_degrees()
  { return scitbx::rad_as_deg(theta); }

  af::double2 phi_theta_degrees()
  { return af::double2(scitbx::rad_as_deg(phi),scitbx::rad_as_deg(theta)); }

  //corrects to standard  0-360 0-90 0-360
  //chi is also interpreted as -chi for the order
  af::double3 phi_theta_degrees(double chi)
  {
    if (std::abs(chi) < DEF_PPH)
      return af::double3(0,0,0); //axis direction does not matter
    af::double3 corrected(scitbx::rad_as_deg(phi),scitbx::rad_as_deg(theta),chi);
    if (corrected[1] > 90) //zenith
    { //reverse direction of rotation
      corrected[1] = 180-corrected[1]; //angle from the antipole
      corrected[0] += 180; //azimuthal, inverted
      corrected[2] *= -1; //reverse rotation direction
    }
    for (auto i : {0,2})
    {
      while (corrected[i] < 0) corrected[i] += 360;
      while (corrected[i] >=360) corrected[i] -= 360;
    }
    return corrected;
  }

  af::double2 phi_theta_radians()
  { return af::double2(phi,theta); }
};

}
#endif
