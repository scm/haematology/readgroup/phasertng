#ifndef __phasertng_mmdb_class__
#define __phasertng_mmdb_class__
#include <string>

namespace phasertng {

class MMDB
{
  public:
  MMDB() {}
  ~MMDB() {}
  std::string ReadCIF(const std::string filename,std::string& CIF_ERRORS);
};

} //phasertng
#endif
