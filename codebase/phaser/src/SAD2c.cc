//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/main/Phaser.h>
#include <phaser/src/RefineSAD.h>
#include <phasertng/main/Error.h>
#include <phaser/lib/maths.h>
#include <phaser/src/Integration.h>

namespace phaser {

// --  centric reflections

centric
RefineSAD::centricReflIntgSAD2(unsigned& r)
{
//the amplitude for centrics is taken from Fpos
  centric thisIntg;
  floatType SigmaNeg  = sigDsqr[r] + 0.5*fn::pow2(POS.SIGF[r]); // named for consistency with acentric case
  assert(SigmaNeg > 0.);
  floatType FH        = std::abs(FHpos[r]);
  floatType FO        = POS.F[r];
  floatType twoVar    = 2*SigmaNeg;
  floatType normFac   = (1/std::sqrt(scitbx::constants::two_pi*SigmaNeg));
  //account for whether FH is parallel or antiparallel to phase restriction
  floatType cosdphi   = (std::cos(std::arg(FHpos[r])-phsr[r]) > 0) ? 1. : -1.;
  static floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxExpArgHalf(maxExpArg/2.); // Plus/minus limits to avoid over/underflows
  floatType exparg1   = -fn::pow2(FO-cosdphi*FH)/twoVar;
  floatType exparg2   = -fn::pow2(FO+cosdphi*FH)/twoVar;
  floatType expArgOffset = maxExpArgHalf-std::max(exparg1,exparg2);
  if (std::abs(exparg1-exparg2)>maxExpArg)
  {
    if (exparg1<exparg2) exparg1 = exparg2 - maxExpArg;
    else exparg2 = exparg1 - maxExpArg;
  }
  floatType prob1     = std::exp(exparg1+expArgOffset)*normFac;
  floatType prob2     = std::exp(exparg2+expArgOffset)*normFac;
  floatType Like      = prob1+prob2;
  assert(Like > 0.);
  thisIntg.LogLike  = -log(Like) + expArgOffset;
  thisIntg.setProb(prob1,prob2,phsr[r]);
  return thisIntg;
}  //centricReflSAD

floatType
RefineSAD::centricReflGradSAD2(unsigned& r,bool atomic,bool sigmaa)
{
//#define __FD_GRAD_CENT__
#ifdef __FD_GRAD_CENT__
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  cmplxType epsilon(shift,0); cmplxType *test = &(FHpos[r]);
  floatType &dL_by_dtest = dL_by_dReFHpos;
  floatType fLogLike = centricReflIntgSAD2(r).LogLike;
  (*test) += epsilon;
  floatType DLogLikeForward = centricReflIntgSAD2(r).LogLike;
  floatType FD_test_forward = (DLogLikeForward-fLogLike)/shift;
  (*test) -= epsilon;
  (*test) -= epsilon;
  floatType DLogLikeBackward = centricReflIntgSAD2(r).LogLike;
  floatType FD_test_backward = (DLogLikeBackward-fLogLike)/shift;
  (*test) += epsilon;
#endif
  static floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxExpArgHalf(maxExpArg/2.); // Plus/minus limits to avoid over/underflows

  //initialisation
  dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0;
  dL_by_dSDsqr = dL_by_dSA = dL_by_dSB = dL_by_dSP = 0;

  floatType SigmaNeg  = sigDsqr[r] + 0.5*fn::pow2(POS.SIGF[r]); // named for consistency with acentric case
  floatType FH        = std::abs(FHpos[r]);
  floatType FO        = POS.F[r];
  floatType twoVar    = 2*SigmaNeg;
  floatType normFac   = (1/std::sqrt(scitbx::constants::two_pi*SigmaNeg));
  //account for whether FH is parallel or antiparallel to phase restriction
  floatType phipos    = std::arg(FHpos[r]);
  floatType cosdphi   = (std::cos(phipos-phsr[r]) > 0) ? 1. : -1.;
  floatType cosdphiFH = cosdphi*FH;
  floatType exparg1   = fn::pow2(FO-cosdphiFH)/twoVar;
  floatType exparg2   = fn::pow2(FO+cosdphiFH)/twoVar;
  floatType expArgOffset = maxExpArgHalf+std::min(exparg1,exparg2);
  if (std::abs(exparg1-exparg2)>maxExpArg)
  {
    if (exparg1>exparg2) exparg1 = exparg2 + maxExpArg;
    else exparg2 = exparg1 + maxExpArg;
  }
  floatType prob1     = std::exp(-exparg1+expArgOffset)*normFac;
  floatType prob2     = std::exp(-exparg2+expArgOffset)*normFac;
  floatType Like      = prob1+prob2;
  assert(Like > 0.);

  if (atomic)
  {
    floatType dL_by_dFH = -(cosdphi/SigmaNeg)*((FO-cosdphiFH)*prob1-(FO+cosdphiFH)*prob2);
    dL_by_dReFHpos = dL_by_dFH*std::cos(phipos);
    dL_by_dImFHpos = dL_by_dFH*std::sin(phipos);
  }

  if (sigmaa)
  {
    dL_by_dSDsqr = (prob1*(0.5-exparg1)+prob2*(0.5-exparg2))/SigmaNeg;
  }

#ifdef __FD_GRAD_CENT__
//if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
   std::cout << "FO " << FO << std::endl;
   std::cout << "FHpos[r] " << FHpos[r] << std::endl;
   std::cout << "sigDsqr[r] " << sigDsqr[r] << std::endl;
   std::cout << "VarFOpos[r] " << VarFOpos[r] << std::endl;
   std::cout << "SigmaNeg " << SigmaNeg << std::endl;
   std::cout << "normFac " << normFac << std::endl;
   std::cout << "P1 " << prob1 << " P2 " << prob2 << std::endl;
   std::cout << "Like " << Like  << std::endl;
   std::cout << "dL_by_dReFHpos " << dL_by_dReFHpos << std::endl;
   std::cout << "dL_by_dImFHpos " << dL_by_dImFHpos << std::endl;
   std::cout << "dL_by_dSDsqr " << dL_by_dSDsqr << std::endl;
}
#endif

  floatType LogLike(-log(Like)+expArgOffset);
  dL_by_dReFHpos /= Like;
  dL_by_dImFHpos /= Like;
  dL_by_dSDsqr   *= epsn[r]/Like;

#ifdef __FD_GRAD_CENT__
  std::cout << "=== First Derivative FD test (r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest << " Forward " << FD_test_forward << " Backward " << FD_test_backward << std::endl;
  std::cout << "Ratio forward " << dL_by_dtest/FD_test_forward <<
                   " backward " << dL_by_dtest/FD_test_backward << "\n";
  //if (r >= std::atof(getenv("PHASER_TEST_NREFL")) || std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
  if (r >= std::atof(getenv("PHASER_TEST_NREFL")) ) std::exit(1);
#endif
  return LogLike;
}

floatType
RefineSAD::centricReflHessSAD2(unsigned& r,bool atomic,bool sigmaa)
{
//#define __FD_HESS_CENT__
#ifdef __FD_HESS_CENT__
 if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_MATHEMATICA") == 0)
    { std::cout << "setenv PHASER_TEST_MATHEMATICA\n" ; std::exit(1); }
 floatType shift_a = std::atof(getenv("PHASER_TEST_SHIFT"));
 floatType shift_b = std::atof(getenv("PHASER_TEST_SHIFT"));
  //tests single first _a, _b and same or mixed second derivatives _a_b
  //epsilons and test type depend on types of derivatives
  //d2L_by_dReFHpos_dReFHneg etc
if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
///mathematica file
std::cout << "===Set Mathematica===\n";
FHpos[r] = cmplxType(21.5104,0.615763);
POS.F[r] = 14.012;
VarFOpos[r] = 0;
sigDsqr[r] = 27692.7;
}
#if 0
  cmplxType epsilon_a(shift_a,0);
  cmplxType epsilon_b(shift_b,0);
  cmplxType *test_a = &(FHpos[r]);
  cmplxType *test_b = &(FHpos[r]);
  floatType &d2L_by_dtest2 = d2L_by_dReFHpos2;
#endif
#if 0
  cmplxType epsilon_a(0,shift_a);
  floatType epsilon_b(shift_b);
  cmplxType *test_a = &(FHneg[r]);
  floatType *test_b = &(sigDsqr[r]);
  floatType &d2L_by_dtest2 = d2L_by_dImFHneg_dSDsqr;
#endif

  floatType f_start = acentricReflGradSAD2(r,true,true);
  (*test_a) += shift_a;
  floatType f_forward = acentricReflGradSAD2(r,true,true);
  (*test_a) -= shift_a;
  (*test_a) -= shift_a;
  floatType f_backward = acentricReflGradSAD2(r,true,true);
  (*test_a) += shift_a;
  floatType FD_test_on_f = (f_forward-2*f_start+f_backward)/shift_a/shift_a;

#endif

  //initialization
  dL_by_dReFHpos = dL_by_dImFHpos = dL_by_dReFHneg = dL_by_dImFHneg = 0;
  d2L_by_dReFHpos2 = d2L_by_dReFHpos_dImFHpos = d2L_by_dReFHpos_dReFHneg = d2L_by_dReFHpos_dImFHneg = 0;
  d2L_by_dImFHpos2 = d2L_by_dImFHpos_dReFHneg = d2L_by_dImFHpos_dImFHneg = 0;
  d2L_by_dReFHneg2 = d2L_by_dReFHneg_dImFHneg = 0;
  d2L_by_dImFHneg2 = 0;
  dL_by_dSDsqr = dL_by_dSA = dL_by_dSB = dL_by_dSP = 0;
  d2L_by_dSDsqr2 = d2L_by_dSA2 = d2L_by_dSB2 = d2L_by_dSP2 = 0;

  floatType SigmaNeg  = sigDsqr[r] + 0.5*fn::pow2(POS.SIGF[r]); // named for consistency with acentric case
  floatType FH        = std::abs(FHpos[r]);
  floatType FO        = POS.F[r];
  floatType twoVar    = 2*SigmaNeg;
  floatType normFac   = (1/std::sqrt(scitbx::constants::two_pi*SigmaNeg));
  //account for whether FH is parallel or antiparallel to phase restriction
  floatType phipos    = std::arg(FHpos[r]);
  floatType cosphipos = std::cos(phipos);
  floatType sinphipos = std::sin(phipos);
  floatType cosdphi   = (std::cos(phipos-phsr[r]) > 0) ? 1. : -1.;
  floatType cosdphiFH = cosdphi*FH;
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  static floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxExpArgHalf(maxExpArg/2.); // maximum abs(arg) allowed to avoid under/overflows
  floatType exparg1   = fn::pow2(FO-cosdphiFH)/twoVar;
  exparg1 = std::min(std::max(exparg1,-maxExpArgHalf),maxExpArgHalf); // ensure that it is within limits
  floatType exparg2   = fn::pow2(FO+cosdphiFH)/twoVar;
  exparg2 = std::min(std::max(exparg2,-maxExpArgHalf),maxExpArgHalf);
  floatType prob1     = std::exp(-exparg1)*normFac;
  floatType prob2     = std::exp(-exparg2)*normFac;
  floatType Like      = prob1+prob2;
  assert(Like > 0.);

  floatType dldsdsqr(0.);
  if (atomic || sigmaa)
  {
    dldsdsqr = (prob1*(0.5-exparg1)+prob2*(0.5-exparg2))/SigmaNeg;
  }
  if (atomic)
  {
    floatType dL_by_dFH = -(cosdphi/SigmaNeg)*((FO-cosdphiFH)*prob1-(FO+cosdphiFH)*prob2);
    dL_by_dReFHpos = dL_by_dFH*cosphipos;
    dL_by_dImFHpos = dL_by_dFH*sinphipos;
    floatType term2 = FH ? dL_by_dFH/FH : 0.;
    floatType term1 = -2*dldsdsqr+term2;
    d2L_by_dReFHpos2 = term1*fn::pow2(cosphipos) - term2;
    d2L_by_dImFHpos2 = term1*fn::pow2(sinphipos) - term2;
    d2L_by_dReFHpos_dImFHpos = term1*cosphipos*sinphipos;

  }

  if (sigmaa)
  {
    dL_by_dSDsqr = dldsdsqr;
    floatType term1b = (1/(4*fn::pow4(SigmaNeg)))*(fn::pow4(FO-cosdphiFH)*prob1+fn::pow4(FO+cosdphiFH)*prob2);
    floatType term2b = (3/(2*fn::pow3(SigmaNeg)))*(-fn::pow2(FO-cosdphiFH)*prob1-fn::pow2(FO+cosdphiFH)*prob2);
    floatType term3b = (3/(4*fn::pow2(SigmaNeg)))*(prob1+prob2);
    d2L_by_dSDsqr2 = (term1b + term2b + term3b);
  }

//before correcting for logarithm
#ifdef __FD_HESS_CENT__
//if (std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1)
{
   std::cout << "FHpos[r] " << FHpos[r] << std::endl;
   std::cout << "FH " << FH << std::endl;
   std::cout << "FO " << FO << std::endl;
   std::cout << "SigmaNeg " << SigmaNeg << std::endl;
   std::cout << "phsr " << phsr[r] << std::endl;
   std::cout << "cosdphi " << cosdphi << std::endl;
   std::cout << "Like " << Like  << " prob1 " << prob1 << " prob2 " << prob2 << std::endl;
   std::cout << "dL_by_dReFHpos " << dL_by_dReFHpos << std::endl;
   std::cout << "dL_by_dImFHpos " << dL_by_dImFHpos << std::endl;
   std::cout << "dL_by_dSDsqr " << dL_by_dSDsqr << std::endl;
   std::cout << "d2L_by_dReFHpos2 " << d2L_by_dReFHpos2 << std::endl;
   std::cout << "d2L_by_dReFHpos_dImFHpos " << d2L_by_dReFHpos_dImFHpos << std::endl;
   std::cout << "d2L_by_dImFHpos2 " << d2L_by_dImFHpos2 << std::endl;
   std::cout << "d2L_by_dSDsqr2 " << d2L_by_dSDsqr2 << std::endl;
}
#endif

  floatType LogLike(-log(Like));
  floatType LikeFactor(1/Like);
//from acentric
  d2L_by_dReFHpos2         = fn::pow2(dL_by_dReFHpos*LikeFactor) - d2L_by_dReFHpos2*LikeFactor;
  d2L_by_dReFHpos_dImFHpos = (dL_by_dReFHpos*LikeFactor)*(dL_by_dImFHpos*LikeFactor) - d2L_by_dReFHpos_dImFHpos*LikeFactor;
  d2L_by_dImFHpos2         = fn::pow2(dL_by_dImFHpos*LikeFactor) - d2L_by_dImFHpos2*LikeFactor;
  d2L_by_dSDsqr2           = fn::pow2(epsn[r])*(fn::pow2(dL_by_dSDsqr*LikeFactor) - d2L_by_dSDsqr2*LikeFactor);
  //now correct gradients as these are also required for the chain rule later
  dL_by_dReFHpos *= LikeFactor;
  dL_by_dImFHpos *= LikeFactor;
  dL_by_dSDsqr   *= epsn[r]*LikeFactor;

#ifdef __FD_HESS_CENT__
  std::cout << "=== Second Derivative FD test (r=" << r << ") ===\n";
  std::cout << "Analytic " << d2L_by_dtest2 << " on Function " << FD_test_on_f << std::endl;
  std::cout << "Ratio " << d2L_by_dtest2/FD_test_on_f << "\n";
  if (r >= std::atof(getenv("PHASER_TEST_NREFL")) || std::atof(getenv("PHASER_TEST_MATHEMATICA")) == 1) std::exit(1);
#endif

  return LogLike;
}

}//phaser
