//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Elmn_class__
#define __phasertng_Elmn_class__
#include <phasertng/main/includes.h>
#include <phasertng/math/HKL_clustered.h>
#include <phasertng/math/table/sphbessel.h>

//#define PHASERTNG_DEBUG_OLD_ELMN_ARRAY
#ifdef PHASERTNG_DEBUG_OLD_ELMN_ARRAY
namespace phasertng {
#include <vector>
  typedef std::vector<std::vector<std::vector< cmplex >>> elmn_array_t;
}
#else
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>
  typedef scitbx::af::versa<phasertng::cmplex, scitbx::af::c_grid<3> >   elmn_array_t;
#endif

namespace phasertng {

  extern const table::sphbessel& tbl_sphbessel;

  class HKL_clustered; //forward declaration

  class Elmn
  {
    public:
      Elmn(int LMAX_=0):
          LMAX(LMAX_) {}
      int LMAX = 0;
      elmn_array_t ELMN;
      int NELMN = 0;
      int NSPHARM = 0;

    private:
      bool USE_STRICTLY_NTHREADS = false;
      int NTHREADS = 1;

    public:
      void set_nthreads(int n,bool b)
      { NTHREADS = n; USE_STRICTLY_NTHREADS = b; }
      af_string hkl_to_elmn(
        const HKL_clustered&,
        const int,
        const std::pair<int,char>
      );
      bool nonzero();

    protected:
      void resize_elmn(int);

    private:
      void clear_elmn();
      sv_cmplex partial_elmn_calculation(
        const HKL_clustered&,
        const int,
        const std::pair<int,char>
      );
  };

}//phasertng
#endif
