#ifndef __phasertng_alternative_origins_table__
#define __phasertng_alternative_origins_table__
#include <vector>
#include <scitbx/vec3.h>

namespace phasertng {
namespace table {

typedef scitbx::vec3<std::string> svect3;

inline bool in_range(int i,int m)
{ return (i==m); }
inline bool in_range(int i,int m, int n)
{ return (i>=m and i <=n); }

  class alternative_origins
  {
    std::vector<svect3> altorig;

    alternative_origins(int i)
    {
      if (in_range(i,1))
        altorig = {svect3("x","y","z")};
      else if (in_range(i,3,4))
        altorig = { svect3("0.0","y","0.0"),
                    svect3("0.0","y","1/2"),
                    svect3("1/2","y","0.0"),
                    svect3("1/2","y","1/2")};
      else if (in_range(i,5))
        altorig = { svect3("0.0","y","0.0"),
                    svect3("0.0","y","1/2") };
      else if (in_range(i,16,19))
        altorig = { svect3("0.0","0.0","1/2"),
                    svect3("0.0","1/2","1/2"),
                    svect3("1/2","0.0","0.0"),
                    svect3("0.0","1/2","0.0"),
                    svect3("1/2","1/2","0.0"),
                    svect3("1/2","0.0","1/2"),
                    svect3("1/2","1/2","1/2")};
      else if (in_range(i,20,21))
        altorig = { svect3("0.0","0.0","1/2"),
                    svect3("1/2","0.0","0.0"),
                    svect3("1/2","0.0","1/2")};
      else if (in_range(i,22))
        altorig = { svect3("1/4","1/4","1/4"),
                    svect3("1/2","1/2","1/2"),
                    svect3("3/4","3/4","3/4") };
      else if (in_range(i,23,24))
        altorig = { svect3("0.0","0.0","1/2"),
                    svect3("0.0","1/2","0.0"),
                    svect3("1/2","0.0","0.0")};
      else if (in_range(i,75,78))
        altorig = { svect3("0.0","0.0","z"),
                    svect3("1/2","1/2","z")};
      else if (in_range(i,79,80))
        altorig = { svect3("0.0","0.0","z")};
      else if (in_range(i,89,96))
        altorig = { svect3("1/2","1/2","0.0"),
                    svect3("1/2","1/2","1/2"),
                    svect3("0.0","0.0","1/2")};
      else if (in_range(i,97,98))
        altorig = { svect3("0.0","0.0","1/2") };
      else if (in_range(i,143,145))
        altorig={  svect3("1/3","2/3","z"),
                   svect3("2/3","1/3","z"),
                   svect3("0","0","z") };
      else if (in_range(i,146))
        altorig= { svect3("0.0","0.0","z") }; //H setting
      else if (in_range(i,155))
        altorig = { svect3("0.0","0.0","1/2") };
      else if (in_range(i,168,173))
        altorig={svect3("0","0","z")};
      else if (in_range(i,177,182))
        altorig={svect3("0","0","1/2")};
      else if (in_range(i,195))
        altorig={svect3("0","0","1/2")};
      else if (in_range(i,196))
        altorig = { svect3("1/4","1/4","1/4"),
                    svect3("1/2","1/2","1/2"),
                    svect3("3/4","3/4","3/4") };
      else if (in_range(i,197) or in_range(i,199))
        altorig={};
      else if (in_range(i,207,208) or in_range(i,212,213))
        altorig={svect3("1/2","1/2","1/2")};
      else if (in_range(i,209,210))
        altorig={svect3("1/2","1/2","1/2")};
      else if (in_range(i,211) or in_range(i,214))
        altorig={};
   }
 };

}}
#endif
