//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  void Phasertng::runPUT()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::DATA))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (data)");

    DAGDB.restart();
    DAGDB.WORK->HALL = REFLECTIONS->SG.HALL;
    DAGDB.WORK->CELL = REFLECTIONS->UC.cctbxUC;
    if (input.value__boolean.at(".phasertng.put_solution.clear.").value_or_default())
    {
      out::stream where = out::SUMMARY;
      logTab(where,"Poses cleared");
      DAGDB.WORK->pose_clear();
      dag::NodeList output_dag;
      output_dag.push_back(*DAGDB.WORK);
      DAGDB.NODES = output_dag;
      DAGDB.reset_entry_pointers(DataBase());
    }

    if (Suite::Extra() and DAGDB.size())
    {
      logUnderLine(out::TESTING,"Starting Dag Nodes");
      auto text = DAGDB.unparse_card_summary();
      logTabArray(out::TESTING,text);
      logBlank(out::TESTING);
    }

    if (!input.value__path.at(".phasertng.put_solution.dag.filename.").is_default())
    {
      out::stream where = out::SUMMARY;
      logUnderLine(where,"Dag Solution");
      logTab(where,"Dag read into memory");
      FileSystem dagfile(
          input.value__path.at(".phasertng.put_solution.dag.filename.").value_or_default());
      logTab(where,"Read: " + dagfile.qfstrq());
      dag::Edge patch = DAGDB.parse_cards_retaining_seeks(dagfile);
      DAGDB.reset_entry_pointers(DataBase());
      DAGDB.PATHLOG.PENULTIMATE = DAGDB.PATHLOG.ULTIMATE;
      DAGDB.PATHLOG.ULTIMATE = DAGDB.PATHWAY.ULTIMATE;
      logTab(where,"Patched from " + DAGDB.PATHWAY.ULTIMATE.str() +"<-"+ DAGDB.PATHWAY.PENULTIMATE.str());
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        DAGDB.NODES[i].set_star_from_pose();
      logTabArray(where,DAGDB.seek_info_array());
    }
    else if (!input.value__uuid_number.at(".phasertng.search.id.").is_default() or
             !input.value__string.at(".phasertng.search.tag.").is_default())
    {
      auto& work = DAGDB.WORK;
      out::stream where = out::SUMMARY;
      logUnderLine(where,"Pose Solution");
      if (DAGDB.size() != 1)
      throw Error(err::INPUT,"Dag must contain one node");
      if (DAGDB.WORK->POSE.size())//in scotty puts are added sequentially
        logTabArray(where,DAGDB.WORK->logAnnotation("",true));
      Identifier identifier(
        input.value__uuid_number.at(".phasertng.search.id.").value_or_default(),
        input.value__string.at(".phasertng.search.tag.").value_or_default());
      auto search = DAGDB.lookup_entry_tag(identifier,DataBase());
      if (search->identify().is_set_valid())
      logTab(where,"Model identifier: " + search->identify().str());

      int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

      //put solution with euler and frac is wrt original coordinate positions
      dvect3 self(0,0,0);
      work->initialize_turn(self,search);
      dvect3 eulerin = input.value__flt_vector.at(".phasertng.put_solution.pose.euler.").value_or_default();
      logTab(where,"Euler: " + dvtoss(eulerin,1));
      //change rotation to wrt mt
      const dmat33 PR = search->PRINCIPAL_ORIENTATION;
      auto& turn = work->NEXT;
      turn.EULER = dag::euler_wrt_mt_from_in(eulerin,PR);
      std::tie(std::ignore,turn.SHIFT_MATRIX) = dag::print_euler_wrt_in_from_mt(turn.EULER,PR);
      //change translation to wrt mt
      dvect3 fracin = input.value__flt_vector.at(".phasertng.put_solution.pose.fractional.").value_or_default();
      logTab(where,"Fractional: " + dvtoss(fracin,3));
      const dvect3 PT = search->PRINCIPAL_TRANSLATION;
      dvect3 fract = dag::fract_wrt_mt_from_in(fracin,turn.EULER,PT,work->CELL);
      dmat33 orthmat = work->CELL.orthogonalization_matrix();
      auto shift_ortht = orthmat*fracin;
      work->convert_turn_to_curl(fract,shift_ortht);
      if (nmol == 1 or
          !input.value__boolean.at(".phasertng.put_solution.use_tncs.").value_or_default())
      {
        work->convert_curl_to_pose({0,0,0});
      }
      else if (input.value__boolean.at(".phasertng.put_solution.use_tncs.").value_or_default())
      {
        logTab(where,"Use tNCS in placement");
        work->convert_curl_to_pose(REFLECTIONS->TNCS_VECTOR);
        logBlank(where);
      }
      work->POSE[0].ORTHR = input.value__flt_vector.at(".phasertng.put_solution.pose.orthogonal_rotation.").value_or_default();
      work->POSE[0].ORTHT = input.value__flt_vector.at(".phasertng.put_solution.pose.orthogonal_position.").value_or_default();
      double bfac = input.value__flt_number.at(".phasertng.put_solution.pose.bfactor.").value_or_default();
      work->POSE[0].BFAC = bfac;
      logTab(where,"B-factor: " + dtos(bfac,1));

      //prints fract_wrt_in_from_mt
      logTabArray(out::VERBOSE,work->logPose("Put Solution"));
      //set the star flags for the seek components corrrectly, with pose they become found
      for (int i = 0; i < DAGDB.NODES.size(); i++)
        DAGDB.NODES[i].set_star_from_pose();
    }
    else
    throw Error(err::INPUT,"Search values not set");

    //special case for the hashing, the tracker +put is not sufficient to define the dag
    //since other things in dag itself will shift the tracker
    std::string edited_mode = DAGDB.PATHWAY.ULTIMATE.tag();
    //phil_str because it is a simple string return function
    //DAGDB.shift_tracker(hashing(),edited_mode);
    DAGDB.restart();
    if (DAGDB.WORK)
    {
      std::string txt = DAGDB.cards();
      if (txt.size() > 1000)
        txt = txt.substr(0,1000);
      DAGDB.shift_tracker(hashing()+txt,edited_mode);
      if (REFLECTIONS)
      {
        double ztotalscat = REFLECTIONS->get_ztotalscat();
        double fs(DAGDB.WORK->fraction_scattering(ztotalscat));
        logUnderLine(out::LOGFILE,"Scattering");
        logTab(out::LOGFILE,"Total scattering from biological unit  = " + dtos(ztotalscat,0));
        logTab(out::LOGFILE,"Scattering from placed component       = " + dtos(DAGDB.WORK->scattering(),0));
        if (fs > 1)
          throw Error(err::INPUT,"Fraction scattering is greater than 1");
      }
    }
  }

} //phasertng
