//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_Site_class__
#define __phasertng_Site_class__
#include <phasertng/main/includes.h>
#include <phasertng/pod/pakdat.h>
#include <boost/logic/tribool.hpp>
#include <set>
#include <iso646.h>

namespace phasertng {

class Site
{
  private:
    boost::logic::tribool   isTop = boost::logic::indeterminate;
  public:
    double value = std::numeric_limits<double>::lowest();
    int    index = 0;
    dvect3 grid = dvect3(0,0,0);
    int    ifTopNumInGroup = 1;
    int    clusterNum = 1; //Number of Top peak this belongs to
    char   packs = pod::pakflag().unset(); // '?' does the top peak pack

  public:
    Site(double value_= 0) : value(value_) {}
    Site(dvect3 grid_, double value_,int index_=0) : value(value_),grid(grid_),index(index_) {}
    Site(double a,double b,double c) : grid(a,b,c),value(0),index(0) {}

    bool clustered() const
    { return !boost::logic::indeterminate(isTop); }

    bool is_topsite() const
    { return boost::logic::indeterminate(isTop) or static_cast<bool>(isTop); }

    void set_topsite_true()
    { isTop = true; }

    void set_topsite_false()
    { isTop = false; clusterNum = 1; ifTopNumInGroup = 1; }

    void clear_topsite(int i)
    { isTop = boost::logic::indeterminate; ifTopNumInGroup = 1; clusterNum = i; }

  public: //functions
    //operators mathematical only concern the value parameter, to avoid copying array
    //for mean and sigma calculation
    Site& operator+=(const Site &right)
    {
      value += right.value;
      return *this;
    }
    const Site operator+(const Site &right) const
    {
      return Site(*this) += right;
    }
    bool operator<(const Site &right) const
    {
      if (grid[0] < right.grid[0]) return true;
      if (grid[0] > right.grid[0]) return false;
      if (grid[1] < right.grid[1]) return true;
      if (grid[1] > right.grid[1]) return false;
      return grid[2] < right.grid[2];
    }
};

}
#endif
