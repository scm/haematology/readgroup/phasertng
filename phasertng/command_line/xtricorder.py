# LIBTBX_SET_DISPATCHER_NAME phasertng.xtricorder

from iotbx.cli_parser import run_program
from phasertng.programs import xtricorder

if (__name__ == '__main__'):
  try:
    run_program(xtricorder.Program)
  except Exception as e:
    print(str(e))
