//c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/data/Scatterers.h>
#include <phasertng/io/PdbRecord.h>
#include <phasertng/main/jiffy.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <cctbx/eltbx/electron_scattering.h>
#include <cctbx/eltbx/neutron.h>
#include <cctbx/eltbx/sasaki.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <phasertng/data/Cluster.h>
#include <phasertng/io/tostr2.h>

namespace phasertng {

  Scatterers::Scatterers(const boost::logic::tribool ANOMALOUS_, const double WAVELENGTH_) :
       WAVELENGTH(WAVELENGTH_)
  {
    PHASER_ASSERT(!boost::logic::indeterminate(ANOMALOUS_));
    ANOMALOUS = bool(ANOMALOUS_);
  }

  void
  Scatterers::insert(scatterType atomtype)
  {
    atomtype = tostr2(atomtype).first;
    //MOVE_TO_EDGE may be set
    //FLUORESCENCE_SCAN may be set for elements
    if (atomtype == "TX")
    {
      Cluster tx("TX");
      create_cluster_harker(atomtype,&tx);
      create_cluster_debye(atomtype,&tx);
    }
    else if (atomtype == "SS")
    {
      Cluster ss("SS");
      create_cluster_harker(atomtype,&ss);
      create_cluster_debye(atomtype,&ss);
    }
    else if (atomtype == "RX")
    {
      //no form factor available
      VDW[atomtype] = 0.01;
      FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(1.0,0.0);
    }
    else if (atomtype == "AX")
    {
      //no form factor available
      VDW[atomtype] = 0.01;
      FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(0.0,1.0);
    }
    else if (atomtype == " H" || atomtype == " D") //not in sasaki table
    {
      VDW[atomtype] = 0.01;
      FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(0.0,0.0);
    }
    else
    {
      //assert that this must be an element
      table::vanderwaals_radii radii;
      VDW[atomtype] = radii.get(atomtype);
      if (ANOMALOUS and WAVELENGTH)
      {
        //fp dfdp and associated
        if (FLUORESCENCE_SCAN.count(atomtype))
        {
          double FP; double FDP; std::string FIX;
          std::tie(FP,FDP,FIX) = FLUORESCENCE_SCAN.at(atomtype);
          FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(FP,FDP); //if this is a cluster, then all set to scan values
          FIX_FDP[atomtype] = FIX;
        }

        if (!FLUORESCENCE_SCAN.count(atomtype) or FIX_FDP[atomtype] == "edge")
        {
          cctbx::eltbx::sasaki::table sasaki(atomtype,false,false);
          if (!sasaki.is_valid())
          { //no throws from constructor
             throw Error(err::INPUT,"\"" + atomtype + "\" not in Sasaki table, give f' and f\"");
             //should have been caught in input
          }
          if (!sasaki.at_angstrom(WAVELENGTH).is_valid())
          { //no throws from constructor
            throw Error(err::INPUT,"Wavelength outside range of Sasaki tables, give f' and f\"");
             //should have been caught in input
          }
          double fdpf = sasaki.at_angstrom(WAVELENGTH+dwave).fdp();
          double fdp0 = sasaki.at_angstrom(WAVELENGTH).fdp();
          double fdpb = sasaki.at_angstrom(WAVELENGTH-dwave).fdp();
          NEAR_EDGE[atomtype] = std::fabs(fdpf-fdp0)/fdp0 > dfdp or std::fabs(fdpb-fdp0)/fdp0 > dfdp;
          if (FIX_FDP[atomtype] == "edge")
          { //here is the default for fixing the fp-fdp if the wavelength is far from an edge
            FIX_FDP[atomtype] = NEAR_EDGE[atomtype] ? "off" : "on";
          }
          // When f' and f" not given, assume wavelength near edge should be at edge.
          if (!FP_FDP.count(atomtype))
          {
            if (NEAR_EDGE[atomtype])
            {
              double peakwave(0.),peakfdp(-1000.);
              for (int i = -100; i <= 100; i++)
              {
                double testwave = WAVELENGTH+i*dwave/100.;
                double testfdp = sasaki.at_angstrom(testwave).fdp();
                if (testfdp > peakfdp)
                {
                  peakfdp = testfdp;
                  peakwave = testwave;
                  PEAKWAVE[atomtype] = peakwave;
                }
              }
              //here is the moving of the fp-fdp to the edge (separate from the fixing)
              //default is True here for those without fluorescence scans
              if (!MOVE_TO_EDGE.count(atomtype) or MOVE_TO_EDGE.at(atomtype))
                FP_FDP[atomtype] = sasaki.at_angstrom(peakwave);
              else
                FP_FDP[atomtype] = sasaki.at_angstrom(WAVELENGTH);
            }
            else
            {
              FP_FDP[atomtype] = sasaki.at_angstrom(WAVELENGTH);
            }
          }
        }
        if (!FP_FDP.count(atomtype))
        throw Error(err::INPUT,"No fdp for atomtype " + atomtype);
        PHASER_ASSERT(FIX_FDP[atomtype] != "edge");
      }
    }
    //form factors
    try //only works for elements
    {
      if (PARTICLE == formfactor::XRAY)
        FORM_FACTOR[atomtype] = cctbx::eltbx::xray_scattering::wk1995(atomtype).fetch();
      else if (PARTICLE == formfactor::ELECTRON)
        FORM_FACTOR[atomtype] = cctbx::eltbx::electron_scattering::peng1996(atomtype).fetch();
      else if (PARTICLE == formfactor::NEUTRON)
        FORM_FACTOR[atomtype] = cctbx::eltbx::neutron::neutron_news_1992_table(atomtype).fetch();
    }
    catch (...) {} //no form factor for clusters etc
    //set defaults if not set
    if (!FP_FDP.count(atomtype)) PHASER_ASSERT(!ANOMALOUS);
    if (!FP_FDP.count(atomtype))       FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(0.0,0.0);
    if (!FIX_FDP.count(atomtype))      FIX_FDP[atomtype] = "on";
    if (!MOVE_TO_EDGE.count(atomtype)) MOVE_TO_EDGE[atomtype] = false;
    if (!NEAR_EDGE.count(atomtype))    NEAR_EDGE[atomtype] = false;
   // if (!PEAKWAVE.count(atomtype))     PEAKWAVE[atomtype] = false; //flag for don't print
  }

  void
  Scatterers::insert(Cluster xx)
  {
    create_cluster_harker("XX",&xx);
    create_cluster_debye("XX",&xx);
  }

  std::string
  Scatterers::TABLE()
  {
    if (PARTICLE == formfactor::XRAY) return "WK1995";
    if (PARTICLE == formfactor::ELECTRON) return "PENG1996";
    if (PARTICLE == formfactor::NEUTRON) return "NEUTRON1992";
    return "WK1995"; //default, xray
  }

  std::pair<std::string,cctbx::eltbx::fp_fdp>
  Scatterers::strongest_anomalous_scatterer() const
  {
    if (!FP_FDP.size()) return {"AX",cctbx::eltbx::fp_fdp(0,1)};
    if (!WAVELENGTH and ANOMALOUS)
      throw Error(err::INPUT,"No wavelength for anomalous scattering factors");
    std::pair<std::string,cctbx::eltbx::fp_fdp> max_fdp = *FP_FDP.begin();
    for (auto item : FP_FDP)
    {
      if (item.first == "AX") return item; //AX by definition is the strongest anomalous scatterer
      //if fdps are the same but this one is a cluster compound (first test)
      if ( (item.second.fdp() == max_fdp.second.fdp() and HARKER.count(item.first))
           or (item.second.fdp() > max_fdp.second.fdp()))
      {
        max_fdp = item;
      }
    }
    return max_fdp;
  }

  cctbx::eltbx::fp_fdp
  Scatterers::fp_fdp(std::string atomtype,const double SSQR) const
  {
    atomtype = tostr2(atomtype).first;
    //if it is a cluster fix the fp and fdp
    if (HARKER.size() and HARKER.count(atomtype))
    {
      cmplex harker(0);
      double sqrtssqr = sqrt(SSQR);
      const auto& cluster = HARKER.at(atomtype);
      for (unsigned a = 0; a < cluster.size(); a++)
      {
        cmplex fpfdp; double twopi_ra;
        std::tie(std::ignore,fpfdp,twopi_ra) = cluster.at(a);
        double sqrtssqr_twopi_ra(sqrtssqr*twopi_ra);
        harker += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? fpfdp :  //limit for approx from Mathematica
                  fpfdp * std::sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
      }
      return cctbx::eltbx::fp_fdp(std::real(harker),std::imag(harker));
    }
    if (FP_FDP.count(atomtype)) return FP_FDP.at(atomtype);
    if (atomtype == "AX") return cctbx::eltbx::fp_fdp(0,1);
    throw Error(err::FATAL,"Atomtype not found for fp/fdp (" + atomtype + ")");
    return cctbx::eltbx::fp_fdp(0,0);
  }

  double
  Scatterers::fp(std::string atomtype,const double SSQR) const
  {
    atomtype = tostr2(atomtype).first;
    return fp_fdp(atomtype,SSQR).fp();
  }

  double
  Scatterers::fdp(std::string atomtype,const double SSQR) const
  {
    atomtype = tostr2(atomtype).first;
    return fp_fdp(atomtype,SSQR).fdp();
  }

  void
  Scatterers::set_fdp(std::string atomtype,const double fdp)
  {
    atomtype = tostr2(atomtype).first;
    phaser_assert(FP_FDP.count(atomtype));
    FP_FDP.at(atomtype) = cctbx::eltbx::fp_fdp(FP_FDP.at(atomtype).fp(),fdp);
  }

  bool
  Scatterers::fix_fdp(std::string atomtype) const
  {
    atomtype = tostr2(atomtype).first;
    if (FIX_FDP.count(atomtype)) return (FIX_FDP.at(atomtype) == "on");
    bool atomtype_not_found_for_formfactor(false);
    PHASER_ASSERT(atomtype_not_found_for_formfactor);
    return false;
  }

  cctbx::eltbx::xray_scattering::gaussian
  Scatterers::form_factor(std::string atomtype) const
  {
    atomtype = tostr2(atomtype).first;
    if (FORM_FACTOR.count(atomtype)) return FORM_FACTOR.at(atomtype);
    throw Error(err::DEVELOPER,"Form factor not available for \"" + atomtype + "\"");
    return cctbx::eltbx::xray_scattering::gaussian();
  }

  double
  Scatterers::fo(std::string atomtype,const double SSQR) const
  {
    atomtype = tostr2(atomtype).first;
    if (atomtype == "AX") return 0;
    if (atomtype == "RX") return 0;
    if (HARKER.size() and HARKER.count(atomtype))
    {
      const auto& cluster = HARKER.at(atomtype);
      double sqrtssqr = sqrt(SSQR);
      double harker(0);
      for (unsigned a = 0; a < cluster.size(); a++)
      {
        cctbx::eltbx::xray_scattering::gaussian FF; double twopi_ra;
        std::tie(FF,std::ignore,twopi_ra) = cluster.at(a);
        double fo = FF.at_d_star_sq(SSQR);
        double sqrtssqr_twopi_ra(sqrtssqr*twopi_ra);
        harker += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? fo :  //limit for approx from Mathematica
                  fo * std::sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
      }
      return harker;
    }
    return form_factor(atomtype).at_d_star_sq(SSQR);
  }

  double
  Scatterers::fo_plus_fp(std::string atomtype,const double SSQR) const
  {
    atomtype = tostr2(atomtype).first;
    return fo(atomtype,SSQR) + fp(atomtype,SSQR);
  }

  int
  Scatterers::number_of_atomtypes() const
  {
    return FP_FDP.size();
  }

  bool
  Scatterers::has_atomtype(std::string atomtype) const
  {
    atomtype = tostr2(atomtype).first;
    return bool(FP_FDP.count(atomtype) > 0);
  }

  std::set<std::string>
  Scatterers::atomtypes() const
  {
    std::set<std::string> tmp;
    for (auto item : FP_FDP)
      tmp.insert(item.first);
    return tmp;
  }

  std::string
  Scatterers::atomtype(int f) const
  {
    auto iter = FP_FDP.begin();
    std::advance(iter,f);
    return iter->first;
  }

  std::string
  Scatterers::formfactor_table_name() const
  {
    if (PARTICLE == formfactor::XRAY) return "WK1995";
    if (PARTICLE == formfactor::ELECTRON) return "PENG1996";
    if (PARTICLE == formfactor::NEUTRON) return "NEUTRON1992";
    return "";
  }

  double
  Scatterers::van_der_waals_radius(std::string atomtype) const
  {
    atomtype = tostr2(atomtype).first;
    return VDW.count(atomtype) ? VDW.at(atomtype) : 0;
  }

  void
  Scatterers::create_cluster_harker(std::string atomtype,Cluster* xx)
  {
    atomtype = tostr2(atomtype).first;//AJM TODO check str2
    auto structure = xx->structure;
    VDW[atomtype] = xx->vanderwaals();
    HARKER[atomtype] =  std::vector<clusterHarkerData>(structure.size());
    FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(0,0);
    int a(0);
    for (int a = 0; a < structure.size(); a++)
    {
      std::string element = structure[a].element;
      double ra = structure[a].twopi_ra;
      cctbx::eltbx::xray_scattering::gaussian FF;
      if (PARTICLE == formfactor::XRAY)
        FF = cctbx::eltbx::xray_scattering::wk1995(element).fetch();
      else if (PARTICLE == formfactor::ELECTRON)
        FF = cctbx::eltbx::electron_scattering::peng1996(element).fetch();
      else if (PARTICLE == formfactor::NEUTRON)
        FF = cctbx::eltbx::neutron::neutron_news_1992_table(element).fetch();
      cctbx::eltbx::sasaki::table sasaki(element,false,false);
      double fp = sasaki.at_angstrom(WAVELENGTH).fp();
      double fdp = sasaki.at_angstrom(WAVELENGTH).fdp();
      if (FLUORESCENCE_SCAN.count(element))
      {
        std::tie(std::ignore,fdp,std::ignore) = FLUORESCENCE_SCAN.at(element);
      }
      HARKER[atomtype][a++] = std::make_tuple(FF,cmplex(fp,fdp),ra);
      if (fdp > FP_FDP[atomtype].fdp())
      {
        FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(fp,fdp);  //this also serves as the list of scatterer types in logging
      }
    }
  }

  void
  Scatterers::create_cluster_debye(std::string atomtype,Cluster* xx)
  {
    atomtype = tostr2(atomtype).first;
    auto twopi_ra_rb = xx->twopi_ra_rb();
    VDW[atomtype] = xx->vanderwaals();
    DEBYE[atomtype] = std::vector<clusterDebyeData>(twopi_ra_rb.size());
    FP_FDP[atomtype] = cctbx::eltbx::fp_fdp(0,0);
    int ab(0);
    for (auto item : twopi_ra_rb)
    {
      std::string elementa,elementb; double rab;
      std::tie(elementa,elementb,rab) = item;
      std::map<std::string,cctbx::eltbx::xray_scattering::gaussian> FF;
      std::map<std::string,cmplex> fpfdp;
      for (auto element : {elementa,elementb})
      {
        if (PARTICLE == formfactor::XRAY)
          FF[element] = cctbx::eltbx::xray_scattering::wk1995(element).fetch();
        else if (PARTICLE == formfactor::ELECTRON)
          FF[element] = cctbx::eltbx::electron_scattering::peng1996(element).fetch();
        else if (PARTICLE == formfactor::NEUTRON)
          FF[element] = cctbx::eltbx::neutron::neutron_news_1992_table(element).fetch();
        cctbx::eltbx::sasaki::table sasaki(element,false,false);
        double fp = sasaki.at_angstrom(WAVELENGTH).fp();
        double fdp = sasaki.at_angstrom(WAVELENGTH).fdp();
        if (FLUORESCENCE_SCAN.count(element))
        {
          std::tie(std::ignore,fdp,std::ignore) = FLUORESCENCE_SCAN.at(element);
        }
        fpfdp[element] = cmplex(fp,fdp);
      }
      DEBYE[atomtype][ab++] = std::make_tuple(
             FF[elementa],FF[elementb],fpfdp[elementa],fpfdp[elementb],rab);
    }
  }

  af_string
  Scatterers::logScatterers(std::string description) const
  {
    af_string output;
    output.push_back("");
    output.push_back(">>Scattering edges: " + description + "<<");
    output.push_back("Wavelength: " + dtos(WAVELENGTH));
    output.push_back("Anomalous Scattering: " + btos(ANOMALOUS));
    output.push_back("Particle type : " + formfactor::particle2String(PARTICLE));
    if (!FP_FDP.size())
    {
      output.push_back("No scatterer types");
      return output;
    }
    output.push_back("Scattering: Sasaki Table / Fluorescence Scan / Native Data");
    output.push_back("  *Flag Fix: EDGE=(Fix if at edge) ON=(Fix) OFF=(Refine)");
    output.push_back(snprintftos(
        "%7s %10s %10s %4s %3s %10s %10s %5s %10s %10s",
        "Element","Sasaki-Fp","Sasaki-Fdp","Flag","Fix","Near-Edge","Peak-Wave","Move","Fp","Fdp"));
    for (auto item : FP_FDP)
    {
      std::string fixflag = "edge";
      if (FLUORESCENCE_SCAN.count(item.first))
        std::tie(std::ignore,std::ignore,fixflag) = FLUORESCENCE_SCAN.at(item.first);
      double sasakifp(0),sasakifdp(0);
      cctbx::eltbx::sasaki::table sasaki(item.first,false,false);
      if (sasaki.is_valid() and sasaki.at_angstrom(WAVELENGTH).is_valid())
      {
        sasakifp = sasaki.at_angstrom(WAVELENGTH).fp();
        sasakifdp = sasaki.at_angstrom(WAVELENGTH).fdp();
      }
      bool test = FIX_FDP.count(item.first) > 0;
      if (test) PHASER_ASSERT(FIX_FDP.at(item.first) != "edge");
      output.push_back(snprintftos(
          "%7s %10s %10s %4s %3s %10s %10s %5s %10.5f %10.5f",
          item.first.c_str(),
          sasakifp ? dtos(sasakifp,10,5).c_str() : "---",
          sasakifdp ? dtos(sasakifdp,10,5).c_str() : "---",
          fixflag.c_str(),
          test ? FIX_FDP.at(item.first).c_str() : "---",
          NEAR_EDGE.count(item.first) ? btos(NEAR_EDGE.at(item.first)).c_str() : "---",
          PEAKWAVE.count(item.first) ? dtos(PEAKWAVE.at(item.first)).c_str() : "---",
          MOVE_TO_EDGE.count(item.first) ? btos(MOVE_TO_EDGE.at(item.first)).c_str() : "ON",
          item.second.fp(),
          item.second.fdp()
       ));
    }
    std::string atomtype = strongest_anomalous_scatterer().first;
    double fp = strongest_anomalous_scatterer().second.fp();
    double fdp = strongest_anomalous_scatterer().second.fdp();
    output.push_back("");
    if (ANOMALOUS)
    {
      output.push_back("Strongest Anomalous Scatterer: " + atomtype + " (f'=" + dtos(fp) + "/f\"=" + dtos(fdp) + ")");
      output.push_back("");
    }
    return output;
  }

} //phasertng
