//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_reflection_ReflRow_class__
#define __phasertng_reflection_ReflRow_class__
#include <phasertng/main/Assert.h>
#include <cctbx/miller.h>

typedef cctbx::miller::index<int> millnx;

namespace phasertng {
namespace reflection {

//this allows for different implementations of the underlying arrays
//std::map will have O(log n)
//  does not require different classes for each type of data
//boost::flat_map will have O(log n) but with data local memory
//  does not require different classes for each type of data
//std::unordered map will have O(1) in the average case and O(n) in the worst case
//  does not require different classes for each type of data
//hardcoding the arrays will have O(1) and local memory
//  requires different classes for each type of data
//using and std::vector will have O(1) lookup in all cases and local memory
//  does not require different class for each type of data
//  but does require different enumerators for each type of data
//  so would need a labin::col => mapped::col converter (which could be a macro)


class ReflRow
{
  public:
    ReflRow() {}
    virtual ~ReflRow() {}

  public: //getters
    bool legal = false;
    virtual const millnx get_miller() const { phaser_assert(legal); return {0,0,0}; }
    virtual const bool   get_both() const { phaser_assert(legal); return 0; };
    virtual const bool   get_cent() const { phaser_assert(legal); return 0; };
    virtual const bool   get_plus() const { phaser_assert(legal); return 0; };
    virtual const bool   get_pnat() const { phaser_assert(legal); return 0; }
    virtual const bool   get_pneg() const { phaser_assert(legal); return 0; }
    virtual const bool   get_ppos() const { phaser_assert(legal); return 0; }
    virtual const double get_aniso() const { phaser_assert(legal); return 0; };
    virtual const double get_anisobeta() const { phaser_assert(legal); return 0; };
    virtual const double get_dobsnat() const { phaser_assert(legal); return 0; };
    virtual const double get_dobsneg() const { phaser_assert(legal); return 0; };
    virtual const double get_dobspos() const { phaser_assert(legal); return 0; };
    virtual const double get_fcnat() const { phaser_assert(legal); return 0; };
    virtual const double get_fcneg() const { phaser_assert(legal); return 0; };
    virtual const double get_fcpos() const { phaser_assert(legal); return 0; };
    virtual const double get_feffnat() const { phaser_assert(legal); return 0; };
    virtual const double get_feffneg() const { phaser_assert(legal); return 0; };
    virtual const double get_feffpos() const { phaser_assert(legal); return 0; };
    virtual const double get_fnat() const { phaser_assert(legal); return 0; };
    virtual const double get_fneg() const { phaser_assert(legal); return 0; };
    virtual const double get_fpos() const { phaser_assert(legal); return 0; };
    virtual const double get_inat() const { phaser_assert(legal); return 0; };
    virtual const double get_ineg() const { phaser_assert(legal); return 0; };
    virtual const double get_ipos() const { phaser_assert(legal); return 0; };
    virtual const double get_mapf() const { phaser_assert(legal); return 0; };
    virtual const double get_mapfom() const { phaser_assert(legal); return 0; };
    virtual const double get_mapph() const { phaser_assert(legal); return 0; };
    virtual const double get_phicnat() const { phaser_assert(legal); return 0; };
    virtual const double get_phicneg() const { phaser_assert(legal); return 0; };
    virtual const double get_phicpos() const { phaser_assert(legal); return 0; };
    virtual const double get_phsr() const { phaser_assert(legal); return 0; };
    virtual const double get_resn() const { phaser_assert(legal); return 0; };
    virtual const double get_siga() const { phaser_assert(legal); return 0; };
    virtual const double get_sigfnat() const { phaser_assert(legal); return 0; };
    virtual const double get_sigfneg() const { phaser_assert(legal); return 0; };
    virtual const double get_sigfpos() const { phaser_assert(legal); return 0; };
    virtual const double get_siginat() const { phaser_assert(legal); return 0; };
    virtual const double get_sigineg() const { phaser_assert(legal); return 0; };
    virtual const double get_sigipos() const { phaser_assert(legal); return 0; };
    virtual const double get_ssqr() const { phaser_assert(legal); return 0; };
    virtual const double get_tbin() const { phaser_assert(legal); return 0; };
    virtual const double get_teps() const { phaser_assert(legal); return 0; };
    virtual const double get_wll() const { phaser_assert(legal); return 0; };
    virtual const int    get_bin() const { phaser_assert(legal); return 0; };
    virtual const double get_eps() const { phaser_assert(legal); return 0; };
    virtual const int    get_free() const { phaser_assert(legal); return 0; };
};

//typedef std::shared_ptr<ReflRow> ReflRowPtr;
typedef const ReflRow* ReflRowPtr;

}} //phasertng
#endif
