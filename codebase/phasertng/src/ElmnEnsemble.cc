//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/src/ElmnEnsemble.h>
#include <phasertng/main/Assert.h>
#include <phasertng/main/constants.h>
#include <phasertng/io/entry/Ensemble.h>

namespace phasertng {

  af_string
  ElmnEnsemble::ELMNxR2(
      const Ensemble* ENSEMBLE,
      double sphereOuter,
      double ELMN_RESO,
      double DRMS
    )
  {
    af_string output;

    HKL_clustered HKL_list;
    double max_resolution = std::numeric_limits<double>::max();
    int nrefl = ENSEMBLE->MILLER.size();
    phaser_assert(nrefl != 0);
    phaser_assert(nrefl == ENSEMBLE->SSQR.size());
    phaser_assert(nrefl == ENSEMBLE->BIN.RBIN.size());
    double occupancy_factor = 1;//std::sqrt(SEARCH_OFAC);
    for (int r = 0; r < nrefl; r++)
    {
      millnx miller = ENSEMBLE->MILLER[r];
      phaser_assert(std::abs(ENSEMBLE->ECALC[r]) != 0);
      if (std::abs(ENSEMBLE->ECALC[r]) != 0)
      {
        double resolution = ENSEMBLE->UC.cctbxUC.d(miller);
        double ssqr = ENSEMBLE->SSQR[r];
        int ibin = ENSEMBLE->BIN.RBIN[r];
        double minusSsqrOn2 = -ssqr/2.0;
        double minusSsqrTwoPiSqrOn3 = -DEF_TWOPISQ_ON_THREE*ssqr;
        if (resolution >= ELMN_RESO)
        {
          max_resolution = std::min(resolution,max_resolution);
          cmplex thisE(ENSEMBLE->ECALC[r]); //not weighted
          double scalefac(ENSEMBLE->SIGMAA[ibin]);
                 scalefac *= std::exp(minusSsqrTwoPiSqrOn3*DRMS);
                 //scalefac *= std::exp(SEARCH_BFAC*minusSsqrOn2/2.);
                 //scalefac *= occupancy_factor;
          thisE *= scalefac;
          phaser_assert(scalefac>=0);
          double thisV = fn::pow2(scalefac);

          double Esqr = std::norm(thisE);
          //  Esqr *= scatFactor; //scale factor unnecessary
          //  thisV *= scatFactor; //scale factor unnecessary
          //AJM TODO change the elmn for each scatFactor so that FRF is on LLG scale
          //see comment below
          //the decisions can be made in the frf
          p3Dc HKL_temp;
#ifdef PHASERTNG_DEBUG_COMPARE_PHASER_FUNCTIONALITY
          HKL_temp.flipped = (miller[2]!=0); //l!=0
#endif
          dvect3 hklscaled = ENSEMBLE->UC.HKLtoVecS(miller[0],miller[1],miller[2]);
          if (hklscaled[0]==0 && hklscaled[1]==0 && hklscaled[2]==0)
            continue;

          HKL_temp.x = hklscaled[0];
          HKL_temp.y = hklscaled[1];
          HKL_temp.z = hklscaled[2];
          HKL_temp.toPolar();
          /* Model Patterson coefficient for LERF1/2 is obtained from equation (20)
             of FRF paper by dividing by SigmaN (left out of observed Patterson
             coefficient in DataMR.cc), giving sigmaA^2*(ECALC^2-1).
             Factor of fracMove/SymP would convert D^2 to sigmaA^2 corresponding to
             the fraction of the total scattering given by one symm copy, but the
             FRF result is normalised later anyway, so this is left out.
          */
          HKL_temp.intensity = Esqr - thisV;
          HKL_list.add(HKL_temp);
        }
      }
    }
    double HIRES = max_resolution; //used for determining maximum resolution of fast rotation function
    phaser_assert(HKL_list.clustered.size());
    //randomize to even up the number on each thread
   // HKL_list.shuffle();
    output.push_back("Number of Model HKL is "+std::to_string(HKL_list.size()));

    //phaser H = 2*scitbx::constants::pi*htp.r*sphereOuter; //this sphereOuter is mean_radius
    //phaser H = two_pi_b*htp.r
    //int LMAX = scitbx::constants::two_pi*2*radius/HIRES;
    //int two_pi_b = LMAX * HIRES; //==2pi*2*radius
    int two_pi_b = 2*scitbx::constants::pi*sphereOuter;
    af_string txt = hkl_to_elmn(HKL_list,two_pi_b,{1,' '});
    for (auto item : txt)
      output.push_back(item);
    return output;
  }

}//phasertng
