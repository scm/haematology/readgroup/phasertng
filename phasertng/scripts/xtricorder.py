import sys,os
from libtbx.phil import parse
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.phil.converter_registry import converter_registry
from phasertng import Phasertng
from phasertng import PhaserError
from phasertng.phil.mode_generator import *
from phasertng.phil.phil_to_cards import phil_to_cards
import argparse

def xtricorder(user_phil_str=""):

  basetng = Phasertng("PHENIX")
  try:
    initparams = build_voyager_phil_file(program_name="xtricorder")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    user_phil = parse(user_phil_str,converter_registry=converter_registry)
    working_params = master_phil.fetch(sources=[user_phil]).extract()
    dbdir = working_params.phasertng.suite.database
    if dbdir is not None and not os.path.isdir(dbdir):
      os.makedirs(dbdir)

    #run silently except for writing out final mtz file, after info
    working_params.phasertng.mode = ["imtz","data","sga","sgx","aniso","tncso","tncs","feff","twin","cca"]
    working_params.phasertng.suite.write_data  = False
    cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    basetng.run(cards)
    working_params.phasertng.mode = ["info"]
    working_params.phasertng.suite.write_data  = True
    cards = phil_to_cards(user_phil=working_params,program_name="InputCard")
    basetng.run(cards)
    return basetng

  except AttributeError as e:
    message = "" if str(e) is None else str(e)
    print("Python error: " + __name__ + ": " + message)
    error = PhaserError()
    error.Set("PYTHON",message)
  except Exception as e:
    message = "" if str(e) is None else str(e)
    print("Python error: " + __name__ + ": " + message)
    error = PhaserError()
    error.Set("PYTHON",message)
  return basetng

if (__name__ == '__main__'):
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--mtz",help="input data file",type=str,required=True)
  parser.add_argument('-s',"--hall",help="reindexing",type=str,required=False)
  args = parser.parse_args()
  user_phil = "phasertng.suite.database = tmp\n"
  if (args.phil is not None):
    user_phil = args.phil.read()
  if (args.mtz is not None):
    user_phil += "phasertng.hklin.filename = \"" + str(args.mtz) + "\"\n"
  if (args.hall is not None):
    user_phil += "phasertng.space_group_expansion.spacegroup = \"Hall: " + str(args.hall) + "\"\n"
  user_phil += "phasertng.suite.write_files = false\n"
  basetng = xtricorder(user_phil)
  error = basetng.get_error_object()
  user_phil = basetng.phil_str()
  if False:
    print ("Error exit code ",error.exit_code())
    print ("Error message ",error.error_message())
    print ("Error exit type ",error.error_type())
  sys.exit(error.exit_code())
