//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_nmol_class__
#define __phasertng_nmol_class__
#include <phasertng/pod/frequencies.h>
#include <scitbx/vec3.h>
#include <vector>

typedef scitbx::vec3<double> dvect3;

namespace phasertng {
namespace pod {

class data_nmol_vecpk
{
  public:
    data_nmol_vecpk() {}

    data_nmol_vecpk(
        dvect3 vec_,
        double pk_
      ) :
        vec(vec_),
        pk(pk_)
    { }

    dvect3 vec = dvect3(0,0,0);
    double pk = 0;

    bool operator<(const data_nmol_vecpk &rhs) const
    { return vec*vec < rhs.vec*rhs.vec; }
    bool operator==(const data_nmol_vecpk &rhs) const
    { return vec==rhs.vec; }
};

class data_nmol
{
  public:
    double HIGH_PATT_PERC = 0;
    double TEST_PEAK = 0;
    double TEST_TOLF = 0;
    double TEST_TOLO = 0;
    double TEST_MISS = 0;
    double MAPCUT_PATT_PERC = 0;
    double ORIGIN_DISTANCE = 0;

  public:
    data_nmol(
              const double HIGH_PATT_PERC_ ,
              const double TEST_PEAK_,
              const double TEST_TOLF_,
              const double TEST_TOLO_,
              const double TEST_MISS_,
              const double MAPCUT_PATT_PERC_ ,
              const double ORIGIN_DISTANCE_
            ) :
            HIGH_PATT_PERC(HIGH_PATT_PERC_),
            TEST_PEAK(TEST_PEAK_),
            TEST_TOLF(TEST_TOLF_),
            TEST_TOLO(TEST_TOLO_),
            TEST_MISS(TEST_MISS_),
            MAPCUT_PATT_PERC(MAPCUT_PATT_PERC_),
            ORIGIN_DISTANCE(ORIGIN_DISTANCE_)
    { }

  public:
    dvect3 vector_frac = dvect3(0,0,0); //fractional coordinates
    double vector_height = 0;
    double vector_distance = 0;
    //not constructor
    int    order = 0;
    //not always set if not commensurate
    double tolfrac = 999; //needs to be able to be x1000 (not numeric limits)
    double tolorth = 999;
    double shortdist = std::numeric_limits<double>::max();
    std::vector<data_nmol_vecpk> vectorset;
    pod::frequencies freq = pod::frequencies();
    bool flag;

    void setup(
        int order_=0,
        dvect3 frac_=dvect3(0,0,0),
        double height_=0,
        double distance_=0
      )
    {
      order = (order_);
      vector_frac = (frac_);
      vector_height = (height_);
      vector_distance = (distance_);
      vectorset.clear();
    }

    dvect3 exact_vector()
    {
      return dvect3(
          (1.0/order)*std::floor(double(order)*vector_frac[0]+0.5), //round
          (1.0/order)*std::floor(double(order)*vector_frac[1]+0.5),
          (1.0/order)*std::floor(double(order)*vector_frac[2]+0.5)
        );
    }

    void add_vector(dvect3 vec,double pk)
    {
      data_nmol_vecpk next(vec,pk);
      if (std::find(vectorset.begin(),vectorset.end(),next) == vectorset.end())
      {
        vectorset.push_back(next);
       // std::sort(vectorset.begin(),vectorset.end());
      }
    }

    void hack_vectorset()
    {
      //for noncommensurate, hack the vectorset to contain the noncommensurate vector
      vectorset.clear();
      data_nmol_vecpk next(vector_frac,vector_height);
      vectorset.push_back(next);
      order = 2;
      shortdist = 0;
      freq.index = ivect3(0,0,0);
      freq.strength = 1.;
      tolorth = 0;
      tolfrac = 0;
    }

    int missing() const
    { //same as below
      int haspeaks = 0;
      for (int i = 0; i < vectorset.size(); i++)
        if (vectorset[i].pk > MAPCUT_PATT_PERC) //percents
          haspeaks++;
      int fullset = order-1;
      return (fullset - haspeaks);
    }

    int allowed_missing() const
    {
      int haspeaks = 0;
      for (int i = 0; i < vectorset.size(); i++)
        if (vectorset[i].pk > MAPCUT_PATT_PERC) //percents
          haspeaks++;
      int fullset = order-1;
      return std::floor(TEST_MISS*fullset/100.); //percent
    }

    bool complete() const
    {
      //peak is present if it has height more than
      //MAPCUT_PATT_PERC percent of HIGH_PATT_PERC percent
      //e.g. 50% of highest peak vector
      if (!vectorset.size()) return true;
      int haspeaks = 0;
      for (int i = 0; i < vectorset.size(); i++)
        if (vectorset[i].pk > MAPCUT_PATT_PERC) //percents
          haspeaks++;
      //there are order-1 non-origin peaks in the set
      //repeat 0 (and order) are the origin peak
      //if allow 20% of the set to be missing
      //this is the second order minimum function for 5<order<10
      //if allow 50% missing, then this is definitely half the modulation
      //if allow <50% e.g. 40%, this means there has to be some indication
      //of intermediate peaks than a halving of the modulation
      int fullset = order-1;
      int allowed_missing = std::floor(TEST_MISS*fullset/100.); //percent
      //increase by one for odd nmol, rather than increasing allowed_missing for odd
      //int missing = fullset - haspeaks;
      return (haspeaks >= fullset-allowed_missing);
    }

    double max_peak_in_complete_set() const
    { //find if this vector explains the highest peak
      double max_peak = vector_height;
      for (int i = 0; i < vectorset.size(); i++)
      {
        if (vectorset[i].pk < 100 and //not the origin peak
            std::fabs(vectorset[i].vec[0]) < DEF_PPM and
            std::fabs(vectorset[i].vec[1]) < DEF_PPM and
            std::fabs(vectorset[i].vec[2]) < DEF_PPM
           )
        max_peak = std::max(max_peak,vectorset[i].pk);
      }
      return max_peak;
    }

    void set_vector_frac_to_shortest_in_vectorset()
    { //set the vector_frac to the minimum in the set of vectorset for nmol > 1
      if (order <= 2) return;
      auto cmp = []( const data_nmol_vecpk &left,const data_nmol_vecpk &right)
         { return left.vec*left.vec < right.vec*right.vec; };
      auto sorted = vectorset;
      std::sort(sorted.begin(),sorted.end(),cmp);
      vector_frac = sorted[0].vec;
    }

    bool commensurate() const
    {
      return ( order and //check not default!
               shortdist >= ORIGIN_DISTANCE and
               vector_height >= HIGH_PATT_PERC and
               complete() and //complete within the missing tolerance
               (freq.strength > TEST_PEAK or  //this should be the case if the tols are low
                 (tolfrac <= TEST_TOLF and tolorth <= TEST_TOLO)));
    }

    std::string log_nmol_analysis()
    {
      std::string Card;
      Card += "\t    order = " +
         std::to_string(order);
      Card += "\n\t    vector = " +
         std::to_string(vector_frac[0]) + " " +
         std::to_string(vector_frac[1]) + " " +
         std::to_string(vector_frac[2]);
      Card += "\n\t    freq = " +
         std::to_string(freq.index[0]) + " " +
         std::to_string(freq.index[1]) + " " +
         std::to_string(freq.index[2]);
      Card += "\n\t    commensurate = " +
        std::string(commensurate()? "true":"false");
      Card += "\n\t      height > high = " +
        std::to_string(vector_height) + " > " +
        std::to_string(HIGH_PATT_PERC) + " " +
        std::string(vector_height>HIGH_PATT_PERC? "true":"false");
      Card += "\n\t          shortdist > patt_dist : " +
        std::to_string(shortdist) + " > " +
        std::to_string(ORIGIN_DISTANCE) + " " +
        std::string(shortdist > ORIGIN_DISTANCE?"true":"false");
      Card += "\n\t      and complete : " +
        std::string(complete()?"true":"false");
      Card += "\n\t      and (freq strength > TEST_PEAK : " +
        std::to_string(freq.strength)  + " > " +
        std::to_string(TEST_PEAK) + " " +
        std::string(freq.strength > TEST_PEAK? "true":"false");
      Card += "\n\t       or (tolfrac <= TEST_TOLF : " +
        std::to_string(tolfrac) + " <= " +
        std::to_string(TEST_TOLF) + " " +
        std::string(tolfrac <= TEST_TOLF?"true":"false");
      Card += "\n\t            and tolorth <= TEST_TOLO : " +
        std::to_string(tolorth) + " <= " +
        std::to_string(TEST_TOLO) + " " +
        std::string(tolorth <= TEST_TOLO ? "true":"false");
      Card += "))\n\t     full-set=" +
         std::to_string(order-1) + " missing=" +
         std::to_string(missing()) + " allowed-missing=" +
         std::string(allowed_missing()? "true":"false");
      Card += "))\n\t     max-peak-in-complete-set=" +
        std::to_string(max_peak_in_complete_set());
      for (int i = 0; i < vectorset.size(); i++)
        Card += "\n\t       vector set =" +
         std::to_string(i+1) + " vec=" +
         std::to_string(vectorset[i].vec[0]) + " " +
         std::to_string(vectorset[i].vec[1]) + " " +
         std::to_string(vectorset[i].vec[2]) + " pk=" +
         std::to_string(vectorset[i].pk) + " > " +
         std::to_string(MAPCUT_PATT_PERC) + " present = " +
         std::string(vectorset[i].pk > MAPCUT_PATT_PERC? "true":"false") ;
      return Card;
    }

};

class vector_data_nmol
{

  public:
    vector_data_nmol() {}
    std::vector<data_nmol> data;
  typedef std::vector<data_nmol>::reference reference;
  typedef std::vector<data_nmol>::const_reference const_reference;
  const_reference operator[](int t) const { return data[t]; }
  reference operator[](int t) { return data[t]; }

  af_string
  logTncsAnalysis(int m=0)
  {
    af_string output;
    output.push_back("C*=Commensurate Modulation");
    output.push_back(snprintftos(
        "%2s %5s  %6s  #%-2s %-23s  %5s %11s %-8s %-8s %9s%c %-9s %4s",
        "C*","TNCSO","%Patt","n","tNCS-vector","%Freq","Frequencies","tol-orth","tol-frac","tncs-dist",' ',"max-%Patt","miss"));
    for (int n = m; n < data.size(); n++)
    {
      //  dvect3 orth = REFLECTIONS->UC.orthogonalization_matrix()*(data[n].vector_frac);
      //  double shortdist = std::sqrt(orth*orth);
      if (data[n].commensurate())
        output.push_back(snprintftos(
            "%2s %5i  %6.1f  #%-2d %+6.4f %+6.4f %+6.4f  %5.1f %3i %3i %3i %8.2f %8.6f %9.1f%c %9.1f %4d",
                data[n].commensurate()? "T " : "F ",
                data[n].order,
                data[n].vector_height,
                1,
                data[n].vector_frac[0],
                data[n].vector_frac[1],
                data[n].vector_frac[2],
                data[n].freq.strength,
                data[n].freq.index[0],
                data[n].freq.index[1],
                data[n].freq.index[2],
                data[n].tolorth,
                data[n].tolfrac,
                data[n].shortdist,'A',
                data[n].max_peak_in_complete_set(),
                data[n].missing()
              ));
      else
        output.push_back(snprintftos(
            "%2s %5i  %6.1f  #%-2d %+6.4f %+6.4f %+6.4f  %5s %3s %3s %3s %8s %8s %9.1f%c %9.1f %4s",
                "F ",
                data[n].order,
                data[n].vector_height,
                1,
                data[n].vector_frac[0],
                data[n].vector_frac[1],
                data[n].vector_frac[2],
                "---",
                "---",
                "---",
                "---",
                "---",
                "---",
                data[n].vector_distance,'A',
                data[n].max_peak_in_complete_set(),
                "---"
              ));
      if (data[n].order > 2)
      {
        for (int i = 1; i < data[n].vectorset.size(); i++)
        {
          dvect3 vec = data[n].vectorset[i].vec;
          output.push_back(snprintftos(
            "%2s %5s  %6.1f  #%-2d %+6.4f %+6.4f %+6.4f",
             "","",
             data[n].vectorset[i].pk,
             i+1,
             vec[0],
             vec[1],
             vec[2]));
        }
      }
    }
    return output;
  }
};

//sort order is that the shortest is better, also see below
//where distance is also part of sort_data_nmol
inline bool sort_nmol_distance(const data_nmol &left,const data_nmol &right)
{ return left.vector_distance < right.vector_distance; }

class sort_data_nmol
{
  public:
    sort_data_nmol() {}

  bool operator()(const data_nmol& lhs, const data_nmol& rhs)
  {
    //highest patterson peak is explained by this modulation
    //this is the primary sort criteria
    //explain the biggest feature in the patterson
    if (lhs.commensurate() != rhs.commensurate()) //stronger frequencies win
    {
      bool commensurate(lhs.commensurate() < rhs.commensurate());
      return commensurate; //reverse sort
    }
    if (lhs.max_peak_in_complete_set() != rhs.max_peak_in_complete_set())
    {
      bool  complete(lhs.max_peak_in_complete_set() < rhs.max_peak_in_complete_set()); //not reverse sort
      return complete; //not reverse sort
    }
    //can't use both tolorth and tolfrac in criteria for sorting
    //use tol_orth since diffraction differences are to do with angstrom measured differences
    //since should only have one distance measure, else conflicts possible
    //tolfrac used for decision making about whether commensurate or not (line 155)
    //only compare to one part per 1000 for tol_orth tolerance
    if (lhs.tolorth != std::numeric_limits<double>::max() and //paranoia, can't x1000
        rhs.tolorth != std::numeric_limits<double>::max() )
    {
      if (std::abs(lhs.tolorth - rhs.tolorth) > DEF_PPT)
      {
        bool tolorth(lhs.tolorth > rhs.tolorth); //reverse sort
        return tolorth; //reverse sort
      }
    }
    if (lhs.order != rhs.order) //higher order wins
    {
      bool order(lhs.order > rhs.order); //not reverse sort
      return order; //not reverse sort
    }
    if (std::fabs(lhs.vector_distance - rhs.vector_distance) > DEF_PPT)
    {
      bool distance(lhs.vector_distance > rhs.vector_distance); //not reverse sort
      return distance;
    }
    if (lhs.freq.index != rhs.freq.index) //simpler frequencies win
    { //simplicity tested on distance measure
      bool freq(lhs.freq.distance > rhs.freq.distance); //reverse sort
      return freq; //reverse sort
    }
    if (lhs.freq.strength != rhs.freq.strength) //stronger frequencies win
    {
      bool strength(lhs.freq.strength > rhs.freq.strength); //reverse sort
      return strength; //reverse sort
    }
    bool height(lhs.vector_height > rhs.vector_height); //reverse sort
    return height; //case when order=2 and no frequencies present
  }
  //call as: sort(v.begin(), v.end(), sort_nmol_table(paramA));
};

}} //phasertng
#endif
