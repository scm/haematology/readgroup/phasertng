//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/math/likelihood/ELLG.h>
#include <phasertng/data/Selected.h>

namespace phasertng {

  //Expected_llg_for_search
  void Phasertng::runETFZ()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::LOGFILE,"No poses");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (!DAGDB.NODES.number_of_seeks())
    {
      logTab(out::LOGFILE,"No seek components");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (DAGDB.constituents_complete())
    {
      logTab(out::LOGFILE,"Constituents complete");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (REFLECTIONS->MAP)
    {
      logTab(out::LOGFILE,"Data as map");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->get_ztotalscat())
    throw Error(err::INPUT,"Total scattering not set");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("seek");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::VARIANCE_MTZ,id,false);
    }}

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;

    //make sure the reflections are sorted low resolution to high resolution
    //this should be done as part of the data preparation step
    if (!REFLECTIONS->check_sort_in_resolution_order())
      throw Error(err::INPUT,"Reflection file not sorted by resolution");

    likelihood::ELLG likelihood_ellg;

    //the average vrms is a horrible approximation, but is good for af2 models
    //which don't have ensembles and are all the same rmsd regardless of target
    double average_vrms_final(0.);
    auto& node0 = DAGDB.WORK; //pointer
    {{
    out::stream where = out::VERBOSE;
    phaser_assert(node0->SEEK.size());
    sv_double average_vrms(node0->SEEK.size());
    for (int s = 0; s < node0->SEEK.size(); s++)
    {
      auto& seek = node0->SEEK[s];
      auto& entry = seek.ENTRY;
      auto  modlid = entry->identify();
      sv_double vrms_final = pod::vrms_final(
        node0->DRMS_SHIFT[modlid].VAL,entry->VRMS_START);
      for (int n = 0; n < vrms_final.size(); n++) //over all in the ensemble
        average_vrms[s] += vrms_final[n]/vrms_final.size();
    }
    for (int s = 0; s < node0->SEEK.size(); s++)
    {
      logTab(where,"Seek #" + ntos(s+1,node0->SEEK.size()) + " VRMS:  [average=" + dtos(average_vrms[s],7,4) + "]");
      average_vrms_final += average_vrms[s]/static_cast<double>(node0->SEEK.size());
    }
    logTab(where,"Overall average VRMS: " + dtos(average_vrms_final,7,4));
    logBlank(where);
    }}

    {{
    auto lognode0 = *node0; //deep copy
    for (int s = 0; s < lognode0.SEEK.size(); s+=nmol)
    {
      if (lognode0.SEEK[s].is_missing() or lognode0.SEEK[s].is_search()) //search not yet set
      {
        lognode0.SEEK[s].set_search();
        break;
      }
    }
    }}

    out::stream where = out::SUMMARY;
    logUnderLine(where,"Dag Seek Permutations");
    logTab(where,"Poses: " + itos(DAGDB.NODES.number_of_poses()));
    logTabArray(out::LOGFILE,DAGDB.seek_info_array());
    std::set<Identifier> set_of_remaining_seek;
    for (int s = 0; s < DAGDB.WORK->SEEK.size(); s++)
      if (!DAGDB.WORK->SEEK[s].is_found())
        set_of_remaining_seek.insert(DAGDB.WORK->SEEK[s].IDENTIFIER);
    PHASER_ASSERT(set_of_remaining_seek.size());
    auto set_of_poses = DAGDB.NODES.used_modlid_pose();
    //copy the start because we are going to modify work by permuting
    dag::NodeList output_dag = DAGDB.NODES;
    struct sort_data {
       double etfz = 0;
       double ellg = 0;
       bool found = false;
       std::vector<dag::Seek> seek;
       std::string id = "";
    };
    std::vector<sort_data> minus_ellg_of_seeks;
    auto cmp = []( const sort_data &a, const sort_data &b )
         { if (a.found == b.found) //if they have the same id then they will have same found flag
             return a.ellg >  //> for reverse sort
                   b.ellg;
           return a.etfz > b.etfz;  //earlier in the list if found
        };
    for (auto next_seek : set_of_remaining_seek)
    {
      DAGDB.permute_seek(next_seek);
      logChevron(where,"Signal for next placement: " + next_seek.str() );
      double seek_cumulative_ellg(0);
      double pose_ellg(0);
      double FP(0);
      sort_data thissortdata;
      for (int s = 0; s < node0->SEEK.size(); s++)
      {
        af_double Siga(REFLECTIONS->NREFL);
        {{
          auto& entry = node0->SEEK[s].ENTRY;
          FP += entry->fraction_scattering(REFLECTIONS->get_ztotalscat());
          //interpV includes solvent term
          auto& variance = entry->VARIANCE;
          for (int r = 0; r < REFLECTIONS->NREFL; r++)
          {
            Siga[r] = std::sqrt(variance.rSigaSqr[r]*FP); //take sqrt binwise
          }
        }}
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          if (selected.get_selected(r) and REFLECTIONS->get_present(friedel::NAT,r))
          {
            const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
            const int& b = work_row->get_bin();
            const double feff = work_row->get_feffnat();
            const double dobs = work_row->get_dobsnat();
            const double resn = work_row->get_resn();
            const double teps = work_row->get_teps();
            const bool cent = work_row->get_cent();
            double DobsSiga = dobs*Siga[b];
            double Eeff = feff/(resn*std::sqrt(teps));
            double expected_LLGI = likelihood_ellg.get(Eeff,DobsSiga,cent);
                   expected_LLGI *= REFLECTIONS->oversampling_correction();
            seek_cumulative_ellg += expected_LLGI;
          }
        }
        if (node0->SEEK[s].is_found())
        {
          pose_ellg = seek_cumulative_ellg;
          logTab(where,"pose ellg: " + dtos(pose_ellg));
        }
        else if (node0->SEEK[s].is_missing() or node0->SEEK[s].is_search()) //search not yet set
        {
          thissortdata.etfz = 0; //this is not implemented
          for (int p = 0; p < node0->POSE.size()/nmol; p++)
            if (node0->SEEK[s].IDENTIFIER.str() == node0->POSE[p].IDENTIFIER.str())
              thissortdata.etfz = node0->POSE[p].TFZ;
          thissortdata.ellg = seek_cumulative_ellg-pose_ellg; //wrong but ok since currently only gives rank
          thissortdata.id = node0->SEEK[s].IDENTIFIER.str();
          thissortdata.found = set_of_poses.count(next_seek);
          thissortdata.seek = node0->SEEK;
          minus_ellg_of_seeks.push_back(thissortdata);
          logTab(where,"seek ellg: " + dtos(thissortdata.ellg));
          break;
        }
      }
    }

    PHASER_ASSERT(minus_ellg_of_seeks.size());
    logBlank(out::SUMMARY);
    std::sort(minus_ellg_of_seeks.begin(),minus_ellg_of_seeks.end(),cmp);
    int i(1);
    for (auto sortedtfz :  minus_ellg_of_seeks)
      logTab(out::SUMMARY,"Expected tfz ranking placement #" + itos(i++) + " ellg=" + dtos(sortedtfz.ellg,2) + " " + sortedtfz.id);
    logBlank(out::SUMMARY);

    //paste the seek for the test onto all the
    int m(1);
    dag::Edge pathway = DAGDB.PATHWAY;
    dag::Edge pathlog = DAGDB.PATHLOG;
    input.clear(".phasertng.expected.subdir.");
    input.clear(".phasertng.expected.filenames_in_subdir.");
    //zcores are wildly inaccurate and not reported
    //only the sort order matters
    for (auto sortedtfz :  minus_ellg_of_seeks)
    {
      DAGDB.PATHWAY = pathway;
      DAGDB.PATHLOG = pathlog;
      DAGDB.NODES = output_dag; //node trackers
      for (int i = 0; i < DAGDB.NODES.size(); i++)
      {
        DAGDB.NODES[i].SEEK = sortedtfz.seek;
        DAGDB.NODES[i].NEXT.PRESENT = false;
      }
      //DAGDB.increment_current_seeks();
      DAGDB.shift_tracker(hashing(),input.running_mode+std::to_string(m));
      if (WriteCards())
      {
        Identifier ultimate = DAGDB.PATHWAY.ULTIMATE;
        std::string counter = ntos(m++,DAGDB.size());
        DAGDB.SetFileSystem(DataBase(),ultimate.FileName("."+counter+".dag.cards"));
        DAGDB.unparse_card();
        logFileWritten(where,WriteFiles(),"Dag Seek Permutation "+counter,DAGDB);
        input.push_back(".phasertng.expected.subdir.",DAGDB.filepath);
        input.push_back(".phasertng.expected.filenames_in_subdir.",DAGDB.filename);
      }
    }

    //revert the dag
    DAGDB.NODES = output_dag;
    //DAGDB.shift_tracker(hashing()+DAGDB.work_phil_str(),input.running_mode);
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

} //phasertng
