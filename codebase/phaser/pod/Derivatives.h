//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DERIVATIVES__
#define __PHASER_DERIVATIVES__
#include <phaser/main/Phaser.h>

namespace phaser {

typedef struct {

  af::shared<miller::index<int> > miller;
  af_cmplx  dL_by_dFC;

} Derivatives;

} //phaser

#endif
