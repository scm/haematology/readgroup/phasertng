//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_AnglesRandom_class__
#define __phasertng_AnglesRandom_class__
#include <phasertng/site/SiteList.h>
#include <scitbx/random.h>
#include <scitbx/math/euler_angles.h>

namespace phasertng {

class AnglesRandom : public SiteList
{
  private:
   scitbx::random::mersenne_twister generator;
   int inext = 0;
   int Size = 0;

  public:
    AnglesRandom(int,unsigned);

  public:
    void        restart(unsigned=0);
    bool        at_end();
    dvect3      next_site();
    std::size_t count_sites() { return Size; }
    af_dvect3   all_sites();
    double      point_distance() { return 0; }
};

}
#endif
