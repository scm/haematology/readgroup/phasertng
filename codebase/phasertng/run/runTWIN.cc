//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Moments.h>
#include <phasertng/data/Selected.h>
#include <mmtbx/scaling/twinning.h>

namespace phasertng {

  //Twin_test
  void Phasertng::runTWIN()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::TNCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (tncs)");
    phaser_assert(REFLECTIONS->has_col(labin::INAT));

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    double hires_all_data(0);
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range());
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_outliers_and_select(REFLECTIONS.get(),
        selected.hires_criteria());
    }}

    //below hold data to full sigfigs
    //output phil only stores to three sigfigs, for numerical stablility
    std::map<std::string,MomentsData> moments_data;
    std::pair<double,double> moments_expected = {0,0};

    Moments moments(REFLECTIONS.get());
    moments.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
    bool has_high_signal_data = moments.check_data_and_restrict_resolution(true,selected.get_selected_array());
    logTab(out::LOGFILE,"High resolution for twin test: " + dtos(moments.hires));
    if (moments.NO_SIGI and moments.NO_I)
      logAdvisory("No sigmas for intensities for data provided as amplitudes");
    else if (moments.NO_SIGI and !moments.NO_I)
      logWarning("No sigmas for intensities for data provided as intensities, results unreliable");
    else if (!has_high_signal_data) //max_s == 0
      logAdvisory("No high signal data for moments test");

    int sigfig(3);
    bool m_twinned(false);
    if (true and has_high_signal_data)
    {
      if (REFLECTIONS->has_col(labin::EPS))
      {
        int datatype = 0;
        logUnderLine(out::LOGFILE,"Data");
        //we have already done the relevant selection, just log here
        logTabArray(out::LOGFILE,selected.logSelected("Data"));
        if (Suite::Extra())
          logTabArray(out::TESTING,selected.logOutliers("Data"));
        moments.setup_moments(datatype,selected.get_selected_array());
        logTabArray(out::VERBOSE,selected.logSelected("Data"));
        logTabArray(out::LOGFILE,moments.logMoments("Data"));
        moments.setup_distributions(datatype,selected.get_selected_array());
        logGraph(out::LOGFILE,moments.logGraph1("Data"));
        logGraph(out::LOGFILE,moments.logGraph2("Data"));
        moments_data["initial"] = moments.get_data();
      }

      if (REFLECTIONS->has_col(labin::RESN))
      {
        int datatype = 1;
        logUnderLine(out::LOGFILE,"Anisotropy Correction");
        { //moments.reject_outliers(1);
          phaser_assert(REFLECTIONS->has_col(labin::RESN));
          sv_double epsnSigmaN(REFLECTIONS->NREFL,0);
          for (int r = 0; r < REFLECTIONS->NREFL; r++)
            epsnSigmaN[r] = moments.epsn_sigman(1,r);
          selected.flag_outliers_and_select(REFLECTIONS.get(),
              selected.hires_criteria(),
              epsnSigmaN
            );
          logTabArray(out::LOGFILE,selected.logSelected("Anisotropy Correction"));
          if (Suite::Extra())
            logTabArray(out::TESTING,selected.logOutliers("Anisotropy Correction"));
        }
        moments.setup_moments(datatype,selected.get_selected_array()); //get raw pointer
        logTabArray(out::VERBOSE,selected.logSelected("Anisotropy Correction"));
        logTabArray(out::LOGFILE,moments.logMoments("Anisotropy Correction"));
        moments.setup_distributions(datatype,selected.get_selected_array()); //get raw pointer
        logGraph(out::LOGFILE,moments.logGraph1("Anisotropy Correction"));
        logGraph(out::LOGFILE,moments.logGraph2("Anisotropy Correction"));
        moments_data["isotropic"] = moments.get_data();
      }

      if (REFLECTIONS->has_col(labin::RESN) and
          !boost::logic::indeterminate(REFLECTIONS->TNCS_PRESENT) and
         //  REFLECTIONS->TNCS_PRESENT and
          REFLECTIONS->has_col(labin::TEPS))
      {
        int datatype = 2;
        out::stream where = out::LOGFILE;
        if (!(!boost::logic::indeterminate(REFLECTIONS->TNCS_PRESENT) and REFLECTIONS->TNCS_PRESENT))
          where = out::VERBOSE;
        logUnderLine(where,"tNCS Correction");
        { //moments.reject_outliers(2);
          phaser_assert(REFLECTIONS->has_col(labin::TEPS));
          sv_double epsnSigmaN(REFLECTIONS->NREFL,0);
          for (int r = 0; r < REFLECTIONS->NREFL; r++)
            epsnSigmaN[r] = moments.epsn_sigman(2,r);
          selected.flag_outliers_and_select(REFLECTIONS.get(),
              selected.hires_criteria(),
              epsnSigmaN
            );
          logTabArray(where,selected.logSelected("tNCS Correction"));
          if (Suite::Extra())
            logTabArray(out::TESTING,selected.logOutliers("tNCS Correction"));
        }
        moments.setup_moments(datatype,selected.get_selected_array());
        logTabArray(out::VERBOSE,selected.logSelected("tNCS Correction"));
        logTabArray(where,moments.logMoments("tNCS Correction"));
        moments.setup_distributions(datatype,selected.get_selected_array());
        logGraph(where,moments.logGraph1("tNCS Correction"));
        logGraph(where,moments.logGraph2("tNCS Correction"));
        moments_data["tncs"] = moments.get_data();
        moments_expected = moments.second_moments_expected();
      }

      {{
        out::stream where(out::LOGFILE);
        logTableTop(where,"tNCS/Twin Detection");
        logTab(where,snprintftos(
             "%29s %16s           %12s",
             "", "-Second Moments-", "--P-values--"));
        logTab(where,snprintftos(
             "%29s %6s %8s     %22s%2.0f%c",
             "", "Centric", "Acentric", "untwinned  twin-frac<", moments.twinperc(),'%'));
        logTab(where,snprintftos(
             "%29s %6.2f  %6.2f",
             "Theoretical for untwinned     ",
             3.0,
             2.0));
        if (moments_expected.first != 0 and moments_expected.second != 0)
        {
          logTab(where,snprintftos(
             "%29s %6.2f  %6.2f",
             "--including measurement error ",
             moments_expected.first,
             moments_expected.second));
        }
        logTab(where,snprintftos(
             "%29s %6.2f  %6.2f",
             "Theoretical for perfect twin  ",
             2.0,
             1.5));
        if (moments_data.count("initial"))
        {
          auto print = moments_data["initial"];
          logTab(where,snprintftos(
              "%27s %6s %7s+/-%5s  %-10.3g %-10.3g",
              "Initial (data as input)       ",
              print.centric_mean > 1000 ? ">1000" : dtos(print.centric_mean,6,2).c_str(),
              print.acentric_mean > 1000 ? ">1000" : dtos(print.acentric_mean,5,2).c_str(),
              print.acentric_sigma > 1000 ? ">1000" : dtos(print.acentric_sigma,5,3).c_str(),
              print.pvalue,
              print.ptwin
              ));
        }
        if (moments_data.count("isotropic"))
        {
          auto print = moments_data["isotropic"];
          logTab(where,snprintftos(
              "%27s %6s %7s+/-%5s  %-10.3g %-10.3g",
              "After Anisotropy Correction   ",
              print.centric_mean > 1000 ? ">1000" : dtos(print.centric_mean,6,2).c_str(),
              print.acentric_mean > 1000 ? ">1000" : dtos(print.acentric_mean,5,2).c_str(),
              print.acentric_sigma > 1000 ? ">1000" : dtos(print.acentric_sigma,5,3).c_str(),
              print.pvalue,
              print.ptwin
              ));
        }
        if (moments_data.count("tncs"))
        {
          auto print = moments_data["tncs"];
          logTab(where,snprintftos(
              "%27s %6s %7s+/-%5s  %-10.3g %-10.3g",
              "After Anisotropy and tNCS     ",
              print.centric_mean > 1000 ? ">1000" : dtos(print.centric_mean,6,2).c_str(),
              print.acentric_mean > 1000 ? ">1000" : dtos(print.acentric_mean,5,2).c_str(),
              print.acentric_sigma > 1000 ? ">1000" : dtos(print.acentric_sigma,5,3).c_str(),
              print.pvalue,
              print.ptwin
              ));
        }
        //else
        //  logTab(where,nprintftocstr("%-29s     ---n/a---","After Anisotropy and tNCS   ");
        logBlank(where);
        logTab(where,snprintftos(
            "%9s%4.2f%6s%2.0f%35s",
             "P-value <",
             moments.plimit(),
             " for <",
             moments.twinperc(),
             "% twinned is considered worth investigating"));
        logTab(where,"Resolution for Twin Analysis (85th percentile I/SIGI > 3): " + dtos(moments.resolution(),5,2) + "A");
        logTableEnd(where);
      }}
      if (moments.twinned())
      logWarning("The tNCS-corrected Moments Test indicates that intensity statistics are different from those of untwinned data");
      m_twinned = moments.twinned();
    }

    bool py_twinned(false);
    bool has_parity = true;
    if (true)
    {
      af::shared< cctbx::miller::index<int> > hkl;
      af::shared< double > intensity;
      cctbx::sgtbx::space_group space_group  = REFLECTIONS->SG.group();
      bool anomalous_flag = false;
      std::vector<ivect3> parities;
      ivect3 phkl = input.value__int_vector.at(".phasertng.tncs.parity.").value_or_default();
      phkl[0] = std::max(2,phkl[0]);
      phkl[1] = std::max(2,phkl[1]);
      phkl[2] = std::max(2,phkl[2]);
      parities.push_back(phkl);
      int max_delta_h = std::max(8,std::max(phkl[0],std::max(phkl[1],phkl[2])));
      if (input.value__int_vector.at(".phasertng.twinning.parity.").is_default())
      {
        phkl = input.value__int_vector.at(".phasertng.twinning.parity.").value_or_default();
        phkl[0] = std::max(2,phkl[0]);
        phkl[1] = std::max(2,phkl[1]);
        phkl[2] = std::max(2,phkl[2]);
        if (parities[0] != phkl)
          parities.push_back(phkl);
        max_delta_h = std::max(max_delta_h,std::max(phkl[0],std::max(phkl[1],phkl[2])));
      }
      for (int r = 0; r < REFLECTIONS->NREFL; r++)
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        if (!work_row->get_cent()) //acentric onlin
        if (work_row->get_pnat())
        if (work_row->get_inat() > 0)
        if (selected.get_selected(r))
        {
          hkl.push_back(work_row->get_miller());
          const double teps = work_row->get_teps();
          const double resn = work_row->get_resn();
          const double iobs = work_row->get_inat();
          double epsnSigmaN = fn::pow2(resn)*teps;
          double jval = iobs/epsnSigmaN;
          PHASER_ASSERT(jval > 0);
          intensity.push_back(jval);
        }
      }

      logUnderLine(out::LOGFILE,"Padilla and Yeates L-test");
      if (hkl.size() == 0)
      {
        has_parity = false;
        logAdvisory("No reflections with correct parity");
      }
      else
      {
        logTab(out::LOGFILE,snprintftos(
            "Number acentric hkl: %d",
            hkl.size()));
        phaser_assert(hkl.size() > 0);
        logTab(out::LOGFILE,snprintftos(
            "Max delta-h: %d",
            max_delta_h));
        logBlank(out::LOGFILE);
        for (auto parity : parities)
        {
          logTab(out::LOGFILE,snprintftos(
              "Padilla-Yeates L-test Parity: %d %d %d  %s",
              parity[0],parity[1],parity[2],
              (parity == ivect3(2,2,2)) ? "(default)":""));
          mmtbx::scaling::twinning::l_test<double> twin_test(
            hkl.const_ref(),
            intensity.const_ref(),
            space_group,
            anomalous_flag,
            parity[0],
            parity[1],
            parity[2],
            max_delta_h);
          double mean_l = twin_test.mean_l();
          double mean_l2 = twin_test.mean_l2();
          logTab(out::LOGFILE,snprintftos(
              "<|L|>       : %5.3f  (untwinned: >0.500; perfect twin: 0.375)",
               mean_l));
          logTab(out::LOGFILE,snprintftos(
              "<L^2>       : %5.3f  (untwinned: >0.333; perfect twin: 0.200)\n",
              mean_l2));
          logBlank(out::LOGFILE);
          double PY = input.value__flt_number.at(".phasertng.twinning.padilla_yeates_test.").value_or_default();
          logTab(out::LOGFILE,"Padilla-Yeates L-test (with tolerance) for <|L|> :" + dtos(PY));
          if (mean_l < PY)
          {
            py_twinned = true;
            logWarning("The Padilla-Yeates L-test indicates that intensity statistics are different from those of untwinned data");
          }
        }
      }
    }

    {{
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Twinning");
    if (!boost::logic::indeterminate(REFLECTIONS->TNCS_PRESENT) and REFLECTIONS->TNCS_PRESENT)
      logTab(where,"Moments Test is tNCS-corrected");
    logTab(where,"Moments Test (has signal):          " + btos(m_twinned) + " (" + btos(has_high_signal_data) +")");
    logTab(where,"Padilla-Yeates L-test (has parity): " + btos(py_twinned) + " (" + btos(has_parity) +")");
    logBlank(where);
    if (m_twinned and !py_twinned)
      logAdvisory("Moments and Padilla-Yeates tests do not agree");
    else if (!m_twinned and py_twinned)
      logAdvisory("Moments and Padilla-Yeates tests do not agree");
    else
      logAdvisory("Moments and Padilla-Yeates tests agree");
    if (input.value__choice.at(".phasertng.twinning.test.").value_or_default_equals("moments_and_ltest"))
    {
      logTab(where,"Moments and Padilla-Yeates tests determine twinning result");
      REFLECTIONS->TWINNED = m_twinned and py_twinned;
    }
    else if (input.value__choice.at(".phasertng.twinning.test.").value_or_default_equals("moments_or_ltest"))
    {
      logTab(where,"Moments or Padilla-Yeates test determines twinning result");
      REFLECTIONS->TWINNED = m_twinned or py_twinned;
    }
    else if (input.value__choice.at(".phasertng.twinning.test.").value_or_default_equals("moments"))
    {
      logTab(where,"Moments test determines twinning result");
      REFLECTIONS->TWINNED = m_twinned;
    }
    else if (input.value__choice.at(".phasertng.twinning.test.").value_or_default_equals("ltest"))
    {
      logTab(where,"Padilla-Yeates test determines twinning result");
      REFLECTIONS->TWINNED = py_twinned;
    }
    }}

    {{
    out::stream where(out::SUMMARY);
    if (boost::logic::indeterminate(REFLECTIONS->TWINNED))
    {
      logBlank(where);
      logTab(where,"Twinning cannot be determined, data quality poor");
      logBlank(where);
      //and twinned flags not set
    }
    else
    {
      if (REFLECTIONS->TWINNED)
      {
        logBlank(where);
        logTab(where,"Twinning indicated");
        logBlank(where);
        logWarning("Tests suggest possibility of twinning although tests based on possible twin laws will be more definitive");
      }
      else if (!REFLECTIONS->TWINNED)
      {
        logBlank(where);
        logTab(where,"Twinning not indicated");
        logBlank(where);
      }
      input.value__boolean.at(".phasertng.twinning.indicated.").set_value(bool(REFLECTIONS->TWINNED));
      logTab(out::VERBOSE,"Set DAG twinned flags");
      for (auto& node : DAGDB.NODES)
      {
        node.TWINNED = (REFLECTIONS->TWINNED) ? 'Y' : 'N';
      }
    }
    }}

    DAGDB.shift_tracker(hashing(),input.running_mode);

    DogTag reflid;
    {{
    out::stream where = out::VERBOSE;
    reflid = DAGDB.initialize_reflection_identifier(REFLECTIONS->reflid().tag());
    logTab(where,"Reflections identifier: " + reflid.str());
    REFLECTIONS->REFLID = reflid.identify();
    REFLECTIONS->REFLPREP.insert(reflprep::FEFF);
    }}

    write_reflid(reflid);
    input.value__path.at(".phasertng.hklout.filename.").set_value(REFLCOLSMAP.fstr());
  }

} //phasertng
