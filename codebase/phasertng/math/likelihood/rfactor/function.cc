//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/rfactor/function.h>
#include <cmath>
#include <algorithm>

namespace phasertng { namespace likelihood { namespace rfactor {

  void
  function::add(
      double FEFF,
      double Fcalc
    )
  {
    numerator += std::fabs(FEFF-Fcalc);
    denominator += FEFF;
  }

  double
  function::get()
  {
    return std::max(minval,std::min(maxval,numerator/denominator));
  }

  void
  function::clear()
  {
    numerator = denominator = 0;
  }

}}} //phasertng
