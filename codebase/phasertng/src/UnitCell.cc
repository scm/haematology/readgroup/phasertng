//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/src/UnitCell.h>
#include <phasertng/main/constants.h>

namespace phasertng {

  UnitCell::UnitCell(cctbx::uctbx::unit_cell u)
  {
    cctbxUC = u;
    original_orthogonal_cell = GetBox();
  }

  UnitCell::UnitCell(const af::double6 u)
  {
    cctbxUC = cctbx::uctbx::unit_cell(u);
    original_orthogonal_cell = GetBox();
  }

  UnitCell::UnitCell(const dvect3 cell)
  {
    cctbxUC = cctbx::uctbx::unit_cell(af::double6(cell[0],cell[1],cell[2],90.0,90.0,90.0));
    original_orthogonal_cell = GetBox();
  }

  double
  UnitCell::cosAlpha() const
  { return (Alpha() == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(Alpha())); }

  double
  UnitCell::cosBeta() const
  { return (Beta() == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(Beta())); }

  double
  UnitCell::cosGamma() const
  { return (Gamma() == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(Gamma())); }

  double
  UnitCell::cosAlphaStar() const
  { return (cctbxUC.reciprocal_parameters()[3] == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[3])); }

  double
  UnitCell::cosBetaStar() const
  { return (cctbxUC.reciprocal_parameters()[4] == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[4])); }

  double
  UnitCell::cosGammaStar() const
  { return (cctbxUC.reciprocal_parameters()[5] == 90.) ?
        0. : std::cos(scitbx::deg_as_rad(cctbxUC.reciprocal_parameters()[5])); }

  double
  UnitCell::aStar() const
  { return cctbxUC.reciprocal_parameters()[0]; }

  double
  UnitCell::bStar() const
  { return cctbxUC.reciprocal_parameters()[1]; }

  double
  UnitCell::cStar() const
  { return cctbxUC.reciprocal_parameters()[2]; }

  dvect3
  UnitCell::HKLtoVecS(const int h,const int k,const int l) const
  {
    double V = cctbxUC.volume();

    dvect3 basisaStar;
    basisaStar[0] = (orthogonalization_matrix()(1,1)*orthogonalization_matrix()(2,2)-orthogonalization_matrix()(1,2)*orthogonalization_matrix()(2,1))/V;
    basisaStar[1] = (orthogonalization_matrix()(2,1)*orthogonalization_matrix()(0,2)-orthogonalization_matrix()(2,2)*orthogonalization_matrix()(0,1))/V;
    basisaStar[2] = (orthogonalization_matrix()(0,1)*orthogonalization_matrix()(1,2)-orthogonalization_matrix()(0,2)*orthogonalization_matrix()(1,1))/V;

    dvect3 basisbStar;
    basisbStar[0] = (orthogonalization_matrix()(1,2)*orthogonalization_matrix()(2,0)-orthogonalization_matrix()(1,0)*orthogonalization_matrix()(2,2))/V;
    basisbStar[1] = (orthogonalization_matrix()(2,2)*orthogonalization_matrix()(0,0)-orthogonalization_matrix()(2,0)*orthogonalization_matrix()(0,2))/V;
    basisbStar[2] = (orthogonalization_matrix()(0,2)*orthogonalization_matrix()(1,0)-orthogonalization_matrix()(0,0)*orthogonalization_matrix()(1,2))/V;

    dvect3 basiscStar;
    basiscStar[0] = (orthogonalization_matrix()(1,0)*orthogonalization_matrix()(2,1)-orthogonalization_matrix()(1,1)*orthogonalization_matrix()(2,0))/V;
    basiscStar[1] = (orthogonalization_matrix()(2,0)*orthogonalization_matrix()(0,1)-orthogonalization_matrix()(2,1)*orthogonalization_matrix()(0,0))/V;
    basiscStar[2] = (orthogonalization_matrix()(0,0)*orthogonalization_matrix()(1,1)-orthogonalization_matrix()(0,1)*orthogonalization_matrix()(1,0))/V;

    dvect3 vecS;
    vecS[0] = h*basisaStar[0]+k*basisbStar[0]+l*basiscStar[0];
    vecS[1] = h*basisaStar[1]+k*basisbStar[1]+l*basiscStar[1];
    vecS[2] = h*basisaStar[2]+k*basisbStar[2]+l*basiscStar[2];
    return vecS;
  }

  bool
  UnitCell::is_default() const
  {
    UnitCell D;
    return A()==D.A() and B()==D.B() and C()==D.C() and
           Alpha()==D.Alpha() and Beta()==D.Beta() and Gamma()==D.Gamma();
  }

  bool
  UnitCell::is_orthogonal() const
  {
    return Alpha()==90. and Beta()==90. and Gamma()==90.;
  }

  double
  UnitCell::extend_resolution(
       double data_hires,
       std::pair<double,double> cell_scale) const
  {
    PHASER_ASSERT(!is_default());
    double scale = std::min(cell_scale.first,cell_scale.second);
    phaser_assert(Alpha() == 90);
    phaser_assert(Beta() == 90);
    phaser_assert(Gamma() == 90);
    //the incoming HIRES is the HIRES of the data
    //the map has to be extended with an additional shell for interpolation
    //including allowance for the maximum cell_scale
    miller::index<int> unit(1,1,1);
    UnitCell scale_uc(scale*GetBox());
    phaser_assert(data_hires > 0);
    phaser_assert(data_hires < 10000);
    double requestres = 1/(1/data_hires + 1/scale_uc.cctbxUC.d(unit));
    PHASER_ASSERT(requestres <= data_hires + DEF_PPM);
    return requestres;
  }

  double
  UnitCell::limit_resolution(
       double config_hires,
       std::pair<double,double> cell_scale) const
  {
    PHASER_ASSERT(!is_default());
    double scale = std::min(cell_scale.first,cell_scale.second);
    phaser_assert(Alpha() == 90);
    phaser_assert(Beta() == 90);
    phaser_assert(Gamma() == 90);
    //the incoming HIRES is the HIRES of the data
    //the map has to be extended with an additional shell for interpolation
    //including allowance for the maximum cell_scale
    UnitCell scale_uc(scale*GetBox());
    miller::index<int> unit(1,1,1);
    double requestres = 1/(1/config_hires - 1/scale_uc.cctbxUC.d(unit));
    PHASER_ASSERT(requestres >= config_hires - DEF_PPM);
    return requestres;
  }

  void
  UnitCell::scale_orthogonal_cell(double scale)
  {
    if (scale == 1) return;
    PHASER_ASSERT(!is_default());
    PHASER_ASSERT(original_orthogonal_cell != dvect3(0,0,0));
    if (Alpha() == 90. and Beta() == 90. and Gamma() == 90.)
      cctbxUC = cctbx::uctbx::unit_cell(af::double6(
                    scale*original_orthogonal_cell[0],
                    scale*original_orthogonal_cell[1],
                    scale*original_orthogonal_cell[2],
                    90.0,90.0,90.0));
  }

  void
  UnitCell::reset_orthogonal_cell()
  {
    PHASER_ASSERT(!is_default());
    if (Alpha() == 90. and Beta() == 90. and Gamma() == 90.)
      cctbxUC = cctbx::uctbx::unit_cell(af::double6(
                    original_orthogonal_cell[0],
                    original_orthogonal_cell[1],
                    original_orthogonal_cell[2],
                    90.0,90.0,90.0));
  }

  bool
  UnitCell::is_orthogonal()
  { return Alpha() == 90 and Beta() == 90 and Gamma() == 90; }

  af_string
  UnitCell::build_variance_unit_cell(double vboxscale,double config_hires)
  {
    PHASER_ASSERT(!is_default());
    af_string output;
    //final check for too small
    dvect3  variance_box = vboxscale*GetBox();
    for (int i = 0; i < 3; i++)
    {
      if (variance_box[i] < 8*config_hires)
      {
        output.push_back("Increase box to minimum of 8*dmin");
        variance_box[i] = 8*config_hires;
      }
    }
    output.push_back("Variance Unit Cell: " +
           std::to_string(variance_box[0]) + " " +
           std::to_string(variance_box[1]) + " " +
           std::to_string(variance_box[2]));
    cctbxUC = cctbx::uctbx::unit_cell(af::double6(
        variance_box[0],variance_box[1],variance_box[2],90.0,90.0,90.0));
    original_orthogonal_cell = GetBox();
    //output.push_back("");
    return output;
  }

  af_string
  UnitCell::build_minimum_unit_cell(double config_hires)
  {
    dvect3 box = GetBox();
   // PHASER_ASSERT(!is_default());
    af_string output;
    //final check for too small
    for (int i = 0; i < 3; i++)
    {
      if (box[i] < 8*config_hires)
      {
        output.push_back("Increase box to minimum of 8*dmin");
        box[i] = 8*config_hires;
      }
    }
    output.push_back("Minimum Unit Cell: " +
           std::to_string(box[0]) + " " +
           std::to_string(box[1]) + " " +
           std::to_string(box[2]));
    //output.push_back("");
    cctbxUC = cctbx::uctbx::unit_cell(af::double6(
        box[0],box[1],box[2],90.0,90.0,90.0));
    original_orthogonal_cell = GetBox();
    return output;
  }

  af_string
  UnitCell::build_decomposition_unit_cell(double sphereOuter,double config_hires)
  {
    //PHASER_ASSERT(!is_default());
    af_string output;
    PHASER_ASSERT(sphereOuter > 0);
    auto getbox = GetBox();
    output.push_back("The extent is: " +
           std::to_string(getbox[0]) + " " +
           std::to_string(getbox[1]) + " " +
           std::to_string(getbox[2]));
    output.push_back("The integration sphere is: " + std::to_string(sphereOuter));
    output.push_back("The unit cell will be a box equal to the extent of the model + integration sphere + resolution");
    output.push_back("The resolution limit is: " + std::to_string(config_hires));
    //no scalar multiplication defined
    dvect3 decomposition_box = GetBox();
    decomposition_box[0] += sphereOuter+config_hires;
    decomposition_box[1] += sphereOuter+config_hires;
    decomposition_box[2] += sphereOuter+config_hires;
    //final check for too small
    for (int i = 0; i < 3; i++)
    {
      if (decomposition_box[i] < 8*config_hires)
      {
        output.push_back("Increase box to minimum of 8*dmin");
        decomposition_box[i] = 8*config_hires;
      }
    }
    output.push_back("Decomposition Unit Cell: " +
           std::to_string(decomposition_box[0]) + " " +
           std::to_string(decomposition_box[1]) + " " +
           std::to_string(decomposition_box[2]));
    //output.push_back("");
    cctbxUC = cctbx::uctbx::unit_cell(af::double6(
        decomposition_box[0],decomposition_box[1],decomposition_box[2],90.0,90.0,90.0));
    original_orthogonal_cell = GetBox();
    return output;
  }

  af_string
  UnitCell::build_interpolation_unit_cell(double boxscale,double config_hires)
  {
    PHASER_ASSERT(!is_default());
    af_string output;
    output.push_back("Interpolation Cell");
    output.push_back("The unit cell will be boxscale * extent of the model");
    phaser_assert(boxscale > 0);
    output.push_back("The boxscale is: " + std::to_string(boxscale));
    output.push_back("The config hires is: " + std::to_string(config_hires));
    dvect3 interpolation_box = GetBox();
      //no scalar multiplication defined for dvect3
    interpolation_box[0] *= boxscale;
    interpolation_box[1] *= boxscale;
    interpolation_box[2] *= boxscale;
    //final check for too small
    output.push_back("Initial Interpolation Unit Cell: " +
           std::to_string(interpolation_box[0]) + " " +
           std::to_string(interpolation_box[1]) + " " +
           std::to_string(interpolation_box[2]));
    for (int i = 0; i < 3; i++)
    {
      if (interpolation_box[i] < 8*config_hires)
      {
        output.push_back("Increase box to minimum of 8*dmin");
        interpolation_box[i] = 8*config_hires;
      }
    }
    output.push_back("Interpolation Unit Cell: " +
           std::to_string(interpolation_box[0]) + " " +
           std::to_string(interpolation_box[1]) + " " +
           std::to_string(interpolation_box[2]));
   // output.push_back("");
    cctbxUC = cctbx::uctbx::unit_cell(af::double6(
       interpolation_box[0],interpolation_box[1],interpolation_box[2],90.0,90.0,90.0));
    original_orthogonal_cell = GetBox();
    return output;
  }

  dmat33
  UnitCell::orth_Rtr_frac(dmat33 Rotsym) const
  {
    return orthogonalization_matrix()*Rotsym.transpose()*fractionalization_matrix();
  }

  std::pair<double,cctbx::uctbx::unit_cell>
  UnitCell::gridding(double dmin,std::pair<double,double> cell_scale,bool interpolate,double padding)
  {
    PHASER_ASSERT(!is_default());
    //when we are building density for structure factor interpolation
    //gridding is more complicated than using the unit cell and resolution for density
    //as we need to allow for cell expansion and contraction in refinement
    //If the cell expands, the number of grid points to a given resolution is larger
    //so need more indices (fewer for contraction, which does not impact worst-case senario)
    //At the very limit, one grid point will be more resolution in the smaller cell,
    //so need to expand the resolution by the unit miller index in the smaller cell
    cctbx::uctbx::unit_cell gridding_uc = cctbxUC;
    double savereso = dmin;
    af::double6 tiny_box = cctbxUC.parameters();
    double cell_scale_min = std::min(cell_scale.first,cell_scale.second);
    double cell_scale_max = std::max(cell_scale.first,cell_scale.second);
    if (cell_scale != std::pair<double,double>(1,1))
    {
      //test for orthogonal P1 lattice, i.e. lattice for interpolation
      if (Alpha() != 90 or Beta() != 90 or Gamma() != 90)
      throw Error(err::DEVELOPER,"Only apply cell scale with orthogonal cell");
      af::double6 huge_box = this->cctbxUC.parameters();
      huge_box[0] *= cell_scale_max;
      huge_box[1] *= cell_scale_max;
      huge_box[2] *= cell_scale_max;
      //change gridding unit cell
      gridding_uc = cctbx::uctbx::unit_cell(huge_box);
      tiny_box[0] *= cell_scale_min;
      tiny_box[1] *= cell_scale_min;
      tiny_box[2] *= cell_scale_min;
    } //AJM TODO always extend
    cctbx::uctbx::unit_cell tiny_uc(tiny_box);
    //extend the resolution at the edges by one grid unit
    if (interpolate)
    {
      int u = std::ceil(padding*std::max(cell_scale_max,1./cell_scale_min));
      millnx unit(u,u,u);
      //add as s=smax+sunit
      double smax = 1./(dmin-0.01);
      double sunit = 1./tiny_uc.d(unit);
      double s = (smax + sunit);
      savereso = (1./s);
    }
    //STORE THE EXTENDED RESOLUTION AS THE DMIN
    return { savereso, gridding_uc };
  }

  double
  UnitCell::resolution(double dmin,std::pair<double,double> cell_scale)
  {
    PHASER_ASSERT(!is_default());
    if (Alpha() != 90 or Beta() != 90 or Gamma() != 90)
    throw Error(err::DEVELOPER,"Not orthogonal cell");
    double savereso(10000);
    double cell_scale_min = std::min(cell_scale.first,cell_scale.second);
    double cell_scale_max = std::max(cell_scale.first,cell_scale.second);
    int u = std::ceil(std::max(cell_scale_max,1./cell_scale_min));
    millnx unit(u,u,u);
    double smax = 1./(dmin-0.01);
    double sunit = 1./cctbxUC.d(unit);
    double s = (smax + sunit);
    savereso = 1./s;
    return savereso;
  }

  std::string
  UnitCell::str(std::string header) const
  {
    char buf[80];
    if (header.size())
    {
      snprintf(buf,80,"[%-38s]",header.c_str());
      return std::string(buf);
    }
    snprintf(buf,80,"[%6.1f %6.1f %6.1f %5.1f %5.1f %5.1f]",
       A(),B(),C(),Alpha(),Beta(),Gamma());
    return std::string(buf);
  }

  int
  UnitCell::width() const
  {
    int dottenth=2;
    int wA = std::max(std::log10(A())+1,2.)+dottenth;
    int wB = std::max(std::log10(B())+1,2.)+dottenth;
    int wC = std::max(std::log10(C())+1,2.)+dottenth;
    return std::max(wC,std::max(wA,wB));
  }

}//phasertng
