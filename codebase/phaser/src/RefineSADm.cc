//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineSAD.h>
#include <phasertng/main/jiffy.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
//#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/index_span.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/adptbx.h>
#include <cctbx/miller/expand_to_p1.h>
#include <phasertng/math/rotation/cmplex2polar_deg.h>
#include <phasertng/dag/Helper.h>

//-- Things to do with gradient maps

namespace phaser {

af_cmplx RefineSAD::calcGradCoefs(std::string LLGC_ATOMTYPE,af_float FLLG,af_float PHLLG)
{
  LLGRAD_SF.clear();
  FLLG.clear();
  PHLLG.clear();
  //hack to get code to work for phenix maps
  if (atomtype2t.find(LLGC_ATOMTYPE) == atomtype2t.end()) assert(LLGC_ATOMTYPE == "AX");
  int t = (atomtype2t.find(LLGC_ATOMTYPE) == atomtype2t.end()) ? 0 : atomtype2t[LLGC_ATOMTYPE];
  cctbx::sgtbx::space_group SgOps(SpaceGroup::getCctbxSG());

/*
  Gfunction gfun(*this,*this,PTNCS.NMOL);
  if (PTNCS.use_and_present())
  {
    dmat33 ncsRmat(1,0,0,0,1,0,0,0,1);
    for (int dir = 0; dir < 3; dir++)
      ncsRmat = xyzRotMatDeg(dir,PTNCS.ROT.ANGLE[dir])*ncsRmat;
    gfun.initArrays(PTNCS.TRA.VECTOR,ncsRmat,PTNCS.GFUNCTION_RADIUS,miller);
  }
*/

  const cmplxType cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
  for (unsigned r = 0; r < NREFL; r++)
  {
/*
    if (PTNCS.use_and_present())
    {
      gfun.calcReflTerms(r);
    }
*/
    miller::sym_equiv_indices s_e_i(SgOps,miller[r]);
    int multiplicity = s_e_i.multiplicity(false);
    //if the search is to be done with dbled sites, then the map must be P1
    int max_isym = std::min(1,multiplicity);
    for (std::size_t isym = 0; isym < max_isym; isym++)
    {
      miller::sym_equiv_index h_eq = s_e_i(isym);
      miller::index<int> h = miller[r];
      if (true)
      {
        cmplxType rLLGRAD_SF = 0;
        floatType rFLLG = 0;
        floatType rPHLLG = phasertng::rad2phi(phsr[r]);
        if (!bad_data[r] && selected[r])
        {
          floatType sharpen = std::exp(-WilsonB*ssqr[r]/4.);
          //hack to get code to work for phenix maps
          floatType fp =  (LLGC_ATOMTYPE == "AX") ? 0 : fo_plus_fp[r][t]*sharpen;
          floatType fdp = (LLGC_ATOMTYPE == "AX") ? 1 : AtomFdp[t]*sharpen;
          cmplxType fp_plus_fdp(fp,fdp);
          fp = std::real(fp_plus_fdp);
          fdp = std::imag(fp_plus_fdp);
          if (both[r])
          {
            acentricReflGradSAD2(r); // Note that FHneg is complex conjugate of H-, and dL refers to minus log-likelihood
            rLLGRAD_SF = -cmplxType(fp*(dL_by_dReFHpos+dL_by_dReFHneg)+fdp*(dL_by_dImFHpos-dL_by_dImFHneg),
                                   fdp*(-dL_by_dReFHpos+dL_by_dReFHneg)+fp*(dL_by_dImFHpos+dL_by_dImFHneg));
          }
          else if (!sad_target_anom_only && cent[r])
          {
            centricReflGradSAD2(r);
            rLLGRAD_SF = -cmplxType(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos,
                                   -fdp*dL_by_dReFHpos+fp*dL_by_dImFHpos);
            rFLLG = std::abs(rLLGRAD_SF);
            floatType phllg_rad = (std::cos(std::arg(rLLGRAD_SF) - phsr[r]) > 0) ? phsr[r] : phsr[r]+scitbx::constants::pi;
            rPHLLG = phasertng::rad2phi(phllg_rad);
            rLLGRAD_SF = std::polar(rFLLG,phllg_rad); // enforce phase restriction
          }
          else if (!sad_target_anom_only) // singleton acentric
          {
            singletonReflGradSAD2(r,plus[r]);
            rLLGRAD_SF = plus[r] ? -cmplxType(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos,-fdp*dL_by_dReFHpos+fp*dL_by_dImFHpos):
                                   -cmplxType(fp*dL_by_dReFHneg-fdp*dL_by_dImFHneg, fdp*dL_by_dReFHneg+fp*dL_by_dImFHneg);
          }
          //rLLGRAD_SF = rLLGRAD_SF;
         // rFLLG = std::abs(rLLGRAD_SF);
          //rPHLLG = rad2phi(std::arg(rLLGRAD_SF));
          std::tie(rFLLG,rPHLLG) = phasertng::cmplex2polar_deg(rLLGRAD_SF);
        } //selected data
        LLGRAD_SF.push_back(rLLGRAD_SF);
        FLLG.push_back(rFLLG);
        PHLLG.push_back(rPHLLG);
      } //isym asu
    } //isym
  }
  return LLGRAD_SF.deep_copy();
}

std::tuple<LlgcMapSites,af_string,af_string>
RefineSAD::calcGradMaps(std::string LLGC_ATOMTYPE,data_llgc LLGCOMPLETE,std::string cC1,int c)
{
  af_string output;
  af_string output_extra;
  calcGradCoefs(LLGC_ATOMTYPE);
  //std::string xx = (LLGC_ATOMTYPE == "AX")?"PURE fpp: ":"ELEMENT " + LLGC_ATOMTYPE + ": ";
  std::string allxx = "[";
  for (auto xx : LLGCOMPLETE.ATOMTYPE)
    allxx += xx + ",";
  allxx.back() = ']';
  std::string xx = (LLGC_ATOMTYPE == "AX")?" (PURE IMAGINARY)":" ("+LLGC_ATOMTYPE + "/" + allxx + ")";
  int io_nprint = 20;
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell UCell = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group      SgOps(SpaceGroup::getCctbxSG());
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
  cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);

  af::shared<miller::index<int> > miller_cp = miller.deep_copy();
  af_cmplx  LLGRAD_SF_cp = LLGRAD_SF.deep_copy();
  //miller_cp = miller::expand_to_p1_indices(SgOps,!FriedelFlag,miller.const_ref());
  //LLGRAD_SF_cp = miller::expand_to_p1_complex<floatType>(SgOps,!FriedelFlag,miller.const_ref(),LLGRAD_SF.const_ref()).data;

  assert(miller_cp.size() == LLGRAD_SF_cp.size());

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  floatType d_min = cctbx::uctbx::d_star_sq_as_d(UCell.max_d_star_sq(miller_cp.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  floatType resolution_factor = 1.0/(2.0*1.5);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
      UCell,d_min,resolution_factor,sym_flags,
      SgInfo, //PtNcs requires P1
      mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

  bool anomalous_flag(false);
  bool conjugate_flag(true);
  bool treat_restricted(false);
  af::c_grid_padded<3> map_grid(rfft.n_complex());
  //multiplier into LLGRAD ????
  cctbx::maptbx::structure_factors::to_map<floatType> gradmap(
    SgOps, //PtNcs requires P1
    anomalous_flag,
    miller_cp.const_ref(),
    LLGRAD_SF_cp.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag,
    treat_restricted);
  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref(
    gradmap.complex_map().begin(),
    af::c_grid<3>(rfft.n_complex()));

  // --- do the fft
  rfft.backward(gradmap_fft_ref);

  af::versa<floatType, af::flex_grid<> > real_map(
    gradmap.complex_map().handle(),
    af::flex_grid<>(af::adapt((rfft.m_real())))
          .set_focus(af::adapt(rfft.n_real())));
  cctbx::maptbx::unpad_in_place(real_map);
  af::versa<floatType, af::c_grid<3> > real_map_unpadded(
    real_map, af::c_grid<3>(rfft.n_real()));

  //-- calculate average occupancy of current atoms
  // 0.9 is "expected occupancy" for target of scattering type assignment
  floatType av_occ(0.9),av_u_iso(cctbx::adptbx::b_as_u(WilsonB));
  if (atoms.size())
  {
    floatType sum_occ(0),sum_u_iso(0);
    floatType natoms(0);
    for (int a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected)
    {
      sum_occ += atoms[a].SCAT.occupancy;
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) sum_u_iso += atoms[a].SCAT.u_iso;
      else
      {
        cctbx::adptbx::factor_u_star_u_iso<floatType> factor(getCctbxUC(),atoms[a].SCAT.u_star);
        sum_u_iso += factor.u_iso;
      }
      natoms++;
    }
    av_occ = sum_occ/natoms;
    av_u_iso = sum_u_iso/natoms;
  }
  output.push_back("");
  output.push_back("New atoms will be added with occupancy = " + phasertng::dtos(av_occ,3) +
                   " and B-factor = " + phasertng::dtos(cctbx::adptbx::u_as_b(av_u_iso),1));

  LlgcMapSites new_sites;
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();

  bool header = true;
  bool1D atoms_bswap(atoms.size(),false); //outside loop because set for holes and peaks
  bool1D resurrect_old_atom(atoms.size(),false);
  floatType separation_dist = LLGCOMPLETE.CLASH;
  floatType maxZM_holes(0);
 //do holes first, they are only used to turn known sites anisotropic
 //) arrays above will contain peak information at end
  for (int holes = 1; holes >= 0; holes--)
  {
    if (holes) for (int i = 0; i < real_map_unpadded.size(); i++) real_map_unpadded[i] *= -1;
    if (!holes) for (int i = 0; i < real_map_unpadded.size(); i++) real_map_unpadded[i] *= -1; //switch back again

    // --- work out mean and sigma for total search
    cctbx::maptbx::statistics<floatType> stats(
      af::const_ref<floatType, af::flex_grid<> >(
        real_map_unpadded.begin(),
        real_map_unpadded.accessor().as_flex_grid()));
    floatType gradmap_mean = stats.mean();
    floatType gradmap_sigma = stats.sigma();

    // --- find symmetry equivalents
    af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));
    tags.build(SgInfo, sym_flags); //PtNcs requires P1
    af::ref<long, af::c_grid<3> > tag_array_ref(
      tags.tag_array().begin(),
      af::c_grid<3>(tags.tag_array().accessor()));
    assert(tags.verify(real_map_const_ref));

    // --- peak search
    int peak_search_level(1);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
      real_map_const_ref,
      tag_array_ref,
      peak_search_level,
      max_peaks,
      interpolate);

    af::shared<floatType> peak_heights = peak_list.heights();
    af::shared<dvect3>    peak_sites = peak_list.sites();

    struct peakinfo {
      dvect3 site;
      double height;
      double zscore;
      double ZonM() const { return zscore/multiplicity; }
      int    multiplicity;
      bool operator<(const peakinfo &right) const
      { return ZonM() > right.ZonM(); } //reverse sort
    };
    std::vector<peakinfo> peaks_Zscores;
    for (std::size_t i = 0; i < peak_heights.size(); i++)
    {
      peakinfo next;
      next.site = peak_sites[i];
      next.height = peak_heights[i];
      next.zscore = (peak_heights[i]-gradmap_mean)/gradmap_sigma;
      cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),peak_sites[i]);
      next.multiplicity = SpaceGroup::NSYMM/peak_sym.multiplicity();
      peaks_Zscores.push_back(next);
    }
      //handle special positions
    //collect the high peaks and store their Z-scores
    //print a histogram of the Z-scores
    unsigned1D peaks_over_cutoff;
    if (peaks_Zscores.size() && gradmap_sigma) //i.e not a flat map
    {
      double maxZ = peaks_Zscores[0].zscore; //for logging
      std::sort(peaks_Zscores.begin(),peaks_Zscores.end());
      //we sort on ZonM here so that i==0 is the top peak of interest, always
      double maxZM = peaks_Zscores[0].ZonM();
      output.push_back("");
      output.push_back(cC1);
      output.push_back("");
      holes ? output.push_back("--Histogram of Z-scores of HOLES--" + xx) :
              output.push_back("--Histogram of Z-scores of PEAKS--" + xx);
      if (holes && LLGCOMPLETE.PEAKS_OVER_HOLES) maxZM_holes = maxZM;
      holes ? output.push_back("Number of holes = " + std::to_string(peak_heights.size())) :
              output.push_back("Number of peaks = " + std::to_string(peak_heights.size()));
      output.push_back("Maximum Z-score = " + std::to_string(maxZ));
      output.push_back("Maximum M-corrected Z-score = " + std::to_string(maxZM));
      int imaxZM(std::ceil(maxZM));
      unsigned1D histogram(imaxZM);
      unsigned maxStars(0);
      for (std::size_t i = 0; i < peaks_Zscores.size(); i++)
      {
        double ZM = peaks_Zscores[i].ZonM();
        if (ZM >= LLGCOMPLETE.SIGMA)
        {
          peaks_over_cutoff.push_back(i);
        }
        int iZ = std::floor(ZM);
        if (iZ >= 0)
        {
          histogram[iZ]++;
          maxStars = std::max(maxStars,histogram[iZ]);
        }
      }
      // the maximum across is 50
      floatType columns = 50;
      //Can not have less than one star per value, divStarFactor must be at least 1
      floatType min_divStarFactor = 1.0;
      floatType divStarFactor = std::max(min_divStarFactor,static_cast<floatType>(maxStars)/columns);
      int grad = static_cast<int>(divStarFactor*10);
      output.push_back(phasertng::snprintftos("    %-10d%-10d%-10d%-10d%-10d%-10d",0,grad,grad*2,grad*3,grad*4,grad*5));
      output.push_back(phasertng::snprintftos("    %-10s%-10s%-10s%-10s%-10s%-10s","|","|","|","|","|","|"));
      for (unsigned h = 0; h < histogram.size(); h++)
      {
        std::string stars = h >= 10 ? std::to_string(h) + "   " : " " + std::to_string(h) + "   ";
        for (int star = 0; star < static_cast<floatType>(histogram[h])/divStarFactor; star++)
          stars += "*";
        if (histogram[h]) stars += " (" + std::to_string(histogram[h]) + ")";
        output.push_back(stars);
      }
    }

    if (peaks_over_cutoff.size())
    {
      //cluster the peaks - i.e. find the top peaks within separation_dist
      //all the flagged peaks are guaranteed to be separation_dist apart
      bool1D cluster_top(peaks_over_cutoff.size(),true);
      int dottenth=2;
      int wA = std::max(std::log10(A())+1,2.)+dottenth;
      int wB = std::max(std::log10(B())+1,2.)+dottenth;
      int wC = std::max(std::log10(C())+1,2.)+dottenth;
      int cellw = std::max(wC,std::max(wA,wB));
      header = true;
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      {
        int cluster_top_num(0);
        floatType cluster_top_dist(0);
        dvect3 peak_site_j(peaks_Zscores[j].site);
        dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
        floatType min_sqrtDelta = std::numeric_limits<floatType>::max();
        for (unsigned i = 0; i < j; i++) //check peaks higher up the list
        {
          dvect3 peak_site_i(peaks_Zscores[i].site);
          for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
          {
            dvect3 frac_i = SpaceGroup::doSymXYZ(isym,peak_site_i);
            dvect3 cellTrans;
            for (cellTrans[0] = -2; cellTrans[0] <= 2; cellTrans[0]++)
              for (cellTrans[1] = -2; cellTrans[1] <= 2; cellTrans[1]++)
                for (cellTrans[2] = -2; cellTrans[2] <= 2; cellTrans[2]++)
                {
                  dvect3 fracCell_i = frac_i + cellTrans;
                  dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
                  floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
                  if (sqrtDelta < min_sqrtDelta)
                  {
                    min_sqrtDelta = sqrtDelta; //find closest one higer up
                    cluster_top_num = i;
                    cluster_top_dist = sqrtDelta;
                    if (sqrtDelta < separation_dist) cluster_top[j] = false;
                  }
                }
          }
        }
        if (header)
        {
          output_extra.push_back("");
          output_extra.push_back(cC1);
          output_extra.push_back("");
          holes ? output_extra.push_back("--Cluster Analysis HOLES--"+xx):
                  output_extra.push_back("--Cluster Analysis PEAKS--"+xx);
          peaks_over_cutoff.size() == 1 ?
            output_extra.push_back("There is 1 site"):
            output_extra.push_back("There are " + std::to_string(peaks_over_cutoff.size()) + " sites");
          output_extra.push_back(phasertng::snprintftos("%3s %-7s %s=%s  %5s %10s %8s",
            "#","Z/M",
            phasertng::dag::header_fract().c_str(),
            phasertng::dag::header_ortht(cellw).c_str(),
            "Top","Cluster To #","Distance"));
          header = false;
        }
        auto orth = cctbxUC.orthogonalization_matrix()*peak_site_j;
        output_extra.push_back(phasertng::snprintftos("%3i %7.1f %s=%s  %5s %10s %8s",
           j+1,peaks_Zscores[j].ZonM(),
           phasertng::dag::print_fract(peak_site_j).c_str(),
           phasertng::dag::print_ortht(orth,cellw).c_str(),
           cluster_top[j] ? " Y " : " N ",
           std::string(j ? std::to_string(cluster_top_num+1) : "---").c_str(),
           std::string(j ? phasertng::dtos(cluster_top_dist,7,2)  : "---").c_str()));
        if (j+1 == io_nprint)
        {
          int more = peaks_over_cutoff.size()-io_nprint;
          if (peaks_over_cutoff.size() > io_nprint)
            output_extra.push_back("--- etc " + phasertng::itos(more) + " more" );
          break;
        }
      }
      int nclus(0);
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
        if (cluster_top[j]) nclus++;
      nclus == 1 ?
        output_extra.push_back("There is 1 clustered site"):
        output_extra.push_back("There are " + std::to_string(nclus) + " clustered sites");
      //now check clashes with atoms
      bool1D close_to_old(peaks_over_cutoff.size(),false);
      int1D  close_to_old_num(peaks_over_cutoff.size(),-1); //flag initialization with -1
      float1D close_to_old_dist(peaks_over_cutoff.size(),0);
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      //calculate not just cluster_top peaks, as others may affect B-swapping
      {
        floatType min_sqrtDelta = std::numeric_limits<floatType>::max();
        dvect3 peak_site_j(peaks_Zscores[j].site);
        dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
        for (unsigned a = 0; a < atoms.size(); a++)
        {
          for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
          {
            dvect3 frac_i = SpaceGroup::doSymXYZ(isym,atoms[a].SCAT.site);
            dvect3 cellTrans;
            for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
              for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
                for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
                {
                  dvect3 fracCell_i = frac_i + cellTrans;
                  dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
                  floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
                  if (sqrtDelta < min_sqrtDelta)
                  {
                    min_sqrtDelta = sqrtDelta;
                    close_to_old_num[j] = a;
                    close_to_old_dist[j] = sqrtDelta;
                    if (sqrtDelta < separation_dist) close_to_old[j] = true;
                  }
                }
          }
        }
      }

      bool1D accept_new_atom(peaks_over_cutoff.size(),false);
      header = true;
      bool first_Zlimit(true);
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      {
        int a = close_to_old_num[j];
      //  int i = peaks_over_cutoff[j]; //index into original array for heights and xyz
        int M = peaks_Zscores[j].multiplicity;
        floatType ZM = peaks_Zscores[j].ZonM();
        bool this_atoms_bswap(false),this_resurrect_old_atom(false);
        //bswap test for peaks and holes
        //always take top if top is requested
        double Zlimit = std::max(maxZM_holes,LLGCOMPLETE.SIGMA);
        if (LLGCOMPLETE.USE_PEAK1_BELOW)
          if (peaks_Zscores[0].ZonM() > LLGCOMPLETE.PEAK1_BELOW_ZSCORE)
            Zlimit = std::min(Zlimit,peaks_Zscores[0].ZonM());
        floatType tol(cctbx::adptbx::b_as_u(0.01));
        if (a >=0)
        {
          this_atoms_bswap = LLGCOMPLETE.USE_BSWAP &&
                             close_to_old[j] &&
                             (ZM > LLGCOMPLETE.SIGMA) &&
                          !atoms[a].XTRA.rejected &&
                          !atoms[a].SCAT.flags.use_u_aniso_only() && //is currently isotropic
                           atoms[a].SCAT.u_iso > lowerU()+tol && atoms[a].SCAT.u_iso < upperU()-tol; //within limits for refinement
          atoms_bswap[a] = atoms_bswap[a] || this_atoms_bswap;
        }
        if (!holes) //additional tests
        {
          if (a >= 0) //there are old atoms
          {
            //resurrection
            this_resurrect_old_atom = close_to_old[j] &&
                                      (ZM > LLGCOMPLETE.SIGMA) &&
                                      atoms[a].XTRA.rejected;
            resurrect_old_atom[a] = resurrect_old_atom[a] || this_resurrect_old_atom;
          }
          //new site
          bool top_site = (peaks_Zscores[j].ZonM() >= maxZM_holes);
          bool top_site_under_hole = (j==0) and //top
                 LLGCOMPLETE.USE_PEAK1_BELOW and
                 peaks_Zscores[0].ZonM() > LLGCOMPLETE.PEAK1_BELOW_ZSCORE;
          top_site = top_site or top_site_under_hole; //in addition allow highest below deepest
          accept_new_atom[j] = !close_to_old[j] && //not separation_dist from old atom
                              cluster_top[j] && //must be separation_dist apart from any others
                              top_site &&
                              !this_resurrect_old_atom && !this_atoms_bswap; //except if the peak is being used to bswap or resurrect
        }
        if (holes)
        {
          if (header)
          {
            output_extra.push_back("");
            output_extra.push_back(cC1);
            output_extra.push_back("");
            output_extra.push_back("--B-Swap Analysis HOLES--"+xx);
   //       output_extra.push_back("Holes within " + phasertng::dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
            output_extra.push_back(phasertng::snprintftos("%-5s %2s %6s %6s %6s %6s to %-5s %5s %5s %4s %6s",
                 "Z-scr","M","X","Y","Z","Dist.","#","Rej\'d","Res\'d","AnoB","B-Swap"));
            header = false;
          }
          if (j+1 <= io_nprint)
          {
            auto site = peaks_Zscores[j].site;
            output_extra.push_back(phasertng::snprintftos("%5.1f %2i %6.3f %6.3f %6.3f %6s to %-5s %5s %5s %4s %6s",
              peaks_Zscores[j].ZonM(),M,site[0],site[1],site[2],
                       std::string((a < 0) ? "---" : phasertng::dtos(close_to_old_dist[j],6,2) ).c_str(),
                       std::string((a < 0) ? "---" : phasertng::itos(close_to_old_num[j]+1)).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].XTRA.rejected ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].XTRA.restored ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].SCAT.flags.use_u_aniso_only() ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (this_atoms_bswap ? "Y" : "N")).c_str()));
          }
          //and store the numbers of the edited sites
          if (a >= 0)
          {
            if (atoms_bswap[a]) new_sites.anisotropic.insert(a);
          }
        }
        else //peaks
        {
          std::string tab("   ");
          if (header)
          {
            output.push_back("");
            output.push_back(cC1);
            output.push_back("");
            output.push_back("--New atoms, B-swap and Resurrection Analysis PEAKS--"+xx);
//          output.push_back("Peaks within:");
//          output.push_back(tab+phasertng::dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
//          output.push_back(tab+phasertng::dtos(separation_dist,2) + "A of a rejected old atom will resurrect the atom");
//          output.push_back("Peaks not within:");
//          output.push_back(tab+phasertng::dtos(separation_dist,2) + "A of a higher peak will qualify as new atoms");
//          output.push_back(tab+phasertng::dtos(separation_dist,2) + "A of an old atom will qualify as new atoms");
//          output.push_back("Peak heights:");
            if (LLGCOMPLETE.PEAKS_OVER_HOLES)
            output.push_back("Z/M > " + phasertng::dtos(maxZM_holes,1) + " (the depth of the biggest hole)");
            if (LLGCOMPLETE.USE_PEAK1_BELOW and peaks_Zscores.size())
            {
              auto zstr1 = phasertng::dtos(peaks_Zscores[0].ZonM(),1);
              auto zstr2 = phasertng::dtos(LLGCOMPLETE.PEAK1_BELOW_ZSCORE,1);
              output.push_back("Z/M > "+zstr1+" (top peak below deepest hole provided above Z-score=" +zstr2+")");
            }
            output.push_back("Z/M > " + phasertng::dtos(LLGCOMPLETE.SIGMA,1) + " (minimum Z-score)");
            output.push_back("Peak Table:");
            output.push_back(phasertng::snprintftos("%5s %2s %6s %6s %6s %6s to %3s %5s %5s %4s %6s %5s %3s %3s",
             "Z/M","M","X","Y","Z","Dist.","#","Rej\'d","Res\'d","AnoB","B-Swap","Renew","Top","New"));
            header = false;
          }
          if (first_Zlimit && peaks_Zscores[j].ZonM() < Zlimit)
          {
            output.push_back("<------ Z-score cutoff for new atoms ------>");
            first_Zlimit = false;
          }
          if (j+1 <= io_nprint)
          {
            auto site = peaks_Zscores[j].site;
            output.push_back(phasertng::snprintftos("%5.1f %2i %6.3f %6.3f %6.3f %6s to %3s %5s %5s %4s %6s %5s %3s %3s",
              peaks_Zscores[j].ZonM(),M,site[0],site[1],site[2],
                       std::string((a < 0) ? "---" : phasertng::dtos(close_to_old_dist[j],6,2) ).c_str(),
                       std::string((a < 0) ? "---" : phasertng::itos(close_to_old_num[j]+1)).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].XTRA.rejected ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].XTRA.restored ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (atoms[a].SCAT.flags.use_u_aniso_only() ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (this_atoms_bswap ? "Y" : "N")).c_str(),
                       std::string((a < 0) ? "---" : (this_resurrect_old_atom ? "Y" : "N")).c_str(),
                       std::string(cluster_top[j] ? "Y" : "N").c_str(),
                       std::string(accept_new_atom[j] ? "Y" : "N").c_str()));
          }
          //now store the added sites
          if (accept_new_atom[j])
          {
            //int i = peaks_over_cutoff[j]; //index into original array for heights
            int t = atomtype2t[LLGC_ATOMTYPE];
            data_atom new_atom;
            new_atom.SCAT = cctbx::xray::scatterer<double>(
              "Added NCYC="+std::to_string(c)+" Z=" + phasertng::dtos(peaks_Zscores[j].ZonM(),1), // label
              peaks_Zscores[j].site, // site
              av_u_iso, // u_iso
              av_occ, // occupancy
              LLGC_ATOMTYPE, // scattering_type
              AtomFp[t], // fp
              AtomFdp[t]); // fdp
            new_atom.set_extra_defaults();  //all defaults
            new_atom.XTRA.zscore = peaks_Zscores[j].ZonM();
            new_sites.atoms.push_back(new_atom);
          }
          //and store the numbers of the edited sites
          if (a >= 0)
          {
            if (atoms_bswap[a]) new_sites.anisotropic.insert(a);
            restored_data this_restored_data(LLGC_ATOMTYPE,peaks_Zscores[j].ZonM(),av_occ);
            if (resurrect_old_atom[a]) new_sites.restored.insert(std::pair<int,restored_data>(a,this_restored_data));
          }
        }
        if (j+1 == io_nprint)
        {
          int more = peaks_over_cutoff.size()-io_nprint;
          if (peaks_over_cutoff.size() > io_nprint)
            output_extra.push_back("--- etc " + phasertng::itos(more) + " more" );
        }
      }
      if (first_Zlimit and peaks_over_cutoff.size() >= io_nprint)
      {
        output.push_back("<------ Z-score cutoff for new atoms ------>");
      }
    }
    else
    {
      if (!holes) output_extra.push_back("No peaks for new atoms above cut-off");
    }
  }//peaks and holes

  output_extra.push_back("");
  output_extra.push_back(cC1);
  output_extra.push_back("");
  output_extra.push_back("--Identify New Atoms for Addition--"+xx);
  header = true;
  for (unsigned j = 0; j < new_sites.atoms.size(); j++)
  {
    if (header) //header
    {
      output_extra.push_back("Peaks over Z-score cutoff");
      output_extra.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %5s %2s",
            "#","X","Y","Z","Z/M","M"));
      header = false;
    }
    cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),new_sites.atoms[j].SCAT.site);
    int M = SpaceGroup::NSYMM/peak_sym.multiplicity();
    output_extra.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %5.1f %2i",
                   j+1,
                   new_sites.atoms[j].SCAT.site[0],new_sites.atoms[j].SCAT.site[1],new_sites.atoms[j].SCAT.site[2],
                   new_sites.atoms[j].XTRA.zscore,M));
    if (j+1 == io_nprint)
    {
      int more = new_sites.atoms.size()-io_nprint;
      if (new_sites.atoms.size() > io_nprint)
        output_extra.push_back("--- etc " + phasertng::itos(more) + " more" );
      break;
    }
  }
  if (!new_sites.atoms.size()) output_extra.push_back("No new atoms identified");

  std::string tab("   ");
  output_extra.push_back("");
  output_extra.push_back(cC1);
  output_extra.push_back("");
  output_extra.push_back("--Identify Old Atoms for Editing--"+xx);
  output_extra.push_back("Old atoms edited if:");
  output_extra.push_back(tab+"peak < " + phasertng::dtos(separation_dist,2) + "A from a previously deleted atom will resurrect the old atom");
  output_extra.push_back(tab+"peak or hole < " + phasertng::dtos(separation_dist,2) + "A from an old atom will turn it anisotropic");
  int nedited = new_sites.nanisotropic() + new_sites.nrestored();
  if (nedited)
  {
    output_extra.push_back("Edited atoms list:");
    output_extra.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %12s %8s", "#", "X","Y","Z","occupancy","Atom"));
  }
  for (std::set<int>::iterator iter = new_sites.anisotropic.begin(); iter != new_sites.anisotropic.end(); iter++)
  {
    int a = (*iter);
    output_extra.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %12.4f %8s  Anisotropic",
      a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str()));
  }
  std::set<int> unique_a;
  for (std::multimap<int,restored_data>::iterator iter = new_sites.restored.begin(); iter != new_sites.restored.end(); iter++)
  {
    int a = iter->first;
    if (unique_a.find(a) == unique_a.end())
    {
      unique_a.insert(a);
      output_extra.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %12.4f %8s  Restored ",
        a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],av_occ,atoms[a].SCAT.scattering_type.c_str()));
    }
  }
  if (!nedited) output_extra.push_back("No atoms for editing");

  output_extra.push_back("");
  output_extra.push_back(cC1);
  output_extra.push_back("");
  output_extra.push_back("--Summary--"+xx);
  output_extra.push_back("Number of new atoms identified this cycle = " + std::to_string(new_sites.atoms.size()));
  output_extra.push_back("Number of old isotropic atoms identified as anisotropic = " + std::to_string(new_sites.nanisotropic()));
  output_extra.push_back("Number of old atoms previously identified for restoration = " + std::to_string(new_sites.nrestored()));
  if (PTNCS.use_and_present())
  {
    output_extra.push_back("Translational NCS correction factors used this cycle");
    output_extra.push_back("Sites added individually (not in pairs related by tncs vector)");
  }
  return std::make_tuple(new_sites,output,output_extra);
}

std::pair<std::set<int>,af_string>
RefineSAD::findDeletedAtoms(std::vector<std::pair<int,std::string> > changed)
{
  int io_nprint = 20;
  af_string output_extra;
  output_extra.push_back("");
  output_extra.push_back("--Identify Old Atoms for Deletion--");
  output_extra.push_back("Low occupancy sites identified for deletion");
  output_extra.push_back("Minimum occupancy depends on scattering type f\"");
  output_extra.push_back("Atoms previously restored are not deleted, to prevent cycling");
  std::set<int> atoms_deleted;
  unsigned nfail(0),ndeleted(0),nrestored(0);
  float1D maxOcc(AtomFdp.size(),0),minOcc(AtomFdp.size(),0),rmOcc(AtomFdp.size());
  std::set<int> changed_set;
  for (int c = 0; c < changed.size(); c++)
    changed_set.insert(changed[c].first);
  if (atoms.size())
  {
    bool1D first_call(AtomFdp.size(),true);
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected && (changed_set.find(a) == changed_set.end()))
    {
      floatType rmFactor(0.1);
      int t = atomtype2t[atoms[a].SCAT.scattering_type];
      if (first_call[t])
      {
        maxOcc[t] = 0;
        minOcc[t] = std::numeric_limits<floatType>::max();
        rmOcc[t] = 0;
      }
      first_call[t] = false;
      maxOcc[t] = std::max(maxOcc[t],atoms[a].SCAT.occupancy);
      minOcc[t] = std::min(minOcc[t],atoms[a].SCAT.occupancy);
      if (atoms[a].SCAT.scattering_type != "AX" && atoms[a].SCAT.scattering_type != "RX")
        rmOcc[t] = maxOcc[t]*rmFactor; // Purely real or imaginary are special
    }
    output_extra.push_back(phasertng::snprintftos("%5s %12s %12s %12s %12s", "Atom","f\"","max occ","min occ","cutoff occ"));
    for (int t = 0; t < AtomFdp.size(); t++)
      output_extra.push_back(phasertng::snprintftos("%5s %12.4f %12.4f %12.4f %12.4f", t2atomtype[t].c_str(),AtomFdp[t],maxOcc[t],minOcc[t],rmOcc[t]));
    bool header = true;
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected && (changed_set.find(a) == changed_set.end()))
    {
      int t = atomtype2t[atoms[a].SCAT.scattering_type];
      if (atoms[a].SCAT.occupancy < rmOcc[t])
      {
        if (header) //header
        {
          output_extra.push_back("Deleted atoms list:");
          output_extra.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %12s %8s", "#", "X","Y","Z","occupancy","Atom"));
          header = false;
        }
        nfail++;
        atoms[a].XTRA.restored ? nrestored++ : ndeleted++;
        if (nfail < io_nprint)
        output_extra.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %12.4f %8s  %s",
          a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str(),
          (atoms[a].XTRA.restored ? "(Previously restored)" : "Rejected" )));
        if (!atoms[a].XTRA.restored) atoms_deleted.insert(a);
      }
    }
    if (nfail >= io_nprint)
    {
      int more = nfail-io_nprint;
      output_extra.push_back("--- etc " + std::to_string(more) + " more");
    }
  }
  output_extra.push_back("Number of old atoms with occupancy less than cut-off = " + std::to_string(nfail));
  output_extra.push_back("Number of these previously restored (therefore not deleted) = " + std::to_string(nrestored));
  output_extra.push_back("Number of old atoms identified for deletion = " + std::to_string(ndeleted));
  return { atoms_deleted,output_extra };
}

std::pair<std::vector<std::pair<int,std::string>>,af_string>
RefineSAD::findChangedAtoms(data_llgc LLGCOMPLETE)
{
  af_string output_extra;
  std::vector<std::pair<int,std::string> > changed;
  bool IMAGINARY(LLGCOMPLETE.ATOMTYPE == stringset({"AX"}));
  if (atoms.size() && (AtomFdp.size() > 1 || IMAGINARY))
  {
    output_extra.push_back("");
    output_extra.push_back("--Identify Atoms for Change of Atomtype--");
    output_extra.push_back("Change atomtype to put expected occupancy (based on f\") closest to target");
    if (!LLGCOMPLETE.ATOM_CHANGE_ORIG)
      output_extra.push_back("Only changes atoms that have been added during completion");
    floatType targetOcc(0.9);
    output_extra.push_back("Target occupancy = " + std::to_string(targetOcc));
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].XTRA.rejected
         && (!atoms[a].ORIG || LLGCOMPLETE.ATOM_CHANGE_ORIG)
         && atoms[a].SCAT.scattering_type != "RX"
         && (atoms[a].SCAT.scattering_type == "AX" && IMAGINARY || atoms[a].SCAT.scattering_type != "AX"))
    {
      floatType minDiffOcc = std::numeric_limits<floatType>::max();
      std::string new_scattering_type = atoms[a].SCAT.scattering_type;
      for (int t = 0; t < t2atomtype.size(); t++)
      {
        std::string atomtype = t2atomtype[t];
        if (LLGCOMPLETE.ATOMTYPE.count(atomtype))
        {
          int old_t = atomtype2t[atoms[a].SCAT.scattering_type];
          floatType new_occupancy =  AtomFdp[t] ? atoms[a].SCAT.occupancy*AtomFdp[old_t]/AtomFdp[t] :
                                                  atoms[a].SCAT.occupancy;
          // Use heuristic difference score that penalises very low occupancies
          floatType diffOcc = (new_occupancy >= targetOcc) ?
              new_occupancy-targetOcc : std::log(targetOcc/new_occupancy);
          if (diffOcc < minDiffOcc)
          {
            minDiffOcc = diffOcc;
            new_scattering_type = atomtype;
          }
        }
      }
      if (atoms[a].SCAT.scattering_type != new_scattering_type)
        changed.push_back(std::pair<int,std::string>(a,new_scattering_type));
    }
    output_extra.push_back("Number of old atoms identified for change of atomtype = " + std::to_string(changed.size()));
  }
  return { changed, output_extra };
}

af_string RefineSAD::changeAtoms(std::vector<std::pair<int,std::string> >& changed)
{
  af_string output_extra;
  if (changed.size())
  {
    output_extra.push_back("");
    output_extra.push_back("--Change Atomtype--");
    output_extra.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %-8s %-9s %-8s %-9s","","","","","Old","Old","New","New"));
    output_extra.push_back(phasertng::snprintftos("%-5s %6s %6s %6s %8s %9s %8s %9s", "#", "X","Y","Z","Atomtype","Occupancy","Atomtype","Occupancy"));
    for (unsigned i = 0; i < changed.size(); i++)
    {
      int a = changed[i].first;
      std::string atomtype = changed[i].second;
      int old_t = atomtype2t[atoms[a].SCAT.scattering_type];
      int new_t = atomtype2t[atomtype];
      floatType new_occupancy = atoms[a].SCAT.occupancy*AtomFdp[old_t]/AtomFdp[new_t];
      output_extra.push_back(phasertng::snprintftos("%-5d %6.3f %6.3f %6.3f %8s %9.4f %8s %9.4f",
          a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],
             atoms[a].SCAT.scattering_type.c_str(),atoms[a].SCAT.occupancy,
             atomtype.c_str(),new_occupancy));
      atoms[a].SCAT.scattering_type = atomtype;
      atoms[a].SCAT.fp = AtomFp[new_t];
      atoms[a].SCAT.fdp = AtomFdp[new_t];
      atoms[a].SCAT.occupancy = new_occupancy;
    }
    output_extra.push_back("Number of old atoms that have changed atomtype = " + std::to_string(changed.size()));
  }
  return output_extra;
}

std::pair<double,af_string>
RefineSAD::phase_o_phrenia(size_t maxpeaks)
{
  //maxpeaks is the maximum number of peaks output in plot
  //optimize speed versus phaser by only clustering until maxpeaks is reached
  af_string output;
  double skew(0);
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell UCell = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group      SgOps(SpaceGroup::getCctbxSG());
  cctbx::sgtbx::space_group_type SgInfo(SgOps);

  //difference with phase-o-phrenia is that this does not generate all reflections to 3.0
  //it just uses the reflections that are present to 3.0
  //atomic data are already calculated
  //So of the data are to lower resolution or not complete then the result will not be the same
  double maxreso = 3.0;
  af_cmplx PHISUB;
  af::shared<miller::index<int> > hires_miller;
  for (int r = 0; r < NREFL; r++)
  if (reso(miller[r]) > maxreso)
  {
    PHISUB.push_back(std::polar<double>(1.0,2*std::arg(FApos[r]+FAneg[r])));
    hires_miller.push_back(miller[r]);
  }

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  floatType d_min = cctbx::uctbx::d_star_sq_as_d(UCell.max_d_star_sq(hires_miller.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  floatType resolution_factor = 1.0/(2.0*1.5);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
      UCell,d_min,resolution_factor,sym_flags,
      SgInfo,
      mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

  bool anomalous_flag(false);
  bool conjugate_flag(true);
  bool treat_restricted(false);
  af::c_grid_padded<3> map_grid(rfft.n_complex());
  cctbx::maptbx::structure_factors::to_map<floatType> gradmap(
    SgOps, //PtNcs requires P1
    anomalous_flag,
    hires_miller.const_ref(),
    PHISUB.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag,
    treat_restricted);
  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref(
    gradmap.complex_map().begin(),
    af::c_grid<3>(rfft.n_complex()));

  // --- do the fft
  rfft.backward(gradmap_fft_ref);

  af::versa<floatType, af::flex_grid<> > real_map(
    gradmap.complex_map().handle(),
    af::flex_grid<>(af::adapt((rfft.m_real())))
          .set_focus(af::adapt(rfft.n_real())));
  cctbx::maptbx::unpad_in_place(real_map);
  af::versa<floatType, af::c_grid<3> > real_map_unpadded(
    real_map, af::c_grid<3>(rfft.n_real()));

  // --- find symmetry equivalents
  af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
      real_map_unpadded.begin(),
      af::c_grid_padded<3>(real_map_unpadded.accessor()));
  tags.build(SgInfo, sym_flags); //PtNcs requires P1
  af::ref<long, af::c_grid<3> > tag_array_ref(
    tags.tag_array().begin(),
    af::c_grid<3>(tags.tag_array().accessor()));
  PHASER_ASSERT(tags.verify(real_map_const_ref));

  // --- peak search
  int peak_search_level(1);
  std::size_t max_peaks(0);
  bool interpolate(true);
  cctbx::maptbx::peak_list<> peak_list(
    real_map_const_ref,
    tag_array_ref,
    peak_search_level,
    max_peaks,
    interpolate);

  af::shared<double> peak_heights = peak_list.heights();
  af::shared<dvect3> peak_sites = peak_list.sites();

  bool1D cluster_top(peak_sites.size(),true);
  double min_sqrtDelta = 4.0; //phase-o-phrenia default
  bool do_cluster(true);
  if (do_cluster)
  if (peak_heights.size())
  {
    //cluster the peaks - i.e. find the top peaks within separation_dist
    //all the flagged peaks are guaranteed to be separation_dist apart
    for (unsigned j = 0; j < peak_sites.size(); j++)
    {
      dvect3 peak_site_j(peak_sites[j]);
      dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
      bool flag = true;
      for (unsigned i = 0; i < j; i++) //check peaks higher up the list
      {
        dvect3 peak_site_i(peak_sites[i]);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMM && flag; isym++)
        {
          dvect3 frac_i = SpaceGroup::doSymXYZ(isym,peak_site_i);
          dvect3 cellTrans;
          for (cellTrans[0] = -2; cellTrans[0] <= 2 && flag; cellTrans[0]++)
          for (cellTrans[1] = -2; cellTrans[1] <= 2 && flag; cellTrans[1]++)
          for (cellTrans[2] = -2; cellTrans[2] <= 2 && flag; cellTrans[2]++)
          {
            dvect3 fracCell_i = frac_i + cellTrans;
            dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
            double sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
            if (sqrtDelta < min_sqrtDelta)
            {
              cluster_top[j] = false;
              flag = false;
            }
          }
        }
      }
      //speed up the clustering so we only cluster to get the top number of peaks
      int count(0);
      for (unsigned i = 0; i < j; i++)
        if (cluster_top[j]) count++;
      if (count > maxpeaks) break;
    }
  }

  if (peak_heights.size()) //i.e not a flat map
  {
    double maxval = peak_heights[0];
    int last(0);
    for (unsigned i = 0; i < peak_sites.size(); i++)
    if (cluster_top[i])
    {
      peak_sites[last] = peak_sites[i];
      peak_heights[last] = peak_heights[i]/maxval; //fraction
      last++;
      if (last == maxpeaks) break;
    }
    peak_sites.erase(peak_sites.begin()+last,peak_sites.end()); //rubbish
    peak_heights.erase(peak_heights.begin()+last,peak_heights.end()); //rubbish
    auto& v = peak_heights; //alias
    double sum(0);
    for (int i = 0; i < v.size(); i++) sum += v[i];
    double mean = sum / v.size();
    sv_double diff(v.size());
    for (int i = 0; i < v.size(); i++) diff[i] = v[i] - mean;
    double sq_sum(0);
    for (int i = 0; i < diff.size(); i++) sq_sum += diff[i]*diff[i] ;
    double stdev = std::sqrt(sq_sum / v.size());
    double median = v[9];
    //Pearson 2 skewness
    //skew = 3*(mean-median)/stdev;
    double cubic(0);
    for (int i = 0; i < v.size(); i++) cubic += fn::pow3(v[i] - mean);
    skew = (cubic/v.size())/fn::pow3(stdev);

    int npks = std::min(peak_heights.size(),maxpeaks);
    output.push_back("Phase-o-Phrenia Histogram");
    output.push_back("-------------------------");
    output.push_back("Space Group: " + spcgrpname());
    output.push_back("Top "+std::to_string(npks)+" peaks in substructure density are plotted");
    output.push_back("Peaks heights reported as percentage of top peak");
    output.push_back("Skewed plots indicate centrosymmetry in substructure and poor phasing");
    output.push_back("Skew = " + phasertng::dtos(skew,3));
    std::string pk2 = phasertng::dtos(peak_heights[1]*100,0) +"%";
    std::string pk5 = phasertng::dtos(peak_heights[4]*100,0) +"%";
    std::string pk20 = phasertng::dtos(peak_heights[19]*100,0) +"%";
    output.push_back("Peaks 2/5/20 = " + pk2 + "/" + pk5 + "/" + pk20);
   // output.push_back("Peaks clustered before plot = " + std::string(do_cluster ? "on" : "off"));
   // if (do_cluster) output.push_back("Cluster distance = " + phasertng::dtos(min_sqrtDelta,2));
   // output.push_back("Maximum resolution of density = " + phasertng::dtos(maxreso,2));
    int width = 50;
    output.push_back(" #|" + std::string(width,'-'));
    for (int i = 0; i < npks; i++)
    {
      int stars = std::ceil(peak_heights[i]*width);
      std::string hstr(stars,'*');
      output.push_back(phasertng::itos(i+1,2) + "|" + hstr + " (" + phasertng::dtos(peak_heights[i]*100,0) + "%)");
    }
    output.push_back("  |" + std::string(width,'-'));
    if (false)
    {
    output.push_back("Peak List:");
    output.push_back("Relative height  Fractional coordinates");
    for (std::size_t i = 0; i < npks; i++)
      output.push_back(phasertng::itos(i+1,3) + ": " + phasertng::dtos(peak_heights[i],7,2) + "     " + phasertng::dvtos(peak_sites[i],8,5));
    output.push_back("");
    }
  }
  return {skew ,output};
}
} //phaser
