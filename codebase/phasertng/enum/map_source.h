//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_map_source_enum__
#define __phasertng_map_source_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace map {
  enum source {
             UNDEFINED, //phasertng namespace, use unique name
             NONE,
             XRAY,
             EM
           };
/*
  MakeEnum ( source,
             (UNDEFINED) //phasertng namespace, use unique name
             (NONE)
             (XRAY)
             (EM)
           );
*/
}} //phasertng

#endif
