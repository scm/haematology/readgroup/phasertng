from pathlib import Path
from phasertng import Phasertng,Identifier,FileSystem,enum_out_stream,enum_err_code,enum_other_data
import argparse
import matplotlib  as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.colors import LinearSegmentedColormap, Normalize
import numpy as np
import os, os.path

class plot_self_rotation_function:
#{{{
#https://stackoverflow.com/questions/66520769/python-contour-polar-plot-from-discrete-data

  def truncate_colormap(self,cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mpl.colors.LinearSegmentedColormap.from_list(
          'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
          cmap(np.linspace(minval, maxval, n)))
    return new_cmap, None

  def constalpha(self):
    return 1

  def bounded_colormap(self):
    # Define a custom colormap
    darkpurple = '#4B0082'
    brightyellow = '#FFEA00'
    lightpink = '#FFB6C1'
    colors = ['grey', 'red',brightyellow,'orange','lightgreen', 'forestgreen', 'cyan', 'blue','plum','magenta', 'burlywood', 'brown','black']
    n_bins = len(colors)  # Total number of bins (12)
    # Create a continuous colormap
   #bounds = [0,1,2,3,4,5,6,7,8,9,10,11,12]  # Bin edges at 0.5 intervals for centered bins
    bounds = [0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5 ]  # Bin edges at 0.5 intervals for centered bins
    cmap = mcolors.ListedColormap(colors)
    norm = mcolors.BoundaryNorm(bounds,cmap.N)
   #print(cmap(norm(3)))
    return cmap, norm


  def chi_section_file(self,io_database,io_indir,io_filename,io_outdir,io_greyscale,suptitle,debug):
    N = 100 #number of points for plotting/interpolation
  # io_name = os.path.join(io_database,io_indir,io_filename)
    io_name = io_filename
    with open(io_name, "r") as f:
      if not f.read().strip():
        return None,None
    y, x, z = np.genfromtxt(io_name, unpack=True)
    xll = x.min(); xul = x.max(); yll = y.min(); yul = y.max();

    tol = 25
    xi = np.linspace(-tol, 360+tol, N) #azimuthal
    yi = np.linspace(-tol, 90+tol, N) #zenith
    xj,yj = np.meshgrid(xi, yi)
    try:
      import scipy.interpolate
      zi = scipy.interpolate.griddata((x, y), z, (xi[None,:], yi[:,None]), method='linear')
    except:
      return None,None
    zj = zi.reshape(len(xi),len(yi))

    plt.rcParams.update({'figure.max_open_warning': 0})
  # fig = plt.figure(figsize=(8.27,11.69),constrained_layout=True) #do not use contrained layout, buggers polar plot
    fig = plt.figure(figsize=(8.27, 11.69))
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)  # Adjust these pscore as needed for padding
    gs = mpl.gridspec.GridSpec(ncols=2, nrows=2, figure=fig, width_ratios=[3, 0.1])
    ax = fig.add_subplot(gs[0,0]) #chi

    vmax = 100
    levels = np.linspace(0,vmax,11)
    cmap = 'Greys'
    cmap,norm = self.truncate_colormap(plt.get_cmap('hsv'),0.16666,1.0)
    if (io_greyscale):
      cmap = 'Greys'
    contours = ax.contourf(xj,yj,zj,levels=levels,alpha=self.constalpha(),cmap=cmap,vmin=1,vmax=vmax)

    pad=5
    plotrepeat=20
    plt.xlim(0,360)
    plt.xlim(-plotrepeat, 360+plotrepeat)
    plt.ylim(0,90)
    plt.ylim(-pad, 90+pad)
    plt.grid()
    plt.xticks(np.arange(0,361,30))
    plt.yticks(np.arange(0,91,10))
    ax.set_aspect(2)
    ax.set_xlabel("Azimuth (from X)")
    ax.set_ylabel("Zenith (from Z)")

    cax = fig.add_subplot(gs[0, 1]) #fraction=0.5, pad=0.04)
    cax.set_aspect(15) #chi
    cbar = fig.colorbar(contours,cax=cax,ticks=levels)
    cbar.set_label("Peak Height%")

    ax = fig.add_subplot(gs[1,0]) #chi
    theta = np.radians(xj) #azimuths
    rad = yj
    #this is NOT A POLAR PLOT this is cartesian without axes
    #plot in the orientation for molrep, which has 0 at south
    x = rad*np.sin(theta)
    y = -rad*np.cos(theta)
    contours = ax.contourf(x,y,zi,levels=levels,alpha=self.constalpha(),cmap=cmap,norm=norm) #vmin=0,vmax=vmax)
    #with contrained_layout off there is no need for padding
    pad=10
    ax.set_xlim(-90-pad,90+pad)
    ax.set_ylim(-90-pad,90+pad)
    ax.set_aspect('equal')
    ax.axis('off')

    ax_polar = fig.add_axes(ax.get_position(), polar=True)
    ax_polar.set_facecolor('none') # make transparent
  # ax_polar.set_xticks(np.arange(0,361,30))
    ax_polar.set_theta_zero_location("E")
    ax_polar.set_xticks(np.arange(0,2*np.pi,np.pi/6))
    ax_polar.set_aspect('equal')
    plt.suptitle("Self Rotation Function Chi Section " +io_filename[-7:-4] + suptitle)
    pad = 10
    ax_polar.set_yticks([x for x in range(10,100,10)])
    degree_symbol = chr(176)
    ax_polar.set_yticklabels([f'{int(x)}{degree_symbol}' for x in range(10,100,10)])
    ax_polar.set_xlim(0, 2*np.pi)
    ax_polar.set_ylim(0, 90+pad)
    ax_polar.set_title("Z : Vertical, X : East, Y : North")

    stem = os.path.basename(io_filename)
    stem = str(Path(stem).stem)
    filename = FileSystem(io_database,io_outdir,str(stem +".png"))
    plt.savefig(filename.fstr())
    #print("Self Rotation Function Chi Section File " +filename)
    return plt, filename

  def chi_section_directory(self,io_database,io_indir,io_outdir,io_greyscale,suptitle,debug):
    #sort files in order of creation so that the highest peaks are displayed as figure 1
    logfile = []
    filenames = []
    plt = None
    for f in range(0,181): #reverse engineer the filenames
      txt = FileSystem(io_database,io_indir,io_indir+".chi-"+str(f).zfill(3)+".txt")
      if debug:
        print(txt.fstr(), os.path.exists(txt.fstr()))
      if os.path.exists(txt.fstr()):
        logfile.append("Reading: " + str(txt.fstr()))
        filename = None
        plt,filename = self.chi_section_file(
             io_database=io_database,
             io_indir=io_indir,
             io_filename=txt.fstr(),
             io_outdir=io_outdir,
             io_greyscale=io_greyscale,
             suptitle=suptitle,
             debug=debug)
        if filename is not None:
        # filename = FileSystem(io_database,io_outdir,filename)
          filenames.append(filename)
    return plt,filenames,logfile

#https://stackoverflow.com/questions/66520769/python-contour-polar-plot-from-discrete-data

  def read_peaks_section(self,io_file,polarplot,plotrepeat,debug=False):
    N = 500 #number of points for plotting/interpolation
    azimuth = []; zenith = []; chi = []; order = []; remainder = []; pscore = []; qscore = []; colors = []; area = []; parallel = []; sgsymm = [];
    with open(io_file, "r") as f:
      if not f.read().strip():
        return azimuth,zenith,chi,order,remainder,pscore,qscore,colors,area,parallel,sgsymm
    rzenith,razimuth,rchi,rorder,rremainder,rpscore,rqscore,rparallel,rsgsymm = np.genfromtxt(io_file, unpack=True)
    if np.ndim(rzenith) == 0:
      #returns a scalar so we must convert to list
      rzenith = np.array([rzenith])  # Convert scalar to an array
      razimuth = np.array([razimuth])  # Convert scalar to an array
      rchi = np.array([rchi])  # Convert scalar to an array
      rorder = np.array([rorder])  # Convert scalar to an array
      rremainder = np.array([rremainder])  # Convert scalar to an array
      rpscore = np.array([rpscore])  # Convert scalar to an array
      rqscore = np.array([rqscore])  # Convert scalar to an array
      rparallel = np.array([rparallel])  # Convert scalar to an array
      rsgsymm = np.array([rsgsymm])  # Convert scalar to an array
    if debug:
      print(rzenith,razimuth,rchi,rorder,remainder,rpscore,rqscore,rparallel,rsgsymm)
    #sort the return in order of the rorder so that the higher order peaks are plotted on top of the low order
    sorted_indices = np.argsort(rorder)
    if debug:
      print(sorted_indices)
    rzenith = rzenith[sorted_indices]
    razimuth = razimuth[sorted_indices]
    rchi = rchi[sorted_indices]
    rorder = rorder[sorted_indices]
    rremainder = rremainder[sorted_indices]
    rpscore = rpscore[sorted_indices]
    rqscore = rqscore[sorted_indices]
    rparallel = rparallel[sorted_indices]
    rsgsymm = rsgsymm[sorted_indices]
    if debug:
      print("after sort",rzenith,razimuth,rchi,rorder,rremainder,rpscore,rqscore,rparallel,rsgsymm)
    if not (rzenith.size == 0): #paranoia
      #massage the data for repeats
      length = len(razimuth)
      #we have to create new arrays so that the sort order is preserved
      #this is so that the largest peaks are in the background with smaller on the top
      top_order_phi0 = 0
      top_percent = 100
      if debug:
        print("length",length)
      for i in range(length):
        #delete the multiple peaks at the centre of the plot leaving the highest order
      # if polarplot and rzenith[i] <= 0.01:
      #   if rorder[i] < top_order_phi0:
      #     continue
      #   else:
      #     top_order_phi0 = rorder[i];
        if (razimuth[i] > 360): continue
        if True: #round(rorder[i]) == 5:
            azimuth.append(razimuth[i])
            zenith.append(rzenith[i])
            chi.append(rchi[i])
            order.append(rorder[i])
            remainder.append(rremainder[i])
            pscore.append(rpscore[i])
            qscore.append(rqscore[i])
            parallel.append(rparallel[i])
            sgsymm.append(rsgsymm[i])
          # c = 'pink' #this would be to specify pscore for integers, but pscore are not integers
          # if (rorder[i] == 1):
          #   c = 'silver'
          # colors.append(c)
            if (razimuth[i] > (360-plotrepeat)):
              azimuth.append(razimuth[i]-360)
              zenith.append(rzenith[i])
              chi.append(rchi[i])
              order.append(rorder[i])
              remainder.append(rremainder[i])
              pscore.append(rpscore[i])
              qscore.append(rqscore[i])
              parallel.append(rparallel[i])
              sgsymm.append(rsgsymm[i])
          #   colors.append(c)
            if (razimuth[i] < (plotrepeat)):
              azimuth.append(razimuth[i]+360)
              zenith.append(rzenith[i])
              chi.append(rchi[i])
              order.append(rorder[i])
              remainder.append(rremainder[i])
              pscore.append(rpscore[i])
              qscore.append(rqscore[i])
              parallel.append(rparallel[i])
              sgsymm.append(rsgsymm[i])
          #   colors.append(c)
      azimuth = np.array(azimuth)
      zenith = np.array(zenith)
      chi = np.array(chi)
      order = np.array(order)
      remainder = np.array(remainder)
      pscore = np.array(pscore)
      qscore = np.array(qscore)
      parallel = np.array(parallel)
      sgsymm = np.array(sgsymm)
     #colors = np.where(remainder < 0.04, order, 1)
      colors = order
    # the s in the scatterplot is the area not the radius
      area = 0.02*(pscore)**2
    # area = pscore*2
    if debug:
      print("return arrays")
    return azimuth,zenith,chi,order,remainder,pscore,qscore,colors,area,parallel,sgsymm

  def bespoke_color_map(self,):
    #https://stackoverflow.com/questions/49367144/modify-matplotlib-colormap
    #modified above, use with vmin and vmax to fix the colors in the scatter plot
    # set upper part: 4 * 256/4 entries
   #upper = mpl.cm.jet(np.arange(256))
    middle = mpl.cm.gist_rainbow(np.arange(256))
    # set lower part: 1 * 256/4 entries
    # - initialize all entries to 1 to make sure that the alpha channel (4th column) is 1
    lower = np.ones((int(256/4),4))
    # - modify the first three columns (RGB):
    #   range linearly between white (1,1,1) and the first color of the middle colormap
    for i in range(3):
      lower[:,i] = np.linspace(0.7, middle[0,i], lower.shape[0])

    # set upper part: 1 * 256/4 entries
    upper = np.ones((int(256/4),4))
    # - modify the first three columns (RGB):
    #   range linearly between white (1,1,1) and the first color of the middle colormap
    for i in range(3):
      upper[:,i] = np.linspace( middle[255,i], 0, upper.shape[0])


    # combine parts of colormap
    cmap = np.vstack(( lower, middle,upper ))

    # convert to matplotlib colormap
    cmap = mpl.colors.ListedColormap(cmap, name='myColorMap', N=cmap.shape[0])
    return cmap

  def peaks_section(self,io_file,debug=False,vmax=12.5):
    plotrepeat = 20;
    if debug:
      print("first read peaks section")
    azimuth,zenith,chi,order,remainder,pscore,qscore,colors,area,parallel,sgsymm = self.read_peaks_section(io_file=io_file,plotrepeat=plotrepeat,polarplot=False,debug=debug)
    if debug:
      print("read",(azimuth,zenith,chi,order,remainder,pscore,qscore,colors,area,parallel,sgsymm))
  # cmap = self.bespoke_color_map()
  # cmap,norm = self.truncate_colormap()
    cmap,norm = self.bounded_colormap()
  # fig = plt.figure(figsize=(8.27, 11.69),constrained_layout=True)
    fig = plt.figure(figsize=(8.27, 11.69))
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)  # Adjust these pscore as needed for padding
    gs = mpl.gridspec.GridSpec(ncols=2, nrows=2, figure=fig, width_ratios=[3, 0.1])
    # gs = fig.add_gridspec()??

    ax = fig.add_subplot(gs[0,0]) #peaks
    circle_mask = remainder < 0.04
    square_mask = ~circle_mask
    alpha = np.where(sgsymm == 0, 1, 0.2)
    if debug:
      print("make the scatter")
    #alpha is different cannot be empty array
    a = alpha[circle_mask] if len(azimuth[circle_mask]) else self.constalpha()
    contours =  ax.scatter(azimuth[circle_mask], zenith[circle_mask], c=colors[circle_mask], s=area[circle_mask], alpha=a,cmap=cmap,norm=norm, marker='o')
    a = alpha[square_mask] if len(azimuth[square_mask]) else self.constalpha()
    contours =  ax.scatter(azimuth[square_mask], zenith[square_mask], c=colors[square_mask], s=area[square_mask], alpha=a,cmap=cmap,norm=norm, marker='^')
    import math
    for i, label in enumerate(order):
      if (float(label) > float(vmax)):
        point_radius = math.sqrt(area[i]/np.pi)
       #ax.text(azimuth[i]+point_radius*2, zenith[i], str(round(label)), fontsize=6, ha='center', color='white')
        ax.text(azimuth[i], zenith[i], str(round(label)), fontsize=6, ha='center', color='white')
    plt.xlim(-plotrepeat, 360+plotrepeat)
    pad=5
    plt.ylim(-pad, 90+pad)
    plt.grid()
    plt.xticks(np.arange(0,361,30))
    plt.yticks(np.arange(0,91,10))
    ax.set_xlabel("Azimuth (from X)")
    ax.set_ylabel("Zenith (from Z)")
    ax.set_aspect(2)

    if debug:
      print("colorbar")
    cax = fig.add_subplot(gs[0, 1]) #fraction=0.5, pad=0.04)
    cax.set_aspect(15)
    cbar = fig.colorbar(contours,cax=cax, ticks=[1,2,3,4,5,6,7,8,9,10,11,12,13])
    cbar.set_label("Rotation Cyclic Symmetry")

  # cmap,norm = self.bounded_colormap()
  # cmap = self.bespoke_color_map()
    if debug:
      print("second read peaks section")
    azimuth,zenith,chi,order,remainder,pscore,qscore,colors,area,parallel,sgsymm = self.read_peaks_section(io_file=io_file,plotrepeat=plotrepeat,polarplot=True,debug=debug)
  # ax = fig.add_subplot(gs[0,2]) #peaks
    ax = fig.add_subplot(gs[1,0]) #chi
    theta = np.radians(azimuth)
    rad = zenith
    #this is NOT A POLAR PLOT this is cartesian without axes
    #plot in the orientation for molrep, which has 0 at south
    x = []
    y = []
    if len(theta):
      x = rad*np.sin(theta)
      y = -rad*np.cos(theta)
  # contours = ax.scatter(x,y,c=colors,s=area,alpha=self.constalpha(),cmap=cmap,norm=norm) #vmin=1,vmax=vmax)
    circle_mask = remainder < 0.04
    square_mask = ~circle_mask
    if debug:
      print("make the scatter")
      print("alpha",alpha)
    a = alpha[circle_mask] if len(azimuth[circle_mask]) else self.constalpha()
    contours =  ax.scatter(x[circle_mask], y[circle_mask], c=colors[circle_mask], s=area[circle_mask], alpha=a,cmap=cmap,vmin=1,vmax=vmax, marker='o')
    a = alpha[square_mask] if len(azimuth[square_mask]) else self.constalpha()
    contours =  ax.scatter(x[square_mask], y[square_mask], c=colors[square_mask], s=area[square_mask], alpha=a,cmap=cmap,vmin=1,vmax=vmax, marker='^')
  # contours = ax.scatter(x,y,c=colors,s=area,alpha=self.constalpha(),cmap=cmap,vmin=1,vmax=vmax)

    pad=10
    ax.set_xlim(-90-pad,90+pad)
    ax.set_ylim(-90-pad,90+pad)
    ax.set_aspect('equal')
    ax.axis('off')

    ax_polar = fig.add_axes(ax.get_position(), polar=True)
    ax_polar.set_facecolor('none') # make transparent
    ax_polar.set_theta_zero_location("E")
    ax_polar.set_xticks(np.arange(0,2*np.pi,np.pi/6))
    pad = 10
    ax_polar.set_yticks([x for x in range(10,100,10)])
    degree_symbol = chr(176)
    ax_polar.set_yticklabels([f'{int(x)}{degree_symbol}' for x in range(10,100,10)])
    ax_polar.set_xlim(0, 2*np.pi)
    ax_polar.set_ylim(0, 90+pad)
    ax_polar.set_rmin(0)
    ax_polar.set_title("Z : Vertical, X : East, Y : North")

    if debug:
      print("stereoplot")
    if True: #peaks
      import math
      legend_perc = [100.,90.,80.,70.,60.,50.,40.,30.,20.,10.]
      legend_perc = np.array(legend_perc)
      legend_area = 0.02*(legend_perc)**2
      legend_labels = [f"{r}%" for r in legend_perc]  # Labels for the legend
      legend_handles = [plt.scatter([], [], s=area, color='gray', alpha=self.constalpha()) for area in legend_area]
      legend_ax = fig.add_subplot(gs[1, 1])  # #peaks Legend axis in the gap
      legend_ax.axis("off")  # Hide the axis lines
      legend_ax.legend( legend_handles, legend_labels, title="Point\nRadii", frameon=False, fontsize=10,labelspacing=2)
    if debug:
      print("return plot")
    return plt
    #don't write file here since it is always a directory, with single .peaks.txt file

  def peaks_directory(self,io_database,io_indir,io_outdir,suptitle,debug=False):
    #sort files in order of creation so that the highest peaks are displayed as figure 1
    txt = FileSystem(io_database,io_indir,io_indir+".peaks.txt")
    logfile = []
    logfile.append("Reading: " + txt.fstr())
    plt = self.peaks_section(io_file=txt.fstr(),debug=debug)
    filename = None
    if plt is not None:
      plt.suptitle("Self Rotation Function Mercator Plot" + suptitle)
      filename = "self_rotation_function_peaks.png" #hardwired
      savefig = FileSystem(io_database,io_outdir,filename)
      plt.savefig(savefig.fstr())
     #logfile.append("Self Rotation Function Mercator Plot " +filename)
    return plt, savefig, logfile
#}}}

#these are internal parameters not interesting for the user
class runSRFP:

  def run_from_basetng_and_input(self,basetng):
    DAGDB = basetng.DAGDATABASE
    if (DAGDB.size() == 0): #not from command line
      raise ValueError(enum_err_code.fileset,"No database")
    #we have already shifted the PATHWAY so look at the previous node
    subdir = DAGDB.PATHWAY.PENULTIMATE.database_subdir()
    self.inner_srfp(basetng,io_subdir=subdir)

  def inner_srfp(self, basetng, io_subdir=None):
 #{{{
    DAGDB = basetng.DAGDATABASE
    DAGDB.shift_tracker(str(basetng.hashing()),"srfp")
    io_directory = os.path.join(basetng.DataBase(),io_subdir)
    if io_directory is None:
      basetng.logTab(enum_out_stream.logfile,"Directory not set")
      raise ValueError(enum_err_code.fileset,"self rotation function directory (command line)")
    basetng.logTab(enum_out_stream.summary,"Self Rotation Function Directory: " + io_directory)
    flat_database = False
    if not os.path.exists(io_directory):
      basetng.logTab(enum_out_stream.logfile,"Directory does not exist")
      flat_database = True
    if flat_database:
      flat_database = False
      for filename in os.listdir(basetng.DataBase()):
        if filename.startswith(io_subdir):
            flat_database = True
      if not flat_database:
        basetng.logTab(enum_out_stream.logfile,"Database and Files do not exist")
        return
    basetng.logBlank(enum_out_stream.logfile)
    debug = False

    subdir = DAGDB.PATHWAY.ULTIMATE.database_subdir()
    root = os.path.join(basetng.DataBase(),subdir)
    wf = basetng.Write()
    if wf and not FileSystem().flat_filesystem() and not os.path.exists(root):
        os.makedirs(root)

    wf = basetng.WriteFiles()
    if not wf: #stupid option for this mode, so just exit
      basetng.logTab(enum_out_stream.logfile,str("No files written: exit"))
      return

    psrf = plot_self_rotation_function()

    def get_title():
      from cctbx import sgtbx
      reflid = basetng.DAGDATABASE.NODES.LIST[0].REFLID.tag()
      space_group_info = sgtbx.space_group_info(symbol=DAGDB.NODES.LIST[0].HALL)
      sgname = space_group_info.group().type().lookup_symbol().replace(" ","")
      pgname = space_group_info.group().build_derived_point_group().type().lookup_symbol().replace(" ","")[1:]
      title = basetng.input.get_as_str(".phasertng.title.")
      suptitle = "\n" + title + "\nPoint Group " + pgname
      if len(reflid):
        suptitle = suptitle + "\nData " + reflid
      return suptitle

    if int(DAGDB.size()) > 0:
   #{
      basetng.logChevron(enum_out_stream.logfile,"Self Rotation Function Mercator Plot")
      title = basetng.input.get_as_str(".phasertng.title.")
      basetng.logEllipsisOpen(enum_out_stream.logfile,"Plotting")
      plt,filename,logfile = psrf.peaks_directory(
                        io_database=basetng.DataBase(),
                        io_indir=io_subdir,
                        io_outdir=subdir,
                        suptitle=get_title(),
                        debug=debug,
                        )
      basetng.logEllipsisShut(enum_out_stream.logfile)
      basetng.logTabArray(enum_out_stream.logfile,logfile)
      basetng.logBlank(enum_out_stream.logfile)
      if filename is not None:
        basetng.logFileWritten(enum_out_stream.logfile,wf,"Mercator Plot",filename)
      #would write the file here but already written, this is a hack
      basetng.logBlank(enum_out_stream.logfile)
   #}
    plot_chi_sections = True
    if plot_chi_sections and int(DAGDB.size()) > 0:
   #{
      basetng.logChevron(enum_out_stream.logfile,"Self Rotation Function Chi Sections")
      basetng.logEllipsisOpen(enum_out_stream.logfile,"Plotting")
      plt,filenames,logfile = psrf.chi_section_directory(
                        io_database=basetng.DataBase(),
                        io_indir=io_subdir,
                        io_outdir=subdir,
                        io_greyscale=False,
                        suptitle=get_title(),
                        debug=debug,
                        )
      basetng.logEllipsisShut(enum_out_stream.logfile)
      basetng.logTab(enum_out_stream.logfile,"Read " + str(len(logfile)) + " files")
      basetng.logTab(enum_out_stream.logfile,str(logfile[0]) + "...")
      basetng.logTab(enum_out_stream.logfile,str(logfile[-1]))
      basetng.logTabArray(enum_out_stream.verbose,logfile)
      if plt is None:
        basetng.logTab(enum_out_stream.logfile,"No valid files for plotting")
      basetng.logBlank(enum_out_stream.logfile)
      if len(filenames) > 1:
        basetng.logTab(enum_out_stream.logfile,"Write " + str(len(filenames)) + " Chi Sections Plots")
        basetng.logTab(enum_out_stream.logfile,filenames[0].qfstrq() + "...")
        basetng.logTab(enum_out_stream.logfile,filenames[-1].qfstrq())
      for f in filenames:
        basetng.logFileWritten(enum_out_stream.verbose,wf,"Chi Sections Plot",f)
      #would write the file here but already written, this is a hack
      basetng.logBlank(enum_out_stream.logfile)
   #}
 #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-o',"--output",help="database directory for output",required=True)
  parser.add_argument('-c',"--cards",help="cards file",type=argparse.FileType('r'),required=False)
  args = parser.parse_args()
  cards = None
  if args.cards is not None:
    cards = args.cards.read()
  cards = cards + "\nphasertng suite database = \"phasertng_srfp\""
  basetng = Phasertng("VOYAGER")
 #basetng.DAGDATABASE.parse_cards(cards)
  basetng.SetDataBase(args.output)
  basetng = runSRFP().inner_srfp(basetng,io_subdir="srfp")
# print(basetng.summary())
