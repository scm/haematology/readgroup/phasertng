//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/percent.h>
#include <sstream>
#include <iomanip>

namespace phasertng {
namespace type {

  percent::percent(
      const std::string parameter,
      const double dephalt_
  ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  { }

  void percent::set_value(double other)
  {
  //phaser_assert(other >= 0. or other <= 100.);
    user = true;
    uvalue = other;
  }

  void percent::set_value(const sv_string& vcards,const double other)
  {
    //check not a fraction between 0 and 1
    paranoia_check(vcards);
    set_value(other);
  }

  double
  percent::value_or_default_percent() const
  { return user ? uvalue : dephalt; }

  double
  percent::value_or_default_fraction() const
  { return user ? uvalue/100. : dephalt/100.; }

  //virtual
  bool
  percent::is_default() const
  { return uvalue == dephalt; }

  void
  percent::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  percent::to_python()
  { return unparse_parameter(false); }

  void
  percent::from_python(std::string str)
  { set_value(std::stod(str)); }

  double
  percent::value_or_default() const
  { return user ? uvalue : dephalt; }

  double
  percent::value() const
  {
   // if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  percent::unparse_parameter(bool phil) const
  {
    if (value_or_default() == DEF_FLT_FLAG)
      return None();
    std::stringstream ss;
    ss << uvalue;
    return ss.str();
  }

  std::string
  percent::show_default(bool phil) const
  {
    if (dephalt == DEF_FLT_FLAG)
      return None();
    std::stringstream ss;
    ss << dephalt;
    return ss.str();
  }

}}
