#ifndef PHASER_NCS_SUFFIX_H
#define PHASER_NCS_SUFFIX_H

#include "Edge.h"

namespace phaser {  namespace ncs {

    class Suffix
    {
        private:
            std::string _word;
            Node _origin;
            size_t _considered_length;
            const Node _empty_node;
            Node _entry_point;

        public:
            // Constructor and destructor
            Suffix( const std::string& word, const Node& origin);
            virtual ~Suffix();

            // Accessors
            const Node& get_origin() const;
            const Node& get_entry_point() const;
            const std::string& get_word() const;
            const std::string get_residual_suffix() const;

            // Logical functions
            bool is_explicit() const;
            bool has_next_char() const;
            bool entry_point_empty() const;

            // Suffix behaviour
            void extend( const size_t char_count = 1 );
            void decrement( const size_t char_count = 1 );
            void reduce();
            void prepare_new_entry();
            void shift_origin_to_suffix();
    };

}  }

#endif /*PHASER_NCS_SUFFIX_H*/
