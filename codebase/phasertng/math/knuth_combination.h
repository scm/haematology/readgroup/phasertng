#ifndef __phasertng_knuth_combinatation_function__
#define __phasertng_knuth_combinatation_function__

namespace phasertng {

//Knuth's combinatorial counting algorithm, avoids overflow
unsigned long long knuth_combination(unsigned long long n, unsigned long long k)
{
    if (k > n) {
        return 0;
    }
    unsigned long long r = 1;
    for (unsigned long long d = 1; d <= k; ++d) {
        r *= n--;
        r /= d;
    }
    return r;
}

}

#endif
