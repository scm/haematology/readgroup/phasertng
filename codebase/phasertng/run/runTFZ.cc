//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/data/Selected.h>
#include <phasertng/site/FastTranslationFunction.h>
#include <phasertng/math/rotation/rotdist.h>
#include <phasertng/site/PointsRandom.h>
#include <phasertng/math/mean_sigma_top.h>
#include <phasertng/pod/parent_mean_sigma_top.h>
#include <scitbx/math/r3_rotation.h>
#include <phasertng/dag/Helper.h>

namespace phasertng {

  //Translation_function_zscore_equivalent
  void Phasertng::runTFZ()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::SUMMARY,"No nodes");
      return;
    }
    if (!DAGDB.NODES.is_all_pose())
    {
      logTab(out::SUMMARY,"No poses");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!DAGDB.NODES.is_all_pose())
    throw Error(err::INPUT,"Dag not prepared with correct mode (pose failed?)");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_pose("curl");
    for (auto id : used_modlid)
      load_entry_from_database(where,entry::INTERPOLATION_MTZ,id,false);
    DAGDB.reset_entry_pointers(DataBase());
    }}
    DAGDB.apply_interpolation(interp::FOUR);

    //determine if this is the first tf or not by looking at the background number of poses
    int nmol = DAGDB.WORK->TNCS_MODELLED ? 1 : DAGDB.WORK->TNCS_ORDER;
    auto tf2 = DAGDB.NODES.number_of_poses() > nmol;
    auto tf1 = !tf2;

    unsigned random_seed = 0;

    //initialize tNCS parameters from reflections and calculate correction terms
    //FTF
    bool store_halfR(true);
    bool store_fullR(tf2);
    Epsilon epsilon;
    epsilon.setup_parameters(REFLECTIONS.get());
    epsilon.store_arrays(REFLECTIONS.get(),store_halfR,store_fullR);

    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.parse(
        input.value__boolean.at(".phasertng.outlier.reject.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.information.").value_or_default(),
        input.value__flt_number.at(".phasertng.outlier.probability.").value_or_default(),
        input.value__int_number.at(".phasertng.outlier.maximum_printed.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    //use the seek resolution for the TFZ because otherwise there will be noise
    auto io_hires = input.value__flt_number.at(".phasertng.resolution.").value_or_default();
    double resolution = io_hires ? io_hires: 0;
    seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        resolution);
    logTabArray(out::LOGFILE,seltxt);
    selected.flag_standard_outliers_and_select(REFLECTIONS.get());
    logTabArray(out::LOGFILE,selected.logSelected(""));
    if (Suite::Extra())
      logTabArray(out::TESTING,selected.logOutliers(""));
    }}

    if (tf1 and REFLECTIONS->SG.is_p1())
    {
      out::stream where = out::SUMMARY;
      logTab(where,"First Translation Function in P1");
      logTab(where,"TFZ unchanged");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }
    if (nmol > 1)
    {
      out::stream where = out::SUMMARY;
      logTab(where,"tNCS present");
      logTab(where,"TFZ unchanged");
      DAGDB.shift_tracker(hashing(),input.running_mode);
      return;
    }

    double io_tfz_min = input.value__flt_paired.at(".phasertng.zscore_equivalent.range_zscore.").value_or_default_min();
    //max limited on input to 99 for printing within %4.1f
    double io_tfz_max = input.value__flt_paired.at(".phasertng.zscore_equivalent.range_zscore.").value_or_default_max();
    double io_sel = input.value__percent.at(".phasertng.zscore_equivalent.range_percent.").value_or_default();
    double io_nprint = input.value__int_number.at(".phasertng.zscore_equivalent.maximum_printed.").value_or_default();

    bool none_packs(true);
    {
      int nrange(0);
      for (const auto& node: DAGDB.NODES)
      {
        if (node.ZSCORE >= io_tfz_min and node.ZSCORE <= io_tfz_max)
          nrange++;
        if (node.PACKS != 'N')
          none_packs = false;
      }
      out::stream where = out::SUMMARY;
      logTab(where,"TFZ after refinement is calculated for values within marginal zone");
      logTab(where,"Range: " + dtos(io_tfz_min,2) + "<TFZ<" + dtos(io_tfz_max,2));
      if (!nrange)
        logTab(where,"No TFZ within range of interest");
      else if (nrange == 1)
        logTab(where,"There was 1 TFZ within range of interest");
      else
        logTab(where,"There were " +itos(nrange) + " TFZ within range of interest");
    }
    bool all_zero = true;
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      if (DAGDB.WORK->ZSCORE) all_zero = false;
    }
    if (all_zero)
      logTab(out::LOGFILE,"No zscores, so rescore all");
    double io_fuse_tfz = 11.;
    //double io_fuse_tfz = input.value__flt_number.at(".phasertng.fuse_translation_function.fuse_tfz.").value_or_default();

    pod::parent_mean_sigma_top stats;
    {{
    int nrand  = input.value__int_number.at(".phasertng.zscore_equivalent.nrand.").value_or_default();
    logEllipsisOpen(out::LOGFILE,"Generating random points (" + itos(nrand) + ")");
    PointsRandom traList(nrand,random_seed++);
    logEllipsisShut(out::LOGFILE);

    double best_llg = std::numeric_limits<double>::lowest();
    double llg_over_fuse_tfz = std::numeric_limits<double>::max(); //llg in first search
    //if any llgs in other searches are over this then calculate the tfz because could go over fuse tfz
    dvect3 best_site;
    int n(1),N(DAGDB.size());
    bool first_node(true);
    bool first_hi(true),first_lo(true);
    double cutoff = (DAGDB.NODES.top_llg()-DAGDB.NODES.low_llg())*(io_sel/100.) + DAGDB.NODES.low_llg()-DEF_PPM;
    logTab(out::VERBOSE,"Range percent (below which TFZ not calculated) = " + dtos(io_sel,0) + "%");
    logTab(out::VERBOSE,"Cutoff LLG (below which TFZ not calculated) = " + dtos(cutoff,2));
    bool advisory(false);
    out::stream progress = out::LOGFILE;
    logProgressBarStart(progress,"Calculating tfz",DAGDB.size());
    DAGDB.restart();
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      //node->clear_additional();
      std::string nN = nNtos(n++,N);
      out::stream where = ((n-2) < io_nprint) ? out::VERBOSE : out::TESTING;
      node->generic_int = (nmol > 1) ? 2 : 0;//!x
      //reflections stored as pointer, so below changes FTF reflections object
      if ((DAGDB.NODES.top_tfz_packs() < io_tfz_max) and //packs for 2qb0, don't use the top one that packs
          ((node->LLG >= cutoff) and //only do the top ones for speed
           (node->PACKS != 'N') and //only do the packing or unknown ones, don't want bad refining even higher
           (node->ZSCORE >= io_tfz_min and node->ZSCORE <= io_tfz_max)) or
           (node->ZSCORE < io_fuse_tfz and node->LLG >= llg_over_fuse_tfz) or //llg means tfz may be > fuse
           all_zero or //start again
           (!none_packs and node->PACKS != 'N' and first_node) or
           (none_packs and first_node)) //do the top one regardless, for reference
      {
        logUnderLine(where,"Translation Function Z-score equivalent" + nN);
        logTab(where,"TFZ = " + dtos(node->ZSCORE,6,2));
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(node->SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        logTabArray(where,node->logPose("Known components"));
        logTab(where,"LLG = " + dtos(node->LLG,2));
        double original_llg = node->LLG; //need to calculate as it is the value for the TFZ==
        double recalc_llg;
        pod::FastPoseType tmp;
        std::tie(recalc_llg) = DAGDB.work_node_likelihood(
            REFLECTIONS.get(),
            selected.get_selected_array(),
            &epsilon,
            NTHREADS,
            USE_STRICTLY_NTHREADS,
            &tmp);
        node->LLG = original_llg; //numerical instablity means this may be different
        //sort order may change in cases where the signal is low if we use recalcated value
        node->generic_flag = true; //generic flag
        node->fraction_scattering(REFLECTIONS->get_ztotalscat()); //throws if >1
        auto original_pose = node->POSE;
        node->clear_last_tncs_group_pose();// have a copy of original
        DAGDB.reset_work_entry_pointers(DataBase());
        //now calculate the e and siga for the background pose, if any
        //original_llg has to be calculated for curl not for pose, since tNCS will change it
        //it can be the pose llg in the absence of tncs
        auto ecalcs_sigmaa_pose = DAGDB.work_ecalcs_sigmaa(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS);
        if (Suite::Extra())
        {
          logTab(out::TESTING,"Recalculated LLG = " + dtos(recalc_llg,2));
          DAGDB.work_node_likelihood(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose);
          logTabArray(out::TESTING,node->logPose("Background poses"));
          logTab(out::TESTING,"LLG reduced poses = " + dtos(node->LLG,2));
        }
        node->POSE = original_pose;
        DAGDB.reset_work_entry_pointers(DataBase());
        auto stored = DAGDB.fast_terms_for_tfz(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon
              );
        if (Suite::Extra())
        {
          DAGDB.work_node_likelihood_for_tfz(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose,
              &stored,
              dvect3(0,0,0));
          logTab(out::TESTING,"LLG check pose = " + dtos(node->LLG,2));
        }
        int l(1); int L(traList.count_sites());
        logProgressBarStart(where,"Scoring random positions",L,1,false); //1 is default
        traList.restart();
        sv_double v;
        while (!traList.at_end())
        {
          std::string lL = nNtos(l++,L);
          dvect3 fract = traList.next_site();
          DAGDB.work_node_likelihood_for_tfz(
              REFLECTIONS.get(),
              selected.get_selected_array(),
              &epsilon,
              NTHREADS,
              USE_STRICTLY_NTHREADS,
              &ecalcs_sigmaa_pose,
              &stored,
              fract);
          //all the same space group
          v.push_back(node->LLG);
          if (node->LLG > best_llg)
          {
            best_llg = node->LLG;
            best_site = fract;
            if (Suite::Extra())
              logProgressBarAgain(where,"Random Best llg = "+dtos(node->LLG,2)+" ("+dvtos(best_site,6,3)+")"+lL);
          }
          logProgressBarNext(where);
        }
        logProgressBarEnd(where);
        dvect3 mst = math::mean_sigma_top(v);
        logTab(out::VERBOSE,"Mean and Standard Deviation (brute search):");
        logTab(out::VERBOSE,"Random Mean TF (Sigma):  " + dtos(mst[0],10,2) + "   (" + dtos(mst[1],5,2) + ")");
        logTab(out::VERBOSE,"Random Highest TF (TFZ): " + dtos(mst[2],10,2) + "   (" + dtos((mst[2]-mst[0])/mst[1],5,2) + ")");
        logTab(out::VERBOSE,"Random Best Site = (" + dvtos(best_site,6,4) + ")");
        //add the growing list to the DAG, note that this will not be sorted overall
        //no selection for top peaks, recore them all
        node->generic_float = node->ZSCORE; //store the old zscore for printing
        node->LLG = original_llg;
        node->FSS = false;
        double zscore = (original_llg-mst[0])/mst[1];
        node->generic_flag = true; //store whether tfz== calculated in this flag
        if (first_node and nmol == 1) //llg not tNCS affected
          llg_over_fuse_tfz = mst[0]+io_fuse_tfz*mst[1];
        if (nmol > 1)
        {
          logTab(where,"Record only");
          node->generic_str = dtos(zscore,8,2); //but record the new zscore anyway
          node->generic_int = 2; //!x
        }
        else if (zscore > node->generic_float)
        {
          logTab(where,"TFZ equivalent better than TFZ from search");
          node->ZSCORE = zscore;
          node->generic_str = dtos(zscore,8,2); //but record the new zscore anyway
        }
        else
        {
          logTab(where,"TFZ equivalent lower than TFZ from search");
          advisory = true;
          node->generic_str = dtos(zscore,8,2); //but record the new zscore anyway
          node->generic_int = 1; //!x
        }
        node->ANNOTATION += " TFZ==" + dtos(std::floor(zscore),0) + "z";
        logTab(where,"TFZ0  : " + dtos(node->generic_float,7,2));
        logTab(where,"TFZ== : " + dtos(zscore,7,2));
        node->NEXT.TRANSLATED = false;
        node->POSE = original_pose; //revert
        node->POSE.back().TFZ = zscore;
        DAGDB.reset_work_entry_pointers(DataBase());
        stats.mst[n-2] = mst; //map
        first_node = false;
      }
      else
      {
        node->generic_flag = false; //store whether tfz== calculated in this flag
        node->generic_float = node->ZSCORE; //store the old zscore for printing
        node->ANNOTATION += " TFZ==>"+dtos(std::floor(node->ZSCORE),0)+"z";
        //above not necessary, generic_float not used
        if (!(DAGDB.NODES.top_tfz() < io_tfz_max))
        {
          if (first_hi)
          {
            logUnderLine(where,"Translation Function Z-score equivalent" + nN);
            logTab(where,"TFZ = " + dtos(node->ZSCORE,6,2));
            logTab(where,"Top TFZ above range for TFZ equivalent calculation; skip");
            first_hi = false;
          }
          node->generic_str = "--:>" + dtoss(io_tfz_max,1) + "z";
        }
        else if (node->LLG < cutoff)//only do the top ones for speed
        {
          if (first_lo)
          {
            logUnderLine(where,"Translation Function Z-score equivalent" + nN);
            logTab(where,"TFZ = " + dtos(node->ZSCORE,6,2));
            logTab(where,"LLG below range for TFZ equivalent calculation; skip");
            first_lo = false;
          }
          node->generic_str = "--:<"+dtoss(io_sel,1)+"%";
        }
        else if (!all_zero and node->ZSCORE < io_tfz_min)
        {
          logUnderLine(where,"Translation Function Z-score equivalent" + nN);
          logTab(where,"TFZ = " + dtos(node->ZSCORE,6,2));
          logTab(where,"TFZ below range for TFZ equivalent calculation");
          node->generic_str = "--:<" + dtoss(io_tfz_min,1) + "z";
        }
        else if (all_zero)
        {
          logUnderLine(where,"Translation Function Z-score equivalent" + nN);
          logTab(where,"TFZ not set ");
          node->generic_str = "--:n/a";
        }
        else if (node->ZSCORE > io_tfz_max)
        {
          logUnderLine(where,"Translation Function Z-score equivalent" + nN);
          logTab(where,"TFZ = " + dtos(node->ZSCORE,6,2));
          logTab(where,"TFZ above range for TFZ equivalent calculation");
          node->generic_str = "--:>" + dtoss(io_tfz_max,1) + "z";
        }
      }
      if (!logProgressBarNext(progress)) break;
    } //dagdb
    logProgressBarEnd(progress);
    DAGDB.restart();
    if (advisory)
      logAdvisory("At least one TFZ equivalent below TFZ from search");
    if (DAGDB.NODES.is_all_tncs_modelled())
      logAdvisory("TFZ equivalent unreliable due to tNCS");
    }}

    {{
    if (Suite::Extra())
    {
      out::stream where = out::TESTING;
      int n(1),N(DAGDB.size());
      phaser_assert(N);
      DAGDB.restart();
      while (!DAGDB.at_end())
      {
        std::string nN = nNtos(n++,N);
        logUnderLine(where,"Translation Function Z-score #" +nN);
        logTabArray(where,DAGDB.WORK->logNode(nN+""));
      }
    }
    }}

    bool hasvalue; double toppacks;
    double topllg = DAGDB.NODES.top_llg();
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Packing Information");
    std::tie(hasvalue,toppacks) = DAGDB.NODES.top_llg_packs();
    if (!hasvalue) logTab(where,"No nodes pack");
    else if (DAGDB.NODES[0].PACKS == 'N') logTab(where,"Top node does not pack");
    else if (DAGDB.NODES[0].PACKS == 'Y') logTab(where,"Top node packs");
    else if (DAGDB.NODES[0].PACKS == '?') logTab(where,"Top node packing status unknown");
    }}
    std::string toppacksstr = hasvalue ? dtos(toppacks,2) : "---";

    double lowest_mean = stats.merged_lowest_mean();
    {{
    int w = itow(io_nprint);
    out::stream where = out::SUMMARY;
    logTableTop(where,"Zscore Equivalent");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()-nmol));
    DAGDB.restart(); //very important to call this to set WORK
    logTab(where,"Last placed: " + DAGDB.WORK->POSE.back().IDENTIFIER.str());
    logTab(where,"Statistics for last placed pose shown");
    logTab(where,"Top/Top-packs/mean for percent: " + dtos(topllg,2) + "/" + toppacksstr + "/" + dtos(lowest_mean));
    int z = DAGDB.size();
    logTab(where,("There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s")));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,"#       Rank of the peak");
      logTab(where,"<       TFZ equivalent lower than TFZ in search - ignored");
      logTab(where,"u       TFZ equivalent unreliable with tNCS present - ignored");
      logTab(where,"--------");
      logTab(where,snprintftos(
          "%-*s %10s %5s %7s %8s%c%c %10s/%-10s %7s %7s %3s",
          w,"#","LLG","Packs","TFZ0","TFZ==",'<','u',"TFZ-Mean","Sigma","%-Top","%-Packs","tag"));
    }
    int i(1);
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      dag::Node* node = DAGDB.WORK;
      std::string range = "---";
      double tscore = 100.*(node->LLG-lowest_mean)/(topllg-lowest_mean);
      std::string pscore = hasvalue ? dtos(100.*(node->LLG-lowest_mean)/(toppacks-lowest_mean),5,1) : "---";
      logTab(where,snprintftos(
          "%-*i % 10.*f %2s%c%2s %7.2f %8s%c%c % 10s/%-10s %7s %7s %s",
          w,i,
          q,node->LLG,
          "",node->PACKS,"",
          node->generic_float,
          (node->generic_int == 2) ? "---" :node->generic_str.c_str(), //brackets on condition essential
          (node->generic_int == 1) ? '<':' ',
          (node->generic_int == 2) ? 'u':' ',
          node->generic_flag ? dtos(stats.mst[i-1][0],q).c_str() : "---",
          node->generic_flag ? dtos(stats.mst[i-1][1],q).c_str() : "---",
          (node->generic_int == 2) ? "---" : dtos(tscore,5,1).c_str(),
          (node->generic_int == 2) ? "---" : pscore.c_str(),
          node->POSE.back().IDENTIFIER.tag().c_str()
          ));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}

    if (!hasvalue) //there is no top packing so don't do anything, all are rescue peaks
    {
      out::stream where = out::LOGFILE;
      logTab(where,"No purge by percent");
    }
    else if (DAGDB.size() > 1 and DAGDB.NODES.is_all_tncs_modelled())
    {
      out::stream where = out::LOGFILE;
      double io_percent = input.value__percent.at(".phasertng.zscore_equivalent.percent.").value_or_default();
      dag::NodeList& output_dag = DAGDB.NODES;
      logEllipsisOpen(where,"Purge by percent");
      int before = output_dag.size();
      if (toppacks > lowest_mean)
        output_dag.apply_partial_sort_and_percent_llg(io_percent,toppacks,lowest_mean);
      logEllipsisShut(where);
      if (toppacks > lowest_mean)
        logTab(where,"Percent cutoff: "  + dtos(io_percent,5,1) + "%");
      else
        logTab(where,"No percent cutoff");
      double cut = (io_percent/100.)*(toppacks-lowest_mean)+lowest_mean;
      logTab(where,"Top-Packs/Mean/Cutoff LLG: " + dtos(toppacks,2) + "/" + dtos(lowest_mean,2) + "/" + dtos(cut,2));
      logTab(where,"Number before purge: " + itos(before));
      logTab(where,"Number after purge:  " + itos(output_dag.size()));
      DAGDB.reset_entry_pointers(DataBase());
    }
    else if (DAGDB.size() > 1 and !DAGDB.NODES.is_all_tncs_modelled())
    {
      out::stream where = out::LOGFILE;
      logTab(where,"No purge by percent");
    }

    {{
    out::stream where = out::SUMMARY;
    int io_nprint(10);
    logTableTop(where,"Translation Function Zscore Summary");
    logTab(where,"Background Poses: " + itos(DAGDB.NODES.number_of_poses()-nmol));
    int z = DAGDB.size();
    logTab(where,"There " + std::string(z==1?"is ":"are ") + itos(z) + " peak" + std::string(z==1?"":"s"));
    logTab(where,"--------");
    int q = DAGDB.NODES.llg_precision(REFLECTIONS->MAP);
    if (DAGDB.size())
    {
      logTab(where,snprintftos(
          "%2s %10s %7s %7s %s  %s  %7s %6s=%-5s %5s %*s",
          "#",
          "LLG","TFZ0","TFZ",
          dag::header_polar().c_str(),
          dag::header_fract().c_str(),
          "Bfactor","DRMS","VRMS","CELL",
          uuid_w,"modlid"));
    }
    logTab(where,"--------");
    int i(1);
    for (auto& node : DAGDB.NODES)
    {
      dag::Pose& pose = node.POSE.back();
      dmat33 fracmat = node.CELL.fractionalization_matrix();
      logTab(where,snprintftos(
          "%2d %10s %7s %7s %s  %s  %+7.1f %+6.2f=%05.3f %5.3f %*s",
          i,
          dtos(node.LLG,q).c_str(),
          dtos(node.generic_float,2).c_str(),
          dtos(node.ZSCORE,2).c_str(),
          dag::print_polar(pose.SHIFT_MATRIX).c_str(),
          dag::print_fract(fracmat*pose.SHIFT_ORTHT).c_str(),
          pose.BFAC,
          node.DRMS_SHIFT[pose.IDENTIFIER].VAL,
          node.VRMS_VALUE[pose.IDENTIFIER].VAL,
          node.CELL_SCALE[pose.IDENTIFIER].VAL,
          uuid_w,pose.IDENTIFIER.string().c_str()));
      if (i == io_nprint)
      {
        int more = DAGDB.size()-io_nprint;
        if (DAGDB.size() > io_nprint)
          logTab(where,"--- etc " + itos(more) + " more" );
        break;
      }
      i++;
    }
    logTableEnd(where);
    }}
    DAGDB.shift_tracker(hashing(),input.running_mode);
  }

}//phasertng
