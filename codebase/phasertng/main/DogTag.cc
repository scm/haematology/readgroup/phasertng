//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/DogTag.h>

namespace phasertng {

  void
  DogTag::initialize(std::pair<std::pair<uuid_t,std::string>,std::pair<uuid_t,std::string>> dogtag)
  {
    PATHWAY.initialize_from_ullint(dogtag.first.first);
    PATHWAY.initialize_tag(dogtag.first.second);
    IDENTIFIER.initialize_from_ullint(dogtag.second.first);
    IDENTIFIER.initialize_tag(dogtag.second.second);
  }

  void
  DogTag::initialize(std::vector<uuid_t> ids,std::vector<std::string> tags)
  {
    PATHWAY.initialize_from_ullint(ids[0]);
    PATHWAY.initialize_tag(tags[0]);
    IDENTIFIER.initialize_from_ullint(ids[1]);
    IDENTIFIER.initialize_tag(tags[1]);
  }

  const std::string DogTag::tag_unparse() const
  {
    if (is_set_valid()) return PATHWAY.tag() + " " + identify().tag();
    return "None"; //which should be the default tag anyway
  }

  const std::string DogTag::id_unparse() const
  {
    if (is_set_valid()) return PATHWAY.string() + " " + IDENTIFIER.string();
    return "None"; //which should be the default tag anyway
  }

  const bool DogTag::is_set_valid() const
  {
    return (PATHWAY.is_set_valid() and IDENTIFIER.is_set_valid());
  }

  std::string
  DogTag::entry_data_stem_ext(entry::data text)
  { return IDENTIFIER.entry_data_stem_ext(text); }

  std::string
  DogTag::other_data_stem_ext(other::data text)
  { return IDENTIFIER.other_data_stem_ext(text); }

  std::string
  DogTag::database_subdir()
  { return PATHWAY.database_subdir(); }

  std::string
  DogTag::extra_file_stem_ext(std::string ext)
  { return IDENTIFIER.extra_file_stem_ext(ext); }

  std::pair<std::string,std::string>
  DogTag::FileName(std::string ext)
  { return { PATHWAY.database_subdir(), IDENTIFIER.extra_file_stem_ext(ext) }; }

  std::pair<std::string,std::string>
  DogTag::FileEntry(entry::data dat)
  { return { PATHWAY.database_subdir() , IDENTIFIER.entry_data_stem_ext(dat) }; }

  std::pair<std::string,std::string>
  DogTag::FileOther(other::data dat)
  { return { PATHWAY.database_subdir() , IDENTIFIER.other_data_stem_ext(dat) }; }

} //phasertng
