#include <phasertng/data/Cluster.h>
#include <phasertng/main/Error.h>
#include <scitbx/constants.h>
#include <phasertng/io/PdbRecord.h>
#include <phasertng/table/vanderwaals_radii.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <fstream>
#include <cctype> //Windows toupper

namespace phasertng {

  double Cluster::debye(double ssqr)
  {
    //This returns the sinx/x factor from Debye's formula
    //This is used to correct f' and f" at the resolution of r
    //For f' and f" this is a crude approximation that assumes that all the atoms
    //anomalously scatter the same so there is only one fi or fpp for the lot.
    //Makes it the same as a heavy atom.

    if (is_not_cluster) return 1;
    double sqrtssqr = sqrt(ssqr);
    double debye(0);
    for (unsigned a = 0; a < structure.size(); a++)
    {
      double sqrtssqr_twopi_ra(sqrtssqr*structure[a].twopi_ra);
      debye += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? 1 : //limit for approx from Mathematica
                sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
    }
    return debye;
  }

  double Cluster::scattering(double ssqr)
  {
    //Calculates the spherically averaged scattering from a cluster
    //using Debye's formula

    if (is_not_cluster) return 0;
    double sqrtssqr = sqrt(ssqr);
    double scatter(0);
    for (auto item : structure)
    {
      double fo = ff[item.element].at_d_star_sq(ssqr);
      double sqrtssqr_twopi_ra(sqrtssqr*item.twopi_ra);
      scatter += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? fo :  //limit for approx from Mathematica
                  fo * sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
    }
    return scatter;
  }

  std::map<std::string,int>
  Cluster::composition() const
  {
    std::map<std::string,int> comp;
    for (auto item : structure)
      comp[item.element] = 0;
    for (auto item : structure)
      comp[item.element]++;
    return comp;
  }

  void
  Cluster::set_twopi_ra_and_ff()
  {
    dvect3 av(0,0,0);
    for (int i = 0; i < structure.size(); i++)
      for (int j = 0; j < 3; j++)
        av[j] += structure[i].site[j];
    for (int j = 0; j < 3; j++)
      av[j] /= structure.size();
    for (int i = 0; i < structure.size(); i++)
      for (int j = 0; j < 3; j++)
        structure[i].site[j] -= av[j];
    ff.clear();
    for (auto& item : structure)
    {
      double ra = std::sqrt(item.site*item.site);
      item.twopi_ra = scitbx::constants::two_pi*ra;
      ff[item.element] = cctbx::eltbx::xray_scattering::wk1995(item.element).fetch();
    }
  }

  std::vector<std::tuple<std::string,std::string,double> >
  Cluster::twopi_ra_rb() const
  {
    std::vector<std::tuple<std::string,std::string,double> > twopi_rab;
    for (int a = 0; a < structure.size(); a++)
    for (int b = 0; b < structure.size(); b++)
    {
      auto ab = structure[a].site-structure[b].site;
      double rab = std::sqrt(ab*ab);
      twopi_rab.push_back(std::make_tuple(
         structure[a].element,structure[b].element,scitbx::constants::two_pi*rab));
    }
    return twopi_rab;
  }

  double
  Cluster::vanderwaals() const
  {
    dvect3 av(0,0,0);
    auto structure_cp = structure;
    for (int i = 0; i < structure.size(); i++)
      for (int j = 0; j < 3; j++)
        av[j] += structure[i].site[j];
    for (int j = 0; j < 3; j++)
      av[j] /= structure.size();
    for (int i = 0; i < structure.size(); i++)
      for (int j = 0; j < 3; j++)
        structure_cp[i].site[j] -= av[j];
    table::vanderwaals_radii radii;
    double maxra(0);
    for (int i = 0; i < structure_cp.size(); i++)
    {
      double ra = std::sqrt(structure_cp[i].site*structure_cp[i].site);
      maxra = radii.get(structure_cp[i].element) + ra;
    }
    return maxra;
  }

  void
  Cluster::ReadPdb()
  {
    std::ifstream infile;
    infile.open(FileSystem::fstr());
    try
    {
      while (!infile.eof())
      {
        std::string buffer;
        std::getline(infile, buffer);
        if (!buffer.find("ATOM") || !buffer.find("HETATM"))
        {
          PdbRecord next_atom;
          next_atom.ReadAtomCard(buffer);
          if (next_atom.O < 0.0 and next_atom.AtomName != "UNK ")
            throw Error(err::INPUT,"Cluster compound atom has negative occupancy");
          if (next_atom.at_xplor_infinity() and next_atom.O != 0)
            throw Error(err::INPUT,"Cluster compound atom has coordinates at \"infinity\"");
          if (!next_atom.valid_element and !next_atom.valid_cluster)
            throw Error(err::INPUT,"Cluster compound atom scatterer not recognised");
          std::string s = next_atom.get_element();
          std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
          structure.push_back({next_atom.get_element(),next_atom.X});
        }
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory if there is
    //something weird/missing at the end of a file
    }
    if (!structure.size())
    throw Error(err::INPUT,"Cluster compound has no atoms");
    set_twopi_ra_and_ff();
  }

  void
  Cluster::setup_ta6br12()
{
/*
COMPND TBR HEXATANTALUM DODECABROMIDE
REMARK TBR Part of HIC-Up: http://xray.bmc.uu.se/hicup
REMARK TBR Extracted from PDB file pdb2bvl.ent
REMARK TBR Formula BR12 TA6
REMARK TBR Nr of non-hydrogen atoms  18
REMARK TBR Eigen-values covariance X/Y/Z       63.6      62.0      60.6
REMARK TBR Residue type TBR
REMARK TBR Residue name   7131
REMARK TBR Original residue name (for O) $A1548
REMARK TBR  RESOLUTION. 2.20 ANGSTROMS.
REMARK TBR occurs in        3 other PDB entries
REMARK TBR Also in 2.0-2.5A  :  1DD4
REMARK TBR Resolution (A)    :  2.40
REMARK TBR Also in 2.5-3.0A  :  1HKX 1Z7L
REMARK TBR Resolution (A)    :  2.65 2.80
REMARK TBR
*/

  structure = {
                 { "TA", { 7.285, -7.899, 32.642} },
                 { "TA", { 5.977,-10.513, 32.571} },
                 { "TA", { 8.878,-10.352, 32.967} },
                 { "TA", { 7.122,-11.158, 35.180} },
                 { "TA", { 8.432, -8.546, 35.250} },
                 { "TA", { 5.530, -8.705, 34.853} },
                 { "BR", { 5.804, -8.747, 30.668} },
                 { "BR", { 9.338, -8.507, 31.160} },
                 { "BR", { 8.827, -6.310, 33.981} },
                 { "BR", { 5.265, -6.496, 33.515} },
                 { "BR", { 7.709,-11.761, 31.102} },
                 { "BR", {10.758, -9.286, 34.415} },
                 { "BR", { 8.604,-10.311, 37.152} },
                 { "BR", { 5.070,-10.550, 36.660} },
                 { "BR", { 5.581,-12.749, 33.837} },
                 { "BR", { 9.143,-12.562, 34.306} },
                 { "BR", { 6.697, -7.297, 36.720} },
                 { "BR", { 3.650, -9.771, 33.405} }};

}

void
Cluster::setup_disulphide()
{
  structure = {
                 { "S", { 0,0,  1.025} },
                 { "S", { 0,0, -1.025} }}; //The disulfide bond is about 2.05 Å in length
}

} //phasertng
