//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved

    //build name required for sequence
    models.REFLID = REFLECTIONS->reflid();
    double min_hires_for_wilson(4);  //data must extend to at least this resolution

    models.RELATIVE_WILSON_B.resize(models.PDB.size());
    std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
    bool damped(false);
    {{
    math::Matrix<double> SigmaP = fcalcs.sigmap_with_binning();
    out::stream where = out::SUMMARY;
    logUnderLine(where,"Wilson Scaling");
    logTab(where,"Data Wilson B-factor: " + dtos(REFLECTIONS->WILSON_B_F)); //local copy for output
    logTab(where,"High Resolution = " + dtos(REFLECTIONS->data_hires()));
    bool data_to_hires_for_wilson(REFLECTIONS->data_hires() < min_hires_for_wilson);
    if (!data_to_hires_for_wilson)
      logTab(where,"Data does not extend beyond " + dtos(min_hires_for_wilson,3) + " Angstroms");
    //for lookup into the bin array, for plotting
    if (wilson_bin.size() < min_bins_for_wilson)
      logTab(where,"Not enough data for Wilson scaling");
    if (REFLECTIONS->MAP)
    {
      logTab(where,"Map: Wilson B-factors set to 0");
      logTab(where,"Wilson B-factors set to 0");
      models.RELATIVE_WILSON_B = sv_double(models.PDB.size(),0);
      std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
    }
    else if (input.value__boolean.at(".phasertng.molecular_transform.skip_bfactor_analysis.").value_or_default())
    {
      logTab(where,"Wilson Scaling not performed");
      logTab(where,"Wilson B-factors set to 0");
      models.RELATIVE_WILSON_B = sv_double(models.PDB.size(),0);
      std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
    }
    else if (data_to_hires_for_wilson and (wilson_bin.size() >= min_bins_for_wilson))
    {
      models.RELATIVE_WILSON_B = sv_double(models.PDB.size());
      std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
      //this is flagged in the pdb file
      //no scalar multiplication defined
      //Must be oversampled or scaling is totally unreliable!!! u_base screws it up
      //these should be similar to the reflection bins values, not ensemble bins
      logTab(out::VERBOSE,"Number of calculated structure factors = " + itos(fcalcs.MILLER.size()));
      logBlank(out::VERBOSE);
      af_double SigmaN; af_int numinbin_data;
      std::tie(SigmaN,numinbin_data) = REFLECTIONS->sigman_with_binning(fcalcs.BIN);
      //scale the SigmaP and SigmaN so they are on an absolute scale
      double KI = REFLECTIONS->SG.SYMFAC*fn::pow2(REFLECTIONS->WILSON_K_F);
   // KI *= REFLECTIONS->get_ztotalscat();
      sv_int models_scattering = models.scattering();
      for (int s = 0; s < fcalcs.BIN.numbins(); s++)
      {
        SigmaN[s] /= KI;
        for (int m = 0; m < models.PDB.size(); m++)
        { //scale this up as though it was fp=1
          double JI = models_scattering[m];
          SigmaP(m,s) = SigmaP(m,s)/JI;
        }
      }
      //arrays below will only contain the values for the linear regression
      af_double ssqr_bin;
      for (auto s : wilson_bin)
      {
        ssqr_bin.push_back(1.0/fn::pow2(fcalcs.BIN.MidRes(s)));
      }
      for (int m = 0; m < models.PDB.size(); m++)
      { //the B-factor is refined wrt the B-factor of the search
        af_double ratio; //between 5 and config_hires
        for (auto s : wilson_bin)
        {
          ratio.push_back(SigmaN[s]/SigmaP(m,s));
        }
        double WilsonB_intensity = -4*wilson_slope_and_intercept(ratio,ssqr_bin).first;
        models.RELATIVE_WILSON_B[m] = WilsonB_intensity/2.0; //local copy for output
        logTab(out::SUMMARY,"Relative Wilson B-factor " + dtos(models.RELATIVE_WILSON_B[m]));

        Loggraph loggraph;
        loggraph.title = "Wilson Plot for Ensemble " + search->identify().str() + " #" + itos(m+1) + " B=" + dtos(models.RELATIVE_WILSON_B[m],7,2);
        loggraph.scatter = false;
        loggraph.data_labels = "1/d^2 d #refl-data #refl-model Mean-Icalc Mean-Iobs Ratio";
        for (int ibin = 0; ibin < wilson_bin.size(); ibin++) //ratio length less than bin limit
        {
          int s = wilson_bin[ibin]; //lookup into bin array
          double midres = 1.0/std::sqrt(ssqr_bin[ibin]);
          loggraph.data_text +=
              dtos(1.0/fn::pow2(midres),5,4) + " " +
              dtos(midres,5,4) + " " +
              itos(numinbin_data[s],15,true,false) + " " +
              itos(fcalcs.BIN.NUMINBIN[s],15,true,false) + " " +
              etos(SigmaP(m,s),15,8) + " " +
              etos(SigmaN[s],15,8)+ " " +
              etos(ratio[ibin],15,8) + " " +
              "\n";
        }
        loggraph.graph.resize(4);
        loggraph.graph[0] = "SigmaN vs Resolution:AUTO:1,6";
        loggraph.graph[1] = "SigmaP vs Resolution:AUTO:1,5";
        loggraph.graph[2] = "Ratio SigmaP/SigmaN vs Resolution:AUTO:1,7";
        loggraph.graph[3] = "Number Reflections vs Resolution:AUTO:1,3,4";
        logGraph(out::LOGFILE,loggraph);
      }
    }
    else
    {
      logTab(where,"Wilson Scaling not performed");
      logTab(where,"Wilson B-factors set to 0");
      (!data_to_hires_for_wilson) ?
        logWarning("Wilson scaling not performed, resolution too low"):
        logWarning("Wilson scaling not performed, insufficient reflections");
      models.RELATIVE_WILSON_B = sv_double(models.PDB.size(),0);
      std::fill(models.RELATIVE_WILSON_B.begin(),models.RELATIVE_WILSON_B.end(),0);
    }
    damped = (models.bfactors_damped_after_relative_wilson_b());
    if (damped)
    {
      logWarning("After Wilson scaling model had sharpened Bfactors which will be damped on output");
    }
    }}

    //switch the results in memory too
    search->MODELS = search->IMODELS;
    search->FCALCS = search->IFCALCS;
