//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Error.h>
#include <phasertng/minimizer/RefineSSA.h>
#include <future>
#include <phasertng/data/Reflections.h>
#include <phasertng/data/reflection/ReflRow.h>
#include <phasertng/data/Selected.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/dag/Node.h>
#include <phasertng/dag/Entry.h>

//#define PHASERTNG_SCA_DEBUG

namespace phasertng {

  RefineSSA::RefineSSA(
      const_ReflectionsPtr REFLECTIONS_,
      SelectedPtr SELECTED_,
      ScatterersPtr SCATTERERS_,
      double NASCAT_,
      double DBSCAT_)
     // DagDatabasePtr DAGDB_)
    : RefineBase(),
      REFLECTIONS(std::move(REFLECTIONS_)),
      SELECTED(std::move(SELECTED_)),
      SCATTERERS(std::move(SCATTERERS_))
      //DAGDB(std::move(DAGDB_))
  {
    NBINS = REFLECTIONS->numbins();
    ANOMTYPE = SCATTERERS->strongest_anomalous_scatterer().first;
    PHASER_ASSERT(ANOMTYPE != "None");
    assert(REFLECTIONS->has_col(labin::FEFFPOS));
    assert(REFLECTIONS->has_col(labin::FEFFNEG));
    assert(REFLECTIONS->has_col(labin::DOBSPOS));
    assert(REFLECTIONS->has_col(labin::DOBSNEG));
    assert(REFLECTIONS->has_col(labin::CENT));
    assert(REFLECTIONS->has_col(labin::BOTH));
    assert(REFLECTIONS->has_col(labin::PLUS));
    assert(REFLECTIONS->has_col(labin::RESN));
    nascat = NASCAT_; // Defaults to one, but review in init_node
    dBscat = DBSCAT_;
    dLL_by_dabsrhoFF.resize(REFLECTIONS->NREFL);
    d2LL_by_dabsrhoFF2.resize(REFLECTIONS->NREFL);
    dLL_by_drhopm.resize(REFLECTIONS->NREFL);
    d2LL_by_drhopm2.resize(REFLECTIONS->NREFL);
    d2LL_by_dabsrhoFF_by_drhopm.resize(REFLECTIONS->NREFL);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      dLL_by_dabsrhoFF[r] = 0.;
      d2LL_by_dabsrhoFF2[r] = 0.;
      dLL_by_drhopm[r] = 0.;
      d2LL_by_drhopm2[r] = 0.;
      d2LL_by_dabsrhoFF_by_drhopm[r] = 0.;
    }
    sca_null.resize(REFLECTIONS->NREFL,0);

  }

  void
  RefineSSA::set_threading(bool USE_STRICTLY_NTHREADS_,int NTHREADS_)
  {
    NTHREADS = NTHREADS_;
    USE_STRICTLY_NTHREADS = USE_STRICTLY_NTHREADS_;
  }

  void
  RefineSSA::init_node(dag::Node* work)
  {
    NODE = work;
    if (ANOMTYPE == "AX") // Special case
    {
      nascat_set = false;
      nascat = 1.;
    }
    else
    {
      for (auto comp : NODE->COMPOSITION)
      {
        double ntype = comp.second*REFLECTIONS->Z;
        if (comp.first == ANOMTYPE)
        {
          if (ntype == 0) // Composition of element is zero if no prior knowledge
          {
            nascat_set = false;
            nascat = 1.; // Starting point for very weakly restrained determination
          }
          else
          {
            nascat_set = true;
            nascat = ntype;
          }
        }
      }
    }
    NODE->SADSIGMAA.setup_midssqr(REFLECTIONS->bin_midssqr(0));
    ascat_bin.resize(NBINS);
#ifdef PHASERTNG_SCA_DEBUG
    std::cout << "Initialisation anomtype,nascat : " << ANOMTYPE << " " << nascat << std::endl;
#endif
    for (int b = 0; b < NBINS; b++)
    {
#ifdef PHASERTNG_SCA_DEBUG
    std::cout << "Initialisation fdp: " << (SCATTERERS->fdp(ANOMTYPE,NODE->SADSIGMAA.MIDSSQR[b])) << std::endl;
#endif
      double symBfac(REFLECTIONS->SG.SYMFAC*std::exp(-2.*REFLECTIONS->WILSON_B_F*NODE->SADSIGMAA.MIDSSQR[b]/4.));
    // Start total covariances with contribution from major anomalous scatterer
      cmplex facmplx(0,0);
      facmplx += SCATTERERS->fo(ANOMTYPE,NODE->SADSIGMAA.MIDSSQR[b]);
      facmplx += SCATTERERS->fp_fdp(ANOMTYPE,NODE->SADSIGMAA.MIDSSQR[b]).as_complex_double();
      facmplx *= std::sqrt(symBfac);
#ifdef PHASERTNG_SCA_DEBUG
          std::cout << "fa cmplex " << ctos(facmplx) << std::endl;
#endif
      ascat_bin[b] = facmplx; // scattering factor corrected for symm ops and Wilson B
      NODE->SADSIGMAA.SigmaQ_bin[b] = nascat * std::norm(facmplx);
      NODE->SADSIGMAA.sigmaQQ_bin[b] = nascat * fn::pow2(facmplx);
      for (auto comp : NODE->COMPOSITION)
      {
        std::string atomtype = comp.first;
#ifdef PHASERTNG_SCA_DEBUG
          std::cout << "Comp " << atomtype << " type=" << ANOMTYPE << std::endl;
#endif
        if (atomtype != ANOMTYPE) // Only background scatterers here
        {
          double ntype = comp.second*REFLECTIONS->Z;
          cmplex fcmplx(0,0);
          fcmplx += SCATTERERS->fo(atomtype,NODE->SADSIGMAA.MIDSSQR[b]);
          if (ANOMTYPE != "AX") // AX type should account for all anomalous
            fcmplx += SCATTERERS->fp_fdp(atomtype,NODE->SADSIGMAA.MIDSSQR[b]).as_complex_double();
#ifdef PHASERTNG_SCA_DEBUG
          std::cout << "Anom " << atomtype << " " << ntype << " Z=" << REFLECTIONS->Z << " " <<  SCATTERERS->fo(atomtype,NODE->SADSIGMAA.MIDSSQR[b]) << " + " << ctos(SCATTERERS->fp_fdp(atomtype,NODE->SADSIGMAA.MIDSSQR[b]).as_complex_double()) << std::endl;
#endif
          fcmplx *= std::sqrt(symBfac);
          cmplex fsqr = fn::pow2(fcmplx);
          NODE->SADSIGMAA.sigmaPP_bin[b] += ntype * fsqr;
          NODE->SADSIGMAA.SigmaP_bin[b]  += ntype * std::norm(fcmplx);
        }
      }
      cmplex sigmaFF = NODE->SADSIGMAA.sigmaPP_bin[b] + NODE->SADSIGMAA.sigmaQQ_bin[b];
      double SigmaNcalc = NODE->SADSIGMAA.SigmaP_bin[b] + NODE->SADSIGMAA.SigmaQ_bin[b];
      cmplex rhoPP(NODE->SADSIGMAA.sigmaPP_bin[b]/NODE->SADSIGMAA.SigmaP_bin[b]);
      cmplex rhoFF(sigmaFF/SigmaNcalc);
      NODE->SADSIGMAA.absrhoFF_bin[b] = std::abs(rhoFF);
      NODE->SADSIGMAA.rhopm_bin[b] = NODE->SADSIGMAA.minimum_rhopm();
      NODE->SADSIGMAA.SigmaN_bin[b] = SigmaNcalc;
#ifdef PHASERTNG_SCA_DEBUG
      std::cout << "Initial s,ISAD.MIDSSQR[b],SigmaNcalc,sigmaFF,SigmaP,sigmaPP,SigmaQ,sigmaQQ,absrhoFF_bin: " << b << " " << NODE->SADSIGMAA.MIDSSQR[b] << " " << SigmaNcalc << " " << ctos(sigmaFF) << " " << NODE->SADSIGMAA.SigmaP_bin[b] << " " << ctos(NODE->SADSIGMAA.sigmaPP_bin[b]) << " " << NODE->SADSIGMAA.SigmaQ_bin[b] << " " << ctos(NODE->SADSIGMAA.sigmaQQ_bin[b]) << " " << NODE->SADSIGMAA.absrhoFF_bin[b] << std::endl;
#endif
      PHASER_ASSERT(NODE->SADSIGMAA.absrhoFF_bin[b] < 1.);
    }
    //now apply the bounds so in limits for start of refinement
    for (int b = 0; b < NBINS; b++)
    {
      {
        // Min anom signal (max absrhoFF) when no anom scatterers added
        double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
        cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
        double rmax = std::abs(sigmaPP) / SigmaP;
        double eps = std::numeric_limits<double>::epsilon();
        rmax = std::min(1. - eps, rmax); // Keep less than 1 even for AX case
        // Max anom signal in terms of anomalous scattering power that minimises absrhoFF
        cmplex fA = ascat_bin[b];
        double namax = 0.9999*nAnomMax(SigmaP,sigmaPP,fA); // Avoid numerical problems near strict bounds
        double rmin = absrhoFFfromNanom(SigmaP,sigmaPP,fA,namax);
        NODE->SADSIGMAA.absrhoFF_bin[b] = std::min(NODE->SADSIGMAA.absrhoFF_bin[b],rmax);
        NODE->SADSIGMAA.absrhoFF_bin[b] = std::max(NODE->SADSIGMAA.absrhoFF_bin[b],rmin);
      }
    }
  }

  void
  RefineSSA::set_macrocycle_protocol(const sv_string const_protocol)
  {
    std::set<std::string> protocol;
    for (auto& item : const_protocol)
      protocol.insert(stolower(item));
    REFINE_RHOFF = (protocol.find("rhoff") != protocol.end());
    REFINE_RHOPM = (protocol.find("rhopm") != protocol.end());
    if (protocol.find("off") != protocol.end()) //overwrite
      REFINE_RHOFF = REFINE_RHOPM = false;

    if (NO_SIGNAL or NOT_CONSISTENT)
      REFINE_RHOFF = REFINE_RHOPM = false;

    refine_parameter_.clear();
    PHASER_ASSERT(refine_parameter_.size() == 0);
    for (int b = 0; b < NBINS; b++)
      REFINE_RHOFF ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    for (int b = 0; b < NBINS; b++)
      REFINE_RHOPM ? refine_parameter_.push_back(true) : refine_parameter_.push_back(false);
    //this is where total_number_of_parameters_ and nmp are DEFINED
    nmp = 0;
    for (int i = 0; i < refine_parameter_.size(); i++)
      if (refine_parameter_[i]) nmp++;
    total_number_of_parameters_ = refine_parameter_.size();
  }

  sv_string
  RefineSSA::macrocycle_parameter_names()
  {
    sv_string names(nmp);
    int i(0),m(0);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) names[i++] = "Complex Correlation Coefficient Bin#" + std::to_string(b+1);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) names[i++] = "Correlated errors scale Bin#" + std::to_string(b+1);
    assert(m == total_number_of_parameters_);
    assert(i == nmp);
    return names;
  }

  sv_double
  RefineSSA::get_macrocycle_parameters()
  {
    int m(0),i(0);
    sv_double pars(nmp);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) pars[i++] = NODE->SADSIGMAA.absrhoFF_bin[b]; // absrhoFF_bin
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) pars[i++] = NODE->SADSIGMAA.rhopm_bin[b]; // rhopm_bin
    assert(m == total_number_of_parameters_);
    assert(i == nmp);
    return pars;
  }

  void
  RefineSSA::set_macrocycle_parameters(sv_double newx)
  {
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) NODE->SADSIGMAA.absrhoFF_bin[b] = newx[i++]; // absrhoFF_bin
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) NODE->SADSIGMAA.rhopm_bin[b] = newx[i++]; // rhopm_bin
    assert(i == nmp);
    assert(m == total_number_of_parameters_);
  }

  sv_double
  RefineSSA::macrocycle_large_shifts()
  {
    sv_double largeShifts(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++])
        largeShifts[i++] = (1.-NODE->SADSIGMAA.absrhoFF_bin[b])/4; // absrhoFF_bin
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) largeShifts[i++] = 0.05; // rhopm_bin
    assert(m == total_number_of_parameters_);
    assert(i == nmp);
    return largeShifts;
  }

  std::vector<dtmin::Bounds>
  RefineSSA::bounds()
  {
    std::vector<dtmin::Bounds> bounds(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
    {
      if (refine_parameter_[m++])
      {
        // Min anom signal (max absrhoFF) when no anom scatterers added
        double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
        cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
        double rmax = std::abs(sigmaPP) / SigmaP;
        double eps = std::numeric_limits<double>::epsilon();
        rmax = std::min(1. - eps, rmax); // Keep less than 1 even for AX case
        // Max anom signal in terms of anomalous scattering power that minimises absrhoFF
        cmplex fA = ascat_bin[b];
        double namax = 0.9999*nAnomMax(SigmaP,sigmaPP,fA); // Avoid numerical problems near strict bounds
        double rmin = absrhoFFfromNanom(SigmaP,sigmaPP,fA,namax);
        bounds[i++].on(rmin, rmax); // absrhoFF_bin
      }
    }
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) bounds[i++].on(NODE->SADSIGMAA.minimum_rhopm(), 0.9);  // rhopm_bin
    assert(m == total_number_of_parameters_);
    assert(i == nmp);
    return bounds;
  }

  std::vector<dtmin::Reparams>
  RefineSSA::reparameterize()
  {
    std::vector<dtmin::Reparams> repar(nmp);
    int m(0),i(0);
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) repar[i++].off(); // absrhoFF_bin
    for (int b = 0; b < NBINS; b++)
      if (refine_parameter_[m++]) repar[i++].off(); // rhopm_bin
    assert(m == total_number_of_parameters_);
    assert(i == nmp);
    return repar;
  }

  TargetType
  RefineSSA::likelihood()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,false);
    //store the llg without restraints?
    double Target = REFLECTIONS->oversampling_correction()*(Tuple.Target);
    NODE->LLG = -Target; //we don't want the -llg, which is only for refinement
    return Target;
  }

  TargetType
  RefineSSA::target()
  {
    TargetGradientHessianType Tuple = combined_tgh(false,false,true);
    return REFLECTIONS->oversampling_correction()*(Tuple.Target);
  }

  TargetGradientType
  RefineSSA::target_gradient()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,false,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  TargetGradientHessianType
  RefineSSA::target_gradient_hessian()
  {
    TargetGradientHessianType Tuple = combined_tgh(true,true,true);
    return REFLECTIONS->oversampling_correction()*Tuple;
  }

  af_string
  RefineSSA::initial_statistics()
  {
    return logSigmaA("Initial");
  }

  af_string
  RefineSSA::current_statistics()
  {
    return logSigmaA("Current");
  }

  af_string
  RefineSSA::final_statistics()
  {
    return logSigmaA("Final");
  }

  af_string
  RefineSSA::reject_outliers()
  {
    //("Reject Outliers");
    sv_double epsnSigmaN; //size=0 means default internal initialization
    SELECTED->flag_outliers_and_select(REFLECTIONS,
      { rejected::RESO,
        rejected::MISSING,
        rejected::SYSABS,
        rejected::WILSONNAT,
        rejected::WILSONPOS,
        rejected::WILSONNEG,
        rejected::LOINFONAT,
        rejected::NOANOM,
        rejected::BADANOM,
      },
      epsnSigmaN,
      NODE->SADSIGMAA
      );
    //(SELECTED->logOutliers("Substructure Content Analysis Refinement"));
    //(SELECTED->logSelected("Substructure Content Analysis Refinement"));
    return af_string();
  }

  double
  RefineSSA::absrhoFFfromNanom(double SigmaP, cmplex sigmaPP, cmplex fA, double nanom)
  {
    return std::abs(sigmaPP + nanom*fn::pow2(fA))/(SigmaP + nanom*std::norm(fA));
  }

  double
  RefineSSA::nAnomMax(double SigmaP, cmplex sigmaPP, cmplex fA)
  {
    double fa2PP = std::real(fn::pow2(fA)*std::conj(sigmaPP));
    return (SigmaP*fa2PP - std::norm(fA*sigmaPP)) /
      (std::norm(fA)*(fa2PP - SigmaP*std::norm(fA)));
  }

  std::pair <double,double>
  RefineSSA::nAnomFromAbsrhoFF(double absrhoFF, double SigmaP, cmplex sigmaPP, cmplex fanom)
  {
    // Report two solutions for equivalent number of anomalous scatterers
    double nascat_low,nascat_high;
    if (absrhoFF < 1.)
    {
      double reff = std::real(fanom);
      double imff = std::imag(fanom);
      double absffsqr = std::norm(fanom);
      double absrhoFFsqr = fn::pow2(absrhoFF);
      double resigmaPP = std::real(sigmaPP);
      double imsigmaPP = std::imag(sigmaPP);
      double aterm = (1.-absrhoFFsqr)*fn::pow2(absffsqr)/2.;
      double bterm = 2.*reff*imff*imsigmaPP +
        fn::pow2(reff)*(resigmaPP - absrhoFFsqr*SigmaP) -
        fn::pow2(imff)*(resigmaPP + absrhoFFsqr*SigmaP);
      double cterm = (fn::pow2(resigmaPP) + fn::pow2(imsigmaPP) -
        fn::pow2(absrhoFF*SigmaP)) / 2;
      double sqrtarg = fn::pow2(bterm) - 4.*aterm*cterm;
      // Report both of two solutions for scattering from anomalous scatterers
      nascat_low = (sqrtarg > 0) ? (-bterm-std::sqrt(sqrtarg))/(2.*aterm) : 0;
      nascat_high = (sqrtarg > 0) ? (-bterm+std::sqrt(sqrtarg))/(2.*aterm) : 0;
    }
    else // Special case where Friedel's law obeyed: no anomalous or all
    {
      nascat_low = 0;
      nascat_high = std::numeric_limits<double>::max();
    }
    return std::pair<double,double>(nascat_low,nascat_high);
  }

  af_string
  RefineSSA::cleanup_parameters()
  {
    af_string txt;
    if (REFINE_RHOFF)
    {
      txt.push_back("Fit anomalous scatterer content to refined absrhoFF curve");
      txt.push_back("xA represents B-weighted sum of occupancies squared");
      txt.push_back("Z(power):   significance of deviation of |rhoFF| from zero anomalous scatterer content");
      txt.push_back(snprintftos(
          "  %3s   %7s %9s     %2s     %8s",
          "Bin","MidSsqr","|rhoFF|","xA","Z(power)"));
      double sumw(0.),sumwxA(0.);
      sv_double rhoFFmin_bin,rhoFFmax_bin;
      sv_double SPdat,rhoFFdat,sigrhoFFdat,SSQRdat;
      sv_cmplex sPPdat,fAdat;
      af_int numinbin = REFLECTIONS->numinbin();
      af_double LORES = REFLECTIONS->bin_lores(0);
      af_double HIRES = REFLECTIONS->bin_hires(0);
      rmszrhoFF = (0.); //important init, +=
      double lastz3(LORES[0]);
      int nweakref(0);
      HessianType hessian_current;
      // Get current Hessian with no restraint terms and (if present) rhopm off-diagonal element
      double save_sigma_atoms = SIGMA_ATOMS;
      SIGMA_ATOMS = 0.;
      auto tgh = combined_tgh(true,true,true);
      hessian_current = tgh.Hessian;
      PHASER_ASSERT(hessian_current.size());
      SIGMA_ATOMS = save_sigma_atoms;
      auto bounds_for_rhoFF = bounds();
      for (int b = 0; b < NBINS; b++)
      {
        // Default to sigma from BFGS-update inverse Hessian
        double hinvdiag = previous_h_inverse_approximation_(b,b);
        if (hinvdiag <= 0.) hinvdiag = 1./hessian_current(b,b); // Inverse approximation may have been filtered
        if (hinvdiag <= 0.)
        {
          af_string txt;
          txt.push_back("Fit anomalous scatterer content to refined absrhoFF curve failed");
          return txt;
        }
        double sigmarhoFF = std::sqrt(hinvdiag);
        if (REFINE_RHOPM)
        { // If rhopm also refined, prefer sigma from analytical Hessian with no restraints
          double newsigma(0);
          double hrhoFF = hessian_current(b,b);
          double hrhopm = hessian_current(b+NBINS,b+NBINS);
          if (hrhoFF > 0. and hrhopm > 0.)
          {
            double hoffdiag = hessian_current(b,b+NBINS);
            double hesscorrsqr = fn::pow2(hoffdiag)/(hrhoFF*hrhopm); // For sigma correction
            if (hesscorrsqr < 1.)
            {
              newsigma = std::sqrt(1./(hrhoFF*(1.-hesscorrsqr)));
              sigmarhoFF = newsigma;
            }
          }
        }
        if (sigmarhoFF <= 0.) sigmarhoFF = 0.0001; // Paranoia fallback
        double ssqr = NODE->SADSIGMAA.MIDSSQR[b];
        double absrhoFF = NODE->SADSIGMAA.absrhoFF_bin[b];
        double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
        cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
        cmplex fanom = ascat_bin[b];
        double namax = nAnomMax(SigmaP,sigmaPP,fanom);
        double rhoFFmin = absrhoFFfromNanom(SigmaP,sigmaPP,fanom,namax);
        double rhoFFmax = absrhoFFfromNanom(SigmaP,sigmaPP,fanom,0.);
        // Keep rhoFFmin and rhoFFmax within refinement limits
        // Note that absrhoFF parameters are first in list of refined parameters
        rhoFFmin = std::max(rhoFFmin, bounds_for_rhoFF[b].lower_limit());
        rhoFFmax = std::min(rhoFFmax, bounds_for_rhoFF[b].upper_limit());
        rhoFFmin_bin.push_back(rhoFFmin);
        rhoFFmax_bin.push_back(rhoFFmax);
        double zrhoFFpower(0.);
        zrhoFFpower = (rhoFFmax-absrhoFF)/sigmarhoFF;
        if (zrhoFFpower < 2)
          nweakref += numinbin[b];
        else if (zrhoFFpower > 3)
          lastz3 = HIRES[b];
        rmszrhoFF += fn::pow2(zrhoFFpower) * numinbin[b];
        PHASER_ASSERT((absrhoFF >= rhoFFmin) && (absrhoFF <= rhoFFmax));

        // Store relevant data for bins, get initial rough estimate of nascat
        fAdat.push_back(fanom);
        SPdat.push_back(SigmaP);
        sPPdat.push_back(sigmaPP);
        rhoFFdat.push_back(absrhoFF);
        sigrhoFFdat.push_back(sigmarhoFF);
        SSQRdat.push_back(ssqr);
        // Approximate esd of xA from how it changes by perturbing absrhoFF
        double safety = (rhoFFmax - rhoFFmin) / 10000.;
        double rfminus = std::max(absrhoFF - sigmarhoFF, rhoFFmin + safety);
        double rfplus  = std::min(absrhoFF + sigmarhoFF, rhoFFmax - safety);
        double delta = rfplus - rfminus;
        double nmax = nAnomFromAbsrhoFF(rfminus,SigmaP,sigmaPP,fanom).first;
        double nmin = nAnomFromAbsrhoFF(rfplus,SigmaP,sigmaPP,fanom).first;
        double sigmanum = (nmax - nmin) * (sigmarhoFF/delta);
        // Downweight higher-resolution data for initial nascat estimate
        double weight = 1./(ssqr*fn::pow2(sigmanum));
        double xA = nAnomFromAbsrhoFF(absrhoFF,SigmaP,sigmaPP,fanom).first;
        txt.push_back(snprintftos(
            "  %3d   %7.5f  %8.6f  %7.4f   %7.3f",
            b+1,NODE->SADSIGMAA.MIDSSQR[b],absrhoFF,xA,zrhoFFpower));
        sumw += weight;
        sumwxA += weight*xA;
      }
      PHASER_ASSERT(sumw > 0.);
      nascat = sumwxA / sumw;
      double namin_global = 0.0001;
      txt.push_back("");
      fraction_weak = (double(nweakref) / REFLECTIONS->NREFL);
      txt.push_back( "Fraction of weak anomalous data with Z(power) < 2: " + dtos(fraction_weak, 6, 4));
      rmszrhoFF = std::sqrt(rmszrhoFF / REFLECTIONS->NREFL);
      txt.push_back( "RMS value of Z(power): " + dtos(rmszrhoFF, 7, 2));
      high_resolution = lastz3;
      txt.push_back( "Highest resolution with Z(power) > 3: " + dtos(high_resolution, 7, 2));
      txt.push_back("");
      if (nascat < namin_global or high_resolution == LORES[0])
      {
        txt.push_back( "Almost no anomalous signal detected");
        NO_SIGNAL = true;
        return txt;
      }

      // Grid search for best nascat,dBscat to fit data
      int ngridpts(200);
      double namin(nascat/3.), namax(nascat*3.);
      double dna = (namax - namin)/(ngridpts-1);
      double bmin(-50.),bmax(50.);
      double dB = (bmax - bmin)/(ngridpts-1);
      double GMkappa = 0.1; // Limited downweighting of outliers seems to be sufficient
      double sigmaB = 5.; // Weak restraint to limit B-factors
      if (FIRST_CALL and !nascat_set)
        sigmaB /= 2.; // Tighter restraint on initial macrocycle with no prior
      int nused = SPdat.size();
      int ntry(0);
      while ((dna > 0.005 or dB > 0.005) and ntry < 10) // Progressively finer grid until fine enough
      {
        int ibest(-1),jbest(-1);
        double bestscore(1.e30);
        for (int i = 0; i < ngridpts; i++)
        {
          double natest = namin + i*dna;
          for (int j = 0; j < ngridpts; j++)
          {
            double btest = bmin + j*dB;
            double GMscore = fn::pow2(btest/sigmaB)/2.; // Start with B-factor restraint
            for (int bin = 0; bin < nused; bin++)
            {
              // Note that xA measures scattering power, so B-factor term is squared
              double xA = natest*std::exp(-btest*SSQRdat[bin]/2.);
              double wtdsqr =
                fn::pow2((rhoFFdat[bin] - absrhoFFfromNanom(SPdat[bin],sPPdat[bin],fAdat[bin],xA)) /
                sigrhoFFdat[bin]);
              GMscore += (wtdsqr/2.)/(1 + GMkappa*wtdsqr);
            }
            if (GMscore < bestscore)
            {
              ibest = i;
              jbest = j;
              bestscore = GMscore;
              nascat = natest;
              dBscat = btest;
            }
          }
        }
        if (ibest > 0 and ibest < ngridpts-1 and jbest > 0 and jbest < ngridpts-1)
        {
          // Finer grid only if best value inside search area, otherwise step outside
          dna /= 2;
          dB /= 2;
        }
        ngridpts = 7;
        namin = std::max(nascat - 3*dna, namin_global);
        bmin = dBscat - 3*dB;
        ntry++;
      }
      txt.push_back("Curve fit: nascat = " + dtos(nascat,9,4) + ", dBscat = " + dtos(dBscat,9,4));
      txt.push_back("");

      double nequivmin(nascat);
      double nequivmax(nascat);
      if (SIGMA_ATOMS) // zero for unrestrained
      {
        nequivmin = std::max(nascat/(3*SIGMA_ATOMS),namin_global);
        nequivmax = nascat*(3*SIGMA_ATOMS);
      }
      txt.push_back("Resolve violations of constraints and restraints on |rhoFF|");
      txt.push_back(snprintftos(
          "%3s %8s %10s    %10s %10s   %10s %10s",
          "Bin","MidRes","Refined","Calculated","Minimum","Maximum","Reset"));
      for (int b = 0; b < NBINS; b++)
      {
        double absrhoFF = NODE->SADSIGMAA.absrhoFF_bin[b];
        double midssqr = NODE->SADSIGMAA.MIDSSQR[b];
        double bterm = std::exp(-dBscat*midssqr/4.); // Squared as part of fanom
        double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
        cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
        cmplex fanom = bterm*ascat_bin[b];
        double nequiv = nAnomFromAbsrhoFF(absrhoFF,SigmaP,sigmaPP,fanom).first;
        nequiv = std::max(nequiv, namin_global);
        double nrestraint(nequiv);
        if (SIGMA_ATOMS) nrestraint = std::min(nequivmax,std::max(nequivmin,nequiv));
        double rhoFFcalc = absrhoFFfromNanom(SigmaP,sigmaPP,fanom,nascat);
        // Move rhoFF closer to restraints for second macrocycle if necessary:
        //   allow equivalent number of scatterers to differ by up to 3 times SIGMA_ATOMS ratio
        double rhoFFmin = rhoFFmin_bin[b];
        double rhoFFmax = rhoFFmax_bin[b];
        if ((nequiv != nrestraint) and FIRST_CALL)
        {
          double newrhoFF = rhoFFcalc;
          if (nequiv != nrestraint) // Not strictly an outlier but too far off expected
            newrhoFF = absrhoFFfromNanom(SigmaP,sigmaPP,fanom,nrestraint);
          if (newrhoFF > rhoFFmax) newrhoFF = 0.999*rhoFFmax + 0.001*rhoFFmin; // close to max
          txt.push_back(snprintftos(
            "%3d %7.2f    %8.6f     %8.6f    %8.6f     %8.6f     %8.6f",
            b+1,1./std::sqrt(midssqr),NODE->SADSIGMAA.absrhoFF_bin[b],rhoFFcalc,rhoFFmin,rhoFFmax,newrhoFF));
          NODE->SADSIGMAA.absrhoFF_bin[b] = newrhoFF;
        }
        else
          txt.push_back(snprintftos(
            "%3d %7.2f    %8.6f     %8.6f    %8.6f     %8.6f",
            b+1,1./std::sqrt(midssqr),NODE->SADSIGMAA.absrhoFF_bin[b],rhoFFcalc,rhoFFmin,rhoFFmax));
      }
      txt.push_back("");
    }
    for (int b = 0; b < NBINS; b++)
    {
      // Make inferred anomalous scattering content per bin consistent with absrhoFF
      double absrhoFF = NODE->SADSIGMAA.absrhoFF_bin[b];
      double midssqr = NODE->SADSIGMAA.MIDSSQR[b];
      double bterm = std::exp(-dBscat*midssqr/4.);
      cmplex fanom = bterm*ascat_bin[b];
      double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
      cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
      double nAnom = nAnomFromAbsrhoFF(absrhoFF,SigmaP,sigmaPP,fanom).first;
      NODE->SADSIGMAA.SigmaQ_bin[b] = nAnom*std::norm(fanom);
      NODE->SADSIGMAA.sigmaQQ_bin[b] = nAnom*fn::pow2(fanom);
      NODE->SADSIGMAA.SigmaN_bin[b] = NODE->SADSIGMAA.SigmaP_bin[b] + NODE->SADSIGMAA.SigmaQ_bin[b];
      //SigmaH and sigmaHH should be zero before there is an anomalous scatterer model
      NODE->SADSIGMAA.SigmaH_bin[b] = 0.;
      NODE->SADSIGMAA.sigmaHH_bin[b] = cmplex(0.,0.);
    }
    FIRST_CALL = false; // Macrocycles after first use specified anomalous scatterer
    return txt;
  }

  af_string
  RefineSSA::finalize_parameters()
  {
    sv_double epsnSigmaN; //size=0 means default internal initialization
    SELECTED->flag_outliers_and_select(REFLECTIONS,
      { rejected::RESO,
        rejected::MISSING,
        rejected::SYSABS,
        rejected::WILSONNAT,
        rejected::WILSONPOS,
        rejected::WILSONNEG,
        rejected::LOINFONAT,
        rejected::NOANOM,
        rejected::BADANOM,
      },
      epsnSigmaN,
      NODE->SADSIGMAA
    );
    return af_string();
  }

  af_string
  RefineSSA::logSigmaA( std::string description)
  {
    af_string txt;
    txt.push_back("Scattering Content Parameters: " + description);
    txt.push_back(snprintftos(
        "%3s %12s %12s %12s",
        "Bin","Resolution","|rhoFF|","|rhopm|"));
    af_double LORES = REFLECTIONS->bin_lores(0);
    af_double HIRES = REFLECTIONS->bin_hires(0);
    for (int b = 0; b < NBINS; b++)
    {
      txt.push_back(itos(b+1,3,false,false) + " " + dtos(LORES[b],6,2) + "-" + dtos(HIRES[b],5,2) + " " + dtos(NODE->SADSIGMAA.absrhoFF_bin[b],12,6) + " " + dtos(NODE->SADSIGMAA.rhopm_bin[b],12,6));
    }
    txt.push_back("");
    txt.push_back( "Fraction of weak anomalous data with Z(power) < 2: " + dtos(fraction_weak, 6, 4));
    txt.push_back( "RMS value of Z(power): " + dtos(rmszrhoFF, 7, 2));
    txt.push_back( "Highest resolution with Z(power) > 3: " + dtos(high_resolution, 7, 2));
    return txt;
  }

  TargetGradientHessianType
  RefineSSA::parallel_combined_tgh(
      bool do_gradient,
      bool do_hessian,
      int beg,
      int end
    )
  {
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,false);

    likelihood::isad::function isad_function(&NODE->SADSIGMAA);
    likelihood::isad::tgh_flags flags;
    flags.gradient(do_gradient and (REFINE_RHOFF or REFINE_RHOPM));
    flags.hessian(do_hessian and (REFINE_RHOFF or REFINE_RHOPM));
    for (int r = beg; r < end; r++)
    {
      if (SELECTED->get_selected(r))
      {
        const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
        const int s = work_row->get_bin();
        likelihood::isad::tgh rtgh;
        const bool both = work_row->get_both();
        const bool cent = work_row->get_cent();
        const bool plus = work_row->get_plus();
        if (both)
        {
          //const double eps = work_row->get_eps();
          const double dobspos = work_row->get_dobspos();
          const double dobsneg = work_row->get_dobsneg();
          const double feffpos = work_row->get_feffpos();
          const double feffneg = work_row->get_feffneg();
          const double resn = work_row->get_resn();
          const double teps = work_row->get_teps();
          rtgh = isad_function.tgh_null(
             s, dobspos, feffpos, dobsneg, feffneg, resn, teps, flags);
          tgh.Target += rtgh.LogLike;
          sca_null[r] = rtgh.LogLike;
          if (do_gradient and REFINE_RHOFF)
          {
            dLL_by_dabsrhoFF[r] = rtgh.dLL_by_dabsrhoFF;
            if (do_hessian)
              d2LL_by_dabsrhoFF2[r] = rtgh.d2LL_by_dabsrhoFF2;
          }
          if (do_gradient and REFINE_RHOPM)
          {
            dLL_by_drhopm[r] = rtgh.dLL_by_drhopm;
            if (do_hessian)
            {
              d2LL_by_drhopm2[r] = rtgh.d2LL_by_drhopm2;
              if (REFINE_RHOFF)
              {
                d2LL_by_dabsrhoFF_by_drhopm[r] = rtgh.d2LL_by_dabsrhoFF_by_drhopm;
              }
            }
          }
        }
        else if (cent)
        {
          const double feffpos = work_row->get_feffpos();
          const double resn = work_row->get_resn();
          const double feff = feffpos;
          const double esn = fn::pow2(resn);
          double LogLike = (fn::pow2(feff) + esn*std::log(scitbx::constants::pi*esn/2.)) / (2.*esn);
          tgh.Target += LogLike;
          sca_null[r] = LogLike;
        }
        else
        {
          const double feff = plus ? work_row->get_feffpos() : work_row->get_feffneg();
          if (feff > 0.)
          {
            const double resn = work_row->get_resn();
            const double  esn = fn::pow2(resn);
            double LogLike = fn::pow2(feff)/esn + std::log(esn/(2.*feff));
            tgh.Target += LogLike;
            sca_null[r] = LogLike;
          }
        }
      }
    }

    return tgh;
  }

  TargetGradientHessianType
  RefineSSA::combined_tgh(
      bool do_gradient,
      bool do_hessian,
      bool do_restraint
    )
  {
    //initialization
    if (do_hessian) do_gradient = true;
    TargetGradientHessianType tgh(nmp,false);
    for (int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      dLL_by_dabsrhoFF[r] = 0.;
      d2LL_by_dabsrhoFF2[r] = 0.;
      dLL_by_drhopm[r] = 0.;
      d2LL_by_drhopm2[r] = 0.;
      d2LL_by_dabsrhoFF_by_drhopm[r] = 0.;
    }

    //calculate target, gradients and hessians without restraints

    // NREFL is expected to always be >> NTHREADS in any sensible case unless you have some seriously crazy hardware,
    // at which point it's probably best to use less threads because of the overhead incurred in
    // creating and destroying them

    if (NTHREADS == 1)
    {
      int beg(0);
      int end(REFLECTIONS->NREFL);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
    }
    else
    {
      std::vector< std::future< TargetGradientHessianType > > results;
      std::vector< std::packaged_task< TargetGradientHessianType(
              bool,
              bool,
              int,
              int)> > packaged_tasks;
      std::vector< std::thread > threads;
      int nthreads_1 = NTHREADS-1;
      results.reserve(nthreads_1);
      if (USE_STRICTLY_NTHREADS) // we don't actually use packaged_tasks or threads if we aren't using USE_STRICTLY_NTHREADS so there's no need to reserve memory for them.
      {
        packaged_tasks.reserve(nthreads_1);
        threads.reserve(nthreads_1);
      }

      // launch threads for all but the first batch of reflections
      for (int t = 0; t < nthreads_1; t++)
      {
        int beg = REFLECTIONS->head_thread(t,NTHREADS);
        int end = REFLECTIONS->tail_thread(t,NTHREADS);
        if (USE_STRICTLY_NTHREADS)
        {
          packaged_tasks.emplace_back(std::bind(&RefineSSA::parallel_combined_tgh, this,
              std::placeholders::_1,
              std::placeholders::_2,
              std::placeholders::_3,
              std::placeholders::_4));
          results.push_back(packaged_tasks[t].get_future());
          // create the thread (and set it off running)
          // note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          threads.emplace_back(std::move(packaged_tasks[t]),
              do_gradient,
              do_hessian,
              beg,
              end);
        }
        else
        {
          // again, note the ternary operator is to allow for the inclusion of any remainder left over when dividing up the reflections
          results.push_back(std::async(std::launch::async, &RefineSSA::parallel_combined_tgh, this,
              do_gradient,
              do_hessian,
              beg,
              end));
        }
      }
      {{
      // now that we've created (and set off) the other threads, do the last set of reflections on the main thread
      int beg = REFLECTIONS->head_thread(nthreads_1,NTHREADS);
      int end = REFLECTIONS->tail_thread(nthreads_1,NTHREADS);
      tgh = parallel_combined_tgh(
              do_gradient,
              do_hessian,
              beg,
              end);
      }}
      // aggregate the results of the other threads
      for (int t = 0; t < nthreads_1; t++)
      {
        auto result = results[t].get();
        if (USE_STRICTLY_NTHREADS) threads[t].join(); // this line is not very well explained in tutorials, but probably necessary to terminate the threads
        tgh.Target += result.Target;
        for (int i=0; i<nmp; i++)
        {
          tgh.Gradient[i] += result.Gradient[i];
          for (int j=0; j<nmp; j++)
            tgh.Hessian(i,j) += result.Hessian(i,j);
        }
      }
    }

    sv_double dLL_by_dabsrhoFF_bin(NBINS, 0.);
    sv_double d2LL_by_dabsrhoFF2_bin(NBINS, 0.);
    sv_double dLL_by_drhopm_bin(NBINS, 0.);
    sv_double d2LL_by_drhopm2_bin(NBINS, 0.);
    sv_double d2LL_by_dabsrhoFF_by_drhopm_bin(NBINS, 0.);
    for(int r = 0; r < REFLECTIONS->NREFL; r++)
    {
      const reflection::ReflRow* work_row = REFLECTIONS->get_row(r);
      const int s = work_row->get_bin();
      dLL_by_dabsrhoFF_bin[s] += dLL_by_dabsrhoFF[r];
      d2LL_by_dabsrhoFF2_bin[s] += d2LL_by_dabsrhoFF2[r];
      dLL_by_drhopm_bin[s] += dLL_by_drhopm[r];
      d2LL_by_drhopm2_bin[s] += d2LL_by_drhopm2[r];
      d2LL_by_dabsrhoFF_by_drhopm_bin[s] += d2LL_by_dabsrhoFF_by_drhopm[r];
    }
    sv_double dR_by_dabsrhoFF_bin(NBINS, 0.);
    sv_double d2R_by_dabsrhoFF2_bin(NBINS, 0.);
    sv_double dR_by_drhopm_bin(NBINS, 0.);
    HessianType d2R_by_drhopm2_bin;
    d2R_by_drhopm2_bin.resize(af::flex_grid<>(NBINS, NBINS), 0.); // resize and zero
    d2R_by_drhopm2_bin.fill(0); // resize and zero

    //add restraint terms
    if (do_restraint)
    {
      if (SIGMA_ATOMS)
      {
        for (int b = 0; b < NBINS; b++)
        {
          double SigmaP = NODE->SADSIGMAA.SigmaP_bin[b];
          cmplex sigmaPP = NODE->SADSIGMAA.sigmaPP_bin[b];
          cmplex fanom = ascat_bin[b];
          const double ssqrbin = NODE->SADSIGMAA.MIDSSQR[b];
          double bterm = std::exp(-dBscat*ssqrbin/4.);
          double nAnom = bterm * nascat; // estimate of weighted number of scatterers
          //
          // Assume that our knowledge about the (weighted) number of sites at
          // a particular resolution follows a logNormal distribution, i.e. we
          // know the number within some uncertainty specified by a factor.
          // Numerical integration is easier if we work in terms of the log of
          // the number of sites and use a normal distribution.
          // For sites with a known prior number (e.g. S or Se from sequence),
          // use a tighter distribution with a sigma corresponding to a factor of
          // SIGMA_ATOM (currently 1.5 by default). For soaking experiments, the
          // number is much more uncertain, but something that allows a wide range
          // of possibilities but prefers small numbers works, e.g. argument of log(1)
          // for logmu in normal distribution and argument of log(4) for the sigma.
          // The value for the sigma (determining the width and expected value of
          // the distribution) could possibly be calibrated by a.u. volume or MW.
          double sigmalogratio, logmu;
          if (FIRST_CALL and !nascat_set)
          {
            sigmalogratio = std::log(4.); // Restrain more weakly to reduce bias to prior
            logmu = 0.;
          }
          else
          {
            sigmalogratio = std::log(SIGMA_ATOMS);
            logmu = std::log(nAnom);
          }
          // Compute expected value and standard deviation of absrhoFF from p(number of scatterers)
          // Integrate over plus/minus 3 sigma, unless upper limit beyond max scatterers
          double lognmin = logmu - 3. * sigmalogratio;
          double maxnAnom = nAnomMax(SigmaP, sigmaPP, fanom);
          double lognmax = std::log(maxnAnom);
          lognmax = std::min(lognmax, logmu + 3. * sigmalogratio);
          int ndiv = 50;
          double dlogn = (lognmax - lognmin)/ndiv;
          double sumprob(0.),mu1(0.),mu2(0.);
          for (int i = 0; i <= ndiv; i++)
          {
            double thislogn = lognmin + i*dlogn;
            double thisnAnom = std::exp(thislogn);
            double plogn = pNormal(thislogn, logmu, sigmalogratio);
            sumprob += dlogn*plogn;
            double thisabsrhoFF = absrhoFFfromNanom(SigmaP,sigmaPP,fanom,thisnAnom);
            mu1 += dlogn*         thisabsrhoFF *plogn;
            mu2 += dlogn*fn::pow2(thisabsrhoFF)*plogn;
          }
          mu1 /= sumprob;
          double calcabsrhoFF = mu1;
          mu2 /= sumprob;
          double var_rhoFF = mu2 - fn::pow2(mu1);
          double sigma_rhoFF = std::sqrt(var_rhoFF);
          double deltarFF = NODE->SADSIGMAA.absrhoFF_bin[b] - calcabsrhoFF;
          double R = fn::pow2(deltarFF / sigma_rhoFF) / 2;
          tgh.Target += R;
          if (do_gradient)
          {
            dR_by_dabsrhoFF_bin[b] = deltarFF / var_rhoFF;
            if (do_hessian)
            {
              d2R_by_dabsrhoFF2_bin[b] = 1. / var_rhoFF;
            }
          }
        }
      }
      if (SIGMA_RHOPM) // Weakly restrain to zero
      {
        for (int b = 0; b < NBINS; b++)
        {
          tgh.Target += fn::pow2(NODE->SADSIGMAA.rhopm_bin[b]/SIGMA_RHOPM)/2.;
          if (do_gradient)
          {
            dR_by_drhopm_bin[b] += NODE->SADSIGMAA.rhopm_bin[b]/fn::pow2(SIGMA_RHOPM);
            if (do_hessian)
            {
              d2R_by_drhopm2_bin(b,b) = 1./fn::pow2(SIGMA_RHOPM);
            }
          }
        }
      }
      // Add local linear smoothing restraint for rhopm
      std::vector<double> midssqr = NODE->SADSIGMAA.MIDSSQR;
      std::vector<double> rhopm_bin = NODE->SADSIGMAA.rhopm_bin;
      dtmin::Bounds rhopm_bounds;
      rhopm_bounds.on(0., 0.9);
      double linsigma(0.025); // linear restraint sigma
      auto smoothed =
          local_linear_restraint(midssqr, rhopm_bin, rhopm_bounds, linsigma, tgh.Diagonal);
      tgh.Target += smoothed.Target;
      for (int b = 0; b < NBINS; b++)
      {
        if (do_gradient)
        {
          dR_by_drhopm_bin[b] += smoothed.Gradient[b];
          if (do_hessian)
          {
            for (int b2 = 0; b2 < NBINS; b2++)
              d2R_by_drhopm2_bin(b,b2) += smoothed.Hessian(b, b2);
          }
        }
      }
    }

    if (do_gradient)
    { //memory
      int m(0),i(0);
      for (int b = 0; b < NBINS; b++)
      {
        if (refine_parameter_[m++])
        {
          tgh.Gradient[i] = dLL_by_dabsrhoFF_bin[b] + dR_by_dabsrhoFF_bin[b];
          if (do_hessian)
            tgh.Hessian(i,i) = d2LL_by_dabsrhoFF2_bin[b] + d2R_by_dabsrhoFF2_bin[b];
          i++;
        }
      }
      for (int b = 0; b < NBINS; b++)
      {
        if (refine_parameter_[m++])
        {
          tgh.Gradient[i] = dLL_by_drhopm_bin[b] + dR_by_drhopm_bin[b];
          if (do_hessian)
          {
            tgh.Hessian(i,i) = d2LL_by_drhopm2_bin[b];
            for (int b2 = 0; b2 < NBINS; b2++)
            {
              tgh.Hessian(i, i + b2 - b) += d2R_by_drhopm2_bin(b, b2);
            }
            if (REFINE_RHOFF and REFINE_RHOPM)
            {
              // In quadratic approximation, absolute value of off-diagonal Hessian
              // element can't be larger than maxoffdiag below.
              // Conditioning these elements here seems to be slightly better than
              // leaving issues to be corrected in adjust_hessian
              double hoffdiag = d2LL_by_dabsrhoFF_by_drhopm_bin[b];
              double hrhoFF = d2LL_by_dabsrhoFF2_bin[b];                // Without restraint term
              double hrhopm = d2LL_by_drhopm2_bin[b];                   // Without restraint term
              double maxoffdiag = std::sqrt(std::abs(hrhoFF * hrhopm)); // Perfect correlation
              if (std::abs(hoffdiag) > maxoffdiag)
                hoffdiag *= maxoffdiag / std::abs(hoffdiag); // transfer sign
              tgh.Hessian(i - NBINS, i) = tgh.Hessian(i, i - NBINS) = hoffdiag;
            }
          }
          i++;
        }
      }
      assert(m == total_number_of_parameters_);
      assert(i == nmp);
    } //memory

    return tgh;
  }

} //phasertng
