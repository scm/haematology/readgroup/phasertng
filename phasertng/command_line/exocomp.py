# LIBTBX_SET_DISPATCHER_NAME phasertng.exocomp

from iotbx.cli_parser import run_program
from phasertng.programs import exocomp

if (__name__ == '__main__'):
  try:
    run_program(exocomp.Program)
  except Exception as e:
    print(str(e))
