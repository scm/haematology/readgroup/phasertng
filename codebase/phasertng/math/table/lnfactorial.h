#ifndef __phasertng_table_lnfactorial_class__
#define __phasertng_table_lnfactorial_class__
#include <scitbx/array_family/shared.h>
#include <scitbx/constants.h>

namespace phasertng {
namespace table {

class lnfactorial
{
  private:
  const double log2pi2 = (log(scitbx::constants::two_pi)/2.);
  scitbx::af::shared<double> table;
  int ntabmax;

  public:
      static lnfactorial& getInstance() { return inst_; }
      static lnfactorial inst_;
  lnfactorial(int n=500); // Actually rarely need higher than 200

  double get(int) const;
};

}}
#endif

