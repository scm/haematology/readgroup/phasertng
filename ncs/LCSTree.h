#ifndef PHASER_NCS_LCSTREE_H
#define PHASER_NCS_LCSTREE_H

#include <phaser/ncs/Node.h>

#include <string>
#include <set>

namespace phaser {  namespace ncs {

    class LCSTree
    {
        private:
            const std::string _separator;
            size_t _word_count;
            std::string _separated_words;

            // Helper methods
            const std::set< char > assign_terminators( const Node& root ) const;
            const std::string get_lcs_under( const Node& root ) const;

        public:
            LCSTree( const std::string& separator = "$" );
            virtual ~LCSTree();

            // Accessors
            size_t get_word_count() const;
            const std::string& get_separator() const;

            // LCS methods
            void insert_word( const std::string& word );
            const std::string get_longest_common_substring() const;
    };

}  }

#endif /*PHASER_NCS_LCSTREE_H*/
