//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/python.hpp>
#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>

namespace phasertng { // so we don't have to write phasertng:: all the time

namespace boost_python {

  void init_phasertng_dag();
  void init_phasertng_output();
 // void init_phasertng_refine();
 // void init_phasertng_Entries();
  void init_phasertng_enum();

namespace { // to make sure there are no name clashes under any circumstance

void
init_phasertng_module()
{
  using namespace boost::python;
  scope().attr( "__path__" ) = "phasertng";

  init_phasertng_dag();
  init_phasertng_output();
 // init_phasertng_refine();
 // init_phasertng_Entries();
  init_phasertng_enum();

}

} // namespace phasertng::<anonymous>

} // namespace boost_python
} // namespace phasertng

BOOST_PYTHON_MODULE(phasertng_ext)
{
  phasertng::boost_python::init_phasertng_module();
}
