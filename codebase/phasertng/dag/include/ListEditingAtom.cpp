//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/dag/DagDatabase.h>
#include <phasertng/dag/DuplicateData.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/euler_angles.h>
#include <future>

namespace phasertng {
namespace dag {

  //ListEditing
  int
  DagDatabase::calculate_duplicate_atoms(
      double dmin,
      bool centrosymmetry,
      bool USE_STRICTLY_NTHREADS,
      int NTHREADS)
  {
    int tsize = size();
    double transMax = dmin/5.0;
    // Compare solutions to find duplicates further down list
    bool all_have_pose(true);
    for (int k = 0; k < NODES.size(); k++)
    {
      NODES[k].generic_flag = true;
      NODES[k].EQUIV = -999;
      if (NODES[k].POSE.size() == 0)
        all_have_pose = false;
      if (k > 0) PHASER_ASSERT(NODES[k].LLG <= NODES[k-1].LLG);
    }
    PHASER_ASSERT(all_have_pose);

    bool is_atom(true);
    dag::DuplicateData data2(NODES[0],is_atom);

    // Compare solutions to find duplicates further down list
    for (int k = 0; k < tsize; k++)
    {
      dag::Node& node_k = NODES[k];
      dmat33 orthmat_k = node_k.CELL.orthogonalization_matrix();
      dmat33 fracmat_k = node_k.CELL.fractionalization_matrix();
      int nmodels = node_k.POSE.size();
      for (unsigned k_chk = k+1; k_chk < tsize; k_chk++) //check those further down the list, including templates
      {
        dag::Node& node_k_chk = NODES[k_chk];
        dmat33 orthmat_kchk = node_k_chk.CELL.orthogonalization_matrix();
        dmat33 fracmat_kchk = node_k_chk.CELL.fractionalization_matrix();
        if (node_k_chk.EQUIV>=0) continue;
        // skip comparison if not the same number of models in trial solutions
        if (node_k_chk.POSE.size() != nmodels) continue;
        bool matched(true);
        //sv_bool used(nmodels,false);
        bool allmatched(true),originDefined(true); //not important initialization
        dvect3 zero3(0,0,0), originShift(0,0,0);
        int isymEuclid(-1);
        // Store some information about first model in reference solution
        dvect3 fracSolC_first = node_k.POSE[0].FRACT;
        // Find possible choices of origin shift of checked solution that make a match with first reference model
        for (unsigned s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
        {
          allmatched = false;
          originDefined = false;
          originShift = zero3;

          dag::Pose& pose_k_chk_s_chk_ori = node_k_chk.POSE[s_chk_ori];
          dag::Pose& pose0 = node_k.POSE[0];
          Identifier& firstmodlid = pose0.IDENTIFIER;
          bool different_id(pose_k_chk_s_chk_ori.IDENTIFIER != firstmodlid);
          if (different_id) continue;
          dvect3 fracSolC_chk = pose_k_chk_s_chk_ori.FRACT;;

          double ONE(1);
          for (int isym = 0; isym < data2.Euclid.group().order_z(); isym++)
          {
            // Determine sensible distance thresholds from dmin and model mean radius
            // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
            {
              // Check that centres of mass are sufficiently similar
              // Centre of mass of ensemble won't change with application
              // of point group symmetry.
              dvect3 fracSolC_chk_sym = data2.Euclid.doSymXYZ(isym,fracSolC_chk);
              for (unsigned j = 0; j < 3; j++)
              {
                while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
                while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
              }
              dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
              for (unsigned j = 0; j < 3; j++)
              {
                while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
                while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
              }
              // Account for any continuous shifts as origin shift
              dvect3 fracdelta_reference = data2.cb_op_r_to_reference_setting * fracdelta;
              dvect3 originShift_reference(0,0,0);
              for (unsigned j = 0; j < 3; j++) {
                if (data2.isshift[j]) {
                  originShift_reference[j] = -fracdelta_reference[j];
                  fracdelta_reference[j] = 0;
                }
              }
              fracdelta=data2.cb_op_r_from_reference_setting*fracdelta_reference;
              originShift=data2.cb_op_r_from_reference_setting*originShift_reference;
              dvect3 delta = orthmat_k*(fracdelta);
              double tdist = delta.length();
              if (tdist <= transMax)
              {
                isymEuclid = isym;
                originDefined = true;
                break;
              }
            } // ensym
            if (originDefined) break;
          } // isym
          if (!originDefined) continue; // Test different origins

          PHASER_ASSERT(originDefined);

          // Now use choice of relative origin (if defined) to test equivalence
          allmatched = true; //So far at least one match.  Negate if one fails.
          sv_dvect3 matched_site(nmodels);
          sv_bool used(nmodels,false);
          for (unsigned s = 0; s < nmodels; s++)
          { // For each model in the reference, check whether any matching models are the same
            // Determine sensible distance thresholds from dmin and model mean radius
            dag::Pose& pose_k_s = node_k.POSE[s];
            dvect3 fracSolC = pose_k_s.FRACT;
            matched = false;
            for (unsigned s_chk = 0; s_chk < nmodels; s_chk++)
            {
              dag::Pose& pose_k_chk_s_chk = node_k_chk.POSE[s_chk];
              Identifier& modlid = pose_k_s.IDENTIFIER;
              if (used[s_chk] or pose_k_chk_s_chk.IDENTIFIER != modlid) continue;
              dvect3 fracSolC_chk = pose_k_chk_s_chk.FRACT;
              if (originDefined)
                fracSolC_chk = data2.Euclid.doSymXYZ(isymEuclid,fracSolC_chk);
              for (unsigned j = 0; j < 3; j++)
              {
                while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
                while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
              }
              if (used[s_chk]) continue;
              if (s_chk == s_chk_ori)
              {
                matched = true;
                used[s_chk] = true;
                matched_site[s_chk] = fracSolC_chk;
              }
              else
              {
                SpaceGroup sg = !originDefined ? data2.Euclid : data2.SG;
                double ONE(1);
                for (unsigned isym = 0; isym < sg.group().order_z(); isym++)
                {
                  {
                    // Check that centres of mass are
                    // sufficiently similar. Centre of mass of ensemble won't
                    // change with application of point group symmetry.
                    dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
                    dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
                    dvect3 shiftdelta(0,0,0);
                    for (unsigned j = 0; j < 3; j++)
                    {
                      while (fracdelta[j] < -0.5) { fracdelta[j] += ONE; shiftdelta[j]++; }
                      while (fracdelta[j] >= 0.5) { fracdelta[j] -= ONE; shiftdelta[j]--; }
                    }
                    dvect3 delta = orthmat_k*(fracdelta);
                    double tdist = delta.length();
                    if (tdist > transMax) continue;
                    matched = true;
                    used[s_chk] = true;
                    matched_site[s_chk] = fracSolC_chk_sym-shiftdelta;
                    if (matched) break;
                  } // ensym
                  if (matched) break;
                } // isym
                if (matched) break;
              } // s_chk
            } // s_chk
            if (!matched) allmatched = false;
          } // s
          if (allmatched)
          {
            PHASER_ASSERT( node_k_chk.POSE.size() == matched_site.size());
            for (unsigned s = 0; s < node_k_chk.POSE.size(); s++)
              node_k_chk.POSE[s].FRACT = matched_site[s];
            break;
          }
          // Failed to match everything.  Try again with another possible origin shift.
          if (allmatched) break;
          for (unsigned m = 0; m < nmodels; m++)
            used[m] = false;
        } // s_chk_ori
        if (allmatched)
        {
          node_k_chk.generic_flag = false;
          node_k_chk.EQUIV = k;
          //replace the kept TFZ with the TFZ highest from those found to be equivalent
          node_k.ZSCORE = std::max(node_k_chk.ZSCORE,node_k.ZSCORE);
        }
      } //k_chk
    } // end #pragma omp parallel

    //after paralled section, when all KEEP is flagged
    for (int k = 0; k < NODES.size(); k++)
      NODES[k].NUM = -999;
    //first pass for known
    int na(1);
    for (int k = 0; k < NODES.size(); k++)
      if (NODES[k].generic_flag)
        NODES[k].NUM = na++; //output number equivalent
    bool all_numbered(true);
    int count_loop(0);
    do
    {
      all_numbered = true;
      for (int k = 0; k < NODES.size(); k++)
        if (!NODES[k].generic_flag) //if this is equivalent to something
          NODES[k].NUM = NODES[NODES[k].EQUIV].NUM; //may be negative
      for (int k = 0; k < NODES.size(); k++)
        if (NODES[k].NUM < 0)
          all_numbered = false; //flag that we are not yet done
      PHASER_ASSERT(count_loop++ < 100); //infinite loop check
    }
    while (!all_numbered);

    int count(0);
    for (int k = 0; k < NODES.size(); k++)
    {
      if (!NODES[k].generic_flag) count++;
    }
    return count;
  }

  int
  DagDatabase::calculate_duplicate_llg()
  {
    int tsize = size();
    // Compare solutions to find duplicates further down list
    bool all_have_pose(true);
    for (int k = 0; k < NODES.size(); k++)
    {
      NODES[k].generic_flag = true;
      NODES[k].EQUIV = -999;
      if (NODES[k].POSE.size() == 0)
        all_have_pose = false;
      if (k > 0) PHASER_ASSERT(NODES[k].LLG <= NODES[k-1].LLG);
    }
    PHASER_ASSERT(all_have_pose);

    // Compare solutions to find duplicates further down list
    for (int k = 0; k < tsize; k++)
    {
      dag::Node& node_k = NODES[k];
      for (unsigned k_chk = k+1; k_chk < tsize; k_chk++) //check those further down the list, including templates
      {
        dag::Node& node_k_chk = NODES[k_chk];
        if (node_k_chk.LLG > node_k.LLG - DEF_PPM && node_k_chk.LLG < node_k.LLG + DEF_PPM)
          NODES[k].generic_flag = false;
      } //k_chk
    } // end #pragma omp parallel

    int count(0);
    for (int k = 0; k < NODES.size(); k++)
    {
      if (!NODES[k].generic_flag) count++;
    }
    return count;
  }

}}
