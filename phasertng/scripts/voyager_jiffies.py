import os, os.path

def emit_callback(text,progress_callback=None):
    #{{{
      if progress_callback is not None:
        progress_callback.emit(text)
    #}}}

def silent_cards(level="silence"):
      dryrun_cards = '''
phasertng suite write_files off
phasertng suite write_data off
phasertng suite write_cards off
phasertng suite write_xml off
phasertng suite write_density off
phasertng suite write_phil off
phasertng suite write_log off
phasertng suite level ''' + str(level)
      return dryrun_cards

def voyager_readme(dbdir):
    dbnotice = "This is a Phasertng-Voyager database directory tree\n" + \
               "It can be moved with no adverse consequences\n" + \
               "Adjust commands accordingly\n" + \
               "As written: DAGDATABASE="+ str(dbdir) + "\n"
    if os.path.exists(os.path.abspath(dbdir)):
      fname = os.path.join(dbdir,"README")
      f = open(fname, "w")
      f.write(dbnotice)
      f.close()
      return dbnotice
    return "" # no message written
