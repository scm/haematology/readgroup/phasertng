import iotbx.phil
import os.path
from collections import defaultdict
import libtbx.phil
from libtbx.phil import parse
from phasertng.phil.master_phil_file import *
from phasertng.phil.master_node_file import *
from phasertng.phil.converter_registry import *

def rreplace(s, old, new, occurrence):
  li = s.rsplit(old, occurrence)
  return new.join(li)

def cards_to_phil(cards=None,dag=False):
  #filter the cards through master_phil, only pick out the lines that are in master
  #don't actually need the keyword_list since we want it to accept all keywords
  #but it is a parameter for build strings, so we pass it here for flexibility
  #likewise for program_name, which should always be None
  master_params = build_voyager_phil_file(program_name="InputCard")
  master_params = master_params + build_node_str()
  master_phil = parse(master_params,converter_registry=converter_registry)
  ' '.join(cards.split("\t ")) #make only one whitespace between keywords
  lines = cards.split("\n")

  multiple_keywords = defaultdict(list)
  for e in master_phil.all_definitions():
    singleline = e.parent.multiple
    #special case so that doesn't have to be multiple!
    extract_attribute_error = False
    if (getattr(e.object, "style", extract_attribute_error) is not extract_attribute_error):
      styletype = str(getattr(e.object, "style"))
      if "multiple" in styletype:
         singleline = True
    if (singleline): #special case so that doesn't have to be multiple!
      tester = ".".join(e.path.split('.')[:-1])
      trailing = ".".join(e.path.split('.')[-1:])
      extract_attribute_error = False
      size = 1
      boolean = False
      if (getattr(e.object, "type", extract_attribute_error) is not extract_attribute_error):
        valuetype = str(getattr(e.object, "type"))
        valuetype = valuetype.replace('(',',',1)
        valuetype = rreplace(valuetype,')',',',1)
        attr_list = valuetype.replace(' ','').split(',')
        for a in attr_list:
          kv = a.split("=")
          if len(kv) == 2:
            if kv[0] == "size":
              size = int(kv[1])
        if "bool" in valuetype:
          boolean = True # special flag, ON/OFF -> True/False
    # choice is x+y+z so single parameter, 999 flag is for separate choices
    #   if "choice" in valuetype:
    #     size = 999 # special flag, maximum choices
      multiple_keywords[tester].append(dict({ trailing: [None,size,boolean] }))

  user_phil = ""
  for line in lines:
    line = line.strip()
    if len(line):
      #algorithm only works with one level down of nesting, so have to remove outer scope
      #  above is rubbish, we don't need to remove it
      #line = line.replace(phil_scope + " ","")
      line = ' '.join(line.split()) #make only one whitespace between keywords
      for e in master_phil.all_definitions():
        if not e.parent.multiple:
          #startswith is not a strong enough condition, must match all keywords in e.path
          edot = e.path.split(".")
          linedot = line.replace(" ",".").lower().split(".")
          linedot = linedot[:len(edot)] #take same number of array elements
          if linedot == edot:
            extract_attribute_error = False
            if (getattr(e.object, "type", extract_attribute_error) is not extract_attribute_error):
              valuetype = str(getattr(e.object, "type"))
              key_phil = e.path + " = "
              uservalue = line[len(e.path):]
              if valuetype == "choice":
                key_phil += ""+uservalue.strip() #"+"-delimited string
              elif valuetype == "bool":
                key_phil += "True" if (uservalue.upper().strip() == "ON") else "False"
              else:
                key_phil += uservalue.strip()
              key_phil += "\n"
              user_phil += key_phil

  # now we only want to look over the multiple keywords and separate out the cards into
  # bracketed scopes
  for key in multiple_keywords:
    for line in lines:
   #  line = phil_scope + " " + line.lstrip()
      line = line.lstrip()
      linedot = line.replace(" ",".").lower()
      multi_keys = ""
      if linedot.startswith(key):
        multi_keys = key + " {" + "\n"
        espace = key.replace("."," ")
        linedot = line.replace(espace,"").split() # whitespace, mulitple=1
        while len(linedot):
          subkey = linedot[0]
          linedot = linedot[1:] #take same number of array elements
          assert(len(linedot))
          for val in multiple_keywords[key]:
            for k in val:
              if k == subkey:
                multi_keys += " " + subkey + " = "
                size = val[k][1]
                boolean = val[k][2]
                #choice has size set to 999 flag
                #look for next subkey instead, as the proxy for the number
                for i in range(0,size):
                  assert(len(linedot))
                  if boolean:
                    multi_keys += "True" if (linedot[0].upper().strip() == "ON") else "False"
                  else:
                    multi_keys += linedot[0] + " "
                  linedot = linedot[1:] #take same number of array elements
              #   if size == 999: # choice
              #     if len(linedot) == 0:
              #       break # run out of choices
              #     for val2 in multiple_keywords[key]:
              #        for k2 in val2:
              #         if k2 == linedot[0]: #choice must not be subkey!!
              #           break # choice
                multi_keys += "\n"
       #multi_keys += "}\n"
        multi_keys += "}"
      user_phil += multi_keys
# print user_phil
  return user_phil

if __name__ == "__main__":
  user_params = '''
phaserdag node hklin  tag "toxd" identifier 7882652528575457769
phaserdag node tncs_order 1
phaserdag node tncs_rms 0
phaserdag node total_scattering 0
phaserdag node z 1
phaserdag node total_molecular_weight 0
phaserdag node annotation None
phaserdag node spacegroup "Hall: P 2ac 2ab (x,y,z)"
phaserdag node unitcell  73.58  38.73  23.19 90.00  90.00  90.00
phaserdag node twinned false
phaserdag node clash_matrix None
phaserdag node clash_worst 0
phaserdag node special_position_warning false
phaserdag node llg 0
phaserdag node zscore 0
phaserdag node rfactor 0
phaserdag node information_content 0
phaserdag node expected_information_content 0
phaserdag node sad_information_content 0
phaserdag node sad_expected_llg 0
phaserdag node parents None
phaserdag node rellg 0
phaserdag node data_scale 1
  '''
  phil = cards_to_phil(cards = user_params)
  print("nodes: ")
  print(phil)
  user_params = '''
phasertng anisotropy beta   -0.0001408570982 0.0004448552612 0.0001771374106 -8.606961904e-20 -1.437636302e-19 -2.731111732e-19
phasertng anisotropy delta_bfactor   2.860072362
phasertng anisotropy direction_cosines   0 1 0 0 0 1 1 0 0
phasertng anisotropy outer_bin_contrast   0.7000815579
phasertng anisotropy sharpening_bfactor   -1.525288225
phasertng anisotropy wilson_bfactor   15.91672189
phasertng anisotropy wilson_scale   0.0001607724274
phasertng dag cards filename   "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.cards"
phasertng dag phil filename   "/tmp/PHASERTNG/toxd/toxd.1626864536279425890.tncs.dag.phil"
phasertng data anomalous   False
phasertng data french_wilson   True
phasertng data intensities   False
phasertng data map   False
phasertng data number_of_reflections   3161
phasertng data resolution_available   2.300205241 36.79100037
phasertng data unitcell   73.58200073 38.73300171 23.18899918 90 90 90
phasertng hklin filename   "../../tutorial/data/toxd/toxd.mtz"
phasertng hklin identifier   15968090302689210158
phasertng hklin tag   "toxd"
phasertng labin aniso   ANISO
phasertng labin fnat   FTOXD3
phasertng labin fneg   F(-)<<F
phasertng labin fpos   F(+)<<F
phasertng labin free   FreeR_flag
phasertng labin inat   I<<FW
phasertng labin ineg   I(-)<<I
phasertng labin ipos   I(+)<<I
phasertng labin resn   RESN
phasertng labin sigfnat   SIGFTOXD3
phasertng labin sigfneg   SIGF(-)<<SIGF
phasertng labin sigfpos   SIGF(+)<<SIGF
phasertng labin siginat   SIGI<<FW
phasertng labin sigineg   SIGI(-)<<SIGI
phasertng labin sigipos   SIGI(+)<<SIGI
phasertng labin wll   WLL
phasertng suite database   "/tmp/PHASERTNG/toxd"
phasertng suite fileroot   "/tmp/PHASERTNG/toxd"
phasertng suite filestem   "toxd"
phasertng suite level   logfile
phasertng tncs order   1
phasertng tncs parity   0 0 0
phasertng tncs unique   True
phasertng tncs vector   0 0 0
phasertng tncs_analysis indicated   False
phasertng tncs_analysis top_non_origin   0
phasertng tncs_analysis result  order  1 vector  0 0 0  parity  0 0 0  unique  True
  '''
  phil = cards_to_phil(cards = user_params)
  print("cards:")
  print(phil)
