import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import importlib
import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError,Identifier
from phasertng import enum_out_stream
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.run.ModeRunner import *
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
import argparse

def scotty(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_scotty)

def inner_scotty(basetng,user_phil_str=""):
  #{{{
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="scotty")
    initparams = initparams +  "\n" +  build_auto_str("scotty")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()
    dbdir = working_params.phasertng.suite.database
    assert(dbdir is not None)
    if not os.path.isdir(dbdir):
      os.makedirs(dbdir)
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    working_params.phasertng.reflections.read_anomalous = False
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    run_silent = False
    if run_silent:
      #permanently turn off all the file writing, and only output the non-database file
      dryrun_cards = silent_cards("logfile")
    else:
      dryrun_cards = ""

    #set the outputs etc
    basetng.input = InputCard()
    ignore_unknown_keys = True
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keys)
    basetng.set_user_input_for_suite(basetng.input)

    loop_auto = str("Scotty")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
       "" if working_params.phasertng.title is None else working_params.phasertng.title)

    xmlout = working_params.phasertng.graph_report.filestem + ".xml"
    xmlout = os.path.join(dbdir,xmlout)
    xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
    #clear the tracker before we start
    if os.path.exists(xmlout):
      os.remove(xmlout)
    ModeRunner(basetng=basetng,mode_list=['chk'])

    basetng.input = InputCard()
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keys)
    #we copy the keywords that are in scotty phil to the equivalent scripting keywords in phasertng
    working_params.phasertng.isostructure.tutorial.ignore_best = working_params.scotty.match.ignore_best_match
    working_params.phasertng.isostructure.tutorial.ignore_match = working_params.scotty.match.ignore_pdbid_list
    #the force match keyword for isostructure can be either a pdbid or a filename
    #but with the scotty keywords we explicity say use_model and then put the model or the discovered into force_match
    working_params.phasertng.isostructure.tutorial.force_match = working_params.scotty.match.force_match_with_pdbid
    ModeRunner(basetng=basetng,mode_list=['walk'])

    #first choice
    hklin = working_params.scotty.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) == 1:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    ModeRunner(basetng=basetng,mode_list=['beam'])
    matches = basetng.input.get_as_str(".phasertng.isostructure.found.match.")
    perfect_match = basetng.input.get_as_str(".phasertng.isostructure.found.perfect_match.")
    pointgroup = basetng.input.get_as_str(".phasertng.isostructure.found.pointgroup.")
    #strip necessary
    if (matches.strip() == "True"): # and pointgroup.strip() == 'same'):
    # ent = basetng.input.get_as_str(".phasertng.isostructure.pdbout.ent.")
    # pdbout = basetng.input.get_as_str(".phasertng.isostructure.pdbout.filename.")
      seqout = isNone(basetng.input.get_as_str(".phasertng.seqout.filename."))
      ifile = { "seqout" : seqout }

     #the pdb list can be extracted from the combinations
     #the pdb has been split into chains
      found_pdb = []
      n = basetng.input.size(".phasertng.search_combination.")
      if (int(n) == 0):
        basetng.logLoopInfo(enum_out_stream.process,"No search combinations")
        basetng.logLoopExit(enum_out_stream.process,loop_auto)
        return
      for i in range(n):
        pdbf = basetng.input.get_as_str_i(".phasertng.search_combination.",i)
        stem = os.path.basename(pdbf).split('.')[0]
        stem = stem.split('-') #ident-pdbid_chain
        assert(len(stem) == 2)
        stem = stem[1]
        pdbf = pdbf.strip("\"").lstrip().rstrip()
        found_pdb.append({ 'pdbf' : pdbf , "stem" : stem})

      #reflid is in the database, but we read it in again
      #fname = basetng.input.get_as_str(".phasertng.hklout.filename.")
      #basetng.input.set_as_str(".phasertng.hklin.filename.",str(fname))
      basetng.input.set_as_str(".phasertng.reflections.read_anomalous.",str(False)) #not required
      aniso1 = ModeRunner(basetng=basetng,mode_list=['imtz','data','sgx','aniso']).solution()

      #generate ensembles
      #ensembles has to be done after the aniso, for the scaling
      loop_ens = "Generate models"
      #we can just change the pathway tracker because pathlog carries the logfile info
      basetng.logLoopOpen(enum_out_stream.process,loop_ens,len(found_pdb),"")
      entries_dagcards = []
      DAGDB = basetng.DAGDATABASE
      for found in found_pdb:
        basetng.logLoopNext(enum_out_stream.process,loop_ens,found['stem'])
      # pdbf = os.path.join(pdbdir,pdbf)
        basetng.input.set_as_str(".phasertng.model.filename.",found['pdbf'])
        basetng.input.set_as_str(".phasertng.model.tag.",found['stem'])
        basetng.input.set_as_str(".phasertng.model.vrms_estimate.",str(0.4)) #Chothia and Lesk
        basetng.input.set_as_str(".phasertng.tncs.order.",str(1)) #tncs present in model AJM TODO
      # we can reduce the boxscale because we are just refining
        basetng.input.set_as_str(".phasertng.molecular_transform.preparation.","interpolation monostructure")
        basetng.input.set_as_str(".phasertng.molecular_transform.boxscale.","3")
        ModeRunner(basetng=basetng,mode_list=['esm'])
        entries_dagcards.append(basetng.input.generic_string)
        basetng.load_moderunner(aniso1,user_cards,dryrun_cards)
      basetng.logLoopShut(enum_out_stream.process,loop_ens)

      #generate ensembles
      basetng.input.set_as_str(".phasertng.composition.number_in_asymmetric_unit.",str(1))
      basetng.input.set_as_str(".phasertng.biological_unit.sequence.filename.",str(ifile['seqout']))
      basetng.input.set_as_str(".phasertng.composition.multiplicity_restriction.",str(1)) #forced
      basetng.input.set_as_str(".phasertng.composition.order_z_expansion_ratio.",str(1)) #forced
      ModeRunner(basetng=basetng,mode_list=['tncso','tncs','feff','cca','ccs'])
      #we add the holds to the nodes here after the cca
      #put the ensembles in the database
      basetng.input.generic_int = ord('A') #0
      basetng.input.generic_string = ""
      for f in entries_dagcards:
        basetng.input.generic_string = basetng.input.generic_string + f + node_separator
      ModeRunner(basetng=basetng,mode_list=['join'])
      loop_put = "Put models"
      basetng.logLoopOpen(enum_out_stream.process,loop_put,len(found_pdb),"")
      for found in found_pdb:
        basetng.logLoopNext(enum_out_stream.process,loop_put,found['stem'])
        basetng.input.set_as_str(".phasertng.search.tag.",str(found['stem']))
        basetng.input.set_as_str(".phasertng.put_solution.clear.",str(False))
        ModeRunner(basetng=basetng,mode_list=['put'])
      basetng.logLoopShut(enum_out_stream.process,loop_put)
      basetng.input.set_as_str(".phasertng.macrbr.ncyc.",str("200 200 200"))
      refpoint = ModeRunner(basetng=basetng,mode_list=['rbr','rbm']).solution()
      strR = str(round(DAGDB.NODES.LIST[0].LLG))
      basetng.logLoopInfo(enum_out_stream.process,"LLG after rigid body refinement = " + strR)
      if float(DAGDB.NODES.LIST[0].LLG) < 0:
        basetng.logLoopInfo(enum_out_stream.process,"Refined negative LLG too low for genuine solution")
      else:
        rundict = dict()
        if  len(working_params.scotty.control.xref_software) == 0:
          ModeRunner(basetng=basetng,mode_list=['rfac'])
          basetng.input.set_as_str(".phasertng.grid_search.resolution.",str('3'))
          basetng.input.set_as_str(".phasertng.grid_search.selection.",str('rot'))
          ModeRunner(basetng=basetng,mode_list=['rbgs'])
          basetng.input.set_as_str(".phasertng.grid_search.selection.",str('tra'))
          ModeRunner(basetng=basetng,mode_list=['rbgs'])
          basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.",str("rotn tran bfac"))
          ModeRunner(basetng=basetng,mode_list=['rbr','rbm'])
          ModeRunner(basetng=basetng,mode_list=['rfac'])
          rundict["no refinement"] = strR
        if working_params.scotty.control.use_xref:
       #{{{
          for selection in ['refmac','phenix']:
         #{{{
            if selection in working_params.scotty.control.xref_software:
           #{{{
              #branches from last refpoint
              basetng.load_moderunner(refpoint,user_cards,dryrun_cards)
              basetng.input.set_as_str(".phasertng.refinement.method.",selection)
              ModeRunner(basetng=basetng,mode_list=['xref'])
              strR = "%.2f" % round(DAGDB.NODES.LIST[0].RFACTOR,2)
              basetng.logLoopInfo(enum_out_stream.process,"R-factor after refinement = " + strR)
              io_solved_rfactor = 25
              if (float(DAGDB.NODES.LIST[0].RFACTOR) > io_solved_rfactor):
                    basetng.input.set_as_str(".phasertng.grid_search.resolution.",str('3'))
                    basetng.input.set_as_str(".phasertng.grid_search.selection.",str('tra'))
                    ModeRunner(basetng=basetng,mode_list=['rbgs'])
                    basetng.input.set_as_str(".phasertng.grid_search.selection.",str('rot'))
                    ModeRunner(basetng=basetng,mode_list=['rbgs'])
                    basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.",str("rotn tran bfac"))
                    ModeRunner(basetng=basetng,mode_list=['rbr','rbm'])
                    ModeRunner(basetng=basetng,mode_list=['rfac'])
                    strR = "%.2f" % round(DAGDB.NODES.LIST[0].RFACTOR,2)
                    basetng.logLoopInfo(enum_out_stream.process,"R-factor after wide search = " + strR)
                    if (float(DAGDB.NODES.LIST[0].RFACTOR) > io_solved_rfactor):
                      ModeRunner(basetng=basetng,mode_list=['xref'])
              hires = float(basetng.input.get_as_str(".phasertng.notifications.hires."))
              if float(hires) < float(basetng.input.get_as_str(".phasertng.refinement.resolution.")):
                    basetng.logLoopInfo(enum_out_stream.process,"Refine to full resolution = " + str(hires))
                    basetng.input.set_as_str(".phasertng.refinement.resolution.",str(0))
                    ModeRunner(basetng=basetng,mode_list=['xref'])
              strR = "%.2f" % round(DAGDB.NODES.LIST[0].RFACTOR,2)
              rundict[selection] = strR
           #}}}
         #}}}
       #}}}
        for k,v in rundict.items():
          basetng.logLoopInfo(enum_out_stream.process,"R-factor after wide search and " + k + " refinement = " + v)
    basetng.logLoopShut(enum_out_stream.process,loop_auto)

    ModeRunner(basetng=basetng,mode_list=['tree'])
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  return basetng
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="mtz data file",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  parser.add_argument('-t',"--tutorial",dest='tutorial',action='store_true',help="tutorial mode",required=False)
  parser.add_argument('-f',"--force",help="force match to pdbid or pdbfile with CRYST1 card",default=None,required=False)
  parser.add_argument('-v',"--verbose",dest='verbose',action='store_true',help="verbose output",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is None and args.output is None):
    print("At least one of either --phil or --output must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  if (args.input is not None):
    if not (os.path.exists(args.input)):
      print("mtz data file cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.hklin.filename=\"" + str(args.input)+"\"\n"
  if (args.verbose):
    user_phil_str = user_phil_str + "\nphasertng.suite.level=verbose\n"
  if (args.output is not None):
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      print("database directory for output cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\nphasertng.suite.database=\"" + str(args.output)+"\"\n"
  user_phil_str = user_phil_str + "\nphasertng.isostructure.tutorial.ignore_best = " + str(args.tutorial)+"\n"
  if (args.force is not None):
    user_phil_str = user_phil_str + "\nphasertng.isostructure.tutorial.force_match = " + str(args.force)+"\n"
  user_phil_str = user_phil_str.replace("\n\n","\n")
  if (args.phil is not None or (args.input is not None and args.output is not None)):
    scotty( user_phil_str=user_phil_str)
  else:
    print("command line arguments insufficient")
    sys.exit(0)
