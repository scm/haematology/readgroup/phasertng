#ifndef __phasertng_aniso_direction_enum__
#define __phasertng_aniso_direction_enum__
//#include <phasertng/enum/EnumUtilities.h>

namespace phasertng {
namespace aniso {
  enum direction {
              HH,
              KK,
              LL,
              HK,
              HL,
              KL
           };
/*
}}
  MakeEnum ( direction,
              (HH)
              (KK)
              (LL)
              (HK)
              (HL)
              (KL)
           );
*/
}}
#endif
