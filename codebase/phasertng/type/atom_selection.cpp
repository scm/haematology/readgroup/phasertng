//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/atom_selection.h>

namespace phasertng {
namespace type {

  atom_selection::atom_selection(const std::string parameter,
        const std::string dephalt_
        ) :
     optional(parameter),
     dephalt(dephalt_),
     uvalue(dephalt_)
  {}

  void
  atom_selection::set_value(std::string other)
  {
    user = true;
    if (!other.size()) return;
//stored unquoted - filename cannot start with quote
    if (other[0] != '"')
    {
      uvalue = other;
    }
    else
    {
      std::string unquoted = other;
      while (unquoted.size() && unquoted[0] == '"')
      {
        std::string tmp = "";
        for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
          tmp += std::string(1,unquoted[i]);
        unquoted = tmp;
      }
    //  phaser_assert(!unquoted.size() || unquoted[0] != '"');
      uvalue = unquoted;
    }
  }

  void
  atom_selection::set_value(const sv_string& vcards,const std::string& other)
  {
    paranoia_check(vcards);
    set_value(other);
  }

  //virtual
  bool
  atom_selection::is_default() const
  { return uvalue == dephalt; }

  void
  atom_selection::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  atom_selection::to_python()
  { return unparse_parameter(false); }

  void
  atom_selection::from_python(std::string str)
  { set_value(str); }

  std::string
  atom_selection::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  atom_selection::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  //if the string contains a space, unparse it with quotes around it
  std::string
  atom_selection::unparse_parameter(bool phil) const
  {
    std::string v = value_or_default();
    if (!v.size())
      v = None();
    return (v.find_first_not_of(' ') == std::string::npos) ? v: "\"" + v + "\"";
  }

  std::string
  atom_selection::show_default(bool phil) const
  {
    std::string v = dephalt;
    if (!v.size())
      v = None();
    return (v.find_first_not_of(' ') == std::string::npos) ? v : "\"" + v + "\"";
  }

}}
