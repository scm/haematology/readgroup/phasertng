//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/main/Phasertng.h>
#include <phasertng/dtmin/Minimizer.h>
#include <phasertng/minimizer/RefineSSR.h>
#include <phasertng/data/Cluster.h>
#include <phasertng/data/Scatterers.h>
#include <phasertng/data/Selected.h>
#include <phasertng/io/PdbHandler.h>
#include <phasertng/io/tostr2.h>

namespace phasertng {

  //Substructure_refinement
  void Phasertng::runSSR()
  {
    if (DAGDB.size() == 0)
    {
      logTab(out::LOGFILE,"No nodes");
      return;
    }
    if (!REFLECTIONS->prepared(reflprep::CCS))
    throw Error(err::INPUT,"Reflections not prepared with correct mode (ccs)");
    if (!REFLECTIONS->ANOMALOUS)
    throw Error(err::INPUT,"Reflections not prepared with anomalous data");
    if (!DAGDB.NODES.is_all_full_or_part())
    throw Error(err::INPUT,"Dag not prepared with correct mode (ssd failed?)");

    if (REFLECTIONS->check_native())
    logAdvisory("Reflections have no anomalous signal");

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Load Entries");
    auto used_modlid = DAGDB.NODES.used_modlid_full_and_part(); //and part
    for (auto id : used_modlid)
    {
      //substructure is from hyss or phassade
      load_entry_from_database(where,entry::SUBSTRUCTURE_PDB,id,true); //optional=true
      //coordinates are molecular replacement solution
      bool loaded = load_entry_from_database(where,entry::COORDINATES_PDB,id,true); //optional=true
      //must have either substructure or coordinates to start, but this will be checked later
      if (!loaded) //it might be a map
        load_entry_from_database(where,entry::FCALCS_MTZ,id,true); //optional=true
    }
    }}

    REFLECTIONS->setup_phsr(
      input.value__mtzcol.at(".phasertng.labin.phsr.").value_or_default());
    input.value__mtzcol.at(".phasertng.labin.phsr.").set_value(input.value__mtzcol.at(".phasertng.labin.phsr.").name());
    //REFLECTIONS->setup_resharpening(); AJM TODO

    //Selected will be reset internally as likelihood changes
    //and will be reset each cycle
    //but is defined externally here
    double sticky_hires(0);
    Selected selected(NTHREADS,USE_STRICTLY_NTHREADS,DAGDB.NODES.is_all_tncs_modelled());
    {{
    logUnderLine(out::LOGFILE,"Select Reflections");
    auto seltxt = selected.set_resolution(
        REFLECTIONS->data_hires(),
        DAGDB.resolution_range(),
        input.value__flt_number.at(".phasertng.resolution.").value_or_default());
    logTabArray(out::LOGFILE,seltxt);
    sticky_hires = selected.STICKY_HIRES;
    }}

    Cluster cluster;
    if (!input.value__path.at(".phasertng.cluster_compound.filename.").is_default())
    {
      cluster.SetFileSystem(input.value__path.at(".phasertng.cluster_compound.filename.").value_or_default());
      cluster.ReadPdb();
    }

    Scatterers scatterers(REFLECTIONS->ANOMALOUS,REFLECTIONS->WAVELENGTH);
    //collect all the scatterers in one go
    {{
    { //all the setup from user input
      int N = input.size(".phasertng.scattering.fluorescence_scan.scatterer.");
      for (int n = 0; n < N; n++)
      {
        scatterers.FLUORESCENCE_SCAN[
          input.array__string.at(".phasertng.scattering.fluorescence_scan.scatterer.").at(n).value_or_default_upper()
          ] = std::make_tuple(
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fp.").at(n).value_or_default(),
          input.array__flt_number.at(".phasertng.scattering.fluorescence_scan.fdp.").at(n).value_or_default(),
          input.array__choice.at(".phasertng.scattering.fluorescence_scan.fix_fdp.").at(n).value_or_default()
         );
      }
      int M = input.size(".phasertng.scattering.sasaki_table.scatterer.");
      for (int m = 0; m < M; m++)
      {
        scatterers.MOVE_TO_EDGE[
          input.array__string.at(".phasertng.scattering.sasaki_table.scatterer.").at(m).value_or_default_upper()
          ] = input.array__boolean.at(".phasertng.scattering.sasaki_table.move_to_edge.").at(m).value_or_default();
      }
      scatterers.PARTICLE = formfactor::particle2Enum(
          input.value__choice.at(".phasertng.scattering.form_factors.").value_or_default_upper());
    }
    // AJM  cluster compounds not implemented
    PHASER_ASSERT(DAGDB.WORK->COMPOSITION.size());
    for (auto comp : DAGDB.WORK->COMPOSITION)
    {
      scatterers.insert(comp.first);
    }

    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      auto& full = DAGDB.WORK->FULL;
      auto& part = DAGDB.WORK->PART;
      auto& substructure = part.ENTRY->SUBSTRUCTURE;
      if (part.PRESENT)
        for (int a = 0; a < substructure.XRAY.size(); a++)
          scatterers.insert(substructure.XRAY[a].scattering_type);
      auto& coordinates = full.ENTRY->COORDINATES;
      if (full.PRESENT)
        for (int a = 0; a < coordinates.XRAY.size(); a++)
          scatterers.insert(coordinates.XRAY[a].scattering_type);
    }
    PHASER_ASSERT(scatterers.FP_FDP.size());
    auto ss = input.value__strings.at(".phasertng.substructure.completion.scatterer.").value_or_default_set();
    for (auto scat : ss)
      scatterers.insert(tostr2(scat).first);
    }}

    //double io_bmin = input.value__flt_number.at(".phasertng.bfactor.minimum.").value_or_default();
    bool io_enantiomorphs = input.value__boolean.at(".phasertng.macsad.enantiomorphs.").value_or_default();
    bool io_hand = input.value__boolean.at(".phasertng.macsad.hand.").value_or_default();
    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Enantiomorphs");
    std::set<SpaceGroup> set_of_spacegroups;
    DAGDB.restart(); //very important to call this to set WORK
    while (!DAGDB.at_end())
    {
      if (DAGDB.WORK->FULL.PRESENT)
      {
        logTab(where,"Cannot change enantiomorphs with partial structure");
        io_enantiomorphs = false;
      }
      SpaceGroup sg(DAGDB.WORK->SG->HALL);
      set_of_spacegroups.insert(sg);
    }
    int s(1),S(set_of_spacegroups.size());
    if (S>1)
      logTab(where,"There are " + itos(S) + " different space groups");
    for (auto& sg : set_of_spacegroups)
    {
      std::string sS = (S==1) ? "":nNtos(s++,S);
      logTab(where,"Original spacegroup" + sS + " " + sg.enantiomorph().str());
      if (io_enantiomorphs)
      {
        sg.type().is_enantiomorphic() ?
          logTab(where,"Enantiomorph change changes spacegroup"):
          logTab(where,"Enantiomorph change does not change spacegroup");
        logTab(where,"Enantiomorphic spacegroup " + sg.enantiomorph().str());
      }
    }
    }}

    //now process the substructure to add fp and fdp as determined by input
    std::vector<std::set<std::string>> macrocycle(3);
    macrocycle[0] = input.value__choice.at(".phasertng.macsad.macrocycle1.").value_or_default_multi_set();
    macrocycle[1] = input.value__choice.at(".phasertng.macsad.macrocycle2.").value_or_default_multi_set();
    macrocycle[2] = input.value__choice.at(".phasertng.macsad.macrocycle3.").value_or_default_multi_set();
    for (int i = 0; i < 3 and REFLECTIONS->check_native(); i++)
      macrocycle[i].erase("sigp");
    std::vector<sv_string> macro = {
      sv_string(macrocycle[0].begin(), macrocycle[0].end()),
      sv_string(macrocycle[1].begin(), macrocycle[1].end()),
      sv_string(macrocycle[2].begin(), macrocycle[2].end())
    };
    std::vector<int> ncyc = {
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(0),
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(1),
        input.value__int_vector.at(".phasertng.macsad.ncyc.").value_or_default(2)
      };
    auto method = input.value__choice.at(".phasertng.macsad.minimizer.").value_or_default();
    logProtocol(out::LOGFILE,macro,ncyc,method);

    bool extra_output = Suite::Level() >= out::VERBOSE;
    std::pair<int,int> use_fft = input.value__int_paired.at(".phasertng.substructure.test_fft_versus_summation.").value_or_default();
    sv_double wilson_llg;
    {{
      out::stream where = out::LOGFILE;
      logUnderLine(where,"Null Hypothesis");
      logTab(out::VERBOSE,"Already scaled? " + btos(REFLECTIONS->WILSON_SCALED));
      double ztotalscat = REFLECTIONS->get_ztotalscat();
      double K = ztotalscat*REFLECTIONS->SG.SYMFAC*fn::pow2(REFLECTIONS->WILSON_K_F);
      double KI = (REFLECTIONS->WILSON_SCALED) ? 1 : K;
      REFLECTIONS->setup_absolute_scale(KI);
      logTab(where,"Scale Fs " + dtos(KI));
      logTab(where,"Wilson-B " + dtos(REFLECTIONS->WILSON_B_F));
      RefineSSR refine(
          REFLECTIONS.get(),
          //&selected,
          //&epsilon,
          &scatterers);
      if (!input.value__int_number.at(".phasertng.macsad.integration_steps.").is_default())
        refine.ISTEPS = input.value__int_number.at(".phasertng.macsad.integration_steps.").value_or_default();
      refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
      dag::Node nowork = *DAGDB.WORK;
      nowork.PART.PRESENT = false;
      nowork.FULL.PRESENT = false;
      auto txt = refine.init_node(&nowork,wilson_llg,use_fft,extra_output); //wilson_llg is null
      logTabArray(out::VERBOSE,txt.first);
      if (txt.second.size()) logAdvisory(txt.second);
      dtmin::Minimizer minimizer(this);
      minimizer.run( //null hypothesis
        refine,
        macro,
        ncyc,
        method,
        false //input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default()
        );
      auto llg = refine.likelihood();
      logTab(out::LOGFILE,"LLG for null hypothesis = " + dtos(llg));
      logBlank(out::LOGFILE);
      logTabArray(out::VERBOSE,refine.logSigmaA(true));
      auto tmp = refine.wilsonFn(wilson_llg);
    }}

    Identifier modlid;
    modlid.initialize_from_text(svtos(DAGDB.WORK->logNode())+Cards);//same for same job
    modlid.initialize_tag("sad"+ntos(0,DAGDB.size())); //thrown away
    //take a copy of this as an throw away shift, since NODES will be replaced
    auto tracker = DAGDB.NODES[0].TRACKER;
    tracker.shift_edge(hashing(),input.running_mode); //as for shift_tracker

    double low_rfactor = input.value__percent.at(".phasertng.substructure.completion.rfactor_termination.").value_or_default();
    phaser::data_llgc llgc;
    {{
    llgc.COMPLETE = input.value__boolean.at(".phasertng.substructure.completion.complete.").value_or_default();
    if (input.value__flt_number.at(".phasertng.substructure.completion.separation_distance.").is_default())
    {
      double max_bond_dist(10.0),optical_res(0.715*REFLECTIONS->data_hires());
      double separation_dist =std::min(max_bond_dist,optical_res);
      llgc.CLASH = separation_dist;
    }
    else
    {
      llgc.CLASH = input.value__flt_number.at(".phasertng.substructure.completion.separation_distance.").value_or_default();
    }
    llgc.SIGMA = input.value__flt_number.at(".phasertng.substructure.completion.zscore.").value_or_default();
  //  llgc.TOP = bool(DEF_LLGC_TOP);
    llgc.NCYC = input.value__int_number.at(".phasertng.substructure.completion.ncyc.").value_or_default();
    //llgc.METHOD = std::string(DEF_LLGC_METH);
    llgc.PEAKS_OVER_HOLES = input.value__boolean.at(".phasertng.substructure.completion.peaks_above_deepest_hole.").value_or_default();
    llgc.USE_PEAK1_BELOW = input.value__boolean.at(".phasertng.substructure.completion.top_peak_below_deepest_hole.").value_or_default();
    llgc.PEAK1_BELOW_ZSCORE = input.value__flt_number.at(".phasertng.substructure.completion.top_peak_below_deepest_hole_zscore.").value_or_default();
    llgc.ATOM_CHANGE_ORIG = input.value__boolean.at(".phasertng.substructure.completion.change_scatterers.").value_or_default();
    llgc.USE_BSWAP = input.value__boolean.at(".phasertng.substructure.completion.swap_to_anisotropic_scatterers.").value_or_default();
    //optical resolution 0.715*resolution
    //if the user has entered a list use this
    auto ss = input.value__strings.at(".phasertng.substructure.completion.scatterer.").value_or_default_set();
    for (auto i : ss)
      llgc.ATOMTYPE.insert(tostr2(i).first);
    //if the list was empty then use the strongest scatterer
    if (!llgc.ATOMTYPE.size())
      llgc.ATOMTYPE.insert(scatterers.strongest_anomalous_scatterer().first);
    //but data is native then f" is irrelevant so pick one
    if (REFLECTIONS->check_native()) //only one allowed as f'== occupancy
      llgc.ATOMTYPE = { *llgc.ATOMTYPE.begin() };
    }}
    //use AX for imaginary

    {{
    out::stream where = out::LOGFILE;
    logUnderLine(where,"Substructure Completion");
    if (llgc.COMPLETE)
    {
      logTab(where,"Maximum of " + itos(llgc.NCYC) + " cycles");
      logTab(where,"Zscore of selection = " + dtos(llgc.SIGMA));
      logTab(where,"Use peaks over holes = " + btos(llgc.PEAKS_OVER_HOLES));
      logTab(where,"Use top peaks under holes = " + btos(llgc.USE_PEAK1_BELOW));
      if (llgc.USE_PEAK1_BELOW)
      logTab(where,"Zscore for top peaks under holes = " + dtos(llgc.PEAK1_BELOW_ZSCORE));
      int ntypes = llgc.ATOMTYPE.size();
      phaser_assert(ntypes);
      ntypes == 1 ?
        logTab(where,"Substructure will be completed with 1 atom type"):
        logTab(where,"Substructure will be completed with "+itos(ntypes)+" atom types");
      for (auto ATOMTYPE : llgc.ATOMTYPE)
         logTab(where,"Atom type: "+ ATOMTYPE);
      double separation_dist = llgc.CLASH;
      std::string tab("= ");
      logTab(where,"Holes within");
      logTab(where,tab+dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
      logTab(where,"Peaks within");
      logTab(where,tab+dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
      logTab(where,tab+dtos(separation_dist,2) + "A of a rejected old atom will resurrect the atom");
      logTab(where,"Peaks not within");
      logTab(where,tab+dtos(separation_dist,2) + "A of a higher peak will qualify as new atoms");
      logTab(where,tab+dtos(separation_dist,2) + "A of an old atom will qualify as new atoms");
      logTab(where,"The depth of the biggest hole will " + std::string(llgc.PEAKS_OVER_HOLES?"":"NOT ") + "be used for peak selection");
      logTab(where,"Z/M > " + dtos(llgc.SIGMA,1) + " minimum Z-score");
    }
    else
    {
      logTab(where,"Substructure will NOT be completed");
    }
    logBlank(where);
    }}

    int n(1),N(io_enantiomorphs ? DAGDB.size()*2 : DAGDB.size());
    DAGDB.restart(); //very important to call this to set WORK
    dag::NodeList output_dag;
    enum hand_enum { first_hand, second_refine, second_phase_only };
    af_string output;
    while (!DAGDB.at_end())
    {
      auto  work = *DAGDB.WORK; //deep copy retain between hands for no refinement
      logTabArray(out::TESTING,work.logNode());//same for same job
      sv_int hand_list = { first_hand }; //false
      if (io_enantiomorphs and
          work.PART.ENTRY->SUBSTRUCTURE.number_of_atomtypes() > 1)
      {
        hand_list = { first_hand, second_refine  }; //false, true
      }
      else if (io_enantiomorphs)
      {
        hand_list = { first_hand, second_phase_only }; //false, true but phase only
      }
      for (auto hand : hand_list)
      {
        std::string nN = nNtos(n,N);
        logUnderLine(out::LOGFILE,"SAD Refinement" + nN);
        logTab(out::LOGFILE,hand == first_hand? "First enantiomorph":"Second enantiomorph");
        bool diffsg = REFLECTIONS->change_to_spacegroup_in_same_pointgroup(work.SG);
        if (diffsg)
          selected.flag_standard_outliers_and_select(REFLECTIONS.get());
        logTab(out::LOGFILE,"Space Group: " + work.SG->CCP4);
        if (work.FULL.PRESENT)
          logTab(out::LOGFILE,"Partial Structure: " + work.FULL.ENTRY->COORDINATES.stem());
        auto eSG = REFLECTIONS->SG.enantiomorph();
        if (work.PART.PRESENT and io_hand and hand == first_hand)
        {
          logTab(out::LOGFILE,"Inverted hand");
          auto& substructure = work.PART.ENTRY->SUBSTRUCTURE; //which could be empty
          substructure.change_to_enantiomorph();
        }
        if (work.PART.PRESENT and hand != first_hand)
        {
          auto& substructure = work.PART.ENTRY->SUBSTRUCTURE; //which could be empty
          auto eSG = REFLECTIONS->SG.enantiomorph();
          substructure.change_to_enantiomorph();
          substructure.SG = eSG;
          REFLECTIONS->SG = eSG;
          work.HALL = eSG.HALL;
          work.SG = &eSG; //this is a hack because it should point to dag spacegroup
          logTabArray(out::LOGFILE,substructure.logChangeHand("Changed Hand"));
        }

        //Copy in memory to data_xtal class rather than Reflections type
        //scaling done on data_xtal
//if part is present, then it has not come from mr/full but from ssm, with modlid
//if mr/full is present, then we can use this entry to build part with same id
//either way we change the modlid because we are going to write out new
//this could include changing the partial structure by removing/replacing atoms

        if (hand == second_refine)
          work = *DAGDB.WORK; //deep copy again for refinement
        work.generic_bool = (hand != first_hand); //record hand
        work.generic_int = n; //record original numbering
        work.generic_int2 = 0; //number of cycles
        auto& full = work.FULL; //partial structure from mr
        auto& part = work.PART; //substructure
        PHASER_ASSERT((full.PRESENT and !part.PRESENT) or part.PRESENT);
        if (full.PRESENT and !part.PRESENT) //has not come from ssm, no substructure
        {
          logTab(out::LOGFILE,"Substructure not prepared with ssm, partial structure");
        }
        else if (full.PRESENT and part.PRESENT)
        {
          PHASER_ASSERT(part.IDENTIFIER == full.IDENTIFIER);
        }

        out::stream where = out::LOGFILE;
        if (hand == second_phase_only)
          logChevron(where,"Phasing Cycle");
        else
          logChevron(where,"Initial Refinement Cycle");

        RefineSSR refine(
            REFLECTIONS.get(),
           // &selected,
           // &epsilon,
            &scatterers);
        refine.ANOMALOUS = input.value__boolean.at(".phasertng.macsad.use_partial_anomalous.").value_or_default();
        refine.SPHERICITY.SIGMA = input.value__flt_number.at(".phasertng.macsad.restraint_sigma.sphericity.").value_or_default();
        refine.WILSON.SIGMA = cctbx::adptbx::b_as_u(input.value__flt_number.at(".phasertng.macsad.restraint_sigma.wilson_bfactor.").value_or_default());
        refine.FDP.SIGMA = input.value__flt_number.at(".phasertng.macsad.restraint_sigma.fdp.").value_or_default();
        if (!input.value__int_number.at(".phasertng.macsad.integration_steps.").is_default())
          refine.ISTEPS = input.value__int_number.at(".phasertng.macsad.integration_steps.").value_or_default();
        refine.set_threading(USE_STRICTLY_NTHREADS,NTHREADS);
        logTab(where,"--Initialize--");
        //refine.setFixFdp()
        auto txt = refine.init_node(&work,wilson_llg,use_fft,extra_output); //sigmaa etc
        logTabArray(out::VERBOSE,txt.first);
        if (txt.second.size()) logAdvisory(txt.second);
        logBlank(out::VERBOSE);
        logTabArray(out::LOGFILE,refine.logAtoms());
        if (Suite::Extra())
        {
          out::stream where2 = out::TESTING;
          refine.target(); //must be called before likelihood to set atoms.LLG (returned)
          work.LLG = refine.likelihood();
          work.RFACTOR = refine.Rfactor();
          work.generic_float = refine.overall_fom();
          logTab(where2,"Input Log-Likelihood = " + dtos(work.LLG,0));
          logTab(where2,"Input R-factor = " + dtos(work.RFACTOR,1));
          logTab(where2,"Input Figure-of-Merit = " + dtos(work.generic_float,3));
          logBlank(where2);
        }
        logTab(where,"--Refinement--");
        logTabArray(out::LOGFILE,refine.logProtocolPars(Suite::Extra()));
        dtmin::Minimizer minimizer(this);
        double small_target(0);
        minimizer.run( //first
          refine,
          macro,
          ncyc,
          method,
          input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default(),
          small_target
          );
        if (input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default())
        {
          throw Error(err::DEVELOPER,"Study parameters: break after first refinement");
        }
        logTabArray(out::LOGFILE,refine.logAtoms());
        logTabArray(out::VERBOSE,refine.logScale());
        logTabArray(out::VERBOSE,refine.logScat());
        logTabArray(out::VERBOSE,refine.logSigmaA());
        //logTabArray(out::VERBOSE,refine.logFOM(true));
        logTabArray(out::VERBOSE,refine.logFOM(false));
        logGraph(out::VERBOSE,refine.fom_loggraph());
        work.LLG = refine.likelihood();
        work.RFACTOR = refine.Rfactor();
        work.generic_float = refine.overall_fom();
        logTab(out::LOGFILE,"Initial Log-Likelihood = " + dtos(work.LLG,0));
        logTab(out::LOGFILE,"Initial R-factor = " + dtos(work.RFACTOR,1));
        logTab(out::LOGFILE,"Initial Figure-of-Merit = " + dtos(work.generic_float,3));

        modlid.increment("sad"+ntos(n,N));
        if (input.value__boolean.at(".phasertng.substructure.completion.write_files.").value_or_default())
        {
          DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
          out::stream where = out::LOGFILE;
          logBlank(where);
          logTab(where,"--Generate initial substructure--");
          Atoms substructure;
          substructure.MODLID = modlid;
          substructure.REFLID = REFLECTIONS->reflid();
          substructure.TIMESTAMP = TimeStamp();
          substructure.SG = REFLECTIONS->SG;
          substructure.UC = REFLECTIONS->UC;
          for (int a = 0; a < refine.atoms.size(); a++)
          {
            substructure.XTRA.push_back(refine.atoms[a].XTRA);
            substructure.XRAY.push_back(refine.atoms[a].SCAT);
          }
          std::string f(".sad"+ntos(n,N)+".cycle"+ntos(0,llgc.NCYC));
          substructure.SetFileSystem(DataBase(),dogtag.FileName(f+".pdb"));
          logFileWritten(where,WriteFiles(),"Substructure",substructure);
          if (WriteFiles()) substructure.write_to_disk();
          logTab(where,"--Calculate initial density--");
          ReflColsMap density;
          density.REFLID = modlid;
          density.copy_and_reformat_header(REFLECTIONS.get());
          refine.calcPhsStats();
          logTab(where,"Copy and reformat data");
          for (auto ext : {"fwt","phwt","fom","hla","hlb","hlc","hld"} )
          {
            std::string key = ".phasertng.labin." + std::string(ext) + ".";
            density.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
            input.value__mtzcol.at(key).set_value(input.value__mtzcol.at(key).name());
          }
          for (int r = 0; r < REFLECTIONS->NREFL; r++)
          {
            density.set_flt(labin::FWT,refine.FWT[r],r);
            density.set_flt(labin::PHWT,refine.PHWT[r],r);
            density.set_flt(labin::FOM,refine.FOM[r],r);
            density.set_flt(labin::HLA,refine.HL[r].a(),r);
            density.set_flt(labin::HLB,refine.HL[r].b(),r);
            density.set_flt(labin::HLC,refine.HL[r].c(),r);
            density.set_flt(labin::HLD,refine.HL[r].d(),r);
          }
          density.SetFileSystem(DataBase(),dogtag.FileName(f+".mtz"));
          logFileWritten(where,WriteFiles(),"Density",density);
          if (WriteFiles()) density.write_to_disk();
        }

        if (hand != second_phase_only)
        {
          for (int c = 1; c <= llgc.NCYC and llgc.COMPLETE; c++)
          {
            std::string cC = nNtos(c,llgc.NCYC);
            std::string cC1 = "Substructure Set"+ nN + " Completion Cycle"+ cC;
            out::stream where = out::LOGFILE;
            phaser::LlgcMapSites new_sites;
            logBlank(where);
            logChevron(where,cC1);
            logBlank(where);
            logTab(where,"Number of atoms: "+itos(refine.atoms.size()));
            logTab(where,"Number of Gradient Maps: " + itos(llgc.ATOMTYPE.size()));
            for (auto LLGC_ATOMTYPE : llgc.ATOMTYPE)
            {
              std::string xx=(LLGC_ATOMTYPE=="AX")?"PURE fpp: ":"ELEMENT "+LLGC_ATOMTYPE+": ";
              phaser::LlgcMapSites llgcmapsites;
              af_string output_extra;
              std::tie(llgcmapsites,output,output_extra) = refine.calcGradMaps(LLGC_ATOMTYPE,llgc,cC1,c);
              logTabArray(out::LOGFILE,output);
              logTabArray(out::VERBOSE,output_extra);
              new_sites.merge(llgcmapsites);
            }
            bool atom_change_orig(true);
            phaser::changed_list changed;
            af_string findChangedAtoms;
            if (!REFLECTIONS->check_native()) //no change for native data, no f"
              std::tie(changed,findChangedAtoms) = refine.findChangedAtoms(llgc);
            int nchanged = changed.size();
            std::set<int> deleted_list;
            af_string findDeletedAtoms;
            std::tie(deleted_list,findDeletedAtoms) = refine.findDeletedAtoms(changed);
            int ndeleted = deleted_list.size();
            if (new_sites.nrestored() or new_sites.nanisotropic() or ndeleted or nchanged)
            {
              logBlank(where);
              logTab(where,cC1);
              logBlank(where);
            }
            if (new_sites.nrestored())
            {
              output = refine.restoreAtoms(new_sites.nrestored(),new_sites.restored,c);
              logTabArray(where,output);
            }
            if (new_sites.nanisotropic())
            {
              output = refine.anisoAtoms(new_sites.nanisotropic(),new_sites.anisotropic,c);
              logTabArray(where,output);
            }
            if (ndeleted)
            {
              output = refine.deleteAtoms(ndeleted,deleted_list,c);
              logTabArray(where,findDeletedAtoms);
              logTabArray(where,output);
            }
            if (nchanged)
            {
              output = refine.changeAtoms(changed);
              logTabArray(where,findChangedAtoms);
              logTabArray(where,output);
            }

            logBlank(where);
            logTab(where,cC1);
            if (new_sites.natoms()) //must do this at end because numbering of atoms changes
            {
              phaser::SpaceGroup SG(REFLECTIONS->SG.hall());
              phaser::UnitCell UC(REFLECTIONS->UC.cctbxUC.parameters());
              auto selectTopSites = new_sites.selectTopSites(SG,UC,llgc,refine.numAtoms());
              output = refine.addAtoms(new_sites.atoms,extra_output);
              logTabArray(where,selectTopSites);
              logTabArray(where,output);
              //atoms+extra added to base HandEP af_atom
            }
            else
            {
              logBlank(where);
              logTab(where,"No New Atoms");
            }
            bool atoms_not_complete = (new_sites.nchanged() or ndeleted or nchanged);

            work.generic_int2 = c; //number of cycles

            logBlank(where);
            if (atoms_not_complete)
            {
              logChevron(where,"Substructure completion has NOT converged");
            }
            else
            {
              logChevron(where,"Substructure");
              logTabArray(where,refine.logAtoms());
              logTabArray(where,refine.logScale());
              logTabArray(where,refine.logScat());
              logTabArray(out::VERBOSE,refine.logSigmaA());
              logTabArray(out::VERBOSE,refine.logFOM(true));
              logTabArray(out::LOGFILE,refine.logFOM(false));
              logGraph(where,refine.fom_loggraph());
              work.LLG = refine.likelihood();
              work.RFACTOR = refine.Rfactor();
              work.generic_float = refine.overall_fom();
              refine.target(); //must be called before likelihood to set atoms.LLG (returned)
              logTab(where,"Current Log-Likelihood = " + dtos(work.LLG,0));
              logTab(where,"Current R-factor = " + dtos(work.RFACTOR,1));
              logTab(where,"Current Figure-of-Merit = " + dtos(work.generic_float,3));
              logTab(where,"--");
              logBlank(where);
              logChevron(where,"Substructure completion has converged");
              logBlank(where);
              break;
            }
            logBlank(where);

            //atoms have been added and other parameters the same
            logBlank(where);
            logTab(where,cC1);
            logBlank(where);
            logTab(where,"--Refinement--");
            logTabArray(out::LOGFILE,refine.logProtocolPars(Suite::Extra()));
            dtmin::Minimizer minimizer(this);
            logBlank(where);
            double small_target(0);
            minimizer.run( //completion
              refine,
              macro,
              ncyc,
              method,
              input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default(),
              small_target
              );
            if (input.value__boolean.at(".phasertng.macsad.study_parameters.").value_or_default())
              return;
            logTabArray(out::VERBOSE,refine.logAtoms());
            logTabArray(out::VERBOSE,refine.logScale());
            logTabArray(out::VERBOSE,refine.logScat());
            logTabArray(out::VERBOSE,refine.logSigmaA());
            logTabArray(out::VERBOSE,refine.logFOM(true));
            logTabArray(out::LOGFILE,refine.logFOM(false));
            logGraph(out::VERBOSE,refine.fom_loggraph());

            logTab(where,"Generate substructure for cycle");
            Atoms  substructure;
            substructure.MODLID = modlid;
            substructure.REFLID = REFLECTIONS->reflid();
            substructure.TIMESTAMP = TimeStamp();
            substructure.SG = REFLECTIONS->SG;
            substructure.UC = REFLECTIONS->UC;
            for (int a = 0; a < refine.atoms.size(); a++)
            {
              auto& atom = refine.atoms[a];
              substructure.XTRA.push_back(atom.XTRA);
              substructure.XRAY.push_back(atom.SCAT);
            }

            logTab(where,"Generate density for cycle");
            ReflColsMap density;
            density.copy_and_reformat_header(REFLECTIONS.get());
            refine.calcPhsStats();
            logTab(out::VERBOSE,"Copy and reformat data");
            for (auto ext : {"fwt","phwt","fom","hla","hlb","hlc","hld"} )
            {
              std::string key = ".phasertng.labin." + std::string(ext) + ".";
              density.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
              input.value__mtzcol.at(key).set_value(input.value__mtzcol.at(key).name());
            }
            for (int r = 0; r < REFLECTIONS->NREFL; r++)
            {
              density.set_flt(labin::FWT,refine.FWT[r],r);
              density.set_flt(labin::PHWT,refine.PHWT[r],r);
              density.set_flt(labin::FOM,refine.FOM[r],r);
              density.set_flt(labin::HLA,refine.HL[r].a(),r);
              density.set_flt(labin::HLB,refine.HL[r].b(),r);
              density.set_flt(labin::HLC,refine.HL[r].c(),r);
              density.set_flt(labin::HLD,refine.HL[r].d(),r);
            }

            if (input.value__boolean.at(".phasertng.substructure.completion.write_files.").value_or_default())
            {
              out::stream where = out::LOGFILE;
              logBlank(where);
              logTab(where,cC1);
              logBlank(where);
              Identifier modlid =  work.TRACKER.PENULTIMATE;
              modlid.initialize_tag("_sad"+ntos(n,N)+"_cycle"+ntos(c,llgc.NCYC));
              DogTag dogtag(DAGDB.PATHWAY.ULTIMATE,modlid);
              substructure.SetFileSystem(DataBase(),dogtag.FileName(".pdb"));
              logFileWritten(where,WriteFiles(),"Substructure",substructure);
              if (WriteFiles()) substructure.write_to_disk();
              logBlank(where);
              density.SetFileSystem(DataBase(),dogtag.FileName(".mtz"));
              logFileWritten(where,WriteFiles(),"Density",density);
              if (WriteFiles()) density.write_to_disk();
            }

            work.LLG = refine.likelihood();
            work.RFACTOR = refine.Rfactor();
            work.generic_float = refine.overall_fom();
            logTab(out::LOGFILE,"Current Log-Likelihood = " + dtos(work.LLG,0));
            logTab(out::LOGFILE,"Current R-factor = " + dtos(work.RFACTOR,1));
            logTab(out::LOGFILE,"Current Figure-of-Merit = " + dtos(work.generic_float,3));

            if (work.RFACTOR < low_rfactor and
                refine.COORDINATES == nullptr) //not mrsad
            {
              logBlank(out::SUMMARY);
              logTab(out::SUMMARY,"LLG completion terminated because R-factor less than " + dtos(low_rfactor,4,1));
              break; //out of ncyc loop
            }
          }
        }

        if (false)
        {
        int number_of_peaks(20);
        double skew20;
        std::tie(skew20,output) = refine.phase_o_phrenia(number_of_peaks);
        work.generic_float2 = skew20;
        logTabArray(out::LOGFILE,output);
        }

        if (refine.COORDINATES != nullptr)
        {{
        out::stream where = out::LOGFILE;
        int nAtoms = refine.COORDINATES->PDB.size();
        af_string sorted; //so they are in substructure atom number order
        for (int a = 0; a < nAtoms; a++)
        {
          int s = refine.COORDINATES->PDB[a].generic_int;
          if (s)
            sorted.push_back(ntos(s,nAtoms) + "=" + refine.COORDINATES->PDB[a].Card());
        }
        std::sort(sorted.begin(),sorted.end());
        if (sorted.size())
        {
          logChevron(where,"Sites on Partial Structure"+nN);
          logTabArray(where,sorted);
        }
        logBlank(where);
        }}

        {{
        out::stream where = out::LOGFILE;
        logChevron(where,"Refinement Statistics"+nN);
        (refine.atoms.size() == 1) ?
          logTab(where,"There is 1 atom"):
          logTab(where,"There are " + itos(refine.atoms.size()) + " atoms");
        work.LLG = refine.likelihood();
        work.RFACTOR = refine.Rfactor();
        work.generic_float = refine.overall_fom();
        logTab(where,"Log-Likelihood = " + dtos(work.LLG,0));
        logTab(where,"R-factor = " + dtos(work.RFACTOR,1));
        logTab(where,"Figure-of-Merit = " + dtos(work.generic_float,3));
       // logTab(where,"Phase-o-Phrenia skew = " + dtos(work.generic_float2,3));
        logBlank(where);
        }}


        {{
        //we already have a modlid, used above for the cycle output
        //instead of using tracker to give modlid, use modlid to fix the tracker
        out::stream where = out::LOGFILE;
        auto& ultimate = tracker.ULTIMATE;
        ultimate.increment("sad"+ntos(n,N));
        //The logic here is that we make a new entry with the current node information
        //so that the map files and coordinates form a new database entry on their own
        work.TRACKER.PENULTIMATE = work.TRACKER.ULTIMATE; //modify tag here
        work.TRACKER.ULTIMATE = ultimate; //shift tracker
        //this will be substructure and density
        work.PART.ENTRY = DAGDB.add_entry(ultimate);
        auto dogtag = work.PART.ENTRY->DOGTAG;
        work.PART.PRESENT = true;
        work.PART.IDENTIFIER = ultimate;
        {{ //substructure
        auto& substructure = work.PART.ENTRY->SUBSTRUCTURE; //alias
        substructure.MODLID = ultimate;
        substructure.REFLID = REFLECTIONS->reflid();
        substructure.TIMESTAMP = TimeStamp();
        substructure.SG = REFLECTIONS->SG;
        substructure.UC = REFLECTIONS->UC;
        for (int a = 0; a < refine.atoms.size(); a++)
        {
          substructure.XTRA.push_back(refine.atoms[a].XTRA);
          substructure.XRAY.push_back(refine.atoms[a].SCAT);
        }
        substructure.SetFileSystem(DataBase(),dogtag.FileEntry(entry::SUBSTRUCTURE_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Substructure",substructure);
        if (WriteFiles()) substructure.write_to_disk();
        }}
        if (refine.COORDINATES != nullptr)
        { //coordinates of partial structure (mrsad)
        auto& coordinates = refine.COORDINATES; //alias
        coordinates->MODLID = ultimate;
        coordinates->REFLID = REFLECTIONS->reflid();
        coordinates->TIMESTAMP = TimeStamp();
        coordinates->SetFileSystem(DataBase(),dogtag.FileEntry(entry::COORDINATES_PDB));
        logFileWritten(out::LOGFILE,WriteFiles(),"Coordinates",*coordinates);
        if (WriteFiles()) coordinates->write_to_disk();
        }
        {{ //density
        auto& density = work.PART.ENTRY->DENSITY;
        density.copy_and_reformat_header(REFLECTIONS.get());
        refine.calcPhsStats();
        std::set<std::string> labin = {"fwt","phwt","fom","hla","hlb","hlc","hld"};
        (REFLECTIONS->INTENSITIES) ? labin.insert("inat"):labin.insert("fnat");
        (REFLECTIONS->INTENSITIES) ? labin.insert("siginat"):labin.insert("sigfnat");
        for (auto ext : labin )
        {
          std::string key = ".phasertng.labin." + std::string(ext) + ".";
          density.setup_mtzcol(input.value__mtzcol.at(key).value_or_default());
          input.value__mtzcol.at(key).set_value(input.value__mtzcol.at(key).name());
        }
        for (int r = 0; r < REFLECTIONS->NREFL; r++)
        {
          density.set_flt(labin::FWT,refine.FWT[r],r);
          density.set_flt(labin::PHWT,refine.PHWT[r],r);
          density.set_flt(labin::FOM,refine.FOM[r],r);
          density.set_flt(labin::HLA,refine.HL[r].a(),r);
          density.set_flt(labin::HLB,refine.HL[r].b(),r);
          density.set_flt(labin::HLC,refine.HL[r].c(),r);
          density.set_flt(labin::HLD,refine.HL[r].d(),r);
          if (REFLECTIONS->INTENSITIES)
          {
            density.set_flt(labin::INAT,REFLECTIONS->get_flt(labin::INAT,r),r);
            density.set_flt(labin::SIGINAT,REFLECTIONS->get_flt(labin::SIGINAT,r),r);
          }
          else
          {
            density.set_flt(labin::FNAT,REFLECTIONS->get_flt(labin::FNAT,r),r);
            density.set_flt(labin::SIGFNAT,REFLECTIONS->get_flt(labin::SIGFNAT,r),r);
          }
        }
        density.SetFileSystem(DataBase(),dogtag.FileEntry(entry::DENSITY_MTZ));
        logFileWritten(out::LOGFILE,WriteFiles(),"Density",density);
        if (WriteFiles()) density.write_to_disk();
        }}
        }}
        output_dag.push_back(work); //which is a copy
        n++;
      }
      if (work.RFACTOR < low_rfactor)
        break;
    }

    DAGDB.NODES = output_dag;
    DAGDB.reset_entry_pointers(DataBase()); //includes enantiomer space group creation
    DAGDB.restart();

    {{
    out::stream where = out::LOGFILE;
    DAGDB.NODES.apply_sort_llg();
    logUnderLine(where,"Final Merge");
    logEllipsisOpen(where,"Flagging duplicates");
    //move the atoms onto the poses for the analysis of duplicates
    for (int i = 0; i < DAGDB.size(); i++)
    {
      dag::Node& work = DAGDB.NODES[i];
      auto& atoms = work.PART.ENTRY->SUBSTRUCTURE; //alias
      work.POSE.clear(); //we want to start again and add all the atoms
      for (auto& atom : atoms.XRAY)
      {
        dag::Pose newpose;
        newpose.FRACT = REFLECTIONS->UC.fractionalization_matrix()*atom.site;
        //newpose.ENTRY = nullptr; //not required
        //newpose.IDENTIFIER = search->identify();
        work.POSE.push_back(newpose);
      }
    }
    int dupl(0); bool centrosymmetry(false);
    if (DAGDB.NODES.front().POSE.size())
      dupl = DAGDB.calculate_duplicate_atoms(sticky_hires,centrosymmetry,USE_STRICTLY_NTHREADS,NTHREADS);
    logEllipsisShut(where);
    (dupl == 1) ?
      logTab(where,"Duplicates: There is " + itos(dupl) + " out of " + itos(DAGDB.size())):
      logTab(where,"Duplicates: There are " + itos(dupl) + " out of " + itos(DAGDB.size()));
    if (false)
    {
      logEllipsisOpen(where,"Purge duplicates");
      int before = DAGDB.size();
      DAGDB.NODES.erase_invalid(); //now can erase after peakpick
      logEllipsisShut(where);
      logTab(where,"Number until purge: " + itos(before));
      logTab(where,"Number after purge: " + itos(DAGDB.size()));
      logBlank(where);
    }
    for (int i = 0; i < DAGDB.size(); i++)
    {
      dag::Node& work = DAGDB.NODES[i];
      work.POSE.clear(); //don't keep these after duplicate calculation
    }
    }}

    for (bool sorted : sv_bool({ true }))
    {
      out::stream where = out::SUMMARY;
      std::string type = sorted ? "" : " (Unsorted)";
      if (sorted) DAGDB.NODES.apply_sort_llg();
      logUnderLine(where,"SAD Refinement Table Summary" + type);
      logTab(where,"E: [o,*]    Enantiomer with first or inverted hand");
      logTab(where,snprintftos("%-4s %-4s %c  %-13s %14s %5s %5s %8s %5s %6s  %s",
        "#","==",'E',"SpaceGroup","Log-Likelihood","atoms","occsq","R-factor","FOM","Cycles","Composition"));
      for (auto& node : DAGDB.NODES)
      {
        int natoms(0);
        if (node.PART.PRESENT)
        {
          auto& atoms = node.PART.ENTRY->SUBSTRUCTURE; //alias
          natoms = atoms.size();
          //generate the generic_str for the table
          std::map<std::string,int> comp;
          for (int a = 0; a < atoms.size(); a++)
            comp[atoms.XRAY[a].scattering_type] = 0;
          for (int a = 0; a < atoms.size(); a++)
            comp[atoms.XRAY[a].scattering_type]++;
          node.generic_str = " ";
          for (auto tmp : comp)
            node.generic_str += tmp.first + "[" + itos(tmp.second) + "] ";
          double sumocc2(0);
          for (int a = 0; a < atoms.size(); a++)
            sumocc2 += fn::pow2(atoms.XRAY[a].occupancy);
          node.generic_float2 = sumocc2;
        }
        SpaceGroup sg(node.SG->HALL);
        logTab(where,snprintftos("%-4d %-4s %c  %-13s %14.1f %5d %5.1f %8s %5.3f %6d  %-s",
          node.generic_int,
          (node.EQUIV >= 0) ? itos(node.EQUIV+1).c_str():"-", //-999 flag for none
          node.generic_bool ? '*':'o',
          node.SG->CCP4.c_str(),
          node.LLG,
          natoms, //Number of atoms in substructure
          node.generic_float2, //sum occ sqr
          (node.RFACTOR > 50) ? ">50.0": dtos(node.RFACTOR,1).c_str(),
          node.generic_float, //FOM
          node.generic_int2, //number of cycles
          node.generic_str.c_str()));
      }
      logTab(where,"-------");
      logBlank(where);
    }

    if (DAGDB.size() == 1 and DAGDB.NODES[0].PART.PRESENT and DAGDB.NODES[0].PART.ENTRY->SUBSTRUCTURE.size() == 0)
    {
      logWarning("No substructure atoms");
    }
  }
} //phasertng
