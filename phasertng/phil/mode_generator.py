
def isNone(param):
  param = param.strip(" ")
  param = param.strip("\"")
  return None if param == 'None' else param

class mode_generator():
  def __init__(self):
    self.boring = ['chk','test']
    self.the_default = 'chk'
    self.mode_list = [] #ordered list
    #True/False for cpp (versus python) coded mode
    self.mode_list.append(["chk","Initialize","Initialize a script",True]) #& check, but no file writing/dag clutter except xml
    self.mode_list.append(["bub","Biological_unit_builder","Builds combinations of models that fill the biological unit",False])
    self.mode_list.append(["walk","Walk directory","Finds files in directory",False])
    self.mode_list.append(["tree","Tree analysis of directories","Generates pyvis html file and analysis of the dag directory structure",False])
    self.mode_list.append(["ipdb","Pdb_file_reader","Read a pdb file, calculate structure factors, and treat them as data",True])
    self.mode_list.append(["imtz","Mtz_file_reader","Read a mtz file for unphased or phased data",True])
    self.mode_list.append(["imap","Electron_microscopy_data_file_reader","Read a map for phased data",True])
    self.mode_list.append(["data","Data_preparation","Prepare the data to fullfill the requirements for subsequent modes",True])
    self.mode_list.append(["sga","Space_group_analysis","Find subgroups of the input space group",True])
    self.mode_list.append(["sgx","Space_group_expansion","Expand the data to a lower symmetry space group",True])
    self.mode_list.append(["aniso","Anisotropy_correction","Correct the data for anisotropy",True])
    self.mode_list.append(["tncso","tNCS_commensurate_modulation_analysis","Find noncrystallographic symmetry from the data",True])
    self.mode_list.append(["tncs","tNCS_correction_refinement","Correct the data for translational non-crystallographic symmetry",True])
    self.mode_list.append(["feff","Effective_amplitudes","Calculate the error-corrected amplitudes and weights for the llgi target function",True])
    self.mode_list.append(["twin","Twin_test","Analyse the data for twinning after correcting for anisotropy and tncs",True])
    self.mode_list.append(["srf","Self_rotation_function","Calculate self rotation function in chi sections",True])
    self.mode_list.append(["srfp","Self_rotation_function_plot","Plot self rotation function in chi sections",False])
    self.mode_list.append(["deep","Deep_search","Split database for deep search",False])
    self.mode_list.append(["join","Join_nodes","Join database nodes for search strategies",False])
    self.mode_list.append(["pool","Pool_nodes","Pool results from changeling rotation functions",False])
    self.mode_list.append(["hyss","Hyss_substructure","Phenix hyss for substructure",False])
    self.mode_list.append(["sscc","Get_cc_mtz_pdb","Substructure CC between mtz and pdb",False])
    self.mode_list.append(["find","Find_components","Split database to find components",False])
    self.mode_list.append(["cca","Cell_content_analysis","Matthews cell content analysis for number of copies of the biological unit in the asymmetric unit",True])
    self.mode_list.append(["ccs","Cell_content_scaling","Scale the data according to expectation for cell content",True])
    self.mode_list.append(["esm","Ensemble_single_model","Calculate data from a model that is not an ensemble structure",True])
    self.mode_list.append(["emm","Ensemble_multi_model","Calculate data from a model that is an ensemble structure",True])
    self.mode_list.append(["msm","Map_single_model","Calculate data from a phased data including cutting out density",True])
    self.mode_list.append(["rellg","Relative_expected_llg_for_model","Relative expected llg for a model",True])
    self.mode_list.append(["perm","Permutation_for_search","Permute the searches by the expected llg",True])
    self.mode_list.append(["ellg","Expected_llg_for_search","Calculate the expected llg for a set of models",True])
    self.mode_list.append(["etfz","Expected_tfz_for_search","Determine the expected tfz for the next placement",True])
    self.mode_list.append(["eatm","Expected_llg_for_atom","Expected llg for a single atom of a given element and with varying B-factors",True])
    self.mode_list.append(["emap","Expected_llg_for_map","Expected llg for phased data",True])
    self.mode_list.append(["frf","Fast_rotation_function","Likelihood enhanced fast rotation function including information from previously placed components of the structure",True])
    self.mode_list.append(["frfr","Fast_rotation_function_rescore","Full likelihood rescoring of fast rotation function peaks information from previously placed components of the structure",True])
    self.mode_list.append(["brf","Brute_rotation_function","Full likelihood function calculated on a grid of angles",True])
    self.mode_list.append(["gyre","Gyre_refinement","Rotational refinement of orientations including information from previously placed components",True])
    self.mode_list.append(["ftf","Fast_translation_function","Likelihood enhanced fast translation function including information from previously placed components of the structure",True])
    self.mode_list.append(["ftfr","Fast_translation_function_rescore","Full likelihood rescoring of fast translation function peaks information from previously placed components of the structure",True])
    self.mode_list.append(["btf","Brute_translation_function","Full likelihood function calculated on a grid of positions",True])
    self.mode_list.append(["ptf","Phased_translation_function","Phased translation function",True])
    self.mode_list.append(["pose","Pose_scoring","Convert nodes from translation function results to nodes with posed ensembles, including application of translational non-crystallographic symmetry",True])
    self.mode_list.append(["put","Put_without_search","Force the placement of a component without searching",True])
    self.mode_list.append(["pak","Packing","Calculate overlaps between posed components of the asymmetric unit",True])
    self.mode_list.append(["move","Moving","Optimize position of components within the asymmetric unit",True])
    self.mode_list.append(["span","Span_space","Determine if the poses connect the lattice in three dimensions",True])
    self.mode_list.append(["exp1","Expand_to_p1","Expand data and poses to space group P1",True])
    self.mode_list.append(["rbr","Rigid_body_refinement","Refine the orientation and position of posed components",True])
    self.mode_list.append(["rbgs","Rigid_body_grid_search","Refine the position of a posed component with a grid search, for very wide radius convergence refinement",True])
    self.mode_list.append(["tfz","Translation_function_zscore_equivalent","Calculate the translation function z-score that would have been obtained had the current refined pose been found in a translation function",True])
    self.mode_list.append(["mapz","Map_docking_zscore","Map docking z-score",True])
    self.mode_list.append(["fuse","Fuse_solutions","Merge high tfz scoring solutions into a single pose, short-circuiting sequential placement",True])
    self.mode_list.append(["spr","Segmented_pruning_refinement","Likelihood-based segmentation and refinement of occupancy and B-factor of segments, without overfitting",True])
    self.mode_list.append(["rmr","Rigid_map_refinement","Refine orientation and position of ensemble in phased data",True])
    self.mode_list.append(["rbm","Rigid_body_maps_for_intensities","Generate coordinates and maps for solutions",True])
    self.mode_list.append(["mcc","Map_correlation_coefficient","Calculate the correlation coefficient between solutions and prune those with no phase difference",True])
    self.mode_list.append(["jog","Jog_by_map_docking","Optimize the placement of poses founded when translational non-crystallographic symmetry is present, to remove cumulative errors arising from small errors in the vector",True])
    self.mode_list.append(["info","Information_content","Calculate the information content of the data",True])
    self.mode_list.append(["ssa","Substructure_content_analysis","Analyse the anomalous signal in the data",True])
    self.mode_list.append(["ssd","Substructure_determination","Find substructure sites",True])
    self.mode_list.append(["ssm","Substructure_model_preparation","Prepare input pdb substructure for use in sad phasing",True])
    self.mode_list.append(["ssp","Substructure_Patterson_analysis","Pick peaks in an likelihood anomalous Patterson",True])
    self.mode_list.append(["ssr","Substructure_refinement","Refine substructure sites",True])
    self.mode_list.append(["rfac","R-factor_calculation","Calculate the R-factor incuding bulk solvent terms",False])
    self.mode_list.append(["beam","Isostructure_search","Find isomorphous structures in the Protein Data Bank",False])
    self.mode_list.append(["fetch","Fetch_pdb","Fetch pdb and load into memory",False])
    self.mode_list.append(["xref","Xray coordinate refinement","Use phenix or refmac to refine coordinates of solutions",False])
    self.mode_list.append(["test","Test","Sandbox",True])

  def possible_modes(self):
    result = []
    for key in self.mode_list:
      if not (key[0] in self.boring):
        result.append(key[0])
    return result

  def descriptor(self,m):
    for key,val,txt,b in self.mode_list:
      if (key == m) : return val + ": " + txt
    return ""

  def unquoted_string(self):
    result = ""
    for key in self.mode_list:
      result += str(key[0]) + " "
    return result #with space at end

  def quoted_list(self):
    result = ""
    for key in self.mode_list:
      result += "\"" + str(key[0]) + "\","
    return result[:-1]

  def type_attr(self):
    result = ""
    for i,key in enumerate(self.mode_list):
      result += key[0] + "=" + str(i) + ","
    result = result[:-1]
    return result

  def style_attr(self):
    r = self.possible_modes()
    result = "tng:input:"
    for key in sorted(r):
      result += "+"+key
    return result

  def caption_attr(self):
    result = ""
    trailer = " \\\n               "
    for key in self.mode_list:
      result += key[1] + trailer
    result = result[:-len(trailer)]
    return result

  def mode_descriptors(self):
    result = {}
    for key in sorted(self.mode_list):
      result[key[0]] = key[1].replace("_"," ")
    return result

  def python_modes(self):
    result = []
    for key in sorted(self.mode_list):
      if (not key[3]): #python, append each mode as a mode_list ie.[mode]
        result.append([str(key[0])])
    return result

  def implemented(self):
    return '''
  mode = ''' + self.the_default + '''
    .style = ''' + self.style_attr() + '''
    .type = choices(''' +  self.type_attr() + ''')
    .expert_level = 4
    .short_caption = Phasertng mode
    .help = Call phasertng with functionality specified by the choice(s) in order

'''
  # .caption = ''' +  self.caption_attr()
   #no comma at the end of the string
