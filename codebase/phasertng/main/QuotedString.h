//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_quoted_string_class__
#define __phasertng_quoted_string_class__
#include <sstream>

namespace phasertng {

class QuotedString
{
  public:
    std::string string_value = "";
    bool error = false;
    bool blank = false;
    bool None = false;

  public:
  QuotedString(std::istringstream& input_stream)
  {
    //forces the string to be quoted, not allowing unquoted
    //allows =,#,! in filenames - ALSO escaped "
    //skip leading spaces (there must be leading spaces)
    bool quoted_string(false);
    bool N(false);
    std::string filename;
    for (;;)
    {
      char ch;
      if (input_stream.get(ch))
      {
        if (ch == '\n')
        {
          input_stream.putback(ch); //so that Token::ENDLINE token is found
          this->blank = true;
          break;
        }
        else if ((isspace)(ch))
        {
        }
        else if (ch == '\"')
        {
          quoted_string = true;
          break;
        }
        else if (ch == 'N')
        {
          filename += ch; //unquoted None
          N = true;
          break;
        }
      }
      else
      {
        input_stream.putback(ch); //so that Token::END can be found
        this->blank = true;
        break;
      }
    }
    if (!quoted_string and !N)
    {
      this->error = true;
      string_value = "";
      return;
    }
    bool last_was_quote_char = false;
    for (;;)
    {
      char ch;
      if (input_stream.get(ch))
      {
        if (ch == '\"' && !last_was_quote_char)
        {
          string_value = filename;
          this->error = false;
          return;
        }
        else if (ch == '\n')
        {
          input_stream.putback(ch); //so that Token::END can be found
          string_value = filename;
          this->error = false;
          return;
        }
        else if (ch == '\"' && last_was_quote_char)
        {
          if (filename.size()) filename.pop_back(); //remove escape char
          filename += ch; //add quote to string
          last_was_quote_char = false;
        }
        else if (ch == '\\' )
        {
          filename += ch;
          last_was_quote_char = true;
        }
        else if (N)
        {
          filename += ch;
          last_was_quote_char = false;
          if (filename == "None") break; //unquoted None
        }
        else
        {
          filename += ch;
          last_was_quote_char = false;
        }
      }
      else
      {
        input_stream.putback(ch); //so that Token::END can be found
        string_value = filename;
        this->error = false;
        return;
      }
    }
    string_value = filename;
    this->None = (string_value == "\"None\"" or string_value == "None");
    this->error = false;
  }

    virtual ~QuotedString() { }
};

}//phasertng

#endif
