// Function to check if a file has the given extension (suffix)
bool hasSuffix(const std::string& filename, const std::string& suffix) {
    if (filename.size() < suffix.size()) {
        return false;
    }
    return std::equal(suffix.rbegin(), suffix.rend(), filename.rbegin());
}


#ifdef _WIN32
// Windows-specific code to list files in a directory
std::string findFileWithSuffix(const std::string& dirPath, const std::string& suffix) {
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = FindFirstFile((dirPath + "\\*").c_str(), &findFileData);
    if (hFind == INVALID_HANDLE_VALUE) {
        std::cerr << "Error opening directory: " << GetLastError() << std::endl;
        return "";
    }
    do {
        if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            // Skip directories like "." and ".."
            continue;
        }
        std::string filename = findFileData.cFileName;
        if (hasSuffix(filename, suffix)) {
            FindClose(hFind);
            return filename; // Return the first matching file
        }
    } while (FindNextFile(hFind, &findFileData) != 0);
    FindClose(hFind);
    return ""; // No matching file found
}
#else
// POSIX (Linux/macOS) specific code to list files in a directory
std::string findFileWithSuffix(const std::string& dirPath, const std::string& suffix) {
    DIR* dir = opendir(dirPath.c_str());
    if (dir == nullptr) {
        std::cerr << "Error opening directory: " << strerror(errno) << std::endl;
        return "";
    }
    struct dirent* entry;
    while ((entry = readdir(dir)) != nullptr) {
        // Skip "." and ".."
        if (std::strcmp(entry->d_name, ".") == 0 || std::strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        std::string filename = entry->d_name;
        if (hasSuffix(filename, suffix)) {
            closedir(dir);
            return filename; // Return the first matching file
        }
    }
    closedir(dir);
    return ""; // No matching file found
}
#endif
