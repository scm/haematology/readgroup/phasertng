//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/type/spacegroup.h>
#include <phasertng/src/SpaceGroup.h>
#include <phasertng/main/Error.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <phasertng/main/hoist.h>
#include <cctype> //Windows toupper

namespace phasertng {
namespace type {

  spacegroup::spacegroup(const std::string parameter,
             const std::string dephalt_
        ) :
    optional(parameter),
    dephalt(dephalt_)
  {
    if (uvalue.size())
    {
      SpaceGroup avoid_clang_compiler_warning;
      try { avoid_clang_compiler_warning = SpaceGroup(uvalue); }
      catch(...) { throw Error(err::INPUT,"Space group symbol not recognised"); }
    }
  }

  void
  spacegroup::set_value(std::string other)
  {
    user = true;
    if (!other.size())
    { //None() reset the keyword as the default
      uvalue = "";
      user = false;
      return;
    }
    if (other.size())
    {
      SpaceGroup avoid_clang_compiler_warning;
      try { avoid_clang_compiler_warning = SpaceGroup(other); }
      catch(...) { throw Error(err::INPUT,"Space group symbol not recognised"); }
      type::spacegroup::set_value_string(other);
    }
  }

  void
  spacegroup::set_value(const sv_string& vcards,const std::string& other)
  {
    paranoia_check(vcards);
    type::spacegroup::set_value_string(other);
  }

  void
  spacegroup::set_value_string(std::string other)
  {
    std::string unquoted = other;
    while (unquoted.size() && unquoted[0] == '"')
    {
      std::string tmp = "";
      for (int i = 1; i < unquoted.size()-1 && unquoted.size() >= 2; i++)
        tmp += std::string(1,unquoted[i]);
      unquoted = tmp;
    }
    //phaser_assert(!unquoted.size() || unquoted[0] != '"');
    uvalue = unquoted;
  }

  //virtual
  bool
  spacegroup::is_default() const
  { return (uvalue == dephalt or uvalue == None()); }

  void
  spacegroup::set_default()
  {
    user = false;
    uvalue = dephalt;
  }

  std::string
  spacegroup::to_python()
  {
    std::string v = value_or_default();
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    //stored unquoted
    hoist::replace_all(vv,"\"","\\\"");
    return vv; //unquoted
  }

  void
  spacegroup::from_python(std::string str)
  { set_value(str); }

  std::string
  spacegroup::value_or_default() const
  { return user ? uvalue : dephalt; }

  std::string
  spacegroup::value_or_default_upper() const
  {
    std::string s = value_or_default();
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return toupper(c); });
    return s;
  }

  std::string
  spacegroup::value() const
  {
    //if (is_default()) phaser_assert(value_is_set_on_input);
    return uvalue;
  }

  std::string
  spacegroup::unparse_parameter(bool phil) const
  {
    std::string v = value_or_default();
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    //stored unquoted
    hoist::replace_all(vv,"\"","\\\"");
    std::string str = "\"" + vv + "\"";
    return str;
  }

  std::string
  spacegroup::show_default(bool phil) const
  {
    std::string v = dephalt;
    if (!v.size())
    {
      v = None();
      return v; //no quotes
    }
    std::string vv = v;
    hoist::replace_all(vv,"\"","\\\"");
    return "\"" + vv + "\"";
  }
}}
