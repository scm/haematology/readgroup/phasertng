//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_int_numbers_class__
#define __phasertng_type_int_numbers_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

typedef std::vector<int> sv_int;

class int_numbers : public optional
{
  sv_int dephalt,uvalue;
  bool pminset = false;
  bool pmaxset = false;
  int  pminval = 0;
  int  pmaxval = 0;

  public:
  const bool& minset() const { return pminset; }
  const int&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const int&  maxval() const { return pmaxval; }

  int_numbers() : optional() {}
  int_numbers(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const sv_int dephalt_ = {}
  );

  void set_value(sv_int other);
  void set_value(const sv_string& vcards,const sv_int& other);
  bool is_default() const;
  void set_default();
  sv_int value_or_default() const;
  int size() const;
  void clear();
  void push_back(const int& other);
  void pop_back();
  sv_int value_or_default_unique() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::int_numbers& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
