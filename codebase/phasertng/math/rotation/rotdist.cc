#include <phasertng/math/rotation/rotdist.h>
#include <scitbx/math/r3_rotation.h>

namespace phasertng {

  std::pair<dvect3,double>
  axis_and_angle_deg(const dvect3& euler,const double angle_limit)
  {
    const dmat33 MROT = zyz_matrix(euler);
    return axis_and_angle_deg(MROT,angle_limit);
  }

  std::pair<dvect3,double>
  axis_and_angle_deg(const dmat33& MROT,const double angle_limit)
  {
    dvect3 AXIS; double ANGLE;
    scitbx::math::r3_rotation::axis_and_angle_from_matrix<double> decompose(MROT);
    ANGLE = scitbx::rad_as_deg(decompose.angle_rad);
    AXIS = decompose.axis;
    //if the angle is < limit, for effective identity rotation
    if (std::fabs(ANGLE) < angle_limit)
    {
      ANGLE = 0; //because the axis is not real
      AXIS = dvect3(0,0,1); //because axis doesn't matter
    }
    if (ANGLE < 0)
    {
      ANGLE = -ANGLE; //report positive rotations
      AXIS = -AXIS;
    }
    return { AXIS,ANGLE };
  }

  double
  rotation_angle_cosine(const dmat33& rotation)
  {
    return std::max(
        std::min( ( ( rotation.trace() - 1.0 ) / 2.0 ), 1.0 ),
        -1.0
    );
  }

  double
  regularize_angle(const double& angle)
  {
    return fmod( 360 + fmod( angle, 360 ), 360 );
  }

  dvect3
  regularize_angle(const dvect3& angle)
  {
    dvect3 res =
           { fmod( 360 + fmod( angle[0], 360 ), 360 ),
             fmod( 360 + fmod( angle[1], 360 ), 360 ),
             fmod( 360 + fmod( angle[2], 360 ), 360 ) };
    //remove below to make code same as for phaser
    if (res[1] > 180) res[1] -= 360; //beta angle between -180,180
    return res;
  }

  double
  euler_alpha_plus_gamma(const dvect3& angle)
  {
    return std::fabs(angle[1]) < 90. ?
      regularize_angle(angle[0]+angle[2]) :
      regularize_angle(angle[0]-angle[2]);
  }

  double
  euler_delta_deg(const dvect3& euler1,const dvect3& euler2)
  {
    dmat33 rmat1 = scitbx::math::euler_angles::zyz_matrix(euler1[0],euler1[1],euler1[2]);
    dmat33 rmat2 = scitbx::math::euler_angles::zyz_matrix(euler2[0],euler2[1],euler2[2]);
    dmat33 dmat = rmat2*rmat1.transpose();
    double cosdel = rotation_angle_cosine(dmat);
    double rad = std::acos(cosdel);
    return scitbx::rad_as_deg(rad);
  }

  double
  matrix_delta_rad(const dmat33& rmat1,const dmat33& rmat2)
  {
    dmat33 dmat = rmat2*rmat1.transpose();
    double cosdel = rotation_angle_cosine(dmat);
    double rad = std::acos(cosdel);
    //return scitbx::rad_as_deg(rad);
    return rad;
  }

  double
  matrix_delta_deg(const dmat33& rmat1,const dmat33& rmat2)
  {
    dmat33 dmat = rmat2*rmat1.transpose();
    double cosdel = rotation_angle_cosine(dmat);
    double rad = std::acos(cosdel);
    return scitbx::rad_as_deg(rad);
  }

// Function that computes the smallest angle in degrees between two 3D vectors
  double
  axis_delta_deg(const dmat33& rmat1,const dmat33& rmat2)
  {
    dvect3 v1,v2; double chi1,chi2;
    std::tie(v1,chi1) = axis_and_angle_deg(rmat1);
    std::tie(v2,chi2) = axis_and_angle_deg(rmat2);
    // Compute the dot product of v1 and v2
    double dot = v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];

    // Compute the magnitudes of v1 and v2
    double mag1 = std::sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
    double mag2 = std::sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);

    // Calculate the cosine of the angle using the dot product formula
    double cosTheta = dot / (mag1 * mag2);

    // Clamp the cosine value to the valid range [-1, 1] to handle floating-point inaccuracies
    if (cosTheta > 1.0)  cosTheta = 1.0;
    if (cosTheta < -1.0) cosTheta = -1.0;

    // Compute the angle in radians and then convert to degrees
    double angle = std::acos(cosTheta);
    angle *= (180.0 / scitbx::constants::pi);
    angle = std::fabs(angle);
    if (angle>90) angle = 180-angle;
    return angle;
  }

} //end namespace phasertng
