#ifndef __phasertng_interp_point_enum__
#define __phasertng_interp_point_enum__

namespace phasertng {
namespace interp {
  enum point {
              FOUR,
              EIGHT,
              TRICUBIC,
           };
}}
#endif
