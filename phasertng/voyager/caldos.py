import warnings
# Suppress all DeprecationWarnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

import sys, os, os.path, traceback
from libtbx.phil import parse
sys.path.append(os.path.join(os.path.dirname(__file__),"..",".."))
from phasertng import Phasertng,InputCard,PhaserError
from phasertng import enum_out_stream
from phasertng.phil.master_node_file import node_separator
from phasertng.phil.master_auto_file import build_auto_str
from phasertng.phil.build_voyager_phil_file import build_voyager_phil_file
from phasertng.phil.converter_registry import converter_registry
from phasertng.phil.phil_to_cards import phil_to_cards
from phasertng.scripts.pdbset import *
from phasertng.run.ModeRunner import *
import argparse

def caldos(user_phil_str="",outsel=6):
  return call_inner_function(user_phil_str=user_phil_str,outsel=outsel,inner_function=inner_caldos)

def inner_caldos(basetng,user_phil_str,progress_callback=None):
  #{{{
  bfactors = [] # returned
  filetags = [] # returned
  import datetime
  from timeit import default_timer as timer
  start = timer()
  print("Start: ",datetime.datetime.now(),flush=True)
  debug = False
  try:
    try:
      #this throws an error if you eg try to put in a cards file
      user_phil = parse(user_phil_str,converter_registry=converter_registry)
    except Exception:
      raise ValueError(enum_err_code.python,"Error in phil parsing: check phil syntax")
    #combined master phil file
    initparams = build_voyager_phil_file(program_name="caldos")
    initparams = initparams +  "\n" +  build_auto_str("caldos")
    assert(len(initparams))
    master_phil = parse(initparams,converter_registry=converter_registry)
    #master_phil.show() #libtbx.phil.scope object
    working_phil,unused = master_phil.fetch(sources=[user_phil],track_unused_definitions=True)
    #remove the keywords starting with output that are added by the program template
    unused = [x for x in unused if not str(x).startswith("output.")]
    unknown = "Error in phil parsing" if len(unused) else ""
    for object_locator in unused:
       unknown = unknown + "\nunknown: " +  str(object_locator)
    if len(unknown):
       raise ValueError(enum_err_code.python,unknown)
    working_params = working_phil.extract()

    #chk does not contribute to the DAG
    working_params.phasertng.mode = ["chk"] # for starters. also the default

    #phil_to_cards selects out only the phasertng cards with the program name
    working_params.phasertng.reflections.read_anomalous = False
    user_cards = phil_to_cards(user_phil=working_params,program_name="InputCard")

    run_silent = False
    if run_silent:
      #permanently turn off all the file writing, and only output the non-database file
      dryrun_cards = silent_cards("process")
    else: #don't generate the database directory or clear the xml file
      dryrun_cards = ""
      dbdir = working_params.phasertng.suite.database
      assert(dbdir is not None)
      if not os.path.isdir(dbdir):
        os.makedirs(dbdir)
      xmlout = working_params.phasertng.graph_report.filestem + ".xml"
      xmlout = os.path.join(dbdir,xmlout)
      xmlout = os.path.abspath(os.path.join(dbdir, xmlout))
      #clear the tracker before we start
      if os.path.exists(xmlout):
        os.remove(xmlout)

    basetng.input = InputCard()
    ignore_unknown_keywords = True
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords)
    basetng.set_user_input_for_suite(basetng.input) #required to silence the Voyager caldos output
    DAGDB = basetng.DAGDATABASE

    loop_auto = str("Caldos")
    basetng.logLoopOpen(enum_out_stream.process,loop_auto,1,
           "Voyager loop")
    basetng.logLoopNext(enum_out_stream.process,loop_auto,
      "" if working_params.phasertng.title is None else working_params.phasertng.title)

    #make a sorted list of just the interesting bits, the id and tag
    ModeRunner(basetng=basetng,mode_list=['walk'])

    #first choice
    hklin = working_params.caldos.hklin
    if hklin is None:
      #second choice
      hklin = isNone(basetng.input.get_as_str(".phasertng.hklin.filename."))
    if hklin is None and int(basetng.input.size(".phasertng.file_discovery.validated.mtz_files.")) == 1:
      #third choice
      hklin = basetng.input.get_as_str_i(".phasertng.file_discovery.validated.mtz_files.",0)
    if hklin is not None: #set to the requirement
      basetng.input.set_as_str(".phasertng.hklin.filename.",str(hklin))

    model = working_params.caldos.xyzin
    if model is None:
      n = basetng.input.size(".phasertng.file_discovery.validated.pdb_files.")
      if n > 0:
        ifile = isNone(basetng.input.get_as_str_i(".phasertng.file_discovery.validated.pdb_files.",int(0)))
        basetng.input.set_as_str(".phasertng.model.filename.",str(ifile)) #first in list
        model = isNone(basetng.input.get_as_str(".phasertng.model.filename."))
    if model is None:
      basetng.logLoopExit(enum_out_stream.process,"No model")
      raise ValueError(enum_err_code.fatal,"No model")

    basetng.input.set_as_str(".phasertng.biological_unit.model.filename.",str(model))
    ModeRunner(basetng=basetng,mode_list=['imtz','data','aniso','feff','cca','ccs'])
    basetng.input.generic_int = ord('G') #11
    ModeRunner(basetng=basetng,mode_list=['join'])
    filenames = basetng.input.generic_string.split(node_separator)
    basetng.logLoopInfo(enum_out_stream.process,"Number of residues is " + str(len(filenames)))
    assert(len(filenames))
  # for i,pdbf in enumerate(sorted(filenames[:2])):
    for i,pdbf in enumerate(filenames):
        tag,pdbstr = pdbf.split(":unique_separator\n")
        basetng.input.generic_string = pdbstr
        basetng.logLoopInfo(enum_out_stream.process,pdbstr)
        basetng.input.set_as_str(".phasertng.pdbout.tag.",tag) #not model id
      # basetng.input.set_as_str(".phasertng.pdbout.id.",str(i+1))
        basetng.input.set_as_def(".phasertng.pdbout.id.")
        basetng.input.set_as_def(".phasertng.pdbout.pdbid.")
        basetng.input.set_as_def(".phasertng.model.filename.")
        basetng.input.set_as_str(".phasertng.molecular_transform.preparation.",str("interpolation monostructure"))
        basetng.input.set_as_str(".phasertng.molecular_transform.fix.",str(True))
        basetng.input.set_as_str(".phasertng.molecular_transform.boxscale.",str(20))
        basetng.input.set_as_str(".phasertng.molecular_transform.skip_bfactor_analysis.",str(True))
        ModeRunner(basetng=basetng,mode_list=['esm'])
        ModeRunner(basetng=basetng,mode_list=['put']) #via search.id
        basetng.input.set_as_str(".phasertng.put_solution.clear.",str(False)) #yes after first, clear rfac model
    basetng.input.set_as_str(".phasertng.macrbr.macrocycle1.",str("bfac"))
    basetng.input.generic_int = ord('H') #9
    ModeRunner(basetng=basetng,mode_list=['rbr'])
    basetng.input.set_as_str(".phasertng.rigid_body_maps.keep_chains.",str(True))
    if len(basetng.DataBase()) > 0:
      basetng.input.set_as_str(".phasertng.suite.write_files.",str(True))
      ModeRunner(basetng=basetng,mode_list=['rbm']) # makes single file
  # ModeRunner(basetng=basetng,mode_list=['rfac']) #requires files on disk
    bfactors = basetng.input.generic_string.split()
    basetng.input.generic_int = ord('H') #9
    basetng.input.generic_string = ""
    for i,bfac in enumerate(bfactors):
      #tags is the info about the residue
      assert(i < len(filenames))
      tag,pdbstr = filenames[i].split(":unique_separator\n")
      basetng.input.generic_string = basetng.input.generic_string + tag +  " bfac=" + str(bfac) + "\n"
    ModeRunner(basetng=basetng,mode_list=['join'])
    node_number = basetng.DAGDATABASE.PATHWAY.ULTIMATE.identifier()
    if progress_callback is not None:
      progress_callback.emit(node_number)
    basetng.logLoopShut(enum_out_stream.process,loop_auto)
    text = voyager_readme(dbdir)
    basetng.logLoopInfo(enum_out_stream.process,text)
    ignore_unknown_keywords = False
    basetng.input.parse_multi("",user_cards,dryrun_cards,ignore_unknown_keywords)
    basetng.set_user_input_for_suite(basetng.input) #special parse
    if len(basetng.DataBase()) > 0:
      basetng.logLoopInfo(enum_out_stream.process,"B-factors in pdb file: " + str(basetng.input.get_as_str(".phasertng.pdbout.filename.")))
    else:
      basetng.logLoopInfo(enum_out_stream.process,"Define database to report B-factors in pdb file")
    basetng.logLoopInfo(enum_out_stream.process,"B-factors as python list: " + str(bfactors))
  except AssertionError:
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb) # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[-1]
    print('')
    print('An error occurred on line {} in statement {}'.format(line, text))
    print('')
  except ValueError as e:
    error = PhaserError()
    error.Set(str(e.args[0]).upper(),e.args[1])
    if e.args[0] == enum_err_code.python: #raise from script
      basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  except Exception as e:
    rawmsg = "" if str(e) is None else str(e)
    #the phasertng error type is encoded in the return as an integer (enum)
    error = PhaserError()
    error.Set("PYTHON",rawmsg)
    basetng.logError(enum_out_stream.concise,error)
    print("Exit Code:",int(error.exit_code()),":",error.error_message())
  final = timer()
  print("Finish:",datetime.datetime.now(),flush=True)
  print("Elapsed: %.2f secs" % round(final-start,2),flush=True)
  if run_silent:
    for i,bfac in enumerate(bfactors):
      if (i < len(filetags)):
        print(filetags[i],"bfac=",bfac)
  return basetng, filetags, bfactors
  #}}}

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-p',"--phil",help="phil file",type=argparse.FileType('r'),required=False)
  parser.add_argument('-i',"--input",help="directory path walked for data files",required=False)
  parser.add_argument('-o',"--output",help="database directory for output",required=False)
  args = parser.parse_args()
  user_phil_str = ""
  if (args.phil is None and args.input is None):
    print("At least one of either --phil or --input must be specified")
    sys.exit(0)
  if (args.phil is None and args.output is None):
    print("At least one of either --phil or --output must be specified")
    sys.exit(0)
  if (args.phil is not None):
    user_phil_str = args.phil.read()
  verbose = False
  if (args.input is not None):
    if verbose: print("Input Directory: " + args.input,flush=True)
    if not (os.path.exists(args.input)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndirectory=\"" + str(args.input)+"\"\n"
  if (args.output is not None):
    if verbose: print("Output Directory: " + args.output,flush=True)
    if not (os.path.exists(args.output)):
      os.mkdir(args.output)
    if not (os.path.exists(args.output)):
      print("Directory cannot be opened")
      sys.exit(0)
    user_phil_str = user_phil_str + "\ndatabase=\"" + str(args.output)+"\"\n"
  user_phil_str = user_phil_str.replace("\n\n","\n")
  if verbose: print("Phil File: \n" + user_phil_str,flush=True)
  if (args.phil is not None or (args.input is not None and args.output is not None)):
    caldos( user_phil_str=user_phil_str)
  else:
    print("command line arguments insufficient",flush=True)
    sys.exit(0)
