#ifndef PHASER_NCS_ENSEMBLESYMMETRY_H
#define PHASER_NCS_ENSEMBLESYMMETRY_H

#include "PointGroup.h"
#include "NCSAssembly.h"

namespace phaser {  namespace ncs {

class EnsembleSymmetry
{
    private:
        iotbx::pdb::hierarchy::root root_;
        std::vector< NCSAssembly > assemblies_;
        bool assemblies_up_to_date_;
        float min_seq_coverage_;
        float min_seq_identity_;
        float max_rmsd_;
        double angular_;
        double spatial_;

        // Helper methods
        void determine_assemblies();

    public:
        EnsembleSymmetry(
            float min_sequence_coverage = 0,
            float min_sequence_identity = 0,
            float max_rmsd = 0,
            double angular_tolerance = 0,
            double spatial_tolerance = 0
            );

        ~EnsembleSymmetry();

        // Calculation
        PointGroup get_point_group();
        scitbx::vec3< double > get_centre_of_assembly() const;

        // Accessors
        const iotbx::pdb::hierarchy::root& get_root() const;
        const std::vector< NCSAssembly >& get_assemblies();
        float get_min_sequence_coverage() const;
        float get_min_sequence_identity() const;
        float get_max_rmsd() const;
        double get_angular_tolerance() const;
        double get_spatial_tolerance() const;

        void set_root(const iotbx::pdb::hierarchy::root& r);
        void set_min_sequence_coverage(float value);
        void set_min_sequence_identity(float value);
        void set_max_rmsd(float value);
        void set_angular_tolerance(double value);
        void set_spatial_tolerance(double value);

};

}  } // namespace phaser::ncs

#endif /*PHASER_NCS_ENSEMBLESYMMETRY_H*/
