import io, copy, os.path
import itertools # import dependency
import iotbx.bioinformatics # import dependency
from libtbx.utils import  multi_out
from  iotbx.file_reader import any_file
import mmtbx.validation.sequence
from cctbx.array_family import flex
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
from iotbx.command_line import pdb_as_fasta
import iotbx.pdb

class TextIO(io.StringIO):
  def write(self, data):
    try:
      if not isinstance(data, unicode):
        data = unicode(data, getattr(self, '_encoding', 'UTF-8'), 'replace')
    except Exception:
      data = str(data)
    return io.StringIO.write(self, data)

def tuple_to_str(perm):
  perm = list(perm)
  incremented = []
  for i in perm:
    incremented.append(int(i)+1)
  permstr = str(tuple(incremented)).replace(",)",")") #.replace(", ","+")
  return str(permstr)

def list_to_str(permlist):
  incremented = []
  for perm in permlist:
    incremented.append(tuple_to_str(perm))
  return str(incremented).replace("'","")

def nresidues(pdb_hierarchy):
  nres = 0
  if (len(pdb_hierarchy.models()) == 1):
    for model in pdb_hierarchy.models():
      for chain in model.chains():
         for rg in chain.residue_groups():
           nres += 1
  return nres

def generate_list_of_pdb_hierarchy_by_chain(pdb_hierarchy):
  #alignment only works with one-model hierarchy
  if len(pdb_hierarchy.models()) != 1:
    sel_cache = pdb_hierarchy.atom_selection_cache()
    first_model = sel_cache.selection("model 1")
    pdb_hierarchy = pdb_hierarchy.select(first_model)
  sel_cache = pdb_hierarchy.atom_selection_cache()
  #just real residues
  sel_cache = pdb_hierarchy.atom_selection_cache()
  nothetero = sel_cache.selection("not hetero")
  pdb_hierarchy = pdb_hierarchy.select(nothetero)
  structure_probe = pdb_hierarchy
  #just interesting protein, but this excludes rna and dna sequences
  #note that nucleotide/peptide selection is phenix.refine syntax
  #sel_cache = pdb_hierarchy.atom_selection_cache()
  #pep = sel_cache.selection("pepnames")
  #pdb_hierarchy = pdb_hierarchy.select(pep)
  structure_probe = pdb_hierarchy
  nres = 0
  chain_type = False # default is false so that if there are no models in the selection it fails later
  #merging all into one chain removes the sequence for the non-main conformer and replaces with X
  #see ribosome
  chains = []
  if (len(structure_probe.models()) == 1): # ie not zero
    for model in structure_probe.models():
      for chain in model.chains():
         chains.append(chain.id)
       # print("array of chains",chain.id,chain.as_padded_sequence())
  #just real residues
  harray= []
  for chain in chains:
    sel_cache = pdb_hierarchy.atom_selection_cache()
    sel = sel_cache.selection("chain \'" + chain + "\'") # blank chain identifier
    newh = pdb_hierarchy.select(sel)
    harray.append(newh)
  return harray

def write_pdb_fasta(pdbf,alignment_params):
  ssname = pdbf + ".fa"
  nseq = int(0)
  nnres = []
  alignment_params.pdb_as_fasta.file_name = []
  alignment_params.pdb_as_fasta.file_name.append(pdbf)
  alignment_params.pdb_as_fasta.output_file = ssname
  of = pdb_as_fasta.run(params=alignment_params)
  seqobj1 = any_file(of)
  if seqobj1.file_type == "seq":
    nseq = len(seqobj1.file_content)
    for n in seqobj1.file_content:
      nnres.append(len(n.sequence))
   #seq_in = any_file(params.input.seq_file, force_type="seq")
  return ssname,nseq,nnres

def sequence_alignment_validation(pdb_hierarchy,sequences,alignment_params,verbose=False):
    assert(len(sequences) == 1) # list, but just pick one
    out_wrapper = multi_out()
    dict_results = {}
    try:
      v_class = mmtbx.validation.sequence.validation(
            pdb_hierarchy=pdb_hierarchy,
            sequences=sequences,
            params=alignment_params,
            log=out_wrapper)
    except Exception as e:
      dict_results["failed_to_align"] = str(e)
      return dict_results # holder for failure
    dict_results["failed_to_align"] = "" #flag for full screen
    #chain is pdb chain which has been hacked to all one chain
    assert(len(v_class.chains) == 1)
    chain = v_class.chains[0]
    breakage = int(0)
    match_length = 0.
    iprev = None
    for i in chain.alignment.match_codes:
      if i == 'm':
        match_length = match_length+1
        if iprev is not None and iprev != 'm':
          breakage = breakage + 1
      if iprev is not None or i == 'm':
        iprev = i
    import re
    dict_results['breakages'] = int(breakage)
    dict_results['match_length'] = int(match_length)
    dict_results['seqid'] = 100.*chain.alignment.calculate_sequence_identity(skip_chars=['X'])
  # nrespdb = nresidues(pdb_hierarchy_by_chain)
    nrespdb = nresidues(pdb_hierarchy)
    dict_results['nres'] = nrespdb
   #replace iiii*  with i in match_codes and count the length, to give coverage of the structure NOT seq
    matchlen = len(re.sub(r'(i)\1+', r'\1', chain.alignment.match_codes))
   #dict_results['coverage'] = 100.*match_length/float(len(chain.alignment.match_codes))
    dict_results['coverage'] = 100.*match_length/float(matchlen)
    dict_results['match_percent'] = 100.*match_length/float(nrespdb)
    dict_results['match_codes'] = chain.alignment.match_codes
    lines = [] #for log output
    lines.append("Match percent = " + str(int(dict_results['match_percent'])) + " [" + str(int(match_length)) + "/" + str(nrespdb) + "]")
    lines.append("Sequence identity in contiguous segment = " + str(int(dict_results['seqid'])))
    lines.append("Coverage = " + str(int(dict_results['coverage'])))
    lines.append("Number of residues in structure = " + str(int(nrespdb)))
    lines.append("Match codes")
    tmp = format_sequence(chain.alignment.match_codes)
    for line in tmp:
      lines.append(line)
    if verbose:
      lines.append("Alignment")
      outstr = TextIO()
      v_class.show(out=outstr)
      tmp = outstr.getvalue().split("\n")
      for t in tmp:
        lines.append(str(t))
    dict_results['output'] = lines
    return dict_results

def format_sequence(sequence):
    line_width = int(60)
    seq = copy.deepcopy(sequence)
    lines = []
    while len(seq) > int(0):
        lines.append(seq[0:line_width])
        seq = seq[line_width:]
    return lines

def setup_alignment_params():
    alignment_params = pdb_as_fasta.master_phil.fetch().extract()
    alignment_params.pdb_as_fasta.pad_missing_residues = False
    alignment_params.pdb_as_fasta.include_insertion_residues = False
    alignment_params.pdb_as_fasta.ignore_missing_residues_at_start = True
    return alignment_params

def cleanup_hierarchy(pdb_hierarchy):
      #clean up the structure, one model, no hetero, ignore not protein or nucleic
      if len(pdb_hierarchy.models()) != 1:
          sel_cache = pdb_hierarchy.atom_selection_cache()
          first_model = sel_cache.selection("model 1")
          pdb_hierarchy = pdb_hierarchy.select(first_model)
      for m in pdb_hierarchy.models():
        for c in m.chains():
          for a in c.atoms():
            a.uij = (-1,-1,-1,-1,-1,-1) #convert all atoms to isotropic
      sel_cache = pdb_hierarchy.atom_selection_cache()
      nothetero = sel_cache.selection("not hetero")
      pdb_hierarchy = pdb_hierarchy.select(nothetero)
      if (not len(pdb_hierarchy.models()[0].chains()[0].atoms())):
        raise Exception("No atoms in selection")
      unknown = []
      #can happen if sequence is UNK residues eg 4v8o ribosome
     #sel_cache = pdb_hierarchy.atom_selection_cache()
     #selall = sel_cache.selection("all") #copy all, there must be a better way to copy
      # delete chains from the copy
      for chain in pdb_hierarchy.models()[0].chains():
          main_conf = chain.conformers()[0]
          if not (main_conf.is_na() or main_conf.is_protein()):
            unknown.append(chain.id)
            #below seems to work even though this is looping and removing at the same time
            pdb_hierarchy.models()[0].remove_chain(chain=chain)
           #selall.models()[0].remove_chain(chain=chain)
      return len(pdb_hierarchy.models()[0].chains()),unknown,pdb_hierarchy

def get_pdb_file_input(phaser_out,
                       found_pdb,
                       io_directory=""):
    new_found_pdb = []
    pdb_file_input = []
    for ifile in found_pdb:
      pdbf = os.path.join(io_directory,ifile)
      pdbobj = iotbx.pdb.input(pdbf)
      #clean up the structure, one model, no hetero, ignore not protein or nucleic
      pdb_hierarchy = pdbobj.construct_hierarchy()
      if len(pdb_hierarchy.models()) != 1:
          sel_cache = pdb_hierarchy.atom_selection_cache()
          first_model = sel_cache.selection("model 1")
          pdb_hierarchy = pdb_hierarchy.select(first_model)
      sel_cache = pdb_hierarchy.atom_selection_cache()
      nothetero = sel_cache.selection("not hetero")
      pdb_hierarchy = pdb_hierarchy.select(nothetero)
     #print(len(pdb_hierarchy.models()[0].chains()))
      unknown = []
      #can happen if sequence is UNK residues eg 4v8o ribosome
      #sel_cache = pdb_hierarchy.atom_selection_cache()
      #selall = sel_cache.selection("all") #copy all, there must be a better way to copy
      # delete chains from the copy
      for chain in pdb_hierarchy.models()[0].chains():
          main_conf = chain.conformers()[0]
          if not (main_conf.is_na() or main_conf.is_protein()):
            unknown.append(chain.id)
            #below seems to work even though this is looping and removing at the same time
            pdb_hierarchy.models()[0].remove_chain(chain=chain)
      if (len(pdb_hierarchy.models()[0].chains()) > 0): #not all removed
        pdb_file_input.append([ifile,pdb_hierarchy,unknown])
        new_found_pdb.append(ifile)
    pdb_file_input.sort(key = lambda x: x[0]) #key needed because hierarchy does not have < defined
    #this is so that the files are processed in same order every time,
    #unsorted order seems to depend on create date?
    #replaced the found_pdb with the good set for the sequence analysis
    return pdb_file_input,new_found_pdb

#renumber the chains to A and the residues sequentially so that alignment
#is not by chain
#hack the protein into a single chain
def pdb_models_as_single_chain(pdb_hierarchy,ffname=None,testing_flag=False):
  #alignment only works with one-model hierarchy
  if len(pdb_hierarchy.models()) != 1:
    sel_cache = pdb_hierarchy.atom_selection_cache()
    first_model = sel_cache.selection("model 1")
    pdb_hierarchy = pdb_hierarchy.select(first_model)
  sel_cache = pdb_hierarchy.atom_selection_cache()
  #just real residues
  sel_cache = pdb_hierarchy.atom_selection_cache()
  nothetero = sel_cache.selection("not hetero and not water") #water will give wrong number of nres
  pdb_hierarchy = pdb_hierarchy.select(nothetero)
  structure_probe = pdb_hierarchy
  #just interesting protein, but this excludes rna and dna sequences
  #note that nucleotide/peptide selection is phenix.refine syntax
  #sel_cache = pdb_hierarchy.atom_selection_cache()
  #pep = sel_cache.selection("pepnames")
  #pdb_hierarchy = pdb_hierarchy.select(pep)
  structure_probe = pdb_hierarchy
  nres = 0
  chain_type = False # default is false so that if there are no models in the selection it fails later
  assert(len(structure_probe.models()) == 1)
  for model in structure_probe.models():
    for chain in model.chains():
         chain.id = "A"
         for rg in chain.residue_groups():
           nres += 1
           rg.resseq=nres
    #TER must be removed so that separate chains (with same chain id A) written
    datap = structure_probe.as_pdb_string().replace("TER\n","")
    if testing_flag: #write the file and read back
      try:
        #write and read the pdb file to get it in a single model
        pdbff = open(ffname, "w")
        #TER must be removed so that separate chains (with same chain id A) written
        pdbff.write(datap)
        pdbff.close()
        pdbobj1 = iotbx.pdb.input(ffname) # replace
        structure_probe = pdbobj1.construct_hierarchy() # replace
      except:
        pass
    else: #construct from the string only, no file intermediate
      pdbobj1 = iotbx.pdb.input(source_info=str("singlechain"),lines=flex.split_lines(str(datap)))
      structure_probe = pdbobj1.construct_hierarchy() # replace
    assert(len(structure_probe.models()[0].chains()) == 1)
    pdb_chain = structure_probe.models()[0].chains()[0]
    main_conf = pdb_chain.conformers()[0]
    chain_type = True # check chain type to avoid print statement in alignment
    if not (main_conf.is_na() or main_conf.is_protein()):
      chain_type = False
  return structure_probe,nres,chain_type

def random_rotation(coordinates, angle_limit):
    import numpy as np
    import time
    from scipy.spatial.transform import Rotation
    np.random.seed(int(time.time()))
    # Calculate the center of mass
    center_of_mass = np.mean(coordinates, axis=0)
    # Subtract the center of mass to make rotations around it
    centered_coordinates = coordinates - center_of_mass
    # Generate a random rotation vector
    rotation_vector = np.random.uniform(-1, 1, size=3)
    # Normalize the rotation vector
    rotation_vector /= np.linalg.norm(rotation_vector)
    # Generate a random rotation angle within the limit
    rotation_angle = np.random.uniform(-angle_limit, angle_limit)
    # Create a rotation matrix
    rotation_matrix = Rotation.from_rotvec(rotation_vector * rotation_angle).as_matrix()
    # Apply the rotation to the centered coordinates
    rotated_coordinates = np.dot(centered_coordinates, rotation_matrix.T) + center_of_mass
    return rotated_coordinates
    '''
# Example usage:
# Generate some random 3D coordinates
coordinates = np.random.rand(10, 3)
# Define the angle limit for the rotation
angle_limit_degrees = 10  # Change this value as needed
# Convert degrees to radians
angle_limit_radians = np.radians(angle_limit_degrees)
# Apply the random rotation
print("Original Coordinates:")
print(coordinates)
print("\nRotated Coordinates:")
print(rotated_coordinates)
'''

def random_translation(coordinates, translation_limit):
    import numpy as np
    # Generate a random translation vector within the limit
    translation_vector = np.random.uniform(-translation_limit, translation_limit, size=3)
    # Add the translation vector to each coordinate
    translated_coordinates = coordinates + translation_vector
    return translated_coordinates
