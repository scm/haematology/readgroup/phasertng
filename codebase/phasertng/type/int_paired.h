//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __phasertng_type_int_paired_class__
#define __phasertng_type_int_paired_class__
#include <phasertng/type/optional.h>

namespace phasertng {
namespace type {

class int_paired : public optional
{
  std::pair<int,int> dephalt = {0,0};
  std::pair<int,int> uvalue = {0,0};
  bool pminset = false;
  bool pmaxset = false;
  int  pminval = 0;
  int  pmaxval = 0;

  public:
  const bool& minset() const { return pminset; }
  const int&  minval() const { return pminval; }
  const bool& maxset() const { return pmaxset; }
  const int&  maxval() const { return pmaxval; }

  int_paired() : optional() {}
  int_paired(
      const std::string parameter,
      const bool pminset_, const int pminval_,
      const bool pmaxset_, const int pmaxval_,
      const std::pair<int,int> dephalt_ = { DEF_INT_FLAG, DEF_INT_FLAG }
  ) ;
  void set_value(std::pair<int,int> other); //allow in place contructor {x,y}
  void set_value(const sv_string& vcards,std::pair<int,int> other);
  bool is_default() const;
  void set_default();
  std::pair<int,int> value_or_default() const;
  int value_or_default_min() const;
  int value_or_default_max() const;
  std::string unparse_parameter(bool phil) const;
  std::string show_default(bool phil) const;
  std::string to_python();
  void        from_python(std::string);

  bool operator==(const type::int_paired& other) const
  { return value_or_default() == other.value_or_default(); }

};

}}
#endif
