//(c) 2000-2024 Cambridge University Technical Services Ltd
//All rights reserved
#include <phasertng/math/likelihood/FourParameterSigmaA.h>
#include <phasertng/main/Assert.h>
#include <cmath>
#include <algorithm>
#include <iso646.h>

namespace phasertng {

  double FourParameterSigmaA::get(const double& SSQR) const
  {
    double DLuzzati = (RMSD==0) ? 1. : std::exp(-(TWOPISQ_ON_THREE)*(RMSD*RMSD)*SSQR);
    double Babinet(1-FSOL*std::exp(-BSOL*SSQR/4));
    //DLuzzati = std::max(BMIN,DLuzzati); cannot limit dluz, insensitive to rmsd
    Babinet = std::max(BMIN,Babinet);
    double siga(Babinet*DLuzzati);
    return siga;
  }

  double FourParameterSigmaA::worst_rms(const double config_hires)
  {
    double SSQR = pow2(1/config_hires);
    double Babinet(1-FSOL*std::exp(-BSOL*SSQR/4));
    double tol = 1.0e-06;
    double logmin = -std::log((SIGA_MIN+tol)/Babinet); //tolerance required
    PHASER_ASSERT(logmin>0); //from below
    double worst = std::sqrt(logmin/(TWOPISQ_ON_THREE*SSQR));
    //could return worst here but do a paranoia check first
    double tmp(RMSD);
    RMSD = worst;
    double siga = get(SSQR);
    PHASER_ASSERT(siga>=SIGA_MIN); //from below
    RMSD = tmp;
    return worst;
  }

} //phasertng
